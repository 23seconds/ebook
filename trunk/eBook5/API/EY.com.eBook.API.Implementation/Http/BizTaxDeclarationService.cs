﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.ComponentModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Core.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Translators;
using System.Web;
using System.Configuration;
using EY.com.eBook.API.Excel.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.Core;
using EY.com.eBook.API.BL.Read;
using EY.com.eBook.BL.Export;
#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BizTaxDeclarationService : IBizTaxDeclarationService
    {


        #region IBizTaxDeclarationService Members


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/Login")]
        public BizTaxPersonDataContract Login(CriteriaWinAccountDataContract cwadc)
        {
            
            return new BizTaxDeclarationHandler().Login(cwadc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetDeclarations")]
        public List<BizTaxDeclarationDataContract> GetDeclarations(CriteriaDepartmentPartnerMainstateDataContract cdpmdc)
        {
            return new BizTaxDeclarationHandler().GetDeclarations(cdpmdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetDeclarationsByClient")]
        public List<BizTaxDeclarationDataContract> GetDeclarationsByClient(CriteriaBaseIdDataContract cbidc)
        {
            return new BizTaxDeclarationHandler().GetDeclarationsByClient(cbidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/LoadNext")]
        public BizTaxDeclarationDataContract LoadNext(CriteriaDepartmentPartnerMainstateDataContract cdpmdc)
        {
            return new BizTaxDeclarationHandler().GetFirstAvailable(cdpmdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/RegisterFailure")]
        public void RegisterFailure(CriteriaIdKeyDataContract cikdc)
        {
            new BizTaxDeclarationHandler().RegisterFailure(cikdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/RegisterSuccess")]
        public void RegisterSuccess(CriteriaIdKeyDataContract cikdc)
        {
            new BizTaxDeclarationHandler().RegisterSuccess(cikdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/RegisterCalculation")]
        public void RegisterCalculation(CriteriaIdSimulationDataContract cisdc)
        {
            new BizTaxDeclarationHandler().RegisterCalculation(cisdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/UpdateStatus")]
        public void UpdateStatus(CriteriaIdStatusContract cisdc)
        {
            new BizTaxDeclarationHandler().UpdateStatus(cisdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/changeStatus")]
        public void ChangeStatus(List<CriteriaIdStatusContract> lcisdc)
        {
            new BizTaxDeclarationHandler().ChangeStatus(lcisdc);
        }

        #endregion
    }	
}
