﻿#region Using directives
using System;
using System.Xml.Linq;
using System.Web;
using System.Transactions;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Security.Principal;
using System.Runtime.Serialization.Json;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Excel.Data;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.Excel.BL;

#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// ExcelService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GlobalService : IGlobalService
    {
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetWorksheetTypes")]
        public List<WorksheetTypeDataContract> GetWorksheetTypes(CriteriaWorksheetTypesDataContract cwtdc)
        {
            return new BL.Read.GlobalRepository().GetWorksheetTypes(cwtdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetWorksheetHelpByID")]
        public List<WorksheetHelpDataContract> GetWorksheetHelpByID(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BL.Read.GlobalRepository().GetWorksheetHelpByID(cicdc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetMappingGroupItems")]
        public List<StringListItemDataContract> GetMappingGroupItems(CriteriaMappingDataContract cmdc)
        {
            return new BL.Read.MappingRepository().GetMappingGroupItems(cmdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetNewId")]
        public Guid GetNewId()
        {
            return Guid.NewGuid();
        }
    }	
}
