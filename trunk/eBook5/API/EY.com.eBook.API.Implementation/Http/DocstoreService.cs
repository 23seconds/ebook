﻿#region Using directives
using System;
using System.Xml.Linq;
using System.Web;
using System.Transactions;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Security.Principal;
using System.Runtime.Serialization.Json;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Excel.Data;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.Excel.BL;
using ProtoBuf.Meta;

#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// ExcelService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DocstoreService : IDocstoreService
    {      
        #region IDocstoreService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetDocumentInfo")]
        public RepositoryItemDataContract GetDocumentInfo(CriteriaIdDataContract cidc)
        {
            return new BL.Read.DocstoreRepository().GetDocumentInfo(cidc);
        }
        /*
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DocStoreLogin")]
        public void DocStoreLogin()
        {
            new BL.Read.DocstoreRepository().Login();
        }
        */
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFlexedTree")]
        public Stream GetFlexedTree(criteriaFlexedTreeDataContract cfdc)
        {
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(new BL.Read.DocstoreRepository().GetFlexedTree(cfdc));
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFlexedTrees")]
        public Stream GetFlexedTrees()
        {
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(new BL.Read.DocstoreRepository().GetFlexedTrees());
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetTeamMembersByClient")]
        public Stream GetTeamMembersByClient(CriteriaIdDataContract cidc)
        {
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(new BL.Read.DocstoreRepository().GetTeamMembersByClient(cidc));
            //return new BL.Read.DocstoreRepository().GetTeamMembersByClient(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/UploadDocument")]
        public string UploadDocument(criteriaDocstoreDocumentDataContract cdddc)
        {
            //WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            //return new MemoryStream(new BL.Read.DocstoreRepository().UploadDocument(cdddc));
            return new BL.Read.DocstoreRepository().UploadDocument(cdddc);
        }

        
        
        /*
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetCoefficientGroups")]
        public List<CoefficientGroupDataContract> GetCoefficientGroups(CriteriaCultureDataContract ccdc)
        {
            return new BL.Read.AdministrationRepository().GetCoefficientGroups(ccdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetCoefficientKeysInGroup")]
        public List<CoefficientKeyDataContract> GetCoefficientKeysInGroup(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BL.Read.AdministrationRepository().GetCoefficientKeysInGroup(cicdc);
        }   

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetCoefficientsInKeys")]
        public List<CoefficientDataContract> GetCoefficientsInKeys(CriteriaStringDataContract csdc)
        {
            return new BL.Read.AdministrationRepository().GetCoefficientsInKeys(csdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/CreateCoefficient")]
        public Guid CreateCoefficient(CriteriaCoefficientDataContract ccdc)
        {
           return new BL.Write.AdministrationRepository().CreateCoefficient(ccdc);
        }   

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DeleteCoefficient")]
        public void DeleteCoefficient(CriteriaBaseIdDataContract cbidc)
        {
            new BL.Write.AdministrationRepository().DeleteCoefficient(cbidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/UpdateCoefficient")]
        public void UpdateCoefficient(CriteriaCoefficientDataContract ccdc)
        {
            new BL.Write.AdministrationRepository().UpdateCoefficient(ccdc);
        }      

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/InsertNewChampion")]
        public void InsertNewChampion(CriteriaChampionDataContract ccdc)
        {
            new BL.Write.AdministrationRepository().InsertNewChampion(ccdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DeleteChampion")]
        public void DeleteChampion(CriteriaChampionDataContract ccdc)
        {
            new BL.Write.AdministrationRepository().DeleteChampion(ccdc);
        }

        
            [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/ReloadMainCache")]
        public bool ReloadMainCache()
        {
            return new BL.Read.AdministrationRepository().ReloadMainCache();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/RestartCachingService")]
        public Boolean RestartCachingService()
        {
            return new BL.Read.AdministrationRepository().RestartCachingService();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetClientEngagementLetterReport")]
        public List<ClientEngagementLetterDataContract> GetClientEngagementLetterReport(CriteriaClientEngagementLetterDataContract cceldc)
        {
            return new BL.Read.AdministrationRepository().GetClientEngagementLetterReport(cceldc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetWorksheets")]
        public List<WorksheetDataContract> GetWorksheets()
        {
            return new BL.Read.AdministrationRepository().GetWorksheets();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetWorksheetHelp")]
        public List<WorksheetHelpDataContract> GetWorksheetHelp(CriteriaBaseIdDataContract cidc)
        {
            return new BL.Read.AdministrationRepository().GetWorksheetHelp(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/InsertHelp")]
        public void InsertHelp(List<CriteriaWorksheetHelpDataContract> lcwhdc) 
        {
            new BL.Write.AdministrationRepository().InsertHelp(lcwhdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/UpdateHelp")]
        public void UpdateHelp(List<CriteriaWorksheetHelpDataContract> lcwhdc)
        {
            new BL.Write.AdministrationRepository().UpdateHelp(lcwhdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetWorksheetHelpById")]
        public List<WorksheetHelpDataContract> GetWorksheetHelpById(CriteriaBaseIdDataContract cidc)
        {
            return new BL.Read.AdministrationRepository().GetWorksheetHelpById(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/DeleteHelp")]
        public void DeleteHelp(CriteriaBaseIdDataContract cidc) 
        {
            new BL.Write.AdministrationRepository().DeleteHelp(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/UpdateHelpOrder")]
        public void UpdateHelpOrder(List<CriteriaIdOrderDataContract> lciodc)
        {
            new BL.Write.AdministrationRepository().UpdateHelpOrder(lciodc);
        }

        */
        

        #endregion
    }
}
