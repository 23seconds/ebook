﻿#region Using directives
using System;
using System.Xml.Linq;
using System.Web;
using System.Transactions;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Security.Principal;
using System.Runtime.Serialization.Json;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Excel.Data;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.API.BL.Translators;

#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BusinessRelationService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BusinessRelationService : IBusinessRelationService
    {

        #region IBusinessRelationService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportSuppliersProAcc")]
        public bool ImportSuppliersProAcc(CriteriaIdAndCultureDataContract cidc)
        {
            new BL.Write.BusinessRelationRepository().ImportSuppliersProAcc(cidc);
            return true;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportCustomersProAcc")]
        public bool ImportCustomersProAcc(CriteriaIdAndCultureDataContract cidc)
        {
            new BL.Write.BusinessRelationRepository().ImportCustomersProAcc(cidc);
            return true;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportBusinessRelationsProAcc")]
        public bool ImportBusinessRelationsProAcc(CriteriaIdAndCultureDataContract cidc)
        {
            new BL.Write.BusinessRelationRepository().ImportBusinessRelationsProAcc(cidc);
            return true;
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetSuppliers")]
        public List<SupplierDataContract> GetSuppliers(CriteriaIdDataContract cidc)
        {
            return new BL.Read.BusinessRelationRepository().GetSuppliers(cidc.Id);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetCustomers")]
        public List<CustomerDataContract> GetCustomers(CriteriaIdDataContract cidc)
        {
            return new BL.Read.BusinessRelationRepository().GetCustomers(cidc.Id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetSuppliersRange")]
        public ResponseDataContract<List<SupplierDataContract>> GetSuppliersRange(CriteriaBaseIdDataContract cbidc)
        {
            if (!cbidc.Start.HasValue && !cbidc.Limit.HasValue) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Pagin attributes not set" });
            return new BL.Read.BusinessRelationRepository().GetSuppliersRange(cbidc.Id, cbidc.Query, cbidc.Start.Value, cbidc.Limit.Value);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetCustomersRange")]
        public ResponseDataContract<List<CustomerDataContract>> GetCustomersRange(CriteriaBaseIdDataContract cbidc)
        {
            if (!cbidc.Start.HasValue && !cbidc.Limit.HasValue) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Pagin attributes not set" });
            return new BL.Read.BusinessRelationRepository().GetCustomersRange(cbidc.Id, cbidc.Query, cbidc.Start.Value, cbidc.Limit.Value);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetBusinessRelation")]
        public BusinessRelationDataContract GetBusinessRelation(CriteriaIdDataContract cidc)
        {
            return new BL.Read.BusinessRelationRepository().GetBusinessRelation(cidc.Id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetBusinessRelationList")]
        public List<BusinessRelationListDataContract> GetBusinessRelationList(CriteriaIdTypeDataContract citdc)
        {
            return new BL.Read.BusinessRelationRepository().GetBusinessRelationList(citdc.Id, citdc.Type);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetCustomersList")]
        public ResponseDataContract<List<StringListItemDataContract>> GetCustomersList(CriteriaBaseIdDataContract cbidc)
        {
            return new BL.Read.BusinessRelationRepository().GetPagedBusinessRelationList(cbidc, "C");
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetSuppliersList")]
        public ResponseDataContract<List<StringListItemDataContract>> GetSuppliersList(CriteriaBaseIdDataContract cbidc)
        {
            return new BL.Read.BusinessRelationRepository().GetPagedBusinessRelationList(cbidc, "S");
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetBusinessRelationListRange")]
        public ResponseDataContract<List<BusinessRelationListDataContract>> GetBusinessRelationListRange(CriteriaBaseIdTypeDataContract cbitdc)
        {
            if (!cbitdc.Start.HasValue && !cbitdc.Limit.HasValue) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Pagin attributes not set" });
            return new BL.Read.BusinessRelationRepository().GetBusinessRelationListRange(cbitdc.Id, cbitdc.Type, cbitdc.Query, cbitdc.Start.Value, cbitdc.Limit.Value);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SaveBusinessRelationDataContract")]
        public BusinessRelationDataContract SaveBusinessRelationDataContract(BusinessRelationDataContract brdc)
        {
            return new BL.Write.BusinessRelationRepository().SaveBusinessRelationDataContract(brdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DeleteBusinessRelation")]
        public void DeleteBusinessRelation(CriteriaIdDataContract cidc)
        {
            new BL.Write.BusinessRelationRepository().DeleteBusinessRelation(cidc.Id);
        }

        #endregion
    }	
}
