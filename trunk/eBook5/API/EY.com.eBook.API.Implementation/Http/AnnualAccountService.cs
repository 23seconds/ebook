﻿#region Using directives
using System;
using System.Xml.Linq;
using System.Web;
using System.Transactions;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Security.Principal;
using System.Runtime.Serialization.Json;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Excel.Data;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.API.BL.Read;
using EY.com.eBook.Core.EF.Read;

#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// FileService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AnnualAccountService : IAnnualAccountService
    {

        #region IAnnualAccountService Members
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/CreateAnnualAccounts")]
        public bool CreateAnnualAccounts(CriteriaFileAnnualAcountsDataContract cfaadc)
        {
            return new BL.Write.AnnualAccountsRepository().CreateAnnualAccounts(cfaadc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/UpdateAnnualAccounts")]
        public bool UpdateAnnualAccounts(CriteriaFileAnnualAcountsDataContract cfaadc)
        {
            return new BL.Write.AnnualAccountsRepository().UpdateAnnualAccounts(cfaadc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/UpdateRepositoryItem")]
        public bool UpdateRepositoryItem(CriteriaFileAnnualAcountsDataContract cfaadc)
        {
            return new BL.Write.AnnualAccountsRepository().UpdateRepositoryItem(cfaadc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFormOverview")]
        public List<FormDataContract> GetFormOverview(CriteriaIdDataContract cidc)
        {
            return new BL.Read.AnnualAccountsRepository().GetFormOverview(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetAnnualAccounts")]
        public AnnualAccountDataContract GetAnnualAccounts(CriteriaIdDataContract cidc)
        {
            return new BL.Read.AnnualAccountsRepository().GetAnnualAccounts(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetEngagementCodes")]
        public List<EngagementCodeDataContract> GetEngagementCodes(CriteriaIdDataContract cidc)
        {
            return new BL.Read.AnnualAccountsRepository().GetEngagementCodes(cidc);
        }
        #endregion
    }
}
