﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.ComponentModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Core.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Translators;
using System.Web;
using System.Configuration;
using EY.com.eBook.API.Excel.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.Core;
using EY.com.eBook.API.BL.Read;
using EY.com.eBook.BL.Export;
#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class OutputService : IOutputService
    {


        #region IOutputService Members
        /* fred
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GenerateLeadSheets")]
        public string GenerateLeadSheets(CriteriaIdAndCultureDataContract cidc)
        {
            return new ITextRepository().GenerateLeadsheets(cidc);
        }
         * */

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetWorksheetPrint")]
        public string GetWorksheetPrint(WorksheetItemDataContract widc)
        {
            return new ITextRepository().GetWorksheetPrint(widc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetDocumentPrint")]
        public string GetDocumentPrint(DocumentItemDataContract didc)
        {
            return new ITextRepository().GetDocumentPrint(didc);
        }

         [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetBiztaxFichePreview")]
        public string GetBiztaxFichePreview(CriteriaBiztaxFichePreviewDataContract cbfpdc)
        {
            return new ITextRepository().GenerateBiztaxFichePreview(cbfpdc);
        }

         [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/GetBiztaxTaxDetail")]
         public string GetBiztaxTaxDetail(CriteriaIdAndCultureDataContract cidc)
         {
             return new ITextRepository().GenerateBiztaxTaxDetail(cidc);
         }
        
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/ExportProAccBookings")]
        public string ExportProAccBookings(CriteriaExportBookingsDataContract cebdc)
        {
            
            cebdc.ActivePerson = new BusinessHelper().DecodePerson(cebdc.EncodedActivePerson);
            eBookWriteManager.Context.LogFileAction(cebdc.ActivePerson.Id, cebdc.FileId, "EXPORT PROACC", "");

            //return Path.GetFileName(path);
            return new ExportProAccBookings().CreatePackage(cebdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/ExportExactBookings")]
        public string ExportExactBookings(CriteriaExportBookingsDataContract cebdc)
        {

            cebdc.ActivePerson = new BusinessHelper().DecodePerson(cebdc.EncodedActivePerson);
            eBookWriteManager.Context.LogFileAction(cebdc.ActivePerson.Id, cebdc.FileId, "EXPORT PROACC", "");

            //return Path.GetFileName(path);
            return new ExportExactBookings().CreatePackage(cebdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/GetWorksheetExcelExport")]
        public string GetWorksheetExcelExport(CriteriaExportWorksheetExcelDataContract cewedc)
        {
            
            switch (cewedc.TypeId.ToString().ToUpper())
            {
                case "3E9BB601-EA23-4072-BD49-E5DC61A6B5A8": // ERELONEN
                    return new ExportErelonen().CreatePackage(cewedc.FileId);
                    break;
            }
            return string.Empty;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/ExportAccountMappings")]
        public string ExportAccountMappings(CriteriaIdAndCultureDataContract cicdc)
        {
            string result = new ExportAccountMappings().CreatePackage(cicdc.Id, cicdc.Culture);

            return string.Format("{0}/{1}", Config.PickupVirtualDir, result);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/ExportFinalTrial")]
        public string ExportFinalTrial(CriteriaIdAndCultureDataContract cicdc)
        {
            return new ExportFinalTrial().CreatePackage(cicdc.Id, cicdc.Culture);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetAnnualAccountsBundle")]
        public string GetAnnualAccountsBundle(CriteriaIdAndCultureDataContract cicdc)
        {
            return new ITextRepository().GetAnnualAccountsBundle(cicdc);
            //return "SUCCESS // ID: " + cicdc.Id + " // Culture: " + cicdc.Culture;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/ExportClientEngagementLetterReport")]
        public CriteriaIdDataContract ExportClientEngagementLetterReport(CriteriaClientEngagementLetterDataContract cceldc)
        {
            var clientEngagementLetterReportExcel = new ExportClientEngagementLetterReport(cceldc);
            return new CriteriaIdDataContract { Id = clientEngagementLetterReportExcel.Id };      
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/ExportClientEngagementLettersByPerson")]
        public CriteriaIdDataContract ExportClientEngagementLettersByPerson(CriteriaPersonEngagementLetterDataContract cpeldc)
        {
            var clientEngagementLetterReportExcel = new ExportClientEngagementLettersByPerson(cpeldc);
            return new CriteriaIdDataContract { Id = clientEngagementLetterReportExcel.Id };
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/ExportCodaPoa")]
        public CriteriaIdDataContract ExportCodaPoa(CriteriaCodaPoaDataContract ccpdc)
        {
            var codaPoaExcel = new ExportCodaPoa(ccpdc);
            return new CriteriaIdDataContract { Id = codaPoaExcel.Id };
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/ExportFileService")]
        public CriteriaIdDataContract ExportFileService(CriteriaClientPersonFileServiceDataContract ccpfsdc)
        {
            var fileServiceExcel = new ExportFileService(ccpfsdc);
            return new CriteriaIdDataContract { Id = fileServiceExcel.Id };
        }
        
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GenerateQuickprint")]
        public string GenerateQuickprint(CriteriaQuickprintDataContract cqdc)
        {
            return new ITextRepository().GenerateQuickprint(cqdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetConfirmationOfApprovalCITR")]
        public string GetConfirmationOfApprovalCITR(CriteriaIdAndDepartmentDataContract ciaddc)
        {
            return new ITextRepository().GetConfirmationOfApprovalCITR(ciaddc);
        }
        
        #endregion
    }	
}
