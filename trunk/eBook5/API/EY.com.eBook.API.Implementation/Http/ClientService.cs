﻿#region Using directives
using System;
using System.Xml.Linq;
using System.Web;
using System.Transactions;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Security.Principal;
using System.Runtime.Serialization.Json;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Excel.Data;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.API.BL.Translators;

#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// FileService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ClientService : IClientService
    {


        #region IClientService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
   RequestFormat = WebMessageFormat.Json,
   ResponseFormat = WebMessageFormat.Json,
   UriTemplate = "/GetClient")]
        public ClientDataContract GetClient(CriteriaIdDataContract cidc)
        {
            return new BL.Read.ClientRepository().GetClient(cidc.Id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
   RequestFormat = WebMessageFormat.Json,
   ResponseFormat = WebMessageFormat.Json,
   UriTemplate = "/GetClientGfis")]
        public ClientDataContract GetClientGfis(CriteriaIdDataContract cidc)
        {
            return new BL.Read.ClientRepository().GetClient(cidc.Id, false);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
   RequestFormat = WebMessageFormat.Json,
   ResponseFormat = WebMessageFormat.Json,
   UriTemplate = "/GetClientBase")]
        public ClientBaseDataContract GetClientBase(CriteriaIdDataContract cidc)
        {
            return new BL.Read.ClientRepository().GetClientBase(cidc.Id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
   RequestFormat = WebMessageFormat.Json,
   ResponseFormat = WebMessageFormat.Json,
   UriTemplate = "/GetClientBaseGfis")]
        public ClientBaseDataContract GetClientBaseGfis(CriteriaIdDataContract cidc)
        {
            return new BL.Read.ClientRepository().GetClientBase(cidc.Id, false,Guid.Empty);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
   RequestFormat = WebMessageFormat.Json,
   ResponseFormat = WebMessageFormat.Json,
   UriTemplate = "/GetClientInfo")]
        public ClientInfoDataContract GetClientInfo(CriteriaIdDataContract cidc)
        {
            BL.Read.ClientRepository cr = new BL.Read.ClientRepository();
            return new ClientInfoDataContract
            {
                Gfis = cr.GetClient(cidc.Id, false)
                ,
                Overrule = cr.GetClientOverrule(cidc.Id)
            };
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
   RequestFormat = WebMessageFormat.Json,
   ResponseFormat = WebMessageFormat.Json,
   UriTemplate = "/GetClientTeam")]
        public List<TeamMemberDataContract> GetClientTeam(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BL.Read.ClientRepository().GetClientTeam(cicdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetClientTeamDepartment")]
        public List<TeamMemberDataContract> GetClientTeamDepartment(CriteriaIdAndDepartmentDataContract ciddc)
        {
            return new BL.Read.ClientRepository().GetClientTeamDepartment(ciddc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetClientDepartments")]
        public List<SimpleStringListDataContract> GetClientDepartments(CriteriaIdDataContract cidc)
        {
            return new BL.Read.ClientRepository().GetClientDepartments(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetClientDepartmentPartners")]
        public List<SimpleGuidListDataContract> GetClientDepartmentPartners(CriteriaIdAndDepartmentDataContract ciddc)
        {
            return new BL.Read.ClientRepository().GetClientDepartmentPartners(ciddc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetClientPartnerByDepartment")]
        public List<PersonBaseDataContract> GetClientPartnerByDepartment(CriteriaIdAndDepartmentDataContract ciddc)
        {
            return new BL.Read.ClientRepository().GetClientPartnerByDepartment(ciddc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetClientEngagementLettersByPerson")]
        public ResponseDataContract<List<ClientEngagementLetterDataContract>> GetClientEngagementLettersByPerson(CriteriaPersonEngagementLetterDataContract cpeldc)
        {
            return new BL.Read.ClientRepository().GetClientEngagementLettersByPerson(cpeldc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetClientExactBanks")]
        public RepositoryMetaConfigDataContract GetClientExactBanks(CriteriaIdKeyDataContract cidc)
        {
            return new BL.Read.ClientRepository().GetClientExactBanks(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AddBTW")]
        public bool AddBTW(BTWDataContract bdc)
        {
            return new BL.Write.ClientRepository().AddBTW(bdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SaveClientOverrule")]
        public bool SaveClientOverrule(ClientOverruleDataContract cdc)
        {
            return new BL.Write.ClientRepository().SaveClientOverrule(cdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SetPrintPreference")]
        public bool SetPrintPreference(CriteriaIdAndBoolDataContract ciabdc)
        {
            return new BL.Write.ClientRepository().SetPrintPreference(ciabdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetPrintPreference")]
        public bool? GetPrintPreference(CriteriaIdDataContract cidc)
        {
            return new BL.Read.ClientRepository().GetPrintPreference(cidc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/GetCodaPoaStatusList")]
        public List<StatusDataContract> GetCodaPoaStatusList(CriteriaIdBoolPersonDataContract cibpdc)
        {
            return new BL.Read.ClientCodaRepository().GetCodaPoaStatusList(cibpdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/GetBanks")]
        public List<BankDataContract> GetBanks()
        {
            return new BL.Read.ClientCodaRepository().GetBanks();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetCodaPoa")]
        public ResponseDataContract<List<CodaPoaDataContract>> GetCodaPoa(CriteriaCodaPoaDataContract ccpdc)
        {
            return new BL.Read.ClientCodaRepository().GetCodaPoa(ccpdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/UpdateCodaPoa")]
        public bool UpdateCodaPoa(UpdateCodaPoaDataContract ucpdc)
        {
            return new BL.Read.ClientCodaRepository().UpdateCodaPoa(ucpdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetCodaPoaLog")]
        public List<CodaPoaLogDataContract> GetCodaPoaLog(CriteriaCodaPoaLogDataContract ccpldc)
        {
            return new BL.Read.ClientCodaRepository().GetCodaPoaLog(ccpldc);
        }
        #endregion
    }	
}
