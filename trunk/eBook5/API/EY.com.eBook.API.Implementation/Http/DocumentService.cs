﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.ComponentModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Core.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Translators;
using System.Web;
using System.Configuration;
using EY.com.eBook.API.Excel.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.Core;
using EY.com.eBook.API.BL.Read;
#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DocumentService : IDocumentService
    {

        #region IDocumentService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetDocuments")]
        public List<DocumentListDataContract> GetDocuments(CriteriaFileDataContract cfdc)
        {
            return new BL.Read.DocumentRepository().GetDocuments(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetDocument")]
        public DocumentDataContract GetDocument(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BL.Read.DocumentRepository().GetDocument(cicdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/SaveDocument")]
        public DocumentDataContract SaveDocument(DocumentDataContract ddc)
        {
            new BL.Write.DocumentRepository().SaveDocument(ddc);
            return new BL.Read.DocumentRepository().GetDocument(new CriteriaIdAndCultureDataContract
            {
                Id = ddc.Id
                ,
                Culture = ddc.Culture
                ,
                ActivePerson = ddc.ActivePerson
                ,
                EncodedActivePerson = ddc.EncodedActivePerson
            });
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/DeleteDocument")]
        public void DeleteDocument(CriteriaIdDataContract cidc)
        {
            new BL.Write.DocumentRepository().DeleteDocument(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetDocumentType")]
        public DocumentTypeDataContract GetDocumentType(CriteriaDocumentDataContract cddc)
        {
            return new BL.Read.DocumentRepository().GetDocumentType(cddc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetDocumentTypes")]
        public List<DocumentTypeBaseDataContract> GetDocumentTypes(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BL.Read.DocumentRepository().GetDocumentTypes(cicdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetDocumentGroups")]
        public List<DocumentGroupDataContract> GetDocumentGroups(CriteriaCultureDataContract ccdc)
        {
            return new BL.Read.DocumentRepository().GetDocumentGroups(ccdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetBlockType")]
        public BlockTypeDataContract GetBlockType(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BL.Read.DocumentRepository().GetBlockType(cicdc);
        }

        #endregion
    }	
}
