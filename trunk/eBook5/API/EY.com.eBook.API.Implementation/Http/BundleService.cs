﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.ComponentModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Core.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Translators;
using System.Web;
using System.Configuration;
using EY.com.eBook.API.Excel.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.Core;
using EY.com.eBook.API.BL.Read;
using EY.com.eBook.BL.Export;
#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BundleService : IBundleService
    {

        #region IBundleService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetBundles")]
        public List<BundleListDataContract> GetBundles(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BundleRepository().GetBundles(cicdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetGenericBundles")]
        public List<BundleListDataContract> GetGenericBundles(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BundleRepository().GetGenericBundles(cicdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetServiceBundles")]
        public List<BundleListDataContract> GetServiceBundles(CriteriaFileServiceDataContract cfsdc)
        {
            return new BundleRepository().GetServiceBundles(cfsdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetItems")]
        public List<LibraryTreeNodeDataContract> GetItems(CriteriaLibraryDataContract cldc)
        {
            return new ITextRepository().GetItems(cldc.NodeId, cldc.Culture, cldc.FileId);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetBundleTemplates")]
        public List<BundleTemplateListDataContract> GetBundleTemplates(CriteriaCultureDataContract ccdc)
        {

            return new BundleRepository().GetBundleTemplates(ccdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetGenericBundleTemplates")]
        public List<BundleTemplateListDataContract> GetGenericBundleTemplates(CriteriaCultureDataContract ccdc)
        {
            return new BundleRepository().GetGenericBundleTemplates(ccdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetServiceBundleTemplates")]
        public List<BundleTemplateListDataContract> GetServiceBundleTemplates(CriteriaServiceCultureDataContract cscdc)
        {
            return new BundleRepository().GetServiceBundleTemplates(cscdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/SaveBundle")]
        public BundleDataContract SaveBundle(BundleDataContract bdc)
        {
            return new BundleRepository().SaveBundle(bdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetBundle")]
        public BundleDataContract GetBundle(Guid id)
        {
            return new BundleRepository().GetBundle(id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetBundleByTemplate")]
        public BundleDataContract GetBundleByTemplate(CriteriaFileBundleTemplateDataContract cfbtdc)
        {
            return new BundleRepository().GetBundleByTemplate(cfbtdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/DeleteBundle")]
        public bool DeleteBundle(Guid id)
        {
            return new BundleRepository().DeleteBundle(id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/CreateBundle")]
        public BundleDataContract CreateBundle(CriteriaCreateBundleDataContract ccbdc)
        {
            return new BundleRepository().CreateBundle(ccbdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/CreateAnnualAccountsDeliverable")]
        public BundleDataContract CreateAnnualAccountsDeliverable(CriteriaCreateBundlePersonDataContract ccbpdc)
        {
            return new BundleRepository().CreateAnnualAccountsDeliverable(ccbpdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GenerateBundle")]
        public string GenerateBundle(BundleDataContract bdc)
        {
            ITextRepository ir = new ITextRepository();
            string p = ir.GenerateBundle(bdc);

            return p;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GenerateBundleById")]
        public string GenerateBundleById(Guid id)
        {
            ITextRepository ir = new ITextRepository();
            string p = ir.GenerateBundle(id);

            return p;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/ValidateBundle")]
        public CheckBundleDataContract ValidateBundle(Guid id)
        {
            return new BundleRepository().ValidateBundle(id);
        }


        // RESULTEERD IN LIJST VAN NIET ONDERTEKENDE REPO ELEMENTEN. MOET DUS LEEF ZIJN VOOR HARD CLOSE
        // enkel File ID benodigd
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/CheckSignedRepos")]
        public List<string> CheckSignedRepos(CriteriaIdDataContract cidc)
        {
            return new BundleRepository().CheckSignedRepos(cidc);
        }

        #endregion
    }	
}
