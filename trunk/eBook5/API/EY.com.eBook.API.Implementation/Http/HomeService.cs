﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.ComponentModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Core.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Translators;
using System.Web;
using System.Configuration;
using EY.com.eBook.API.Excel.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.Core;
using EY.com.eBook.API.BL.Read;
#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class HomeService : IHomeService
    {

        #region IHomeService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetRecentFiles")]
        public List<RecentFileDataContract> GetRecentFiles(CriteriaClientDataContract ccdc)
        {
            return new HomeRepository().GetRecentFiles(ccdc);  
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetClients")]
        public ResponseDataContract<List<ClientBaseDataContract>> GetClients(CriteriaClientDataContract ccdc)
        {
            ResponseDataContract < List < ClientBaseDataContract >> t = new HomeRepository().GetClients(ccdc);
            return t;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetPersons")]
        public ResponseDataContract<List<PersonDataContract>> GetPersons(CriteriaBaseDataContract cbdc)
        {
            return new HomeRepository().GetPersons(cbdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetAllOffices")]
        public List<OfficeDataContract> GetAllOffices()
        {
            return new HomeRepository().GetAllOffices();
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/Login")]
        public PersonDataContract Login(CriteriaWinAccountDataContract cwadc)
        {
           
            //cwadc.WindowsAccount = cwadc.WindowsAccount.ToLower().Replace("betgov", "BEBGACCSPRE"); //begeaudmqua
            return new HomeRepository().Login(cwadc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetAllChampions")]
        public List<ChampionDataContract> GetAllChampions()
        {
            return new HomeRepository().GetAllChampions();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
     RequestFormat = WebMessageFormat.Json,
     ResponseFormat = WebMessageFormat.Json,
     UriTemplate = "/SetLastUsedRole")]
        public void SetLastUsedRole(CriteriaPersonRolesDataContract cprdc)
        {
            new BL.Write.HomeRepository().SetLastUsedRole(cprdc);
        }

        #endregion
    }	
}
