﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.ComponentModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Core.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Translators;
using System.Web;
using System.Configuration;
using EY.com.eBook.API.Excel.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.Core;
using EY.com.eBook.API.BL.Read;
#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ListService : IListService
    {

        #region IListService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetFodList")]
        public List<FodListDataContract> GetFodList(CriteriaFodListDataContract cfldc)
        {
            return new ListRepository().GetFodListItems(cfldc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetFodGlobalList")]
        public List<StringListItemDataContract> GetFodGlobalList(CriteriaFodListDataContract cfldc)
        {
            return new ListRepository().GetFodGlobalList(cfldc);
        }

        

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetLegalTypes")]
        public List<LegalTypeDataContract> GetLegalTypes(CriteriaCultureDataContract ccdc)
        {
            return new ListRepository().GetLegalTypes(ccdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/GetYears")]
        public List<StringListItemDataContract> GetYears(CriteriaYearsDataContract cydc)
        {
            return new ListRepository().GetYears(cydc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetBookyearMonths")]
        public List<StringListItemDataContract> GetBookyearMonths(CriteriaMonthListDataContract cmldc)
        {
            return new ListRepository().GetBookyearMonths(cmldc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetList")]
        public List<GuidListItemDataContract> GetList(CriteriaListDataContract cldc)
        {
            return new ListRepository().GetList(cldc);
        }


        #endregion

    }	
}
