﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Globalization;

namespace EY.com.eBook.API.Contracts
{
    public class DataContractReader<T> : IDisposable
    {
        public T Instance { get; set; }
        public CultureInfo Culture { get; set; }

        public DataContractReader(T instance)
        {
            Instance = instance;
            Culture = CultureInfo.InvariantCulture;
        }


        public DataContractReader(T instance, CultureInfo culture)
        {
            Instance = instance;
            Culture = culture;
        }

        public object GetTypedValue(string property)
        {

            PropertyInfo propInfo = Instance.GetType().GetProperty(property);
            if (propInfo != null)
            {
                return propInfo.GetValue(Instance, null);
            }
            return null;

        }

        public string GetValue(string property, string format)
        {


            object val = GetTypedValue(property);
            if (val == null) return string.Empty;
            switch (val.GetType().Name.ToLower())
            {
                case "datetime":
                    if (!string.IsNullOrEmpty(format))
                    {
                        return ((DateTime)val).ToString(format, Culture.DateTimeFormat);
                    }
                    return ((DateTime)val).ToString(CultureInfo.InvariantCulture.DateTimeFormat.ShortDatePattern, CultureInfo.InvariantCulture.DateTimeFormat);

                case "float":
                    if (!string.IsNullOrEmpty(format))
                    {
                        return ((float)val).ToString(format, Culture.NumberFormat);
                    }
                    return ((float)val).ToString();

                case "decimal":
                    if (!string.IsNullOrEmpty(format))
                    {
                        return ((decimal)val).ToString(format, Culture.NumberFormat);
                    }
                    return ((decimal)val).ToString();
                default:
                    return val.ToString();

            }
            return null;
        }

        #region IDisposable Members

        public void Dispose()
        {
            Instance = default(T);
            Culture = null;
            System.GC.SuppressFinalize(this);
        }

        #endregion
    }
}
