﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaWinAccountDataContract
    /// </summary>
    [DataContract]
    public class CriteriaWinAccountDataContract
    
{
        [DataMember]
        public string WindowsAccount { get; set; }

    }
}
