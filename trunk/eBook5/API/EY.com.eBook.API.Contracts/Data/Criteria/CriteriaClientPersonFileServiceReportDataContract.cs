﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaClientPersonFileServiceDataContract
    /// </summary>
    [DataContract]
    public class CriteriaClientPersonFileServiceDataContract : CriteriaBaseDataContract
    {
        [DataMember(Name = "cn")]
        public string ClientName { get; set; }

        [DataMember(Name = "ed")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "dm")]
        public DateTime? DateModified { get; set; }

        [DataMember(Name = "sid")]
        public string StatusIds { get; set; }

        [DataMember(Name = "pid")]
        public Guid? PersonId { get; set; }

        [DataMember(Name = "lmb")]
        public Guid? LastModifiedBy { get; set; } 

        [DataMember(Name = "sf")]
        public string SortField { get; set; }

        [DataMember(Name = "so")]
        public string SortOrder { get; set; }

        [DataMember(Name = "of")]
        public string Office { get; set; }

        [DataMember(Name = "sv")]
        public Guid Service { get; set; }

        [DataMember(Name = "dp")]
        public string Department { get; set; }

        [DataMember(Name = "nf")]
        public bool? NewestFile { get; set; }

        [DataMember(Name = "own")]
        public bool? Owned { get; set; }
    }

    
}
