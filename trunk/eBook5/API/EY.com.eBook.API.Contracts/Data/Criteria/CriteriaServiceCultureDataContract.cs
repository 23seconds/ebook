﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaServiceCultureDataContract
    /// </summary>
    [DataContract]
    public class CriteriaServiceCultureDataContract
    {
        /// <summary>
        /// ServiceId
        /// </summary>
        /// <value>The Service id</value>
        [DataMember(Order=1)]
        public Guid ServiceId { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

     


    }

    
}
