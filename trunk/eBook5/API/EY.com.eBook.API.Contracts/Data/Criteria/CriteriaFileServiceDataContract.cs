﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaFileServiceDataContract
    /// </summary>
    [DataContract]
    public class CriteriaFileServiceDataContract
    {
        [DataMember(Order = 1)]
        public Guid FileId { get; set; }

        [DataMember(Order=2)]
        public Guid ServiceId { get; set; }

        [DataMember(Order=3)]
        public string Culture { get; set; }

        [DataMember(Order=4)]
        public string Order { get; set; }

        [DataMember(Order=5)]
        public Guid ClientId { get; set; }
    }

    
}
