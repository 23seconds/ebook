﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaFileServiceLogDataContract
    {
        [DataMember(Name = "fi")]
        public Guid FileId { get; set; }

        [DataMember(Name = "si")]
        public Guid ServiceId { get; set; }
    }
}