﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaFileServicePersonDataContract
    /// </summary>
    [DataContract]
    public class CriteriaFileServicePersonDataContract
    {
        [DataMember()]
        public Guid FileId { get; set; }

        [DataMember()]
        public Guid ServiceId { get; set; }

        [DataMember()]
        public Guid BundleId { get; set; }

        [DataMember()]
        public short? Status { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 

     


    }

    
}
