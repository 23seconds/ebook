﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaIdAndDepartmentDataContract
    /// </summary>
    [DataContract]
    public class CriteriaIdAndDepartmentDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Department{ get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember]
        public string Culture { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }

}
