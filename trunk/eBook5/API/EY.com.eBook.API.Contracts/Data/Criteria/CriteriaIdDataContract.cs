﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaIdDataContract
    /// </summary>
    [DataContract]
    public class CriteriaIdDataContract
    {
        /// <summary>
        /// Id
        /// </summary>
        /// <value>The Guid - Id.</value>
        [DataMember]
        public Guid Id { get; set; }

    }
}
