﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaCodaPoaDataContract : CriteriaBaseDataContract
    {
        [DataMember(Name = "ba")]
        public string BankAccount { get; set; }

        [DataMember(Name = "bic")]
        public string Bic { get; set; }

        [DataMember(Name = "lm")]
        public DateTime? LastModified { get; set; }

        [DataMember(Name = "sm")]
        public DateTime? StatusModified { get; set; }

        [DataMember(Name = "sid")]
        public int? StatusId { get; set; }

        [DataMember(Name = "ac")]
        public bool? AllowCoda { get; set; }

        [DataMember(Name = "cn")]
        public string ClientName { get; set; }

        [DataMember(Name = "bid")]
        public string BankId { get; set; }

        [DataMember(Name = "sf")]
        public string SortField { get; set; }

        [DataMember(Name = "so")]
        public string SortOrder { get; set; }

        [DataMember(Name = "pid")]
        public Guid PersonId { get; set; }

        [DataMember(Name = "ar")]
        public string ActiveRole { get; set; }

        [DataMember(Name = "li")]
        public bool? EOL_EYLicense { get; set; }

        [DataMember(Name = "dm")]
        public bool? EOL_DigMailbox { get; set; }

        [DataMember(Name = "aca")]
        public bool? activeClientACR { get; set; }
    }
}