﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaFileDataContract
    /// </summary>
    [DataContract]
    public class CriteriaFileBundleTemplateDataContract
    {
        /// <summary>
        /// File ID
        /// </summary>
        /// <value>The file id</value>
        [DataMember(Order=1)]
        public Guid FileId { get; set; }

        [DataMember(Order=2)]
        public Guid TemplateId { get; set; }
    }

    
}
