﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaBaseDataContract
    /// </summary>
    [DataContract]
    public class CriteriaWorksheetTypesDataContract
    {
        /// <summary>
        /// The culture to use
        /// </summary>
        /// <value>Culture.</value>
        [DataMember]
        public string Culture { get; set; }

        /// <summary>
        /// The type (null = all active)
        /// </summary>
        /// <value>Type.</value>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// The FileId
        /// </summary>
        /// <value>FileId.</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }
}
