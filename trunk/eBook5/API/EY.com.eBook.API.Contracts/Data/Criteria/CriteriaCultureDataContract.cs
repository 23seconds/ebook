﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaCultureDataContract
    /// </summary>
    [DataContract]
    public class CriteriaCultureDataContract
    {
        /// <summary>
        /// Culture
        /// </summary>
        /// <value>The Culture</value>
        [DataMember]
        public string Culture { get; set; }
    }
}
