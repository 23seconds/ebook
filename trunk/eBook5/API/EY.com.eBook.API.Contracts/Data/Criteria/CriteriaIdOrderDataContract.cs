﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaIdStatusDataContract
    /// </summary>
    [DataContract]
    public class CriteriaIdOrderDataContract
    {

        [DataMember]
        public Guid Id { get; set; }


        [DataMember]
        public int Order { get; set; }

    }
}
