﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaFileAnnualAcountsDataContract
    /// </summary>
    [DataContract]
    public class CriteriaFileAnnualAcountsDataContract
    {
        [DataMember(Order = 1)]
        public Guid FileId { get; set; }

        [DataMember(Order = 2)]
        public Guid? RepositoryItemId { get; set; }

        [DataMember(Order = 3)]
        public DateTime? DefinitiveGeneralAssembly { get; set; }

        [DataMember(Order = 4)]
        public string CompanyType { get; set; }

        [DataMember(Order = 5)]
        public string Model { get; set; }

        [DataMember(Order = 6)]
        public bool? ChangesAfterGeneralAssembly { get; set; }

        [DataMember(Order = 7)]
        public string ChangesAfterGeneralAssemblyDescription { get; set; }

        [DataMember(Order = 8)]
        public string EngagementCode { get; set; }

        [DataMember(Order = 9)]
        public string InvoiceLanguage { get; set; }

        [DataMember(Order = 10)]
        public string InvoiceRechargeFilingExpense { get; set; }

        [DataMember(Order = 11)]
        public decimal? InvoiceAmount { get; set; }

        [DataMember(Order = 12)]
        public string InvoiceStructuredMessage { get; set; }

        [DataMember(Order = 13)]
        public Guid? InvoiceRepositoryItemId { get; set; }
    }    
}
