﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaIdStatusDataContract
    /// </summary>
    [DataContract]
    public class CriteriaIdAndBoolDataContract
    {

        [DataMember]
        public Guid Id { get; set; }


        [DataMember]
        public bool Boolean { get; set; }

    }
}
