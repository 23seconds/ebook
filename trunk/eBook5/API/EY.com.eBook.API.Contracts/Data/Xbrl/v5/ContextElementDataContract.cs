﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    
    [DataContract]
    public class ContextElementDataContract
    {
        [DataMember(Order = 1)]
        public string Id { get; set; } // change with automated code?

        [DataMember(Order = 2)]
        public bool AutoRendered { get; set; }

        [DataMember(Order = 3)]
        public ContextEntityDataContract Entity { get; set; }

        [DataMember(Order = 4)]
        public ContextPeriodDataContract Period { get; set; }

        [DataMember(Order = 5)]
        public List<ContextScenarioDataContract> Scenario {get;set;}
    }
}
