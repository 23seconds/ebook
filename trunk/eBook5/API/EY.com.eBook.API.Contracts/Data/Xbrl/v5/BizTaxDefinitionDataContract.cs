﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class BizTaxDefinitionDataContract
    {
        [DataMember]
        public int AssessmentYear { get; set; }

        [DataMember]
        public string DefaultNamespace { get; set; }

        [DataMember]
        public List<ContextElementDataContract> DefaultContexts {get;set;}

        [DataMember]
        public List<XbrlElementDataContract> DefaultElements { get; set; } // ????

        [DataMember]
        public List<UnitElementDataContract> Units { get; set; }


    }
}
