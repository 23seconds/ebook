﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class BizTaxDataContract
    {
        [DataMember(Order=1)]
        public string AssessmentYear { get; set; }

        [DataMember(Order = 2)]
        public ContextEntityDataContract EntityIdentifier { get; set; } // used in ALL contexts

        [DataMember(Order = 3)]
        public List<ContextElementDataContract> Contexts { get; set; }

        [DataMember(Order = 4)]
        public List<UnitElementDataContract> Units { get; set; }

        [DataMember(Order = 5)]
        public List<XbrlElementDataContract> Elements { get; set; }

        [DataMember(Order = 6)]
        public DateTime? LastCalculated { get; set; }

        [DataMember(Order = 7)]
        public DateTime? LastSaved { get; set; }

        [DataMember(Order = 8)]
        public Guid? Partner { get; set; }

        [DataMember(Order = 9)]
        public Guid? ProxyId { get; set; }

        [DataMember(Order = 10)]
        public string ProxyTitle { get; set; }

        /*[DataMember(Order = 9)]
        public Guid? Partner { get; set; }*/
    }
}
