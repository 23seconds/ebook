﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlDataContract
    {
        [DataMember(Order=1)]
        public Guid FileId { get; set; }

        [DataMember(Order = 2)]
        public string Type { get; set; } // used in ALL contexts

        [DataMember(Order = 3)]
        public BizTaxDataContract Data { get; set; }

        [DataMember(Order = 4)]
        public string Calculation { get; set; }

        [DataMember(Order = 5)]
        public bool Validated { get; set; }

        [DataMember(Order = 6)]
        public int Status { get; set; }

        [DataMember(Order = 7)]
        public bool Locked { get; set; }

        [DataMember(Order = 8)]
        public List<XbrlHistoryDataContract> History { get; set; }

        [DataMember(Order = 9, Name="department")]
        public string DeclaringDepartment { get; set; }

        [DataMember(Order = 10, Name = "partnerId")]
        public Guid? DeclaringPartnerId { get; set; }

        [DataMember(Order = 11, Name = "partnerName")]
        public string DeclaringPartnerName { get; set; }

        [DataMember(Order = 12, Name = "person")]
        public string EncodedActivePerson { get; set; }
    }

    [DataContract]
    public class XbrlHistoryDataContract
    {
        [DataMember(Order=1)]
        public Guid FileId {get;set;}

        [DataMember(Order=2)]
        public string Type {get;set;}

        [DataMember(Order=3)]
        public DateTime TimeStamp {get;set;}

        [DataMember(Order=4)]
        public Guid PersonId {get;set;}

        [DataMember(Order=5)]
        public string PersonFullName {get;set;}

        [DataMember(Order=6)]
        public int ActionId {get;set;}

        [DataMember(Order=7)]
        public string ActionText {get;set;}
    }
}
