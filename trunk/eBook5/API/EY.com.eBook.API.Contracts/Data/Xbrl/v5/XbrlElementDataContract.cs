﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    
   
    [KnownType(typeof(ContextScenarioDataContract))]
    [KnownType(typeof(UnitElementDataContract))]
    [ProtoBuf.ProtoInclude(101, typeof(ContextScenarioDataContract))]
    [ProtoBuf.ProtoInclude(102, typeof(UnitElementDataContract))]
    [DataContract]
    public class XbrlElementDataContract
    {
        [DataMember(Order = 1)]
        public bool AutoRendered { get; set; }

        [DataMember(Order = 2)]
        public virtual string Prefix { get; set; } // should be namespace only
        [DataMember(Order = 3)]
        public virtual string NameSpace { get; set; }
        [DataMember(Order = 4)]
        public virtual string Name { get; set; }
        [DataMember(Order = 5)]
        public virtual string ContextRef {
            get
            {
                if (string.IsNullOrEmpty(Context))
                {
                    return string.Format("{0}", Period);
                }
                return string.Format("{0}__{1}", Period, Context);
            }
            set
            {
            }
        }

        [DataMember(Order = 6)]
        public virtual string Decimals { get; set; }
        [DataMember(Order = 7)]
        public virtual string UnitRef { get; set; }

        [DataMember(Order = 8)]
        public virtual string Value { get; set; }

        [DataMember(Order = 9)]
        public List<XbrlElementDataContract> Children { get; set; }

        [DataMember(Order = 10)]
        public virtual string Id
        {
            get
            {
                if (string.IsNullOrEmpty(ContextRef))
                {
                    return string.Format("{0}_{1}", Prefix, Name);
                }
                return string.Format("{0}_{1}_{2}", Prefix, Name, ContextRef);
            }
            set
            {
            }
        }

        [DataMember(Order = 11)]
        public virtual string Context { get; set; }

        [DataMember(Order = 12)]
        public virtual string Period { get; set; }

        [DataMember(Order = 13)]
        public virtual List<IndexItemBaseDataContract> BinaryValue { get; set; }

    }


    public class PanelXbrlElementDataContract : XbrlElementDataContract
    {

    }
}
