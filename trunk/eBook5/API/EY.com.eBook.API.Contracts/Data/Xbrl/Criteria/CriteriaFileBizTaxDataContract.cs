﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaFileDataContract
    /// </summary>
    [DataContract]
    public class CriteriaFileBizTaxDataContract
    {
        /// <summary>
        /// File ID
        /// </summary>
        /// <value>The file id</value>
        [DataMember(Order=1)]
        public Guid FileId { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public BizTaxDataContract BizTax { get; set; }
    }

    [DataContract]
    public class CriteriaXbrlDataContract
    {
        /// <summary>
        /// File ID
        /// </summary>
        /// <value>The file id</value>
        [DataMember(Order = 1)]
        public Guid FileId { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public XbrlDataContract Xbrl { get; set; }
    }

    
}
