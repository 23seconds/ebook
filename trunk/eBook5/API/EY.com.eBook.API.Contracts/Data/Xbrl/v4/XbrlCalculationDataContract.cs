﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlCalculationDataContract
    {
        [DataMember(Name = "eBook")]
        public decimal? eBook { get; set; }

        [DataMember(Name = "biztax")]
        public decimal? BizTax { get; set; }
    }
}
