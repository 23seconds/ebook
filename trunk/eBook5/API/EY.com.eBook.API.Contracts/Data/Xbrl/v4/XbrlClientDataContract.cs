﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlClientDataContract
    {
        [DataMember(Name="n")]
        public string Name { get; set; }

        [DataMember(Name = "enr")]
        public string EnterpriseNumber { get; set; }


        [DataMember(Name = "cenr")]
        public string CountryEnterpriseNumber { 
            get { return string.Format("BE{0}", EnterpriseNumber); }
            set { }
        }
        

        [DataMember(Name = "lti")]
        public string LegalTypeId { get; set; }

        [DataMember(Name = "foda")]
        public FodAddressDataContract Address { get; set; }

        [DataMember(Name = "b")]
        public XbrlBankDataContract Bank {get;set;}
    }
}
