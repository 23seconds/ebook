﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlFichesDataContract
    {
        [DataMember(Name="2043")]
        public XbrlAttachmentDataContract Fiche_2043 {get;set;}
        [DataMember(Name="328K")]
        public XbrlAttachmentDataContract Fiche_328K { get; set; }
        [DataMember(Name="328L")]
        public XbrlAttachmentDataContract Fiche_328L { get; set; }
        [DataMember(Name="275B")]
        public XbrlAttachmentDataContract Fiche_275B {get;set;}
        [DataMember(Name="275C")]
        public XbrlAttachmentDataContract Fiche_275C {get;set;}
        [DataMember(Name="275F")]
        public XbrlAttachmentDataContract Fiche_275F {get;set;}
        [DataMember(Name="275K")]
        public XbrlAttachmentDataContract Fiche_275K {get;set;}
        [DataMember(Name="275P")]
        public XbrlAttachmentDataContract Fiche_275P {get;set;}
        [DataMember(Name="275U")]
        public XbrlAttachmentDataContract Fiche_275U {get;set;}
        [DataMember(Name="275R")]
        public XbrlAttachmentDataContract Fiche_275R {get;set;}
        [DataMember(Name="275W")]
        public XbrlAttachmentDataContract Fiche_275W {get;set;}
        [DataMember(Name="276K")]
        public XbrlAttachmentDataContract Fiche_276K {get;set;}
        [DataMember(Name="276N")]
        public XbrlAttachmentDataContract Fiche_276N {get;set;}
        [DataMember(Name="276P")]
        public XbrlAttachmentDataContract Fiche_276P {get;set;}
        [DataMember(Name="276T")]
        public XbrlAttachmentDataContract Fiche_276T {get;set;}
        [DataMember(Name="276W1")]
        public XbrlAttachmentDataContract Fiche_276W1 {get;set;}
        [DataMember(Name="276W2")]
        public XbrlAttachmentDataContract Fiche_276W2 {get;set;}
        [DataMember(Name="276W3")]
        public XbrlAttachmentDataContract Fiche_276W3 {get;set;}
        [DataMember(Name="276W4")]
        public XbrlAttachmentDataContract Fiche_276W4 {get;set;}

    }

}
