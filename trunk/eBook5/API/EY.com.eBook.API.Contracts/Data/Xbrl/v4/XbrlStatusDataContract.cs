﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlStatusDataContract
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "txt")]
        public string Description { get; set; }

    }
}
