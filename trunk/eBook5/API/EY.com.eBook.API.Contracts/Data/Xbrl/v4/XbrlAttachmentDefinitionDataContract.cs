﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlAttachmentDefinitionDataContract
    {
        [DataMember]
        public int AssessmentYear { get; set; }

        [DataMember]
        public string Node { get; set; }

        [DataMember]
        public bool Required { get; set; }

        [DataMember]
        public XElement Definition { get; set; }

    }
}
