﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlDefinitionDataContract
    {
        [DataMember]
        public int AssessmentYear { get; set; }

        [DataMember]
        public string DefinitionPath { get; set; }
    }
}
