﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlAttachmentDataContract
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }
       
        [DataMember(Name = "automated")]
        public bool Automated { get; set; }
        
        [DataMember(Name = "checked")]
        public bool Checked { get; set; }

        [DataMember(Name = "required")]
        public int Required { get; set; }
        
        [DataMember(Name = "idx")]
        public int Order { get; set; }
        
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "subText")]
        public string SubText { get; set; }

        [DataMember(Name = "items")]
        public List<IndexItemBaseDataContract> Items { get; set; }
    }
}
