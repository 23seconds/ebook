﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FileXbrlHistoryDataContract
    {
        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }

        [DataMember(Name = "dte")]
        public DateTime Date { get; set; }

        [DataMember(Name = "pid")]
        public Guid PersonId { get; set; }

        [DataMember(Name = "pfn")]
        public string PersonFullname { get; set; }

        [DataMember(Name = "aid")]
        public int ActionId { get; set; }

        [DataMember(Name = "atx")]
        public string Action { get; set; }

    }
}
