﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlBankDataContract
    {
        [DataMember(Name = "act")]
        public bool Active { get; set; }

        [DataMember(Name = "iban")]
        public string Iban { get; set; }

        [DataMember(Name = "bic")]
        public string Bic { get; set; }
    }
}
