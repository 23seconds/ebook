﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{

    


    [DataContract]
    public class FileXbrlDataContract
    {
        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }

        [DataMember(Name = "client")]
        public XbrlClientDataContract Client { get; set; }

        [DataMember(Name = "contact")]
        public FodContactDataContract ContactInfo { get; set; }

        [DataMember(Name = "fiches")]
        public List<XbrlAttachmentDataContract> Fiches { get; set; }

        [DataMember(Name = "divers")]
        public List<XbrlAttachmentDataContract> Divers { get; set; }

        [DataMember(Name = "info")]
        public List<XbrlAttachmentDataContract> Info { get; set; }

        [DataMember(Name = "status")]
        public XbrlStatusDataContract Status { get; set; }

        [DataMember(Name = "version")]
        public decimal Version { get; set; }

        [DataMember(Name = "calculation")]
        public XbrlCalculationDataContract Calculation { get; set; }

        [DataMember(Name = "validated")]
        public bool ValidatedByPartner { get; set; }

        [DataMember(Name = "resultfolder")]
        public string ResultFolder { get; set; }

        [DataMember(Name = "result")]
        public DeclarationResultDataContract Result { get; set; }

        [DataMember(Name = "history")]
        public List<FileXbrlHistoryDataContract> History { get; set; }
    }
}
