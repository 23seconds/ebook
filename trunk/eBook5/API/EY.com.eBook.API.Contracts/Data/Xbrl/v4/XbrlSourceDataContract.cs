﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlSourceDataContract
    {
        [DataMember]
        public WorksheetDataContract Declaration { get; set; }

        [DataMember]
        public List<XbrlAttachmentDataContract> Attachments { get; set; }

        

    }
}
