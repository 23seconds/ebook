﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlDiversDataContract
    {
        [DataMember(Name="nbb")]
        public XbrlAttachmentDataContract NBB { get; set; }

        [DataMember(Name = "rep")]
        public XbrlAttachmentDataContract Reports { get; set; }

        [DataMember(Name = "hom")]
        public XbrlAttachmentDataContract ExonerationProfit { get; set; }

        [DataMember(Name = "intacc")]
        public XbrlAttachmentDataContract InternalAccounts { get; set; }

        [DataMember(Name = "afshr")]
        public XbrlAttachmentDataContract DeductionTable { get; set; }

        [DataMember(Name = "other")]
        public XbrlAttachmentDataContract Other { get; set; }

    }
}
