﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FodListDataContract
    {
        [DataMember(Name="li")]
        public string ListId { get; set; }
        [DataMember(Name = "id")]
        public string ItemId { get; set; }
        [DataMember(Name = "c")]
        public string Culture { get; set; }
        [DataMember(Name = "d")]
        public string Description { get; set; }
        [DataMember(Name = "m")]
        public string Meta { get; set; }
        [DataMember(Name = "v")]
        public string Value { get; set; }
    }
}
