﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FodAddressDataContract
    {
        [DataMember(Name="at")]
        public FodListDataContract AddressType { get; set; }
        [DataMember(Name = "s")]
        public string Street { get; set; }
        [DataMember(Name = "n")]
        public string Nr { get; set; }
        [DataMember(Name = "b")]
        public string Bus { get; set; }
        [DataMember(Name = "co")]
        public FodListDataContract Country { get; set; }
        [DataMember(Name = "z")]
        public FodListDataContract ZipCode { get; set; }
        [DataMember(Name = "ci")]
        public string City { get; set; }

    }
}
