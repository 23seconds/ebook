﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FodPhoneDataContract
    {
        [DataMember(Name = "act")]
        public bool Active { get; set; }
        [DataMember(Name="cc")]
        public string CountryCode { get; set; }
        [DataMember(Name = "z")]
        public string Zone { get; set; }
        [DataMember(Name = "l")]
        public string LocalNr { get; set; }
        [DataMember(Name = "e")]
        public string ExtensionNr { get; set; }
    }
}
