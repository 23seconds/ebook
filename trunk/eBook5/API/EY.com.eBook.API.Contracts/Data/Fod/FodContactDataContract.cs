﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FodContactDataContract
    {
        
        [DataMember(Name="ct")]
        public FodListDataContract ContactType { get; set; }
        [DataMember(Name="f")]
        public string Function { get; set; }
        [DataMember(Name="n")]
        public string Name { get; set; }
        [DataMember(Name="fn")]
        public string FirstName { get; set; }
        [DataMember(Name="e")]
        public string Email { get; set; }

        [DataMember(Name = "a")]
        public FodAddressDataContract Address { get; set; }
        [DataMember(Name = "p")]
        public FodPhoneDataContract Phone { get; set; }
        [DataMember(Name = "fax")]
        public FodPhoneDataContract Fax { get; set; }
    }
}
