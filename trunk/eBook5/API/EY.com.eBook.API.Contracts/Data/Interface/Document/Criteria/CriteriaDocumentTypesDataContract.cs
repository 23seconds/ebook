﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaDocumentTypesDataContract
    /// </summary>
    [DataContract]
    public class CriteriaDocumentTypesDataContract
    {
        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the document type id.
        /// </summary>
        /// <value>The document type id.</value>
        [DataMember]
        public Guid? GroupId { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember]
        public string Culture { get; set; }  

    }
}
