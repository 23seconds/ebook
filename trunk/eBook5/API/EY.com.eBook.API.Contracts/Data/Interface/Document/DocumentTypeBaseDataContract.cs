﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// DocumentTypeBaseDataContract
    /// </summary>
    [KnownType(typeof(DocumentTypeDataContract))]
    [DataContract]
    public class DocumentTypeBaseDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        [DataMember(Name = "k")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is fully editable.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is fully editable; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "ife")]
        public bool IsFullyEditable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value><c>true</c> if this instance is active; otherwise, <c>false</c>.</value>
        [DataMember(Name = "ia")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }
    }
}
