﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// DocumentListDataContract
    /// </summary>
    [KnownType(typeof(DocumentDataContract))]
    [DataContract]
    public class DocumentListDataContract
    {

        /// <summary>
        /// Id
        /// </summary>
        /// <value>The document Id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// DocumentTypeId
        /// </summary>
        /// <value>The DocumentTypeId.</value>
        [DataMember(Name = "dtid")]
        public Guid DocumentTypeId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        /// <value>The Name.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }


        /// <summary>
        /// Culture
        /// </summary>
        /// <value>The Culture.</value>
        [DataMember(Name = "c")]
        public string Culture { get; set; }

    }
}
