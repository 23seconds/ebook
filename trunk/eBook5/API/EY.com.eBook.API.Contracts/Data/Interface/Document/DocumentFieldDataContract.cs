﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// DocumentFieldDataContract
    /// </summary>
    [DataContract]
    public class DocumentFieldDataContract
    {
        /// <summary>
        /// Gets or sets the DocumentId.
        /// </summary>
        /// <value>The DocumentId.</value>
        [DataMember(Name = "did")]
        public Guid DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        /// <value>The Name.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the DataType.
        /// </summary>
        /// <value>The DataType.</value>
        [DataMember(Name = "dt")]
        public string DataType { get; set; }

        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        /// <value>The Value.</value>
        [DataMember(Name = "v")]
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        /// <value>The Value.</value>
        [DataMember(Name = "dv")]
        public DateTime? DateValue
        {
            get
            {
                if (Value.GetType() == typeof(DateTime)) return (DateTime)Value;
                return null;
            }
            set { Value = value; }
        }
    }
}
