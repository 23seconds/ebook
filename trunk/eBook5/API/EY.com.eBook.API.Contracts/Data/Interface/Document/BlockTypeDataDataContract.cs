﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BlockTypeDataDataContract
    /// </summary>
    [DataContract]
    public class BlockTypeDataDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember(Name = "c")]
        public string Culture { get; set; }
        /// <summary>
        /// Gets or sets the block type ID.
        /// </summary>
        /// <value>The block type ID.</value>
        [DataMember(Name = "bt")]
        public Guid BlockTypeID { get; set; }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        [DataMember(Name = "ti")]
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>The text.</value>
        [DataMember(Name = "text")]
        public string Text { get; set; }
        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        /// <value>The order.</value>
        [DataMember(Name = "o")]
        public int Order { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [edit on start].
        /// </summary>
        /// <value><c>true</c> if [edit on start]; otherwise, <c>false</c>.</value>
        [DataMember(Name = "eos")]
        public bool EditOnStart { get; set; }
    }
}
