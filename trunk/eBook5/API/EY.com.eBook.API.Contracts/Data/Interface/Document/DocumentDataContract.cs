﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// DocumentDataContract
    /// </summary>
    [DataContract]
    public class DocumentDataContract : DocumentListDataContract
    {
       
        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the block container data.
        /// </summary>
        /// <value>The block container data.</value>
        [DataMember(Name = "bcd")]
        public List<BlockContainerDataContract> BlockContainerData { get; set; }

        /// <summary>
        /// Gets or sets the block container data.
        /// </summary>
        /// <value>The block container data.</value>
        [DataMember(Name = "df")]
        public List<DocumentFieldDataContract> Fields { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 

    }
}
