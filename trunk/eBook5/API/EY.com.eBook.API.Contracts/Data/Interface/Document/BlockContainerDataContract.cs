﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BlockContainerDataContract
    /// </summary>
    [DataContract]
    public class BlockContainerDataContract
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the blocks.
        /// </summary>
        /// <value>The blocks.</value>
        [DataMember(Name = "bs")]
        public List<BlockDataDataContract> Blocks { get; set; }

    }
}
