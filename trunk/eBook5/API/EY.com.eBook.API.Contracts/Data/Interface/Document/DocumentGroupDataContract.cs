﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// DocumentGroupDataContract
    /// </summary>
    [DataContract]
    public class DocumentGroupDataContract 
    {
       
        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the block container data.
        /// </summary>
        /// <value>The block container data.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }

    }
}
