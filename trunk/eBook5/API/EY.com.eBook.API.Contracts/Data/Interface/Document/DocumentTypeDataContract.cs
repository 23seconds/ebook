﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// DocumentTypeDataContract
    /// </summary>
    [DataContract]
    public class DocumentTypeDataContract : DocumentTypeBaseDataContract
    {
        // <summary>
        /// Gets or sets the template
        /// </summary>
        /// <value>The template.</value>
        [DataMember(Name = "tmp")]
        public string Template { get; set; }

        // <summary>
        /// Gets or sets the block types
        /// </summary>
        /// <value>The block types.</value>
        [DataMember(Name = "bts")]
        public List<BlockTypeDataContract> BlockTypes { get; set; }

    }
}
