﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BlockDataDataContract
    /// </summary>
    [DataContract]
    public class BlockDataDataContract
    {
        /// <summary>
        /// Gets or sets the Order.
        /// </summary>
        /// <value>The Order.</value>
        [DataMember(Name = "o")]
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>The Title.</value>
        [DataMember(Name = "ti")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Text.
        /// </summary>
        /// <value>The Text.</value>
        [DataMember(Name = "text")]
        public string Text { get; set; }

    }
}

