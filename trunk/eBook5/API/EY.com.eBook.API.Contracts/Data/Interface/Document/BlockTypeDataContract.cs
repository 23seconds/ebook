﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BlockTypeDataContract
    /// </summary>
    [DataContract]
    public class BlockTypeDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        [DataMember(Name = "k")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "ie")]
        public bool IsEditable { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember(Name = "c")]
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "n")]
        public string ShortName { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "fn")]
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the block type data.
        /// </summary>
        /// <value>The block type data.</value>
        [DataMember(Name = "bt")]
        public List<BlockTypeDataDataContract> BlockTypeData { get; set; }
    }
}
