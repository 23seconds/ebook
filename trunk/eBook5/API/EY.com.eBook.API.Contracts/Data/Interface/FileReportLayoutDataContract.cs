﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// FileReportsListDataContract
    /// </summary>
    [DataContract]
    public class FileReportsListDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the type of the name.
        /// </summary>
        /// <value>The type of the name.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the culture.
        /// </summary>
        /// <value>The type of the culture.</value>
        [DataMember(Name = "c")]
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the type of the culture.
        /// </summary>
        /// <value>The type of the culture.</value>
        [DataMember(Name = "cl")]
        public bool Closed { get; set; }

        /// <summary>
        /// Gets or sets ClosingDepartment
        /// </summary>
        /// <value>ClosingDepartment.</value>
        [DataMember(Name = "cldp")]
        public string ClosingDepartment { get; set; }


    }

    /// <summary>
    /// FileReportLayoutDataContract
    /// </summary>
    [DataContract]
    public class FileReportLayoutDataContract
    {

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the type id.
        /// </summary>
        /// <value>The type id.</value>
        [DataMember(Name = "tid")]
        public Guid TypeId { get; set; }

        /// <summary>
        /// Gets or sets the type of the layout.
        /// </summary>
        /// <value>The type of the layout.</value>
        [DataMember(Name = "c")]
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the layout.
        /// </summary>
        /// <value>The layout.</value>
        [DataMember(Name = "ly")]
        public ReportDataContract Layout { get; set; }


        /// <summary>
        /// Gets or sets the type of the name.
        /// </summary>
        /// <value>The type of the name.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Closed
        /// </summary>
        /// <value>Closed.</value>
        [DataMember(Name = "cl")]
        public bool Closed { get; set; }

        /// <summary>
        /// Gets or sets ClosingDepartment
        /// </summary>
        /// <value>ClosingDepartment.</value>
        [DataMember(Name = "cldp")]
        public string ClosingDepartment { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }

}
