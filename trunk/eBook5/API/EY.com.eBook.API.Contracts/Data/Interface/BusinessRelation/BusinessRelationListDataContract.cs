﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BusinessRelationDataContract
    /// </summary>
    [DataContract]
    [KnownType(typeof(CustomerListDataContract))]
    [KnownType(typeof(SupplierListDataContract))]
    public class BusinessRelationListDataContract
    {
        /// <summary>
        /// Gets/sets the id
        /// </summary>
        /// <value>Businessrelation id.</value>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets/sets the client id
        /// </summary>
        /// <value>Businessrelation client id.</value>
        [DataMember(Name="cid")]
        public Guid ClientId { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [DataMember(Name="n")]
        public string Name { get; set; }

    }

    /// <summary>
    /// CustomerListDataContract
    /// </summary>
    [DataContract]
    public class CustomerListDataContract : BusinessRelationListDataContract
    {
    }

    /// <summary>
    /// SupplierListDataContract
    /// </summary>
    [DataContract]
    public class SupplierListDataContract : BusinessRelationListDataContract
    {
    }
}
