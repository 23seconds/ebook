﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BusinessRelationDataContract
    /// </summary>
    [DataContract]
    [KnownType(typeof(CustomerDataContract))]
    [KnownType(typeof(SupplierDataContract))]
    public class BusinessRelationDataContract
    {
        /// <summary>
        /// Gets/sets the id
        /// </summary>
        /// <value>Businessrelation id.</value>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the imported id, set only internal usage
        /// </summary>
        /// <value>Imported id.</value>
        [DataMember(Name="iid")]
        public string ImportedId { get; set; }

        /// <summary>
        /// Gets/sets the client id
        /// </summary>
        /// <value>Businessrelation client id.</value>
        [DataMember(Name="cid")]
        public Guid ClientId { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        [DataMember(Name="ln")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        [DataMember(Name = "fn")]
        public string FirstName { get; set; }


        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>The full name.</value>
        [DataMember(Name = "fulln")]
        public string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
                    return FirstName;

                if (!string.IsNullOrEmpty(LastName) && string.IsNullOrEmpty(FirstName))
                    return LastName;

                return string.Format("{0} {1}", FirstName, LastName) ;

            }
            set { }
        }

        /// <summary>
        /// Gets/sets the address
        /// </summary>
        /// <value>Businessrelation address.</value>
        [DataMember(Name = "ad")]
        public string Address { get; set; }

        /// <summary>
        /// Gets/sets the zipcode
        /// </summary>
        /// <value>Businessrelation zipcode.</value>
        [DataMember(Name = "zc")]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets/sets the city
        /// </summary>
        /// <value>Businessrelation city.</value>
        [DataMember(Name = "ci")]
        public string City { get; set; }

        /// <summary>
        /// Gets/sets the country
        /// </summary>
        /// <value>Businessrelation country.</value>
        [DataMember(Name = "co")]
        public string Country { get; set; }

        /// <summary>
        /// Gets/sets the Vat
        /// </summary>
        /// <value>Businessrelation Vat.</value>
        [DataMember(Name = "vt")]
        public string Vat { get; set; }

        /// <summary>
        /// Gets/sets the enterprisenumber
        /// </summary>
        /// <value>Businessrelation enterprisenumber.</value>
        [DataMember(Name = "cn")]
        public string CompanyNr { get; set; }

        /// <summary>
        /// Gets/sets the type (Customer/Supplier/Personnel)
        /// </summary>
        /// <value>Businessrelation type.</value>
        [DataMember(Name = "bt")]
        public char BusinessRelationType { get; set; }


        public StringListItemDataContract GetListItem()
        {
            return new StringListItemDataContract
            {
                Id = Id.ToString()
                ,
                EN = FullName
                ,
                FR = FullName
                ,
                NL = FullName
            };
        }
        ///// <summary>
        ///// Gets or sets the birthdate.
        ///// </summary>
        ///// <value>The birthdate.</value>
        //[DataMember(Name = "bd")]
        //public DateTime? Birthdate { get; set; }

        ///// <summary>
        ///// Gets or sets the currency id.
        ///// </summary>
        ///// <value>The currency id.</value>
        //[DataMember(Name = "cy")]
        //public Guid? CurrencyId { get; set; }

        ///// <summary>
        ///// Gets or sets the start date.
        ///// </summary>
        ///// <value>The start date.</value>
        //[DataMember(Name = "sd")]
        //public DateTime? StartDate { get; set; }

        ///// <summary>
        ///// Gets or sets the end date.
        ///// </summary>
        ///// <value>The end date.</value>
        //[DataMember(Name = "ed")]
        //public DateTime? EndDate { get; set; }

    }
}
