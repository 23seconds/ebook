﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class StatusDataContract
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "sn")]
        public string StatusName { get; set; }

        [DataMember(Name = "rnk")]
        public int Rank { get; set; }
    }
}
