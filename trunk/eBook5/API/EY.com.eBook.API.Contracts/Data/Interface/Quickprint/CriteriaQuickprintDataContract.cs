﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{ 
    [DataContract]
    public class CriteriaQuickprintDataContract
    {
        [DataMember]
        public Guid FileId { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public List<string> Options { get; set; }

        [DataMember]
        public string AccountNr { get; set; }

        [DataMember]
        public int StartSaldo { get; set; }
    }

}


