﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Meta
{
    /// <summary>
    /// MetaPersonDataContract
    /// </summary>
    [KnownType(typeof(ShareHolderDataContract))]
    [DataContract]
    public class MetaPersonDataContract
    {

        [DataMember(Name = "gr")]
        public string Gender { get; set; }

        [DataMember(Name = "nme")]
        public string Name { get; set; }

        
    }
}
