﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Meta
{
    /// <summary>
    /// FileMetaDataContract
    /// </summary>
    [DataContract]
    public class FileMetaDataContract
    {
        /// <summary>
        /// FileID
        /// </summary>
        /// <value>The file ID.</value>
        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }

        /// <summary>
        /// InternalNr
        /// </summary>
        /// <value>The internal nr.</value>
        [DataMember(Name = "dta")]
        public FileMetaDataDataContract Data { get; set; }
    }
}
