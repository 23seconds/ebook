﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Meta
{
    /// <summary>
    /// FileMetaDataDataContract
    /// </summary>
    [DataContract]
    public class FileMetaDataDataContract
    {

        [DataMember(Name="mgrs")]
        public List<MetaPersonDataContract> Managers { get; set; }

        //[DataMember(Name = "prts")]
        //public List<MetaPersonDataContract> Partners { get; set; }


        [DataMember(Name = "shrhls")]
        public List<ShareHolderDataContract> ShareHolders { get; set; }

        [DataMember(Name = "bmeet")]
        public MeetingDataContract BoardMeeting { get; set; }

        [DataMember(Name = "smeet")]
        public ShareHoldersMeetingDataContract ShareHoldersMeeting { get; set; }

        [DataMember(Name = "mmeet")]
        public MeetingDataContract ManagersMeeting { get; set; }

        [DataMember(Name = "pmeet")]
        public MeetingDataContract PartnersMeeting { get; set; }

        [DataMember(Name = "pgroup")]
        public List<PeerGroupItemDataContract> PeerGroup { get; set; }

    }
}
