﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Meta
{
    /// <summary>
    /// ShareHolderDataContract
    /// </summary>
    [DataContract]
    public class ShareHolderDataContract : MetaPersonDataContract
    {
        
        [DataMember(Name="repr")]
        public MetaPersonDataContract RepresentedBy { get; set; }

        [DataMember(Name="shrs")]
        public int? Shares { get; set; }


    }
}
