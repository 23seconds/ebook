﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Meta
{
    /// <summary>
    /// ShareHoldesMeetingDataContract
    /// </summary>
    [DataContract]
    public class ShareHoldersMeetingDataContract : MeetingDataContract
    {
        [DataMember(Name = "secr")]
        public MetaPersonDataContract Secretary { get; set; }

        [DataMember(Name = "tell")]
        public MetaPersonDataContract Teller { get; set; }

        [DataMember(Name = "shrs")]
        public int? TotalShares { get; set; }

    }
}
