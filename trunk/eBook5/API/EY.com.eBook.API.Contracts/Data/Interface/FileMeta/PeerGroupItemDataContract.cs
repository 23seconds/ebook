﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Meta
{
    /// <summary>
    /// PeerGroupItemDataContract
    /// </summary>
    [DataContract]
    public class PeerGroupItemDataContract
    {
        
        [DataMember(Name="ond")]
        public string EnterpriseNumber { get; set; }

        [DataMember(Name="nme")]
        public string Name { get; set; }


    }
}
