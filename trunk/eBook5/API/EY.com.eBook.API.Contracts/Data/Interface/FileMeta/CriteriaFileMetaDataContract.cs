﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data.Meta;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaFileMetaDataContract
    /// </summary>
    [DataContract]
    public class CriteriaFileMetaDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name="fmdc")]
        public FileMetaDataContract FileMeta { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }

}
