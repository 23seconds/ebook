﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Meta
{
    /// <summary>
    /// MeetingDataContract
    /// </summary>
    [KnownType(typeof(ShareHoldersMeetingDataContract))]
    [DataContract]
    public class MeetingDataContract
    {
        [DataMember(Name="dte")]
        public DateTime? Date { get; set; }

        [DataMember(Name = "time")]
        public string Time { get; set; }

        [DataMember(Name = "chm")]
        public MetaPersonDataContract Chairman { get; set; }

        [DataMember(Name = "del")]
        public bool Delayed {get;set;}

        [DataMember(Name = "deldte")]
        public DateTime? DelayedDate { get; set; }

        [DataMember(Name = "deltime")]
        public string DelayedTime { get; set; }

    }
}
