﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ReportItemDataContract
    /// </summary>
    [DataContract]
    public class ReportItemDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the data id.
        /// </summary>
        /// <value>The data id.</value>
        [DataMember]
        public Guid DataId { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>The text.</value>
        [DataMember]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the type of the node.
        /// </summary>
        /// <value>The type of the node.</value>
        [DataMember]
        public string NodeType { get; set; }

        /// <summary>
        /// Gets or sets the childeren.
        /// </summary>
        /// <value>The childeren.</value>
        [DataMember]
        public List<ReportItemDataContract> Childeren { get; set; }
    }
}
