﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CodaPoaDataContract
    {
        [DataMember(Name="ba")]
        public string BankAccount { get; set; }

        [DataMember(Name = "bic")]
        public string Bic { get; set; }

        [DataMember(Name = "lm")]
        public DateTime LastModified { get; set; }

        [DataMember(Name = "sm")]
        public DateTime? StatusModified { get; set; }

        [DataMember(Name = "ac")]
        public bool? AllowCoda { get; set; }

        [DataMember(Name = "cid")]
        public Guid ClientId { get; set; }

        [DataMember(Name = "sid")]
        public int StatusId { get; set; }

        [DataMember(Name = "riid")]
        public Guid? RepositoryItemId { get; set; }

        [DataMember(Name = "dsid")]
        public Guid? DocStoreId { get; set; }

        [DataMember(Name = "cn")]
        public string ClientName { get; set; }

        [DataMember(Name = "cg")]
        public string ClientGfis { get; set; }

        [DataMember(Name = "bn")]
        public string BankName { get; set; }

        [DataMember(Name = "sn")]
        public string StatusName { get; set; }

        [DataMember(Name = "eoll")]
        public Boolean? EOL_EYLicense { get; set; }

        [DataMember(Name = "eolm")]
        public Boolean? EOL_DigMailbox { get; set; }

        [DataMember(Name = "eold")]
        public int? EOL_division { get; set; }

        [DataMember(Name = "aca")]
        public Boolean activeClientACR { get; set; }
    }
}
