﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class BankDataContract
    {
        // fake id, is constructed of all bic linked to name EN
        [DataMember(Name="id")]
        public string Id { get; set; }

        [DataMember(Name = "n")]
        public string Name { get; set; }


    }
}
