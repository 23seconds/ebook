﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class RepositoryMetaConfigDataContract
    {
        [DataMember(Name="flds")]
        public List<RepositoryMetaFieldConfigDataContract> Fields { get; set; }

         [DataMember(Name = "ft")]
        public string FeatureType { get; set; }

        // in het geval van Coda volmacht folder
        // uitgelezen uit db repo structure
        // bevat:
        // Fields: leeg
        // FeatureType: CLIENTBANK
    }

    [DataContract]
    public class RepositoryMetaFieldConfigDataContract
    {
        [DataMember]
        public string FieldType { get; set; }
        [DataMember]
        public List<RepositoryMetaFieldAttribute> Attributes { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public RepositoryMetaTranslationDataContract Names { get; set; }
        [DataMember]
        public bool Required { get; set; }

    }

    [DataContract]
    public enum RepositoryMetaFieldTypesEnum
    {
        [EnumMember]
        TextField,
        [EnumMember]
        NumberField,
        [EnumMember]
        EuroField,
        [EnumMember]
        TextArea,
        [EnumMember]
        GlobalList,
        [EnumMember]
        Date,
        [EnumMember]
        Checkbox
    }

    [DataContract]
    public class RepositoryMetaFieldAttribute
    {
        [DataMember(Name="k")]
        public string Key { get; set; }

        [DataMember(Name = "v")]
        public string Value { get; set; }
    }

    [DataContract]
    public class RepositoryMetaTranslationDataContract
    {
        [DataMember(Name="nl")]
        public string Nl { get; set; }

        [DataMember(Name = "fr")]
        public string Fr { get; set; }

        [DataMember(Name = "en")]
        public string En { get; set; }

       
    }
}
