﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class RepositoryBasicItemDataContract
    {

        [DataMember()]
        public Guid Id { get; set; }

        [DataMember()]
        public string FileName { get; set; }

    }
}
