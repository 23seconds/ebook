﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaItemLinksDataContract
    {
        [DataMember]
        public Guid ItemId { get; set; }

        [DataMember]
        public Guid? FileId { get; set; }

        [DataMember]
        public string Culture { get; set; }

    }
}
