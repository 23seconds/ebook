﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaReplaceRepositoryItemDataContract
    {
        [DataMember]
        public Guid NewFileId { get; set; }

        [DataMember]
        public string NewFileName { get; set; }

        [DataMember]
        public string Extension { get; set; }

        [DataMember]
        public string Contenttype { get; set; }

        [DataMember]
        public Guid ItemId { get; set; }


    }
}
