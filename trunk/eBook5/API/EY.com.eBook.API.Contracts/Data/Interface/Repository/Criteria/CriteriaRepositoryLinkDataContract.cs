﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaRepositoryLinkDataContract
    {
        [DataMember]
        public Guid ClientId { get; set; }

        [DataMember]
        public Guid ItemId { get; set; }

        [DataMember]
        public string Link { get; set; }

        [DataMember]
        public string LinkType { get; set; }

    }
}
