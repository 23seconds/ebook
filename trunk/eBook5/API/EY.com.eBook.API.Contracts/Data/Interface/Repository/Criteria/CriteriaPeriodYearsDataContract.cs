﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaQueryFilesDataContract
    {
        [DataMember(Name="cid")]
        public Guid ClientId { get; set; }

        [DataMember(Name="sid")]
        public Guid? StructureId { get; set; }

        [DataMember(Name = "ps")]
        public DateTime? PeriodStart { get; set; }

        [DataMember(Name = "pe")]
        public DateTime? PeriodEnd { get; set; }

        [DataMember(Name = "st")]
        public Guid? Status { get; set; }

        [DataMember(Name = "fnp")]
        public string FileNamePartial { get; set; }

        [DataMember(Name = "cu")]
        public string Culture { get; set; }
    }
}
