﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaLinkRepositoryItemToDocstoreDataContract
    {
        [DataMember]
        public Guid RepositoryItemId { get; set; }

        [DataMember]
        public Guid DocstoreItemId { get; set; }

    }
}
