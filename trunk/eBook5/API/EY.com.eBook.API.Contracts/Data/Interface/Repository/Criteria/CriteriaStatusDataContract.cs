﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaStatusDataContract
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

    }
}
