﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaQuarterMonthDataContract
    {
        [DataMember(Name = "qm")]
        public string QuarterOrMonth { get; set; }

        [DataMember(Name = "ps")]
        public DateTime? PeriodStart { get; set; }

        [DataMember(Name = "pe")]
        public DateTime? PeriodEnd { get; set; }

    }
}
