﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaRepositoryChildrenDataContract
    {
        [DataMember(Name = "cid")]
        public Guid ClientId { get; set; }

        [DataMember(Name = "cult")]
        public string Culture { get; set; }

        [DataMember(Name="ps")]
        public DateTime? PeriodStart { get; set; }

        [DataMember(Name = "pe")]
        public DateTime? PeriodEnd { get; set; }

        [DataMember(Name = "stat")]
        public Guid? StatusId { get; set; }

        [DataMember(Name = "strucId")]
        public Guid? StructureId { get; set; } // IF NULL => ROOT CHILDREN REQUESTED

        [DataMember(Name = "key")]
        public string Key { get; set; } // needed to check for file container

        [DataMember(Name = "strucTp")]
        public string StructuralType { get; set; }

        [DataMember(Name="showFiles")]
        public bool ShowFiles { get; set; }

        [DataMember(Name = "fileId")]
        public Guid? FileId { get; set; }
    }
}
