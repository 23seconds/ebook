﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaSaveRepositoryItemDataContract
    {
        [DataMember]
        public bool IsNew { get; set; } // move to repository folder if is new

        [DataMember]
        public RepositoryItemDataContract Item { get; set; }

    }
}
