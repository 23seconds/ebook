﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class RepositoryLinkDataContract
    {

        [DataMember()]
        public Guid ClientId { get; set; }

        [DataMember()]
        public Guid FileId { get; set; }

        [DataMember()]
        public Guid ItemId { get; set; }

        [DataMember()]
        public string ConnectionType { get; set; }

        [DataMember()]
        public Guid ConnectionGuid { get; set; }

        [DataMember()]
        public string ConnectionAccount { get; set; }

        [DataMember()]
        public string ConnectionDetailedPath { get; set; }

       
    }
}
