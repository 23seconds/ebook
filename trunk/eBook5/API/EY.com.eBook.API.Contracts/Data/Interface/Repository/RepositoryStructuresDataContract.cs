﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
   
    [DataContract]
    public class RepositoryStructuresDataContract
    {
        [DataMember(Name="perm")]
        public List<RepositoryNodeDataContract> PermanentStructure { get; set; }

        [DataMember(Name = "period")]
        public List<RepositoryNodeDataContract> PeriodStructure { get; set; }
    }
}
