﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class RepositoryItemDataContract
    {

        [DataMember()]
        public Guid ClientId { get; set; }

        [DataMember()]
        public Guid PhysicalFileId { get; set; }

        [DataMember()]
        public Guid StructureId { get; set; }

        [DataMember()]
        public bool DocstoreAccess { get; set; }

        [DataMember()]
        public Guid? DocstoreId { get; set; }

        [DataMember()]
        public Guid Id { get; set; }

        [DataMember()]
        public string FileName { get; set; }

        [DataMember()]
        public string FilePath { get; set; }

        [DataMember()]
        public string Extension { get; set; }

        [DataMember()]
        public string ContentType { get; set; }

        [DataMember()]
        public List<RepositoryMetaItemDataContract> Meta { get; set; }

        [DataMember()]
        public DateTime? PeriodStart { get; set; }

        [DataMember()]
        public DateTime? PeriodEnd { get; set; }

        [DataMember()]
        public int TimeZoneOffset { get; set; }

        // Consider still needed
        //          \/
        [DataMember()]
        public string Prequel { get; set; }


        [DataMember()]
        public Guid? Status { get; set; }

        [DataMember()]
        public string StatusText { get; set; }

        [DataMember()]
        public string PartnerGpnAcr { get; set; }

        [DataMember()]
        public string IconCls { get; set; }
    }
}
