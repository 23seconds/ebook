﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public enum RepositoryNodeTypeEnumContract
    {
        [EnumMember]
        Folder,
        [EnumMember]
        File
    }

    [DataContract]
    public class RepositoryNodeDataContract
    {
        #region default Ext TreeNode
        [DataMember(Name="id")]
        public Guid Id { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "iconCls")]
        public string IconCls { get; set; }


        [DataMember(Name = "leaf")]
        public bool Leaf { get; set; }

        [DataMember(Name = "allowChildren")]
        public bool AllowChildren { get; set; }

        [DataMember(Name = "allowDrag")]
        public bool AllowDrag { get; set; }

        //public bool Editable { get; set; }

        [DataMember(Name = "cls")]
        public string Class { get; set; }

        [DataMember(Name = "children")]
        public List<RepositoryNodeDataContract> Children { get; set; }

        #endregion
        // 

        #region Extended attributes
        [DataMember()]
        public string Path { get; set; }

        [DataMember()]
        public int Order { get; set; }

        [DataMember()]
        public RepositoryItemDataContract Item { get; set; }
        //public RepositoryMetaConfigDataContract MetaConfig { get; set; }
        [DataMember()]
        public DateTime? PeriodStart { get; set; }

        [DataMember()]
        public DateTime? PeriodEnd { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public bool Files { get; set; }

        [DataMember]
        public bool AllowLinks { get; set; }

        [DataMember]
        public string StructuralType { get; set; }

        [DataMember]
        public string DateType { get; set; }

        [DataMember]
        public int? DateInterval { get; set; }

        [DataMember]
        public string StatusType { get; set; }

        [DataMember]
        public RepositoryMetaConfigDataContract MetaConfig { get; set; }

        [DataMember]
        public bool DocstoreAccess { get; set; }

        [DataMember(Name="isDynamic")]
        public bool Dynamic { get; set; }

        #endregion
    }
}
