﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
   
    [DataContract]
    public class RepositoryLinkViewDataContract
    {
        [DataMember()]
        public Guid ClientId { get; set; }

        [DataMember()]
        public Guid StructureId { get; set; }

        [DataMember()]
        public Guid Id { get; set; }

        [DataMember()]
        public string FileName { get; set; }

        [DataMember()]
        public string Prequel { get; set; }

        [DataMember()]
        public string Extension { get; set; }

        [DataMember()]
        public string ContentType { get; set; }

        [DataMember()]
        public Guid? Status { get; set; }

        [DataMember()]
        public List<RepositoryLinkDataContract> Links { get; set; }

    }

}
