﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class NewBundleDataContract
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid FileId { get; set; }

        [DataMember]
        public Guid TemplateId { get; set; }

        [DataMember]
        public string HeaderFooterType { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool Draft { get; set; }

        [DataMember]
        public bool RectoVerso { get; set; }

        [DataMember]
        public List<BundleItemBaseDataContract> Contents { get; set; }

        [DataMember]
        public bool Deleted { get; set; }

    }
}
