﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class BundleGenerationDataContract
    {
        [DataMember]
        public ClientDataContract Client { get; set; }

       
        [DataMember]
        public ReportDataContract Report { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public ReportStyleDataContract ReportStyle { get; set; }

        [DataMember]
        public string BundleName { get; set; }

        [DataMember]
        public Guid SessionId { get; set; }

        [DataMember]
        public bool PerformClosing { get; set; }

        [DataMember]
        public string Department { get; set; }
    }

    [DataContract]
    public class PDFGenerateDataContract
    {
        [DataMember]
        public ClientDataContract Client { get; set; }

       

        [DataMember]
        public IndexItemBaseDataContract Item { get; set; }

        [DataMember]
        public string Culture { get; set; }

    }
}
