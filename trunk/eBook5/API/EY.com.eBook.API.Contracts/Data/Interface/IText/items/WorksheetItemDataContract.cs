﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;


namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class WorksheetItemDataContract : IndexItemBaseDataContract
    {
      //  [DataMember(Name = "man")]
      //  public InRuleManualWorksheetDataContract Manual { get; set; }

        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type { get; set; }

        [DataMember]
        [ProtoMember(13)]
        public Guid WorksheetType { get; set; }

        [DataMember]
        [ProtoMember(14)]
        public string RuleApp { get; set; }

        [DataMember]
        [ProtoMember(15)]
        public Guid FileId { get; set; }

        [DataMember]
        [ProtoMember(16)]
        public string Culture { get; set; }

        [DataMember]
        [ProtoMember(17)]
        public string PrintLayout { get; set; }

        [DataMember]
        [ProtoMember(18)]
        public string Data { get; set; }

        [DataMember]
        [ProtoMember(19)]
        public DateTime? LastChanged { get; set; }

    }
}
