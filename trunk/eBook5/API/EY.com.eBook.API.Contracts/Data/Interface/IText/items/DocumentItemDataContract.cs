﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// PDFDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>

    [DataContract]
    public class DocumentItemDataContract : IndexItemBaseDataContract
    {
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "DOCUMENT";

        [DataMember]
        [ProtoMember(13)]
        public Guid FileId { get; set; }

        [DataMember]
        [ProtoMember(14)]
        public Guid DocumentId { get; set; }

        [DataMember]
        [ProtoMember(15)]
        public Guid DocumentTypeId { get; set; }

        //Culture is document related, might not be needed to state again
        [DataMember]
        [ProtoMember(16)]
        public string Culture { get; set; }
    }
}
