﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BundleTextDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    [DataContract]
    public class BundleTextDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        public string Type = "BUNDLETEXT";

        [DataMember]
        public string Text { get; set; }
        /// <summary>
        /// Gets or sets the hidden value. --> replace with indexitembasedatacontract ShowInIndex
        /// </summary>
        /// <value>The hidden.</value>
        [DataMember(Name = "hidden")]
        public bool Hidden { get; set; }

        public string Culture { get; set; }

    }
}
