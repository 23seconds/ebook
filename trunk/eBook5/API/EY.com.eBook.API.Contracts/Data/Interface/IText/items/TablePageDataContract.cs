﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CoverpageDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    [DataContract]
    public class TablePageDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "TABLEPAGE";

        /// <summary>
        /// Gets or sets the table values.
        /// </summary>
        /// <value>The table.</value>
        [DataMember(Name = "table")]
        [ProtoMember(13)]
        public string[,] Table;

    }
}
