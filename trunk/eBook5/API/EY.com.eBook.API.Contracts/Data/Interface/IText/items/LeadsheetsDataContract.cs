﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;


namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class LeadsheetsDataContract : IndexItemBaseDataContract
    {

        [DataMember(Name = "type")]
        [ProtoMember(1)]
        public string Type
        {
            get { return "STATEMENT"; }
            set { }
        }

        [DataMember]
        public Guid FileId { get; set; }

        [DataMember]
        public string Culture { get; set; }
        
    }
}
