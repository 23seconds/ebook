﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ReportDataContract
    /// </summary>
    [DataContract]
    public class ReportDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public HeaderConfigDataContract HeaderConfig { get; set; }

        [DataMember]
        public FooterConfigDataContract FooterConfig { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>The text.</value>
        [DataMember]
        public bool Draft { get; set; }

        /// <summary>
        /// Gets or sets the Index.
        /// </summary>
        /// <value>The Index.</value>
        [DataMember]
        public IndexDataContract Index { get; set; }
    }
}
