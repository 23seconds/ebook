﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;


namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CorrectionBookingDataContract : IndexItemBaseDataContract
    {

        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type
        {
            get { return "STATEMENT"; }
            set { }
        }

        [DataMember(Name = "culture")]
        [ProtoMember(13)]
        public string culture { get; set; }

    }
}
