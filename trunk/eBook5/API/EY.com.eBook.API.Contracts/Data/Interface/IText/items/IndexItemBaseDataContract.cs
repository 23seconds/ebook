﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// IndexItemBaseDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    [KnownType(typeof(CoverpageDataContract))]
    [KnownType(typeof(IndexPageDataContract))]
    [KnownType(typeof(ChapterDataContract))]
    [KnownType(typeof(PDFDataContract))]
    [KnownType(typeof(WorksheetItemDataContract))]
    [KnownType(typeof(StatementsDataContract))]
    [KnownType(typeof(FicheItemDataContract))]
    [KnownType(typeof(DocumentItemDataContract))]
    [KnownType(typeof(XbrlFicheItemDataContract))]
    [KnownType(typeof(XbrlFrontPageItemDataContract))]
    [KnownType(typeof(PDFManualDataContract))]
    [KnownType(typeof(BizTaxBundleDataContract))]
    [KnownType(typeof(LeadsheetsDataContract))]
    [KnownType(typeof(CorrectionBookingDataContract))]
    [KnownType(typeof(BizTaxFicheDataContract))]
    [KnownType(typeof(AutoBizTaxDataContract))]
    [KnownType(typeof(BizTaxTaxCalcDataContract))]
    [ProtoInclude(900,typeof(CoverpageDataContract))]
    [ProtoInclude(901, typeof(IndexPageDataContract))]
    [ProtoInclude(902, typeof(ChapterDataContract))]
    [ProtoInclude(903, typeof(PDFDataContract))]
    [ProtoInclude(904, typeof(WorksheetItemDataContract))]
    [ProtoInclude(905, typeof(StatementsDataContract))]
//    [ProtoInclude(906, typeof(FicheItemDataContract))]
    [ProtoInclude(907, typeof(DocumentItemDataContract))]
 //   [ProtoInclude(908, typeof(XbrlFicheItemDataContract))]
    //[ProtoInclude(909, typeof(XbrlFrontPageItemDataContract))]
   // [ProtoInclude(910, typeof(PDFManualDataContract))]
    [ProtoInclude(911, typeof(BizTaxBundleDataContract))]
    [ProtoInclude(912, typeof(LeadsheetsDataContract))]
    //[ProtoInclude(913, typeof(BookingDataContract))]
    [ProtoInclude(913, typeof(CorrectionBookingDataContract))]
    [ProtoInclude(914, typeof(BizTaxFicheDataContract))]
     [ProtoInclude(915, typeof(AutoBizTaxDataContract))]
     [ProtoInclude(916, typeof(BizTaxTaxCalcDataContract))]
    [DataContract]
    public class IndexItemBaseDataContract
    {

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id")]
        [ProtoMember(1)]
        public Guid Id { get; set; }

        [DataMember(Name = "iconCls")]
        [ProtoMember(2)]
        public string IconCls { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        [DataMember(Name = "title")]
        [ProtoMember(3)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the visibility in the index.
        /// </summary>
        /// <value>The flag.</value>
        [DataMember(Name = "indexed")]
        [ProtoMember(4)]
        public bool ShowInIndex { get; set; }

        /// <summary>
        /// Gets or sets the visibility in the index.
        /// </summary>
        /// <value>The flag.</value>
        [DataMember(Name = "locked")]
        [ProtoMember(5)]
        public bool Locked { get; set; }

        [DataMember()]
        [ProtoMember(6)]
        public HeaderConfigDataContract HeaderConfig { get; set; }

        [DataMember()]
        [ProtoMember(7)]
        public FooterConfigDataContract FooterConfig { get; set; }

        public bool Delete { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember()]
        [ProtoMember(8)]
        public string iTextTemplate { get; set; }

        [DataMember()]
        [ProtoMember(9)]
        public int StartsAt { get; set; }

        [DataMember()]
        [ProtoMember(10)]
        public int EndsAt { get; set; }

        [DataMember()]
        [ProtoMember(11)]
        public bool NoDraft { get; set; }
    }
}
