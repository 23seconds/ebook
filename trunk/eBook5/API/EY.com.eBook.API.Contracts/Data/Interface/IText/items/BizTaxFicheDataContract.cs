﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BizTaxBundleDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    /// 
     [DataContract]
    public class BizTaxTaxCalcDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "BIZTAXCALC";
    }

    [DataContract]
    public class BizTaxFicheDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "BIZTAXFICHE";

        [DataMember(Name = "Fiche")]
        [ProtoMember(13)]
        public string Fiche { get; set; }
    }

    [DataContract]
    public class AutoBizTaxDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "AUTOBIZTAX";
    }
}
