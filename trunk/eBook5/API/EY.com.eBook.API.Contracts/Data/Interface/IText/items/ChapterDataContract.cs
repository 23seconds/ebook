﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ChapterDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    
    [DataContract]
    public class ChapterDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "CHAPTER";


        [DataMember(Name = "nocoverpage")]
        [ProtoMember(13)]
        public bool NoCoverpage { get; set; }
       
        /// <summary>
        /// Gets or sets the child items.
        /// </summary>
        /// <value>The items.</value>
        [DataMember(Name = "items")]
        [ProtoMember(14)]
        public List<IndexItemBaseDataContract> Items { get; set; }

        [DataMember(Name = "signedrepo")]
        [ProtoMember(15)]
        public bool RequiredSignedRepo { get; set; }

    }
}
