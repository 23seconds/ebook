﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;


namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ConfirmationOfApprovalCitrDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "CONFIRMATIONOFAPPROVALCITR";
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "RuleApp")]
        [ProtoMember(13)]
        public string RuleApp { get; set; }
        /// <summary>
        /// Gets or sets the cvba.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "cvba")]
        [ProtoMember(14)]
        public string CVBA { get; set; }

        [DataMember(Name = "culture")]
        [ProtoMember(15)]
        public string Culture { get; set; }

        [DataMember(Name = "printlayout")]
        [ProtoMember(16)]
        public string PrintLayout { get; set; }

        [DataMember(Name = "bni")]
        [ProtoMember(17)]
        public bool BNI { get; set; }
    }
}
