﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;


namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class StatementsDataContract : IndexItemBaseDataContract
    {

        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type
        {
            get { return "STATEMENT"; }
            set { }
        }

        [DataMember]
        [ProtoMember(13)]
        public Guid FileId { get; set; }

        /// <summary>
        /// layout type of the statement. a.k.a. active/passive/results/short results
        /// </summary>
        /// <value>The pagecount.</value>
        [DataMember(Name = "layout")]
        [ProtoMember(14)]
        public string Layout { get; set; }

        [DataMember]
        [ProtoMember(15)]
        public bool Detailed { get; set; }

        [DataMember]
        [ProtoMember(16)]
        public string Culture { get; set; }

        [DataMember]
        [ProtoMember(17)]
        public Boolean Comparable { get; set; }
    }
}
