﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// IndexDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    [KnownType(typeof(IndexDataContract))]
    [DataContract]
    public class IndexDataContract
    {
        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        /// <value>The items.</value>
        [DataMember(Name = "items")]
        public List<IndexItemBaseDataContract> Items { get; set; }

    }
}
