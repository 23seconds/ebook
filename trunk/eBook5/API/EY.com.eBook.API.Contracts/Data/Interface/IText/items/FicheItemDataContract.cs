﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;


namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FicheItemDataContract : WorksheetItemDataContract
    {

        [DataMember]
        public Guid FicheTypeId { get; set; }


        [DataMember]
        public int? ToPage { get; set; }

      
    }

    
}
