﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ReportStyleDataContract
    /// Contains default styles to take into account while generating a report
    /// </summary>
    [DataContract]
    public class ReportStyleDataContract
    {
        /// <summary>
        /// Gets or sets the coverpage.
        /// </summary>
        /// <value>The coverpage.</value>
        [DataMember(Name = "coverpage")]
        public string CoverPage { get; set; }

        /// <summary>
        /// Gets or sets the chapter title page.
        /// </summary>
        /// <value>The coverpage.</value>
        [DataMember(Name = "chapterTitlePage")]
        public string ChapterTitlePage { get; set; }
        
        /// <summary>
        /// Gets or sets the index page.
        /// </summary>
        /// <value>The index page.</value>
        [DataMember(Name = "indexpage")]
        public string IndexPage { get; set; }

        [DataMember]
        public List<HeaderFooterDataDataContract> HeaderData { get; set; }

        [DataMember]
        public List<HeaderFooterDataDataContract> FooterData { get; set; }

    }
}
