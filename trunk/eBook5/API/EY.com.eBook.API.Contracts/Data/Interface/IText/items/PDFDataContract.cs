﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// PDFDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>

    [KnownType(typeof(PDFManualDataContract))]
    [DataContract]
    public class PDFDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "PDFPAGE";

        [DataMember(Name = "ItemId")]
        [ProtoMember(13)]
        public Guid ItemId { get; set; }

        /// <summary>
        /// Gets or sets from which page on to import
        /// </summary>
        /// <value>The page.</value>
        [DataMember(Name = "fromPage")]
        [ProtoMember(14)]
        public int? FromPage { get; set; }

        /// <summary>
        /// Gets or sets from which page on to import
        /// </summary>
        /// <value>The page.</value>
        [DataMember(Name = "toPage")]
        [ProtoMember(15)]
        public int? ToPage { get; set; }

        /// <summary>
        /// Gets or sets the amount of pages
        /// </summary>
        /// <value>The pagecount.</value>
        [DataMember(Name = "pages")]
        [ProtoMember(16)]
        public int Pages { get; set; }

        /// <summary>
        /// Gets or sets the location of the PDF document
        /// </summary>
        /// <value>The location of the PDF document.</value>
        [DataMember(Name = "pdfLocation")]
        [ProtoMember(17)]
        public string PDFLocation { get; set; }

        public bool IsTempRender { get; set; }

        [DataMember(Name = "outlines")]
        [ProtoMember(18)]
        public ContentsOutline Outlines { get; set; }

        [DataMember(Name = "origin")]
        [ProtoMember(19)]
        public string Origin { get; set; }
    }

    public class PDFManualDataContract : PDFDataContract
    {
    }
}
