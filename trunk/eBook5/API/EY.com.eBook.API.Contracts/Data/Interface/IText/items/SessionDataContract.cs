﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ThreadDataContract
    /// </summary>
    [DataContract]
    public class ITextSessionDataContract
    {
        /// <summary>
        /// Gets or sets the session id.
        /// </summary> 
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the percentage.
        /// </summary>
        [DataMember]
        public int Percentage { get; set; }

        /// <summary>
        /// Gets or sets the start date
        /// </summary>
        [DataMember]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the finished date
        /// </summary>
        [DataMember]
        public DateTime? FinishedDate { get; set; }

        /// <summary>
        /// Gets or sets the xml document
        /// </summary>
        [DataMember]
        public List<String> ActionList { get; set; }


        /// <summary>
        /// Gets or sets the error 
        /// </summary>
        [DataMember]
        public Object Error { get; set; }

        

        /// <summary>
        /// Gets or sets the pdf document path
        /// </summary>
        [DataMember]
        public string PdfDocumentPath { get; set; }
    }
}
