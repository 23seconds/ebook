﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.API.Contracts.Data.IText
{
    [DataContract]
    public class ITextRenderDataContract
    {
        [DataMember]
        public XElement Template { get; set; }

        [DataMember]
        public string Orientation { get; set; }



    }
}
