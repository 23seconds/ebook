﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CoverpageDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    [DataContract]
    public class CoverpageDataContract : IndexItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        [ProtoMember(12)]
        public string Type = "COVERPAGE";

        /// <summary>
        /// Gets or sets the hidden value. --> replace with indexitembasedatacontract ShowInIndex
        /// </summary>
        /// <value>The hidden.</value>
        [DataMember(Name = "hidden")]
        [ProtoMember(13)]
        public bool Hidden { get; set; }

        public string Culture { get; set; }

        public string CoverType { get; set; }

    }
}
