﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class LibraryFolderDataContract
    {
        [DataMember(Name = "type")]
        public string Type
        {
            get { return "FOLDER"; }
            set { }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        [DataMember(Name = "title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the child items.
        /// </summary>
        /// <value>The items.</value>
        [DataMember(Name = "items")]
        public List<IndexItemBaseDataContract> Items { get; set; }
    }
}
