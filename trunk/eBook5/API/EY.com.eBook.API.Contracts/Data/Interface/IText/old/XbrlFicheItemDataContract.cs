﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class XbrlFicheItemDataContract : IndexItemBaseDataContract
    {
        [DataMember]
        public XbrlAttachmentDataContract Fiche { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public int Assessmentyear { get; set; }

        [DataMember]
        public XElement XbrlData { get; set; }
    }
}
