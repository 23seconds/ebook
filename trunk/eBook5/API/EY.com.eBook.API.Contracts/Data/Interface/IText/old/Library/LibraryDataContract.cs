﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class LibraryDataContract
    {
        [DataMember(Name = "type")]
        public string Type
        {
            get { return "LIBRARY"; }
            set { }
        }

        /// <summary>
        /// Gets or sets the child items.
        /// </summary>
        /// <value>The items.</value>
        [DataMember(Name = "folders")]
        public List<LibraryFolderDataContract> Folders { get; set; }

    }
}
