﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class BundleStatementsDataContract : BundleItemBaseDataContract
    {

        [DataMember(Name = "type")]
        public string Type
        {
            get { return "STATEMENT"; }
            set { }
        }

        [DataMember(Name = "layout")]
        public string Layout { get; set; }

        [DataMember]
        public bool Detailed { get; set; }

    }
}
