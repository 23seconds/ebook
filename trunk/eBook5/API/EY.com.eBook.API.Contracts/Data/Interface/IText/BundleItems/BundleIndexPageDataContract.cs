﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// IndexPageDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    
    [DataContract]
    public class BundleIndexPageDataContract : BundleItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        public string Type = "INDEXPAGE";

    }
}