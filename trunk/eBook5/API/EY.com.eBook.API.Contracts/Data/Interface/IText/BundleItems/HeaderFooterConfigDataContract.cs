﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;
using System.Xml.Linq;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class HeaderConfigDataContract
    {
        [DataMember]
        [ProtoMember(1)]
        public bool Enabled { get; set; }

        [DataMember]
        [ProtoMember(2)]
        public bool ShowTitle { get; set; }

        [DataMember]
        [ProtoMember(3)]
        public string Style { get; set; }

       

    }

    [DataContract]
    public class FooterConfigDataContract {

        [DataMember]
        [ProtoMember(1)]
        public bool Enabled { get; set; }

        [DataMember]
        [ProtoMember(2)]
        public bool ShowFooterNote { get; set; }

        [DataMember]
        [ProtoMember(3)]
        public string FootNote { get; set; }

        [DataMember]
        [ProtoMember(4)]
        public bool ShowPageNr { get; set; }

        [DataMember]
        [ProtoMember(5)]
        public string Style { get; set; }

    }
}
