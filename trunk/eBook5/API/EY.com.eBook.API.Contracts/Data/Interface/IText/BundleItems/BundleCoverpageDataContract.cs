﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CoverpageDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    [DataContract]
    public class BundleCoverpageDataContract : BundleItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        public string Type = "COVERPAGE";

        [DataMember] // ACR or TAX
        public string CoverType { get; set; }

    }
}
