﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ChapterDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
    
    [DataContract]
    public class BundleChapterDataContract : BundleItemBaseDataContract
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [DataMember(Name = "type")]
        public string Type = "CHAPTER";


        [DataMember(Name = "nocoverpage")]
        public bool NoCoverpage { get; set; }
       
        /// <summary>
        /// Gets or sets the child items.
        /// </summary>
        /// <value>The items.</value>
        [DataMember(Name = "items")]
        public List<IndexItemBaseDataContract> Items { get; set; }

    }
}
