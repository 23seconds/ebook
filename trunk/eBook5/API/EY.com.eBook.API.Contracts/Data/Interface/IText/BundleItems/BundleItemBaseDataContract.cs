﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// IndexItemBaseDataContract
    /// Contains properties only needed for listing and settings in client.
    /// </summary>
  /*  [KnownType(typeof(CoverpageDataContract))]
    [KnownType(typeof(IndexPageDataContract))]
    [KnownType(typeof(ChapterDataContract))]
    [KnownType(typeof(PDFDataContract))]
    [KnownType(typeof(WorksheetItemDataContract))]
    [KnownType(typeof(StatementsDataContract))]
    [KnownType(typeof(FicheItemDataContract))]
    [KnownType(typeof(DocumentItemDataContract))]
    [KnownType(typeof(XbrlFicheItemDataContract))]
    [KnownType(typeof(XbrlFrontPageItemDataContract))]
    [KnownType(typeof(PDFManualDataContract))]*/
    [DataContract]
    public class BundleItemBaseDataContract
    {

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        [DataMember(Name = "iconCls")]
        public string IconCls { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        [DataMember(Name = "title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the visibility in the index.
        /// </summary>
        /// <value>The flag.</value>
        [DataMember(Name = "indexed")]
        public bool ShowInIndex { get; set; }

        [DataMember(Name = "childrenIndexed")]
        public bool ShowChildrenInIndex { get; set; }

        [DataMember]
        public HeaderConfigDataContract HeaderConfig { get; set; }

        [DataMember]
        public FooterConfigDataContract FooterConfig { get; set; }

       
        [DataMember()]
        public string iTextTemplate { get; set; }

        [DataMember()]
        public int StartsAt { get; set; }

        [DataMember()]
        public int EndsAt { get; set; }

        [DataMember]
        public string Culture { get; set; }

    }
}
