﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaLibraryDataContract
    {

        [DataMember(Order=1)]
        public string NodeId { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }


        [DataMember(Order = 3)]
        public Guid FileId { get; set; }

    
    }
}
