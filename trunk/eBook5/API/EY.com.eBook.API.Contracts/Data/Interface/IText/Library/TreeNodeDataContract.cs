﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class LibraryTreeNodeDataContract
    {
        #region default Ext TreeNode
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "iconCls")]
        public string IconCls { get; set; }


        [DataMember(Name = "leaf")]
        public bool Leaf { get; set; }

        [DataMember(Name = "allowChildren")]
        public bool AllowChildren { get; set; }

        [DataMember(Name = "allowDrag")]
        public bool AllowDrag { get; set; }

        //public bool Editable { get; set; }

        [DataMember(Name = "cls")]
        public string Class { get; set; }

        #endregion

        [DataMember(Name = "indexItem")]
        public IndexItemBaseDataContract IndexItem { get; set; }
    }
}
