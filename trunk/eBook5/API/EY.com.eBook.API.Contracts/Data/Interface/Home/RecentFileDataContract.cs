﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class RecentFileDataContract
    {
        [DataMember(Name="f")]
        public FileInfoDataContract File { get; set; }

        [DataMember(Name="c")]
        public ClientBaseDataContract Client { get; set; }
       
        [DataMember(Name="la")]
        public DateTime LastAccessed { get; set; }

    }
}
