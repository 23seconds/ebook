﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{   
    [DataContract]
    public class WorksheetHelpDataContract
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        [DataMember(Name = "whid")]
        public Guid WorksheetHelpId { get; set; }

        [DataMember(Name = "wtid")]
        public Guid WorksheetTypeId { get; set; }

        [DataMember(Name = "c")]
        public string Culture { get; set; }

        [DataMember(Name = "s")]
        public string Subject { get; set; }

        [DataMember(Name = "b")]
        public string Body { get; set; }

        [DataMember(Name = "o")]
        public int Order { get; set; }

    }
}
