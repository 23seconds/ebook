﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaChampionDataContract
    {
        [DataMember(Name="pid")]
        public Guid PersonId { get; set; }

        [DataMember(Name="oid")]
        public string OfficeId { get; set; }
    }
}


