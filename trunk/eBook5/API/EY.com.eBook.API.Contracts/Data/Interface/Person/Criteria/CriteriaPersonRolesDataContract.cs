﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaPersonRolesDataContract
    {
        [DataMember(Name= "pid")]
        public Guid PersonId { get; set; }

        [DataMember(Name = "r")]
        public string Role { get; set; }

        [DataMember(Name = "c")]
        public Boolean Check { get; set; }
    }
}


