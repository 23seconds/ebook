﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class PersonRolesDataContract
    {
        [DataMember(Name="r", Order = 1)]
        public string role { get; set; }

        [DataMember(Name = "c", Order = 2)]
        public Boolean check { get; set; }

    }
}
