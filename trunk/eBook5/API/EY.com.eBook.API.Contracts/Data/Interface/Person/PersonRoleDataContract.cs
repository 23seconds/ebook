﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// PersonDataContract, only list related data (grid view)
    /// DB:viewPersons
    /// </summary>
    [DataContract]
    public class PersonRoleDataContract
    {
        /// <summary>
        /// Gets the cdb person id, item is read only to outside spaces
        /// </summary>
        /// <value>person cdb id.</value>
        [DataMember(Name = "id")]
        public Guid PersonId { get; set; }

        [DataMember(Name = "pes")]
        public PersonDataContract Person { get; set; }

        /// <summary>
        /// Gets the role, item is read only to outside spaces
        /// </summary>
        /// <value>person role.</value>
        [DataMember(Name = "r")]
        public string Role { get; set; }

        /// <summary>
        /// manual role / not
        /// </summary>
        /// <value>manual.</value>
        [DataMember(Name = "m")]
        public bool Manual { get; set; }

        /// <summary>
        /// last used role
        /// </summary>
        /// <value>last used.</value>
        [DataMember(Name = "lu")]
        public bool LastUsed { get; set; }


        public int Order { get { return Manual ? 1 : 0; } }
    }


}
