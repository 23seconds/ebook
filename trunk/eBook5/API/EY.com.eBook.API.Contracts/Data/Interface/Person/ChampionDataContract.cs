﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{    
    /// <summary>
    /// ChampionDataContract
    /// </summary>
    [DataContract]
    public class ChampionDataContract
    {
        
        [DataMember(Name = "id")]
        public Guid Id { get;  set; }

        [DataMember(Name = "fn")]
        public string FirstName { get; set; }

        [DataMember(Name = "ln")]
        public string LastName { get; set; }

        [DataMember(Name = "em")]
        public string Email { get; set; }

        [DataMember(Name="o")]
        public string Office { get; set; }

        [DataMember(Name="oid")]
        public string OfficeId { get; set; }

        [DataMember(Name = "dep")]
        public string Department { get; set; }
    }

  }
