﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{    
    /// <summary>
    /// PersonDataContract, only list related data (grid view)
    /// DB:viewPersons
    /// </summary>
    [DataContract]
    public class PersonDataContract : PersonBaseDataContract
    {

        /// <summary>
        /// Gets whether user is champion, item is read only to outside spaces
        /// </summary>
        /// <value>champion.</value>
        [DataMember(Name = "ic")]
        public bool IsChampion { get; set; }

        /// <summary>
        /// Gets whether user is system administrator, item is read only to outside spaces
        /// </summary>
        /// <value>system admin/not.</value>
        [DataMember(Name = "isa")]
        public bool IsSysAdmin { get; set; }

        /// <summary>
        /// Gets the role, item is read only to outside spaces
        /// </summary>
        /// <value>person role.</value>
        [DataMember(Name = "rs")]
        public List<PersonRoleDataContract> Roles { get;  set; }

        /// <summary>
        /// Gets the Champion Offices, item is read only to outside spaces
        /// </summary>
        /// <value>person Offices.</value>
        [DataMember(Name = "os")]
        public List<OfficeDataContract> Offices { get; set; }

        [DataMember(Name = "dof")]
        public OfficeDataContract DefaultOffice { get; set; }

    }

  }
