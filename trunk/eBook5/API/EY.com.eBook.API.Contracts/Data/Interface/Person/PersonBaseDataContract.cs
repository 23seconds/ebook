﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{    
    /// <summary>
    /// PersonDataContract, only list related data (grid view)
    /// DB:viewPersons
    /// </summary>
    [KnownType(typeof(PersonDataContract))]
    [DataContract]
    public class PersonBaseDataContract
    {
        /// <summary>
        /// Gets the cdb person id, item is read only to outside spaces
        /// </summary>
        /// <value>person cdb id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get;  set; }

        /// <summary>
        /// Gets the windows username, item is read only to outside spaces
        /// </summary>
        /// <value>person username.</value>
        [DataMember(Name = "wa")]
        public string WindowsAccount { get;  set; }

        /// <summary>
        /// Gets the first name, item is read only to outside spaces
        /// </summary>
        /// <value>person first name.</value>
        [DataMember(Name = "fn")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets the last name, item is read only to outside spaces
        /// </summary>
        /// <value>person last name.</value>
        [DataMember(Name = "ln")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets the email, item is read only to outside spaces
        /// </summary>
        /// <value>person email.</value>
        [DataMember(Name = "em")]
        public string Email { get; set; }

        /// <summary>
        /// Gets the last logon date, item is read only to outside spaces
        /// </summary>
        /// <value>person last logon date.</value>
        [DataMember(Name = "ll")]
        public DateTime? LastLogon { get; set; }

        

        [DataMember(Name = "dofi")]
        public string DefaultOfficeId { get; set; }

        [DataMember(Name="dp")]
        public string Department { get; set; }

        [DataMember(Name = "ecs")]
        public string Champions { get; set; }

        [DataMember(Name = "gpn")]
        public string GPN { get; set; }
    }

  }
