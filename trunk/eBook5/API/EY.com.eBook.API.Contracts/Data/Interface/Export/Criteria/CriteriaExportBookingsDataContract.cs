﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaExportBookingsDataContract
    {

        [DataMember(Order=1)]
        public Guid FileId { get; set; }

        [DataMember(Order = 2)]
        public List<Guid> BookingIds { get; set; }


        [DataMember(Order = 3,Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Order = 4)]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }
}
