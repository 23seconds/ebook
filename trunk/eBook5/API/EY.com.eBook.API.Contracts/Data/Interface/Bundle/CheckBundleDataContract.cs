﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BundleDataContract
    /// </summary>
    [DataContract]
    public class CheckBundleDataContract
    {

        [DataMember]
        public Guid? BundleId { get; set; }
        
        [DataMember]
        public bool Valid { get; set; }

        [DataMember]
        public bool AccountingMemorandum { get; set; }

        [DataMember]
        public bool SignedStatutoryAccounts { get; set; }

        [DataMember]
        public bool SignedMinutesGeneralAssemble { get; set; }

        [DataMember]
        public bool SignedRepresentationLetter { get; set; }

        [DataMember]
        public bool SignedAccountingMemorandum { get; set; }

        [DataMember]
        public bool SignedEngagementAgreement { get; set; }

        [DataMember]
        public bool DepositNBB { get; set; }

        [DataMember]
        public string Fault { get; set; }

    }
}
