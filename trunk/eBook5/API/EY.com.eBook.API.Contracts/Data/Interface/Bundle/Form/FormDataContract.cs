﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FormDataContract
    {
        [DataMember(Name="f")]
        public string Field { get; set; }

        [DataMember(Name="v")]
        public string Value { get; set; }
    }
}
