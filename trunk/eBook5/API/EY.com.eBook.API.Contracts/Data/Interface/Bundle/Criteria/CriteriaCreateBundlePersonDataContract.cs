﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaCreateBundlePersonDataContract
    {
        [DataMember(Order = 1)]
        public Guid FileId { get; set; }


        [DataMember(Order = 2)]
        public Guid TemplateId { get; set; }

        [DataMember(Order = 3)]
        public string Culture { get; set; }

        [DataMember(Order = 4)]
        public string Name { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }
}
