﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaCreateBundleDataContract
    {

        [DataMember(Order=1)]
        public Guid FileId { get; set; }


        [DataMember(Order = 2)]
        public Guid TemplateId { get; set; }

        [DataMember(Order = 3)]
        public string Culture { get; set; }

        [DataMember(Order = 4)]
        public string Name { get; set; }
    }
}
