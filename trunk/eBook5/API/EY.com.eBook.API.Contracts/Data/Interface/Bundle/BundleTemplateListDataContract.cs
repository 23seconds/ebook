﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BundleTemplateListDataContract
    /// </summary>
    [DataContract]
    public class BundleTemplateListDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
