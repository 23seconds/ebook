﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BundleDataContract
    /// </summary>
    [DataContract]
    public class BundleDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid FileId { get; set; }

        [DataMember]
        public Guid TemplateId { get; set; }

        [DataMember]
        public string HeaderFooterType { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public bool Draft { get; set; }

        [DataMember]
        public bool RectoVerso { get; set; }

        [DataMember]
        public List<IndexItemBaseDataContract> Contents { get; set; }


        [DataMember]
        public bool Deleted { get; set; }

        [DataMember]
        public bool Locked { get; set; }

        [DataMember]
        public string Filepath { get; set; }
    }
}
