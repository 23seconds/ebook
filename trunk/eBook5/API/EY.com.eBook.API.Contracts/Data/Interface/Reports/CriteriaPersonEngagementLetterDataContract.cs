﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaPersonEngagementLetterDataContract : CriteriaBaseDataContract
    {
        [DataMember(Name="sdt")]
        public DateTime? StartDateTo { get; set; }

        [DataMember(Name = "sdf")]
        public DateTime? StartDateFrom { get; set; }

        [DataMember(Name = "edt")]
        public DateTime? EndDateTo { get; set; }

        [DataMember(Name = "edf")]
        public DateTime? EndDateFrom { get; set; }

        [DataMember(Name = "stid")]
        public string StructureId { get; set; }

        [DataMember(Name = "sid")]
        public Guid? StatusId { get; set; }

        [DataMember(Name = "tid")]
        public Guid? TemplateId { get; set; }

        [DataMember(Name = "t")]
        public string TemplateName { get; set; }

        [DataMember(Name = "cn")]
        public string ClientName { get; set; }

        [DataMember(Name = "ex")]
        public string Expiration { get; set; }

        [DataMember(Name = "ro")]
        public bool? RenewalOverview { get; set; }

        [DataMember(Name = "gth")]
        public string Gth { get; set; }

        [DataMember(Name = "ac")]
        public string activeClient { get; set; }

        [DataMember(Name = "pid")]
        public Guid PersonId { get; set; }

        [DataMember(Name = "sf")]
        public string SortField { get; set; }

        [DataMember(Name = "so")]
        public string SortOrder { get; set; } 
    }
}


