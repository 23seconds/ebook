﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaClientEngagementLetterDataContract
    {
        [DataMember(Name="sd")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "ed")]
        public DateTime EndDate { get; set; }

        [DataMember(Name = "stid")]
        public string StructureId { get; set; }

        [DataMember(Name = "sid")]
        public string StatusId { get; set; }

        [DataMember(Name = "tid")]
        public string TemplateId { get; set; }

        [DataMember(Name = "t")]
        public string TemplateName { get; set; }

        /*
        [DataMember(Name = "d")]
        public List<string> Department { get; set; }
         */ 
    }
}


