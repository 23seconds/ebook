﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// </summary>
    [DataContract]
    public class ClientPersonFileServiceDataContract
    {
        [DataMember(Name = "cid")]
        public string ClientId { get; set; }

        [DataMember(Name = "c")]
        public string ClientName { get; set; }

        [DataMember(Name = "cg")]
        public string ClientGfis { get; set; }

        [DataMember(Name = "s")]
        public string Status { get; set; }

        [DataMember(Name = "sid")]
        public Int16? StatusId { get; set; }

        [DataMember(Name = "sd")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "ed")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "nam")]
        public DateTime? NextAnnualMeeting { get; set; }

        [DataMember(Name = "dm")]
        public DateTime? DateModified { get; set; }

        [DataMember(Name = "o")]
        public string Office { get; set; }

        [DataMember(Name = "p")]
        public string Partner{ get; set; }

        [DataMember(Name = "m")]
        public string Manager { get; set; }

        [DataMember(Name = "pe")]
        public string Person { get; set; }

        [DataMember(Name = "dos")]
        public int? DaysOnStatus { get; set; }

        [DataMember(Name = "fid")]
        public string FileId { get; set; }

        [DataMember(Name = "f")]
        public string FileName { get; set; }
    }

}
