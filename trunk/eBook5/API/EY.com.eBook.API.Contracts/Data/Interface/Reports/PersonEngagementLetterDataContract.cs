﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// </summary>
    [DataContract]
    public class PersonEngagementLetterDataContract
    {
        [DataMember(Name = "c")]
        public string ClientName { get; set; }

        [DataMember(Name = "cg")]
        public string ClientGfis { get; set; }

        [DataMember(Name = "fn")]
        public string FileName { get; set; }

        [DataMember(Name = "s")]
        public string status { get; set; }

        [DataMember(Name = "t")]
        public string template { get; set; }

        [DataMember(Name = "sd")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "ed")]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "o")]
        public string Office { get; set; }

        [DataMember(Name = "cp")]
        public RepositoryMetaItemDataContract ClausePeriod { get; set; }

        [DataMember(Name = "cpo")]
        public RepositoryMetaItemDataContract ClausePeriodOther { get; set; }

        [DataMember(Name = "tc")]
        public RepositoryMetaItemDataContract TermsConditions { get; set; }

        [DataMember(Name = "de")]
        public RepositoryMetaItemDataContract DateELEA { get; set; }

        [DataMember(Name = "ea")]
        public RepositoryMetaItemDataContract EAAmount { get; set; }

        [DataMember(Name = "gth")]
        public RepositoryMetaItemDataContract GTHValidated { get; set; }

        [DataMember(Name = "sfn")]
        public RepositoryMetaItemDataContract SignerFirstName { get; set; }

        [DataMember(Name = "sln")]
        public RepositoryMetaItemDataContract SignerLastName { get; set; }

        [DataMember(Name = "sf")]
        public RepositoryMetaItemDataContract SignerFunction { get; set; }

        [DataMember(Name = "sof")]
        public RepositoryMetaItemDataContract SignerOtherFunction { get; set; }

        [DataMember(Name = "si")]
        public RepositoryMetaItemDataContract SignerIdentify { get; set; }
        /*
        [DataMember(Name = "d")]
        public string Department { get; set; }
         */
    }

}
