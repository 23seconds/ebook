﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FileServiceLogDataContract
    {
        [DataMember(Name = "act")]
        public string Action { get; set; }

        [DataMember(Name = "cm")]
        public string Comment { get; set; }

        [DataMember(Name = "st")]
        public string Status { get; set; }

        [DataMember(Name = "ts")]
        public DateTime Timestamp { get; set; }

        [DataMember(Name = "du")]
        public bool DocumentUploaded { get; set; }

        [DataMember(Name = "pi")]
        public Guid? PdfAttachment { get; set; }

        [DataMember(Name = "pn")]
        public string PersonName { get; set; }
    }
}
