﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FileServiceStatusDataContract
    {
        [DataMember(Name = "si")]
        public short StatusId { get; set; }

        [DataMember(Name = "s")]
        public string Status { get; set; }

        [DataMember(Name = "r")]
        public int Rank { get; set; }

        [DataMember(Name = "act")]
        public string Activation { get; set; }

        [DataMember(Name = "fus")]
        public string FollowUpStatuses { get; set; }

        [DataMember(Name = "fua")]
        public string FollowUpActions { get; set; }
    }
}
