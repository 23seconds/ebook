﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// MappingDataContract
    /// </summary>
    [DataContract]
    public class MappingDataContract
    {
        [DataMember(Name="k")]
        public string Key { get; set; }

        [DataMember(Name = "d")]
        public string Description { get; set; }

        [DataMember(Name = "o")]
        public int Order { get; set; }

        [DataMember(Name = "its")]
        public List<MappingListItemDataContract> Items { get; set; }
    }

}
