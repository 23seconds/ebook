﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class MappingItemDataContract : MappingListItemDataContract
    {
        [DataMember(Name = "Id", Order=4)]
        public Guid Id { get; set; }

        [DataMember(Name = "f", Order = 5)]
        public DateTime From { get; set; }

        [DataMember(Name = "t", Order = 6)]
        public DateTime? To { get; set; }

        [DataMember(Name = "k", Order = 7)]
        public string Key { get; set; }


    }
}
