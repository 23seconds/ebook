﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [KnownType(typeof(MappingMetaDataContract))]
    [KnownType(typeof(MappingItemDataContract))]
    [DataContract]
    [ProtoBuf.ProtoInclude(100,typeof(MappingMetaDataContract))]
    [ProtoBuf.ProtoInclude(101, typeof(MappingItemDataContract))]
    public class MappingListItemDataContract
    {
        [DataMember(Name = "d", Order=1)]
        public string Description { get; set; }

        [DataMember(Name = "m", Order = 2)]
        public string Meta { get; set; }

        [DataMember(Name = "lo", Order = 3)]
        public GuidListItemDataContract ListObject { get; set; }
    }

}
