﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ClientInfoDataContract
    {
        [DataMember(Name = "gfis")]
        public ClientDataContract Gfis { get; set; }

        [DataMember(Name = "overrule")]
        public ClientOverruleDataContract Overrule { get; set; }
    }

}
