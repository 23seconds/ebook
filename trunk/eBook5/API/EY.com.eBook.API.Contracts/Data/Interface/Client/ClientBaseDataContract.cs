﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// </summary>
    [DataContract]
    public class ClientBaseDataContract
    {
        /// <summary>
        /// Gets the id, item is read only to outside spaces
        /// </summary>
        /// <value>Client id.</value>
        [DataMember(Name = "id",Order=1)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the name, item is read only to outside spaces
        /// </summary>
        /// <value>Client name.</value>
        [DataMember(Name = "n", Order = 2)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the legal type id.
        /// </summary>
        /// <value>The legal type id.</value>
        [DataMember(Name = "lti", Order = 3)]
        public string LegalTypeId { get; set; }

        /// <summary>
        /// Gets the GFIS Code, item is read only to outside spaces
        /// </summary>
        /// <value>Client GFIS Code.</value>
        [DataMember(Name = "gc", Order = 4)]
        public string GfisCode { get; set; }

        /// <summary>
        /// Gets the address, item is read only to outside spaces
        /// </summary>
        /// <value>Client address.</value>
        [DataMember(Name = "a", Order = 5)]
        public string Address { get; set; }

        /// <summary>
        /// Gets the zipcode, item is read only to outside spaces
        /// </summary>
        /// <value>Client zipcode.</value>
        [DataMember(Name = "zip", Order = 6)]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets the city, item is read only to outside spaces
        /// </summary>
        /// <value>Client city.</value>
        [DataMember(Name = "ci", Order = 7)]
        public string City { get; set; }

        /// <summary>
        /// Gets the country, item is read only to outside spaces
        /// </summary>
        /// <value>Client country.</value>
        [DataMember(Name = "co", Order = 8)]
        public string Country { get; set; }
        
        /// <summary>
        /// Gets the vat, item is read only to outside spaces
        /// </summary>
        /// <value>Client Vat.</value>
        [DataMember(Name = "vnr", Order = 9)]
        public string Vat { get; set; }

        [DataMember(Name = "sh", Order = 10)]
        public bool Shared { get; set; }

        /// <summary>
        /// Gets the enterprisenumber, item is read only to outside spaces
        /// </summary>
        /// <value>Client enterprisenumber.</value>
        [DataMember(Name = "enr", Order = 11)]
        public string EnterpriseNumber { get; set; }

        [DataMember(Name = "pas", Order = 12)]
        public string ProAccServer { get; set; }

        [DataMember(Name = "pad", Order = 13)]
        public string ProAccDatabase { get; set; }

        [DataMember(Name = "palu", Order = 14)]
        public DateTime? ProAccLinkUpdated { get; set; }

        [DataMember(Name = "bizf", Order = 15)]
        public string BizTaxFolder {get;set;}

        [DataMember(Name = "roles", Order = 16)]
        public string[] Roles { get; set; }

        [DataMember(Name = "lts", Order = 17)]
        public string LegalTypeStructure { get; set; }

        [DataMember(Name = "bni", Order = 18)]
        public bool BNI { get; set; }

        [DataMember(Name = "foda")]
        [ProtoBuf.ProtoIgnore]
        public FodAddressDataContract FodAddress { get; set; }


        public string AddressPartStreet
        {
            get
            {
                string street = "";
                if (!string.IsNullOrEmpty(Address))
                {
                    street = Address;
                    Regex re = new Regex("(?<nr>[0-9]{1,})");
                    MatchCollection mc = re.Matches(Address);
                    if (mc.Count > 0)
                    {
                        street = re.Replace(street, "");
                       // street = street.Substring(0, mc[0].Index - 1).Trim();
                        return street;

                    }
                }
                return null;
            }
        }

        public string AddressPartNr
        {
            get
            {
                string street = "";
                if (!string.IsNullOrEmpty(Address))
                {
                    street = Address;
                    Regex re = new Regex("(?<nr>[0-9]{1,})");
                    MatchCollection mc = re.Matches(Address);
                    if (mc.Count > 0)
                    {
                        return mc[0].Value.Trim();
                        
                    }
                }
                return null;
            }
        }

        public string AddressPartBus
        {
            get
            {
                string street = "";
                if (!string.IsNullOrEmpty(Address))
                {
                    street = Address;
                    Regex re = new Regex("(?<nr>[0-9]{1,})");
                    MatchCollection mc = re.Matches(Address);
                    if (mc.Count > 0)
                    {
                        if (mc.Count > 1)
                        {
                            return mc[1].Value.Trim();
                        }

                    }
                }
                return null;
            }
        }
        

    }

}
