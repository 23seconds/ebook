﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class TeamMemberDataContract
    {
        [DataMember(Name="d")]
        public string Department { get; set; }

        [DataMember(Name = "pid")]
        public Guid PersonId { get; set; }

        [DataMember(Name = "cid")]
        public Guid ClientId { get; set; }

        [DataMember(Name = "r")]
        public string Role { get; set; }

        [DataMember(Name = "gfis")]
        public bool Gfis { get; set; }

        [DataMember(Name = "n")]
        public string Name { get; set; }

        [DataMember(Name = "m")]
        public string Mail { get; set; }

        [DataMember(Name = "l")]
        public int Level { get; set; }

    }
}
