﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaClientOptionalOverruleDataContract
    {
        [DataMember]
        public Guid ClientId { get; set; }

        [DataMember]
        public bool Overrule { get; set; }



    }
}
