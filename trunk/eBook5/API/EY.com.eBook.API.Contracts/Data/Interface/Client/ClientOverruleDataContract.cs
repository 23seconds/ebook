﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ClientOverruleDataContract
    /// </summary>
    [DataContract]
    public class ClientOverruleDataContract
    {
        /// <summary>
        /// Gets the id, item is read only to outside spaces
        /// </summary>
        /// <value>Client id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the name, item is read only to outside spaces
        /// </summary>
        /// <value>Client name.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }

        /// <summary>
        /// Gets the GFIS Code, item is read only to outside spaces
        /// </summary>
        /// <value>Client GFIS Code.</value>
        [DataMember(Name = "gc")]
        public string GfisCode { get; set; }

        /// <summary>
        /// Gets the address, item is read only to outside spaces
        /// </summary>
        /// <value>Client address.</value>
        [DataMember(Name = "a")]
        public string Address { get; set; }

        /// <summary>
        /// Gets the zipcode, item is read only to outside spaces
        /// </summary>
        /// <value>Client zipcode.</value>
        [DataMember(Name = "zip")]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets the city, item is read only to outside spaces
        /// </summary>
        /// <value>Client city.</value>
        [DataMember(Name = "ci")]
        public string City { get; set; }

        /// <summary>
        /// Gets the country, item is read only to outside spaces
        /// </summary>
        /// <value>Client country.</value>
        [DataMember(Name = "co")]
        public string Country { get; set; }

        /// <summary>
        /// Gets the vat, item is read only to outside spaces
        /// </summary>
        /// <value>Client Vat.</value>
        [DataMember(Name = "vnr")]
        public string Vat { get; set; }

        /// <summary>
        /// Gets the enterprisenumber, item is read only to outside spaces
        /// </summary>
        /// <value>Client enterprisenumber.</value>
        [DataMember(Name = "enr")]
        public string EnterpriseNumber { get; set; }

        /// <summary>
        /// Gets the phone, item is read only to outside spaces
        /// </summary>
        /// <value>Client phone.</value>
        [DataMember(Name = "ph")]
        public string Phone { get; set; }

        /// <summary>
        /// Gets the fax, item is read only to outside spaces
        /// </summary>
        /// <value>Client fax.</value>
        [DataMember(Name = "fx")]
        public string Fax { get; set; }

        /// <summary>
        /// Gets the email, item is read only to outside spaces
        /// </summary>
        /// <value>Client email.</value>
        [DataMember(Name = "em")]
        public string Email { get; set; }

        /// <summary>
        /// Gets the legal structure, item is read only to outside spaces
        /// </summary>
        /// <value>Client legal structure.</value>
        [DataMember(Name = "ls")]
        public string LegalStructure { get; set; }

        /// <summary>
        /// Gets the rpr, item is read only to outside spaces
        /// </summary>
        /// <value>Client rpr.</value>
        [DataMember(Name = "rpr")]
        public string Rpr { get; set; }

        /// <summary>
        /// Gets wether the client is Vat mandatory , item is read only to outside spaces
        /// </summary>
        /// <value>Client Vat mandatory.</value>
        [DataMember(Name = "iv")]
        public bool? IsVat { get; set; }

        /// <summary>
        /// Gets the director's name, item is read only to outside spaces
        /// </summary>
        /// <value>Client director's name.</value>
        [DataMember(Name = "dn")]
        public string DirectorName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value><c>true</c> if this instance is active; otherwise, <c>false</c>.</value>
        [DataMember(Name = "ia")]
        public bool? IsActive { get; set; }

        /// <summary>
        /// Gets or sets the legal type id.
        /// </summary>
        /// <value>The legal type id.</value>
        [DataMember(Name = "lti")]
        public string LegalTypeId { get; set; }

        [DataMember(Name = "foda")]
        public FodAddressDataContract FodAddress { get; set; }
    
    }

}
