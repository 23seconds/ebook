﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class EngagementCodeDataContract
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "cid")]
        public string CustomerId { get; set; }

        [DataMember(Name = "d")]
        public string Description { get; set; }

        [DataMember(Name = "s")]
        public string Status { get; set; }

        [DataMember(Name = "sl")]
        public string ServiceLine { get; set; }

        [DataMember(Name = "sld")]
        public string ServiceLineDescription { get; set; }

        [DataMember(Name = "bu")]
        public string BU { get; set; }
    }
}
