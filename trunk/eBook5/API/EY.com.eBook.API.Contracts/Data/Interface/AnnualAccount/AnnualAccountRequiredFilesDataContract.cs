﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class AnnualAccountRequiredFilesDataContract
    {
        [DataMember]
        public List<string> FileTypesMissing { get; set; }

        [DataMember]
        public List<string> FileTypesDuplicates { get; set; }

        [DataMember]
        public List<RepositoryItemDataContract> FilesFound { get; set; }
    }
}
