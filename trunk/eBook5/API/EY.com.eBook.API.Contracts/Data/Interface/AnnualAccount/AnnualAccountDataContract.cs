﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class AnnualAccountDataContract
    {
        [DataMember(Name = "repositoryItemId")]
        public Guid? RepositoryItemId { get; set; }

        [DataMember(Name = "definitiveGeneralAssembly")]
        public DateTime? DefinitiveGeneralAssembly { get; set; }

        [DataMember(Name = "companyType")]
        public string CompanyType { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "changesAfterGeneralAssembly")]
        public bool? ChangesAfterGeneralAssembly { get; set; }

        [DataMember(Name = "changesAfterGeneralAssemblyDescription")]
        public string ChangesAfterGeneralAssemblyDescription { get; set; }

        [DataMember(Name = "engagementCode")]
        public string EngagementCode { get; set; }

        [DataMember(Name = "invoiceLanguage")]
        public string InvoiceLanguage { get; set; }

        [DataMember(Name = "invoiceRechargeFilingExpense")]
        public string InvoiceRechargeFilingExpense { get; set; }

        [DataMember(Name = "invoiceAmount")]
        public decimal? InvoiceAmount { get; set; }

        [DataMember(Name = "invoiceStructuredMessage")]
        public string InvoiceStructuredMessage { get; set; }

        [DataMember(Name = "invoiceRepositoryItemId")]
        public Guid? InvoiceRepositoryItemId { get; set; }

        [DataMember(Name = "engagementPartner")]
        public string EngagementPartner { get; set; }

        [DataMember(Name = "companyNumber")]
        public string CompanyNumber { get; set; }

        [DataMember(Name = "engagementManager")]
        public string EngagementManager { get; set; }

        [DataMember(Name = "office")]
        public string Office { get; set; }

        [DataMember(Name = "serviceLine")]
        public string ServiceLine { get; set; }

        [DataMember(Name = "staffing")]
        public string Staffing { get; set; }

        [DataMember(Name = "timeLimit")]
        public string TimeLimit { get; set; }

        [DataMember(Name = "fileId")]
        public Guid FileId { get; set; }
    }
}
