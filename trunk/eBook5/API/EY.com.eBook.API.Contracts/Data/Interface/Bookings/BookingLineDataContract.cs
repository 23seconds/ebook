﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BookingLineDataContract
    /// </summary>
    [DataContract]
    public class BookingLineDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id", Order = 1)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the booking id.
        /// </summary>
        /// <value>The booking id.</value>
        [DataMember(Name = "bi", Order = 2)]
        public Guid BookingId { get; set; }


        /// <summary>
        /// Gets or sets the business relation id.
        /// </summary>
        /// <value>The business relation id.</value>
        [DataMember(Name = "bri", Order = 3)]
        public Guid? BusinessRelationId { get; set; }
        
        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount.</value>
        [DataMember(Name = "a", Order = 4)]
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the account nr.
        /// </summary>
        /// <value>The account nr.</value>
        [DataMember(Name = "anr", Order = 5)]
        public StringListItemDataContract AccountNr { get; set; }


        [DataMember(Name = "adesc", Order = 6)]
        public string AccountDescription { get; set; }

        // <summary>
        /// Gets or sets the account nr.
        /// </summary>
        /// <value>The account nr.</value>
        [DataMember(Name = "vnr", Order = 7)]
        public string VisibleNr { get; set; }

        [DataMember(Name = "com", Order = 8)]
        public string Comment { get; set; }

        [DataMember(Name = "brn", Order = 9)]
        public string BusinessRelationName { get; set; }
    }
}
