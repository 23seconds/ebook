﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaBookingsDataContract
    /// </summary>
    [DataContract]
    public class CriteriaBookingsDataContract
    {   
        /// <summary>
        /// Fields to search in
        /// </summary>
        /// <value>Fields.</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember]
        public string Culture { get; set; }


    }
}
