﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BookingSingleDataContract
    /// </summary>
    [DataContract]
    public class BookingSingleDataContract
    {
        #region BookingInfo 

        /// <summary>
        /// Gets or sets the booking id.
        /// </summary>
        /// <value>The booking id.</value>
        [DataMember(Name = "bid")]
        public Guid? BookingId { get; set; }

        /// <summary>
        /// Gets or sets the nr.
        /// </summary>
        /// <value>The nr.</value>
        [DataMember(Name = "bcd")]
        public DateTime BookingCreationDate { get; set; }

        [DataMember(Name = "gnr")]
        public int GroupNr { get; set; }

        /// <summary>
        /// Gets or sets the booking description.
        /// </summary>
        /// <value>The booking description.</value>
        [DataMember(Name = "bd")]
        public string BookingDescription { get; set; }


        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "wt")]
        public Guid? WorksheetTypeId { get; set; }

        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "wtc")]
        public string WorksheetCollection { get; set; }

        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "wtr")]
        public Guid? WorksheetRowId { get; set; }

        /// <summary>
        /// Gets or sets the worksheet booking key.
        /// </summary>
        /// <value>The worksheet booking key.</value>
        [DataMember(Name = "wbk")]
        public string WorksheetBookingKey { get; set; }


        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "ec")]
        public short ExportCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="BookingDataContract"/> is hidden.
        /// </summary>
        /// <value><c>true</c> if hidden; otherwise, <c>false</c>.</value>
        [DataMember(Name = "h")]
        public bool? Hidden { get; set; }

        #endregion
        #region Booking LINE info
        
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id")]
        public Guid? Id { get; set; }
        

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount.</value>
        [DataMember(Name = "d")]
        public decimal? Debet { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount.</value>
        [DataMember(Name = "c")]
        public decimal? Credit { get; set; }

        /// <summary>
        /// Gets or sets the account nr.
        /// </summary>
        /// <value>The account nr.</value>
        [DataMember(Name = "acc")]
        public StringListItemDataContract Account { get; set; }

        [DataMember(Name = "ianr")]
        public string AccountNr { get; set; }

        [DataMember(Name = "com")]
        public string Comment { get; set; }

        [DataMember(Name = "csi")]
        public Guid? ClientSupplierId { get; set; }

        [DataMember(Name = "csn")]
        public string ClientSupplierName { get; set; }

        #endregion
    }
}
