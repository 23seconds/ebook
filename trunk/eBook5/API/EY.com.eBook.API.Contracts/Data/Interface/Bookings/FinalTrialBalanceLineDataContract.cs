﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// FinalTrialBalanceLineDataContract
    /// </summary>
    [DataContract]
    public class FinalTrialBalanceLineDataContract
    {
        #region BookingInfo 

        /// <summary>
        /// Gets or sets the booking id.
        /// </summary>
        /// <value>The booking id.</value>
        [DataMember(Name = "ian")]
        public string InternalAccountNr { get; set; }

        [DataMember(Name = "vnr")]
        public string VisualNr { get; set; }

        [DataMember(Name = "dc")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the nr.
        /// </summary>
        /// <value>The nr.</value>
        [DataMember(Name = "acc")]
        public StringListItemDataContract Account { get; set; }

        /// <summary>
        /// Gets or sets the booking description.
        /// </summary>
        /// <value>The booking description.</value>
        [DataMember(Name = "sb")]
        public decimal StartBalance { get; set; }

        
        /// <summary>
        /// Gets or sets the type of the journal.
        /// </summary>
        /// <value>The type of the journal.</value>
        [DataMember(Name = "ad")]
        public decimal AdjustmentsDebet { get; set; }

        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "ac")]
        public decimal AdjustmentsCredit { get; set; }


        /// <summary>
        /// Gets or sets the worksheet booking key.
        /// </summary>
        /// <value>The worksheet booking key.</value>
        [DataMember(Name = "eb")]
        public decimal EndBalance { get; set; }

      
        #endregion
        
    }
}
