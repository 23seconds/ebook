﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BookingDataContract
    /// </summary>
    [DataContract]
    public class BookingDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id",Order=1)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember(Name = "ft", Order = 2)]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the nr.
        /// </summary>
        /// <value>The nr.</value>
        [DataMember(Name = "cd", Order = 3)]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember(Name = "ds", Order = 4)]
        public string Description { get; set; }
              

        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "wt", Order = 5)]
        public Guid? WorksheetTypeId { get; set; }

        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "wtc", Order = 6)]
        public string WorksheetCollection { get; set; }

        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "wtr", Order = 7)]
        public Guid? WorksheetRowId { get; set; }

        /// <summary>
        /// Gets or sets the worksheet booking key.
        /// </summary>
        /// <value>The worksheet booking key.</value>
        [DataMember(Name = "wbk", Order = 8)]
        public string WorksheetBookingKey { get; set; }


        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "ec", Order = 9)]
        public short ExportCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="BookingDataContract"/> is hidden.
        /// </summary>
        /// <value><c>true</c> if hidden; otherwise, <c>false</c>.</value>
        [DataMember(Name = "h", Order = 10)]
        public bool Hidden { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="BookingDataContract"/> is deleted.
        /// </summary>
        /// <value><c>true</c> if deleted; otherwise, <c>false</c>.</value>
        [DataMember(Name = "d", Order = 11)]
        public bool Deleted { get; set; }
        
        /// <summary>
        /// Gets or sets the booking lines.
        /// </summary>
        /// <value>The booking lines.</value>
        [DataMember(Name = "bl", Order = 12, IsRequired=true)]
        public List<BookingLineDataContract> BookingLines { get; set; }

        
    }
}
