﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaFilesDataContract
    /// </summary>
    [DataContract]
    public class CriteriaFilesDataContract
    {
        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>The client id.</value>
        [DataMember]
        public Guid ClientId { get; set; }

        /// <summary>
        /// Gets or sets filter for files marked for deletion.
        /// </summary>
        /// <value>Marked For Deletion.</value>
        [DataMember]
        public bool MarkedForDeletion { get; set; }

        /// <summary>
        /// Gets or sets filter for deleted files 
        /// </summary>
        /// <value>Deleted.</value>
        [DataMember]
        public bool Deleted { get; set; }


        /// <summary>
        /// Gets or sets filter for closed files.
        /// </summary>
        /// <value>Closed.</value>
        [DataMember]
        public bool? Closed { get; set; }


        [DataMember]
        public string query { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }
    }
}

