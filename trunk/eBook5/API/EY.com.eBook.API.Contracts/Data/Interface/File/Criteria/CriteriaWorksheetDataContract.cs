﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaWorksheetDataContract
    /// </summary>
    [DataContract]
    public class CriteriaWorksheetDataContract
    {
        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the type id.
        /// </summary>
        /// <value>The type id.</value>
        [DataMember]
        public Guid TypeId { get; set; }

        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; }
    }
}
