﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaCreateFileDataContract
    /// </summary>
    [DataContract]
    public class CriteriaCreateFileDataContract
    {
       

        /// <summary>
        /// Client id
        /// </summary>
        /// <value>Client id.</value>
        [DataMember(Name = "cid")]
        public Guid ClientId { get; set; }

        [DataMember(Name = "as")]
        public List<Guid> ActiveServices { get; set; }

        /// <summary>
        /// Culture
        /// </summary>
        /// <value>Culture.</value>
        [DataMember(Name = "c")]
        public string Culture { get; set; }

        /// <summary>
        /// StartDate
        /// </summary>
        /// <value>StartDate.</value>
        [DataMember(Name = "s")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// EndDate
        /// </summary>
        /// <value>EndDate.</value>
        [DataMember(Name = "e")]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// PreviousStartDate
        /// </summary>
        /// <value>PreviousStartDate.</value>
        [DataMember(Name = "ps")]
        public DateTime? PreviousStartDate { get; set; }

        /// <summary>
        /// PreviousEndDate
        /// </summary>
        /// <value>PreviousEndDate.</value>
        [DataMember(Name = "pe")]
        public DateTime? PreviousEndDate { get; set; }

        /// <summary>
        /// PreviousFileId
        /// </summary>
        /// <value>PreviousFileId.</value>
        [DataMember(Name="pfid")]
        public Guid? PreviousFileId { get; set; }

        [DataMember(Name="fs")]
        public FileSettingsDataContract Settings {get;set;}

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

  
        [DataMember(Name="it")]
        public string ImportType {get;set;}
    }
}
