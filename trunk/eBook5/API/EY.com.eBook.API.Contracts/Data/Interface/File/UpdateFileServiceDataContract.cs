﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class UpdateFileServiceDataContract
    {
        [DataMember]
        public Guid FileId { get; set; }

        [DataMember]
        public Guid ServiceId { get; set; }

        [DataMember]
        public Guid? BundleId { get; set; }

        [DataMember]
        public int? Status { get; set; }

        [DataMember]
        public String Comment { get; set; }

        [DataMember]
        public Guid? RepositoryItem { get; set; }

        [DataMember]
        public Guid? PdfAttachment { get; set; }

        [DataMember]
        public bool? Locked { get; set; }

        [DataMember]
        public string Actions { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }
}
