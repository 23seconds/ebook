﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FileSettingsDataContract
    {
        [DataMember(Name="fid", Order=1)]
        public Guid FileId { get; set; }

        [DataMember(Name = "fby", Order = 2)]
        public bool FirstBookyear { get; set; }

        [DataMember(Name = "oft", Order = 3)]
        public bool OneOfFirstThree { get; set; }

        [DataMember(Name = "bps", Order = 4)]
        public bool BeforePreviousPeriodSmall { get; set; }

        [DataMember(Name = "ps", Order = 5)]
        public bool PreviousPeriodSmall { get; set; }

        [DataMember(Name = "cs", Order = 6)]
        public bool CurrentPeriodSmall { get; set; }

        [DataMember(Name = "ts", Order = 7)]
        public bool TaxShelter { get; set; }

       
    }
}
