﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FileSettingsInfoDataContract
    {
        [DataMember]
        public Guid FileId { get; set; }

        [DataMember]
        public FileSettingsDataContract Settings { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public DateTime? PreviousStartDate { get; set; }
        
        [DataMember]
        public DateTime? PreviousEndDate { get; set; }

    }
}
