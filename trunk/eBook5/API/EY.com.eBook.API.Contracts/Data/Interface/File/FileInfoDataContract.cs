﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FileInfoDataContract
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        //public FileTypeDataContract FileType { get; set; }

        [DataMember(Name="cid")]
        public Guid ClientId { get; set; }

        [DataMember(Name = "n")]
        public string Name { get; set; }

        [DataMember(Name = "dsp")]
        public string Display { get; set; }

        [DataMember(Name = "c")]
        public string Culture { get; set; }

        [DataMember(Name = "sd")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "ed")]
        public DateTime EndDate { get; set; }

        [DataMember(Name = "psd")]
        public DateTime? PreviousStartDate { get; set; }

        [DataMember(Name = "ped")]
        public DateTime? PreviousEndDate { get; set; }

        [DataMember(Name = "nl")]
        public bool NumbersLocked { get; set; }

        [DataMember(Name = "cl")]
        public bool Closed { get; set; }

        [DataMember(Name = "ay")]
        public int AssessmentYear
        {
            get { return EndDate.AddDays(1).Year; }
            set { }
        }

        [DataMember(Name = "ers")]
        public int Errors { get; set; }

        [DataMember(Name = "wrs")]
        public int Warnings { get; set; }

        [DataMember(Name = "imd")]
        public DateTime? ImportDate { get; set; }

        [DataMember(Name = "imt")]
        public string ImportType { get; set; }

        [DataMember(Name = "svc")]
        public List<FileServiceDataContract> Services { get; set; }

    }
}
