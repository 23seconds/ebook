﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ImportAccountseBookDataContract
    {
        [DataMember(Name="fid")]
        public Guid FileId { get; set; }

        [DataMember(Name = "ips")]
        public bool IncludePreviousSaldi { get; set; }
    }
}
