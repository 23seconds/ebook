﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FileActionDataContract
    {
        [DataMember(Name="fid")]
        public Guid FileId { get; set; }

        [DataMember(Name = "a")]
        public string Action { get; set; }

        [DataMember(Name = "am")]
        public string ActionMessage { get; set; }

        [DataMember(Name = "d")]
        public DateTime Date { get; set; }

        [DataMember(Name = "p")]
        public PersonBaseDataContract Person { get; set; }

        [DataMember(Name = "m")]
        public string Meta { get; set; }
    }
}
