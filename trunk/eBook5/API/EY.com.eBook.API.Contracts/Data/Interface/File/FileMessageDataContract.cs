﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public enum FileMessageConnectionTypeDataContract
    {
        [EnumMember]
        WORKSHEET,

        [EnumMember]
        ACCOUNT,

        [EnumMember]
        DOCUMENT,

        [EnumMember]
        BUNDLE
    }

    [DataContract]
    public class FileMessageDataContract
    {
        [DataMember(Name="fid", Order=1, IsRequired=true)]
        public Guid FileId { get; set; }

        [DataMember(Name = "mid", Order = 2, IsRequired = true)]
        public Guid MessageId { get; set; }

        [DataMember(Name = "c", Order = 3, IsRequired = true)]
        public string Culture { get; set; }

        [DataMember(Name = "tp", Order = 4, IsRequired = true)]
        public MessageTypeDataContract Type { get; set; }

        [DataMember(Name = "tx", Order = 5, IsRequired = true)]
        public string Text { get; set; }

        [DataMember(Name = "ct", Order = 6, IsRequired = true)]
        public FileMessageConnectionTypeDataContract ConnectionType { get; set; }

        [DataMember(Name = "ca", Order = 7, IsRequired = true)]
        public string ConnectionAccount { get; set; }

        [DataMember(Name = "cg", Order = 8, IsRequired = true)]
        public Guid? ConnectionGuid { get; set; }

        [DataMember(Name = "wtc", Order = 9, IsRequired = true)]
        public string WorksheetTypeCollection { get; set; }

        [DataMember(Name = "wtrid", Order = 10, IsRequired = true)]
        public Guid? WorksheetTypeRowId { get; set; }

        [DataMember(Name = "wtfld", Order = 11, IsRequired = true)]
        public string WorksheetTypeField { get; set; }

        [DataMember(Name = "wtct", Order = 12, IsRequired = false)]
        public string WorksheetTypeCollectionText { get; set; }

        [DataMember(Name = "wtfldt", Order = 13, IsRequired = false)]
        public string WorksheetTypeFieldText { get; set; } 



    }
}
