﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class GTHClientStatusDataContract
    {

        [DataMember()]
        public Guid Id { get; set; }

        [DataMember()]
        public Guid ClientId { get; set; }

        [DataMember()]
        public string ClientName { get; set; }

        [DataMember()]
        public string ClientGfis { get; set; }

        [DataMember()]
        public string ClientEnterprise { get; set; }

        [DataMember()]
        public int TeamId { get; set; }

        [DataMember()]
        public string Status { get; set; }

        [DataMember()]
        public string ServiceStatus { get; set; }

        [DataMember()]
        public Int16 ServiceStatusId { get; set; }

        [DataMember()]
        public DateTime LastUpdate { get; set; }

        [DataMember()]
        public DateTime? StartDate { get; set; }

        [DataMember()]
        public DateTime? EndDate { get; set; }

        [DataMember()]
        public Guid PersonId { get; set; }

        [DataMember()]
        public string PersonName { get; set; }

    }
}
