﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// PersonDataContract, only list related data (grid view)
    /// DB:viewPersons
    /// </summary>
    [DataContract]
    public class GTHPersonClientDataContract
    {
        [DataMember(Name="pid")]
        public Guid PersonId { get; set; }

        [DataMember(Name="fn")]
        public string FirstName { get; set; }

        [DataMember(Name="ln")]
        public string LastName { get; set; }

        [DataMember(Name="cc")]
        public int ActiveClientCount { get; set; }
    }

}
