﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class CriteriaGTHTeamDataContract
    {

        [DataMember()]
        public int TeamId { get; set; }

        [DataMember()]
        public List<string> StatusList { get; set; }

    }
}
