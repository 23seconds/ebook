﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaReplacePersonsDataContract
    {
        [DataMember(Name = "pf")]
        public Guid personFrom { get; set; }

        [DataMember(Name = "pt")]
        public Guid personTo { get; set; }

        [DataMember(Name = "t")]
        public int team { get; set; }
    }
}
