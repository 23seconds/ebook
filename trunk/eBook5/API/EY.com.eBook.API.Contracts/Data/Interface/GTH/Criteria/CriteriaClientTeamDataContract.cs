﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class CriteriaClientTeamDataContract
    {

        [DataMember()]
        public int TeamId { get; set; }

        [DataMember()]
        public Guid? GTHId { get; set; }

        [DataMember()]
        public string Status { get; set; }

        [DataMember()]
        public string SubStatus { get; set; }

        [DataMember()]
        public Guid PersonId { get; set; }

    }
}
