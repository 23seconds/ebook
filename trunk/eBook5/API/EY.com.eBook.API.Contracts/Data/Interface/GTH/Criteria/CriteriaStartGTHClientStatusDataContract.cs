﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class CriteriaStartGTHClientStatusDataContract
    {

        [DataMember()]
        public Guid StatusId { get; set; }

        [DataMember()]
        public Guid PersonId { get; set; }

        [DataMember()]
        public string ToStatus { get; set; }

    }
}
