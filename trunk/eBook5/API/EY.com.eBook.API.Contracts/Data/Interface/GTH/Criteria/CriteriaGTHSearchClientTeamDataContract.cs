﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class CriteriaGTHSearchClientTeamDataContract
    {

        [DataMember()]
        public int TeamId { get; set; }

        [DataMember()]
        public Guid PersonId { get; set; }

        [DataMember()]
        public List<string> StatusList { get; set; }

        /// <summary>
        /// Fields to search in
        /// </summary>
        /// <value>Fields.</value>
        [DataMember]
        public string[] Fields { get; set; }

        /// <summary>
        /// String to search for
        /// </summary>
        /// <value>query.</value>
        [DataMember]
        public string Query { get; set; }

        /// <summary>
        /// Limit for paging
        /// </summary>
        /// <value>Limit.</value>
        [DataMember]
        public int? Limit { get; set; }

        /// <summary>
        /// Start from... (for paging)
        /// </summary>
        /// <value>Start.</value>
        [DataMember]
        public int? Start { get; set; }  

    }
}
