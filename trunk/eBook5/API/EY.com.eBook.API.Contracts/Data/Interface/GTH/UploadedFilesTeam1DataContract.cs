﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class UploadedFileDataContract
    {

        [DataMember()]
        public Guid Id { get; set; }

        [DataMember()]
        public string ContentType { get; set; }

        [DataMember()]
        public string Extension { get; set; }

        [DataMember()]
        public string Name { get; set; }

        [DataMember(Name="fileName")]
        public string FileName { get; set; }

        [DataMember()]
        public DateTime Start { get; set; }

        [DataMember()]
        public DateTime End { get; set; }

        [DataMember(Name = "fileNameParentheticalRemark")]
        public string FileNameParentheticalRemark { get; set; }
    }
}
