﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{

    [DataContract]
    public class GTHClientStartDataContract
    {

        
        [DataMember()]
        public int TeamId { get; set; }

        [DataMember()]
        public List<GTHClientStatusDataContract> Todo{ get; set; }

        [DataMember()]
        public List<GTHClientStatusDataContract> Progress { get; set; }

        [DataMember()]
        public List<GTHClientStatusDataContract> Final { get; set; }

    }
}
