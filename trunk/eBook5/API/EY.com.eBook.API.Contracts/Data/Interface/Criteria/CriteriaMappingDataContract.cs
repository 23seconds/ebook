﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaMappingDataContract
    {

        [DataMember(Order=1)]
        public string Key { get; set; }


        [DataMember(Order = 2)]
        public string Meta { get; set; }

        [DataMember(Order = 3)]
        public string Culture { get; set; }
    }
}
