﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CoefficientDataContract
    {
        [DataMember(Name = "gk", Order = 1)]
        public string GroupKey { get; set; }

        [DataMember(Name = "k", Order = 2)]
        public string Key { get; set; }

        [DataMember(Name = "v", Order = 3)]
        public decimal Value { get; set; }

        [DataMember(Name = "ay", Order = 4)]
        public int? AssessmentYear { get; set; }

        [DataMember(Name = "sd", Order = 5)]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "ed", Order = 6)]
        public DateTime? EndDate { get; set; }

        [DataMember(Name = "d", Order = 7)]
        public string Description { get; set; }

        [DataMember(Name = "id", Order = 8)]
        public Guid Id { get; set; }


    }

    public class CoefficientFixedDataContract
    {
        [DataMember]
        public string GroupKey { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public decimal Value { get; set; }

        [DataMember]
        public int AssessmentYear { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }


    }
}
