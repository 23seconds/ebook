﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CoefficientGroupDescriptionDataContract
    {
        [DataMember(Name = "id", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Name = "t", Order = 2)]
        public string Translation { get; set; }

        [DataMember(Name = "c", Order = 2)]
        public string Culture { get; set; }
    }
}
