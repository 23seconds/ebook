﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaCoefficientsListItemDataContract
    {
        [DataMember]
        public Guid ListItemId { get; set; }

        [DataMember]
        public int? AssessmentYear { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }


    }
}
