﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaCoefficientDataContract
    {
        [DataMember]
        public Guid GroupId { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? AssessmentYear { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public decimal Value { get; set; }

        [DataMember]
        public Guid Id { get; set; }


    }
}
