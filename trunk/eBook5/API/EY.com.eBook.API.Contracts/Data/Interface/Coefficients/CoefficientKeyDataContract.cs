﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CoefficientKeyDataContract
    {
        [DataMember(Name = "id", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Name = "k", Order = 2)]
        public string Key { get; set; }

        [DataMember(Name = "d", Order = 3)]
        public string Description { get; set; }

        [DataMember(Name = "s", Order = 4)]
        public CoefficientKeySettingsDataContract Settings { get; set; }
    }
}
