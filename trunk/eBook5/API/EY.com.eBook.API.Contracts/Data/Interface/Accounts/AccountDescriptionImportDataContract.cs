﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class AccountDescriptionImportDataContract
    {
        [DataMember]
        public string AccountNr { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
