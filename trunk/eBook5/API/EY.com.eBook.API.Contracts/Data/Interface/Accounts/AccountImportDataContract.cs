﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class AccountImportDataContract
    {
        [DataMember]
        public string AccountNr { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public decimal PreviousAmount { get; set; }

    }
}
