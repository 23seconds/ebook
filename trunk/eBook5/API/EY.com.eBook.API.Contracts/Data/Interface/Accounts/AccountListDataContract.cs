﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// AccountListDataContract
    /// </summary>
    [DataContract]
    public class AccountListDataContract
    {
        /// <summary>
        /// FileID
        /// </summary>
        /// <value>The file ID.</value>
        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }

        /// <summary>
        /// InternalNr
        /// </summary>
        /// <value>The internal nr.</value>
        [DataMember(Name = "inr")]
        public string InternalNr { get; set; }

        /// <summary>
        /// Visual nr
        /// </summary>
        /// <value>The visual nr.</value>
        [DataMember(Name = "vnr")]
        public string VisualNr { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember(Name = "d")]
        public string Description { get; set; }

        /// <summary>
        /// imported id
        /// </summary>
        /// <value>The imported nr.</value>
        [DataMember(Name = "iid")]
        public string ImportedId { get; set; }

       

        /// <summary>
        /// HasTaxAdjustments
        /// </summary>
        /// <value>HasTaxAdjustments.</value>
        [DataMember(Name = "hta")]
        public bool HasTaxAdjustments { get; set; }

        /// <summary>
        /// ExistedLastYear
        /// </summary>
        /// <value>ExistedLastYear.</value>
        [DataMember(Name = "ely")]
        public bool ExistedLastYear { get; set; }

        [DataMember(Name = "ta")]
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Gets or sets the total amount last bookyear.
        /// </summary>
        /// <value>The total amount.</value>
        [DataMember(Name = "tap")]
        public decimal TotalAmountPrevious { get; set; }

    }
}

