﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaAccountsByRangesDataContract
    /// </summary>
    [DataContract]
    public class CriteriaAccountsByRangesDataContract : CriteriaAccountsDataContract
    {
        
        /// <summary>
        /// Gets or sets the ranges.
        /// </summary>
        /// <value>The ranges.</value>
        [DataMember]
        public string[] Ranges { get; set; }

        
    }
}


