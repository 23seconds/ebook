﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaAccountDataContract
    /// </summary>
    [DataContract]
    public class CriteriaAccountsDataContract : CriteriaBaseDataContract
    {
        /// <summary>
        /// FileID
        /// </summary>
        /// <value>The file ID.</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember]
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the flag whether to include zero saldi.
        /// </summary>
        /// <value>The flag.</value>
        [DataMember]
        public bool? IncludeZeroes { get; set; }

        [DataMember]
        public bool? IncludeMappingDetails { get; set; }

        [DataMember]
        public bool? IncludeAllDescriptions { get; set; }
    }
}
