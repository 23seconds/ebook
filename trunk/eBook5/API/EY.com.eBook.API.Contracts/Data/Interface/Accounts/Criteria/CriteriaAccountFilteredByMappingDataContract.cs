﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaAccountFilteredByMappingDataContract
    /// </summary>
    [DataContract]
    public class CriteriaAccountFilteredByMappingDataContract : CriteriaAccountsDataContract
    {

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Meta { get; set; }

        [DataMember]
        public List<string> MappingItemIds { get; set; }
    }
}
