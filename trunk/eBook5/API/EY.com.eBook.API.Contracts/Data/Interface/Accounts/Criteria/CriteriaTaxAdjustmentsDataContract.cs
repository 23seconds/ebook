﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaTaxAdjustmentsDataContract
    /// </summary>
    [DataContract]
    public class CriteriaTaxAdjustmentsDataContract
    {
        
        [DataMember]
        public AccountDataContract Parent { get; set; }

        [DataMember]
        public List<AccountDataContract> Adjustments { get; set; }
    }
}
