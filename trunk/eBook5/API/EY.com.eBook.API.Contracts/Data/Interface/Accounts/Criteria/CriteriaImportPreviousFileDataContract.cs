﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaImportPreviousFileDataContract
    /// </summary>
    [DataContract]
    public class CriteriaImportPreviousFileDataContract
    {
        
        [DataMember]
        public Guid TargetFileId { get; set; }

        [DataMember]
        public Guid SourceFileId { get; set; }

        public bool IncludePreviousSaldi { get; set; }

    }
}
