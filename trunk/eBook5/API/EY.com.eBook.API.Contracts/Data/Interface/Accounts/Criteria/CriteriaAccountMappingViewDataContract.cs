﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaAccountMappingViewDataContract
    /// </summary>
    [DataContract]
    public class CriteriaAccountMappingViewDataContract
    {
        /// <summary>
        /// FileID
        /// </summary>
        /// <value>The file ID.</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember]
        public string Culture { get; set; }


        [DataMember]
        public bool? IncludeZeroes { get; set; }

        [DataMember]
        public bool? MappedOnly { get; set; }

        [DataMember]
        public string Query { get; set; }

    }
}
