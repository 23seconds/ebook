﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaAccountMappingDataContract
    /// </summary>
    [DataContract]
    public class CriteriaAccountMappingDataContract
    {
        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the internal nr.
        /// </summary>
        /// <value>The internal nr.</value>
        [DataMember]
        public string InternalNr { get; set; }


        [DataMember]
        public string MappingKey { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }
}
