﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaShemaStateDataContract
    /// </summary>
    [DataContract]
    public class CriteriaShemaStateDataContract
    {
        /// <summary>
        /// FileID
        /// </summary>
        /// <value>The file ID.</value>
        [DataMember]
        public Guid FileId { get; set; }


        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public FileSchemaState SchemaState {get;set;}


    }
}
