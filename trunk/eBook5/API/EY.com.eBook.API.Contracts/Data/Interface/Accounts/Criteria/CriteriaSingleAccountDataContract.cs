﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaSingleAccountDataContract
    /// </summary>
    [DataContract]
    public class CriteriaSingleAccountDataContract
    {
        /// <summary>
        /// FileID
        /// </summary>
        /// <value>The file ID.</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// InternalNr
        /// </summary>
        /// <value>The internal nr.</value>
        [DataMember]
        public string InternalNr { get; set; }


    }
}
