﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class AccountStateDataContract
    {
        
        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }

        /// <summary>
        /// InternalNr
        /// </summary>
        /// <value>The internal nr.</value>
        [DataMember(Name = "inr")]
        public string InternalNr { get; set; }

        /// <summary>
        /// Visual nr
        /// </summary>
        /// <value>The visual nr.</value>
        [DataMember(Name = "vnr")]
        public string VisualNr { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember(Name = "d")]
        public string Description { get; set; }

        [DataMember(Name = "db")]
        public decimal Debet { get; set; }

        [DataMember(Name = "cr")]
        public decimal Credit { get; set; }
    }
}
