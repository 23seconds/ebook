﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// LeadSheetLineDataContract
    /// </summary>
    [DataContract]
    public class LeadSheetLineDataContract
    {
        #region BookingInfo 

        /// <summary>
        /// Gets or sets the booking id.
        /// </summary>
        /// <value>The booking id.</value>
        [DataMember(Name = "ian")]
        public string InternalNr { get; set; }

        [DataMember(Name = "vnr")]
        public string VisualNr { get; set; }

        [DataMember(Name = "dc")]
        public string Description { get; set; }


        /// <summary>
        /// Gets or sets the booking description.
        /// </summary>
        /// <value>The booking description.</value>
        [DataMember(Name = "sb")]
        public decimal StartBalance { get; set; }

        
        /// <summary>
        /// Gets or sets the type of the journal.
        /// </summary>
        /// <value>The type of the journal.</value>
        [DataMember(Name = "ad")]
        public decimal Adjustments{ get; set; }


        /// <summary>
        /// Gets or sets the worksheet booking key.
        /// </summary>
        /// <value>The worksheet booking key.</value>
        [DataMember(Name = "eb")]
        public decimal EndBalance { get; set; }

        /// <summary>
        /// Gets or sets the worksheet booking key.
        /// </summary>
        /// <value>The worksheet booking key.</value>
        [DataMember(Name = "pb")]
        public decimal PreviousBalance { get; set; }

      
        #endregion
        
    }
}
