﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// WorksheetTypeDataContract
    /// </summary>
    [DataContract]
    public class WorksheetTypeDataContract
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        [DataMember(Name = "ra")]
        public string RuleApp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is fully editable.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is fully editable; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "o")]
        public int Order { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value><c>true</c> if this instance is active; otherwise, <c>false</c>.</value>
        [DataMember(Name = "t")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "ia")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "if")]
        public bool IsFiche { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember(Name = "c")]
        public string Culture { get; set; }

        [DataMember(Name = "cl")]
        public bool Closed { get; set; }

        /// <summary>
        /// Gets or sets the closing department.
        /// </summary>
        /// <value>The closing department.</value>
        [DataMember(Name = "cldp")]
        public string ClosingDepartment { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Worksheettype translations.
        /// </summary>
        /// <value>The Worksheettype translations.</value>
        [DataMember(Name = "ts")]
        public List<WorksheetTypeTranslationDataContract> Translations { get; set; }

        /// <summary>
        /// Errors count
        /// </summary>
        /// <value>errors count.</value>
        [DataMember(Name = "ers")]
        public int Errors { get; set; }

        /// <summary>
        /// Errors count
        /// </summary>
        /// <value>errors count.</value>
        [DataMember(Name = "wrs")]
        public int Warnings { get; set; }
    }
}
