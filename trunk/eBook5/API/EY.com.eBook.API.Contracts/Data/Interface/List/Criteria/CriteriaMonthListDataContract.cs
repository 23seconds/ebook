﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaMonthListDataContract
    {
        [DataMember()]
        public DateTime From { get; set; }
        [DataMember()]
        public DateTime To { get; set; }
       
    }
}
