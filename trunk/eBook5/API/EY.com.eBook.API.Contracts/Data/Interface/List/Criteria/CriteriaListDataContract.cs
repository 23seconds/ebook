﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaListDataContract
    {
        [DataMember(Name="lid")]
        public Guid? ListId { get; set; }

        [DataMember(Name = "lik")]
        public string ListKey { get; set; }

        [DataMember(Name = "c")]
        public string Culture { get; set; }

        [DataMember(Name = "ay")]
        public int? AssessmentYear { get; set; }

        [DataMember(Name = "sd")]
        public DateTime? StartDate { get; set; }

        [DataMember(Name = "ed")]
        public DateTime? EndDate { get; set; }
    }
}
