﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaYearsDataContract
    /// </summary>
    [DataContract]
    public class CriteriaYearsDataContract
    {

        /// <summary>
        /// File ID
        /// </summary>
        /// <value>The file id</value>
        [DataMember]
        public Guid FileId { get; set; }

        /// <summary>
        /// From
        /// </summary>
        /// <value>(Base) offset From </value>
        [DataMember]
        public int From { get; set; }

        /// <summary>
        /// To
        /// </summary>
        /// <value>(Base) offset To</value>
        [DataMember]
        public int To { get; set; }

        /// <summary>
        /// Base (optional: (standard)'Assessment' / 'End')
        /// </summary>
        /// <value>The offset base</value>
        [DataMember]
        public string Base { get; set; }

        [DataMember]
        public int? Min { get; set; }

        [DataMember]
        public int? Max { get; set; }

    }
}
