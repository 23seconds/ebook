﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class CriteriaFodListDataContract
    {
        [DataMember()]
        public string ListId { get; set; }
        [DataMember()]
        public string Culture { get; set; }
        [DataMember()]
        public string Query { get; set; }
    }
}
