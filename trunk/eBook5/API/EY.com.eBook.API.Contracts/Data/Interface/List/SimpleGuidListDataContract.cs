﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class SimpleGuidListDataContract
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        [DataMember(Name = "desc")]
        public string Description { get; set; }

    }
}
