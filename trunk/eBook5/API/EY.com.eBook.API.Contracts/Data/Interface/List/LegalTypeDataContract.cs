﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class LegalTypeDataContract
    {
        [DataMember(Name="id")]
        public string Id { get; set; }

        [DataMember(Name="ld")]
        public string ListDescription { get; set; }

        [DataMember(Name="c")]
        public string Code { get; set; }

        [DataMember(Name="n")]
        public string Name { get; set; }
    }
}
