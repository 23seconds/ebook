﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract(Namespace="")]
    public class StringListItemDataContract
    {

        [DataMember(Name="id",Order=1)]
        public string Id { get; set; }

        [DataMember(Name = "nl", Order = 2)]
        public string NL { get; set; }

        [DataMember(Name = "fr", Order = 3)]
        public string FR { get; set; }

        [DataMember(Name = "en", Order = 4)]
        public string EN { get; set; }

        [DataMember(Name = "value", Order = 5)]
        public decimal? Value { get; set; }
    }
}
