﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ListItemDataContract
    {

        [DataMember(Name="id")]
        public Guid Id { get; set; }

        [DataMember(Name = "nl")]
        public string NL { get; set; }

        [DataMember(Name = "fr")]
        public string FR { get; set; }

        [DataMember(Name = "en")]
        public string EN { get; set; }
    }
}
