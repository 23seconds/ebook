﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class docstoreTeamMemberDataContract
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }
        [DataMember(Name = "pid")]
        public Guid PersonId { get; set; }
        [DataMember(Name = "lid")]
        public Guid LoginEmailId { get; set; }
        [DataMember(Name = "fn")]
        public string firstName { get; set; }
        [DataMember(Name = "ln")]
        public string surname { get; set; }
        [DataMember(Name = "r")]
        public string role { get; set; }
        [DataMember(Name = "mr")]
        public string memberRole { get; set; }
        [DataMember(Name = "e")]
        public string email { get; set; }
        [DataMember(Name = "f")]
        public string fax { get; set; }
        [DataMember(Name = "t")]
        public string phone { get; set; }
        [DataMember(Name = "ph")]
        public byte Photo { get; set; }
        [DataMember(Name = "gp")]
        public string GPN { get; set; }
        [DataMember(Name = "d")]
        public DateTime? Disabled { get; set; }
        [DataMember(Name = "o1")]
        public string Office1 { get; set; }
        [DataMember(Name = "O2")]
        public string Office2 { get; set; }
        [DataMember(Name = "o3")]
        public string Office3 { get; set; }
        [DataMember(Name = "o4")]
        public string Office4 { get; set; }
        [DataMember(Name = "o5")]
        public string Office5 { get; set; }

    }
}
