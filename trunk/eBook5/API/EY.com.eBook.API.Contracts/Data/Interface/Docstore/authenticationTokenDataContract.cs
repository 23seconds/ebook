﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BlockContainerDataContract
    /// </summary>
    [DataContract]
    public class authenticationTokenDataContract
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember(Name="AuthenticationToken")]
        public string AuthenticationToken { get; set; } //GFIS
    }
}
