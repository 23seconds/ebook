﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BlockContainerDataContract
    /// </summary>
    [DataContract]
    public class criteriaDocstoreDocumentDataContract
    {
        [DataMember(Name = "c")]
        public string ClientCode { get; set; }

        [DataMember(Name = "cid")]
        public Guid ClientId { get; set; }

        [DataMember(Name = "n")]
        public string Filename { get; set; }

        [DataMember(Name = "a")]
        public string Author { get; set; }

        [DataMember(Name = "fp")]
        public string Filepath { get; set; }

        [DataMember(Name = "sd")]
        public bool Secureddoc { get; set; }

        [DataMember(Name = "mbs")]
        public string MustBeSigned { get; set; }

        [DataMember(Name = "otv")]
        public string OnTopViewable { get; set; }

        [DataMember(Name = "ubc")]
        public bool UploadedByClient { get; set; }

        [DataMember(Name = "ct")]
        public string ContentType { get; set; }

        [DataMember(Name = "did")]
        public Guid DocumentId { get; set; }

        [DataMember(Name = "tmmbs")]
        public List<docstoreTeamMemberDataContract> TeamMembers { get; set; }

        
        [DataMember(Name = "ugpn")]
        public string UploaderGpn { get; set; }
          
    }
}
