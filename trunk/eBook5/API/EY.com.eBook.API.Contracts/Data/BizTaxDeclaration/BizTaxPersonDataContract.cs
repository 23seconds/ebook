﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class BizTaxPersonDataContract : PersonDataContract
    {
       
       
        [DataMember]
        public string DeclaringDepartment { get; set; }

      
        [DataMember]
        public Guid DeclaringPartnerId { get; set; }


        [DataMember]
        public bool ReadOnly { get; set; }



    }
}
