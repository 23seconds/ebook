﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class BizTaxDeclarationDataContract
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /* 
         * 10= to upload, 15 = uploading
         * 20= to validate, 25 = validating
         * 30= to finalize, 35 = finalizing
         * -99 = errors
         * 99 = success
         */
        [DataMember(Name = "s")]
        public int Status { get; set; } 
       
        [DataMember(Name="dep")]
        public string Department { get; set; }

        [DataMember(Name = "pn")]
        public string PartnerName { get; set; }

        [DataMember(Name = "pid")]
        public Guid PartnerId { get; set; }

        [DataMember(Name = "cn")]
        public string ClientName { get; set; }

        [DataMember(Name = "ce")]
        public string ClientEnterpriseNumber { get; set; }

        [DataMember(Name = "ay")]
        public int AssessmentYear { get; set; }

        [DataMember(Name = "cp")]
        public string CurrentPath { get; set; }

        [DataMember(Name = "px")]
        public bool Proxy { get; set; }

        [DataMember(Name = "t")]
        public string Type { get; set; } // manual or full

        [DataMember(Name = "sdb")]
        public string SendToDeclareBy { get; set; }

        [DataMember(Name = "sdd")]
        public DateTime SendToDeclareDate { get; set; }

        [DataMember(Name = "ip")]
        public string InProgressBy { get; set; }

        [DataMember(Name = "ec")]
        public decimal? EBookTaxCalculation { get; set; }

        [DataMember(Name = "bc")]
        public decimal? BizTaxTaxCalculation { get; set; }

        [DataMember(Name = "ers")]
        public string Errors { get; set; }

    }
}
