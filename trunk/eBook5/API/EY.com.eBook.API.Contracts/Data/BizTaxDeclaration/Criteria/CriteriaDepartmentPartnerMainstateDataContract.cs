﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaDepartmentPartnerMainstateDataContract
    /// </summary>
    [DataContract]
    public class CriteriaDepartmentPartnerMainstateDataContract
    {
        

        [DataMember]
        public string Department{ get; set; }

        [DataMember]
        public Guid Partner { get; set; }

        [DataMember]
        public int MainStatus { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }

}
