﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaIdKeyDataContract
    /// </summary>
    [DataContract]
    public class CriteriaIdKeyDataContract
    {
      
        [DataMember]
        public Guid Id{ get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Errors { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 
    }

}
