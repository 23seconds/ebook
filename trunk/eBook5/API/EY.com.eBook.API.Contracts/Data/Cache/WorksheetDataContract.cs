﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Cache
{
    /// <summary>
    /// WorksheetDataContract
    /// </summary>
    [DataContract]
    public class WorksheetDataContract
    {
        //[DataMember(Name = "man")]
        //public InRuleManualWorksheetDataContract Manual { get; set; }
        ///// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the type id.
        /// </summary>
        /// <value>The type id.</value>
        [DataMember(Name = "tid")]
        public Guid TypeId { get; set; }

        
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
        [DataMember(Name = "d")]
        public string Data { get; set; }

        [DataMember(Name="cl")]
        public bool Closed { get; set; }

    }
}
