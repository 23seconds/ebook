﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ProtoBuf;

namespace EY.com.eBook.API.Contracts.Data.Cache
{
    /// <summary>
    /// AccountDataContract
    /// </summary>
    [DataContract]
    public class AccountDataContract
    {
        /// <summary>
        /// FileID
        /// </summary>
        /// <value>The file ID.</value>
        [DataMember(Name = "fid")]
        [ProtoMember(1)]
        public Guid FileId { get; set; }

        /// <summary>
        /// InternalNr
        /// </summary>
        /// <value>The internal nr.</value>
        [DataMember(Name = "inr")]
        [ProtoMember(2)]
        public string InternalNr { get; set; }

        /// <summary>
        /// VisualNr
        /// </summary>
        /// <value>The visual nr.</value>
        [DataMember(Name = "vnr")]
        [ProtoMember(3)]
        public string VisualNr { get; set; }

        /// <summary>
        /// imported id
        /// </summary>
        /// <value>The imported nr.</value>
        [DataMember(Name = "iid")]
        [ProtoMember(4)]
        public string ImportedId { get; set; }

        /// <summary>
        /// HasTaxAdjustments
        /// </summary>
        /// <value>HasTaxAdjustments.</value>
        [DataMember(Name = "hta")]
        [ProtoMember(5)]
        public bool HasTaxAdjustments { get; set; }

        /// <summary>
        /// ExistedLastYear
        /// </summary>
        /// <value>ExistedLastYear.</value>
        [DataMember(Name = "ely")]
        [ProtoMember(6)]
        public bool ExistedLastYear { get; set; }


        [DataMember(Name = "s")]
        [ProtoMember(7)]
        public decimal Saldo { get; set; }

        [DataMember(Name = "ss")]
        [ProtoMember(8)]
        public decimal StartSaldo { get; set; }

        [DataMember(Name = "ps")]
        [ProtoMember(9)]
        public decimal PreviousSaldo { get; set; }

        [DataMember(Name = "ita")]
        [ProtoMember(10)]
        public bool IsTaxAdjusment { get; set; }

        [DataMember(Name = "dd")]
        [ProtoMember(11)]
        public string DefaultDescription { get; set; }

        [DataMember(Name = "c")]
        [ProtoMember(12)]
        public string Culture { get; set; }

        [DataMember(Name = "lc")]
        [ProtoMember(13)]
        public DateTime LastChanged { get; set; }

        [DataMember(Name = "ads")]
        [ProtoMember(14)]
        public List<AccountDescriptionDataContract> Descriptions { get; set; }

        [DataMember(Name = "ams")]
        [ProtoMember(15)]
        public List<AccountMappingDataContract> Mappings { get; set; }

        [DataMember(Name = "adj")]
        [ProtoMember(16)]
        public List<AccountDataContract> Adjustments { get; set; }


        public StringListItemDataContract GetListItem()
        {
            
            if (Descriptions == null)
            {
                string desc = string.IsNullOrEmpty(DefaultDescription) ? InternalNr.Substring(0,6) : DefaultDescription;
                return new StringListItemDataContract
                {
                    Id = InternalNr
                    ,
                    NL = desc
                    ,
                    FR = desc
                    ,
                    EN = desc

                };
            }
            return new StringListItemDataContract
            {
                Id = InternalNr
                ,
                NL = string.Format("{0} {1}", string.IsNullOrEmpty(VisualNr) ? InternalNr.Substring(0, 6) : VisualNr, Descriptions.Count(d => d.Culture == "nl-BE") > 0 ? Descriptions.First(d => d.Culture == "nl-BE").Description : DefaultDescription)
                ,
                FR = string.Format("{0} {1}", string.IsNullOrEmpty(VisualNr) ? InternalNr.Substring(0, 6) : VisualNr, Descriptions.Count(d => d.Culture == "fr-FR") > 0 ? Descriptions.First(d => d.Culture == "fr-FR").Description : DefaultDescription)
                ,
                EN = string.Format("{0} {1}", string.IsNullOrEmpty(VisualNr) ? InternalNr.Substring(0, 6) : VisualNr, Descriptions.Count(d => d.Culture == "en-US") > 0 ? Descriptions.First(d => d.Culture == "en-US").Description : DefaultDescription)

            };
        }
    }
}
