﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Cache
{
    /// <summary>
    /// AccountDescriptionDataContract
    /// </summary>
    [DataContract]
    public class AccountDescriptionDataContract
    {
        /// <summary>
        /// FileID
        /// </summary>
        /// <value>The file ID.</value>
        [DataMember(Name = "fid",Order=1)]
        public Guid FileId { get; set; }

        /// <summary>
        /// InternalNr
        /// </summary>
        /// <value>The internal nr.</value>
        [DataMember(Name = "inr", Order = 2)]
        public string InternalNr { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember(Name = "c", Order = 3)]
        public string Culture { get; set; }
            
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        [DataMember(Name = "d", Order = 4)]
        public string Description { get; set; }

        [DataMember(Name = "ch", Order = 5)]
        public bool changed { get; set; }
    }
}

