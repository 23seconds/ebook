﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Cache
{
    [DataContract]
    public class AccountSaldiUpdate
    {
        [DataMember(Order=1)]
        public string AccountNr { get; set; }
        [DataMember(Order = 2)]
        public decimal Saldo { get; set; }
        [DataMember(Order = 3)]
        public decimal PreviousSaldo { get; set; }
        [DataMember(Order = 4)]
        public decimal StartSaldo { get; set; }
        
    }
}
