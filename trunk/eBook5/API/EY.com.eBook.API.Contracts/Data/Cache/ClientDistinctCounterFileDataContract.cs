﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data.Cache
{
    [DataContract]
    public class ClientDistinctCounterFileDataContract
    {
        [DataMember(Name = "c")]
        public ClientBaseDataContract Client { get; set; }

        [DataMember(Name = "f")]
        public List<DistinctCounterFileDataContract> Files { get; set; }

    }
}
