﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class FileServiceDataContract
    {
        [DataMember(Name="fid",Order=1)]
        public Guid FileId { get; set; }

        [DataMember(Name = "sid", Order = 2)]
        public Guid ServiceId { get; set; }

        [DataMember(Name = "c", Order = 3)]
        public DateTime? Completed { get; set; }

        [DataMember(Name = "cb", Order = 4)]
        public string CompletedBy { get; set; }

        [DataMember(Name = "o", Order = 5)]
        public int Order { get; set; }

        [DataMember(Name = "s", Order = 6)]
        public short? Status { get; set; }

    }
}
