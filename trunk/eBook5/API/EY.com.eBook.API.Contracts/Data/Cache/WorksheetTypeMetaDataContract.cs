﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class WorksheetTypeMetaDataContract
    {
        [DataMember]
        public Guid WorksheetTypeId { get; set; }

        [DataMember]
        public string RuleAppName { get; set; }

        [DataMember]
        public List<string> IWorksheetClasses { get; set; }


    }
}
