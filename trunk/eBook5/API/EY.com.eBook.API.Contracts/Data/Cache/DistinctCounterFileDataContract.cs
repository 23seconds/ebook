﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data.Cache
{
    [DataContract]
    public class DistinctCounterFileDataContract 
    {
        [DataMember(Name = "cnt")]
        public int Counter { get; set; }

        [DataMember(Name = "sd")]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "ed")]
        public DateTime EndDate { get; set; }

        [DataMember(Name = "cn")]
        public string ClientName { get; set; }

        [DataMember(Name = "cid")]
        public Guid ClientId { get; set; }

        [DataMember(Name = "fid")]
        public Guid FileId { get; set; }
    }
}
