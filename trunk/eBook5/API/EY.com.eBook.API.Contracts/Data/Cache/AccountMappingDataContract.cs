﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Cache
{
    /// <summary>
    /// AccountMappingDataContract
    /// </summary>
    [DataContract]
    public class AccountMappingDataContract
    {
        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember(Name = "fid", Order = 1)]
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the internal nr.
        /// </summary>
        /// <value>The internal nr.</value>
        [DataMember(Name = "inr", Order = 2)]
        public string InternalNr { get; set; }

        /// <summary>
        /// Gets or sets the worksheet type id.
        /// </summary>
        /// <value>The worksheet type id.</value>
        [DataMember(Name = "mk", Order = 3)]
        public string MappingKey { get; set; }

        [DataMember(Name = "mm", Order = 4)]
        public string MappingMeta { get; set; }

        [DataMember(Name = "miid", Order = 5)]
        public Guid? MappingItemId { get;set;}

        [DataMember(Name = "pmiid", Order = 6)]
        public Guid? PreviousMappingItemId { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person", Order = 7)]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Order=8)]
        public ActivePersonDataContract ActivePerson { get; set; } 

    }
}
