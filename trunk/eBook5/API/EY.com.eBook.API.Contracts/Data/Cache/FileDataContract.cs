﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.Contracts.Data.Cache
{
    [DataContract]
    public class FileDataContract
    {
        [DataMember(Name = "id",Order=1)]
        public Guid Id { get; set; }

        //public FileTypeDataContract FileType { get; set; }

        [DataMember(Name = "cid", Order = 2)]
        public Guid ClientId { get; set; }

        [DataMember(Name = "n", Order = 3)]
        public string Name { get; set; }

        [DataMember(Name = "dsp", Order = 4)]
        public string Display { get; set; }

        [DataMember(Name = "c", Order = 5)]
        public string Culture { get; set; }

        [DataMember(Name = "sd", Order = 6)]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "ed", Order = 7)]
        public DateTime EndDate { get; set; }

        [DataMember(Name = "psd", Order = 8)]
        public DateTime? PreviousStartDate { get; set; }

        [DataMember(Name = "ped", Order = 9)]
        public DateTime? PreviousEndDate { get; set; }

        [DataMember(Name = "pfid", Order = 10)]
        public Guid? PreviousFileId { get; set; }

        [DataMember(Name = "crd", Order = 11)]
        public DateTime CreationDate { get; set; }

        [DataMember(Name = "crb", Order = 12)]
        public string CreatedBy { get; set; }

        [DataMember(Name = "nl", Order = 13)]
        public bool NumbersLocked { get; set; }

        [DataMember(Name = "cl", Order = 14)]
        public bool Closed { get; set; }

        [DataMember(Name = "cld", Order = 15)]
        public DateTime? ClosedDate { get; set; }

        [DataMember(Name = "imd", Order = 16)]
        public DateTime? ImportDate { get; set; }

        [DataMember(Name = "clb", Order = 17)]
        public string ClosedBy { get; set; }

        [DataMember(Name = "md", Order = 18)]
        public bool MarkedForDeletion { get; set; }

        [DataMember(Name = "d", Order = 19)]
        public bool Deleted { get; set; }

        [DataMember(Name = "ay", Order = 20)]
        public int AssessmentYear
        {
            get { return EndDate.AddDays(1).Year; }
            set { }
        }

        [DataMember(Name = "dd", Order = 21)]
        public string DataDirectory
        {
            get
            {
                return ClientId.ToPath();
            }
            set { 
                //nothing
            }
        }

        [DataMember(Name = "ers", Order = 22)]
        public int Errors { get; set; }

        [DataMember(Name = "wrs", Order = 23)]
        public int Warnings { get; set; }

        [DataMember(Name = "svc", Order = 24)]
        public List<FileServiceDataContract> Services { get; set; }

    }
}
