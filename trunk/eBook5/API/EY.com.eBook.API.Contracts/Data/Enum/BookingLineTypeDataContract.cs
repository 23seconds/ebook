﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BookingLineTypeDataContract
    /// </summary>
    [DataContract]
    public enum BookingLineTypeDataContract
    {
        /// <summary>
        /// Account
        /// </summary>
        [EnumMember]
        Account = 1,

        /// <summary>
        /// Customer
        /// </summary>
        [EnumMember]
        Customer = 2,

        /// <summary>
        /// Supplier
        /// </summary>
        [EnumMember]
        Supplier = 3
    }
}
