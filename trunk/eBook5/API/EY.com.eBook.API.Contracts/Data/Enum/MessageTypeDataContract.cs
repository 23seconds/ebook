﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// MessageTypeDataContract
    /// </summary>
    [DataContract]
    public enum MessageTypeDataContract
    {
        /// <summary>
        /// Error
        /// </summary>
        [EnumMember]
        Error,

        /// <summary>
        /// Warning
        /// </summary>
        [EnumMember]
        Warning,

        /// <summary>
        /// Information
        /// </summary>
        [EnumMember]
        Information,

        /// <summary>
        /// Note
        /// </summary>
        [EnumMember]
        Note

    }
}
