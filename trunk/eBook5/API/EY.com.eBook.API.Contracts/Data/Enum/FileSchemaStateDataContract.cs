﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// FileSchemaStateDataContract
    /// </summary>
    [DataContract]
    public enum FileSchemaState
    {
        [EnumMember]
        Previous = 1,
        [EnumMember]
        Start = 2
    }
    
}
