﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// BusinessRelationType
    /// </summary>
    [DataContract]
    public enum BusinessRelationTypeDataContract
    {
        /// <summary>
        /// Customer
        /// </summary>
        [EnumMember]
        Customer = 'C',

        /// <summary>
        /// Supplier
        /// </summary>
        [EnumMember]
        Supplier = 'S',

        /// <summary>
        /// Personnel
        /// </summary>
        [EnumMember]
        Personnel = 'P'

    }   
}
