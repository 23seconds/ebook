﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class DeclarationResultDataContract
    {
        [DataMember(Name="s")]
        public string SourceFolder { get; set; }

        [DataMember(Name = "f")]
        public List<BinaryFileDataContract> Files { get; set; }
    }
}
