﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// AccountHistoryDataContract
    /// </summary>
    [DataContract]
    public class AccountHistoryDataContract
    {
        /// <summary>
        /// AccountNr
        /// </summary>
        /// <value>AccountNr</value>
        [DataMember]
        public string AccountNr { get; set; }

        /// <summary>
        /// History
        /// </summary>
        /// <value>History</value>
        [DataMember]
        public List<AccountHistoryLineDataContract> History { get; set; }

    }
}
