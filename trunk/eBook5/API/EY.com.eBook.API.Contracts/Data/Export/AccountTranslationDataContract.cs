﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// AccountTranslationDataContract
    /// </summary>
    [DataContract]
    public class AccountTranslationDataContract
    {
        /// <summary>
        /// Culture
        /// </summary>
        /// <value>Culture</value>
        [DataMember]
        public string Culture { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        /// <value>Description</value>
        [DataMember]
        public string Description { get; set; }
    }
}
