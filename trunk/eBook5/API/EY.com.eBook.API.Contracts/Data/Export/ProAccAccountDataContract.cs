﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// AccountDataContract
    /// </summary>
    [DataContract]
    public class ProAccAccountDataContract
    {
        /// <summary>
        /// AccountNr
        /// </summary>
        /// <value>AccountNr</value>
        [DataMember]
        public string InternalNr { get; set; }

        /// <summary>
        /// VisualNr
        /// </summary>
        /// <value>VisualNr</value>
        [DataMember]
        public string VisualNr { get; set; }

        [DataMember]
        public decimal CurrentSaldo { get; set; }

        [DataMember]
        public decimal PreviousSaldo { get; set; }

        /// <summary>
        /// Translations
        /// </summary>
        /// <value>Translations</value>
        [DataMember]
        public List<AccountTranslationDataContract> Translations { get; set; }
    }
}
