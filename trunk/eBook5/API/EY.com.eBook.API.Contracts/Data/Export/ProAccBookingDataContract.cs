﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ProAccBookingDataContract
    /// </summary>
    [DataContract]
    public class ProAccBookingDataContract
    {
        [DataMember]
        public int BookingNr {get;set;}

        [DataMember]
        public string BookingDescription { get; set; }

        [DataMember]
        public string PeriodId { get; set; }

        [DataMember]
        public DateTime? TransactionDate { get; set; }

        [DataMember]
        public DateTime? Start { get; set; }

        [DataMember]
        public DateTime? End { get; set; }

        [DataMember]
        public List<AccountHistoryLineDataContract> BookingLines { get; set; }
    }
}
