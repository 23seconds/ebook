﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.ProAcc.API.Contracts.Data;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// AccountHistoryLineDataContract
    /// </summary>
    [DataContract]
    public class AccountHistoryLineDataContract
    {
        /// <summary>
        /// AccountNr
        /// </summary>
        /// <value>AccountNr</value>
        [DataMember]
        public string AccountNr { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        /// <value>Date</value>
        [DataMember]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Debet
        /// </summary>
        /// <value>Debet</value>
        [DataMember]
        public decimal Debet { get; set; }

        /// <summary>
        /// Credit
        /// </summary>
        /// <value>Credit</value>
        [DataMember]
        public decimal Credit { get; set; }


        /// <summary>
        /// BusinessRelationProAccId
        /// </summary>
        /// <value>BusinessRelationProAccId</value>
        [DataMember]
        public string BusinessRelationProAccId { get; set; }

        [DataMember]
        public List<AccountTranslationDataContract> Translations { get; set; }

    }
}
