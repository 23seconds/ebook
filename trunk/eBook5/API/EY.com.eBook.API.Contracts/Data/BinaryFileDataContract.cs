﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class BinaryFileDataContract
    {
        [DataMember(Name = "folder")]
        public string Folder { get; set; }

        [DataMember(Name = "file")]
        public string FileName { get; set; }
    }
}
