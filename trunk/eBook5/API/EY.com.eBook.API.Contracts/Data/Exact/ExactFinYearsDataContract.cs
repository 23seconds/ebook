﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ExactFinYearsDataContract 
    {
        [DataMember(Name = "efyid")]
        public int ExactFinYearId { get; set; }

        [DataMember(Name = "efyac")]
        public string ExactFinYearAdminCode { get; set; }

        [DataMember(Name = "efyn")]
        public int ExactFinYearNumber { get; set; }

        [DataMember(Name = "efysd")]
        public DateTime ExactFinYearStartDate { get; set; }

        [DataMember(Name = "efyed")]
        public DateTime ExactFinYearEndDate { get; set; }

        [DataMember(Name = "efyd")]
        public string ExactFinYearDisplay { get; set; }
    }
}
