﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ExactBankBicDataContract 
    {
        [DataMember(Name = "ebac")]
        public string ExactBankAdminCode { get; set; }

        [DataMember(Name = "ebb")]
        public string ExactBankBIC { get; set; }

        [DataMember(Name = "lebi")]
        public List<string> ExactBankIbans { get; set; }
    }
}
