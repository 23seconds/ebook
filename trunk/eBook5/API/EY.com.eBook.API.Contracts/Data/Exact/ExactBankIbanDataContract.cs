﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ExactBankIbanDataContract 
    {
        [DataMember(Name = "ebi")]
        public int ExactBankIBAN { get; set; }
    }
}
