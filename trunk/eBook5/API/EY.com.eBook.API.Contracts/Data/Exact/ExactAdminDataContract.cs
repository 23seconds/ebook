﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ClientDataContract, extends ClientBaseDataContract
    /// </summary>
    [DataContract]
    public class ExactAdminDataContract 
    {
        [DataMember(Name = "eacode")]
        public string ExactAdminCode { get; set; }

        [DataMember(Name = "eanr")]
        public string ExactAdminNr { get; set; }

        [DataMember(Name = "eaname")]
        public string ExactAdminName { get; set; }

        [DataMember(Name = "eaal1")]
        public string ExactAdminAddresLine1 { get; set; }

        [DataMember(Name = "eaal2")]
        public string ExactAdminAddresLine2 { get; set; }

        [DataMember(Name = "eaal3")]
        public string ExactAdminAddresLine3 { get; set; }

        [DataMember(Name = "eazc")]
        public string ExactAdminZipCode { get; set; }

        [DataMember(Name = "eacity")]
        public string ExactAdminCity { get; set; }

        [DataMember(Name = "eas")]
        public string ExactAdminState { get; set; }

        [DataMember(Name = "eacountry")]
        public string ExactAdminCountry { get; set; }

        [DataMember(Name = "eacurrency")]
        public string ExactAdminCurrency { get; set; }

        [DataMember(Name = "eavat")]
        public string ExactAdminVat { get; set; }

        [DataMember(Name = "eactid")]
        public int? ExactAdminClusterTermId { get; set; }

        [DataMember(Name = "eacc")]
        public string ExactAdminClusterCode { get; set; }
        
        [DataMember(Name = "eamp")]
        public string ExactAdminMainPostbox { get; set; }

        [DataMember(Name = "ead")]
        public string ExactAdminDisplay { get; set; }

        [DataMember(Name = "eahsp")]
        public Boolean ExactAdminHasFinancialPeriods { get; set; }
        
    }
}
