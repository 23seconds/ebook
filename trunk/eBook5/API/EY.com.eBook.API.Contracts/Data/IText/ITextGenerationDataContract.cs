﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.API.Contracts.Data.IText
{
    [DataContract]
    public class FontConfigDataContract
    {
        [DataMember]
        public string Path { get; set; } //Config.StandardFont

        [DataMember]
        public string Alias { get; set; }  //"EYInterstate-Light"
    }

    [DataContract]
    public class GenerationSettings
    {
        [DataMember]
        public string GenerationPath { get; set; }

        [DataMember]
        public string ResultPath { get; set; }
    }

    [DataContract]
    public class ITextGenerationDataContract
    {
        [DataMember]
        public Guid GenerationId { get; set; }

        [DataMember]
        public GenerationSettings Settings { get; set; }

        [DataMember]
        public FontConfigDataContract FontLocation { get; set; }

        [DataMember]
        public ClientDataContract Client { get; set; }

        [DataMember]
        public FileDataContract File { get; set; }

        [DataMember]
        public IndexItemBaseDataContract Item { get; set; }
    }

    [DataContract]
    public class LeadSheetGenerationDataContract : ITextGenerationDataContract
    {
        [DataMember]
        public List<LeadSheetLineDataContract> Lines { get; set; }

        [DataMember]
        public List<RepositoryListItemDataContract> LinkedFiles { get; set; }

        [DataMember]
        public XDocument Ranges { get; set; }
    }
}
