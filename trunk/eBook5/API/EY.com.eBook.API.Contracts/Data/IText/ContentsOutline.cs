﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ContentsOutline
    {
        public ContentsOutline()
        {
            Children = new List<ContentsOutline>();
        }

        [DataMember(Order=1)]
        public Guid Id { get; set; }
        
        [DataMember(Order = 2)]
        public string Title { get; set; }
        
        [DataMember(Order = 3)]
        public int Page { get; set; }
        
        [DataMember(Order = 4)]
        public bool IsTitlePage { get; set; }

        [DataMember(Order = 5)]
        public List<ContentsOutline> Children { get; set; }

        [DataMember(Order = 6)]
        public Guid? ParentId { get; set; }

    }
}
