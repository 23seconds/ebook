﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// WorksheetTypeTranslationDataContract
    /// </summary>
    [DataContract]
    public class WorksheetTypeTranslationDataContract
    {
        /// <summary>
        /// Gets or sets the WorksheetTypeId.
        /// </summary>
        /// <value>The WorksheetTypeId.</value>
        [DataMember(Order=1)]
        public Guid WorksheetTypeId { get; set; }

        /// <summary>
        /// Gets or sets the EntityOrCollection.
        /// </summary>
        /// <value>The EntityOrCollection.</value>
        [DataMember(Order = 2)]
        public string EntityOrCollection { get; set; }

        /// <summary>
        /// Gets or sets the FieldName.
        /// </summary>
        /// <value>The FieldName.</value>
        [DataMember(Order = 3)]
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the Culture.
        /// </summary>
        /// <value>The Culture.</value>
        [DataMember(Order = 4)]
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the OutputType.
        /// </summary>
        /// <value>The OutputType.</value>
        [DataMember(Order = 5)]
        public string OutputType { get; set; }

        /// <summary>
        /// Gets or sets the Translation.
        /// </summary>
        /// <value>The Translation.</value>
        [DataMember(Order = 6)]
        public string Translation { get; set; }
    }
}
