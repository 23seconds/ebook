﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// WorksheetTypeMessageTranslationDataContract
    /// </summary>
    [DataContract]
    public class WorksheetTypeMessageTranslationDataContract
    {
        /// <summary>
        /// Gets or sets the WorksheetTypeId.
        /// </summary>
        /// <value>The WorksheetTypeId.</value>
        [DataMember(Order=1)]
        public Guid WorksheetTypeId { get; set; }

        [DataMember(Order = 2)]
        public Guid MessageId { get; set; }

        [DataMember(Order = 3)]
        public List<MessageTranslationDataContract> Translations { get; set; }

        /// <summary>
        /// Gets or sets the Collection.
        /// </summary>
        /// <value>The Collection.</value>
        [DataMember(Order = 4)]
        public string Collection { get; set; }

        /// <summary>
        /// Gets or sets the FieldName.
        /// </summary>
        /// <value>The FieldName.</value>
        [DataMember(Order = 5)]
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the Culture.
        /// </summary>
        /// <value>The Culture.</value>
        [DataMember(Order = 6)]
        public string Key { get; set; }

    }
}
