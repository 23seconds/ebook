﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax
{
    
    [DataContract]
    public class ContextElementDataContract
    {
        [DataMember(Order = 1)]
        public string Id { get; set; } // change with automated code?

        [DataMember(Order = 2)]
        public bool AutoRendered { get; set; }

        [DataMember(Order = 3)]
        public ContextEntityDataContract Entity { get; set; }

        [DataMember(Order = 4)]
        public ContextPeriodDataContract Period { get; set; }

        [DataMember(Order = 5)]
        public List<ContextScenarioDataContract> Scenario {get;set;}

        [DataMember(Order = 6)]
        public string ScenarioDef
        {
            get
            {
                if(Scenario==null || Scenario.Count==0) return string.Empty;
                string[] dims = Scenario.OrderBy(s => s.Dimension).Select(s => s.Dimension).ToArray();
                return string.Join("/", dims);
            }
            set
            {
            }
        }

        [DataMember(Order = 7)]
        public string ScenarioId
        {
            get
            {
                if (Scenario == null || Scenario.Count == 0) return string.Empty;
                
                string[] dims = Scenario.OrderBy(s => s.Dimension).Select(s => s.Dimension + (s.Name!="typedMember" ? "::" + s.Value : "")).ToArray();
                return string.Join("/", dims);
            }
            set
            {
            }
        }

        [DataMember(Order = 8)]
        public string PeriodId
        {
            get
            {
                return Id.Split(new char[] { '_' }).First();
            }
            set { }
        }

        [DataMember(Order = 9)]
        public Guid? DefId { get; set; }

        [DataMember(Order = 10)]
        public string PresContextId { get; set; }

        public string GetMyDefId()
        {
            string[] id = Id.Split(new string[] { "__" }, StringSplitOptions.None);
            return string.Join("__", id.Skip(1).ToArray());

        }
    }
}
