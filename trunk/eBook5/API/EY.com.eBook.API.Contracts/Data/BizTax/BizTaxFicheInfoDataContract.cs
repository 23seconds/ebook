﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.BizTax
{
    [DataContract]
    public class BizTaxFicheInfoDataContract
    {
        [DataMember]
        public string FicheId { get; set; }

        [DataMember]
        public List<EY.com.eBook.BizTax.XbrlElementDataContract> Annexes { get; set; }
    }

    
}
