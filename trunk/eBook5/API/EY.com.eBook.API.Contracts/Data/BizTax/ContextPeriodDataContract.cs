﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax
{
    [DataContract]
    public class ContextPeriodDataContract
    {
        [DataMember(Order = 1)]
        public string Instant { get; set; }

        [DataMember(Order = 2)]
        public string StartDate { get; set; }
        [DataMember(Order = 3)]
        public string EndDate { get; set; }
    }
}
