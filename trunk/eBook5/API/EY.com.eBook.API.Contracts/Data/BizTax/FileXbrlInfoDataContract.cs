﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax.Contracts
{
    [DataContract]
    public class FileXbrlInfoDataContract
    {
        
        [DataMember(Order = 1)]
        public Guid FileId { get; set; }

        [DataMember(Order = 2)]
        public string Type { get; set; } // used in ALL contexts

        [DataMember(Order = 3)]
        public string Calculation { get; set; }

        [DataMember(Order = 4)]
        public bool Validated { get; set; }

        [DataMember(Order = 5)]
        public int Status { get; set; }

        [DataMember(Order = 6)]
        public bool Locked { get; set; }

        [DataMember(Order = 7, Name = "department")]
        public string DeclaringDepartment { get; set; }

        [DataMember(Order = 8, Name = "partnerId")]
        public Guid? DeclaringPartnerId { get; set; }

        [DataMember(Order = 9, Name = "partnerName")]
        public string DeclaringPartnerName { get; set; }

        [DataMember(Order = 10, Name = "person")]
        public string EncodedActivePerson { get; set; }
        
        
    }
}
