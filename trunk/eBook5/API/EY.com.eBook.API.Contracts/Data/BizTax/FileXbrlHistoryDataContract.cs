﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax.Contracts
{
    
    [DataContract]
    public class FileXbrlHistoryDataContract
    {
        [DataMember(Order = 1)]
        public Guid FileId { get; set; }

        [DataMember(Order = 2)]
        public string Type { get; set; }

        [DataMember(Order = 3)]
        public DateTime TimeStamp { get; set; }

        [DataMember(Order = 4)]
        public Guid PersonId { get; set; }

        [DataMember(Order = 5)]
        public string PersonFullName { get; set; }

        [DataMember(Order = 6)]
        public int ActionId { get; set; }

        [DataMember(Order = 7)]
        public string ActionText { get; set; }
    }
}
