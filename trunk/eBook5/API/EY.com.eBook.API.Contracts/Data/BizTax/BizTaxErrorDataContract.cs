﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTax
{

    public class BizTaxErrorDataContract
    {
        public Guid FileId { get; set; }

        public string Id { get; set; }

        public List<BizTaxErrorMessageDataContract> Messages { get; set; }

        public List<string> Fields { get; set; }


    }

    public class BizTaxErrorMessageDataContract
    {
        public string Culture { get; set; }

        public string Message { get; set; }

        public void test()
        {
           
        }
    }
}
