﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;
using System.Text.RegularExpressions;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.BizTax
{
    
   
    [KnownType(typeof(ContextScenarioDataContract))]
    [KnownType(typeof(UnitElementDataContract))]
    [ProtoBuf.ProtoInclude(101, typeof(ContextScenarioDataContract))]
    [ProtoBuf.ProtoInclude(102, typeof(UnitElementDataContract))]
    [DataContract]
    public class XbrlElementDataContract
    {
        [DataMember(Order = 1)]
        public bool AutoRendered { get; set; }

        [DataMember(Order = 2)]
        public virtual string Prefix { get; set; } // should be namespace only
        [DataMember(Order = 3)]
        public virtual string NameSpace { get; set; }
        [DataMember(Order = 4)]
        public virtual string Name { get; set; }
        [DataMember(Order = 5)]
        public virtual string ContextRef {
            get
            {
                if (string.IsNullOrEmpty(Context))
                {
                    return string.Format("{0}", Period);
                }
                return string.Format("{0}__{1}", Period, Context);
            }
            set
            {
            }
        }

        [DataMember(Order = 6)]
        public virtual string Decimals { get; set; }
        [DataMember(Order = 7)]
        public virtual string UnitRef { get; set; }

        [DataMember(Order = 8)]
        public virtual string Value { get; set; }

        [DataMember(Order = 9)]
        public List<XbrlElementDataContract> Children { get; set; }

        [DataMember(Order = 10)]
        public virtual string Id
        {
            get
            {
                if (string.IsNullOrEmpty(ContextRef))
                {
                    return string.Format("{0}_{1}", Prefix, Name);
                }
                return string.Format("{0}_{1}_{2}", Prefix, Name, ContextRef);
            }
            set
            {
            }
        }

        [DataMember(Order = 11)]
        public virtual string Context { get; set; }

        [DataMember(Order = 12)]
        public virtual string Period { get; set; }

        [DataMember(Order = 13)]
        public virtual List<IndexItemBaseDataContract> BinaryValue { get; set; }

        [DataMember(Order = 10)]
        public virtual string FullName
        {
            get
            {
                return string.Format("{0}:{1}", Prefix, Name);
            }
            set
            {
            }
        }

        [DataMember(Order = 11)]
        public bool Calculated{ get; set; }

        [DataMember(Order = 12)]
        public bool Locked { get; set; }


        /* 
         * ADDED CONVENIENCE METHODS 
         * ==========================
         * A Datacontract shouldn't have methods embedded, though these
         * methods only apply to this contract, and greatly simplify calculation & validations
         * 
         */

        public DateTime? GetDate()
        {
            if (string.IsNullOrEmpty(Value)) return null;

            DateTime result = DateTime.MinValue;
            DateTime.TryParse(Value,null,DateTimeStyles.RoundtripKind,out result);
            if (result == DateTime.MinValue) return null;
            return result;
        }

        public void SetDate(DateTime dte)
        {
            // Iso 8601. We are not interested in time specific, so excluding all time and timezone info.
            Value = dte.ToString("yyyy-MM-dd");
        }

        public decimal GetNumberOrDefault(decimal defaultValue)
        {
            if (string.IsNullOrEmpty(Value)) return defaultValue;
            decimal? nr = GetNumber();
            if (!nr.HasValue) return defaultValue;
            return nr.Value;

        }

        public decimal GetDateNumberOrDefault(decimal defaultValue)
        {
            if (string.IsNullOrEmpty(Value)) return defaultValue;
            decimal? nr = GetDateNumber();
            if (!nr.HasValue) return defaultValue;
            return nr.Value;
        }

        public decimal? GetDateNumber()
        {
            if (string.IsNullOrEmpty(Value)) return null;
            decimal res = 0;
            decimal.TryParse(Value.Replace("-",""), NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out res);
            return res;
        }

        public decimal? GetNumber()
        {
            if (string.IsNullOrEmpty(Value)) return null;
            if (!(new Regex(@"[0-9]|\.").IsMatch(Value))) return null;
            decimal res = 0;
            decimal.TryParse(Value, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out res);
            return res;
        }

        public void SetNumber(decimal val)
        {
            Value = val.ToString(CultureInfo.InvariantCulture);
        }

        public int GetIntegerOrDefault(int defaultValue)
        {
            if (string.IsNullOrEmpty(Value)) return defaultValue;
            int? nr = GetInteger();
            if (!nr.HasValue) return defaultValue;
            return nr.Value;
        }

        public int? GetInteger()
        {
            if (string.IsNullOrEmpty(Value)) return null;
            if (!(new Regex(@"[0-9]").IsMatch(Value))) return null;
            int res = 0;
            int.TryParse(Value, NumberStyles.Any, null, out res);
            return res;
        }

        public void SetInteger(int val)
        {
            Value = val.ToString(CultureInfo.InvariantCulture);
        }






        public bool? GetBool()
        {
            if (string.IsNullOrEmpty(Value)) return null;
            return Value.ToLower() == "true";
        }
    }


    
}
