﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.BizTax
{
    [DataContract(Namespace ="BizTax.BaseTaxCalc")]
    [KnownType(typeof(EY.com.eBook.BizTax.AY2014.TaxCalc.TaxCalc))]
    [KnownType(typeof(EY.com.eBook.BizTax.AY2015.TaxCalc.TaxCalc))]
    public class BaseTaxCalc
    {
    }
}
