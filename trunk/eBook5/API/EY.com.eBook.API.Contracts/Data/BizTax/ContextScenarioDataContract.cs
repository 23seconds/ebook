﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax
{

    
    [ProtoBuf.ProtoInclude(110,typeof(ContextScenarioExplicitDataContract))]
    [ProtoBuf.ProtoInclude(111, typeof(ContextScenarioTypeDataContract))]
    [DataContract]
    [KnownType(typeof(ContextScenarioExplicitDataContract))]
    [KnownType(typeof(ContextScenarioTypeDataContract))]
    public class ContextScenarioDataContract : XbrlElementDataContract
    {
        [DataMember(Order = 2)]
        public override string Prefix
        {
            get
            {
                return "xbrldi";
            }
            set
            {
                base.Prefix = "xbrldi";
            }
        }

        [DataMember(Order = 20)]
        public string Dimension { get; set; }

        [DataMember(Order = 30)]
        public string Type { get; set; }

    }

    [DataContract]
    public class ContextScenarioExplicitDataContract : ContextScenarioDataContract
    {
        [DataMember(Order = 4)]
        public override string Name
        {
            get
            {
                return "explicitMember";
            }
            set
            {
                base.Name = "explicitMember";
            }
        }
    }

    [DataContract]
    public class ContextScenarioTypeDataContract : ContextScenarioDataContract
    {
        [DataMember(Order = 4)]
        public override string Name
        {
            get
            {
                return "typedMember";
            }
            set
            {
                base.Name = "typedMember";
            }
        }

       
    }
}
