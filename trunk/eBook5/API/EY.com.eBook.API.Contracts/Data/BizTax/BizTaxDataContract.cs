﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data.BizTax;

namespace EY.com.eBook.BizTax.Contracts
{
    [DataContract]
    public class BizTaxDataContract
    {
        [DataMember(Order = 1)]
        public int AssessmentYear { get; set; }

        [DataMember(Order = 2)]
        public List<XbrlNamespace> NameSpaces { get; set; }

        [DataMember(Order = 3)]
        public ContextEntityDataContract EntityIdentifier { get; set; } // used in ALL contexts

        [DataMember(Order = 4)]
        public List<ContextElementDataContract> Contexts { get; set; }

        [DataMember(Order = 5)]
        public List<UnitElementDataContract> Units { get; set; }

        [DataMember(Order = 6)]
        public List<XbrlElementDataContract> Elements { get; set; }

        [DataMember(Order = 7)]
        public DateTime? LastCalculated { get; set; }

        [DataMember(Order = 8)]
        public DateTime? LastSaved { get; set; }

        [DataMember(Order = 9)]
        public Guid? Partner { get; set; }

        [DataMember(Order = 10)]
        public Guid? ProxyId { get; set; }

        [DataMember(Order = 11)]
        public string ProxyTitle { get; set; }

        [DataMember(Order = 12)]
        public string SchemaRef { get; set; }

        [DataMember(Order = 13)]
        public List<BizTaxErrorDataContract> Errors { get; set; }

        [DataMember(Order = 14)]
        public decimal? TaxCalc { get; set; }

        [DataMember(Order = 15)]
        public List<EY.com.eBook.API.Contracts.Data.BizTax.BizTaxFicheInfoDataContract> Fiches { get; set; }

        [DataMember(Order = 16)]
        public BaseTaxCalc TaxCalcDetail { get; set; }

        /*[DataMember(Order = 9)]
        public Guid? Partner { get; set; }*/
    }
}
