﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax
{
    [DataContract]
    public class UnitElementDataContract : XbrlElementDataContract
    {
        [DataMember(Order = 10)]
        public override string Id { get; set; }

        [DataMember(Order = 8)]
        public override string Value
        {
            get
            {
                return Measure;
            }
            set
            {
                Measure = value;
            }
        }

        [DataMember(Order = 40)]
        public string Measure { get; set; }

        [DataMember(Order = 2)]
        public override string Prefix
        {
            get
            {
                return string.Empty;
            }
            set
            {
                base.Prefix = string.Empty;
            }
        }

        [DataMember(Order = 4)]
        public override string Name
        {
            get
            {
                return "unit";
            }
            set
            {
                base.Name = "unit";
            }
        }
        
    }
}
