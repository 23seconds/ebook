﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax
{
    [DataContract]
    public class ContextEntityDataContract
    {
        [DataMember(Order = 1)]
        public string IdentifierValue { get; set; }
        [DataMember(Order = 2)]
        public string IdentifierScheme { get; set; }
    }
}
