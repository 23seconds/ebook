﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ImportBookingLineDataContract
    /// </summary>
    [DataContract]
    public class ImportBookingLineDataContract
    {
        
        [DataMember]
        public string InternalNr {get;set;}
        
        [DataMember]
        public decimal Saldo { get; set; }

        [DataMember]
        public Guid? BusinessRelationId { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }


    }
}
