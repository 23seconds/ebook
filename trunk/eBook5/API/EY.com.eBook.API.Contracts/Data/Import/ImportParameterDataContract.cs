﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// 
    /// </summary>
    [KnownType(typeof(ImportHistoryParameterDataContract))]
    [DataContract]
    public class ImportParameterDataContract
    {
        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>The client id.</value>
        [DataMember(Name = "cid")]
        public Guid? ClientId { get; set; }

        /// <summary>
        /// Gets or sets the file id.
        /// </summary>
        /// <value>The file id.</value>
        [DataMember(Name = "fid")]
        public Guid? FileId { get; set; }

        /* import item */

        [DataMember(Name="it")]
        public string ImportType { get; set; }

        [DataMember(Name="ias")]
        public List<ImportAttribute> ImportAttributes { get; set; }

        [DataMember(Name="m")]
        public List<ExcelMappingDataContract> Mappings { get; set; }

        /// <summary>
        /// Person info base 64 encoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember(Name = "Person")]
        public string EncodedActivePerson { get; set; }

        /// <summary>
        /// Person info base 64 decoded json
        /// </summary>
        /// <value>Person info.</value>
        [DataMember]
        public ActivePersonDataContract ActivePerson { get; set; } 

    }
}
