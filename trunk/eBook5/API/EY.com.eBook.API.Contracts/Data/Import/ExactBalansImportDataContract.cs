﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ExactBalansImportDataContract
    {
        [DataMember]
        public string InternalNr
        {
            get { return AccountNr.PadRight(12, '0'); }
            set
            { //nothing
            }
        }

        [DataMember]
        public string AccountNr { get; set; }

        [DataMember]
        public string AccountDescription { get; set; }

        [DataMember]
        public decimal Saldo { get; set; }
    }
}
