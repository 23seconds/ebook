﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ImportHistoryParameterDataContract : ImportParameterDataContract
    {
        public List<string> AccountNrs { get; set; }
    }
}
