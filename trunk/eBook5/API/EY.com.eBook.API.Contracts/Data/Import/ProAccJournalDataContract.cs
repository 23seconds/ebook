﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    [DataContract]
    public class ProAccJournalDataContract
    {
        [DataMember(Name="id")]
        public short Id { get; set; }

        [DataMember(Name = "n")]
        public string Name { get; set; }
    }
}
