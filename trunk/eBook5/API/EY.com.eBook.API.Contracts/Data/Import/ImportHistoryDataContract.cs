﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ImportHistoryDataContract
    /// </summary>
    [DataContract]
    public class ImportHistoryDataContract
    {
        [DataMember]
        public string InternalNr { get; set; }

        [DataMember]
        public List<ImportBookingLineDataContract> BookingLines { get; set; }
    }
}
