﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ClientDataContract, extends ClientBaseDataContract
    /// </summary>
    [DataContract]
    public class ClientDataContract : ClientBaseDataContract
    {
        /// <summary>
        /// Gets the phone, item is read only to outside spaces
        /// </summary>
        /// <value>Client phone.</value>
        [DataMember(Name = "ph")]
        public string Phone { get; set; }

        /// <summary>
        /// Gets the fax, item is read only to outside spaces
        /// </summary>
        /// <value>Client fax.</value>
        [DataMember(Name = "fx")]
        public string Fax { get; set; }

        /// <summary>
        /// Gets the email, item is read only to outside spaces
        /// </summary>
        /// <value>Client email.</value>
        [DataMember(Name = "em")]
        public string Email { get; set; }

        /// <summary>
        /// Gets the legal structure, item is read only to outside spaces
        /// </summary>
        /// <value>Client legal structure.</value>
        [DataMember(Name = "ls")]
        public string LegalStructure { get; set; }

        /// <summary>
        /// Gets the rpr, item is read only to outside spaces
        /// </summary>
        /// <value>Client rpr.</value>
        [DataMember(Name = "rpr")]
        public string Rpr { get; set; }

        /// <summary>
        /// Gets wether the client is Vat mandatory , item is read only to outside spaces
        /// </summary>
        /// <value>Client Vat mandatory.</value>
        [DataMember(Name = "iv")]
        public bool IsVat { get; set; }

        /// <summary>
        /// Gets the director's name, item is read only to outside spaces
        /// </summary>
        /// <value>Client director's name.</value>
        [DataMember(Name = "dn")]
        public string DirectorName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value><c>true</c> if this instance is active; otherwise, <c>false</c>.</value>
        [DataMember(Name = "ia")]
        public bool IsActive { get; set; }


        /// <summary>
        /// Gets or sets the sec ult.
        /// </summary>
        /// <value>The sec ult.</value>
        [DataMember(Name = "su")]
        public string SecUlt { get; set; }

    }


    /*
   
    /// <summary>
    /// ClientDataContract, extends ClientBaseDataContract
    /// </summary>
    [DataContract]
    public class ClientDataContract : ClientBaseDataContract
    {        
        /// <summary>
        /// Gets the phone, item is read only to outside spaces
        /// </summary>
        /// <value>Client phone.</value>
        [DataMember(Name = "ph")]
        public string Phone { get; set; }

        /// <summary>
        /// Gets the fax, item is read only to outside spaces
        /// </summary>
        /// <value>Client fax.</value>
        [DataMember(Name = "fx")]
        public string Fax { get; set; }

        /// <summary>
        /// Gets the email, item is read only to outside spaces
        /// </summary>
        /// <value>Client email.</value>
        [DataMember(Name = "em")]
        public string Email { get; set; }
        
        /// <summary>
        /// Gets the legal structure, item is read only to outside spaces
        /// </summary>
        /// <value>Client legal structure.</value>
        [DataMember(Name = "ls")]
        public string LegalStructure { get; set; }

        /// <summary>
        /// Gets the rpr, item is read only to outside spaces
        /// </summary>
        /// <value>Client rpr.</value>
        [DataMember(Name = "rpr")]
        public string Rpr { get; set; }

        /// <summary>
        /// Gets wether the client is Vat mandatory , item is read only to outside spaces
        /// </summary>
        /// <value>Client Vat mandatory.</value>
        [DataMember(Name = "iv")]
        public bool IsVat { get; set; }

        /// <summary>
        /// Gets the director's name, item is read only to outside spaces
        /// </summary>
        /// <value>Client director's name.</value>
        [DataMember(Name = "dn")]
        public string DirectorName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is active.
        /// </summary>
        /// <value><c>true</c> if this instance is active; otherwise, <c>false</c>.</value>
        [DataMember(Name = "ia")]
        public bool IsActive { get; set; }

        

        /// <summary>
        /// Gets or sets the sec ult.
        /// </summary>
        /// <value>The sec ult.</value>
        [DataMember(Name = "su")]
        public string SecUlt { get; set; }


        /// <summary>
        /// Gets the metadata, item is read only to outside spaces
        /// </summary>
        /// <value>Client metadata.</value>
        [DataMember(Name = "m")]
        public List<ClientMetaDataContract> Meta { get; set; }

        /// <summary>
        /// Gets the files, item is read only to outside spaces
        /// </summary>
        /// <value>Client files.</value>
        [DataMember(Name = "f")]
        public List<FileDataContract> Files { get; set; }



        /// <summary>
        /// Gets the suppliers, item is read only to outside spaces
        /// </summary>
        /// <value>Client suppliers.</value>
        [DataMember(Name = "s")]
        public List<SupplierDataContract> Suppliers { get; set; }

        /// <summary>
        /// Gets the customers, item is read only to outside spaces
        /// </summary>
        /// <value>Client customers.</value>
        [DataMember(Name = "c")]
        public List<CustomerDataContract> Customers { get; set; }
    }

*/    
}
