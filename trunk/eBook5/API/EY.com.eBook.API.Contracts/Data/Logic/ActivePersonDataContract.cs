﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{    
    /// <summary>
    /// PersonDataContract, only list related data (grid view)
    /// DB:viewPersons
    /// </summary>
    [DataContract]
    public class ActivePersonDataContract
    {
        /// <summary>
        /// Gets the person id, item is read only to outside spaces
        /// </summary>
        /// <value>person id.</value>
        [DataMember(Name = "id",Order=1)]
        public Guid Id { get;  set; }

        /// <summary>
        /// Gets the active role (person role)
        /// </summary>
        /// <value>active person role.</value>
        [DataMember(Name = "ar",Order=2)]
        public string ActiveRole { get;  set; }

        /// <summary>
        /// Gets the active client-person role
        /// </summary>
        /// <value>active client-person role.</value>
        [DataMember(Name = "acr",Order=3)]
        public string[] ActiveClientRoles { get; set; }

        [DataMember(Name = "nm", Order = 4)]
        public string Name { get; set; }

    }

  }
