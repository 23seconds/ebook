﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Logic
{
    /// <summary>
    /// ResponseDataContract
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    public class ResponseDataContract<T> where T:class
    {
        /// <summary>
        /// Gets or sets the response data
        /// </summary>
        /// <value>Response data.</value>
        [DataMember]
        public T Data { get; set; }

        /// <summary>
        /// Gets or sets succesful execution
        /// </summary>
        /// <value>succesfull.</value>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the error object to respond with
        /// </summary>
        /// <value>In case of failure, error object (ServiceErrorDataContract). Null otherwise</value>
        [DataMember]
        public ServiceErrorDataContract ServiceError { get; set; }

        /// <summary>
        /// Gets or sets the total count of records (used for max in paging)
        /// </summary>
        /// <value>Total records</value>
        [DataMember]
        public int Total { get; set; }
    }
}
