﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

// Logic namespace holds data contracts soly for client-service transactions
namespace EY.com.eBook.API.Contracts.Data.Logic
{
    /// <summary>
    /// FileCreationDataContract
    /// </summary>
    [DataContract]
    public class FileCreationDataContract
    {
        
        /// <summary>
        /// Gets or sets the type of the file.
        /// </summary>
        /// <value>The type of the file.</value>
       // [DataMember]
       // public FileTypeDataContract FileType { get; set; }

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>The client id.</value>
        [DataMember]
        public int ClientId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        /// <value>The culture.</value>
        [DataMember]
        public string Culture { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        [DataMember]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        [DataMember]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the prev start date.
        /// </summary>
        /// <value>The prev start date.</value>
        [DataMember]
        public DateTime? PrevStartDate { get; set; }

        /// <summary>
        /// Gets or sets the prev end date.
        /// </summary>
        /// <value>The prev end date.</value>
        [DataMember]
        public DateTime? PrevEndDate { get; set; }

        /// <summary>
        /// Gets or sets the previous file id.
        /// </summary>
        /// <value>The previous file id.</value>
        [DataMember]
        public Guid? PreviousFileId { get; set; }


        /// <summary>
        /// Gets or sets the import plugin
        /// </summary>
        /// <value>The import plugin.</value>
        [DataMember]
        public string ImportPlugin { get; set; }
    }

}
