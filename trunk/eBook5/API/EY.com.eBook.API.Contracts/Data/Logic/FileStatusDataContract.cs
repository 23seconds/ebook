﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{    
    /// <summary>
    ///FileStatusDataContract
    /// </summary>
    [DataContract]
    public class FileStatusDataContract
    {
        [DataMember(Name="shared")]
        public bool ClientShared { get; set; }

        [DataMember(Name="numbersLocked")]
        public bool NumbersClosed { get; set; }

        [DataMember(Name="worksheetsLocked")]
        public bool WorksheetsClosed { get; set; }

        [DataMember(Name="reportsLocked")]
        public bool ReportsClosed { get; set; }

        [DataMember(Name = "fileLocked")]
        public bool FileClosed { get; set; }

        [DataMember(Name = "biztaxLocked")]
        public bool BizTaxLocked { get; set; }

        [DataMember(Name="worksheets")]
        public List<WorksheetTypeDataContract> Worksheets { get; set; }

        [DataMember(Name = "reports")]
        public List<FileReportsListDataContract> Reports { get; set; }

    }

  }
