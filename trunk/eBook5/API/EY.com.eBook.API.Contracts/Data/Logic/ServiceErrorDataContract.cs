﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data.Logic
{
    /// <summary>
    /// ServiceErrorDataContract
    /// </summary>
    [DataContract]
    public class ServiceErrorDataContract
    {
        /// <summary>
        /// Gets or sets the source of the error
        /// </summary>
        /// <value>source.</value>
        [DataMember]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the message of the error
        /// </summary>
        /// <value>message.</value>
        [DataMember]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the technical detail of the error
        /// </summary>
        /// <value>technical details.</value>
        [DataMember]
        public string TechnicalDetails { get; set; }
    }
}
