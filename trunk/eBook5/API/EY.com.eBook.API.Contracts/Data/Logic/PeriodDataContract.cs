﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts
{
    [DataContract]
    public class PeriodDataContract
    {
        [DataMember]
        public string Period { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime? End { get; set; }

        [DataMember]
        public DateTime? Start { get; set; }
    }
}
