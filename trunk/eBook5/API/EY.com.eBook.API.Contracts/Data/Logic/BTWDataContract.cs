﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.API.Contracts.Data
{    
    /// <summary>
    /// BTWDataContract
    /// </summary>
    [DataContract]
    public class BTWDataContract
    {
        
        [DataMember]
        public Guid ClientId { get;  set; }

        [DataMember]
        public string VatNumber { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string GfisCode { get; set; }

        [DataMember]
        public XElement Data { get; set; }

    }

  }
