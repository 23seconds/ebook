﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.WCF;

namespace EY.com.eBook.API.Contracts.Proxies
{
    /// <summary>
    /// ProAccServiceProxy
    /// </summary>
    public class ProAccServiceProxy : ProxyBase<IProAccService>, IProAccService
    {

        #region IProAccService Members

        /// <summary>
        /// Gets the available booking years
        /// </summary>
        /// <param name="id">client id</param>
        /// <returns>List<int></returns>
        public List<BookyearDataContract> ProAccGetBookingYears(CriteriaProAccBaseDataContract cpbdc)
        {
            return Channel.ProAccGetBookingYears(cpbdc);
        }

        public List<PeriodDataContract> ProAccGetPeriods(CriteriaProAccBaseDataContract cpbdc)
        {
            return Channel.ProAccGetPeriods(cpbdc);
        }

        public List<ProAccJournalDataContract> ProAccGetJournals(CriteriaProAccBaseDataContract cpbdc)
        {
            return Channel.ProAccGetJournals(cpbdc);
        }

        /// <summary>
        /// Gets the start and enddate for a bookyear
        /// </summary>
        /// <param name="bookyear">bookyear</param>
        /// <returns>StartEndDataContract</returns>
        public StartEndDataContract ProAccGetStartAndEndDate(CriteriaProAccBookYearDataContract cpbydc)
        {
            return Channel.ProAccGetStartAndEndDate(cpbydc);
        }

        /// <summary>
        /// Retrieves the customers
        /// </summary>
        /// <returns>List<BusinessRelationDataContract></returns>
        public List<BusinessRelationDataContract> ProAccGetCustomers(CriteriaProAccClientDataContract cpcdc)
        {
            return Channel.ProAccGetCustomers(cpcdc);
        }

        /// <summary>
        /// Retrieves the suppliers
        /// </summary>
        /// <returns>List<BusinessRelationDataContract></returns>
        /// <summary>
        public List<BusinessRelationDataContract> ProAccGetSuppliers(CriteriaProAccClientDataContract cpcdc)
        {
            return Channel.ProAccGetSuppliers(cpcdc);
        }

        /// Gets the history of an account
        /// </summary>
        /// <param name="proAccAccountNr">accountnr as known in proacc</param>
        /// <returns>List<AccountHistoryLineDataContract></returns>
        public List<AccountHistoryLineDataContract> ProAccGetAccountHistory(CriteriaProAccAccountDataContract cpadc)
        {
            return Channel.ProAccGetAccountHistory(cpadc);
        }

        
        /// <summary>
        /// Gets the history of multiple accounts
        /// </summary>
        /// <param name="proAccAccountNrs">list of accountnrs as known in proacc</param>
        /// <returns>List<AccountHistoryDataContract></returns>
        public List<AccountHistoryDataContract> ProAccGetAccountsHistory(CriteriaProAccAccountsDataContract cpasdc)
        {
            return Channel.ProAccGetAccountsHistory(cpasdc);
        }

        /// <summary>
        /// Gets the accountschema in ProAcc
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>List<EY.com.eBook.ProAcc.API.Contracts.Data.AccountDataContract></returns>
        public List<ProAccAccountDataContract> ProAccGetAccounts(CriteriaProAccStartAndEndDatesDataContract cpasedc)
        {
            return Channel.ProAccGetAccounts(cpasedc);
        }

        public ProAccImportDetailsDataContract GetImportDetails(CriteriaProAccStartAndEndDatesDataContract cpbdc)
        {
            return Channel.GetImportDetails(cpbdc);
        }

        #endregion
    }
}
