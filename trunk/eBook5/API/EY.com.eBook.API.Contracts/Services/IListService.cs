﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IListService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FodListDataContract> GetFodList(CriteriaFodListDataContract cfldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<StringListItemDataContract> GetFodGlobalList(CriteriaFodListDataContract cfldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<LegalTypeDataContract> GetLegalTypes(CriteriaCultureDataContract ccdc);

        [OperationContract]
        List<StringListItemDataContract> GetYears(CriteriaYearsDataContract cydc);

        /// <summary>
        /// Gets the months in bookyear.
        /// </summary>
        /// <param name="cfdc">The cfdc.</param>
        /// <returns></returns>
        [OperationContract]
        List<StringListItemDataContract> GetBookyearMonths(CriteriaMonthListDataContract cmldc);

        [OperationContract]
        List<GuidListItemDataContract> GetList(CriteriaListDataContract cldc);
    }
}
