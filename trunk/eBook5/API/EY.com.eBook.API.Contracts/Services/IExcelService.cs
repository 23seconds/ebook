﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.API.Excel.Data;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IExcelService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ExcelInfoDataContract ExcelGetBasicInfo(CriteriaExcelDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<ExcelGridRowDataContract>> ExcelGetSheetData(CriteriaExcelSheetDataContract cesdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<ExcelAccountImportDataContract>> ExcelGetAccountSchema(CriteriaExcelSheetMappedDataContract cesmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<ExcelBalansImportDataContract>> ExcelGetBalans(CriteriaExcelSheetMappedDataContract cesmdc);


    }
}
