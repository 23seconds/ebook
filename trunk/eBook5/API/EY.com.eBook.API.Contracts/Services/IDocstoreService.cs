﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.IO;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IDocstoreService
    {

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        RepositoryItemDataContract GetDocumentInfo(CriteriaIdDataContract cidc);

        /*
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void DocStoreLogin();
         * */

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Stream GetFlexedTree(criteriaFlexedTreeDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Stream GetFlexedTrees();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Stream GetTeamMembersByClient(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string UploadDocument(criteriaDocstoreDocumentDataContract cdddc);
    }
}
