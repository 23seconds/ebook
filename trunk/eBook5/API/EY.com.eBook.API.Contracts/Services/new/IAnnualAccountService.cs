﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IAnnualAccountService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool CreateAnnualAccounts(CriteriaFileAnnualAcountsDataContract cfaadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool UpdateAnnualAccounts(CriteriaFileAnnualAcountsDataContract cfaadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool UpdateRepositoryItem(CriteriaFileAnnualAcountsDataContract cfaadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FormDataContract> GetFormOverview(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        AnnualAccountDataContract GetAnnualAccounts(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<EngagementCodeDataContract> GetEngagementCodes(CriteriaIdDataContract cidc);
    }
}
