﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IBundleService
    {
        //List<IndexItemBaseDataContract> GetLibrary{string node);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BundleListDataContract> GetBundles(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BundleListDataContract> GetGenericBundles(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BundleListDataContract> GetServiceBundles(CriteriaFileServiceDataContract cfsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<LibraryTreeNodeDataContract> GetItems(CriteriaLibraryDataContract cldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BundleTemplateListDataContract> GetBundleTemplates(CriteriaCultureDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BundleTemplateListDataContract> GetGenericBundleTemplates(CriteriaCultureDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BundleTemplateListDataContract> GetServiceBundleTemplates(CriteriaServiceCultureDataContract cscdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BundleDataContract SaveBundle(BundleDataContract bdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BundleDataContract GetBundle(Guid id);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BundleDataContract GetBundleByTemplate(CriteriaFileBundleTemplateDataContract cfbtdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool DeleteBundle(Guid id);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BundleDataContract CreateBundle(CriteriaCreateBundleDataContract ccbdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BundleDataContract CreateAnnualAccountsDeliverable(CriteriaCreateBundlePersonDataContract ccbpdc);
        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GenerateBundle(BundleDataContract bdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GenerateBundleById(Guid id);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        CheckBundleDataContract ValidateBundle(Guid id);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<string> CheckSignedRepos(CriteriaIdDataContract cidc);

    }
}
