﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IDocumentService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<DocumentListDataContract> GetDocuments(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DocumentDataContract GetDocument(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DocumentDataContract SaveDocument(DocumentDataContract ddc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void DeleteDocument(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DocumentTypeDataContract GetDocumentType(CriteriaDocumentDataContract cddc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<DocumentTypeBaseDataContract> GetDocumentTypes(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<DocumentGroupDataContract> GetDocumentGroups(CriteriaCultureDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BlockTypeDataContract GetBlockType(CriteriaIdAndCultureDataContract cicdc);


    }
}
