﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.IO;
using EY.com.eBook.API.Contracts.Data.Meta;

namespace EY.com.eBook.API.Contracts.Services
{
    /// <summary>
    /// Output Service interface
    /// </summary>
    [ServiceContract]
    public interface IOutputService
    {
        /* fred
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GenerateLeadSheets(CriteriaIdAndCultureDataContract cidc);
         */

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GetWorksheetPrint(WorksheetItemDataContract widc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GetDocumentPrint(DocumentItemDataContract didc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GetBiztaxFichePreview(CriteriaBiztaxFichePreviewDataContract cbfpdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GetBiztaxTaxDetail(CriteriaIdAndCultureDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string ExportProAccBookings(CriteriaExportBookingsDataContract cebdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string ExportExactBookings(CriteriaExportBookingsDataContract cebdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GetWorksheetExcelExport(CriteriaExportWorksheetExcelDataContract cewedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string ExportAccountMappings(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string ExportFinalTrial(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GetAnnualAccountsBundle(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        CriteriaIdDataContract ExportClientEngagementLetterReport(CriteriaClientEngagementLetterDataContract cceldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        CriteriaIdDataContract ExportClientEngagementLettersByPerson(CriteriaPersonEngagementLetterDataContract cpeldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        CriteriaIdDataContract ExportCodaPoa(CriteriaCodaPoaDataContract ccpdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        CriteriaIdDataContract ExportFileService(CriteriaClientPersonFileServiceDataContract ccpfsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GenerateQuickprint(CriteriaQuickprintDataContract cqdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        string GetConfirmationOfApprovalCITR(CriteriaIdAndDepartmentDataContract ciaddc);
    }
}
