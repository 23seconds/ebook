﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IBizTaxDeclarationService
    {
        //List<IndexItemBaseDataContract> GetLibrary{string node);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BizTaxPersonDataContract Login(CriteriaWinAccountDataContract cwadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BizTaxDeclarationDataContract> GetDeclarations(CriteriaDepartmentPartnerMainstateDataContract cdpmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BizTaxDeclarationDataContract> GetDeclarationsByClient(CriteriaBaseIdDataContract cbidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BizTaxDeclarationDataContract LoadNext(CriteriaDepartmentPartnerMainstateDataContract cdpmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void RegisterFailure(CriteriaIdKeyDataContract cikdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void RegisterSuccess(CriteriaIdKeyDataContract cikdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void RegisterCalculation(CriteriaIdSimulationDataContract cisdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateStatus(CriteriaIdStatusContract cisdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ChangeStatus(List<CriteriaIdStatusContract> lcisdc);
    }
}
