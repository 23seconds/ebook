﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IFileService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void CloseService(CriteriaFileServicePersonDataContract cfspdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ReOpenService(CriteriaFileServicePersonDataContract cfspdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool DeclareBizTax(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileInfoDataContract> GetFileInfos(CriteriaFilesDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileDataContract> GetFiles(CriteriaFilesDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        FileDataContract GetFile(CriteriaIdAndCultureDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Guid CreateFile(CriteriaCreateFileDataContract ccfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ImportAccountseBook(ImportAccountseBookDataContract iaedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ImportMappingseBook(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DateTime? ImportAccountsProAcc(ImportAccountseBookDataContract iaedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DateTime? ImportAccountsExcel(CriteriaExcelSheetMappedDataContract cesmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DateTime? ImportAccountsTxt(CriteriaExcelSheetMappedDataContract cesmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DateTime? ImportCurrentStateExcel(CriteriaExcelSheetMappedDataContract cesmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DateTime? ImportCurrentStateTxt(CriteriaExcelSheetMappedDataContract cesmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        DateTime? ImportPreviousStateExcel(CriteriaExcelSheetMappedDataContract cesmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SetFileInCache(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ForceFileCacheReload(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> GetMessages(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> GetMessagesWithoutNotes(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> GetNotes(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        FileMetaDataContract GetFileMeta(CriteriaIdAndPersonDataContract cipdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMetaDataContract> GetMetaByClientId(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SaveFileMeta(CriteriaFileMetaDataContract cfmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        FileSettingsInfoDataContract GetFileSettings(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SaveFileSettings(CriteriaSaveFileSettingsDataContract csfsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SetFileMarkDeleted(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SetFileUnMarkDeleted(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool DeleteFile(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ReopenFile(List<CriteriaIdAndBoolDataContract> lcibdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void CloseFile(List<CriteriaIdDataContract> lcidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<ExactAdminDataContract> GetExactAdmins(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<ExactFinYearsDataContract> GetExactFinYears(CriteriaStringDataContract csdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<DistinctCounterFileDataContract> GetDistinctFilesWithCounter(CriteriaStringDataContract csdc);

        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        FileServiceStatusDataContract GetFileServiceStatus(CriteriaFileServiceDataContract cfsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileServiceLogDataContract> GetFileServiceLog(CriteriaFileServiceDataContract cfsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void LogFileServiceActions(UpdateFileServiceDataContract ufsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        FileServiceStatusDataContract UpdateFileService(UpdateFileServiceDataContract ufsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ResetServiceFlow(UpdateFileServiceDataContract ufsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<ClientPersonFileServiceDataContract>> GetFileServiceReport(CriteriaClientPersonFileServiceDataContract ccpfsdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<StatusDataContract> GetServiceStatusesList(CriteriaServiceCultureDataContract cscdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<StatusDataContract> GetUserSetStatusesList(CriteriaFileServicePersonDataContract cfspdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        FileServiceDataContract AddService(CriteriaFileServicePersonDataContract cfspdc);
    }
}
