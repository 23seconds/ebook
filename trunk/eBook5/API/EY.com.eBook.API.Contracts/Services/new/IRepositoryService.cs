﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.API.Contracts.Services
{
    /// <summary>
    /// IRepositoryService
    /// </summary>
    [ServiceContract]
    public interface IRepositoryService
    {

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool UploadFile();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        RepositoryStructuresDataContract GetStructureTree(CriteriaCultureDataContract ccdc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<RepositoryNodeDataContract> GetChildren(CriteriaRepositoryChildrenDataContract crcdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool DeleteFile(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<GuidListItemDataContract> GetStatusList(CriteriaStatusDataContract csdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<YearItemDataContract> GetYearsInt(CriteriaPeriodYearsDataContract cpydc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ReplaceItem(CriteriaReplaceRepositoryItemDataContract crridc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Guid SaveItem(CriteriaSaveRepositoryItemDataContract csridc);
       // void SaveMetaData(RepositoryMetadataDataContract rmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool AddLink(RepositoryLinkDataContract rldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool DeleteLink(RepositoryListItemDataContract rlidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<RepositoryListItemDataContract> GetItemLinks(CriteriaItemLinksDataContract cildc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<MonthDataContract> GetQuarterOrMonthList(CriteriaQuarterMonthDataContract cqmdc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<RepositoryListItemDataContract> GetFileLinks(CriteriaIdAndCultureDataContract cicdc);
            //System.IO.Stream GetFileLinks(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        RepositoryNodeDataContract GetFileNode(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<RepositoryNodeDataContract> QueryFiles(CriteriaQueryFilesDataContract cqfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<RepositoryItemDataContract> QueryRepositoryItems(CriteriaQueryFilesDataContract cqfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Boolean GthCheckEngagementLetters(CriteriaRepositoryChildrenDataContract crcdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Boolean PersonHasGTHrole(CriteriaIdDataContract cidc);
    }
    
}
