﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.Contracts.Services
{
    /// <summary>
    /// Client Service interface
    /// </summary>
    [ServiceContract]
    public interface IClientService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ClientDataContract GetClient(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ClientDataContract GetClientGfis(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ClientBaseDataContract GetClientBase(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ClientBaseDataContract GetClientBaseGfis(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ClientInfoDataContract GetClientInfo(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<TeamMemberDataContract> GetClientTeam(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<TeamMemberDataContract> GetClientTeamDepartment(CriteriaIdAndDepartmentDataContract ciddc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<PersonBaseDataContract> GetClientPartnerByDepartment(CriteriaIdAndDepartmentDataContract ciddc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        RepositoryMetaConfigDataContract GetClientExactBanks(CriteriaIdKeyDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<SimpleStringListDataContract> GetClientDepartments(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<SimpleGuidListDataContract> GetClientDepartmentPartners(CriteriaIdAndDepartmentDataContract ciddc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<ClientEngagementLetterDataContract>> GetClientEngagementLettersByPerson(CriteriaPersonEngagementLetterDataContract cpeldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool AddBTW(BTWDataContract bdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SaveClientOverrule(ClientOverruleDataContract cdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SetPrintPreference(CriteriaIdAndBoolDataContract ciabdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool? GetPrintPreference(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<StatusDataContract> GetCodaPoaStatusList(CriteriaIdBoolPersonDataContract cibpdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BankDataContract> GetBanks();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<CodaPoaDataContract>> GetCodaPoa(CriteriaCodaPoaDataContract ccpdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool UpdateCodaPoa(UpdateCodaPoaDataContract ucpdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<CodaPoaLogDataContract> GetCodaPoaLog(CriteriaCodaPoaLogDataContract ccpldc);


    }
}
