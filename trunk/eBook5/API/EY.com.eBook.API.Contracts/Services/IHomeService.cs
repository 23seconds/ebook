﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.IO;
using EY.com.eBook.API.Contracts.Data.Meta;

namespace EY.com.eBook.API.Contracts.Services
{
    /// <summary>
    /// Home Service interface
    /// </summary>
    [ServiceContract]
    public interface IHomeService
    {
        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<RecentFileDataContract> GetRecentFiles(CriteriaClientDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<ClientBaseDataContract>> GetClients(CriteriaClientDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<PersonDataContract>> GetPersons(CriteriaBaseDataContract cbdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<OfficeDataContract> GetAllOffices();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        PersonDataContract Login(CriteriaWinAccountDataContract cwadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<ChampionDataContract> GetAllChampions();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void SetLastUsedRole(CriteriaPersonRolesDataContract cprdc);
    }
}
