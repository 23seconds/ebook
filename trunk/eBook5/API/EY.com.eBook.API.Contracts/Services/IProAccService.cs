﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts;

namespace EY.com.eBook.ProAcc.API.Contracts.Services
{
    /// <summary>
    /// ProAcc Service interface
    /// </summary>
    [ServiceContract]
    public interface IProAccService
    {
        /// <summary>
        /// Gets the available booking years
        /// </summary>
        /// <param name="id">client id</param>
        /// <returns>List<int></returns>
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookyearDataContract> ProAccGetBookingYears(CriteriaProAccBaseDataContract cpbdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<PeriodDataContract> ProAccGetPeriods(CriteriaProAccBaseDataContract cpbdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<ProAccJournalDataContract> ProAccGetJournals(CriteriaProAccBaseDataContract cpbdc);

        /// <summary>
        /// Gets the start and enddate for a bookyear
        /// </summary>
        /// <param name="bookyear">bookyear</param>
        /// <returns>StartEndDataContract</returns>
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        StartEndDataContract ProAccGetStartAndEndDate(CriteriaProAccBookYearDataContract cpbydc);


        /// <summary>
        /// Retrieves the customers
        /// </summary>
        /// <returns>List<BusinessRelationDataContract></returns>
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BusinessRelationDataContract> ProAccGetCustomers(CriteriaProAccClientDataContract cpcdc);

        /// <summary>
        /// Retrieves the suppliers
        /// </summary>
        /// <returns>List<BusinessRelationDataContract></returns>
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BusinessRelationDataContract> ProAccGetSuppliers(CriteriaProAccClientDataContract cpcdc);

        /// <summary>
        /// Gets the history of an account
        /// </summary>
        /// <param name="proAccAccountNr">accountnr as known in proacc</param>
        /// <returns>List<AccountHistoryLineDataContract></returns>
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountHistoryLineDataContract> ProAccGetAccountHistory(CriteriaProAccAccountDataContract cpadc);

        /// <summary>
        /// Gets the history of multiple accounts
        /// </summary>
        /// <param name="proAccAccountNrs">list of accountnrs as known in proacc</param>
        /// <returns>List<AccountHistoryDataContract></returns>
        [OperationContract]
        List<AccountHistoryDataContract> ProAccGetAccountsHistory(CriteriaProAccAccountsDataContract cpasdc);

        /// <summary>
        /// Gets the accountschema in ProAcc
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>List<EY.com.eBook.ProAcc.API.Contracts.Data.AccountDataContract></returns>
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<ProAccAccountDataContract> ProAccGetAccounts(CriteriaProAccStartAndEndDatesDataContract cpasedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ProAccImportDetailsDataContract GetImportDetails(CriteriaProAccStartAndEndDatesDataContract cpbdc);
        
    }
}
