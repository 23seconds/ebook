﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.Logic;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IAdministrationService
    {

        

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<CoefficientGroupDataContract> GetCoefficientGroups(CriteriaCultureDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<CoefficientKeyDataContract> GetCoefficientKeysInGroup(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<CoefficientDataContract> GetCoefficientsInKeys(CriteriaStringDataContract csdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Guid CreateCoefficient(CriteriaCoefficientDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void DeleteCoefficient(CriteriaBaseIdDataContract cbidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateCoefficient(CriteriaCoefficientDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void InsertNewChampion(CriteriaChampionDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void DeleteChampion(CriteriaChampionDataContract ccdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ReloadMainCache();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Boolean RestartCachingService();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<ClientEngagementLetterDataContract> GetClientEngagementLetterReport(CriteriaClientEngagementLetterDataContract cceldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<WorksheetDataContract> GetWorksheets();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<WorksheetHelpDataContract> GetWorksheetHelp(CriteriaBaseIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void InsertHelp(List<CriteriaWorksheetHelpDataContract> lcwhdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<WorksheetHelpDataContract> GetWorksheetHelpById(CriteriaBaseIdDataContract cidc);
        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateHelp(List<CriteriaWorksheetHelpDataContract> lcwhdc);
        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void DeleteHelp(CriteriaBaseIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateHelpOrder(List<CriteriaIdOrderDataContract> lciodc);
        
    }
}
