﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.Logic;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IGlobalService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<WorksheetTypeDataContract> GetWorksheetTypes(CriteriaWorksheetTypesDataContract cwtdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<WorksheetHelpDataContract> GetWorksheetHelpByID(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<StringListItemDataContract> GetMappingGroupItems(CriteriaMappingDataContract cmdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Guid GetNewId();

    }
}
