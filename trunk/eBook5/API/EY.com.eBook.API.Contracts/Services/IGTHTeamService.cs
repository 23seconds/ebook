﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Logic;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IGTHTeamService
    {

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<GTHClientStatusDataContract>> GetClients(CriteriaGTHSearchClientTeamDataContract cstdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<GTHClientStatusDataContract>> GetAllClients(CriteriaGTHSearchClientTeamDataContract cstdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        GTHClientStatusDataContract SelectClient(CriteriaClientTeamDataContract cctdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        GTHClientStartDataContract GetClientStartList(CriteriaGTHTeamDataContract ctdc);

        GTHClientStatusDataContract StartClient(CriteriaStartGTHClientStatusDataContract csdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ProcessFilesTeam1(CriteriaProcessFilesDataContract cpfdc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ProcessFilesTeam2(CriteriaProcessFilesDataContract cpfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ProcessTeam6(CriteriaProcessFilesDataContract cpfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<PersonBaseDataContract> getPersons();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<GTHPersonClientDataContract> GetPersonClients(CriteriaStringDataContract csdc); 

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void SavePerson(CriteriaPersonDataContract cpdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<PersonRolesDataContract> GetPersonRoles(CriteriaBaseIdDataContract cbidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void SavePersonRoles(List<CriteriaPersonRolesDataContract> lcprdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<GTHRolesDataContract> GetRoles();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ReplacePersons(CriteriaReplacePersonsDataContract crpdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void Team5NotifyTeam(CriteriaIdAndCultureDataContract ciacdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void Team5MissingFiles(CriteriaIdAndCultureDataContract ciacdc);
    }
}
