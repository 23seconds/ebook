﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface ICombinedService : IClientService, IProAccService
    {
    }
}
