﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.Logic;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface IAccountSchemaService
    {

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountDataContract> GetAccountsMappingView(CriteriaAccountMappingViewDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<StringListItemDataContract>> GetAccountsList(CriteriaAccountsDataContract cadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<StringListItemDataContract> GetQueriedAccountsList(CriteriaAccountsDataContract cadc);
        

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountDescriptionDataContract> GetAccountTranslations(CriteriaAccountDataContract cadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract GetAccount(CriteriaAccountDataContract cadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        StringListItemDataContract GetAccountListItem(CriteriaAccountDataContract cadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<StringListItemDataContract>> GetAccountsListByRanges(CriteriaAccountsByRangesDataContract cardc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountDataContract> GetAllAccounts(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<AccountDataContract>> GetAccountsDeep(CriteriaAccountsDataContract cardc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<StringListItemDataContract>> GetAccountsListFilteredByMapping(CriteriaAccountFilteredByMappingDataContract cadc);
        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountMappingDataContract> GetAccountMappings(CriteriaSingleAccountDataContract csadc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SaveNewAccount(AccountDataContract adc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool SaveAccount(AccountDataContract adc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        AccountMappingDataContract SaveAccountMapping(AccountMappingDataContract amdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void DeleteAccountMapping(CriteriaAccountMappingDataContract camdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ImportAccountsProAcc(CriteriaIdDataContract cidc);

        

        

        // IMPORT EXCEL


        #region Bookings
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingSingleDataContract> GetAllBookings(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingSingleDataContract> GetAutomaticBookings(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingSingleDataContract> GetManualBookings(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingSingleDataContract> GetWorksheetBookings(CriteriaWorksheetDataContract cwdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountStateDataContract> GetState(CriteriaShemaStateDataContract cssdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FinalTrialBalanceLineDataContract> GetFinalTrialBalance(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BookingDataContract GetBooking(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingDataContract> GetProAccExportBookings(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingDataContract> GetBookings(CriteriaIdAndCultureDataContract cicdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BookingDataContract SaveBooking(CriteriaSaveBookingDataContract csbdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void DeleteBooking(Guid id);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingSingleDataContract> GetBookingLinesByAccountNr(CriteriaBookingLineDataContract cbldc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingSingleDataContract> GetSchemaBooking(CriteriaIdDataContract cidc);

        #endregion
    }
}
