﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.Contracts.Services
{
    /// <summary>
    /// Business Relation Service interface
    /// </summary>
    [ServiceContract]
    public interface IBusinessRelationService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ImportSuppliersProAcc(CriteriaIdAndCultureDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ImportCustomersProAcc(CriteriaIdAndCultureDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ImportBusinessRelationsProAcc(CriteriaIdAndCultureDataContract cidc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<SupplierDataContract> GetSuppliers(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<CustomerDataContract> GetCustomers(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<SupplierDataContract>> GetSuppliersRange(CriteriaBaseIdDataContract cbidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<CustomerDataContract>> GetCustomersRange(CriteriaBaseIdDataContract cbidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<StringListItemDataContract>> GetCustomersList(CriteriaBaseIdDataContract cbidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<StringListItemDataContract>> GetSuppliersList(CriteriaBaseIdDataContract cbidc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BusinessRelationDataContract GetBusinessRelation(CriteriaIdDataContract cidc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BusinessRelationListDataContract> GetBusinessRelationList(CriteriaIdTypeDataContract citdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        ResponseDataContract<List<BusinessRelationListDataContract>> GetBusinessRelationListRange(CriteriaBaseIdTypeDataContract cbitdc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BusinessRelationDataContract SaveBusinessRelationDataContract(BusinessRelationDataContract brdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void DeleteBusinessRelation(CriteriaIdDataContract cidc);


    }
}
