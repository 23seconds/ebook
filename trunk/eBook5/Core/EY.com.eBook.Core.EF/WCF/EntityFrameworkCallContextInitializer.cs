﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Dispatcher;

namespace EY.com.eBook.Core.EF
{
    /// <summary>
    /// Entity Framework Context initializer class. Will make ensure that 
    /// the object context will be disposed after every call in WCF.
    /// </summary>
    public class EntityFrameworkCallContextInitializer : ICallContextInitializer
    {
        /// <summary>
        /// Implement to participate in the initialization of the operation thread.
        /// </summary>
        /// <param name="instanceContext">The service instance for the operation.</param>
        /// <param name="channel">The client channel.</param>
        /// <param name="message">The incoming message.</param>
        /// <returns>
        /// A correlation object passed back as the parameter of the <see cref="M:System.ServiceModel.Dispatcher.ICallContextInitializer.AfterInvoke(System.Object)"/> method.
        /// </returns>
        public object BeforeInvoke(InstanceContext instanceContext, IClientChannel channel, System.ServiceModel.Channels.Message message)
        {
            return null;
        }

        /// <summary>
        /// Implement to participate in cleaning up the thread that invoked the operation.
        /// </summary>
        /// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.ICallContextInitializer.BeforeInvoke(System.ServiceModel.InstanceContext,System.ServiceModel.IClientChannel,System.ServiceModel.Channels.Message)"/> method.</param>
        public void AfterInvoke(object correlationState)
        {
                /*
                 * This method closes the connection. After Dispose is called, operations that require an open connection, 
                 * such as executing a query or calling theToTraceString method, will cause an exception. 
                 * Operations that do not require an open connection, such as composing a query or attaching objects, 
                 * will not cause an exception.
                 */

            eBookReadManager.SetContextNull();
            eBookWriteManager.SetContextNull();

        }
    }
}
