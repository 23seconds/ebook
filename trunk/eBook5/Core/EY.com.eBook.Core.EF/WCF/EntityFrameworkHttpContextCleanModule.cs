﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Objects;

namespace EY.com.eBook.Core.EF.WCF
{

    public class EntityFrameworkHttpContextCleanModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.EndRequest += (s, e) => { Dispose(); };
        }

        public void Dispose()
        {
            ClearContext(typeof(eBookReadManager).Name);
            ClearContext(typeof(eBookWriteManager).Name);

        }

        private void ClearContext(string name)
        {
            if (HttpContext.Current.Items[name] != null)
            {
                var edmx = (ObjectContext)HttpContext.Current.Items[name];
                try
                {
                    edmx.Connection.Close();
                }
                catch { }
                edmx.Dispose();
                edmx = null;
                HttpContext.Current.Items.Remove(name);
            }
        }
    }
}
