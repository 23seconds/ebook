﻿using System;
using System.Collections.Generic;
using System.Data;


using Microsoft.SqlServer.Server;
using System.Reflection;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Data.Objects;
using System.Linq.Expressions;
using System.Text;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;
using System.Data.EntityClient;

namespace EY.com.eBook.Core.EF
{

    public class SelfReferencesTracking
    {
        public string EntitySetName;
        public EntityObject NewEntityObject;
        public EntityKey OriginalKeys;
    }

    /// <summary>
    /// Class containing custom entity framework extension methods to use throughout our application.
    /// </summary>
    public static class EFExtensionMethods
    {

        public static void ExecuteSql(this ObjectContext c, string sql)
        {
            var entityConnection = (System.Data.EntityClient.EntityConnection)c.Connection;
            DbConnection conn = entityConnection.StoreConnection;

            ConnectionState initialState = conn.State;
            try
            {
                if (initialState != ConnectionState.Open)
                    conn.Open();  // open connection if not already open
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                if (initialState != ConnectionState.Open)
                    conn.Close(); // only close connection if not initially open
            }
        }

        /// <summary>
        /// Bulk copy data using SqlBulkCopy.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityReference">The entity reference.</param>
        public static void BulkCopy<T>(this IEnumerable<T> entities, string tableName, ObjectContext context) where T : EntityObject
        {

            SqlConnection sc = (SqlConnection)((EntityConnection)context.Connection).StoreConnection;
            SqlBulkCopy bulk = new SqlBulkCopy(sc);

            bulk.DestinationTableName = tableName;
            try
            {
                bulk.WriteToServer(entities.AsDataReader());
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            
        }
        /*
        public static void BulkCopy<T>(this List<T> queryable, IEnumerable<T> entities, string tableName, ObjectContext context) where T : EntityObject
        {
            SqlConnection sc = (SqlConnection)((EntityConnection)context.Connection).StoreConnection;
            SqlBulkCopy bulk = new SqlBulkCopy(sc);
            bulk.DestinationTableName = tableName;

            bulk.WriteToServer(entities.AsDataReader());
        }

        public static void BulkCopy<T>(this IQueryable<T> queryable, IEnumerable<T> entities, string tableName, ObjectContext context) where T : EntityObject
        {
            SqlConnection sc = (SqlConnection)((EntityConnection)context.Connection).StoreConnection;
            SqlBulkCopy bulk = new SqlBulkCopy(sc);
            bulk.DestinationTableName = tableName;

            bulk.WriteToServer(entities.AsDataReader());
        }
        */
        public static void BulkCopyManual<T, Y>(this ObjectQuery<T> queryable, BulkCopyInfo<Y> bcInfo, ObjectContext context) where T : EntityObject
        {
            SqlConnection sc = (SqlConnection)((EntityConnection)context.Connection).StoreConnection;
            SqlBulkCopy bulk = new SqlBulkCopy(sc);

            bulk.DestinationTableName = bcInfo.TableName;

            bulk.WriteToServer(bcInfo.GetReader());
        }


        
        /// <summary>
        /// Loads an entity reference. Also performs a check if it hasn't been loaded before.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityReference">The entity reference.</param>
        public static void LoadIfNotLoaded<T>(this EntityReference<T> entityReference) where T : class, IEntityWithRelationships
        {
            if (!entityReference.IsLoaded)
                entityReference.Load();
        }

        /// <summary>
        /// Loads an entity collection. Also performs a check if it hasn't been loaded before.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entityReference">The entity reference.</param>
        public static void LoadIfNotLoaded<T>(this EntityCollection<T> entityReference) where T : class, IEntityWithRelationships
        {
            if (!entityReference.IsLoaded)
                entityReference.Load();
        }

        /// <summary>
        /// Shows the translated sql for a given query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public static string ToTraceString(this IQueryable query)
        {
            MethodInfo toTraceStringMethod = query.GetType().GetMethod("ToTraceString");

            if (toTraceStringMethod != null)
                return toTraceStringMethod.Invoke(query, null).ToString();

            return "";
        }

      

        /// <summary>   
        /// Specifies the related objects to include in the query results using   
        /// a lambda expression mentioning the path members.   
        /// </summary>   
        /// <returns>A new System.Data.Objects.ObjectQuery<T> with the defined query path.</returns>   
        public static ObjectQuery<T> Include<T>(this ObjectQuery<T> query, Expression<Func<T, object>> path)
        {
            // Retrieve member path:   
            var members = new List<PropertyInfo>();
            EntityFrameworkHelper.CollectRelationalMembers(path, members);

            // Build string path:   
            var sb = new StringBuilder();
            string separator = "";
            foreach (PropertyInfo member in members)
            {
                sb.Append(separator);
                sb.Append(member.Name);
                separator = ".";
            }

            // Apply Include:   
            return query.Include(sb.ToString());
        }


        /// <summary>
        /// The where in expression extension method.
        /// </summary>
        /// <typeparam name="TElement">The type of the element.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="propertySelector">The property selector.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        private static Expression<Func<TElement, bool>> GetWhereInExpression<TElement, TValue>(Expression<Func<TElement, TValue>> propertySelector, IEnumerable<TValue> values)
        {
            if (null == propertySelector) { throw new ArgumentNullException("propertySelector"); }
            if (null == values) { throw new ArgumentNullException("values"); }
            ParameterExpression p = propertySelector.Parameters.Single();
            if (!values.Any())
                return e => false;

            var equals = values.Select(value => (Expression)Expression.Equal(propertySelector.Body, Expression.Constant(value, typeof(TValue))));
            var body = equals.Aggregate(Expression.Or);

            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }

        /// <summary> 
        /// Return the element that the specified property's value is contained in the specifiec values 
        /// </summary> 
        /// <typeparam name="TElement">The type of the element.</typeparam> 
        /// <typeparam name="TValue">The type of the values.</typeparam> 
        /// <param name="source">The source.</param> 
        /// <param name="propertySelector">The property to be tested.</param> 
        /// <param name="values">The accepted values of the property.</param> 
        /// <returns>The accepted elements.</returns> 
        public static IQueryable<TElement> WhereIn<TElement, TValue>(this IQueryable<TElement> source, Expression<Func<TElement, TValue>> propertySelector, params TValue[] values)
        {
            return source.Where(GetWhereInExpression(propertySelector, values));
        }

        /// <summary> 
        /// Return the element that the specified property's value is contained in the specifiec values 
        /// </summary> 
        /// <typeparam name="TElement">The type of the element.</typeparam> 
        /// <typeparam name="TValue">The type of the values.</typeparam> 
        /// <param name="source">The source.</param> 
        /// <param name="propertySelector">The property to be tested.</param> 
        /// <param name="values">The accepted values of the property.</param> 
        /// <returns>The accepted elements.</returns> 
        public static IQueryable<TElement> WhereIn<TElement, TValue>(this IQueryable<TElement> source, Expression<Func<TElement, TValue>> propertySelector, IEnumerable<TValue> values)
        {
            return source.Where(GetWhereInExpression(propertySelector, values));
        }

        /// <summary>
        /// IsRelationshipForKey
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool IsRelationshipForKey(this ObjectStateEntry entry, EntityKey key)
        {
            if (entry.IsRelationship == false)
            {
                return false;
            }
            return ((EntityKey)entry.UsableValues()[0] == key) || ((EntityKey)entry.UsableValues()[1] == key);
        }

        /// <summary>
        /// Others the end key.
        /// </summary>
        /// <param name="relationshipEntry">The relationship entry.</param>
        /// <param name="thisEndKey">The this end key.</param>
        /// <returns></returns>
        public static EntityKey OtherEndKey(this ObjectStateEntry relationshipEntry, EntityKey thisEndKey)
        {
            if ((EntityKey)relationshipEntry.UsableValues()[0] == thisEndKey)
            {
                return (EntityKey)relationshipEntry.UsableValues()[1];
            }
            else if ((EntityKey)relationshipEntry.UsableValues()[1] == thisEndKey)
            {
                return (EntityKey)relationshipEntry.UsableValues()[0];
            }
            else
            {
                throw new InvalidOperationException("Neither end of the relationship contains the passed in key.");
            }
        }

        /// <summary>
        /// Usables the values.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <returns></returns>
        public static IExtendedDataRecord UsableValues(this ObjectStateEntry entry)
        {
            switch (entry.State)
            {
                case EntityState.Added:
                case EntityState.Detached:
                case EntityState.Unchanged:
                case EntityState.Modified:
                    return (IExtendedDataRecord)entry.CurrentValues;
                case EntityState.Deleted:
                    return (IExtendedDataRecord)entry.OriginalValues;
                default:
                    throw new InvalidOperationException("This entity state should not exist.");
            }

        }

        /// <summary>
        /// EntityCollection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static EntityCollection<T> ToEntityCollection<T>(this IEnumerable<T> source) where T : class, IEntityWithRelationships
        {
            EntityCollection<T> set = new EntityCollection<T>();
            foreach (var item in source)
                set.Add(item);
            return set;
        }
        /*
        private static readonly List<SelfReferencesTracking> _tracking = new List<SelfReferencesTracking>();

        public static EntityObject Clone(this EntityObject entityObject)
        {
            //Get constructor for new object
            var newEntityObject = entityObject.GetType().GetConstructor(
            new Type[0]).Invoke(new object[0]);

            _tracking.Add(new SelfReferencesTracking
            {
                EntitySetName = entityObject.EntityKey.EntitySetName,
                OriginalKeys = entityObject.EntityKey,
                NewEntityObject = (EntityObject)newEntityObject
            });

            //Copy all properties and its values of the given type
            var properties = entityObject.GetType().GetProperties();
            foreach (var property in properties)
            {
                try
                {
                    var propertyValue = property.GetValue(entityObject, null);
                    PropertyInfo myProperty = property;
                    if (entityObject.EntityKey.EntityKeyValues.Where(x => x.Key == myProperty.Name).Count() == 0)
                    {
                        //Ignore all properties of these types
                        if (property.PropertyType != typeof(EntityKey) &&
                        property.PropertyType != typeof(EntityState) &&
                        property.PropertyType != typeof(EntityReference<>))
                        {
                            //Check, if the property is a complex type (collection), in that
                            //case, some special calls are necessary
                            if (property.GetCustomAttributes(typeof(EdmRelationshipNavigationPropertyAttribute), false).Count() == 1)
                            {
                                //Check for self referencing entities
                                if (propertyValue.GetType() == entityObject.GetType())
                                {
                                    //Get the self referenced entity object
                                    var selfRefrencedEntityObject =
                                    (EntityObject)property.GetValue(entityObject, null);

                                    //This variable is used to store the new parent entity objects
                                    EntityObject newParentEntityObject = null;

                                    //This loops might be replaced by LINQ queries... I didn't try that
                                    foreach (var tracking in _tracking.Where(x => x.EntitySetName == selfRefrencedEntityObject.EntityKey.EntitySetName))
                                    {
                                        //Check, if the key is in the tracking list
                                        foreach (var newKeyValues in selfRefrencedEntityObject.EntityKey.EntityKeyValues)
                                        {
                                            //Iterate trough the keys and values
                                            foreach (var orgKeyValues in tracking.OriginalKeys.EntityKeyValues)
                                            {
                                                //The key is stored in the tracking list, which means, this is
                                                //the foreign key used by the self referencing property
                                                if (newParentEntityObject == null)
                                                {
                                                    if (orgKeyValues.Key == newKeyValues.Key &&
                                                    orgKeyValues.Value == newKeyValues.Value)
                                                    {
                                                        //Store the parent entity object
                                                        newParentEntityObject = tracking.NewEntityObject;
                                                    }
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                            }
                                        }

                                        //Set the value to the new parent entity object
                                        property.SetValue(newEntityObject, newParentEntityObject, null);
                                    }
                                }
                                else
                                {
                                    //Entity collections are always generic
                                    if (propertyValue.GetType().IsGenericType)
                                    {
                                        //Don't include self references collection, e.g. Orders1, Orders2 etc.
                                        //Check for equality of the types (string comparison)
                                        if (!propertyValue.GetType().GetGenericArguments().First().FullName.Equals(entityObject.GetType().FullName))
                                        {
                                            //Get the entities of the given property
                                            var entities = (RelatedEnd)property.GetValue(entityObject, null);

                                            //Load underlying collection, if not yet done...
                                            if (!entities.IsLoaded) entities.Load();

                                            //Create a generic instance of the entities collection object
                                            var t = typeof(EntityCollection<>).MakeGenericType(
                                            new[] { property.PropertyType.GetGenericArguments()[0] });

                                            var newEntityCollection = Activator.CreateInstance(t);

                                            //Iterate trough the entities collection
                                            foreach (var entity in entities)
                                            {
                                                //Add the found entity to the dynamic generic collection
                                                var addToCollection = newEntityCollection.GetType().GetMethod("Add");
                                                addToCollection.Invoke(
                                                newEntityCollection,
                                                    //new object[] {(EntityObject) entity});
                                                new object[] { Clone((EntityObject)entity) });
                                            }

                                            //Set the property value
                                            property.SetValue(newEntityObject, newEntityCollection, null);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //Common task, just copy the simple type property into the new
                                //entity object
                                property.SetValue(newEntityObject, property.GetValue(entityObject, null), null);
                            }
                        }
                    }
                }
                catch (InvalidCastException ie)
                {
                    //Hmm, something happend...
                    Debug.WriteLine(ie.Message);

                    continue;
                }
                catch (Exception ex)
                {
                    //Hmm, something happend...
                    Debug.WriteLine(ex.Message);

                    continue;
                }
            }

            return (EntityObject)newEntityObject;
        }
        */
    }

    /// <summary>
    /// EntityFrameworkHelper
    /// </summary>
    internal static class EntityFrameworkHelper
    {
        /// <summary>
        /// Collects the relational members.
        /// </summary>
        /// <param name="exp">The exp.</param>
        /// <param name="members">The members.</param>
        internal static void CollectRelationalMembers(Expression exp, IList<PropertyInfo> members)
        {
            if (exp.NodeType == ExpressionType.Lambda)
            {
                // At root, explore body:   
                CollectRelationalMembers(((LambdaExpression)exp).Body, members);
            }
            else if (exp.NodeType == ExpressionType.MemberAccess)
            {
                var mexp = (MemberExpression)exp;
                CollectRelationalMembers(mexp.Expression, members);
                members.Add((PropertyInfo)mexp.Member);
            }
            else if (exp.NodeType == ExpressionType.Call)
            {
                var cexp = (MethodCallExpression)exp;

                if (cexp.Method.IsStatic == false)
                    throw new InvalidOperationException("Invalid type of expression.");

                foreach (var arg in cexp.Arguments)
                    CollectRelationalMembers(arg, members);
            }
            else if (exp.NodeType == ExpressionType.Parameter)
            {
                // Reached the toplevel:   
                return;
            }
            else
            {
                throw new InvalidOperationException("Invalid type of expression.");
            }
        }
    }

}
