﻿using System;
using System.Collections.Generic;
using System.Data;


using Microsoft.SqlServer.Server;
using System.Reflection;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Data.Objects;
using System.Linq.Expressions;
using System.Text;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;

namespace EY.com.eBook.Core.EF.Read
{

    /// <summary>
    /// Class containing custom entity framework extension methods to use throughout our application.
    /// </summary>
    public static class eBookReadExtensionMethods
    {

        /// <summary>
        /// Marks all the objects in this collection for deletion. Also loads the objects if they were not loaded before.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        public static void MarkAllObjectsForDeletion<T>(this EntityCollection<T> collection) where T : EntityObject
        {
            collection.LoadIfNotLoaded();

            var arrayCollection = collection.ToArray();
            foreach (var obj in arrayCollection)
            {
                eBookReadManager.Context.DeleteObject(obj);
            }
        }

        /// <summary>
        /// Marks all objects for deletion within this list of entity objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        public static void MarkAllObjectsForDeletion<T>(this List<T> list) where T : EntityObject
        {
            if (list != null)
            {
                var arr = list.ToArray();
                foreach (var obj in arr)
                {
                    eBookReadManager.Context.DeleteObject(obj);
                }
            }
        }

        /// <summary>
        /// Marks all objects for deletion within this list of entity objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        public static void DetachAll<T>(this List<T> list) where T : EntityObject
        {
            if (list != null)
            {
                var arr = list.ToArray();
                foreach (var obj in arr)
                {
                    eBookReadManager.Context.Detach(obj);
                }
            }
        }

        /// <summary>
        /// Marks all objects for deletion within this queryable of entity objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryable">The queryable.</param>
        public static void MarkAllObjectsForDeletion<T>(this IQueryable<T> queryable) where T : EntityObject
        {
            if (queryable != null)
            {
                var arr = queryable.ToArray();
                foreach (var obj in arr)
                {
                    eBookReadManager.Context.DeleteObject(obj);
                }
            }
        }

        /// <summary>
        /// Marks all objects for deletion within this queryable of entity objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="iEnumerable">The IEnumerable.</param>
        public static void MarkAllObjectsForDeletion<T>(this IEnumerable<T> iEnumerable) where T : EntityObject
        {
            if (iEnumerable != null)
            {
                var arr = iEnumerable.ToArray();
                foreach (var obj in arr)
                {
                    eBookReadManager.Context.DeleteObject(obj);
                }
            }
        }

        
    }

   
}
