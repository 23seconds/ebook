﻿public void ImportPersonFromCDB(string email, string windowsAccount)
        {
            
            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"EbookModel.ImportPersonFromCDB";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter emailParameter = new EntityParameter("email", System.Data.DbType.String);
            emailParameter.Value = email;
            command.Parameters.Add(emailParameter);
            EntityParameter WAParameter = new EntityParameter("windowsAccount", System.Data.DbType.String);
            WAParameter.Value = windowsAccount;
            command.Parameters.Add(WAParameter);
            command.ExecuteNonQuery();
        }

        public void UpdatePersonAllClientRoles(int personId)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"EbookModel.UpdatePersonAllClientRoles";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter personParameter = new EntityParameter("personID", System.Data.DbType.Int32);
            personParameter.Value = personId;
            command.Parameters.Add(personParameter);
            command.ExecuteNonQuery();
        }

        public void UpdatePersonSingleClientRoles(int personId, Guid clientId)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"EbookModel.UpdatePersonSingleClientRoles";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter personParameter = new EntityParameter("personID", System.Data.DbType.Int32);
            personParameter.Value = personId;
            command.Parameters.Add(personParameter);
            EntityParameter clientParameter = new EntityParameter("clientID", System.Data.DbType.Guid);
            clientParameter.Value = clientId;
            command.Parameters.Add(clientParameter);
            command.ExecuteNonQuery();
        }