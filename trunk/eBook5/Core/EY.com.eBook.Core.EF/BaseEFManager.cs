﻿using System;
using System.Data.Objects;
using System.Web;

namespace EY.com.eBook.Core.EF
{
    /// <summary>
    /// The EFContextManager will initialize only one context per thread. It 
    /// will dispose the entity model context in WCF when the request has finished.
    /// Within an ASP.NET application it will be managed in the HttpContext per request.
    /// It would be better to save it in the http Session, but in multi server environments this 
    /// causes difficulties to share.
    /// </summary>
    public abstract class BaseEFManager<T> where T : ObjectContext, new()
    {
        [ThreadStatic]
        private static T _ctx;

        public static T Context
        {
            // There is no need for locking in this singleton pattern due to the fact that every thread
            // will have it's own context anyway. 
            get
            {
                if (!IsInWebContext())
                {
                    if (_ctx == null)
                    {
                        _ctx = new T();
                        _ctx.CommandTimeout = 120;
                        _ctx.Connection.Open();
                    }
                    
                    return _ctx;
                }
                else // Else when the model is being used within ASP.NET instead of WCF OR WCF with AspnetCompabilityModus on. 
                {
                    var contextKeyName = typeof (T).Name;

                    if (HttpContext.Current.Items[contextKeyName] != null)
                    {
                        return HttpContext.Current.Items[contextKeyName] as T;
                    }
                    T efHttpContext = new T();
                    efHttpContext.Connection.Open();
                    HttpContext.Current.Items.Add(contextKeyName, efHttpContext);

                    return efHttpContext;
                }
            }
            set
            {
                _ctx = value;
            }
        }

        /// <summary>
        /// Determines whether we are running in a web context.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if the application is running in a web context; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsInWebContext()
        {
            return HttpContext.Current != null;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has an initialized context.
        /// </summary>
        public static bool HasInitializedContext
        {
            get { return _ctx != null; }
        }

        /// <summary>
        /// Sets the context null.
        /// </summary>
        public static void SetContextNull()
        {
            if (HasInitializedContext)
            {
                _ctx.Connection.Close();
                _ctx.Dispose();
                _ctx = null;
            }
            if (IsInWebContext())
            {
                var contextKeyName = typeof(T).Name;
                try
                {
                    var edmx = (ObjectContext)HttpContext.Current.Items[contextKeyName];
                    try
                    {
                        edmx.Connection.Close();
                    }
                    catch { }
                    edmx.Dispose();
                    edmx = null;
                    HttpContext.Current.Items.Remove(contextKeyName);
                }
                catch { }
                
            }
        }
    }
}
