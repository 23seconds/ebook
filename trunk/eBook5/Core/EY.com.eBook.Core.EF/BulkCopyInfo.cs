﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;

namespace EY.com.eBook.Core.EF
{
    public class BulkCopyInfo<T> 
    {
        private IEnumerator<T> enumerator;
        public T Current;
        bool closed = false;

        public string TableName { get; set; }

        public List<T> Data {get;set;}

        public IDataReader GetReader()
        {
            return new GenericListDataReader<T>(Data);
        }

        
    }
}
