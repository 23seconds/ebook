﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.EntityClient;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core.EF.Write;
using System.Xml.Linq;

namespace EY.com.eBook.Core.EF
{
    public class eBookWriteExtendedModel : eBookWrite
    {
        public bool UpdateWorksheet(Guid fileId, Guid typeId, string data)
        {
            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.UpdateWorksheet";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            
            EntityParameter fileParameter = new EntityParameter("FileId", System.Data.DbType.Guid);
            fileParameter.Value = fileId;
            command.Parameters.Add(fileParameter);

            EntityParameter typeParameter = new EntityParameter("TypeId", System.Data.DbType.Guid);
            typeParameter.Value = typeId;
            command.Parameters.Add(typeParameter);

            EntityParameter dataParameter = new EntityParameter("Data", System.Data.DbType.Xml);
            dataParameter.Value = data;
            command.Parameters.Add(dataParameter);
            int res = command.ExecuteNonQuery();
            return res != -1;
        }

        public bool ImportPreviousAccounts(Guid fileId, bool includeSaldi)
        {
            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.ImportPreviousAccounts";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter fileParameter = new EntityParameter("fileId", System.Data.DbType.Guid);
            fileParameter.Value = fileId;
            command.Parameters.Add(fileParameter);
            EntityParameter saldiParameter = new EntityParameter("includeSaldi", System.Data.DbType.Guid);
            saldiParameter.Value = includeSaldi;
            command.Parameters.Add(saldiParameter);
            int res = command.ExecuteNonQuery();
            return res != -1;
        }

        public bool ImportPreviousMappings(Guid fileId)
        {
            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.ImportPreviousMappings";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter fileParameter = new EntityParameter("fileId", System.Data.DbType.Guid);
            fileParameter.Value = fileId;
            command.Parameters.Add(fileParameter);
            int res = command.ExecuteNonQuery();
            return res != -1;
        }

        public void ImportPersonFromCDB(string email, string windowsAccount)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.ImportPersonFromCDB";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter emailParameter = new EntityParameter("email", System.Data.DbType.String);
            emailParameter.Value = email;
            command.Parameters.Add(emailParameter);
            EntityParameter WAParameter = new EntityParameter("windowsAccount", System.Data.DbType.String);
            WAParameter.Value = windowsAccount;
            command.Parameters.Add(WAParameter);
            command.ExecuteNonQuery();

        }

        public void ClearFileAccounts(Guid fileId)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.ClearFileAccounts";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter fileParameter = new EntityParameter("fileid", System.Data.DbType.Guid);
            fileParameter.Value = fileId;
            command.Parameters.Add(fileParameter);
            command.ExecuteNonQuery();

        }

        public DefaultFaultContract GetErrorMessage(DefaultFaultContract fault, string culture, string[] data)
        {
            int code = fault.ErrorCode;

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.GetErrorMessage";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter codeParameter = new EntityParameter("code", System.Data.DbType.Int32);
            codeParameter.Value = code;
            command.Parameters.Add(codeParameter);
            EntityParameter cultureParameter = new EntityParameter("culture", System.Data.DbType.String);
            cultureParameter.Value = culture;
            command.Parameters.Add(cultureParameter);
            object scalReturn = command.ExecuteScalar();
            string msg = scalReturn == null ? null : scalReturn.ToString();
            if (!string.IsNullOrEmpty(msg))
            {
                fault.Message = msg;
                if (data != null)
                {
                    fault.Message = string.Format(fault.Message, data);
                }
            }
            return fault;

        }

        public void UpdatePersonAllClientRoles(Guid personId)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.UpdatePersonAllClientRoles";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter personParameter = new EntityParameter("personID", System.Data.DbType.Guid);
            personParameter.Value = personId;
            command.Parameters.Add(personParameter);
            command.ExecuteNonQuery();
        }

        public void UpdatePersonSingleClientRoles(Guid personId, Guid clientId)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.UpdatePersonSingleClientRoles";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter personParameter = new EntityParameter("personID", System.Data.DbType.Guid);
            personParameter.Value = personId;
            command.Parameters.Add(personParameter);
            EntityParameter clientParameter = new EntityParameter("clientID", System.Data.DbType.Guid);
            clientParameter.Value = clientId;
            command.Parameters.Add(clientParameter);
            command.ExecuteNonQuery();
        }

        public void LogFileAction(Guid personId, Guid fileId, string action, string meta)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.AddFileLog";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter personParameter = new EntityParameter("personId", System.Data.DbType.Guid);
            personParameter.Value = personId;
            command.Parameters.Add(personParameter);
            EntityParameter fileParameter = new EntityParameter("fileId", System.Data.DbType.Guid);
            fileParameter.Value = fileId;
            command.Parameters.Add(fileParameter);
            EntityParameter actionParameter = new EntityParameter("action", System.Data.DbType.String);
            actionParameter.Value = action;
            command.Parameters.Add(actionParameter);
            EntityParameter metaParameter = new EntityParameter("meta", System.Data.DbType.String);
            metaParameter.Value = meta;
            command.Parameters.Add(metaParameter);
            command.ExecuteNonQuery();
        }

        public void UpdateFileAllAccountsSaldi(Guid fileId)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.UpdateFileAllAccountsSaldi";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter fileParameter = new EntityParameter("FileId", System.Data.DbType.Guid);
            fileParameter.Value = fileId;
            command.Parameters.Add(fileParameter);
            
            command.ExecuteNonQuery();
        }

        public void MarkFileAccountLastChanged(Guid fileId, DateTime dte)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.MarkFileAccountLastChanged";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter fileParameter = new EntityParameter("FileId", System.Data.DbType.Guid);
            fileParameter.Value = fileId;
            command.Parameters.Add(fileParameter);
            EntityParameter dteParameter = new EntityParameter("Date", System.Data.DbType.DateTime);
            dteParameter.Value = dte;
            command.Parameters.Add(dteParameter);

            command.ExecuteNonQuery();
        }

        public void UpdateFileAccountListSaldi(Guid fileId,List<string> accountNrs)
        {

            if (this.Connection.State != System.Data.ConnectionState.Open)
                this.Connection.Open();
            System.Data.EntityClient.EntityCommand command = new System.Data.EntityClient.EntityCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = @"eBookWrite.UpdateFileAccountListSaldi";
            command.Connection = (System.Data.EntityClient.EntityConnection)this.Connection;
            EntityParameter fileParameter = new EntityParameter("FileId", System.Data.DbType.Guid);
            fileParameter.Value = fileId;
            command.Parameters.Add(fileParameter);
            XElement xe = XElement.Parse(CoreHelper.SerializeToString(accountNrs));
            xe = xe.StripNamespaces();
            EntityParameter accountListParameter = new EntityParameter("accountList", System.Data.DbType.Guid);

            accountListParameter.Value = xe.ToString();
            command.Parameters.Add(accountListParameter);

            command.ExecuteNonQuery();
        }

        // import previous eBook accounts
        // import previous eBook mappings
        // 

    }
}
