﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Proxies;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.Core.IText
{
    /// <summary>
    /// XSLTWorker
    /// </summary>
    public class XSLTWorker
    {
        public FileDataContract File { get; set; }
        public ClientBaseDataContract Client { get; set; }
        public IndexDataContract Index { get; set; }


        public void TraverseIndex(IndexDataContract idc)
        {
            IndexDataContract resultIndex = new IndexDataContract();
            resultIndex.Items = new List<IndexItemBaseDataContract>();
            foreach (IndexItemBaseDataContract iibdc in idc.Items)
            {
                resultIndex.Items.AddRange((List<IndexItemBaseDataContract>)HandleIndexItem(iibdc));
            }
        }

        private List<IndexItemBaseDataContract> HandleIndexItem(IndexItemBaseDataContract iibdc)
        {
            List<IndexItemBaseDataContract> items = new List<IndexItemBaseDataContract>();

            switch (iibdc.GetType().Name)
            {
                case "DocumentItemDataContract":
                    items.Add(DocumentRender((DocumentItemDataContract)iibdc));
                    break;

                case "FicheItemDataContract":
                    FicheRender((FicheItemDataContract)iibdc);
                    items.Add((FicheItemDataContract)iibdc);
                    break;

                case "StatementsDataContract":
                    StatementsRender((StatementsDataContract)iibdc);
                    items.Add((StatementsDataContract)iibdc);
                    break;

                case "WorksheetItemDataContract":
                    items.Add(WorksheetRender((WorksheetItemDataContract)iibdc));
                    break;

                case "ChapterDataContract":
                    // handle chapter it self
                    ChapterDataContract cdc = (ChapterDataContract)iibdc;
                    ChapterDataContract cdcNew = new ChapterDataContract { };
                    // handle chapter children
                    if (cdc.Items != null)
                    {
                        cdcNew.Items = new List<IndexItemBaseDataContract>();
                        foreach (IndexItemBaseDataContract ciibdc in cdc.Items)
                        {
                            cdcNew.Items.AddRange((List<IndexItemBaseDataContract>)HandleIndexItem(ciibdc));
                        }
                    }
                    items.Add((ChapterDataContract)cdcNew);

                    break;
                default:

                    break;
            }
            return items;
        }

        private PDFDataContract DocumentRender(DocumentItemDataContract documentItemDataContract)
        {
            PDFDataContract pdc = new PDFDataContract();
            return pdc;
        }

        private PDFDataContract FicheRender(FicheItemDataContract ficheItemDataContract)
        {
            PDFDataContract pdc = new PDFDataContract();
            return pdc;
        }

        private PDFDataContract StatementsRender(StatementsDataContract statementsDataContract)
        {
            PDFDataContract pdc = new PDFDataContract();
            return pdc;
        }

        private PDFDataContract WorksheetRender(WorksheetItemDataContract worksheetItemDataContract)
        {

            PDFDataContract pdc = new PDFDataContract();
            return pdc;

        }

    }
}
