﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Configuration;

namespace EY.com.eBook.Core.IText
{
    public class IndexRenderer
    {
        private IndexDataContract _index;
        private BaseFont _baseFont;
        private int pageCounter =1;
        private float rootWidth;

        public void Render(IndexDataContract idx, string culture, ref Document document, ref PdfWriter writer)
        {
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            _baseFont = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            _index = idx;
            PdfPTable rootTable = new PdfPTable(1);
            rootTable.KeepTogether = true;
            rootTable.SplitLate = false;
            rootTable.SplitRows = true;
            rootTable.WidthPercentage = 90f;
            rootWidth = (document.PageSize.Width - document.LeftMargin - document.RightMargin);// * (rootTable.WidthPercentage / 100));
            rootTable.TotalWidth = rootWidth;

            // move translations to database
            // and out of renderer. For now:

            PdfPTable titleTable = new PdfPTable(2);
            titleTable.SetWidths(new float[] { 90f, 10f });
            Font titleFont = new Font(_baseFont, 12);
            titleFont.SetStyle(Font.BOLD | Font.UNDERLINE);
            Phrase ph = null;
            PdfPCell pCell = null;

            switch (culture.ToLower())
            {
                case "nl-be":
                    ph = new Phrase("Inhoudstafel", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    titleTable.AddCell(pCell);

                    ph = new Phrase("Blz.", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    pCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    titleTable.AddCell(pCell);
                    break;
                case "fr-fr":
                    ph = new Phrase("Table des matières", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    titleTable.AddCell(pCell);

                    ph = new Phrase("Page", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    pCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    titleTable.AddCell(pCell);
                    break;
                default:
                    ph = new Phrase("Index", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    titleTable.AddCell(pCell);

                    ph = new Phrase("Page", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    pCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    titleTable.AddCell(pCell);
                    break;
            }
            PdfPCell titleCell = new PdfPCell(titleTable);
            titleCell.Border = 0;
            titleCell.BorderWidth = 0;
            rootTable.AddCell(titleCell);

            Phrase spacerPhrase = new Phrase(" ",GetFont(0));
            PdfPCell spacerCell = new PdfPCell(spacerPhrase);
            spacerCell.Border = 0;
            rootTable.AddCell(spacerCell);
            rootTable.AddCell(RenderLevel(0,idx.Items, ref document, ref writer,rootWidth*0.9f));
            document.Add(rootTable);
        }

        private PdfPCell RenderLevel(int level, List<IndexItemBaseDataContract> items, ref Document document, ref PdfWriter writer, float innerTableWidth)
        {
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(new float[] { 90f, 10f });
            table.WidthPercentage = 90f;
            table.TotalWidth = innerTableWidth;
            
            int headCounter = 1;

            foreach (IndexItemBaseDataContract iibdc in items)
            {
                if (level == 0 && iibdc.GetType().Name != "IndexPageDataContract" && iibdc.GetType().Name != "CoverpageDataContract")
                {
                    Phrase spacerPH = new Phrase(" ");
                    PdfPCell spacerCL = new PdfPCell(spacerPH);
                    spacerCL.Border = 0;
                    spacerCL.BorderWidth = 0;
                    spacerCL.Colspan = 2;
                    table.AddCell(spacerCL);
                }
                
                    switch (iibdc.GetType().Name)
                    {
                        case "ChapterDataContract":
                            ChapterDataContract cdc = (ChapterDataContract)iibdc;
                            if (cdc.Items != null && cdc.Items.Where(it => it.ShowInIndex).Count() > 0)
                            {
                                pageCounter = pageCounter + 2;
                                
                                
                                if (iibdc.ShowInIndex)
                                {
                                    Phrase headingPhrase = new Phrase(string.Format("{0}. {1}", headCounter, cdc.Title), GetFont(level));
                                    PdfPCell sctitleCell = new PdfPCell(headingPhrase);
                                    sctitleCell.Border = 0;
                                    sctitleCell.BorderWidth = 0;
                                    sctitleCell.Colspan = 2;
                                    table.AddCell(sctitleCell);
                                    
                                }
                                PdfPCell chapterContents = RenderLevel((level + 1), cdc.Items, ref document, ref writer, innerTableWidth * 0.9f);
                                if (iibdc.ShowInIndex)
                                {
                                    table.AddCell(chapterContents);
                                }
                            }
                            else
                            {
                                if (iibdc.ShowInIndex && cdc.Items != null)
                                {
                                    table.AddCell(CreateIndexCell(level, string.Format("{0}. {1}", headCounter, cdc.Title), innerTableWidth * 0.9f));
                                    PdfPCell cpageCell = new PdfPCell(new Phrase(pageCounter.ToString(), GetFont(level)));
                                    cpageCell.Border = 0;
                                    cpageCell.BorderWidth = 0;
                                    cpageCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    table.AddCell(cpageCell);
                                }
                                pageCounter = pageCounter + 2;
                                PdfPCell chapteritems = null;
                                if (cdc.Items != null)
                                {
                                    chapteritems = RenderLevel((level + 1), cdc.Items, ref document, ref writer, innerTableWidth * 0.9f);
                                }
                            }
                            headCounter++;
                            break;
                        case "IndexPageDataContract":
                        case "CoverpageDataContract":
                            pageCounter = pageCounter + 2;
                            //nothing to do
                            break;
                        case "PDFDataContract":
                            PDFDataContract pdc = (PDFDataContract)iibdc;
                            int from = 1;
                            int to = pdc.Pages != 0 ? pdc.Pages : 1;
                            if (pdc.FromPage.HasValue) from = pdc.FromPage.Value;
                            if (pdc.ToPage.HasValue) to = pdc.ToPage.Value;
                            int pages = to - from + 1;
                            if (iibdc.ShowInIndex)
                            {
                                table.AddCell(CreateIndexCell(level, string.Format("{0}. {1}", headCounter, pdc.Title), innerTableWidth * 0.9f));
                                PdfPCell pageCell = new PdfPCell(new Phrase(pageCounter.ToString(), GetFont(level)));
                                pageCell.Border = 0;
                                pageCell.BorderWidth = 0;
                                pageCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                table.AddCell(pageCell);
                            }
                            pageCounter = pageCounter + pages;
                            if (pages % 2 != 0) pageCounter++;
                            headCounter++;
                            break;
                    }
               
            }

            PdfPCell contentCell = new PdfPCell(table);
            contentCell.Border = 0;
            contentCell.BorderWidth = 0;
            if (level > 0) contentCell.Colspan = 2;
            return contentCell;
        }

        private PdfPCell CreateIndexCell(int level, string title, float maxWidth)
        {
            List<string> multipleLines = new List<string>();
            Font font = GetFont(level);
            
            float spaceSize = new Chunk(" ", font).GetWidthPoint();
            float pointSize = new Chunk(".", font).GetWidthPoint();
            maxWidth = (rootWidth*0.9f*0.9f) - (pointSize*2);

            int startText = title.IndexOf(' ') + 3;
            title = string.Concat("".PadLeft(level * 10, ' '),title);
            Phrase goodPhrase;
            Phrase chk = new Phrase(title, font);
            float titleWidth = GetWidth(chk) + 10;

            if (titleWidth > maxWidth)
            {
                int startIdx = 0;
                while (titleWidth > maxWidth)
                {

                    string titlePart = title;
                    Phrase fChk = new Phrase(titlePart, font);
                    while ((GetWidth(fChk) + 10) > maxWidth)
                    {
                        titlePart = titlePart.Substring(0, titlePart.LastIndexOf(' '));
                        fChk = new Phrase(titlePart, font);
                    }
                    multipleLines.Add(titlePart);
                    title = title.Substring(titlePart.Length).Trim();
                    title = string.Concat("".PadLeft((level * 10) + startText, ' '), title);
                    chk = new Phrase(title, font);
                    titleWidth = GetWidth(chk) + 10;

                }
            }

            float pointsWidth=0;
            while (titleWidth + pointsWidth < (maxWidth))
            {
                title = title + ".";
                pointsWidth += pointSize;
            }
            if (multipleLines.Count>0)
            {
                string finalTitle = "";
                foreach (string titlePart in multipleLines)
                {
                    finalTitle = finalTitle + titlePart + Chunk.NEWLINE;
                }
                title = finalTitle + title;
            }
            PdfPCell cell = new PdfPCell(new Phrase(title, font));
            
            cell.Border = 0;
            cell.BorderWidth = 0;
            return cell;
        }

        private float GetWidth(Phrase phrase)
        {
            float wd = 0f;
            foreach (Chunk chk in phrase.Chunks)
            {
                wd += chk.GetWidthPoint();
            }
            return wd;
        }

        private Font GetFont(int level)
        {
            Font font = new Font(_baseFont, level==0? 11 : 10);
            if (level == 0) font.SetStyle(Font.BOLD);
            return font;
        }
    }
}
