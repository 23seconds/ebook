﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Configuration;
using System.ComponentModel;
//using EY.com.eBook.Core.EF;
//using EY.com.eBook.Core.Data;
//using EY.com.eBook.Core;
using System.Data;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml;
using EY.com.eBook.API.Contracts.Data;




namespace EY.com.eBook.Core.IText
{
    /*
    public sealed class DataContext
    {
        private static readonly DataContext _ctx = new DataContext();


       

        private System.Threading.ReaderWriterLockSlim readWriteLock = new System.Threading.ReaderWriterLockSlim();

        private Dictionary<Guid, ITextSessionDataContract> _sessions = new Dictionary<Guid, ITextSessionDataContract>();

        private DataContext() { }

        public static DataContext Context { get { return _ctx; } }

        public string Status { get; set; }

        public void StartUp(int minThreads, int maxThreads)
        {

            // Create Threadpool

            readWriteLock.EnterWriteLock();
            try
            {
                
            }
            finally
            {
                readWriteLock.ExitWriteLock();
            }
            //started up smart threadpool
        }

        public void Stop()
        {
            
        }

        public void AddSession(ITextSessionDataContract sdc)
        {
            readWriteLock.EnterWriteLock();
            try
            {
                _sessions.Add(sdc.Id, sdc);
            }
            finally
            {
                readWriteLock.ExitWriteLock();
            }
        }

        public void RemoveSession(Guid sessionId)
        {
            readWriteLock.EnterWriteLock();
            try
            {
                if (_sessions.ContainsKey(sessionId)) _sessions.Remove(sessionId);
            }
            finally
            {
                readWriteLock.ExitWriteLock();
            }
        }

        public void UpdateSession(ITextSessionDataContract sdc)
        {
            readWriteLock.EnterWriteLock();
            try
            {
                if (_sessions.ContainsKey(sdc.Id)) _sessions[sdc.Id]=sdc;
            }
            finally
            {
                readWriteLock.ExitWriteLock();
            }
        }

        public ITextSessionDataContract GetSession(Guid sessionId)
        {
            ITextSessionDataContract sdc = null;
            readWriteLock.EnterReadLock();
            try
            {
                if (_sessions.ContainsKey(sessionId))
                    sdc = _sessions[sessionId];
            }
            finally
            {
                readWriteLock.ExitReadLock();
            }
            if (sdc.Percentage == 100) RemoveSession(sessionId);
            return sdc;
        }

        public ITextSessionDataContract AddGenerateBundle(BundleGenerationDataContract bgdc)
        {
            ITextSessionDataContract sdc = new ITextSessionDataContract
            {
                Id = Guid.NewGuid()
                ,
                StartDate = DateTime.Now
                ,
                Percentage = 0
            };
            AddSession(sdc);

            bgdc.SessionId = sdc.Id;

            
            return sdc;
        }


        public object GenerateBundle(object state)
        {
            BundleGenerationDataContract bgdc = (BundleGenerationDataContract)state;
            Worker worker = new Worker
            {
                Client = bgdc.Client
                ,
                File = bgdc.File
            };

            // loop through index datacontract, detecting items that need to be rendered before.
            // everything but a pdfpagedatacontract
             
            bgdc.BundleName = Path.Combine(ConfigurationManager.AppSettings["eBook.Itext.WorkingFolder"], string.Format("{0}.", bgdc.SessionId.ToString()));
            worker.BundlePDFFiles(bgdc.Report.Index, bgdc.ReportStyle, bgdc.SessionId,bgdc.Culture);

            ITextSessionDataContract sdc = DataContext.Context.GetSession(bgdc.SessionId);
            sdc.Percentage = 100;
            sdc.FinishedDate = DateTime.Now;
            sdc.PdfDocumentPath = bgdc.BundleName;
            DataContext.Context.UpdateSession(sdc);

            return state;
        }

        public ITextSessionDataContract AddGeneratePDF(PDFGenerationDataContract pgdc)
        {
            ITextSessionDataContract sdc = new ITextSessionDataContract
            {
                Id = Guid.NewGuid()
                ,
                StartDate = DateTime.Now
                ,
                Percentage = 0
            };
            AddSession(sdc);

            pgdc.SessionId = sdc.Id;

            
            return sdc;
        }

        public object GeneratePDF(object state)
        {
            PDFGenerationDataContract pgdc = (PDFGenerationDataContract)state;
            Worker worker = new Worker
            {
                File = pgdc.File
            };

            pgdc.PdfDocumentPath = worker.PDFGenerator(pgdc.XmlDocument,pgdc.SessionId);
            
            ITextSessionDataContract sdc = DataContext.Context.GetSession(pgdc.SessionId);
            sdc.Percentage = 100;
            sdc.FinishedDate = DateTime.Now;
            sdc.PdfDocumentPath = pgdc.PdfDocumentPath;
            DataContext.Context.UpdateSession(sdc);

            return state;
        }
    }

    */
}

