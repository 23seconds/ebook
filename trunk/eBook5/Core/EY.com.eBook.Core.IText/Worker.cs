﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Xml;
using System.Globalization;
using System.util;
using iTextSharp.text.factories;
using System.Text.RegularExpressions;
using EY.com.eBook.API.Contracts.Data;
using System.Configuration;
using EY.com.eBook.API.Contracts.Proxies;
using System.Xml.Linq;
using System.Linq;
using System.ServiceModel;
using iTextSharp.text.pdf.codec;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.Core.IText
{
    /// <summary>
    /// Worker
    /// </summary>
    public class Worker : IDisposable
    {
        private readonly float[] _defaultMargins = new float[4];
        private Dictionary<Guid, string> pdfFiles;
        private PdfWriter writer;
        private Font _standardFont;
        private float _standardFontSize;
        private BaseFont bf;
        private float _defaultStandardFontSize = 10;
        private int minusChap = -2;
        private iTextSharp.text.Document _document;
        private bool _standalone;
        private Rectangle _pageSize;
        private Font _defaultStandardFont;
        private bool _draft = true;
        private Rectangle _pageSizeLandscape;
        private Rectangle _pageSizePortret;
        private int page_number;
        private int page_total;       
        private string end_date = DateTime.Now.Date.ToShortDateString();
        private List<int> _emptyPages = new List<int>();
        private Font _lastFont;
        private string _imageFolder = Config.PdfImageResource;


        public FileDataContract File { get; set; }
        public ClientDataContract Client { get; set; }
        public IndexDataContract IndexDataContract { get; set; }
        public bool NoDraft { get; set; }
        public string Culture { get; set; }

        private IndexItemBaseDataContract _currentIndexItem;

        public Worker()
        {
            SetFont();
        }

        public string PDFGenerator(string xmlDocument, Guid sessionID)
        {
            
            return CreatePdf(xmlDocument,null).PDFLocation;
        }

        public PDFDataContract CreatePdf(string xmlDocument,IndexItemBaseDataContract iibdc)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlDocument);

            PDFDataContract pdc = new PDFDataContract
             {
                 Id = Guid.NewGuid()
             };
            if (iibdc != null)
            {
                pdc.HeaderConfig = iibdc.HeaderConfig;
                pdc.FooterConfig = iibdc.FooterConfig;
                //pdc.Id = iibdc.Id != Guid.Empty ? iibdc.Id : Guid.NewGuid();
                pdc.ShowInIndex = iibdc.ShowInIndex;
                pdc.Title = iibdc.Title;
            }
            pdc.FromPage = 1;
            pdc.PDFLocation=Path.Combine(Config.PdfWorkingFolder,string.Format("{0}.pdf",pdc.Id.ToString()));

            XmlElement rootNode = doc.DocumentElement;

            _document = new iTextSharp.text.Document();


            PdfWriter wri = PdfWriter.GetInstance(_document, new FileStream(pdc.PDFLocation, FileMode.Create));
            

            if (rootNode != null)
            {
                SetRootAttributes(rootNode);
                SetPageSize(rootNode);
                SetFont(rootNode);
                SetDefaultMargins(rootNode);

                _document.Open();

            }
            else
            {
                throw new MissingMemberException("The provided document doesn't contain a rootnode");
            }

            
            //  int percentageToAdd = 59 / xmlNodeList.Count;
            
            Chapter chapter = new Chapter(0);

            if (rootNode.ChildNodes.Count == 0)
            {
                rootNode.InnerXml = "<phrase>ITEM HAS NO DATA</phrase>";
            }

            chapter = (Chapter)ProcessData(rootNode, chapter);
            _document.Add(chapter);
            
            _document.Close();

            PdfReader reader = new PdfReader(pdc.PDFLocation);
            pdc.Pages = reader.NumberOfPages;
            pdc.ToPage = pdc.Pages;
            pdc.NoDraft = iibdc.NoDraft;
            return pdc;
        }

        

        public void AddHeaderAndFooters(string xmlDocument, string pdfDocument)
        {

            // Selecteer gegevens van de headerfooter in de xml file
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlDocument);

            XmlElement rootNode = doc.DocumentElement;
            SetFont(rootNode);
            Boolean landscape = true;

            if (rootNode.SelectNodes("//Landscape").Count.Equals(0))
                landscape = false;

            XmlNodeList headerfooterlist = rootNode.SelectNodes("content//chapter//section//headerfooter");
            XmlNode headerfooterToUse = null;

            foreach (XmlNode hfNode in headerfooterlist)
            {
                if (landscape)
                {
                    if (hfNode.Attributes[0].Name.ToString() == "id" && hfNode.Attributes[0].Value.Contains("landscape"))
                        headerfooterToUse = hfNode;
                }
                else
                {
                    if (hfNode.Attributes[0].Name.ToString() == "id" && !hfNode.Attributes[0].Value.Contains("landscape"))
                        headerfooterToUse = hfNode;
                }
            }

            FileStream fs = new FileStream(pdfDocument, FileMode.Open, FileAccess.ReadWrite);
            PdfReader reader = new PdfReader(fs);
            PdfStamper stamper = new PdfStamper(reader, fs);

            page_total = reader.NumberOfPages;
            page_number = 1;

            while (page_number < reader.NumberOfPages + 1)
            {
                var pdfContentByte = stamper.GetUnderContent(page_number);

                pdfContentByte.BeginText();

                foreach (XmlNode node in headerfooterToUse)
                    ProcessContentBlock(pdfContentByte, node, reader.GetPageSize(page_number));

                pdfContentByte.EndText();
                page_number++;
            }

            stamper.Close();
        }

        public void AddHeaderAndFooters2(string xmlDocument, string pdfDocument)
        {
            // Selecteer gegevens van de headerfooter in de xml file
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlDocument);

            XmlElement rootNode = doc.DocumentElement;
            SetFont();

            XmlNode headerfooterToUse = rootNode.SelectNodes("//headerfooter")[0];

            FileStream fs = new FileStream(pdfDocument, FileMode.Open, FileAccess.ReadWrite);
            PdfReader reader = new PdfReader(fs);
            PdfStamper stamper = new PdfStamper(reader, fs);

            //page_total_teller = reader.NumberOfPages;
            int page_number_teller = 1;

            while (page_number_teller < reader.NumberOfPages + 1)
            {
                page_number++;
                var pdfContentByte = stamper.GetUnderContent(page_number_teller);

                pdfContentByte.BeginText();

                foreach (XmlNode node in headerfooterToUse)
                    ProcessContentBlock(pdfContentByte, node, reader.GetPageSize(page_number_teller));

                pdfContentByte.EndText();
                page_number_teller++;
            }

            stamper.Close();
        }


        private static void GetAttributesFromCurrentNode(XmlNode node, ref string align, ref string top, ref string left, ref string border, ref string right, ref string bottom)
        {
            foreach (XmlAttribute attr in node.Attributes)
            {
                if (attr.Name == "align")
                    align = attr.Value;

                if (attr.Name == "top")
                    top = attr.Value;

                if (attr.Name == "left")
                    left = attr.Value;

                if (attr.Name == "border")
                    border = attr.Value;

                if (attr.Name == "right")
                    right = attr.Value;

                if (attr.Name == "bottom")
                    bottom = attr.Value;
            }
        }

        

        private IndexDataContract TraverseIndex(IndexDataContract idc, ReportStyleDataContract style, ref iTextSharp.text.Document document, Guid sessionID)
        {
            int percentage = 40;
            int percentageToAdd = 49 / idc.Items.Count;

            for (int i = 0; i < idc.Items.Count; i++)
            {
                idc.Items[i] = HandleIndexItem(idc, idc.Items[i], style, ref document);
                //percentage += percentageToAdd;
                //UpdateSessionPercentage(sessionID, percentage);
            }
            return idc;
        }

        private int InsertPDF(string pdfLocation)
        {
            return InsertPDF(pdfLocation, -1, -1);
        }

        private int InsertPDF(string pdfLocation, int fromPage, int toPage)
        {
            int startsAt = 0;
            PdfContentByte cb = writer.DirectContent;
            PdfImportedPage page;
            int rotation;
            //AddHeaderAndFooters2(((PDFDataContract)iibdc).HeaderFooter, ((PDFDataContract)iibdc).PDFLocation);
            PdfReader reader = new PdfReader(pdfLocation);
            toPage = toPage > -1 ? toPage : reader.NumberOfPages;
            fromPage = fromPage > -1 ? fromPage : 1;

            for (int i = fromPage; i <= toPage; i++)
            {
                _document.SetPageSize(reader.GetPageSizeWithRotation(i));
                _document.NewPage();
                //_document.Add(new Chunk(" "));
                if (i == fromPage) startsAt = writer.CurrentPageNumber;
                page = writer.GetImportedPage(reader, i);

                rotation = reader.GetPageRotation(i);
                if (rotation == 90 || rotation == 270)
                    cb.AddTemplate(page, 0, 0, 0, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                else
                    cb.AddTemplate(page, 0, 0);
            }
            _pageSize = PageSize.A4;
            //_document.SetPageSize(PageSize.A4);
            return startsAt;
        }

        private IndexItemBaseDataContract HandleIndexItem(IndexDataContract idc, IndexItemBaseDataContract iibdc, ReportStyleDataContract style, ref iTextSharp.text.Document document)
        {
            _currentIndexItem = iibdc;
            string tst = string.Empty;
            switch (iibdc.GetType().Name)
            {
                case "CoverpageDataContract":
                    document.NewPage();
                    document.Add(new Chunk(""));
                    iibdc.StartsAt = writer.CurrentPageNumber;
                    //ProcessData(ConvertReportStyleToXmlNode(style.CoverPage, "CoverPage"));
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(iibdc.iTextTemplate);
                    ProcessItem(doc.DocumentElement);
                    // insert empty page (back side)
                    document.NewPage();
                    document.Add(new Chunk(""));
                    iibdc.EndsAt = writer.CurrentPageNumber;
                    _emptyPages.Add(iibdc.EndsAt);

                    break;
                case "IndexPageDataContract":
                    int bf = writer.CurrentPageNumber;
                    document.NewPage();
                    document.Add(new Chunk(" "));
                    iibdc.StartsAt = writer.CurrentPageNumber;
                    new IndexRenderer().Render(IndexDataContract,Culture, ref document, ref writer);
                    iibdc.EndsAt = writer.CurrentPageNumber;
                    if (iibdc.EndsAt % 2 != 0) // make sure ends at face down
                    {
                        document.NewPage();
                        document.Add(new Chunk(" "));
                        iibdc.EndsAt = writer.CurrentPageNumber;
                        _emptyPages.Add(iibdc.EndsAt);
                    }
                    break;
                case "PDFDataContract":
                    PDFDataContract pdc = (PDFDataContract)iibdc;
                    //iibdc.StartsAt = writer.CurrentPageNumber+1;
                    try
                    {
                        
                        if (!pdc.IsTempRender)
                        {
                            pdc.PDFLocation = Path.Combine(ConfigurationManager.AppSettings["eBook.Folders.ExtraPdfFiles"], string.Format("{0}.pdf", pdc.Id.ToString()));
                        }
                        iibdc.StartsAt = InsertPDF(pdc.PDFLocation);
                    }
                    catch (Exception e)
                    {
                        if (e.Message.Contains("PdfReader not opened with owner password"))
                        {
                            throw new FaultException<string>(string.Format("The PDF with title {0} is a secured pdf. Please upload a security free version. (deleting/replacing a pdf in the library will come soon)", pdc.Title));
                        }
                        else
                        {
                            throw new FaultException<string>(string.Format("PDF:{0}({1}), ERROR:{2}", pdc.Title, pdc.PDFLocation, e.Message));
                        }
                    }
                    
                    iibdc.EndsAt = writer.CurrentPageNumber;
                    if (iibdc.EndsAt % 2 != 0) // make sure ends at face down
                    {
                        document.NewPage();
                        document.Add(new Chunk(" "));
                        iibdc.EndsAt = writer.CurrentPageNumber;
                        _emptyPages.Add(iibdc.EndsAt);
                    }
                    break;
                case "ChapterDataContract":
                    iibdc.StartsAt = writer.CurrentPageNumber + 1;
                    document.SetPageSize(PageSize.A4);
                    document.NewPage();
                    
                    // handle chapter it self
                    ChapterDataContract cdc = (ChapterDataContract)iibdc;
                    XmlDocument cdoc = new XmlDocument();
                    cdoc.LoadXml(cdc.iTextTemplate);
                    ProcessItem(cdoc.DocumentElement);

                    // insert empty page (back side) // make sure ends at face down
                    document.NewPage(); 
                    document.Add(new Chunk(" "));
                    iibdc.EndsAt = writer.CurrentPageNumber;
                    _emptyPages.Add(iibdc.EndsAt);
                    // handle chapter children
                    for(int i=0;i<cdc.Items.Count;i++) {
                         cdc.Items[i] = HandleIndexItem(idc, cdc.Items[i], style, ref document);
                    }

                    break;
            }
            return iibdc;
        }

        public string BundlePDFFiles(IndexDataContract index, ReportStyleDataContract style, Guid sessionID,string culture)
        {
            //Set font
            SetFont();
            Culture = culture; 
            // step 0: create all needed pdf's

            // step 1: creation of a document-object
            _document = new iTextSharp.text.Document(PageSize.A4);
            SetDefaultMargins();

            string tempName = Path.Combine(Config.PdfWorkingFolder, string.Format("{0}.pdf", Guid.NewGuid().ToString()));
            string bundlePDFDocument = Path.Combine(Config.PdfWorkingFolder, string.Format("{0}.pdf", sessionID.ToString()));
            //UpdateSessionPercentage(sessionID, 10);

            // step 2: we create a writer that listens to the document
            writer = PdfWriter.GetInstance(_document, new FileStream(tempName, FileMode.Create));

            //UpdateSessionPercentage(sessionID, 20);

            // step 3: we open the document
            _document.Open();
            _document.SetMargins(60f, 60f, 95f, 45f);

           // UpdateSessionPercentage(sessionID, 30);

            // step 4: we calculate the page_total
            /*
            foreach (IndexItemBaseDataContract iibdc in index.Items)
            {
                CalculatePageTotal(iibdc);
            }*/

           // UpdateSessionPercentage(sessionID, 40);

            index = TraverseIndex(index, style, ref _document, sessionID);

            /*
             * TEST ROTATION
            string landscapeRotation ="nStart = 0;\r"
                + "nEnd = this.numPages - 1;\r"
                + "nRotate = 270;\r"
                + "bLandscape = true;\r"
                + "pp = this.getPrintParams();\r"
                + "app.alert(pp.printerName);\r"
                + @"if (pp.printerName.indexOf('BRUS-BLACK-WHITE')>-1)"
                + "{\r"
                    + "app.alert('ROTATION ACTIVE');\r"
                    + "for (var i = nStart; i <= nEnd; i++)\r"
                    + "{\r"
                        + "aRect = this.getPageBox('Media',i);\r"
                        + "Width = aRect[2] - aRect[0];\r"
                        + "Height = aRect[1] - aRect[3];\r"
                        + "app.alert('width:'+Width);\r"
                        + "app.alert('height:'+Height);\r"
                        + "if (Height > Width) {\r"
                            + "if (!bLandscape) {\r"
                                + "app.alert('rotating page '+ i + ',rotation:'+nRotate);\r"
                                + "try { this.setPageRotations(i,i,nRotate); } catch(e) {app.alert('Processing error: '+e);}\r"
                                 + "app.alert('rotated')"
                            + "}\r"
                        + "}\r"
                        + "else {\r"
                            + "             if (bLandscape) {\r"
                            + "app.alert('rotating (else) page '+ i + ',rotation:'+nRotate);\r"
                                + "try { this.setPageRotations(i,i,nRotate); } catch(e) {app.alert('Processing error: '+e);}\r"
                                + "app.alert('rotated')"
                            + "}\r"
                        + "}\r"
                    + "}\r"
                + "}\r";

            string landscapeRestore = "nStart = 0;\r"
            + "nEnd = this.numPages - 1;\r"
            + "nRotate = 90;\r"
            + "bLandscape = true;\r"
            + "for (var i = nStart; i <= nEnd; i++)\r"
            + "{\r"
                + "aRect = this.getPageBox('Media',i);\r"
                + "Width = aRect[2] - aRect[0];\r"
                + "Height = aRect[1] - aRect[3];\r"
                + "if (Height > Width) {\r"
                    + "if (!bLandscape) {\r"
                        + "this.setPageRotations(i,i,nRotate);\r"
                    + "}\r"
                + "}\r"
               + "else {\r"
                    + "  if (bLandscape) {\r"
                        + "this.setPageRotations(i,i,nRotate);\r"
                    + "}\r"
                + "}\r"
            + "}\r";

            //Set action on event WillPrint and DidPrint

            PdfAction printLandscapeFix = PdfAction.JavaScript(landscapeRotation, writer);

            PdfAction restoreLandscape = PdfAction.JavaScript(landscapeRestore, writer);


            
            writer.SetAdditionalAction(PdfWriter.WILL_PRINT, printLandscapeFix);

            writer.SetAdditionalAction(PdfWriter.DID_PRINT, restoreLandscape);
            */
            _document.Close();
            
            writer.Close();

            PdfReader reader = new PdfReader(tempName, true);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(bundlePDFDocument, FileMode.Create));
            
            PostProcessDocument(index,ref stamper);
            
            // step 5: we close the document
            reader.Close();
            stamper.Close();
            writer = null;
            
            stamper = null;
            reader = null;
            System.IO.File.Delete(tempName);
            /*
             * test security
            PdfReader encreader = new PdfReader(bundlePDFDocument);
            PdfEncryptor.Encrypt(encreader, new FileStream(tempName, FileMode.Create), null,
                null, PdfWriter.ALLOW_ASSEMBLY | PdfWriter.ALLOW_COPY
                    | PdfWriter.ALLOW_DEGRADED_PRINTING | PdfWriter.ALLOW_FILL_IN
                    | PdfWriter.ALLOW_MODIFY_ANNOTATIONS |
                        PdfWriter.ALLOW_MODIFY_CONTENTS
                    | PdfWriter.ALLOW_PRINTING | PdfWriter.ALLOW_SCREENREADERS,
                false);

            System.IO.File.Delete(bundlePDFDocument);
            System.IO.File.Move(tempName, bundlePDFDocument);
            */
            //UpdateSessionPercentage(sessionID, 90);
            
            return bundlePDFDocument;
        }

        private void PostProcessDocument(IndexDataContract index, ref PdfStamper stamper)
        {
            foreach (IndexItemBaseDataContract iibdc in index.Items)
            {
                PostProcess(iibdc, ref stamper);
            }
        }


        public void PostProcess(IndexItemBaseDataContract iibdc, ref PdfStamper stamper)
        {

            SetDraft(iibdc, ref stamper);
            RenderHeaderFooter(iibdc, ref stamper);
            if (iibdc.GetType().Name == "ChapterDataContract")
            {
                foreach (IndexItemBaseDataContract ciibdc in ((ChapterDataContract)iibdc).Items)
                {
                    PostProcess(ciibdc, ref stamper);
                }
            }
        }

        private void SetDraft(IndexItemBaseDataContract iibdc, ref PdfStamper stamper)
        {
            if (NoDraft || iibdc.NoDraft ) return;
            //if (iibdc.GetType().Name == "ChapterDataContract" ract" || iibdc.GetType().Name == "CoverpageDataContract") return;
            string dpath = Path.Combine(Config.PdfImageResource, "draft.png");
            Image img = Image.GetInstance(dpath);

            img.ScalePercent(80);

            for (int i = iibdc.StartsAt; i <= iibdc.EndsAt; i++)
            {
                if (!_emptyPages.Contains(i))
                {
                    Rectangle pageSize = stamper.Reader.GetPageSize(i);
                    bool landscape = pageSize.Width > pageSize.Height;
                    if (!landscape && (pageSize.Rotation == 90 || pageSize.Rotation == 180))
                    {
                        landscape = true;
                    }
                    if (landscape)
                    {
                        img.SetAbsolutePosition(150, 50);
                    }
                    else
                    {
                        img.SetAbsolutePosition(50, 150);
                    }

                    PdfContentByte under = stamper.GetUnderContent(i);
                    under.PdfDocument.SetPageSize(pageSize);
                    under.AddImage(img);
                }
            }
        }

        private void RenderHeaderFooter(IndexItemBaseDataContract iibdc, ref PdfStamper stamper)
        {
            if (iibdc.HeaderConfig != null)
            {
                if (iibdc.HeaderConfig.Enabled && !string.IsNullOrEmpty(iibdc.HeaderConfig.Style))
                {
                    for (int i = iibdc.StartsAt; i <= iibdc.EndsAt; i++)
                    {
                        if (!_emptyPages.Contains(i))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(iibdc.HeaderConfig.Style);
                            ProcessHeaderFooter(stamper.GetOverContent(i), doc.DocumentElement, stamper.Reader.GetPageSize(i), i, stamper.Reader.NumberOfPages);
                        }
                    }
                }
            }
            if (iibdc.FooterConfig != null)
            {
                if (iibdc.FooterConfig.Enabled && !string.IsNullOrEmpty(iibdc.FooterConfig.Style))
                {
                    for (int i = iibdc.StartsAt; i <= iibdc.EndsAt; i++)
                    {
                        if (!_emptyPages.Contains(i))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(iibdc.FooterConfig.Style);
                            ProcessHeaderFooter(stamper.GetOverContent(i), doc.DocumentElement, stamper.Reader.GetPageSize(i), i, stamper.Reader.NumberOfPages);
                        }
                    }
                }
            }
        }

        private void ProcessHeaderFooter(PdfContentByte over, XmlNode xmlNode, Rectangle rect, int currentPage, int totalPages)
        {
            bool isLandscape = rect.Height < rect.Width;
            foreach (XmlNode block in xmlNode.SelectNodes("block"))
            {
                float top = 0f;
                float left = 0f;
                //float.Parse(block.Attributes["top"].Value, NumberFormatInfo.InvariantInfo);
                int align = Element.ALIGN_UNDEFINED;
                switch (block.Attributes["type"].Value.ToUpper())
                {
                    case "TOPLEFT":
                        align = Element.ALIGN_LEFT;
                        top = isLandscape ? 20f : 20f;
                        left = isLandscape ? 50f : 20f;
                        break;
                    case "TOPRIGHT":
                        align = Element.ALIGN_RIGHT;
                        top = isLandscape ? 20f : 20f;
                        left = -50f;
                        break;
                    case "TOPTITLE":
                        align = Element.ALIGN_LEFT;
                        top = 85f;
                        left = 60f;
                        break;
                    case "BOTTOMCENTER":
                        align = Element.ALIGN_CENTER;
                        top = -50f;
                        left = isLandscape ? 440f : 300f;
                        break;
                }
                
                
                if (left < 0)
                {
                    left += rect.Width;
                }
                if (top > 0)
                {
                    top = rect.Height - top;
                }
                else
                {
                    top = Math.Abs(top);
                }

                foreach (XmlNode subNode in block.SelectNodes("(phrase|image)"))
                {
                    Properties attributes = GetAttributes(subNode);

                    switch (subNode.Name.ToUpper())
                    {
                        case "PHRASE":
                            string itemText = subNode.InnerText;
                            itemText = itemText.Replace("[PAGE_NUMBER]", currentPage.ToString());
                            itemText = itemText.Replace("[PAGE_TOTAL]", totalPages.ToString());

                            Phrase phText = new Phrase(itemText, GetFont(subNode));

                            ColumnText.ShowTextAligned(over, align, phText, left, top, 0);

                            float padding = 2;
                            if (attributes.ContainsKey("padding"))
                            {
                                padding = float.Parse(attributes["padding"], NumberFormatInfo.InvariantInfo);
                            }

                            top -= (phText.Font.Size + padding);
                            break;
                      /*  case "IMAGE":
                            Image img = ElementFactory.GetImage(attributes);
                            img.SetAbsolutePosition(left, top);
                            if (attributes.ContainsKey("scalepercent"))
                            {
                                img.ScalePercent(float.Parse(attributes["scalepercent"], NumberFormatInfo.InvariantInfo));
                            }
                            over.AddImage(img);
                            break;*/
                    }
                }
            }
        }
        

        private static void UpdateSessionPercentage(Guid sessionID, int percentage)
        {
            //ITextSessionDataContract sdc = DataContext.Context.GetSession(sessionID);
            //sdc.Percentage = percentage;
            //DataContext.Context.UpdateSession(sdc);
        }

        private void CalculatePageTotal(IndexItemBaseDataContract iibdc)
        {


            switch (iibdc.GetType().Name)
            {
                case "CoverpageDataContract":
                case "IndexPageDataContract":
                    page_total++;

                    page_total++;
                    break;
                case "PDFDataContract":
                    PdfReader reader = new PdfReader(((PDFDataContract)(iibdc)).PDFLocation);
                    page_total = page_total + reader.NumberOfPages;
                    break;
                case "ChapterDataContract":

                    if (page_total % 2 != 0)
                    {
                        page_total++;
                    }
                    page_total++;
                    page_total++;
                    ChapterDataContract cdc = (ChapterDataContract)iibdc;
                    foreach (IndexItemBaseDataContract ciibdc in cdc.Items)
                    {
                        CalculatePageTotal(ciibdc);
                    }

                    break;
            }
        }


        #region "Helperclass"
        private static float ConfigureDefaultMargins(XmlElement RootNode, string position)
        {
            return float.Parse(RootNode.Attributes.GetNamedItem(position).Value, NumberFormatInfo.InvariantInfo);
        }

        private static float ConfigureDefaultMargins(XmlNode node, string position)
        {
            return float.Parse(node.Attributes.GetNamedItem(position).Value, NumberFormatInfo.InvariantInfo);
        }

        private static bool NodeHasAttribute(XmlNode node, string attributeName)
        {
            XmlNode attrib;
            if (node.Attributes == null)
                attrib = null;
            else
                attrib = node.Attributes.GetNamedItem(attributeName);
            return attrib != null;
        }

        private void SetDefaultMargins()
        {
            _defaultMargins[0] = 60f;
            _defaultMargins[1] = 60f;
            _defaultMargins[2] = 110f;
            _defaultMargins[3] = 55f;

            _document.SetMargins(_defaultMargins[0], _defaultMargins[1], _defaultMargins[2], _defaultMargins[3]);
        }

        private void SetDefaultMargins(XmlElement RootNode)
        {
            if (NodeHasAttribute(RootNode, "margins"))
            {

                string[] margs = RootNode.Attributes.GetNamedItem("margins").Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                float[] fmargs = new float[4];
                for (int i = 0; i < 4; i++)
                {
                    if (margs.Length > i)
                    {
                        fmargs[i] = 60f;
                        float.TryParse(margs[i], out fmargs[i]);
                    }
                    else
                    {
                        fmargs[i] = 60f;
                    }
                }
            }
            else
            {
                SetDefaultMargins();
            }
        }

        private string GetSize(XmlNode node)
        {
            if (NodeHasAttribute(node, "size"))
                return node.Attributes.GetNamedItem("size").Value;

            return node.ParentNode != null
                       ? GetSize(node.ParentNode)
                       : _standardFontSize.ToString();
        }


        private Font GetFont(XmlNode node)
        {
            Font font = new Font(_standardFont.BaseFont, _standardFontSize);
            font.SetStyle(Font.NORMAL);
            font.Size = float.Parse(GetSize(node));
            string style = string.Empty;
            if (NodeHasAttribute(node, "fontstyle"))
                style = node.Attributes.GetNamedItem("fontstyle").Value;

            if (NodeHasAttribute(node, "style"))
                style += node.Attributes.GetNamedItem("style").Value;

            if (style.ToLower().IndexOf("bold") > -1)
                font.SetStyle(Font.BOLD);

            if (style.ToLower().IndexOf("italic") > -1)
                font.SetStyle(Font.ITALIC);

            if (style.ToLower().IndexOf("underline") > -1)
                font.SetStyle(Font.UNDERLINE);
            _lastFont = font;
            return font;
        }

        private string GetImportGenericTag(Properties attributes, int page, bool first)
        {
            StringBuilder importKeyTag = new StringBuilder("IMPORT::");
            if (attributes.ContainsKey("fileid"))
            {
                importKeyTag.Append(attributes["fileid"]);
                importKeyTag.Append("::").Append(page.ToString());
                importKeyTag.Append("::").Append(first.ToString());
            }
            else
            {
                importKeyTag.Append(Guid.Empty.ToString());
                importKeyTag.Append("::").Append(page.ToString());
                importKeyTag.Append("::" + false.ToString());
            }
            if (attributes.ContainsKey("headerfooter"))
                importKeyTag.Append("::").Append(attributes["headerfooter"]);
            else
                importKeyTag.Append("::").Append(false.ToString());

            return importKeyTag.ToString();
        }

        private static Properties GetAttributes(XmlNode node)
        {
            Properties attributes = new Properties();

            foreach (XmlNode attrib in node.Attributes)
                attributes.Add(attrib.Name, attrib.Value);

            return attributes;
        }

        private static float GetAttributeValue(XmlNode block, string position)
        {
            if (block.Attributes[position] == null)
                return 0;
            else
                return float.Parse(block.Attributes[position].Value, NumberFormatInfo.InvariantInfo);
        }

        private static int GetAlignment(string alignment)
        {
            switch (alignment.ToUpper())
            {
                case "TOP":
                    return Element.ALIGN_TOP;
                case "BOTTOM":
                    return Element.ALIGN_BOTTOM;
                case "BASELINE":
                    return Element.ALIGN_BASELINE;
                case "CENTER":
                    return Element.ALIGN_CENTER;
                case "MIDDLE":
                    return Element.ALIGN_MIDDLE;
                case "JUSTIFIED":
                    return Element.ALIGN_JUSTIFIED;
                case "LEFT":
                    return Element.ALIGN_LEFT;
                case "RIGHT":
                    return Element.ALIGN_RIGHT;
                default:
                    return Element.ALIGN_UNDEFINED;
            }
        }

        private void SetPageSize(XmlElement RootNode)
        {
            Properties attribs = GetAttributes(RootNode);

            _pageSizePortret = PageSize.A4;
                //(Rectangle)typeof(PageSize).GetField(RootNode.Attributes.GetNamedItem("pagesize").Value).GetValue(null);
            _pageSizeLandscape = new RectangleReadOnly(842, 595);        

            if (attribs.ContainsKey("orientation") && attribs["orientation"].ToUpper().Trim()=="LANDSCAPE")
                _pageSize = _pageSizeLandscape;
            else
                _pageSize = _pageSizePortret;

            _document.SetPageSize(_pageSize);
        }

        private void SetFont(XmlElement RootNode)
        {
            SetFont();
           /* FontFactory.Register(@RootNode.Attributes.GetNamedItem("font").Value.ToString(), "EYInterstate-Light");
            bf = BaseFont.CreateFont(@RootNode.Attributes.GetNamedItem("font").Value.ToString(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            _defaultStandardFontSize = (float)Convert.ToDouble(RootNode.Attributes.GetNamedItem("size").Value);
            _defaultStandardFont = new Font(bf, (float)Convert.ToDouble(RootNode.Attributes.GetNamedItem("size").Value));
            _standardFont = _defaultStandardFont;*/
        }
        private void SetFont()
        {
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            _defaultStandardFontSize = (float)Convert.ToDouble(12);
            _defaultStandardFont = new Font(bf, (float)Convert.ToDouble(12));
            _standardFont = _defaultStandardFont;
            _standardFontSize = _defaultStandardFontSize;
            _lastFont = _standardFont;
        }

        private Font GetFont(float size)
        {
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            BaseFont basef = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            _lastFont= new Font(basef, size);
            return _lastFont;
        }

        private void SetFont(XmlNode node)
        {
            FontFactory.Register(node.Attributes.GetNamedItem("font").Value.ToString(), "EYInterstate-Light");
            bf = BaseFont.CreateFont(node.Attributes.GetNamedItem("font").Value.ToString(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            _defaultStandardFontSize = (float)Convert.ToDouble(node.Attributes.GetNamedItem("size").Value);
            _defaultStandardFont = new Font(bf, (float)Convert.ToDouble(node.Attributes.GetNamedItem("size").Value));
            _standardFont = _defaultStandardFont;
            _lastFont = _standardFont;
        }

        private void SetRootAttributes(XmlElement RootNode)
        {
            Properties rootAttribs = GetAttributes(RootNode);

            if (rootAttribs.ContainsKey("font"))
                bf = FontFactory.GetFont(rootAttribs).BaseFont;

            if (rootAttribs.ContainsKey("standalone"))
                _standalone = (rootAttribs["standalone"] == "true");

            if (rootAttribs.ContainsKey("draft"))
                _draft = (rootAttribs["draft"] == "true");

            if (rootAttribs.ContainsKey("title"))
                _document.AddTitle(rootAttribs["title"]);
            if (rootAttribs.ContainsKey("author"))
                _document.AddAuthor(rootAttribs["author"]);
            if (rootAttribs.ContainsKey("subject"))
                _document.AddSubject(rootAttribs["subject"]);
            if (rootAttribs.ContainsKey("creator"))
                _document.AddCreator(rootAttribs["creator"]);
            if (rootAttribs.ContainsKey("keywords"))
                _document.AddKeywords(rootAttribs["keywords"]);
        }

        private void SetPageMargins(XmlNode node)
        {
            float[] margins = new float[4];
            _defaultMargins.CopyTo(margins, 0);
            if (NodeHasAttribute(node, "left"))
                margins[0] = ConfigureDefaultMargins(node, "left");

            if (NodeHasAttribute(node, "right"))
                margins[1] = ConfigureDefaultMargins(node, "right");

            if (NodeHasAttribute(node, "top"))
                margins[2] = ConfigureDefaultMargins(node, "top");

            if (NodeHasAttribute(node, "bottom"))
                margins[3] = ConfigureDefaultMargins(node, "bottom");

            _document.SetMargins(margins[0], margins[1], margins[2], margins[3]);
        }

        #endregion

        #region "Process"

        private List<Cell> ProcessCells(XmlNode currentNode)
        {
            List<Cell> cells = new List<Cell>();

            foreach (XmlNode node in currentNode.SelectNodes("cell"))
            {
                Properties props = GetAttributes(node);
                Cell cell = ElementFactory.GetCell(props);
                if (props.ContainsKey("useBorderPadding"))
                    cell.UseBorderPadding = bool.Parse(props["useBorderPadding"]);

                if (props.ContainsKey("useAscender"))
                    cell.UseAscender = bool.Parse(props["useAscender"]);

                if (props.ContainsKey("useDescender"))
                    cell.UseDescender = bool.Parse(props["useDescender"]);

                cell = (Cell)ProcessData(node, cell);
                cells.Add(cell);
            }
            return cells;
        }

        private static Table ProcessRows(List<Cell> cells, Table table)
        {
            Cell cell;
            int columns = 0;
            foreach (Cell cellItem in cells)
                columns += cellItem.Colspan;

            if (table.Columns < columns)
                table.AddColumns(columns - table.Columns);

            String width;
            float[] cellWidths = new float[columns];
            bool[] cellNulls = new bool[columns];
            for (int i = 0; i < columns; i++)
            {
                cellWidths[i] = 0;
                cellNulls[i] = true;
            }
            float total = 0;
            int j = 0;
            foreach (Cell c in cells)
            {
                cell = c;
                width = cell.GetWidthAsString();
                if (cell.Width == 0)
                {
                    if (cell.Colspan == 1 && cellWidths[j] == 0)
                    {
                        try
                        {
                            cellWidths[j] = 100f / columns;
                            total += cellWidths[j];
                        }
                        catch
                        {
                            // empty on purpose
                        }
                    }
                    else if (cell.Colspan == 1)
                        cellNulls[j] = false;
                }
                else if (cell.Colspan == 1 && width.EndsWith("%"))
                {
                    try
                    {
                        cellWidths[j] = float.Parse(width.Substring(0, width.Length - 1), NumberFormatInfo.InvariantInfo);
                        total += cellWidths[j];
                    }
                    catch
                    {
                        // empty on purpose
                    }
                }
                j += cell.Colspan;
                table.AddCell(cell);
            }
            float[] widths = table.ProportionalWidths;
            if (widths.Length == columns)
            {
                float left = 0.0f;
                for (int i = 0; i < columns; i++)
                {
                    if (cellNulls[i] && widths[i] != 0)
                    {
                        left += widths[i];
                        cellWidths[i] = widths[i];
                    }
                }
                if (100.0 >= total)
                {
                    for (int i = 0; i < widths.Length; i++)
                        if (cellWidths[i] == 0 && widths[i] != 0)
                            cellWidths[i] = (widths[i] / left) * (100.0f - total);
                }
                table.Widths = cellWidths;
            }
            return table;
        }

        private void ProcessData(XmlNode currentNode)
        {
            foreach (XmlNode node in currentNode.ChildNodes)
            {
                ProcessData(node);
            }
        }

        private void ProcessItem(XmlNode node)
        {
            switch (node.Name.ToUpper())
            {
                case "BLOCK":
                    ProcessContentBlock(writer.DirectContent, node, writer.PageSize);
                    break;
                case "CHAPTER":
                    Chapter chapter;
                    if (NodeHasAttribute(node, "nr"))
                    {
                        if (NodeHasAttribute(node, "usecustomtitlepage"))
                        {
                            if (NodeHasAttribute(node, "title"))
                            {
                                if (node.Attributes["title"].Value == "$EXCLUDE_INDEX")
                                {
                                    chapter = new Chapter(minusChap);
                                    minusChap--;
                                }
                                else
                                {
                                    chapter = new Chapter(node.Attributes["title"].Value, int.Parse(node.Attributes["nr"].Value));
                                    chapter.TriggerNewPage = true;
                                }
                            }
                            else
                            {
                                chapter = new Chapter(int.Parse(node.Attributes["nr"].Value));
                                chapter.TriggerNewPage = true;
                            }
                            if (chapter != null && bool.Parse(node.Attributes["usecustomtitlepage"].Value))
                            {
                                chapter.UseCustomTitlePage = bool.Parse(node.Attributes["usecustomtitlepage"].Value);
                                chapter.TriggerNewPage = true;
                            }
                        }
                        else
                        {
                            if (NodeHasAttribute(node, "title"))
                                chapter = new Chapter(node.Attributes["title"].Value, int.Parse(node.Attributes["nr"].Value));
                            else
                            {
                                chapter = new Chapter(int.Parse(node.Attributes["nr"].Value));
                                chapter.TriggerNewPage = true;
                            }
                        }

                        if (NodeHasAttribute(node, "bookmarktitle"))
                            chapter.BookmarkTitle = node.Attributes["bookmarktitle"].Value;

                        chapter = (Chapter)ProcessData(node, chapter);
                        if (NodeHasAttribute(node, "depth"))
                            chapter.NumberDepth = int.Parse(node.Attributes["depth"].Value);

                        _document.Add(chapter);
                    }
                    else
                        throw new BadElementException("Chapter element must have number attribute");
                    break;
                case "COVERPAGE":
                    Chapter coverpage = new Chapter("$COVERPAGE$", -1);
                    coverpage.UseCustomTitlePage = true;
                    coverpage.BookmarkTitle = "$COVERPAGE$";

                    coverpage = (Chapter)ProcessData(node, coverpage);
                    _document.Add(coverpage);
                    if (!_standalone)
                    {
                        _document.Add(new Chunk().SetNewPage());
                        Chunk nph = new Chunk(" ");
                        nph.SetGenericTag("empty");
                        _document.Add(nph);
                        _document.Add(new Phrase(" "));
                    }
                    break;
                case "INDEXPAGE":
                    Chapter indexpage = new Chapter("$INDEXPAGE$", 0);
                    indexpage.UseCustomTitlePage = true;
                    indexpage.BookmarkTitle = "$INDEXPAGE$";

                    indexpage.Add(new Paragraph(" "));
                    _document.Add(indexpage);
                    break;
                case "TITLEPAGE":
                    Chunk chk = new Chunk("");
                    chk.SetGenericTag("TITLEPAGE::empty");

                    _document.Add(chk);
                    ProcessData(node);
                    break;
                case "TABLE":
                    ProcessDataTable(node, null, false);
                    break;
                case "PTABLE":
                    PdfPTable ptable = ProcessDataPTablePart1(node, 0);
                    _document.Add(ptable);
                    break;

                case "PARAGRAPH":
                    Paragraph paragraph = new Paragraph();
                    
                    paragraph.Font = GetFont(node);
                    if (NodeHasAttribute(node, "lineheight"))
                        paragraph.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, NumberFormatInfo.InvariantInfo) * paragraph.Font.Size;
                    if (NodeHasAttribute(node, "align"))
                        paragraph.Alignment = GetAlignment(node.Attributes.GetNamedItem("align").Value, NodeHasAttribute(node, "verticalalign") ? node.Attributes.GetNamedItem("verticalalign").Value : null);

                    paragraph = (Paragraph)ProcessData(node, paragraph);
                    paragraph.Font = GetFont(node);
                    _document.Add(paragraph);

                    break;
                case "PHRASE":
                    Phrase phrase = null;
                    if (node.HasChildNodes)
                    {
                        if (node.FirstChild.NodeType == XmlNodeType.Text || node.FirstChild.NodeType == XmlNodeType.CDATA)
                        {
                            phrase = new Phrase(node.FirstChild.InnerText, GetFont(node));
                            if (NodeHasAttribute(node, "lineheight"))
                                phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;

                        }
                        else
                        {
                            if (node.FirstChild.Name.ToUpper() == "PHRASE")
                            {
                                phrase = new Phrase();
                                phrase.Font = GetFont(node);
                                if (NodeHasAttribute(node, "lineheight"))
                                    phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;

                                phrase = (Phrase)ProcessData(node, phrase);
                            }
                        }
                    }
                    else
                    {
                        phrase = new Phrase();
                        phrase.Font = GetFont(node);
                        if (NodeHasAttribute(node, "lineheight"))
                            phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                    }

                    _document.Add(phrase);
                    break;
                case "NEWLINE":
                    Chunk chkNewLine = Chunk.NEWLINE;
                    chkNewLine.Font = _standardFont;
                    _document.Add(chkNewLine);

                    break;
                case "NEWPAGE":
                    _document.NewPage();
                    break;
                case "IMAGE":
                    break;
                case "TEMPLATE":
                    break;
            }
        }
        /*
        private int GetAlignment(string halign)
        {
            return GetAlignment(halign, null);
        }
        */
        private int GetAlignment(string halign, string valign)
        {
            int align = 0;
            switch (halign.ToLower())
            {
                case "left":
                    align= Element.ALIGN_LEFT;
                    break;
                case "right":
                    align= Element.ALIGN_RIGHT;
                    break;
                case "center":
                    align = Element.ALIGN_CENTER;
                    break;
                case "justified":
                    align = Element.ALIGN_JUSTIFIED;
                    break;
                case "justified_all":
                    align = Element.ALIGN_JUSTIFIED_ALL;
                    break;
            }
            if (!string.IsNullOrEmpty(valign))
            {
                switch (valign.ToLower())
                {
                    case "top":
                        align = align | Element.ALIGN_TOP;
                        break;
                    case "bottom":
                        align = align | Element.ALIGN_BOTTOM;
                        break;
                }
            }
            return align;
        }
        

        private IElement ProcessData(XmlNode currentNode, IElement parentObject)
        {
            foreach (XmlNode node in currentNode.ChildNodes)
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (node.Name.ToUpper())
                        {
                            case "BLOCK":
                                ProcessContentBlock(writer.DirectContent, node, writer.PageSize);
                                break;
                            case "TITLEPAGE":
                                Chunk tchk = new Chunk("");
                                if (NodeHasAttribute(node, "headerfooterid"))
                                    tchk.SetGenericTag("TITLEPAGE::" + node.Attributes["headerfooterid"].Value);
                                else
                                    tchk.SetGenericTag("TITLEPAGE::empty");

                                _document.Add(tchk);
                                ProcessData(node);
                                break;
                            case "SECTION":
                                if (parentObject.GetType().Name != "Chapter")
                                    throw new InvalidOperationException("A section can only be loaded beneath a chapter");

                                try
                                {
                                    SetPageMargins(node);
                                    Chapter sectionChapter = (Chapter)parentObject;

                                    if (NodeHasAttribute(node, "title"))
                                    {
                                        Section section;

                                        if (NodeHasAttribute(node, "depth"))
                                            section = sectionChapter.AddSection(node.Attributes["title"].Value, int.Parse(node.Attributes["depth"].Value));
                                        else
                                            section = sectionChapter.AddSection(node.Attributes["title"].Value);

                                        if (NodeHasAttribute(node, "triggernewpage"))
                                            section.TriggerNewPage = bool.Parse(node.Attributes["triggernewpage"].Value);

                                        section = (Section)ProcessData(node, section);
                                        Chunk sectionEnd = new Chunk(" ");
                                        sectionEnd.SetGenericTag("SECTION::END");
                                        sectionChapter.Add(sectionEnd);
                                    }
                                }
                                catch
                                {
                                }
                                break;
                            case "TABLE":
                                ProcessDataTable(node, parentObject, true);
                                break;
                            case "ROW":
                                Table rowTable = (Table)parentObject;
                                rowTable = ProcessRows(ProcessCells(node), rowTable);
                                parentObject = rowTable;

                                break;
                            case "PTABLE":
                                PdfPTable ptable = null;
                                if (parentObject.GetType().Equals(typeof(PdfPCell)))
                                {
                                    ProcessDataPTablePart1(node, 0);
                                }
                                else
                                {
                                    ptable = ProcessDataPTablePart1(node, 0f);
                                }

                                if (parentObject == null)
                                {
                                    parentObject = ptable;
                                    return parentObject;
                                }

                                try
                                {
                                    ITextElementArray parent = (ITextElementArray)parentObject;
                                    parent.Add(ptable);
                                    parentObject = parent;
                                }
                                catch
                                {
                                    parentObject = ptable;
                                    return parentObject;
                                }
                                break;
                            case "PCELL":
                                if (parentObject.GetType().Name != "PdfPTable")
                                {
                                    throw new InvalidOperationException("PCELL can only be beneath a PTABLE tag");
                                }
                                PdfPCell cell;
                                XmlNodeList phrases = node.SelectNodes("./phrase");


                                // get cell width
                                PdfPTable pCellParentTable = (PdfPTable)parentObject;
                                float cellWidth = 0;
                                if (pCellParentTable.NumberOfColumns > 1)
                                {
                                    XmlNodeList preceding = node.SelectNodes("./preceding-sibling::*");
                                    int widthIdx = 0;
                                    if (pCellParentTable.NumberOfColumns < (preceding.Count + 1))
                                    {
                                        widthIdx = ((preceding.Count + 1) % pCellParentTable.NumberOfColumns);
                                        widthIdx = pCellParentTable.NumberOfColumns - widthIdx - 1;// zero bound, hence -1
                                    }
                                    else
                                    {
                                        widthIdx = preceding.Count; //zero bound
                                    }
                                    cellWidth = pCellParentTable.AbsoluteWidths[widthIdx];

                                }
                                else
                                {
                                    cellWidth = pCellParentTable.AbsoluteWidths[0];
                                }

                                if (phrases.Count > 0)
                                {
                                    if (phrases.Count == 1)
                                    {
                                        Phrase ph = (Phrase)ProcessData(node, null);
                                        if (NodeHasAttribute(node, "lineheight"))
                                            ph.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, NumberFormatInfo.InvariantInfo) * ph.Font.Size;

                                        cell = new PdfPCell(ph);
                                    }
                                    else
                                        cell = new PdfPCell();
                                }
                                else if (node.FirstChild.Name.ToUpper() == "PTABLE")
                                {
                                    PdfPTable innerTable = ProcessDataPTablePart1(node.FirstChild, cellWidth);
                                    cell = new PdfPCell(innerTable);
                                }
                                else
                                {
                                    Phrase ph = new Phrase(node.InnerText);
                                    cell = new PdfPCell(ph);
                                }

                                cell.Border = 0;
                                //cell.Width = cellWidth;
                                if (NodeHasAttribute(node, "align"))
                                    cell.HorizontalAlignment = GetAlignment(node.Attributes["align"].Value);

                                if (NodeHasAttribute(node, "valign"))
                                    cell.VerticalAlignment = GetAlignment(node.Attributes["valign"].Value);

                                if (NodeHasAttribute(node, "border"))
                                    cell.Border = int.Parse(node.Attributes["border"].Value);

                                if (NodeHasAttribute(node, "padding"))
                                    cell.Padding = float.Parse(node.Attributes["padding"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "paddingleft"))
                                    cell.PaddingLeft = float.Parse(node.Attributes["paddingleft"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "paddingright"))
                                    cell.PaddingRight = float.Parse(node.Attributes["paddingright"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "paddingtop"))
                                    cell.PaddingTop = float.Parse(node.Attributes["paddingtop"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "paddingbottom"))
                                    cell.PaddingBottom = float.Parse(node.Attributes["paddingbottom"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "indent"))
                                    cell.Indent = float.Parse(node.Attributes["indent"].Value, NumberFormatInfo.InvariantInfo);

                                if (parentObject != null)
                                {
                                    try
                                    {
                                        PdfPTable cellParent = (PdfPTable)parentObject;

                                        if (NodeHasAttribute(currentNode, "fullpage"))
                                        {
                                            if (bool.Parse(currentNode.Attributes["fullpage"].Value) &&
                                                cellParent.NumberOfColumns == 1)
                                            {

                                                cell.FixedHeight = _document.PageSize.Height - _document.TopMargin -
                                                                   _document.BottomMargin;
                                                cellParent.SetTotalWidth(new float[]
                                                                             {
                                                                                 _document.PageSize.Width - _document.LeftMargin -
                                                                                 _document.BottomMargin
                                                                             });
                                                cellParent.LockedWidth = true;
                                            }
                                        }
                                        else
                                        {
                                            if (NodeHasAttribute(currentNode, "contentwidth"))
                                            {
                                                if (bool.Parse(currentNode.Attributes["contentwidth"].Value) &&
                                                    cellParent.NumberOfColumns == 1)
                                                {
                                                    float maxWidth = 0;

                                                    for (int i = 0; i < cell.Phrase.Chunks.Count; i++)
                                                    {
                                                        Chunk chk = (Chunk)cell.Phrase.Chunks[i];
                                                        chk.Font = _lastFont;
                                                        if ((chk.GetWidthPoint() + cell.PaddingLeft + cell.PaddingRight) > maxWidth)
                                                            maxWidth = (chk.GetWidthPoint() + cell.PaddingLeft + cell.PaddingRight);

                                                    }
                                                    cellParent.SetTotalWidth(new float[] { maxWidth + 20 });
                                                    cellParent.LockedWidth = true;
                                                }
                                            }
                                        }

                                        if (node.FirstChild.Name.ToUpper() == "PTABLE")
                                        {

                                            PdfPTable ptableChild = ProcessDataPTablePart1(node.FirstChild, cellWidth);
                                            ptableChild.DefaultCell.Border = 0;
                                            ptableChild.DefaultCell.BorderWidth = 0f;
                                            cellParent.AddCell(new PdfPCell(ptableChild, ptableChild.DefaultCell));
                                        }
                                        else
                                            cellParent.AddCell(cell);
                                    }
                                    catch
                                    {

                                    }
                                }
                                else
                                {
                                    return cell;
                                }

                                // return cell;
                                break;

                            case "INDEXCELL":
                                if (parentObject.GetType().Name != "PdfPTable")
                                {
                                    throw new InvalidOperationException("PCELL can only be beneath a PTABLE tag");
                                }

                                // get cell width
                                PdfPTable parentTable = (PdfPTable)parentObject;
                                float icellWidth = 0;
                                if (parentTable.NumberOfColumns > 1)
                                {
                                    XmlNodeList preceding = node.SelectNodes("./preceding-sibling::*");
                                    int widthIdx = 0;
                                    if (parentTable.NumberOfColumns < (preceding.Count + 1))
                                    {
                                        widthIdx = ((preceding.Count + 1) % parentTable.NumberOfColumns);
                                        widthIdx = parentTable.NumberOfColumns - widthIdx - 1;// zero bound, hence -1
                                    }
                                    else
                                    {
                                        widthIdx = preceding.Count; //zero bound
                                    }
                                    icellWidth = parentTable.AbsoluteWidths[widthIdx];

                                }
                                else
                                {
                                    icellWidth = parentTable.AbsoluteWidths[0];
                                }
                                //Get indent of cell
                                float indent = 0;
                                if (NodeHasAttribute(node, "indent"))
                                    indent = int.Parse(node.Attributes["indent"].Value);

                                float tablewidth = icellWidth;

                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.BorderWidth);
                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.BorderWidthLeft);
                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.BorderWidthRight);
                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.PaddingLeft);
                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.PaddingRight);
                                tablewidth = tablewidth - indent;
                                tablewidth = tablewidth - 7;

                                // temporary correction, review width calculation by IText upon processing to PDF
                                tablewidth += 40f;

                                Phrase indexPhrase = new Phrase(ProcessIndexLine(node.InnerText, tablewidth));
                                indexPhrase.Font.Size = _defaultStandardFontSize;
                                indexPhrase.Font = _defaultStandardFont;

                                PdfPCell indexCell = new PdfPCell(indexPhrase);
                                indexCell.Indent = indent;
                                indexCell.Border = 0;

                                if (parentObject != null)
                                {
                                    try
                                    {
                                        PdfPTable cellParent = (PdfPTable)parentObject;

                                        if (node.FirstChild.Name.ToUpper() == "PTABLE")
                                        {
                                            PdfPTable ptableChild = ProcessDataPTablePart1(node.FirstChild, icellWidth);
                                            ptableChild.DefaultCell.Border = 0;
                                            ptableChild.DefaultCell.BorderWidth = 0f;
                                            cellParent.AddCell(new PdfPCell(ptableChild, ptableChild.DefaultCell));
                                        }
                                        else
                                            cellParent.AddCell(indexCell);
                                    }
                                    catch
                                    {

                                    }
                                }
                                else
                                {
                                    return indexCell;
                                }
                                break;

                            case "LIST":
                                List list;
                                Properties listAttribs = GetAttributes(node);
                                bool orderedList = iTextSharp.text.Utilities.CheckTrueOrFalse(listAttribs, "ordered");
                                orderedList = orderedList || iTextSharp.text.Utilities.CheckTrueOrFalse(listAttribs, "numbered");
                                orderedList = orderedList || iTextSharp.text.Utilities.CheckTrueOrFalse(listAttribs, "lettered");
                                float symbolIndent = 10;
                                if (listAttribs.ContainsKey("symbolindent"))
                                {
                                    symbolIndent = float.Parse(listAttribs["symbolindent"], NumberFormatInfo.InvariantInfo);
                                }
                                if (parentObject.GetType() == typeof(List))
                                {
                                    List parentList = (List)parentObject;
                                    symbolIndent += parentList.SymbolIndent;
                                }

                                list = orderedList
                                           ? new List(List.ORDERED, symbolIndent)
                                           : new List(List.UNORDERED, symbolIndent);
                                list = ElementFactory.SetList(list, listAttribs);
                                if (list.Numbered || list.Lettered) list.Autoindent = true;
                                if (!orderedList)
                                {
                                    list.ListSymbol = new Chunk("\u2022", FontFactory.GetFont(FontFactory.HELVETICA, 16));
                                }
                                else
                                {
                                    list.ListSymbol = new Chunk("-", GetFont(_standardFontSize));
                                    if (node.HasChildNodes)
                                    {
                                        XmlNode cn = node.SelectSingleNode("*[1]");
                                        if (cn != null)
                                        {
                                            list.ListSymbol = new Chunk("-", GetFont(cn));
                                            if (cn.HasChildNodes)
                                            {
                                                cn = node.SelectSingleNode("phrase|paragraph[1]");
                                                if (cn != null)
                                                {
                                                    list.ListSymbol = new Chunk("-", GetFont(cn));
                                                }
                                            }
                                        }
                                    }
                                }
                                list = (List)ProcessData(node, list);
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        parent.Add(list);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = list;
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    return list;
                                }
                                break;
                            case "IMAGE":
                                if (NodeHasAttribute(node, "id"))
                                {
                                    Image img = GetImage(node.Attributes.GetNamedItem("id").Value);
                                    if (NodeHasAttribute(node, "center"))
                                    {
                                        img.Alignment = Element.ALIGN_CENTER;
                                    }
                                    if (NodeHasAttribute(node, "indentationleft"))
                                    {
                                        img.IndentationLeft = float.Parse(node.Attributes.GetNamedItem("indentationleft").Value);
                                    }
                                    if (parentObject != null)
                                    {
                                        if (parentObject.Type == Element.CELL)
                                        {
                                           // img.ScaleAbsoluteHeight(_lastFont.Size - 1f);
                                            img.ScalePercent(50f);
                                        }
                                        try
                                        {
                                            ITextElementArray parent = (ITextElementArray)parentObject;
                                            parent.Add(img);
                                            parentObject = parent;
                                        }
                                        catch
                                        {
                                            parentObject = img;
                                            return parentObject;
                                        }
                                    }
                                    else
                                    {
                                        return img;
                                    }
                                }
                                break;
                            case "LISTITEM":
                                Paragraph listPara = ElementFactory.GetParagraph(GetAttributes(node));
                                if (NodeHasAttribute(node, "lineheight"))
                                    listPara.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, NumberFormatInfo.InvariantInfo) * listPara.Font.Size;
                                if(NodeHasAttribute(node,"align"))
                                    listPara.Alignment = GetAlignment(node.Attributes.GetNamedItem("align").Value, NodeHasAttribute(node, "verticalalign") ? node.Attributes.GetNamedItem("verticalalign").Value : null);

                                listPara = (Paragraph)ProcessData(node, listPara);
                                List itemParentList = (List)parentObject;
                                itemParentList.Add(new ListItem(listPara));
                                parentObject = itemParentList;
                                break;
                            case "PARAGRAPH":
                                Paragraph paragraph = new Paragraph();

                                paragraph.Font = GetFont(node);
                                if (NodeHasAttribute(node, "lineheight"))
                                    paragraph.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, NumberFormatInfo.InvariantInfo) * paragraph.Font.Size;
                                if (NodeHasAttribute(node, "align"))
                                    paragraph.Alignment = GetAlignment(node.Attributes.GetNamedItem("align").Value, NodeHasAttribute(node, "verticalalign") ? node.Attributes.GetNamedItem("verticalalign").Value : null);
                                if (NodeHasAttribute(node, "indentationleft"))
                                paragraph.IndentationLeft = float.Parse(node.Attributes.GetNamedItem("indentationleft").Value, NumberFormatInfo.InvariantInfo);

                                paragraph = (Paragraph)ProcessData(node, paragraph);
                                paragraph.Font = GetFont(node);
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        parent.Add(paragraph);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = paragraph;
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    return paragraph;
                                }
                                break;

                            case "CHUNK":
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        Chunk chunk = new Chunk(node.InnerText, FontFactory.GetFont(GetAttributes(node)));
                                        parent.Add(chunk);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = new Chunk(node.InnerText, FontFactory.GetFont(GetAttributes(node)));
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    parentObject = new Chunk(node.InnerText, FontFactory.GetFont(GetAttributes(node)));
                                    return parentObject;
                                }
                                break;
                            case "PHRASE":
                                Phrase phrase = null;
                                if (node.HasChildNodes)
                                {
                                    if (node.FirstChild.NodeType == XmlNodeType.Text || node.FirstChild.NodeType == XmlNodeType.CDATA)
                                    {
                                        phrase = new Phrase();
                                        phrase.Font = GetFont(node);
                                        if (NodeHasAttribute(node, "lineheight"))
                                            phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                                        phrase = (Phrase)ProcessData(node, phrase);

                                        //phrase = new Phrase(ReplaceVariableWithValue(node.FirstChild.InnerText), GetFont(node));
                                        //if (NodeHasAttribute(node, "lineheight"))
                                        //    phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                                    }
                                    else
                                    {
                                        if (node.FirstChild.Name.ToUpper() == "PHRASE")
                                        {
                                            phrase = new Phrase();
                                            phrase.Font = GetFont(node);
                                            if (NodeHasAttribute(node, "lineheight"))
                                                phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;

                                            phrase = (Phrase)ProcessData(node, phrase);
                                        }
                                        else
                                        {
                                            phrase = new Phrase();
                                            phrase.Font = GetFont(node);
                                            if (NodeHasAttribute(node, "lineheight"))
                                                phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                                            phrase = (Phrase)ProcessData(node, phrase);
                                        }
                                    }
                                }
                                else
                                {
                                    phrase = new Phrase();
                                    phrase.Font = GetFont(node);
                                    if (NodeHasAttribute(node, "lineheight"))
                                        phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                                }

                                if (parentObject == null)
                                {
                                    parentObject = phrase;
                                    return parentObject;
                                }

                                try
                                {
                                    ITextElementArray parent = (ITextElementArray)parentObject;
                                    parent.Add(phrase);
                                    parentObject = parent;
                                }
                                catch
                                {
                                    parentObject = phrase;
                                    return parentObject;
                                }
                                break;
                            case "NEWLINE":
                                Chunk chkNewLine = Chunk.NEWLINE;
                                chkNewLine.Font = _standardFont;
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        parent.Add(chkNewLine);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = chkNewLine;
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    parentObject = chkNewLine;
                                    return parentObject;
                                }
                                break;
                            case "IMPORT":

                                Properties attributes = GetAttributes(node);

                                if (parentObject != null)
                                {
                                    try
                                    {
                                        Section parent = (Section)parentObject;
                                        bool first = true;
                                        for (int i = int.Parse(attributes["fromPage"]);
                                             i <= int.Parse(attributes["toPage"]);
                                             i++)
                                        {
                                            Chunk importChunk = new Chunk(" ");
                                            importChunk.SetGenericTag(GetImportGenericTag(attributes, i, first));
                                            if (node.ParentNode.PreviousSibling != null && node.ParentNode.PreviousSibling.Name.ToLower() == "titlepage")
                                            {
                                                if (!first) parent.Add(new Chunk().SetNewPage());
                                            }
                                            else
                                                parent.Add(new Chunk().SetNewPage());

                                            first = false;
                                            parent.Add(importChunk);
                                        }

                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    return parentObject;
                                }
                                break;
                            case "SPACE":
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        Chunk chunk = new Chunk(" ",_lastFont);
                                        parent.Add(chunk);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = new Chunk(" ", _lastFont);
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    parentObject = new Chunk(" ", _lastFont);
                                    return parentObject;
                                }
                                break;
                            case "NEWPAGE":
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        parent.Add(new Chunk().SetNewPage());
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = new Chunk().SetNewPage();
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    parentObject = new Chunk().SetNewPage();
                                    return parentObject;
                                }

                                break;
                        }
                        break;
                    case XmlNodeType.Text:
                    case XmlNodeType.CDATA:
                        /*
                         * GOAL:
                         * Replace all known variables in a string with their value,
                         * variables are within rectangular brackets "[XXX]"
                         * for example, [PAGE_NUMBER] has to be replaced by the current page number.
                         */

                        Regex re = new Regex("\\[([A-Z]|_){1,}\\]");//("\[([A-Z]|_){1,}\]");
                        MatchCollection mc = re.Matches(node.InnerText);

                        foreach (Match m in mc)
                        {
                            if (m.Value == "[PAGE_NUMBER]")
                            {
                                string tst = string.Empty;
                                //m.Value = page_number.ToString();
                            }
                        }

                        if (parentObject != null)
                        {
                            try
                            {
                                ITextElementArray parent = (ITextElementArray)parentObject;
                                parent.Add(new Chunk(node.InnerText, _lastFont));
                                parentObject = parent;
                            }
                            catch
                            {
                                parentObject = new Chunk(node.InnerText, _lastFont);
                                return parentObject;
                            }
                        }
                        else
                            return new Chunk(node.InnerText);

                        break;
                }
            }
            return parentObject;
        }

        private float GetWidth(string text, Font font)
        {
            Chunk chk = new Chunk(text);
            chk.Font = font;
            return chk.GetWidthPoint();
        }

        private string ProcessIndexLine(string indexLine, float tableWidth)
        {
            //string[] parts = new string[3] { indexLine.Substring(0, indexLine.LastIndexOf(" ")), "", indexLine.Substring(indexLine.LastIndexOf(" ") + 1) };
            /*
            Chunk chunkTextPart = new Chunk(indexLine.Substring(0, indexLine.LastIndexOf(" ")));
            chunkTextPart.Font = _standardFont;
            chunkTextPart.Font.Size = _standardFontSize;
            float textPartWidth = chunkTextPart.GetWidthPoint();

            Chunk chunknumberPart = new Chunk(indexLine.Substring(indexLine.LastIndexOf(" ") + 1));
            chunknumberPart.Font = _standardFont;
            chunknumberPart.Font.Size = _standardFontSize;
            float numberPartWidth = chunknumberPart.GetWidthPoint();

            Chunk chunkPointPart = new Chunk("");
            chunkPointPart.Font = _standardFont;
            chunkPointPart.Font.Size = _standardFontSize;
             * */

            Font font = new Font(_standardFont.BaseFont, _standardFontSize);

            //while (GetWidth(string.Concat(parts), font) < (tableWidth - 4))
            while (GetWidth(indexLine, font) < (tableWidth - 4))
            //while(GetWidth(indexLine,font) < (tableWidth - 30))
            {
                indexLine = indexLine + ".";
                //parts[1] = parts[1] + ".";
            }

            // indexLine = string.Concat(parts);

            return indexLine;
        }

        private void ProcessContentBlock(PdfContentByte over, XmlNode block, Rectangle rect)
        {
            int align = Element.ALIGN_UNDEFINED;
            if (block.Attributes["align"] != null)
                align = GetAlignment(block.Attributes["align"].Value);

            float top = GetAttributeValue(block, "top");
            float left = GetAttributeValue(block, "left");

            if (left < 0)
                left += rect.Width;

            if (top > 0)
                top = rect.Height - top;
            else
                top = Math.Abs(top);

            BaseFont bf = _standardFont.BaseFont;

            foreach (XmlNode subNode in block.SelectNodes("(phrase|newline|image)"))
            {
                Properties attributes = GetAttributes(subNode);

                switch (subNode.Name.ToUpper())
                {
                    case "NEWLINE":
                        top -= 20;
                        break;
                    case "PHRASE":
                        string text = ReplaceVariableWithValue(subNode.InnerText);

                        Phrase phText = new Phrase(text, GetFont(subNode));

                        ColumnText.ShowTextAligned(over, align, phText, left, top, 0);

                        float padding = 2;
                        if (attributes.ContainsKey("padding"))
                            padding = float.Parse(attributes["padding"], NumberFormatInfo.InvariantInfo);

                        top -= (phText.Font.Size + padding);
                        break;
                    case "IMAGE":
                        Image img = ElementFactory.GetImage(attributes);
                        img.SetAbsolutePosition(left, top);
                        if (attributes.ContainsKey("scalepercent"))
                            img.ScalePercent(float.Parse(attributes["scalepercent"], NumberFormatInfo.InvariantInfo));

                        over.AddImage(img);
                        break;
                }
            }
        }

        /// <summary>
        /// Replace all known variables in a string with their value,
        /// variables are within rectangular brackets "[XXX]"
        /// for example, [PAGE_NUMBER] has to be replaced by the current page number.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private string ReplaceVariableWithValue(string text)
        {
            Regex re = new Regex(@"\[(?<Key>([A-Z]|_){1,})(\:\:){0,1}(?<Item>([A-Z]|_){1,}){0,1}\]", RegexOptions.IgnoreCase);
            /*
             * (\:\:){0,1}
             * ?<Item>(([A-Z]|_){1,})
             * \\[(([A-Z]|_){1,}\)\]
             */
            MatchCollection mc = re.Matches(text);

            foreach (Match m in mc)
            {
                if (m.Groups["Key"].Success)
                {
                    string type = "";
                    string item = m.Groups["Key"].Value;
                    if (m.Groups["Item"].Success)
                    {

                        type = m.Groups["Key"].Value;
                        item = m.Groups["Item"].Value;
                    }


                    switch (type.ToUpper())
                    {
                        case "CLIENT":
                            switch (item.ToUpper())
                            {
                                case "NAME":
                                    text = text.Replace(m.Value, Client.Name);
                                    break;
                                case "ADDRESS":
                                    text = text.Replace(m.Value, Client.Address);
                                    break;
                                case "ZIPCODE":
                                case "ZIP":
                                    text = text.Replace(m.Value, Client.ZipCode);
                                    break;
                                case "CITY":
                                    text = text.Replace(m.Value, Client.City);
                                    break;
                                case "COUNTRY":
                                    text = text.Replace(m.Value, Client.Country);
                                    break;
                            }
                            break;
                        case "FILE":
                            switch (item.ToUpper())
                            {
                                case "ENDDATE":
                                    text = text.Replace(m.Value, File.EndDate.ToString(DateTimeFormatInfo.CurrentInfo.ShortDatePattern));
                                    break;
                                case "STARTDATE":
                                    text = text.Replace(m.Value, File.StartDate.ToString(DateTimeFormatInfo.CurrentInfo.ShortDatePattern));
                                    break;
                            }
                            break;
                        case "CHAPTER":
                            ChapterDataContract cdc = (ChapterDataContract)_currentIndexItem;
                            switch (item.ToUpper())
                            {
                                case "TITLE":
                                    text = text.Replace(m.Value, cdc.Title);
                                    break;
                                case "PAGE_NUMBER":
                                    text = text.Replace(m.Value, page_number.ToString());
                                    break;
                                case "PAGE_TOTAL":
                                    text = text.Replace(m.Value, page_total.ToString());
                                    break;
                                case "HEADERFOOTER":
                                   // text = text.Replace(m.Value, cdc.HeaderFooter);
                                    break;
                            }
                            break;
                        default:
                            switch (item.ToUpper())
                            {
                                case "PAGE_NUMBER":
                                    text = text.Replace(m.Value, page_number.ToString());
                                    break;
                                case "PAGE_TOTAL":
                                    text = text.Replace(m.Value, page_total.ToString());
                                    break;
                            }
                            break;
                    }
                }
            }
            return text;
        }

        private void ProcessDataTable(XmlNode node, IElement parentObject, Boolean withParent)
        {
            Table table = ElementFactory.GetTable(GetAttributes(node));
            float[] widths = table.ProportionalWidths;
            for (int i = 0; i < widths.Length; i++)
                if (widths[i] == 0)
                    widths[i] = 100.0f / widths.Length;
            try
            {
                table.Widths = widths;
            }
            catch (BadElementException bee)
            {
                // this shouldn't happen
                throw new Exception("", bee);
            }
            finally
            {
                table = (Table)ProcessData(node, table);
                if (withParent)
                {
                    try
                    {
                        ITextElementArray parento = (ITextElementArray)parentObject;

                        //if (parento.Type == Element.CELL)
                        //{
                        //    parentObject = 
                        //}
                        //else
                        //{

                            parento.Add(table);
                            parentObject = parento;
                        //}
                    }
                    catch
                    {
                    }
                }
                else
                    _document.Add(table);
            }
        }


        private PdfPTable ProcessDataPTablePart1(XmlNode node, float width)
        {

            int columnCount = int.Parse(node.Attributes.GetNamedItem("columns").Value);
            PdfPTable ptable = new PdfPTable(columnCount);
            if (width > 0) ptable.TotalWidth = width;
            if (node.Attributes.GetNamedItem("width") != null) ptable.TotalWidth = float.Parse(node.Attributes.GetNamedItem("width").Value);
            if (ptable.TotalWidth == 0) ptable.TotalWidth = (_document.PageSize.Width - _document.LeftMargin - _document.BottomMargin) * (ptable.WidthPercentage / 100);
            if (NodeHasAttribute(node, "columnWidths"))
            {
                List<float> cfWidths = new List<float>();
                foreach (string wd in node.Attributes.GetNamedItem("columnWidths").Value.Split(new char[] { ',' }))
                {
                    cfWidths.Add(float.Parse(wd));
                }
                ptable.SetWidths(cfWidths.ToArray());
            }
            ptable = (PdfPTable)ProcessData(node, ptable);
            return ptable;
        }

        #endregion

        private Chapter GetStatement(StatementsDataContract sdc)
        {
            string path = Path.Combine(Config.SchemaTemplates, string.Format("{0}.xml", sdc.Layout));
            XDocument xdoc = XDocument.Load(path);
            return new Chapter(-1);
            //return new Statements.Renderer().Render(xdoc, sdc.Detailed, File.Accounts, sdc.Culture);
        }

        public PDFDataContract CreateStatement(StatementsDataContract sdc)
        {

            XDocument xdoc = XDocument.Parse(sdc.iTextTemplate);
            PDFDataContract pdc = new PDFDataContract
            {
                Id = Guid.NewGuid(),
                HeaderConfig = sdc.HeaderConfig,
                FooterConfig = sdc.FooterConfig,
                ShowInIndex = sdc.ShowInIndex,
                Title = sdc.Title,
                FromPage = 1
            };

            pdc.PDFLocation = Path.Combine(Config.PdfWorkingFolder, string.Format("{0}.pdf", pdc.Id.ToString()));

            
            _document = new iTextSharp.text.Document();


            writer = PdfWriter.GetInstance(_document, new FileStream(pdc.PDFLocation, FileMode.Create));

            _document.SetPageSize(PageSize.A4);
            SetFont();
            SetDefaultMargins();

            _document.Open();


            //  int percentageToAdd = 59 / xmlNodeList.Count;


            _document.Add(GetStatement(sdc));

            _document.Close();

            PdfReader reader = new PdfReader(pdc.PDFLocation);
            pdc.Pages = reader.NumberOfPages;
            pdc.FromPage = 1;
            pdc.ToPage = pdc.Pages;
            reader.Close();
            _document = null;
            reader = null;
            return pdc;
        }

        private Image GetImage(string name)
        {
            string path = Path.Combine(_imageFolder, string.Format("{0}.png", name));
            Stream sr = new FileStream(path, FileMode.Open, FileAccess.Read);
            System.Drawing.Image img = System.Drawing.Image.FromStream(sr);
            return Image.GetInstance(img, System.Drawing.Imaging.ImageFormat.Png);
        }

        #region IDisposable Members

        public void Dispose()
        {
            
            pdfFiles = null;


            if (writer != null) { try { writer.Close(); } catch { };}
            _standardFont=null;
            bf = null;
            _document =null;
            _pageSize = null;
            _defaultStandardFont = null;
            _pageSizeLandscape=null;
            _pageSizePortret=null;
            _emptyPages = null;
            File=null;
            Client=null;
            IndexDataContract =null;
            _currentIndexItem=null;
             
        }

        #endregion
    }


}

