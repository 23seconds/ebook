﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using System.Configuration;
using System.ServiceModel;
using iTextSharp.text;
using System.Xml;
using System.Xml.Linq;
using iTextSharp.text.pdf;
using System.Collections;
using System.Globalization;
using System.util;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.Core.IText.Render
{
   

    public class Base64BundleRenderer : ITextRenderer
    {
        private List<IndexItemBaseDataContract> _items;



        public Base64BundleRenderer(List<IndexItemBaseDataContract> items)
            :base(null,null)
        {
            _items=items;
           
        }

        public void Combine()
        {
            foreach (IndexItemBaseDataContract item in _items)
            {
                if (item.GetType().Name == "PDFDataContract")
                {
                    PDFDataContract pdc = (PDFDataContract)item;
                    if (!System.IO.File.Exists(pdc.PDFLocation))
                    {
                        throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -1, Message = string.Format("Pdf file not found: {0} {1}", pdc.Title, pdc.PDFLocation) });
                    }
                    PdfReader reader = new PdfReader(pdc.PDFLocation);
                    reader.ConsolidateNamedDestinations();

                    int n = reader.NumberOfPages;

                    PdfImportedPage page;
                    for (int i = 1; i <= n; i++)
                    {
                        page = writer.GetImportedPage(reader, i);
                        //page.re

                        Rectangle rec = reader.GetPageSizeWithRotation(i);
                        Rectangle newRec = new Rectangle(rec.Width, rec.Height);

                        bool rotated = newRec.Width > rec.Height;

                        _document.SetPageSize(newRec);

                        _document.NewPage();
                        // _document.Add(new Phrase(" PDF:: " + pdc.Title));
                        _document.Add(new Chunk(" "));
                        

                        int rotation = reader.GetPageRotation(i);
                        if (rotation == 90)
                        {
                            writer.DirectContentUnder.AddTemplate(page, 0, -1.0F, 1.0F, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                        }
                        else if (rotation == 270 && rotated)
                        {
                            writer.DirectContentUnder.AddTemplate(page, 0, 1.0F, -1.0F, 0, reader.GetPageSizeWithRotation(i).Width + 60, -30);
                        }
                        else
                        {
                            writer.DirectContentUnder.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0);
                        }


                    }

                    reader.Close();
                }
            }
        }

        public MemoryStream _ms;

        public string CreateBase64()
        {
            _ms = new MemoryStream();
            _document = new iTextSharp.text.Document();
            writer = PdfWriter.GetInstance(_document, _ms);

            if (_rootNode != null)
            {
                SetRootAttributes(_rootNode);
                SetPageSize(_rootNode);
                SetFont(_rootNode);
                SetDefaultMargins(_rootNode);

            }
            else
            {
                _document.SetPageSize(PageSize.A4);
                SetFont();
                SetDefaultMargins();
            }

            _document.Open();
            Combine();
            _document.Close();

            byte[] bytes = _ms.ToArray();

            // Convert byte[] to Base64 String
            string base64String = Convert.ToBase64String(bytes);
            return base64String;
        }

        public override void HandleDocument()
        {

        }

        public override PDFDataContract CreatePdf()
        {
            return null;
        }

       
        public override void CloseDocument()
        {
          
        }

        

       
    }

   
}
