﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using iTextSharp.text.pdf;
using System.Globalization;
using iTextSharp.text;
using System.util;
using iTextSharp.text.factories;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace EY.com.eBook.Core.IText.Render
{
    public class TextRenderer : ITextRenderer
    {

        private BundleTextDataContract _btdc;

        public TextRenderer(BundleTextDataContract btdc)
            : base(null, btdc)
        {
            _btdc = btdc;
           
        }

        public override void HandleDocument()
        {
            
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(bf, 8f);


            Chapter chapter = new Chapter(0);
            chapter.Add(new Phrase(_btdc.Text,font));
           
            _document.Add(chapter);

           
        }

  



    

        #region IDisposable Members

        public void Dispose()
        {
            
          

            base.Dispose();
        }

        #endregion
    }
}
