﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using iTextSharp.text.pdf;
using System.Globalization;
using iTextSharp.text;
using System.util;
using iTextSharp.text.factories;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace EY.com.eBook.Core.IText.Render
{
    public class XbrlFrontPageRenderer : ITextRenderer
    {
        
        private Table _table;
        private Font _font;
        private BaseFont bf;
        private CultureInfo _cultureInfo;
        private XDocument _statementXDoc;
        private XbrlFrontPageItemDataContract _xdc;
        private LegalTypeDataContract _legatType;
        private string _culture;

        public XbrlFrontPageRenderer(XbrlFrontPageItemDataContract xfdc, LegalTypeDataContract legalType, string culture)
            : base(null, xfdc)
        {
            _xdc = xfdc;
            _legatType = legalType;
            _cultureInfo = CultureInfo.CreateSpecificCulture(culture);
            _culture = culture;
           
        }

        public override void HandleDocument()
        {
            
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            _font = new Font(bf, 8f);


            Chapter chapter = new Chapter(0);
            _table = new Table(2);
            _table.TableFitsPage = true;
            _table.Convert2pdfptable = true;
            _table.CellsFitPage = true;
            _table.Width = 100f;
            _table.Alignment = Element.ALIGN_CENTER;
            _table.Border = 0;
            _table.Cellpadding = 1f;
            _table.Cellspacing = 0f;
            _table.SetWidths(new int[] {50,50 });

            // HEADING
            Cell cell = new Cell();
            cell.Colspan = 2;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.BorderWidth = 0;

            Font tFont1 = new Font(bf, 10);
            tFont1.SetStyle(Font.BOLD | Font.UNDERLINE);

            Font tFont2 = new Font(bf, 10);
            tFont1.SetStyle(Font.BOLD | Font.UNDERLINE);

            Phrase phrase = new Phrase(_culture == "nl-BE" ? "Identificatiegegevens van de onderneming" : "Identification de la société", tFont1);
            phrase.Leading = 2f * phrase.Font.Size;
            phrase.Leading = phrase.Leading;
            cell.Add(phrase);
            _table.AddCell(cell);


            // SPACER
            cell = new Cell();
            cell.Colspan = 2;
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(" ", tFont1));
            _table.AddCell(cell);

            // Wettelijke benaming
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_culture == "nl-BE" ? "Wettelijke benaming" : "Raison sociale", tFont2));
            _table.AddCell(cell);
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_xdc.Client.Name, tFont2));
            _table.AddCell(cell);

            // Ondernemingsnummer
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_culture == "nl-BE" ? "Ondernemingsnummer" : "Numéro d'entreprise", tFont2));
            _table.AddCell(cell);
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_xdc.Client.EnterpriseNumber, tFont2));
            _table.AddCell(cell);

            // Rechtsvorm
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_culture == "nl-BE" ? "Rechtsvorm":"Forme juridique", tFont2));
            _table.AddCell(cell);
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            
            cell.Add(new Phrase(_legatType.Name, tFont2));
            _table.AddCell(cell);

            // Adres
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_culture == "nl-BE" ? "Adres" : "Adresse", tFont2));
            _table.AddCell(cell);
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_xdc.Client.Address.AddressType.Description, tFont2));
            string address = string.Format("{0} {1}", _xdc.Client.Address.Street, _xdc.Client.Address.Nr);
            if(!string.IsNullOrEmpty(_xdc.Client.Address.Bus)) 
                address += " - " + _xdc.Client.Address.Bus;

            cell.Add(new Phrase(address, tFont2));
            cell.Add(new Phrase(string.Format("{0} {1}",_xdc.Client.Address.ZipCode.Description,_xdc.Client.Address.City) , tFont2));
            cell.Add(new Phrase(_xdc.Client.Address.Country.Description, tFont2));
            _table.AddCell(cell);

            // SPACER
            cell = new Cell();
            cell.Colspan = 2;
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(" ", tFont1));
            _table.AddCell(cell);

            if (_xdc.Client.Bank.Active)
            {
                // Bank informatie
                cell = new Cell();
                cell.Colspan = 2;
                cell.Border = 0;
                cell.BorderWidth = 0;
                cell.Add(new Phrase(_culture == "nl-BE" ?"Bank informatie":"Information bancaire", tFont2));
                _table.AddCell(cell);

                // BIC
                cell = new Cell();
                cell.Border = 0;
                cell.BorderWidth = 0;
                cell.Add(new Phrase("BIC", tFont2));
                _table.AddCell(cell);
                cell = new Cell();
                cell.Border = 0;
                cell.BorderWidth = 0;
                cell.Add(new Phrase(_xdc.Client.Bank.Bic, tFont2));
                _table.AddCell(cell);

                // IBAN
                cell = new Cell();
                cell.Border = 0;
                cell.BorderWidth = 0;
                cell.Add(new Phrase("IBAN", tFont2));
                _table.AddCell(cell);
                cell = new Cell();
                cell.Border = 0;
                cell.BorderWidth = 0;
                cell.Add(new Phrase(_xdc.Client.Bank.Iban, tFont2));
                _table.AddCell(cell);
            }

            cell = new Cell();
            cell.Colspan = 2;
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(" ", tFont1));
            _table.AddCell(cell);

            // Contactpersoon
            cell = new Cell();
            cell.Colspan = 2;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.BorderWidth = 0;

            phrase = new Phrase(_culture == "nl-BE" ? "Contactpersoon" : "Personne de contact", tFont1);
            phrase.Leading = 2f * phrase.Font.Size;
            phrase.Leading = phrase.Leading;
            cell.Add(phrase);
            _table.AddCell(cell);

            // Type
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_culture == "nl-BE" ? "Hoedanigheid" : "Capacité", tFont2));
            _table.AddCell(cell);
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_xdc.Contact.ContactType.Description, tFont2));
            _table.AddCell(cell);


            // functie
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_culture == "nl-BE" ? "Functie" : "Fonction", tFont2));
            _table.AddCell(cell);
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_xdc.Contact.Function, tFont2));
            _table.AddCell(cell);


            // naam
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_culture == "nl-BE" ? "Naam":"Nom", tFont2));
            _table.AddCell(cell);
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(string.Format("{0} {1}",_xdc.Contact.FirstName,_xdc.Contact.Name), tFont2));
            _table.AddCell(cell);

            if (!string.IsNullOrEmpty(_xdc.Contact.Email))
            {
                cell = new Cell();
                cell.Border = 0;
                cell.BorderWidth = 0;
                cell.Add(new Phrase("E-mail", tFont2));
                _table.AddCell(cell);
                cell = new Cell();
                cell.Border = 0;
                cell.BorderWidth = 0;
                cell.Add(new Phrase(_xdc.Contact.Email, tFont2));
                _table.AddCell(cell);
            }

            // Adres
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_culture == "nl-BE" ? "Adres":"Adresse", tFont2));
            _table.AddCell(cell);
            cell = new Cell();
            cell.Border = 0;
            cell.BorderWidth = 0;
            cell.Add(new Phrase(_xdc.Contact.Address.AddressType.Description, tFont2));
            address = string.Format("{0} {1}", _xdc.Contact.Address.Street, _xdc.Contact.Address.Nr);
            if (!string.IsNullOrEmpty(_xdc.Contact.Address.Bus))
                address += " - " + _xdc.Contact.Address.Bus;

            cell.Add(new Phrase(address, tFont2));
            cell.Add(new Phrase(string.Format("{0} {1}", _xdc.Contact.Address.ZipCode.Description, _xdc.Contact.Address.City), tFont2));
            cell.Add(new Phrase(_xdc.Contact.Address.Country.Description, tFont2));
            _table.AddCell(cell);

            if (_xdc.Contact.Phone != null && _xdc.Contact.Phone.Active)
            {
                cell = new Cell();
                cell.Border = 0;
                cell.BorderWidth = 0;
                cell.Add(new Phrase(_culture == "nl-BE" ? "Telefoon":"Téléphone", tFont2));
                _table.AddCell(cell);
                cell = new Cell();
                cell.Border = 0;
                cell.BorderWidth = 0;
                string phone = string.Format("{0} {1} {2}", _xdc.Contact.Phone.CountryCode, _xdc.Contact.Phone.Zone, _xdc.Contact.Phone.LocalNr);
                if (!string.IsNullOrEmpty(_xdc.Contact.Phone.ExtensionNr))
                    phone += " " + _xdc.Contact.Phone.ExtensionNr;

                cell.Add(new Phrase(phone, tFont2));
                _table.AddCell(cell);
            }

            chapter.Add(_table);

            _document.Add(chapter);

           
        }

  



    

        #region IDisposable Members

        public void Dispose()
        {
            
            _table = null;
            _font = null;
            bf = null;
            _cultureInfo = null;
            _statementXDoc = null;
            _xdc = null;
            _legatType = null;

            base.Dispose();
        }

        #endregion
    }
}
