﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Xml;

namespace EY.com.eBook.Core.IText.Render
{
    public class CoverpageRenderer : ITextRenderer
    {
        private CoverpageDataContract _cpdc;
        private FileDataContract _fdc;
        private ClientDataContract _cdc;

        public CoverpageRenderer(CoverpageDataContract cpdc, FileDataContract fdc, ClientDataContract cdc)
            : base(null,cpdc)
        {
            _cpdc = cpdc;
            _fdc = fdc;
            _cdc = cdc;
            _pdfDataContract.Delete = true;
            _pdfDataContract.ShowInIndex = false;
        }

        public override void HandleDocument()
        {
            Writeme();
        }

        public void Writeme()
        {
            string title="";
            XElement block =null;

            switch (_cpdc.CoverType.ToLower())
            {
                case "xbrl":
                    string titlex1 = "{0}";
                    string titlex2 = "{0}";
                    switch (_cpdc.Culture)
                    {
                        case "nl-BE":
                            title = "Aangifte in de vennootschapsbelasting - BizTax";
                            titlex1 = "Aanslagjaar {0}";
                            titlex2 = "Boekjaar per {0}";
                            break;
                        case "fr-FR":
                            title = "Déclaration à l'impôt des sociétés - BizTax";
                            titlex1 = "Exercice d'imposition {0}";
                            titlex2 = "Exercice comptable au {0}";
                            break;
                    }

                    titlex1 = string.Format(titlex1, _fdc.AssessmentYear);
                    titlex2 = string.Format(titlex2, _fdc.EndDate.ToString("dd/MM/yyyy"));

                    block = new XElement("block",
                                        new XAttribute("align", "center"),
                                        new XAttribute("top", "195"),
                                        new XAttribute("left", "-220"),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(_cdc.Name)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(_cdc.Address)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(string.Format("{0} {1}", _cdc.ZipCode, _cdc.City))
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "12"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(" ")
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(title)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(titlex1)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(titlex2)
                                        )
                                    );
                    break;
                case "tax":
                    string title1 = "{0}";
                    string title2 = "{0}";
                    switch (_cpdc.Culture)
                    {
                        case "nl-BE":
                            title = "Aangifte in de vennootschapsbelasting " + (_cdc.BNI ? "van niet inwoners" : "");
                            title1 = "Aanslagjaar {0}";
                            title2 = "Boekjaar per {0}";
                            break;
                        case "fr-FR":
                            title = _cdc.BNI ? "Déclaration à l'impôt non résidents" :"Déclaration à l'impôt des sociétés";
                            title1 = "Exercice d'imposition {0}";
                            title2 = "Exercice comptable au {0}";
                            break;
                    }

                    title1 = string.Format(title1, _fdc.AssessmentYear);
                    title2 = string.Format(title2, _fdc.EndDate.ToString("dd/MM/yyyy"));

                    block = new XElement("block",
                                        new XAttribute("align", "center"),
                                        new XAttribute("top", "195"),
                                        new XAttribute("left", "-220"),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(_cdc.Name)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(_cdc.Address)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(string.Format("{0} {1}", _cdc.ZipCode, _cdc.City))
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "12"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(" ")
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(title)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(title1)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(title2)
                                        )
                                    );
                    break;
                default:

                    switch (_cpdc.Culture)
                    {
                        case "nl-BE":
                            title = "JAARREKENING per {0}";
                            break;
                        case "fr-FR":
                            title = "COMPTES ANNUELS au {0}";
                            break;
                    }
                    title = string.Format(title, _fdc.EndDate.ToString("dd/MM/yyyy"));


                    block = new XElement("block",
                                        new XAttribute("align", "center"),
                                        new XAttribute("top", "195"),
                                        new XAttribute("left", "-220"),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(_cdc.Name)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(_cdc.Address)
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(string.Format("{0} {1}", _cdc.ZipCode, _cdc.City))
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "12"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(" ")
                                        ),
                                        new XElement("phrase",
                                            new XAttribute("size", "14"),
                                            new XAttribute("fontstyle", "bold"),
                                            new XCData(title)
                                        )
                                    );
                    break;
            }
            XmlDocument doc = new XmlDocument();
            
            doc.LoadXml(block.ToString());
            ProcessContentBlock(writer.DirectContent, doc.FirstChild, writer.PageSize);
        }
    }
}
