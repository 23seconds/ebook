﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace EY.com.eBook.Core.IText.Render
{
    public class TablePageRenderer : ITextRenderer
    {
        private TablePageDataContract _tpdc;

        public TablePageRenderer(TablePageDataContract tpdc)
            : base(null, tpdc)
        {
            _tpdc = tpdc;
            _pdfDataContract.Delete = true;
            _pdfDataContract.ShowInIndex = false;
        }

        /// <summary>
        /// Dynamically create a table from a string[,]
        /// </summary>
        public override void HandleDocument()
        {
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(bf, 8f);

            string title = !String.IsNullOrEmpty(_tpdc.Title) ? _tpdc.Title : "";
            int rows = _tpdc.Table.GetLength(0);
            int columns = _tpdc.Table.GetLength(1);
            Table table = new iTextSharp.text.Table(columns, rows);
            table.Border = Rectangle.NO_BORDER;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Phrase phrase = new Phrase(_tpdc.Table[i, j], font);
                    Cell cell = new Cell(phrase);
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.Border = Rectangle.NO_BORDER;
                    table.AddCell(cell, i, j);
                }
            }

            Chapter chapter = new Chapter(0);
            chapter.Add(table);
            _document.Add(chapter);
        }
    }
}
