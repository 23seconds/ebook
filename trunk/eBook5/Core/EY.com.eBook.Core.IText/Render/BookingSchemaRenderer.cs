﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using iTextSharp.text.pdf;
using System.Globalization;
using iTextSharp.text;
using System.util;
using iTextSharp.text.factories;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.Core.IText.Render
{
    public class BookingSchemaRenderer : ITextRenderer
    {

        private List<BookingSingleDataContract> _bookings;
        private Table _table;
        private Font _font;
        private BaseFont bf;
        private CultureInfo _cultureInfo;
        private CorrectionBookingSchemaDataContract _cbsdc;
        


        public BookingSchemaRenderer(List<BookingSingleDataContract> bookings, CorrectionBookingSchemaDataContract cbsdc)
            : base(null, cbsdc)
        {
            _bookings = bookings;
            _cbsdc = cbsdc;
            _cultureInfo = CultureInfo.CreateSpecificCulture(_cbsdc.culture);

            //string dteLastChanged = _accounts.OrderBy(a => a.LastChanged).First().LastChanged.ToString();

            //string path = Path.Combine(Config.SchemaTemplates, string.Format("{0}.xml", sdc.Layout));
            


            string renderId = new Guid().ToString();// sdc.FileId.ToString() + dteLastChanged + sdc.Layout + sdc.Detailed.ToString() + sdc.Comparable.ToString() + sdc.Culture;
            //_sdc.Id = renderId.DeterministicGuid();

            //_cultureInfo = CultureInfo.CreateSpecificCulture(sdc.Culture);
            /*
            if (_sdc.Comparable)
            {
                _levelColors = new Color[] { Color.BLACK, Color.BLACK, Color.BLACK, Color.DARK_GRAY, Color.GRAY, Color.LIGHT_GRAY, Color.LIGHT_GRAY };
            }
            else
            {
                _levelColors = new Color[] { Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK };
            }
            */


        }

        public override void HandleDocument()
        {

            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            _font = new Font(bf, 8f);


            Chapter chapter = new Chapter(0);
            _table = new Table(5);
            _table.TableFitsPage = true;
            _table.Convert2pdfptable = true;
            _table.CellsFitPage = true;
            _table.Width = 100f;
            _table.Alignment = Element.ALIGN_CENTER;
            _table.Border = 0;
            _table.Cellpadding = 1f;
            _table.Cellspacing = 0f;
            _table.SetWidths(new int[] {35, 5, 25, 25, 34 });
            // set column headers

            RenderBookingLines();
            chapter.Add(_table);
            _document.Add(chapter);


        }

        private void RenderBookingLines() {

            string geimporteerdSaldo = "";
            string saldo = "";
            string totaal = "";
            string rekening = "";
            string klant = "";
            switch (_cbsdc.culture) { 
                case "nl-BE":
                    geimporteerdSaldo = "Geïmporteerd saldo";
                    saldo = "Saldo";
                    totaal = "Totaal";
                    rekening = "Rekening";
                    klant = "Klant";
                    break;
                case "fr-FR":
                    geimporteerdSaldo = "Solde importé";
                    saldo = "Solde";
                    totaal = "Total";
                    rekening = "Compte";
                    klant = "Client";
                    break;
                case "en-US":
                    geimporteerdSaldo = "Geïmporteerd saldo";
                    saldo = "Saldo";
                    totaal = "Totaal";
                    rekening = "Rekening";
                    klant = "Client";
                    break;
            }

            Cell dummyCell = new Cell();
            Cell textCell = new Cell();
            Phrase textPhrase = new Phrase();
            Cell numberCell = new Cell();
            Phrase numberPhrase = new Phrase();

            int CreditT = 0;
            int DebetT = 0;

            // HEADER
            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_LEFT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            Font tFontH = new Font(bf, 10);
            tFontH.SetStyle(Font.BOLD | Font.UNDERLINE);
            textPhrase = new Phrase(rekening, tFontH);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_LEFT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            tFontH.SetStyle(Font.BOLD | Font.UNDERLINE);
            textPhrase = new Phrase("Nr", tFontH);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;
                        
            tFontH.SetStyle(Font.BOLD | Font.UNDERLINE);
            textPhrase = new Phrase("Debet", tFontH);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            textPhrase = new Phrase("Credit", tFontH);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            textPhrase = new Phrase(klant, tFontH);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);
            

            // ROW STARTSALDO
            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_LEFT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            Font tFontF = new Font(bf, 10);
            tFontF.SetStyle(Font.BOLD);
            textPhrase = new Phrase(geimporteerdSaldo, tFontF);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // dummy to fill up row
            dummyCell = GetDummyCell();
            dummyCell.Colspan = 1;
            _table.AddCell(dummyCell);

                  

            if (_cbsdc.StartSaldo > 0)
            {
                DebetT = _cbsdc.StartSaldo;

                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;


                textPhrase = new Phrase(_cbsdc.StartSaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), tFontF);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                // dummy to fill up row
                dummyCell = GetDummyCell();
                dummyCell.Colspan = 1;
                _table.AddCell(dummyCell);
            }
            else
            {
                CreditT += (_cbsdc.StartSaldo * (-1));

                // dummy to fill up row
                dummyCell = GetDummyCell();
                dummyCell.Colspan = 1;
                _table.AddCell(dummyCell);

                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                textPhrase = new Phrase((_cbsdc.StartSaldo * (-1)).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFontF);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);
            }

            // dummy to fill up row
            dummyCell = GetDummyCell();
            dummyCell.Colspan = 1;
            _table.AddCell(dummyCell);

            

            Font tFontE = new Font(bf, 10);
            
            foreach (BookingSingleDataContract b in _bookings) {
                CreditT += Convert.ToInt32(b.Credit);
                DebetT += Convert.ToInt32(b.Debet);
                
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                textPhrase = new Phrase(b.AccountNr, tFontE);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                textPhrase = new Phrase(b.GroupNr.ToString(), tFontE);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                textPhrase = new Phrase(Convert.ToDecimal(b.Debet).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFontE);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                textPhrase = new Phrase(Convert.ToDecimal(b.Credit).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFontE);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                textPhrase = new Phrase(b.ClientSupplierName, tFontE);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                

                if (b.Comment != null) {
                    // Booking description
                    textCell = new Cell();
                    textCell.Colspan = 1;
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    Font tFontI = new Font(bf, 9);
                    tFontI.SetStyle(Font.ITALIC);
                    textPhrase = new Phrase("Comment:", tFontI);
                    textPhrase.Leading = 2f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);
                    _table.AddCell(textCell);

                    // Booking description
                    textCell = new Cell();
                    textCell.Colspan = 4;
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    textPhrase = new Phrase(b.Comment, tFontI);
                    textPhrase.Leading = 2f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);
                    _table.AddCell(textCell);
                }
            }

            // ROW TOTAAL
            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_LEFT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            tFontF.SetStyle(Font.BOLD);
            textPhrase = new Phrase(totaal, tFontF);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // dummy to fill up row
            dummyCell = GetDummyCell();
            dummyCell.Colspan = 1;
            _table.AddCell(dummyCell);
            
            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;


            textPhrase = new Phrase(DebetT.ToString("#,##0.00€", _cultureInfo.NumberFormat), tFontF);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            textPhrase = new Phrase(CreditT.ToString("#,##0.00€", _cultureInfo.NumberFormat), tFontF);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);
            

            // dummy to fill up row
            dummyCell = GetDummyCell();
            dummyCell.Colspan = 1;
            _table.AddCell(dummyCell);



            // ROW SALDO
            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_LEFT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            tFontF.SetStyle(Font.BOLD);
            textPhrase = new Phrase(saldo, tFontF);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // dummy to fill up row
            dummyCell = GetDummyCell();
            dummyCell.Colspan = 1;
            _table.AddCell(dummyCell);

            if (DebetT > CreditT)
            {
                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                textPhrase = new Phrase((DebetT - CreditT).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFontF);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                // dummy to fill up row
                dummyCell = GetDummyCell();
                dummyCell.Colspan = 1;
                _table.AddCell(dummyCell);
            }
            else {
                // dummy to fill up row
                dummyCell = GetDummyCell();
                dummyCell.Colspan = 1;
                _table.AddCell(dummyCell);

                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                textPhrase = new Phrase((CreditT - DebetT).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFontF);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                
            }

            // dummy to fill up row
            dummyCell = GetDummyCell();
            dummyCell.Colspan = 1;
            _table.AddCell(dummyCell);

            
            
        }

        public override PDFDataContract CreatePdf()
        {
            PDFDataContract pdf = base.CreatePdf();

            //pdf.HeaderConfig.Enabled = true;
            return pdf;
        }
        

        private Cell GetDummyCell()
        {
            Cell dummy = new Cell(" ");
            dummy.Border = 0;
            //dummy.BorderWidth = 0;
            return dummy;
        }       
    }
}
