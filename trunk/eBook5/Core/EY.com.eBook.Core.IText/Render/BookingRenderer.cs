﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using iTextSharp.text.pdf;
using System.Globalization;
using iTextSharp.text;
using System.util;
using iTextSharp.text.factories;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.Core.IText.Render
{
    public class BookingRenderer : ITextRenderer
    {

        private List<BookingDataContract> _bookings;
        private Table _table;
        private Font _font;
        private BaseFont bf;
        private CultureInfo _cultureInfo;
        private CorrectionBookingDataContract _cbdc;
        


        public BookingRenderer(List<BookingDataContract> bookings, CorrectionBookingDataContract cbdc)
            : base(null, cbdc)
        {
            _bookings = bookings;
            _cbdc = cbdc;
            _cultureInfo = CultureInfo.CreateSpecificCulture(_cbdc.culture);

            //string dteLastChanged = _accounts.OrderBy(a => a.LastChanged).First().LastChanged.ToString();

            //string path = Path.Combine(Config.SchemaTemplates, string.Format("{0}.xml", sdc.Layout));
            


            string renderId = new Guid().ToString();// sdc.FileId.ToString() + dteLastChanged + sdc.Layout + sdc.Detailed.ToString() + sdc.Comparable.ToString() + sdc.Culture;
            //_sdc.Id = renderId.DeterministicGuid();

            //_cultureInfo = CultureInfo.CreateSpecificCulture(sdc.Culture);
            /*
            if (_sdc.Comparable)
            {
                _levelColors = new Color[] { Color.BLACK, Color.BLACK, Color.BLACK, Color.DARK_GRAY, Color.GRAY, Color.LIGHT_GRAY, Color.LIGHT_GRAY };
            }
            else
            {
                _levelColors = new Color[] { Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK };
            }
            */


        }

        public override void HandleDocument()
        {

            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            _font = new Font(bf, 8f);


            Chapter chapter = new Chapter(0);
            _table = new Table(4);
            _table.TableFitsPage = true;
            _table.Convert2pdfptable = true;
            _table.CellsFitPage = true;
            _table.Width = 100f;
            _table.Alignment = Element.ALIGN_CENTER;
            _table.Border = 0;
            _table.Cellpadding = 1f;
            _table.Cellspacing = 0f;
            _table.SetWidths(new int[] {50, 26, 26, 22 });
            // set column headers

            RenderBookingLines();
            chapter.Add(_table);
            _document.Add(chapter);


        }

        private void RenderBookingLines() {
            Cell dummyCell = new Cell();
            Cell textCell = new Cell();
            Phrase textPhrase = new Phrase();
            Cell numberCell = new Cell();
            Phrase numberPhrase = new Phrase();

            // HEADER
            // dummy to fill up row
            dummyCell = GetDummyCell();
            dummyCell.Colspan = 1;
            _table.AddCell(dummyCell);

            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            Font tFontH = new Font(bf, 10);
            tFontH.SetStyle(Font.BOLD | Font.UNDERLINE);
            textPhrase = new Phrase("Debet", tFontH);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            textPhrase = new Phrase("Credit", tFontH);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);

            // Booking description
            textCell = new Cell();
            textCell.Colspan = 1;
            textCell.VerticalAlignment = Element.ALIGN_TOP;
            textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            textCell.Border = 0;
            textCell.BorderWidth = 0;

            tFontH.SetStyle(Font.BOLD | Font.UNDERLINE);
            textPhrase = new Phrase("Client", tFontH);
            textPhrase.Leading = 2f * textPhrase.Font.Size;
            textCell.Leading = textPhrase.Leading;
            textCell.Add(textPhrase);
            _table.AddCell(textCell);


            int cntB = 1;
            int totalBl = 0;
            decimal totalDebet = 0;
            decimal totalCredit = 0;

            foreach (BookingDataContract b in _bookings) {
                totalBl += b.BookingLines.Count;

                // Booking description
                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;

                Font tFont1 = new Font(bf, 10);
                tFont1.SetStyle(Font.BOLD | Font.UNDERLINE);
                textPhrase = new Phrase(cntB + " - " + b.Description, tFont1);
                textPhrase.Leading = 2f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                // dummy to fill up row
                dummyCell = GetDummyCell();
                dummyCell.Colspan = 3;
                _table.AddCell(dummyCell);

                decimal debet = 0;
                decimal credit = 0;
                int cntBL = 1;
                Font tFont2 = new Font(bf, 10);
                // render bookinglines
                foreach (BookingLineDataContract bl in b.BookingLines) {

                    string blTranslation = null;
                    switch (_cbdc.culture)
                    {
                        case "nl-BE":
                            blTranslation = bl.AccountNr.NL;
                            break;
                        case "fr-FR":
                            blTranslation = bl.AccountNr.FR;
                            break;
                        case "en-US":
                            blTranslation = bl.AccountNr.EN;
                            break;
                    }

                    //Account nr + description
                    textCell = new Cell();
                    textCell.Colspan = 1;
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    
                    //tFont2.SetStyle(Font.BOLD | Font.UNDERLINE);
                    textPhrase = new Phrase(blTranslation, tFont2);
                    textPhrase.Leading = cntBL == 1 ? 1.5f * textPhrase.Font.Size : 1.1f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);
                    _table.AddCell(textCell);

                    if (bl.Amount > 0)
                    {
                        //debet
                        debet += bl.Amount;
                        // Ticket #60
                        totalDebet += bl.Amount;
                        //amount
                        numberCell = new Cell();
                        numberCell.Colspan = 1;
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;

                        numberPhrase = new Phrase(bl.Amount.ToString("#,##0.00€", _cultureInfo.NumberFormat), tFont2);
                        numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                        numberCell.Leading = numberPhrase.Leading;
                        numberCell.Add(numberPhrase);
                        _table.AddCell(numberCell);

                        // dummy to fill up row
                        dummyCell = GetDummyCell();
                        dummyCell.Colspan = 1;
                        _table.AddCell(dummyCell);

                        textCell = new Cell();
                        textCell.Colspan = 1;
                        textCell.VerticalAlignment = Element.ALIGN_TOP;
                        textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        textCell.Border = 0;
                        textCell.BorderWidth = 0;


                        //tFont2.SetStyle(Font.BOLD | Font.UNDERLINE);
                        textPhrase = new Phrase(bl.BusinessRelationName, tFont2);
                        textPhrase.Leading = cntBL == 1 ? 1.5f * textPhrase.Font.Size : 1.1f * textPhrase.Font.Size;
                        textCell.Leading = textPhrase.Leading;
                        textCell.Add(textPhrase);
                        _table.AddCell(textCell);
                    }
                    else { 
                        //credit
                        credit += (0 - bl.Amount);
                        // Ticket #60
                        totalCredit += (0 - bl.Amount);
                        // dummy to fill up row
                        dummyCell = GetDummyCell();
                        dummyCell.Colspan = 1;
                        _table.AddCell(dummyCell);

                        //amount
                        numberCell = new Cell();
                        numberCell.Colspan = 1;
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;

                        numberPhrase = new Phrase((0 - bl.Amount).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFont2);
                        numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                        numberCell.Leading = numberPhrase.Leading;
                        numberCell.Add(numberPhrase);
                        _table.AddCell(numberCell);

                        textCell = new Cell();
                        textCell.Colspan = 1;
                        textCell.VerticalAlignment = Element.ALIGN_TOP;
                        textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        textCell.Border = 0;
                        textCell.BorderWidth = 0;


                        //tFont2.SetStyle(Font.BOLD | Font.UNDERLINE);
                        textPhrase = new Phrase(bl.BusinessRelationName, tFont2);
                        textPhrase.Leading = cntBL == 1 ? 1.5f * textPhrase.Font.Size : 1.1f * textPhrase.Font.Size;
                        textCell.Leading = textPhrase.Leading;
                        textCell.Add(textPhrase);
                        _table.AddCell(textCell);
                    }

                    if (bl.Comment != null)
                    {
                        // Booking description
                        textCell = new Cell();
                        textCell.Colspan = 1;
                        textCell.VerticalAlignment = Element.ALIGN_TOP;
                        textCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        textCell.Border = 0;
                        textCell.BorderWidth = 0;

                        Font tFontI = new Font(bf, 9);
                        tFontI.SetStyle(Font.ITALIC);
                        textPhrase = new Phrase("Comment:", tFontI);
                        textPhrase.Leading = 2f * textPhrase.Font.Size;
                        textCell.Leading = textPhrase.Leading;
                        textCell.Add(textPhrase);
                        _table.AddCell(textCell);

                        // Booking description
                        textCell = new Cell();
                        textCell.Colspan = 3;
                        textCell.VerticalAlignment = Element.ALIGN_TOP;
                        textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        textCell.Border = 0;
                        textCell.BorderWidth = 0;

                        textPhrase = new Phrase(bl.Comment, tFontI);
                        textPhrase.Leading = 2f * textPhrase.Font.Size;
                        textCell.Leading = textPhrase.Leading;
                        textCell.Add(textPhrase);
                        _table.AddCell(textCell);
                    }

                    cntBL++;                    
                }
                //total per bookingblock
                // total lines
                //Account nr + description
                dummyCell = GetDummyCell();
                dummyCell.Colspan = 4;
                _table.AddCell(dummyCell);

                textCell = new Cell();
                textCell.Colspan = 1;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                textCell.Border = 1;
                textCell.BorderColorTop = Color.BLACK;                             
                
                textPhrase = new Phrase(b.BookingLines.Count + " lines", tFont2);
                textPhrase.Leading = 1.3f * textPhrase.Font.Size;
                textCell.Leading = textPhrase.Leading;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                //total debet
                numberCell = new Cell();
                numberCell.Colspan = 1;
                numberCell.VerticalAlignment = Element.ALIGN_TOP;
                numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                numberCell.Border = 1;
                numberCell.BorderColorTop = Color.BLACK;

                Font tFonttotal = new Font(bf, 10);
                tFonttotal.SetStyle(Font.BOLD);
                numberPhrase = new Phrase((debet).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFonttotal);
                numberPhrase.Leading = 1.3f * textPhrase.Font.Size;
                numberCell.Leading = numberPhrase.Leading;
                numberCell.Add(numberPhrase);
                _table.AddCell(numberCell);

                //total credit
                numberCell = new Cell();
                numberCell.Colspan = 1;
                numberCell.VerticalAlignment = Element.ALIGN_TOP;
                numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                numberCell.Border = 1;
                numberCell.BorderColorTop = Color.BLACK;

                numberPhrase = new Phrase((credit).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFonttotal);
                numberPhrase.Leading = 1.3f * textPhrase.Font.Size;
                numberCell.Leading = numberPhrase.Leading;
                numberCell.Add(numberPhrase);
                _table.AddCell(numberCell);

                dummyCell = GetDummyCell();
                dummyCell.Colspan = 1;
                dummyCell.Border = 1;
                dummyCell.BorderColorTop = Color.BLACK;
                _table.AddCell(dummyCell);

                if (cntB < _bookings.Count)
                {
                    // Create 3 line breaks after each booking
                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 4;
                    _table.AddCell(dummyCell);

                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 4;
                    _table.AddCell(dummyCell);

                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 4;
                    _table.AddCell(dummyCell);


                }
                else {

                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 4;
                    _table.AddCell(dummyCell);

                    

                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 4;
                    dummyCell.Border = 1;
                    dummyCell.BorderColorTop = Color.BLACK;
                    _table.AddCell(dummyCell);

                    textCell = new Cell();
                    textCell.Colspan = 1;
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 1;
                    textCell.BorderColorTop = Color.BLACK;

                    textPhrase = new Phrase(totalBl + " lines", tFonttotal);
                    textPhrase.Leading = 1.3f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);
                    _table.AddCell(textCell);

                    //total debet
                    numberCell = new Cell();
                    numberCell.Colspan = 1;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 1;
                    numberCell.BorderColorTop = Color.BLACK;

                    numberPhrase = new Phrase((totalDebet).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFonttotal);
                    numberPhrase.Leading = 1.3f * textPhrase.Font.Size;
                    numberCell.Leading = numberPhrase.Leading;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);

                    //total credit
                    numberCell = new Cell();
                    numberCell.Colspan = 1;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 1;
                    numberCell.BorderColorTop = Color.BLACK;

                    numberPhrase = new Phrase((totalCredit).ToString("#,##0.00€", _cultureInfo.NumberFormat), tFonttotal);
                    numberPhrase.Leading = 1.3f * textPhrase.Font.Size;
                    numberCell.Leading = numberPhrase.Leading;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);

                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 1;
                    dummyCell.Border = 1;
                    dummyCell.BorderColorTop = Color.BLACK;
                    _table.AddCell(dummyCell);
                }
                cntB++;
            }
        }

        public override PDFDataContract CreatePdf()
        {
            PDFDataContract pdf = base.CreatePdf();

            //pdf.HeaderConfig.Enabled = true;
            return pdf;
        }
        /*
        public override PDFDataContract CreatePdf()
        {
            string path = Path.Combine(Config.RepositoryLocal, _fdc.DataDirectory);
            path = Path.Combine(path, "Generation");
            if (!System.IO.Directory.Exists(path)) System.IO.Directory.CreateDirectory(path);

            path = Path.Combine(path, string.Format("{0}.pdf", _sdc.Id.ToString()));

            if (System.IO.File.Exists(path))
            {
                DateTime accountchanged = _accounts.Max(a => a.LastChanged);
                DateTime lw = System.IO.File.GetLastWriteTime(path);
                /*
                if (lw >= accountchanged)
                {
                    PdfReader pr = new PdfReader(path);

                    PDFDataContract epdf = new PDFDataContract
                    {
                        PDFLocation = path
                        ,
                        Title = _sdc.Title
                        ,
                        HeaderConfig = _sdc.HeaderConfig
                        ,
                        FooterConfig = _sdc.FooterConfig
                        ,
                        ShowInIndex = _sdc.ShowInIndex
                        ,
                        Pages = pr.NumberOfPages
                        ,
                        FromPage = 0
                        ,
                        ToPage = pr.NumberOfPages
                    };
                    pr.Close();
                    pr = null;
                    return epdf;

                }
                 * *//*
                System.IO.File.Delete(path);
            }

            PDFDataContract pdf = base.CreatePdf();
            //if(System.IO.File.Exists(path)) 
            System.IO.File.Move(pdf.PDFLocation, path);
            pdf.PDFLocation = path;
            pdf.Id = _sdc.Id;

            return pdf;

        }
*/

        private Cell GetDummyCell()
        {
            Cell dummy = new Cell(" ");
            dummy.Border = 0;
            //dummy.BorderWidth = 0;
            return dummy;
        }

        /*
        private void RenderItem(XElement node, int level, bool inverse)
        {

            Cell dummyCell = new Cell();
            Cell textCell = new Cell();
            Phrase textPhrase = new Phrase();
            Cell numberCell = new Cell();
            Phrase numberPhrase = new Phrase();
            decimal saldo = GetSaldi(node);
            decimal prevsaldo = _sdc.Comparable ? GetPreviousSaldi(node) : 0;




            // checks whether this section contains accounts with saldi different from 0
            bool hasValidSubAccounts = HasValidAccounts(node, _sdc.Comparable);

            if (level > 0 && saldo == 0 && !hasValidSubAccounts && (_sdc.Comparable && prevsaldo == 0 || !_sdc.Comparable)) return;

            XAttribute typeAttrib = node.Attributes("type").FirstOrDefault();
            if (typeAttrib != null)
            {
                inverse = typeAttrib.Value.ToLower() == "credit" || typeAttrib.Value.ToLower() == "range";
            }
            if (inverse) saldo = 0 - saldo;
            if (inverse) prevsaldo = 0 - prevsaldo;

            switch (level)
            {
                case 0:
                    break;
                case 1:
                    textCell = new Cell();
                    textCell.Colspan = _sdc.Comparable ? 5 : 7;
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    Font tFont1 = new Font(bf, 10);
                    tFont1.SetStyle(Font.BOLD | Font.UNDERLINE);
                    textPhrase = new Phrase(GetTranslation(node).ToUpper(), tFont1);
                    textPhrase.Leading = 2f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);
                    _table.AddCell(textCell);

                    if (_sdc.Comparable)
                    {
                        numberCell = new Cell();
                        numberCell.Colspan = 1;
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;

                        Font nFont1prev = new Font(bf, 8f);
                        nFont1prev.Color = _levelColors[level];
                        nFont1prev.SetStyle(Font.BOLD | Font.UNDERLINE);
                        numberPhrase = new Phrase(prevsaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont1prev);
                        numberPhrase.Leading = 2f * textPhrase.Font.Size;
                        numberCell.Leading = numberPhrase.Leading;
                        numberCell.Add(numberPhrase);
                        _table.AddCell(numberCell);
                    }

                    numberCell = new Cell();
                    numberCell.Colspan = _sdc.Comparable ? 1 : 2;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    Font nFont1 = new Font(bf, 8f);
                    nFont1.SetStyle(Font.BOLD | Font.UNDERLINE);
                    if (_sdc.Comparable) nFont1.Color = _levelColors[level];
                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont1);
                    numberPhrase.Leading = 2f * textPhrase.Font.Size;
                    numberCell.Leading = numberPhrase.Leading;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);

                    break;
                case 2:
                    dummyCell = GetDummyCell();

                    _table.AddCell(dummyCell);


                    textCell = new Cell();
                    textCell.Colspan = _sdc.Comparable ? 4 : 6;
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    Font tFont2 = new Font(bf, 10);
                    tFont2.SetStyle(Font.UNDERLINE);
                    textPhrase = new Phrase(GetTranslation(node).ToUpper(), tFont2);
                    textPhrase.Font = _font;
                    textPhrase.Font.SetStyle(Font.UNDERLINE);
                    textPhrase.Leading = 2f * textPhrase.Font.Size;
                    textCell.Add(textPhrase);
                    textCell.Leading = textPhrase.Leading;
                    _table.AddCell(textCell);

                    Font nFont2 = new Font(bf, 8);
                    nFont2.SetStyle(Font.UNDERLINE);
                    if (_sdc.Comparable) nFont2.Color = _levelColors[level];

                    if (_sdc.Comparable)
                    {
                        numberCell = new Cell();
                        numberCell.Colspan = 1;
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;



                        numberPhrase = new Phrase(prevsaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont2);
                        numberPhrase.Leading = 2f * textPhrase.Font.Size;
                        numberCell.Leading = numberPhrase.Leading;
                        numberCell.Add(numberPhrase);
                        _table.AddCell(numberCell);
                    }


                    numberCell = new Cell();
                    numberCell.Colspan = _sdc.Comparable ? 1 : 2;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;


                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont2);
                    numberPhrase.Leading = 2f * textPhrase.Font.Size;
                    numberCell.Leading = numberPhrase.Leading;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);

                    break;
                case 3:
                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 2;
                    _table.AddCell(dummyCell);

                    textCell = new Cell();
                    textCell.Colspan = _sdc.Comparable ? 3 : 4;
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    textPhrase = new Phrase(GetTranslation(node), new Font(bf, 10));
                    textPhrase.Font = _font;
                    textPhrase.Leading = 1.3f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);

                    _table.AddCell(textCell);

                    Font nFont3 = new Font(bf, 8);
                    if (_sdc.Comparable) nFont3.Color = _levelColors[level];

                    if (_sdc.Comparable)
                    {
                        numberCell = new Cell();
                        numberCell.Colspan = 1;
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;

                        numberPhrase = new Phrase(prevsaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont3);
                        numberPhrase.Leading = 1.3f * textPhrase.Font.Size;
                        numberCell.Add(numberPhrase);
                        numberCell.Leading = numberPhrase.Leading;
                        _table.AddCell(numberCell);
                    }

                    numberCell = new Cell();
                    numberCell.Colspan = _sdc.Comparable ? 1 : 2;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont3);
                    numberPhrase.Leading = 1.3f * textPhrase.Font.Size;
                    numberCell.Add(numberPhrase);
                    numberCell.Leading = numberPhrase.Leading;
                    _table.AddCell(numberCell);

                    if (!_sdc.Comparable)
                    {
                        dummyCell = new Cell(" ");
                        dummyCell.Border = 0;
                        dummyCell.BorderWidth = 0;

                        _table.AddCell(dummyCell);
                    }
                    break;
                case 4:
                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 3;
                    _table.AddCell(dummyCell);

                    textCell = new Cell();
                    textCell.Colspan = 2;
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;
                    textPhrase = new Phrase(GetTranslation(node), new Font(bf, 10));
                    textPhrase.Leading = 1.1f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);

                    _table.AddCell(textCell);

                    Font nFont4 = new Font(bf, 8);
                    if (_sdc.Comparable) nFont4.Color = _levelColors[level];

                    if (_sdc.Comparable)
                    {
                        numberCell = new Cell();
                        numberCell.Colspan = 1;
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;

                        numberPhrase = new Phrase(prevsaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont4);
                        numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                        numberCell.Add(numberPhrase);
                        numberCell.Leading = numberPhrase.Leading;
                        _table.AddCell(numberCell);
                    }

                    numberCell = new Cell();
                    numberCell.Colspan = _sdc.Comparable ? 1 : 2;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont4);
                    numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                    numberCell.Add(numberPhrase);
                    numberCell.Leading = numberPhrase.Leading;
                    _table.AddCell(numberCell);

                    if (!_sdc.Comparable)
                    {
                        dummyCell = GetDummyCell();
                        dummyCell.Colspan = 2;
                        _table.AddCell(dummyCell);
                    }
                    break;
                case 5:
                    dummyCell = GetDummyCell();
                    dummyCell.Colspan = 4;
                    _table.AddCell(dummyCell);

                    textCell = new Cell();
                    textCell.VerticalAlignment = Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    textPhrase = new Phrase(GetTranslation(node), new Font(bf, 9f));
                    textPhrase.Leading = 1.1f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);

                    _table.AddCell(textCell);

                    Font nFont5 = new Font(bf, 8);
                    if (_sdc.Comparable) nFont5.Color = _levelColors[level];

                    if (_sdc.Comparable)
                    {
                        numberCell = new Cell();
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;

                        numberPhrase = new Phrase(prevsaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont5);
                        numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                        numberCell.Leading = numberPhrase.Leading;
                        numberCell.Add(numberPhrase);
                        _table.AddCell(numberCell);
                    }

                    numberCell = new Cell();
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont5);
                    numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                    numberCell.Leading = numberPhrase.Leading;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);

                    if (!_sdc.Comparable)
                    {
                        dummyCell = GetDummyCell();
                        dummyCell.Colspan = 3;
                        _table.AddCell(dummyCell);
                    }
                    break;
            }

            if (node.Elements("AccountSection").Count() > 0)
            {
                int subLevel = level + 1;
                foreach (XElement childSection in node.Elements("AccountSection"))
                {
                    RenderItem(childSection, subLevel, inverse);
                }
            }
            else
            {
                if (_sdc.Detailed && level > 1)
                {
                    foreach (AccountDataContract adc in GetAccounts(node))
                    {
                        // render extra detail node.
                        dummyCell = GetDummyCell();
                        dummyCell.Colspan = 4;
                        _table.AddCell(dummyCell);

                        textCell = new Cell();
                        textCell.Colspan = _sdc.Comparable ? 1 : 2;
                        textCell.VerticalAlignment = Element.ALIGN_TOP;
                        textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        textCell.Border = 0;
                        textCell.BorderWidth = 0;
                        if (adc.Descriptions == null) adc.Descriptions = new List<AccountDescriptionDataContract>();
                        AccountDescriptionDataContract addc = adc.Descriptions.FirstOrDefault(d => d.Culture == _sdc.Culture);
                        if (addc == null) addc = adc.Descriptions.FirstOrDefault();
                        string desc = addc != null ? addc.Description : adc.DefaultDescription;

                        textPhrase = new Phrase(desc, new Font(bf, 8f));
                        textPhrase.Leading = 1.1f * 10f;
                        textCell.Leading = textPhrase.Leading;
                        textCell.Add(textPhrase);

                        _table.AddCell(textCell);

                        Font nFont6 = new Font(bf, 8);
                        if (_sdc.Comparable) nFont6.Color = _levelColors[6];


                        if (_sdc.Comparable)
                        {
                            numberCell = new Cell();
                            numberCell.Colspan = 1;
                            numberCell.VerticalAlignment = Element.ALIGN_TOP;
                            numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            numberCell.Border = 0;
                            numberCell.BorderWidth = 0;

                            decimal apSaldo = adc.PreviousSaldo;
                            if (inverse) apSaldo = 0 - apSaldo;
                            numberPhrase = new Phrase(apSaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont6);
                            numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                            numberCell.Leading = numberPhrase.Leading;
                            numberCell.Add(numberPhrase);
                            _table.AddCell(numberCell);
                        }

                        numberCell = new Cell();
                        numberCell.Colspan = 1;
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;

                        decimal aSaldo = adc.Saldo;
                        if (inverse) aSaldo = 0 - aSaldo;
                        numberPhrase = new Phrase(aSaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont6);
                        numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                        numberCell.Leading = numberPhrase.Leading;
                        numberCell.Add(numberPhrase);
                        _table.AddCell(numberCell);

                        if (!_sdc.Comparable)
                        {
                            dummyCell = GetDummyCell();
                            dummyCell.Colspan = 2;
                            _table.AddCell(dummyCell);
                        }
                    }
                }
            }
            if (level == 0 && node.Attributes("hide").FirstOrDefault() == null)
            {
                textCell = new Cell();
                textCell.Colspan = _sdc.Comparable ? 5 : 7;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;


                Font tFont0 = new Font(bf, 10f);
                tFont0.SetStyle(Font.BOLD | Font.UNDERLINE);

                textPhrase = new Phrase(GetTranslation(node).ToUpper(), tFont0);
                textPhrase.Leading = 1.4f * textPhrase.Font.Size;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                tFont0 = new Font(bf, 9f);
                tFont0.SetStyle(Font.BOLD | Font.UNDERLINE);
                if (_sdc.Comparable) tFont0.Color = _levelColors[level];


                if (_sdc.Comparable)
                {
                    numberCell = new Cell();
                    numberCell.Colspan = 1;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;


                    numberPhrase = new Phrase(prevsaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), tFont0);
                    numberPhrase.Font.SetStyle(Font.BOLD | Font.UNDERLINE);
                    numberPhrase.Leading = 1.4f * textPhrase.Font.Size;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);

                }

                numberCell = new Cell();
                numberCell.Colspan = _sdc.Comparable ? 1 : 2;
                numberCell.VerticalAlignment = Element.ALIGN_TOP;
                numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                numberCell.Border = 0;
                numberCell.BorderWidth = 0;


                numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), tFont0);
                numberPhrase.Font.SetStyle(Font.BOLD | Font.UNDERLINE);
                numberPhrase.Leading = 1.4f * textPhrase.Font.Size;
                numberCell.Add(numberPhrase);
                _table.AddCell(numberCell);

            }


        }

        private string GetTranslation(XElement node)
        {
            XElement el = node.Element("Translations").Elements("Translation").SingleOrDefault(x => x.Attribute("culture").Value == _sdc.Culture);
            if (el == null) return string.Empty;
            return el.Value;
        }

        private decimal GetSaldi(XElement node)
        {
            decimal saldi = 0;
            // traverse regions
            foreach (XElement rangeNode in node.Element("AccountRegions").Elements("AccountRegion"))
            {

                string range = rangeNode.Attribute("range").Value;
                if (rangeNode.Attributes("exclude").Count() > 0)
                {
                    string exclude = rangeNode.Attribute("exclude").Value;
                    saldi += _accounts.Where(a => a.InternalNr.StartsWith(range) && !a.InternalNr.StartsWith(exclude)).Sum(a => a.Saldo);
                }
                else
                {
                    saldi += _accounts.Where(a => a.InternalNr.StartsWith(range)).Sum(a => a.Saldo);
                }
            }
            return saldi;
        }

        private decimal GetPreviousSaldi(XElement node)
        {
            decimal saldi = 0;
            // traverse regions
            foreach (XElement rangeNode in node.Element("AccountRegions").Elements("AccountRegion"))
            {

                string range = rangeNode.Attribute("range").Value;
                if (rangeNode.Attributes("exclude").Count() > 0)
                {
                    string exclude = rangeNode.Attribute("exclude").Value;
                    saldi += _accounts.Where(a => a.InternalNr.StartsWith(range) && !a.InternalNr.StartsWith(exclude)).Sum(a => a.PreviousSaldo);
                }
                else
                {
                    saldi += _accounts.Where(a => a.InternalNr.StartsWith(range)).Sum(a => a.PreviousSaldo);
                }
            }
            return saldi;
        }

        private bool HasValidAccounts(XElement node, bool includePrev)
        {
            if (includePrev)
            {
                // traverse regions
                foreach (XElement rangeNode in node.Element("AccountRegions").Elements("AccountRegion"))
                {

                    string range = rangeNode.Attribute("range").Value;
                    if (rangeNode.Attributes("exclude").Count() > 0)
                    {
                        string exclude = rangeNode.Attribute("exclude").Value;
                        if (_accounts.Where(a => a.InternalNr.StartsWith(range)
                                                && !a.InternalNr.StartsWith(exclude)
                                                && (a.Saldo != 0 || a.PreviousSaldo != 0)).Count() > 0)
                        {
                            return true;
                        }

                    }
                    else
                    {
                        if (_accounts.Where(a => a.InternalNr.StartsWith(range)
                                                && (a.Saldo != 0 || a.PreviousSaldo != 0)).Count() > 0)
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {

                // traverse regions
                foreach (XElement rangeNode in node.Element("AccountRegions").Elements("AccountRegion"))
                {

                    string range = rangeNode.Attribute("range").Value;
                    if (rangeNode.Attributes("exclude").Count() > 0)
                    {
                        string exclude = rangeNode.Attribute("exclude").Value;
                        if (_accounts.Where(a => a.InternalNr.StartsWith(range)
                                                && !a.InternalNr.StartsWith(exclude)
                                                && a.Saldo != 0).Count() > 0)
                        {
                            return true;
                        }

                    }
                    else
                    {
                        if (_accounts.Where(a => a.InternalNr.StartsWith(range)
                                                && a.Saldo != 0).Count() > 0)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private List<AccountDataContract> GetAccounts(XElement node)
        {
            List<AccountDataContract> list = new List<AccountDataContract>();

            // traverse regions
            foreach (XElement rangeNode in node.Element("AccountRegions").Elements("AccountRegion"))
            {
                List<AccountDataContract> accountsInRange = new List<AccountDataContract>();
                string range = rangeNode.Attribute("range").Value;
                if (rangeNode.Attributes("exclude").Count() > 0)
                {
                    string exclude = rangeNode.Attribute("exclude").Value;
                    accountsInRange = _accounts.Where(a => a.InternalNr.StartsWith(range) && !a.InternalNr.StartsWith(exclude)).ToList();
                }
                else
                {
                    accountsInRange = _accounts.Where(a => a.InternalNr.StartsWith(range)).ToList();
                }
                foreach (AccountDataContract adc in accountsInRange)
                {
                    if (!list.Contains(adc) && adc.Saldo != 0) list.Add(adc);
                }
            }
            return list.OrderBy(a => a.InternalNr).ToList();
        }
       


        #region IDisposable Members

        public void Dispose()
        {
            _accounts = null;
            _table = null;
            _font = null;
            bf = null;
            _cultureInfo = null;
            _statementXDoc = null;
            _sdc = null;

            base.Dispose();
        }

        #endregion
 */
    }
}
