﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using System.Configuration;
using System.ServiceModel;
using iTextSharp.text;
using System.Xml;
using System.Xml.Linq;
using iTextSharp.text.pdf;
using System.Collections;
using System.Globalization;
using System.util;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.Core.IText.Render
{
    public class IndexItemPager {
        public IndexItemBaseDataContract Id  {get;set;}
        public int StartPage  {get;set;}
        public int EndPage  {get;set;}
    }

    public class BundleRenderer : ITextRenderer
    {
        private BundleDataContract _bdc;
        private HeaderConfigDataContract _defaultHeader;
        private FooterConfigDataContract _defaultFooter;
        private int pageOffset = 0;
        private List<int> empties = new List<int>();
        private List<int> nopaging = new List<int>();
        //private List<
        private int chapter = 0;
        private bool _index = false;
        private int _indexNr = 1;
        private XElement _header;
        private XElement _footer;
        private int lastEmptyChapter=0;
        private bool _showPagenr;
        

        public BundleRenderer(BundleDataContract bdc, HeaderConfigDataContract defaultHeader, FooterConfigDataContract defaultFooter
            , XElement header)
            :base(null,null)
        {
            _bdc = bdc;
            _defaultHeader = defaultHeader;
            _defaultFooter = defaultFooter;
            _header = header;
           

        }

       

        public override void HandleDocument()
        {
            writer.SetLinearPageMode();
            //Set font
            //  CreateDocument();
            _document.SetMargins(60f, 60f, 95f, 45f);


            WriteBundleItems(_bdc.Contents,null);

            int startPage = writer.CurrentPageNumber+1;
            if (lastEmptyChapter == (writer.CurrentPageNumber - 1)) startPage = writer.CurrentPageNumber;
       //     TextWriter tw = File.CreateText(@"c:\temp\bookmarks.xml");
            
            // write index at end

            if (_index)
            {                
                int j = 0;
                int pageShift = CreateIndexInMem(writer.RootOutline.Kids);
                if (pageShift % 2 != 0 && _bdc.RectoVerso) pageShift++; //(ensure recto verso

                CreateIndex(writer.RootOutline.Kids,_document,pageShift);
                _document.NewPage();
                //_document.Add(new Phrase(""));
                if ((writer.CurrentPageNumber - startPage % 2) != 0 && _bdc.RectoVerso)
                {
                    _document.Add(new Chunk(""));
                    empties.Add(writer.CurrentPageNumber);
                    _document.NewPage();
                }
                int pgAfter = writer.CurrentPageNumber;

                List<int> indexEmpties = new List<int>();

                List<int> reorder = new List<int>();
                for (int i = 1; i < _indexNr; i++)
                {
                    reorder.Add(i);
                }
                if (startPage == writer.CurrentPageNumber) startPage--;
                for (int i = startPage; i < pgAfter; i++)
                {
                    j++;
                    reorder.Add(i);
                    if (empties.Contains(i))
                    {
                        indexEmpties.Add(reorder.Count);
                        empties.Remove(i);
                    }
                }
                for (int i = _indexNr; i < startPage; i++)
                {
                    reorder.Add(i);
                }
                
                writer.ReorderPages(reorder.ToArray());
                ReIndexEmpties(_indexNr, j);
                foreach (int empt in indexEmpties)
                {
                    if (!empties.Contains(empt)) empties.Add(empt);
                }
                ReIndexPaging(_indexNr-1, j);

              //  SimpleBookmark.ShiftPageNumbers(writer.Outlines, j, null);
            }
            


            //writer.ReorderPages();
            
           
            //CloseDocument();


        }

        public void ReIndexPaging(int startfrom, int shift)
        {
            List<int> nw = (from e in nopaging
                            select (e > startfrom) ? e + shift : e).ToList();
            nopaging = nw;
            
        }
        

        public void ReIndexEmpties(int startfrom, int shift)
        {
            List<int> nw = (from e in empties
                            select (e > startfrom) ? e + shift : e).ToList();
            empties = nw;
            
        }
        float mxWid = 0;


        public int CreateIndexInMem(ArrayList bookmarks)
        {
            MemoryStream ms = new MemoryStream();
            iTextSharp.text.Document memDoc = new iTextSharp.text.Document();
            PdfWriter memwriter = PdfWriter.GetInstance(memDoc, ms);
            memwriter.CloseStream = false;
            memDoc.SetMargins(_document.LeftMargin, _document.RightMargin, _document.TopMargin, _document.BottomMargin);
            memDoc.SetPageSize(PageSize.A4);
            memDoc.Open();
            CreateIndex(bookmarks, memDoc, -1);
            int pages = memwriter.CurrentPageNumber;
            memDoc.Close();
            memwriter.Close();
            //ms.Position = 0;
            //PdfReader pr = new PdfReader(ms);
            //pages = pr.NumberOfPages;
            //pr.Close();
            ms.Close();
            
            return pages;
        }

        public void CreateIndex(ArrayList bookmarks, iTextSharp.text.Document idxDoc)
        {
            CreateIndex(bookmarks, idxDoc, 0);
        }

        public void CreateIndex(ArrayList bookmarks,iTextSharp.text.Document idxDoc, int pageShift)
        {
          
            idxDoc.SetPageSize(PageSize.A4);
            idxDoc.NewPage();
            PdfPTable rootTable = new PdfPTable(1);
            rootTable.KeepTogether = true;
            rootTable.SplitLate = false;
            rootTable.SplitRows = true;
            rootTable.WidthPercentage = 90f;
            float rootWidth = (PageSize.A4.Width - idxDoc.LeftMargin - idxDoc.RightMargin);// * (rootTable.WidthPercentage / 100));
            rootTable.TotalWidth = rootWidth;
            mxWid = rootWidth * 0.9f * 0.9f;
            // move translations to database
            // and out of renderer. For now:

            PdfPTable titleTable = new PdfPTable(2);
            titleTable.SetWidths(new float[] { 90f, 10f });
            Font titleFont = new Font(bf, 12);
            titleFont.SetStyle(Font.BOLD | Font.UNDERLINE);
            Phrase ph = null;
            PdfPCell pCell = null;

            switch (_bdc.Culture.ToLower())
            {
                case "nl-be":
                    ph = new Phrase("Inhoudstafel", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    titleTable.AddCell(pCell);
                    if (_defaultFooter != null && _defaultFooter.ShowPageNr)
                    {
                        ph = new Phrase("Blz.", titleFont);
                    }
                    else
                    {
                        ph = new Phrase(" ", titleFont);
                    }
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    pCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    titleTable.AddCell(pCell);
                    break;
                case "fr-fr":
                    ph = new Phrase("Table des matières", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    titleTable.AddCell(pCell);
                    if (_defaultFooter != null && _defaultFooter.ShowPageNr)
                    {
                        ph = new Phrase("Page", titleFont);
                    }
                    else
                    {
                        ph = new Phrase(" ", titleFont);
                    }
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    pCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    titleTable.AddCell(pCell);
                    break;
                default:
                    ph = new Phrase("Index", titleFont);
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    titleTable.AddCell(pCell);

                    if (_defaultFooter != null && _defaultFooter.ShowPageNr)
                    {
                        ph = new Phrase("Page", titleFont);
                    }
                    else
                    {
                        ph = new Phrase(" ", titleFont);
                    }
                    pCell = new PdfPCell(ph);
                    pCell.Border = 0;
                    pCell.BorderWidth = 0;
                    pCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    titleTable.AddCell(pCell);
                    break;
            }
            PdfPCell titleCell = new PdfPCell(titleTable);
            titleCell.Border = 0;
            titleCell.BorderWidth = 0;
            rootTable.AddCell(titleCell);

            Phrase spacerPhrase = new Phrase(" ", GetFont(0));
            PdfPCell spacerCell = new PdfPCell(spacerPhrase);
            spacerCell.Border = 0;
            rootTable.AddCell(spacerCell);

            rootTable.AddCell(RenderLevel(0, bookmarks, rootWidth * 0.9f, pageShift, (_defaultFooter != null && _defaultFooter.ShowPageNr)));
            idxDoc.Add(rootTable);
        }

        protected void RenderHeaderFooter(PdfContentByte over, XElement xmlNode, Rectangle rect, string title)
        {
            RenderHeaderFooter(0, over, xmlNode, rect, title);
        }

        protected void RenderHeaderFooter(int rotate, PdfContentByte over, XElement xmlNode, Rectangle rect,string title)
        {
            bool isLandscape = rect.Height < rect.Width;
            foreach (XElement block in xmlNode.Elements("block"))
            {
                float top = 0f;
                float left = 0f;
                //float.Parse(block.Attributes["top"].Value, NumberFormatInfo.InvariantInfo);
                int align = Element.ALIGN_UNDEFINED;
                switch (block.Attribute("type").Value.ToUpper())
                {
                    case "TOPLEFT":
                        align = Element.ALIGN_LEFT;
                        top = isLandscape ? 20f : 20f;
                        left = isLandscape ? 50f : 20f;
                        break;
                    case "TOPRIGHT":
                        align = Element.ALIGN_RIGHT;
                        top = isLandscape ? 20f : 20f;
                        left = -50f;
                        break;
                    case "TOPTITLE":
                        align = Element.ALIGN_LEFT;
                        top = 85f;
                        left = 60f;
                        break;
                    case "BOTTOMCENTER":
                        align = Element.ALIGN_CENTER;
                        top = -50f;
                        left = isLandscape ? 440f : 300f;

                        if (rotate!=0)
                        {
                            top = 440f; //rect.Width / 2; //left;
                            left = rect.Height - 25f;
                            if (rotate == 270) left = 25f;
                        }
                        break;
                    case "COLUMNHEADER":
                        align = Element.ALIGN_RIGHT;
                        top = 100f;
                        left = -50f;
                        break;
                }

                if (rotate == 0)
                {
                    if (left < 0)
                    {
                        left += rect.Width;
                    }
                    if (top > 0)
                    {
                        top = rect.Height - top;
                    }
                    else
                    {
                        top = Math.Abs(top);
                    }
                }

                foreach (XElement subNode in block.Elements("phrase"))
                {
                    Properties attributes = GetAttributes(subNode);

                    switch (subNode.Name.LocalName.ToUpper())
                    {
                        case "PHRASE":
                            string itemText = subNode.Value;

                            itemText = itemText.Replace("[VAR::TITLE]", title);
                            //itemText = itemText.Replace("[PAGE_TOTAL]", totalPages.ToString());

                            Phrase phText = new Phrase(itemText, GetFont(subNode));

                            ColumnText.ShowTextAligned(over, align, phText, left, top, rotate);

                            float padding = 2;
                            if (attributes.ContainsKey("padding"))
                            {
                                padding = float.Parse(attributes["padding"], NumberFormatInfo.InvariantInfo);
                            }

                            top -= (phText.Font.Size + padding);
                            break;
                        /*  case "IMAGE":
                              Image img = ElementFactory.GetImage(attributes);
                              img.SetAbsolutePosition(left, top);
                              if (attributes.ContainsKey("scalepercent"))
                              {
                                  img.ScalePercent(float.Parse(attributes["scalepercent"], NumberFormatInfo.InvariantInfo));
                              }
                              over.AddImage(img);
                              break;*/
                    }
                }
            }
        }


        private XElement GetFooter(FooterConfigDataContract fcg)
        {
            if (fcg!=null && fcg.Enabled)
            {
                string footerline = "";
                if(fcg.ShowFooterNote) {
                    footerline= _defaultFooter.FootNote;
                }
                if (footerline == null) footerline = string.Empty;
                return new XElement("data",
                    new XElement("block",
                        new XAttribute("type", "BOTTOMCENTER"),
                        new XElement("phrase",
                            new XAttribute("font", Config.StandardFont),
                            new XAttribute("size", "10"),
                            new XCData(" ")),
                        new XElement("phrase",
                            new XAttribute("font", Config.StandardFont),
                            new XAttribute("size", "8"),
                            new XCData(footerline))
                    )
                );
            }
            return new XElement("data");
        }

        public override void CloseDocument()
        {
            base.CloseDocument();

            
            // stamping time.

            PdfReader reader = new PdfReader(_pdfDataContract.PDFLocation);
            _pdfDataContract.PDFLocation = _pdfDataContract.PDFLocation.Replace(".pdf","_stamped.pdf");
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    SimpleBookmark.ExportToXML(SimpleBookmark.GetBookmark(reader), ms, "UTF-8", false);
            //    ms.Position = 0;
            //    XDocument xdoc = XDocument.Load(XmlReader.Create(ms));
            //    xdoc.Save(_pdfDataContract.PDFLocation.Replace(".pdf", ".xml"));
            //}
            //ArrayList bookmarks = SimpleBookmark.GetBookmark(reader);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(_pdfDataContract.PDFLocation, FileMode.Create));

            XElement pager = new XElement("data",
                new XElement("block",
                    new XAttribute("type", "BOTTOMCENTER"),
                    new XElement("phrase",
                        new XAttribute("font", Config.StandardFont),
                        new XAttribute("size", "10"),
                        new XCData(" ")),
                    new XElement("phrase",
                        new XAttribute("font", Config.StandardFont),
                        new XAttribute("size", "8"),
                        new XCData(" ")),
                    new XElement("phrase",
                        new XAttribute("font", Config.StandardFont),
                        new XAttribute("size", "10"),
                        new XCData("[PAGELINE]"))
                )
                );
            

            for (int i = 1; i <=reader.NumberOfPages; i++)
            {
                int rotateMe = 0;
                Rectangle pageSize = stamper.Reader.GetPageSize(i);
                bool landscape = pageSize.Width > pageSize.Height;
                if (!landscape && (pageSize.Rotation == 90 || pageSize.Rotation == 180))
                {
                    landscape = true;
                }
                if (landscape && _bdc.RectoVerso)
                {
                    PdfDictionary pageDict = reader.GetPageN(i);
                    pageDict.Put(PdfName.ROTATE, new PdfNumber(90));
                    rotateMe = 90;
                }
                if (!empties.Contains(i))
                {
                    
                    if (_bdc.Draft)
                    {

                        string dpath = Path.Combine(Config.PdfImageResource, "draft.png");
                        Image img = Image.GetInstance(dpath);
                        img.ScalePercent(80);

                        PdfContentByte under = stamper.GetUnderContent(i);
                        under.PdfDocument.SetPageSize(pageSize);

                        if (rotateMe > 0)
                        {
                            //float a = (float)Math.Cos(rotateMe * (float)Math.PI / 180) * img.ScaledHeight;
                            //float b = (float)Math.Sin(rotateMe * (float)Math.PI / 180) * img.ScaledWidth;
                            //float c = -(float)Math.Sin(rotateMe * (float)Math.PI / 180) * img.ScaledHeight;
                            //float d = (float)Math.Cos(rotateMe * (float)Math.PI / 180) * img.ScaledWidth;
                            img.SetAbsolutePosition(50, 150);
                            //img.RotationDegrees = 90f;
                            img.RotationDegrees = Math.Abs(360f-rotateMe);
                            under.AddImage(img);
                        }
                        else
                        {

                            if (landscape)
                            {
                                img.SetAbsolutePosition(150, 50);
                            }
                            else
                            {
                                img.SetAbsolutePosition(50, 150);
                            }
                            under.AddImage(img);
                        }

                        

                    }
                    // set paging
                   if (!nopaging.Contains(i)) RenderHeaderFooter(landscape && _bdc.RectoVerso ? Math.Abs(360 - rotateMe) : 0, stamper.GetOverContent(i), XElement.Parse(pager.ToString().Replace("[PAGELINE]", string.Format("{0}/{1}", i, reader.NumberOfPages))), stamper.Reader.GetPageSize(i), "");

                   
                }

            }
            stamper.Close();
            
            

        }

        private void InsertRectoVerso()
        {
            if (_bdc.RectoVerso && writer.CurrentPageNumber % 2 == 1)
            {
                _document.NewPage();
                _document.Add(new Phrase(" "));
                empties.Add(writer.CurrentPageNumber);
                pageOffset++;
            }
        }

        private void InsertRectoVersoReversed()
        {
            if (_bdc.RectoVerso && writer.CurrentPageNumber % 2 == 0)
            {
                _document.NewPage();
                _document.Add(new Phrase(" "));
                empties.Add(writer.CurrentPageNumber);
                pageOffset++;
            }
        }

        private List<Guid> PdfGuids = new List<Guid>();

        public void WriteBundleItems(List<IndexItemBaseDataContract> index, PdfOutline parentOutline)
        {
            // all items rendered from standard eBook contents are pre-rendered
            // and inserted as pdfdatacontracts.
            // Only index and chapters remain.
            PdfOutline po = null;
            int itCount = 0;
            foreach (IndexItemBaseDataContract item in index)
            {
                Chunk dest = new Chunk(" ");

                switch (item.GetType().Name)
                {
                    case "ChapterDataContract":
                        ChapterDataContract chdc = (ChapterDataContract)item;
                        _document.SetPageSize(PageSize.A4);
                        if (itCount == 0) { InsertRectoVersoReversed(); } else { InsertRectoVerso(); }

                        _document.NewPage();
                        
                        
                        //_document.Add(new Chunk(" "));
                        //RenderHeaderFooter(writer.DirectContent, _header, _document.PageSize,"");
                        

                        if (chdc.ShowInIndex)
                        {
                            PdfAction caction = PdfAction.GotoLocalPage(chdc.Id.ToString(), true);
                            if (parentOutline != null)
                            {
                                //po = new PdfOutline(parentOutline, PdfAction.GotoLocalPage(writer.CurrentPageNumber, new PdfDestination(writer.CurrentPageNumber), writer.DirectContent.PdfWriter), item.Title);

                                po = new PdfOutline(parentOutline, caction, item.Title);
                            }
                            else
                            {
                                //po = new PdfOutline(writer.DirectContent.RootOutline, PdfAction.GotoLocalPage(writer.CurrentPageNumber, new PdfDestination(writer.CurrentPageNumber), writer.DirectContent.PdfWriter), item.Title);
                                po = new PdfOutline(writer.DirectContent.RootOutline, caction, item.Title);
                            }
                            po.Tag = string.Format("{0}#{1}", chdc.Id.ToString(), writer.CurrentPageNumber);
                        }
                        else
                        {
                            foreach (IndexItemBaseDataContract subItem in chdc.Items)
                            {
                                subItem.ShowInIndex = false;
                            }
                        }
                        
                        PdfPTable table = new PdfPTable(1);
                        table.SetTotalWidth(new float[]
                                                                             {
                                                                                 _document.PageSize.Width - _document.LeftMargin -
                                                                                 _document.BottomMargin
                                                                             });
                        table.LockedWidth = true;

                        Font font = new Font(bf, 14, Font.BOLD);
                        Chunk ph = new Chunk(chdc.Title, font);
                        //ph.SetLocalDestination("PG_" + writer.CurrentPageNumber);
                        ph.SetLocalDestination(chdc.Id.ToString());

                        PdfPCell cell = new PdfPCell(new Phrase(ph));

                        cell.FixedHeight = _document.PageSize.Height - _document.TopMargin -_document.BottomMargin;
                        cell.Padding = 0;
                        cell.PaddingBottom = 160;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.Border = 0;
                        //cell.Width = table.TotalWidth;
                      //  cell.Height = table.TotalHeight;
                       

                       
                        table.AddCell(cell);
                        _document.Add(table);
                        
                        nopaging.Add(writer.CurrentPageNumber-1);
                        
                        
                        WriteBundleItems(chdc.Items,po);
                        if (chdc.Items.Count == 0 && !_bdc.RectoVerso) lastEmptyChapter = writer.CurrentPageNumber - 1;
                        if (chdc.Items.Count == 0 && _bdc.RectoVerso) InsertRectoVersoReversed();
                      //  if (chdc.Items.Count == 0 && !_bdc.RectoVerso) _document.NewPage();
                        break;
                    case "IndexPageDataContract":
                        _index = true;
                        _indexNr = writer.CurrentPageNumber+1;
                        break;
                    case "PDFDataContract":
                        if (_pageSize != null && _document != null)
                        {
                            if (_pageSize == _pageSizeLandscape)
                                _document.SetPageSize(_pageSizePortret);
                        }
                        PDFDataContract pdc = (PDFDataContract)item;
                        if (!System.IO.File.Exists(pdc.PDFLocation))
                        {
                            throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -1, Message = string.Format("Pdf file not found: {0} {1} {2}",pdc.Title,pdc.PDFLocation, _bdc.Name) });
                        }
                        PdfReader reader = new PdfReader(pdc.PDFLocation);
                        reader.ConsolidateNamedDestinations();
                        
                        int n = reader.NumberOfPages;

                        int offset = writer.CurrentPageNumber;
                        

                        ArrayList bkmrks = SimpleBookmark.GetBookmark(reader);
                        List<int> pdfnopages = new List<int>();

                       

                        if (pdc.Title.ToLower().Contains("biztax") && !pdfnopages.Contains(1)) pdfnopages.Add(1);
                        //SimpleBookmark.ShiftPageNumbers(bkmrks, writer.CurrentPageNumber, null);
                        
                        
                       if (itCount == 0) { InsertRectoVersoReversed(); } else { InsertRectoVerso(); }

                        PdfImportedPage page;
                        for (int i = 1; i <= n; i++)
                        {
                            page = writer.GetImportedPage(reader, i);
                            //page.re
                            
                            Rectangle rec = reader.GetPageSize(i);
                            Rectangle newRec = new Rectangle(rec.Width,rec.Height);

                            bool rotated = newRec.Width > rec.Height;

                            _document.SetPageSize(newRec);
                            
                            _document.NewPage();
                           // _document.Add(new Phrase(" PDF:: " + pdc.Title));
                            Chunk chunk = new Chunk(" ");
                            
                            if (i == 1)
                            {
                                if (pdc.ShowInIndex)
                                {
                                    Guid pid = Guid.NewGuid();
                                    if (!PdfGuids.Contains(pdc.Id)) pid = pdc.Id;
                                    chunk.SetLocalDestination(pid.ToString());
                                    PdfAction paction = PdfAction.GotoLocalPage(pid.ToString(), true);
                                    if (parentOutline != null)
                                    {
                                        po = new PdfOutline(parentOutline, paction, item.Title);
                                    }
                                    else
                                    {
                                        po = new PdfOutline(writer.DirectContent.RootOutline, paction, item.Title);
                                    }
                                    po.Tag = string.Format("{0}#{1}", pid.ToString(), writer.CurrentPageNumber);
                                    PdfGuids.Add(pid);
                                    if (pdc.Title.ToLower().Contains("biztax") && bkmrks != null && bkmrks.Count > 0)
                                    {

                                        pdfnopages = AddBookmarks(bkmrks, ref po, offset);
                                        // po = new PdfOutline(writer.DirectContent.RootOutline, PdfAction.GotoLocalPage(writer.CurrentPageNumber, new PdfDestination(writer.CurrentPageNumber), writer.DirectContent.PdfWriter), item.Title);
                                    }
                                    
                                }
                                
                            }
                            _document.Add(chunk);
                            
                            
                            if (pdc.HeaderConfig != null && pdc.HeaderConfig.Enabled == true)
                            {
                                if (!pdfnopages.Contains(i))
                                {
                                    RenderHeaderFooter(writer.DirectContent, _header, _document.PageSize, pdc.HeaderConfig.ShowTitle ? pdc.Title : "");
                                }
                            }
                            if (pdc.FooterConfig != null && pdc.FooterConfig.Enabled == true)
                            {
                                if (!pdfnopages.Contains(i))
                                {
                                    RenderHeaderFooter(writer.DirectContent, GetFooter(pdc.FooterConfig), _document.PageSize, "");
                                }
                            }
                            if (pdc.FooterConfig == null || pdc.FooterConfig.Enabled == false || pdc.FooterConfig.ShowPageNr == false)
                                nopaging.Add(writer.CurrentPageNumber);
                           // dest.SetLocalDestination("PG_" + writer.CurrentPageNumber);
                            //_document.Add(dest);
                            

                            int rotation = reader.GetPageRotation(i);
                            if (rotation == 90)
                            {
                                writer.DirectContentUnder.AddTemplate(page, 0, -1.0F, 1.0F, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                            }
                            else if (rotation == 270 && rotated)
                            {
                                writer.DirectContentUnder.AddTemplate(page, 0, 1.0F, -1.0F, 0, reader.GetPageSizeWithRotation(i).Width + 60, -30);
                            }
                            else
                            {
                                writer.DirectContentUnder.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0);
                            }
                            
                            
                        }

                        
                        
                        if (n == 1 && _bdc.RectoVerso && pdc.Title == "COVERPAGE")
                        {
                            _document.NewPage();
                            _document.Add(new Chunk(" "));
                            empties.Add(writer.CurrentPageNumber);
                        }

                        reader.Close();
                        
                        break;
                }
                itCount++;
            }
        }

        private List<int> AddBookmarks(ArrayList bkmrks, ref PdfOutline parent, int offset)
        {
            List<int> nobookmarkpages = new List<int>();
            for (int i = 0; i < bkmrks.Count; i++)
            {
                
                Hashtable ht = (Hashtable)bkmrks[i];
                string title = ht["Title"].ToString();
                string name = ht["Named"].ToString();
                string[] parts = name.Split(new char[] { '#' });
                int pg = 0;
                int.TryParse(parts[1], out pg);
                nobookmarkpages.Add((pg-1));
                pg += (offset-1);
                
                PdfAction paction = PdfAction.GotoLocalPage(parts[0], true);
                PdfOutline po = new PdfOutline(parent, paction, title);
                po.Tag = string.Format("{0}#{1}",parts[0],pg);
                //string page = ht["Page"].ToString().Split(new char[] {' '})[0];
                //int pg = int.Parse(page);
                //PdfDestination dest = new PdfDestination(PdfDestination.FIT);
                //dest.AddPage(writer.GetPageReference(pg));
                //PdfOutline po = new PdfOutline(parent, dest, title);
                //po.p
                if (ht.ContainsKey("Kids"))
                {
                    List<int> addnoheads = AddBookmarks((ArrayList)ht["Kids"], ref po, offset);
                    if(addnoheads.Count>0) nobookmarkpages.AddRange(addnoheads);
                }

            }
            return nobookmarkpages;
        }

        private PdfPCell RenderLevel(int level, ArrayList items, float innerTableWidth, int pageShift, bool pagenr)
        {
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(new float[] { 90f, 10f });
            table.WidthPercentage = 90f;
            table.TotalWidth = innerTableWidth;
            float mxWd = (PageSize.A4.Width - _document.LeftMargin - _document.RightMargin);// * (rootTable.WidthPercentage / 100));
            int headCounter = 1;

            //  IndexPageDataContract ipdc = (IndexPageDataContract)items.First(i => i.GetType().Name == "IndexPageDataContract");
            bool start = level > 0;


            foreach (PdfOutline map in items) 
            {

                

                if (level == 0)
                {
                    Phrase spacerPH = new Phrase(" ");
                    PdfPCell spacerCL = new PdfPCell(spacerPH);
                    spacerCL.Border = 0;
                    spacerCL.BorderWidth = 0;
                    spacerCL.Colspan = 2;
                    table.AddCell(spacerCL);
                }

                //int pg = map.action.PageNr + pageShift;
                //int pg=0;
                //if (map.action != null)
                //{
                //    pg = pg = map.action.PageNr + pageShift;
                //}
                //else
                //{
                //    pg = map.PdfDestination.GetPageNum();
                //}
                string[] tagParts = map.Tag.Split(new char[] { '#' });
                int pg = 0;
                int.TryParse(tagParts[1], out pg);
                pg += pageShift;
                if (map.Kids != null && map.Kids.Count > 0)
                {
                    string title = string.Format("{0}. {1}", headCounter, map.Title);
                    title = string.Concat("".PadLeft(level * 10, ' '), title);
                    Chunk ct = new Chunk(title, GetFont(level));
                   // ct.SetAction(map.action);
                    
                    //if (map.action != null)
                    //{
                    //     if(pageShift> -1)  ct.SetLocalGoto("PG_" + map.action.PageNr);
                    //}
                    //else
                    //{
                    //    ct.SetAction(PdfAction.GotoLocalPage(map.PdfDestination.GetPageNum(), map.PdfDestination, writer));
                    //}
                    ct.SetAction(PdfAction.GotoLocalPage(tagParts[0], false));
                    Paragraph headingPhrase = new Paragraph(ct);
                    
                    PdfPCell sctitleCell = new PdfPCell(headingPhrase);
                    sctitleCell.Border = 0;
                    sctitleCell.BorderWidth = 0;
                    sctitleCell.Colspan = 2;
                    table.AddCell(sctitleCell);
                    
                    PdfPCell chapterContents = RenderLevel((level + 1), map.Kids, innerTableWidth * 0.9f,pageShift,pagenr);
                    table.AddCell(chapterContents);
                } else {

                   // PdfAction action = PdfAction.GotoLocalPage(map.Tag, false);

                    table.AddCell(CreateIndexCell(level, string.Format("{0}. {1}", headCounter, map.Title), innerTableWidth, map, pageShift,pagenr));
                    
                    PdfPCell cpageCell = new PdfPCell(new Phrase(pagenr ? pg.ToString(): " ", GetFont(level)));
                    cpageCell.Border = 0;
                    cpageCell.BorderWidth = 0;
                    cpageCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    table.AddCell(cpageCell);
                }
                headCounter++;

                //switch (iibdc.GetType().Name)
                //{
                //    case "ChapterDataContract":
                //        ChapterDataContract cdc = (ChapterDataContract)iibdc;
                //        if (!cdc.NoCoverpage)
                //        {
                //            _pageCounter = _pageCounter + 2;
                //        }
                //        if (cdc.Items != null && cdc.Items.Where(it => it.ShowInIndex).Count() > 0)
                //        {
                //            if (iibdc.ShowInIndex && start)
                //            {
                //                string title = string.Format("{0}. {1}", headCounter, cdc.Title);
                //                title = string.Concat("".PadLeft(level * 10, ' '), title);
                //                Phrase headingPhrase = new Phrase(title, GetFont(level));
                //                PdfPCell sctitleCell = new PdfPCell(headingPhrase);
                //                sctitleCell.Border = 0;
                //                sctitleCell.BorderWidth = 0;
                //                sctitleCell.Colspan = 2;
                //                table.AddCell(sctitleCell);

                //            }
                //            PdfPCell chapterContents = RenderLevel((level + 1), cdc.Items, ref writer, innerTableWidth * 0.9f);
                //            if (iibdc.ShowInIndex && start)
                //            {
                //                table.AddCell(chapterContents);
                //            }
                //        }
                //        else
                //        {
                //            if (iibdc.ShowInIndex && start)
                //            {
                //                table.AddCell(CreateIndexCell(level, string.Format("{0}. {1}", headCounter, cdc.Title), innerTableWidth * 0.9f));
                //                PdfPCell cpageCell = new PdfPCell(new Phrase((_pageCounter + 1).ToString(), GetFont(level)));
                //                cpageCell.Border = 0;
                //                cpageCell.BorderWidth = 0;
                //                cpageCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                //                table.AddCell(cpageCell);
                //            }
                //            PdfPCell chapteritems = null;
                //            if (cdc.Items != null)
                //            {
                //                chapteritems = RenderLevel((level + 1), cdc.Items, ref writer, innerTableWidth * 0.9f);
                //            }
                //        }
                //        if (start) headCounter++;
                //        break;
                //    case "PDFDataContract":
                //        PDFDataContract pdc = (PDFDataContract)iibdc;
                //        int from = 1;
                //        int to = pdc.Pages != 0 ? pdc.Pages : 1;
                //        if (pdc.FromPage.HasValue) from = pdc.FromPage.Value;
                //        if (pdc.ToPage.HasValue) to = pdc.ToPage.Value;
                //        int pages = to - from + 1;
                //        if (iibdc.ShowInIndex && start)
                //        {
                //            table.AddCell(CreateIndexCell(level, string.Format("{0}. {1}", headCounter, pdc.Title), innerTableWidth * 0.9f));
                //            PdfPCell pageCell = new PdfPCell(new Phrase((_pageCounter + 1).ToString(), GetFont(level)));
                //            pageCell.Border = 0;
                //            pageCell.BorderWidth = 0;
                //            pageCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                //            table.AddCell(pageCell);
                //        }
                //        _pageCounter = _pageCounter + pages;
                //        if (pages % 2 != 0) _pageCounter++;
                //        if (start) headCounter++;
                //        break;
                //}

            }

            PdfPCell contentCell = new PdfPCell(table);
            contentCell.Border = 0;
            contentCell.BorderWidth = 0;
            if (level > 0) contentCell.Colspan = 2;
            return contentCell;
        }

        private PdfPCell CreateIndexCell(int level, string title, float maxWidth, PdfOutline map,int pageShift, bool pagenr)
        {
            string[] tagParts = map.Tag.Split(new char[] { '#' });
            List<string> multipleLines = new List<string>();
            Font font = GetFont(level);

            float spaceSize = new Chunk(" ", font).GetWidthPoint();
            float pointSize = new Chunk(".", font).GetWidthPoint();
            maxWidth = (mxWid) - (pointSize * 2); //* 0.9f * 0.9f

            int startText = title.IndexOf(' ') + 3;
            title = string.Concat("".PadLeft(level * 10, ' '), title);
            Phrase goodPhrase;
            Phrase chk = new Phrase(title, font);
            float titleWidth = GetWidth(chk) + 10;

            if (titleWidth > maxWidth)
            {
                int startIdx = 0;
                while (titleWidth > maxWidth)
                {

                    string titlePart = title;
                    Phrase fChk = new Phrase(titlePart, font);
                    while ((GetWidth(fChk) + 10) > maxWidth)
                    {
                        titlePart = titlePart.Substring(0, titlePart.LastIndexOf(' '));
                        fChk = new Phrase(titlePart, font);
                    }
                    multipleLines.Add(titlePart);
                    title = title.Substring(titlePart.Length).Trim();
                    title = string.Concat("".PadLeft((level * 10) + startText, ' '), title);
                    chk = new Phrase(title, font);
                    titleWidth = GetWidth(chk) + 10;

                }
            }

            float pointsWidth = 0;
            while (titleWidth + pointsWidth < (maxWidth))
            {
                title = title + ".";
                pointsWidth += pointSize;
            }
            if (multipleLines.Count > 0)
            {
                string finalTitle = "";
                foreach (string titlePart in multipleLines)
                {
                    finalTitle = finalTitle + titlePart + Chunk.NEWLINE;
                }
                title = finalTitle + title;
            }

            Chunk ct = new Chunk(title, font);
            //if (map.action != null)
            //{
            //    if (pageShift > -1) ct.SetLocalGoto("PG_" + map.action.PageNr);
            //}
            //else
            //{
            //    ct.SetAction(PdfAction.GotoLocalPage(map.PdfDestination.GetPageNum(), map.PdfDestination, writer));
            //}
            ct.SetAction(PdfAction.GotoLocalPage(tagParts[0], false));
            //ct.SetAction(map.action);
            Paragraph headingPhrase = new Paragraph(ct);
            PdfPCell cell = new PdfPCell(headingPhrase);

            cell.Border = 0;
            cell.BorderWidth = 0;
            return cell;
        }

        private float GetWidth(Phrase phrase)
        {
            float wd = 0f;
            foreach (Chunk chk in phrase.Chunks)
            {
                wd += chk.GetWidthPoint();
            }
            return wd;
        }

        private Font GetFont(int level)
        {
            Font font = new Font(bf, level == 0 ? 11 : 10);
            if (level == 0) font.SetStyle(Font.BOLD);
            return font;
        }

    }

    /*
     * Recto - verso?
     * Client details
     * File details
     * 
     */


    /*
    public class BundleRenderer : ITextRenderer
    {
        private IndexDataContract _index;
        private string _culture;
        private string _resultFilePath;
        private IndexPageDataContract _indexPage;

        private HeaderConfigDataContract _defaultHeader=null;
        private FooterConfigDataContract _defaultFooter=null;

        public BundleRenderer(IndexDataContract index, string culture, string resultFilePath, HeaderConfigDataContract defaultHeader, FooterConfigDataContract defaultFooter)
            : this(index, culture, resultFilePath)
        {
            _defaultHeader = defaultHeader;
            _defaultFooter = defaultFooter;
        }

        public BundleRenderer(IndexDataContract index, string culture,string resultFilePath)
            : base(null, null)
        {
            _index = index;
            _culture = culture;
            _resultFilePath = resultFilePath;
        }

        public override void HandleDocument()
        {
            //Set font
          //  CreateDocument();
            _document.SetMargins(60f, 60f, 95f, 45f);

            TraverseIndex(_index.Items);

            //CloseDocument();
            
            
        }

        public override void CloseDocument()
        {
            base.CloseDocument();
            
            if (!string.IsNullOrEmpty(_resultFilePath))
            {
                if (System.IO.File.Exists(_resultFilePath)) System.IO.File.Delete(_resultFilePath);
                System.IO.File.Move(_pdfDataContract.PDFLocation, _resultFilePath);
                _pdfDataContract.PDFLocation = _resultFilePath;
            }
            PostProcess();
        }

        private void PostProcess()
        {
            
            string stampPath = Path.Combine(ConfigurationManager.AppSettings["eBook.Itext.WorkingFolder"], string.Format("{0}_stamped.pdf", _pdfDataContract.Id.ToString()));


            PdfReader reader = new PdfReader(_pdfDataContract.PDFLocation, true);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(stampPath, FileMode.Create));
            PostProcessItems(_index.Items, ref stamper);

            reader.Close();
            stamper.Close();
            writer = null;

            stamper = null;
            reader = null;

            File.Delete(_pdfDataContract.PDFLocation);
            File.Move(stampPath, _pdfDataContract.PDFLocation);
           
        }

        private void PostProcessItems(List<IndexItemBaseDataContract> items, ref PdfStamper stamper)
        {
            foreach (IndexItemBaseDataContract iibdc in items)
            {
                if (iibdc.HeaderConfig == null && _defaultHeader != null)
                {
                    iibdc.HeaderConfig = _defaultHeader;
                }
                if (iibdc.FooterConfig == null && _defaultFooter != null)
                {
                    iibdc.FooterConfig = _defaultFooter;
                }

                
                //switch(
                ProcessHeaderFooter(iibdc, ref stamper);



                if (iibdc.GetType().Name == "ChapterDataContract")
                {
                    PostProcessItems(((ChapterDataContract)iibdc).Items, ref stamper);
                }
            
            }
        }

        private void ProcessHeaderFooter(IndexItemBaseDataContract iibdc, ref PdfStamper stamper)
        {
            XElement xe;
            if (iibdc.HeaderConfig != null)
            {

                if (iibdc.HeaderConfig.Enabled)
                {
                    if (!string.IsNullOrEmpty(iibdc.HeaderConfig.Style))
                    {
                        xe = XElement.Parse(iibdc.HeaderConfig.Style);
                    }
                    else
                    {
                        xe = RenderItextHeader("XBRL", iibdc);
                    }
                    for (int i = iibdc.StartsAt; i <= iibdc.EndsAt; i++)
                    {
                        if (!_emptyPages.Contains(i))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(xe.ToString());
                            
                            RenderHeaderFooter(stamper.GetOverContent(i), doc.DocumentElement, stamper.Reader.GetPageSize(i), i, stamper.Reader.NumberOfPages);
                        }
                    }
                }
            }
            if (iibdc.FooterConfig != null)
            {
                if (iibdc.FooterConfig.Enabled)
                {
                    xe = RenderItextFooter("XBRL", iibdc);
                    for (int i = iibdc.StartsAt; i <= iibdc.EndsAt; i++)
                    {
                        if (!_emptyPages.Contains(i))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(xe.ToString());
                            RenderHeaderFooter(stamper.GetOverContent(i), doc.DocumentElement, stamper.Reader.GetPageSize(i), i, stamper.Reader.NumberOfPages);
                        }
                    }
                }
            }
        }

        private XElement RenderItextHeader(string department, IndexItemBaseDataContract iibdc)
        {
            return XElement.Parse("<data/>");
            //if (iibdc.HeaderConfig == null) return XElement.Parse("<data/>");
            //if (iibdc.HeaderConfig.Enabled == false) return XElement.Parse("<data/>");

            //string assessment = "{0}{1}";
            //switch (_culture)
            //{
            //    case "nl-BE":
            //        assessment = "Aanslagjaar {0}{1}";
            //        break;
            //    case "fr-FR":
            //        assessment = "Exercice d'imposition {0}{1}";
            //        break;
            //    case "en-US":
            //        assessment = "Year of assessment {0}{1}";
            //        break;
            //}

            //XElement element = new XElement("data",
            //    new XElement("block",
            //        new XAttribute("type", "TOPLEFT"),
            //        new XElement("phrase", " "),
            //        new XElement("phrase",
            //            new XAttribute("font", Config.StandardFont),
            //            new XAttribute("fontstyle", "bold"),
            //            new XAttribute("size", "10"),
            //            new XCData(string.Format("          {0}", client.Name))
            //            ),
            //        new XElement("phrase",
            //            new XAttribute("font", Config.StandardFont),
            //            new XAttribute("fontstyle", "bold"),
            //            new XAttribute("size", "10"),
            //            new XCData(string.Format("          {0}", string.IsNullOrEmpty(client.EnterpriseNumber) ? "" : FormatVAT(client.EnterpriseNumber)))
            //            )
            //    ),
            //    new XElement("block",
            //        new XAttribute("type", "TOPRIGHT"),
            //        new XElement("phrase", " "),
            //        new XElement("phrase",
            //            new XAttribute("font", Config.StandardFont),
            //            new XAttribute("size", "10"),
            //            new XCData(string.Format("{0} - {1}", file.StartDate.ToString("dd/MM/yyyy"), file.EndDate.ToString("dd/MM/yyyy")))
            //            ),
            //        new XElement("phrase",
            //            new XAttribute("font", Config.StandardFont),
            //            new XAttribute("size", "10"),
            //            new XCData(string.Format(assessment, file.AssessmentYear, ""))
            //            )
            //    )
            //);

            //if (iibdc.HeaderConfig.ShowTitle)
            //{
            //    element.Add(new XElement("block",
            //        new XAttribute("type", "TOPTITLE"),
            //        new XElement("phrase",
            //            new XAttribute("font", Config.StandardFont),
            //            new XAttribute("size", "10"),
            //            new XAttribute("fontstyle", "bold underline"),
            //            new XCData(iibdc.Title)
            //            )
            //        )
            //    );
            //}

            //return element;
        }



        private XElement RenderItextFooter(string department, IndexItemBaseDataContract iibdc)
        {
            if (iibdc.FooterConfig == null) return XElement.Parse("<data/>");
            if (iibdc.FooterConfig.Enabled == false) return XElement.Parse("<data/>");
            if (iibdc.FooterConfig.ShowFooterNote == false && iibdc.FooterConfig.ShowPageNr == false) return XElement.Parse("<data/>");
            string footerLine = "";
            string pageLine = "";
            if (iibdc.FooterConfig.ShowFooterNote)
            {
                //if(iibdc.FooterConfig.FootNote
                if (!string.IsNullOrEmpty(department))
                {
                    switch (department.ToUpper())
                    {
                        case "TAX":
                            switch (_culture)
                            {
                                case "nl-BE":
                                    footerLine = "Voor echt verklaard";
                                    break;
                                case "fr-FR":
                                    footerLine = "Certifié exact";
                                    break;
                            }
                            break;
                        case "ACR":
                       
                            switch (_culture)
                            {
                                case "nl-BE":
                                    footerLine = "Deze cijfers werden niet onderworpen aan een audit of beperkt nazicht door Ernst & Young";
                                    break;
                                case "fr-FR":
                                    footerLine = "Ces chiffres n'ont pas fait l'objet ni d'un controle révisoral, ni d'une révision limitée de la part d' Ernst & Young";
                                    break;
                            }
                            break;
                        default:
                            footerLine = "";
                            break;
                    }
                }
            }
            if (iibdc.FooterConfig.ShowPageNr)
            {
                pageLine = "p. [PAGE_NUMBER]/[PAGE_TOTAL]";
            }
            return new XElement("data",
                new XElement("block",
                    new XAttribute("type", "BOTTOMCENTER"),
                    new XElement("phrase",
                        new XAttribute("font", Config.StandardFont),
                        new XAttribute("size", "10"),
                        new XCData(" ")),
                    new XElement("phrase",
                        new XAttribute("font", Config.StandardFont),
                        new XAttribute("size", "8"),
                        new XCData(footerLine)),
                    new XElement("phrase",
                        new XAttribute("font", Config.StandardFont),
                        new XAttribute("size", "10"),
                        new XCData(pageLine))
                )
                );

        }

        private List<IndexItemBaseDataContract> TraverseIndex(List<IndexItemBaseDataContract> items)
        {

            foreach (IndexItemBaseDataContract iibdc in items)
            {
                switch (iibdc.GetType().Name)
                {
                    case "CoverpageDataContract":
                        break;
                    case "IndexPageDataContract":
                        //_indexPage = (IndexPageDataContract)iibdc;
                        break;
                    case "PDFDataContract":
                        PDFDataContract pdc = (PDFDataContract)iibdc;
                        //iibdc.StartsAt = writer.CurrentPageNumber+1;
                        try
                        {
                            iibdc.StartsAt = InsertPDF(pdc.PDFLocation);
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("PdfReader not opened with owner password"))
                            {
                                throw new FaultException<string>(string.Format("The PDF with title {0} is a secured pdf. Please upload a security free version. (deleting/replacing a pdf in the library will come soon)", pdc.Title));
                            }
                            else
                            {
                                throw new FaultException<string>(string.Format("PDF:{0}({1}), ERROR:{2}", pdc.Title, pdc.PDFLocation, e.Message));
                            }
                        }

                        iibdc.EndsAt = writer.CurrentPageNumber;
                        if (iibdc.EndsAt % 2 != 0) // make sure ends at face down
                        {
                            _document.NewPage();
                            _document.Add(new Chunk(" "));
                            iibdc.EndsAt = writer.CurrentPageNumber;
                            _emptyPages.Add(iibdc.EndsAt);
                        }
                        break;
                    case "ChapterDataContract":
                        ChapterDataContract cdc = (ChapterDataContract)iibdc;
                        if (!cdc.NoCoverpage)
                        {
                            iibdc.StartsAt = writer.CurrentPageNumber + 1;
                            _document.SetPageSize(PageSize.A4);
                            _document.NewPage();

                            // handle chapter it self
                            XmlDocument cdoc = new XmlDocument();
                            cdoc.LoadXml(cdc.iTextTemplate);
                            ProcessItem(cdoc.DocumentElement);

                            // insert empty page (back side) // make sure ends at face down
                            _document.NewPage();
                            _document.Add(new Chunk(" "));
                            iibdc.EndsAt = writer.CurrentPageNumber;
                            _emptyPages.Add(iibdc.EndsAt);
                            // handle chapter children
                            cdc.Items = TraverseIndex(cdc.Items);
                            
                        }
                        break;
                }
            }
            return items;
        }
        
    }
    */
}
