﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using iTextSharp.text.pdf;
using System.Globalization;
using iTextSharp.text;
using System.util;
using iTextSharp.text.factories;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace EY.com.eBook.Core.IText.Render
{
    public class ITextRenderer : IDisposable
    {

        protected readonly float[] _defaultMargins = new float[4];
        protected Dictionary<Guid, string> pdfFiles;
        protected PdfWriter writer;
        protected Font _standardFont;
        protected float _standardFontSize;
        protected BaseFont bf;
        protected float _defaultStandardFontSize = 10;
        protected int minusChap = -2;
        public iTextSharp.text.Document _document;
        protected bool _standalone;
        public Rectangle _pageSize;
        protected Font _defaultStandardFont;
        protected bool _draft = true;
        public Rectangle _pageSizeLandscape;
        public Rectangle _pageSizePortret;
        protected int page_number;
        protected int page_total;
        protected string end_date = DateTime.Now.Date.ToShortDateString();
        protected List<int> _emptyPages = new List<int>();
        protected Font _lastFont;
        protected string _imageFolder = Config.PdfImageResource;

        protected XmlDocument _sourceDocument;
        protected PDFDataContract _pdfDataContract;
        protected XmlElement _rootNode;
        private TOCPageEvent _tocPageEvent = new TOCPageEvent();
        protected int symbolSize = 16;

        public bool utiliseChapter = true;

        //public PdfOutline rootOutline;
        //public PdfOutline currentOutline;


        public ContentsOutline Outlines;

        private ContentsOutline _currentOutline;

        public ITextRenderer()
        {
            SetFont();
        }

        public ITextRenderer(string iTextTemplate, IndexItemBaseDataContract iibdc) : this()
        {
            Init(iTextTemplate, iibdc);
        }

        public void Init(string iTextTemplate, IndexItemBaseDataContract iibdc)
        {
            if (!string.IsNullOrEmpty(iTextTemplate))
            {
                _sourceDocument = new XmlDocument();
                _sourceDocument.LoadXml(iTextTemplate);
                _rootNode = _sourceDocument.DocumentElement;
            }

            _pdfDataContract = new PDFDataContract
            {
                Id = Guid.NewGuid()
                ,
                FromPage = 1
            };

            _pdfDataContract.PDFLocation = Path.Combine(Config.PdfWorkingFolder, string.Format("{0}.pdf", _pdfDataContract.Id.ToString()));
            if (iibdc != null)
            {
                _pdfDataContract.Delete = iibdc.Delete;
                _pdfDataContract.HeaderConfig = iibdc.HeaderConfig;
                _pdfDataContract.FooterConfig = iibdc.FooterConfig;
                _pdfDataContract.Id = iibdc.Id != Guid.Empty ? iibdc.Id : Guid.NewGuid();
                _pdfDataContract.ShowInIndex = iibdc.ShowInIndex;
                _pdfDataContract.Title = iibdc.Title;
                _pdfDataContract.NoDraft = iibdc.NoDraft;
            }
        }

        public void CreateDocument()
        {
            
            _document = new iTextSharp.text.Document();
            
            writer = PdfWriter.GetInstance(_document, new FileStream(_pdfDataContract.PDFLocation, FileMode.Create));
            
            writer.PageEvent = _tocPageEvent;
            Outlines = new ContentsOutline
            {
                Id = Guid.Empty
                ,
                IsTitlePage = false
                ,
                Page = 0
                ,
                Title = string.Empty
                ,
                Children = new List<ContentsOutline>()
            };
            _currentOutline = Outlines;
            //rootOutline = new PdfOutline(writer);
            //currentOutline = rootOutline;

            if (_rootNode != null)
            {
                SetRootAttributes(_rootNode);
                SetPageSize(_rootNode);
                SetFont(_rootNode);
                SetDefaultMargins(_rootNode);

            }
            else
            {
                _document.SetPageSize(PageSize.A4);
                SetFont();
                SetDefaultMargins();
            }

            _document.Open();
        }

        private void BuildOutlines(ref PdfOutline ppoutline,ContentsOutline outlinesParent)
        {

            foreach (ContentsOutline outline in outlinesParent.Children)
            {
                if (_tocPageEvent.ItemPages.ContainsKey(outline.Id))
                {
                    outline.Page = _tocPageEvent.ItemPages[outline.Id];
                    string tag = string.Format("{0}#{1}", outline.Id, outline.Page);
                    PdfAction caction = PdfAction.GotoLocalPage(tag, true);
                    PdfOutline pout = new PdfOutline(ppoutline, caction, outline.Title);
                    if (outline.Children != null && outline.Children.Count > 0)
                    {
                        BuildOutlines(ref pout, outline);
                    }
                }
                
                
                // 
                // 
                // outline.Tag = string.Format("{0}#{1}", id, writer.CurrentPageNumber);
            }
        }

        public virtual void CloseDocument()
        {

            PdfOutline rootOutline = new PdfOutline(writer);
            BuildOutlines(ref rootOutline, Outlines);
            if (rootOutline != null && rootOutline.Kids.Count > 0)
            {
                writer.DirectContent.PdfDocument.rootOutline = rootOutline;
            }
            _document.Close();

            PdfReader reader = new PdfReader(_pdfDataContract.PDFLocation);
            _pdfDataContract.Pages = reader.NumberOfPages;
            reader.Close();
            _pdfDataContract.ToPage = _pdfDataContract.Pages;
            _pdfDataContract.Outlines = Outlines;
            
        }

        public virtual void HandleDocument()
        {
            if (utiliseChapter)
            {
                Chapter chapter = new Chapter(0);

                if (_rootNode.ChildNodes.Count == 0)
                {
                    _rootNode.InnerXml = "<phrase>ITEM HAS NO DATA</phrase>";
                }

                chapter = (Chapter)ProcessData(_rootNode, chapter);
                _document.Add(chapter);
            }
            else
            {
                ProcessData(_rootNode);
            }
        }

        public virtual PDFDataContract CreatePdf()
        {

            CreateDocument();
            HandleDocument();
            CloseDocument();
            return _pdfDataContract;
        }


        #region "Helperclass"
        protected static float ConfigureDefaultMargins(XmlElement RootNode, string position)
        {
            return float.Parse(RootNode.Attributes.GetNamedItem(position).Value, NumberFormatInfo.InvariantInfo);
        }

        protected static float ConfigureDefaultMargins(XmlNode node, string position)
        {
            return float.Parse(node.Attributes.GetNamedItem(position).Value, NumberFormatInfo.InvariantInfo);
        }

        protected static bool NodeHasAttribute(XmlNode node, string attributeName)
        {
            XmlNode attrib;
            if (node.Attributes == null)
                attrib = null;
            else
                attrib = node.Attributes.GetNamedItem(attributeName);
            return attrib != null;
        }

        protected void SetDefaultMargins()
        {
            _defaultMargins[0] = 60f;
            _defaultMargins[1] = 60f;
            _defaultMargins[2] = 110f;
            _defaultMargins[3] = 55f;

            _document.SetMargins(_defaultMargins[0], _defaultMargins[1], _defaultMargins[2], _defaultMargins[3]);
        }

        protected void SetDefaultMargins(XmlElement RootNode)
        {
            if (NodeHasAttribute(RootNode, "margins"))
            {

                string[] margs = RootNode.Attributes.GetNamedItem("margins").Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                float[] fmargs = new float[4];
                for (int i = 0; i < 4; i++)
                {
                    if (margs.Length > i)
                    {
                        fmargs[i] = 60f;
                        float.TryParse(margs[i], out fmargs[i]);
                    }
                    else
                    {
                        fmargs[i] = 60f;
                    }
                }
            }
            else
            {
                SetDefaultMargins();
            }
        }

        protected string GetSize(XmlNode node)
        {
            if (NodeHasAttribute(node, "size"))
                return node.Attributes.GetNamedItem("size").Value;

            return node.ParentNode != null
                       ? GetSize(node.ParentNode)
                       : _standardFontSize.ToString();
        }

        protected string GetSize(XElement node)
        {
            if (node.Attribute("size")!=null)
                return node.Attribute("size").Value;

            return node.Parent != null
                       ? GetSize(node.Parent)
                       : _standardFontSize.ToString();
        }

        protected Font GetFont(XElement node)
        {
            Font font = new Font(_standardFont.BaseFont, _standardFontSize);
            font.SetStyle(Font.NORMAL);
            font.Size = float.Parse(GetSize(node));
            string style = string.Empty;
            if (node.Attribute("fontstyle")!=null)
                style = node.Attribute("fontstyle").Value;

            if (node.Attribute( "style") != null)
                style += node.Attribute("style").Value;

            if (style.ToLower().IndexOf("bold") > -1)
                font.SetStyle(Font.BOLD);

            if (style.ToLower().IndexOf("italic") > -1)
                font.SetStyle(Font.ITALIC);

            if (style.ToLower().IndexOf("underline") > -1)
                font.SetStyle(Font.UNDERLINE);
            _lastFont = font;
            return font;
        }

        protected Font GetFont(XmlNode node)
        {
            Font font = new Font(_standardFont.BaseFont, _standardFontSize);
            font.SetStyle(Font.NORMAL);
            font.Size = float.Parse(GetSize(node));
            string style = string.Empty;
            if (NodeHasAttribute(node, "fontstyle"))
                style = node.Attributes.GetNamedItem("fontstyle").Value;

            if (NodeHasAttribute(node, "style"))
                style += node.Attributes.GetNamedItem("style").Value;

            if (style.ToLower().IndexOf("bold") > -1)
                font.SetStyle(Font.BOLD);

            if (style.ToLower().IndexOf("italic") > -1)
                font.SetStyle(Font.ITALIC);

            if (style.ToLower().IndexOf("underline") > -1)
                font.SetStyle(Font.UNDERLINE);
            _lastFont = font;
            return font;
        }

       
        protected string GetImportGenericTag(Properties attributes, int page, bool first)
        {
            StringBuilder importKeyTag = new StringBuilder("IMPORT::");
            if (attributes.ContainsKey("fileid"))
            {
                importKeyTag.Append(attributes["fileid"]);
                importKeyTag.Append("::").Append(page.ToString());
                importKeyTag.Append("::").Append(first.ToString());
            }
            else
            {
                importKeyTag.Append(Guid.Empty.ToString());
                importKeyTag.Append("::").Append(page.ToString());
                importKeyTag.Append("::" + false.ToString());
            }
            if (attributes.ContainsKey("headerfooter"))
                importKeyTag.Append("::").Append(attributes["headerfooter"]);
            else
                importKeyTag.Append("::").Append(false.ToString());

            return importKeyTag.ToString();
        }

        protected static Properties GetAttributes(XmlNode node)
        {
            Properties attributes = new Properties();

            foreach (XmlNode attrib in node.Attributes)
                attributes.Add(attrib.Name, attrib.Value);

            return attributes;
        }

        protected static Properties GetAttributes(XElement node)
        {
            Properties attributes = new Properties();

            foreach (XAttribute attrib in node.Attributes())
                attributes.Add(attrib.Name.LocalName, attrib.Value);

            return attributes;
        }

        protected static float GetAttributeValue(XmlNode block, string position)
        {
            if (block.Attributes[position] == null)
                return 0;
            else
                return float.Parse(block.Attributes[position].Value, NumberFormatInfo.InvariantInfo);
        }

        protected static int GetAlignment(string alignment)
        {
            switch (alignment.ToUpper())
            {
                case "TOP":
                    return Element.ALIGN_TOP;
                case "BOTTOM":
                    return Element.ALIGN_BOTTOM;
                case "BASELINE":
                    return Element.ALIGN_BASELINE;
                case "CENTER":
                    return Element.ALIGN_CENTER;
                case "MIDDLE":
                    return Element.ALIGN_MIDDLE;
                case "JUSTIFIED":
                    return Element.ALIGN_JUSTIFIED;
                case "LEFT":
                    return Element.ALIGN_LEFT;
                case "RIGHT":
                    return Element.ALIGN_RIGHT;
                default:
                    return Element.ALIGN_UNDEFINED;
            }
        }

        protected void SetPageSize(XmlElement RootNode)
        {
            Properties attribs = GetAttributes(RootNode);

            _pageSizePortret = PageSize.A4;
            //(Rectangle)typeof(PageSize).GetField(RootNode.Attributes.GetNamedItem("pagesize").Value).GetValue(null);
            _pageSizeLandscape = new RectangleReadOnly(842, 595);

            if (attribs.ContainsKey("orientation") && attribs["orientation"].ToUpper().Trim() == "LANDSCAPE")
                _pageSize = _pageSizeLandscape;
            else
                _pageSize = _pageSizePortret;

            _document.SetPageSize(_pageSize);
        }

        protected void SetFont(XmlElement RootNode)
        {
            SetFont();
            /* FontFactory.Register(@RootNode.Attributes.GetNamedItem("font").Value.ToString(), "EYInterstate-Light");
             bf = BaseFont.CreateFont(@RootNode.Attributes.GetNamedItem("font").Value.ToString(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

             _defaultStandardFontSize = (float)Convert.ToDouble(RootNode.Attributes.GetNamedItem("size").Value);
             _defaultStandardFont = new Font(bf, (float)Convert.ToDouble(RootNode.Attributes.GetNamedItem("size").Value));
             _standardFont = _defaultStandardFont;*/
        }
        protected void SetFont()
        {
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            _defaultStandardFontSize = (float)Convert.ToDouble(12);
            _defaultStandardFont = new Font(bf, (float)Convert.ToDouble(12));
            _standardFont = _defaultStandardFont;
            _standardFontSize = _defaultStandardFontSize;
            _lastFont = _standardFont;
        }

        protected Font GetFont(float size)
        {
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            BaseFont basef = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            _lastFont = new Font(basef, size);
            return _lastFont;
        }

        protected void SetFont(XmlNode node)
        {
            FontFactory.Register(node.Attributes.GetNamedItem("font").Value.ToString(), "EYInterstate-Light");
            bf = BaseFont.CreateFont(node.Attributes.GetNamedItem("font").Value.ToString(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            _defaultStandardFontSize = (float)Convert.ToDouble(node.Attributes.GetNamedItem("size").Value);
            _defaultStandardFont = new Font(bf, (float)Convert.ToDouble(node.Attributes.GetNamedItem("size").Value));
            _standardFont = _defaultStandardFont;
            _lastFont = _standardFont;
        }

        protected void SetRootAttributes(XmlElement RootNode)
        {
            Properties rootAttribs = GetAttributes(RootNode);

            if (rootAttribs.ContainsKey("font"))
                bf = FontFactory.GetFont(rootAttribs).BaseFont;

            if (rootAttribs.ContainsKey("standalone"))
                _standalone = (rootAttribs["standalone"] == "true");

            if (rootAttribs.ContainsKey("draft"))
                _draft = (rootAttribs["draft"] == "true");

            if (rootAttribs.ContainsKey("title"))
                _document.AddTitle(rootAttribs["title"]);
            if (rootAttribs.ContainsKey("author"))
                _document.AddAuthor(rootAttribs["author"]);
            if (rootAttribs.ContainsKey("subject"))
                _document.AddSubject(rootAttribs["subject"]);
            if (rootAttribs.ContainsKey("creator"))
                _document.AddCreator(rootAttribs["creator"]);
            if (rootAttribs.ContainsKey("keywords"))
                _document.AddKeywords(rootAttribs["keywords"]);
        }

        protected void SetPageMargins(XmlNode node)
        {
            float[] margins = new float[4];
            _defaultMargins.CopyTo(margins, 0);
            if (NodeHasAttribute(node, "left"))
                margins[0] = ConfigureDefaultMargins(node, "left");

            if (NodeHasAttribute(node, "right"))
                margins[1] = ConfigureDefaultMargins(node, "right");

            if (NodeHasAttribute(node, "top"))
                margins[2] = ConfigureDefaultMargins(node, "top");

            if (NodeHasAttribute(node, "bottom"))
                margins[3] = ConfigureDefaultMargins(node, "bottom");

            _document.SetMargins(margins[0], margins[1], margins[2], margins[3]);
        }

        #endregion

        #region "Process"

        protected List<Cell> ProcessCells(XmlNode currentNode)
        {
            List<Cell> cells = new List<Cell>();

            foreach (XmlNode node in currentNode.SelectNodes("cell"))
            {
                Properties props = GetAttributes(node);
                Cell cell = ElementFactory.GetCell(props);
                if (props.ContainsKey("useBorderPadding"))
                    cell.UseBorderPadding = bool.Parse(props["useBorderPadding"]);

                if (props.ContainsKey("useAscender"))
                    cell.UseAscender = bool.Parse(props["useAscender"]);

                if (props.ContainsKey("useDescender"))
                    cell.UseDescender = bool.Parse(props["useDescender"]);

                if (props.ContainsKey("leading"))
                    cell.Leading = float.Parse(props["leading"]);

                if (props.ContainsKey("horizontalAlignment"))
                {
                    cell.HorizontalAlignment = GetAlignment(props["horizontalAlignment"].ToString());
                }

                if (props.ContainsKey("verticalAlignment"))
                {
                    cell.HorizontalAlignment = GetAlignment(props["verticalAlignment"].ToString());
                }

                cell = (Cell)ProcessData(node, cell);
                cells.Add(cell);
            }
            return cells;
        }

        protected static Table ProcessRows(List<Cell> cells, Table table)
        {
            Cell cell;
            int columns = 0;
            foreach (Cell cellItem in cells)
                columns += cellItem.Colspan;

            if (table.Columns < columns)
                table.AddColumns(columns - table.Columns);

            String width;
            float[] cellWidths = new float[columns];
            bool[] cellNulls = new bool[columns];
            for (int i = 0; i < columns; i++)
            {
                cellWidths[i] = 0;
                cellNulls[i] = true;
            }
            float total = 0;
            int j = 0;
            foreach (Cell c in cells)
            {
                cell = c;
                width = cell.GetWidthAsString();
                if (cell.Width == 0)
                {
                    if (cell.Colspan == 1 && cellWidths[j] == 0)
                    {
                        try
                        {
                            cellWidths[j] = 100f / columns;
                            total += cellWidths[j];
                        }
                        catch
                        {
                            // empty on purpose
                        }
                    }
                    else if (cell.Colspan == 1)
                        cellNulls[j] = false;
                }
                else if (cell.Colspan == 1 && width.EndsWith("%"))
                {
                    try
                    {
                        cellWidths[j] = float.Parse(width.Substring(0, width.Length - 1), NumberFormatInfo.InvariantInfo);
                        total += cellWidths[j];
                    }
                    catch
                    {
                        // empty on purpose
                    }
                }
                j += cell.Colspan;
                table.AddCell(cell);
            }
            float[] widths = table.ProportionalWidths;
            if (widths.Length == columns)
            {
                float left = 0.0f;
                for (int i = 0; i < columns; i++)
                {
                    if (cellNulls[i] && widths[i] != 0)
                    {
                        left += widths[i];
                        cellWidths[i] = widths[i];
                    }
                }
                if (100.0 >= total)
                {
                    for (int i = 0; i < widths.Length; i++)
                        if (cellWidths[i] == 0 && widths[i] != 0)
                            cellWidths[i] = (widths[i] / left) * (100.0f - total);
                }
                table.Widths = cellWidths;
            }
            return table;
        }

        protected void ProcessData(XmlNode currentNode)
        {
            foreach (XmlNode node in currentNode.ChildNodes)
            {
                //ProcessData(node);
                ProcessItem(node);
            }
        }

        protected void ProcessItem(XmlNode node)
        {
            switch (node.Name.ToUpper())
            {
                case "PDF":
                    if (NodeHasAttribute(node, "path"))
                    {
                        InsertPDF(node.Attributes.GetNamedItem("path").Value);
                    }
                    break;
                case "BLOCK":
                    ProcessContentBlock(writer.DirectContent, node, writer.PageSize);
                    break;
                case "CHAPTER":

                    if (NodeHasAttribute(node, "title"))
                    {
                        
                        Rectangle pg = _document.PageSize;
                        if (NodeHasAttribute(node, "orientation"))
                        {
                            
                            switch (node.Attributes.GetNamedItem("orientation").Value.ToLower())
                            {
                                case "portrait":
                                    pg=_pageSizePortret;
                                    break;
                                case "landscape":
                                    pg = _pageSizeLandscape;
                                    break;
                            }
                        }
                        if (_document.PageSize != pg || _document.PageSize.Rotation!=pg.Rotation)
                        {
                            _document.SetPageSize(pg);
                            
                        }
                        _document.NewPage();
                       // ChapterAutoNumber cha = new ChapterAutoNumber(node.Attributes["title"].Value);
                        PdfPTable table = new PdfPTable(1);
                        table.SetTotalWidth(new float[]
                                 {
                                     _document.PageSize.Width - _document.LeftMargin - _document.BottomMargin
                                 });
                        table.LockedWidth = true;

                        Font font = new Font(bf, 14, Font.BOLD);
                        Chunk ph = new Chunk(node.Attributes["title"].Value, font);
                        Guid id = Guid.NewGuid();
                        ph.SetLocalDestination(id.ToString());
                        ph.SetGenericTag(id.ToString());

                        ContentsOutline outline = new ContentsOutline
                        {
                            Children = new List<ContentsOutline>()
                            ,
                            Id = id
                            ,
                            IsTitlePage = true
                            ,
                            Title = node.Attributes["title"].Value
                            , ParentId = _currentOutline.Id
                        };
                        _currentOutline.Children.Add(outline);

                        //_currentOutline.Children.Add(
/*
                        float fith = _document.PageSize.Height;
                        PdfDestination destination;

                        int rotation = _document.PageSize.Rotation;
                        if (rotation == 90 || rotation == 180)
                            fith = _document.PageSize.Height - fith;

                        destination = new PdfDestination(PdfDestination.FITH, fith);
                        */

                       
                        //ph.SetLocalDestination("PG_" + writer.CurrentPageNumber);
                        

                        PdfPCell cell = new PdfPCell(new Phrase(ph));

                        cell.FixedHeight = _document.PageSize.Height - _document.TopMargin - _document.BottomMargin;
                        cell.Padding = 0;
                        cell.PaddingBottom = 160;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.Border = 0;
                        //cell.Width = table.TotalWidth;
                        //  cell.Height = table.TotalHeight;



                        table.AddCell(cell);

                        _document.Add(table);
                       
                       // string tag = string.Format("{0}#{1}", id, writer.CurrentPageNumber);
                       // PdfAction caction = PdfAction.GotoLocalPage(tag, true);
                       // PdfOutline outline =new PdfOutline(currentOutline, caction, node.Attributes["title"].Value);
                       // outline.Tag = string.Format("{0}#{1}", id, writer.CurrentPageNumber);
                       // currentOutline = outline;
                        if (NodeHasAttribute(node, "orientation"))
                        {
                            switch (node.Attributes.GetNamedItem("orientation").Value.ToLower())
                            {
                                case "portrait":
                                    _document.SetPageSize(_pageSizePortret);
                                    break;
                                case "landscape":
                                    _document.SetPageSize(_pageSizeLandscape);
                                    break;
                            }
                        }
                        _document.NewPage();
                        ProcessData(node);
                        _currentOutline = GetOutline(_currentOutline.ParentId);
                       // currentOutline = outline.Parent;

                    }
                    else
                        throw new BadElementException("Chapter element must have title attribute");
                    /*
                    Chapter chapter;
                    if (NodeHasAttribute(node, "nr"))
                    {
                        if (NodeHasAttribute(node, "usecustomtitlepage"))
                        {
                            if (NodeHasAttribute(node, "title"))
                            {
                                if (node.Attributes["title"].Value == "$EXCLUDE_INDEX")
                                {
                                    chapter = new Chapter(minusChap);
                                    minusChap--;
                                }
                                else
                                {
                                    chapter = new Chapter(node.Attributes["title"].Value, int.Parse(node.Attributes["nr"].Value));
                                    chapter.TriggerNewPage = true;
                                }
                            }
                            else
                            {
                                chapter = new Chapter(int.Parse(node.Attributes["nr"].Value));
                                chapter.TriggerNewPage = true;
                            }
                            if (chapter != null && bool.Parse(node.Attributes["usecustomtitlepage"].Value))
                            {
                                chapter.UseCustomTitlePage = bool.Parse(node.Attributes["usecustomtitlepage"].Value);
                                chapter.TriggerNewPage = true;
                            }
                        }
                        else
                        {
                            if (NodeHasAttribute(node, "title"))
                                chapter = new Chapter(node.Attributes["title"].Value, int.Parse(node.Attributes["nr"].Value));
                            else
                            {
                                chapter = new Chapter(int.Parse(node.Attributes["nr"].Value));
                                chapter.TriggerNewPage = true;
                            }
                        }

                        if (NodeHasAttribute(node, "bookmarktitle"))
                            chapter.BookmarkTitle = node.Attributes["bookmarktitle"].Value;

                        chapter = (Chapter)ProcessData(node, chapter);
                        if (NodeHasAttribute(node, "depth"))
                            chapter.NumberDepth = int.Parse(node.Attributes["depth"].Value);

                        _document.Add(chapter);
                    }
                    else
                        throw new BadElementException("Chapter element must have number attribute");
                     */
                    break;
                case "COVERPAGE":
                    Chapter coverpage = new Chapter("$COVERPAGE$", -1);
                    coverpage.UseCustomTitlePage = true;
                    coverpage.BookmarkTitle = "$COVERPAGE$";
                    coverpage = (Chapter)ProcessData(node, coverpage);
                    _document.Add(coverpage);
                    if (!_standalone)
                    {
                        _document.Add(new Chunk().SetNewPage());
                        Chunk nph = new Chunk(" ");
                        nph.SetGenericTag("empty");
                        _document.Add(nph);
                        _document.Add(new Phrase(" "));
                    }
                    break;
                case "INDEXPAGE":
                    Chapter indexpage = new Chapter("$INDEXPAGE$", 0);
                    indexpage.UseCustomTitlePage = true;
                    indexpage.BookmarkTitle = "$INDEXPAGE$";

                    indexpage.Add(new Paragraph(" "));
                    _document.Add(indexpage);
                    break;
                case "TITLEPAGE":
                    Chunk chk = new Chunk("");
                    chk.SetGenericTag("TITLEPAGE::empty");

                    _document.Add(chk);
                    ProcessData(node);
                    break;
                case "TABLE":
                    ProcessDataTable(node, null, false);
                    break;
                case "PTABLE":
                    PdfPTable ptable = ProcessDataPTablePart1(node, 0);
                    _document.Add(ptable);
                    break;

                case "PARAGRAPH":
                    Paragraph paragraph = new Paragraph();

                    paragraph.Font = GetFont(node);
                    if (NodeHasAttribute(node, "lineheight"))
                        paragraph.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, NumberFormatInfo.InvariantInfo) * paragraph.Font.Size;
                    if (NodeHasAttribute(node, "align"))
                        paragraph.Alignment = GetAlignment(node.Attributes.GetNamedItem("align").Value, NodeHasAttribute(node, "verticalalign") ? node.Attributes.GetNamedItem("verticalalign").Value : null);

                    paragraph = (Paragraph)ProcessData(node, paragraph);
                    paragraph.Font = GetFont(node);
                    _document.Add(paragraph);

                    break;
                case "PHRASE":
                    Phrase phrase = null;
                    if (node.HasChildNodes)
                    {
                        if (node.FirstChild.NodeType == XmlNodeType.Text || node.FirstChild.NodeType == XmlNodeType.CDATA)
                        {
                            phrase = new Phrase(node.FirstChild.InnerText, GetFont(node));
                            if (NodeHasAttribute(node, "lineheight"))
                                phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;

                        }
                        else
                        {
                            if (node.FirstChild.Name.ToUpper() == "PHRASE")
                            {
                                phrase = new Phrase();
                                phrase.Font = GetFont(node);
                                if (NodeHasAttribute(node, "lineheight"))
                                    phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;

                                phrase = (Phrase)ProcessData(node, phrase);
                            }
                        }
                    }
                    else
                    {
                        phrase = new Phrase();
                        phrase.Font = GetFont(node);
                        if (NodeHasAttribute(node, "lineheight"))
                            phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                    }

                    _document.Add(phrase);
                    break;
                case "NEWLINE":
                    Chunk chkNewLine = Chunk.NEWLINE;
                    chkNewLine.Font = _standardFont;
                    _document.Add(chkNewLine);

                    break;
                case "NEWPAGE":
                    if (NodeHasAttribute(node, "orientation"))
                    {
                        switch (node.Attributes.GetNamedItem("orientation").Value.ToLower())
                        {
                            case "portrait":
                                _document.SetPageSize(_pageSizePortret);
                                break;
                            case "landscape":
                                _document.SetPageSize(_pageSizeLandscape);
                                break;
                        }
                    }
                    _document.NewPage();
                    break;
                case "IMAGE":
                    break;
                case "TEMPLATE":
                    break;
                default:
                    if (node.HasChildNodes)
                    {
                        ProcessData(node);
                    }
                    break;
            }
        }

        private ContentsOutline GetOutline(Guid? parentId)
        {
            if (!parentId.HasValue || parentId.Value == Guid.Empty) return Outlines;
            return FindOutline(Outlines.Children, parentId.Value);
            
        }

        private ContentsOutline FindOutline(List<ContentsOutline> olines, Guid id)
        {
            foreach (ContentsOutline outline in olines)
            {
                if (outline.Id == id) return outline;

            }
            foreach (ContentsOutline outline in olines.Where(o => o.Children != null && o.Children.Count > 0))
            {
                ContentsOutline childO = FindOutline(outline.Children, id);
                if (childO != null) return childO;
            }
            return null;
        }
        /*
        protected int GetAlignment(string halign)
        {
            return GetAlignment(halign, null);
        }
        */
        protected int GetAlignment(string halign, string valign)
        {
            int align = 0;
            switch (halign.ToLower())
            {
                case "left":
                    align = Element.ALIGN_LEFT;
                    break;
                case "right":
                    align = Element.ALIGN_RIGHT;
                    break;
                case "center":
                    align = Element.ALIGN_CENTER;
                    break;
                case "justified":
                    align = Element.ALIGN_JUSTIFIED;
                    break;
                case "justified_all":
                    align = Element.ALIGN_JUSTIFIED_ALL;
                    break;
            }
            if (!string.IsNullOrEmpty(valign))
            {
                switch (valign.ToLower())
                {
                    case "top":
                        align = align | Element.ALIGN_TOP;
                        break;
                    case "bottom":
                        align = align | Element.ALIGN_BOTTOM;
                        break;
                }
            }
            return align;
        }


        protected IElement ProcessData(XmlNode currentNode, IElement parentObject)
        {
            foreach (XmlNode node in currentNode.ChildNodes)
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (node.Name.ToUpper())
                        {
                            case "PDF":
                                if (NodeHasAttribute(node, "path"))
                                {
                                    InsertPDF(node.Attributes.GetNamedItem("path").Value);
                                }
                                break;
                            case "BLOCK":
                                ProcessContentBlock(writer.DirectContent, node, writer.PageSize);
                                break;
                            case "TITLEPAGE":
                                Chunk tchk = new Chunk("");
                                if (NodeHasAttribute(node, "headerfooterid"))
                                    tchk.SetGenericTag("TITLEPAGE::" + node.Attributes["headerfooterid"].Value);
                                else
                                    tchk.SetGenericTag("TITLEPAGE::empty");

                                _document.Add(tchk);
                                ProcessData(node);
                                break;
                            case "SECTION":
                                if (parentObject.GetType().Name != "Chapter")
                                    throw new InvalidOperationException("A section can only be loaded beneath a chapter");

                                try
                                {
                                    SetPageMargins(node);
                                    Chapter sectionChapter = (Chapter)parentObject;

                                    if (NodeHasAttribute(node, "title"))
                                    {
                                        Section section;

                                        if (NodeHasAttribute(node, "depth"))
                                            section = sectionChapter.AddSection(node.Attributes["title"].Value, int.Parse(node.Attributes["depth"].Value));
                                        else
                                            section = sectionChapter.AddSection(node.Attributes["title"].Value);

                                        if (NodeHasAttribute(node, "triggernewpage"))
                                            section.TriggerNewPage = bool.Parse(node.Attributes["triggernewpage"].Value);

                                        section = (Section)ProcessData(node, section);
                                        Chunk sectionEnd = new Chunk(" ");
                                        sectionEnd.SetGenericTag("SECTION::END");
                                        sectionChapter.Add(sectionEnd);
                                    }
                                }
                                catch
                                {
                                }
                                break;
                            case "TABLE":
                                ProcessDataTable(node, parentObject, true);
                                break;
                            case "ROW":
                                Table rowTable = (Table)parentObject;
                                rowTable = ProcessRows(ProcessCells(node), rowTable);
                                parentObject = rowTable;

                                break;
                            case "PTABLE":
                                PdfPTable ptable = null;
                                if (parentObject.GetType().Equals(typeof(PdfPCell)))
                                {
                                    ProcessDataPTablePart1(node, 0);
                                }
                                else
                                {
                                    ptable = ProcessDataPTablePart1(node, 0f);
                                }

                                if (parentObject == null)
                                {
                                    parentObject = ptable;
                                    return parentObject;
                                }

                                try
                                {
                                    ITextElementArray parent = (ITextElementArray)parentObject;
                                    parent.Add(ptable);
                                    parentObject = parent;
                                }
                                catch
                                {
                                    parentObject = ptable;
                                    return parentObject;
                                }
                                break;
                            case "PCELL":
                                if (parentObject.GetType().Name != "PdfPTable")
                                {
                                    throw new InvalidOperationException("PCELL can only be beneath a PTABLE tag");
                                }
                                PdfPCell cell;
                                XmlNodeList phrases = node.SelectNodes("./phrase");


                                // get cell width
                                PdfPTable pCellParentTable = (PdfPTable)parentObject;
                                float cellWidth = 0;
                                if (pCellParentTable.NumberOfColumns > 1)
                                {
                                    XmlNodeList preceding = node.SelectNodes("./preceding-sibling::*");
                                    int widthIdx = 0;
                                    if (pCellParentTable.NumberOfColumns < (preceding.Count + 1))
                                    {
                                        widthIdx = ((preceding.Count + 1) % pCellParentTable.NumberOfColumns);
                                        widthIdx = pCellParentTable.NumberOfColumns - widthIdx - 1;// zero bound, hence -1
                                    }
                                    else
                                    {
                                        widthIdx = preceding.Count; //zero bound
                                    }
                                    cellWidth = pCellParentTable.AbsoluteWidths[widthIdx];

                                }
                                else
                                {
                                    cellWidth = pCellParentTable.AbsoluteWidths[0];
                                }

                                if (phrases.Count > 0)
                                {
                                    if (phrases.Count == 1)
                                    {
                                        Phrase ph = (Phrase)ProcessData(node, null);
                                        if (NodeHasAttribute(node, "lineheight"))
                                            ph.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, NumberFormatInfo.InvariantInfo) * ph.Font.Size;

                                        cell = new PdfPCell(ph);
                                    }
                                    else
                                        cell = new PdfPCell();
                                }
                                else if (node.FirstChild.Name.ToUpper() == "PTABLE")
                                {
                                    PdfPTable innerTable = ProcessDataPTablePart1(node.FirstChild, cellWidth);
                                    cell = new PdfPCell(innerTable);
                                }
                                else
                                {
                                    Phrase ph = new Phrase(node.InnerText);
                                    cell = new PdfPCell(ph);
                                }

                                cell.Border = 0;
                                //cell.Width = cellWidth;
                                if (NodeHasAttribute(node, "align"))
                                    cell.HorizontalAlignment = GetAlignment(node.Attributes["align"].Value);

                                if (NodeHasAttribute(node, "valign"))
                                    cell.VerticalAlignment = GetAlignment(node.Attributes["valign"].Value);

                                if (NodeHasAttribute(node, "border"))
                                    cell.Border = int.Parse(node.Attributes["border"].Value);

                                if (NodeHasAttribute(node, "padding"))
                                    cell.Padding = float.Parse(node.Attributes["padding"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "paddingleft"))
                                    cell.PaddingLeft = float.Parse(node.Attributes["paddingleft"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "paddingright"))
                                    cell.PaddingRight = float.Parse(node.Attributes["paddingright"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "paddingtop"))
                                    cell.PaddingTop = float.Parse(node.Attributes["paddingtop"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "paddingbottom"))
                                    cell.PaddingBottom = float.Parse(node.Attributes["paddingbottom"].Value, NumberFormatInfo.InvariantInfo);

                                if (NodeHasAttribute(node, "indent"))
                                    cell.Indent = float.Parse(node.Attributes["indent"].Value, NumberFormatInfo.InvariantInfo);

                                if (parentObject != null)
                                {
                                    try
                                    {
                                        PdfPTable cellParent = (PdfPTable)parentObject;

                                        if (NodeHasAttribute(currentNode, "fullpage"))
                                        {
                                            if (bool.Parse(currentNode.Attributes["fullpage"].Value) &&
                                                cellParent.NumberOfColumns == 1)
                                            {

                                                cell.FixedHeight = _document.PageSize.Height - _document.TopMargin -
                                                                   _document.BottomMargin;
                                                cellParent.SetTotalWidth(new float[]
                                                                             {
                                                                                 _document.PageSize.Width - _document.LeftMargin -
                                                                                 _document.BottomMargin
                                                                             });
                                                cellParent.LockedWidth = true;
                                            }
                                        }
                                        else
                                        {
                                            if (NodeHasAttribute(currentNode, "contentwidth"))
                                            {
                                                if (bool.Parse(currentNode.Attributes["contentwidth"].Value) &&
                                                    cellParent.NumberOfColumns == 1)
                                                {
                                                    float maxWidth = 0;

                                                    for (int i = 0; i < cell.Phrase.Chunks.Count; i++)
                                                    {
                                                        Chunk chk = (Chunk)cell.Phrase.Chunks[i];
                                                        chk.Font = _lastFont;
                                                        if ((chk.GetWidthPoint() + cell.PaddingLeft + cell.PaddingRight) > maxWidth)
                                                            maxWidth = (chk.GetWidthPoint() + cell.PaddingLeft + cell.PaddingRight);

                                                    }
                                                    cellParent.SetTotalWidth(new float[] { maxWidth + 20 });
                                                    cellParent.LockedWidth = true;
                                                }
                                            }
                                        }

                                        if (node.FirstChild.Name.ToUpper() == "PTABLE")
                                        {

                                            PdfPTable ptableChild = ProcessDataPTablePart1(node.FirstChild, cellWidth);
                                            ptableChild.DefaultCell.Border = 0;
                                            ptableChild.DefaultCell.BorderWidth = 0f;
                                            cellParent.AddCell(new PdfPCell(ptableChild, ptableChild.DefaultCell));
                                        }
                                        else
                                            cellParent.AddCell(cell);
                                    }
                                    catch
                                    {

                                    }
                                }
                                else
                                {
                                    return cell;
                                }

                                // return cell;
                                break;

                            case "INDEXCELL":
                                if (parentObject.GetType().Name != "PdfPTable")
                                {
                                    throw new InvalidOperationException("PCELL can only be beneath a PTABLE tag");
                                }

                                // get cell width
                                PdfPTable parentTable = (PdfPTable)parentObject;
                                float icellWidth = 0;
                                if (parentTable.NumberOfColumns > 1)
                                {
                                    XmlNodeList preceding = node.SelectNodes("./preceding-sibling::*");
                                    int widthIdx = 0;
                                    if (parentTable.NumberOfColumns < (preceding.Count + 1))
                                    {
                                        widthIdx = ((preceding.Count + 1) % parentTable.NumberOfColumns);
                                        widthIdx = parentTable.NumberOfColumns - widthIdx - 1;// zero bound, hence -1
                                    }
                                    else
                                    {
                                        widthIdx = preceding.Count; //zero bound
                                    }
                                    icellWidth = parentTable.AbsoluteWidths[widthIdx];

                                }
                                else
                                {
                                    icellWidth = parentTable.AbsoluteWidths[0];
                                }
                                //Get indent of cell
                                float indent = 0;
                                if (NodeHasAttribute(node, "indent"))
                                    indent = int.Parse(node.Attributes["indent"].Value);

                                float tablewidth = icellWidth;

                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.BorderWidth);
                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.BorderWidthLeft);
                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.BorderWidthRight);
                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.PaddingLeft);
                                tablewidth = tablewidth - ((float)((PdfPTable)parentObject).DefaultCell.PaddingRight);
                                tablewidth = tablewidth - indent;
                                tablewidth = tablewidth - 7;

                                // temporary correction, review width calculation by IText upon processing to PDF
                                tablewidth += 40f;

                                Phrase indexPhrase = new Phrase(ProcessIndexLine(node.InnerText, tablewidth));
                                indexPhrase.Font.Size = _defaultStandardFontSize;
                                indexPhrase.Font = _defaultStandardFont;

                                PdfPCell indexCell = new PdfPCell(indexPhrase);
                                indexCell.Indent = indent;
                                indexCell.Border = 0;

                                if (parentObject != null)
                                {
                                    try
                                    {
                                        PdfPTable cellParent = (PdfPTable)parentObject;

                                        if (node.FirstChild.Name.ToUpper() == "PTABLE")
                                        {
                                            PdfPTable ptableChild = ProcessDataPTablePart1(node.FirstChild, icellWidth);
                                            ptableChild.DefaultCell.Border = 0;
                                            ptableChild.DefaultCell.BorderWidth = 0f;
                                            cellParent.AddCell(new PdfPCell(ptableChild, ptableChild.DefaultCell));
                                        }
                                        else
                                            cellParent.AddCell(indexCell);
                                    }
                                    catch
                                    {

                                    }
                                }
                                else
                                {
                                    return indexCell;
                                }
                                break;

                            case "LIST":
                                List list;
                                Properties listAttribs = GetAttributes(node);
                                bool orderedList = iTextSharp.text.Utilities.CheckTrueOrFalse(listAttribs, "ordered");
                                orderedList = orderedList || iTextSharp.text.Utilities.CheckTrueOrFalse(listAttribs, "numbered");
                                orderedList = orderedList || iTextSharp.text.Utilities.CheckTrueOrFalse(listAttribs, "lettered");
                                symbolSize = 16;
                                if (NodeHasAttribute(node, "symbolsize"))
                                    symbolSize = int.Parse(node.Attributes.GetNamedItem("symbolsize").Value, NumberFormatInfo.InvariantInfo);
                               
                                float symbolIndent = 10;
                                if (listAttribs.ContainsKey("symbolindent"))
                                {
                                    symbolIndent = float.Parse(listAttribs["symbolindent"], NumberFormatInfo.InvariantInfo);
                                }
                                if (parentObject.GetType() == typeof(List))
                                {
                                    List parentList = (List)parentObject;
                                    symbolIndent += parentList.SymbolIndent;
                                }

                                list = orderedList
                                           ? new List(List.ORDERED, symbolIndent)
                                           : new List(List.UNORDERED, symbolIndent);
                                list = ElementFactory.SetList(list, listAttribs);
                                if (list.Numbered || list.Lettered) list.Autoindent = true;
                                if (!orderedList)
                                {
                                    list.ListSymbol = new Chunk("\u2022", FontFactory.GetFont(FontFactory.HELVETICA, symbolSize));
                                }
                                else
                                {
                                    list.ListSymbol = new Chunk("-", GetFont(_standardFontSize));
                                    if (node.HasChildNodes)
                                    {
                                        XmlNode cn = node.SelectSingleNode("*[1]");
                                        if (cn != null)
                                        {
                                            list.ListSymbol = new Chunk("-", GetFont(cn));
                                            if (cn.HasChildNodes)
                                            {
                                                cn = node.SelectSingleNode("phrase|paragraph[1]");
                                                if (cn != null)
                                                {
                                                    list.ListSymbol = new Chunk("-", GetFont(cn));
                                                }
                                            }
                                        }
                                    }
                                }
                                list = (List)ProcessData(node, list);
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        parent.Add(list);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = list;
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    return list;
                                }
                                break;
                            case "IMAGE":
                                if (NodeHasAttribute(node, "id"))
                                {
                                    Image img = GetImage(node.Attributes.GetNamedItem("id").Value);
                                    if (NodeHasAttribute(node, "center"))
                                    {
                                        img.Alignment = Element.ALIGN_CENTER;
                                    }
                                    if (NodeHasAttribute(node, "indentationleft"))
                                    {
                                        img.IndentationLeft = float.Parse(node.Attributes.GetNamedItem("indentationleft").Value);
                                    }
                                    if (parentObject != null)
                                    {
                                        if (parentObject.Type == Element.CELL)
                                        {
                                            // img.ScaleAbsoluteHeight(_lastFont.Size - 1f);
                                            img.ScalePercent(50f);
                                        }
                                        try
                                        {
                                            ITextElementArray parent = (ITextElementArray)parentObject;
                                            parent.Add(img);
                                            parentObject = parent;
                                        }
                                        catch
                                        {
                                            parentObject = img;
                                            return parentObject;
                                        }
                                    }
                                    else
                                    {
                                        return img;
                                    }
                                }
                                break;
                            case "LISTITEM":
                                Paragraph listPara = ElementFactory.GetParagraph(GetAttributes(node));
                                if (NodeHasAttribute(node, "lineheight"))
                                    listPara.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, NumberFormatInfo.InvariantInfo) * listPara.Font.Size;
                                if (NodeHasAttribute(node, "align"))
                                    listPara.Alignment = GetAlignment(node.Attributes.GetNamedItem("align").Value, NodeHasAttribute(node, "verticalalign") ? node.Attributes.GetNamedItem("verticalalign").Value : null);

                                listPara = (Paragraph)ProcessData(node, listPara);
                                List itemParentList = (List)parentObject;
                                itemParentList.Add(new ListItem(listPara));
                                parentObject = itemParentList;
                                break;
                            case "PARAGRAPH":
                                Paragraph paragraph = new Paragraph();

                                paragraph.Font = GetFont(node);
                                if (NodeHasAttribute(node, "lineheight"))
                                    paragraph.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, NumberFormatInfo.InvariantInfo) * paragraph.Font.Size;
                                if (NodeHasAttribute(node, "align"))
                                    paragraph.Alignment = GetAlignment(node.Attributes.GetNamedItem("align").Value, NodeHasAttribute(node, "verticalalign") ? node.Attributes.GetNamedItem("verticalalign").Value : null);
                                if (NodeHasAttribute(node, "indentationleft"))
                                    paragraph.IndentationLeft = float.Parse(node.Attributes.GetNamedItem("indentationleft").Value, NumberFormatInfo.InvariantInfo);
                                if (NodeHasAttribute(node, "indentationright"))
                                    paragraph.IndentationRight = float.Parse(node.Attributes.GetNamedItem("indentationright").Value, NumberFormatInfo.InvariantInfo);
                                if (NodeHasAttribute(node, "spacingafter"))
                                    paragraph.SpacingAfter = float.Parse(node.Attributes.GetNamedItem("spacingafter").Value, NumberFormatInfo.InvariantInfo);
                                if (NodeHasAttribute(node, "spacingbefore"))
                                    paragraph.SpacingBefore = float.Parse(node.Attributes.GetNamedItem("spacingbefore").Value, NumberFormatInfo.InvariantInfo);

                                paragraph = (Paragraph)ProcessData(node, paragraph);
                                paragraph.Font = GetFont(node);
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        parent.Add(paragraph);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = paragraph;
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    return paragraph;
                                }
                                break;

                            case "SYMBOL":
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        //BaseFont bf = BaseFont.CreateFont(FontFactory.HELVETICA, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                                        symbolSize = 16;

                                        if (NodeHasAttribute(node, "symbolsize"))
                                            symbolSize = int.Parse(node.Attributes.GetNamedItem("symbolsize").Value, NumberFormatInfo.InvariantInfo);

                                        Chunk chunk = new Chunk("\u2022   ", FontFactory.GetFont(FontFactory.HELVETICA, symbolSize));
                                        parent.Add(chunk);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = new Chunk(node.InnerText, FontFactory.GetFont(GetAttributes(node)));
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    parentObject = new Chunk(node.InnerText, FontFactory.GetFont(GetAttributes(node)));
                                    return parentObject;
                                }
                                break;
                            case "CHUNK":
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        Chunk chunk = new Chunk(node.InnerText, FontFactory.GetFont(GetAttributes(node)));
                                        parent.Add(chunk);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = new Chunk(node.InnerText, FontFactory.GetFont(GetAttributes(node)));
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    parentObject = new Chunk(node.InnerText, FontFactory.GetFont(GetAttributes(node)));
                                    return parentObject;
                                }
                                break;
                            case "PHRASE":
                                Phrase phrase = null;
                                if (node.HasChildNodes)
                                {
                                    if (node.FirstChild.NodeType == XmlNodeType.Text || node.FirstChild.NodeType == XmlNodeType.CDATA)
                                    {
                                        phrase = new Phrase();
                                        phrase.Font = GetFont(node);
                                        if (NodeHasAttribute(node, "lineheight"))
                                            phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                                        phrase = (Phrase)ProcessData(node, phrase);

                                        //phrase = new Phrase(ReplaceVariableWithValue(node.FirstChild.InnerText), GetFont(node));
                                        //if (NodeHasAttribute(node, "lineheight"))
                                        //    phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                                    }
                                    else
                                    {
                                        if (node.FirstChild.Name.ToUpper() == "PHRASE")
                                        {
                                            phrase = new Phrase();
                                            phrase.Font = GetFont(node);
                                            if (NodeHasAttribute(node, "lineheight"))
                                                phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;

                                            phrase = (Phrase)ProcessData(node, phrase);
                                        }
                                        else
                                        {
                                            phrase = new Phrase();
                                            phrase.Font = GetFont(node);
                                            if (NodeHasAttribute(node, "lineheight"))
                                                phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                                            phrase = (Phrase)ProcessData(node, phrase);
                                        }
                                    }
                                }
                                else
                                {
                                    phrase = new Phrase();
                                    phrase.Font = GetFont(node);
                                    if (NodeHasAttribute(node, "lineheight"))
                                        phrase.Leading = float.Parse(node.Attributes.GetNamedItem("lineheight").Value, CultureInfo.InvariantCulture.NumberFormat) * phrase.Font.Size;
                                }

                                if (parentObject == null)
                                {
                                    parentObject = phrase;
                                    return parentObject;
                                }

                                try
                                {
                                    ITextElementArray parent = (ITextElementArray)parentObject;
                                    parent.Add(phrase);
                                    parentObject = parent;
                                }
                                catch
                                {
                                    parentObject = phrase;
                                    return parentObject;
                                }
                                break;
                            case "NEWLINE":
                                Chunk chkNewLine = Chunk.NEWLINE;
                                chkNewLine.Font = _standardFont;
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        parent.Add(chkNewLine);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = chkNewLine;
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    parentObject = chkNewLine;
                                    return parentObject;
                                }
                                break;
                            case "IMPORT":

                                Properties attributes = GetAttributes(node);

                                if (parentObject != null)
                                {
                                    try
                                    {
                                        Section parent = (Section)parentObject;
                                        bool first = true;
                                        for (int i = int.Parse(attributes["fromPage"]);
                                             i <= int.Parse(attributes["toPage"]);
                                             i++)
                                        {
                                            Chunk importChunk = new Chunk(" ");
                                            importChunk.SetGenericTag(GetImportGenericTag(attributes, i, first));
                                            if (node.ParentNode.PreviousSibling != null && node.ParentNode.PreviousSibling.Name.ToLower() == "titlepage")
                                            {
                                                if (!first) parent.Add(new Chunk().SetNewPage());
                                            }
                                            else
                                                parent.Add(new Chunk().SetNewPage());

                                            first = false;
                                            parent.Add(importChunk);
                                        }

                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    return parentObject;
                                }
                                break;
                            case "SPACE":
                                if (parentObject != null)
                                {
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        Chunk chunk = new Chunk(" ", _lastFont);
                                        parent.Add(chunk);
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        parentObject = new Chunk(" ", _lastFont);
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    parentObject = new Chunk(" ", _lastFont);
                                    return parentObject;
                                }
                                break;
                            case "NEWPAGE":
                                if (NodeHasAttribute(node, "orientation"))
                                {
                                    switch (node.Attributes.GetNamedItem("orientation").Value.ToLower())
                                    {
                                        case "portrait":
                                            _document.SetPageSize(_pageSizePortret);
                                            break;
                                        case "landscape":
                                            _document.SetPageSize(_pageSizePortret.Rotate());
                                            break;
                                    }
                                }
                                
                                if (parentObject != null)
                                {
                                    //new Chunk().setN
                                    try
                                    {
                                        ITextElementArray parent = (ITextElementArray)parentObject;
                                        if (parent.Type == iTextSharp.text.Element.CHAPTER)
                                        {
                                            parent.Add(new Chunk().SetNewPage());
                                        }
                                        else
                                        {
                                            parent.Add(new Chunk().SetNewPage());
                                        }
                                        parent.Add(new Chunk());
                                        parentObject = parent;
                                    }
                                    catch
                                    {
                                        _document.NewPage();
                                       // parentObject = new Chunk().SetNewPage();
                                        parentObject = new Chunk();
                                        return parentObject;
                                    }
                                }
                                else
                                {
                                    _document.NewPage();
                                    //parentObject = new Chunk().SetNewPage();
                                    parentObject = new Chunk();
                                    return parentObject;
                                }

                                break;
                        }
                        break;
                    case XmlNodeType.Text:
                    case XmlNodeType.CDATA:
                        /*
                         * GOAL:
                         * Replace all known variables in a string with their value,
                         * variables are within rectangular brackets "[XXX]"
                         * for example, [PAGE_NUMBER] has to be replaced by the current page number.
                         */

                        Regex re = new Regex("\\[([A-Z]|_){1,}\\]");//("\[([A-Z]|_){1,}\]");
                        MatchCollection mc = re.Matches(node.InnerText);

                        foreach (Match m in mc)
                        {
                            if (m.Value == "[PAGE_NUMBER]")
                            {
                                string tst = string.Empty;
                                //m.Value = page_number.ToString();
                            }
                        }

                        if (parentObject != null)
                        {
                            try
                            {
                                ITextElementArray parent = (ITextElementArray)parentObject;
                                parent.Add(new Chunk(node.InnerText, _lastFont));
                                parentObject = parent;
                            }
                            catch
                            {
                                parentObject = new Chunk(node.InnerText, _lastFont);
                                return parentObject;
                            }
                        }
                        else
                            return new Chunk(node.InnerText);

                        break;
                }
            }
            return parentObject;
        }

        protected float GetWidth(string text, Font font)
        {
            Chunk chk = new Chunk(text);
            chk.Font = font;
            return chk.GetWidthPoint();
        }

        protected string ProcessIndexLine(string indexLine, float tableWidth)
        {
          

            Font font = new Font(_standardFont.BaseFont, _standardFontSize);

            //while (GetWidth(string.Concat(parts), font) < (tableWidth - 4))
            while (GetWidth(indexLine, font) < (tableWidth - 4))
            //while(GetWidth(indexLine,font) < (tableWidth - 30))
            {
                indexLine = indexLine + ".";
                //parts[1] = parts[1] + ".";
            }

            // indexLine = string.Concat(parts);

            return indexLine;
        }

        protected void ProcessContentBlock(PdfContentByte over, XmlNode block, Rectangle rect)
        {
            int align = Element.ALIGN_UNDEFINED;
            if (block.Attributes["align"] != null)
                align = GetAlignment(block.Attributes["align"].Value);

            float top = GetAttributeValue(block, "top");
            float left = GetAttributeValue(block, "left");

            if (left < 0)
                left += rect.Width;

            if (top > 0)
                top = rect.Height - top;
            else
                top = Math.Abs(top);

            BaseFont bf = _standardFont.BaseFont;

            foreach (XmlNode subNode in block.SelectNodes("(phrase|newline|image)"))
            {
                Properties attributes = GetAttributes(subNode);

                switch (subNode.Name.ToUpper())
                {
                    case "NEWLINE":
                        top -= 20;
                        break;
                    case "PHRASE":
                        string text = ReplaceVariableWithValue(subNode.InnerText);

                        Phrase phText = new Phrase(text, GetFont(subNode));

                        ColumnText.ShowTextAligned(over, align, phText, left, top, 0);

                        float padding = 2;
                        if (attributes.ContainsKey("padding"))
                            padding = float.Parse(attributes["padding"], NumberFormatInfo.InvariantInfo);

                        top -= (phText.Font.Size + padding);
                        break;
                    case "IMAGE":
                        Image img = ElementFactory.GetImage(attributes);
                        img.SetAbsolutePosition(left, top);
                        if (attributes.ContainsKey("scalepercent"))
                            img.ScalePercent(float.Parse(attributes["scalepercent"], NumberFormatInfo.InvariantInfo));

                        over.AddImage(img);
                        break;
                }
            }
        }

        /// <summary>
        /// Replace all known variables in a string with their value,
        /// variables are within rectangular brackets "[XXX]"
        /// for example, [PAGE_NUMBER] has to be replaced by the current page number.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        protected string ReplaceVariableWithValue(string text)
        {
            //Regex re = new Regex(@"\[(?<Key>([A-Z]|_){1,})(\:\:){0,1}(?<Item>([A-Z]|_){1,}){0,1}\]", RegexOptions.IgnoreCase);
            ///*
            // * (\:\:){0,1}
            // * ?<Item>(([A-Z]|_){1,})
            // * \\[(([A-Z]|_){1,}\)\]
            // */
            //MatchCollection mc = re.Matches(text);

            //foreach (Match m in mc)
            //{
            //    if (m.Groups["Key"].Success)
            //    {
            //        string type = "";
            //        string item = m.Groups["Key"].Value;
            //        if (m.Groups["Item"].Success)
            //        {

            //            type = m.Groups["Key"].Value;
            //            item = m.Groups["Item"].Value;
            //        }


            //        switch (type.ToUpper())
            //        {
            //            case "CLIENT":
            //                switch (item.ToUpper())
            //                {
            //                    case "NAME":
            //                        text = text.Replace(m.Value, Client.Name);
            //                        break;
            //                    case "ADDRESS":
            //                        text = text.Replace(m.Value, Client.Address);
            //                        break;
            //                    case "ZIPCODE":
            //                    case "ZIP":
            //                        text = text.Replace(m.Value, Client.ZipCode);
            //                        break;
            //                    case "CITY":
            //                        text = text.Replace(m.Value, Client.City);
            //                        break;
            //                    case "COUNTRY":
            //                        text = text.Replace(m.Value, Client.Country);
            //                        break;
            //                }
            //                break;
            //            case "FILE":
            //                switch (item.ToUpper())
            //                {
            //                    case "ENDDATE":
            //                        text = text.Replace(m.Value, File.EndDate.ToString(DateTimeFormatInfo.CurrentInfo.ShortDatePattern));
            //                        break;
            //                    case "STARTDATE":
            //                        text = text.Replace(m.Value, File.StartDate.ToString(DateTimeFormatInfo.CurrentInfo.ShortDatePattern));
            //                        break;
            //                }
            //                break;
            //            case "CHAPTER":
            //                ChapterDataContract cdc = (ChapterDataContract)_currentIndexItem;
            //                switch (item.ToUpper())
            //                {
            //                    case "TITLE":
            //                        text = text.Replace(m.Value, cdc.Title);
            //                        break;
            //                    case "PAGE_NUMBER":
            //                        text = text.Replace(m.Value, page_number.ToString());
            //                        break;
            //                    case "PAGE_TOTAL":
            //                        text = text.Replace(m.Value, page_total.ToString());
            //                        break;
            //                    case "HEADERFOOTER":
            //                        // text = text.Replace(m.Value, cdc.HeaderFooter);
            //                        break;
            //                }
            //                break;
            //            default:
            //                switch (item.ToUpper())
            //                {
            //                    case "PAGE_NUMBER":
            //                        text = text.Replace(m.Value, page_number.ToString());
            //                        break;
            //                    case "PAGE_TOTAL":
            //                        text = text.Replace(m.Value, page_total.ToString());
            //                        break;
            //                }
            //                break;
            //        }
            //    }
            //}
            return text;
        }

        protected void ProcessDataTable(XmlNode node, IElement parentObject, Boolean withParent)
        {
            Table table = ElementFactory.GetTable(GetAttributes(node));
            float[] widths = table.ProportionalWidths;
            for (int i = 0; i < widths.Length; i++)
                if (widths[i] == 0)
                    widths[i] = 100.0f / widths.Length;
            try
            {
                table.Widths = widths;
            }
            catch (BadElementException bee)
            {
                // this shouldn't happen
                throw new Exception("", bee);
            }
            finally
            {
                table = (Table)ProcessData(node, table);
                if (withParent)
                {
                    try
                    {
                        ITextElementArray parento = (ITextElementArray)parentObject;

                        //if (parento.Type == Element.CELL)
                        //{
                        //    parentObject = 
                        //}
                        //else
                        //{

                        parento.Add(table);
                        parentObject = parento;
                        //}
                    }
                    catch
                    {
                    }
                }
                else
                    _document.Add(table);
            }
        }


        protected PdfPTable ProcessDataPTablePart1(XmlNode node, float width)
        {

            int columnCount = int.Parse(node.Attributes.GetNamedItem("columns").Value);
            PdfPTable ptable = new PdfPTable(columnCount);
            if (width > 0) ptable.TotalWidth = width;
            if (node.Attributes.GetNamedItem("width") != null) ptable.TotalWidth = float.Parse(node.Attributes.GetNamedItem("width").Value);
            if (ptable.TotalWidth == 0) ptable.TotalWidth = (_document.PageSize.Width - _document.LeftMargin - _document.BottomMargin) * (ptable.WidthPercentage / 100);
            if (NodeHasAttribute(node, "columnWidths"))
            {
                List<float> cfWidths = new List<float>();
                foreach (string wd in node.Attributes.GetNamedItem("columnWidths").Value.Split(new char[] { ',' }))
                {
                    cfWidths.Add(float.Parse(wd));
                }
                ptable.SetWidths(cfWidths.ToArray());
            }
            ptable = (PdfPTable)ProcessData(node, ptable);
            return ptable;
        }

        #endregion

        protected Image GetImage(string name)
        {
            string path = Path.Combine(_imageFolder, string.Format("{0}.png", name));
            Stream sr = new FileStream(path, FileMode.Open, FileAccess.Read);
            System.Drawing.Image img = System.Drawing.Image.FromStream(sr);
            return Image.GetInstance(img, System.Drawing.Imaging.ImageFormat.Png);
        }

        #region IDisposable Members

        public void Dispose()
        {

            
            pdfFiles = null;


            if (writer != null) { try { writer.Close(); } catch { };}
            _standardFont = null;
            bf = null;
            _document = null;
            _pageSize = null;
            _defaultStandardFont = null;
            _pageSizeLandscape = null;
            _pageSizePortret = null;
            _emptyPages = null;
            
            _rootNode=null;
            _sourceDocument = null;
            _pdfDataContract = null;
            // System.GC.SuppressFinalize(this);
        }

        #endregion

        protected int InsertPDF(string pdfLocation)
        {
            return InsertPDF(pdfLocation, -1, -1);
        }

        protected int InsertPDF(string pdfLocation, int fromPage, int toPage)
        {
            int startsAt = 0;
            PdfContentByte cb = writer.DirectContent;
            PdfImportedPage page;
            int rotation;
            //AddHeaderAndFooters2(((PDFDataContract)iibdc).HeaderFooter, ((PDFDataContract)iibdc).PDFLocation);
            PdfReader reader = new PdfReader(pdfLocation);
            reader.RemoveUsageRights();
            toPage = toPage > -1 ? toPage : reader.NumberOfPages;
            fromPage = fromPage > -1 ? fromPage : 1;

            for (int i = fromPage; i <= toPage; i++)
            {
                _document.SetPageSize(reader.GetPageSizeWithRotation(i));
                _document.NewPage();
                _document.Add(new Chunk(" "));
                if (i == fromPage) startsAt = writer.CurrentPageNumber;
                page = writer.GetImportedPage(reader, i);
                
                 // page.ToString()
                rotation = reader.GetPageRotation(i);
                //PdfTemplate templ = PdfTemplate.CreateTemplate(writer,page.Width,page.Height);
                  //  templ.Add
                //templ.AddRaw(reader.GetPageContent(i));
                if (rotation == 90 || rotation == 270)
                    cb.AddTemplate(page, 0, 0, 0, 0, 0, reader.GetPageSizeWithRotation(i).Height);

                else
                    cb.AddTemplate(page, 0, 0);

                //if (rotation == 90 || rotation == 270)
                //    cb.AddRawTemplate(reader.GetPageContent(i), 0, 0, 0, 0, 0, reader.GetPageSizeWithRotation(i).Height);

                //else
                //    cb.AddRawTemplate(reader.GetPageContent(i), 0, 0);
            }
            _pageSize = PageSize.A4;
            //_document.SetPageSize(PageSize.A4);
            return startsAt;
        }

        protected void RenderHeaderFooter(PdfContentByte over, XmlNode xmlNode, Rectangle rect, int currentPage, int totalPages)
        {
            bool isLandscape = rect.Height < rect.Width;
            foreach (XmlNode block in xmlNode.SelectNodes("block"))
            {
                float top = 0f;
                float left = 0f;
                //float.Parse(block.Attributes["top"].Value, NumberFormatInfo.InvariantInfo);
                int align = Element.ALIGN_UNDEFINED;
                switch (block.Attributes["type"].Value.ToUpper())
                {
                    case "TOPLEFT":
                        align = Element.ALIGN_LEFT;
                        top = isLandscape ? 20f : 20f;
                        left = isLandscape ? 50f : 20f;
                        break;
                    case "TOPRIGHT":
                        align = Element.ALIGN_RIGHT;
                        top = isLandscape ? 20f : 20f;
                        left = -50f;
                        break;
                    case "TOPTITLE":
                        align = Element.ALIGN_LEFT;
                        top = 85f;
                        left = 60f;
                        break;
                    case "BOTTOMCENTER":
                        align = Element.ALIGN_CENTER;
                        top = -50f;
                        left = isLandscape ? 440f : 300f;
                        break;
                }


                if (left < 0)
                {
                    left += rect.Width;
                }
                if (top > 0)
                {
                    top = rect.Height - top;
                }
                else
                {
                    top = Math.Abs(top);
                }

                foreach (XmlNode subNode in block.SelectNodes("(phrase|image)"))
                {
                    Properties attributes = GetAttributes(subNode);

                    switch (subNode.Name.ToUpper())
                    {
                        case "PHRASE":
                            string itemText = subNode.InnerText;
                            itemText = itemText.Replace("[PAGE_NUMBER]", currentPage.ToString());
                            itemText = itemText.Replace("[PAGE_TOTAL]", totalPages.ToString());

                            Phrase phText = new Phrase(itemText, GetFont(subNode));

                            ColumnText.ShowTextAligned(over, align, phText, left, top, 0);

                            float padding = 2;
                            if (attributes.ContainsKey("padding"))
                            {
                                padding = float.Parse(attributes["padding"], NumberFormatInfo.InvariantInfo);
                            }

                            top -= (phText.Font.Size + padding);
                            break;
                        /*  case "IMAGE":
                              Image img = ElementFactory.GetImage(attributes);
                              img.SetAbsolutePosition(left, top);
                              if (attributes.ContainsKey("scalepercent"))
                              {
                                  img.ScalePercent(float.Parse(attributes["scalepercent"], NumberFormatInfo.InvariantInfo));
                              }
                              over.AddImage(img);
                              break;*/
                    }
                }
            }
        }



        public void PostProcessHeaderFooter(PDFDataContract pdc, XElement blocks, string resultPath)
        {
            
            PdfReader reader = new PdfReader(pdc.PDFLocation, true);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(resultPath, FileMode.Create));

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(blocks.ToString());

            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
                if (reader.GetPageContent(i).Length > 20)
                {
                    RenderHeaderFooter(stamper.GetOverContent(i), doc.DocumentElement, stamper.Reader.GetPageSize(i), i, stamper.Reader.NumberOfPages);
                }
                
            }

            reader.Close();
            stamper.Close();

            writer = null;

            stamper = null;
            reader = null;

        }
    }
}
