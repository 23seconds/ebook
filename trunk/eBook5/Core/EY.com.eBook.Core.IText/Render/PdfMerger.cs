﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace EY.com.eBook.Core.IText.Render
{
   

    public class PdfMerge
    {
        //Pre-Condition:
        // string destinationFile, the file which will be saved for output after merger
        // string[] sourceFiles, a string array of the location of the files to be merged
        // string[] pageName, a string array of the bookmark names for each pdf file to be merged
        //Post-Condition:
        // the seperate pdf files are merged with bookmarks added then saved to the destinationFile
        public static void MergeFiles(string destinationFile, string[] sourceFiles, string[] pageName)
        {
            try
            {
                int f = 0;
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(sourceFiles[f]);
                // we retrieve the total number of pages
                int n = reader.NumberOfPages;
                //Console.WriteLine("There are " + n + " pages in the original file.");
                // step 1: creation of a document-object
                Document document = new Document(reader.GetPageSizeWithRotation(1));
                // step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationFile, FileMode.Create));
                // step 3: we open the document
                document.Open();
                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;

                int rotation;
                // step 4: we add content
                int pdfPageName = 0;
                bool pdfNewPageFlag = true;
                while (f < sourceFiles.Length)
                {
                    int i = 0;
                    while (i < n)
                    {
                        i++;
                        document.SetPageSize(reader.GetPageSizeWithRotation(i));
                        document.NewPage();
                        if (pdfNewPageFlag == true)
                        {
                            Chapter chapter = new Chapter(pageName[pdfPageName], pdfPageName + 1);
                            document.Add(chapter);
                            pdfNewPageFlag = false;
                            pdfPageName++;
                        }

                        page = writer.GetImportedPage(reader, i);
                        rotation = reader.GetPageRotation(i);

                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                        //Console.WriteLine("Processed page " + i);

                    }
                    f++;
                    if (f < sourceFiles.Length)
                    {
                        reader = new PdfReader(sourceFiles[f]);
                        // we retrieve the total number of pages
                        n = reader.NumberOfPages;


                        //Console.WriteLine("There are " + n + " pages in the original file.");
                        //
                        pdfNewPageFlag = true;
                    }
                }
                // step 5: we close the document
                document.Close();
            }
            catch (Exception e)
            {
                //Console.Error.WriteLine(e.Message);
                //Console.Error.WriteLine(e.StackTrace);
            }
        }
    }
}
