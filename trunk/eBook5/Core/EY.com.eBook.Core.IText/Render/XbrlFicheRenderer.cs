﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using iTextSharp.text.pdf;
using System.Globalization;
using iTextSharp.text;
using System.util;
using iTextSharp.text.factories;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;

namespace EY.com.eBook.Core.IText.Render
{
    /*
    public class XbrlFicheRenderer : ITextRenderer
    {
        private int _border = 0;
        private float _borderWidth = 0;
        private Font _font;
        private Font _textfont;
        private Font _titlefont;
        private Font _smalltextfont;
        private XbrlFicheItemDataContract _xfidc;
        private List<XbrlAttachmentNodeDataContract> _nodes;
        private XmlNamespaceManager _namespaceManager;
        private XmlNameTable _nameTable;
        private float _grayFill = 1f;

        public XbrlFicheRenderer(XbrlFicheItemDataContract xfidc, List<XbrlAttachmentNodeDataContract> nodes)
            : base(null,xfidc)
        {
            _xfidc = xfidc;
            _nodes = nodes;
            XmlReader reader = XmlReader.Create(new StringReader(xfidc.XbrlData.ToString()));
           
            _nameTable = reader.NameTable;
            _namespaceManager = new XmlNamespaceManager(_nameTable);
            _namespaceManager.AddNamespace("tax-inc", "http://www.minfin.fgov.be/be/tax/inc/2011-04-30");
            reader.Close();
        }

        public override void HandleDocument()
        {
            _document.SetMargins(60f, 60f, 90f, 55f);
            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            _font = new Font(bf, 8f);
            _textfont = new Font(bf, 10f);
            _smalltextfont = new Font(bf, 6f);
            _titlefont = new Font(bf, 10f);
            _titlefont.SetStyle(Font.BOLD | Font.UNDERLINE);

            switch (_xfidc.Fiche.Id.ToUpper())
            {
                case "2043":
                    Render2043();
                    break;
                case "275C":
                    Render275C();
                    break;
            }
        }

        private void Render275C()
        {

            Chapter chapter = new Chapter(0);
            Table _table = new Table(3);
            _table.TableFitsPage = true;
            _table.Convert2pdfptable = true;
            _table.CellsFitPage = true;
            _table.Width = 100f;
            _table.Alignment = Element.ALIGN_CENTER;
            _table.Border = 0;
            _table.Cellpadding = 1f;
            _table.Cellspacing = 0f;
            _table.SetWidths(new int[] { 70,15,15 });

            

            _table.AddCell(GetTitle(GetTranslation("AllowanceCorporateEquityForm"),3));
            _table.AddCell(GetSpacer(3));

            _border = 1;
            _borderWidth = 0.2f;
            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("Equity"), 2), "LTB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("Equity"),"F"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DeductionsEquityAllowanceCorporateEquity"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("DeductionsEquityAllowanceCorporateEquity"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("OwnSharesFiscalValue")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("OwnSharesFiscalValue"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("FinancialFixedAssetsParticipationsOtherShares")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("FinancialFixedAssetsParticipationsOtherShares"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("SharesInvestmentCorporations")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("SharesInvestmentCorporations"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1), "RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("BranchesCountryTaxTreaty")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("BranchesCountryTaxTreaty"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ImmovablePropertyCountryTaxTreaty")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("ImmovablePropertyCountryTaxTreaty"), "LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("TangibleFixedAssetsUnreasonableRelatedCosts")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("TangibleFixedAssetsUnreasonableRelatedCosts"), "LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("InvestmentsNoPeriodicalIncome")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("InvestmentsNoPeriodicalIncome"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ImmovablePropertyUseManager")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("ImmovablePropertyUseManager"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("UnrealisedExpressedCapitalGains")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("UnrealisedExpressedCapitalGains"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("InvestmentGrants")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("InvestmentGrants"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ActualisationStockRecognisedDiamondTraders")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("ActualisationStockRecognisedDiamondTraders"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch")),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch"),"LB"));
            _table.AddCell(SetBorderCell(GetSpacerFilled(1),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("MovementEquityAfterDeductionsAllowanceCorporateEquity"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("MovementEquityAfterDeductionsAllowanceCorporateEquity"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("AllowanceCorporateEquityCurrentTaxPeriod"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("AllowanceCorporateEquityCurrentTaxPeriod"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DeductibleAllowanceCorporateEquityCurrentAssessmentYear"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("DeductibleAllowanceCorporateEquityCurrentAssessmentYear"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DeductionAllowanceCorporateEquityCurrentAssessmentYear"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("DeductionAllowanceCorporateEquityCurrentAssessmentYear"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("AllowanceCorporateEquityPreviousAssessmentYears"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("AllowanceCorporateEquityPreviousAssessmentYears"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DeductionAllowanceCorporateEquityPreviousAssessmentYears"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("DeductionAllowanceCorporateEquityPreviousAssessmentYears"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("AllowanceCorporateEquity"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("AllowanceCorporateEquity",1,"D"),"RLB"));

            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("CarryOverInsufficientProfitsAllowanceCorporateEquity"), 2),"LB"));
            _grayFill = 1f;
            _table.AddCell(SetBorderCell(GetFieldValue("CarryOverInsufficientProfitsAllowanceCorporateEquity"), "RLB"));
            
            chapter.Add(_table);

            _document.Add(chapter);
        }

        private Cell GetFieldValue(string element)
        {
            return GetFieldValue(element, 1);
        }

        private Cell GetFieldValue(XElement xe)
        {
            return GetFieldValue(1,xe);
        }

        private Cell GetFieldName(string element)
        {
            return GetFieldName(element, 1);
        }

        private Cell GetFieldValue(string element, int colspan)
        {
            XElement xe= _xfidc.XbrlData.XPathSelectElement(string.Format(@"//tax-inc:{0}",element),_namespaceManager);
            return GetFieldValue(colspan, xe);
        }

        private Cell GetFieldValue(string element, int colspan, string contextRef)
        {
            XElement xe = _xfidc.XbrlData.XPathSelectElement(string.Format(@"//tax-inc:{0}[@contextRef='" + contextRef + "']", element), _namespaceManager);
            return GetFieldValue(colspan, xe);
        }

        private Cell GetFieldValue(int colspan, XElement xe)
        {

            Cell cell = new Cell();
            cell.Colspan = colspan;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = _border;
            cell.BorderWidth = _borderWidth;
            
            
            cell.BorderWidthBottom = _borderWidth;
            cell.BorderWidthLeft = _borderWidth;
            cell.BorderWidthRight = _borderWidth;
            cell.BorderWidthTop = _borderWidth;
            Phrase phrase;
            if (xe != null)
            {
                decimal d = 0;
                decimal.TryParse(xe.Value, out d);
                phrase = new Phrase(d.ToString("#,0.00 €", CultureInfo.CreateSpecificCulture("nl-BE").NumberFormat), _font);

            }
            else
            {
                phrase = new Phrase(" ", _font);
            }
            phrase.Leading = 2f * phrase.Font.Size;
            cell.Leading = phrase.Leading;
            cell.Add(phrase);
            return cell;


        }

        private Cell GetFieldValueString( int colspan, XElement xe)
        {

            Cell cell = new Cell();
            cell.Colspan = colspan;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = _border;
            cell.BorderWidth = _borderWidth;
            cell.BorderWidthBottom = _borderWidth;
            cell.BorderWidthLeft = _borderWidth;
            cell.BorderWidthRight = _borderWidth;
            cell.BorderWidthTop = _borderWidth;
            Phrase phrase;
            if (xe != null)
            {
                phrase = new Phrase(xe.Value, _font);

            }
            else
            {
                phrase = new Phrase(" ", _font);
            }
            phrase.Leading = 2f * phrase.Font.Size;
            cell.Leading = phrase.Leading;
            cell.Add(phrase);
            return cell;


        }

        private Cell GetFieldName(string name, int colspan)
        {
            return GetFieldName(name, colspan, _textfont);
        }

        private Cell GetFieldName(string name, int colspan, Font font)
        {
            Cell cell = new Cell();
            cell.Colspan = colspan;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = _border;
            cell.BorderWidth = _borderWidth;
            cell.BorderWidthBottom = _borderWidth;
            cell.BorderWidthLeft = _borderWidth;
            cell.BorderWidthRight = _borderWidth;
            cell.BorderWidthTop = _borderWidth;
            cell.GrayFill = _grayFill;
            Phrase phrase = new Phrase(name, font);
            phrase.Leading = 1.2f * phrase.Font.Size;
            cell.Leading = phrase.Leading;
            cell.Add(phrase);
            return cell;
        }

        private Cell GetSpacer(int colspan)
        {
            Cell cell = new Cell();
            cell.Colspan = colspan;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = _border;
            cell.BorderWidth = _borderWidth;
            
            cell.Add(new Chunk("\n",_font));
            return cell;
        }

        private Cell GetSpacerFilled(int colspan)
        {
            Cell cell = new Cell();
            cell.Colspan = colspan;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = _border;
            cell.BorderWidth = _borderWidth;
            cell.GrayFill = 0.9f;

            cell.Add(new Chunk("\n", _font));
            return cell;
        }

        private Cell GetTitle(string title,int colspan)
        {
            Cell cell = new Cell();
            cell.Colspan = colspan;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.Border = 0;
            cell.BorderWidth = 0;
            

            Phrase phrase = new Phrase(title, _titlefont);
            phrase.Leading = 2f * phrase.Font.Size;
            cell.Leading = phrase.Leading;
            cell.Add(phrase);
            return cell;
        }

        private void Render2043()
        {

            _font = new Font(bf, 6f);
            _textfont = new Font(bf,6f);
            _smalltextfont = new Font(bf, 6f);
            _titlefont = new Font(bf, 10f);
            _titlefont.SetStyle(Font.BOLD | Font.UNDERLINE);


            _document.SetPageSize(new RectangleReadOnly(842, 595));
            Chapter chapter = new Chapter(0);

            Table _table = new Table(11);
            _table.TableFitsPage = true;
            _table.Convert2pdfptable = true;
            _table.CellsFitPage = true;
            _table.Width = 100f;
            _table.Alignment = Element.ALIGN_CENTER;
            _table.Border = 0;
            _table.Cellpadding = 1f;
            _table.Cellspacing = 0f;
            _table.SetWidths(new int[] { 10,9,9,9,9,9,9,9,9,9,9});



            _table.AddCell(GetTitle(GetTranslation("WriteDownsDebtClaimsProvisionsExpensesRisksForm"), 11));
            _table.AddCell(GetSpacer(11));

            _titlefont = new Font(bf, 7f);
            _titlefont.SetStyle(Font.BOLD | Font.UNDERLINE);

            _table.AddCell(GetTitle(GetTranslation("WriteDownsDebtClaimsSection"), 11));
            _table.AddCell(GetSpacer(11));

            _border = 1;
            _borderWidth = 0.2f;
            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("IdentityTradeDebtor"),1),"LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("AddressTradeDebtor"), 1),"LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DebtClaim"), 1),"LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DebtClaimEnd"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ExemptWriteDownDebtClaim"), 1),"LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ExemptWriteDownDebtClaimEnd"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod"), 1),"LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod"), 1),"LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss"), 1),"LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("IncreaseExemptWriteDownDebtClaim"), 1),"LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("JustificationExemptWriteDown"), 1), "F"));

            _grayFill = 1f;

            List<string> refs = _xfidc.XbrlData.XPathSelectElements("//tax-inc:IdentityTradeDebtor",_namespaceManager).Select(e => e.Attribute("contextRef").Value).Distinct().ToList();
            refs = refs.Select(r => r.Replace("D__", "").Replace("I-Start__", "").Replace("I-End__", "")).ToList();

            foreach (string rid in refs)
            {
                WriteDubDebLine(rid, ref _table);
            }

            chapter.Add(_table);
            chapter.Add(new Paragraph("   ", _font));

            _table = new Table(8);
            _table.TableFitsPage = true;
            _table.Convert2pdfptable = true;
            _table.CellsFitPage = true;
            _table.Width = 100f;
            _table.Alignment = Element.ALIGN_CENTER;
            _table.Border = 0;
            _table.Cellpadding = 1f;
            _table.Cellspacing = 0f;
            _table.SetWidths(new int[] { 12,12,12,12,12,12,12,16 });

            _titlefont = new Font(bf, 8f);
            _titlefont.SetStyle(Font.BOLD | Font.UNDERLINE);
            _border = 0;
            _borderWidth = 0f;
            _grayFill = 1f;
            _table.AddCell(GetTitle(GetTranslation("ProvisionsRisksExpensesSection"), 6));
            _table.AddCell(GetSpacer(8));

            _border = 1;
            _borderWidth = 0.2f;
            _grayFill = 0.9f;
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ProbableCost"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ProbableCostEnd"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ExemptProvisionRisksExpenses"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("ExemptProvisionRisksExpensesEnd"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("IncreaseExemptProvisionRisksExpenses"), 1), "LBT"));
            _table.AddCell(SetBorderCell(GetFieldName(GetTranslation("JustificationExemptProvision"), 1), "F"));

            _grayFill = 1f;

            refs = _xfidc.XbrlData.XPathSelectElements("//tax-inc:ProbableCost", _namespaceManager).Select(e => e.Attribute("contextRef").Value).Distinct().ToList();
            refs = refs.Select(r => r.Replace("D__", "").Replace("I-Start__","").Replace("I-End__","")).ToList();

            foreach (string rid in refs)
            {
                WriteVoorzLine(rid, ref _table);
            }

            chapter.Add(_table);

            _document.Add(chapter);
        }


        private void WriteVoorzLine(string rid, ref Table table)
        {
            List<XElement> lineElements = _xfidc.XbrlData.XPathSelectElements("//tax-inc:*[contains(@contextRef,'" + rid + "')]", _namespaceManager).ToList();

            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "ProbableCost" && x.Attribute("contextRef").Value.Contains("I-Start"))), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "ProbableCost" && x.Attribute("contextRef").Value.Contains("I-End"))), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "ExemptProvisionRisksExpenses" && x.Attribute("contextRef").Value.Contains("I-Start"))), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "ExemptProvisionRisksExpenses" && x.Attribute("contextRef").Value.Contains("I-End"))), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "IncreaseExemptProvisionRisksExpenses")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValueString(1, lineElements.FirstOrDefault(x => x.Name.LocalName == "JustificationExemptProvision")), "RLB"));
        }

        private void WriteDubDebLine(string rid, ref Table table)
        {
            List<XElement> lineElements=  _xfidc.XbrlData.XPathSelectElements("//tax-inc:*[contains(@contextRef,'" + rid + "')]",_namespaceManager).ToList();

            
            
            table.AddCell(SetBorderCell(GetFieldValueString(1, lineElements.FirstOrDefault(x => x.Name.LocalName == "IdentityTradeDebtor")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValueString(1, lineElements.FirstOrDefault(x => x.Name.LocalName == "AddressTradeDebtor")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "DebtClaim" && x.Attribute("contextRef").Value.Contains("I-Start"))), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "DebtClaim" && x.Attribute("contextRef").Value.Contains("I-End"))), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "ExemptWriteDownDebtClaim" && x.Attribute("contextRef").Value.Contains("I-Start"))), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "ExemptWriteDownDebtClaim" && x.Attribute("contextRef").Value.Contains("I-End"))), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValue(lineElements.FirstOrDefault(x => x.Name.LocalName == "IncreaseExemptWriteDownDebtClaim")), "LB"));
            table.AddCell(SetBorderCell(GetFieldValueString(1, lineElements.FirstOrDefault(x => x.Name.LocalName == "JustificationExemptWriteDown")), "RLB"));
        }

        private Cell SetBorderCell(Cell cell, string dir)
        {
            switch (dir)
            {
                case "LB":
                    cell.BorderWidthBottom = _borderWidth;
                    cell.BorderWidthLeft = _borderWidth;
                    cell.BorderWidthRight = 0;
                    cell.BorderWidthTop = 0;
                    break;
                case "LT":
                    cell.BorderWidthTop= _borderWidth;
                    cell.BorderWidthLeft = _borderWidth;
                    cell.BorderWidthRight = 0;
                    cell.BorderWidthBottom  = 0;
                    break;
                case "RB":
                    cell.BorderWidthBottom = _borderWidth;
                    cell.BorderWidthRight= _borderWidth;
                    cell.BorderWidthLeft  = 0;
                    cell.BorderWidthTop = 0;
                    break;
                case "RT":
                    cell.BorderWidthTop = _borderWidth;
                    cell.BorderWidthRight= _borderWidth;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthBottom = 0;
                    break;
                case "RLB":
                    cell.BorderWidthTop = 0;
                    cell.BorderWidthRight = _borderWidth;
                    cell.BorderWidthLeft = _borderWidth;
                    cell.BorderWidthBottom = _borderWidth;
                    break;
                case "RLT":
                    cell.BorderWidthBottom = 0;
                    cell.BorderWidthRight = _borderWidth;
                    cell.BorderWidthLeft = _borderWidth;
                    cell.BorderWidthTop = _borderWidth;
                    break;
                case "RBT":
                    cell.BorderWidthBottom = _borderWidth;
                    cell.BorderWidthRight = _borderWidth;
                    cell.BorderWidthLeft = 0;
                    cell.BorderWidthTop = _borderWidth;
                    break;
                case "LBT":
                    cell.BorderWidthBottom = _borderWidth;
                    cell.BorderWidthRight = 0 ;
                    cell.BorderWidthLeft = _borderWidth;
                    cell.BorderWidthTop = _borderWidth;
                    break;
                case "F":
                    cell.BorderWidthTop = _borderWidth;
                    cell.BorderWidthRight = _borderWidth;
                    cell.BorderWidthLeft = _borderWidth;
                    cell.BorderWidthBottom = _borderWidth;
                    break;
            }
            return cell;
            
        }

        public string GetTranslation(XElement node)
        {
            return GetTranslation(node.Name.ToString());
        }

        public string GetTranslation(string node)
        {
            // TO REVIEW
            //XbrlAttachmentNodeDataContract xandc = _nodes.FirstOrDefault(n => n.Node.ToUpper() == node.ToUpper());
            //if (xandc != null)
            //{
            //    return xandc.Transaltion;
            //}
            return node + "(NO TRANSALATION)";
        }

    }
    */
}
