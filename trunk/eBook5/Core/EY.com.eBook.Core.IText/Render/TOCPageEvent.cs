﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace EY.com.eBook.Core.IText.Render
{
    public class TOCPageEvent : PdfPageEventHelper
    {
        private int page = 0;
        public Dictionary<Guid, int> ItemPages = new Dictionary<Guid, int>();

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            page++;
           

        }
        public override void OnGenericTag(PdfWriter writer, iTextSharp.text.Document document, iTextSharp.text.Rectangle rect, string text)
        {
            if (!ItemPages.ContainsKey(new Guid(text)))
            {

                ItemPages.Add(new Guid(text), page);
            }
        }
    }
}
