﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using EY.com.eBook.API.Contracts.Data;
using System.IO;
using System.Configuration;
using System.Globalization;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.IText;

namespace EY.com.eBook.Core.IText.LeadSheets
{
    public class Renderer 
    {
        private Document doc;
        private PdfWriter wri;
        public string Render(LeadSheetGenerationDataContract generationCfg)
        {
            //ClientDataContract cdc, FileBaseDataContract fbdc
           
            List<XElement> sets = generationCfg.Ranges.Root.Elements("set").ToList();
            doc = new Document();
            FontFactory.Register(generationCfg.FontLocation.Path, generationCfg.FontLocation.Alias);

            string fName = string.Format("{0}.pdf",generationCfg.GenerationId.ToString());
            string fPath = Path.Combine(generationCfg.Settings.GenerationPath, fName);
            wri = PdfWriter.GetInstance(doc, new FileStream(fPath, FileMode.Create));
           
            doc.SetMargins(8f, 0f, 60f, 60f);
            doc.Open();
            
            List<Chapter> chapters = RenderChapter(sets,generationCfg);
            foreach (Chapter chapter in chapters)
            {
                doc.Add(chapter);
            }
            
            doc.Close();
            wri.Close();

            doc = null;
            wri = null;

            if (!string.IsNullOrEmpty(generationCfg.Settings.ResultPath))
            {
                string copyTo = Path.Combine(generationCfg.Settings.ResultPath, fName);
                System.IO.File.Copy(fPath, copyTo);
                fPath = copyTo;
            }

            return fPath;
            
        }

        public List<Chapter> RenderChapter(List<XElement> sets, LeadSheetGenerationDataContract generationCfg)
        {
            
            CultureInfo ci = CultureInfo.CreateSpecificCulture(generationCfg.File.Culture);
            //cb.Ellipse(100f, 200f, 400f, 300f);
            //cb.Stroke();
           // sets = sets.Take(5).ToList();
            BaseFont bf = BaseFont.CreateFont(generationCfg.FontLocation.Path, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(bf, 7f);
            Font fontBold = new Font(bf, 7f);
            Font fontItalic = new Font(bf, 7f);
            fontBold.SetStyle(Font.BOLD);
            fontItalic.SetStyle(Font.ITALIC);
            //sets = sets.Take(1).ToList();
            //595,842 = A4
            string title;

            List<Chapter> chapters = new List<Chapter>();
            int i = 1;
            foreach (XElement set in sets)
            {
                List<LeadSheetLineDataContract> setAccounts = new List<LeadSheetLineDataContract>();
                foreach (XElement range in set.Elements("range"))
                {
                    //Ticket #59
                    setAccounts.AddRange(generationCfg.Lines.Where(a => a.InternalNr.StartsWith(range.Value) && !a.InternalNr.Contains(".")).ToList());
                }
                setAccounts = setAccounts.OrderBy(a => a.InternalNr).ToList();
                if (setAccounts.Count > 0)
                {
                    PdfTemplate template = PdfTemplate.CreateTemplate(wri, 200, 240);
                    template.Ellipse(10, 10, 195, 235);
                    template.Stroke();
                    template.BeginText();
                    template.SetFontAndSize(bf, 24);
                    template.SetTextMatrix(65, 180);
                    template.ShowText("w/p ref");

                    template.SetFontAndSize(fontBold.BaseFont, 30);
                    template.ShowTextAligned(PdfTemplate.ALIGN_CENTER, set.Attribute("desc").Value + " lead", 100, 100, 0);
                    template.EndText();


                    Image img = Image.GetInstance(template);


                    // XElement set = sets[0];
                    Chapter ls = new Chapter(set.Attribute("desc").Value, i);
                    ls.BookmarkTitle = set.Attribute("desc").Value;
                    ls.Title = new Paragraph(set.Attribute("desc").Value);
                   
                    
                    PdfPTable ptable = new PdfPTable(7);
                    
                    ptable.WidthPercentage = 85f;
                    ptable.SetTotalWidth(new float[] { 45f, 113f, 60f, 85f, 85f, 85f, 85f });
                    

                    PdfPTable nest1 = new PdfPTable(6);
                    nest1.SetTotalWidth(new float[] { 65f, 103f, 60f, 85f, 85f, 85f });
                    PdfPTable nest2 = new PdfPTable(2);
                    nest2.SetTotalWidth(new float[] { 28f ,57f });
                    ptable.DefaultCell.BorderWidth = 0;
                    nest1.DefaultCell.BorderWidth = 0;
                    nest1.DefaultCell.PaddingTop = 10;
                    nest1.DefaultCell.PaddingBottom = 5;
                    nest1.DefaultCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    nest2.DefaultCell.BorderWidth = 0;

                    // ptable.TotalWidth=PageSize.A4.Width - 10;
                    // nest1.TotalWidth = PageSize.A4.Width - 210;
                    // nest2.TotalWidth = 200;
                    // nest2.


                    ptable.DeleteBodyRows();

                  

                    // row 0
                    PdfPCell currCell = new PdfPCell(new Phrase("Client:", font));
                    currCell.BorderWidth = 0;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(generationCfg.Client.Name, fontBold));
                    currCell.Colspan = 2;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthBottom = 0.3f;
                    currCell.BorderColorBottom = Color.LIGHT_GRAY;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(" ", font));
                    currCell.BorderWidth = 0;
                    currCell.Colspan = 3;
                    nest1.AddCell(currCell);

                    //row 1
                    currCell = new PdfPCell(new Phrase("eBook File:", font));
                    currCell.BorderWidth = 0;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(generationCfg.File.Name, fontItalic));
                    currCell.Colspan = 2;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthBottom = 0.3f;
                    currCell.BorderColorBottom = Color.LIGHT_GRAY;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(" ", font));
                    currCell.BorderWidth = 0;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase("Initials: ", font));
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthBottom = 0.3f;
                    currCell.BorderColorBottom = Color.WHITE;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase("Created: " + DateTime.Now.ToString("dd/MM/yyyy", ci.DateTimeFormat), font));
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthBottom = 0.3f;
                    currCell.BorderColorBottom = Color.WHITE;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(" "));
                    //currCell.RowSpan = 3;
                    currCell.BorderWidth = 0;
                    nest2.AddCell(currCell);

                    currCell = new PdfPCell(img, true);
                    //currCell.RowSpan = 3;
                    currCell.BorderWidth = 0;
                    nest2.AddCell(currCell);

                    // row 2
                    currCell = new PdfPCell(new Phrase("Current Period:", font));
                    currCell.BorderWidth = 0;
                    currCell.MinimumHeight = 18f;
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    nest1.AddCell(currCell);
                    string period = string.Format("{0} - {1}", generationCfg.File.StartDate.ToString("dd/MM/yyyy", ci.DateTimeFormat)
                                                             , generationCfg.File.EndDate.ToString("dd/MM/yyyy", ci.DateTimeFormat));
                    currCell = new PdfPCell(new Phrase(period, font));
                    currCell.Colspan = 2;
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthBottom = 0.3f;
                    currCell.BorderColorBottom = Color.LIGHT_GRAY;
                    nest1.AddCell(currCell);
                    currCell = new PdfPCell(new Phrase(" Prepared by:", font));
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    currCell.PaddingLeft = 5;
                    nest1.AddCell(currCell);
                    currCell = new PdfPCell(new Phrase(" ", font));
                    currCell.Colspan = 2;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthBottom = 0.3f;
                    currCell.BorderColorBottom = Color.LIGHT_GRAY;
                    nest1.AddCell(currCell);
                    //currCell = new PdfPCell(new Phrase(" ", font));
                    //currCell.BorderWidth = 0;
                    //ptable.AddCell(currCell);

                    // row 3
                    currCell = new PdfPCell(new Phrase("Subject:", font));
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    currCell.MinimumHeight = 18f;
                    nest1.AddCell(currCell);
                    currCell = new PdfPCell(new Phrase(set.Attribute("title").Value.ToUpper(), font));
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthBottom = 0.3f;
                    currCell.BorderColorBottom = Color.LIGHT_GRAY;
                    currCell.Colspan = 2;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(" Reviewed by:", font));
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    currCell.PaddingLeft = 5;
                    nest1.AddCell(currCell);
                    currCell = new PdfPCell(new Phrase(" ", font));
                    currCell.Colspan = 2;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthBottom = 0.3f;
                    currCell.BorderColorBottom = Color.LIGHT_GRAY;
                    nest1.AddCell(currCell);
                    //currCell = new PdfPCell(new Phrase(" "));
                    //currCell.BorderWidth = 0;
                    //ptable.AddCell(currCell);

                    // empty row 
                    currCell = new PdfPCell(new Phrase(" "));
                    currCell.Colspan = 6;
                    currCell.BorderWidth = 0;
                    nest1.AddCell(currCell);

                    currCell = new PdfPCell(nest1);
                    currCell.BorderWidth = 0;
                    currCell.Colspan = 6;
                    ptable.AddCell(currCell);
                    ptable.AddCell(nest2);



                    // empty row 
                    currCell = new PdfPCell(new Phrase(" "));
                    currCell.Colspan = 7;
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);

                    // row 3
                    currCell = new PdfPCell(new Phrase("Account Number", fontBold));
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);
                    currCell = new PdfPCell(new Phrase("Description", fontBold));
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);
                    currCell = new PdfPCell(new Phrase("", fontBold));
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);

                    title = generationCfg.File.EndDate.ToString("dd/MM/yyyy", ci.DateTimeFormat);
                    title = string.Format("Current Period\nbefore adj.\n{0}",title);
                    currCell = new PdfPCell(new Phrase(title, fontBold));
                    currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);

                    title = generationCfg.File.EndDate.ToString("dd/MM/yyyy", ci.DateTimeFormat);
                    title = string.Format("Adjustments\n{0}", title);
                    currCell = new PdfPCell(new Phrase(title, fontBold));
                    currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);

                    title = generationCfg.File.EndDate.ToString("dd/MM/yyyy", ci.DateTimeFormat);
                    title = string.Format("Current Period\nafter adj.\n{0}", title);
                    currCell = new PdfPCell(new Phrase(title, fontBold));
                    currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);

                    title = generationCfg.File.StartDate.AddDays(-1).ToString("dd/MM/yyyy", ci.DateTimeFormat);
                    title = string.Format("Last Period\n{0}", title);
                    currCell = new PdfPCell(new Phrase(title, fontBold));
                    currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);

                   

                    // empty row 
                    currCell = new PdfPCell(new Phrase(" "));
                    currCell.Colspan = 7;
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);

                    decimal afterCnt = 0;
                    decimal beforeCnt = 0;
                    decimal adjCnt = 0;
                    decimal previousCnt=0;

                    foreach (LeadSheetLineDataContract adc in setAccounts)
                    {
                        // DATA rows
                        currCell = new PdfPCell(new Phrase(adc.VisualNr, font));
                        currCell.BorderWidth = 0;
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        ptable.AddCell(currCell);
                        currCell = new PdfPCell(new Phrase(adc.Description, font));
                        currCell.Colspan = 2;
                        currCell.BorderWidth = 0;
                        ptable.AddCell(currCell);

                        /*
                        currCell = new PdfPCell(new Phrase(" ", font));
                        currCell.BorderWidth = 0;
                        ptable.AddCell(currCell);
                         * */

                        currCell = new PdfPCell(new Phrase(adc.StartBalance.ToString("#,0.00 €", ci.NumberFormat), font));
                        currCell.BorderWidth = 0;
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        ptable.AddCell(currCell);

                        currCell = new PdfPCell(new Phrase(adc.Adjustments.ToString("#,0.00 €", ci.NumberFormat), font));
                        currCell.BorderWidth = 0;
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        ptable.AddCell(currCell);

                        currCell = new PdfPCell(new Phrase(adc.EndBalance.ToString("#,0.00 €", ci.NumberFormat), font));
                        currCell.BorderWidth = 0;
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        ptable.AddCell(currCell);

                        currCell = new PdfPCell(new Phrase(adc.PreviousBalance.ToString("#,0.00 €", ci.NumberFormat), font));
                        currCell.BorderWidth = 0;
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                        ptable.AddCell(currCell);

                        beforeCnt += adc.StartBalance;
                        afterCnt += adc.EndBalance;
                        adjCnt += adc.Adjustments;
                        previousCnt += adc.PreviousBalance;
                    }

                    // empty row 
                    currCell = new PdfPCell(new Phrase(" "));
                    currCell.Colspan = 7;
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);

                    // TOTAL row 
                    currCell = new PdfPCell(new Phrase(" ", font));
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);
                    currCell = new PdfPCell(new Phrase("Total", font));
                    currCell.Colspan = 2;
                    currCell.BorderWidth = 0;
                    ptable.AddCell(currCell);
                    

                    currCell = new PdfPCell(new Phrase(beforeCnt.ToString("#,0.00 €", ci.NumberFormat), font));
                    currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    currCell.VerticalAlignment = PdfCell.ALIGN_MIDDLE;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthTop = 0.5f;
                    currCell.BorderWidthBottom = 1f;
                    currCell.PaddingBottom = 5f;
                    ptable.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(adjCnt.ToString("#,0.00 €", ci.NumberFormat), font));
                    currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    currCell.VerticalAlignment = PdfCell.ALIGN_MIDDLE;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthTop = 0.5f;
                    currCell.BorderWidthBottom = 1f;
                    currCell.PaddingBottom = 5f;
                    ptable.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(afterCnt.ToString("#,0.00 €", ci.NumberFormat), font));
                    currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    currCell.VerticalAlignment = PdfCell.ALIGN_MIDDLE;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthTop = 0.5f;
                    currCell.BorderWidthBottom = 1f;
                    currCell.PaddingBottom = 5f;
                    ptable.AddCell(currCell);

                    currCell = new PdfPCell(new Phrase(previousCnt.ToString("#,0.00 €", ci.NumberFormat), font));
                    currCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    currCell.VerticalAlignment = PdfCell.ALIGN_MIDDLE;
                    currCell.BorderWidth = 0;
                    currCell.BorderWidthTop = 0.5f;
                    currCell.BorderWidthBottom = 1f;
                    currCell.PaddingBottom = 5f;
                    ptable.AddCell(currCell);


                    // Apply file links
                    List<RepositoryListItemDataContract> links = generationCfg.LinkedFiles
                        .Where(f => setAccounts.Count(a => a.InternalNr == f.ConnectionAccount) > 0)
                        .OrderBy(f=>f.ConnectionAccount)
                        .ToList();

                    if (links.Count > 0)
                    {
                        currCell = new PdfPCell(new Phrase(" "));
                        currCell.Colspan = 7;
                        currCell.BorderWidth = 0;
                        ptable.AddCell(currCell);

                        currCell = new PdfPCell(new Phrase("Linked attachments", fontBold));
                        currCell.Colspan = 7;
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                        currCell.BorderWidth = 0;
                        ptable.AddCell(currCell);

                        currCell = new PdfPCell(new Phrase(" "));
                        currCell.Colspan = 7;
                        currCell.BorderWidth = 0;
                        ptable.AddCell(currCell);

                        currCell = new PdfPCell(new Phrase("Account", fontBold));
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                        currCell.BorderWidth = 0;
                        currCell.BorderWidthBottom = 0.3f;
                        currCell.BorderColorBottom = Color.LIGHT_GRAY;
                        ptable.AddCell(currCell);

                        currCell = new PdfPCell(new Phrase("Linked file", fontBold));
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                        currCell.BorderWidth = 0;
                        currCell.BorderWidthBottom = 0.3f;
                        currCell.BorderColorBottom = Color.LIGHT_GRAY;
                        ptable.AddCell(currCell);

                        currCell = new PdfPCell(new Phrase("Repository path", fontBold));
                        currCell.Colspan = 5;
                        currCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                        currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                        currCell.BorderWidth = 0;
                        currCell.BorderWidthBottom = 0.3f;
                        currCell.BorderColorBottom = Color.LIGHT_GRAY;
                        ptable.AddCell(currCell);


                        foreach (RepositoryListItemDataContract rli in links)
                        {
                            LeadSheetLineDataContract lldc = setAccounts.First(a => a.InternalNr == rli.ConnectionAccount);
                            currCell = new PdfPCell(new Phrase(lldc.VisualNr, font));
                            currCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                            currCell.BorderWidth = 0;
                            ptable.AddCell(currCell);

                            currCell = new PdfPCell(new Phrase(rli.Description, font));
                            currCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                            currCell.BorderWidth = 0;
                            ptable.AddCell(currCell);

                            currCell = new PdfPCell(new Phrase(rli.ItemTextualPath, font));
                            currCell.Colspan = 5;
                            currCell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            currCell.VerticalAlignment = PdfPCell.ALIGN_BOTTOM;
                            currCell.BorderWidth = 0;
                            ptable.AddCell(currCell);
                        }

                    }



                    ls.Add(ptable);

                    chapters.Add(ls);
                    i++;
                }
            }

            return chapters;
        }

    }
}
