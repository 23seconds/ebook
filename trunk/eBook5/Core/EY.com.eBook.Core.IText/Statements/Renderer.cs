﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data;
using iTextSharp.text.pdf;
using System.Globalization;
using System.Configuration;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.Core.IText.Statements
{
    public class Renderer 
    {
        /*
        private Table _table;
        private bool _detailed;
        private List<AccountDataContract> _accounts;
        private string _culture;
        private Font _font;
        private BaseFont bf;
        private CultureInfo _cultureInfo;
        
        public Chapter Render(XDocument data, bool detailed, List<AccountDataContract> accounts, string culture)
        {
            _accounts=accounts;
            _detailed = detailed;
            _culture = culture;
            _cultureInfo = CultureInfo.CreateSpecificCulture(culture);

            FontFactory.Register(Config.StandardFont, "EYInterstate-Light");
            bf = BaseFont.CreateFont(Config.StandardFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            _font = new Font(bf, 8f);


            Chapter chapter = new Chapter(0);
            _table = new Table(9);
            _table.TableFitsPage = true;
            _table.Convert2pdfptable = true;
            _table.CellsFitPage = true;
            _table.Width = 100f;
            _table.Alignment = Element.ALIGN_CENTER;
            _table.Border = 0;
            _table.Cellpadding = 1f;
            _table.Cellspacing = 0f;
            _table.SetWidths(new int[] { 1, 1, 1, 1, 37, 18, 18, 18, 18 });

            RenderItem(data.Root,0,false);
            chapter.Add(_table);

            return chapter;
        }

        private Cell GetDummyCell()
        {
            Cell dummy = new Cell(" ");
            dummy.Border = 0;
            dummy.BorderWidth = 0;
            return dummy;
        }
        

        private void RenderItem(XElement node, int level,bool inverse)
        {
            Cell dummyCell=new Cell();
            Cell textCell=new Cell();
            Phrase textPhrase=new Phrase();
            Cell numberCell=new Cell();
            Phrase numberPhrase = new Phrase();
            decimal saldo = GetSaldi(node);

            // checks whether this section contains accounts with saldi different from 0
            bool hasValidSubAccounts = HasValidAccounts(node);

            if (level > 0 && saldo == 0 && !hasValidSubAccounts) return;

            XAttribute typeAttrib = node.Attributes("type").FirstOrDefault();
            if (typeAttrib != null)
            {
                inverse = typeAttrib.Value.ToLower() == "credit" || typeAttrib.Value.ToLower() == "range";
            }
            if(inverse) saldo = 0-saldo;

            switch (level)
            {
                case 0:
                    break;
                case 1:
                    textCell = new Cell() ;
                    textCell.Colspan=7;
                    textCell.VerticalAlignment= Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    Font tFont1 = new Font(bf, 10);
                    tFont1.SetStyle(Font.BOLD | Font.UNDERLINE);
                    textPhrase = new Phrase(GetTranslation(node).ToUpper(),tFont1);
                    textPhrase.Leading = 2f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);
                    _table.AddCell(textCell);

                    numberCell = new Cell();
                    numberCell.Colspan = 2;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    Font nFont1 = new Font(bf, 8f);
                    nFont1.SetStyle(Font.BOLD | Font.UNDERLINE);
                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), nFont1);
                    numberPhrase.Leading = 2f * textPhrase.Font.Size;
                    numberCell.Leading = numberPhrase.Leading;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);

                    break;
                case 2:
                    dummyCell = GetDummyCell();

                    _table.AddCell(dummyCell);


                    textCell = new Cell();
                    textCell.Colspan=6;
                    textCell.VerticalAlignment= Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    Font tFont2 = new Font(bf, 10);
                    tFont2.SetStyle(Font.UNDERLINE);
                    textPhrase = new Phrase(GetTranslation(node).ToUpper(),tFont2);
                    textPhrase.Font= _font;
                    textPhrase.Font.SetStyle(Font.UNDERLINE);
                    textPhrase.Leading = 2f *textPhrase.Font.Size;
                    textCell.Add(textPhrase);
                    textCell.Leading = textPhrase.Leading;
                    _table.AddCell(textCell);

                    numberCell = new Cell();
                    numberCell.Colspan = 2;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    Font nFont2 = new Font(bf, 8);
                    nFont2.SetStyle(Font.UNDERLINE);
                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€",_cultureInfo.NumberFormat),nFont2);
                    numberPhrase.Leading = 2f *textPhrase.Font.Size;
                    numberCell.Leading = numberPhrase.Leading;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);
                    break;
                case 3:
                    dummyCell = GetDummyCell();
                    dummyCell.Colspan=2;
                    _table.AddCell(dummyCell);

                    textCell = new Cell();
                    textCell.Colspan=4;
                    textCell.VerticalAlignment= Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    textPhrase = new Phrase(GetTranslation(node), new Font(bf, 10));
                    textPhrase.Font= _font;
                    textPhrase.Leading = 1.3f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);

                    _table.AddCell(textCell);

                    numberCell = new Cell();
                    numberCell.Colspan = 2;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), new Font(bf, 8));
                    numberPhrase.Leading = 1.3f * textPhrase.Font.Size;
                    numberCell.Add(numberPhrase);
                    numberCell.Leading = numberPhrase.Leading;
                    _table.AddCell(numberCell);

                    dummyCell = new Cell(" ");
                    dummyCell.Border = 0;
                    dummyCell.BorderWidth = 0;

                    _table.AddCell(dummyCell);
                    break;
                case 4:
                    dummyCell = GetDummyCell();
                    dummyCell.Colspan=3;
                    _table.AddCell(dummyCell);

                    textCell = new Cell();
                    textCell.Colspan=2;
                    textCell.VerticalAlignment= Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;
                    textPhrase = new Phrase(GetTranslation(node), new Font(bf, 10));
                    textPhrase.Leading = 1.1f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);

                    _table.AddCell(textCell);

                    numberCell = new Cell();
                    numberCell.Colspan = 2;
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), new Font(bf, 8));
                    numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                    numberCell.Add(numberPhrase);
                    numberCell.Leading = numberPhrase.Leading;
                    _table.AddCell(numberCell);

                    dummyCell = GetDummyCell();
                    dummyCell.Colspan=2;
                    _table.AddCell(dummyCell);
                    break;
                case 5:
                    dummyCell = GetDummyCell();
                    dummyCell.Colspan=4;
                    _table.AddCell(dummyCell);

                    textCell = new Cell();
                    textCell.VerticalAlignment= Element.ALIGN_TOP;
                    textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    textCell.Border = 0;
                    textCell.BorderWidth = 0;

                    textPhrase = new Phrase(GetTranslation(node), new Font(bf, 9f));
                    textPhrase.Leading = 1.1f * textPhrase.Font.Size;
                    textCell.Leading = textPhrase.Leading;
                    textCell.Add(textPhrase);

                    _table.AddCell(textCell);

                    numberCell = new Cell();
                    numberCell.VerticalAlignment = Element.ALIGN_TOP;
                    numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    numberCell.Border = 0;
                    numberCell.BorderWidth = 0;

                    numberPhrase = new Phrase(saldo.ToString("#,##0.00€",_cultureInfo.NumberFormat),new Font(bf, 8));
                    numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                    numberCell.Leading = numberPhrase.Leading;
                    numberCell.Add(numberPhrase);
                    _table.AddCell(numberCell);

                    dummyCell = GetDummyCell();
                    dummyCell.Colspan=3;
                    _table.AddCell(dummyCell);
                    break;
            }

            if (node.Elements("AccountSection").Count() > 0)
            {
                int subLevel = level + 1;
                foreach (XElement childSection in node.Elements("AccountSection"))
                {
                    RenderItem(childSection, subLevel,inverse);
                }
            }
            else
            {
                if (_detailed && level > 1)
                {
                    foreach (AccountDataContract adc in GetAccounts(node))
                    {
                        // render extra detail node.
                        dummyCell = GetDummyCell();
                        dummyCell.Colspan = 4;
                        _table.AddCell(dummyCell);

                        textCell = new Cell();
                        textCell.Colspan = 2;
                        textCell.VerticalAlignment = Element.ALIGN_TOP;
                        textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        textCell.Border = 0;
                        textCell.BorderWidth = 0;
                        

                        textPhrase = new Phrase(adc.DefaultDescription,new Font(bf,8f));
                        textPhrase.Leading = 1.1f * 10f;
                        textCell.Leading = textPhrase.Leading;
                        textCell.Add(textPhrase);

                        _table.AddCell(textCell);

                        numberCell = new Cell();
                        numberCell.Colspan = 1;
                        numberCell.VerticalAlignment = Element.ALIGN_TOP;
                        numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        numberCell.Border = 0;
                        numberCell.BorderWidth = 0;

                        decimal aSaldo = adc.TotalAmount;
                        if (inverse) aSaldo = 0 - aSaldo;
                        numberPhrase = new Phrase(aSaldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), new Font(bf, 8f));
                        numberPhrase.Leading = 1.1f * textPhrase.Font.Size;
                        numberCell.Leading = numberPhrase.Leading;
                        numberCell.Add(numberPhrase);
                        _table.AddCell(numberCell);

                        dummyCell = GetDummyCell();
                        dummyCell.Colspan = 2;
                        _table.AddCell(dummyCell);
                    }
                }
            }
            if (level == 0 && node.Attributes("hide").FirstOrDefault()==null)
            {
                textCell = new Cell();
                textCell.Colspan = 7;
                textCell.VerticalAlignment = Element.ALIGN_TOP;
                textCell.HorizontalAlignment = Element.ALIGN_LEFT;
                textCell.Border = 0;
                textCell.BorderWidth = 0;


                Font tFont0 = new Font(bf,10f);
                tFont0.SetStyle(Font.BOLD | Font.UNDERLINE);

                textPhrase = new Phrase(GetTranslation(node).ToUpper(), tFont0);
                textPhrase.Leading = 1.4f * textPhrase.Font.Size;
                textCell.Add(textPhrase);
                _table.AddCell(textCell);

                numberCell = new Cell();
                numberCell.Colspan = 2;
                numberCell.VerticalAlignment = Element.ALIGN_TOP;
                numberCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                numberCell.Border = 0;
                numberCell.BorderWidth = 0;

                tFont0 = new Font(bf, 9f);
                tFont0.SetStyle(Font.BOLD | Font.UNDERLINE);
                numberPhrase = new Phrase(saldo.ToString("#,##0.00€", _cultureInfo.NumberFormat), tFont0);
                numberPhrase.Font.SetStyle(Font.BOLD | Font.UNDERLINE);
                numberPhrase.Leading = 1.4f * textPhrase.Font.Size;
                numberCell.Add(numberPhrase);
                _table.AddCell(numberCell);

            }

        }

        private string GetTranslation(XElement node)
        {
            XElement el = node.Element("Translations").Elements("Translation").SingleOrDefault(x => x.Attribute("culture").Value == _culture);
            if (el == null) return string.Empty;
            return el.Value;
        }

        private decimal GetSaldi(XElement node)
        {
            decimal saldi = 0;
            // traverse regions
            foreach (XElement rangeNode in node.Element("AccountRegions").Elements("AccountRegion"))
            {
                
                string range = rangeNode.Attribute("range").Value;
                if(rangeNode.Attributes("exclude").Count()>0) {
                    string exclude = rangeNode.Attribute("exclude").Value;
                    saldi += _accounts.Where(a => a.InternalNr.StartsWith(range) && !a.InternalNr.StartsWith(exclude)).Sum(a => a.TotalAmount);
                } else {
                    saldi += _accounts.Where(a => a.InternalNr.StartsWith(range)).Sum(a => a.TotalAmount);
                }
            }
            return saldi;
        }

        private bool HasValidAccounts(XElement node)
        {
            
            // traverse regions
            foreach (XElement rangeNode in node.Element("AccountRegions").Elements("AccountRegion"))
            {
                
                string range = rangeNode.Attribute("range").Value;
                if (rangeNode.Attributes("exclude").Count() > 0)
                {
                    string exclude = rangeNode.Attribute("exclude").Value;
                    if (_accounts.Where(a => a.InternalNr.StartsWith(range)
                                            && !a.InternalNr.StartsWith(exclude)
                                            && a.TotalAmount != 0).Count() > 0)
                    {
                        return true;
                    }
                    
                }
                else
                {
                    if (_accounts.Where(a => a.InternalNr.StartsWith(range)
                                            && a.TotalAmount != 0).Count() > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private List<AccountDataContract> GetAccounts(XElement node)
        {
            List<AccountDataContract> list = new List<AccountDataContract>();
            
            // traverse regions
            foreach (XElement rangeNode in node.Element("AccountRegions").Elements("AccountRegion"))
            {
                List<AccountDataContract> accountsInRange = new List<AccountDataContract>();
                string range = rangeNode.Attribute("range").Value;
                if (rangeNode.Attributes("exclude").Count() > 0)
                {
                    string exclude = rangeNode.Attribute("exclude").Value;
                    accountsInRange = _accounts.Where(a => a.InternalNr.StartsWith(range) && !a.InternalNr.StartsWith(exclude)).ToList();
                }
                else
                {
                    accountsInRange = _accounts.Where(a => a.InternalNr.StartsWith(range)).ToList();
                }
                foreach (AccountDataContract adc in accountsInRange)
                {
                    if (!list.Contains(adc) && adc.TotalAmount!=0) list.Add(adc);
                }
            }
            return list.OrderBy(a=>a.InternalNr).ToList();
        }



        #region IDisposable Members

        public void Dispose()
        {
            _table = null;
            _accounts = null;
            _font = null;
            bf = null;
            _cultureInfo = null;

            System.GC.SuppressFinalize(this);
        }

        #endregion
         */
    }
}
