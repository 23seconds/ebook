﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.Core
{
    public struct DateRange
    {

        private DateTime _start;
        private DateTime _end;

        public DateRange(DateTime start, DateTime end)
        {
            _start = start;
            _end = end;
        }

        public DateTime Start
        {
            get
            {
                return _start;
            }
            set { _start = value; }
        }

        public DateTime End
        {
            get
            {
                return _end;
            }
            set { _end = value; }
        }

    }
}
