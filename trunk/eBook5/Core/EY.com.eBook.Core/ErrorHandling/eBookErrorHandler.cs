﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Dispatcher;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Runtime.Serialization;
using System.Net;
using System.Runtime.Serialization.Json;
using EY.com.eBook.Core.Data;
using System.Diagnostics;
using System.Configuration;

namespace EY.com.eBook.Core.ErrorHandling
{
    public class eBookErrorHandler : IErrorHandler
    {
        private string source = "eBook";

        public eBookErrorHandler(string name)
        {
            source = name;
        }

        public bool HandleError(Exception error)
        {
            //Tell the system that we handle all errors here. 
            return true;
        }

        public void ProvideFault(Exception error, System.ServiceModel.Channels.MessageVersion version, ref System.ServiceModel.Channels.Message fault)
        {
            
            
            //Trace.TraceError(error.Message);
            if (error is FaultException<string>)
            {
                error = new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = ((FaultException<string>)error).Detail });
            }
            if (error is FaultException<int>)
            {
                // is a managed client exception
                FaultException<int> fe = (FaultException<int>)error;

                //Detail for the returned value 
                int faultCode = fe.Detail;
                string cause = fe.Message;
                //The json serializable object 
                DefaultFaultContract msErrObject = new DefaultFaultContract { Message = cause, ErrorCode = faultCode };

                //The fault to be returned 
                fault = Message.CreateMessage(version, "", msErrObject, new DataContractJsonSerializer(msErrObject.GetType()));

                // tell WCF to use JSON encoding rather than default XML 
                WebBodyFormatMessageProperty wbf = new WebBodyFormatMessageProperty(WebContentFormat.Json);

                // Add the formatter to the fault 
                fault.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);

                //Modify response 
                HttpResponseMessageProperty rmp = new HttpResponseMessageProperty();

                // return custom error code, 400. 
                rmp.StatusCode = System.Net.HttpStatusCode.BadRequest;
                rmp.StatusDescription = "Bad request";

                //Mark the jsonerror and json content 
                rmp.Headers[HttpResponseHeader.ContentType] = "application/json";
                rmp.Headers["jsonerror"] = "true";

                //Add to fault 
                fault.Properties.Add(HttpResponseMessageProperty.Name, rmp);
            }
            if (error is FaultException<DefaultFaultContract>)
            {
                // is a managed client exception
                FaultException<DefaultFaultContract> fe = (FaultException<DefaultFaultContract>)error;

                //Detail for the returned value 
                DefaultFaultContract faultCode = fe.Detail;
                string cause = faultCode.Message;


                //The fault to be returned 
                fault = Message.CreateMessage(version, "", faultCode, new DataContractJsonSerializer(faultCode.GetType()));

                // tell WCF to use JSON encoding rather than default XML 
                WebBodyFormatMessageProperty wbf = new WebBodyFormatMessageProperty(WebContentFormat.Json);

                // Add the formatter to the fault 
                fault.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);

                //Modify response 
                HttpResponseMessageProperty rmp = new HttpResponseMessageProperty();

                // return custom error code, 400. 
                rmp.StatusCode = System.Net.HttpStatusCode.BadRequest;
                rmp.StatusDescription = "Bad request";

                //Mark the jsonerror and json content 
                rmp.Headers[HttpResponseHeader.ContentType] = "application/json";
                rmp.Headers["jsonerror"] = "true";

                //Add to fault 
                fault.Properties.Add(HttpResponseMessageProperty.Name, rmp);
            }
            else
            {

                // handle exception type when service is unavailable.



                // is a unmanaged exception
                //Arbitraty error 
                DefaultFaultContract msErrObject = new DefaultFaultContract { Message = error.Message, ErrorCode = -1 };
                if (error.InnerException != null)
                {
                    msErrObject.Message = error.InnerException.Message;
                }
                msErrObject.StackTrace = error.StackTrace;


                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["eBook.EventLog.Enabled"]) && ConfigurationManager.AppSettings["eBook.EventLog.Enabled"].ToLower() == "true")
                {

                    //if (!EventLog.SourceExists(source)) EventLog.CreateEventSource(source, "eBook");
                    string msg = String.Format("{0}\nSOURCE:{1} \nStacktrace:\n{2}", msErrObject.Message,source, msErrObject.StackTrace);
                    System.Diagnostics.EventLog.WriteEntry("eBook", msg, EventLogEntryType.Error);
                }
                
                // create a fault message containing our FaultContract object 
                fault = Message.CreateMessage(version, "", msErrObject, new DataContractJsonSerializer(msErrObject.GetType()));

                // tell WCF to use JSON encoding rather than default XML 
                var wbf = new WebBodyFormatMessageProperty(WebContentFormat.Json);
                fault.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);

                //Modify response 
                var rmp = new HttpResponseMessageProperty();

                rmp.Headers[HttpResponseHeader.ContentType] = "application/json";
                rmp.Headers["jsonerror"] = "true";

                //Internal server error with exception mesasage as status. 
                rmp.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                rmp.StatusDescription = error.Message;

                fault.Properties.Add(HttpResponseMessageProperty.Name, rmp);
            }
        }

    }
}