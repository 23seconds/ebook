﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Configuration;

namespace EY.com.eBook.Core.ErrorHandling
{

    public class eBookErrorBehaviorElement : BehaviorExtensionElement
    {

        public override Type BehaviorType
        {

            get { return typeof(AddErrorHandlerBehavior); }

        }

        protected override object CreateBehavior()
        {

            return new AddErrorHandlerBehavior();

        }

    }
}
