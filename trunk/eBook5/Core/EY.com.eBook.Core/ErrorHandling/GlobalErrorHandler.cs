﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Configuration;
using EY.com.eBook.Core.Data;
using System.Configuration;
using System.Diagnostics;

namespace EY.com.eBook.Core.ErrorHandling
{
    public class GlobalErrorHandler : GlobalErrorHandlerBehaviorExtensionElement, IErrorHandler, IServiceBehavior
    {
        #region IErrorHandler Members

        public bool HandleError(Exception error)
        {
            // log the exception to the event log, and send it to our logging infrastructure

            return true;  // error has been handled.
        }

        public void ProvideFault(Exception error, System.ServiceModel.Channels.MessageVersion version, ref System.ServiceModel.Channels.Message fault)
        {
            if (error is FaultException<DefaultFaultContract>)
            {
                //handled error.

            } else if (!(error is FaultException))
            {
                
                MessageFault msgFault = null;

                DefaultFaultContract faultDetail = new DefaultFaultContract
                {
                    Detail = error.StackTrace
                    ,
                    ErrorCode = -1
                    ,
                    Message = error.Message
                    ,
                    StackTrace = error.StackTrace
                };
                if (error.InnerException != null)
                {
                    faultDetail.Message += "\nINNER EXCEPTION:" + error.InnerException.Message;
                }

                FaultException<DefaultFaultContract> fex = new FaultException<DefaultFaultContract>(faultDetail, error.Message);
                
                msgFault = fex.CreateMessageFault();

                fault = Message.CreateMessage(version, msgFault, "");

                if (!string.IsNullOrEmpty(this.EventLog))
                {

                    //if (!EventLog.SourceExists(source)) EventLog.CreateEventSource(source, "eBook");
                    string msg = String.Format("{0}\nSOURCE:{1} \nStacktrace:\n{2}", faultDetail.Message, this.EventLogSource, faultDetail.StackTrace);
                    System.Diagnostics.EventLog.WriteEntry(this.EventLog, msg, EventLogEntryType.Error);
                }
            }


        }

        #endregion

        #region IServiceBehavior Members

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            return;
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher channDisp in serviceHostBase.ChannelDispatchers)
            {
                channDisp.ErrorHandlers.Add(this);
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (var svcEndpoint in serviceDescription.Endpoints)
            {
                if (svcEndpoint.Contract.Name != "IMetadataExchange")
                {
                    foreach (var opDesc in svcEndpoint.Contract.Operations)
                    {
                        if (opDesc.Faults.Count == 0)
                        {
                            string msg =
                                string.Format("GlobalErrorHandler requires a FaultContract(typeof(DefaultFaultContract)) on each operation contract. The {0} contains no FaultContracts.", opDesc.Name);
                            throw new InvalidOperationException(msg);
                        }

                        var fcExists = from fc in opDesc.Faults
                                       where fc.DetailType == typeof (DefaultFaultContract)
                                       select fc;

                        if (fcExists.Count() == 0)
                        {
                            string msg = string.Format("GlobalErrorHandler requires a FaultContract(typeof(DefaultFaultContract)) on each operation contract.");
                            throw new InvalidOperationException(msg);
                        }
                    }
                }
            }
        }

        #endregion
    }

    public class GlobalErrorHandlerBehaviorExtensionElement : BehaviorExtensionElement
    {
       

        [ConfigurationProperty("eventlogSource", DefaultValue = "", IsRequired = false)]
        public string EventLogSource
        {
            get { return (string)base["eventlogSource"]; }
            set { base["eventlogSource"] = value; }
        }

        [ConfigurationProperty("eventlog", DefaultValue = "", IsRequired = false)]
        public string EventLog
        {
            get { return (string)base["eventlog"]; }
            set { base["eventlog"] = value; }
        }


        public override Type BehaviorType
        {
            get { return typeof(GlobalErrorHandler); }
        }

        protected override object CreateBehavior()
        {
            GlobalErrorHandler g = new GlobalErrorHandler();
            g.EventLog = EventLog;
            g.EventLogSource = EventLogSource;
            return g;
        }

        private ConfigurationPropertyCollection _properties; 

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                if (_properties == null)
                {
                    _properties = new ConfigurationPropertyCollection();
                    _properties.Add(new ConfigurationProperty(
                       "eventlog", typeof(string), "",
                       ConfigurationPropertyOptions.None
                       ));
                    _properties.Add(new ConfigurationProperty(
                       "eventlogSource", typeof(string), "",
                       ConfigurationPropertyOptions.None
                       ));
                }
                return _properties;
            }
        }

        public override void CopyFrom(ServiceModelExtensionElement from)
        {
            base.CopyFrom(from);
            GlobalErrorHandler element = (GlobalErrorHandler)from;
            EventLog = element.EventLog;
            EventLogSource = element.EventLogSource;
        }
        
    }
}
