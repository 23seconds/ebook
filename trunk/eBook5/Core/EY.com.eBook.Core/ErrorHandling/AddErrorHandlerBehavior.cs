﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace EY.com.eBook.Core.ErrorHandling
{
    public class AddErrorHandlerBehavior : WebHttpBehavior
    {
        protected override void AddServerErrorHandlers(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            base.AddServerErrorHandlers(endpoint, endpointDispatcher);
            //Remove all other error handlers 
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Clear();
            //Add our own 
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Add(new eBookErrorHandler(endpoint.Name));
        }
    }

    


}
