﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace EY.com.eBook.Core.ErrorHandling
{
    [DataContractFormat]
    public class JsonError
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public int FaultCode { get; set; }
    } 
}
