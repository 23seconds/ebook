﻿using System.Diagnostics;
using System;
using EY.com.eBook.Core.EntityTranslation;

namespace EY.com.eBook.Core
{
    /// <summary>
    /// RepositoryBase
    /// </summary>
    public abstract class RepositoryBase
    {
        /// <summary>
        /// Gets or sets the translator.
        /// </summary>
        /// <value>The translator.</value>
        public EntityTranslatorService Translator { get; set; }
        /// <summary>
        /// Gets or sets the translator by data contract.
        /// </summary>
        /// <value>The translator by data contract.</value>
        public EntityTranslatorServiceByDataContract TranslatorByDataContract { get; set; }
        /// <summary>
        /// Registers the translators.
        /// </summary>
        protected abstract void RegisterTranslators();


        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase"/> class.
        /// </summary>
        [DebuggerStepThrough()]
        public RepositoryBase()
        {
            Translator = new EntityTranslatorService();
            TranslatorByDataContract = new EntityTranslatorServiceByDataContract();
            RegisterTranslators();
        }
    }
}
