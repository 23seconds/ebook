﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Configuration;

namespace EY.com.eBook.Core.WCF
{

    

    /// <summary>
    /// The baseclass for all proxy classes
    /// </summary>
    /// <typeparam name="TChannel">The type of the channel.</typeparam>
    public class ProxyBase<TChannel> : ClientBase<TChannel>, IProxyBase where TChannel : class
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyBase&lt;TChannel&gt;"/> class and loads the configuration for its endpoints.
        /// </summary>
        /// <param name="endpointConfiguration">The endpoint configuration in the config file of the caller.</param>
        public ProxyBase(string endpointConfiguration)
            : base(endpointConfiguration)
        {
           // Authenticate(ServiceCredential.Username, ServiceCredential.Password);
        }
        public ProxyBase()
        {
        }


        /// <summary>
        /// Authenticates the specified username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        public void Authenticate(string username, string password)
        {
            ClientCredentials.UserName.UserName = username;
            ClientCredentials.UserName.Password = password;
        }


        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            try
            {
                if (State == CommunicationState.Opened)
                    Close();
                else if (State == CommunicationState.Faulted)
                    Abort();
            }
            catch (Exception)
            {
                Abort();
                throw;
            }
        }

        #endregion

        /// <summary>
        /// Gets or sets the name of the endpoint configuration.
        /// </summary>
        /// <value>The name of the endpoint configuration.</value>
        public virtual string EndpointConfigurationName { get; set; }



    }
}
