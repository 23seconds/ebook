﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace EY.com.eBook.Core.WCF
{
    /// <summary>
    /// IProxyBase
    /// </summary>
    public interface IProxyBase : IDisposable
    {
        /// <summary>
        /// Opens this instance.
        /// </summary>
        void Open();
        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <value>The state.</value>
        CommunicationState State { get; }
        /// <summary>
        /// Aborts this instance.
        /// </summary>
        void Abort();
        /// <summary>
        /// Closes this instance.
        /// </summary>
        void Close();
    }
}
