﻿using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace EY.com.eBook.Core.WCF
{
    /// <summary>
    /// The Model serializer which uses the DataContractSerializer to perform advanced serialization and deserialization.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public class ModelSerializer<TModel>
    {

        DataContractSerializer _serializer;


        /// <summary>
        /// Deserializes the specified XML.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns></returns>
        public virtual TModel Deserialize(string xml)
        {
            TModel result;

            using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
            {
                result = (TModel)Serializer.ReadObject(reader);
            }

            return result;
        }

        /// <summary>
        /// Serializes the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public virtual string Serialize(TModel model)
        {
            StringBuilder sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(sb))
            {
                Serializer.WriteObject(writer, model);

                writer.Flush();
            }

            return sb.ToString();
        }

        /// <summary>
        /// Gets the serializer.
        /// </summary>
        /// <value>The serializer.</value>
        private DataContractSerializer Serializer
        {
            get
            {
                if (_serializer == null)
                    _serializer = new DataContractSerializer(typeof(TModel));
                return _serializer;
            }
        }
    }
}
