﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace EY.com.eBook.Core.WCF
{
    /// <summary>
    /// Class to provide a WCf Service username and password that is stored in the configuration file.
    /// If the same ServiceCredential is used more than once while it has not been disposed it will store the 
    /// username and password in a local variable thus preventing the need to read from the config file everytime.
    /// </summary>
    public static class ServiceCredential
    {
        static ServiceCredential() { }

        private static string _user;
        private static string _pwd;

        /// <summary>
        /// Gets a value indicating the username that was configured by setting the ServiceUser value in the appSettings.
        /// </summary>
        public static string Username
        {
            get
            {
                if (string.IsNullOrEmpty(_user))
                {
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServiceUser"]))
                    {
                        _user = ConfigurationManager.AppSettings["ServiceUser"];
                    }
                }

                return _user;
            }
        }

        /// <summary>
        /// Gets a value indicating the password that was configured by setting the ServicePwd value in the appSettings.
        /// </summary>
        public static string Password
        {
            get
            {
                if (string.IsNullOrEmpty(_pwd))
                {
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServicePwd"]))
                    {
                        _pwd = ConfigurationManager.AppSettings["ServicePwd"];
                    }
                }

                return _pwd;
            }
        }

    }
}
