﻿using System;
using System.Runtime.Serialization;

namespace EY.com.eBook.Core.WCF
{
    /// <summary>
    /// The audit information data contract
    /// </summary>
    [DataContract]
    public class AuditInfoDataContract
    {
        /// <summary>
        /// Gets or sets the created by date.
        /// </summary>
        /// <value>The created by.</value>
        [DataMember]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the last updated by date.
        /// </summary>
        /// <value>The last updated by.</value>
        [DataMember]
        public string LastUpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the created on date.
        /// </summary>
        /// <value>The created on.</value>
        [DataMember]
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the last updated on date.
        /// </summary>
        /// <value>The last updated on.</value>
        [DataMember]
        public DateTime? LastUpdatedOn { get; set; }
    }
}
