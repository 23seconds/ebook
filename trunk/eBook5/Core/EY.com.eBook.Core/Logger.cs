﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using log4net;

namespace EY.com.eBook.Core
{
    public class Logger
    {
        private static readonly bool LogEnabled = false;
        private static readonly short Category;
        private static readonly EventLog EventLogWriter;

        private static readonly string EventSource = "eBook.Caching";
        private static readonly string EventLogName = "eBook.Caching";

        private static readonly ILog _logger;

        static Logger()
        {
            if (!(!string.IsNullOrEmpty(ConfigurationManager.AppSettings["eBook.EventLog.Enabled"]) &&
                          ConfigurationManager.AppSettings["eBook.EventLog.Enabled"].ToLower() == "true")) return;

            var configuredCategoy = ConfigurationManager.AppSettings["eBook.EventLog.Category"];
            Category = string.IsNullOrEmpty(configuredCategoy) ? (short)0 : Convert.ToInt16(configuredCategoy);

            var configuredSource = ConfigurationManager.AppSettings["eBook.EventLog.Source"];
            if (!string.IsNullOrEmpty(configuredSource)) EventSource = configuredSource;

            _logger = LogManager.GetLogger("eBook." + EventSource + "." + Category);

            if (!EventLog.SourceExists(EventSource))
            {
                EventLog.CreateEventSource(EventSource, EventLogName);
            }
            else
            {
                EventLogName = EventLog.LogNameFromSourceName(EventSource, ".");
            }

            EventLogWriter = new EventLog(EventLogName, ".", EventSource);

            LogEnabled = true;
        }

        private static void LogMessage(EventLogEntryType type, string message, object[] arguments)
        {
            EventLogWriter.WriteEntry(String.Format(message, arguments), type, Math.Abs(message.GetHashCode() % 65535), Category);
        }

        public static void Error(Exception ex)
        {
            Error("{0} was unhandled by user code\n\tMessage = {1}\n\tSource={2}\n\tStackTrace:\n{3}",
                  new object[] { ex.GetType(), ex.Message, ex.Source, ex.StackTrace });
            if (ex.InnerException != null)
                Error(ex.InnerException);
        }

        public static void Error(string message, params object[] arguments)
        {
            Console.Error.WriteLine(message, arguments);
            if (_logger != null)
                _logger.ErrorFormat(message, arguments);
            if (!LogEnabled) return;
            LogMessage(EventLogEntryType.Error, message, arguments);
        }

        public static void Warning(string message, params object[] arguments)
        {
            if (_logger != null)
                _logger.WarnFormat(message, arguments);
            if (!LogEnabled) return;
            LogMessage(EventLogEntryType.Warning, message, arguments);
        }

        public static void Info(string message, params object[] arguments)
        {
            if (_logger != null)
                _logger.InfoFormat(message, arguments);
            if (!LogEnabled) return;
            LogMessage(EventLogEntryType.Information, message, arguments);
        }

        public static void Debug(string message, params object[] arguments)
        {
            if (_logger != null)
                _logger.DebugFormat(message, arguments);
        }
    }
}
