﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Net.Mail;
using System.ServiceModel;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.Core
{
    /// <summary>
    /// DecodedBase64
    /// </summary>
    public static class CoreHelper
    {
        public static Guid DEFAULT_CURRENCY_ID = new Guid("07cfc4ca-1eed-4ed9-a003-6dd89be6cbab");
        public const int retriesLeft = 10;
      

        /// <summary>
        /// Encodes to base64.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string EncodeToBase64(string value)
        {

            return Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(value));
        }
        

        /// <summary>
        /// Decodeds from base64.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string DecodeFromBase64(string value)
        {
            
            return UTF8Encoding.UTF8.GetString(Convert.FromBase64String(value));
        }


        public static T DeepCopy<T>(T obj)
        {
            return ProtoBuf.Serializer.DeepClone<T>(obj);
        }


        public static T DeepConeDC<T>(T obj)
        {
            string t = SerializeToString(obj);
            return DeserializeFromString<T>(t);
        }

        public static T DeserializeFromString<T>(string input)
        {
            T result = default(T);
            DataContractSerializer dcs = new DataContractSerializer(typeof(T));
            var sr = new System.IO.StringReader(input);
            var xr = new XmlTextReader(sr);
            result = (T)dcs.ReadObject(xr);
            xr.Close();
            sr.Close();
            return result;
        }

        public static string SerializeToStringProto(object instance)
        {
            
            string result = null;
            MemoryStream ms = new MemoryStream();            
            ProtoBuf.Serializer.Serialize( ms, instance);

            result = System.Convert.ToBase64String(ms.ToArray());
            
            return result;
        }

        public static string SerializeToString(object instance)
        {
            string result = null;
            DataContractSerializer dcs = new DataContractSerializer(instance.GetType());
            var sw = new System.IO.StringWriter();
            var xw = new XmlTextWriter(sw);
            dcs.WriteObject(xw,instance);
            xw.Close();
            result = sw.ToString();
            sw.Close();
            return result;
        }

        public static XElement SerializeToXElement(object instance, bool removeNs)
        {
            string result = null;
            DataContractSerializer dcs = new DataContractSerializer(instance.GetType());
            var sw = new System.IO.StringWriter();
            var xw = new XmlTextWriter(sw);
            dcs.WriteObject(xw, instance);
            xw.Close();
            result = sw.ToString();
            sw.Close();
            XElement xe = XElement.Parse(result);
            if (removeNs) xe = xe.StripNamespaces();
            return xe;
        }

        public static string SerializeToString(object instance, XmlWriterSettings settings)
        {
           // string result = null;
            DataContractSerializer dcs = new DataContractSerializer(instance.GetType());
           // var sw = new System.IO.StringWriter();
            StringBuilder sb = new StringBuilder();

            var xw =  XmlWriter.Create(sb,settings);
            dcs.WriteObject(xw, instance);
            xw.Close();
            return sb.ToString();
        }
        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            return Regex.Replace(name, invalidReStr, "_");
        }

        public static T ToEnum<T>(string value)
        {
            return (T)System.Enum.Parse(typeof(T), value);
        }

        public static void SendMailGTH(string[] to, string[] cc, string body, string subject, bool html)
        {
            SendMail(System.Configuration.ConfigurationManager.AppSettings["Mailing.GTHFrom"], to, cc, body, subject, html, null);
        }

        public static void SendMail(string from, string[] to, string[] cc, string body, string subject, bool html, string filePath)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["Mailing.Enabled"] != "true") return;
            
            if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Mailing.StandardTo"]))
            {
                //if the key standardTo exists, utilize this instead values in to (used on test environments)
                to = System.Configuration.ConfigurationManager.AppSettings["Mailing.StandardTo"].Split(new Char[] { ';' });
                //same goes for cc
                cc = System.Configuration.ConfigurationManager.AppSettings["Mailing.StandardTo"].Split(new Char[] { ';' });
            }
            

            try
            {
                SmtpClient client = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings.Get("Mailing.Server"));

                if (string.IsNullOrEmpty(from))
                {
                    from = System.Configuration.ConfigurationManager.AppSettings["Mailing.StandardFrom"];
                }

                MailMessage message = new MailMessage();
                message.From = new MailAddress(from);
               
                if (to == null || to.Length == 0)
                {
                    throw new Exception("MAIL TO NOT PROVIDED  - SUBJECT:{" + subject + "}");
                }

                foreach (string t in to)
                {
                    try
                    {
                        message.To.Add(new MailAddress(t.Trim()));
                    }
                    catch (Exception e)
                    {
                        //Log("MAILING", "TO ADDRESS FAIL " + t + " " + e.Message, true);
                    }
                }

                if (cc != null)
                {
                    if (cc.Length > 0)
                    {
                        foreach (string c in cc)
                        {
                            if (!string.IsNullOrEmpty(c))
                            {
                                try
                                {
                                    message.CC.Add(new MailAddress(c.Trim()));
                                }
                                catch (Exception e)
                                {
                                    //Log("MAILING", "TO ADDRESS FAIL " + c + " " + e.Message, true);
                                }
                            }
                        }
                    }
                }

                if (!String.IsNullOrEmpty(filePath))
                {
                    if (!System.IO.Directory.Exists(filePath))
                    {
                        throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Could not find the e-mail attachment at " + filePath + "; Transaction aborted." });
                    }
                    //System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType(); version to old I reckon, to be investigated
                    //contentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Octet;
                    //contentType.Name = "test.docx";
                    message.Attachments.Add(new Attachment(filePath));
                }

                message.Subject = subject;
                message.IsBodyHtml = html;
                message.Body = body;

                //Send
                try
                {
                    client.Send(message);
                }
                catch (Exception erro)
                {
                    Console.WriteLine(erro.Message);
                }
            }
            catch (Exception e)
            {
                //Log("MAILING", e.Message, true);
                throw new Exception("MAIL NOT SEND  - Exception:{" + e.Message + "}");
            }

        }

        public static void resendMail(MailMessage message,int retriesLeft)
        {
            if (retriesLeft > 1)
            {
                try
                {
                    SmtpClient client = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings.Get("Mailing.Server"));
                    client.Send(message);
                }
                catch (Exception e)
                {
                    resendMail(message, retriesLeft);
                    throw new Exception("MAIL RETRY NOT SEND  - Exception:{" + e.Message + "}");
                }
            }
        }
    }


    public static class Extensions
    {
        public static string GetValueOrDefault(this string txt)
        {
            if (string.IsNullOrEmpty(txt)) return string.Empty;
            return txt;
        }

        public static string GetValueOrDefault(this string txt, string defaultValue)
        {
            if (string.IsNullOrEmpty(txt)) return defaultValue;
            return txt;
        }

        public static string ToIso8601DateOnly(this DateTime dte)
        {
            return dte.ToString("yyyy-MM-dd");
        }

        public static string ToBelgianEnterprise(this string entNr)
        {
            if (string.IsNullOrEmpty(entNr)) return "";
            entNr = entNr.Replace(".", "").Replace("BE", "");
            while (entNr.Length < 10)
            {
                entNr = "0" + entNr;
            }
            return "BE" + entNr;
        }

        public static DateTime ToDateOnly(this DateTime dte)
        {
            return new DateTime(dte.Year, dte.Month, dte.Day);
        }

        public static int IncludedDaysDiff(this DateTime startDate, DateTime endDate)
        {
            DateTime start = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            DateTime end = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59).AddSeconds(1);

            return Math.Abs((end - start).Days);
        }
        public static int MonthsBetween(this DateTime startDate, DateTime endDate)
        {
            DateTime st = startDate.Day==1 ? startDate.AddDays(1) : startDate;
            DateTime ed = endDate.Day < 2 ? new DateTime(endDate.Year,endDate.Month,2) : endDate;
            if (st == ed || st > ed) return 0;
            TimeSpan startTs = st - (new DateTime(1, 1, 1));
            DateTime diff = ed.Subtract(startTs).AddDays(-1);
            return diff.Month + ((diff.Year - 1) * 12);
        }

        public static int FullMonthsBetween(this DateTime startDate, DateTime endDate)
        {
            if(startDate > endDate) {
                DateTime tmp = startDate;
                startDate = endDate;
                endDate = tmp;
            }
            int sday = startDate.Day;
            int eday = endDate.Day;
            int months = 0;
            DateTime calcStart = new DateTime(startDate.Year,startDate.Month,1);
            DateTime calcEnd = new DateTime(endDate.Year,endDate.Month,1);
            while (calcStart.Month != calcEnd.Month || calcStart.Year != calcEnd.Year)
            {
                months++;
                //calcStart.AddDays(DateTime.DaysInMonth(calcStart.Year,calcStart.Month));
                calcStart=calcStart.AddMonths(1);
            }
            return months;
            
        }

        public static bool IsGuid(this string candidate)
        {
            if (candidate != null)
            {
                Regex guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");

                return guidRegEx.IsMatch(candidate);
            }
            return false;
        }

        public static string ToPath(this Guid candidate)
        {
            return candidate.ToPath(@"\");
        }

        public static string ToPath(this Guid candidate,string seperator)
        {
            string sid = candidate.ToString().Replace("-", "");
            string path = "";
            int i = 0;
            foreach (char c in sid.ToCharArray())
            {
                if (i % 2 == 0 && i != 0) path += seperator;
                path += c;
                i++;
            }
            return path;
        }

        public static string Utf8ToLatin1(this string source)
        {
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(source);
            byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
            return iso.GetString(isoBytes);
        }

        public static Guid DeterministicGuid(this string source)
        {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider(); 
            byte[] inputBytes = Encoding.Default.GetBytes(source);
            byte[] hashBytes = provider.ComputeHash(inputBytes);
            Guid hashGuid = new Guid(hashBytes);

            return hashGuid;
        }

        public static XElement StripNamespaces(this XElement root)
        {
            XElement res = new XElement(
                root.Name.LocalName,
                root.HasElements ?
                    root.Elements().Select(el => el.StripNamespaces()) :
                    (object)root.Value
            );

            res.ReplaceAttributes(
                root.Attributes().Where(attr => (!attr.IsNamespaceDeclaration)));

            return res;
        }

        
    }  
}
