﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Globalization;

namespace EY.com.eBook.Core
{
    public class XsltDistinctComparer : IEqualityComparer<XPathNavigator>
    {

        public XsltDistinctComparer(string xpathToCompare)
        {
            _xpathToCompare = xpathToCompare;
        }

        public XsltDistinctComparer(string xpathToCompare, bool ignoreCase)
        {
            _xpathToCompare = xpathToCompare;
            _ignoreCase = ignoreCase;
        }

        public XsltDistinctComparer(string xpathToCompare, bool ignoreCase, bool numeric)
        {
            _xpathToCompare = xpathToCompare;
            _ignoreCase = ignoreCase;
            _numeric = numeric;
        }

        private bool _numeric = false;
        private bool _ignoreCase = false;
        private string _xpathToCompare;

        private string GetValueToCompare(XPathNavigator x)
        {
            XPathNavigator v = x.SelectSingleNode(_xpathToCompare);
            if (v == null) return string.Empty;
            return v.Value;
        }

        #region IEqualityComparer<XPathNavigator> Members

        public bool Equals(XPathNavigator x, XPathNavigator y)
        {
            if (_ignoreCase) return GetValueToCompare(x).ToLower().Equals(GetValueToCompare(y).ToLower(), StringComparison.OrdinalIgnoreCase);
            if (_numeric)
            {
                string valx = GetValueToCompare(x);
                string valy = GetValueToCompare(y);

                decimal valdx = 0;
                decimal valdy = 0;
                decimal.TryParse(valx, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out valdx);
                decimal.TryParse(valy, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out valdy);
                return valdx == valdy;
            }
            return GetValueToCompare(x).Equals(GetValueToCompare(y),StringComparison.Ordinal);
        }

        public int GetHashCode(XPathNavigator obj)
        {
            if (_ignoreCase) return GetValueToCompare(obj).ToLower().GetHashCode();
            if (_numeric)
            {
                string val = GetValueToCompare(obj);

                decimal vald = 0;
      
                decimal.TryParse(val, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out vald);
                return vald.GetHashCode();
            }
            return GetValueToCompare(obj).GetHashCode();
        }

        #endregion
    }
   

}
