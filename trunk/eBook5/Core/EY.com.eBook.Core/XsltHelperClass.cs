﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Globalization;

namespace EY.com.eBook.Core
{
    public class XsltHelperClass
    {

        private Dictionary<string, decimal> _sums = new Dictionary<string, decimal>();
        public CultureInfo Culture { get; set; }

        public XsltHelperClass() { }

        public string GetFontSize(int filledColumns)
        {
            string fontSize = "10";

            if (filledColumns > 6 && filledColumns <= 8)
            {
                fontSize = "8";
            }
            if (filledColumns > 8)
            {
                fontSize = "6";
            }

            return fontSize;
        }

        public string GetColumnsSizesX(int maxPercent, int columns, int spaceCells, decimal maxSize) // -1=before, 0=none, 1=after
        {
            List<int> cols = new List<int>();
            decimal colSize = maxSize * columns;
            
            decimal spacing = 1;
            decimal remaining  = maxPercent - (columns * Math.Abs(spaceCells));
            if (colSize < remaining && spaceCells != 0)
            {
                spacing += Math.Floor(((remaining - colSize) / columns)*100) / 100;
                remaining = maxPercent - (columns * spacing);
            }
            if (spaceCells == 0) spacing = 0;

            colSize = Math.Floor((remaining / columns) * 100) / 100;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < columns; i++)
            {
                if (spaceCells == -1) sb.Append(spacing.ToString(CultureInfo.InvariantCulture.NumberFormat)).Append(";");
                sb.Append(colSize.ToString(CultureInfo.InvariantCulture.NumberFormat));
                if (spaceCells == 1) sb.Append(";").Append(spacing.ToString(CultureInfo.InvariantCulture.NumberFormat)).Append(";");
                if (i < columns - 1) sb.Append(";");
            }
            return sb.ToString();
        }

        public string GetColumnSizes(int total, int columns)
        {
            string spacing = "1;";
            if (columns <= 15)
            {
                spacing = "5;";
            }

            int realColumns = (columns) / 2;
            realColumns = realColumns + 1;
            decimal columnSize = total / realColumns;
            string sizes = "";
            for (int i = 0; i < columns; i++)
            {
                sizes += columnSize.ToString() + ";";
                if (i < columns - 1)
                {
                    sizes += spacing;
                }
                i = i + 1;
            }
            return sizes;
        }

        public string ConvertDate(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {

                DateTime dte;
                try
                {
                    dte = System.DateTime.Parse(input, Culture.DateTimeFormat);
                    return dte.ToString(Culture.DateTimeFormat.ShortDatePattern);
                }
                catch
                {
                    try
                    {
                        dte = System.DateTime.Parse(input, CultureInfo.InvariantCulture.DateTimeFormat);
                        return dte.ToString(Culture.DateTimeFormat.ShortDatePattern);
                    }
                    catch { return string.Empty; }
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public string ConvertDateLongFormat(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                try
                {
                    DateTime dte = System.DateTime.Parse(input, CultureInfo.InvariantCulture.DateTimeFormat);
                    return dte.ToString(Culture.DateTimeFormat.LongDatePattern);
                }
                catch { return string.Empty; }
            }
            else
            {
                return string.Empty;
            }
        }
        
        public decimal CheckNaN(string nr)
        {
            decimal nrd = 0;
            decimal.TryParse(nr, out nrd);
            return nrd;
        }

        public decimal CheckNaNAbsolute(string nr)
        {
            decimal nrd = 0;
            decimal.TryParse(nr, out nrd);
            return Math.Abs(nrd);
        }

        public decimal AddSum(string key, decimal value)
        {
            if (!_sums.ContainsKey(key)) _sums[key] = 0;
            _sums[key] = _sums[key] + value;
            return _sums[key];
        }

        public decimal ResetSum(string key)
        {
            if (!_sums.ContainsKey(key)) _sums[key] = 0;
            _sums[key] = 0;
            return 0;
        }

        //public XPathNavigator[] 


        // XPathNodeIterator
        public XPathNavigator[] Distinct(XPathNavigator[] list, string xpathToCompare)
        {

            XPathNavigator[] results = list.Distinct(new XsltDistinctComparer(xpathToCompare)).ToArray<XPathNavigator>();
            return results;
        }

        public XPathNavigator[] SortNumericValue(XPathNavigator[] list, string xpathToSortOn)
        {
            XPathNavigator[] results = list.OrderBy(x => x.SelectSingleNode(xpathToSortOn).ValueAsDouble).ToArray();
            return results;
        }

        public XPathNavigator[] Sort(XPathNavigator[] list, string xpathToSortOn)
        {
            XPathNavigator[] results = list.OrderBy(x => x.SelectSingleNode(xpathToSortOn).Value).ToArray();
            return results;
        }

        public XPathNavigator[] DistinctNumeric(XPathNavigator[] list, string xpathToCompare)
        {

            XPathNavigator[] results = list.Distinct(new XsltDistinctComparer(xpathToCompare,false,true)).ToArray<XPathNavigator>();
            return results;
        }

        public XPathNavigator[] DistinctCaseInsensitive(XPathNavigator[] list, string xpathToCompare)
        {

            XPathNavigator[] results = list.Distinct(new XsltDistinctComparer(xpathToCompare, true)).ToArray<XPathNavigator>();
            return results;
        }

        public XPathNavigator GetColumns(double columns)
        {
            
            XElement root = new XElement("root");
            for (int i = 1; i <= columns; i++)
            {
                root.Add(new XElement("Column", i));
            }
            return root.CreateNavigator();
        }

        public XPathNavigator Split(string input, string splitChar)
        {
            string[] items = input.Split(new string[] { splitChar },StringSplitOptions.RemoveEmptyEntries);
            XElement root = new XElement("root");
            for (int i = 0; i < items.Length; i++)
            {
                root.Add(new XElement("Item", items[i]));
            }
            return root.CreateNavigator();
        }

        public string Replace(string input,string find, string replacement)
        {
            return input.Replace(find, replacement);
        }

        public string SubStrRev(string input, int reverseIndex)
        {
            return input.Substring(0, input.Length - reverseIndex);
        }

        
    }
}
