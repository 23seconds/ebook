﻿using System;

namespace EY.com.eBook.Core.EntityTranslation
{

    /// <summary>
    /// Translate Level to define how many levels of relations that will be translated
    /// </summary>
    public struct TranslateLevel
    {

        /// <summary>
        /// TranslateLevel
        /// </summary>
        private enum TranslateLevelEnum
        {
            /// <summary>
            /// Infinite
            /// </summary>
            Infinite = 0,
            /// <summary>
            /// No Children
            /// </summary>
            NoChildren = 10,
            /// <summary>
            /// First Level
            /// </summary>
            FirstLevel = 11,
            /// <summary>
            /// Second Level
            /// </summary>
            SecondLevel = 12,
            /// <summary>
            /// Third Level
            /// </summary>
            ThirdLevel = 13,
            /// <summary>
            /// Fourth Level
            /// </summary>
            FourthLevel = 14,
            /// <summary>
            /// Fifth Level
            /// </summary>
            FifthLevel = 15,
            /// <summary>
            /// Sixth Level
            /// </summary>
            SixthLevel = 16,
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TranslateLevel"/> struct.
        /// </summary>
        /// <param name="value">The value.</param>
        private TranslateLevel(TranslateLevelEnum value)
            : this()
        {
            Value = value;
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        private TranslateLevelEnum Value { get; set; }

        /// <summary>
        /// Gets a value indicating whether [translate relations].
        /// </summary>
        /// <value><c>true</c> if [translate relations]; otherwise, <c>false</c>.</value>
        public bool TranslateRelations
        {
            get
            {
                return ( Value == TranslateLevelEnum.Infinite ) || ( Value > TranslateLevelEnum.NoChildren );
            }
        }

        /// <summary>
        /// Decreases the level.
        /// </summary>
        /// <returns></returns>
        public TranslateLevel DecreaseLevel()
        {
            //if (this.Value == TranslateLevelEnum.NoChildren)
            //    throw new ArgumentException("The value of the TranslateLevel cannot be NoChildren");
            //else if (this.Value == TranslateLevelEnum.Infinite)
            //    return this;
            //else
            //    return new TranslateLevel(this.Value - 1);

            switch (Value)
            {
                case TranslateLevelEnum.NoChildren:
                    throw new ArgumentException("The value of the TranslateLevel cannot be NoChildren");
                case TranslateLevelEnum.Infinite:
                    return this;
                default:
                    return new TranslateLevel(Value - 1);
            }
        }

        /// <summary>
        /// Gets the no children.
        /// </summary>
        /// <value>The no children.</value>
        public static TranslateLevel NoChildren { get { return new TranslateLevel(TranslateLevelEnum.NoChildren); } }
        /// <summary>
        /// Gets the first level.
        /// </summary>
        /// <value>The first level.</value>
        public static TranslateLevel FirstLevel { get { return new TranslateLevel(TranslateLevelEnum.FirstLevel); } }
        /// <summary>
        /// Gets the second level.
        /// </summary>
        /// <value>The second level.</value>
        public static TranslateLevel SecondLevel { get { return new TranslateLevel(TranslateLevelEnum.SecondLevel); } }
        /// <summary>
        /// Gets the third level.
        /// </summary>
        /// <value>The third level.</value>
        public static TranslateLevel ThirdLevel { get { return new TranslateLevel(TranslateLevelEnum.ThirdLevel); } }
        /// <summary>
        /// Gets the fourth level.
        /// </summary>
        /// <value>The fourth level.</value>
        public static TranslateLevel FourthLevel { get { return new TranslateLevel(TranslateLevelEnum.FourthLevel); } }
        /// <summary>
        /// Gets the fifth level.
        /// </summary>
        /// <value>The fifth level.</value>
        public static TranslateLevel FifthLevel { get { return new TranslateLevel(TranslateLevelEnum.FifthLevel); } }
        /// <summary>
        /// Gets the sixth level.
        /// </summary>
        /// <value>The sixth level.</value>
        public static TranslateLevel SixthLevel { get { return new TranslateLevel(TranslateLevelEnum.SixthLevel); } }
        /// <summary>
        /// Gets the infinite.
        /// </summary>
        /// <value>The infinite.</value>
        public static TranslateLevel Infinite { get { return new TranslateLevel(TranslateLevelEnum.Infinite); } }
    }
}
