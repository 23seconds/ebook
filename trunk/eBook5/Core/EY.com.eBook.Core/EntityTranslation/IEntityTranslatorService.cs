using System;
using System.Collections.Generic;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.Core.EntityTranslation
{
    /// <summary>
    /// IEntityTranslatorService
    /// </summary>
    public interface IEntityTranslatorService
    {
        /// <summary>
        /// Determines whether this instance can translate the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        bool CanTranslate(Type targetType, Type sourceType);
        /// <summary>
        /// Determines whether this instance can translate.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// 	<c>true</c> if this instance can translate; otherwise, <c>false</c>.
        /// </returns>
        bool CanTranslate<TTarget, TSource>();
        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        object Translate(Type targetType, object source);
        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        object Translate(Type targetType, object source,string culture);
        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        object Translate(Type targetType, object source, TranslateLevel levelsToTranslate);
        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        object Translate(Type targetType, object source, TranslateLevel levelsToTranslate,string culture);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TTarget Translate<TTarget>(object source);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TTarget Translate<TTarget>(object source,string culture);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        TTarget Translate<TTarget>(object source, TranslateLevel levelsToTranslate);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        TTarget Translate<TTarget>(object source, TranslateLevel levelsToTranslate,string culture);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSuperType">The type of the super type.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TTarget Translate<TTarget, TSuperType>(object source);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSuperType">The type of the super type.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TTarget Translate<TTarget, TSuperType>(object source,string culture);
        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <returns></returns>
        List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList);
        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <returns></returns>
        List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList,string culture);
        /// <summary>
        /// Registers the entity translator.
        /// </summary>
        /// <param name="translator">The translator.</param>
        void RegisterEntityTranslator(IEntityTranslator translator);
        /// <summary>
        /// Removes the entity translator.
        /// </summary>
        /// <param name="translator">The translator.</param>
        void RemoveEntityTranslator(IEntityTranslator translator);
    }


    /// <summary>
    /// IEntityTranslatorServiceByDataContract
    /// </summary>
    public interface IEntityTranslatorServiceByDataContract
    {
        /// <summary>
        /// Determines whether this instance can translate the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        bool CanTranslate(Type targetType, Type sourceType);
        /// <summary>
        /// Determines whether this instance can translate.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// 	<c>true</c> if this instance can translate; otherwise, <c>false</c>.
        /// </returns>
        bool CanTranslate<TTarget, TSource>();
        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        object Translate(Type targetType, object source);
        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        object Translate(Type targetType, object source, IDataContract dataContract);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TTarget Translate<TTarget>(object source);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        TTarget Translate<TTarget>(object source, IDataContract dataContract);
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSuperType">The type of the super type.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        TTarget Translate<TTarget, TSuperType>(object source);
        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <returns></returns>
        List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList);
        /// <summary>
        /// Registers the entity translator.
        /// </summary>
        /// <param name="translator">The translator.</param>
        void RegisterEntityTranslator(IEntityTranslatorByDataContract translator);
        /// <summary>
        /// Removes the entity translator.
        /// </summary>
        /// <param name="translator">The translator.</param>
        void RemoveEntityTranslator(IEntityTranslatorByDataContract translator);
    }
}
