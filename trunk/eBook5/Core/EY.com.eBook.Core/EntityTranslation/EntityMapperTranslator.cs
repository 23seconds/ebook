
using System;
using System.Collections.Generic;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.Core.EntityTranslation
{
    /// <summary>
    /// Entity mapper translator base class.
    /// </summary>
    /// <typeparam name="TBusinessEntity">The type of the business entity.</typeparam>
    /// <typeparam name="TServiceEntity">The type of the service entity.</typeparam>
    public abstract class EntityMapperTranslator<TBusinessEntity, TServiceEntity> : BaseTranslator
    {
        
        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanTranslate(Type targetType, Type sourceType)
        {
            return (targetType == typeof(TBusinessEntity) && sourceType == typeof(TServiceEntity)) ||
                (targetType == typeof(TServiceEntity) && sourceType == typeof(TBusinessEntity));
        }

        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels of child objects to translate.
        /// null =&gt; the translater gets all the levels possible.
        /// 0 =&gt; just translate itself and no children.
        /// 1 or more =&gt; Translate itself and 1 or more levels of childobjects</param>
        /// <returns></returns>
        public override object Translate(IEntityTranslatorService service, Type targetType, object source, TranslateLevel levelsToTranslate)
        {
            if (targetType == typeof(TBusinessEntity))
                return ServiceToBusiness(service, (TServiceEntity)source, levelsToTranslate);
            if (targetType == typeof(TServiceEntity))
                return BusinessToService(service, (TBusinessEntity)source, levelsToTranslate);

            throw new EntityTranslatorException();
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="busList">The bus list.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        public List<TServiceEntity> TranslateList(IEntityTranslatorService service, List<TBusinessEntity> busList, TranslateLevel levelsToTranslate)
        {
            List<TServiceEntity> servList = new List<TServiceEntity>();
            foreach (TBusinessEntity busEntity in busList)
            {
                servList.Add(this.Translate<TServiceEntity>(service, busEntity, levelsToTranslate));
            }
            return servList;
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="busList">The bus list.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        public List<TBusinessEntity> TranslateList(IEntityTranslatorService service, List<TServiceEntity> busList, TranslateLevel levelsToTranslate)
        {
            List<TBusinessEntity> servList = new List<TBusinessEntity>();
            foreach (TServiceEntity busEntity in busList)
            {
                servList.Add(this.Translate<TBusinessEntity>(service, busEntity, levelsToTranslate));
            }
            return servList;
        }

        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected abstract TServiceEntity BusinessToService(IEntityTranslatorService service, TBusinessEntity value, TranslateLevel levelsToTranslate);
        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected abstract TBusinessEntity ServiceToBusiness(IEntityTranslatorService service, TServiceEntity value, TranslateLevel levelsToTranslate);

    }

    /// <summary>
    /// Entity mapper translator by data contract base class.
    /// </summary>
    /// <typeparam name="TBusinessEntity">The type of the business entity.</typeparam>
    /// <typeparam name="TServiceEntity">The type of the service entity.</typeparam>
    public abstract class EntityMapperTranslatorByDataContract<TBusinessEntity, TServiceEntity> : BaseTranslatorByDataContract
    {
        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanTranslate(Type targetType, Type sourceType)
        {
            return (targetType == typeof(TBusinessEntity) && sourceType == typeof(TServiceEntity)) ||
                (targetType == typeof(TServiceEntity) && sourceType == typeof(TBusinessEntity));
        }

        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public override object Translate(IEntityTranslatorServiceByDataContract service, Type targetType, object source, IDataContract dataContract)
        {
            if (targetType == typeof(TBusinessEntity))
                return ServiceToBusiness(service, (TServiceEntity)source, dataContract);
            if (targetType == typeof(TServiceEntity))
                return BusinessToService(service, (TBusinessEntity)source, dataContract);

            throw new EntityTranslatorException();
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="busList">The bus list.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public List<TServiceEntity> TranslateList(IEntityTranslatorServiceByDataContract service, List<TBusinessEntity> busList, IDataContract dataContract)
        {
            List<TServiceEntity> servList = new List<TServiceEntity>();
            foreach (TBusinessEntity busEntity in busList)
            {
                servList.Add(this.Translate<TServiceEntity>(service, busEntity, dataContract));
            }
            return servList;
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="busList">The bus list.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public List<TBusinessEntity> TranslateList(IEntityTranslatorServiceByDataContract service, List<TServiceEntity> busList, IDataContract dataContract)
        {
            List<TBusinessEntity> servList = new List<TBusinessEntity>();
            foreach (TServiceEntity busEntity in busList)
            {
                servList.Add(this.Translate<TBusinessEntity>(service, busEntity, dataContract));
            }
            return servList;
        }

        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        protected abstract IDataContract BusinessToService(IEntityTranslatorServiceByDataContract service, TBusinessEntity value, IDataContract dataContract);
        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        protected abstract TBusinessEntity ServiceToBusiness(IEntityTranslatorServiceByDataContract service, TServiceEntity value, IDataContract dataContract);
    }
}
