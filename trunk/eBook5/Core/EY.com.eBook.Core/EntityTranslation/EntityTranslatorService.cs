using System;
using System.Collections.Generic;
using EY.com.eBook.Core.Data;
using System.Globalization;
using System.Linq;

namespace EY.com.eBook.Core.EntityTranslation
{

    /// <summary>
    /// EntityTranslatorService
    /// </summary>
    public class EntityTranslatorService : IEntityTranslatorService
    {
        /// <summary>
        /// translators
        /// </summary>
        private List<IEntityTranslator> _translators = new List<IEntityTranslator>();

        /// <summary>
        /// Registers the entity translator.
        /// </summary>
        /// <param name="translator">The translator.</param>
        public void RegisterEntityTranslator(IEntityTranslator translator)
        {
            if (translator == null)
                throw new ArgumentNullException("translator");

            _translators.Add(translator);
        }

        /// <summary>
        /// Removes the entity translator.
        /// </summary>
        /// <param name="translator">The translator.</param>
        public void RemoveEntityTranslator(IEntityTranslator translator)
        {
            if (translator == null)
                throw new ArgumentNullException("translator");

            _translators.Remove(translator);
        }

        public void RemoveEntityTranslatorByType(Type type)
        {
            if(type==null)
                throw new ArgumentNullException("type");
            List<IEntityTranslator> transToRemove = _translators.Where(t => t.GetType().Equals(type)).ToList();
            foreach (IEntityTranslator trans in transToRemove)
            {
                if (_translators.Contains(trans)) _translators.Remove(trans);
            }
        }

        /// <summary>
        /// Determines whether this instance can translate.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// 	<c>true</c> if this instance can translate; otherwise, <c>false</c>.
        /// </returns>
        public bool CanTranslate<TTarget, TSource>()
        {
            return CanTranslate(typeof(TTarget), typeof(TSource));
        }

        /// <summary>
        /// Determines whether this instance can translate the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        public bool CanTranslate(Type targetType, Type sourceType)
        {
            if (targetType == null)
                throw new ArgumentNullException("targetType");
            if (sourceType == null)
                throw new ArgumentNullException("sourceType");

            return IsArrayConversionPossible(targetType, sourceType) || FindTranslator(targetType, sourceType) != null;
        }

        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(object source)
        {
            return Translate<TTarget>(source, TranslateLevel.Infinite,CultureInfo.CurrentCulture.ToString());
        }
        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(object source, string culture)
        {
            return Translate<TTarget>(source, TranslateLevel.Infinite, culture);
        }

        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(object source, TranslateLevel levelsToTranslate)
        {
            return (TTarget)Translate(typeof(TTarget), source, levelsToTranslate, CultureInfo.CurrentCulture.ToString());
        }

        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(object source, TranslateLevel levelsToTranslate,string culture)
        {
            return (TTarget)Translate(typeof(TTarget), source, levelsToTranslate,culture);
        }

        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TExpectedType">The type of the expected type.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget, TExpectedType>(object source)
        {
            return (TTarget)Translate(typeof(TTarget), typeof(TExpectedType), source, CultureInfo.CurrentCulture.ToString());
        }

        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TExpectedType">The type of the expected type.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget, TExpectedType>(object source, string culture)
        {
            return (TTarget)Translate(typeof(TTarget), typeof(TExpectedType), source,culture);
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public object Translate(Type targetType, object source)
        {
            return Translate(targetType, source, TranslateLevel.Infinite);
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public object Translate(Type targetType, object source,string culture)
        {
            return Translate(targetType, source, TranslateLevel.Infinite,culture);
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="expectedType">Expected type.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public object Translate(Type targetType, Type expectedType, object source)
        {
            return Translate(targetType, expectedType, source, CultureInfo.CurrentCulture.ToString());
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="expectedType">Expected type.</param>
        /// <param name="source">The source.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public object Translate(Type targetType, Type expectedType, object source,string culture)
        {
            if (targetType == null)
                throw new ArgumentNullException("targetType");

            if (source == null)
            {
                if (targetType.IsArray)
                {
                    return null;
                }
                else
                {
                    throw new ArgumentNullException("source");
                }
            }

            if (IsArrayConversionPossible(targetType, expectedType))
            {
                return TranslateArray(targetType, source, TranslateLevel.Infinite,culture);
            }
            else
            {
                IEntityTranslator translator = FindTranslator(targetType, expectedType);
                if (translator != null)
                {
                    translator.Culture = culture;
                    return translator.Translate(this, targetType, source, TranslateLevel.Infinite);
                }
            }

            throw new EntityTranslatorException("No translator is available to perform the operation.");
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        public object Translate(Type targetType, object source, TranslateLevel levelsToTranslate)
        {
            return Translate(targetType, source, levelsToTranslate, CultureInfo.CurrentCulture.ToString());
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public object Translate(Type targetType, object source, TranslateLevel levelsToTranslate, string culture)
        {
            if (targetType == null)
                throw new ArgumentNullException("targetType");

            if (source == null)
            {
                if (targetType.IsArray)
                {
                    return null;
                }
                else
                {
                    throw new ArgumentNullException("source");
                }
            }

            Type sourceType = source.GetType();
            
            if (IsArrayConversionPossible(targetType, sourceType))
            {
                return TranslateArray(targetType, source, levelsToTranslate,culture);
            }
            else
            {
                IEntityTranslator translator = FindTranslator(targetType, sourceType);
                if (translator != null)
                {
                    translator.Culture = culture;
                    return translator.Translate(this, targetType, source, levelsToTranslate);
                }
            }

            throw new EntityTranslatorException("No translator is available to perform the operation.");
        }

        /// <summary>
        /// Translates the array.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        private object TranslateArray(Type targetType, object source, TranslateLevel levelsToTranslate)
        {
            return TranslateArray(targetType, source, levelsToTranslate, CultureInfo.CurrentCulture.ToString());
        }

        /// <summary>
        /// Translates the array.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        private object TranslateArray(Type targetType, object source, TranslateLevel levelsToTranslate,string culture)
        {
            Type targetItemType = targetType.GetElementType();
            var sourceArray = (Array)source;
            var result = (Array)Activator.CreateInstance(targetType, sourceArray.Length);
            for (int i = 0; i < sourceArray.Length; i++)
            {
                object value = sourceArray.GetValue(i);
                if (value != null)
                    result.SetValue(Translate(targetItemType, sourceArray.GetValue(i), levelsToTranslate,culture), i);
            }
            return result;
        }

        /// <summary>
        /// Determines whether [is array conversion possible] [the specified target type].
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if [is array conversion possible] [the specified target type]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsArrayConversionPossible(Type targetType, Type sourceType)
        {
            if (targetType.IsArray && targetType.GetArrayRank() == 1 && sourceType.IsArray && sourceType.GetArrayRank() == 1)
            {
                return CanTranslate(targetType.GetElementType(), sourceType.GetElementType());
            }
            return false;
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList)
        {
            return TranslateList<TTarget, TSource>(sourceList, TranslateLevel.Infinite, CultureInfo.CurrentCulture.ToString());
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList, TranslateLevel levelsToTranslate)
        {
            return TranslateList<TTarget, TSource>(sourceList, levelsToTranslate, CultureInfo.CurrentCulture.ToString());
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList,string culture)
        {
            return TranslateList<TTarget, TSource>(sourceList, TranslateLevel.Infinite,culture);
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <param name="culture">The culture.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList, TranslateLevel levelsToTranslate,string culture)
        {
            if (CanTranslate<TTarget, TSource>())
            {
                IEntityTranslator translator = FindTranslator<TTarget, TSource>();
                if (translator != null)
                {
                    translator.Culture = culture;
                    return translator.TranslateList<TTarget, TSource>(this, sourceList, levelsToTranslate);
                }
            }
            throw new EntityTranslatorException("No translator is available to perform the operation.");
        }

        /// <summary>
        /// Finds the translator.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns></returns>
        private IEntityTranslator FindTranslator<TTarget, TSource>()
        {
            return FindTranslator(typeof(TTarget), typeof(TSource));
        }

        /// <summary>
        /// Finds the translator.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns></returns>
        private IEntityTranslator FindTranslator(Type targetType, Type sourceType)
        {
            return _translators.Find(t => t.CanTranslate(targetType, sourceType));
        }        
    }

    /// <summary>
    /// EntityTranslatorServiceByDataContract
    /// </summary>
    public class EntityTranslatorServiceByDataContract : IEntityTranslatorServiceByDataContract
    {
        /// <summary>
        /// translators
        /// </summary>
        private List<IEntityTranslatorByDataContract> _translators = new List<IEntityTranslatorByDataContract>();

        /// <summary>
        /// Registers the entity translator.
        /// </summary>
        /// <param name="translator">The translator.</param>
        public void RegisterEntityTranslator(IEntityTranslatorByDataContract translator)
        {
            if (translator == null)
                throw new ArgumentNullException("translator");

            _translators.Add(translator);
        }

        /// <summary>
        /// Removes the entity translator.
        /// </summary>
        /// <param name="translator">The translator.</param>
        public void RemoveEntityTranslator(IEntityTranslatorByDataContract translator)
        {
            if (translator == null)
                throw new ArgumentNullException("translator");

            _translators.Remove(translator);
        }

        /// <summary>
        /// Determines whether this instance can translate.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// 	<c>true</c> if this instance can translate; otherwise, <c>false</c>.
        /// </returns>
        public bool CanTranslate<TTarget, TSource>()
        {
            return CanTranslate(typeof(TTarget), typeof(TSource));
        }

        /// <summary>
        /// Determines whether this instance can translate the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        public bool CanTranslate(Type targetType, Type sourceType)
        {
            if (targetType == null)
                throw new ArgumentNullException("targetType");
            if (sourceType == null)
                throw new ArgumentNullException("sourceType");

            return IsArrayConversionPossible(targetType, sourceType) || FindTranslator(targetType, sourceType) != null;
        }

        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(object source)
        {
            return Translate<TTarget>(source, new BaseDataContract());
        }

        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(object source, IDataContract dataContract)
        {
            return (TTarget)Translate(typeof(TTarget), source, dataContract);
        }

        /// <summary>
        /// Translates the specified source.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TExpectedType">The type of the expected type.</typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget, TExpectedType>(object source)
        {
            return (TTarget)Translate(typeof(TTarget), typeof(TExpectedType), source);
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public object Translate(Type targetType, object source)
        {
            return Translate(targetType, source, new BaseDataContract());
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="expectedType">Expected type.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public object Translate(Type targetType, Type expectedType, object source)
        {
            if (targetType == null)
                throw new ArgumentNullException("targetType");

            if (source == null)
            {
                if (targetType.IsArray)
                {
                    return null;
                }
                else
                {
                    throw new ArgumentNullException("source");
                }
            }

            if (IsArrayConversionPossible(targetType, expectedType))
            {
                return TranslateArray(targetType, source, new BaseDataContract());
            }
            else
            {
                IEntityTranslatorByDataContract translator = FindTranslator(targetType, expectedType);
                if (translator != null)
                {
                    return translator.Translate(this, targetType, source, new BaseDataContract());
                }
            }

            throw new EntityTranslatorException("No translator is available to perform the operation.");
        }

        /// <summary>
        /// Translates the specified target type.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public object Translate(Type targetType, object source, IDataContract dataContract)
        {
            if (targetType == null)
                throw new ArgumentNullException("targetType");

            if (source == null)
            {
                if (targetType.IsArray)
                {
                    return null;
                }
                else
                {
                    throw new ArgumentNullException("source");
                }
            }

            Type sourceType = source.GetType();

            if (IsArrayConversionPossible(targetType, sourceType))
            {
                return TranslateArray(targetType, source, dataContract);
            }
            else
            {
                IEntityTranslatorByDataContract translator = FindTranslator(targetType, sourceType);
                if (translator != null)
                {
                    return translator.Translate(this, targetType, source, dataContract);
                }
            }

            throw new EntityTranslatorException("No translator is available to perform the operation.");
        }

        /// <summary>
        /// Translates the array.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        private object TranslateArray(Type targetType, object source, IDataContract dataContract)
        {
            Type targetItemType = targetType.GetElementType();
            var sourceArray = (Array)source;
            var result = (Array)Activator.CreateInstance(targetType, sourceArray.Length);
            for (int i = 0; i < sourceArray.Length; i++)
            {
                object value = sourceArray.GetValue(i);
                if (value != null)
                    result.SetValue(Translate(targetItemType, sourceArray.GetValue(i), dataContract), i);
            }
            return result;
        }

        /// <summary>
        /// Determines whether [is array conversion possible] [the specified target type].
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if [is array conversion possible] [the specified target type]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsArrayConversionPossible(Type targetType, Type sourceType)
        {
            if (targetType.IsArray && targetType.GetArrayRank() == 1 && sourceType.IsArray && sourceType.GetArrayRank() == 1)
            {
                return CanTranslate(targetType.GetElementType(), sourceType.GetElementType());
            }
            return false;
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList)
        {
            return TranslateList<TTarget, TSource>(sourceList, new BaseDataContract());
        }

        /// <summary>
        /// Translates the list.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(List<TSource> sourceList, IDataContract dataContract)
        {
            if (CanTranslate<TTarget, TSource>())
            {
                IEntityTranslatorByDataContract translator = FindTranslator<TTarget, TSource>();
                if (translator != null)
                {
                    return translator.TranslateList<TTarget, TSource>(this, sourceList, dataContract);
                }
            }
            throw new EntityTranslatorException("No translator is available to perform the operation.");
        }

        /// <summary>
        /// Finds the translator.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns></returns>
        private IEntityTranslatorByDataContract FindTranslator<TTarget, TSource>()
        {
            return FindTranslator(typeof(TTarget), typeof(TSource));
        }

        /// <summary>
        /// Finds the translator.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns></returns>
        private IEntityTranslatorByDataContract FindTranslator(Type targetType, Type sourceType)
        {
            return _translators.Find(t => t.CanTranslate(targetType, sourceType));
        }
    }
}
