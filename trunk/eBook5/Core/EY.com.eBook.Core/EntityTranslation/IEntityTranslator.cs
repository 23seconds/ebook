using System;
using System.Collections.Generic;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.Core.EntityTranslation
{
    /// <summary>
    /// Entity translator interface.
    /// </summary>
    public interface IEntityTranslator
    {
        /// <summary>
        /// Culture 
        /// </summary>
        string Culture { get; set; }
        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        bool CanTranslate(Type targetType, Type sourceType);
        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// 	<c>true</c> if this instance can translate; otherwise, <c>false</c>.
        /// </returns>
        bool CanTranslate<TTarget, TSource>();
        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        object Translate(IEntityTranslatorService service, Type targetType, object source, TranslateLevel levelsToTranslate);
        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        TTarget Translate<TTarget>(IEntityTranslatorService service, object source, TranslateLevel levelsToTranslate);
        /// <summary>
        /// Translates a list of objects to a list of objects of a specified type.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="sourceList">The source list.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        List<TTarget> TranslateList<TTarget, TSource>(IEntityTranslatorService service, List<TSource> sourceList, TranslateLevel levelsToTranslate);
    }

    /// <summary>
    /// Entity translator by data contract interface.
    /// </summary>
    public interface IEntityTranslatorByDataContract
    {
        /// <summary>
        /// Culture 
        /// </summary>
        string Culture { get; set; }
        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        bool CanTranslate(Type targetType, Type sourceType);
        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// 	<c>true</c> if this instance can translate; otherwise, <c>false</c>.
        /// </returns>
        bool CanTranslate<TTarget, TSource>();
        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        object Translate(IEntityTranslatorServiceByDataContract service, Type targetType, object source, IDataContract dataContract);
        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        TTarget Translate<TTarget>(IEntityTranslatorServiceByDataContract service, object source, IDataContract dataContract);
        /// <summary>
        /// Translates a list of objects to a list of objects of a specified type.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="sourceList">The source list.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        List<TTarget> TranslateList<TTarget, TSource>(IEntityTranslatorServiceByDataContract service, List<TSource> sourceList, IDataContract dataContract);
    }
}
