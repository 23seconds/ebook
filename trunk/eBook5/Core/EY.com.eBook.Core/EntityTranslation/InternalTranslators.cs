﻿using System;
using System.Collections.Generic;

namespace EY.com.eBook.Core.EntityTranslation
{
    /// <summary>
    /// InternalTranslators
    /// </summary>
    public sealed class InternalTranslators : IDisposable
    {

        /// <summary>
        /// service
        /// </summary>
        private IEntityTranslatorService _service;
        /// <summary>
        /// translators
        /// </summary>
        private List<IEntityTranslator> _translators;

        /// <summary>
        /// Initializes a new instance of the <see cref="InternalTranslators"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="translators">The translators.</param>
        public InternalTranslators(IEntityTranslatorService service, List<IEntityTranslator> translators)
        {
            _service = service;
            _translators = translators;
            _translators.ForEach(t => _service.RegisterEntityTranslator(t));
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _translators.ForEach(t => _service.RemoveEntityTranslator(t));
        }

        #endregion
    }
}
