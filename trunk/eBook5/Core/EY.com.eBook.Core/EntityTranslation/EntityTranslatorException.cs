using System;
using System.Runtime.Serialization;

namespace EY.com.eBook.Core.EntityTranslation
{
    /// <summary>
    /// EntityTranslatorException
    /// </summary>
    [Serializable]
    public class EntityTranslatorException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityTranslatorException"/> class.
        /// </summary>
        public EntityTranslatorException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityTranslatorException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public EntityTranslatorException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityTranslatorException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public EntityTranslatorException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityTranslatorException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected EntityTranslatorException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base (serializationInfo, streamingContext){ }
    }
}
