using System;
using System.Collections.Generic;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.Core.EntityTranslation
{
    /// <summary>
    /// Translator base class.
    /// </summary>
    public abstract class BaseTranslator : IEntityTranslator
    {
        public string Culture { get; set; }
        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool CanTranslate(Type targetType, Type sourceType);

        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels of child objects to translate.
        /// null =&gt; the translater gets all the levels possible.
        /// 0 =&gt; just translate itself and no children.
        /// 1 or more =&gt; Translate itself and 1 or more levels of childobjects</param>
        /// <returns></returns>
        public abstract object Translate(IEntityTranslatorService service, Type targetType, object source, TranslateLevel levelsToTranslate);

        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// 	<c>true</c> if this instance can translate; otherwise, <c>false</c>.
        /// </returns>
        public bool CanTranslate<TTarget, TSource>()
        {
            return CanTranslate(typeof(TTarget), typeof(TSource));
        }


        /// <summary>
        /// Translates using the specified service.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(IEntityTranslatorService service, object source)
        {
            return Translate<TTarget>(service, source, TranslateLevel.Infinite);
        }

        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="source">The source.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(IEntityTranslatorService service, object source, TranslateLevel levelsToTranslate)
        {
            return (TTarget)Translate(service, typeof(TTarget), source, levelsToTranslate);
        }

        /// <summary>
        /// Translates a list of objects to a specified target.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="sourceList">The source list.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(IEntityTranslatorService service, List<TSource> sourceList)
        {
            return TranslateList<TTarget, TSource>(service, sourceList, TranslateLevel.Infinite);
        }

        /// <summary>
        /// Translates a list of objects to a list of objects of a specified type.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="sourceList">The source list.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(IEntityTranslatorService service, List<TSource> sourceList, TranslateLevel levelsToTranslate)
        {
            List<TTarget> targetList = new List<TTarget>();
            if (CanTranslate<TTarget, TSource>())
            {
                foreach (TSource source in sourceList)
                {
                    targetList.Add(Translate<TTarget>(service, source, levelsToTranslate));
                }
            }
            return targetList;
        }
    }

    /// <summary>
    /// Translator by data contract base class.
    /// </summary>
    public abstract class BaseTranslatorByDataContract : IEntityTranslatorByDataContract
    {
        public string Culture { get; set; }
        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="sourceType">Type of the source.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can translate the specified target type; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool CanTranslate(Type targetType, Type sourceType);

        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="targetType">Type of the target.</param>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public abstract object Translate(IEntityTranslatorServiceByDataContract service, Type targetType, object source, IDataContract dataContract);

        /// <summary>
        /// Determines whether the SourceType can be translated the specified TargetType.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <returns>
        /// 	<c>true</c> if this instance can translate; otherwise, <c>false</c>.
        /// </returns>
        public bool CanTranslate<TTarget, TSource>()
        {
            return CanTranslate(typeof(TTarget), typeof(TSource));
        }

        /// <summary>
        /// Translates using the specified service.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(IEntityTranslatorServiceByDataContract service, object source)
        {
            return Translate<TTarget>(service, source, new BaseDataContract());
        }

        /// <summary>
        /// Translates an object to a specified type.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="source">The source.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public TTarget Translate<TTarget>(IEntityTranslatorServiceByDataContract service, object source, IDataContract dataContract)
        {
            return (TTarget)Translate(service, typeof(TTarget), source, dataContract);
        }

        /// <summary>
        /// Translates a list of objects to a list of objects of a specified type.
        /// </summary>
        /// <typeparam name="TTarget">The type of the target.</typeparam>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="service">The service.</param>
        /// <param name="sourceList">The source list.</param>
        /// <param name="dataContract">The DataContract to translate to.</param>
        /// <returns></returns>
        public List<TTarget> TranslateList<TTarget, TSource>(IEntityTranslatorServiceByDataContract service, List<TSource> sourceList, IDataContract dataContract)
        {
            List<TTarget> targetList = new List<TTarget>();
            if (CanTranslate<TTarget, TSource>())
            {
                foreach (TSource source in sourceList)
                {
                    targetList.Add(Translate<TTarget>(service, source, dataContract));
                }
            }
            return targetList;
        }
    }
}
