﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace EY.com.eBook.Core.Data
{
    /// <summary>
    /// Base class which has to be used by every DataContract.
    /// Through this baseclass, the IDataContract members are passed to every DataContract
    /// </summary>
    [DataContract]   
    public class BaseDataContract : IDataContract
    {
        /// <summary>
        /// Gets or sets the display name.
        /// The string that should be shown on UI
        /// </summary>
        /// <value>The display name.</value>
        [DataMember]
        public string DisplayName { get; set; }
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public int Id { get; set; }
    }

    /// <summary>
    /// Interface that defines the members that every DataContract should implement
    /// if it wants to be translated by contract.
    /// </summary>
    public interface IDataContract
    {
        /// <summary>
        /// Gets or sets the display name.
        /// The string that should be shown on UI
        /// </summary>
        /// <value>The display name.</value>
        string DisplayName { get; set; }
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        int Id { get; set; }
    }

    /// <summary>
    /// The base class for PagedDataContracts
    /// </summary>
    /// <typeparam name="T">
    /// The datacontract that will be used to fill the PagedList, this contract must implement IDataContract
    /// </typeparam>
    [DataContract]
    public class BasePagedList<T> where T:IDataContract
    {
        /// <summary>
        /// Will contain a list with the specified datacontracts
        /// </summary>
        [DataMember]
        public List<T> PagedList { get; set; }
        /// <summary>
        /// Is set to the total number of records that comply with the given searchcriteria
        /// </summary>
        [DataMember]
        public int TotalRecords { get; set; }
    }

    /// <summary>
    /// <para>
    /// Datacontract used to populate a MultipleSelectControl
    /// </para>
    /// <para>
    /// Member: FromList (List&lt;T&gt;)
    /// </para>
    /// <para>
    /// Member: ToList (List&lt;T&gt;)
    /// </para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    public class BaseMultipleSelectDataContract<T> where T:IDataContract
    {
        /// <summary>
        /// Gets or sets from list.
        /// </summary>
        /// <value>From list.</value>
        [DataMember]
        public List<T> FromList { get; set; }
        /// <summary>
        /// Gets or sets to list.
        /// </summary>
        /// <value>To list.</value>
        [DataMember]
        public List<T> ToList { get; set; }
    }
}
