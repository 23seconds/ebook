﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.Core.Data
{
    /// <summary>
    /// DefaultFaultContract
    /// </summary>
    [DataContract]
    public class DefaultFaultContract
    {


        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>The error code.</value>
        [DataMember(Order=1)]
        public int ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(Order = 2)]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the detail.
        /// </summary>
        /// <value>The detail.</value>
        [DataMember(Order = 3)]
        public string Detail { get; set; }

        [DataMember(Order = 4)]
        public string StackTrace { get; set; }

        [DataMember(Order = 5)]
        public Guid? FileId { get; set; }

    }
}
