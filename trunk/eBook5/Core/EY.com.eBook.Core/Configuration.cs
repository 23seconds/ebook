﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;

namespace EY.com.eBook.Core
{
    public static class Config
    {
        public static string GetConfig(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static bool DebugXslt()
        {
            string value = GetConfig("eBook.DebugXslt");
            return value == "true";
        }

        public static string GetWorksheetTemplate(string app, int assyear, string printLayout)
        {
            string path = Path.Combine(WorksheetTemplatesRoot, assyear.ToString());
            if(string.IsNullOrEmpty(printLayout)) printLayout = "default";
            return Path.Combine(path,string.Format("{0}_{1}.xsl",app,printLayout));
        }

        public static string GetBiztaxFicheTemplate(int assessmenyear, string type, string ficheid)
        {
            string path = Path.Combine(Path.Combine(BiztaxFicheTemplatesRoot,assessmenyear.ToString()), type);
            return Path.Combine(path,string.Format("{0}.xslt",ficheid));
        }

        public static string GetPdfTemplate(string app, int assyear, string printLayout)
        {
            string path = Path.Combine(PdfTemplatesRoot, assyear.ToString());
            if (string.IsNullOrEmpty(printLayout)) printLayout = "default";
            return Path.Combine(path, string.Format("{0}_{1}.xsl", app, printLayout));
        }

        public static string UploadFolder
        {
            get
            {
                return Path.Combine(GetConfig("eBook.Temporary"), GetConfig("eBook.Temporary.Upload"));
            }
        }

        public static string PdfWorkingFolder
        {
            get
            {
                return Path.Combine(GetConfig("eBook.Temporary"), GetConfig("eBook.Temporary.PdfGeneration"));
            }
        }

        public static string ExcelWorkingFolder
        {
            get
            {
                return Path.Combine(GetConfig("eBook.Temporary"), GetConfig("eBook.Temporary.ExcelGeneration"));
            }
        }

        public static string PdfImageResource
        {
            get
            {
                return Path.Combine(GetConfig("eBook.Resources"), GetConfig("eBook.Resources.Images"));
            }
        }

        public static string PublicFolder
        {
            get
            {
                return GetConfig("eBook.Public");
            }
        }

        public static string RepositoryLocal
        {
            get
            {
                return Path.Combine(PublicFolder, GetConfig("eBook.Repository.Local"));
            }
        }

        public static string RepositoryVirtualDir
        {
            get
            {
                return GetConfig("eBook.Repository.VirtualDir");
            }
        }


        public static string PickupLocal
        {
            get
            {
                return Path.Combine(PublicFolder, GetConfig("eBook.Pickup.Local"));
            }
        }

        public static string PickupVirtualDir
        {
            get
            {
                return GetConfig("eBook.Pickup.VirtualDir");
            }
        }

        public static string NBBDir
        {
            get
            {
                return GetConfig("eBook.NBB");
            }
        }


        public static string NBBTodoDir
        {
            get
            {
                return GetConfig("eBook.NBB.Todo");
            }
        }

        public static string NBBDoneDir
        {
            get
            {
                return GetConfig("eBook.NBB.Done");
            }
        }
        
        public static string NBBFinanceTo
        {
            get
            {
                return GetConfig("eBook.NBB.FinanceTo");
            }
        }

        public static string TemplatesRoot
        {
            get
            {
                return Path.Combine(GetConfig("eBook.Resources"), GetConfig("eBook.Resources.Templates"));
            }
        }

        public static string WorksheetTemplatesRoot
        {
            get
            {
                return Path.Combine(TemplatesRoot, GetConfig("eBook.Resources.Templates.Worksheets"));
            }
        }

        public static string BiztaxFicheTemplatesRoot
        {
            get
            {
                return Path.Combine(TemplatesRoot, "BizTax");
            }
        }

        public static string ExcelTemplates
        {
            get
            {
                return Path.Combine(TemplatesRoot, GetConfig("eBook.Resources.Templates.Excel"));
            }
        }

        public static string PdfTemplatesRoot
        {
            get
            {
                return Path.Combine(TemplatesRoot, GetConfig("eBook.Resources.Templates.Pdf"));
            }
        }

        public static string SchemaTemplates
        {
            get
            {
                return Path.Combine(TemplatesRoot, GetConfig("eBook.Resources.Templates.Schema"));
            }
        }

        public static string XbrlTemplates
        {
            get
            {
                return Path.Combine(TemplatesRoot, GetConfig("eBook.Resources.Templates.Xbrl"));
            }
        }

        public static string StandardFont
        {
            get
            {
                return GetConfig("eBook.Resources.StandardFont");
            }
        }

        public static string AppuserUsername
        {
            get
            {
                return GetConfig("eBook.ApplicationUser.UserName");
            }
        }

        public static string AppuserName
        {
            get
            {
                return GetConfig("eBook.ApplicationUser.Name");
            }
        }

        public static string UserName
        {
            get
            {
                return GetConfig("eBook.ApplicationUser.UserName");
            }
        }

        public static string AppuserDomain
        {
            get
            {
                return GetConfig("eBook.ApplicationUser.Domain");
            }
        }
        public static string AppuserPwd
        {
            get
            {
                return GetConfig("eBook.ApplicationUser.Pwd");
            }
        }


        public static string GetBiztaxPublishFolder(string departement,string assessmentyear)
        {
            return GetConfig("BizTax.Publish." + departement).Replace("{AY}", assessmentyear);
            
           

        }
    }
}
