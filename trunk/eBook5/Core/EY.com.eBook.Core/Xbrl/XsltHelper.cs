﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.XPath;
using System.Globalization;

namespace EY.com.eBook.Core.Xbrl
{

    public class XsltHelper
    {
        public double ZeroOrValue(XPathNodeIterator iterator)
        {

            double test = 0;

            if (iterator.MoveNext())
            {
                XPathNavigator xnode = iterator.Current;
                string val = xnode.Value;
                if (!string.IsNullOrEmpty(xnode.Value)) double.TryParse(val, System.Globalization.NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture.NumberFormat, out test);
            }
            return test;
        }

        public string NullOrValue(XPathNodeIterator iterator)
        {
            if (iterator.MoveNext())
            {
                XPathNavigator xnode = iterator.Current;
                if (!string.IsNullOrEmpty(xnode.Value)) return xnode.Value;
            }
            return "0";
        }

        public string StripSpaces(XPathNodeIterator iterator)
        {
            if (iterator.MoveNext())
            {
                XPathNavigator xnode = iterator.Current;
                if (!string.IsNullOrEmpty(xnode.Value))
                {
                    string val = xnode.Value.Replace(" ", "");
                    return CoreHelper.MakeValidFileName(val);
                }
            }
            return "";
        }

        public string MinusOneDay(XPathNodeIterator iterator)
        {
            if (iterator.MoveNext())
            {
                XPathNavigator xnode = iterator.Current;
                if (string.IsNullOrEmpty(xnode.Value)) return "";
                DateTime dt = DateTime.ParseExact(xnode.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture.DateTimeFormat);
                dt = dt.AddDays(-1);
                return dt.ToString("yyyy-MM-dd");
            }
            return "";
        }

    }
}