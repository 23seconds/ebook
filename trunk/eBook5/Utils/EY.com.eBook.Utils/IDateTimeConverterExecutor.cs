namespace EY.com.eBook.Utils
{
    public interface IDateTimeConverterExecutor
    {
        void Execute(object item);
    }
}