﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace EY.com.eBook.Utils.Cryptography
{
    /// <summary>
    /// A Helper class that stores the encryption keys and does the encryption
    /// </summary>
    public class SymEncryptionSet : IEncryptionSet
    {

        private readonly byte[] _Key;
        private readonly byte[] _IVector;
        private readonly bool _HasIVector;

        private readonly SymmetricAlgorithm _Algorithm;
        public SymEncryptionSet(byte[] key, byte[] iVector)
        {
            _Key = key;
            _IVector = iVector;
            _Algorithm = SymmetricAlgorithm.Create();
            _HasIVector = true;
        }

        public SymEncryptionSet(string name, byte[] key, byte[] iVector)
        {
            _Key = key;
            _IVector = iVector;
            _Algorithm = SymmetricAlgorithm.Create(name);
            _HasIVector = true;
        }

        public SymEncryptionSet(byte[] key)
        {
            _Key = key;
            _Algorithm = SymmetricAlgorithm.Create();
            _HasIVector = false;
        }

        public SymEncryptionSet(string name, byte[] key)
        {
            _Key = key;
            _Algorithm = SymmetricAlgorithm.Create(name);
            _HasIVector = false;
        }

        private static string ConvertToBase64(byte[] bytes)
        {
            var newLength = bytes.Length * 4 / 3.0;
            var modulo = newLength % 4;
            int arraySize = Convert.ToInt32(newLength - modulo);
            if (Math.Abs(Math.Round(modulo, MidpointRounding.AwayFromZero)) > 0)
                arraySize = arraySize + 4;
            var result = new char[arraySize];
            Convert.ToBase64CharArray(bytes, 0, bytes.Length, result, 0);
            return new string(result);
        }

        private static byte[] AddIVVector(byte[] encryptedText, byte[] iVector)
        {
            var returnValue = new byte[iVector.Length + encryptedText.Length + 1];
            Buffer.BlockCopy(iVector, 0, returnValue, 0, iVector.Length);
            Buffer.BlockCopy(encryptedText, 0, returnValue, iVector.Length, encryptedText.Length);
            return returnValue;
        }

        public string Encrypt(string value)
        {
            string returnValue;
            MemoryStream result = null;
            CryptoStream encrypted = null;
            try
            {
                byte[] iVector = _IVector;
                if (!((iVector != null) && iVector.Length > 0))
                {
                    _Algorithm.GenerateIV();
                    iVector = _Algorithm.IV;
                }
                result = new MemoryStream();
                encrypted = new CryptoStream(result, _Algorithm.CreateEncryptor(_Key, iVector), CryptoStreamMode.Write);
                byte[] toEncrypt = Encoding.ASCII.GetBytes(value);
                encrypted.Write(toEncrypt, 0, toEncrypt.Length);
                encrypted.FlushFinalBlock();
                encrypted.Close();
                encrypted.Clear();
                result.Close();
                byte[] encryptedText = result.ToArray();
                if (!_HasIVector)
                {
                    encryptedText = AddIVVector(encryptedText, iVector);
                }
                returnValue = ConvertToBase64(encryptedText);
            }
            finally
            {
                if ((encrypted != null))
                    ((IDisposable)encrypted).Dispose();
                if ((result != null))
                    ((IDisposable)result).Dispose();
            }
            return returnValue;
        }

        private byte[][] ExtractIV(byte[] encryptedText)
        {
            _Algorithm.GenerateIV();
            int ivLength = _Algorithm.IV.Length;
            var ivBuffer = new byte[ivLength];
            if ((encryptedText.Length < (ivLength + 1)))
            {
                throw new CryptographicException("Invalid encryption");
            }
            var newEncryptedText = new byte[(encryptedText.Length - ivLength)];
            Buffer.BlockCopy(encryptedText, 0, ivBuffer, 0, ivLength);
            Buffer.BlockCopy(encryptedText, ivLength, newEncryptedText, 0, newEncryptedText.Length - 1);
            return new[]
                       {
			newEncryptedText,
			ivBuffer
		};
        }


        public string Decrypt(string value)
        {
            string returnValue;
            MemoryStream result = null;
            CryptoStream decrypted = null;
            try
            {
                char[] chars = value.ToCharArray();
                byte[] toDecrypt = Convert.FromBase64CharArray(chars, 0, chars.Length);
                byte[] iVector = _IVector;
                if (!_HasIVector)
                {
                    byte[][] ivResult = ExtractIV(toDecrypt);
                    toDecrypt = ivResult[0];
                    iVector = ivResult[1];
                }
                result = new MemoryStream();
                decrypted = new CryptoStream(result, _Algorithm.CreateDecryptor(_Key, iVector), CryptoStreamMode.Write);
                decrypted.Write(toDecrypt, 0, toDecrypt.Length - 1);
                decrypted.Close();
                decrypted.Clear();
                result.Close();
                returnValue = Encoding.ASCII.GetString(result.ToArray());
            }
            finally
            {
                if ((decrypted != null))
                    ((IDisposable)decrypted).Dispose();
                if ((result != null))
                    ((IDisposable)result).Dispose();
            }
            return returnValue;
        }
    }
}
