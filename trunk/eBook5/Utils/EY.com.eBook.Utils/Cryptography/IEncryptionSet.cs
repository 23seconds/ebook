﻿namespace EY.com.eBook.Utils.Cryptography
{
    public interface IEncryptionSet
    {
        string Encrypt(string value);
        string Decrypt(string value);
    }
}
