namespace EY.com.eBook.Utils.Cryptography
{
    public enum DpapiKeyType
    {
        UserKey = 1,
        MachineKey
    }
}