﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace EY.com.eBook.Utils.Cryptography
{
    public class EncryptionKeyStore
    {
        public static EncryptionKeyStore Container = new EncryptionKeyStore();

        public const string DefaultEncryption = "__default";

        private readonly Dictionary<string, IEncryptionSet> _keyTable;
        internal EncryptionKeyStore()
        {
            var machine = new DpapiEncryptionSet(DpapiKeyType.MachineKey);
            _keyTable = new Dictionary<string, IEncryptionSet>
                        {
                            {"common", new SymEncryptionSet("Rijndael", new Byte[] {66, 82, 118, 88, 91, 73, 68, 123, 111, 71, 118, 60, 102, 77, 112, 121, 52, 67, 66, 83, 50, 102, 112, 67, 68, 82, 72, 52, 80, 105, 119, 54})},
                            {DefaultEncryption, machine},
                            {"dpapi_machine", machine},
                            {"dpapi_user", new DpapiEncryptionSet(DpapiKeyType.UserKey)},
                            {"text", new PlainText()},
                        };
        }

        internal IEncryptionSet this[string key]
        {
            get { return _keyTable[key]; }
        }

        internal int Count
        {
            get { return _keyTable.Count; }
        }

        internal ICollection Keys
        {
            get { return _keyTable.Keys; }
        }

        internal bool ContainsKey(string key)
        {
            return _keyTable.ContainsKey(key);
        }
    }
}
