﻿namespace EY.com.eBook.Utils.Cryptography
{
    // Flag indicating the type of key. DPAPI terminology refers to
    // key types as user store or machine store.

    // <summary>
    // The main entry point for the application.
    // </summary>
    public class DpapiEncryptionSet : IEncryptionSet
    {

        private readonly DpapiKeyType _Type;

        private string _Entropy;
        public DpapiEncryptionSet()
            : this(DpapiKeyType.UserKey)
        {
        }

        public DpapiEncryptionSet(string entropy)
            : this(DpapiKeyType.UserKey, entropy)
        {
        }

        public DpapiEncryptionSet(DpapiKeyType type)
            : this(type, string.Empty)
        {
        }

        public DpapiEncryptionSet(DpapiKeyType type, string entropy)
        {
            _Type = type;
            _Entropy = entropy;
        }

        public string Decrypt(string value)
        {
            if (string.IsNullOrEmpty(_Entropy))
                return DPAPI.Decrypt(value);
            return DPAPI.Decrypt(value, ref _Entropy);
        }

        public string Encrypt(string value)
        {
            if (string.IsNullOrEmpty(_Entropy))
                return DPAPI.Encrypt(_Type, value);
            return DPAPI.Encrypt(_Type, value, _Entropy);
        }
    }
}
