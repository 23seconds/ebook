﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Threading;

namespace EY.com.eBook.Utils
{
    public class DateTimeConverterFactory
    {
        public static void Convert(object argument)
        {
            var enumerable = argument as IEnumerable;
            if (enumerable == null) ProcessObject(argument);
            else ProcessObjectList(enumerable);
        }

        #region TypeCreation
        private static AssemblyBuilder _asmBuilder;
        private static ModuleBuilder _modBuilder;

        private static IDictionary<Type, IDateTimeConverterExecutor> _converterCache = new Dictionary<Type, IDateTimeConverterExecutor>();
        private static readonly Object _ConverterCacheLock = new Object();

        private static IDateTimeConverterExecutor CreateTraverser(Type type)
        {
            IDateTimeConverterExecutor returnValue;
            if (_converterCache.TryGetValue(type, out returnValue)) return returnValue;
            lock (_ConverterCacheLock)
            {
                if (_converterCache.TryGetValue(type, out returnValue)) return returnValue;
                returnValue = CreateNewTraverser(type);
                _converterCache = new Dictionary<Type, IDateTimeConverterExecutor>(_converterCache) { { type, returnValue } };
                return returnValue;
            }
        }

        private static IDateTimeConverterExecutor CreateNewTraverser(Type type)
        {
            if (_asmBuilder == null)
            {
                GenerateAssemblyAndModule();
            }

            TypeBuilder typeBuilder = CreateType(_modBuilder, type.Namespace.Replace('.', '_') + "_" + type.Name + "_TypeTraverser");
            CreateConstructor(typeBuilder);
            CreateExecute(typeBuilder, type);

            Type mapperType = typeBuilder.CreateType();

            return (IDateTimeConverterExecutor)mapperType.GetConstructor(Type.EmptyTypes).Invoke(Type.EmptyTypes);
        }

        private static void GenerateAssemblyAndModule()
        {
            if (_asmBuilder == null)
            {
                AssemblyName assemblyName = new AssemblyName();
                assemblyName.Name = "DynamicDataContractTraverser";
                AppDomain thisDomain = Thread.GetDomain();
                _asmBuilder = thisDomain.DefineDynamicAssembly(assemblyName,
                             AssemblyBuilderAccess.Run);

                _modBuilder = _asmBuilder.DefineDynamicModule(
                             _asmBuilder.GetName().Name, false);
            }
        }

        private static TypeBuilder CreateType(ModuleBuilder modBuilder, string typeName)
        {
            TypeBuilder typeBuilder = modBuilder.DefineType(typeName,
                        TypeAttributes.Public |
                        TypeAttributes.Class |
                        TypeAttributes.AutoClass |
                        TypeAttributes.AnsiClass |
                        TypeAttributes.BeforeFieldInit |
                        TypeAttributes.AutoLayout,
                        typeof(object),
                        new Type[] { typeof(IDateTimeConverterExecutor) });

            return typeBuilder;
        }

        private static void CreateConstructor(TypeBuilder typeBuilder)
        {
            var constructor = typeBuilder.DefineConstructor(
                                MethodAttributes.Public |
                                MethodAttributes.SpecialName |
                                MethodAttributes.RTSpecialName,
                                CallingConventions.Standard,
                                new Type[0]);

            //Define the reflection ConstructorInfor for System.Object
            var conObj = typeof(object).GetConstructor(new Type[0]);

            //call constructor of base object
            var il = constructor.GetILGenerator();
            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Call, conObj);
            il.Emit(OpCodes.Ret);
        }

        private static void CreateExecute(TypeBuilder typeBuilder, Type forType)
        {
            var execute = typeBuilder.DefineMethod("Execute",
                                MethodAttributes.Public |
                                MethodAttributes.HideBySig |
                                MethodAttributes.NewSlot |
                                MethodAttributes.Virtual |
                                MethodAttributes.Final,
                                typeof(void),
                                new[] { typeof(object) });

            var factory = typeof(DateTimeConverterFactory);
            var validateDateTime = factory.GetMethod("ValidateDateTime", BindingFlags.Static | BindingFlags.Public, Type.DefaultBinder, new[] { typeof(DateTime) }, null);
            var validateDateTimeNullable = factory.GetMethod("ValidateDateTime", BindingFlags.Static | BindingFlags.Public, Type.DefaultBinder, new[] { typeof(DateTime?) }, null);
            var processObject = factory.GetMethod("ProcessObject", BindingFlags.Static | BindingFlags.Public, Type.DefaultBinder, new[] { typeof(Object) }, null);
            var processObjectList = factory.GetMethod("ProcessObjectList", BindingFlags.Static | BindingFlags.Public, Type.DefaultBinder, new[] { typeof(IEnumerable) }, null);

            var il = execute.GetILGenerator();
            il.DeclareLocal(forType);
            // var val = (TestContract)item;
            il.Emit(OpCodes.Ldarg_1);
            il.Emit(OpCodes.Castclass, forType);
            il.Emit(OpCodes.Stloc_0);

            var allProperties = forType.GetProperties()
                .Where(x => x.GetCustomAttributes(false).OfType<DataMemberAttribute>().Any())
                .ToList();
            foreach (var property in allProperties
                .Where(x => x.PropertyType == typeof(DateTime)))
            {
                il.Emit(OpCodes.Ldloc_0);
                il.Emit(OpCodes.Ldloc_0);
                il.Emit(OpCodes.Callvirt, property.GetGetMethod());
                il.Emit(OpCodes.Call, validateDateTime);
                il.EmitCall(OpCodes.Callvirt, property.GetSetMethod(), new[] { typeof(DateTime) });
            }
            foreach (var property in allProperties
                .Where(x => x.PropertyType == typeof(DateTime?)))
            {
                il.Emit(OpCodes.Ldloc_0);
                il.Emit(OpCodes.Ldloc_0);
                il.Emit(OpCodes.Callvirt, property.GetGetMethod());
                il.Emit(OpCodes.Call, validateDateTimeNullable);
                il.EmitCall(OpCodes.Callvirt, property.GetSetMethod(), new[] { typeof(DateTime?) });
            }
            foreach (var property in allProperties
                .Where(x => x.PropertyType.GetCustomAttributes(true).OfType<DataContractAttribute>().Any()
                            && !x.PropertyType.IsEnum))
            {
                il.Emit(OpCodes.Ldloc_0);
                il.Emit(OpCodes.Callvirt, property.GetGetMethod());
                il.Emit(OpCodes.Call, processObject);
            }
            foreach (var property in allProperties
                .Where(x => typeof(IEnumerable).IsAssignableFrom(x.PropertyType)))
            {
                il.Emit(OpCodes.Ldloc_0);
                il.Emit(OpCodes.Callvirt, property.GetGetMethod());
                il.Emit(OpCodes.Call, processObjectList);
            }
            il.Emit(OpCodes.Ret);
        }
        #endregion

        #region Public Helpers
        public static DateTime ValidateDateTime(DateTime date)
        {
            if (date == DateTime.MinValue)
                return date.AddDays(1);
            return date;
        }

        public static DateTime? ValidateDateTime(DateTime? date)
        {
            if (date == null) return null;
            return ValidateDateTime(date.Value);
        }

        public static void ProcessObject(object item)
        {
            if (item == null) return;
            var type = item.GetType();
            if (type.IsEnum) return;
            if (type.IsValueType) return;

            var traverser = CreateTraverser(type);
            traverser.Execute(item);
        }

        public static void ProcessObjectList(IEnumerable list)
        {
            if (list == null) return;
            foreach (var item in list) ProcessObject(item);
        }
        #endregion

    }
}