﻿using System;
using System.ServiceModel.Configuration;

namespace EY.com.eBook.Utils.Services
{
    public class CultureAwareBehaviorElement : BehaviorExtensionElement
    {

        public override Type BehaviorType
        {

            get { return typeof(CultureAwareBehavior); }

        }

        protected override object CreateBehavior()
        {

            return new CultureAwareBehavior();

        }

    }
}