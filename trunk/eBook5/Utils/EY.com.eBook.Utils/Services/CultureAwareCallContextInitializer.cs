﻿using System.Globalization;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text.RegularExpressions;
using System.Threading;

namespace EY.com.eBook.Utils.Services
{
    public class CultureAwareCallContextInitializer : ICallContextInitializer
    {
        private static Regex LanguageCookieParser = new Regex("EY.com.eBook.Language=([^;]*)");

        public void AfterInvoke(object correlationState)
        {
            var culture = correlationState as CultureInfo;
            if (culture != null)
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }

        public object BeforeInvoke(InstanceContext instanceContext, IClientChannel channel, Message message)
        {
            object prop;
            if (!message.Properties.TryGetValue(HttpRequestMessageProperty.Name, out prop)) return null;
            
            var httpProp = prop as HttpRequestMessageProperty;
            var allCookies = httpProp.Headers[HttpRequestHeader.Cookie];
            var match = LanguageCookieParser.Match(allCookies);
            if (!match.Success) return null;
            if (match.Groups.Count != 2) return null;

            var acceptLanguage = match.Groups[1].Value;
            if (string.IsNullOrEmpty(acceptLanguage)) return null;

            object correlationState = Thread.CurrentThread.CurrentCulture;

            var requestCulture = new CultureInfo(acceptLanguage.Trim());
            Thread.CurrentThread.CurrentCulture = requestCulture;

            return correlationState;
        }
    }
}
