﻿using System;
using System.ServiceModel;

namespace EY.com.eBook.Utils.Services
{
    /// <summary>
    ///     Proxy helper class
    /// </summary>
    /// <typeparam name="TService">The type of the service.</typeparam>
    public static class ProxyHelper<TService>
    {
        public static void Call(Action<TService> action)
        {
            Call(action, new ChannelFactory<TService>());
        }

        /// <summary>
        ///     Calls the specified action on the service.
        ///     Usage: ProxyHelper.Call&lt;IMyService>(service => myReturnValue = service.MyServiceMethod(myParam1, ...));
        /// </summary>
        /// <typeparam name="TService">The interface of the service to call</typeparam>
        /// <param name="action">The action on the service.</param>
        /// <param name="endpointName">The configured name of the endpoint</param>
        public static void Call(Action<TService> action, string endpointName)
        {
            Call(action, new ChannelFactory<TService>(endpointName));
        }

        /// <summary>
        ///     Calls the specified action on the service.
        ///     Usage: ProxyHelper.Calllt;IMyService>(service => myReturnValue = service.MyServiceMethod(myParam1, ...), "SomeEndpointName");
        /// </summary>
        /// <typeparam name="TService">The interface of the service to call</typeparam>
        /// <param name="action">The action on the service.</param>
        /// <param name="endpointName">The endpoint name of the service.</param>
        /// <param name="address">An alternative address for the endpoint</param>
        public static void Call(Action<TService> action, string endpointName, string address)
        {
            Call(action, new ChannelFactory<TService>(endpointName, new EndpointAddress(address)));
        }

        public static TResult Call<TResult>(Func<TService, TResult> func)
        {
            return Call(func, new ChannelFactory<TService>());
        }

        /// <summary>
        ///     Calls the specified func.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="func">The func.</param>
        /// <param name="endpointName">Name of the endpoint.</param>
        /// <returns></returns>
        public static TResult Call<TResult>(Func<TService, TResult> func, string endpointName)
        {
            return Call(func, new ChannelFactory<TService>(endpointName));
        }

        /// <summary>
        ///     Calls the specified func.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="func">The func.</param>
        /// <param name="endpointName">Name of the endpoint.</param>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        public static TResult Call<TResult>(Func<TService, TResult> func, string endpointName, string address)
        {
            return Call(func, new ChannelFactory<TService>(endpointName, new EndpointAddress(address)));
        }

        /// <summary>
        ///     Calls the specified function on the service.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="func">The func.</param>
        /// <param name="channelFactory">The channel factory.</param>
        /// <returns></returns>
        private static TResult Call<TResult>(Func<TService, TResult> func, ChannelFactory<TService> channelFactory)
        {
            var service = (IClientChannel) channelFactory.CreateChannel();
            try
            {
                var result = func((TService) service);
                service.Close();

                return result;
            }
            catch (FaultException)
            {
                service.Abort();
                throw;
            }
            catch (CommunicationException)
            {
                service.Abort();
                throw;
            }
            catch (TimeoutException)
            {
                service.Abort();
                throw;
            }
        }

        private static void Call(Action<TService> action, ChannelFactory<TService> channelFactory)
        {
            var service = (IClientChannel) channelFactory.CreateChannel();

            try
            {
                action((TService) service);
                service.Close();
            }
            catch (FaultException)
            {
                service.Abort();
                throw;
            }
            catch (CommunicationException)
            {
                service.Abort();
                throw;
            }
            catch (TimeoutException)
            {
                service.Abort();
                throw;
            }
        }
    }
}