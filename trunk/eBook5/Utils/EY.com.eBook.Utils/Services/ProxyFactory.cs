using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Web;
using Microsoft.CSharp;

namespace EY.com.eBook.Utils.Services
{
    public static class ProxyFactory
    {
        private const string DefaultNamespace = "EY.com.eBook.Utils.Services.Clients";
        private static readonly Dictionary<Type, Type> TypeLookup = new Dictionary<Type, Type>();

        public static object CreateInstance(string type, string endpointName)
        {
            return CreateInstance(type, endpointName, string.Empty);
        }

        public static object CreateInstance(string type, string endpointName, string address)
        {
            return CreateInstance(type, endpointName, address, false);
        }

        public static object CreateInstance(string type, string endpointName, string address, bool webEnabled)
        {
            var interfaceType = Type.GetType(type, true, false);
            if (interfaceType == null) return null;
            if (!TypeLookup.ContainsKey(interfaceType))
            {
                var assembly = CreateAssembly(interfaceType, webEnabled);
                TypeLookup.Add(interfaceType, assembly.GetType(DefaultNamespace + "." + interfaceType.Name + "Proxy"));
            }
            if (string.IsNullOrEmpty(address)) return Activator.CreateInstance(TypeLookup[interfaceType], endpointName);
            return Activator.CreateInstance(TypeLookup[interfaceType], endpointName, address);
        }

        private static string ConvertTypeToCompileSyntax(Type type)
        {
            if (type.IsGenericType)
            {
                var types = from s in type.GetGenericArguments() select ConvertTypeToCompileSyntax(s);
                var typeName = type.FullName;
                return typeName.Substring(0, typeName.IndexOf("`", StringComparison.Ordinal)) + "<" +
                       string.Join(", ", types.ToArray()) + ">";
            }
            return type.FullName;
        }

        private static Assembly CreateAssembly(Type interfaceType, bool webEnabled)
        {
            var myCodeProvider = new CSharpCodeProvider();
            var assemblyList = new List<string>
                {
                    interfaceType.Assembly.Location,
                    typeof (ProxyFactory).Assembly.Location,
                    Assembly.Load("System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
                            .Location,
                };
            foreach (var assemblyName in interfaceType.Assembly.GetReferencedAssemblies())
            {
                var location = Assembly.Load(assemblyName).Location;
                if (assemblyList.All(x => x != location)) assemblyList.Add(location);
            }
            if (webEnabled)
            {
                assemblyList.Add(typeof (ServiceBehaviorAttribute).Assembly.Location);
                assemblyList.Add(Assembly.Load("EY.com.eBook.Core.EF").Location);
                assemblyList.Add(typeof(WebInvokeAttribute).Assembly.Location);
            }

            var myCompilerParameters = new CompilerParameters(assemblyList.ToArray(),
                                                              Path.Combine(Path.GetTempPath(),
                                                                           "EY.com.eBook.Services.CustomClientsFor." +
                                                                           interfaceType.Name + ".dll"))
                {
                    GenerateExecutable = false,
                    GenerateInMemory = true,
                    TempFiles = new TempFileCollection(Path.GetTempPath()),
                };


            var sourceCode = new List<string>
                {
                    "using System;",
                    "using EY.com.eBook.Utils.Services;",
                    webEnabled? "using EY.com.eBook.Core.EF;" : string.Empty,
                    webEnabled? "using System.ServiceModel;" : string.Empty,
                    webEnabled? "using System.ServiceModel.Activation;" : string.Empty,
                    webEnabled? "using System.ServiceModel.Web;" : string.Empty,
                    string.Empty,
                    "namespace " + DefaultNamespace,
                    "{",
                    webEnabled? "[EntityFrameworkServiceBehavior][ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)][AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]" : string.Empty,
                    string.Format(
                        (interfaceType.Namespace == null
                             ? "    internal class {0}Proxy : {0}"
                             : "    internal class {0}Proxy : {1}.{0}"),
                        interfaceType.Name,
                        interfaceType.Namespace),
                    "    {",
                    "        private string EndpointName;",
                    "        private bool HasEndpointName;",
                    "        private string Address;",
                    "        private bool HasAddress;",
                    "        public " + interfaceType.Name +
                    "Proxy(string endpointName) { EndpointName = endpointName; HasEndpointName = true; }",
                    "        public " + interfaceType.Name +
                    "Proxy(string endpointName, string address) { EndpointName = endpointName; Address = address; HasAddress = true; }",
                };


            foreach (var method in interfaceType.GetMethods())
            {
                string attributes = string.Empty;
                string methodName;
                string callStatement;
                string innerCallStatement;
                if (webEnabled)
                {
                    attributes =
                        "[WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = \"/" + method.Name + "\")]";

                }
                if (method.ReturnType == typeof (void))
                {
                    methodName = attributes + string.Format("        public void {0}(", method.Name);
                    innerCallStatement = "                public void Call(" + interfaceType.FullName + " s) { s." +
                                         method.Name + "(";
                    callStatement = "            ProxyHelper<" + interfaceType.FullName + ">.Call(new Action<" +
                                    interfaceType.FullName + ">((new " + method.Name + "_Func(";
                }
                else
                {
                    var returnTypeFullName = ConvertTypeToCompileSyntax(method.ReturnType);
                    methodName = attributes + string.Format("        public {1} {0}(", method.Name, returnTypeFullName);
                    innerCallStatement = "                public " + returnTypeFullName + " Call(" +
                                         interfaceType.FullName + " s) { return s." + method.Name + "(";
                    callStatement = "            return ProxyHelper<" + interfaceType.FullName + ">.Call(new Func<" +
                                    interfaceType.FullName + ", " + returnTypeFullName + ">((new " + method.Name +
                                    "_Func(";
                }
                var privateFields = new List<string>();
                var assignPrivateFields = new List<string>();
                var arguments = new List<string>();
                var callArguments = new List<string>();
                var callPrivateArguments = new List<string>();
                foreach (var parameter in method.GetParameters())
                {
                    var parameterTypeFullName = ConvertTypeToCompileSyntax(parameter.ParameterType);
                    privateFields.Add(parameterTypeFullName + " _" + parameter.Name + ";");
                    assignPrivateFields.Add("_" + parameter.Name + " = " + parameter.Name + ";");
                    arguments.Add(parameterTypeFullName + " " + parameter.Name);
                    callArguments.Add(parameter.Name);
                    callPrivateArguments.Add("_" + parameter.Name);
                }

                sourceCode.Add("        internal class " + method.Name + "_Func");
                sourceCode.Add("        {");
                sourceCode.Add(string.Join("\n", privateFields.ToArray()));
                sourceCode.Add("        public " + method.Name + "_Func(" + string.Join(", ", arguments.ToArray()) +
                               ") { " + string.Join("; ", assignPrivateFields.ToArray()) + " }");
                sourceCode.Add(innerCallStatement + string.Join(", ", callPrivateArguments.ToArray()) + "); }");
                sourceCode.Add("        }");

                sourceCode.Add(methodName + string.Join(", ", arguments.ToArray()) + ")");
                sourceCode.Add("        {");
                sourceCode.Add("             if (HasEndpointName) ");
                sourceCode.Add(callStatement + string.Join(", ", callArguments.ToArray()) + ")).Call), EndpointName);");
                sourceCode.Add("             else ");
                sourceCode.Add(callStatement + string.Join(", ", callArguments.ToArray()) +
                               ")).Call), EndpointName, Address);");
                sourceCode.Add("        }");
            }

            sourceCode.Add("    }");
            sourceCode.Add("}");

            Trace.Write("assemblies: " + string.Join("\n", assemblyList.ToArray()));
            if (myCompilerParameters.OutputAssembly.Contains("C:\\WINDOWS\\TEMP\\"))
    {
                myCompilerParameters.OutputAssembly = myCompilerParameters.OutputAssembly.Replace("C:\\WINDOWS\\TEMP\\", "C:\\temp\\");
    }
            var myCompilerResults = myCodeProvider.CompileAssemblyFromSource(myCompilerParameters, new[]
                {
                    string.Join("\n", sourceCode.ToArray())
                });
            if (myCompilerResults.Errors.Count > 0)
                foreach (var error in myCompilerResults.Errors)
                {
                    Trace.Fail("Assembly creation error: {0}", error.ToString());
                }
            return myCompilerResults.CompiledAssembly;
        }
    }
}