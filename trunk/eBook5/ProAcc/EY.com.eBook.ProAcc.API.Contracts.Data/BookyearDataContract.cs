﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// BookyearDataContract
    /// </summary>
    [DataContract]
    public class BookyearDataContract
    {
        [DataMember]
        public int Bookyear {get;set;}
    }
}
