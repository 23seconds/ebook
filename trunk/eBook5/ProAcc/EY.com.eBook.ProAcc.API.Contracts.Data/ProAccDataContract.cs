﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// ProAccDataContract
    /// </summary>
    [DataContract]
    public class ProAccDataContract
    {
        /// <summary>
        /// ConnectionString
        /// </summary>
        /// <value>ConnectionString</value>
        [DataMember(Name = "cn")]
        public string ConnectionString { get; set; }

       
        
    }
}
