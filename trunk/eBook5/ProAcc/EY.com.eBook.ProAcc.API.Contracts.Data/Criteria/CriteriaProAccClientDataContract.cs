﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// CriteriaProAccClientDataContract
    /// </summary>
    [DataContract]
    public class CriteriaProAccClientDataContract : CriteriaProAccBaseDataContract
    {
        [DataMember]
        public Guid ClientId { get; set; }
    }
}
