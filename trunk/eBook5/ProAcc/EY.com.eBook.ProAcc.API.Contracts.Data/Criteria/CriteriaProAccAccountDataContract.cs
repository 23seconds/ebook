﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// CriteriaProAccAccountDataContract
    /// </summary>
    [DataContract]
    public class CriteriaProAccAccountDataContract : CriteriaProAccBaseDataContract
    {
       
        /// <summary>
        /// AccountNr
        /// </summary>
        /// <value>The AccountNr</value>
        [DataMember]
        public string AccountNr{ get; set; }

        [DataMember]
        public DateTime Start { get; set; }

        [DataMember]
        public DateTime End { get; set; }
    }
}
