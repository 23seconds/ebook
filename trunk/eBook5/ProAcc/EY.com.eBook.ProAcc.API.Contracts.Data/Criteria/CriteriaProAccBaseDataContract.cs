﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// CriteriaProAccBaseDataContract
    /// </summary>
    [DataContract]
    [KnownType(typeof(CriteriaProAccBookYearDataContract))]
    [KnownType(typeof(CriteriaProAccAccountDataContract))]
    [KnownType(typeof(CriteriaProAccAccountsDataContract))]
    [KnownType(typeof(CriteriaProAccStartAndEndDateDataContract))]
    [KnownType(typeof(CriteriaProAccClientDataContract))]
    public class CriteriaProAccBaseDataContract
    {
        /// <summary>
        /// Server
        /// </summary>
        /// <value>The server name</value>
        [DataMember]
        public string Server { get; set; }

        /// <summary>
        /// Database
        /// </summary>
        /// <value>The database name</value>
        [DataMember]
        public string Database { get; set; }
    }

}
