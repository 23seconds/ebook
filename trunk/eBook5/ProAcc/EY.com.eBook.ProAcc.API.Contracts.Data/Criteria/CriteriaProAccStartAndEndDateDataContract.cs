﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// CriteriaProAccStartAndEndDateDataContract
    /// </summary>
    [KnownType(typeof(CriteriaProAccBookingDataContract))]
    [DataContract]
    public class CriteriaProAccStartAndEndDateDataContract : CriteriaProAccBaseDataContract
    {
       
        /// <summary>
        /// start date
        /// </summary>
        /// <value>The start date</value>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// end date
        /// </summary>
        /// <value>The end date</value>
        [DataMember]
        public DateTime End { get; set; }
    }

    public class CriteriaProAccStartAndEndDatesDataContract : CriteriaProAccBaseDataContract
    {

        /// <summary>
        /// start date
        /// </summary>
        /// <value>The start date</value>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// end date
        /// </summary>
        /// <value>The end date</value>
        [DataMember]
        public DateTime End { get; set; }

        [DataMember]
        public DateTime? StartPrevious { get; set; }

        /// <summary>
        /// end date
        /// </summary>
        /// <value>The end date</value>
        [DataMember]
        public DateTime? EndPrevious { get; set; }
    }
}
