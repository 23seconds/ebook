﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// CriteriaProAccAccountsDataContract
    /// </summary>
    [DataContract]
    public class CriteriaProAccAccountsDataContract : CriteriaProAccBaseDataContract
    {
       
        /// <summary>
        /// AccountNrs
        /// </summary>
        /// <value>The AccountNrs</value>
        [DataMember]
        public List<string> AccountNrs{ get; set; }

        [DataMember]
        public DateTime Start { get; set; }

        [DataMember]
        public DateTime End { get; set; }
    }
}
