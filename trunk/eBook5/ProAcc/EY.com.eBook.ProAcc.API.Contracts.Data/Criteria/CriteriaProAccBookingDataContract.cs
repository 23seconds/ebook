﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// CriteriaProAccBookingDataContract
    /// </summary>
    [DataContract]
    public class CriteriaProAccBookingDataContract : CriteriaProAccStartAndEndDateDataContract
    {
        public Guid BookingId { get; set; }
    }
}
