﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// CriteriaProAccBookYearDataContract
    /// </summary>
    [DataContract]
    public class CriteriaProAccBookYearDataContract : CriteriaProAccBaseDataContract
    {
       
        /// <summary>
        /// bookyear
        /// </summary>
        /// <value>The bookyear</value>
        [DataMember]
        public int Bookyear { get; set; }
    }
}
