﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// AccountBalanceDataContract
    /// </summary>
    [DataContract]
    public class AccountBalanceDataContract
    {
        /// <summary>
        /// AccountNr
        /// </summary>
        /// <value>AccountNr</value>
        [DataMember]
        public string AccountNr { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        /// <value>Amount</value>
        [DataMember]
        public decimal Amount { get; set; }
    }
}
