﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// StartEndDataContract
    /// </summary>
    [DataContract]
    public class StartEndDataContract
    {
        /// <summary>
        /// StartDate
        /// </summary>
        /// <value>StartDate</value>
        [DataMember]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// EndDate
        /// </summary>
        /// <value>EndDate</value>
        [DataMember]
        public DateTime? EndDate { get; set; }
       
        
    }
}
