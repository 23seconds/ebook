﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.ProAcc.API.Contracts.Data
{
    /// <summary>
    /// ProAccImportDetailsDataContract
    /// </summary>
    [DataContract]
    public class ProAccImportDetailsDataContract
    {
        [DataMember(Order=1)]
        public int LastBookingNr { get; set; }

        [DataMember(Order = 2)]
        public int DiversJournalId { get; set; }

        [DataMember(Order = 3)]
        public string LastPeriodId { get; set; }


       
        
    }
}
