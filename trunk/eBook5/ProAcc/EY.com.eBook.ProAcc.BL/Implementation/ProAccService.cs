﻿#region Using directives
using System;
using System.Xml.Linq;
using System.Web;
using System.Transactions;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Security.Principal;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.BL;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using EY.com.eBook.BL.ProAcc;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data;

#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class ProAccService : IProAccService
    {
        #region IProAccService Members

        public List<BookyearDataContract> ProAccGetBookingYears(CriteriaProAccBaseDataContract cpbdc)
        {
            ProAccRepository pr = new ProAccRepository(cpbdc.Server, cpbdc.Database);
            return pr.GetBookingYears();
        }

        public List<PeriodDataContract> ProAccGetPeriods(CriteriaProAccBaseDataContract cpbdc)
        {
            ProAccRepository pr = new ProAccRepository(cpbdc.Server, cpbdc.Database);
            return pr.GetPeriods();
        }

        public List<ProAccJournalDataContract> ProAccGetJournals(CriteriaProAccBaseDataContract cpbdc)
        {
            ProAccRepository pr = new ProAccRepository(cpbdc.Server, cpbdc.Database);
            return pr.GetJournals();
        }

        public StartEndDataContract ProAccGetStartAndEndDate(CriteriaProAccBookYearDataContract cpbydc)
        {
            ProAccRepository pr = new ProAccRepository(cpbydc.Server, cpbydc.Database);
            return pr.GetStartAndEndDate(cpbydc.Bookyear);
        }

        public List<BookingLineDataContract> ProAccGetEndBalans(CriteriaProAccBookingDataContract cpsedc)
        {
            ProAccRepository pr = new ProAccRepository(cpsedc.Server, cpsedc.Database);
            //return pr.GetEndBalans(cpsedc.Start, cpsedc.End,cpsedc.BookingId);
            return new List<BookingLineDataContract>();
        }

        public List<BusinessRelationDataContract> ProAccGetCustomers(CriteriaProAccClientDataContract cpcdc)
        {
            ProAccRepository pr = new ProAccRepository(cpcdc.Server, cpcdc.Database);
            return pr.GetCustomers(cpcdc.ClientId);
        }

        public List<BusinessRelationDataContract> ProAccGetSuppliers(CriteriaProAccClientDataContract cpcdc)
        {
            ProAccRepository pr = new ProAccRepository(cpcdc.Server, cpcdc.Database);
            return pr.GetSuppliers(cpcdc.ClientId);
        }

        public List<AccountHistoryLineDataContract> ProAccGetAccountHistory(CriteriaProAccAccountDataContract cpadc)
        {
            ProAccRepository pr = new ProAccRepository(cpadc.Server, cpadc.Database);
            return pr.GetAccountHistory(cpadc.AccountNr, cpadc.Start, cpadc.End);
        }

        public List<AccountHistoryDataContract> ProAccGetAccountsHistory(CriteriaProAccAccountsDataContract cpasdc)
        {
            ProAccRepository pr = new ProAccRepository(cpasdc.Server, cpasdc.Database);
            return pr.GetAccountsHistory(cpasdc.AccountNrs, cpasdc.Start, cpasdc.End);
        }

        public List<ProAccAccountDataContract> ProAccGetAccounts(CriteriaProAccStartAndEndDatesDataContract cpbdc)
        {
            ProAccRepository pr = new ProAccRepository(cpbdc.Server, cpbdc.Database);
            return pr.GetAccounts(cpbdc);
        }

        public ProAccImportDetailsDataContract GetImportDetails(CriteriaProAccStartAndEndDatesDataContract cpbdc)
        {
            ProAccRepository pr = new ProAccRepository(cpbdc.Server, cpbdc.Database);
            return pr.GetImportDetails(cpbdc.Start, cpbdc.End);
        }

        #endregion
    }
}
