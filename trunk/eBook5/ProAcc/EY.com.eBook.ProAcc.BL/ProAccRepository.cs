﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EF=EY.com.eBook.ProAcc.Core.EF;
//using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core;
using System.Xml.Linq;
using System.Xml;
using System.Transactions;
using System.ServiceModel;
using EY.com.eBook.ProAcc.Core.EF;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.BL.Translators;
using EY.com.eBook.API.Contracts.Data;
using System.Data.EntityClient;
using System.Configuration;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.Core.EntityTranslation;





namespace EY.com.eBook.BL.ProAcc
{
    public class ProAccRepository : RepositoryBase
    {
        private EF.ProAcc EFProacc;

        // {0} = database name
        // {1} = IP address server
        
        //default
        private string eConnectionString = "metadata=res://*/ProAcc.csdl|res://*/ProAcc.ssdl|res://*/ProAcc.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source={1};Initial Catalog={0};User ID=uefin;Password=dialogrules;MultipleActiveResultSets=True\"";
        

        public ProAccRepository(string server, string database)
        {
            eConnectionString = ConfigurationManager.ConnectionStrings["ProAcc"].ToString();
            string asp25 = ConfigurationManager.AppSettings["eBook.ProAcc.Server1"];
            string asp26 = ConfigurationManager.AppSettings["eBook.ProAcc.Server2"];
            switch (server.ToUpper())
            {
                case @"DEFRANMCEASPV25\ASPVS25":
                    EFProacc = new EF.ProAcc(string.Format(eConnectionString, database, asp25));
                    break;
                case @"DEFRANMCEASPV26\ASPVS26":
                    EFProacc = new EF.ProAcc(string.Format(eConnectionString, database, asp26));
                    break;
                case @"DEVELOPMENT":
                    //EFProacc = new EF.ProAcc("metadata=res://*/ProAcc.csdl|res://*/ProAcc.ssdl|res://*/ProAcc.msl;provider=System.Data.SqlClient;provider connection string=&quot;Data Source=defranmcetsqv08\tsqlvs08;Initial Catalog=InRuleCatalog32;Password=800kUser;Persist Security Info=True;User ID=eBookUser;MultipleActiveResultSets=True&quot;");
                    EFProacc = new EY.com.eBook.ProAcc.Core.EF.ProAcc();
                    break;
                default:
                    EFProacc = new EF.ProAcc(string.Format(eConnectionString, database,server));
                    break;

            }
            
            
        }

        public List<BookyearDataContract> GetBookingYears()
        {
            var f = from p in EFProacc.Period
                    select p.Bookyear ;
            return Translator.TranslateList<BookyearDataContract, int>(f.Distinct().ToList().ConvertAll<int>(it => int.Parse(it)));
            
        }

        public List<PeriodDataContract> GetPeriods()
        {
            //where p.End.Value.Date == end.Date
            var f = from p in EFProacc.Period
                    select p;
            f= f.Distinct();
            f= f.OrderByDescending(p=>p.End);
            return Translator.TranslateList<PeriodDataContract, Period>(f.ToList());
        }

        public PeriodDataContract GetPeriod(string id)
        {
            //where p.End.Value.Date == end.Date
            var f = from p in EFProacc.Period
                    where p.Id==id
                    select p;

            Period pd = f.FirstOrDefault();
            if (pd == null) return null;
            return Translator.Translate<PeriodDataContract>(pd);
        }


        public List<ProAccJournalDataContract> GetJournals()
        {
            var f = EFProacc.JournalSet.Where(d=>d.Type=="D").Distinct();
            return Translator.TranslateList<ProAccJournalDataContract, Journal>(f.ToList());

        }

        public StartEndDataContract GetStartAndEndDate(int bookyear)
        {
            string sBookyear = bookyear.ToString();
            IQueryable<Period> qryPeriods = EFProacc.Period.Where(p => p.Bookyear == sBookyear);

            return new StartEndDataContract
            {
                StartDate = qryPeriods.Min(p => p.Begin)
                ,
                EndDate = qryPeriods.Max(p => p.End)
            };
            
        }

        // get the end balans in proacc for a certain period
        public List<AccountBalanceDataContract> GetEndBalans(DateTime start, DateTime end, Guid bookingId)
        {
            /*
            var lines = from h in EFProacc.HistorySet
                        where h.TransactionDate >= start && h.TransactionDate <= end
                        group h by h.Account.AccountNr.PadRight(12,'0') into g
                        select new AccountBalanceDataContract { AccountNr = g.Key, Amount = g.Sum(h => h.Debet) - g.Sum(h => h.Credit) };
            */
            

            List<History> histories = EFProacc.HistorySet.Include("Account").Include("Period").Where(h => h.Period.Begin >= start && h.Period.End<= end ).ToList();
            var lines = from h in histories
                        group h by h.Account.AccountNr.PadRight(12, '0') into g
                        select new AccountBalanceDataContract
                        {
                            AccountNr = g.Key.PadRight(12, '0'),
                            Amount = g.Sum(h => h.Debet) - g.Sum(h => h.Credit)
                        };

            return lines.OrderBy(h=>h.AccountNr).ToList();
                        
        }

        public List<BusinessRelationDataContract> GetCustomers(Guid clientId)
        {
            Translator.RemoveEntityTranslatorByType(typeof(BusinessRelationClientTranslator));
            Translator.RegisterEntityTranslator(new BusinessRelationClientTranslator { ClientId = clientId });
            return Translator.TranslateList<BusinessRelationDataContract, Client>(EFProacc.Client
                                                                                            .Include("City")
                                                                                            .Include("City.Country")
                                                                                            .ToList());
        }

        public List<BusinessRelationDataContract> GetSuppliers(Guid clientId)
        {
            Translator.RemoveEntityTranslatorByType(typeof(BusinessRelationSupplierTranslator));
            Translator.RegisterEntityTranslator(new BusinessRelationSupplierTranslator { ClientId = clientId });
            return Translator.TranslateList<BusinessRelationDataContract, Supplier>(EFProacc.Supplier
                                                                                            .Include("City")
                                                                                            .Include("City.Country")
                                                                                            .ToList());
        }

        public List<ProAccBookingDataContract> GetHistoryBookings(DateTime start, DateTime end)
        {
            //List<Period> periods = EFProacc.Period.Include("History").Where(p => p.Begin >= start && p.End <= end).ToList();
            List<History> history = EFProacc.HistorySet.Include("Period")
                                                        .Include("Account")
                                                        .Include("Account.Translations")
                                                        .Where(h=>h.Period.Begin >= start && h.Period.End<=end).ToList();
            List<ProAccBookingDataContract> pBookings = history.Select(h =>
                                                                        new ProAccBookingDataContract
                                                                        {
                                                                            BookingNr = h.BookingNr,
                                                                            PeriodId = h.Period.Id,
                                                                            Start = h.Period.Begin,
                                                                            End = h.Period.End,
                                                                            BookingDescription = h.BookingDescription,
                                                                            TransactionDate = h.TransactionDate,
                                                                            BookingLines = new List<AccountHistoryLineDataContract>()
                                                                        })
                                                                    .Distinct()
                                                                    .OrderBy(p => p.TransactionDate)
                                                                    .ThenBy(p => p.BookingNr)
                                                                    .ToList();


            foreach (ProAccBookingDataContract pBooking in pBookings)
            {
                List<History> lines = history.Where(h=>h.Period.Id==pBooking.PeriodId && h.BookingNr==pBooking.BookingNr).ToList();
                pBooking.BookingLines = Translator.TranslateList<AccountHistoryLineDataContract, History>(lines,TranslateLevel.FirstLevel);
                    
            }

            return pBookings;
        }

        public ProAccImportDetailsDataContract GetImportDetails(DateTime start, DateTime end)
        {
            return new ProAccImportDetailsDataContract
            {
                DiversJournalId = 1
                ,
                LastPeriodId = EFProacc.Period.Where(p => p.End.HasValue && p.End <= end).OrderByDescending(p => p.End).First().Id
                ,
                LastBookingNr = EFProacc.HistorySet.Include("Period").Where(h => h.Period.Begin >= start && h.Period.End <= end).Max(h => h.BookingNr)
            };
        }

        public List<AccountHistoryLineDataContract> GetAccountHistory(string account,DateTime start, DateTime end)
        {
            account = account.PadRight(12,'0');
            /*var historyLines = from h in EFProacc.HistorySet
                               where h.Account.AccountNr == account
                               orderby h.TransactionDate
                               select new AccountHistoryLineDataContract
                               {
                                  AccountNr = h.Account.AccountNr.PadRight(12,'0')
                                  , BusinessRelationProAccId = h.CLISUPBANK_ID
                                  , Credit = h.Credit
                                  , Debet =h.Debet
                                  , Date=h.TransactionDate
                               }*/

            IOrderedEnumerable<History> historyLines = EFProacc.HistorySet.Include("Account").Include("Period").Where(h => h.Period.Begin >= start && h.Period.End<= end)
                        .AsEnumerable().Where(h => h.Account.AccountNr.PadRight(12, '0') == account)
                        .OrderBy(h => h.TransactionDate);
            return Translator.TranslateList<AccountHistoryLineDataContract, History>(historyLines.ToList());
        }

        public List<AccountHistoryDataContract> GetAccountsHistory(List<string> accountNrs, DateTime start, DateTime end)
        {
            List<AccountHistoryDataContract> history = new List<AccountHistoryDataContract>();
            foreach (string accountNr in accountNrs)
            {
                history.Add(new AccountHistoryDataContract
                {
                    AccountNr = accountNr
                    ,
                    History = GetAccountHistory(accountNr,start,end)
                });
            }
            return history;
        }

        private string GetLanguageCulture(string languageKey)
        {
            switch (languageKey.ToUpper())
            {
                case "N":
                    return "nl-BE";
                case "F":
                    return "fr-FR";
                case "D":
                    return "de-DE";
            }
            return "en-US";
        }


        public List<ProAccAccountDataContract> GetAccountsBare()
        {
            // since business sometimes incorrectly create 2 similar accounts

            int langid = Convert.ToInt32(EFProacc.CompanySet.FirstOrDefault().LanguageId.Value);
            string defaultLang = EFProacc.LanguageSet.Where(l => l.Seq == langid).FirstOrDefault().LanguageId;
            defaultLang = GetLanguageCulture(defaultLang);

            List<ProAccAccountDataContract> accounts = new List<ProAccAccountDataContract>();

            foreach (Account acc in EFProacc.AccountSet.Include("Translations")
                                                        .OrderBy(a => a.AccountNr))
            {
                string accountNr = acc.AccountNr.PadRight(12, '0');
                //decimal? curSaldo = acc.History.Where(h=>h.Period.Begin > start && h.Period.End < end).Sum(h=>h.Debet-h.Credit);
                //decimal? prevSaldo = includePrevious ? acc.History.Where(h => h.Period.Begin > prevstart && h.Period.End < prevend).Sum(h => h.Debet - h.Credit) : 0;

                ProAccAccountDataContract adc = new ProAccAccountDataContract
                {
                    InternalNr = accountNr
                    ,
                    VisualNr = acc.AccountNr

                    ,
                    CurrentSaldo = 0
                    ,
                    PreviousSaldo = 0
                    ,
                    Translations = Translator.TranslateList<AccountTranslationDataContract, Translations>(acc.Translations.ToList())
                };

                if (adc.Translations.Where(t => t.Culture == defaultLang).Count() == 0)
                {
                    adc.Translations.Add(new AccountTranslationDataContract
                    {
                        Culture = defaultLang
                        ,
                        Description = acc.Description.Length > 250 ? acc.Description.Substring(0, 249) : acc.Description
                    });
                }
                accounts.Add(adc);

                //acc.AccountNr;
            }


            List<string> nrs = accounts.Select(a => a.InternalNr).Distinct().ToList();

            // cleanup of doubles
            foreach (string nr in nrs.Where(n => accounts.Count(a => a.InternalNr == n) > 1))
            {
                ProAccAccountDataContract padc = accounts.OrderBy(a => a.VisualNr).First(a => a.InternalNr == nr);
                //padc.CurrentSaldo = accounts.Where(a => a.InternalNr == nr).Sum(a => a.CurrentSaldo);
                //padc.PreviousSaldo = accounts.Where(a => a.InternalNr == nr).Sum(a => a.PreviousSaldo);
                accounts.RemoveAll(a => a.InternalNr == nr);
                accounts.Add(padc);
            }

           

            return accounts.OrderBy(a => a.InternalNr).ToList();
        }

        public List<ProAccAccountDataContract> GetAccounts(CriteriaProAccStartAndEndDatesDataContract cpasedc)
        {
            // since business sometimes incorrectly create 2 similar accounts

            int langid = Convert.ToInt32(EFProacc.CompanySet.FirstOrDefault().LanguageId.Value);
            string defaultLang = EFProacc.LanguageSet.Where(l => l.Seq == langid).FirstOrDefault().LanguageId;
            defaultLang = GetLanguageCulture(defaultLang);

            List<ProAccAccountDataContract> accounts = new List<ProAccAccountDataContract>();

            bool includePrevious = cpasedc.StartPrevious.HasValue && cpasedc.EndPrevious.HasValue;

            DateTime start = new DateTime(cpasedc.Start.Year,cpasedc.Start.Month,cpasedc.Start.Day,23,59,59).AddDays(-1);
            DateTime end = new DateTime(cpasedc.End.Year,cpasedc.End.Month,cpasedc.End.Day,0,0,1).AddDays(1);


            DateTime prevstart = includePrevious ? new DateTime(cpasedc.StartPrevious.Value.Year,cpasedc.StartPrevious.Value.Month,cpasedc.StartPrevious.Value.Day,23,59,59).AddDays(-1): DateTime.MinValue;
            DateTime prevend = includePrevious ? new DateTime(cpasedc.EndPrevious.Value.Year,cpasedc.EndPrevious.Value.Month,cpasedc.EndPrevious.Value.Day,0,0,1).AddDays(1): DateTime.MinValue;

            foreach (Account acc in EFProacc.AccountSet.Include("Translations")
                                                        .OrderBy(a => a.AccountNr))
            {
                string accountNr = acc.AccountNr.PadRight(12, '0');
                //decimal? curSaldo = acc.History.Where(h=>h.Period.Begin > start && h.Period.End < end).Sum(h=>h.Debet-h.Credit);
                //decimal? prevSaldo = includePrevious ? acc.History.Where(h => h.Period.Begin > prevstart && h.Period.End < prevend).Sum(h => h.Debet - h.Credit) : 0;

                ProAccAccountDataContract adc = new ProAccAccountDataContract
                {
                    InternalNr = accountNr
                    ,
                    VisualNr = acc.AccountNr
                    
                    , CurrentSaldo = 0
                    , PreviousSaldo = 0
                    ,
                    Translations = Translator.TranslateList<AccountTranslationDataContract, Translations>(acc.Translations.ToList())
                };

                if (adc.Translations.Where(t => t.Culture == defaultLang).Count() == 0)
                {
                    adc.Translations.Add(new AccountTranslationDataContract
                    {
                        Culture = defaultLang
                        ,
                        Description = acc.Description.Length > 250 ? acc.Description.Substring(0,249) : acc.Description
                    });
                }
                accounts.Add(adc);
                    
                //acc.AccountNr;
            }


            List<string> nrs = accounts.Select(a => a.InternalNr).Distinct().ToList();

            // cleanup of doubles
            foreach (string nr in nrs.Where(n => accounts.Count(a => a.InternalNr == n) > 1))
            {
                ProAccAccountDataContract padc = accounts.OrderBy(a => a.VisualNr).First(a => a.InternalNr == nr);
                //padc.CurrentSaldo = accounts.Where(a => a.InternalNr == nr).Sum(a => a.CurrentSaldo);
                //padc.PreviousSaldo = accounts.Where(a => a.InternalNr == nr).Sum(a => a.PreviousSaldo);
                accounts.RemoveAll(a => a.InternalNr == nr);
                accounts.Add(padc);
            }

            List<AccountBalanceDataContract> current = GetEndBalans(cpasedc.Start, cpasedc.End,Guid.NewGuid());

            foreach (AccountBalanceDataContract bldc in current)
            {
                ProAccAccountDataContract pac = accounts.FirstOrDefault(a => a.InternalNr == bldc.AccountNr);
                if (pac != null)
                {
                    pac.CurrentSaldo = bldc.Amount;
                }
            }

            if (includePrevious)
            {
                List<AccountBalanceDataContract> prev = GetEndBalans(cpasedc.StartPrevious.Value, cpasedc.EndPrevious.Value, Guid.NewGuid());

                foreach (AccountBalanceDataContract bldc in prev)
                {
                    ProAccAccountDataContract pac = accounts.FirstOrDefault(a => a.InternalNr == bldc.AccountNr);
                    if (pac != null)
                    {
                        pac.PreviousSaldo = bldc.Amount;
                    }
                }
            }
            

            return accounts.OrderBy(a=>a.InternalNr).ToList();
        }

        protected override void RegisterTranslators()
        {
            
            Translator.RegisterEntityTranslator(new AccountHistoryLineTranslator());
            Translator.RegisterEntityTranslator(new AccountTranslationTranslator());
            Translator.RegisterEntityTranslator(new BusinessRelationClientTranslator());
            Translator.RegisterEntityTranslator(new BusinessRelationSupplierTranslator());
            Translator.RegisterEntityTranslator(new BookyearTranslator());
            Translator.RegisterEntityTranslator(new JournalTranslator());
            Translator.RegisterEntityTranslator(new PeriodTranslator());
        }
    
    }
}
