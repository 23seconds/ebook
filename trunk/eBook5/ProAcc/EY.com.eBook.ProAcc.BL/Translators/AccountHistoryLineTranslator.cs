﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.Core.EF;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.ProAcc.API.BL.Translators
{
    /// <summary>
    /// AccountHistoryLineTranslator
    /// </summary>
    public class AccountHistoryLineTranslator : EntityMapperTranslator<History, AccountHistoryLineDataContract>
    {


        

        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override AccountHistoryLineDataContract BusinessToService(IEntityTranslatorService service, History value, TranslateLevel levelsToTranslate)
        {
            AccountHistoryLineDataContract ahl;
            using (new InternalTranslators(service, new List<IEntityTranslator> { new AccountTranslationTranslator() }))
            {
                ahl = new AccountHistoryLineDataContract
                {
                    AccountNr = value.Account.AccountNr.PadRight(12, '0')
                    ,
                    BusinessRelationProAccId = value.CLISUPBANK_ID
                    ,
                    Credit = value.Credit
                    ,
                    Debet = value.Debet
                    ,
                    Date = value.TransactionDate
                    ,
                    Translations= new List<AccountTranslationDataContract>()
                };
                if (levelsToTranslate.TranslateRelations)
                {
                    if (value.Account.Translations != null)
                    {
                        ahl.Translations = service.TranslateList<AccountTranslationDataContract, Translations>(value.Account.Translations.ToList());
                    }
                }

            }

            return ahl;

        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override History ServiceToBusiness(IEntityTranslatorService service, AccountHistoryLineDataContract value, TranslateLevel levelsToTranslate)
        {

            throw new NotImplementedException();
        }
        
    }
}
