﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.Core.EF;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.ProAcc.API.BL.Translators
{
    /// <summary>
    /// BookyearTranslator
    /// </summary>
    public class JournalTranslator : EntityMapperTranslator<Journal, ProAccJournalDataContract>
    {
        
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override ProAccJournalDataContract BusinessToService(IEntityTranslatorService service, Journal value, TranslateLevel levelsToTranslate)
        {

            return new ProAccJournalDataContract
            {
                 Id=value.Number
                 , Name = string.Format("{0} - {1}",value.Number, value.Name)
            };

            
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Journal ServiceToBusiness(IEntityTranslatorService service, ProAccJournalDataContract value, TranslateLevel levelsToTranslate)
        {

            throw new NotImplementedException();
        }
    }
}
