﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.Core.EF;

namespace EY.com.eBook.ProAcc.API.BL.Translators
{
    /// <summary>
    /// BookyearTranslator
    /// </summary>
    public class BookyearTranslator : EntityMapperTranslator<int, BookyearDataContract>
    {
        
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BookyearDataContract BusinessToService(IEntityTranslatorService service, int value, TranslateLevel levelsToTranslate)
        {

            return new BookyearDataContract
            {
                Bookyear=value
            };

            
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override int ServiceToBusiness(IEntityTranslatorService service, BookyearDataContract value, TranslateLevel levelsToTranslate)
        {

            throw new NotImplementedException();
        }
    }
}
