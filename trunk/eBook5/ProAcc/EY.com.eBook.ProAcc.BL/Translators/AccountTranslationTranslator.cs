﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.Core.EF;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.ProAcc.API.BL.Translators
{
    /// <summary>
    /// PeriodBookingYearsTranslator
    /// </summary>
    public class AccountTranslationTranslator : EntityMapperTranslator<Translations, AccountTranslationDataContract>
    {


        

        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override AccountTranslationDataContract BusinessToService(IEntityTranslatorService service, Translations value, TranslateLevel levelsToTranslate)
        {

            AccountTranslationDataContract atdc = new AccountTranslationDataContract
            {
                Culture=value.LangKey
                ,Description = value.Translation
            };

            switch (atdc.Culture.ToUpper())
            {
                case "N":
                    atdc.Culture = "nl-BE";
                    break;
                case "F":
                    atdc.Culture = "fr-FR";
                    break;
                case "D":
                    atdc.Culture = "de-DE";
                    break;
                default:
                    atdc.Culture = "en-US";
                    break;
            }
            return atdc;
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Translations ServiceToBusiness(IEntityTranslatorService service, AccountTranslationDataContract value, TranslateLevel levelsToTranslate)
        {

            throw new NotImplementedException();
        }
        
    }
}
