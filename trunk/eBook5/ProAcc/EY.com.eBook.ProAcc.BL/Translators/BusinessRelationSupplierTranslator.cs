﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.Core.EF;
using EY.com.eBook.API.Contracts.Data;


namespace EY.com.eBook.ProAcc.API.BL.Translators
{
    /// <summary>
    /// BusinessRelationClientTranslator
    /// </summary>
    public class BusinessRelationSupplierTranslator : EntityMapperTranslator<Supplier, BusinessRelationDataContract>
    {
        public Guid ClientId { get; set; }
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BusinessRelationDataContract BusinessToService(IEntityTranslatorService service, Supplier value, TranslateLevel levelsToTranslate)
        {
            return new BusinessRelationDataContract
            {
                Id = Guid.Empty
                ,
                ImportedId = value.Id
                ,
                LastName = string.IsNullOrEmpty(value.CompanyName) ? value.LastName : value.CompanyName
                ,
                FirstName = string.IsNullOrEmpty(value.FirstName) ? null : value.FirstName
                ,
                BusinessRelationType = 'S'
                ,
                ClientId = ClientId
                ,
                Address = value.Street
                ,
                ZipCode = value.City != null ? value.City.ZipCode : null
                ,
                City = value.City != null ? value.City.Name : null
                ,
                Country = value.City != null ? value.City.Country != null ? value.City.Country.Name : null : null
                ,
                CompanyNr = value.CompanyNumber
                ,
                Vat = value.VatNumber
            };

        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Supplier ServiceToBusiness(IEntityTranslatorService service, BusinessRelationDataContract value, TranslateLevel levelsToTranslate)
        {

            throw new NotImplementedException();
        }

    }
}
