﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.Core.EF;
using EY.com.eBook.API.Contracts;

namespace EY.com.eBook.ProAcc.API.BL.Translators
{
    /// <summary>
    /// BookyearTranslator
    /// </summary>
    public class PeriodTranslator : EntityMapperTranslator<Period, PeriodDataContract>
    {
        
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override PeriodDataContract BusinessToService(IEntityTranslatorService service, Period value, TranslateLevel levelsToTranslate)
        {

            return new PeriodDataContract
            {
                Period=value.Id
                , Name = string.Format("{0} [{1}-{2}]",value.Id,value.Begin.HasValue ? value.Begin.Value.ToString("dd/MM/yyyy") : "..", value.End.HasValue ? value.End.Value.ToString("dd/MM/yyyy"):"..")
                , End =value.End
                , Start = value.Begin
            };

            
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Period ServiceToBusiness(IEntityTranslatorService service, PeriodDataContract value, TranslateLevel levelsToTranslate)
        {

            throw new NotImplementedException();
        }
    }
}
