﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

public partial class ViewFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
            string flname = Request.QueryString["file"];
            string flFolder = Request.QueryString["folder"];
            flFolder =  flFolder.Replace(@"/", @"\");
            flFolder = HttpUtility.UrlDecode(flFolder);


            string startpath = ConfigurationManager.AppSettings["eBook.Folders.FileData"];

            string fullPath = Path.Combine(startpath, Path.Combine(flFolder, flname));
            

            Response.Clear();
            Response.BufferOutput = true;
            switch (Path.GetExtension(flname).ToLower())
            {
                case ".pdf":
                    Response.ContentType = "application/pdf";
                    break;
                case ".xbrl":
                    Response.ContentType = "text/xml";
                    break;
            }
            
            Response.AddHeader("Content-Disposition", "attachment;filename=" + flname);
            Response.WriteFile(fullPath);

        }
        catch (FileNotFoundException fnfe)
        {
            Response.Write(fnfe.Message);
        }
        catch (Exception ex)
        {
            Response.Clear();
            Response.BufferOutput = true;
            Response.ContentType = "text/html";
            Response.Write(ex.Message);
        }

    }
}
