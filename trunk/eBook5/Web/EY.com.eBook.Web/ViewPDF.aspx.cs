﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;

public partial class ViewPDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string id = Request.QueryString["id"];
            string name = Request.QueryString["nm"];
            if (!name.EndsWith(".pdf")) name = name + ".pdf";

            string fpath = Path.Combine(ConfigurationManager.AppSettings["eBook.Folders.ExtraPdfFiles"], string.Format("{0}.pdf", id));
            if (!File.Exists(fpath)) throw new FileNotFoundException("File not found.", Path.GetFileName(fpath));
            Response.Clear();
            Response.BufferOutput = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + name);
            Response.WriteFile(fpath);
           
        }
        catch (FileNotFoundException fnfe)
        {
            Response.Write(fnfe.Message);
        }
        catch (Exception ex)
        {
            Response.Clear();
            Response.BufferOutput = true;
            Response.ContentType = "text/html";
            Response.Write(ex.Message);
        }

    }
}
