﻿<%@ Page Language="C#"  Debug="true"  AutoEventWireup="true" CodeFile="AdminInfo.aspx.cs" Inherits="AdminInfo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    body 
    {
        background-color: Black;
    }
    div 
    {
        color: #009999;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>---------------------AdminInfo----------------------</div>
    <div>
    <div>Environment.UserName: <%= Environment.UserName %></div>
    <div>Environment.MachineName: <%= Environment.MachineName %></div>
    <div>Environment.Version: <%= Environment.Version %></div>
    <div>WindowsIdentity.GetCurrent.Name: <%= System.Security.Principal.WindowsIdentity.GetCurrent().Name %></div>
    <div>Identity.IsAuthenticated: <%= User.Identity.IsAuthenticated.ToString() %></div>
    <div>AuthenticationType: <%= User.Identity.AuthenticationType%></div>
    <div>Identity.Name: <%= User.Identity.Name%></div>
    <div>NBB URL: <%=@"""\\10.149.65.192\projects\ACR Central Processing\GTH\NBB\TODO"""%> %></div>
    </div>
    <div>---------------------AdminInfo----------------------</div>
    <asp:Button id="b1" Text="Submit" runat="server" onclick="b1_Click" />
    <asp:GridView ID="GridView1"  runat="server" />
    </form>
</body>
</html>
