/*!
 * 
 */
/*!
 * 
 */




eBook.Accounts.Mappings.accountCols = [{ colIdx: 0, name: ' ', dataIndex: 'id', render: function(vals, colCfg) { return vals[colCfg.dataIndex]; } }
                    , { colIdx: 1, name: 'Rekening', render: function(vals, colCfg) {
                        if (vals.ita) {
                            var str = vals.vnr.split('.')[1];
                            return '.' + str + ' ' + vals['dd'];
                        }
                        return vals['vnr'] + ' ' + vals['dd'];
                    }
                    }
                    , { colIdx: 2, name: 'Saldo N-1', dataIndex: 'ps', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 3, name: 'Saldo N', dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    ]; 

                    eBook.Accounts.Mappings.mappingCols = [{ colIdx: 4, key: 'VerworpenUitgaven', name: eBook.Accounts.Mappings.GridPanel_ColumnVU, dataIndex: 'id', render: function(vals, colCfg) { return vals[colCfg.dataIndex]; } }
                    , { colIdx: 5, key: 'Personeel', name: eBook.Accounts.Mappings.GridPanel_ColumnSalaris, render: function(vals, colCfg) { return vals['vnr'] + ' ' + vals['dd']; } }
                    , { colIdx: 6, key: 'BTW', name: eBook.Accounts.Mappings.GridPanel_ColumnBTW, dataIndex: 'ps', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 7, key: 'VoordelenAlleAard', name: eBook.Accounts.Mappings.GridPanel_ColumnVAA, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 8, key: 'Erelonen', name: eBook.Accounts.Mappings.GridPanel_ColumnErelonen, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    //, { colIdx: 9, key: 'VoordelenAlleAard', name: eBook.Accounts.Mappings.GridPanel_ColumnRV, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 9, key: 'RentegevendeVoorschotten', name: eBook.Accounts.Mappings.GridPanel_ColumnRV, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 10, key: 'RVIntresten', name: eBook.Accounts.Mappings.GridPanel_ColumnRVI, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 11, key: 'Reserves', name: eBook.Accounts.Mappings.GridPanel_ColumnBelRes, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 12, key: 'DBI', name: eBook.Accounts.Mappings.GridPanel_ColumnDBI, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 13, key: 'meerwaardeaandelen', name: eBook.Accounts.Mappings.GridPanel_ColumnMWAandelen, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    ];


                    eBook.Accounts.Mappings.MappingPane = Ext.extend(Ext.Panel, {
                        mainTpl: new Ext.XTemplate('<tpl for="."><div class="eb-map-container">'
                , '<div class="eb-map-accounts">'
                    , '<div class="eb-map-header">'
                        , '<div class="eb-map-header-inner">'
                            , '<div class="eb-map-row eb-map-row-hd" row="hd">'
                                , '<tpl for="accountHeaders">'
                                    , '<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-hd" id="eb-map-col-row-hd-ig-{colIdx}" row="hd" col="{colIdx}">{name}</div>'
                                , '</tpl>'
                            , '<div class="x-clear"></div></div>'
                        , '</div>'
                    , '</div>'
                    , '<div class="eb-map-body">'
                        , '<div class="eb-map-scroller">'
                            , '<tpl for="accounts">'
                                , '<tpl exec="values.rowIdx = xindex;"></tpl>'
                                , '<div class="eb-map-row eb-map-row-{rowIdx}<tpl if=\"hta\"> eb-map-adjusted</tpl>" row="{rowIdx}" acc="{inr}">'
                                    , '<tpl for="this.getAccountColumned(parent,values)">'
                                        , '<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx}<tpl if=\"this.hasRepoLinks(values.internalNr);\"> eb-map-hasAttachments</tpl>" id="eb-map-col-row-{rowIdx}-{colIdx}" row="{rowIdx}" col="{colIdx}" acc="{internalNr}">'
                                            , '<tpl if="colIdx==1">'
                                                , '<div class="eb-map-row-tools"<tpl if="!hta"></tpl> >'
                                                    , '<div class="eb-map-add-adjust"></div>'
                                                    , '<div class="eb-map-show-adjust"></div>'
                                                , '</div>'
                                            , '</tpl>'
                                            , '<tpl if="colIdx==3">'
                                                    , '<tpl if="startsaldo!=saldo">'
                                                        , '<div class="eb-map-content eb-map-saldo eb-map-saldo-changed" saldo="{saldo}" startsaldo="{startsaldo}" acc="{internalNr}">{content}</div>'
                                                    , '</tpl>'
                                                    , '<tpl if="startsaldo==saldo">'
                                                        , '<div class="eb-map-content eb-map-saldo">{content}</div>'
                                                    , '</tpl>'
                                            , '</tpl>'
                                             , '<tpl if="colIdx==0">'
                                                    , '<div class="eb-map-icons eb-map-attach"></div>'
                                                    , '<div class="eb-map-icons eb-map-health"></div>'
                                            , '</tpl>'
                                            , '<tpl if="colIdx!=3 && colIdx!=0">'
                                                , '<div class="eb-map-content">{content}</div>'
                                            , '</tpl>'

                                        , '</div>'
                                    , '</tpl>'
                                    , '<div class="x-clear"></div>'
                                    , '<tpl if="hta && adj!=null">'
                                        , '<tpl for="adj">'
                                            , '<tpl exec="values.rowIdx = parent.rowIdx;"></tpl>'
                                            , '<tpl exec="values.idx = xindex;"></tpl>'
                                            , '<div class="eb-map-row eb-map-row-{rowIdx}-{idx} eb-map-adjustment" row="{rowIdx}" adj="{idx}">'
                                                , '<tpl for="this.getAccountAdjColumned(parent,values)">'
                                                    , '<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx}-{idx}" id="eb-map-col-row-{rowIdx}-{colIdx}-{idx}" row="{rowIdx}" col="{colIdx}" adj="{idx}" acc="{internalNr}">'
                                                    , '<div class="eb-map-content">{content}</div>'
                                                    , '</div>'
                                                , '</tpl>'
                                                 , '<div class="x-clear"></div>'
                                                , '</div>'
                                            , '</tpl>'
                                    , '</tpl>'
                                , '</div>'
                            , '</tpl>'
                        , '</div>'
                    , '</div>'
                , '</div>'
                , '<div class="eb-map-mappings">'
                    , '<div class="eb-map-header">'
                        , '<div class="eb-map-header-inner">'
                        , '<div class="eb-map-row eb-map-row-hd" row="hd">'
                                , '<tpl for="mappingHeaders">'
                                    , '<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-hd" id="eb-map-col-row-hd-{colIdx}" row="hd" col="{colIdx}">{name}</div>'
                                , '</tpl>'
                            , '<div class="x-clear"></div></div>'
                        , '</div>'
                    , '</div>'
                    , '<div class="eb-map-body">'
                        , '<div class="eb-map-scroller">'
                           , '<tpl for="accounts">'
                                , '<tpl exec="values.rowIdx = xindex;"></tpl>'
                                , '<div class="eb-map-row eb-map-row-{rowIdx}<tpl if=\"hta\"> eb-map-adjusted</tpl>" row="{rowIdx}" acc="{inr}">'
                                    , '<tpl for="this.getMappingColumned(parent,values)">'
                                        , '<div qtip="{altText}" class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx} eb-map-mapping<tpl if=\"hasCurrent\"> eb-map-mapping-current</tpl><tpl if=\"hasPrevious\"> eb-map-mapping-previous</tpl>" id="eb-map-col-row-{rowIdx}-{colIdx}" row="{rowIdx}" col="{colIdx}"  acc="{internalNr}" key="{key}" currentMap="{current}" previousMap="{previous}" >'
                                            , '<div class="eb-map-delete">&nbsp;</div>'
                                        , '</div>'
                                    , '</tpl>'
                                    , '<div class="x-clear"></div>'
                                , '</div>'
                                 , '<tpl if="hta && adj!=null">'
                                        , '<tpl for="adj">'
                                            , '<tpl exec="values.idx = xindex;"></tpl>'
                                             , '<tpl exec="values.rowIdx = parent.rowIdx;"></tpl>'
                                            , '<div class="eb-map-adjustment eb-map-row eb-map-row-{rowIdx}-{idx}<tpl if=\"hta\"> eb-map-adjusted</tpl>" row="{rowIdx}" adj="{idx}">'
                                                , '<tpl for="this.getMappingColumned(parent,values)">'
                                                    , '<div qtip="{altText}" class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx} eb-map-adjustment eb-map-mapping<tpl if=\"hasCurrent\"> eb-map-mapping-current</tpl><tpl if=\"hasPrevious\"> eb-map-mapping-previous</tpl>" id="eb-map-col-row-{rowIdx}-{colIdx}-{idx}" row="{rowIdx}" col="{colIdx}"  adj="{idx}"  acc="{internalNr}" key="{key}" currentMap="{current}" previousMap="{previous}" >'
                                                        , '<div class="eb-map-delete">&nbsp;</div>'
                                                    , '</div>'
                                                , '</tpl>'
                                                , '<div class="x-clear"></div>'
                                            , '</div>'
                                        , '</tpl>'
                                 , '</tpl>'
                            , '</tpl>'
                        , '</div>'
                    , '</div>'
                , '</div>'
            , '</div></tpl>'
            , {
                hasRepoLinks: function (inr) {
                    return Ext.isDefined(eBook.Interface.fileRepository['L' + inr]);
                },
                getAccountAdjColumned: function (parent, values) {
                    var cols = eBook.Accounts.Mappings.accountCols;
                    var rval = [];
                    for (var i = 0; i < cols.length; i++) {
                        rval.push({
                            rowIdx: values.rowIdx
                                        , colIdx: cols[i].colIdx
                                        , internalNr: values.inr
                                        , hta: values.hta
                                        , startsaldo: values.ss
                                        , saldo: values.s
                                        , content: cols[i].render(values, cols[i])
                                        , parent: parent
                                        , idx: values.idx
                        });
                    }
                    return rval;
                }
                , getAccountColumned: function (parent, values) {
                    var cols = eBook.Accounts.Mappings.accountCols;
                    var rval = [];
                    for (var i = 0; i < cols.length; i++) {
                        rval.push({
                            rowIdx: values.rowIdx
                            , colIdx: cols[i].colIdx
                            , internalNr: values.inr
                            , startsaldo: values.ss
                            , saldo: values.s
                            , hta: values.hta
                            , idx: values.idx
                            , content: cols[i].render(values, cols[i])
                        });
                    }
                    return rval;
                },
                getMappingColumned: function (parent, values, idx) {
                    var cols = eBook.Accounts.Mappings.mappingCols;
                    var rval = [];
                    for (var i = 0; i < cols.length; i++) {
                        var mp = this.getMapped(values, cols[i].key);
                        var alt = values.hta ? "<b>not used: overruled by adjustments</b><br/>" : "";
                        alt += mp ? !Ext.isEmpty(mp.miid) ? eBook.Accounts.Mappings.Descs['M' + mp.miid] : '' : '';
                        var altprev = mp ? !Ext.isEmpty(mp.pmiid) ? 'previous: ' + eBook.Accounts.Mappings.Descs['M' + mp.pmiid] : '' : '';
                        if (alt != '' && altprev != '') alt += '<br/>';
                        alt += altprev

                        rval.push({
                            rowIdx: values.rowIdx
                            , colIdx: cols[i].colIdx
                            , idx: values.idx
                            , internalNr: values.inr
                            , content: cols[i].render(values, cols[i])
                            , key: cols[i].key
                            , hasCurrent: mp ? !Ext.isEmpty(mp.miid) : false
                            , hasPrevious: mp ? !Ext.isEmpty(mp.pmiid) : false
                            , current: mp ? !Ext.isEmpty(mp.miid) ? mp.miid : '' : ''
                            , previous: mp ? !Ext.isEmpty(mp.pmiid) ? mp.pmiid : '' : ''
                            , mapped: mp
                            , altText: alt
                        });
                    }
                    return rval;
                }
                , getMapped: function (values, key) {
                    if (!values.ams) return;
                    for (var i = 0; i < values.ams.length; i++) {
                        if (values.ams[i].mk == key) {
                            return values.ams[i];
                        }
                    }
                }
            })
                        // IF NEEDED APART (is contained in mainTpl)
    , headerTpl: new Ext.XTemplate('<div class="eb-map-row eb-map-row-hd" row="hd">columns<div class="x-clear"></div></div>')
                        // IF NEEDED APART (is contained in mainTpl)
    , headerColumnTpl: new Ext.XTemplate('<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-hd" id="eb-map-col-row-hd-{colIdx}" row="hd" col="{colIdx}">{name}</div>')

    , createPagingToolBar: function () {
        return [{
            xtype: 'buttongroup',
            ref: 'nav',
            border: false,
            id: this.id + '-toolbar-nav',
            columns: 11,
            items: [{
                ref: 'firstPage',
                tooltip: Ext.PagingToolbar.prototype.firstText,
                overflowText: Ext.PagingToolbar.prototype.firstText,
                iconCls: 'x-tbar-page-first',
                disabled: true,
                handler: this.moveFirst,
                scope: this
            }, {
                ref: 'previousPage',
                tooltip: Ext.PagingToolbar.prototype.prevText,
                overflowText: Ext.PagingToolbar.prototype.prevText,
                iconCls: 'x-tbar-page-prev',
                disabled: true,
                handler: this.movePrevious,
                scope: this
            },
             { xtype: 'tbtext',
                 text: Ext.PagingToolbar.prototype.beforePageText
             },
            { xtype: 'numberfield',
                cls: 'x-tbar-page-number',
                ref: 'currentpage',
                allowDecimals: false,
                allowNegative: false,
                enableKeyEvents: true,
                selectOnFocus: true,
                value: 1,
                submitValue: false,
                listeners: {
                    scope: this,
                    keydown: this.onPagingKeyDown,
                    blur: this.onPagingBlur
                }
            },
            { xtype: 'tbtext',
                ref: 'pages',
                text: String.format(Ext.PagingToolbar.prototype.afterPageText, 1)
            }
            , {
                ref: 'nextPage',
                tooltip: Ext.PagingToolbar.prototype.nextText,
                overflowText: Ext.PagingToolbar.prototype.nextText,
                iconCls: 'x-tbar-page-next',
                disabled: true,
                handler: this.moveNext,
                scope: this
            }, {
                ref: 'lastPage',
                tooltip: Ext.PagingToolbar.prototype.lastText,
                overflowText: Ext.PagingToolbar.prototype.lastText,
                iconCls: 'x-tbar-page-last',
                disabled: true,
                handler: this.moveLast,
                scope: this
            }

            , {
                tooltip: Ext.PagingToolbar.prototype.refreshText,
                overflowText: Ext.PagingToolbar.prototype.refreshText,
                iconCls: 'x-tbar-loading',
                handler: this.doRefresh,
                scope: this
            }]
        }
        , { xtype: 'mappingsearch', name: 'query', ref: 'query', emptyText: 'enter search term', width: '100%'
            , scope: this, qryFunction: 'performQuery'

        }
        ];

    }
    , performQuery: function (val) {
        this.loadAccounts();
    }
    , onPagingKeyDown: function (field, e) {
        var k = e.getKey(), pageNum;
        var tb = this.getTopToolbar();
        var nav = tb.navigate.nav;
        var pageNum = nav.currentpage.getValue()
        if (k == e.RETURN) {
            e.stopEvent();
            this.updateContents(pageNum);
        } else if (k == e.HOME || k == e.END) {
            e.stopEvent();
            pageNum = 1;
            this.updateContents(pageNum);
        } else if (k == e.UP || k == e.PAGEUP) {
            this.moveNext();
        } else if (k == e.DOWN || k == e.PAGEDOWN) {
            this.movePrevious();
        }
    }
    , onPagingBlur: function () {
        //
    }

    , doRefresh: function () {
        this.loadAccounts(true);
    }
    , moveFirst: function () {
        this.updateContents(1);
    }
    , movePrevious: function () {
        var tb = this.getTopToolbar();
        var nav = tb.navigate.nav;
        var curpage = nav.currentpage.getValue() - 1;
        this.updateContents(curpage);
    }
    , moveNext: function () {
        var tb = this.getTopToolbar();
        var nav = tb.navigate.nav;
        var curpage = nav.currentpage.getValue() + 1;
        this.updateContents(curpage);
    }
    , moveLast: function () {
        this.updateContents(this.pageCount);
    }
    , initComponent: function () {
        this.mainTpl.compile();
        var fileClosed = eBook.Interface.currentFile.get('Closed');

        Ext.apply(this, {
            //autoLoad:'mapping.html?v1.2'
            html: '<div>loading</div>'
            // , id: 'ebook-account-mapping'
            , listeners: { 'resize': { fn: this.updateSizes, scope: this} }
            //, bbar: this.createPagingToolBar()
            , tbar: new Ext.Toolbar({
                enableOverflow: true,
                items: [{
                    xtype: 'buttongroup',
                    ref: 'edit',
                    id: this.id + '-toolbar-edit',
                    columns: 1,
                    // height:90,
                    hidden: fileClosed,
                    title: eBook.Accounts.Mappings.GridPanel_ManageAccounts,
                    items: [
                                {
                                    ref: 'addaccount',
                                    text: eBook.Accounts.Mappings.GridPanel_NewAccount,
                                    iconCls: 'eBook-icon-addaccount-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    handler: this.onAddAccountClick,
                                    disabled: this.accountsLocked,
                                    width: 70,
                                    scope: this
                                }]
                }, {
                    xtype: 'buttongroup',
                    ref: 'view',
                    id: this.id + '-toolbar-view',
                    columns: 2,
                    title: eBook.Accounts.Mappings.GridPanel_ViewFilter,
                    items: [
                                {
                                    ref: 'zeroes',
                                    text: eBook.Accounts.Mappings.GridPanel_ViewSaldi0,
                                    enableToggle: true,
                                    iconCls: 'eBook-icon-saldi0account-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    toggleHandler: this.onZeroesToggle,
                                    //width: 70,
                                    scope: this
                                }, {
                                    ref: 'mappings',
                                    text: 'Only mapped accounts',
                                    enableToggle: true,
                                    iconCls: 'eBook-icon-detailedaccount-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    toggleHandler: this.onMappedToggle,
                                    //width: 70,
                                    scope: this
                                }]
                }, {
                    xtype: 'buttongroup',
                    ref: 'importexport',
                    id: this.id + '-toolbar-importexport',
                    columns: 4,
                    hidden: fileClosed,
                    title: eBook.Accounts.Mappings.GridPanel_ImportExport,
                    items: [
                                {
                                    text: 'ProAcc',
                                    iconCls: 'eBook-icon-proacc-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    menu: {

                                        width: 215,
                                        items: [{
                                            ref: 'importProAcc',
                                            text: eBook.Accounts.Mappings.GridPanel_ImportProAcc,
                                            //iconCls: 'eBook-icon-importproacc-24',
                                            //scale: 'medium',
                                            //iconAlign: 'top',
                                            handler: this.onImportProAccClick,
                                            //width: 175,
                                            scope: this,
                                            disabled: this.accountsLocked ? true : !eBook.Interface.isProAccClient()
                                        }]
                                    }
                                }, {
                                    text: 'Excel',
                                    iconCls: 'eBook-icon-excel-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    menu: {
                                        width: 215,
                                        items: [{
                                            ref: 'importExcel',
                                            text: eBook.Accounts.Mappings.GridPanel_ImportExcel,
                                            // iconCls: 'eBook-icon-importexcel-24',
                                            //scale: 'medium',
                                            //iconAlign: 'top',
                                            handler: this.onImportExcelClick,
                                            disabled: this.accountsLocked,
                                            //width: 175,
                                            scope: this
                                        }, {
                                            ref: 'excelMappings',
                                            text: eBook.Accounts.Mappings.GridPanel_ExportMappings,
                                            // iconCls: 'eBook-icon-exportexcel-24',
                                            // scale: 'medium',
                                            //  iconAlign: 'top',
                                            handler: this.onExportExcelMappingsClick,
                                            // width: 175,
                                            scope: this
                                        }, {
                                            ref: 'excelState',
                                            text: eBook.Accounts.Mappings.GridPanel_ExportState,
                                            // iconCls: 'eBook-icon-exportexcel-24',
                                            // scale: 'medium',
                                            // iconAlign: 'top',
                                            handler: this.onExportExcelStateClick,
                                            // width: 175,
                                            scope: this
                                        }]
                                    }
                                },
                                {
                                    text: 'Exact',
                                    iconCls: 'eBook-exact-ico-24-9',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    menu: {

                                        width: 215,
                                        items: [{
                                            ref: 'importExact',
                                            text: eBook.Accounts.Mappings.GridPanel_ImportExact,
                                            handler: this.onImportExactClick,
                                            scope: this,
                                            disabled: this.accountsLocked ? true : !eBook.Interface.isExactFile()
                                        }]
                                    }
                                }]
                }
                        , {
                            xtype: 'buttongroup',
                            ref: 'navigate',
                            id: this.id + '-toolbar-navigate',
                            columns: 1,
                            title: "Navigatie",
                            items: this.createPagingToolBar()
                        }, '->', {
                            ref: 'repositorySelect',
                            //text: 'Repository',
                            iconCls: 'eBook-repository-select-ico',
                            scale: 'xlarge',
                            enableToggle: true,
                            toggleHandler: this.onShowRepository,
                            disabled: eBook.Interface.isFileClosed(),
                            // disabled: this.accountsLocked,
                            //width: 175,
                            width: 64, height: 64,
                            scope: this
                        }]
            })
        });

        eBook.Accounts.Mappings.MappingPane.superclass.initComponent.apply(this, arguments);
    }
    , onShowRepository: function (btn, state) {
        eBook.Interface.showRepositorySelection(btn, state);
    }
    , syncScrollMappings: function (e) {
        //this.lockedScroller.dom.scrollTop = mb.scrollTop;
        this.mapHeaderScroller.dom.scrollLeft = this.mapScroller.dom.scrollLeft;
        this.accountsScroller.dom.scrollTop = this.mapScroller.dom.scrollTop;
    }

    , afterRender: function () {
        eBook.Accounts.Mappings.MappingPane.superclass.afterRender.call(this);

        // set events
        this.body.on('mouseover', this.onBodyMouseOver, this);
        this.body.on('mouseout', this.onBodyMouseOut, this);
        // check if file is closed
        if (eBook.Interface.currentFile != null && !eBook.Interface.currentFile.get('Closed')) {
            this.body.on('click', this.onBodyClick, this);
            this.body.on('contextmenu', this.onBodyContext, this);
        }

        this.updateContents();
        this.setDropZone();
    }
    , setDropZone: function () {
        this.dropZone = new Ext.dd.DropZone(this.body, {
            ddGroup: 'repository',
            pane: this,
            getTargetFromEvent: function (e) {
                return e.getTarget('.eb-map-row');
            },

            onNodeEnter: function (target, dd, e, data) {
                // Ext.fly(target).addClass('my-row-highlight-class');
            },

            //      On exit from a target node, unhighlight that node.
            onNodeOut: function (target, dd, e, data) {
                //  Ext.fly(target).removeClass('my-row-highlight-class');
            },

            onNodeOver: function (target, dd, e, data) {
                if (target) {
                    rw = target.getAttribute('row');
                    if (!Ext.isEmpty(rw) && rw != "hd" && dd.groups.repository) return Ext.dd.DropZone.prototype.dropAllowed;
                }

                return Ext.dd.DropZone.prototype.dropNotAllowed;
            },
            onNodeDrop: function (target, dd, e, data) {
                this.pane.addAttachment(dd.dragData.node.attributes.Item.Id, target.getAttribute('acc'), target.getAttribute('row'));
                data.node.ownerTree.startHide();
                return true;
            }
        });
    }

    , onBodyContext: function (e, t, o) {

        var acc = e.getTarget('.eb-map-col-1');
        if (acc) {
            var hta = Ext.get(acc.parentNode).hasClass('eb-map-adjusted') || Ext.get(acc.parentNode).hasClass('eb-map-adjustment');
            var nr = acc.getAttribute('acc');
            var rw = acc.getAttribute('row');
            if (!this.contextMenu) {
                this.contextMenu = new eBook.Accounts.ContextMenu({ scope: this });
            }
            this.contextMenu.showMeAt(acc, nr, hta, e.xy);
        }
        //
        return true;
    }
    , onBodyClick: function (e, t, o) {
        //if (eBook.Interface.currentFile != null && !eBook.Interface.currentFile.get('Closed')){

        var map = e.getTarget('.eb-map-mapping');
        var delmap = e.getTarget('.eb-map-delete');
        var changedSaldi = e.getTarget('.eb-map-saldo-changed');

        if (changedSaldi) {
            var wn = new eBook.Accounts.Adjustments.Window({ AccountNr: changedSaldi.getAttribute('acc'), StartSaldo: changedSaldi.getAttribute('startSaldo') });
            wn.show();
            //changedSaldi.getAttribute('acc')
        }

        if (delmap) {
            var xdelmap = Ext.get(delmap);

            var xmap = xdelmap.parent();
            var row = xmap.parent('.eb-map-row');
            if (row.hasClass('eb-map-adjusted')) return;

            var key = map.getAttribute('key');

            this.deleteMapping(key, map);
            xdelmap.setStyle('display', 'none');
        } else if (map) {
            //            if (this.activeMapEl!= map && this.activeMapEl) {
            //                this.activeMapEl.setAttribute('active', 'false');
            //                this.activeMapEl = null;
            //            }
            var xmap = Ext.get(map);

            var row = xmap.parent('.eb-map-row');
            if (row.hasClass('eb-map-adjusted')) return;

            var key = map.getAttribute('key');
            var mnu = eBook.Accounts.Mappings.Menus[key];
            //mnu.syncSize();
            mnu.show(map);
            map.setAttribute('active', 'true');
            this.activeMapEl = map;
            this.setRowClass.defer(100, this, [row.getAttribute('row'), map.getAttribute('col'), map.getAttribute('adj'), true]);
        } else {
            //            if(this.activeMapEl) {
            //                this.activeMapEl.setAttribute('active', 'false');
            //              //  this.setRowClass.defer(100, this, [this.activeMapEl.getAttribute('row'), this.activeMapEl.getAttribute('col'), true]);
            //                this.activeMapEl = null;
            //            }
        }
        //}

    }
    , setRowClass: function (ridx, ajidx, cidx, st) {
        var cls = '.eb-map-row-' + ridx;
        if (ajidx) cls += '-' + ajidx;
        var els = this.body.query(cls);

        for (var i = 0; i < els.length; i++) {
            if (st) Ext.get(els[i]).addClass('eb-map-row-active');
            if (!st) Ext.get(els[i]).removeClass('eb-map-row-active');
        }
        if (cidx) {
            var hd = Ext.get('eb-map-col-row-hd-' + cidx);
            if (hd) {
                if (st) {
                    hd.addClass('eb-map-col-active');
                } else {
                    hd.removeClass('eb-map-col-active');
                }
            }
        }
    }
                        /*if (cidx) {
                        var camelRe = /(-[a-z])/gi;
                        var camelFn = function(m, a) { return a.charAt(1).toUpperCase(); };
                        var clr = st ? 'rgba(255, 230, 0, 0.5)' : 'transparent';
                        var prop = 'background-color';
                        mapRules['.eb-map-col-' + cidx].style[prop.replace(camelRe, camelFn)] = clr;


                        }*/
    , showSaldiTip: function (el, startsaldo, saldo) {
        var nw = false;
        if (this.saldiTip == null) {
            this.saldiTip = new Ext.ToolTip({
                id: 'content-anchor-tip',
                anchor: 'left',
                html: null,
                width: 160,
                autoHide: true,
                closable: false
            });
            nw = true;
        }
        if (this.saldiTip.target != el) {
            this.saldiTip.target = el;
            this.saldiTip.initTarget(el);

        }
        this.saldiTip.show();
        startsaldo = Ext.util.Format.euroMoney(startsaldo);
        saldo = Ext.util.Format.euroMoney(saldo);
        this.saldiTip.update('<div class="eb-saldi-qt-txt"><div class="eb-saldi-qt-title">imported:</div><div class="eb-saldi-qt-amount">' + startsaldo + '</div></div><div class="eb-down-arrow">&nbsp;</div><div class="eb-saldi-qt-txt"><div class="eb-saldi-qt-title">adjusted to: </div><div class="eb-saldi-qt-amount">' + saldo + '</div></div>');
    }
    , hideSaldiTip: function () {
        if (this.saldiTip) this.saldiTip.hide();
    }
    , onBodyMouseOver: function (e, t, o) {
        var attachment = e.getTarget('.eb-map-attach');
        var changedSaldi = e.getTarget('.eb-map-saldo-changed');
        var mapped = e.getTarget('.eb-map-mapping-current');
        var rowOver = e.getTarget('.eb-map-row');
        var colOver = e.getTarget('.eb-map-col');


        if (attachment) {
            var par = Ext.get(attachment.parentNode);
            if (par.hasClass('eb-map-hasAttachments')) {
                eBook.Interface.showRepoLinksToolTip(attachment, 'ACCOUNT', par.dom.getAttribute('acc'), this);
            }

        }

        if (changedSaldi) {
            changedSaldi = Ext.get(changedSaldi);
            var ss = changedSaldi.getAttribute('startsaldo');
            var s = changedSaldi.getAttribute('saldo');
            this.showSaldiTip(changedSaldi, ss, s);
        } else if (rowOver && colOver) {
            rowOver = Ext.get(rowOver);
            colOver = Ext.get(colOver);
            if (rowOver.hasClass('eb-map-row-hd')) return;
            this.setRowClass(rowOver.getAttribute('row'), rowOver.getAttribute('adj'), colOver.getAttribute('col'), true);

        } else if (rowOver) {
            rowOver = Ext.get(rowOver);
            if (rowOver.hasClass('eb-map-row-hd')) return;
            this.setRowClass(rowOver.getAttribute('row'), rowOver.getAttribute('adj'), null, true);

        }
        if (mapped) {
            var xmap = Ext.get(mapped);
            var xdel = xmap.child('.eb-map-delete');
            if (xdel) {
                xdel.setStyle('display', 'block');
            }
        }

    }
    , onBodyMouseOut: function (e, t, o) {
        var changedSaldi = e.getTarget('.eb-map-saldo-changed');
        var mapped = e.getTarget('.eb-map-mapping-current');
        var map = e.getTarget('.eb-map-mapping');
        var rowOver = e.getTarget('.eb-map-row');
        var colOver = e.getTarget('.eb-map-col');


        if (map) {
            // if(map.getAttribute('active')=='true') return;
        }

        if (changedSaldi) {
            changedSaldi = Ext.get(changedSaldi);
            this.hideSaldiTip();
        } else if (rowOver && colOver) {
            rowOver = Ext.get(rowOver);
            colOver = Ext.get(colOver);
            if (rowOver.hasClass('eb-map-row-hd')) return;
            this.setRowClass(rowOver.getAttribute('row'), rowOver.getAttribute('adj'), colOver.getAttribute('col'), false);
        } else if (rowOver) {
            rowOver = Ext.get(rowOver);
            if (rowOver.hasClass('eb-map-row-hd')) return;
            this.setRowClass(rowOver.getAttribute('row'), rowOver.getAttribute('adj'), null, false);

        }
        if (mapped) {
            var xmap = Ext.get(mapped);
            var xdel = xmap.child('.eb-map-delete');
            if (xdel) {
                xdel.setStyle('display', 'none');
            }
        }
    }
    , updateContents: function (page) {

        this.body.update(this.mainTpl.apply(this.getDta(page)));
        this.mapScroller = this.body.child('.eb-map-mappings .eb-map-body');
        this.mapHeaderScroller = this.body.child('.eb-map-mappings .eb-map-header');
        this.accountsScroller = this.body.child('.eb-map-accounts .eb-map-body');
        this.mon(this.mapScroller, 'scroll', this.syncScrollMappings, this);
        this.syncSize();
    }
    , updateSizes: function (me, adjWidth, adjHeight, rawWidth, rawHeight) {
        var ct = this.body.child('.eb-map-container');
        var cts = ct.getSize();
        var ac = this.body.child('.eb-map-accounts');
        var acs = ac.getSize();
        var minWd = 100 * 11;
        var mapWd = (cts.width - acs.width);
        var mp = this.body.child('.eb-map-mappings');
        var scrollWd = mapWd;
        if (scrollWd < minWd) scrollWd = minWd;
        mp.setStyle('width', mapWd + 'px');

        var acb = ac.child('.eb-map-body');
        var mpb = mp.child('.eb-map-body');
        var mpbs = mpb.child('.eb-map-scroller');
        var mph = mp.child('.eb-map-header');
        var mphs = mph.child('.eb-map-header-inner');
        if (mp && mpb && mph) {

            mpb.setStyle('width', mapWd + 'px');
            mph.setStyle('width', mapWd + 'px');
            mphs.setStyle('width', scrollWd + 'px');
            mpbs.setStyle('width', scrollWd + 'px');

            var szh = mph.getSize();
            var sz = mp.getSize();
            acb.setStyle('height', (sz.height - szh.height) + 'px');
            mpb.setStyle('height', (sz.height - szh.height) + 'px');

        }
    }
    , getQuery: function () {
        var tb = this.getTopToolbar();
        return tb.navigate.query.getValue();
    }
    , importDone: function () {
        this.loadAccounts();
    }
    , loadAccounts: function (retainPage) {
        this.getEl().mask("Loading", 'x-mask-loading');
        var tb = this.getTopToolbar();
        var includeZeroes = tb.view.zeroes.pressed;
        var onlyMapped = tb.view.mappings.pressed;
        retainPage = retainPage == true;

        var nav = tb.navigate.nav;
        var curpage = nav.currentpage.getValue();

        eBook.CachedAjax.request({
            url: eBook.Service.schema + 'GetAccountsMappingView'
            , method: 'POST'
            , params: Ext.encode({ cicdc: { FileId: eBook.Interface.currentFile.get('Id')
                                    , Culture: eBook.Interface.Culture
                                    , IncludeZeroes: includeZeroes
                                    , MappedOnly: onlyMapped
                                    , Query: this.getQuery()
            }
            })
            , callback: this.loadCallback
            , scope: this
            , pageNum: retainPage ? curpage : null

        });
    }
    , onAddAccountClick: function () {
        var wn = new eBook.Accounts.Manage.Window({});
        wn.show(this);
    }
    , onZeroesToggle: function (btn, state) {
        this.loadAccounts();
    }
    , onMappedToggle: function (btn, state) {
        this.loadAccounts();
    }
    , onImportProAccClick: function () {
        eBook.Interface.showProAccImport(this);
    }
    , onExportProAccAdjustClick: function () {
    }
    , onImportExcelClick: function () {
        var wn = new eBook.Excel.UploadWindow({ mappingPane: this });
        wn.show();
    }
    , onImportExactClick: function () {
        var wn = new eBook.Exact.UploadWindow({mappingPane: this});
        wn.show();
    }
    , onExportExcelMappingsClick: function () {

        this.getEl().mask("Preparing export", 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.output + 'ExportAccountMappings'
            , method: 'POST'
            , params: Ext.encode({ cicdc: { Id: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.Culture} })
            , callback: this.onExportExcelMappingsCallback
            , scope: this
        });
    }
    , onExportExcelMappingsCallback: function (opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.ExportAccountMappingsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onExportExcelStateClick: function () {
    }
    , accounts: []
    , loadCallback: function (opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                var rc = robj['GetAccountsMappingViewResult'];
                this.accounts = rc;
                this.updateContents(opts.pageNum);
                this.getEl().unmask();
            } else {
                eBook.Interface.showResponseError(resp, this.title);
                this.getEl().unmask();
            }
        } catch (e) {
            //console.log(e);
            this.getEl().unmask();
        }

    }
    , currentpage: 1
    , setPaging: function (page) {
        var tb = this.getTopToolbar();
        var nav = tb.navigate.nav;

        this.pageCount = Math.floor(this.accounts.length / this.maxLoad);
        if (this.accounts.length % this.maxLoad > 0) this.pageCount++;
        if (page > this.pageCount) return;
        nav.pages.setText(String.format(Ext.PagingToolbar.prototype.afterPageText, this.pageCount));

        nav.currentpage.setValue(page);
        this.currentpage = page;
        if (page > 1) {
            nav.firstPage.enable();
            nav.previousPage.enable();
        } else {
            nav.firstPage.disable();
            nav.previousPage.disable();
        }

        if (page < this.pageCount) {
            nav.nextPage.enable();
            nav.lastPage.enable();
        } else {
            nav.nextPage.disable();
            nav.lastPage.disable();
        }

    }
    , maxLoad: 30
    , getDta: function (page) {
        var from = 0;
        var to = this.maxLoad;
        page = Ext.isEmpty(page) ? 1 : page;
        var ranged = Ext.isDefined(page);
        if (!ranged && this.accounts.length > this.maxLoad) {
            ranged = true;
            from = 0;
            to = this.maxLoad;
            page = 1;
        } else if (ranged) {
            from = page == 1 ? 0 : ((page - 1) * this.maxLoad);
            to = this.maxLoad * page;
        } else {
            page = 1;
        }
        this.setPaging(page, from, to, this.accounts.length);
        return {
            accountHeaders: eBook.Accounts.Mappings.accountCols
            , mappingHeaders: eBook.Accounts.Mappings.mappingCols
            , accounts: ranged ? this.accounts.slice(from, to) : this.accounts
        }
    }
    , hasChanges: false
    , deleteMapping: function (key, mapEl) {
        var row = mapEl.getAttribute('row');
        var adj = mapEl.getAttribute('adj');
        if (Ext.isDefined(adj) && !Ext.isEmpty(adj)) {
            adj = parseInt(adj) - 1;
        } else {
            adj = -1;
        }
        var pg = this.currentpage == 1 ? 0 : ((this.currentpage - 1) * this.maxLoad);
        var idx = pg + parseInt(row) - 1
        mapEl = Ext.get(mapEl);
        var nr = mapEl.dom.getAttribute('acc');
        mapEl.dom.setAttribute('currentMap', '');
        mapEl.removeClass('eb-map-mapping-current');
        this.setQTip(mapEl);
        this.hasChanges = true;

        var acc = this.accounts[idx];
        if (adj > -1) acc = acc.adj[adj];

        if (!acc.ams) {
        } else {
            var del = null;
            for (var i = 0; i < acc.ams.length; i++) {
                if (acc.ams[i].mk == key) {
                    if (acc.ams[i].pmiid) {
                        acc.ams[i].mm = null;
                        acc.ams[i].miid = null;
                    } else {
                        del = acc.ams[i];
                    }
                    break;
                }
            }
            if (del) acc.ams.remove(del);
        }
        if (adj > -1) {
            this.accounts[idx].adj[adj] = acc;
        } else {
            this.accounts[idx] = acc;
        }

        var action = {
            text: String.format(eBook.Accounts.Mappings.Window_SavingMapping, nr, '')
            , url: eBook.Service.schema
            , params: { camdc: {
                FileId: eBook.Interface.currentFile.get('Id')
                        , InternalNr: nr
                        , MappingKey: key
                        , Person: eBook.User.getActivePersonDataContract()
                //,pmiid:null
            }
            }
            , action: 'DeleteAccountMapping'
        };
        this.refOwner.addAction(action);

    }
    , setQTip: function (el) {
        if (el.dom) el = el.dom;
        var key = el.getAttribute('key');
        var cur = el.getAttribute('currentMap');
        var prev = el.getAttribute('previousMap');
        if (Ext.isEmpty(key)) return;
        //var mp = this.getMapped(values, cols[i].key);


        if (Ext.isEmpty(cur) && Ext.isEmpty(prev)) {
            el.setAttribute('qtip', '');
        }
        else {
            var alt = !Ext.isEmpty(cur) ? eBook.Accounts.Mappings.Descs['M' + cur] : '';
            var altprev = !Ext.isEmpty(prev) ? eBook.Accounts.Mappings.Descs['M' + prev] : '';
            if (alt != '' && altprev != '') alt += '<br/>Previous:';
            alt += altprev;
            el.setAttribute('qtip', alt);
        }
    }
    , changeMapping: function (mnuEl, mapEl) {
        var row = mapEl.getAttribute('row');
        var adj = mapEl.getAttribute('adj');
        if (Ext.isDefined(adj) && !Ext.isEmpty(adj)) {
            adj = parseInt(adj) - 1;
        } else {
            adj = -1;
        }
        var pg = this.currentpage == 1 ? 0 : ((this.currentpage - 1) * this.maxLoad);
        var idx = pg + parseInt(row) - 1;

        mapEl = Ext.get(mapEl);
        mapEl.addClass('eb-map-mapping-current');
        var cur = mapEl.dom.getAttribute('currentMap');
        if (cur == mnuEl.mapId) return;

        mapEl.dom.setAttribute('currentMap', mnuEl.mapId);
        this.setQTip(mapEl);
        var nr = mapEl.dom.getAttribute('acc');

        var amdc = {
            fid: eBook.Interface.currentFile.get('Id')
                        , inr: nr
                        , mk: mnuEl.key
                        , mm: mnuEl.meta
                        , miid: mnuEl.mapId
        };

        var acc = this.accounts[idx];
        if (adj > -1) acc = acc.adj[adj];

        if (!acc.ams) {
            acc.ams = [];
            acc.ams.push(amdc);
        } else {
            var ok = false;
            for (var i = 0; i < acc.ams.length; i++) {
                if (acc.ams[i].mk == amdc.mk) {
                    acc.ams[i].mm = amdc.mm;
                    acc.ams[i].miid = amdc.miid;
                    ok = true;
                    break;
                }
            }
            if (!ok) acc.ams.push(amdc);
        }
        if (adj > -1) {
            this.accounts[idx].adj[adj] = acc;
        } else {
            this.accounts[idx] = acc;
        }
        // mapEl.setAttribute('active', 'false');
        this.hasChanges = true;
        var action = {
            text: String.format(eBook.Accounts.Mappings.Window_SavingMapping, nr, '')
            , url: eBook.Service.schema
            , params: { amdc: {
                fid: eBook.Interface.currentFile.get('Id')
                        , inr: nr
                        , mk: mnuEl.key
                        , mm: mnuEl.meta
                        , miid: mnuEl.mapId
                        , Person: eBook.User.getActivePersonDataContract()
                //,pmiid:null
            }
            }
            , action: 'SaveAccountMapping'
        };
        this.refOwner.addAction(action);
        // this.pane.changeMapping(mnuEl, mapEl);
    }
    , addAttachment: function (reposId, accountNr, rowNr) {
        var el = Ext.get('eb-map-col-row-' + rowNr + '-0');
        el.addClass('eb-map-hasAttachments');
        var action = {
            text: 'Adding repository link to ' + accountNr//String.format(eBook.Accounts.Mappings.Window_SavingMapping, nr, '')
                , url: eBook.Service.repository
                , params: { rldc: {
                    ClientId: eBook.Interface.currentClient.get('Id')
                        , FileId: eBook.Interface.currentFile.get('Id')
                            , ItemId: reposId
                            , ConnectionType: 'ACCOUNT'
                            , ConnectionGuid: eBook.EmptyGuid
                            , ConnectionAccount: accountNr
                            , ConnectionDetailedPath: ''
                    // , Person: eBook.User.getActivePersonDataContract()
                    //,pmiid:null
                }
                }
                , action: 'AddLink'
        };
        this.refOwner.addAction(action);


        this.refOwner.addAction({
            text: 'updating links'
                , action: function () { eBook.Interface.loadRepoLinks(); return true; }
        });

    }
    , onEditAccountClick: function (mnuItem, e) {
        var accountNr = mnuItem.parentMenu.accountNr;
        var wn = new eBook.Accounts.Manage.Window({ internalNr: accountNr });
        wn.show(this, "DESC");
    }
    , onAddAjdustmentClick: function (mnuItem, e) {
        var accountNr = mnuItem.parentMenu.accountNr;
        var wn = new eBook.Accounts.Manage.Window({ internalNr: accountNr });
        wn.show(this, "ADDADJ");
    }
    , onEditAjdustmentsClick: function (mnuItem, e) {
        var accountNr = mnuItem.parentMenu.accountNr;
        var wn = new eBook.Accounts.Manage.Window({ internalNr: accountNr });
        wn.show(this, "ADJ");
    }
                    });

Ext.reg('mappingpane', eBook.Accounts.Mappings.MappingPane);

/*
bodyRowAccTpl


[
                { rowIdx: 0, id: '1ac', vnr: '100000', dd: 'Test rekening', ps: 12000, s: 324324.34, hta: false }
                , { rowIdx: 1, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false, ams: [{ mk: 'VerworpenUitgaven', miid: '2168e25f-286e-4955-ace0-cf62124105a2', pmiid: '2168e25f-286e-4955-ace0-cf62124105a2'}] }
                , { rowIdx: 2, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 3, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 4, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 5, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 6, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: true }
                , { rowIdx: 7, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 8, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 9, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 10, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: true }
                , { rowIdx: 11, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 12, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 13, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 14, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 15, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 16, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 17, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 18, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 19, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                ]
                

// adjustment
<tpl for="adjustments">
<div class="eb-map-row eb-map-row-{rowIdx}" row="{rowIdx}">
<tpl for=""
<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx}" id="eb-map-col-row-{rowIdx}-{colIdx}-{adjIdx}" row="{rowIdx}" col="{colIdx}" adj="{adjIdx}" acc="{internalnr}"><div class="eb-map-content">im</div></div>
 <div class="x-clear"></div>

bodyRowMapTpl

//ROW:
//ROW: eb-map-hide-adjusts

*/


eBook.Accounts.Mappings.Search = function(config) {
    Ext.apply(this, config);
    eBook.Accounts.Mappings.Search.superclass.constructor.call(this);
}; // eo constructor

Ext.extend(eBook.Accounts.Mappings.Search, Ext.form.TwinTriggerField, {

    searchText: 'Search'
    , searchTipText: 'Type a text to search and press Enter'
    , selectAllText: 'Select All'
    , iconCls: 'icon-magnifier'
    , checkIndexes: 'all', disableIndexes: []
    , dateFormat: undefined
    , showSelectAll: true
    
    , initComponent: function() {
        Ext.apply(this, {
            selectOnFocus: undefined === this.selectOnFocus ? true : this.selectOnFocus
            , trigger1Class: 'x-form-clear-trigger'
            , trigger2Class: 'x-form-search-trigger'
            , onTrigger1Click: this.onTriggerClear.createDelegate(this)
            , onTrigger2Click: this.onTriggerSearch.createDelegate(this)
            , minLength: this.minLength
            , width:210
        });
        eBook.Accounts.Mappings.Search.superclass.initComponent.apply(this, arguments);
    }
    , onRender: function(container, position) {
        eBook.Accounts.Mappings.Search.superclass.onRender.call(this, container, position);
        this.el.dom.qtip = this.searchTipText;

        if (this.minChars) {
            this.el.on({ scope: this, buffer: 300, keyup: this.onKeyUp });
        }

        // install key map
        var map = new Ext.KeyMap(this.el, [{
            key: Ext.EventObject.ENTER
                , scope: this
                , fn: this.onTriggerSearch
        }, {
            key: Ext.EventObject.ESC
                , scope: this
                , fn: this.onTriggerClear
        }]);
        map.stopEvent = true;


        this.reconfigure();
    }
    , onTriggerClear: function() {
        this.setValue('');
        this.focus();
        this.onTriggerSearch();
    } // eo function onTriggerClear
        // }}}
        // {{{
        /**
        * private Search Trigger click handler (executes the search, local or remote)
        */
    , onTriggerSearch: function() {
        if (!this.isValid()) {
            return;
        }
        var val = this.getValue();
        var sc = this.scope;
        sc[this.qryFunction].call(sc, val);

    } // eo function onTriggerSearch
        // }}}
        // {{{

        /**
        * field el keypup event handler. Triggers the search
        * @private
        */
    , onKeyUp: function(e, t, o) {

        //if (e.isNavKeyPress()) {
        if (e.isSpecialKey() && e.keyCode != e.BACKSPACE) { // do not trigger on special keys (except backspace)
            return;
        }

        var length = this.getValue().toString().length;
        if (0 === length || this.minChars <= length) {
            this.onTriggerSearch();
        }
    } // eo function onKeyUp
        // }}}
        // {{{
        /**
        * @param {Boolean} true to disable search (TwinTriggerField), false to enable
        */
//    , setDisabled: function() {
//        this.setDisabled.apply(this.field, arguments);
//    }
//    , enable: function() {
//        this.setDisabled(false);
//    }
//    , disable: function() {
//        this.setDisabled(true);
//    }
    , reconfigure: function() {




    }
    
});
Ext.reg('mappingsearch', eBook.Accounts.Mappings.Search);
eBook.Accounts.Mappings.Window = Ext.extend(eBook.Window, {
    accountsLocked: false,
    initComponent: function() {
        this.accountsLocked = eBook.Interface.currentFile.get('NumbersLocked');
        //alert(this.accountsLocked);
        Ext.apply(this, {
            title: eBook.Accounts.Mappings.Window_Title
            , busyMsg: eBook.Accounts.Mappings.Window_SavingMappings // TO TRANSLATE
            , layout: 'fit'
            , id: 'eBook-accountmapping-window'
            , iconCls: 'eBook-Window-accountmappings-ico'
            , actionsShowWait: false
            , items: [
                { xtype: 'mappingpane', ref: 'pane', accountsLocked: this.accountsLocked }
            ]
            , enableFileCache: true
        });
        eBook.Accounts.Mappings.Window.superclass.initComponent.apply(this, arguments);

    }
    , show: function() {
        eBook.Accounts.Mappings.Window.superclass.show.call(this);
        this.pane.loadAccounts();
    }
    , changeMapping: function(mnuEl, mapEl) {
        this.pane.changeMapping(mnuEl, mapEl);
    }
    , amReady: false
    , onBeforeClose: function() {

        if (this.actions.length == 0 && !this.isBusy) {
            if (this.pane.saldiTip) this.pane.saldiTip.destroy();
            if (this.pane.hasChanges) eBook.Interface.center.fileMenu.updateWorksheets();
            return true;
        }
        return eBook.Accounts.Mappings.Window.superclass.onBeforeClose.call(this);
    }

});
eBook.Accounts.Adjustments.BookingLinesPanel = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function () {
        Ext.apply(this, {
            store:
                 new Ext.data.JsonStore({
                     StartSaldo: this.StartSaldo,
                     autoLoad: true,
                     autoDestroy: false,
                     root: 'GetBookingLinesByAccountNrResult',
                     fields: eBook.data.RecordTypes.BookingLines,
                     baseParams: {
                         FileId: eBook.Interface.currentFile.get('Id')
                        , AccountNr: this.AccountNr
                     },
                     proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                         url: eBook.Service.schema + 'GetBookingLinesByAccountNr'
                         , criteriaParameter: 'cbldc'
                         , method: 'POST'
                     }),
                     listeners: {

                         'load': {
                             fn: function (t, r, o) {
                                 var totalC = 0;
                                 var totalD = 0;


                                 if (this.StartSaldo > 0) {
                                     var r1 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_gs, 'Debet': this.StartSaldo, 'Bold': 'true' }, 1); // create new record
                                     totalD = parseInt(this.StartSaldo);
                                 } else {
                                     var r1 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_gs, 'Credit': this.StartSaldo * (-1), 'Bold': 'true' }, 1); // create new record
                                     totalC = parseInt(this.StartSaldo * (-1));
                                 }
                                 t.insert(0, r1);

                                 for (var i = 0; i < r.length; i++) {
                                     totalC += parseInt(r[i].get('Credit'));
                                     totalD += parseInt(r[i].get('Debet'));
                                 }

                                 var r2 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_t, 'Debet': totalD, 'Credit': totalC, 'Bold': 'true' }, t.data.length); // create new record
                                 t.insert(t.data.length, r2);

                                 if (totalD > totalC) {
                                     var r3 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_s, 'Debet': totalD - totalC, 'Bold': 'true' }, t.data.length); // create new record
                                 } else {
                                     var r3 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_s, 'Credit': totalC - totalD, 'Bold': 'true' }, t.data.length); // create new record
                                 }
                                 t.insert(t.data.length, r3);

                                 return true;
                             }
                         }
                     }

                 })
             , loadMask: true
             , autoScroll: true
             , autoExpandColumn: 'CommentColumnSchemaAdjustments'
             , disableSelection: true
             , viewConfig: {
                 stripeRows: false,
                 getRowClass: function (record) {
                     if (record.get('Bold') == 'true') {
                         return 'schemaAdjustmentBold';
                     }
                     return '';
                     //return calcAge(record.get('dob')) < 18 ? 'minor' : 'adult';
                 }
             }
            , listeners: {
                'rowclick': {
                    fn: function (t, idx, e) {
                        if (this.store.getAt(idx).get('Bold') == "") {
                            var wn = new eBook.Journal.Window({ LaunchedFromSchema: true, bookingId: this.store.getAt(idx).get('BookingId') });
                            wn.show();
                        }
                    }
                , scope: this
                }

            }
            , columns: [{
                header: eBook.Journal.GroupedGrid_Account,
                width: 150,
                dataIndex: 'InternalAccountNr',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == 'true') {
                        metaData.css += 'schemaAdjustmentBold';
                    } else {
                        metaData.css += 'schemaAdjustmentPointer';
                    }
                    return value;
                }

            }, {
                header: 'Nr',
                dataIndex: 'GroupNr',
                align: 'center',
                width: 30

            }, {
                header: eBook.Journal.GroupedGrid_Debet,
                dataIndex: 'Debet',
                align: 'right',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == 'true') {
                        metaData.css += 'schemaAdjustmentBold';
                    } else {
                        metaData.css += 'schemaAdjustmentPointer';
                    }
                    return Ext.util.Format.euroMoney(value);
                }
            }, {
                header: eBook.Journal.GroupedGrid_Credit,
                dataIndex: 'Credit',
                align: 'right',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == 'true') {
                        metaData.css += 'schemaAdjustmentBold';
                    } else {
                        metaData.css += 'schemaAdjustmentPointer';
                    }
                    return Ext.util.Format.euroMoney(value);
                }
            }, {
                header: eBook.Journal.GroupedGrid_BusinessRelation,
                width: 150,
                dataIndex: 'ClientSupplierName',
                align: 'center',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == '') {
                        metaData.css += 'schemaAdjustmentPointer';
                    }
                    return value;
                }
            }, {
                header: eBook.Journal.GroupedGrid_Comment,
                dataIndex: 'Comment',
                id: 'CommentColumnSchemaAdjustments',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == '') {
                        metaData.css += 'schemaAdjustmentPointer';
                        metaData.attr = 'ext:qtip="' + value + '"';
                    }
                    return value;
                }
            }]


        });
        eBook.Accounts.Adjustments.BookingLinesPanel.superclass.initComponent.apply(this, arguments);
    }

});

Ext.reg('bookingLinesPanel', eBook.Accounts.Adjustments.BookingLinesPanel);
eBook.Accounts.Adjustments.Window = Ext.extend(eBook.Window, {
    accountsLocked: false,
    initComponent: function () {

        Ext.apply(this, {
            title: "Adjustments"//eBook.Accounts.Adjustments.Window_Title
            , layout: 'fit'
            , width: 800
            , height: 500
            , id: 'eBook-accountadjustments-window'
            , iconCls: 'eBook-Window-accountmappings-ico'
            , modal: true
            , actionsShowWait: false
            , items: [
                { xtype: 'bookingLinesPanel', ref: 'blpanel', AccountNr: this.AccountNr, StartSaldo: this.StartSaldo }
            ]
            , enableFileCache: true
            , tbar: {
                xtype: 'toolbar',
                items: [{
                    ref: '../PrintView',
                    text: eBook.Document.Window_PrintPreview,
                    iconCls: 'eBook-icon-previewdocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onPrintPreviewClick,
                    scope: this
                }]
            }
        });
        eBook.Accounts.Adjustments.Window.superclass.initComponent.apply(this, arguments);

    }
    , show: function () {
        eBook.Accounts.Adjustments.Window.superclass.show.call(this);
        //this.loadData();
        //this.pane.loadAccounts();
    }
    , onPrintPreviewClick: function () {
        var checkedCheckboxes = new Array();
        checkedCheckboxes.push('correctieboekingenSchema');
        eBook.Splash.setText("Generating quickprint");
        eBook.Splash.show();
        Ext.Ajax.request({
            url: eBook.Service.output + 'GenerateQuickprint'
            , method: 'POST'
            , params: Ext.encode({
                cqdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.Culture,
                    Options: checkedCheckboxes,
                    AccountNr: this.AccountNr,
                    StartSaldo: this.StartSaldo
                }
            })
            , callback: this.onPrintPreviewReady
            , scope: this
        });
    }
    , onPrintPreviewReady: function (opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GenerateQuickprintResult);
            //window.open('pickup/' + robj.GenerateLeadSheetsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }

});

// Normal grid, viewing ONE booking.
// Used for single booking "journals" like eBook opening & state at end last year.

eBook.Journal.FTBGrid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {

       var storeCfg = {
                    selectAction: 'GetFinalTrialBalance',
                    fields: eBook.data.RecordTypes.FinalTrialBalance,
                    idField: 'InternalAccountNr',
                    enableFileCached: true,
                    autoDestroy: true,
                    criteriaParameter: 'cicdc',
                    serviceUrl:eBook.Service.schema,
                    baseParams: {
                        Id: eBook.Interface.currentFile.get('Id')
                             , Culture: eBook.Interface.Culture
                    }
                };
        var cols = [{
                            header: eBook.Journal.Grid_Account,
                            dataIndex: 'Account',
                            align: 'left',
                            renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                                if (value != null) {
                                    return value[eBook.Interface.Culture.substr(0, 2)];
                                } else {
                                    return null;
                                }
                            },
                            summaryType: 'count',
                            summaryRenderer: function(v, params, data) {
                                return ((v === 0 || v > 1) ? '(' + v + ' Lines)' : '(1 Line)');
                            },
                            hideable: false,
                            sortable: true
                        }, {
                            header: eBook.Journal.Grid_Start,
                            width: 140,
                            dataIndex: 'StartBalance',
                            renderer: Ext.util.Format.euroMoney,
                            summaryType: 'sum',
                            align: 'right',
                            fixed: true,
                            hideable: false,
                            sortable: true
                        }, {
                            header: eBook.Journal.Grid_AdjustmentsDebet,
                            width: 140,
                            dataIndex: 'AdjustmentsDebet',
                            renderer: Ext.util.Format.euroMoneyJournal,
                            summaryType: 'sum',
                            align: 'right',
                            fixed: true,
                            hideable: false,
                            sortable: true
                        }, {
                            header: eBook.Journal.Grid_AdjustmentsCredit,
                            width: 140,
                            dataIndex: 'AdjustmentsCredit',
                            renderer: Ext.util.Format.euroMoneyJournal,
                            summaryType: 'sum',
                            align: 'right',
                            fixed: true,
                            hideable: false,
                            sortable: true
                        }, {
                            header: eBook.Journal.Grid_End,
                            width: 140,
                            dataIndex: 'EndBalance',
                            renderer: Ext.util.Format.euroMoney,
                            summaryType: 'sum',
                            align: 'right',
                            fixed: true,
                            hideable: false,
                            sortable: true
}];
                        
        Ext.apply(this, {
            store: new eBook.data.JsonStore(storeCfg)
            , plugins: [new Ext.ux.grid.GridSummary()]
            , loadMask: true
            , columnLines: true
            , columns: cols
            , viewConfig: {
                forceFit: true
            }
        });
        eBook.Journal.FTBGrid.superclass.initComponent.apply(this, arguments);
    }

});

Ext.reg('eBook.Journal.FTBGrid', eBook.Journal.FTBGrid);


eBook.Journal.FTBWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        var its = [];
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        var tbarCfg = [{
            ref: 'excel',
            text: eBook.Journal.Window_ExportExcel,
            iconCls: 'eBook-icon-exportexcel-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onExportExcelClick,
            width: 70,
            scope: this
}];
            Ext.apply(this, {
                items: [{ xtype: 'eBook.Journal.FTBGrid', ref: 'grid'}]
                , layout: 'fit'
                , tbar: new Ext.Toolbar({ items: tbarCfg })
            });
            eBook.Journal.FTBWindow.superclass.initComponent.apply(this, arguments);
        }
    , show: function() {
        eBook.Journal.FTBWindow.superclass.show.call(this);
        this.grid.store.load();
    }

    , onExportExcelClick: function() {

        this.getEl().mask(eBook.Journal.Window_ExcelExporting, 'x-mask-loading');
        eBook.CachedAjax.request({
                url: eBook.Service.output + 'ExportFinalTrial'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.Culture
                }
                })
                , callback: this.onExportExcelFinalComplete
                , scope: this
        });
    }
    , onExportExcelFinalComplete: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open("pickup/" + robj.ExportFinalTrialResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    });

// Grid containing bookings and their booking lines, grouped by booking
// Used for manual and automatic bookings.

eBook.Journal.GroupingView = Ext.extend(Ext.grid.GroupingView, {
    initTemplates : function(){
        Ext.grid.GroupingView.superclass.initTemplates.call(this);
        this.state = {};

        var sm = this.grid.getSelectionModel();
        sm.on(sm.selectRow ? 'beforerowselect' : 'beforecellselect',
                this.onBeforeRowSelect, this);

        if(!this.startGroup){
            this.startGroup = new Ext.XTemplate(
                '<div id="{groupId}" class="x-grid-group {cls} {[this.isAuto(values)]}">',
                    '<div id="{[values.rs[0].get(\'BookingId\')]}" class="x-grid-group-hd" style="{style}"><div class="x-grid-group-title">', this.groupTextTpl, '</div></div>',
                    '<div id="{groupId}-bd" class="x-grid-group-body">'
                , {
                    isAuto: function(values) {
                        var record = values.rs[0];
                        if (!Ext.isEmpty(record.get('WorksheetTypeId'))) {
                            return 'eBook-auto-booking';
                        }
                        return '';
                    }
            });
        }
        this.startGroup.compile();

        if (!this.endGroup) {
            this.endGroup = '</div></div>';
        }
    }
});


eBook.Journal.GroupedGrid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function () {
        var gActions = [{
            iconCls: 'eBook-grid-row-edit-ico'
                , tooltip: eBook.Journal.GroupedGrid_EditBooking
        }];

        gActions.push({
            iconCls: 'eBook-grid-row-delete-ico'
                , tooltip: eBook.Journal.GroupedGrid_DeleteBooking
        });
        this.actions = new Ext.ux.grid.RowActions({
            header: 'Actions'
        , align: 'left'
        , keepSelection: true
        , actions: []
        , groupActions: (eBook.Interface.currentFile.get('Closed')) ? [] : gActions
        });

        this.actions.on({ groupaction: this.onGroupAction });

        if (this.launchedFromSchema) {
            var storeCfg = {
                selectAction: 'GetSchemaBooking',
                fields: eBook.data.RecordTypes.BookingLines,
                idField: 'Id',
                autoDestroy: true,
                criteriaParameter: 'cidc',
                serviceUrl: eBook.Service.schema,
                baseParams: {
                    Id: this.bookingId
                 , Culture: eBook.Interface.Culture
                }
                , enableFileCached: true
                , groupField: 'GroupNr'
                //, groupField:'BookingId'
            };
        } else {
            var storeCfg = {
                selectAction: 'GetAllBookings',
                fields: eBook.data.RecordTypes.BookingLines,
                idField: 'Id',
                autoDestroy: true,
                criteriaParameter: 'cfdc',
                serviceUrl: eBook.Service.schema,
                baseParams: {
                    FileId: eBook.Interface.currentFile.get('Id')
                 , Culture: eBook.Interface.Culture
                }
                , enableFileCached: true
                , groupField: 'GroupNr'
                //, groupField:'BookingId'
            };
        }
        

        

        Ext.apply(this, {
            store: new eBook.data.GroupedJsonStore(storeCfg)
            , plugins: [new Ext.ux.grid.GridSummary(), new Ext.ux.grid.GroupSummary(), this.actions]
            , loadMask: true
            , columnLines: true
            , columns: [{
                header: eBook.Journal.GroupedGrid_Booking,
                dataIndex: 'GroupNr',
                align: 'left',
                hidden: true,
                groupRenderer: function (v, unused, r, ridx, cidx, ds) {
                    var desc = r.get('BookingDescription');
                    if (!Ext.isEmpty(r.get('WorksheetTypeId'))) {
                        var wrec = eBook.Interface.center.fileMenu.getWorksheetRecord(r.get('WorksheetTypeId'));
                        if (wrec) {
                            desc = wrec.get('name') + ' ' + desc;
                        }
                    }
                    var exp = '';
                    if (r.get('ExportCount')) exp = '<span class="eb-journal-exported-msg">Already exported [' + r.get('ExportCount') + ' time(s)] to ProAcc</span>';

                    return String.format("{0} - {1} {2}", r.get('GroupNr'), desc, exp);
                }
                , hideable: false
            }, {
                header: eBook.Journal.GroupedGrid_Account,
                dataIndex: 'Account',
                align: 'left',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (value != null) {
                        return value[eBook.Interface.Culture.substr(0, 2)];
                    } else {
                        return null;
                    }
                },
                summaryType: 'count',
                summaryRenderer: function (v, params, data) {
                    return ((v === 0 || v > 1) ? '(' + v + ' Lines)' : '(1 Line)');
                }
                , hideable: false
                , sortable: true
                , width: 200
            }, {
                header: eBook.Journal.GroupedGrid_Debet,
                width: 140,
                dataIndex: 'Debet',
                renderer: Ext.util.Format.euroMoneyJournal,
                summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
            }, {
                header: eBook.Journal.GroupedGrid_Credit,
                width: 140,
                dataIndex: 'Credit',
                renderer: Ext.util.Format.euroMoneyJournal,
                summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
            }, {
                header: eBook.Journal.GroupedGrid_BusinessRelation,
                width: 140,
                dataIndex: 'ClientSupplierName',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    return value;
                },
                //summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
            }, {
                header: eBook.Journal.GroupedGrid_Comment,
                width: 300,
                dataIndex: 'Comment',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    metaData.attr = 'ext:qtip="' + value + '"';
                    return value;
                },
                //summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
            }, this.actions
                ]
            , enableHdMenu: true
            , view: new eBook.Journal.GroupingView({
                forceFit: true
                , groupTextTpl: '<input type="checkbox" value="{[values.rs[0].get(\'BookingId\')]}" class="eb-journal-select"/> {group}'
                , showGroupName: false
                , enableGrouping: true
                , enableGroupingMenu: false
                , enableNoGroups: false
                , hideGroupedColumn: true
                // , groupMode: 'display'

            })
        });
        eBook.Journal.GroupedGrid.superclass.initComponent.apply(this, arguments);
    }
    , onGroupAction: function (grid, records, action, groupId) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        if (eBook.Interface.currentFile.get('NumbersLocked')) return;

        //console.log(groupId);
        //console.log(records);
        //console.log(grid.getStore());
        //records = grid.getStore().query('Nr', groupId.replace('Nr-',''), false).getRange();
        //records =

        if (!records) return;
        if (records.length == 0) return;
        var rec = records[0];
        if (!Ext.isEmpty(rec.get('WorksheetTypeId'))) {
            eBook.Interface.center.fileMenu.openWorksheetSh(rec.get('WorksheetTypeId'), rec.get('WorksheetCollection'), rec.get('WorksheetRowId'));
            return;
        }

        switch (action) {
            case 'eBook-grid-row-edit-ico':
                //if (this.grid.journal == 'MANUAL' || this.grid.journal == 'LASTYEAR') {
                var wn = new eBook.Journal.Booking.Window({});
                wn.show(grid, records);
                //}
                break;
            case 'eBook-grid-row-delete-ico':
                Ext.Msg.confirm(eBook.Journal.GroupedGrid_DeleteBookingTitle
                    , String.format(eBook.Journal.GroupedGrid_DeleteBookingMsg, records[0].get('BookingDescription'))
                    , function (b) { if (b == 'yes') { this.onDeleteBookingPerform(records); } }
                    , grid);

                break;
        }
    }
    , onEditBookingClick: function () {
        //alert('edit booking');
    }
    , onDeleteBookingPerform: function (records) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var record = records[0];
        var deleteAction = {
            text: String.format(eBook.Journal.GroupedGrid_DeletingBooking, record.get('BookingDescription'))
            , params: { id: record.get('BookingId') }
            , action: 'DeleteBooking'
            , url: eBook.Service.schema
        };
        this.store.remove(records);
        this.refOwner.addAction(deleteAction);
        this.refOwner.hasChanges = true;

    }
    , onPerformDeleteBooking: function () {
        alert('performing delete');
    }
    , getChecked: function () {
        var inputs = this.getEl().query('.eb-journal-select'), i = 0, bids = [];
        for (i = 0; i < inputs.length; i++) {
            if (inputs[i].checked) bids.push(inputs[i].value);
        }
        return bids;
    }
    , setExportModus: function (onOrOff) {
        var tb = this.ownerCt.getTopToolbar();
        if (onOrOff) {
            tb.exportproacc.show();
            tb.addbooking.disable();
            this.getEl().addClass('eb-journal-export');
            var its = this.store.data.items, i = 0, col = {};
            for (i = 0; i < its.length; i++) {
                if (!col['I' + its[i].data.BookingId]) {
                    col['I' + its[i].data.BookingId] = true;
                    var group = Ext.get(its[i].data.BookingId);
                    if (its[i].data.ExportCount == 0) {
                        group.child('.eb-journal-select').dom.checked = true;
                    } else {
                        group.child('.eb-journal-select').dom.checked = false;
                    }
                }
            }
            Ext.Msg.alert('Export ProAcc', "Select all bookings you wish to export to ProAcc and perform the export.");
        } else {
            tb.exportproacc.hide();
            tb.addbooking.enable();
            this.getEl().removeClass('eb-journal-export');
        }
    }
});

Ext.reg("eBook.Journal.GroupedGrid", eBook.Journal.GroupedGrid);
eBook.Journal.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        var its = [];
        var fileClosed = eBook.Interface.currentFile.get('Closed') || (eBook.Interface.serviceYearend && eBook.Interface.serviceYearend.c);
        var tbarCfg = [{
            xtype: 'buttongroup',
            ref: 'importexport',
            id: this.id + '-toolbar-importexport',
            columns: 3,
            title: eBook.Journal.Window_ImportExport,
            items: [
                        {
                            ref: 'importProAcc',
                            text: eBook.Journal.Window_ImportProAcc,
                            iconCls: 'eBook-icon-importproacc-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onImportProAccClick,
                            width: 70,
                            scope: this,
                            disabled: fileClosed ? true : !eBook.Interface.isProAccClient()
                        }, {
                            ref: 'importProAcc',
                            text: eBook.Journal.Window_ImportExcel,
                            iconCls: 'eBook-icon-importexcel-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onImportExcelClick,
                            width: 70,
                            disabled: fileClosed,
                            scope: this
                        }, {
                            ref: 'excel',
                            text: eBook.Journal.Window_ExportExcel,
                            iconCls: 'eBook-icon-exportexcel-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onExportExcelClick,
                            width: 70,
                            scope: this,
                            hidden: true
                        }]
        }];

        switch (this.journal) {
            case 'FINAL':
                tbarCfg = [{
                    ref: 'excel',
                    text: eBook.Journal.Window_ExportExcel,
                    iconCls: 'eBook-icon-exportexcel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onExportExcelClick,
                    width: 70,
                    scope: this
                }];
            default:
                tbarCfg = [{
                    ref: 'addbooking',
                    text: eBook.Journal.Window_AddBooking,
                    iconCls: 'eBook-icon-addbooking-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onAddBookingClick,
                    disabled: fileClosed,
                    width: 70,
                    scope: this
                }, {
                    ref: 'startexportproacc',
                    text: 'Start export ProAcc',
                    iconCls: 'eBook-icon-exportexcel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    enableToggle: true,
                    toggleHandler: this.onToggleExportProaccClick,
                    hidden: false,//!eBook.Interface.isProAccClient(),
                    width: 70,
                    scope: this
                }, {
                    ref: 'exportproacc',
                    text: eBook.Accounts.Mappings.GridPanel_ExportProAccAdjustments,
                    iconCls: 'eBook-icon-exportexcel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onExportProaccClick,
                    width: 70,
                    hidden: true,
                    scope: this
                }
                        ];
                its.push({ xtype: 'eBook.Journal.GroupedGrid'
                            , journal: 'ALL'
                            , ref: 'grid'
                            , launchedFromSchema: this.LaunchedFromSchema
                            , bookingId: this.bookingId
                });
                break;

        }

        Ext.apply(this, {
            items: its
                , layout: 'fit'
                , tbar: new Ext.Toolbar({ items: tbarCfg })
        });
        eBook.Journal.Window.superclass.initComponent.apply(this, arguments);
    }
    , onAddBookingClick: function () {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var wn = new eBook.Journal.Booking.Window({});
        wn.show(this.grid);
    }
    , show: function () {
        eBook.Journal.Window.superclass.show.call(this);
        this.grid.store.load();
    }
    , onImportProAccClick: function () {
        if (eBook.Interface.currentFile.get('Closed')) return;
        eBook.Interface.showProAccImport(this);
    }
    , onImportExcelClick: function () {
        if (eBook.Interface.currentFile.get('Closed')) return;
        eBook.Interface.showExcelImport(this);
    }
    , onExportExcelClick: function () {
        if (this.journal == "FINAL") {
            this.getEl().mask(eBook.Journal.Window_ExcelExporting, 'x-mask-loading');
            eBook.CachedAjax.request({
                url: eBook.Service.url + 'ExportFinalTrial'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.Culture
                }
                })
                , callback: this.onExportExcelFinalComplete
                , scope: this
            });
        }
    }
    , onExportExcelFinalComplete: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open("pickup/" + robj.ExportFinalTrialResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onToggleExportProaccClick: function (btn, state) {
        this.grid.setExportModus(state);
    }
    , onExportProaccClick: function () {
        var bids = this.grid.getChecked();
        if (bids.length == 0) {
            Ext.Msg.alert('Export ProAcc', 'No bookings selected');
        } else {
            this.getEl().mask('Creating export file for ProAcc', 'x-mask-loading');
            eBook.CachedAjax.request({
                url: eBook.Service.output + 'ExportExactBookings'
                , method: 'POST'
                , params: Ext.encode({ cebdc: {
                    FileId: eBook.Interface.currentFile.get('Id')
                        , BookingIds: bids
                        , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onExportProaccCallback
                , scope: this
            });
        }
    }
    , onExportProaccCallback: function (opts, success, resp) {
        this.getEl().unmask();
        var tb = this.getTopToolbar();
        tb.startexportproacc.toggle();
        if (!success) {
            eBook.Interface.showResponseError(resp, this.title);
        } else {
            var robj = Ext.decode(resp.responseText);
            window.open("pickup/" + robj.ExportExactBookingsResult);
        }
        this.grid.store.load();
        //this.grid.setExportModus(false);
    }
    , importDone: function () {
        this.grid.store.load();
    }
    , hasChanges: false
    , onBeforeClose: function () {
        if (this.hasChanges) eBook.Interface.center.fileMenu.updateWorksheets();
    }
});
if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function(str) {
        return this.indexOf(str) == 0;
    };
}

eBook.Journal.Booking.AccountDropdown = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: 'GetAccountsList'
                , baseParams: {
                    FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                }
                , serviceUrl: eBook.Service.schema
                , criteriaParameter: 'cadc'
                , autoDestroy: true
                , fields: eBook.data.RecordTypes.GlobalListItem
                 , enableFileCached: true
            })
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            ,minChars:1
            , hideTrigger: true
            , typeAhead: false
            , lazyRender: true
            , pageSize: 20
            , listWidth: 300
            ,queryParam:'Query'
            , selectOnFocus: true
        });
        eBook.Journal.Booking.AccountDropdown.superclass.initComponent.apply(this, arguments);
    }
 
});

eBook.Journal.Booking.AccountEditor = Ext.extend(Ext.grid.GridEditor, {
    startEdit: function(el, value) {
        eBook.Journal.Booking.AccountEditor.superclass.startEdit.apply(this, arguments);
        if (!this.record) return;
        this.field.setActiveRecord(value);
        //        this.field.setValue(this.record.get('Account').id);
        //        this.field.setDisplayValue(this.record.get('Account')[eBook.Interface.Culture.substr(0, 2)]);
    }
    , completeEdit: function(remainVisible) {
        // Find a way to load the account description (and full account visiblenr)
        // in the background. (preferably using window status).
        eBook.Journal.Booking.AccountEditor.superclass.completeEdit.apply(this, arguments);
        var raw = this.field.getValue();
        var rec = this.field.getRecord();

        var getRec = rec && !Ext.isEmpty(raw) && rec.data.id.startsWith(raw);

        if (getRec) {
            this.record.set('Account', rec.data);
        } else {
            if (raw == 'undefined') return;
            this.record.set('Account', raw);
            this.getAccountObject(raw, this.record);
        }

    }
    , getAccountObject: function(val, rec) {
        eBook.CachedAjax.request({
            url: eBook.Service.schema + 'GetAccountListItem'
                    , method: 'POST'
                    , params: Ext.encode({ cadc: { FileId: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.Culture, InternalNr: val} })
                    , callback: this.onAccountObjectCallback
                    , scope: this
                    , record: rec
        });
    }
    , onAccountObjectCallback: function(opts, success, resp) {
        if (success) {
            var obj = Ext.decode(resp.responseText);
            obj = obj.GetAccountListItemResult;
            opts.record.set('Account', obj);
        }
    }
});

eBook.Journal.Booking.AccountEditorColumn = Ext.extend(Ext.grid.Column, {

    constructor: function(cfg) {
        eBook.Journal.Booking.AccountEditorColumn.superclass.constructor.call(this, cfg);
        //this.renderer =
    }
    ,getCellEditor: function(rowIndex){
        var ed = this.getEditor(rowIndex);
        if(ed){
            if(!ed.startEdit){
                if(!ed.gridEditor){
                    ed.gridEditor = new eBook.Journal.Booking.AccountEditor(ed);
                }
                ed = ed.gridEditor;
            }
        }
        return ed;
    }
});
Ext.grid.Column.types.accountEditorColumn = eBook.Journal.Booking.AccountEditorColumn

eBook.Journal.Booking.Panel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            items: [{
                        xtype:'hidden',
                        name:'bookingId',
                        ref:'bookingId'
                    },
                    { 
                        xtype:'numberfield'
                        ,hidden:true
                        ,name:'bookingNr'
                        ,ref:'bookingNr'
                    },
                    {
                        xtype:'textfield'
                        , fieldLabel: eBook.Journal.Booking.Panel_Desc
                        ,name:'bookingDescription'
                        , ref: 'bookingDescription'
                        ,width:350
                    },
                    {   
                        xtype:'checkbox'
                        ,fieldLabel:eBook.Journal.Booking.Panel_HideClients //'Hide booking for clients'
                        ,name:'bookingHidden'
                        ,ref:'bookingHidden'
                    }]
            ,bodyStyle:'padding:5px'
            , layout: 'form'
            , layoutConfig: { labelWidth: 260 }
        });
        eBook.Journal.Booking.Panel.superclass.initComponent.apply(this, arguments);
    }
    , loadBooking: function(rec) {
        this.bookingId.setValue(rec.get('BookingId'));
        this.bookingNr.setValue(rec.get('Nr'));
        this.bookingDescription.setValue(rec.get('BookingDescription'));
        this.bookingHidden.setValue(rec.get('Hidden'));
    }
    , getData: function() {
        return {
            bi: this.bookingId.getValue() == "" ? null : this.bookingId.getValue()
            , bid: this.bookingId.getValue() == "" ? null : this.bookingId.getValue()
            , bnr: this.bookingNr.getValue() == "" ? null : this.bookingNr.getValue()
            , bd: this.bookingDescription.getValue()
            , bh: this.bookingHidden.getValue()
            , bwt: null
            , bwk: null
            , bjt:this.refOwner.caller.journal == 'LASTYEAR' ? -1 : 0
        };
    }
});

eBook.Journal.Booking.Grid = Ext.extend(Ext.grid.EditorGridPanel, {
    emptyRows: 20
    , initComponent: function () {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        this.actions = new Ext.ux.grid.RowActions({
            header: 'Actions'
                , align: 'left'
                , widthSlope: 30
                , keepSelection: true
                , actions: fileClosed ? [] : [{ iconCls: 'eBook-grid-row-delete-ico', tooltip: eBook.Journal.Booking.Grid_DeleteLineTip}]
        });

        this.actions.on({ action: this.onAction });

        Ext.apply(this, {
            store: new eBook.data.BaseJsonStore({
                root: 'data'
                , fields: eBook.data.RecordTypes.BookingLines
                , autoDestroy: true
                , autoSave: false

            })
            , plugins: [new Ext.ux.grid.GridSummary(), this.actions]
            , columnLines: true
            , columns: [{
                xtype: 'accountEditorColumn',
                header: eBook.Journal.Booking.Grid_AccountNr,
                dataIndex: 'Account',
                align: 'left',
                summaryType: 'count',
                editor: new eBook.Journal.Booking.AccountDropdown(),
                summaryRenderer: function (v, params, data) {
                    return ((v === 0 || v > 1) ? '(' + v + ' Lines)' : '(1 Line)');
                }
                , width: 300
                , renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    var acc = record.get('Account');
                    if (Ext.isObject(acc)) return acc[eBook.Interface.Culture.substr(0, 2)];
                    return acc;
                }
                , hideable: false
                , sortable: true
                , editable: !fileClosed
            }, {
                header: eBook.Journal.Booking.Grid_Debet,
                width: 140,
                dataIndex: 'Debet',
                renderer: Ext.util.Format.euroMoneyJournal,
                summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
                , editable: !fileClosed
                , editor: new Ext.form.NumberField({
                    allowBlank: true,
                    allowNegative: false,
                    allowDecimals: true,
                    decimalPrecision: 2 //,
                    //decimalSeparator: ''
                })
            }, {
                header: eBook.Journal.Booking.Grid_Credit,
                width: 140,
                dataIndex: 'Credit',
                renderer: Ext.util.Format.euroMoneyJournal,
                summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
                , editable: !fileClosed
                , editor: new Ext.form.NumberField({
                    allowBlank: true,
                    allowNegative: false,
                    allowDecimals: true,
                    decimalPrecision: 2//,
                    //decimalSeparator: ','
                })
            }, {
                header: eBook.Journal.Booking.Grid_BusinessRelations,
                width: 140,
                dataIndex: 'ClientSupplierName',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {


                    var re = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
                    
                    var match = re.exec(value);

                    
                    if (match) {
                        var data = this.editor.store.getAt(this.editor.store.find('Id', value)).data;
                        record.set('ClientSupplierId',data.Id);
                        record.set('ClientSupplierName', data.Name);
                        return data.Name;
                    } else {
                        return value;
                    }

                },
                //summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
                , editable: !fileClosed
                , editor: new Ext.form.ComboBox({
                    fieldLabel: eBook.Create.Empty.PreviousFileFieldSet_EbookFile
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , valueField: 'Id'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    //, mode: 'remote'
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetBusinessRelationList'
                            , serviceUrl: eBook.Service.businessrelation
                            , autoLoad: true
                            //, autoDestroy: true
                            , criteriaParameter: 'citdc'
                           , baseParams: {
                               Id: eBook.Interface.currentClient.id
                                , Type: null
                           }
                            , fields: eBook.data.RecordTypes.BusinessRelationList
                           
                        })
                    
                })

            }, {
                header: eBook.Journal.Booking.Grid_Comment,
                width: 300,
                dataIndex: 'Comment',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    metaData.attr = 'ext:qtip="' + value + '"';
                    return value;
                },
                //summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                //, sortable: true
                , editable: !fileClosed
                , editor: new Ext.form.TextField({
                    allowBlank: true
                    //allowNegative: false,
                    //allowDecimals: true,
                    //decimalPrecision: 2 //,
                    //decimalSeparator: ''
                })
            }, this.actions]
        });
        eBook.Journal.Booking.Grid.superclass.initComponent.apply(this, arguments);
    }
    , setEmptyRows: function () {
        var recs = [];
        for (var i = 0; i < this.emptyRows; i++) {
            recs[i] = new this.store.recordType();
        }
        this.store.add(recs);

    }
    , onAction: function (g, r, a, ri, ci) {
        g.store.removeAt(ri);
    }

});

eBook.Journal.Booking.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        Ext.apply(this, {
            items: [new eBook.Journal.Booking.Panel({ region: 'north', height: 80, ref: 'panel' })
                , new eBook.Journal.Booking.Grid({ region: 'center', ref: 'grid' })]
            , layout: 'border'
        });

        if (!fileClosed) {
            Ext.apply(this, {
                tbar: new Ext.Toolbar({
                    items: [{
                        ref: 'save',
                        text: eBook.Journal.Booking.Window_Save,
                        iconCls: 'eBook-businessrelation-add-ico',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onSaveClick,
                        width: 70,
                        scope: this
                    }, {
                        ref: 'addlines',
                        text: eBook.Journal.Booking.Window_AddLines,
                        iconCls: 'eBook-icon-addlines-24 ',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onAddLinesClick,
                        width: 70,
                        scope: this
                    }, {
                        ref: 'importexcel',
                        text: eBook.Journal.Booking.Window_ImportExcel,
                        iconCls: 'eBook-icon-importexcel-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onImportExcelClick,
                        width: 70,
                        scope: this,
                        hidden: true
}]
                    })
                });
            }
            eBook.Journal.Booking.Window.superclass.initComponent.apply(this, arguments);

            this.on('beforeClose', this.onBeforeCloseWindow, this);
        }
    , show: function(caller, recs) {
        this.caller = caller;
        eBook.Journal.Booking.Window.superclass.show.call(this, arguments);
        if (Ext.isDefined(recs)) {
            this.grid.store.removeAll();
            this.grid.store.add(recs);
            this.panel.loadBooking(recs[0]);
        } else {
            this.grid.store.removeAll();
            this.grid.setEmptyRows();

        }
    }
    , action: 'SaveBooking'
    , onSaveClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.caller.refOwner.hasChanges = true;
        var booking = this.panel.getData();
        var bookingLines = this.grid.store.getJsonRecords(booking, function(rec) {
            return rec.get('Account') != null && rec.get('Account').id != null && !Ext.isEmpty(rec.get('Account').id);
        });
        this.getEl().mask(String.format(eBook.Journal.Booking.Window_Saving, bookingLines[0].bd), 'x-mask-loading');

        var d = Math.abs(Math.round(this.grid.store.sum('Debet') * 100) / 100);
        var c = Math.abs(Math.round(this.grid.store.sum('Credit') * 100) / 100);

        if (d != c) { //'Debet {0} is not equal to credit {1}'
            alert(String.format(eBook.Journal.Booking.Window_DebetCredit, d, c));
            this.getEl().unmask();
            return;
        }

        this.activeId = bookingLines[0].bid;

        eBook.CachedAjax.request({
            url: eBook.Service.schema + 'SaveBooking'
                        , method: 'POST'
                        , params: Ext.encode({
                            csbdc: {
                        FileId: eBook.Interface.currentFile.get('Id')
                            , Lines: bookingLines
                            , Person: eBook.User.getActivePersonDataContract()
                    }
                })
                , callback: this.onSaveBookingResponse
                , scope: this
        });

    }
    , onSaveBookingResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                var rc = robj[this.action + 'Result'];
                this.activeId = rc.id;
                this.caller.store.addListener("load", this.onReloaded, this, { single: true });
                this.caller.store.reload();

            } else {
                eBook.Interface.showResponseError(resp, this.title);
                this.getEl().unmask();
            }
        } catch (e) {
            console.log(e);
            this.getEl().unmask();
        }

    }
    , onReloaded: function() {
        var cl = this.caller.store.query("BookingId", this.activeId);
        this.grid.store.removeAll();
        this.grid.store.add(cl.items);
        this.panel.loadBooking(cl.first());
        this.getEl().unmask();
    }
    , onImportExcelClick: function() {
        // alert('import a booking from excel, using excel mapping tool = TODO');
    }
    , onAddLinesClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.grid.setEmptyRows();
    }
    , onBeforeCloseWindow: function() {
        // add check save changes?
        if (this.caller && this.caller.store) this.caller.store.reload();
        return true;
    }
    });eBook.Bundle.NewWindow = Ext.extend(eBook.Window, {
    initComponent: function () {
        var me = this,
            selectAction = 'GetGenericBundleTemplates',
            criteriaParameter = 'ccdc';

        //If serviceId is defined, get service linked bundle templates only
        if (me.serviceId) {
            selectAction = 'GetServiceBundleTemplates';
            criteriaParameter = 'cscdc';
        }

        Ext.apply(this, {
            layout: 'form'
            , labelWidth: 200
            , width: 450
            , height: 250
            , modal: true
            , maximizable: false
            , bodyStyle: 'padding:10px;'
            , resizable: false
            , items: [
                /* {
                 xtype: 'checkbox'
                 , ref: 'includePrevious'
                 , boxLabel: eBook.Bundle.NewWindow_IncludePrevious
                 , listeners: {
                 'check': {
                 fn: this.loadLayoutType
                 , scope: this
                 }
                 }
                 , hidden: true
                 }
                 , */{
                    xtype: 'languagebox'
                    , ref: 'culture'
                    , fieldLabel: eBook.Bundle.NewWindow_Language
                    , allowBlank: false
                    , autoSelect: true
                    , anchor: '90%'
                    , value: eBook.Interface.Culture
                    , listeners: {
                        'select': {
                            fn: this.loadLayoutType
                            , scope: this
                        }
                    }
                }, {
                    xtype: 'combo'
                    , ref: 'layouttype'
                    , fieldLabel: eBook.Bundle.NewWindow_Layout
                    , name: 'layout'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Id'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: new eBook.data.JsonStore({
                        selectAction: selectAction
                        , serviceUrl: eBook.Service.bundle
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: criteriaParameter
                        , listeners: {
                            'beforeload': {
                                fn: this.updateLayoutType,
                                scope: this
                            },
                            'load':{
                                fn: this.continueLoadIfPossible,
                                scope: this
                            }
                        }
                        , fields: eBook.data.RecordTypes.FileReportList
                    })
                    , listeners: {
                        'select': {
                            fn: this.setName
                            , scope: this
                        }
                    }
                    , anchor: '90%'
                }, {
                    xtype: 'textfield',
                    ref: 'reportname',
                    name: 'Name',
                    allowBlank: false,
                    value: me.serviceId == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B' ? 'Annual Accounts' : '',
                    fieldLabel: me.serviceId ? eBook.Bundle.NewWindow_Deliverable_Name : eBook.Bundle.NewWindow_Bundle_Name,
                    anchor: '90%'
                }]
            , tbar: [{
                ref: 'createreport',
                text: me.serviceId ? eBook.Bundle.NewWindow_Deliverable_Create : eBook.Bundle.NewWindow_Bundle_Create,
                iconCls: 'eBook-bundle-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onCreateReportClick,
                scope: this
            }],
            listeners: {
                afterrender: function () {
                    //remove mask if there is one
                    if (me.maskedCmp) {
                        me.maskedCmp.getEl().unmask();
                    }
                    //name is set for services
                    if (me.serviceId) {
                        me.reportname.hide();
                    }
                    //language is hidden for Annual Accounts
                    if (me.serviceId == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') {
                        me.culture.hide();
                    }
                }
            }
        });
        eBook.Bundle.NewWindow.superclass.initComponent.apply(this, arguments);
    },
    ///skip user interaction when only one type of layout is available and other fields are hidden
    continueLoadIfPossible: function() {
        var me = this;

        if(me.culture.hidden && me.reportname.hidden && me.layouttype.store.getCount() == 1)
        {
            var record = me.layouttype.store.getAt(0);
            me.layouttype.setValue(record.get("Id"));
            me.createReport();
        }
    },
    onCreateReportClick: function () {
        var me = this;
        me.createReport();
    },
    createReport: function() {
        var me = this;

        //validation
        if (!me.reportname.isValid()) {return;}
        if (!me.layouttype.isValid()) {return;}
        if (!me.culture.isValid()) {return;}

        //vars
        var fileId = eBook.Interface.currentFile.get('Id'),
            templateId = me.layouttype.getValue(),
            culture = me.culture.getValue(),
            name = me.reportname.getValue(),
            service = 'CreateBundle',
            returnObject = 'CreateBundleResult',
            params = {},
            dc =  'ccbdc',
            dcParams = {
                FileId: fileId,
                TemplateId: templateId,
                Culture: culture,
                Name: name
            };

        if (me.serviceId == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') //Annual Accounts
        {
            //Service verifies and creates a custom bundle out of 5 pdf's
            service = 'CreateAnnualAccountsDeliverable';
            dc = 'ccbpdc';
            dcParams.Person = eBook.User.getActivePersonDataContract();
            returnObject = 'CreateAnnualAccountsDeliverableResult';
        }

        //Create params object
        params[dc] = dcParams;

        //mask
        me.getEl().mask(eBook.Bundle.NewWindow_Creating, 'x-mask-loading');

        //ajax call
        eBook.CachedAjax.request({
            url: eBook.Service.bundle + service
            , method: 'POST'
            , params: Ext.encode(params)
            , callback: this.onCreatedReportCallback
            , name: name
            , scope: this
            , returnObject: returnObject
        });
    }
    , onCreatedReportCallback: function (opts, success, resp) {
        var me = this,
            returnObject = null,
            robj = null;

        if (opts.returnObject) returnObject = opts.returnObject;
        if (resp && resp.responseText) {
            robj = Ext.decode(resp.responseText);
        }

        me.getEl().unmask();
        if (success) {
            //Get the bundle result object, depending on what function you called
            if (robj && robj[returnObject]) {
                var bundle = robj[returnObject];
                //Once the bundle is created, retrieve the file service status and open edit window
                if (me.readyCallback) {
                    me.readyCallback.fn.call(me.readyCallback.scope, bundle, me.serviceId);
                }
                else {
                    var wn = new eBook.Bundle.EditWindow({bundle: bundle, serviceId: me.serviceId});
                    wn.show();
                }
            }
        } else {
            if (robj) {

                Ext.Msg.show({
                    title: 'Could not create ' + opts.name ? opts.name : '' + ' bundle',
                    msg: robj.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 400
                });
            }
            else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        }

        me.close();
    }
    , show: function (parent) {
        this.callingParent = parent;
        eBook.Bundle.NewWindow.superclass.show.call(this);
        var pfid = eBook.Interface.currentFile.get('PreviousFileId');
        //if (!Ext.isEmpty(pfid)) this.includePrevious.show();
    }
    , loadLayoutType: function () {
        this.layouttype.clearValue();
        this.layouttype.store.load();
    }
    , updateLayoutType: function () {
        var me = this,
            cult = me.culture.getValue() != '' ? me.culture.getValue() : eBook.Interface.Culture;
        baseParams = {Culture: cult};

        //var ipf = this.includePrevious.isVisible() ? this.includePrevious.getValue() : false;
        //this.layouttype.store.baseParams = { FileId: eBook.Interface.currentFile.get('Id'), Culture: cult, IncludePrevious: ipf };

        if (me.serviceId) {
            baseParams = {ServiceId: me.serviceId, Culture: cult};
        }

        me.layouttype.store.baseParams = baseParams;
    }
    , setName: function (c, r, idx) {
        if (r) {
            if (r.get("Id") == "dd3b9dc3-e3ad-4c10-800c-069b627fca52") {
                Ext.Msg.alert('Creating a year end deliverable?', 'Please use the "Year-end Deliverables" menu, when creating a year end deliverable, in order to start the process.');
            }

            this.reportname.setValue(r.get('Name'));
        }
    }
});


eBook.Bundle.AsyncNode = Ext.extend(Ext.tree.AsyncTreeNode, {
    constructor: function (config, closed) {
        var cfg = {
            properties: config.properties ? config.properties : config
        };
        cfg.allowChildren = false;
        cfg.ui = eBook.Bundle.NodeUI;
        cfg.allowDrag = true;
        cfg.text = config.title ? config.title : config.text;
        cfg.leaf = true;
        config.type = config.properties ? config.properties.type : config.type;

        if (!Ext.isDefined(config.__type)) config.__type = config.type ? config.type : "ROOT";
        switch (config.__type) {
            case 'ROOT':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = true;
                cfg.allowDrop = true;
                cfg.allowDrag = false;
                cfg.iconCls = 'eBook-bundle-tree-bundle ';
                if (closed) cfg.iconCls += 'ebook-bundle-tree-closed ';
                cfg.leaf = false;
                cfg.readOnly = false;
                cfg.children = config.properties ? config.children : config.Index.items;
                cfg.expanded = true;
                cfg.properties.type = 'ROOT';
                break;
            case 'LIBRARY':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = false;
                cfg.allowDrop = false;
                cfg.allowDrag = false;
                cfg.iconCls = '';
                cfg.leaf = false;
                cfg.readOnly = true;
                cfg.children = config.folders;
                cfg.properties.type = 'LIBRARY';
                break;

            case 'FOLDER':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = false;
                cfg.allowDrop = false;
                cfg.allowDrag = false;
                cfg.icon = '';
                cfg.leaf = false;
                cfg.readOnly = true;
                cfg.children = config.items;
                cfg.properties.type = 'FOLDER';
                break;
            case 'ChapterDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'CHAPTER':
                cfg.allowChildren = true;
                cfg.isTarget = true;
                cfg.allowDrop = true;
                cfg.iconCls = 'eBook-bundle-tree-chapter';
                cfg.leaf = false;
                cfg.children = config.properties ? config.children : config.items;
                cfg.properties.type = 'CHAPTER';
                //'ChapterDataContract:#EY.com.eBook.API.Contracts.Data':
                break;
            case 'CoverpageDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'COVERPAGE':
                //'CoverpageDataContract:#EY.com.eBook.API.Contracts.Data':
                cfg.iconCls = 'eBook-bundle-tree-cover';
                cfg.properties.type = 'COVERPAGE';
                break;
            case 'IndexPageDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'INDEXPAGE':
                cfg.iconCls = 'eBook-bundle-tree-index';
                cfg.properties.type = 'INDEXPAGE';
                break;
            case 'PDFDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'PDFPAGE':
                cfg.iconCls = 'eBook-bundle-tree-pdf';
                cfg.properties.type = 'PDFPAGE';
                cfg.comment = cfg.properties.title;
                break;
            case 'DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'DOCUMENT':
                cfg.iconCls = 'eBook-bundle-tree-document';
                cfg.properties.type = 'DOCUMENT';
                if (cfg.properties.DocumentId == eBook.EmptyGuid) {
                    cfg.cls = 'eBook-bundle-tree-missing';
                }
                break;
            case 'FicheItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'FICHE':
                cfg.iconCls = 'eBook-bundle-tree-fiche';
                cfg.properties.type = 'FICHE';
                break;
            case 'StatementsDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'STATEMENT':
                cfg.iconCls = 'eBook-bundle-tree-statement';
                cfg.properties.type = 'STATEMENT';
                break;
            case 'WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'WORKSHEET':
                cfg.iconCls = 'eBook-bundle-tree-worksheet';
                cfg.properties.type = 'WORKSHEET';
                break;
        }
        if (cfg.properties.locked == true) {
            cfg.readOnly = true;
            cfg.allowDrag = false;
        }
        cfg.properties.items = null;
        cfg.properties.Index = null;
        delete cfg.properties.items;
        delete cfg.properties.Index;
        eBook.Bundle.AsyncNode.superclass.constructor.call(this, cfg);
    }
     , setProperties: function (props) {
         if (this.attributes.properties == null) this.attributes.properties = {};
         Ext.apply(this.attributes.properties, props);
         if (props.type != "ROOT") this.setText(props.title);
     }
     , applyProperties: function (props, deep) {
         if (!(this.attributes.readOnly == true)) Ext.apply(this.attributes.properties, props);
         if (deep) {
             for (var i = 0; i < this.childNodes.length; i++) {
                 this.childNodes[i].applyProperties(props, deep);
             }
         }
     }
     , getReportObject: function () {
         var props = this.attributes.properties;
         delete props.loader;
         delete props.baseParams;
         delete props.preloadChildren;
         delete props.events;
         switch (props.type) {
             case "ROOT":
                 delete props.__type;
                 delete props.title;
                 props.Index = { items: this.getChildrenObjects() };
                 break;
             case "CHAPTER":
                 props.items = this.getChildrenObjects();
                 break;
         }
         return props;
     }
     , getChildrenObjects: function () {
         var arr = [];
         for (var i = 0; i < this.childNodes.length; i++) {
             arr.push(this.childNodes[i].getReportObject());
         }
         return arr;
     }
});

Ext.tree.TreePanel.nodeTypes.bundleAsyncNode = eBook.Bundle.AsyncNode;


eBook.Bundle.DropLibraryNode = function(config) {
    Ext.apply(this, config);
    eBook.Bundle.DropLibraryNode.superclass.constructor.call(this);
};
Ext.extend(eBook.Bundle.DropLibraryNode, Ext.util.Observable, {
    enabled: true,
    copyFromComponents: null,
    acceptFromComponents: null,
    acceptFromSelf: true,
    copyLeafs: true,
    copyBranches: true,
    preventCopyFromSelf: true,
    copyInSameTreeChangeId: true,
    copyToDifferentTreeChangeId: true,
    fullBranchCopy: false,
    nodeFilter: null,
    attributeFilter: null,
    attributeMapping: null,
    init: function(parent) {
        this.parent = parent;
        this.parent.copyDropNode = this;
        this.parent.on('beforenodedrop', this.onBeforeNodeDrop, this);
        this.parent.addEvents(
               'beforecopydropnode',
            'aftercopydropnode'
        );
    },
    //@private
    onBeforeNodeDrop: function(e) {
        var finishDrop = this.parent.fireEvent("beforecopydropnode", this, e);

        var enabled = this.enabled;
        var copyFromComponents = (typeof (this.copyFromComponents) == 'string') ? [this.copyFromComponents] : this.copyFromComponents;
        var acceptFromComponents = (typeof (this.acceptFromComponents) == 'string') ? [this.acceptFromComponents] : this.acceptFromComponents;

        var copyLeafs = this.copyLeafs;
        var copyBranches = this.copyBranches;
        var preventCopyFromSelf = this.preventCopyFromSelf;
        var acceptFromSelf = this.acceptFromSelf;

        var node = e.dropNode;
        var isLeaf = (typeof (node.attributes.leaf) != 'undefined' && node.attributes.leaf);
        var fromTreeId = e.source.tree.id;
        var toTreeId = e.tree.id;
        var fromSelf = (fromTreeId == toTreeId);

        var doCopy = e.doCopy;
        doCopy = (typeof (doCopy) == 'undefined') ? node.doCopy : doCopy;

        var newNode;

        finishDrop = ((Ext.isArray(acceptFromComponents) && this.inArray(acceptFromComponents, fromTreeId)) || !acceptFromComponents || (fromSelf && acceptFromSelf)) ? finishDrop : false;

        if (finishDrop) {
            if (enabled) {
                if (typeof (doCopy) == 'undefined') {
                    doCopy = ((Ext.isArray(copyFromComponents) && this.inArray(copyFromComponents, fromTreeId)) || !copyFromComponents);
                    doCopy = ((isLeaf && copyLeafs) || (!isLeaf && copyBranches)) ? doCopy : false;
                    doCopy = (!preventCopyFromSelf || !fromSelf) ? doCopy : false;
                }
                //newNode = this.copyDropNode(e, node);
                newNode = (doCopy) ? this.copyDropNode(e, node) : null;
                this.parent.fireEvent("aftercopydropnode", this, doCopy, newNode, e);
            }
            return true;
        } else {
            return false;
        }
    },
    nodeType: 'async',
    //@private
    copyDropNode: function(e, node) {
        //We make a new node based on the attributes of the node that was going to be dropped and then we swap it over the old node that was on the event in order to cause the new node to be added rather than the old node to be moved. Then the rest of the standard drag and drop functionality will proceed as normal.
        var fromSelf = (node.ownerTree.id == e.tree.id), attributes;
        if (node.attributes.IsRepository) {
            var repItem = node.attributes.Item;
            attributes = {
                __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'
                , Type: "PDFPAGE"
                , ItemId: repItem.Id
                , id: eBook.NewGuid()
                , title: repItem.FileName
                , indexed: true
            };
        } else {
            attributes = node.attributes.indexItem
        }
        if (fromSelf) attributes = node.attributes;

        var newNode = new Ext.tree.TreePanel.nodeTypes['bundleAsyncNode'](attributes);

        if ((fromSelf && this.copyInSameTreeChangeId) || (!fromSelf && this.copyToDifferentTreeChangeId)) {
            var dummyNode = new Ext.tree.TreePanel.nodeTypes[this.nodeType]({});
            newNode.setId(dummyNode.id);
        }
        e.dropStatus = true;
        e.dropNode = newNode;
        return newNode;
    },
    //@private
    inArray: function(array, value, caseSensitive) {
        var i;
        for (i = 0; i < array.length; i++) {
            // use === to check for Matches. ie., identical (===),
            if (caseSensitive) { //performs match even the string is case sensitive
                if (array[i].toLowerCase() == value.toLowerCase()) {
                    return true;
                }
            } else {
                if (array[i] == value) {
                    return true;
                }
            }
        }
        return false;
    }
});
Ext.preg('bundleDropLibraryNode', eBook.Bundle.DropLibraryNode);

eBook.Bundle.TreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    createNode: function(attr) {
        // apply baseAttrs, nice idea Corey!
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        if (Ext.isString(attr.uiProvider)) {
            attr.uiProvider = this.uiProviders[attr.uiProvider] || eval(attr.uiProvider);
        }
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                        new eBook.Bundle.Node(attr) :
                        new eBook.Bundle.AsyncNode(attr);
        }
    }
});

eBook.Bundle.Editor = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        var root = new eBook.Bundle.AsyncNode({
            text: 'empty',
            expanded: true,
            type: 'ROOT',
            Index: { items: [] }
        });
        if (this.bundle) {
            root = new eBook.Bundle.AsyncNode({ text: this.bundle.Name,
                expanded: true,
                type: 'ROOT',
                Index: { items: this.bundle.Contents }
            });
        }
        Ext.apply(this, {
            title: eBook.Bundle.BundleTree_Title,
            autoScroll: true,
            animate: true, // replace with eBook config
            enableDD: !eBook.Interface.isFileClosed(),
            containerScroll: true,
            loader: new eBook.Bundle.TreeLoader({ preloadChildren: true }),
            ddGroup: 'eBook.Bundle',
            //dragConfig:
            //dropConfig
            //enableDrag, //drag only
            root: root,
            lines: true,
            plugins: [
	            { ptype: 'bundleDropLibraryNode', nodeType: 'bundleAsyncNode', copyToDifferentTreeChangeId: false }
	        ],
            keys: [
	            {
	                key: Ext.EventObject.DELETE
	                , fn: this.onDeleteItem
	                , scope: this
	            }
	        ],
            tbar: [{
                ref: 'newChapter',
                text: eBook.Bundle.Window_NewChapter,
                iconCls: 'eBook-icon-addchapter-16',
                scale: 'small',
                iconAlign: 'left',
                handler: this.onAddChapterClick,
                scope: this
                , disabled: eBook.Interface.isFileClosed()
            }, {
                ref: 'deleteItem',
                text: eBook.Bundle.Window_DeleteItem,
                iconCls: 'eBook-icon-deleteitem-16',
                scale: 'small',
                iconAlign: 'left',
                disabled: true,
                handler: this.onDeleteItem,
                scope: this
                , disabled: eBook.Interface.isFileClosed()
}],
                //margins:
                //requestMethod:'POST',
                rootVisible: true
            });
            eBook.Bundle.Editor.superclass.initComponent.apply(this, arguments);
            this.getSelectionModel().on('selectionchange', this.onNodeSelect, this);
            this.on('nodedrop', this.onDropped, this);

        }
    , onNodeSelect: function(sm, node) {
        var tb = this.getTopToolbar();
        tb.deleteItem.disable();
        if (node != null) {
            if (!eBook.Interface.isFileClosed()) {
                tb.deleteItem.enable();
            }
            this.refOwner.loadNodeSettings(node);
        } else {
            this.selectNode(this.getRootNode());
        }
    }
    , onAddChapterClick: function() {
        var props = {
            __type: 'ChapterDataContract:#EY.com.eBook.API.Contracts.Data'
            , type: 'CHAPTER'
            , title: eBook.Bundle.BundleTree_NewChapter
            , items: []
            , FooterConfig: { Enabled: false, ShowFooterNote: false, ShowPageNr: false }
            , HeaderConfig: { Enabled: false, ShowTitle: false }
            , id: eBook.NewGuid()
            , indexed: true
        };
        var n = new eBook.Bundle.AsyncNode(props);
        this.getRootNode().appendChild(n);
        this.selectNode(n);
    }
    , onDropped: function(dde) {
        this.getSelectionModel().select(dde.dropNode);
    }
    , setRootNode: function(node) {
        eBook.Bundle.Editor.superclass.setRootNode.call(this, node);
        this.selectNode.defer(500, this, [node]);
    }
    , selectNode: function(node) {
        this.getSelectionModel().select(node);
    }
    , getReport: function() {
        return this.getRootNode().getReportObject();
    }
    , onDeleteItem: function() {
        var s = this.getSelectionModel().getSelectedNode();
        if (s != null) s.remove(true);
        this.selectNode(this.getRootNode());
    }
    , getContent: function() {
        var cnts = [];
        var r = this.getRootNode().getChildrenObjects();
        return r;
    }
    });

Ext.reg('bundleEditor', eBook.Bundle.Editor);

eBook.Bundle.NodeInfoFieldSet = Ext.extend(Ext.form.FieldSet, {
    onCheckClick: function() {
        eBook.Bundle.NodeInfoFieldSet.superclass.onCheckClick.call(this);
        this.refOwner.onChange();
    }
});
Ext.reg('eBook.Bundle.NodeInfoFieldSet', eBook.Bundle.NodeInfoFieldSet);

eBook.Bundle.ItemSettings = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        var disIt = eBook.Interface.isFileClosed();
        Ext.apply(this, {
              width: 400
            , title: String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, "")
            , labelAlign: 'left'
            , disabled: disIt
            , items: [
                {
                    xtype: 'iconlabelfield',
                    cls: 'eBook-bundle-iconlabelfield ',
                    text: '',
                    ref: 'nodeType'
                    , disabled: disIt
                },
                 {
                     xtype: 'label'
                    , text: eBook.Bundle.NodeInfoPanel_Title
                    , style: 'margin-bottom:2px;font-size:10pt;'
                    , ref: 'titlelabel'
                    , disabled: disIt
                 }, {
                     xtype: 'textfield',
                     hideLabel: true,
                     value: '',
                     name: 'title',
                     ref: 'titleField',
                     listeners: {
                         'change': { fn: this.onChange, scope: this }
                     }, anchor: '100%', style: 'margin-bottom:10px'
                     , disabled: disIt
                 }, {
                     xtype: 'languagebox'
                    , ref: 'culture'
                    , fieldLabel: eBook.Bundle.NodeInfoPanel_Language
                    , allowBlank: false
                    , autoSelect: true
                    , anchor: '90%'
                    , listeners: {
                        'select': { fn: this.onChange, scope: this }
                    }
                    , disabled: disIt
}//ShowInIndex
                , {
                    xtype: 'checkbox',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_Detailed,
                    ref: 'detailed',
                    hidden: true,
                    listeners: {
                        'check': { fn: this.onChange, scope: this }
                    }
                    , disabled: disIt
                }, {
                    xtype: 'checkbox',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_ShowInIndex,
                    ref: 'showindex',
                    hidden: false,
                    listeners: {
                        'check': { fn: this.onChange, scope: this }
                    }
                    , disabled: disIt
                }, {
                    xtype: 'numberfield',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_PageFrom,
                    defaultValue: 1,
                    minValue: 1,
                    maxValue: 1,
                    ref: 'pageFrom',
                    hidden: true,
                    listeners: {
                        'change': { fn: this.onChange, scope: this }
                    }
                    , disabled: disIt
                }, {
                    xtype: 'numberfield',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_PageTo,
                    ref: 'pageTo',
                    defaultValue: 1,
                    minValue: 1,
                    maxValue: 1,
                    hidden: true,
                    listeners: {
                        'change': { fn: this.onChange, scope: this }
                    }
                    , disabled: disIt
                }
                , {
                    xtype: 'eBook.Bundle.NodeInfoFieldSet',
                    title: eBook.Bundle.NodeInfoPanel_Header,
                    checkboxToggle: true,
                    collapsed: true,
                    items: [{
                        xtype: 'checkbox',
                        fieldLabel: eBook.Bundle.NodeInfoPanel_ShowTitle,
                        ref: 'titleField',
                        disabled: disIt,
                        listeners: {
                            'check': { fn: this.onChange, scope: this }
                        }
}],
                        ref: 'headerData'
                        , disabled: disIt
                    }
                , {
                    xtype: 'eBook.Bundle.NodeInfoFieldSet',
                    title: eBook.Bundle.NodeInfoPanel_Footer,
                    checkboxToggle: true,
                    collapsed: false,
                    checked: true,
                    items: [
                            { xtype: 'checkbox',
                                fieldLabel: eBook.Bundle.NodeInfoPanel_ShowPageNr,
                                ref: 'pagenr',
                                disabled: disIt,
                                listeners: {
                                    'check': { fn: this.onChange, scope: this }
                                }
                            }, {
                                xtype: 'checkbox',
                                fieldLabel: eBook.Bundle.NodeInfoPanel_ShowFootNote,
                                ref: 'footnote',
                                disabled: disIt,
                                listeners: {
                                    'check': { fn: this.onChange, scope: this }
                                }
}],
                    ref: 'footerData'
                    , disabled: disIt
                }
                , {
                    xtype: 'button'
                    , text: eBook.Bundle.NodeInfoPanel_ReconfigureChildren
                    , handler: this.onReconfigureChildrenClick
                    , ref: 'resetChildren'
                    , style: 'margin-top:10px;margin-bottom:10px;'
                    , scope: this
                    , disabled: disIt
                }
                ]
        });
        eBook.Bundle.ItemSettings.superclass.initComponent.apply(this, arguments);

    }
    , nodeUpdate: true
    , onChange: function() {
        if (!this.nodeUpdate) return;
        this.currentNode.setProperties(this.getProperties());
    }
    , onReconfigureChildrenClick: function() {
        Ext.Msg.show({
            title: eBook.Bundle.NodeInfoPanel_UpdateChildrenTitle,
            msg: eBook.Bundle.NodeInfoPanel_UpdateChildrenMsg,
            buttons: Ext.Msg.YESNO,
            fn: this.reconfigureChildren,
            scope: this,
            icon: Ext.MessageBox.QUESTION
        });
    }
    , reconfigureChildren: function(btnid) {

        if (btnid == 'yes') {
            var props = this.getProperties();
            for (var i = 0; i < this.currentNode.childNodes.length; i++) {
                this.currentNode.childNodes[i].applyProperties({ HeaderConfig: props.HeaderConfig, FooterConfig: props.FooterConfig }, true);
            }
        }
    }
    , getProperties: function() {
        var hd = this.headerData.checkbox.dom.checked;
        var ft = this.footerData.checkbox.dom.checked;
        var props = {
            type: this.currentNode.attributes.properties.type,
            title: this.titleField.getValue(),
            indexed: this.showindex.getValue(),
            HeaderConfig: {
                Enabled: hd
                , ShowTitle: hd ? this.headerData.titleField.getValue() : false
            },
            FooterConfig: {
                Enabled: ft
                , ShowPageNr: ft ? this.footerData.pagenr.getValue() : false
                , ShowFooterNote: ft ? this.footerData.footnote.getValue() : false
            }
        };
        if (this.culture.isVisible()) props.Culture = this.culture.getValue();
        if (props.type == "STATEMENT") props.Detailed = this.detailed.getValue();
        if (props.type == "PDFPAGE") {
            props.FromPage = this.pageFrom.getValue();
            props.ToPage = this.pageTo.getValue();
            if (props.ToPage < props.FromPage) {
                var t = props.FromPage;
                props.FromPage = props.ToPage;
                props.ToPage = t;
            }
        }
        this.setTitle(String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, props.title));
        return props;
    }
    , loadNode: function(node, closed) {
        this.nodeUpdate = false;
        this.getForm().reset();
        this.currentNode = node;
        if (node == null) { this.getForm().reset(); return }
        if (!node.attributes.properties) {
            this.getForm().reset();
            this.disable();
            return;
        }
        if (!closed) this.enable();
        this.setFields();
        if (node.attributes.readOnly || node.attributes.properties.locked || eBook.Interface.isFileClosed()) this.disable();
        this.nodeUpdate = true;
        if (closed) {
            this.currentNode.attributes.iconCls = "eBook-lock-16";
            this.disable();
        } else {
            this.titleField.focus();
        }
        // load form fields with node data.

        // show/hide conform the node type.
    }
    , setFields: function() {
        //var rsettings = this.refOwner.getReportSettings();
        
        var props = this.currentNode.attributes.properties;
        //this.nodeType.setValue(props.type);
        this.nodeType.update(props.title, this.currentNode.attributes.iconCls);
        if (!Ext.isDefined(props.title)) props.title = this.currentNode.attributes.text;
        this.setTitle(String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, props.title));
        this.titleField.setValue(props.title);
        if (!eBook.Interface.isFileClosed()) {
            this.titleField.enable();
        }

        this.headerData.show();
        this.footerData.show();
        this.showindex.show();

        if (Ext.isDefined(props.Culture)) {
            this.culture.setValue(props.Culture);
            if (!eBook.Interface.isFileClosed()) {
                this.culture.enable();
            }
            this.culture.show();
        } else {
            this.culture.hide();
        }

        if (props.type == "DOCUMENT") {
            this.culture.disable(); //document language is set on creation.
        }

        if (props.type == "ROOT") {
            this.nodeType.setText(eBook.Bundle.NodeInfoPanel_ReportSettings);
            this.resetChildren.show();
            this.titleField.hide();
        } else { this.titleField.show(); this.resetChildren.hide(); }

        if (props.type == "STATEMENT") {
            this.detailed.show();
            this.detailed.setValue(props.Detailed);
        } else {
            this.detailed.hide();
        }


        if (props.type == "PDFPAGE") {
            // this.nodeType.setValue(props.type + ' (' + props.pageCount + ' pages)');
            //this.pageFrom.
            this.pageFrom.setMaxValue(props.Pages);
            this.pageFrom.setValue(Ext.isDefined(props.FromPage) ? props.FromPage : 1);
            this.pageTo.setMaxValue(props.Pages);
            this.pageTo.setValue(Ext.isDefined(props.ToPage) ? props.ToPage : props.Pages);

            this.pageFrom.show();
            this.pageTo.show();
        } else {
            this.pageFrom.hide();
            this.pageTo.hide();
        }

        this.headerData.checkbox.dom.checked = Ext.isDefined(props.HeaderConfig) && props.HeaderConfig != null ? props.HeaderConfig.Enabled : false;
        this.footerData.checkbox.dom.checked = Ext.isDefined(props.FooterConfig) && props.FooterConfig != null ? props.FooterConfig.Enabled : false;

        this.showindex.setValue(Ext.isDefined(props.indexed) ? props.indexed : true);
        if (Ext.isDefined(props.HeaderConfig) && props.HeaderConfig != null) {
            if (props.HeaderConfig.Enabled) this.headerData.titleField.setValue(props.HeaderConfig.ShowTitle);
        }

        if (Ext.isDefined(props.FooterConfig) && props.FooterConfig != null) {
            if (props.FooterConfig.Enabled) this.footerData.pagenr.setValue(props.FooterConfig.ShowPageNr);
            if (props.FooterConfig.Enabled) this.footerData.footnote.setValue(props.FooterConfig.ShowFooterNote);
        }
        this.headerData.onCheckClick();
        this.footerData.onCheckClick();

        if (props.type == "COVERPAGE" || props.type == "INDEXPAGE") {
            this.titleField.hide();
            this.titlelabel.hide();
            this.headerData.hide();
            this.footerData.hide();
            this.showindex.hide();
        } else {
            this.titleField.show();
            this.titlelabel.show();
            this.showindex.show();
        }

    }
});


Ext.reg('bundleItemSettings', eBook.Bundle.ItemSettings);eBook.Bundle.bundleSettings = Ext.extend(Ext.FormPanel, {
    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            title: String.format(eBook.Bundle.NodeInfoPanel_BundleSettingsTitle, ""),
            width: 400,
            labelAlign: 'left',
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Status',
                    ref: '../../fsServiceStatus',
                    html: '<div id="eBook-bundle-toolbar-status" class="eBook-bundle-toolbar-status">' + (this.serviceStatus && this.serviceStatus.s ? this.serviceStatus.s : '') + '</div>'
                },
                {
                    xtype: 'fieldset',
                    title: 'Customer preference',
                    ref: '../../fsCustomerPreference',
                    items: [
                        {
                            xtype: 'radiogroup',
                            ref: 'rgCustomerReceivalPreference',
                            columns: 1,
                            hideLabel: true,
                            items: [
                                {
                                    boxLabel: 'E-mail',
                                    id: 'rCustomerMailPreference',
                                    ref: 'rCustomerMailPreference',
                                    name: 'rgCustomerReceivalPreference',
                                    inputValue: 'mail',
                                    checked: true
                                },
                                {
                                    boxLabel: 'Printed copy',
                                    id: 'rCustomerPrintPreference',
                                    ref: 'rCustomerPrintPreference',
                                    name: 'rgCustomerReceivalPreference',
                                    inputValue: 'print'
                                }
                            ],
                            listeners: {
                                afterrender: function (radiogroup) {
                                    Ext.Ajax.request({
                                        url: eBook.Service.client + 'GetPrintPreference',
                                        method: 'POST',
                                        jsonData: {
                                            cidc: {
                                                'Id' : eBook.CurrentClient
                                            }
                                        },
                                        callback: function (options, success, response) {
                                            if (success && response.responseText) {
                                                if(Ext.decode(response.responseText).GetPrintPreferenceResult)
                                                {
                                                    radiogroup.setValue('print');
                                                }
                                                else
                                                {
                                                    radiogroup.setValue('mail');
                                                }
                                            }
                                            else {
                                                Ext.Msg.alert('Could not retrieve client print preference.', 'Something went wrong');
                                            }
                                        },
                                        scope: me
                                    });
                                },
                                change: function (radiogroup,radio) {
                                    Ext.Ajax.request({
                                        url: eBook.Service.client + 'SetPrintPreference',
                                        method: 'POST',
                                        jsonData: {
                                            ciabdc: {
                                                'Id': eBook.CurrentClient,
                                                'Boolean': radio.inputValue == 'print' ? true : false
                                            }
                                        },
                                        callback: function (options, success, resp) {
                                            if (!success || !resp.responseText) {
                                                Ext.Msg.alert('Could not retrieve client print preference.', 'Something went wrong');
                                            }
                                        },
                                        scope: me
                                    });
                                }
                            }
                        }
                    ]
                }
            ]
        });

        eBook.Bundle.bundleSettings.superclass.initComponent.apply(me, arguments);
    }
});


Ext.reg('bundleSettings', eBook.Bundle.bundleSettings);
eBook.Bundle.LibraryTreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    filter: {
        ps: null,
        pe: null,
        stat: null
    },
    Culture: 'nl-BE',
    selectionMode: false,
    createNode: function(attr) {
        // apply baseAttrs, nice idea Corey!
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        if (Ext.isString(attr.uiProvider)) {
            attr.uiProvider = this.uiProviders[attr.uiProvider] || eval(attr.uiProvider);
        }
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                        new Ext.tree.TreeNode(attr) :
                        new Ext.tree.AsyncTreeNode(attr);
        }
    },
    requestData: function(node, callback, scope) {
        //REPOSITORY
        if (node.attributes.IsRepository || node.id == 'REPOSITORY') {
            this.transId = eBook.CachedAjax.request({
                method: 'POST',
                url: eBook.Service.repository + 'GetChildren',
                success: this.handleResponse,
                failure: this.handleFailure,
                scope: this,
                argument: { callback: callback, node: node, scope: scope },
                params: this.getRepositorySendingParam(node)
            });
        } else {
            this.transId = eBook.CachedAjax.request({
                method: 'POST',
                url: this.dataUrl || this.url,
                success: this.handleResponse,
                failure: this.handleFailure,
                scope: this,
                argument: { callback: callback, node: node, scope: scope },
                params: this.getSendingParam(node)
            });
        }

    },
    processResponse: function(response, node, callback, scope) {
        var json = response.responseText;
        try {

            var o = response.responseData || Ext.decode(json);
            var repos = false;
            if (node.attributes.IsRepository || node.id == 'REPOSITORY') {
                repos = true;
                o = o.GetChildrenResult;
            } else {
                o = o.GetItemsResult;
            }
            node.beginUpdate();
            for (var i = 0, len = o.length; i < len; i++) {
                o[i].IsRepository = repos;
                var n = this.createNode(o[i]);
                if (n) {
                    if (n.attributes.Files == false && this.selectionMode) {
                        if (!Ext.isEmpty(n.attributes.cls) && n.attributes.cls.length > 0) {
                            n.attributes.cls += ' ';
                        } else {
                            n.attributes.cls = '';
                        }
                        n.attributes.cls += "tree-node-unselectable";
                    }

                    node.appendChild(n);
                }
            }
            node.endUpdate();
            this.runCallback(callback, scope || node, [node]);
        } catch (e) {
            this.handleFailure(response);
        }
    },
    getRepositorySendingParam: function(node) {

        var o = { crcdc: {
            cid: eBook.Interface.currentClient.get('Id'),
        cult: eBook.Interface.Culture,
        strucId: node.id == 'REPOSITORY' ? null : node.id,
        key: node.attributes.Key,
        strucTp: node.attributes.StructuralType,
        showFiles: true,
        fileId: eBook.Interface.currentFile ? eBook.Interface.currentFile.get('Id') : null
        }
        };

        Ext.apply(o.crcdc, this.filter);
        var periodParent = null;
        if (node.attributes.StructuralType == "FILE") {

            periodParent = node.findParentBy(function() {
                return this.attributes.PeriodStart && !Ext.isEmpty(this.attributes.PeriodStart);
            }, null);
        }
        if (periodParent) {
            o.crcdc.ps = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodStart);
            o.crcdc.pe = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodEnd);
        }

        return Ext.encode(o);

    },
    getSendingParam: function(node) {
        var o = { cldc: {
            NodeId: node.id,
            Culture: this.Culture,
            FileId: eBook.Interface.currentFile.get('Id')
        }
        };

        /* REPOSITORY
        var o = { crcdc: {
        cid: eBook.Interface.currentClient.get('Id')
        , cult: eBook.Interface.Culture
        , strucId: node.id == 'root' ? null : node.id
        , key: node.attributes.Key
        , strucTp: node.attributes.StructuralType
        , showFiles: this.showFiles
        , fileId: eBook.Interface.currentFile ? eBook.Interface.currentFile.get('Id') : null
        }
        };

        Ext.apply(o.crcdc, this.filter);
        var periodParent = null;
        if (node.attributes.StructuralType == "FILE") {

            periodParent = node.findParentBy(function() {
        return this.attributes.PeriodStart && !Ext.isEmpty(this.attributes.PeriodStart);
        }, null);
        }
        if (periodParent) {
        o.crcdc.ps = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodStart);
        o.crcdc.pe = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodEnd);
        }
        */
        return Ext.encode(o);

    }
});


eBook.Bundle.Library = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        this.standardFilter = {
            ps: eBook.Interface.currentFile.get('StartDate'),
            pe: eBook.Interface.currentFile.get('EndDate'),
            stat: null
        };

        if (this.serviceId) {
            if (this.serviceId.toUpperCase() == "BD7C2EAE-8500-4103-8984-0131E73D07FA") {
                this.rootName = 'rootbiztaxbundle';
            } else if (this.serviceId.toUpperCase() == "60CA9DF9-C549-4A61-A2AD-3FDB1705CF55") {
                this.rootName = 'rootmanualbiztaxbundle';
            }
        }
        Ext.apply(this, {
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDD: false,
            enableDrag: true,
            containerScroll: true,
            title: eBook.Bundle.ObjectsTree_Title,
            //border: false,
            // auto create TreeLoader
            // dataUrl: 'get-nodes.php',
            toolTemplate: new Ext.XTemplate(
                '<tpl if="id=\'add\'">',
                    '<div class="eBook-repository-add-ico-24 eBook-header-tools-btn x-btn x-btn-text">Add...</div>',
                '</tpl>'),
            tools: [{
                id: 'add',
                qtip: 'Add file to repository',
                handler: function(e, toolEl, panel, tc){
                        var wn = new eBook.Repository.SingleUploadWindow({
                                clientId: eBook.CurrentClient,
                                readycallback: {fn: panel.reload, scope: panel}
                            });

                        wn.show(panel);
                }
            }],
            loader: new eBook.Bundle.LibraryTreeLoader({
                dataUrl: eBook.Service.bundle + 'GetItems',
                requestMethod: "POST", filter: this.standardFilter,
                Culture: this.culture
            }),
            ddGroup: 'eBook.Bundle',
            rootVisible: false,
            root: {
                nodeType: 'async',
                text: 'Library',
                draggable: false,
                id: this.rootName ? this.rootName : 'root',
                expanded: true
            }

        });
        eBook.Bundle.Library.superclass.initComponent.apply(this, arguments);
    }
    , reload: function() {
        this.setRootNode({
            nodeType: 'async',
            text: 'Library',
            draggable: false,
            id: 'root',
            expanded: true
        });
    }
});

Ext.reg('bundlelibrary', eBook.Bundle.Library);eBook.Bundle.BundleList = Ext.extend(Ext.list.ListView, {
    initComponent: function () {
        var me = this,
            selectAction = 'GetGenericBundles',
            criteriaParameter = 'cicdc',
            baseParams = {'Id': eBook.Interface.currentFile.get('Id'), 'Culture': eBook.Interface.Culture};

        //If serviceId is defined, get service linked bundles only
        if (me.serviceId) {
            selectAction = 'GetServiceBundles';
            criteriaParameter = 'cfsdc';
            baseParams = {
                'ServiceId': this.serviceId,
                'FileId': eBook.Interface.currentFile.get('Id'),
                'Culture': eBook.Interface.Culture
            };
        }

        Ext.apply(this, {
            loadingText: eBook.Bundle.Window_LoadingMenu,
            hideHeaders: true,
            multiSelect: false,
            singleSelect: true,
            store: new eBook.data.JsonStore({
                selectAction: selectAction,
                serviceUrl: eBook.Service.bundle,
                autoDestroy: true,
                autoLoad: true,
                criteriaParameter: criteriaParameter,
                fields: eBook.data.RecordTypes.FileReportList,
                baseParams: baseParams
            }),
            emptyText: eBook.Bundle.Window_EmptyMenu,
            columns: [{
                header: 'Dossier',
                dataIndex: 'Name'
            }]
        });

        eBook.Bundle.BundleList.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('bundleList', eBook.Bundle.BundleList);

eBook.Bundle.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        var me = this;

        //var fileClosed = eBook.Interface.currentFile.get('Closed');
        var fileClosed = eBook.Interface.isFileClosed();
        Ext.apply(this, {
            title: me.serviceId ? eBook.Bundle.Window_Deliverable_Title : eBook.Bundle.Window_Bundle_Title,
            layout: 'fit',
            width: 405,
            cls: 'eBook-Bundle-Window',
            iconCls: 'eBook-Bundle-icon-16',
            bodyStyle: 'background-color:#FFF;',
            items: [{
                xtype: 'bundleList', ref: 'blst', serviceId: this.serviceId,
                listeners: {
                    'dblclick': {fn: this.onBundleItemDblClick, scope: this},
                    'selectionchange': {fn: this.onBundleSelectionChange, scope: this}
                }
            }],
            tbar: {
                xtype: 'toolbar',
                items: [{
                    xtype: 'buttongroup',
                    ref: 'generalActions',
                    id: this.id + '-toolbar-general',
                    columns: 6,
                    title: eBook.Bundle.Window_GeneralActions,
                    items: [{
                        ref: '../newBundle',
                        text: me.serviceId ? eBook.Bundle.Window_Deliverable_New : eBook.Bundle.Window_Bundle_New,
                        iconCls: 'eBook-icon-addreport-24 ',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onNewBundleClick,
                        scope: this,
                        disabled: fileClosed
                    }, {
                        ref: '../openBundle',
                        text: me.serviceId ? eBook.Bundle.Window_Deliverable_Open : eBook.Bundle.Window_Bundle_Open,
                        iconCls: 'eBook-icon-openreport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onOpenBundleClick,
                        disabled: true,
                        scope: this
                    }, {
                        ref: '../deleteBundle',
                        text: me.serviceId ? eBook.Bundle.Window_Deliverable_Delete : eBook.Bundle.Window_Bundle_Delete,
                        iconCls: 'eBook-icon-deletereport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onDeleteBundleClick,
                        disabled: true,
                        scope: this
                    }]
                }]
            },
            listeners: {
                afterrender: function () {
                    //remove mask if there is one
                    if (me.maskedCmp) {
                        me.maskedCmp.getEl().unmask();
                    }
                }
            }
        });

        eBook.Bundle.Window.superclass.initComponent.apply(this, arguments);
    },
    onBundleItemDblClick: function (dv, i, n, e) {
        var rec = dv.getStore().getAt(i);
        this.getBundle(rec.data);
        // this.onLoadBundleClick(this, rec);
    },
    onBundleSelectionChange: function (dv, nds) {
        var tb = this.getTopToolbar();
        if (nds.length === 0) {
            tb.openBundle.disable();
            tb.deleteBundle.disable();
        } else {
            var recs = dv.getRecords(nds);
            //recs[0]
            tb.openBundle.enable();
            if (eBook.Interface.currentFile && !eBook.Interface.currentFile.get('Closed')) {
                tb.deleteBundle.enable();
            }
        }
    },
    onOpenBundleClick: function () {
        var recs = this.blst.getSelectedRecords();
        this.getBundle(recs[0].data);
    },
    getBundle: function (bundle) {
        //this function is also called from interface.js, hence the if's (quick fix)
        if (this.getEl()) {
            this.getEl().mask('Loading');
        }

        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'GetBundle',
            method: 'POST',
            params: Ext.encode({id: bundle.Id}),
            callback: this.onGetBundleCallBack,
            scope: this
        });
    },
    onGetFileServiceStatus: function (bundle, serviceId) {
        var me = this;

        eBook.CachedAjax.request({
            url: eBook.Service.file + 'GetFileServiceStatus',
            method: 'POST',
            params: Ext.encode({cfsdc: {FileId: bundle.FileId, ServiceId: serviceId, ClientId: eBook.CurrentClient}}),
            callback: me.onGetFileServiceStatusCallback.createDelegate(me, [bundle, serviceId], true),
            scope: me
        });
    },
    onGetBundleCallBack: function (opts, success, resp) {
        var me = this;
        if (success && resp.responseText) {
            var robj = Ext.decode(resp.responseText);
            me.bundle = robj.GetBundleResult;

            if (me.bundle && me.serviceId) {
                //after the bundle has been retrieved, get its file service status
                me.onGetFileServiceStatus(me.bundle, me.serviceId);
            }
            else {
                me.showEditWindow({bundle: me.bundle, serviceId: me.serviceId});
            }
        }
        else {
            eBook.Interface.showResponseError(resp, me.title);
            if (me.maskedCmp) {
                me.maskedCmp.unmask();
            }
            else {
                me.getEl().unmask();
            }
        }
    },
    onGetFileServiceStatusCallback: function (opts, success, resp, bundle, serviceId) {
        var me = this;
        if (success && resp.responseText) {
            var robj = Ext.decode(resp.responseText);
            var serviceStatus = robj.GetFileServiceStatusResult;
            me.showEditWindow({bundle: bundle, serviceStatus: serviceStatus, serviceId: serviceId});
        }
        else {
            eBook.Interface.showResponseError(resp, me.title);
            if (me.maskedCmp) {
                me.maskedCmp.unmask();
            }
        }
    },
    showEditWindow: function (properties) {
        var me = this;

        if (me.maskedCmp) {
            properties.maskedCmp = me.maskedCmp;
        }

        var wn = new eBook.Bundle.EditWindow(properties);
        wn.show();
        me.close();
    },
    onNewBundleClick: function () {
        var me = this;

        if (eBook.Interface.currentFile.get('Closed')){ return;}
        var wn = new eBook.Bundle.NewWindow(
            {
                serviceId: me.serviceId, //serviceId is set in client/newHome as it is button specific
                readyCallback: me.serviceId ? {fn: me.onGetFileServiceStatus, scope: me} : null //only retrieve fileservice if service set
            });

        wn.show(this);
    },
    onDeleteItem: function () {
        if (eBook.Interface.currentFile.get('Closed')){ return;}
        this.bundleContainer.deleteSelected();
    },
    onLoadBundleClickNoRec: function (id, name, culture) {
        //to update
    },
    onLoadBundleClick: function (lv, rec) {
        this.onLoadBundleClickNoRec(rec.get('Id'), rec.get('Name'), rec.get('Culture'));
    },
    onDeleteBundleClick: function () {
        var me = this;
        if (eBook.Interface.currentFile.get('Closed')){ return;}
        var recs = this.blst.getSelectedRecords();
        if (recs.length === 0){ return;}
        var rec = recs[0];
        if (rec) {
            Ext.Msg.show({
                title: me.serviceId ? eBook.Bundle.Window_Deliverable_Delete : eBook.Bundle.Window_Bundle_Delete,
                msg: String.format(eBook.Bundle.Window_DeleteReportMsg, rec.get('Name')),
                buttons: Ext.Msg.YESNO,
                fn: me.onPerformDeleteBundle,
                scope: me,
                icon: Ext.MessageBox.QUESTION,
                rec: rec
            });
        }
    },
    onPerformDeleteBundle: function (btnid, txt, opts) {
        var me = this;

        if (opts.rec && btnid == 'yes') {
            var bundleName = opts.rec.get('Name');
            var bundleId = opts.rec.get('Id');
            //close bundle
            // this.closeAll();
            me.getEl().mask(String.format(eBook.Bundle.Window_DeletingReport, bundleName), 'x-mask-loading');
            eBook.CachedAjax.request({
                url: eBook.Service.bundle + 'DeleteBundle',
                method: 'POST',
                params: Ext.encode({id: bundleId}),
                callback: me.onDeleteBundleCallBack.createDelegate(me, [bundleId], true),
                scope: me
            });
        }
    },
    onDeleteBundleCallBack: function (opts, success, resp, bundleId) {
        var me = this;
        me.getEl().unmask();
        if (success === false) {
            eBook.Interface.showResponseError(resp, this.title);
        } else {
            me.blst.store.load();
            if (me.serviceId){ me.onResetFlow(bundleId);}
        }
        // if (this.loadMenu) this.loadMenu.reload();
    },
    onResetFlow: function (bundleId) {
        var me = this;
        me.getEl().mask("reset flow");
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'ResetServiceFlow',
            method: 'POST',
            jsonData: {
                ufsdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    ServiceId: me.serviceId,
                    BundleId: bundleId,
                    Person: eBook.User.getActivePersonDataContract()
                }
            },
            callback: function (options, success) {
                if (!success) {
                    Ext.Msg.alert('Could not reset service', 'Something went wrong');
                }
                me.getEl().unmask();
            },
            scope: me
        });
    }
});

Ext.reg('eBook.Bundle.Window', eBook.Bundle.Window);


eBook.Bundle.Notifier = Ext.extend(Ext.Panel, {
    initComponent: function () {
        Ext.apply(this, {
            html: 'notifier'
        });
        eBook.Bundle.Notifier.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('bundleStatusNotifier', eBook.Bundle.Notifier);

/*
 eBook.Bundle.ItemSettings = Ext.extend(Ext.Panel, {
 initComponent: function() {
 Ext.apply(this, {
 html: 'Item settings'
 });
 eBook.Bundle.ItemSettings.superclass.initComponent.apply(this, arguments);
 }
 });
 Ext.reg('bundleItemSettings', eBook.Bundle.ItemSettings);

 eBook.Bundle.Editor = Ext.extend(Ext.Panel, {
 initComponent: function() {
 Ext.apply(this, {
 html: 'editor'
 });
 eBook.Bundle.Editor.superclass.initComponent.apply(this, arguments);
 }
 });
 Ext.reg('bundleEditor', eBook.Bundle.Editor);
 */
/*
 eBook.Bundle.Library = Ext.extend(Ext.Panel, {
 initComponent: function() {
 Ext.apply(this, {
 html: 'Library'
 });
 eBook.Bundle.Library.superclass.initComponent.apply(this, arguments);
 }
 });
 Ext.reg('bundlelibrary', eBook.Bundle.Library);
 */


eBook.Bundle.Log = Ext.extend(Ext.Panel,{

    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetFileServiceLog'
                , serviceUrl: eBook.Service.file
                , criteriaParameter: 'cfsdc'
                , fields: eBook.data.RecordTypes.Log
            }),
            tpl = new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="eBook-reports-logEntry">',
                '<div><div id="personName" style="float:left;" class="eBook-reports-header eBook-reports-icon eBook-icon-checklist-24">{personName}</div><div style="float:right;" id="timestamp">{timestamp:date("d/m/Y H:i")}</div></div>',
                '<table style="clear:both;width:100%;">',
                '<tr>',
                '<td class="eBook-reports-logEntry-details">',
                '<ul>',
                '<tpl if="[this.actionPerformed(actions,\'changesAfterGAD\')] &gt; -1">',
                '<li class="eBook-history-16" id="changesAfterGAD">Changes made after general assembly date</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'comment\')] &gt; -1">',
                '<li class="eBook-icon-checklist-24" id="comment">Review notes added</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'pdfAttachment\')] &gt; -1">',
                '<li class="eBook-pdf-view-24" id="comment">Review notes attachment added</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'bundleLocked\')] &gt; -1">',
                '<li class="eBook-lock-16" id="bundleLocked">Deliverable locked</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'draftGenerated\')] &gt; -1">',
                '<li class="eBook-Pdf-draft-icon" id="draftGenerated">Draft PDF generated</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'finalGenerated\')] &gt; -1">',
                '<li class="eBook-Pdf-final-icon"  id="finalGenerated">PDF generated</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'bundleScanned\')] &gt; -1">',
                '<li class="eBook-Pdf-signed-icon" id="bundleScanned">Deliverable scanned</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'status\')] &gt; -1">',
                    '<tpl if="[this.isRejectionStatus(status)] &gt; -1">',
                        '<li class="eBook-icon-delete-16" id="status">{status}</li>',
                 '</tpl>',
                    '<tpl if="[this.isRejectionStatus(status)] == -1">',
                        '<li class="eBook-tickcircle-16" id="status">{status}</li>',
                    '</tpl>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'bundleOpen\')] &gt; -1">',
                '<li class="eBook-unlock-16" id="bundleOpen">Deliverable reopened</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'confirmationMemorandumSigned\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="confirmationMemorandumSigned">Signed memorandum confirmed</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'confirmationSignedDocumentsAddedToFirstFile\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="confirmationSignedDocumentsAddedToFirstFile">Signed documents added confirmed</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'representationLetterSigned\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="representationLetterSigned">Signed representation letter uploaded</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'minutesGeneralAssembleSigned\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="minutesGeneralAssembleSigned">Signed minutes general assemble uploaded</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'statutoryAccountsSigned\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="statutoryAccountsSigned">Signed statutory accounts uploaded</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'annualAccounts\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="annualAccounts">Annual Accounts uploaded</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'engagementCodeUpdate\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="engagementCodeUpdate">Inactive engagement code replaced</li>',
                '</tpl>',
                '</ul>',
                '</td>',
                '</table>',
                '</div>',
                '</tpl>',{
                    actionPerformed: function(actions,action) {
                        return actions.indexOf(action);
                    },
                    isRejectionStatus: function(status) {
                        return status.indexOf("Rejected");
                    }
                }
            );

        Ext.apply(me, {
            title: 'Log entries',
            //cls: 'eBook-reports-logEntries',
            width: 400,
            minWidth: 400,
            //collapsedCls: 'eBook-reports-logEntries-collapsed', not working in Extjs 3 due to border layout?
            autoScroll: true,
            items: new Ext.DataView({
                itemId: 'dataviewLog',
                tpl: tpl,
                store: store,
                autoHeight:true,
                multiSelect: true,
                overClass:'x-view-over',
                itemSelector:'div.thumb-wrap',
                emptyText: 'No log entries to display',
                storeLoad: function (fileId,serviceId) {
                    if(!fileId || !serviceId)
                    { Ext.Msg.alert("failure","failed to load comments."); return; }

                    var me = this,
                        params = {
                            FileId: fileId,
                            ServiceId: serviceId,
                            Culture: null,
                            Order: 'DESC'
                        };

                    me.getEl().mask("loading data");
                    //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                    Ext.apply(me.store.baseParams, params);

                    me.store.load({
                        params: params,
                        callback: me.onLoadSuccess,
                        scope: me
                    });
                },
                onLoadSuccess: function (r, options, success) {
                    var me = this;
                    console.log(r);
                    if (success) {
                        me.getEl().unmask();
                    }
                    else {
                        me.getEl().mask("Loading of data failed");
                    }
                }
            }),
            listeners: {
                afterrender: function () {
                    var me = this;

                    //retrieve the fileservice store using the file ID and service ID
                    if(me.fileId && me.serviceId) {
                        me.getComponent("dataviewLog").storeLoad(me.fileId,me.serviceId);
                    }
                }
            }
        });

        eBook.Bundle.Log.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('fileServicelog', eBook.Bundle.Log);eBook.Bundle.ReviewNotesWindow = Ext.extend(eBook.Window, {

    initComponent: function () {
        var me = this,
            title =  me.serviceId ? me.serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B' ? 'NBB filing' : me.serviceId.toUpperCase() == 'C3B8C8DB-3822-4263-9098-541FAE897D02' ? 'Year end close' : '' : '',
            clientName = eBook.Interface.currentClient ? eBook.Interface.currentClient.get("Name") : me.clientName,
            endDate = eBook.Interface.currentFile ? eBook.Interface.currentFile.get('EndDate') : me.endDate,
            store = new eBook.data.JsonStore({
                    selectAction: 'GetFileServiceLog'
                    , serviceUrl: eBook.Service.file
                    , criteriaParameter: 'cfsdc'
                    , fields: eBook.data.RecordTypes.Log
                }),
            tpl = new Ext.XTemplate(
                '<p>',
                '<h3 style="text-align: center;">Review Notes</h3>',
                '<h4 style="text-align: center;">' + clientName + '  -  ' + title + ' per ' + endDate.format("d/m/Y") + '</h4>',
                '</p>',
                '<tpl for=".">',
                '<tpl if="[this.actionPerformed(actions,\'comment\')] &gt; -1"=>',
                '<div class="eBook-reports-logEntry">',
                '<div>',
                '<div id="personName" style="float:left;" class="eBook-reports-header eBook-reports-icon eBook-icon-checklist-24">{personName}</div>',
                '<tpl if="[this.actionPerformed(actions,\'pdfAttachment\')] &gt; -1"=>',
                '<div style="float:left;  margin-left: 20px;">(</div><div id="pdfAttachment" style="float:left;" class="eBook-reports-logEntry-pdfAttachment eBook-reports-icon eBook-bundle-tree-pdf" pdfAttachmentId="{pdfAttachmentId}">Open attachment</div>)<div style="float:left;margin: 0px 5px;"></div>',
                '</tpl>',
                '<div style="float:right;" id="timestamp">{timestamp:date("d/m/Y H:i")}</div>',
                '</div>',
                '<div id="logEntry-center" class="eBook-reports-logEntry-center" style="clear:both"><div id="comment" class="eBook-reports-logEntry-comment-only eBook-reports-logEntry-comment">{comment}</div></div>',
                '</div>',
                '</tpl>',
                '</tpl>', {
                    actionPerformed: function (actions, action) {
                        return actions.indexOf(action);
                    }
                }
            ),
            items = [
                {
                    xtype: 'dataview',
                    itemId: 'dataviewLog',
                    flex: 3,
                    tpl: tpl,
                    itemSelector:'.eBook-reports-logEntry-pdfAttachment',
                    listeners: {
                        click: function (dataview,indexn,node) {
                            dataview.openDocument(node.getAttribute("pdfAttachmentId"),eBook.CurrentClient);
                        }
                    },
                    store: store,
                    autoScroll: true,
                    /*overClass: 'x-view-over',
                     itemSelector: 'div.thumb-wrap',*/
                    emptyText: 'No log entries to display',
                    storeLoad: function (fileId, serviceId) {
                        if (!fileId || !serviceId) {
                            Ext.Msg.alert("failure", "failed to load comments.");
                            return;
                        }

                        var me = this,
                            params = {
                                FileId: fileId,
                                ServiceId: serviceId
                            };

                            me.getEl().mask("loading data");
                        //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                        Ext.apply(me.store.baseParams, params);

                        me.store.load({
                            params: params,
                            callback: me.onLoadSuccess,
                            scope: me
                        });
                    },
                    onLoadSuccess: function (r, options, success) {
                        var me = this;
                        console.log(r);
                        if (success) {
                            me.getEl().scroll('b', Infinity);
                            me.getEl().unmask();
                        }
                        else {
                            me.getEl().mask("Loading of data failed");
                        }
                    },
                    openDocument: function (repositoryItemId, clientId) {
                        var wn = new eBook.Repository.FileDetailsWindow({
                            repositoryItemId: repositoryItemId,
                            readOnly: false,
                            readOnlyMeta: false,
                            clientId: clientId
                        });

                        wn.show();
                        wn.loadById();
                    }
                },
                new eBook.Bundle.HtmlEditor({ ref: 'htmlEditor', flex: 1 })
            ];

        Ext.apply(this, {
            title: eBook.Bundle.Window_AddComment_Title,
            ref: 'reviewNotes',
            layout:'vbox',
            layoutConfig: {
                align : 'stretch'
            },
            bodyStyle: 'background-color:#FFF;',
            //cls: 'eBook-reports-logEntries',
            width: Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width  : 1000,
            height: Ext.getBody().getViewSize().height - 50,
            minimizable: true,
            items: items,
            bbar: [
            {
                xtype: 'fieldset',
                style: 'border: none;',
                ref: '../pdfAttachmentLabel',
                html: '<div id="eBook-toolbar-pdfAttachmentLabel eBook-Pdf-final-icon" class="eBook-toolbar-pdfAttachmentLabel">' + (me.pdfAttachmentId ? 'attachment added' : '') + '</div>'
            },{
                    xtype: 'tbfill'
            },{
                text: 'Attach',
                ref: '../pdfAttachmentButton',
                iconCls: 'eBook-icon-uploadpdf-24',
                scale: 'medium',
                iconAlign: 'top',
                buttonAlign: 'right',
                handler: me.onPdfAttachClick,
                tooltip: 'Add and attach a PDF file to the review notes (useful for adding scanned written notes).',
                width: 100,
                scope: this
            },{
                text: 'Print',
                ref: '../printButton',
                iconCls: 'eBook-iconprint-24',
                scale: 'medium',
                iconAlign: 'top',
                buttonAlign: 'right',
                handler: me.onPrintClick,
                width: 100,
                scope: this
            }, {
                text: 'Continue',
                ref: '../saveButton',
                iconCls: 'eBook-yes-24',
                scale: 'medium',
                iconAlign: 'top',
                buttonAlign: 'right',
                handler: me.onSaveClick,
                width: 100,
                scope: this
            }],
            listeners: {
                afterrender: function (p) {
                    var me = this;

                    //retrieve the fileservice store using the file ID and service ID
                    if (me.fileId && me.serviceId) {
                        me.getComponent("dataviewLog").storeLoad(me.fileId, me.serviceId);
                    }

                    if(me.readOnly == true)
                    {
                        me.htmlEditor.hide();
                        me.saveButton.hide();
                    }
                },
                minimize: function(p) {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');

                    if(this.minimized)
                    {
                        p.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width  : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else {
                        p.restoreSize = this.getSize();
                        p.restorePos = this.getPosition(true);
                        p.getComponent("dataviewLog").hide();
                        p.setSize(Ext.getBody().getViewSize().width / 5, Ext.getBody().getViewSize().height / 5);
                        p.setPosition(Ext.getBody().getViewSize().width - p.getWidth() - 10, Ext.getBody().getViewSize().height - p.getHeight() - 10)
                        p.minimized = true;
                        bundleEditWindow.hide();
                    }
                },
                maximize: function(p)
                {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');
                    if(this.minimized)
                    {
                        p.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width  : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else{
                        p.maximize();
                    }
                },
                restore: function(p)
                {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');
                    if(this.minimized)
                    {
                        p.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width  : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else{
                        p.restore();
                    }
                }
            }
        });

        eBook.Bundle.ReviewNotesWindow.superclass.initComponent.apply(this, arguments);
    },
    onSaveClick: function()
    {
        var me = this;

        if(me.form)
        {
            me.submitForm(); //if the optional form panel is present, process flow
        }
        else
        {
            me.save();
        }
    },
    submitForm: function()
    {
        if(!this.form) return;

        //Variable data retrieved from form
        var me = this,
            service = me.form.service,
            action = me.form.action,
            criteriaParameter = me.form.criteriaParameter,
            jsonData = {};

        //Validate user input
        if(me.form.isValid())
        {
            //Retrieve user form input, create json
            jsonData[criteriaParameter] = me.form.getData();

            //Save form data
            Ext.Ajax.request({
                url: service + action,
                method: 'POST',
                jsonData: jsonData,
                callback: me.formSubmitted,
                scope: me
            });
        }
    },
    formSubmitted: function(options, success, response) {
        var me = this;

        if(success)
        {
            me.save(); //continue main review notes flow
        }
        else
        {
            Ext.Msg.alert("Issue","There was an issue with form. Please contact the administrator.")
        }
    },
    save: function() {
        var me = this,
            comment = me.htmlEditor.getValue(),
            wrapper = document.createElement('div');

        //Quick fixes in order to avoid copy pase issues as uses tend to copy paste the entire floating window
        //Proper implementation would be preferable
        wrapper.innerHTML = comment;

        if(comment.length > 0) {
            if (comment.indexOf("x-box-item") != -1) //user copied editor
            {
                alert("Please make sure you only copy the review notes and not the editor.");
                return;
            }

            var walkDom = function walk(node, func) {
                func(node);
                node = node.firstChild;
                while (node) {
                    walk(node, func);
                    node = node.nextSibling;
                }
            };

            //remove any potential hazardous attributes in order to avoid copy paste issues
            walkDom(wrapper.firstChild, function (element) {
                if (element.removeAttribute) {
                    element.removeAttribute('id');
                    element.removeAttribute('style');
                    element.removeAttribute('class');
                }
            });

            comment = wrapper.innerHTML;
        }

        //If review not required or at least 15 characters
        if(comment.length > 15 && (me.required || me.pdfAttachmentId) || (!me.required && !me.pdfAttachmentId)) { //comment also required on pdf attachment
            if (me.readyCallback) { //it requires a readyCallback function in order to continue

                if(this.readyCallback.fnName && this.readyCallback.fnName == 'closeService') //is this function closeService
                {
                    this.readyCallback.fn.call(me.readyCallback.scope, me.readyCallback.bundle, this.readyCallback.serviceId, this.readyCallback.status, comment, null, me.pdfAttachmentId);
                }
                else {
                    //If the readyCallback function triggers yet another function
                    if (me.readyCallback.delegateFn) { //delegate function attached to readyCallback function?
                        //Add review notes to the to be returned parameters array
                        me.readyCallback.delegateParams.push(comment);
                        me.readyCallback.delegateParams.push(me.pdfAttachmentId);

                        me.readyCallback.fn.defer(10, me.readyCallback.scope, [me.readyCallback.delegateFn, me.readyCallback.delegateParams]);
                    }
                    else { //UpdateFileService is the common standard used function
                        me.readyCallback.fn.call(me.readyCallback.scope, me.readyCallback.bundle, me.readyCallback.serviceId, me.readyCallback.statusId, me.readyCallback.repositoryItem, comment, null, me.pdfAttachmentId);
                    }
                }
                me.close();
            }
        }
        else
        {
            Ext.Msg.alert('Review notes','Please add review notes.');
        }
    },
    onPrintClick: function() {
        var me = this,
            divReviewNotes = me.getComponent("dataviewLog").getEl().dom.innerHTML,
            wn = window.open('','Print version','height=600,width=800'),
            reportImage= 'iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAEnklEQVRYha2XS2wTVxSGv3l4PE4ysYMd8sJJwCk0bYOREkpBVBWKkCqhFjVqVaWtKvWhqII9rFlXYtlNpG5qKXSRRbtAFYRNC+VVhIoqCqUEjJsmhAYcIPF47JnbRWJje2yTiTjSyHPO/ef8v8+5544tUWKtra0cOXIERVE4+HKa8NKFbY6dyy0bA9Mn/+lnadnEqwkhAJidneX48ePYtl22rpY6o6OjjI6O9kqSdFRysro1v/UtsXxfcgLdZw/seCMvSYpnAY7jAFwXQnw9NTXF1atXawtQFAUhxGFFUb5ycjnykZ2QmkIN9vbKis8TsWmaaJqGLMvYto0QYtLv909X4soECCFwHEe1LAv5xneo3ftA9aNH+jyRA0xMTBCPx4nFYoVQ1W8gF24CPjjQM4OdWSR760ekzr2Y06dRYwdxHMfzJcsy4+PjTE9PF2OF/VBVwIHXVDZk/mgxb598J/94hsy9CwS2f4Ysy66H1mJNTU0YhkEikSCZTLKwsIDP5y5CsQXNOpgP734UCG07Q3Yxpm/sRW0Iw/J9sNKeBeiyRTAYJJvNMjk5STQa7Y7H4zfv3LnDzMyMW0A2L7DV4GXr0d2Pjf53aQiFABCXjiLjffy6zSC3Gl9F0zQsy2Jubu77wcHB/ZFI5MqxY8fcAs7csPlwZNelcHTvRcdxdhfimf/+xXk6g1frBj41LiAhaHrvB+Tg5hbTND85ceLElVJcUcDcE/gp1cUhJVCYXQCyVh47m/csYMVWnitsPl3X8fv9ZYiyMXQcB9u2UZRnB45v6/tIC3+tUwBIqo7c2F5zvdo5UFaBpqFD6yavtGpj6BJg23bNmb0/P08hHAo2o+s6AKZlYduOC+9TVTRfGYUrr6sFhTZUWsY0+Wb826L/9v5hdr++E4DL1/7kwUP3qG6JdrKj/6Wa5C4Bz6tApRUwewe3PxdTy3dVIJ/Po6rlZQOQJYnWSLjYgoaGBhfm2s3bZX441ExXW2tNcpeAUmAl2OfzcXjsy7qYShJRg/S5AioTmZZFxszWTQTQ1RYp83VN89aCWsDU7Dx/J9dxGnZs5JW+3rqYNbUgFu0kFu30LKCQq56/pgoA2LbNrxcvFf2+LZvpaF854e7NzrOUcb+wNgQN2sItL0aAlcvx87nzRV/TNNrb2oprZtZy5cjl8i9mE1bzEc9iWzZ11CR4YS1QFYU39+wuxjd1dbowp879huBZrLt9I/2xHu8Cqs24LMvs2bWzbrJd8X5K+NE0tfxcqJK3ZgVK34gPHi0yv+D9Z1k4ZNAe2VAXs6YWyJKEqnr/UyJJkvc9IIRIQXkFQkYjIaPRs4DKPNUEKAAjIyOMjY0xMDCAZVkX/X7/dcMwPlgXYx0zTfOcoiinhoeHGRoaIhAIIAEkEgmADmAP0Ae09fT0fK7rehOsYFZNcmWtbaLkUwgh8qlUaiKTyfwCXANuAY8LLZCAOPAFEAOak8kkQIaVKikl5FLJVY3UKbkXgM3Kr9McsA/oB84CCeB3FSCdTotQKHQaOA8Yq1cQ8ANNq36BsAFoqSJArApeXCUVQBZYAp6WrD1ZjZnpdJr/AXMfZUp9LpliAAAAAElFTkSuQmCC';

        wn.document.write('<html><head>');
        wn.document.write('<style TYPE="text/css">.eBook-icon-checklist-24{background-image:url(data:image/gif;base64,' + reportImage + ');}.eBook-reports-column { padding:10px; } .eBook-reports-button-column { padding: 10px; } .eBook-reports-button { margin-bottom: 4px; } .eBook-reports-row { border-color: white; width: 500% !important; } .eBook-reports-poa .x-grid3-cell-inner { line-height: 15px; padding: 3px 3px 1px 5px; } .eBook-reports-poa-row-button { border: 1px solid #C1C1C1; border-radius: 2px; cursor: pointer; text-align: center; } .eBook-reports-logEntries { top: 119px !important; } .eBook-reports-logEntries-collapsed { visibility: hidden; } .eBook-reports-logEntry { position: relative; border: 1px solid gray; border-radius: 4px; background-color: #F0F0F0; text-align: left; margin: 5px; padding: 5px; } .eBook-reports-header { font-weight: bold; height: 14px; line-height: 14px; } .eBook-reports-icon { padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; padding-bottom: 7px; background-size: 12px 12px; } .eBook-reports-logEntry-details{ width: 187px; padding: 2px 5px; vertical-align: top; } .eBook-reports-logEntry-comment{ padding: 2px 2px 2px 5px; vertical-align: top; border-left: 1px solid gray; list-style-position: inside; font: normal 13px tahoma,arial,helvetica,sans-serif; } .eBook-reports-logEntry-comment-only{ border-left: 1px solid rgb(229, 172, 23); } .eBook-reports-logEntry-comment li { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-comment ul { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-comment ol { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-center { margin-left: 6px; } .eBook-reports-logEntry-footer{ padding: 5px 5px 0px 5px; } .eBook-reports-logEntry-details ul{ padding:0; } .eBook-reports-logEntry-details li { padding: 0.1em 0 0.1em 1.5em; margin-bottom: 0.2em; text-indent: 0.4em; list-style: none; background-repeat: no-repeat; background-size: 12px 12px; background-position: 2px 3px; background-image: url(../images/icons/16/tick-circle.png); }</style>');
        wn.document.write('</head><body >');
        wn.document.write(divReviewNotes);
        wn.document.write('</body></html>');
        wn.document.close(); // necessary for IE >= 10
        wn.focus(); // necessary for IE >= 10
        wn.print();
        wn.close();
    },
    onPdfAttachClick: function() {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
            today = new Date(),
            dd = ("0" + today.getDate()).slice(-2),
            mm = ("0" + (today.getMonth() + 1)).slice(-2),
            yyyy = today.getFullYear();

            wn = new eBook.Repository.SingleUploadWindow({
                clientId: eBook.CurrentClient,
                standards: {
                    location: 'f344c1b4-7db5-4249-9a11-8bd6fca7aafc', //F. Review Note Attachments
                    fileName: me.readyCallback.bundle.Name+ '-ReviewNoteAttachment-' + yyyy + mm + dd,
                    statusid: null
                },
                readycallback: {
                    fn: me.onUploadSuccess,
                    scope: me
                }
            });

        wn.show(me);
    },
    onUploadSuccess: function (repositoryItem)
    {
        var me = this;
        me.pdfAttachmentId = repositoryItem;
        //update label
        me.pdfAttachmentLabel.update('<div id="eBook-toolbar-pdfAttachmentLabel eBook-Pdf-final-icon" class="eBook-toolbar-pdfAttachmentLabel">' + (me.pdfAttachmentId ? 'attachment added' : '') + '</div>');
    }
});eBook.Bundle.HtmlEditor = Ext.extend(Ext.ux.TinyMCE, {
    initComponent: function () {
        var me = this;
        me.pdfAttachmentId = null;

        Ext.apply(this, {
            tinymceSettings: {
                theme: "advanced",
                plugins: "pagebreak,table,iespell,insertdatetime,searchreplace,paste,directionality,noneditable,xhtmlxtras",
                theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|cut,copy,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,cleanup,code,|,insertdate,inserttime",
                theme_advanced_buttons2: "",
                theme_advanced_buttons3: "",
                //theme_advanced_buttons4: ",abbr,acronym,del,ins,|,visualchars,nonbreaking,pagebreak",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: true,
                extended_valid_elements: ""
            },
            boxMinHeight: 200
        });
        eBook.Bundle.HtmlEditor.superclass.initComponent.apply(this, arguments);
    }
    , startEdit: function (el, val) {
        if (Ext.isDefined(val)) this.setValue(val);
        if (this.rendered) {
            this.el.remove();
            this.rendered = false;
        }

        this.render(el);
    }
});//The form view resembles the review notes window
//but it adds an overview and form panel
eBook.Bundle.Form = Ext.extend(eBook.Window, {

    initComponent: function () {
        var me = this,
            clientName = eBook.Interface.currentClient ? eBook.Interface.currentClient.get("Name") : me.clientName,
            endDate = eBook.Interface.currentFile ? eBook.Interface.currentFile.get('EndDate') : me.endDate,
            store = new eBook.data.JsonStore({
                selectAction: 'GetFileServiceLog'
                , serviceUrl: eBook.Service.file
                , criteriaParameter: 'cfsdc'
                , fields: eBook.data.RecordTypes.Log
            }),
            tpl = new Ext.XTemplate(
                '<p>',
                '<h3 style="text-align: center;">Review Notes</h3>',
                '<h4 style="text-align: center;">' + clientName + '  -  ' + me.serviceName + ' ' + endDate.format("d/m/Y") + '</h4>',
                '</p>',
                '<tpl for=".">',
                '<tpl if="[this.actionPerformed(actions,\'comment\')] &gt; -1"=>',
                '<div class="eBook-reports-logEntry">',
                '<div>',
                '<div id="personName" style="float:left;" class="eBook-reports-header eBook-reports-icon eBook-icon-checklist-24">{personName}</div>',
                '<tpl if="[this.actionPerformed(actions,\'pdfAttachment\')] &gt; -1"=>',
                '<div style="float:left;  margin-left: 20px;">(</div><div id="pdfAttachment" style="float:left;" class="eBook-reports-logEntry-pdfAttachment eBook-reports-icon -eBook-bundle-tree-pdf" pdfAttachmentId="{pdfAttachmentId}">Open attachment</div>)<div style="float:left;margin: 0px 5px;"></div>',
                '</tpl>',
                '<div style="float:right;" id="timestamp">{timestamp:date("d/m/Y H:i")}</div>',
                '</div>',
                '<div id="logEntry-center" class="eBook-reports-logEntry-center" style="clear:both"><div id="comment" class="eBook-reports-logEntry-comment-only eBook-reports-logEntry-comment">{comment}</div></div>',
                '</div>',
                '</tpl>',
                '</tpl>', {
                    actionPerformed: function (actions, action) {
                        return actions.indexOf(action);
                    }
                }
            ),
            vboxItems = [
                {
                    xtype: 'dataview',
                    itemId: 'dataviewLog',
                    ref: '../dataviewlog',
                    flex: 3,
                    tpl: tpl,
                    itemSelector: '.eBook-reports-logEntry-pdfAttachment',
                    listeners: {
                        click: function (dataview, indexn, node, e) {
                            dataview.openDocument(node.getAttribute("pdfAttachmentId"), eBook.CurrentClient);
                        }
                    },
                    store: store,
                    autoScroll: true,
                    /*overClass: 'x-view-over',
                     itemSelector: 'div.thumb-wrap',*/
                    emptyText: 'No log entries to display',
                    storeLoad: function (fileId, serviceId) {
                        if (!fileId || !serviceId) {
                            Ext.Msg.alert("failure", "failed to load comments.");
                            return;
                        }

                        var me = this,
                            params = {
                                FileId: fileId,
                                ServiceId: serviceId
                            };

                        me.getEl().mask("loading data");
                        //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                        Ext.apply(me.store.baseParams, params);

                        me.store.load({
                            params: params,
                            callback: me.onLoadSuccess,
                            scope: me
                        });
                    },
                    onLoadSuccess: function (r, options, success) {
                        var me = this;
                        console.log(r);
                        if (success) {
                            me.getEl().scroll('b', Infinity);
                            me.getEl().unmask();
                        }
                        else {
                            me.getEl().mask("Loading of data failed");
                        }
                    },
                    openDocument: function (repositoryItemId, clientId) {
                        var wn = new eBook.Repository.FileDetailsWindow({
                            repositoryItemId: repositoryItemId,
                            readOnly: false,
                            readOnlyMeta: false,
                            clientId: clientId
                        });

                        wn.show();
                        wn.loadById();
                    }
                },
                new eBook.Bundle.HtmlEditor({ref: '../htmlEditor', flex: 1, title: 'Add note'})
            ],
            hboxItems = [
                {
                    xtype: 'formOverview',
                    ref: 'formOverview',
                    flex: 1,
                    serviceUrl: me.serviceUrl,
                    fileId: me.fileId
                },
                {
                    xtype: 'panel',
                    flex: 2,
                    layout: 'vbox',
                    title: 'Review notes',
                    ref: 'rightPanel',
                    layoutConfig: {
                        align: 'stretch'
                    },
                    items: vboxItems
                }
            ],
            bbar = [
                {
                    xtype: 'fieldset',
                    style: 'border: none;',
                    ref: '../../pdfAttachmentLabel',
                    html: '<div id="eBook-toolbar-pdfAttachmentLabel eBook-Pdf-final-icon" class="eBook-toolbar-pdfAttachmentLabel">' + (me.pdfAttachmentId ? 'attachment added' : '') + '</div>'
                }, {
                    xtype: 'tbfill'
                }, {
                    text: 'Attach',
                    ref: '../../pdfAttachmentButton',
                    iconCls: 'eBook-icon-uploadpdf-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.onPdfAttachClick,
                    tooltip: 'Add and attach a PDF file to the review notes (useful for adding scanned written notes).',
                    width: 100,
                    scope: this
                }, {
                    text: 'Print',
                    ref: '../../printButton',
                    iconCls: 'eBook-iconprint-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.onPrintClick,
                    width: 100,
                    scope: this
                }];


        //If form is not null, add the form and form overview panel
        if(me.form) {
            vboxItems.splice(1, 0, me.form);
            //notes panel
            vboxItems[0].flex = 1;
            //form panel
            vboxItems[1].height = 225;
            vboxItems[1].border = false;
            vboxItems[1].parent = this;
        }

        if (me.formButtons) {
            //overwrite template handler and scope of form buttons
            for (var i = 0; i < me.formButtons.length; i++) {
                me.formButtons[i].handler = me.onSaveClick;
                me.formButtons[i].scope = me;
            }

            //Add form buttons to bottom bar
            bbar.push(me.formButtons);

        } else {
            bbar.push({
                text: 'Continue',
                ref: '../../saveButton',
                iconCls: 'eBook-yes-24',
                scale: 'medium',
                iconAlign: 'top',
                buttonAlign: 'right',
                handler: me.onSaveClick,
                width: 100,
                scope: this
            });
        }

        //Overwrite handler, utiliz

        Ext.apply(this, {
            title: me.serviceName,
            ref: 'form',
            layout: 'hbox',
            layoutConfig: {
                align: 'stretch'
            },
            bodyStyle: 'background-color:#FFF;',
            //cls: 'eBook-reports-logEntries',
            width: Ext.getBody().getViewSize().width < 1300 ? Ext.getBody().getViewSize().width : 1300,
            height: Ext.getBody().getViewSize().height - 50,
            minimizable: true,
            items: hboxItems,
            bbar: bbar,
            listeners: {
                afterrender: function (p) {
                    var me = this;

                    //retrieve the fileservice store
                    if (me.fileId && me.serviceId) {
                        me.rightPanel.getComponent("dataviewLog").storeLoad(me.fileId, me.serviceId);

                        //retrieve form data
                        if(me.form)
                        {
                            //Save form data
                            Ext.Ajax.request({
                                url: me.serviceUrl + me.getAction,
                                method: 'POST',
                                jsonData: {
                                    cidc: {
                                        'Id' : me.fileId
                                    }
                                },
                                callback: me.setFormData,
                                scope: me
                            });
                        }
                    }


                    if (me.readOnly == true) {
                        me.htmlEditor.hide();
                        me.saveButton.hide();
                    }
                },
                minimize: function (p) {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');

                    if (this.minimized) {
                        p.rightPanel.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else {
                        p.restoreSize = this.getSize();
                        p.restorePos = this.getPosition(true);
                        p.rightPanel.getComponent("dataviewLog").hide();
                        p.setSize(Ext.getBody().getViewSize().width / 5, Ext.getBody().getViewSize().height / 5);
                        p.setPosition(Ext.getBody().getViewSize().width - p.getWidth() - 10, Ext.getBody().getViewSize().height - p.getHeight() - 10)
                        p.minimized = true;
                        bundleEditWindow.hide();
                    }
                },
                maximize: function (p) {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');
                    if (this.minimized) {
                        p.rightPanel.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else {
                        p.maximize();
                    }
                },
                restore: function (p) {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');
                    if (this.minimized) {
                        p.rightPanel.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else {
                        p.restore();
                    }
                }
            }
        });

        eBook.Bundle.Form.superclass.initComponent.apply(this, arguments);
    },
    onSaveClick: function (btn) {
        var me = this;

        if (btn.reviewNotesRequired) {
            me.required = btn.reviewNotesRequired;
        }

        if (me.form) {
            me.submitForm(btn); //if the optional form panel is present, process flow
        }
        else {
            me.save(btn);
        }
    },
    submitForm: function (btn) {
        if (!this.form) {return;}

        //Variable data retrieved from form
        var me = this,
            jsonData = {};

        //Validate user input, if it's not a rejection
        if (btn.type == "reject" || me.form.isValid()) {
            //Retrieve user form input, create json
            jsonData[me.criteriaParameter] = me.form.getData();
            //Add file id
            jsonData[me.criteriaParameter].FileId = me.fileId;

            //Save form data
            Ext.Ajax.request({
                url: me.serviceUrl + me.updateAction,
                method: 'POST',
                jsonData: jsonData,
                callback: me.formSubmitted,
                btn: btn,
                scope: me
            });
        }
    },
    formSubmitted: function (options, success, response) {
        var me = this;

        if (success) {
            me.save(options.btn); //continue main review notes flow
        }
        else {
            Ext.Msg.alert("Issue", "There was an issue with form. Please contact the administrator.");
        }
    },
    setFormData: function (options, success, response) {
        var me = this;

        if(success && response.responseText) {
            me.form.setData(Ext.decode(response.responseText)[me.getAction + "Result"]);
        }
    },
    save: function (btn) {
        var me = this,
            comment = me.htmlEditor.getValue(),
            wrapper = document.createElement('div');

        //Quick fixes in order to avoid copy pase issues as uses tend to copy paste the entire floating window
        //Proper implementation would be preferable
        wrapper.innerHTML = comment;

        if (comment.length > 0) {
            if (comment.indexOf("x-box-item") != -1) //user copied editor
            {
                alert("Please make sure you only copy the review notes and not the editor.");
                return;
            }

            var walkDom = function walk(node, func) {
                func(node);
                node = node.firstChild;
                while (node) {
                    walk(node, func);
                    node = node.nextSibling;
                }
            };

            //remove any potential hazardous attributes in order to avoid copy paste issues
            walkDom(wrapper.firstChild, function (element) {
                if (element.removeAttribute) {
                    element.removeAttribute('id');
                    element.removeAttribute('style');
                    element.removeAttribute('class');
                }
            });

            comment = wrapper.innerHTML;
        }

        //If review not required or at least 15 characters
        if (comment.length > 15 && (me.required || me.pdfAttachmentId) || (!me.required && !me.pdfAttachmentId)) { //comment also required on pdf attachment
            if (me.readyCallback) { //it requires a readyCallback function in order to continue
                if (this.readyCallback.fnName && this.readyCallback.fnName == 'closeService') //is this function closeService
                {
                    this.readyCallback.fn.call(me.readyCallback.scope, me.readyCallback.bundle, this.readyCallback.serviceId, btn.followUpStatus, comment, null, me.pdfAttachmentId);
                }
                else {
                    //If the readyCallback function triggers another function
                    if (me.readyCallback.delegateFn) { //delegate function attached to readyCallback function?
                        //Add review notes to the to be returned parameters array
                        me.readyCallback.delegateParams.push(comment);
                        me.readyCallback.delegateParams.push(me.pdfAttachmentId);

                        me.readyCallback.fn.defer(10, me.readyCallback.scope, [me.readyCallback.delegateFn, me.readyCallback.delegateParams]);
                    }
                    else { //UpdateFileService is the common standard used function
                        me.readyCallback.fn.call(me.readyCallback.scope, me.readyCallback.bundle, me.readyCallback.serviceId, btn.followUpStatus, me.readyCallback.repositoryItem, comment, null, me.pdfAttachmentId);
                    }
                }
                me.close();
            }
        }
        else {
            Ext.Msg.alert('Review notes', 'Please add review notes.');
        }
    },
    onPrintClick: function () {
        var me = this,
            divReviewNotes = me.rightPanel.getComponent("dataviewLog").getEl().dom.innerHTML,
            wn = window.open('', 'Print version', 'height=600,width=800'),
            reportImage = 'iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAEnklEQVRYha2XS2wTVxSGv3l4PE4ysYMd8sJJwCk0bYOREkpBVBWKkCqhFjVqVaWtKvWhqII9rFlXYtlNpG5qKXSRRbtAFYRNC+VVhIoqCqUEjJsmhAYcIPF47JnbRWJje2yTiTjSyHPO/ef8v8+5544tUWKtra0cOXIERVE4+HKa8NKFbY6dyy0bA9Mn/+lnadnEqwkhAJidneX48ePYtl22rpY6o6OjjI6O9kqSdFRysro1v/UtsXxfcgLdZw/seCMvSYpnAY7jAFwXQnw9NTXF1atXawtQFAUhxGFFUb5ycjnykZ2QmkIN9vbKis8TsWmaaJqGLMvYto0QYtLv909X4soECCFwHEe1LAv5xneo3ftA9aNH+jyRA0xMTBCPx4nFYoVQ1W8gF24CPjjQM4OdWSR760ekzr2Y06dRYwdxHMfzJcsy4+PjTE9PF2OF/VBVwIHXVDZk/mgxb598J/94hsy9CwS2f4Ysy66H1mJNTU0YhkEikSCZTLKwsIDP5y5CsQXNOpgP734UCG07Q3Yxpm/sRW0Iw/J9sNKeBeiyRTAYJJvNMjk5STQa7Y7H4zfv3LnDzMyMW0A2L7DV4GXr0d2Pjf53aQiFABCXjiLjffy6zSC3Gl9F0zQsy2Jubu77wcHB/ZFI5MqxY8fcAs7csPlwZNelcHTvRcdxdhfimf/+xXk6g1frBj41LiAhaHrvB+Tg5hbTND85ceLElVJcUcDcE/gp1cUhJVCYXQCyVh47m/csYMVWnitsPl3X8fv9ZYiyMXQcB9u2UZRnB45v6/tIC3+tUwBIqo7c2F5zvdo5UFaBpqFD6yavtGpj6BJg23bNmb0/P08hHAo2o+s6AKZlYduOC+9TVTRfGYUrr6sFhTZUWsY0+Wb826L/9v5hdr++E4DL1/7kwUP3qG6JdrKj/6Wa5C4Bz6tApRUwewe3PxdTy3dVIJ/Po6rlZQOQJYnWSLjYgoaGBhfm2s3bZX441ExXW2tNcpeAUmAl2OfzcXjsy7qYShJRg/S5AioTmZZFxszWTQTQ1RYp83VN89aCWsDU7Dx/J9dxGnZs5JW+3rqYNbUgFu0kFu30LKCQq56/pgoA2LbNrxcvFf2+LZvpaF854e7NzrOUcb+wNgQN2sItL0aAlcvx87nzRV/TNNrb2oprZtZy5cjl8i9mE1bzEc9iWzZ11CR4YS1QFYU39+wuxjd1dbowp879huBZrLt9I/2xHu8Cqs24LMvs2bWzbrJd8X5K+NE0tfxcqJK3ZgVK34gPHi0yv+D9Z1k4ZNAe2VAXs6YWyJKEqnr/UyJJkvc9IIRIQXkFQkYjIaPRs4DKPNUEKAAjIyOMjY0xMDCAZVkX/X7/dcMwPlgXYx0zTfOcoiinhoeHGRoaIhAIIAEkEgmADmAP0Ae09fT0fK7rehOsYFZNcmWtbaLkUwgh8qlUaiKTyfwCXANuAY8LLZCAOPAFEAOak8kkQIaVKikl5FLJVY3UKbkXgM3Kr9McsA/oB84CCeB3FSCdTotQKHQaOA8Yq1cQ8ANNq36BsAFoqSJArApeXCUVQBZYAp6WrD1ZjZnpdJr/AXMfZUp9LpliAAAAAElFTkSuQmCC';

        wn.document.write('<html><head>');
        wn.document.write('<style TYPE="text/css">.eBook-icon-checklist-24{background-image:url(data:image/gif;base64,' + reportImage + ');}.eBook-reports-column { padding:10px; } .eBook-reports-button-column { padding: 10px; } .eBook-reports-button { margin-bottom: 4px; } .eBook-reports-row { border-color: white; width: 500% !important; } .eBook-reports-poa .x-grid3-cell-inner { line-height: 15px; padding: 3px 3px 1px 5px; } .eBook-reports-poa-row-button { border: 1px solid #C1C1C1; border-radius: 2px; cursor: pointer; text-align: center; } .eBook-reports-logEntries { top: 119px !important; } .eBook-reports-logEntries-collapsed { visibility: hidden; } .eBook-reports-logEntry { position: relative; border: 1px solid gray; border-radius: 4px; background-color: #F0F0F0; text-align: left; margin: 5px; padding: 5px; } .eBook-reports-header { font-weight: bold; height: 14px; line-height: 14px; } .eBook-reports-icon { padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; padding-bottom: 7px; background-size: 12px 12px; } .eBook-reports-logEntry-details{ width: 187px; padding: 2px 5px; vertical-align: top; } .eBook-reports-logEntry-comment{ padding: 2px 2px 2px 5px; vertical-align: top; border-left: 1px solid gray; list-style-position: inside; font: normal 13px tahoma,arial,helvetica,sans-serif; } .eBook-reports-logEntry-comment-only{ border-left: 1px solid rgb(229, 172, 23); } .eBook-reports-logEntry-comment li { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-comment ul { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-comment ol { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-center { margin-left: 6px; } .eBook-reports-logEntry-footer{ padding: 5px 5px 0px 5px; } .eBook-reports-logEntry-details ul{ padding:0; } .eBook-reports-logEntry-details li { padding: 0.1em 0 0.1em 1.5em; margin-bottom: 0.2em; text-indent: 0.4em; list-style: none; background-repeat: no-repeat; background-size: 12px 12px; background-position: 2px 3px; background-image: url(../images/icons/16/tick-circle.png); }</style>');
        wn.document.write('</head><body >');
        wn.document.write(divReviewNotes);
        wn.document.write('</body></html>');
        wn.document.close(); // necessary for IE >= 10
        wn.focus(); // necessary for IE >= 10
        wn.print();
        wn.close();
    },
    onPdfAttachClick: function () {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
            today = new Date(),
            dd = ("0" + today.getDate()).slice(-2),
            mm = ("0" + (today.getMonth() + 1)).slice(-2),
            yyyy = today.getFullYear();

        wn = new eBook.Repository.SingleUploadWindow({
            clientId: eBook.CurrentClient,
            standards: {
                location: 'f344c1b4-7db5-4249-9a11-8bd6fca7aafc', //F. Review Note Attachments
                fileName: me.readyCallback.bundle.Name + '-ReviewNoteAttachment-' + yyyy + mm + dd,
                statusid: null
            },
            readycallback: {
                fn: me.onUploadSuccess,
                scope: me
            }
        });

        wn.show(me);
    },
    onUploadSuccess: function (repositoryItem) {
        var me = this;
        me.pdfAttachmentId = repositoryItem;
        //update label
        me.pdfAttachmentLabel.update('<div id="eBook-toolbar-pdfAttachmentLabel eBook-Pdf-final-icon" class="eBook-toolbar-pdfAttachmentLabel">' + (me.pdfAttachmentId ? 'attachment added' : '') + '</div>');
    }
});///The FormOverview is simple grid panel loading all form flow related data for a partular service
eBook.Bundle.FormOverview = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            title: "Overview",
            loadMask: true,
            cls: "ebook-formOverview",
            store: new eBook.data.JsonStore({
                selectAction: 'GetFormOverview',
                criteriaParameter: 'cidc',
                autoDestroy: true,
                serviceUrl: me.serviceUrl,
                fields: eBook.data.RecordTypes.Form,
                autoLoad: false
            }),
            viewConfig: {
                forceFit: true,
                autoFill: true,
                scrollOffset: 0
            },
            listeners: {
                afterrender: function (p) {
                    //if the file id is already supplied, "autoLoad" the store
                    if(p.fileId)
                    {
                        p.loadStore(p.fileId);
                    }
                }
            },
            columns: [{
                header: "Field",
                flex: 1,
                dataIndex: 'field',
                css: "font-weight: bold;background-color: #F0F0F0;"
            }, {
                header: "Value",
                flex: 1,
                dataIndex: 'value'
            }]
        });
        eBook.Bundle.FormOverview.superclass.initComponent.apply(me, arguments);
    },
    //retrieve the fileservice form overview using the file id
    loadStore: function (fileId)
    {
        var me = this;

        me.store.load({
            params: {
                Id: fileId
            }
        });
    },

    clearStore: function ()
    {
        var me = this;

        me.store.removeAll();
    }
});
Ext.reg('formOverview', eBook.Bundle.FormOverview);

eBook.Bundle.EditWindow = Ext.extend(eBook.Window, {
    initComponent: function () {
        var me = this,
            bundleWestItems =[
                {
                    xtype: 'bundleSettings',
                    ref: '../bundleSettings',
                    serviceStatus: me.serviceStatus,
                    height: 200
                },
                {
                    xtype: 'bundleItemSettings',
                    ref: '../itemSettings',
                    flex: 1,
                    disabled: this.bundle.Locked
                }
            ];

        //Dynamic panel changes in bundle edit window
        if(me.serviceId && me.serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') //if annual accounts service, hide bundleItemSettings, add form overview
        {
            //hide bundleItemSettings
            var bundleItemSettings = bundleWestItems.filter(function( item ) {
                return item.xtype == 'bundleItemSettings';
            })[0];
            if(bundleItemSettings)
            {
                bundleItemSettings.hidden = true;
            }
            //formOverview, panel shows service specific data in grid
            bundleWestItems.push({
                xtype: 'formOverview',
                ref: '../formOverview',
                flex: 1,
                serviceUrl: eBook.Service.annualAccount,
                fileId: me.bundle.FileId
            });
        }

        Ext.apply(this, {
            layout: 'border'
            , id: 'bundleEditWindow'
            , ref: 'bundleEditWindow'
            , title: me.bundle.Name
            , items: [
                {
                    xtype: 'bundleStatusNotifier',
                    ref: 'notifier',
                    region: 'north',
                    height: 0
                },
                {
                    ref: 'bundleWest',
                    region: 'west',
                    layout: 'vbox',
                    autoScroll: false,
                    width: 400,
                    items: bundleWestItems
                },
                {
                    ref: 'bundleCenter',
                    region: 'center',
                    layout: 'border',
                    layoutconfig: {columns: 2},
                    items: [
                        {
                            /*
                             ref: 'bundleView',

                             items: [
                             {
                             */
                            xtype: 'bundleEditor',
                            region: 'center',
                            ref: '../bundleEditor',
                            autoScroll: true,
                            bundle: this.bundle,
                            bodyCssClass: 'eBook-bundle-bundleEditor'
                            /*}
                             ,
                             {
                             ref: '../../bundlePDF',
                             xtype: 'RepositoryPreview',
                             height: 600
                             }
                             ]*/
                        },
                        {
                            xtype: 'bundlelibrary',
                            ref: '../library',
                            region: 'east',
                            disabled: this.bundle.Locked,
                            width: 300,
                            serviceId: this.serviceId,
                            culture: this.bundle.Culture
                        }
                    ]
                },
                {
                    ref: 'fileServicelog',
                    xtype: 'fileServicelog',
                    region: 'east',
                    width: 300,
                    serviceId: this.serviceId,
                    fileId: this.bundle.FileId,
                    collapsible: false,
                    collapsed: false,
                    hidden: false,
                    hideCollapseTool: false,
                    animCollapse: true
                }
            ]
            , tbar: [{
                xtype: 'buttongroup',
                //ref: '../bundleSettings',
                id: this.id + '-toolbar-settings',
                ref: '../bgDraft',
                columns: 3,
                title: 'Draft',
                items: [{xtype: 'tbspacer'}, {
                    ref: '../draft',
                    text: 'DRAFT',
                    iconCls: 'eBook-icon-draftreport-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    enableToggle: true,
                    pressed: this.bundle.Draft,
                    toggleHandler: this.onToggleDraft,
                    scope: this,
                    disabled: !eBook.User.checkCloser("ACR", true) || (eBook.Interface.isFileClosed()) || this.bundle.Locked
                }, {xtype: 'tbspacer'}]
            }
                , {xtype: 'tbspacer'}
                , {
                    xtype: 'buttongroup',
                    ref: '../bgGeneralActions',
                    id: this.id + '-toolbar-general',
                    columns: 6,
                    title: eBook.Bundle.Window_GeneralActions,
                    items: [{
                        ref: '../../saveBundle',
                        text: eBook.Bundle.Window_ReportSave,
                        iconCls: 'eBook-icon-savereport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onSaveBundleClick,
                        scope: this,
                        disabled: eBook.Interface.isFileClosed() || this.bundle.Locked
                    }, {
                        ref: '../../generateBundleMail',
                        text: 'Preview for e-mail', // eBook.Bundle.Window_ReportGenerate,
                        iconCls: 'eBook-icon-rendermail-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onGenerateBundleMailClick,
                        scope: this
                    }, {
                        ref: '../../generateBundlePrint',
                        text: 'Preview for printing', //eBook.Bundle.Window_ReportGenerate,
                        iconCls: 'eBook-icon-renderprint-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onGenerateBundlePrintClick,
                        scope: this
                    }]
                }
                /*
                 , {xtype: 'tbspacer'}
                 , {
                 xtype: 'buttongroup',
                 title: eBook.Bundle.Window_Status,
                 ref:'../tbServiceStatus',
                 html: '<div id="eBook-bundle-toolbar-status" class="eBook-bundle-toolbar-status">' + (this.serviceStatus.s ? this.serviceStatus.s : '') + '</div>'
                 }
                 */
                , {xtype: 'tbspacer'}
                , {
                    xtype: 'buttongroup',
                    itemId: 'bgReviewNotes',
                    ref: '../bgReviewNotes',
                    cls: 'eBook-bundle-bgReviewnotes',

                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    //hidden: "Champion,Administrator".indexOf(eBook.User.activeClientRoles) == -1, // for testing purposes
                    columns: 2,
                    title: eBook.Bundle.Window_ReviewNotes,
                    items: [{
                        ref: 'addComment',
                        text: eBook.Bundle.Window_AddComment,
                        iconCls: 'eBook-icon-checklist-24',
                        textAlign: 'center',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onAddReviewClick,
                        scope: this,
                        width: 102
                    }]
                }
                , {xtype: 'tbspacer'}
                , {
                    xtype: 'buttongroup',
                    ref: '../bgSpecificActions',
                    //hidden: "Champion,Administrator".indexOf(eBook.User.activeClientRole) == -1, // for testing purposes
                    width: 300,
                    //id: this.id + '-toolbar-service',
                    columns: 5,
                    title: eBook.Bundle.Window_SpecificActions,
                    items: [] //updated through addServiceSpecificButtons function
                }
            ],
            listeners: {
                afterrender: function () {
                    var me = this;

                    if (me.maskedCmp) //unmask any supplied masked component
                    {
                        me.maskedCmp.getEl().unmask();
                    }

                    if (me.serviceId) {
                        me.addServiceSpecificButtons();
                        me.updateBundleRelatedComponents();
                    }

                    //if the delivarable linked to a service has specific buttons, hide certain general buttons
                    if (me.bgSpecificActions.items.getCount() > 0) {
                        //me.bgGeneralActions.hide();
                        me.bgDraft.hide();
                    }
                    else { //hide service specific buttons
                        me.bgSpecificActions.hide();
                    }
                    if (!me.serviceId) {
                        me.bgReviewNotes.hide();
                    }
                    /*
                     if(eBook.Interface.isFileClosed() || me.bundle.Locked) {
                     me.showPdfView(true);
                     }
                     else
                     {
                     me.showPdfView(false);
                     }
                     */
                },
                beforeclose: function (window) {
                    var me = this;
                    if (!window.pendingClose && !me.bundle.Locked) {
                        window.pendingClose = true;
                        Ext.Msg.confirm(eBook.Bundle.Window_Deliverable_Save + '?',
                            'Do you want to save the deliverable?',
                            function (btnid) {
                                if (btnid == 'yes') {
                                    me.onSaveBundle();
                                }
                                window.close();
                            },
                            me
                        );
                        return false;
                    }
                    delete window.pendingclose;
                }
            }
        });
        me.closed = me.bundle.Locked;
        eBook.Bundle.EditWindow.superclass.initComponent.apply(me, arguments);
    }
    ,
    closed: false
    ,
    show: function () {
        eBook.Bundle.EditWindow.superclass.show.call(this);
    }
    ,
    compileBundle: function () {
        var tb = this.getTopToolbar();

        //var bdc = this.bundle;
        if (this.bgDraft.hidden == false) {
            this.bundle.Draft = tb.draft.pressed;
        }
        this.bundle.Contents = this.bundleEditor.getContent()
    }
    ,
    loadBundle: function (id) {
        // load bundle
    }
    ,
    loadNodeSettings: function (node) {
        this.itemSettings.loadNode(node, closed);
    }
    ,
    onGenerateBundleMailClick: function () {
        this.onGenerateBundleClick(false);
    }
    ,
    onGenerateBundlePrintClick: function () {
        this.onGenerateBundleClick(true);
    }
    ,
    onGenerateBundleClick: function (rectoVerso) {
        this.getEl().mask('Generating');

        //if bundle hidden, go with locked me.bundle settings
        this.compileBundle();

        this.bundle.RectoVerso = rectoVerso;
        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'GenerateBundle'
            , method: 'POST'
            , params: Ext.encode({bdc: this.bundle})
            , callback: this.onGenerated
            , scope: this
            , timeout: 1000 * 120
        });
    }
    ,
    onGenerated: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GenerateBundleResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    ,
    onSaveBundleClick: function () {
        var me = this;
        me.onSaveBundle();
    }
    ,
    onSaveBundle: function (callback, callBackAgrs) {
        var me = this;
        //if optional args callback isn't specified, set to null
        callback = (typeof callback === "undefined") ? null : callback;
        callBackAgrs = (typeof callBackAgrs === "undefined") ? null : callBackAgrs;

        me.getEl().mask('Saving');
        me.compileBundle();
        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'SaveBundle'
            , method: 'POST'
            , params: Ext.encode({bdc: me.bundle})
            , callback: this.onSaved.createDelegate(me, [callback, callBackAgrs], true)
            , scope: me
        });
    }
    ,
    onSaved: function (opts, success, resp, callback, callbackArgs) {
        var me = this;
        me.getEl().unmask();
        if (!success) {
            eBook.Interface.showResponseError(resp, me.title);
        }
        if (callback) {
            callback.apply(this, callbackArgs);
        }
    }
    ,
    onCommitBundleClick: function (btn) {
        var me = this,
            fn = me.onCommitBundle;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    }
    ,
    onCommitBundle: function (btn) {
        var me = this;
        //if bundle valid, allow user to enter comment and continue flow
        if (!me.bundle.Locked) {
            me.onSaveBundle(me.validateBundle, [me.bundle.Id, me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]]);
        }
        else {
            me.validateBundle(me.bundle.Id, me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]);
        }
    },
    onSubmitFormClick: function (btn) {
        var me = this,
            fn = me.onSubmitForm;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    },
    onSubmitForm: function (btn) {
        var me = this,
            form = null,
            formObj = {};

        //If any, create form button property
        if (btn.formButtons) {
            formObj.formButtons = me.createServiceSpecificButtons(btn.formButtons);
        }

        //Check what the follow-up status is
        if (me.serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') {
            var formConfig = {
                fileId: me.bundle.FileId,
                serviceId: me.bundle.serviceId
            };
            //Form properties
            formObj.serviceUrl = eBook.Service.annualAccount;
            formObj.updateAction = 'UpdateAnnualAccounts';
            formObj.getAction = 'GetAnnualAccounts';
            formObj.criteriaParameter = 'cfaadc';
            formObj.serviceName = "Filing NBB";
            formObj.fileId = me.bundle.FileId;
            formObj.serviceId = me.serviceId;


            //Create instance of form
            switch (btn.followUpStatus) {
                case 2:
                    formObj.form = new eBook.Forms.AnnualAccounts.Staff(formConfig);
                    required = true;
                    break;
                case 3:
                    formObj.form = new eBook.Forms.AnnualAccounts.Manager(formConfig);
                    required = true;
                    break;
                //case 5: Partner fields moved to manager
                    //formObj.form = new eBook.Forms.AnnualAccounts.Partner(formConfig);
                    //required = true;
                    //break;
            }
        }

        //Open form, containing a subform and form overview panel, after save (not on lock) and validation
        if (!me.bundle.Locked) {
            //bundle, serviceId, repositoryItemId, readyCallback, formObj
            me.onSaveBundle(me.validateBundle, [me.bundle.Id, me.showForm, [me.bundle, me.serviceId, btn.followUpStatus, null, null, formObj]]);
        }
        else {
            me.validateBundle(me.bundle.Id, me.showForm, [me.bundle, me.serviceId, btn.followUpStatus, null, null, formObj]);
        }
    }
    ,
    onApproveBundleClick: function (btn) {
        var me = this,
            fn = me.onApproveBundle;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }

        //If OV, ask confirmation about
        /*if (Ext.Array.contains(eBook.User.activeClientRoles, "OfficeValidator") && !Ext.Array.contains(eBook.User.activeClientRoles, "OfficeValidator")) {
         Ext.Msg.show({
         title: 'Do you have a signed accounting memorandum?',
         msg: 'Do you have an accounting memorandum signed by a partner in your possession?',
         buttons: Ext.Msg.YESNO,
         fn: function (btnid) {
         if (btnid == 'yes') {
         //log
         me.logFileServiceAction(me.bundle.FileId, me.serviceId, 'ConfirmationMemorandumSigned', null);
         me.onApproveBundle(btn);
         }
         },
         scope: me,
         icon: Ext.MessageBox.QUESTION
         });
         }
         else {
         me.onApproveBundle(btn);
         }*/
    }
    ,
    onApproveBundle: function (btn) {
        var me = this;
        //if bundle valid, allow user to enter comment and continue flow
        if (!me.bundle.Locked) {
            me.onSaveBundle(me.validateBundle, [me.bundle.Id, me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]]);
        }
        else {
            me.validateBundle(me.bundle.Id, me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]);
        }
    }
    ,
    onRejectBundleClick: function (btn) {
        var me = this,
            fn = me.onRejectBundle;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    }
    ,
    onRejectBundle: function (btn) {
        var me = this;
        //if bundle valid, compel user to enter comment and continue flow
        if (!me.bundle.Locked) {
            me.onSaveBundle(me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]);
        }
        else {
            me.showReviewNotes(btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null);
        }
    },
    onUploadBundleClick: function (btn) {
        var me = this,
            fn = me.checkFolderForExistingFile;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    },
    ///Check if the location folder already contains a file with suplied status
    checkFolderForExistingFile: function (btn) {
        var me = this,
            params = {
                cid: eBook.CurrentClient,
                sid: btn.location,
                ps: eBook.Interface.currentFile.get('StartDate'),
                pe: eBook.Interface.currentFile.get('EndDate')
            };

        //optionals
        if(btn.repositoryStatusId)
        {
            params.st = btn.repositoryStatusId.toLowerCase();
        }

        me.getEl().mask("Searching for any existing files");

        Ext.Ajax.request({
            url: eBook.Service.repository + 'QueryFiles',
            method: 'POST',
            params: Ext.encode({
                cqfdc: params
            }),
            callback: this.onFolderExistingFilesRetrieved,
            scope: this,
            btn: btn
        });
    },
    onFolderExistingFilesRetrieved: function (options, success, response) {
        var me = this,
            filename = null,
            repositoryStatusId = null,
            btn = options.btn,
            existingItemId = null;

        //optionals
        if (btn.filename)  {filename = btn.filename;}
        if (btn.repositoryStatusId) {repositoryStatusId = btn.repositoryStatusId;}

        me.getEl().unmask();
        if (success) {
            var files = Ext.decode(response.responseText).QueryFilesResult;

            if (files.length > 0) {
                existingItemId = files[0].id;
                Ext.Msg.show({
                    title: "Replace file?",
                    msg: String.format(eBook.Repository.Window_ReplaceMsg, files[0].text),
                    buttons: Ext.Msg.YESNO,
                    fn: function (btnid) {
                        if (existingItemId && btnid == 'yes') {
                            me.openUploadWindow(btn.location, btn.filename, repositoryStatusId, btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, existingItemId);
                        }
                    },
                    scope: me,
                    icon: Ext.MessageBox.QUESTION
                });
            }
            else {
                me.openUploadWindow(btn.location, btn.filename, repositoryStatusId, btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, existingItemId);
            }
        } else {
            eBook.Interface.showResponseError(response, me.title);
        }
    },
    onToggleBundleClosureClick: function (btn) {
        var me = this,
            fn = me.onToggleBundleClosure;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    },
    onToggleBundleClosure: function (btn) {
        var me = this;

        if (me.bundle.Locked) {
            me.reopenService(me.bundle, me.serviceId, btn.followUpStatus); //open service & bundle
        }
        else {

            var readyCallback = { //Close service & bundle
                fn: me.closeService,
                fnName: 'closeService',
                scope: me,
                bundle: me.bundle,
                serviceId: me.serviceId,
                status: btn.followUpStatus
            };

            me.onSaveBundle(me.validateBundle, [me.bundle.Id, me.showReviewNotes, [true, me.bundle, me.bundle.FileId, me.serviceId, null, null, readyCallback]]);
        }
    },
    //A button may contain a confirmation property which defines that the user has to make a confirmation depending on the evaluation
    onConfirmationRequired: function (btn, fn) {
        var me = this,
            condition = btn.confirmation.condition, //If condition
            question = btn.confirmation.question, //Question to be asked
            action = btn.confirmation.logAction; //Confirmation action to be logged

        try {
            required = eval(condition);
        }
        catch (exception) {
            Ext.Msg.alert("Eror", "Evaluation for confirmation failed");
            return;
        }

        if (required) {
            Ext.Msg.show({
                title: 'Confirmation required',
                msg: question,
                buttons: Ext.Msg.YESNO,
                fn: function (btnid) {
                    if (btnid == 'yes') {
                        me.logFileServiceAction(me.bundle.FileId, me.serviceId, action, null); //log
                        fn.call(me,btn);
                    }
                },
                scope: me,
                icon: Ext.MessageBox.QUESTION
            });
        }
        else {
            fn.call(me,btn);
        }
    },
    onResetFlow: function () {
        var me = this;
        me.getEl().mask("Resetting flow");
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'ResetServiceFlow'
            , method: 'POST'
            , jsonData: {
                ufsdc: {
                    FileId: me.bundle.FileId,
                    ServiceId: me.serviceId,
                    BundleId: me.bundle.Id,
                    Person: eBook.User.getActivePersonDataContract()
                }
            }
            , callback: function (options, success) {
                if (success) {
                    Ext.Msg.alert('flow reset executed');
                    me.close();
                }
                else {
                    Ext.Msg.alert('Could not reset service', 'Something went wrong');
                }
                me.getEl().unmask();
            }
            , scope: me
        });
    },
    closeService: function (bundle, serviceId, status, comment, pdfAttachmentId) {
        var me = this;
        me.getEl().mask("Closing service and bundle");
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'CloseService'
            , method: 'POST'
            , jsonData: {
                cfspdc: {
                    FileId: bundle.FileId,
                    ServiceId: serviceId,
                    Status: status,
                    BundleId: bundle.Id,
                    Person: eBook.User.getActivePersonDataContract()
                }
            }
            , callback: function (options, success) {
                if (success) {
                    me.updateFileService(me.bundle, me.serviceId, null, null, comment, true, pdfAttachmentId); //log locked
                }
                else {
                    Ext.Msg.alert('Could not close service', 'Something went wrong');
                }
            }
            , scope: me
        });
    },
    reopenService: function (bundle, serviceId, status) {
        var me = this;
        me.getEl().mask("Reopening service and deliverable");
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'ReOpenService'
            , method: 'POST'
            , jsonData: {
                cfspdc: {
                    FileId: bundle.FileId,
                    ServiceId: serviceId,
                    Status: status,
                    BundleId: bundle.Id,
                    Person: eBook.User.getActivePersonDataContract()
                }
            }
            , callback: function (options, success) {
                if (success) {
                    me.updateFileService(me.bundle, me.serviceId, null, null, null, false, null); //log open
                }
                else {
                    Ext.Msg.alert('Could not reopen service', 'Something went wrong');
                }
            }
            , scope: me
        });
    },
    onAddReviewClick: function (btn) {
        var me = this;

        me.showReviewNotes(false, me.bundle, me.bundle.FileId, me.serviceId, null, null);
    },
    validateBundle: function (bundleId, callback, callbackArgs) {
        var me = this,
            errorMessage = null,
            title = 'Cannot continue. Not valid.',
            templateIds = ['43C27DBE-AE64-4208-BD57-060553D41EC8', 'C29A94BF-BB06-466D-9054-5B2C632E1D97'];

        me.getEl().mask('Validating');

        Ext.Ajax.request({
            url: eBook.Service.bundle + 'ValidateBundle'
            , method: 'POST'
            , params: Ext.encode({id: bundleId})
            , callback: function (opts, success, resp) {
                if (success) {
                    var me = this,
                        obj = Ext.decode(resp.responseText).ValidateBundleResult;

                    if (!obj.Valid) {
                        errorMessage = obj.Fault;
                    }

                    //If the interface shows any upload buttons, change the icon if the validation returns that it has been added
                    var serviceButtons = this.bgSpecificActions.items.items;
                    if (serviceButtons.length > 0) {
                        //Get upload buttons
                        var uploadButtons = serviceButtons.filter(function (obj) {
                            return obj.refName == "uploadBundle";
                        });

                        for (var i = 0; i < uploadButtons.length; i++) {
                            //Check if the filename property of the button, with "Signed" added and spaces removed, is a property
                            //of the validation return and if the value is true, set the icon of the button to added.
                            if (obj[this.bgSpecificActions.items.items[i].validationId] == true) {
                                uploadButtons[i].setIconClass('eBook-repository-added-ico-24');
                            }
                        }
                    }
                } else {
                    errorMessage = 'Something went wrong. Please contact eBook Questions';
                }

                me.getEl().unmask();

                //if there is an error message, prompt the user
                if (errorMessage) {
                    Ext.Msg.alert(title, errorMessage);
                }

                if (obj.Valid && callback) {
                    callback.apply(this, callbackArgs);
                }
            }
            , scope: me
        });
    },
    showForm: function (bundle, serviceId, statusId, repositoryItemId, readyCallback, formObj) {
        var me = this,
            wn = null;

        //if optional args aren't specified, set to null
        readyCallback = (typeof readyCallback === "undefined") ? null : readyCallback;

        if (!readyCallback) {
            readyCallback = {
                fn: me.updateFileService,
                scope: me,
                bundle: bundle,
                serviceId: serviceId,
                statusId: statusId,
                repositoryItemId: repositoryItemId
            };
        }

        formObj.readyCallback = readyCallback;
        wn = new eBook.Bundle.Form(formObj);
        wn.show();
    }
    ,
    showReviewNotes: function (required, bundle, fileId, serviceId, statusId, repositoryItemId, readyCallback) {
        var me = this,
            wn = null,
            obj = {
                required: required,
                fileId: fileId,
                serviceId: serviceId
            };

        //if optional args aren't specified, set to null
        readyCallback = (typeof readyCallback === "undefined") ? null : readyCallback;

        if (!readyCallback) {
            readyCallback = {
                fn: me.updateFileService,
                scope: me,
                bundle: bundle,
                serviceId: serviceId,
                statusId: statusId,
                repositoryItemId: repositoryItemId
            };
        }

        obj.readyCallback = readyCallback;
        wn = new eBook.Bundle.ReviewNotesWindow(obj);
        wn.show();
    }
    ,
    openUploadWindow: function (location, filename, repositoryStatusId, required, bundle, fileId, serviceId, statusId, existingItemId) {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
            standards = {
                location: location
            };

        //optionals
        if (filename) standards.fileName = filename;
        if (repositoryStatusId) standards.statusid = repositoryStatusId;

        var wn = new eBook.Repository.SingleUploadWindow({
            clientId: eBook.CurrentClient,
            existingItemId: existingItemId,
            changeFileName: true,
            standards: standards,
            readycallback: {
                fn: me.onUploadSuccess,
                scope: me,
                required: required,
                bundle: bundle,
                fileId: fileId,
                serviceId: serviceId,
                statusId: statusId,
                filename: filename
            }
        });

        wn.show(me);
    }
    ,
    onUploadSuccess: function (required, bundle, fileId, serviceId, statusId, repositoryItemId, filename) {
        var me = this;

        //retrieve filename for logging purposes (should be replaced by the new filetype prop, but this requires a data fix)
        if (filename) {
            //log upload using filename
            filename = filename.replace(/ /g, '');
            filename = filename.substring(0, 1).toLowerCase() + filename.substring(1);
        }
        else {
            if (serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') //AnnualAccounts
            {
                filename = 'annualAccounts';

                //Update annual accounts record
                me.onNBBUploaded(fileId, repositoryItemId);
            }
        }

        //log action
        me.logFileServiceAction(fileId, serviceId, filename, repositoryItemId);

        var uploadBtnOnly = true;
        me.bgSpecificActions.items.each(function (btn) {
            if (btn.type != "upload") uploadBtnOnly = false;
        }, me);

        //If the step only contains upload buttons, check if bundle is valid in order to continue fileservice flow
        //Any other button has the advantage when it comes to proceeding the flow, hence no further action is taken.
        if (uploadBtnOnly) {
            me.validateBundle(bundle.Id, me.showReviewNotes, [required, bundle, fileId, serviceId, statusId, repositoryItemId]);
        }
    }
    ,
    onNBBUploaded: function (fileId, repositoryItemId) {
        var me = this;
        Ext.Ajax.request({
            url: eBook.Service.annualAccount + 'UpdateRepositoryItem',
            method: 'POST',
            jsonData: {
                cfaadc: {
                    FileId: fileId,
                    RepositoryItemId: repositoryItemId
                }
            },
            callback: function (options, success) {
                if (!success) {
                    Ext.Msg.alert("update failed", "Could not update annual accounts repository item");
                }
            },
            scope: me
        });
    }
    ,
    logFileServiceAction: function (fileId, serviceId, actions, repositoryItemId) {
        var me = this,
            ufsdc = {
                FileId: fileId,
                ServiceId: serviceId,
                Actions: actions,
                RepositoryItem: repositoryItemId,
                Person: eBook.User.getActivePersonDataContract()
            };

        //call
        Ext.Ajax.request({
            url: eBook.Service.file + 'LogFileServiceActions',
            method: 'POST',
            jsonData: {
                ufsdc: ufsdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText) {
                    me.refreshLog(me.bundle.FileId, me.serviceId)
                } else {
                    eBook.Interface.showResponseError(response, me.title);
                    me.getEl().unmask();
                }
            },
            scope: me
        });
    }
    ,
    updateFileService: function (bundle, serviceId, statusId, repositoryItemId, comment, locked, pdfAttachment) {
        //Although all three are optional, at least one must be filled in
        if (!statusId && !repositoryItemId && !comment && (locked == null)) return;

        var me = this,
        //file service update object
            ufsdc = {
                FileId: bundle.FileId,
                ServiceId: serviceId,
                BundleId: bundle.Id,
                Person: eBook.User.getActivePersonDataContract()
            };

        me.getEl().mask('Updating');

        //set optional updates
        if (statusId) ufsdc.Status = statusId;
        if (repositoryItemId) ufsdc.RepositoryItem = repositoryItemId;
        if (comment) ufsdc.Comment = comment;
        if (locked != null) ufsdc.Locked = locked;
        if (pdfAttachment) ufsdc.PdfAttachment = pdfAttachment;

        //call
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'UpdateFileService',
            method: 'POST',
            jsonData: {
                ufsdc: ufsdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText && Ext.decode(response.responseText).UpdateFileServiceResult) {
                    me.onUpdateFileServiceSuccess(Ext.decode(response.responseText).UpdateFileServiceResult);
                } else {
                    eBook.Interface.showResponseError(response, me.title);
                    me.getEl().unmask();
                }
            },
            scope: me
        });
    },
    //handle successful update of file service
    onUpdateFileServiceSuccess: function (serviceStatus) {
        var me = this;
        //Update serviceStatus
        me.serviceStatus = serviceStatus;
        //Update status
        me.updateServiceStatus();
        //Refresh log
        me.refreshLog(me.bundle.FileId, me.serviceId);
        //Update buttons
        me.addServiceSpecificButtons();
        //Optional: update formOverview
        if(me.formOverview) {me.formOverview.loadStore(me.bundle.FileId)};

        //Check if bundle is open
        me.getBundle(me.bundle.Id);

        eBook.Interface.updateFile(eBook.Interface.currentFile.get('Id'));
    },
    getBundle: function (bundleId) {
        if (this.getEl())
            this.getEl().mask('Loading');

        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'GetBundle'
            , method: 'POST'
            , params: Ext.encode({id: bundleId})
            , callback: this.onGetBundleCallBack
            , scope: this
        });
    },
    onGetBundleCallBack: function (options, success, response) {
        var me = this,
            robj = Ext.decode(response.responseText),
            bundle = robj.GetBundleResult;
        me.bundle = bundle;
        me.updateBundleRelatedComponents();
    },
    ///Refresh anything related on bundle status
    updateBundleRelatedComponents: function () {
        var me = this;

        //Handle general buttons and panels
        if (me.bundle.Locked) {
            me.saveBundle.disable();
            me.library.disable();
        }
        else {
            me.saveBundle.enable();
            me.library.enable();
        }

        //Handle specific buttons
        me.bgSpecificActions.items.each(function (btn) {
            if (me.bundle.Locked && btn.type == "lock") { //set unlock UI
                btn.setIconClass('eBook-unlock-24');
                btn.setText(eBook.Bundle.Window_Unlock);
                me.bgSpecificActions.setWidth(132);
            }
            else if (btn.type == "lock") { //set lock UI
                btn.setIconClass('eBook-lock-24');
                btn.setText(eBook.Bundle.Window_Lock);
            }
            else {
                if (!me.bundle.Locked && btn.followUpStatus > 2) { //not locked beyond status 1
                    btn.disable();
                    btn.setTooltip("Bundle must be closed.");
                }
                else {
                    if (Ext.Array.similarity(btn.arrFlowAllowedRoles, eBook.User.activeClientRoles) || btn.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1) {
                        btn.enable();
                    }

                    btn.setTooltip("The client roles " + btn.arrFlowAllowedRoles.join("/") + " can perform this action.");
                }
            }
        }, this);

        me.getEl().unmask();

        /*
         if (me.bundle.Locked) {
         me.saveBundle.disable();
         me.library.disable();
         //me.showPdfView(true);
         if (me.lockBundle) {
         me.lockBundle.setIconClass('eBook-unlock-24');
         me.lockBundle.setText(eBook.Bundle.Window_Unlock);
         me.bgSpecificActions.setWidth(132);
         }
         if (me.commitBundle)
         {
         if(Ext.Array.similarity(me.commitBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.commitBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1)
         {
         me.commitBundle.enable();
         me.commitBundle.setTooltip("The client roles " + me.commitBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         if (me.submitBundle)
         {
         if(Ext.Array.similarity(me.submitBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.submitBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1)
         {
         me.submitBundle.enable();
         me.submitBundle.setTooltip("The client roles " + me.commitBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         if (me.rejectBundle) {
         if(Ext.Array.similarity(me.rejectBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.rejectBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1) {
         me.rejectBundle.enable();
         me.rejectBundle.setTooltip("The client roles " + me.rejectBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         if (me.approveBundle) {
         if(Ext.Array.similarity(me.approveBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.approveBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1) {
         me.approveBundle.enable();
         me.approveBundle.setTooltip("The client roles " + me.approveBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         if (me.uploadBundle) {
         if(Ext.Array.similarity(me.uploadBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.uploadBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1) {
         me.uploadBundle.enable();
         me.uploadBundle.setTooltip("The client roles " + me.uploadBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         }
         else {

         //me.showPdfView(false);
         if (me.lockBundle) {
         me.lockBundle.setIconClass('eBook-lock-24');
         me.lockBundle.setText(eBook.Bundle.Window_Lock);
         }
         //bundle must be locked after when status > 2
         if (me.commitBundle && me.commitBundle.followUpStatus > 2) {
         me.commitBundle.disable();
         me.commitBundle.setTooltip("Bundle must be closed.");
         }
         if (me.submitBundle) {
         me.submitBundle.disable();
         me.submitBundle.setTooltip("Bundle must be closed.");
         }
         if (me.rejectBundle) {
         me.rejectBundle.disable();
         me.rejectBundle.setTooltip("Bundle must be closed.");
         }
         if (me.approveBundle) {
         me.approveBundle.disable();
         me.approveBundle.setTooltip("Bundle must be closed.");
         }
         if (me.uploadBundle) {
         me.uploadBundle.disable();
         me.uploadBundle.setTooltip("Bundle must be closed.");
         }
         }
         //  eBook.Interface.redrawWorksheetsView();
         // me.bgSpecificActions.doLayout();
         */
        me.getEl().unmask();
    },
    //Add service specific buttons to the toolbar
    addServiceSpecificButtons: function () {
        var me = this,
            arrServiceSpecificButtons = null;

        //Get buttons
        arrServiceSpecificButtons = me.createServiceSpecificButtons(me.serviceStatus.fua);

        me.getTopToolbar().remove(me.bgSpecificActions);
        if (arrServiceSpecificButtons) {
            me.getTopToolbar().add({
                xtype: 'buttongroup',
                ref: '../bgSpecificActions',

                width: arrServiceSpecificButtons.length * 130,
                //id: this.id + '-toolbar-service',
                columns: arrServiceSpecificButtons.length,
                title: eBook.Bundle.Window_SpecificActions,
                items: arrServiceSpecificButtons //updated through addServiceSpecificButtons function
            });
        }
        me.doLayout(false, true);
        return true;
    },
    //Within the service_statuses table, each service step has its buttons and actions on those buttons defined
    //These followUpActions are merged with their button templates and returned
    createServiceSpecificButtons: function (arrFollowUpActions) {
        //vars
        var me = this,
        //button templates
            serviceSpecificButtonTemplates = {
                //Proceed to next step, backend executes necessary actions
                commit: {
                    ref: '../../commitBundle',
                    text: eBook.Bundle.Window_Submit,
                    iconCls: 'eBook-yes-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onCommitBundleClick,
                    scope: me
                },
                //Proceed to next step, after frontend interaction (e.x. form)
                submit: {
                    ref: '../../submitBundle',
                    text: eBook.Bundle.Window_Submit,
                    iconCls: 'eBook-yes-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onSubmitFormClick,
                    scope: me
                },
                //Return to certain step
                reject: {
                    ref: '../../rejectBundle',
                    text: eBook.Bundle.Window_Reject,
                    iconCls: 'eBook-no-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onRejectBundleClick,
                    disabled: !me.bundle.Locked,
                    scope: me
                },
                //Proceed to next step
                approve: {
                    ref: '../../approveBundle',
                    text: eBook.Bundle.Window_Approve,
                    iconCls: 'eBook-yes-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onApproveBundleClick,
                    disabled: !me.bundle.Locked,
                    scope: me
                },
                //Upload file and potentially proceed to next step after validateBundle returns valid
                upload: {
                    ref: '../../uploadBundle',
                    text: eBook.Bundle.Window_Upload,
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onUploadBundleClick,
                    scope: me
                },
                //Lock or unlock bundle
                lock: {
                    ref: '../../lockBundle',
                    text: me.bundle.Locked ? eBook.Bundle.Window_Unlock : eBook.Bundle.Window_Lock,
                    iconCls: me.bundle.Locked ? 'eBook-unlock-24' : 'eBook-lock-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    tooltip: 'Temporary unlock of the file and bundle. Year end flow cannot be completed while unlocked.',
                    handler: me.onToggleBundleClosureClick,
                    scope: me
                }
            },
            arrServiceSpecificButtons = [],
            iServiceSpecificButtons = arrServiceSpecificButtons.length,
        //Default roles
            arrAlwaysAllowedRoles = ["Administrator", "Admin"];

        //Eval
        arrFollowUpActions = eval(arrFollowUpActions);

        //Merge service action, roles and button template
        if (arrFollowUpActions.length > 0) {
            for (i = 0; i < arrFollowUpActions.length; i++) {
                //Add template buttons based on the serviceStatus object type retrieved through the FileServiceStatus call
                arrServiceSpecificButtons.push(Object.create(serviceSpecificButtonTemplates[arrFollowUpActions[i].type]));
                //Merge all template and service specific database button properties
                for (var prop in arrFollowUpActions[i]) arrServiceSpecificButtons[i + iServiceSpecificButtons][prop] = arrFollowUpActions[i][prop];
                //Disable if active client role is not allowed
                var arrFlowAllowedRoles = arrFollowUpActions[i].activation.split(",").concat(arrAlwaysAllowedRoles);
                arrServiceSpecificButtons[i + iServiceSpecificButtons].arrFlowAllowedRoles = arrFlowAllowedRoles; //Add the always allowed roles to the flow roles
                arrServiceSpecificButtons[i + iServiceSpecificButtons].disabled = !eBook.Interface.isActiveRoleAllowed(arrFlowAllowedRoles) && !Ext.Array.similarity(arrFlowAllowedRoles, eBook.User.activeClientRoles); //disable button of both the activeRole and activeClientRole aren't within the allowed list
                arrServiceSpecificButtons[i + iServiceSpecificButtons].tooltip = "The client roles " + arrFlowAllowedRoles.join("/") + " can perform this action.";
            }
        }

        return arrServiceSpecificButtons;
    },
    //updates the status shown in the toolbar
    updateServiceStatus: function () {
        var me = this;
        //update html of status buttongroup
        me.fsServiceStatus.update('<div id="eBook-bundle-toolbar-status" class="eBook-bundle-toolbar-status">' + (this.serviceStatus.s ? this.serviceStatus.s : '') + '</div>');
    },
    //refresh fileservice log
    refreshLog: function (fileId, serviceId) {
        var me = this;

        me.fileServicelog.getComponent("dataviewLog").storeLoad(fileId, serviceId);
    },
    showPdfView: function (show) {
        var me = this;
        if (show) {
            if (me.bundle.Filepath) {
                me.bundlePDF.loadFile(me.bundle.Filepath)
                me.bundlePDF.show();
            }
        }
        else {
            //hide component
            me.bundlePDF.hide();
        }
    }
});
eBook.BizTax.Store = Ext.extend(Object, {
    constructor: function (config) {
        //this.listeners = config.listeners ? config.listeners : config;
        this.elementsByFields = {};
        this.fieldsByElements = {};
        this.biztax = config.biztax;
        this.serviceUrl = config.serviceUrl;
        this.moduleId = config.moduleId;
    }
    , activeTabs: []
    , data: {}
    , fields: {
        byElementPath: {}
        , byElementId: {}
        , contextGrids: {}
        , linkedContextGrids: {}
    }
    , indexes: {
        elementsByContextRef: {}
        , elementsById: {}
        , elementsByPath: {}
        , contextsByDefId: {}
        , contextsByRef: {}
        , contextsByParent: {}
    }
    , getDataContract: function () {
        return this.data;
    }
    , myServiceUrl: function (method) {
        return this.serviceUrl + this.biztax.assessmentyear + "/" + this.biztax.type + ".svc/" + method;
    }
    , loadData: function (callback, scope) {
        var me = this;
        Ext.Ajax.request({
            url: me.myServiceUrl("GetBizTax")
            , method: 'POST'
            , params: Ext.encode({ cbdc: {
                FileId: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
                    , Type: me.biztax.type
            }
            })
            , callback: me.onDataRetreived
            , scope: me
            , callerCallback: { fn: callback, scope: scope }
        });
    }
    , onDataRetreived: function (opts, success, resp) {
        var robj, mod = this.getModule()
        if (!success) {
            eBook.Interface.showResponseError(resp, 'Failed retrieving Biztax data.');
            opts.callerCallback.fn.call(opts.callerCallback.scope || this);
            return null;
        }
        robj = Ext.decode(resp.responseText);
        this.data = robj.GetBizTaxResult;

        if (this.data.WorksheetsNewer) {
            this.updateFromWorksheets(opts.callerCallback);
            return;
        }

        mod.preRenderTabs(Ext.pluck(this.data.Data.Fiches, "FicheId"));


        // clear existing data
        // copy global info
        this.clearErrors();
        this.indexData();
        this.setErrors(this.data.Data.Errors);
        opts.callerCallback.fn.call(opts.callerCallback.scope || this);

    }
    , updateFromWorksheets: function () {
        this.getModule().getEl().mask("Updating worksheet info");
        eBook.CachedAjax.request({
            url: eBook.Service.rule(eBook.Interface.currentFile.get('AssessmentYear')) + 'UpdateBizTax'
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.Culture} })
                , callback: function () {
                    var mod = this.getModule();
                    mod.loadBizTax('' + eBook.Interface.currentFile.get('AssessmentYear'), mod.biztaxType)
                }
                , started: new Date().getTime()
                , scope: this
        });
    }


    , registerContextGrid: function (cg, contextid, linkedids) {
        var cgid;
        if (!Ext.isArray(linkedids)) {
            linkedids = [linkedids];
        }

        this.fields.contextGrids[contextid] = {
            cmpId: cg.id
        };

        this.fields.linkedContextGrids[contextid] = linkedids


        return this.getContextGridData(contextid);
    }
    , updateLinkedContextGrids: function (contextid) {
        var ctxids = this.fields.linkedContextGrids[contextid];
        if (ctxids.length == 0) {
            return;
        }
        for (var j = 0; j < ctxids.length; j++) {
            if (ctxids[j] != contextid && this.fields.contextGrids[ctxids[j]]) {
                Ext.getCmp(this.fields.contextGrids[ctxids[j]].cmpId).reload();
            }
        }
    }
    , getContextGridData: function (contextid) {
        var ctxids = this.fields.linkedContextGrids[contextid];
        var ids = {};
        var res = [];
        for (var k = 0; k < ctxids.length; k++) {
            cgid = ctxids[k];
            if (this.indexes.contextsByParent[cgid]) {
                for (var i = 0; i < this.indexes.contextsByParent[cgid].length; i++) {
                    var did = this.indexes.contextsByParent[cgid][i];
                    if (!ids['_' + did]) {
                        ids['_' + did] = true;
                        var rs = { defid: did, data: {} };
                        var ctx = null;
                        var ctxIdxs = this.indexes.contextsByDefId[did];
                        for (var j = 0; j < ctxIdxs.length; j++) {
                            var ct = this.data.Data.Contexts[ctxIdxs[j]];
                            var pr = ct.Id.split('_')[0];
                            if (ctx == null) {
                                ctx = ct;
                            }
                            rs[pr] = ct;
                        }
                        for (var j = 0; j < ctx.Scenario.length; j++) {
                            rs.data[ctx.Scenario[j].Dimension.replace(":", "_")] = ctx.Scenario[j].Value;
                        }

                        res.push(rs);
                    }
                }
            }
        }
        return res;
    }

    , registerField: function (fld, contextgrid) {
        var fpath = fld.defattribs["data-xbrl-path"] + "/" + fld.defattribs["data-xbrl-id"];
        var xbrlId = fld.defattribs["data-xbrl-id"];
        var xtype = fld.getXType() || fld.xtype;
        if (xtype == "combo") fpath = fld.defattribs["data-xbrl-path"] + "/" + fld.defattribs["data-xbrl-name"];

        if (fld.elementPath && this.fields.byElementPath[fld.elementPath]) {
            delete this.fields.byElementPath[fld.elementPath];
        }
        if (!this.fields.byElementPath[fpath]) this.fields.byElementPath[fpath] = [];
        fld.elementPath = fpath;

        if (this.errFields['_' + xbrlId]) {
            fld.markInvalid(this.errFields['_' + xbrlId]);
            this.getModule().setTabInvalid(fld.tabCode);
        }

        this.fields.byElementPath[fpath].push({
            cmpId: fld.id
            , elPath: fpath
            , xtype: fld.xtype
            , elId: fld.defattribs["data-xbrl-id"]
        });

        this.setFieldValueListener(fld);
        var el = null;
        var val = null;
        if (this.data.Data) {
            el = this.getElementByPath(fpath);
            val = this.getElementValue(fpath, xtype);
        }
        if (xtype == "checkbox" && (val == null || val == "")) {
            this.onFieldChange(fld);
        }

        if (!fld.isCalculated) {
            if ((el && el.Locked) || this.data.Locked) {
                fld.disable();
            } else {
                fld.enable();
            }
        }

        if (xtype == 'biztax-ValuelistOrOther') {
            return el;
        }
        return val;
    }
    , removeContextsByDefId: function (defId, flds) {
        var ctxIdxs = this.indexes.contextsByDefId[defId];
        var elidxs = [];

        if (!ctxIdxs) return;
        for (var j = 0; j < ctxIdxs.length; j++) {
            var ctx = this.data.Data.Contexts[ctxIdxs[j]];
            var idxs = this.indexes.elementsByContextRef[ctx.Id];
            if (idxs) {
                elidxs = elidxs.concat(idxs);
            }
        }

        /*
        for (var j = 0; j < elidxs.length; j++) {
        var 
        var elidx = this.indexes.elementsByPath[fld.elementPath];
        if (Ext.isDefined(elidx)) {
        if (Ext.isArray(elidx)) {
        elidxs.push(elidx[0]);
        } else {
        elidxs.push(elidx);
        }
        delete this.fields.byElementPath[fld.elementPath];
        }
        }
        */


        ctxIdxs.sort(function sortfunction(a, b) { return (a - b); });
        elidxs.sort(function sortfunction(a, b) { return (a - b); });

        for (var i = elidxs.length - 1; i > -1; i--) {
            this.data.Data.Elements.splice(elidxs[i], 1);
        }

        for (var i = ctxIdxs.length - 1; i > -1; i--) {
            this.data.Data.Contexts.splice(ctxIdxs[i], 1);
        }

        this.indexData();
        this.triggerSave();
    }

    , setFieldValueListener: function (fld) {
        var xtype = fld.getXType() || fld.xtype;

        if (xtype) {
            var listType = xtype.indexOf('check') > -1 ? 'check' : xtype.indexOf('combo') > -1 ? 'select' : 'change';
            fld.addListener(listType, this.onFieldChange, this, { buffer: 300 });
        }
    }
    , onFieldChange: function (fld) {
        if (!this.data || !this.data.Data) return;
        if (this.data.Data && this.data.Locked) return;
        var el = this.getElementByPath(fld.elementPath);
        var xtype = fld.getXType() || fld.xtype;
        var value = fld.getValue();

        if (value && value != '' && xtype == 'datefield') {
            value = value.format('Y-m-d');
        }

        if (!el) {
            el = this.createElementFromField(fld);
        }


        if (xtype == "biztax-uploadfield") {
            el.BinaryValue = Ext.clone(value);
            el.Value = null;
        } if (xtype == "biztax-ValuelistOrOther") {
            this.setValueListOrOtherValue(el, value, fld);
        } if (xtype == "combo") {
            this.setComboValueValue(el, fld);
        } else {
            el.Value = "" + (value == null ? "" : value);
        }
        this.updateFields(fld.elementPath, el)
        this.triggerSave();

    }
    , setValueListOrOtherValue: function (el, values, fld) {
        el.Children = [];
        for (var i = 0; i < values.fields.length; i++) {
            var mfld = values.fields[i];
            if (values.tpe == "valuelist") {
                var val = mfld.getValue();
                if (val != "" && val != null) {
                    val = val.split(':');
                    var valp = val[1].split('_');
                    el.Children.push({
                        AutoRendered: false
                    , Children: null
                    , BinaryValue: null
                    , Context: ""// fld.defattribs['data-xbrl-context']
                    , ContextRef: "D"
                    , Decimals: null
                    , Id: mfld.getValue().replace(':', '_') + "_" + mfld.defattribs['data-xbrl-period']
                    , Name: val[1]
                    , FullName: mfld.getValue()
                    , NameSpace: ""
                    , Period: mfld.defattribs['data-xbrl-period']
                    , Prefix: "pfs-vl"
                    , UnitRef: null
                    , Value: valp[valp.length - 1]
                    });
                }
            } else {
                el.Children.push({
                    AutoRendered: false
                    , Children: null
                    , BinaryValue: null
                    , Context: mfld.defattribs['data-xbrl-context']
                    , ContextRef: mfld.defattribs['data-xbrl-period']
                    , Decimals: mfld.defattribs['data-xbrl-decimals']
                    , Id: mfld.defattribs['data-xbrl-id']
                    , Name: mfld.defattribs['data-xbrl-name']
                     , FullName: mfld.defattribs['data-xbrl-prefix'] + ":" + mfld.defattribs['data-xbrl-name']
                    , NameSpace: ""
                    , Period: mfld.defattribs['data-xbrl-period']
                    , Prefix: mfld.defattribs['data-xbrl-prefix']
                    , UnitRef: mfld.defattribs['data-xbrl-unitref']
                    , Value: mfld.getValue()
                });
            }
        }
    }
    , setComboValueValue: function (el, fld) {
        var val = fld.getValue().split(':');
        var valp = val[1].split('_');
        el.Children = [{
            AutoRendered: false
            , Children: null
            , BinaryValue: null
            , Context: ""// fld.defattribs['data-xbrl-context']
            , ContextRef: "D"
            , Decimals: null
            , Id: fld.getValue().replace(':', '_') + "_" + fld.defattribs['data-xbrl-period']
            , Name: val[1]
            , NameSpace: ""
            , Period: fld.defattribs['data-xbrl-period']
            , Prefix: "pfs-vl"
            , UnitRef: null
            , Value: valp[valp.length - 1]
        }
        ];

    }
    , getMeta: function () {
        return {
            FileId: this.data.FileId,
            Type: this.data.Type,
            Calculation: this.data.Calculation,
            Validated: this.data.Validated,
            Status: this.data.Status,
            Locked: this.data.Status > 0,
            department: this.data.department,
            partnerId: this.data.partnerId,
            partnerName: this.data.partnerName,
            person: this.data.FileId
        };
    }
    , saveMeta: function (reloadData, cb) {
        var module = this.getModule();
        var scope = this;
        if (reloadData) {
            module.getEl().mask("Updating meta and reloading biztax data");
            cb = function () { this.loadData(this.getModule().onDataLoaded, this.getModule()); };
        } else {
            if (cb == null) {
                cb = function () {

                }; //dummy
            }
            else {
                scope = cb.scope;
                cb = cb.fn;
            }
        }


        Ext.Ajax.request({
            url: this.myServiceUrl("UpdateBizTaxMetaData")
            , method: 'POST'
            , params: Ext.encode({ fxidc: this.getMeta() })
            , callback: cb
            , scope: scope
            // , callerCallback: { fn: module.onDataLoaded, scope: module }
        });


    }
    , save: function () {
        var ip = this.getModule().getTopToolbar().infoPane;
        ip.clearSaveWait();
        ip.setSaveProgress();
        Ext.Ajax.request({
            url: this.myServiceUrl("SaveBizTax")
            , method: 'POST'
            , params: Ext.encode({ fxdc: this.data })
            , callback: this.onSaved
            , scope: this
            //, callerCallback: { fn: callback, scope: scope }
        });

    }
    , errFields: {}
    , clearErrors: function () {
        var mod = this.getModule(),
            fels = mod.getEl().query('.fld-biztax-error'),
            i = 0;
        this.errFields = {};
        for (i = 0; i < fels.length; i++) {
            Ext.getCmp(fels[i].id).clearInvalid();
        }
    }
    , setFieldError: function (mod, xbrlId, msg) {
        var dels = mod.getEl().query('div[data-xbrl-id=' + xbrlId + ']'),
            mtab = null;

        if (!this.errFields['_' + xbrlId]) {
            this.errFields['_' + xbrlId] = msg;
        }
        for (var i = 0; i < dels.length; i++) {
            dels[0] = Ext.get(dels[0]);
            var iel = dels[0].child('input');
           
            if (iel) {
                iel = Ext.getCmp(iel.id);
                iel.markInvalid(msg);
                mod.setTabInvalid(iel.tabCode);
            }
        }
    }
    , setErrors: function (errors) {
        var mod = this.getModule(),
            vitals = mod.getTopToolbar().vitals,
            i = 0, j = 0, err;

        mod.clearTabErrors();
        this.clearErrors();
        var cultidx = eBook.Interface.Culture == 'fr-FR' ? 1 : 0;

        for (i = 0; i < errors.length; i++) {
            err = errors[i];
            var msg = err.Messages[cultidx].Message;
            for (j = 0; j < err.Fields.length; j++) {
                this.setFieldError(mod, err.Fields[j], msg);
            }
        }
        //this.getModule().getTopToolbar().validationPane.updateMe(errors);
        if (errors.length > 0) {
            vitals.getEl().mask('validation errors');
            if (Ext.query('.errorCount').length == 0) {
                Ext.query('.eBook-Warning-ico-24')[0].outerHTML += "<div class='errorCount'>" + errors.length + "</div>";
            } else {
                Ext.query('.errorCount')[0].innerText = errors.length
            }
        } else {
            vitals.getEl().unmask(true);
            if (Ext.query('.errorCount').length != 0) {
                Ext.get(Ext.query('.errorCount')[0]).remove();
            }
        }
    }
    , onSaved: function (opts, success, resp) {
        var robj = Ext.decode(resp.responseText);
        var res = robj.SaveBizTaxResult;
        this.data.Data.Errors = res.Errors;
        this.data.Data.TaxCalc = res.TaxCalc;
        this.setErrors(res.Errors);

        //alert(res.Errors.length + " errors in taxonomy");
        for (var i = 0; i < res.Elements.length; i++) {
            var elPath = "/" + res.Elements[i].Name + "_" + res.Elements[i].ContextRef;
            var el = null;
            if (!this.indexes.elementsByPath[elPath]) {
                this.data.Data.Elements.push(res.Elements[i]);
                el = this.data.Data.Elements[this.data.Data.Elements.length - 1];
                this.addElementToIndex(el, this.data.Data.Elements.length - 1, "");
            } else {
                el = this.getElementByPath(elPath);
                el.Value = res.Elements[i].Value;
            }
            this.updateFields(elPath, el)
        }

        var ip = this.getModule().getTopToolbar().infoPane;
        ip.setCalc(this.data.Data.TaxCalc);
        ip.setSave(new Date());
        //alert("saved " + success);
    }
    , triggerSave: function () {
        if (!this.saveTask) {
            this.saveTask = new Ext.util.DelayedTask(function () {
                //this.getModule().saving();
                this.save();
                // this.saveTask.cancel();
            }, this);
        }
        this.saveTask.cancel();
        this.saveTask.delay(500);
        this.getModule().getTopToolbar().infoPane.setSaveWait();
    }
    , cancelSaveTrigger: function () {
        this.saveTask.cancel();
    }
    , getModule: function () {
        return Ext.getCmp(this.moduleId);
    }
    , createElementFromField: function (fld, value) {
        var cref = fld.defattribs['data-xbrl-id'].split('_');
        var xtype = fld.getXType() || fld.xtype;
        cref.shift();
        cref = cref.join('_');
        var el = {
            AutoRendered: false
            , Children: null
            , BinaryValue: null
            , Context: xtype == 'combo' ? "" : fld.defattribs['data-xbrl-context']
            , ContextRef: xtype == 'combo' ? "" : cref
            , Decimals: fld.defattribs['data-xbrl-decimals']
            , Id: xtype == 'combo' ? fld.defattribs['data-xbrl-name'] : fld.defattribs['data-xbrl-id']
            , Name: fld.defattribs['data-xbrl-name']
            , NameSpace: ""
            , Period: xtype == 'combo' ? "" : fld.defattribs['data-xbrl-period']
            , Prefix: fld.defattribs['data-xbrl-prefix']
            , UnitRef: fld.defattribs['data-xbrl-unitref']
            , Value: null
        };

        /* if (xtype != 'combo' && !this.indexes.contextsByRef[cref]) {
        // create context
        this.createContexts(defId, dimensions, oldRefs, contextId)
        }
        */

        var eps = fld.elementPath.split('/');
        eps.shift(); // clear out start
        eps.pop();
        if (eps.length > 0) {
            var arr = [];
            var path = "";
            var prevpath = "";
            var pel = null;

            for (var i = 0; i < eps.length; i++) {
                path += "/" + eps[i];
                var mel = this.getElementByPath(path);
                if (!mel) {
                    mel = {
                        AutoRendered: false
                    , Children: []
                    , BinaryValue: null
                    , Context: null
                    , ContextRef: null
                    , Decimals: null
                    , Id: eps[i]
                    , Name: eps[i]
                    , NameSpace: ""
                    , Period: null
                    , Prefix: "pfs-gcd"
                    , UnitRef: null
                    , Value: null
                    };
                    if (pel == null) {
                        arr.push(this.data.Data.Elements.length);
                        this.addElementToIndex(mel, this.data.Data.Elements.length, "");
                        this.data.Data.Elements.push(mel);
                        pel = mel;
                    } else {
                        this.addElementToIndex(mel, pel.Children.length, prevpath, arr.slice(0));
                        arr.push(pel.Children.length);
                        pel.Children.push(mel);
                        pel = mel;
                    }
                } else {
                    var parr = this.indexes.elementsByPath[path];
                    if (!Ext.isArray(parr)) parr = [parr];
                    arr = parr.slice(0);
                    pel = mel;
                }
                prevpath = path;

            }
            var idxel = pel.Children.length;

            pel.Children.push(el);
            this.addElementToIndex(el, idxel, path, arr);

        }
        else {
            this.addElementToIndex(el, this.data.Data.Elements.length, "");
            this.data.Data.Elements.push(el);
        }
        return el;
    }
    , getElementValue: function (fpath, xtype) {
        var el = this.getElementByPath(fpath);
        if (el && xtype == 'biztax-ValuelistOrOther') return el;
        if (el && xtype == 'biztax-uploadfield') return el.BinaryValue;
        if (el && xtype != 'combo') return el.Value;
        if (el && xtype == 'combo' && el.Children != null && el.Children.length > 0) {
            return el.Children[0].Prefix + ":" + el.Children[0].Name;
        }
        return null;
    }
    , getElementByPath: function (fpath) {
        return this.getElementAtIndex(this.indexes.elementsByPath[fpath]);
    }
    , getElementAtIndex: function (indxes, parent) {
        var idxes = indxes;
        var id = -1;
        if (Ext.isEmpty(idxes)) return null;
        if (Ext.isArray(idxes)) idxes = indxes.slice(0);
        if (Ext.isArray(idxes) && idxes.length == 1) {
            idxes = idxes[0]
        }
        if (Ext.isArray(idxes)) {
            id = parseInt(idxes.shift());
            if (parent) {
                return this.getElementAtIndex(idxes, parent.Children[id]);
            } else {
                return this.getElementAtIndex(idxes, this.data.Data.Elements[id]);
            }
        } else {
            id = parseInt(idxes);
            if (parent) {
                return parent.Children[id];
            } else {
                return this.data.Data.Elements[id];
            }
        }
    }
    , getElementsByIndexes: function (arrIdxs) {
        if (!arrIdxs) return [];
        var res = [];
        for (var i = 0; i < arrIdxs.length; i++) {
            res.push(this.getElementAtIndex(arrIdxs[i]));
        }
        return res;
    }
    , resetIndexes: function () {

    }
    , indexData: function () {
        //this.resetIndexes();
        this.indexContexts();
        this.indexElements();

    }
    , addIdx: function (idxName, id, idx) {
        if (!this[idxName][id]) this[idxName][id] = [];
        this[idxName][id].push(idx);
    }
    , indexContexts: function () {
        this.indexes.contextsByDefId = {};
        this.indexes.contextsByRef = {};
        this.indexes.contextsByParent = {};
        for (var i = 0; i < this.data.Data.Contexts.length; i++) {
            var ctx = this.data.Data.Contexts[i];
            if (!this.indexes.contextsByDefId[ctx.DefId]) {
                this.indexes.contextsByDefId[ctx.DefId] = [];
            }
            this.indexes.contextsByDefId[ctx.DefId].push(i);
            if (!(ctx.Id.indexOf('_') == -1 && ctx.Scenario != null && ctx.Scenario.length > 0)) {
                this.indexes.contextsByRef[ctx.Id] = i;
            }

            if (!this.indexes.contextsByParent[ctx.PresContextId]) {
                this.indexes.contextsByParent[ctx.PresContextId] = [ctx.DefId];
            } else {
                var addpres = true;
                for (var j = 0; j < this.indexes.contextsByParent[ctx.PresContextId].length; j++) {
                    if (this.indexes.contextsByParent[ctx.PresContextId][j] == ctx.DefId) {
                        addpres = false;
                        break;
                    }
                }
                if (addpres) {
                    this.indexes.contextsByParent[ctx.PresContextId].push(ctx.DefId);
                }
            }
        }
    }
    , getContextByRef: function (ref) {
        var idx = this.indexes.contextsByRef[ref];
        if (Ext.isNumber(idx)) {
            return this.data.Data.Contexts[idx];
        }
        return null;
    }
    , indexElements: function () {
        this.indexes.elementsByPath = {};
        this.indexes.elementsByContextRef = {};
        this.indexes.elementsById = {};
        for (var i = 0; i < this.data.Data.Elements.length; i++) {
            var el = this.data.Data.Elements[i];
            this.addElementToIndex(el, i, "", null, true);
        }
    }
    , addElementToIndex: function (el, i, path, arr, update) {
        var path = path + "/" + el.Name + (!Ext.isEmpty(el.ContextRef) ? "_" + el.ContextRef : "");
        if (!this.indexes.elementsByContextRef[el.ContextRef]) {
            this.indexes.elementsByContextRef[el.ContextRef] = [];
        }

        if (arr) {
            arr.push(i);
            this.indexes.elementsByPath[path] = arr;
            this.indexes.elementsByContextRef[el.ContextRef].push(arr);
            this.indexes.elementsById[el.Id] = arr;
        } else {
            arr = [i];
            this.indexes.elementsByPath[path] = i;
            this.indexes.elementsByContextRef[el.ContextRef].push(i);
            this.indexes.elementsById[el.Id] = i;
        }
        if (el.Children != null && el.Children.length > 0) {
            for (var j = 0; j < el.Children.length; j++) {
                this.addElementToIndex(el.Children[0 + j], 0 + j, "" + path, arr.slice(0), update);
            }

        }
        if (update) { this.updateFields(path, el); }

    }
    , updateFields: function (path, el) {
        var flds = this.fields.byElementPath[path];
        if (flds) {
            for (var t = 0; t < flds.length; t++) {
                var f = Ext.getCmp(flds[t].cmpId);
                if (f.xtype == 'combo') {
                    if (el.Children != null && el.Children.length > 0) {
                        f.setValue(el.Children[0].Prefix + ":" + el.Children[0].Name);
                    }
                } else if (f.xtype == 'biztax-ValuelistOrOther') {
                    f.setValue(el);
                } else if (f.xtype == 'biztax-uploadfield') {
                    f.setValue(Ext.clone(el.BinaryValue));
                } else {
                    f.setValue(el.Value);
                }
                if (!f.isCalculated) {
                    if ((el && el.Locked) || this.data.Locked) {
                        f.disable();
                    } else {
                        f.enable();
                    }
                }
            }
        }
    }
    , updateContexts: function (defId, dimensions, oldRefs, contextId, flds) {

        var ctxs = this.createContexts(defId, dimensions, oldRefs, contextId);
        var periods = ['D', 'I-Start', 'I-End'];

        for (var i = 0; i < periods.length; i++) {
            var exist = this.indexes.contextsByRef[ctxs[periods[i]].Id];
            if (Ext.isDefined(exist) && exist != null && exist > -1) {
                var ctxEx = this.data.Data.Contexts[exist];
                if (ctxEx.DefId != defId) {
                    return false;
                }
            }
        }

        this.removeContextsByDefId(defId, flds);
        for (var i = 0; i < periods.length; i++) {

            if (ctxs[periods[i]]) {
                this.data.Data.Contexts.push(ctxs[periods[i]]);
            }
        }


        this.indexContexts();
        this.triggerSave();
        return ctxs;
    }
    , removeContextIndexes: function (refid) {
        var idxs = [];
        if (this.indexes.elementsByContextRef[refid]) {
            idxs = this.indexes.elementsByContextRef[refid];
            var els = this.getElementsByIndexes(idxs);
            for (var i = 0; i < els.length; i++) {
                delete this.fields.byElementPath["/" + els[i].Name];
                delete this.indexes.elementsByPath["/" + els[i].Name];
                delete this.indexes.elementsById[els.Id];
            }
            delete this.indexes.elementsByContextRef[refid];
            idxs.sort(function sortfunction(a, b) { return (a - b); });

            for (var i = idxs.length - 1; i > -1; i--) {
                var eli = idxs[i];
                if (Ext.isArray(eli)) eli = eli[0];
                this.data.Data.Elements.splice(eli, 1);
            }
        }

        delete this.indexes.contextsByRef[refid];

    }
    , cleanerRegEx: /[^a-zA-Z0-9\-]/gi
    , createContexts: function (defId, dimensions, oldRefs, contextId) {
        if (!Ext.isArray(dimensions)) dimensions = [dimensions];

        var scenario = [];
        var mainId = '';
        var d = Ext.clone(this.getContextByRef("D"));
        var istart = Ext.clone(this.getContextByRef("I-Start"));
        var iend = Ext.clone(this.getContextByRef("I-End"));

        for (var i = 0; i < dimensions.length; i++) {
            var ds = dimensions[i];
            var scen = { Dimension: ds.dimension };
            if (!ds.value) ds.value = "";
            var idVal = '';
            switch (ds.type) {
                case "typedMember":
                    scen.__type = 'ContextScenarioTypeDataContract:#EY.com.eBook.BizTax';
                    scen.Value = ds.value;
                    scen.Name = ds.type;
                    scen.Type = ds.memberType;
                    idVal = scen.Value.replace(this.cleanerRegEx, '');
                    break;
                case "explicitMember":
                    scen.__type = 'ContextScenarioExplicitDataContract:#EY.com.eBook.BizTax';
                    scen.Value = ds.value;
                    scen.Name = ds.type;
                    idVal = scen.Value.split(':')[1];
            }
            mainId += '__id__' + idVal;
            scenario.push(scen);
        }


        d.Scenario = scenario;
        d.Id = 'D' + mainId;
        d.DefId = defId;
        d.PresContextId = contextId;

        istart.Scenario = scenario;
        istart.Id = 'I-Start' + mainId;
        istart.DefId = defId;
        istart.PresContextId = contextId;

        iend.Scenario = scenario;
        iend.Id = 'I-End' + mainId;
        iend.DefId = defId;
        iend.PresContextId = contextId;

        result = {
            'D': d
            , 'I-Start': istart
            , 'I-End': iend
        };

        return result;
    }


    // OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD
    , clearElementIndexes: function () {
        this.elementsByPeriod = {};
        this.elementsByNamePeriod = {};
        this.elementsByContext = {};
        this.elementsByNameContext = {};
        this.elementsByNamePeriodContext = {}; //unique
        this.elementsByName = {};
    }
    , clearData: function () {
        this.biztaxGeneral = {};
        this.contexts = {};
        this.elements = [];
        this.clearElementIndexes();
        //        this.elementsByFields = {};
        //        this.fieldsByElements = {};
    }

    , getElementValueById: function (id, xtype) {
        var el = this.getElementById(id);
        return this.retrieveElementValue(el, xtype);
    }
    , retrieveElementValue: function (el, xtype) {
        if (!el) return null;
        if (!Ext.isEmpty(el.Decimals)) {
            return Ext.isEmpty(el.Value) ? 0 : parseFloat(el.Value);
        }
        if (xtype == "biztax-uploadfield") {
            return Ext.clone(el.BinaryValue);
        }
        if (xtype == "combo") {
            if (el.Children != null && el.Children.length > 0) {
                return el.Children[0].Prefix + '_' + el.Children[0].Name;
            }
        }
        //  if (el.Value == "true" || el.Value == "false") return eval(el.Value);
        return el.Value;
    }
    , getNumericElementValueById: function (id) {
        var val = this.getElementValueById(id);
        if (val == null) return 0;
        return val;
    }
    , addOrUpdateElement: function (cfg) {
        if (!this.elementsByNamePeriodContext[cfg.id]) {
            this.addElement(this.createElementByCalcCfg(cfg));
        } else {
            this.updateElement(cfg.id, cfg.value, '', null, false); //true  
        }
    }
    , updateElement: function (idxorEl, value, xtype, fld, donotoverwrite) {
        if (Ext.isString(idxorEl)) {
            idxorEl = this.elementsByNamePeriodContext[idxorEl];
            if (idxorEl == null || !Ext.isDefined(idxorEl)) return;
        }
        if (Ext.isObject(idxorEl)) {
            // for child elements.
            if (idxorEl.AutoRendered && donotoverwrite) return;
            if (xtype == "biztax-uploadfield") {
                idxorEl.BinaryValue = Ext.decode(Ext.encode(value));
                idxorEl.Value = null;
            } if (xtype == "combo") {
                var child = this.createComboValueElement(fld);
                idxorEl.Children = child ? [child] : [];
            } else {
                idxorEl.Value = "" + (value == null ? "" : value);
            }
            idxorEl.AutoRendered = false;
            return idxorEl;
        } else {
            if (this.elements[idxorEl].AutoRendered && donotoverwrite) return;
            if (xtype == "biztax-uploadfield") {
                this.elements[idxorEl].BinaryValue = Ext.decode(Ext.encode(value));
                this.elements[idxorEl].Value = null;
            } if (xtype == "combo") {
                var child = this.createComboValueElement(value);
                this.elements[idxorEl].Children = child ? [child] : [];
            } else {
                this.elements[idxorEl].Value = "" + (value == null ? "" : value);
            }
            this.elements[idxorEl].AutoRendered = false;
        }
    }
    , findElementsByIndex: function (idxName, id) {
        if (!this[idxName]) return [];
        if (!this[idxName][id]) return [];
        return this.getElementsByIndexes(this[idxName][id]);
    }
    , getElementById: function (id) {
        var res = this.findElementsForId(id);
        if (res.length == 0) return null;
        return res[0];
    }
    , findElementsForId: function (id) {
        return this.findElementsByIndex('elementsByNamePeriodContext', id);
    }
    , findElementsForName: function (fullname) {
        return this.findElementsByIndex('elementsByName', fullname);
    }
    , findElementsForPeriod: function (period) {
        return this.findElementsByIndex('elementsByPeriod', period);
    }
    , findElementsForNamePeriod: function (fullname, period) {
        return this.findElementsByIndex('elementsByNamePeriod', fullname + '_' + period);
    }
    , findElementsForContext: function (context) {
        return this.findElementsByIndex('elementsByContext', context);
    }
    , findElementsForNameContext: function (fullname, context) {
        return this.findElementsByIndex('elementsByNameContext', fullname + '_' + context);
    }

    , destroy: function () {
        delete this.data;
        delete this.fields;
        delete this.indexes;
        //        eBook.BizTax.Store.superclass.destroy.call(this);
    }
});
eBook.BizTax.ToolbarInfo = Ext.extend(Ext.Toolbar.Item, {
    constructor: function (config) {
        eBook.BizTax.ToolbarInfo.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function (ct, position) {
        this.autoEl = { cls: 'biztax-tb-info', children: [
                { cls: 'biztax-tb-status-text', html: eBook.BizTax.Status_0 }
                , { cls: 'biztax-tb-info-validation', html: '.....................' }
                 , { cls: 'biztax-tb-info-calc', html: '.....................' }
                , { cls: 'biztax-tb-info-save', html: '.....................' }
                ]
        };
        eBook.BizTax.ToolbarInfo.superclass.onRender.call(this, ct, position);
    },
    setValidationProgress: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-validation').update("validating...");
            this.el.child('.biztax-tb-info-validation').addClass('biztax-tb-info-validation-busy');
        }
    }
    ,
    setCalcProgress: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-calc').update("calculating...");
            this.el.child('.biztax-tb-info-calc').addClass('biztax-tb-info-calc-busy');
        }
    },
    setSaveProgress: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-save').update("saving...");
            this.el.child('.biztax-tb-info-save').addClass('biztax-tb-info-save-busy');
        }
    }
    , setSaveWait: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-validation').update('Waiting to save, wait 0.5s');
            this.el.child('.biztax-tb-info-validation').addClass('biztax-tb-info-validation-busy');
        }
    }
    , clearSaveWait: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-validation').update('...');
            this.el.child('.biztax-tb-info-validation').removeClass('biztax-tb-info-validation-busy');
        }
    }
     , setValidation: function (d, valid) {
         var t = d == null ? '.....................' : d.format('d/m/Y H:i:s');

         if (this.rendered) {
             this.el.child('.biztax-tb-info-validation').update(t);
             this.el.child('.biztax-tb-info-validation').removeClass('biztax-tb-info-validation-busy');
             if (!valid) {
                 this.el.child('.biztax-tb-info-validation').addClass('biztax-tb-info-validation-invalid');
             } else {
                 this.el.child('.biztax-tb-info-validation').removeClass('biztax-tb-info-validation-invalid');
             }
         } else {
             this.text = t;
         }
     }
    , setCalc: function (d) {
        var t = d == null ? '---' : Ext.util.Format.number(d, '0.000,00/i') + '€';
        eBook.Interface.center.fileMenu.setBizTax(d);
        
        if (this.rendered) {
            this.el.child('.biztax-tb-info-calc').removeClass('biztax-tb-info-calc-pos');
            this.el.child('.biztax-tb-info-calc').removeClass('biztax-tb-info-calc-neg');
            this.el.child('.biztax-tb-info-calc').addClass('biztax-tb-info-calc-' + (d <= 0 ? 'pos' : 'neg'));
            this.el.child('.biztax-tb-info-calc').update(t);
            this.el.child('.biztax-tb-info-calc').removeClass('biztax-tb-info-calc-busy');
        } else {
            this.text = t;
        }
    }
    , setSave: function (d) {
        var t = d == null ? '.....................' : d.format('d/m/Y H:i:s');
        if (this.rendered) {
            this.el.child('.biztax-tb-info-save').update(t);
            this.el.child('.biztax-tb-info-save').removeClass('biztax-tb-info-save-busy');
        } else {
            this.text = t;
        }
    }
});
Ext.reg('biztax-tb-info', eBook.BizTax.ToolbarInfo);

eBook.BizTax.ToolbarStatus = Ext.extend(Ext.Toolbar.Item, {
    constructor: function (config) {
        eBook.BizTax.ToolbarStatus.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function (ct, position) {
        //this.autoEl = { cls: 'biztax-tb-status', children: [{ cls: 'biztax-tb-status-text', html: eBook.BizTax.Status_0}] };

        this.autoEl = { children: [
           
             { cls: 'biztax-tb-proxy biztax-tb-noproxy',
                children: [{ cls: 'biztax-tb-proxy-content', html: 'PROXY: !! no proxy found, upload !!'}]
            }
            ]
        };

        eBook.BizTax.ToolbarStatus.superclass.onRender.call(this, ct, position);
    },
    setStatus: function (s) {
        var t = 'Status: ' + eBook.BizTax["Status_" + s];
        var cls = 'biztax-status-' + s;
        if (s > 0) cls += ' biztax-lock-16';
        if (this.rendered) {
            this.el.child('.biztax-tb-status-text').update(t);
            this.el.dom.className = 'biztax-tb-status ' + cls;
        } else {
            this.autoEl.cls = 'biztax-tb-status ' + cls;
            this.text = t;
        }
    }
});
Ext.reg('biztax-tb-status', eBook.BizTax.ToolbarStatus);

/*
children:[
{tag:'span',cls:'biztax-tb-proxy-path',html: '1. Final Deliverables\\B. Corporate Income Tax Return\\Proxy CITR\\' }
,{tag:'span',cls:'biztax-tb-proxy-file',html: '!! no proxy found, upload !!' }
]
}
*/
eBook.BizTax.ToolbarProxy = Ext.extend(Ext.Toolbar.Item, {
    constructor: function (config) {
        eBook.BizTax.ToolbarProxy.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function (ct, position) {
        this.autoEl = { cls: 'biztax-tb-proxy biztax-tb-noproxy',
            children: [{ cls: 'biztax-tb-proxy-content', html: 'PROXY: !! no proxy found, upload !!' }
            ]
        };
        eBook.BizTax.ToolbarProxy.superclass.onRender.call(this, ct, position);
    },
    setProxy: function (s) {
        var cls = 'biztax-tb-proxy';
        var t = 'PROXY: ';
        if (!s || s == '') {
            cls += ' biztax-tb-noproxy';
            t += '!! no proxy found, upload !!';
        } else {
            t += s;
        }
        if (this.rendered) {
            this.el.dom.className = cls;
            this.el.child('.biztax-tb-proxy-content').update(t);
        } else {
            this.autoEl.cls = cls;
            this.autoEl.children[1].html = t;
        }
    }
});
Ext.reg('biztax-tb-proxy', eBook.BizTax.ToolbarProxy);



eBook.BizTax.ToolbarValidation = Ext.extend(Ext.Toolbar.Item, {
    mainTpl: new Ext.XTemplate('<div class="eBook-BizTax-errorMgs-wrapper">' +
                                    '<tpl for=".">' +
                                        '<tpl for="Messages">' +
                                            '<tpl if="Culture==eBook.Interface.Culture">' +
                                                '<div class="eBook-BizTax-errorMg" ext:qtitle="{Id}"  ext:qtip="{Message}">{parent.Id}: {[this.FirstChars(values.Message)]}</div>' +
                                              '</tpl>' +
                                        '</tpl>' +
                                     '</tpl>' +
                                '</div>'
                                , { compiled: true, FirstChars: function (msg) { if (msg.length > 100) { return msg.substr(0, 100) + "..."; } else { return msg; } } }),
    constructor: function (config) {
        eBook.BizTax.ToolbarValidation.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    updateMe: function (errorMgs) {
        /* for (var i = 0; i < this.messages.length; i++) {
        this.errorMgs.push(this.messages[i].Messages[0].Message);
        }*/
        this.el.child('.biztax-tb-validation-body').update(this.mainTpl.apply(errorMgs));
    },
    onRender: function (ct, position) {
        //this.autoEl = { cls: 'biztax-tb-status', children: [{ cls: 'biztax-tb-status-text', html: eBook.BizTax.Status_0}] };

        this.autoEl = { cls: 'biztax-tb-validation'
                , children: [
                     { cls: 'biztax-tb-validation-state' }
                     , { cls: 'biztax-tb-validation-body' }
                ]
        };

        eBook.BizTax.ToolbarValidation.superclass.onRender.call(this, ct, position);
    }
    , setBusy: function () {
        this.el.mask('Save &amp; validation in progress');
    }
});
Ext.reg('biztax-tb-validation', eBook.BizTax.ToolbarValidation);eBook.BizTax.Contextgrid = Ext.extend(Ext.Component, {
    lines: []
    , indexes: {
        lineIds: {}
    }
    , initComponent: function () {
        var cgDef = Ext.get(this.domConfig);

        var cgIds = cgDef.getAttribute('data-xbrl-cg-ids');

        Ext.apply(this, {
            lineTpl: cgDef.child('lineTpl').child('tr').dom.innerHTML
            , renderTo: cgDef.parent()
            , autoEl: {
                tag: 'div', cls: 'biztax-contextgrid-container'
                , children: [
                    { tag: 'div', cls: 'biztax-contextgrid-body', html: cgDef.child('contextheading').dom.innerHTML
                    }
                    , { tag: 'div', cls: 'biztax-contextgrid-footer'
                       , children: [{ tag: 'div', cls: 'biztax-contextgrid-addbutton', html: 'Toevoegen'}]
                    }
                ]
            }
            , contextId: cgDef.getAttribute('data-xbrl-cg-parent-id')
            , contextLinkedIds: cgIds ? cgIds.split(',') : [cgDef.getAttribute('data-xbrl-cg-parent-id')]
            , dimensionSets: cgDef.getAttribute('data-xbrl-dimensionset').split('/')
        });



        if (!Ext.isArray(this.dimensionSets)) this.dimensionSets = [this.dimensionSets];
        cgDef.remove();
        eBook.BizTax.Contextgrid.superclass.initComponent.apply(this, arguments);
    }
    , afterRender: function (cnt) {
        eBook.BizTax.Contextgrid.superclass.afterRender.call(this);
        this.tableEl = this.el.child('.biztax-contextgrid-body table');
        var addBtnEl = this.el.child('.biztax-contextgrid-addbutton');
        addBtnEl.on('click', this.addLine, this);
        var res = this.getModule().store.registerContextGrid(this, this.contextId, this.contextLinkedIds);
        //this.clearLines();
        this.loadExisting(res);

    }
    , beforeRender: function () {
        eBook.BizTax.Contextgrid.superclass.beforeRender.call(this);
        this.clearLines();
    }
    , loadExisting: function (res) {
        if (res && res.length > 0) {
            for (var i = 0; i < res.length; i++) {
                this.addLine(null, null, null, res[i].defid, res[i]);
            }
        }
    }
    , reload: function () {
        this.getEl().mask('reloading items');
        this.clearLines();
        this.loadExisting(this.getModule().store.getContextGridData(this.contextId));
        this.getEl().unmask();
    }
    , removeAll: function () {
        this.tableEl.child('tbody').update("");
        this.lines = [];
    }

    , clearLines: function () {
        var lid;

        for (var i = 0; i < this.lines.length; i++) {
            lid = this.contextId + this.lines[i].myId;
            Ext.get(lid).remove();
        }
        this.lines = [];
        this.indexes.lineIds = {};
    }
    , getModule: function () {
        return Ext.getCmp(this.moduleId);
    }
    , addLine: function (evt, el, n1, id, contextData) {
        var myId = id ? id : eBook.NewGuid();

        if(myId == '6197ceba-7ee0-c7bb-ed6d-c0ea7b061e65')
        {
            console.log('6197ceba-7ee0-c7bb-ed6d-c0ea7b061e65');
        }

        else if(myId == '585b3b97-ec3c-e244-4f09-6f5ca4cd903d')
        {
            console.log('585b3b97-ec3c-e244-4f09-6f5ca4cd903d');
        }


        var tr = this.tableEl.child('tbody').createChild({ tag: 'tr', html: this.lineTpl, id: this.contextId + myId, 'data-id': this.lines.length, cls: 'biztax-cg-incomplete' });
        // var ctxs = this.getModule().store.createContexts(, myId);
        var line = {
            tr: Ext.get(tr)
            , myId: myId
            , fields: []
            , contextFields: []
            , incomplete: true
            , dimensions: []
            , contextRefs: {}
        };
        for (var i = 0; i < this.dimensionSets.length; i++) {
            var dm = { dimension: this.dimensionSets[i] };
            if (contextData) {
            }
            line.dimensions.push(dm);
        }

        line = this.applyFields(line, myId, contextData);

        this.indexes.lineIds['_' + myId] = this.lines.length;
        this.lines.push(line);

        if (!contextData) {
            for (var i = 0; i < line.contextFields.length; i++) {
                this.onContextFieldChange(line.contextFields[i]);
            }
        } else {
            this.lines[this.lines.length - 1].contextRefs = contextData;
            this.resetLineDims(this.lines[this.lines.length - 1]);
        }
    }
    , verifyRemoveLine: function (el) {
        var tg = el.target;
        Ext.Msg.show({
            title: 'Lijn verwijderen',
            msg: 'Bent u zeker dat u de geselecteerde lijn wenst te verwijderen?',
            buttons: Ext.Msg.YESNO,
            fn: function (btnid) {
                this.onRemoveLine(btnid, tg);
            },
            icon: Ext.MessageBox.QUESTION,
            scope: this
        });
    }
    , onRemoveLine: function (btnid, el) {
        if (btnid == 'yes') {
            this.getEl().mask('removing item');
            el = Ext.get(el);
            var idx = el.parent('tr').getAttribute('data-id');


            // remove contexts as well
            this.getModule().store.removeContextsByDefId(this.lines[idx].myId, this.lines[idx].fields);
            this.lines[idx].tr.remove();

            this.lines.splice(idx, 1);
            // reindex
            for (i = 0; i < this.lines.length; i++) {
                this.lines[i].tr.set({ 'data-id': i });
            }
            this.getEl().unmask();
            this.getModule().store.updateLinkedContextGrids(this.contextId);
        }
    }
    , applyFields: function (line, myId, contextData) {
        var fieldTypes = eBook.BizTax[this.bizTaxLocator].FieldTypes;
        var remButton = line.tr.child('.biztax-context-removeitem');
        remButton.on('click', this.verifyRemoveLine, this);
        var locked = this.getModule().store.data.Locked;


        var flds = line.tr.select('.biztax-field-wrapper').elements;
        for (var i = 0; i < flds.length; i++) {
            var cfg = { defattribs: this.getFieldAttribs(flds[i]), renderTo: flds[i], width: '90%'
                , contextDefId: myId
            };
            if (cfg.defattribs['data-xbrl-calc'] == 'true') {
                cfg.disabled = true;
            }
            if (cfg.defattribs['data-xbrl-fieldtype']) {

                if (contextData) {
                    var period = cfg.defattribs['data-xbrl-period'];
                    var cps = contextData[period].Id.split("__");
                    cps.shift();
                    var context = cps.join('__');
                    cfg.defattribs['data-xbrl-id'] = cfg.defattribs['data-xbrl-name'] + "_" + contextData[period].Id;
                    cfg.defattribs['data-xbrl-contextRef'] = contextData[period].Id;
                    cfg.defattribs['data-xbrl-context'] = context;
                }

                if (cfg.defattribs['data-xbrl-fieldtype'] == 'valuelist') {
                    Ext.apply(cfg, Ext.clone(fieldTypes[cfg.defattribs['data-xbrl-valuelistid']]));
                    var fld = Ext.create(cfg);
                    //    fld.on('select', this.onFieldChange, this);
                    line.fields.push(fld);
                } else {
                    Ext.apply(cfg, fieldTypes[cfg.defattribs['data-xbrl-fieldtype']]);
                    var fld = Ext.create(cfg);
                    //  fld.on('change', this.onFieldChange, this);
                    // this.registerField(fld);
                    line.fields.push(fld);
                }
                if (contextData) {
                    var val = this.registerField(fld);
                    if (val) fld.setValue(val);
                    if (fld.disabled) locked = true;
                }

            } else {
                flds[i] = Ext.get(flds[i]);
                var dimid = cfg.defattribs['data-xbrl-dimension-id'].replace(':', '_')
                if (flds[i].hasClass('biztax-contextfield')) {
                    var dxt = cfg.defattribs['data-xbrl-type'];
                    if (dxt == 'select') {
                        var lst = flds[i].query('div');
                        var dta = [];
                        for (var j = 0; j < lst.length; j++) {
                            dta.push({ id: lst[j].attributes['data-value'].value.replace(':', '_'), text: lst[j].innerText });

                        }
                        cfg.dimensionType = "explicitMember";
                        flds[i].update('');
                        Ext.apply(cfg, {
                            xtype: 'combo',
                            triggerAction: 'all',
                            forceSelection: true,
                            autoSelect: true,
                            allowBlank: false,
                            mode: 'local',
                            valueField: 'id',
                            displayField: 'text',
                            store: { xtype: 'jsonstore', autoDestroy: true, data: dta, idProperty: 'id', fields: ['id', 'text'] }
                        });
                        if (dta.length == 1) {
                            cfg.value = dta[0].id;
                        }
                        if (contextData && contextData.data[dimid]) {
                            cfg.value = contextData.data[dimid].replace(':', '_');
                        }
                        var fld = Ext.create(cfg);
                        fld.on('select', this.onContextFieldChange, this);
                        line.contextFields.push(fld);
                    } else {
                        Ext.apply(cfg, fieldTypes[cfg.defattribs['data-xbrl-datatype']]);
                        cfg.allowBlank = false;
                        cfg.dimensionType = "typedMember";
                        if (contextData && contextData.data[dimid]) {
                            cfg.value = contextData.data[dimid];
                        }
                        var fld = Ext.create(cfg);
                        fld.on('change', this.onContextFieldChange, this);
                        //this.registerField(fld);
                        line.contextFields.push(fld);
                    }
                }

            }
            Ext.get(flds[i]).addClass("biztax-field-rendered");
        }
        for (var k = 0; k < line.contextFields.length; k++) {
            if (locked) {
                line.contextFields[k].disable();
            } else {
                line.contextFields[k].enable();
            }
        }
        if (locked) {
            remButton.hide();
        } else {
            remButton.show();
        }

        return line;
    }
    , resetLineDims: function (line) {
        var dims = [];
        for (var i = 0; i < line.contextFields.length; i++) {
            var fld = line.contextFields[i];
            var dim = { dimension: fld.defattribs['data-xbrl-dimension-id'], type: fld.dimensionType, value: fld.getValue() };
            if (dim.type == "explicitMember") {
                var sp = dim.value.split('_');
                dim.value = sp[0] + ":" + sp[1];
                for (var j = 2; j < sp.length; j++) {
                    dim.value = dim.value + "_" + sp[j];
                }
            }
            dim.memberType = dim.dimension.split(':')[0] + ':' + fld.defattribs['data-xbrl-datatype'];
            if (Ext.isDate(dim.value)) {
                dim.value = dim.value.format('Y-m-d');
            }
            dim.value = "" + dim.value;
            dims.push(dim);
        }
        line.dimensions = dims;
        var allDone = true;

        for (var i = 0; i < line.dimensions.length; i++) {
            if (line.dimensions[i].dimension == dim.dimension) {
                toAdd = false;
                line.dimensions[i] = dim;
            }
            if (!line.dimensions[i].value || Ext.isEmpty(line.dimensions[i].value.trim())) {
                allDone = false;
            }

        }

        if (allDone) {
            line.incomplete = false;
            line.tr.removeClass('biztax-cg-incomplete');
        }
    }
    , onContextFieldChange: function (fld) {
        var myId = fld.contextDefId;

        if(myId == '6197ceba-7ee0-c7bb-ed6d-c0ea7b061e65')
        {
            console.log('6197ceba-7ee0-c7bb-ed6d-c0ea7b061e65');
        }
        else if(myId == '585b3b97-ec3c-e244-4f09-6f5ca4cd903d')
        {
            console.log('585b3b97-ec3c-e244-4f09-6f5ca4cd903d');
        }

        var lineIdx = this.indexes.lineIds['_' + myId];
        var line = this.lines[lineIdx];
        var dim = { dimension: fld.defattribs['data-xbrl-dimension-id'], type: fld.dimensionType, value: fld.getValue() };
        if (dim.type == "explicitMember") {
            var sp = dim.value.split('_');
            dim.value = sp[0] + ":" + sp[1];
            for (var j = 2; j < sp.length; j++) {
                dim.value = dim.value + "_" + sp[j];
            }
        }
        dim.memberType = dim.dimension.split(':')[0] + ':' + fld.defattribs['data-xbrl-datatype'];
        if (!fld.isValid()) dim.value = "";
        if (Ext.isDate(dim.value)) {
            dim.value = dim.value.format('Y-m-d');
        }
        dim.value = "" + dim.value;
        var allDone = true;

        for (var i = 0; i < line.dimensions.length; i++) {
            if (line.dimensions[i].dimension == dim.dimension) {
                toAdd = false;
                line.dimensions[i] = dim;
            }
            if (!line.dimensions[i].value || Ext.isEmpty(line.dimensions[i].value.trim())) {
                allDone = false;
            }
        }

        if (allDone) { //&& line.incomplete

            var ctxResults = this.getModule().store.updateContexts(myId, line.dimensions, line.contextRefs, this.contextId, line.fields);
            if (ctxResults === false) {
                line.incomplete = true;
                line.tr.addClass('biztax-cg-incomplete');
                for (var i = 0; i < line.contextFields.length; i++) {
                    line.contextFields[i].markInvalid('Combination of contextfields can only occur once');
                }
            } else {
                line.incomplete = false;
                line.tr.removeClass('biztax-cg-incomplete');
                for (var i = 0; i < line.contextFields.length; i++) {
                    line.contextFields[i].clearInvalid();
                }

                for (var i = 0; i < line.fields.length; i++) {
                    var period = line.fields[i].defattribs['data-xbrl-period'];
                    var oldRef = line.contextRefs[period];
                    var cps = ctxResults[period].Id.split("__");
                    cps.shift();
                    var context = cps.join('__');
                    line.fields[i].defattribs['data-xbrl-id'] = line.fields[i].defattribs['data-xbrl-name'] + "_" + ctxResults[period].Id;
                    line.fields[i].defattribs['data-xbrl-contextRef'] = ctxResults[period].Id;
                    line.fields[i].defattribs['data-xbrl-context'] = context;
                    this.registerField(line.fields[i], oldRef);
                    this.getModule().store.onFieldChange(line.fields[i]);
                }
                line.contextRefs = ctxResults;
                this.getModule().store.updateLinkedContextGrids(this.contextId);

            }
        } else if (!allDone) {
            line.incomplete = true;
            line.tr.addClass('biztax-cg-incomplete');
        }


    }
    , getFieldAttribs: function (el) {
        var attribs = {};
        for (var attr, i = 0, attrs = el.attributes, l = attrs.length; i < l; i++) {
            attr = attrs.item(i);
            attribs[attr.nodeName] = attr.nodeValue;
        }
        return attribs;
    },
    registerField: function (el, oldref) {
        return this.getModule().store.registerField(el, oldref);
    }
    , onFieldChange: function (fld) {
        // this.getModule().store.onFieldChange(fld);
    }
    , destroy: function () {
        this.removeAll();
        this.clearLines();
        eBook.BizTax.Contextgrid.superclass.destroy.call(this);
    }
});eBook.BizTax.ValuelistOrOther = Ext.extend(Ext.Component, {
    xtype: 'biztax-ValuelistOrOther'
    , initComponent: function () {
        var cgDef = Ext.get(this.domConfig);
        Ext.apply(this, {
            applyTo: cgDef,
            defattribs: this.getFieldAttribs(cgDef)
        });

        eBook.BizTax.ValuelistOrOther.superclass.initComponent.apply(this, arguments);
    }
    , getFieldAttribs: function (el) {
        var attribs = {};
        if (el.dom) el = el.dom;
        for (var attr, i = 0, attrs = el.attributes, l = attrs.length; i < l; i++) {
            attr = attrs.item(i);
            attribs[attr.nodeName] = attr.nodeValue;
        }
        return attribs;
    }
    , afterRender: function (cnt) {
        eBook.BizTax.ValuelistOrOther.superclass.afterRender.call(this);
        this.tdVlEl = this.el.child('.biztax-valuelistorother-vl');
        this.defattribs["data-xbrl-prefix"] = this.tdVlEl.getAttribute('data-xbrl-prefix');
        this.tdVlEl.child('input').remove();
        var lstlbl = this.tdVlEl.dom.innerText;
        this.tdVlEl.update("");
        this.radioVl = new Ext.form.Radio({ renderTo: this.tdVlEl, name: this.id, value: 'LIST', boxLabel: lstlbl
            , tpe: 'LIST'
        });

        this.mon(this.radioVl, 'check', this.onRadioCheck, this);
        this.mon(this.radioVl, 'click', this.onRadioCheck, this);
        //this.radioVl.on('click', this.onRadioCheck, this);
        this.tdOtEl = this.el.child('.biztax-valuelistorother-other');
        this.tdOtEl.child('input').remove();
        lstlbl = this.tdOtEl.dom.innerText;
        this.tdOtEl.update("");
        this.radioOt = new Ext.form.Radio({ renderTo: this.tdOtEl, name: this.id, value: 'OTHER', boxLabel: lstlbl, tpe: 'OTHER'
        });
        this.mon(this.radioOt, 'check', this.onRadioCheck, this);

        this.applyFieldRendering();
        this.getModule().store.registerField(this);

        this.setActiveFields('valuelist');
    }
    , getValue: function () {
        var res = {
            tpe: this.activeTpe,
            fields: []
        };
        switch (this.activeTpe) {
            case 'valuelist':
                res.fields.push(this.valueField);
                break;
            default:
                for (var i = 0; i < this.otherFields.length; i++) {
                    res.fields.push(this.otherFields[i]);
                }
                break;
        }

        return res;
    }
    , onFieldChange: function (fld) {
        if (!this.suspended) this.getModule().store.onFieldChange(this);
    }
    , onRadioCheck: function (fld, chck) {
        if (chck) {
            var tpe = fld.tpe == "LIST" ? "valuelist" : "others";
            this.setActiveFields(tpe);
        }
    }
    , getModule: function () {
        return Ext.getCmp(this.moduleId);
    }
     , applyFieldRendering: function () {
         var fieldTypes = eBook.BizTax[this.getModule().locator].FieldTypes;
         this.otherFields = [];
         this.fields = [];
         var tdval = this.tdVlEl.parent('tr').child('td:nth-child(2)');
         var flds = this.el.select('.biztax-field-wrapper').elements;
         for (var i = 0; i < flds.length; i++) {
             var cfg = { defattribs: this.getFieldAttribs(flds[i]), renderTo: flds[i], width: '90%' };
             if (cfg.defattribs['data-xbrl-fieldtype']) {
                 if (cfg.defattribs['data-xbrl-fieldtype'] == 'valuelist') {
                     Ext.apply(cfg, Ext.clone(fieldTypes[cfg.defattribs['data-xbrl-valuelistid']]));
                     cfg.width = '90%'; // Ext.get(tdval.get
                     var fld = Ext.create(cfg);
                     this.valueField = fld;
                     this.valueField.setWidth(400);
                     this.mon(this.valueField, 'select', this.onFieldChange, this);
                     // this.fields.push(fld);
                 } else {
                     Ext.apply(cfg, fieldTypes[cfg.defattribs['data-xbrl-fieldtype']]);
                     var fld = Ext.create(cfg);
                     this.mon(fld, 'change', this.onFieldChange, this);
                     this.otherFields.push(fld);
                     //  this.fields.push(fld);
                 }
             } else {
             }
             Ext.get(flds[i]).addClass("biztax-field-rendered");
         }
     }
     , clearOthers: function () {
         for (var i = 0; i < this.otherFields.length; i++) {
             this.otherFields[i].suspendEvents(false);
             this.otherFields[i].setValue('');
             this.otherFields[i].resumeEvents();
         }
     }
     , hideOthers: function () {
         for (var i = 0; i < this.otherFields.length; i++) {
             this.otherFields[i].hide();
         }
     }
       , showOthers: function () {
           for (var i = 0; i < this.otherFields.length; i++) {
               this.otherFields[i].show();
           }
       }
     , setValue: function (el) {
         this.suspended = true;
         if (el.Children == null || el.Children.length == 0) {
             this.setActiveFields('valuelist');
             this.valueField.suspendEvents(false);
             this.valueField.clearValue();
             this.valueField.resumeEvents();
             this.clearOthers();

         } else {
             if (el.Children[0].FullName.indexOf('pfs-vl') > -1) {
                 this.setActiveFields('valuelist');
                 this.clearOthers();
                 this.valueField.suspendEvents(false);
                 this.valueField.setValue(el.Children[0].FullName);
                 this.valueField.resumeEvents();
             } else {
                 this.setActiveFields('others');
                 for (var i = 0; i < el.Children.length; i++) {
                     for (var j = 0; j < this.otherFields.length; j++) {

                         if (this.otherFields[j].defattribs["data-xbrl-name"] == el.Children[i].Name) {
                             this.otherFields[j].suspendEvents(false);
                             this.otherFields[j].setValue(el.Children[i].Value);
                             this.otherFields[j].resumeEvents();
                         }
                     }
                 }
                 this.valueField.clearValue();
                 //for(var i=0
             }
         }
         this.suspended = false;

     }
     , setActiveFields: function (tpe) {
         this.activeTpe = tpe;
         switch (tpe) {
             case 'valuelist':
                 this.valueField.show();
                 this.hideOthers();
                 this.radioVl.suspendEvents(false);
                 this.radioVl.setValue(true);
                 this.radioVl.resumeEvents();
                 this.radioOt.suspendEvents(false);
                 this.radioOt.setValue(false);
                 this.radioOt.resumeEvents();
                 break;
             default:
                 this.valueField.hide();
                 this.showOthers();
                 this.radioVl.suspendEvents(false);
                 this.radioVl.setValue(false);
                 this.radioVl.resumeEvents();
                 this.radioOt.suspendEvents(false);
                 this.radioOt.setValue(true);
                 this.radioOt.resumeEvents();
                 break;
         }
     }

     , destroy: function () {
         this.valueField.destroy();
         for (var j = this.otherFields.length - 1; j > -1; j--) {
             this.otherFields[j].destroy;
         }
         delete this.valueField;
         delete this.otherFields;
         this.radioVl.destroy();
         this.radiooT.destroy();
         eBook.BizTax.ValuelistOrOther.superclass.destroy.call(this);
     }


});
 Ext.reg('biztax-ValuelistOrOther', eBook.BizTax.ValuelistOrOther);eBook.BizTax['Status_-99'] = 'Declaration FAILED, REVIEW!!';
eBook.BizTax.Status_0 = 'In preparation';
eBook.BizTax.Status_1 = 'Locked (In validation by partner)';
eBook.BizTax.Status_10 = 'Send to be declared';
eBook.BizTax.Status_30 = 'Declaration in progress';
eBook.BizTax.Status_99 = 'Succesfully declared';


eBook.BizTax.Module = Ext.extend(Ext.ux.IconTabPanel, {
    xbrlData: {},
    biztaxRendered: false,
    initComponent: function () {
        this.validationMsgs = [];
        // this.elementStore = new Ext.data.JsonStore();
        var fileClosed = false;
        Ext.apply(this, {
            tabWidth: 200
            , enableTabTitles: true
            , itemsCls: 'eb-biztax-tabitem'
            , cls: 'eb-biztax-tab'
            //, activeTab: 0
            , ref: 'module'
            , tbar: [
                {xtype: 'tbspacer', width: 50}
                , {xtype: 'biztax-tb-info', text: '', ref: 'infoPane'}
                , {
                    ref: 'validerrors',
                    text: 'Validation errors',
                    iconCls: 'eBook-Warning-ico-24',
                    cls: 'btnValidationErrors',
                    scale: 'medium',
                    //width: 115,
                    iconAlign: 'top',
                    handler: function () {
                        var wn = new eBook.BizTax.ValidationOverview({messages: this.store.data.Data.Errors});
                        wn.show();
                        //wn.items.items[0].updateMe(this.store.data.Data.Errors);
                    },
                    scope: this
                    //,disabled: true //fileClosed
                }

                , '->'

                , {xtype: 'biztax-tb-proxy', ref: 'proxyPane'}, {
                    ref: 'addproxy',
                    text: 'Upload proxy', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onUploadProxyClick,
                    width: 70,
                    scope: this,
                    disabled: eBook.Interface.isFileClosed()
                }
                , {
                    ref: 'schema',
                    text: 'Schema',
                    iconCls: 'eBook-icon-account-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function () {
                        eBook.Interface.openWindow('eBook-File-Schema');
                    },
                    scope: this,
                    disabled: fileClosed
                }
                , {
                    ref: 'quickprint',
                    text: 'Quickprint',
                    iconCls: 'eBook-icon-quickprint-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function () {
                        eBook.Interface.openWindow('eBook-File-quickprint');
                    },
                    scope: this
                }
                , {
                    ref: 'repository',
                    text: 'Repository',
                    iconCls: 'eBook-repository-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function () {
                        eBook.Interface.openWindow('eBook-File-repository');
                    },
                    scope: this,
                    disabled: fileClosed
                }, {
                    ref: 'taxCalc',
                    text: 'Tax calculation', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-biztax-taxcalc-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onPreviewTaxCalc,
                    width: 70,
                    scope: this
                },
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    ref: 'vitals',
                    items: [
                        {
                            ref: '../clientBundle',
                            text: 'Client Bundle',
                            iconCls: 'eBook-client-bundle-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: function () {
                                eBook.Interface.openWindow('eBook-File-ReportGeneration-biztax');
                            },
                            scope: this,
                            disabled: fileClosed
                        },
                        {
                            ref: 'ConfirmationOfApprovalCITR',
                            text: 'Confirmation of Approval CITR', // eBook.Accounts.Manage.Window_AddAccount,
                            iconCls: 'eBook-client-approval-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onConfirmationOfApprovalCITR,
                            width: 70,
                            scope: this
                        }
                        , {
                            ref: '../unlock',
                            text: 'Unlock contents',
                            iconCls: 'eBook-unlock-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: function () {
                                if (eBook.User.isActiveClientRolesFullAccess()) {
                                    this.changeStatus(0);
                                } else {
                                    Ext.Msg.show({
                                        title: 'Executive only',
                                        msg: 'Only executives are allowed to unlock the Biztax module. Contact your team manager/partner.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        scope: this
                                    });
                                }
                            },
                            scope: this,
                            hidden: true
                        }
                        , {
                            ref: '../lock',
                            text: 'Lock contents',
                            iconCls: 'eBook-lock-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: function () {
                                if (eBook.User.isActiveClientRolesFullAccess()) {
                                    this.changeStatus(1);
                                } else {
                                    Ext.Msg.show({
                                        title: 'Executive only',
                                        msg: 'Only executives are allowed to lock the Biztax module. Contact your team manager/partner.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        scope: this
                                    });
                                }
                            },
                            scope: this,
                            disabled: fileClosed
                        }
                        , {
                            ref: '../declare',
                            text: 'Declare',
                            iconCls: 'eBook-biztax-send-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: function () {
                                if (eBook.User.isActiveClientRolesFullAccess()) {
                                    var wn = new eBook.BizTax.Declaration.Window({module: this});
                                    wn.show();
                                } else {
                                    Ext.Msg.show({
                                        title: 'Executive only',
                                        msg: 'Only executives are allowed to lock declare to Biztax. Contact your team manager/partner.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        scope: this
                                    });
                                }
                            },
                            scope: this,
                            disabled: fileClosed
                        }]
                }, {xtype: 'tbspacer', width: 50}
            ]
            , listeners: {
                'activate': {
                    fn: this.onActivate
                    , scope: this
                }
            }
        });
        eBook.BizTax.Module.superclass.initComponent.apply(this, arguments);

    }
    , clearTabErrors: function () {
        var els = this.tabStrip.query('.biztax-tabstrip-invalid'),
            i = 0;

        for (i = 0; i < els.length; i++) {
            Ext.get(els[i]).removeClass('biztax-tabstrip-invalid');
        }

    }
    , setTabInvalid: function (code) {
        var childIdx = this.items.indexOfKey("biztax-tab-" + code);
        var el = this.tabStrip.dom.childNodes[childIdx];
        if (el) {
            Ext.get(el).addClass('biztax-tabstrip-invalid');
        }

    }
    , preRenderTabs: function (tabids) {
        var i = 0, tabid;
        for (i = 0; i < tabids.length; i++) {
            tabid = tabids[i];
            if (tabid.indexOf("biztax-tab-") == -1) {
                tabid = "biztax-tab-" + tabid;
            }
            this.preRenderTab(tabid);
        }
    }
    , preRenderTab: function (tabid) {
        var ly = this.getLayout(),
            ct = this.container, target = this.getLayoutTarget(),
            item = this.items.get(tabid),
            idx = this.items.indexOfKey(tabid);
        if (item && !item.rendered) {
            ly.renderItem(item, idx, target);
        }
    }
    , onPreviewTaxCalc: function () {
        eBook.Splash.setText("Generating print Biztax Tax calculation ");
        eBook.Splash.show();
        Ext.Ajax.request({
            url: eBook.Service.output + 'GetBiztaxTaxDetail'
            , method: 'POST'
            , params: Ext.encode({
                cidc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.Culture
                }
            })
            , callback: this.onPreviewTaxCalcResponse
            , scope: this
        });
    }
    , onPreviewTaxCalcResponse: function (opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetBiztaxTaxDetailResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    ///Open messagebox which allows the user to choose some settings regarding the confirmation of aproval CITR
    , onConfirmationOfApprovalCITR : function() {
        var me = this;
        var msg = 'Choose the cooperative:<br /><br /><select id="coop"><option value="1">EY Tax Consultants BV CVBA</option><option value="2">EY Accountants BV CVBA</option><option value="3">EY Fiduciaire BV CVBA</option></select>';
        msg += '<br/><br/>Language:<br /><br /><select id="language" style="width:207px;"><option value="nl-BE">Nederlands</option><option value="fr-FR">Français</option><option value="en-GB">English</option></select>';
        //Ask user to choose the cvba through a messagebox containing a combobox
        Ext.MessageBox.show({
            title: 'Confirmation of Approval CITR',
            msg: msg,
            buttons: Ext.MessageBox.OKCANCEL,
            fn: function (btn) {
                if (btn == 'ok') {
                    me.GetConfirmationOfApprovalCITR(Ext.get('coop').getValue(),Ext.get('language').getValue());
                }
            }
        });
    }
    ///Get Confirmation Of Approval Corporate Income Tax Return PDF
    , GetConfirmationOfApprovalCITR: function(cooperative, language) {
        eBook.Splash.setText("Generating print Confirmation of Approval Corporate Income Tax Return");
        eBook.Splash.show();

        Ext.Ajax.request({
            url: eBook.Service.output + 'GetConfirmationOfApprovalCITR'
            , method: 'POST'
            , params: Ext.encode({
                ciaddc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: language,
                    Department: cooperative
                }
            })
            , callback: this.onGetConfirmationOfApprovalCITRResponse
            , scope: this
        });
    }
    ///Remove splash, open pdf
    , onGetConfirmationOfApprovalCITRResponse: function(opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetConfirmationOfApprovalCITRResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onUploadProxyClick: function () {
        // var wn = new eBook.Repository.LocalFileBrowser({});
        //var wn = new eBook.Window({layout:'fit',items:[new eBook.Repository.FileDetailsPanel({})]});
        var wn = new eBook.Repository.SingleUploadWindow({
            standards: {
                statusid: 'fcf7a560-3d74-4341-a32b-310c8fa336cf'
                , location: 'd33ce2b5-fbeb-4d58-9c47-2a4731804173'
            }
            , readycallback: {fn: this.store.loadData, callerCallback: this.onDataLoaded, scope: this.store, callerCallbackScope: this}
        });
        wn.show();
    }
    , onActivate: function () {
        this.biztaxType = eBook.Interface.currentClient.get('bni') ? 'nrcorp' : 'rcorp';
        this.loadBizTax('' + eBook.Interface.currentFile.get('AssessmentYear'), this.biztaxType); // (n)rcorp
    }
    , clearAll: function () {


        if (this.biztaxRendered) {
            if (this.store) {
                this.store.destroy();
                delete this.store;
            }
            this.removeAll(true);

            this.biztaxRendered = false;
        }
    }
    , removeAll: function (autoDestroy) {
        this.initItems();
        var item, rem = [], items = [];
        this.items.each(function (i) {
            rem.push(i);
        });
        for (var i = rem.length - 1; i > -1; i--) {
            item = rem[i];
            this.remove(item, autoDestroy);
            if (item.ownerCt !== this) {
                items.push(item);
            }
        }
        return items;
    }

    , changeStatus: function (newStatus) {
        var cb = null;

        this.store.data.Status = newStatus;
        this.store.saveMeta(true);
    }
    , loadBizTax: function (AY, biztaxtype) {
        //AY = '2012';

        if (parseInt(AY) >= 2014) {
            this.topToolbar.declare.disabled = true;
        }
        this.saveTriggerDte = null;
        this.biztaxtype = biztaxtype;
        if (!this.biztaxRendered) {
            this.getEl().mask("Rendering BizTax " + AY + ' ' + biztaxtype);
            var cult = eBook.Interface.Culture;
            if (cult == 'en-US') cult = 'nl-BE';
            eBook.LazyLoad.loadOnce.defer(50, this, ["BizTax/" + AY + "/" + biztaxtype + "/js/" + cult + ".js?" + eBook.SpecificVersion, this.onSpecificsLoaded, {
                AY: AY,
                biztaxtype: biztaxtype
            }, this, true]);
            // fetch biztax info (lazy load)


        } else {

            this.getEl().mask("Loading biztax data " + AY + ' ' + biztaxtype);
            if (this.store) {
                this.store.loadData(this.onDataLoaded, this);
            }
        }

    }
    , onSpecificsLoaded: function (cfg) {
        this.locator = 'AY' + cfg.AY + '_' + cfg.biztaxtype;
        if (!eBook.BizTax[this.locator]) {
            //alert("No biztax for assessmentyear " + AY);
            this.getTopToolbar().hide();
            this.add({
                xtype: 'panel',
                title: 'No BizTax module available',
                html: '<h2>There is no ' + cfg.biztaxtype + ' BizTax module for assessmentyear ' + cfg.AY + '</h2>'
            });
            this.setActiveTab(0);
            this.biztaxRendered = true;
            this.getEl().unmask();

            return;
        }
        this.getTopToolbar().show();
        this.store = new eBook.BizTax.Store({
            serviceUrl: eBook.Service.biztax
            , biztax: {
                type: cfg.biztaxtype

                , assessmentyear: cfg.AY
            }
            , moduleId: this.id
        });

        this.fields = {};
        this.contextGrids = [];
        this.fieldsIdx = [];
        this.add(Ext.clone(eBook.BizTax[this.locator].Tabs, true));
        this.biztaxRendered = true;
        this.setActiveTab(0);
        /*
         this.calcWorker = new Worker(eBook.BizTax[locator].Calculator+"?" + eBook.SpecificVersion); // forex:'js/Taxonomy/BizTax-calc-rcorp-'+AY+'.js');
         this.calcWorker.addEventListener('message', this.onCalculated, false);
         this.calcWorker.addEventListener('error', this.onCalculationError, false);

         this.validWorker = new Worker(eBook.BizTax[locator].Validator+"?" + eBook.SpecificVersion); // forex:'js/Taxonomy/BizTax-validation-rcorp-'+AY+'.js');
         this.validWorker.addEventListener('message', this.onValidated, false);
         this.validWorker.addEventListener('error', this.onValidationError, false);
         */

        this.getEl().mask("Loading biztax data " + cfg.AY + ' ' + cfg.biztaxtype);

        this.store.loadData(this.onDataLoaded, this);
    }
    , onDataLoaded: function () {


        var tb = this.getTopToolbar();
        var ip = tb.infoPane;
        if (this.store.data != null) {
            ip.setCalc(this.store.data.Data.TaxCalc);
        }
        if (this.store.lastSaved != null) {
            ip.setSave(this.store.data.Data.LastSaved);
        }
        /*
         tb.statusPane.setStatus(this.store.status);
         */
        tb.proxyPane.setProxy(this.store.data.Data.ProxyTitle);
        if (this.store.data.Data.ProxyId) {
            tb.addproxy.hide();
        } else {
            tb.addproxy.show();
        }
        tb.unlock.hide();
        tb.lock.show();
        tb.declare.disable();
        if (this.store.data.Status == 1) {
            tb.unlock.show();
            tb.lock.hide();
            if (eBook.User.isActiveClientRolesFullAccess()) {
                tb.declare.enable();
            }
            else {
            }
        } else if (this.store.data.Status > 1) {
            tb.lock.hide();
        }
        //  this.store.partner 

        // this.setFields();
        // this.setContextGrids();
        // this.triggerValidation();
        this.getEl().unmask(true);
    }


});
Ext.reg('biztax-module', eBook.BizTax.Module);

eBook.BizTax.DateField = Ext.extend(Ext.form.DateField, {});

eBook.BizTax.NumberField = Ext.extend(Ext.form.NumberField, {
    initComponent: function () {
        Ext.apply(this, {
            align: 'right', msgTarget: 'side'
        });
        eBook.BizTax.NumberField.superclass.initComponent.apply(this, arguments);
    }
    , markInvalid: function (msg) {
        var myEl = this.getEl();
        this.invalid = true;


    }
});
Ext.reg('biztax-numberfield', eBook.BizTax.NumberField);

eBook.BizTax.ValidationOverview = Ext.extend(eBook.Window, {
    initComponent: function () {
        this.errorMgs = [];
        var cultidx = eBook.Interface.Culture == 'fr-FR' ? 1 : 0;
        for (var i = 0; i < this.messages.length; i++) {
            this.errorMgs.push({id: this.messages[i].Id, msg: this.messages[i].Messages[cultidx].Message});
        }
        Ext.apply(this, {
            title: 'Validation Errors'
            , layout: 'fit'
            , width: 405
            , height: 405
            , iconCls: 'eBook-Bundle-icon-16'
            , bodyStyle: 'background-color:#FFF;'
            , items: [{xtype: 'errorPnl'}]
            , tbar: {
                xtype: 'toolbar',
                items: []
            }

        });
        eBook.BizTax.ValidationOverview.superclass.initComponent.apply(this, arguments);
    },

    show: function () {
        //alert('test show');
        eBook.Bundle.EditWindow.superclass.show.call(this);
        this.items.items[0].updateMe(this.errorMgs);
    }


});
Ext.reg('biztax-validationOverview', eBook.BizTax.ValidationOverview);


eBook.BizTax.ErrorPanel = Ext.extend(Ext.Panel, {
    errorMgs: this.messages,
    mainTpl: new Ext.XTemplate('<div class="eBook-BizTax-errorMgs-wrapper">' +
        '<tpl for=".">' +
        '<div class="eBook-BizTax-errorMgs-mgs" >{id}: {msg}</div>' +
        '</tpl>' +
        '</div>'
        , {compiled: true}),
    autoScroll: true,
    /*
     initComponent: function () {

     Ext.apply(this, {
     title:'TEST'

     });
     eBook.BizTax.ErrorPanel.superclass.initComponent.apply(this, arguments);
     },
     */
    updateMe: function (errorMgs) {
        this.body.update(this.mainTpl.apply(errorMgs));
    }

});

Ext.reg('errorPnl', eBook.BizTax.ErrorPanel);
eBook.BizTax.Tab = Ext.extend(Ext.Panel, {
    initComponent: function () {

        Ext.apply(this, {
            autoScroll: true,
            padding: 15,
            titleTxt: this.title,
            title: '<span style="color:#900;">' + this.code + '. </span>' + this.title,
            cls: 'biztax-tab-body biztax-tab-code-' + this.code,
            id: 'biztax-tab-' + this.code,
            autoLoad: this.url,
            tbar: ['->', {
                ref: 'printpreview',
                iconCls: 'eBook-pdf-view-24',
                scale: 'medium',
                //width: 115,
                // iconAlign: 'top',
                handler: this.onPrintPreview,
                scope: this
                //,disabled: true //fileClosed
            }]
        });

        eBook.BizTax.Tab.superclass.initComponent.apply(this, arguments);
        if (this.elementsDisabledFn) this.elementsDisabled = this.elementsDisabledFn();
        this.elementsDisabled = this.elementsDisabled ? true : false;
    }
    , onPrintPreview: function () {
        eBook.Splash.setText("Generating print preview " + this.code);
        eBook.Splash.show();
        Ext.Ajax.request({
            url: eBook.Service.output + 'GetBiztaxFichePreview'
            , method: 'POST'
            , params: Ext.encode({
                cbfpdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.currentCulture,
                    FicheId: this.code,
                    FicheTitle: this.titleTxt
                }
            })
            , callback: this.onPrintPreviewResponse
            , scope: this
        });
    }
    , onPrintPreviewResponse: function (opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetBiztaxFichePreviewResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , getModule: function () {
        return this.ownerCt;
    }
    , afterRender: function (cnt) {
        eBook.BizTax.Tab.superclass.afterRender.call(this);
        if (!this.autoLoad) {
            this.applyBizTaxRendering();
        }
    }
    , applyBizTaxRendering: function () {
        this.applyVlOrOtherRendering();
        this.applyContextRendering();
        this.applyFieldRendering();
    }
    , applyVlOrOtherRendering: function () {
        this.valuelistOrOther = [];
        var ctxs = this.el.select('.biztax-valuelistorother').elements;
        for (var i = 0; i < ctxs.length; i++) {
            var ctx = new eBook.BizTax.ValuelistOrOther({
                domConfig: ctxs[i]
                , bizTaxLocator: this.getModule().locator, moduleId: this.getModule().id
                , tabCode: this.code
            });
            this.valuelistOrOther.push(ctx);
        }
    }
    , applyContextRendering: function () {
        this.contextgrids = [];
        var ctxs = this.el.select('contextgrid').elements;
        for (var i = 0; i < ctxs.length; i++) {
            var ctx = new eBook.BizTax.Contextgrid({
                domConfig: ctxs[i], bizTaxLocator: this.getModule().locator, moduleId: this.getModule().id
                , tabCode: this.code
            });
            this.contextgrids.push(ctx);
        }
    }
    , applyFieldRendering: function () {
        var fieldTypes = eBook.BizTax[this.getModule().locator].FieldTypes;
        this.fields = [];
        var flds = this.el.select('.biztax-field-wrapper').elements;
        for (var i = 0; i < flds.length; i++) {
            var xfld = Ext.get(flds[i]);
            if (!xfld.hasClass("biztax-field-rendered")) {
                var cfg = { defattribs: this.getFieldAttribs(flds[i]), renderTo: flds[i]
                    , width: '90%'
                    , tabCode: this.code
                };
                cfg.isCalculated = false;
                if (cfg.defattribs['data-xbrl-calc'] == 'true') {
                    cfg.isCalculated = true;
                    cfg.disabled = true;
                }

                if (!xfld.parent('table.biztax-valuelistorother')) {
                    if (cfg.defattribs['data-xbrl-fieldtype']) {

                        if (cfg.defattribs['data-xbrl-fieldtype'] == 'valuelist') {
                            cfg.defattribs = this.getFieldAttribs(flds[i])
                            Ext.apply(cfg, Ext.clone(fieldTypes[cfg.defattribs['data-xbrl-valuelistid']]));
                            var fld = Ext.create(cfg);
                            var val = this.registerField(fld);
                            if (val) fld.setValue(val);
                            if (this.code == "Id") {
                                fld.setWidth(400);
                            }
                            this.fields.push(fld);
                        } else {
                            cfg.msgTarget = 'sidebiztax';
                            Ext.apply(cfg, fieldTypes[cfg.defattribs['data-xbrl-fieldtype']]);
                            var fld = Ext.create(cfg);
                            var val = this.registerField(fld);
                            if (val) fld.setValue(val);
                            this.fields.push(fld);
                        }
                    } else {
                    }
                }
            }
        }
    },
    getFieldAttribs: function (el) {
        var attribs = {};
        for (var attr, i = 0, attrs = el.attributes, l = attrs.length; i < l; i++) {
            attr = attrs.item(i);
            attribs[attr.nodeName] = attr.nodeValue;
        }
        return attribs;
    },
    doAutoLoad: function () {
        var u = this.body.getUpdater();
        u.on("update", this.applyBizTaxRendering, this);
        if (this.renderer) {
            u.setRenderer(this.renderer);
        }
        u.update(Ext.isObject(this.autoLoad) ? this.autoLoad : { url: this.autoLoad });
    }
    , destroy: function () {
        for (var i = this.fields.length - 1; i > -1; i--) {
            this.fields[i].destroy();
            delete this.fields[i];
        }
        alert("tab destroyed, fields left: " + this.fields.length);
        eBook.BizTax.Tab.superclass.destroy.call(this);
    }
    , registerField: function (fld) {
        return this.getModule().store.registerField(fld);
    }
    , destroy: function () {

        if (this.valuelistOrOther) {
            for (var j = this.valuelistOrOther.length - 1; j > -1; j--) {
                this.valuelistOrOther[j].destroy;
            }
        }
        delete this.valuelistOrOther;

        if (this.contextgrids) {
            for (var j = this.contextgrids.length - 1; j > -1; j--) {
                this.contextgrids[j].destroy;
            }
        }
        delete this.contextgrids;

        if (this.fields) {
            for (var j = this.fields.length - 1; j > -1; j--) {
                this.fields[j].destroy;
            }
        }
        delete this.fields;


        eBook.BizTax.Tab.superclass.destroy.call(this);
    }
});
Ext.reg('biztax-tab', eBook.BizTax.Tab);
/* VERSION 0.3 */
/*eBook.BizTax.FieldLabel = Ext.extend();

eBook.BizTax.FieldItem = Ext.extend();

eBook.BizTax.CodeLabel = Ext.extend();
*/

// or "bundle field"

eBook.BizTax.UploadField = Ext.extend(Ext.BoxComponent, {
    autoEl: {
        tag: 'div',
        cls: 'biztax-upload-field'
    }
    , mytpl: new Ext.XTemplate('<tpl for="."><ul><tpl for="data"><li class="{iconCls}">{title}</li></tpl></ul></tpl>', { compiled: true })
    , setValue: function (val) { }
    , initComponent: function () {
        Ext.apply(this, {
            autoEl: {
                tag: 'div',
                cls: 'biztax-upload-field'//,
                // html: 'JIPPIE'
            }
        });
        this.data = [];

        eBook.BizTax.UploadField.superclass.initComponent.apply(this, arguments);
    }
    , getModule: function () {
        return this.findParentByType('biztax-module');
    }
    , applyToMarkup: function (el) {
        this.allowDomMove = false;

        this.el = Ext.get(el);
        var parent = this.el.parent();
        this.el.remove();
        this.el = parent.createChild(this.autoEl);
        //parent.update('HERE COMES THE UPLOAD');
        this.render(parent);
    }
    , onRender: function (container, position) {
        eBook.BizTax.UploadField.superclass.onRender.apply(this, arguments);
        this.ul = this.el.createChild({ tag: 'ul' });
        this.buttons = this.el.createChild({
            tag: 'div', cls: 'biztax-upload-field-buttons'
            , children: [{ tag: 'div', cls: 'biztax-contentpane-button', html: 'EDIT'}]
        });
        this.mon(this.buttons.child('.biztax-contentpane-button'), 'click', this.onEdit, this);
        this.renderContents();
        this.validate()
    }
    , onEdit: function () {
        var w = new eBook.BizTax.UploadEditorWindow({ caller: this, data: this.data });
        w.show();
    }
    , getValue: function () {
        return Ext.clone(this.data);
    }
    , setValue: function (val) {
        var upd = false;
        this.data = val;
        if (this.data) {

            for (var i = this.data.length; i > -1; i--) {
                if (this.data[i] == "") {
                    upd = true;
                    this.data.splice(i, 1);
                }
            }
        }

        this.renderContents();
        this.validate();
        if (upd) {
            this.fireEvent('change', this);
        }
    }
    , updateValue: function (val) {
        this.setValue(val);
        // write to store
        this.fireEvent('change', this);
    }
    , renderContents: function () {
        this.ul.remove();
        if (this.data == null) this.data = [];
        this.ul = this.mytpl.insertBefore(this.buttons, this, true);
    }
    , markInValid: function () {
        this.el.addClass('x-form-invalid');
    }
    , markValid: function () {
        this.el.removeClass('x-form-invalid');
    }
    , isValid: function () {
        if (this.isRequired) {
            if (this.data == null || this.data.length == 0) {
                return false;
            }
        }
        return true;
    }
    , validate: function () {
        var bl = this.isValid();
        if (!bl) {
            this.markInValid();
        } else {
            this.markValid();
        }
        return bl;
    }
});

Ext.reg('biztax-uploadfield', eBook.BizTax.UploadField);
eBook.BizTax.Declaration.Overview = Ext.extend(Ext.Panel, {
    mtpl: new Ext.XTemplate('<div class="eBook-Xbrl-Overview">'
            , '<div class="eBook-Xbrl-Overview-Block eBook-Client-ico-32">'
                , ' <b><u>{clientname}</u></b>'
                , '<br/><br/> Period: {start:date("d/m/Y")} &gt; {end:date("d/m/Y")}'
                , '<br/> Assessment: {assessmentyear}'
             , '</div>'
             , '<div class="eBook-Xbrl-Overview-Block eBook-Partner-ico-32">'
             , 'Department: <b>{department}</b>'
             , '<br/>Declaring partner: <b>{partnername}</b>'
             , '</div>'
             , '</div>', { compiled: true })
    , initComponent: function() {
        Ext.apply(this, {
            html: 'loading'
            , bbar: [{ xtype: 'button', text: eBook.Xbrl.Wizard_Previous, ref: 'bck', handler: this.onPrevious, scope: this, iconCls: 'eBook-back-24',
                scale: 'medium',
                iconAlign: 'top'
            }, '->', {
                text: eBook.Xbrl.Overview_SendToBizTax,
                iconCls: 'eBook-biztax-send-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'biztax',
                handler: this.onSendToBizTax,
                scope: this
}]
            });
            eBook.BizTax.Declaration.Overview.superclass.initComponent.apply(this, arguments);
            
            
        }
    , getBodyEl: function() {
        return this.getEl().child('.x-panel-body');
    }
    , setStep1Data: function(data) {
        var dta = {
            clientname: eBook.Interface.currentClient.get('Name')
            , start: eBook.Interface.currentFile.get('StartDate')
            , end: eBook.Interface.currentFile.get('EndDate')
            , assessmentyear: eBook.Interface.currentFile.get('AssessmentYear')
            , department: data.department
            , partnername: data.partnerName
        };
        this.getBodyEl().update(this.mtpl.apply(dta));
    }
    , onSendToBizTax: function() {
        this.refOwner.sendToBeDeclared();
    }
    , onPrevious: function() {
        this.refOwner.getLayout().setActiveItem(0);
    }
    });
Ext.reg('biztax-declaration-overview', eBook.BizTax.Declaration.Overview);

eBook.BizTax.Declaration.PartnerSelection = Ext.extend(Ext.form.FormPanel, {
    initComponent: function () {
        Ext.apply(this, {
            layoutConfig: { labelAlign: 'top' }
            , labelAlign: 'top'
            , cls: 'biztax-partner-select'
            , bodyStyle: 'padding:15px'
            , defaults: { margins: { top: 10, bottom: 10, right: 0, left: 0} }
            , items: [
                { xtype: 'checkbox', ref: 'partnerok', boxLabel: 'Did the partner formally approved this declaration?'
                    //, disabled: true
                    , listeners: { 'check': { fn: this.onPartnerCheck, scope: this} }
                }
                , { xtype: 'label', labelStyle: 'font-style:italic', text: 'If department and/or partner is not in the lists below, update the MyPMT teammembers.' }
                , { xtype: 'combo', ref: 'departments'
                     , disabled: true
                    , typeAhead: false
                    , forceSelection: true
                    , mode: 'local'
                    , triggerAction: 'all'
                    , width: 300
                    , autoSelect: true
                    , valueField: 'id'
                    , fieldLabel: 'Select the deparment which will perform the declaration'
                    , displayField: 'desc'
                    , store: new eBook.data.JsonStore({
                        selectAction: 'GetClientDepartments'
                                , serviceUrl: eBook.Service.client
                                , autoLoad: true
                                , autoDestroy: true
                                , criteriaParameter: 'cidc'
                                , baseParams: { Id: eBook.Interface.currentClient.get('Id') }
                                , fields: eBook.data.RecordTypes.SimpleList
                    })
                    , listeners: { 'select': { fn: this.onDepartmentSelect, scope: this} }
                }
                , { xtype: 'combo', ref: 'partners'
                    , disabled: true
                    , typeAhead: false
                    , forceSelection: true
                    , autoSelect: true
                    , mode: 'local'
                    , triggerAction: 'all'
                    , emptyText: 'No partners where found for this department, check/update the MyPMT teammembers.'
                    , width: 300
                    , valueField: 'id'
                    , fieldLabel: 'Select the partner in whose name the declaration will be performed'
                    , displayField: 'desc'
                    , store: new eBook.data.JsonStore({
                        selectAction: 'GetClientDepartmentPartners'
                                , serviceUrl: eBook.Service.client
                                , autoLoad: false
                                , autoDestroy: true
                                , criteriaParameter: 'ciddc'
                        // , baseParams: { Id: eBook.Interface.currentClient.get('Id'), Department: 'ACR' }
                                , fields: eBook.data.RecordTypes.SimpleList
                    })
                    , listeners: { 'select': { fn: this.onPartnerSelect, scope: this} }
                }
            ]
            , bbar: ['->', { xtype: 'button', text: eBook.Xbrl.Wizard_Next, ref: 'nxt', handler: this.onNext, scope: this, iconCls: 'eBook-next-24',
                scale: 'medium', disabled: true,
                iconAlign: 'top'
            }]
        });
        eBook.BizTax.Declaration.PartnerSelection.superclass.initComponent.apply(this, arguments);
    }
    , mdata: { department: '', partnerId: '', partnerName: '', validated: false }
    , onPartnerCheck: function (fld, chk) {
        this.getBottomToolbar().nxt.disable();
        this.mdata.validated = chk;
        this.mdata.department = null;
        this.mdata.partnerId = null;
        this.mdata.partnerName = null;
        if (chk) {
            this.departments.enable();
            if (this.departments.store.getCount() == 1) {
                this.departments.setValue(this.departments.store.getAt(0).get('id'));
                this.onDepartmentSelect(this.departments, this.departments.store.getAt(0), 0);
            }
        } else {
            this.departments.clearValue();
            this.departments.disable();
            this.partners.clearValue();
            this.partners.disable();
        }
    }
    , onDepartmentSelect: function (fld, rec, idx) {
        this.getBottomToolbar().nxt.disable();
        if (rec) {
            this.partners.clearValue();
            this.mdata.department = rec.get('id');
            this.mdata.partnerId = null;
            this.mdata.partnerName = null;
            if (rec.get('id') == 'ACR') {
                // central admin -> always Lena Plas.
                this.partners.store.removeAll();
                var rt = new this.partners.store.recordType({ id: '607d8203-f4a1-4f70-9b5d-d6078b5ea8cb', desc: 'LENA PLAS' });
                this.partners.store.add(rt);
                this.partners.setValue(rt.get('id'));
                this.onPartnerSelect(this.partners, rt, 0);
                this.partners.disable();
                if (!this.refOwner.hasproxy) {
                    this.getBottomToolbar().nxt.disable();
                    Ext.Msg.show({
                        title: 'Proxy missing',
                        msg: 'There is currently no signed proxy for this client and period. Declaration is not allowed until the signed proxy is uploaded.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        scope: this
                    });
                }
            } else {
                this.partners.store.load({ params: { Id: eBook.Interface.currentClient.get('Id'), Department: rec.get('id')} });
                this.partners.enable();
            }
        } else {
            this.mdata.department = null;
            this.mdata.partnerId = null;
            this.mdata.partnerName = null;
            this.partners.clearValue();
            this.partners.disable();
        }
    }
    , onPartnerSelect: function (fld, rec, idx) {
        if (rec) {
            this.mdata.partnerId = rec.get('id');
            this.mdata.partnerName = rec.get('desc');
            this.getBottomToolbar().nxt.enable();
        } else {
            this.mdata.partnerId = null;
            this.mdata.partnerName = null;
        }
    }
    , onNext: function () {
        this.refOwner.overview.setStep1Data(this.mdata);
        this.refOwner.getLayout().setActiveItem(1);
    }
});

/*
eBook.Service.client

CriteriaIdAndDepartmentDataContract

*/

Ext.reg('biztax-declaration-partner', eBook.BizTax.Declaration.PartnerSelection);



eBook.BizTax.Declaration.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        Ext.apply(this, {
            title: 'BizTax declaration'
            , layout: 'card'
            , activeItem: 0
            , width: 612
            , height: 375
            , modal: true
            , items: [{ xtype: 'biztax-declaration-partner', ref: 'partnerpanel' }
                      , { xtype: 'biztax-declaration-overview', ref: 'overview'}]
        });
        eBook.BizTax.Declaration.Window.superclass.initComponent.apply(this, arguments);
        this.hasproxy = !Ext.isEmpty(this.module.store.data.Data.ProxyId);
    }
    /* , show: function(module) {
    eBook.BizTax.Declaration.Window.superclass.show.call(this);
    this.module = module;
    }*/
    , sendToBeDeclared: function () {
        this.getEl().mask("Saving meta data");
        var meta = this.partnerpanel.mdata;
        // UPDATE STORE 
        this.module.store.data.department = meta.department;
        this.module.store.data.partnerId = meta.partnerId;
        this.module.store.data.partnerName = meta.partnerName;
        // SAVE META ONLY (no changes to xbrl)
        this.module.store.saveMeta(false,{ fn: this.onMetaSaved, scope: this });
    }
    , onMetaSaved: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.getEl().mask("Publishing BizTax declaration to be send");
            Ext.Ajax.request({
                url: this.module.store.myServiceUrl("Publish")
            , method: 'POST'
            , params: Ext.encode({ cbdc: {
                FileId: eBook.Interface.currentFile.get('Id'), 
                Culture: eBook.Interface.Culture,
                Type:this.module.biztaxtype
        }
             })
            , callback: this.onDeclared
            , scope: this
                // , callerCallback: { fn: module.onDataLoaded, scope: module }
            });
        } else {
            eBook.Interface.showResponseError(resp, "Failed saving meta");
        }
    }
    , onDeclared: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.module.store.loadData(this.module.onDataLoaded, this.module);
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, "Biztax publish failed");
        }
    }
});

Ext.reg('biztax-declaration-window', eBook.BizTax.Declaration.Window);        
eBook.BizTax.NodeInfoFieldSet = Ext.extend(Ext.form.FieldSet, {
    onCheckClick: function() {
        eBook.BizTax.NodeInfoFieldSet.superclass.onCheckClick.call(this);
        this.refOwner.onChange();
    }
});
Ext.reg('eBook.BizTax.NodeInfoFieldSet', eBook.BizTax.NodeInfoFieldSet);


eBook.BizTax.ItemSettings = Ext.extend(Ext.FormPanel, {
    disIt: true
    , initComponent: function() {
        //var disIt = eBook.Interface.isFileClosed();

        Ext.apply(this, {
            //items: [],
            title: String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, "")
            , labelAlign: 'left'
            , disIt: this.disIt
            , disabled: this.disIt
            , items: [
                {
                    xtype: 'iconlabelfield',
                    cls: 'eBook-bundle-iconlabelfield ',
                    text: '',
                    ref: 'nodeType'
                    , disabled: this.disIt
                },
                 {
                     xtype: 'label'
                    , text: eBook.Bundle.NodeInfoPanel_Title
                    , style: 'margin-bottom:2px;font-size:10pt;'
                    , ref: 'titlelabel'
                    , disabled: this.disIt
                 }, {
                     xtype: 'textfield',
                     hideLabel: true,
                     value: '',
                     name: 'title',
                     ref: 'titleField',
                     listeners: {
                         'change': { fn: this.onChange, scope: this }
                     }, anchor: '100%', style: 'margin-bottom:10px'
                     , disabled: this.disIt
                 }, {
                     xtype: 'languagebox'
                    , ref: 'culture'
                    , fieldLabel: eBook.Bundle.NodeInfoPanel_Language
                    , allowBlank: false
                    , autoSelect: true
                    , anchor: '90%'
                    , listeners: {
                        'select': { fn: this.onCultChange, scope: this }
                    }
                    , disabled: this.disIt
}//ShowInIndex
                , {
                    xtype: 'checkbox',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_Detailed,
                    ref: 'detailed',
                    hidden: true,
                    listeners: {
                        'check': { fn: this.onChange, scope: this }
                    }
                    , disabled: this.disIt
                }, {
                    xtype: 'numberfield',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_PageFrom,
                    defaultValue: 1,
                    minValue: 1,
                    maxValue: 1,
                    ref: 'pageFrom',
                    hidden: true,
                    listeners: {
                        'change': { fn: this.onChange, scope: this }
                    }
                    , disabled: this.disIt
                }, {
                    xtype: 'numberfield',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_PageTo,
                    ref: 'pageTo',
                    defaultValue: 1,
                    minValue: 1,
                    maxValue: 1,
                    hidden: true,
                    listeners: {
                        'change': { fn: this.onChange, scope: this }
                    }
                    , disabled: this.disIt
}]
        });
        eBook.BizTax.ItemSettings.superclass.initComponent.apply(this, arguments);

    }
    , nodeUpdate: true
    , onCultChange: function() {
        if (!this.nodeUpdate) return;
        //this.currentNode.setProperties(this.getProperties());
        this.refOwner.uploadEditorPnl.editor.setProperties(this.getProperties());
    }
    , onChange: function() {
        if (!this.nodeUpdate) return;
        //this.currentNode.setProperties(this.getProperties());
        this.refOwner.uploadEditorPnl.editor.setProperties(this.getProperties());
    } /*
    , onReconfigureChildrenClick: function() {
        Ext.Msg.show({
            title: eBook.Bundle.NodeInfoPanel_UpdateChildrenTitle,
            msg: eBook.Bundle.NodeInfoPanel_UpdateChildrenMsg,
            buttons: Ext.Msg.YESNO,
            fn: this.reconfigureChildren,
            scope: this,
            icon: Ext.MessageBox.QUESTION
        });
    }
    , reconfigureChildren: function(btnid) {

        if (btnid == 'yes') {
            var props = this.getProperties();
            for (var i = 0; i < this.currentNode.childNodes.length; i++) {
                this.currentNode.childNodes[i].applyProperties({ HeaderConfig: props.HeaderConfig, FooterConfig: props.FooterConfig }, true);
            }
        }
    } */
    , getProperties: function() {
        //        var hd = this.headerData.checkbox.dom.checked;
        //        var ft = this.footerData.checkbox.dom.checked;

        var props = {

            type: this.currentNode.get('indexItem').type,
            title: this.titleField.getValue()
            //indexed: this.showindex.getValue(),
            //            HeaderConfig: {
            //                Enabled: hd
            //                , ShowTitle: hd ? this.headerData.titleField.getValue() : false
            //            },
            //            FooterConfig: {
            //                Enabled: ft
            //                , ShowPageNr: ft ? this.footerData.pagenr.getValue() : false
            //                , ShowFooterNote: ft ? this.footerData.footnote.getValue() : false
            //            }
        };
        if (this.culture.isVisible(true)) {
//            alert(this.culture.isVisible(true));
            props.Culture = this.culture.getValue();
        } else {
           // alert(this.culture.isVisible(true));
        }
        if (props.type == "STATEMENT") props.Detailed = this.detailed.getValue();
        if (props.type == "PDFPAGE") {
            props.FromPage = this.pageFrom.getValue();
            props.ToPage = this.pageTo.getValue();
            if (props.ToPage < props.FromPage) {
                var t = props.FromPage;
                props.FromPage = props.ToPage;
                props.ToPage = t;
            }
        }
        this.setTitle(String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, props.title));
        return props;
    }
    , loadNode: function(node, closed) {

        this.nodeUpdate = false;
        this.getForm().reset();
        this.currentNode = node;
        if (node == null) { this.getForm().reset(); return }
        if (!node) {
            this.getForm().reset();
            this.disable();
            return;
        }
        if (!closed) this.enable();
        this.setFields();
        //if (node.attributes.readOnly || node.attributes.properties.locked || eBook.Interface.isFileClosed()) this.disable();
        this.nodeUpdate = true;
        //        if (closed) {
        //            this.currentNode.attributes.iconCls = "eBook-lock-16";
        //            this.disable();
        //        } else {
        this.titleField.focus();
        //        }
        // load form fields with node data.

        // show/hide conform the node type.
    }
    , setFields: function() {
        //var rsettings = this.refOwner.getReportSettings();
        var props = this.currentNode.get('indexItem');
        //this.nodeType.setValue(props.type);
        //this.nodeType.update(props.title, this.currentNode.attributes.iconCls);
        // if (!Ext.isDefined(props.title)) props.title = props.textContent;
        this.setTitle(String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, props.title));
        this.titleField.setValue(props.title);
        //        if (!eBook.Interface.isFileClosed()) {
        //            this.titleField.enable();
        //        }

        //        this.headerData.show();
        //        this.footerData.show();
        //        this.showindex.show();

        if (Ext.isDefined(props.Culture)) {
            this.culture.setValue(props.Culture);
            if (!eBook.Interface.isFileClosed()) {
                this.culture.enable();
            }
            this.culture.setVisible(true);
        } else {
            this.culture.setVisible(false);
        }

        if (props.type == "DOCUMENT") {
            this.culture.disable(); //document language is set on creation.
        }

        if (props.type == "ROOT") {
            this.nodeType.setText(eBook.Bundle.NodeInfoPanel_ReportSettings);
            //this.resetChildren.show();
            this.titleField.hide();
        } else { this.titleField.show(); /*this.resetChildren.hide(); */ }

        if (props.type == "STATEMENT") {
            this.detailed.show();
            this.detailed.setValue(props.Detailed);
        } else {
            this.detailed.hide();
        }


        if (props.type == "PDFPAGE") {
            // this.nodeType.setValue(props.type + ' (' + props.pageCount + ' pages)');
            //this.pageFrom.
            this.pageFrom.setMaxValue(props.Pages);
            this.pageFrom.setValue(Ext.isDefined(props.FromPage) ? props.FromPage : 1);
            this.pageTo.setMaxValue(props.Pages);
            this.pageTo.setValue(Ext.isDefined(props.ToPage) ? props.ToPage : props.Pages);

            this.pageFrom.show();
            this.pageTo.show();
        } else {
            this.pageFrom.hide();
            this.pageTo.hide();
        }

        //        this.headerData.checkbox.dom.checked = Ext.isDefined(props.HeaderConfig) && props.HeaderConfig != null ? props.HeaderConfig.Enabled : false;
        //        this.footerData.checkbox.dom.checked = Ext.isDefined(props.FooterConfig) && props.FooterConfig != null ? props.FooterConfig.Enabled : false;

        //this.showindex.setValue(Ext.isDefined(props.indexed) ? props.indexed : true);
        //        if (Ext.isDefined(props.HeaderConfig) && props.HeaderConfig != null) {
        //            if (props.HeaderConfig.Enabled) this.headerData.titleField.setValue(props.HeaderConfig.ShowTitle);
        //        }

        //        if (Ext.isDefined(props.FooterConfig) && props.FooterConfig != null) {
        //            if (props.FooterConfig.Enabled) this.footerData.pagenr.setValue(props.FooterConfig.ShowPageNr);
        //            if (props.FooterConfig.Enabled) this.footerData.footnote.setValue(props.FooterConfig.ShowFooterNote);
        //        }
        //        this.headerData.onCheckClick();
        //        this.footerData.onCheckClick();

        if (props.type == "COVERPAGE" || props.type == "INDEXPAGE") {
            this.titleField.hide();
            this.titlelabel.hide();
            //            this.headerData.hide();
            //            this.footerData.hide();
            //            this.showindex.hide();
        } else {
            this.titleField.show();
            this.titlelabel.show();
            //            this.showindex.show();
        }

    }
});


Ext.reg('biztaxItemSettings', eBook.BizTax.ItemSettings);/* TIM G REMARKS

    CHANGES:
        Added getData and setData functionality in order to retreive or set data contents
        Changed the tpl as well to add icons
        Added container-wide events to handle mouseovers 
        Added visible selection.
        Moved listeners on dataview to initcomponent. Only render when needed and distinctive listeners if opened multiple times
        Changed the decode/encode copy which was implemented to ensure value-copy rather then reference-copy to
            newly implemented Ext.clone (code-reuse + tweaking of Ext 4)... yay
        Ensured iconCls for indexItem is filled in serverside.
        
    TODO: 
        change store approach with single array, switch from dataview to Panel extension
        
    WHY?: 
        We don't need 80% of store functionality and don't really need to create 2 components (panel+dataview)
            The only reason why a panel and a dataview are used is for top toolbar implementation.
            -> switching to customised view will improve performance and memory signature for this component (less overhead)
            
    REALLY???: 
        Yep, really. Might sound a bit stupid, though consider the magnitude of the entire application and all components.
        Keeping an extensive application such as eBook fast, means cutting back overhead all were applicable.
        Cheers :-)
*/


eBook.BizTax.UploadEditorPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            tbar: [{
                xtype: 'button'
                , text: 'Delete'
                , ref: '../btnDelete'
                , iconCls: 'eBook-icon-delete-16'
                , disabled: true
               , handler: this.onDelete
               , scope: this
            }, '-', {
                xtype: 'button'
                , text: 'Move up'
                , ref: '../btnMoveUp'
                , iconCls: 'eBook-biztax-arrowup-ico'
                , disabled: true
                , direction: 'up'
                , handler: this.onMove
                , scope: this
            }, '-', {
                xtype: 'button'
                , text: 'Move Down'
                , ref: '../btnMoveDown'
                , iconCls: 'eBook-biztax-arrowdown-ico'
                , disabled: true
                , direction: 'down'
                , handler: this.onMove
                , scope: this
}]
            , items: [{
                xtype: 'biztaxUploadEditor', ref: 'editor'
}]
            });
            eBook.BizTax.UploadEditorPanel.superclass.initComponent.apply(this, arguments);
        }
        , onDelete: function() {
            this.editor.deleteSelected();
        }
        , getData: function() {
            return this.editor.getData();
        }
        , setData: function(dta) {
            this.editor.setData(dta);
        }
        , onMove: function(btn, e) {
            this.editor.moveSelected(btn.direction);
        }
        , toggleTButtons: function(idx) {
            this.btnDelete.enable();
            (idx == 0) ? this.btnMoveUp.disable() : this.btnMoveUp.enable();
            (idx == this.editor.all.elements.length - 1) ? this.btnMoveDown.disable() : this.btnMoveDown.enable();
        }
        , disableTButtons: function() {
            this.btnDelete.disable();
            this.btnMoveUp.disable();
            this.btnMoveDown.disable();
        }
        , afterRender: function() {
            eBook.BizTax.UploadEditorPanel.superclass.afterRender.apply(this, arguments);
            // DROP
            this.dvDDTarget = new Ext.dd.DropTarget(this.getEl(), {
                ddGroup: 'eBook.Bundle'
                   , srcView: this.items.items[0]
                   , srcPanel: this
                    , notifyDrop: function(ddSource, e, data) {
                        if (this.srcView) {
                            // If node = pdf
                            if (data.node.attributes.IsRepository) {
                                var repItem = data.node.attributes.Item;
                                attributes = {
                                    __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'
                                        , type: "PDFPAGE"
                                        , ItemId: repItem.Id
                                        , id: eBook.NewGuid()
                                        , title: repItem.FileName
                                        , indexed: true
                                        , iconCls: data.node.attributes.iconCls
                                };
                                data.node.attributes.indexItem = attributes;
                            }


                            // ENCODE DECODE in order to add a unique value instead of a reference/pointer
                            // Tim G: Changed encode decode with cloning method (transferred from Ext 4) :-)
                            var rt = new this.srcView.store.recordType({ indexItem: Ext.clone(data.node.attributes.indexItem) }); //data.node.attributes
                            this.srcView.store.add(rt);
                            this.srcView.clearSelections();
                            this.srcPanel.disableTButtons();
                        }
                        return true;
                    }
            });
        }
    });
    Ext.reg('biztaxUploadEditorPanel', eBook.BizTax.UploadEditorPanel);





    // indexItem should always have a value... so why the split?
    // if indexItem doesn't exist, neither should the record ==> is invalid.
    eBook.BizTax.UploadEditor = Ext.extend(Ext.DataView, {
        tpl: new Ext.XTemplate('<tpl for=".">'
                            , '<div class="biztax-editorwindow-node">'
                                , '<tpl if="values.indexItem != undefined"><tpl for="indexItem"><div class="biztax-uploadeditor-thumb {iconCls}"></div><div class="biztax-uploadeditor-txt">{title}</div></tpl></tpl>'
                                , '<tpl if="values.indexItem == undefined"><div class="biztax-uploadeditor-thumb ebook-icon-16-unknown"></div><div class="biztax-uploadeditor-txt">{text}</div></tpl>'
                            , '</div>'
                          , '</tpl>'
                          , { compiled: true }
    )
    , initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.biztax-editorwindow-node'
            , singleSelect: true
            , selectedClass: 'biztax-editorwindow-nodeselect'
// NEVER USED...
//            , parentWindow: this.refOwner.refOwner
//            , parentPanel: this.refOwner
            , store: new Ext.data.JsonStore({
                root: 'root'
                        , autoDestroy: true
                        , autoLoad: false
                        , fields: eBook.data.RecordTypes.LibraryNode
            })
            , autoScroll: true
        });

        eBook.BizTax.UploadEditor.superclass.initComponent.apply(this, arguments);
    }
    , getData: function() {
        var recs = this.store.getRange();
        return Ext.pluck(Ext.pluck(recs, 'data'), 'indexItem');
    }
    , setData: function(vals) {
        vals = Ext.clone(vals);
        for (var i = 0; i < vals.length; i++) {
            vals[i] = { indexItem: vals[i] };
        }
        this.store.loadData({ root: vals }, false);
    }
    , moveSelected: function(direction) {
        var recs = this.getSelectedRecords();
        if (recs.length == 0) return;
        var rec = recs[0];
        var idx = this.store.indexOf(rec);
        //if ((direction == "down" && idx == this.store.getCount() - 1) || direction == "up" && idx == 0) return;
        var nidx = direction == "down" ? idx + 1 : idx - 1;
        this.store.removeAt(idx);
        this.store.insert(nidx, [rec]);

        // Select your inserted node
        this.select(this.all.elements[nidx]);
        this.refOwner.toggleTButtons(nidx);
    }
    , deleteSelected: function() {
        var recs = this.getSelectedRecords();
        if (recs.lenght == 0) return;
        var rec = recs[0];
        var idx = this.store.indexOf(rec);
        this.store.removeAt(idx); // of index

        // reset the itemsetting form
        //this.ownerCt.ownerCt.itemSettings.suspendEvents();
        this.refOwner.refOwner.itemSettings.getForm().reset();
        this.refOwner.refOwner.itemSettings.disable();
        this.refOwner.refOwner.itemSettings.resumeEvents();
        this.refOwner.disableTButtons();
    }
    , setProperties: function(props) {
        //        if (this.attributes.properties == null) this.attributes.properties = {};
        //        Ext.apply(this.attributes.properties, props);
        //        if (props.type != "ROOT") this.setText(props.title);

        if (this.currentNode.get('indexItem') == null) this.currentNode.get('indexItem') = {};
        Ext.apply(this.currentNode.get('indexItem'), props);
        this.store.commitChanges();
        this.refresh();
        //if (props.type != "ROOT") this.setText(props.title);

    }
    , listeners: {
        click: {
            fn: function(dataView, index, node, e) {

                var parentPanel = this.refOwner;
                this.currentNode = dataView.store.getAt(index);
                parentPanel.refOwner.loadNodeSettings(this.currentNode);
                //parentPanel.enableTButtons();
                parentPanel.toggleTButtons(index);
            }
        }
    }
    });
Ext.reg('biztaxUploadEditor', eBook.BizTax.UploadEditor);
eBook.BizTax.UploadEditorWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , items: [
                { xtype: 'biztaxItemSettings', region: 'west', width: 300, ref: 'itemSettings' }
                , { xtype: 'biztaxUploadEditorPanel', region: 'center', ref: 'uploadEditorPnl', bundle: null} // DATAVIEW INSTEAD OF TREE
                , { xtype: 'bundlelibrary', region: 'east', width: 300, ref: 'library', culture: eBook.Interface.currentFile.get('Culture'), rootName: 'rootBizTax' }
            ]
            /*, tbar: [{
            ref: 'saveBundle',
            text: eBook.BizTax.Window_ReportSave,
            iconCls: 'eBook-icon-savereport-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onSaveList,
            scope: this
            }]*/
        });
        eBook.BizTax.UploadEditorWindow.superclass.initComponent.apply(this, arguments);
        if (this.data) {
            this.uploadEditorPnl.setData(this.data);
        }
    }
    , onSaveList: function() {
        var json = { "allowChildren": false, "allowDrag": true, "cls": "", "iconCls": "eBook-bundle-tree-statement", "id": "activa", "indexItem": { "__type": "StatementsDataContract:#EY.com.eBook.API.Contracts.Data", "EndsAt": 0, "FooterConfig": { "Enabled": true, "FootNote": null, "ShowFooterNote": true, "ShowPageNr": true, "Style": null }, "HeaderConfig": { "Enabled": true, "ShowTitle": true, "Style": null }, "NoDraft": false, "StartsAt": 0, "iTextTemplate": null, "iconCls": null, "id": "efa72689-3502-4bb9-b14e-da020945fd09", "indexed": true, "locked": false, "title": "Activa", "Culture": "nl-BE", "Detailed": true, "FileId": "fe14f2cd-49e8-42cc-897c-61e03afefe26", "layout": "ACTIVA", "type": "STATEMENT" }, "leaf": true, "text": "Activa" };
        var rt = new this.uploadEditorPnl.store.recordType(json);
        this.uploadEditorPnl.store.add(rt);
//        alert("save it to parent and close");
    }
    , close: function() {
        if (this.caller) {
            if(this.uploadEditorPnl) this.caller.updateValue(this.uploadEditorPnl.getData());
        }
        eBook.BizTax.UploadEditorWindow.superclass.close.call(this);
    }
    , loadNodeSettings: function(node) {
        this.itemSettings.loadNode(node, closed);
    }
});        
    
eBook.Client.ManualResourcesWindow = Ext.extend(eBook.Window, {
    currentData:[]
    ,initComponent: function() {
        Ext.apply(this, {
            title: 'eBook resources'
            , layout: 'border'
            , modal:true
            , items: [{ xtype: 'ManualResourceSelectionTab', ref: 'tabs', region: 'center', culture: this.culture, manualId: this.manualId }
                        , { xtype: 'ResourcesSelected', ref: 'overview', region: 'east', width: 250 }
            ]
            , bbar: ['->',{
                text: 'Cancel',
                iconCls: 'eBook-no-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'cancelbtn',
                handler: this.close,
                scope: this
            }, {
                text: 'OK (x selecties)',
                iconCls: 'eBook-yes-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'okbtn',
                handler: this.onOK,
                scope: this
            }]
            , tbar: [{
                text: eBook.Pdf.Window_AddPdf,
                iconCls: 'eBook-pdf-add-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'addpdf',
                handler: this.onAddPdf,
                scope: this
                }]
        });
        eBook.Client.ManualResourcesWindow.superclass.initComponent.apply(this, arguments);
    }
    , onAddPdf: function() {
        var wn = new eBook.Pdf.ManualUploadWindow({ parentCaller: this,manualId:this.manualId });
        wn.show(this);
    }
    , refreshPdfs: function() {
        this.tabs.store.reload();
    }
    , updateSelected: function(dta) {
        this.currentData=dta;
        this.overview.loadData(dta);
        this.getBottomToolbar().okbtn.setText("OK (" + dta.length + " selecties)");
    }
    ,show:function(src) {
        this.src = src;
        eBook.Client.ManualResourcesWindow.superclass.show.call(this);
    }
    , onOK: function(e) {
        if(this.src && this.src.resourcesPicked) {
            this.src.resourcesPicked(this.currentData);
        }
        this.close();
    }
});
Ext.reg('ManualResourcesWindow', eBook.Client.ManualResourcesWindow);

eBook.Client.ManualResourceSelectionTab = Ext.extend(Ext.DataView, {

    initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.eBook-resource-item'
            , style: 'padding:5px'
            , title: 'Pdf repository'
            , autoScroll: true
            , store: new eBook.data.JsonStore({
                selectAction: 'GetResourcesOfType'
                            , autoDestroy: true
                            , autoLoad: true
                            , criteriaParameter: 'crtdc'
                            , fields: eBook.data.RecordTypes.Resource
                            , baseParams: {
                                'Culture': this.culture
                                , 'Id': this.manualId
                                , 'ResourceType': 'MPDF'
                            }
            })
            , tpl: new Ext.XTemplate('<tpl for=".">'
                            , '<div class="eBook-resource-item {iconCls}">{Title}</div><div class="x-clear"></div>'
                            , '</tpl>', { compiled: true })
            , selectedClass: 'x-list-selected'
            , simpleSelect: true
            , multiSelect: true
            , overClass: 'x-list-over'
            // , data: [{ id: 1, title: 'test', __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'}]
            , prepareData: function(dta) {
                dta.iconCls = 'test';
                switch (dta.DC) {
                    //case "PDFManualDataContract:#EY.com.eBook.API.Contracts.Data":
                    case "PDFDataContract:#EY.com.eBook.API.Contracts.Data":
                        dta.iconCls = 'eBook-Client-tree-pdf';
                        break;
                }
                return dta;
            }
        });
        eBook.Client.ManualResourceSelectionTab.superclass.initComponent.apply(this, arguments);
        this.on('selectionchange', this.onChecks, this);
    }
    , onChecks: function(view, nodes) {
        // var dta = Ext.pluck(this.getSelectedRecords(), "json");
        //var dta = Ext.pluck([], "json");
        this.ownerCt.updateSelected(this.getMySelections());
    }
    , getMySelections: function() {
        return Ext.pluck(this.getSelectedRecords(), "json");
    }
});



Ext.reg('ManualResourceSelectionTab', eBook.Client.ManualResourceSelectionTab);


eBook.Worksheets.Fields.Field = {
    attributes: []

    , handleConstruction: function (config) {
        // read attributes
        if (Ext.isDefined(config) && Ext.isDefined(config.attributes)) {
            if (Ext.isDefined(config.attributes.LabelSeperate) && config.isLabel && Ext.isDefined(config.attributes.LabelClass)) {
                config.cls = config.attributes.LabelClass;
            }
            if (Ext.isDefined(config.attributes.LabelSeperate) && !config.isLabel) {
                config.hideLabel = config.attributes.LabelSeperate == 'true';
            }
            if (Ext.isDefined(config.attributes.ItemWidth)) {
                config.width = parseInt(config.attributes.ItemWidth);
            }
            if (Ext.isDefined(config.attributes.ItemColspan)) {
                config.colspan = parseInt(config.attributes.ItemColspan);
            }
            if (Ext.isDefined(config.attributes.LabelSeperate) && config.isLabel && Ext.isDefined(config.attributes.LabelColspan)) {
                config.colspan = parseInt(config.attributes.LabelColspan);
            }
            if (Ext.isDefined(config.attributes.ItemRowspan)) {
                config.rowspan = parseInt(config.attributes.ItemRowspan);
            }
            if (Ext.isDefined(config.attributes.LabelSeperate) && config.isLabel && Ext.isDefined(config.attributes.LabelRowSpan)) {
                config.rowspan = parseInt(config.attributes.LabelRowSpan);
            }
            if (Ext.isDefined(config.attributes.column)) {
                config.column = parseInt(config.attributes.column);
            }
            if (Ext.isDefined(config.attributes.ItemClass) && !config.isLabel) {
                config.cls = config.attributes.ItemClass;
            }
            if (Ext.isDefined(config.attributes.Form)) {
                config.hidden = config.attributes.Form == 'false';
            }
            if (Ext.isDefined(config.attributes.LabelHidden && !config.isLabel)) {
                config.hideLabel = config.attributes.LabelHidden == 'true';
            }
            if (Ext.isDefined(config.attributes.Disabled)) {
                if (config.attributes.Disabled == 'true' || config.attributes.Disabled == 'false') {
                    config.disabled = config.attributes.Disabled == 'true';
                }
                else {
                    config.disabledField = config.attributes.Disabled;
                }
            }

            // construct style
            var style = this.getStyleFromAttributes(config.attributes, 'Item', config.isLabel);
            var labelStyle = this.getStyleFromAttributes(config.attributes, 'Label', config.isLabel);
            if (config.isLabel) {
                if (labelStyle.length > 0) {
                    config.labelStyle = labelStyle;
                } else {
                    if (style.length > 0) { config.labelStyle = style; }
                }
            } else {
                if (!Ext.isDefined(config.hideLabel) || config.hideLabel != false) {
                    if (labelStyle.length > 0) {
                        config.labelStyle = labelStyle;
                    } else {
                        if (style.length > 0) { config.labelStyle = style; }
                    }
                    if (config.hideLabel) {
                   //     config.style = style;
                    }
                }

            }
        }
        return config;
    }
    , onShow: function () {
        this.getVisibilityEl().removeClass('x-hide-' + this.hideMode);
        if (this.disabledField) {
            var fpanel = this.findParentByType(eBook.Worksheets.FormPanel);
            if (!fpanel) return;
            var fld = fpanel.getForm().findField(this.disabledField);
            if (!fld) return;
            var val = fld.getValue();
            if (!Ext.isBoolean(val)) {
                val = val == "true" || val == 1;
            }
            if (val) {
                this.disable();
            } else {
                this.enable();
            }
        }
    }
    , getStyleFromAttributes: function (attribs, tpe, islabel) { //tpe = 'Item' / 'Label'
        var style = '';
        if (Ext.isDefined(attribs[tpe + 'Style'])) {
            if (attribs[tpe + 'Style'].length > 0) {
                style = attribs[tpe + 'Style'];
                style = style.lastIndexOf(';') < style.length - 1 ? style + ';' : style;
            }
        }
        if (Ext.isDefined(attribs[tpe + 'Align'])) {
            style += 'textalign:' + attribs[tpe + 'Align'] + ';';
        }
        if (Ext.isDefined(attribs[tpe + 'TextDecoration'])) {
            style += 'textdecoration:' + attribs[tpe + 'TextDecoration'] + ';';
        }
        if (Ext.isDefined(attribs[tpe + 'PaddingLeft'])) {
            style += 'padding-left:' + parseInt(attribs[tpe + 'PaddingLeft']) + 'px;';
        }
        if (Ext.isDefined(attribs[tpe + 'PaddingRight'])) {
            style += 'padding-right:' + parseInt(attribs[tpe + 'PaddingRight']) + 'px;';
        }
        if (Ext.isDefined(attribs[tpe + 'PaddingTop'])) {
            style += 'padding-top:' + parseInt(attribs[tpe + 'PaddingTop']) + 'px;';
        }
        if (Ext.isDefined(attribs[tpe + 'PaddingBottom'])) {
            style += 'padding-bottom:' + parseInt(attribs[tpe + 'PaddingBottom']) + 'px;';
        }
        if (Ext.isDefined(attribs['Indentation'])) {
            if (islabel) {
                style += 'padding-left:' + parseInt(attribs['Indentation']) + 'px;';
            } else {
                style += 'margin-right:' + parseInt(attribs['Indentation']) + 'px;';
            }
        }
        if (Ext.isDefined(attribs[tpe + 'Style'])) {
            style += attribs[tpe + 'Style'];
            if (style.charAt[style.length - 1] != ';') style += ';';
        }

        return style;
    }
};



eBook.Worksheets.Fields.NumberField = function (config) {
    if (config.attributes) {
        if (config.attributes["minimumValue"]) config.minValue = parseFloat(config.attributes["minimumValue"]);
        if (config.attributes["maximumValue"]) config.maxValue = parseFloat(config.attributes["maximumValue"]);
        if (config.attributes["negative"]) config.allowNegative = config.attributes["negative"].toLower() == 'true';
        if (config.attributes["decimalPrecision"]) {
            config.decimalPrecision = parseInt(config.attributes["decimalPrecision"]);
        } else {
            config.decimalPrecision = 2;
        }
        
        //autoStripChars

    }
    //    if (config.hasInterfaceEvents) {
    //        config.listeners = {
    //            'changed': {
    //                fn: this.handleOnEventTrigger,
    //                scope: this,
    //                delay: 100
    //            }
    //        };
    //    }
    config = this.handleConstruction(config);

    eBook.Worksheets.Fields.NumberField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.NumberField, Ext.form.NumberField, eBook.Worksheets.Fields.Field);
Ext.reg('ewnumberfield', eBook.Worksheets.Fields.NumberField);

eBook.Worksheets.Fields.CurrencyField = function(config) {
    config.decimalPrecision = 2;
    if (config.attributes) {
        if (config.attributes["minimumValue"]) config.minValue = parseFloat(config.attributes["minimumValue"]);
        if (config.attributes["maximumValue"]) config.maxValue = parseFloat(config.attributes["maximumValue"]);
        if (config.attributes["negative"]) config.allowNegative = config.attributes["negative"].toLower() == 'true';
    }
    eBook.Worksheets.Fields.CurrencyField.superclass.constructor.call(this, config);
};

Ext.extend(eBook.Worksheets.Fields.CurrencyField, eBook.Worksheets.Fields.NumberField);
Ext.reg('ewCurrencyField', eBook.Worksheets.Fields.CurrencyField);


eBook.Worksheets.Fields.TextField = function(config) {
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'changed': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config = this.handleConstruction(config);
    eBook.Worksheets.Fields.TextField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.TextField, Ext.form.TextField, eBook.Worksheets.Fields.Field);
Ext.reg('ewtextfield', eBook.Worksheets.Fields.TextField);



eBook.Worksheets.Fields.TextAreaField = function(config) {
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'changed': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };

//    }
    config = this.handleConstruction(config);
    config.grow = true;
    config.width = 400;
    config.growMin = 150;
    //config.preventScrollbars = true;
    eBook.Worksheets.Fields.TextAreaField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.TextAreaField, Ext.form.TextArea, eBook.Worksheets.Fields.Field);
Ext.reg('ewTextArea', eBook.Worksheets.Fields.TextAreaField);

eBook.Worksheets.Fields.DateField = function(config) {
    config.width = config.width ? config.width : 100;
    config.format = 'd/m/Y';
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'changed': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config = this.handleConstruction(config);
    
    eBook.Worksheets.Fields.DateField.superclass.constructor.call(this, config);
};


Ext.extend(eBook.Worksheets.Fields.DateField, Ext.form.DateField, eBook.Worksheets.Fields.Field);

/*
{
    parseDate: function(value) {
        var v = eBook.Worksheets.Fields.DateField.superclass.parseDate.call(this, value);
        if (v == null) {
            value = Ext.data.Types.WCFDATE.convert(value);
            v = eBook.Worksheets.Fields.DateField.superclass.parseDate.call(this, value);
        }
        return v;
    }
},
*/

Ext.reg('ewdatefield', eBook.Worksheets.Fields.DateField);

eBook.Worksheets.Fields.Checkbox = function(config) {
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'check': {
                fn: this.handleOnEventTrigger,
                scope: this
            }
        };
    }

    if (Ext.isDefined(config) && Ext.isDefined(config.attributes)) {
        if (Ext.isDefined(config.attributes.LabelSide)) {
            if (config.attributes.LabelSide == "right") {
                config.boxLabel = config.fieldLabel;
                config.hideLabel = true;
            }
        }
    }
    config = this.handleConstruction(config);

    eBook.Worksheets.Fields.Checkbox.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.Checkbox, Ext.form.Checkbox, eBook.Worksheets.Fields.Field);
Ext.reg('ewcheckbox', eBook.Worksheets.Fields.Checkbox);

eBook.Worksheets.Fields.Booking = function(config) {
    this.isBookingField = true;
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'check': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config = this.handleConstruction(config);
    eBook.Worksheets.Fields.Booking.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.Booking, Ext.form.Checkbox, eBook.Worksheets.Fields.Field);
Ext.reg('ewBooking', eBook.Worksheets.Fields.Booking);

eBook.Worksheets.Fields.Label = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.isLabel = true;
    config = this.handleConstruction(config);
    config.autoWidth = true;
    if (config.labelStyle) config.style = config.labelStyle;
    config.disabledClass = 'eBook-ws-disabled';
    eBook.Worksheets.Fields.Label.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.Label, Ext.form.Label, eBook.Worksheets.Fields.Field);
Ext.reg('ewLabelField', eBook.Worksheets.Fields.Label);
eBook.Worksheets.Fields.SpacerField = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.text = ' ';
    config = this.handleConstruction(config);
    config.hideLabel = true;
    eBook.Worksheets.Fields.SpacerField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.SpacerField, Ext.form.Label, eBook.Worksheets.Fields.Field);
Ext.reg('ewSpacerField', eBook.Worksheets.Fields.SpacerField);


eBook.Worksheets.Fields.PercentField = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.minValue = 0;
    config.maxValue = 100;
    config.allowNegative = false;
    config.decimalPrecision = 4;
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'changed': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config = this.handleConstruction(config);
    eBook.Worksheets.Fields.PercentField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.PercentField, Ext.form.NumberField, eBook.Worksheets.Fields.Field);
Ext.reg('ewpercentfield', eBook.Worksheets.Fields.PercentField);
Ext.reg('ewPercentageField', eBook.Worksheets.Fields.PercentField);

eBook.Worksheets.Fields.Dropdown = function(config) {
    if (Ext.isDefined(config) && Ext.isDefined(config.attributes)) {
        if (Ext.isDefined(config.attributes.ListWidth)) {
            config.listWidth = parseInt(config.attributes.ListWidth);
        }
    }
    config.resizable = Ext.isDefined(config.resizable) ? config.resizable : true;
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'select': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config.nullable=true;
    config = this.handleConstruction(config);
    eBook.Worksheets.Fields.Dropdown.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.Dropdown, Ext.form.ComboBox, eBook.Worksheets.Fields.Field);

Ext.override(eBook.Worksheets.Fields.Dropdown, {
    show: function() {
        eBook.Worksheets.Fields.Dropdown.superclass.show.call(this);
        if(this.rendered) this.getEl().dom.style.width = "";
    }
    , getValue: function() {
        var v = eBook.Worksheets.Fields.Dropdown.superclass.getValue.call(this);
        var r = this.findRecord(this.valueField || this.displayField, v);
        if (r) return r.data;
        if (this.standardValue) return this.standardValue.data;
        return null;
    }
    , setValue: function(dta) {
        if (!dta) {
            this.standardValue = null;
            return;
        }
        if (Ext.isString(dta)) {
            eBook.Worksheets.Fields.Dropdown.superclass.setValue.call(this, dta);
        } else {
            this.standardValue = new this.store.recordType(dta);
            if (this.findIndex(this.valueField || this.displayField, dta.id) == -1) {
                this.store.add(this.standardValue);
            }
            eBook.Worksheets.Fields.Dropdown.superclass.setValue.call(this, dta.id);
        }

    }
    , findRecord: function(prop, val) {
        var idx = this.findIndex(prop, val);
        if (idx == -1) return null;
        return this.store.getAt(idx);
    }
    , findIndex: function(prop, val) {
        return this.store.find(prop, val);
    }
});
Ext.reg('ewdropdown', eBook.Worksheets.Fields.Dropdown);


eBook.Worksheets.Lists.Global = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {

        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cldc';
        cfg.storeParams = { lik: cfg.attributes.ListAttribute1
                            ,lid:null
                            , c: eBook.Interface.Culture
                            , ay: (eBook.Interface.currentFile!=null ? eBook.Interface.currentFile.get('AssessmentYear') : null)
                            , sd: null
                            ,ed:null
        };

        eBook.Worksheets.Lists.Global.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
            })
            
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: this.width || 200
        });
        eBook.Worksheets.Lists.Global.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Global', eBook.Worksheets.Lists.Global);




eBook.Worksheets.Lists.FOD = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {

        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetFodGlobalList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cfldc';
        cfg.storeParams = { ListId: cfg.attributes.ListAttribute1
                            , Culture: eBook.Interface.Culture
        };

        eBook.Worksheets.Lists.FOD.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
            })
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.FOD.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_FOD', eBook.Worksheets.Lists.FOD);


eBook.Worksheets.Lists.InnerRuleAppList = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'InRuleGetList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cirldc';
        cfg.storeParams = {
            FileId: cfg.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
            , Culture: eBook.Interface.Culture
            , RuleApp: cfg.ruleApp
            , BaseFieldName: cfg.entityCollection
            , ListFieldName: cfg.name
        };
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes.ListAttribute1) && Ext.isDefined(cfg.attributes.ListAttribute2)) {

        }

        eBook.Worksheets.Lists.InnerRuleAppList.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        var parfrm = this.findParentByType(eBook.Worksheets.FormPanel);
        if (parfrm && this.manual) {
            this.storeParams.Manual = parfrm.manual;
        }
        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                , rootProp: 'Data'
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.InRuleList
            })
            , displayField: 'Display'
            , valueField: 'Value'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , width: 200
            //, pageSize: 20
            //, listWidth: 100
        });
        eBook.Worksheets.Lists.InnerRuleAppList.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_InRule', eBook.Worksheets.Lists.InnerRuleAppList);

eBook.Worksheets.Lists.Accounts = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        if (cfg.attributes && cfg.attributes['ListAttribute1']) {
            cfg.storeMethod = 'GetAccountsListByRanges';
            cfg.criteriaParameter = 'cardc';
            cfg.mode = 'remote';
            cfg.queryParam = 'Query';
            cfg.storeParams = { FileId: eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
                            , Ranges: cfg.attributes.ListAttribute1.split(',')
            };
        } else {
            cfg.storeMethod = 'GetAccountsList';
            cfg.mode = 'remote';
            cfg.criteriaParameter = 'cadc';
            cfg.storeParams = { FileId: eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
            };
            cfg.queryParam = 'Query';
        }
        // exclude zero saldi?
        if (cfg.attributes && cfg.attributes['ListAttribute2']) {
            if (cfg.attributes.ListAttribute2 == "true") {
                cfg.storeParams.IncludeZeroes = false;
            }
        }
        eBook.Worksheets.Lists.Accounts.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.schema
            })
            , minChars: 1
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , pageSize: 20
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.Accounts.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Accounts', eBook.Worksheets.Lists.Accounts);

eBook.Worksheets.Lists.AccountsFromMapping = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes['ListAttribute1'])) {
            cfg.storeMethod = 'GetAccountsListFilteredByMapping';
            cfg.criteriaParameter = 'cadc';
            cfg.mode = 'remote';
            cfg.queryParam = 'Query';
            cfg.storeParams = { FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                        , Key: cfg.attributes.ListAttribute1
                        , Meta: Ext.isDefined(cfg.attributes.ListAttribute2) ? cfg.attributes.ListAttribute2 : null
                        , MappingItemIds: Ext.isDefined(cfg.attributes.ListAttribute3) ? cfg.attributes.ListAttribute3 : null
            };
//            if (Ext.isDefined(cfg.attributes['ListAttribute3'])) {
//                if (eBook.isGuid(cfg.attributes['ListAttribute3'])) {
//                    cfg.storeParams.WorksheetTypeId = cfg.attributes.ListAttribute3;
//                }
//                else {
//                    cfg.storeParams.WorksheetTypeId = null;
//                    cfg.storeParams.WorksheetTypeRuleApp = cfg.attributes.ListAttribute3;
//                }
//            }

        }
        eBook.Worksheets.Lists.AccountsFromMapping.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.schema
            })
            , minChars: 1
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , pageSize: 20
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.AccountsFromMapping.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_AccountsFromMapping', eBook.Worksheets.Lists.AccountsFromMapping);

eBook.Worksheets.Lists.Mappings = Ext.extend(Ext.form.TriggerField, {
    initComponent: function() {
        this.mappingKey = this.attributes['ListAttribute1'];
        Ext.apply(this, {
            triggerConfig: {
                tag: 'span', cls: 'x-form-twin-triggers', cn: [
                { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger ' },
                { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger x-form-clear-trigger' }
                ]
            }
            , grow: true
            , growMax: 200
            , growMin: 100
            , nullable: true
        });
        this.addEvents(
            'clear',
            'change',
            'autosize'
        );
        this.on('show', function() {
            this.trigger_clear.show();
            this.fireEvent('change', this);
        });
        eBook.Worksheets.Lists.Mappings.superclass.initComponent.apply(this, arguments);
    }
    , onTriggerClick: function() {
        var mnu = eBook.Accounts.Mappings.Menus[this.mappingKey];
        mnu.showField(this.getEl());
        if (mnu.getWidth() < this.getWidth()) {
            mnu.setWidth(this.getWidth());
        }
    }
    , getValue: function() {
        return this.value;
    }
    , setValue: function(val) {
        if (val == null) {
            this.clearValue();
            return;
        }
        eBook.Worksheets.Lists.Mappings.superclass.setValue.call(this, val[eBook.Interface.Culture.substr(0, 2)]);
        this.autoSize();
        this.value = val;
    }
    , autoSize: function() {
        if (!this.grow || !this.rendered) {
            return;
        }
        if (!this.metrics) {
            this.metrics = Ext.util.TextMetrics.createInstance(this.el);
        }
        var el = this.el;
        var v = el.dom.value;
        var d = document.createElement('div');
        d.appendChild(document.createTextNode(v));
        v = d.innerHTML;
        Ext.removeNode(d);
        d = null;
        v += ' ';
        var w = Math.min(this.growMax, Math.max(this.metrics.getWidth(v) + /* add extra padding */10, this.growMin));
        this.el.setWidth(w);
        this.fireEvent('autosize', this, w);
    }
    , onRender: function(ct, position) {
        eBook.Worksheets.Lists.Mappings.superclass.onRender.call(this, ct, position);
        var self = this;
        var triggers = this.trigger.select('.x-form-trigger', true).elements;
        for (var i; i < triggers.length; i++) {
            triggers[i].addClassOnOver('x-form-trigger-over');
            triggers[i].addClassOnOver('x-form-trigger-over');
        }
        if (this.nullable) {
            this.trigger_clear = triggers[1];
            this.trigger_clear.hide = function() {
                var w = self.wrap.getWidth();
                this.dom.style.display = 'none';
                self.el.setWidth(w - self.trigger.getWidth());
            };
            this.trigger_clear.show = function() {
                var w = self.wrap.getWidth();
                this.dom.style.display = '';
                self.el.setWidth(w - self.trigger.getWidth());
            };
            this.mon(this.trigger_clear, 'click', this.clearValue, this, { stopPropagation: true });
        }

        //if (!this.getValue())
        //this.trigger_clear.hide();
    }
    , clearValue: function() {
        if (this.hiddenField) {
            this.hiddenField.value = '';
        }
        this.setRawValue('');
        this.lastSelectionText = '';
        this.applyEmptyText();
        this.value = null;
    }
    , onDestroy: function() {
        Ext.destroy(this.triggers);
        return eBook.Worksheets.Lists.Mappings.superclass.onDestroy.call(this);
    }
});

/*
eBook.Worksheets.Lists.Mappings = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        if (cfg.attributes) {
            cfg.storeMethod = 'GetMappingGroupItems';
            cfg.criteriaParameter = 'cmdc';
            cfg.mode = 'remote';
            cfg.storeParams = { Culture: eBook.Interface.Culture
                            , Key: cfg.attributes['ListAttribute1']
                            , Meta: Ext.isDefined(cfg.attributes['ListAttribute2']) ? cfg.attributes['ListAttribute2'] : null
            };
            console.log(cfg.attributes['ListAttribute1']);
        }
        eBook.Worksheets.Lists.Mappings.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: 'cmdc'
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , autoDestroy: true
            })
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.Mappings.superclass.initComponent.apply(this, arguments);
    }
});
*/
Ext.reg('ewList_Mappings', eBook.Worksheets.Lists.Mappings);


eBook.Worksheets.Lists.Customers = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {

            if (!Ext.isDefined(cfg)) cfg = {};
            cfg.storeMethod = 'GetCustomersList';
            cfg.mode = 'remote';
            cfg.criteriaParameter = 'cbidc';
            cfg.storeParams = { Id: eBook.CurrentClient
                            ,Fields:['Name','FirstName']
            };
            cfg.queryParam = 'Query';

            eBook.Worksheets.Lists.Customers.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.businessrelation
            })
            , minChars:3
            , displayField: eBook.Interface.Culture.substr(0,2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , forceSelection:true
            , lazyRender: true
            , pageSize: 20
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.Customers.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Customers', eBook.Worksheets.Lists.Customers);

eBook.Worksheets.Lists.Suppliers = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {

        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetSuppliersList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cbidc';
        cfg.storeParams = { Id: eBook.CurrentClient
                             , Fields: ['Name', 'FirstName']
        };
        cfg.queryParam = 'Query';

        eBook.Worksheets.Lists.Suppliers.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.businessrelation
            })
            , minChars: 3
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , lazyRender: true
            , pageSize: 20
            , listWidth: 300
        });
        eBook.Worksheets.Lists.Suppliers.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Suppliers', eBook.Worksheets.Lists.Suppliers);


eBook.Worksheets.Lists.Years = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetYears';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cydc';
        cfg.storeParams = { FileId: cfg.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
                        , From: -1
                        , To: 1
                        , Base: 'Assessment'
        };
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes.ListAttribute1) && Ext.isDefined(cfg.attributes.ListAttribute2)) {
            cfg.storeParams.From = parseInt(cfg.attributes.ListAttribute1);
            cfg.storeParams.To = parseInt(cfg.attributes.ListAttribute2);
            if (Ext.isDefined(cfg.attributes.ListAttribute3)) {
                cfg.storeParams.Base = cfg.attributes.ListAttribute3;
            }
            if (Ext.isDefined(cfg.attributes.ListAttribute4)) {
                cfg.storeParams.Min = parseInt(cfg.attributes.ListAttribute4);
            }
            if (Ext.isDefined(cfg.attributes.ListAttribute5)) {
                cfg.storeParams.Max = parseInt(cfg.attributes.ListAttribute5);
            }
        }

        eBook.Worksheets.Lists.Years.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
            })
            , displayField: 'nl'
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , width: 200
            //, pageSize: 20
            //, listWidth: 100
        });
        eBook.Worksheets.Lists.Years.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Years', eBook.Worksheets.Lists.Years);


eBook.Worksheets.Lists.BookyearMonths = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetBookyearMonths';
        cfg.mode = 'local';
        cfg.criteriaParameter = 'cmldc';
        cfg.storeParams = { From: eBook.Interface.currentFile.get('StartDate'), To: eBook.Interface.currentFile.get('EndDate') };

        eBook.Worksheets.Lists.BookyearMonths.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
                    , autoLoad:true
            })
            , displayField: 'nl'
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , forceSelection: true
            , lazyRender: true
            , width: 200
            //, pageSize: 20
            //, listWidth: 300
        });
        eBook.Worksheets.Lists.BookyearMonths.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_BookyearMonths', eBook.Worksheets.Lists.BookyearMonths);

eBook.Worksheets.Lists.Collections = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.mode = 'local';
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes.ListAttribute1) && Ext.isDefined(cfg.attributes.ListAttribute2) && Ext.isDefined(cfg.attributes.ListAttribute3)) {
            cfg.collections = cfg.attributes.ListAttribute1.split(',');
            cfg.displayField = cfg.attributes.ListAttribute2;
            cfg.valueField = cfg.attributes.ListAttribute3;
        }

        eBook.Worksheets.Lists.Collections.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //
        id = Ext.id();
        var sid = String.format("{0}_{1}", this.entityCollection, this.name);
        Ext.apply(this, {
            store: this.ownerCt.ownerCt.ownerCt.getCollectionsStore(this.collections, this.displayField, this.valueField, id)
            , typeAhead: false
            , id:id
            , triggerAction: 'all'
            , lazyRender: true
            , width: 200
            , forceSelection: true
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            //, pageSize: 20
            //, listWidth: 100
        });
        eBook.Worksheets.Lists.Collections.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Collections', eBook.Worksheets.Lists.Collections);



eBook.Worksheets.Lists.DropDownFieldsCopy = Ext.extend(eBook.Worksheets.Lists.Collections, {
    constructor: function(config) {
        if (!Ext.isDefined(config)) config = {};
        if (!Ext.isDefined(config.attributes)) config.attributes = {};
        config.attributes.ListAttribute3 = 'ID';
        eBook.Worksheets.Lists.DropDownFieldsCopy.superclass.constructor.call(this,config);
        this.on('collapse', this.onCollapse);
    }
    , onCollapse: function(combo) {
        var di = eBook.Worksheet[this.ruleApp].findDataItem(this.collections,this.valueField, this.getValue());
        var frmp = this.findParentByType(eBook.Worksheets.FormPanel);
        if (frmp && di!=null) {
            frm = frmp.getForm();
            for (var it in di) {
                if (Ext.isDefined(it) && it != 'remove' && it!='ID') {
                    var f = frm.findField(it);
                    if (Ext.isDefined(f) && f != null) {
                        f.setValue(di[it]);
                    }
                }
            }
            frmp.performEvents();
        }
    }
});
Ext.reg('ewList_DropDownFieldsCopy', eBook.Worksheets.Lists.DropDownFieldsCopy);
Ext.reg('ewDropDownFieldsCopy', eBook.Worksheets.Lists.DropDownFieldsCopy);

eBook.Worksheets.Form = Ext.extend(Ext.form.FormPanel, {
    initComponent: function () {
        var closed = eBook.Interface.currentFile.get('Closed') || this.wsClosed;
        /*
        var test = this.findField('IRRemainingResultBelgium');
        test.markInvalid('You dumb shit!');

        
        var formItems = this.items[0].items;
        console.log(formItems[0].entityCollection);
        console.log('------');
        for (var i = 0; i < formItems.length; i++) {
        console.log(formItems[i].name);
        formItems[i].markInvalid('You dumb shit!');
        }
        console.log(' ');
        */
        Ext.apply(this, {
            bodyStyle: 'padding:10px'
            , autoScroll: true
            , cls: 'ebook-worksheet-form'
            , layoutConfig: { labelAlign: 'left', labelWidth: 300 }
            , labelWidth: 300
        });

        if (closed) {
            Ext.apply(this, {
                defaults: { disabled: true }
            });
        }

        if (!closed) {
            Ext.apply(this, {
                buttons: [{
                    //text:'Bewaar gegevens',
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onValidate,
                    scope: this
                }, {
                    //text: 'Annuleer',
                    ref: '../cancelButton',
                    iconCls: 'eBook-window-cancel-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onCancelClick,
                    scope: this
                }
                ]
            });
        }

        eBook.Worksheets.Form.superclass.initComponent.apply(this, arguments);
    }
    , onCancelClick: function (silent) {
        // detect form only
        var standalone = this.initialConfig.ref == 'myView';
        if (standalone) {
            this.getForm().reset();
        }
        else {
            this.refOwner.showView();
        }
    }
    , onValidate: function () {
        if (this.hasDirtyFields()) {
            var win = this.refOwner.ownerCt.ownerCt;
            var reqObj = {
                cedc: {
                    Id: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
                    , CollectionPath: this.collectionName
                    , Entity: this.getEntityObject()
                }
            };
            win.getEl().mask('Validating', 'x-mask-loading');



            /*
            tabs.each(function(tab) {
            tab.setIconCls = eBook.Interface.center.fileMenu.checkHealthTab(this.tabPanels[i].xtype.split('-')[3]);
            }, this);
            */
            var url = this.ruleEngine.url + this.ruleEngine.baseMethod + this.entityName + 'Validate'; ;
            eBook.CachedAjax.request({
                url: url //eBook.Service.url + action
                , method: 'POST'
                , params: Ext.encode(reqObj)
                , success: this.onValidateSuccess
                , failure: this.onValidateFailure
                , scope: this
            });

        } else {
            this.refOwner.showView();
        }
    }
    , onValidateSuccess: function (resp, opts) {
        // this.entityName
        var win = this.refOwner.ownerCt.ownerCt;
        var resultName = this.ruleEngine.baseMethod + this.entityName + 'ValidateResult'
        var o = Ext.decode(resp.responseText);
        var result = o[resultName];
        if (result.length == 0) {
            //success, can be saved
            this.saveForm();
        } else {
            win.getEl().unmask();
            //alert("ERRORS - To be implemented");
            var bfrm = this.getForm();
            for (var i = 0; i < result.length; i++) {
                var msg = result[i];
                var f = bfrm.findField(msg.wtfld);
                if (f) {
                    f.setActiveError(msg.tx, false);
                    f.markInvalid(msg.tx);
                }
            }
        }

    }
    , onValidateFailure: function (resp, opts) {
        var win = this.refOwner.ownerCt.ownerCt;
        win.unmask();
        alert("validation failed");
    }
    , saveForm: function () {

        if (this.hasDirtyFields()) {
            // find worksheet window to mask
            var win = this.refOwner.ownerCt.ownerCt;

            win.mask(eBook.Worksheet.FormPanel_UpdatingItem, 'x-mask-loading');
            var reqObj = { cedc: {
                Id: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
                    , CollectionPath: this.collectionName
                    , Entity: this.getEntityObject()
            }
            };

            var url = this.ruleEngine.url + this.ruleEngine.baseMethod + this.collectionName;
            url += (reqObj.cedc.Entity.ID == eBook.EmptyGuid) ? 'Add' : 'Update';
            //var action = url + ((id == "ADDING" || Ext.isEmpty(id)) ? "InRuleAddItem" : "InRuleUpdateItem");
            Ext.Ajax.request({
                url: url //eBook.Service.url + action
                , method: 'POST'
                , params: Ext.encode(reqObj)
                , success: this.onSaveSuccess
                , failure: this.onSaveFailure
                , scope: this
            });
        } else {
            this.refOwner.ownerCt.ownerCt.getEl().unmask();
            this.refOwner.showView();
        }
    }
    , onSaveFailure: function (resp, opts) {
        var wn = this.refOwner.ownerCt.ownerCt;
        wn.getEl().unmask();
        eBook.Interface.showResponseError(resp, this.title);
    }
    , onSaveSuccess: function (resp, opts) {
        // show/hide error icon in tab
        var win = this.refOwner.ownerCt.ownerCt;
        var tabs = win.items.items[0].items.items;
        eBook.Interface.reloadHealthUpdateTabs(tabs);


        // subset van alle velden in deze tab met error/warning
        // alle veld markeringen verwijderen, dan
        // veld ophalen voor css toe te passen
        // nml. this.getForm().findField('');

        this.refOwner.ownerCt.ownerCt.getEl().unmask();
        //update worksheet data
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";
        //  this.loadData(o[resKey].Data);
        if (o[resKey]) {
            this.refOwner.showView();
            this.refOwner.ownerCt.loadData(o[resKey]);



            eBook.Interface.reloadHealthUpdateFields(this.getForm());


            /*
            // show / hide error message on field
            var fVb = this.getForm().findField('IRRemainingResultBelgium');
            if (fVb) {
            fVb.enable();
            fVb.markInvalid('You dumb shit!');
            //var fieldId = fVb.id;
            //this.getForm().markInvalid({ id: fieldId, msg: 'You dumb shit 2' });
            }
            */
        } else {
            // eBook.Worksheet[this.ruleApp].loadMessages(o[resKey].Messages);
            //this.setFieldMessages();
            Ext.Msg.show({ icon: Ext.Msg.ERROR, modal: true, msg: eBook.Worksheet.FormPanel_ErrorsMsg, title: eBook.Worksheet.FormPanel_ErrorsTitle, buttons: Ext.Msg.OK });
        }
    }
    , getEntityObject: function () {
        var o = {},
            n,
            key,
            val;
        this.getForm().items.each(function (f) {
            n = f.getName();
            key = o[n];
            val = f.getValue();
            if (Ext.isEmpty(val) || val == "null") val = null;
            if (Ext.isDefined(key)) {
                if (Ext.isArray(key)) {
                    o[n].push(val);
                } else {
                    o[n] = [key, val];
                }
            } else {
                o[n] = val;
            }
        });
        if (o.ID == null) o.ID = eBook.EmptyGuid;
        return o;
    }
    , fieldVisibility: function () {
        var ent = this.getEntityObject();
        this.getForm().items.each(function (f) {
            if (f.functions && f.functions.visibility) {
                var vis = f.functions.visibility(ent);
                if (vis) {
                    f.show();
                } else {
                    f.hide();
                }
            }
        });
    }
    , getWorksheetObject: function () {
        return {
            data: this.refOwner.ownerCt.data
            , findCollectionItem: function (collection, field, value) {
                var col = this.data[collection];
                if (col) {
                    if (!Ext.isArray(col)) return col;
                    for (var i = 0; i < col.length; i++) {
                        var fld = col[i][field];
                        if (Ext.isDefined(fld) && fld == value) return col[i];
                    }
                }
                return {};
            }
        };
    }
    , onFieldChange: function (fld, nv, ov) {
        var funcs = false;
        if (fld.functions) {
            if (fld.functions.hasFunctions) {
                for (var fnc in fld.functions) {
                    var fnt = fld.functions[fnc];
                    if (Ext.isFunction(fnt)) {
                        var ent = fnt.call(this, fld, this, this.getEntityObject(), this.getWorksheetObject(), this);
                        funcs = true;
                        this.setValues(ent, fld.name, true);
                        this.resumeEvents();
                    }
                }
            }
        }
        if (!funcs) this.fieldVisibility();
    }
    , loadRecord: function (rec) {
        if (rec) {
            this.loadDta(rec.data, rec.invalid);
        } else {
            this.clearFields();
        }
    }
    , loadDta: function (dta, invalid, exclude, silent) {
        this.clearFields();
        if (dta) {
            this.setValues(dta, invalid, exclude, silent);

        }
    }
    , clearValues: function () {
        //        this.getForm().applyToFields({ originalValue: null });
        //        this.getForm().items.each(function(f) {
        //            f.suspendEvents();
        //            f.reset();
        //            f.resumeEvents();
        //        });
        //        this.fieldVisibility();
    }
    , clearFields: function () {
        this.getForm().applyToFields({ originalValue: null });
        this.getForm().reset();
        this.fieldVisibility();
    }
    , setValues: function (values, invalid, exclude, silent) {
        var bfrm = this.getForm();

        bfrm.loadedFromGrid = true;

        if (Ext.isArray(values)) {
            for (var i = 0, len = values.length; i < len; i++) {
                var v = values[i];
                if ((!Ext.isEmpty(exclude) && v.id != exclude) || Ext.isEmpty(exclude)) {
                    var f = bfrm.findField(v.id);
                    if (f) {
                        if (silent) f.suspendEvents();
                        if (Ext.isFunction(f.getRecord)) {
                            f.setActiveRecord(values[id]);
                        } else {
                            f.setValue(v.value);
                            f.invalid = invalid
                        }
                        if (bfrm.trackResetOnLoad) {
                            f.originalValue = f.getValue();
                        }
                        if (silent) f.resumeEvents();
                    }
                }
            }
        } else {
            var field, id;
            for (id in values) {
                if ((!Ext.isEmpty(exclude) && id != exclude) || Ext.isEmpty(exclude)) {
                    if (!Ext.isFunction(values[id]) && (field = bfrm.findField(id))) {
                        if (silent) field.suspendEvents();
                        if (Ext.isFunction(field.getRecord)) {
                            field.setActiveRecord(values[id]);
                        } else {
                            field.setValue(values[id]);
                            field.invalid = invalid;
                        }
                        if (bfrm.trackResetOnLoad) {
                            field.originalValue = field.getValue();
                        }
                        if (silent) field.resumeEvents();
                    }
                }
            }
        }
        this.fieldVisibility();
        this.markInvalid(bfrm);
        /*
        var fields = eBook.Interface.center.fileMenu.getFieldsByTab(bfrm.xtype.split('-')[3]);
        if (fields) {
        for (var i = 0; i < fields.items.length; i++) {
        var field = fields.items[i].data;
        var fieldName = field.Field;
        var item = bfrm.findField(fieldName);
        if (item) {
        item.markInvalid(field.Text);
        }
        }
        }
        */
        return this;
    }
    , hasDirtyFields: function () {
        var obj = this.getForm().getFieldValues(true);
        for (var p in obj) {
            return true;
        }
        return false;
    }
    , markInvalid: function (form) {
        var typeId = form.ownerCt.ownerCt.tpeId;
        var fields = eBook.Interface.center.fileMenu.getFieldsByTab(typeId, form.xtype.split('-')[3]);
        if (fields) {
            for (var i = 0; i < fields.items.length; i++) {
                var field = fields.items[i].data;
                var fieldName = field.Field;
                var item = form.findField(fieldName);
                if (item) {
                    if (form.loadedFromGrid) {
                        if (item.invalid) {
                            item.markInvalid(field.Text);
                        }
                    } else {
                        item.markInvalid(field.Text);
                    }
                }

            }
        }
    }
});

Ext.reg('worksheet-form', eBook.Worksheets.Form);eBook.Worksheets.GridRowSelectionModel = Ext.extend(Ext.grid.RowSelectionModel, {
    handleMouseDown: function(g, rowIndex, e) {
        var detailedClick = e.getTarget('.x-grid3-row-expander');
        if (detailedClick) {
            this.grid.plugins.onMouseDown(e, null);
        } else {
            eBook.Worksheets.GridRowSelectionModel.superclass.handleMouseDown.apply(this, arguments);
        }
    }
});


eBook.Worksheets.Grid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        // construct tbar
        var details;
        var expander = null;
        var cols = [];
        var plugins = null;
        if (this.detailGrid) {
            expander = new Ext.ux.grid.RowExpander({
                getBodyContent: function(record, index) {
                    if (!this.enableCaching) {
                        return this.tpl.apply(record.json);
                    }
                    var content = this.bodyContent[record.id];
                    if (!content) {
                        content = this.tpl.apply(record.json);
                        this.bodyContent[record.id] = content;
                    }
                    return content;
                }
            , enableCaching: false
            , dataSelector: this.detailGrid.dataContainer
            , tpl: new Ext.XTemplate(
                '<div class="worksheet-detail-spacing">&nbsp;</div>'
		        , '<div class="worksheet-detail">'
			        , '<div class="worksheet-detail-left-out">'
				        , '<div class="worksheet-detail-left-out-top">&nbsp;</div>'
				        , '<div class="worksheet-detail-left-out-center">&nbsp;</div>'
				        , '<div class="worksheet-detail-left-out-bottom">&nbsp;</div>'
			        , '</div>'
			        , '<div class="worksheet-detail-left-in">'
				        , '<div class="worksheet-detail-left-in-top">&nbsp;</div>'
				        , '<div class="worksheet-detail-left-in-center">&nbsp;</div>'
				        , '<div class="worksheet-detail-left-in-bottom">&nbsp;</div>'
			        , '</div>'
			        , '<div class="worksheet-detail-center">'
				        , '<div class="worksheet-detail-center-center">'
					        , '<div class="worksheet-detail-title">Calculation details:</div>'
					        , '<div class="worksheet-detail-content">'
                                , this.detailGrid.tableTpl
                            , '</div>'
		                , '</div>'
                    , '</div>'
                    , '<div class="worksheet-detail-right-in">'
                        , '<div class="worksheet-detail-right-in-top">&nbsp;</div>'
                        , '<div class="worksheet-detail-right-in-center">&nbsp;</div>'
                        , '<div class="worksheet-detail-right-in-bottom">&nbsp;</div>'
                    , '</div>'
                    , '<div class="worksheet-detail-right-out">'
	                    , '<div class="worksheet-detail-right-out-top">&nbsp;</div>'
	                    , '<div class="worksheet-detail-right-out-center">&nbsp;</div>'
	                    , '<div class="worksheet-detail-right-out-bottom">&nbsp;</div>'
                    , '</div>'
                , '</div>', { getDate: function(val, fmat) {
                    val = Ext.data.Types.WCFDATE.convert(val);
                    return val.format(fmat);
                }
                })
            });
            cols.push(expander);
            plugins = expander;
        }

        cols.push({
            header: ' ',
            width: 20,
            dataIndex: 'IsValid',
            renderer: function(v, m, r, ridx, cidx, s) {
                var css = eBook.Interface.center.fileMenu.getItemMessageType(r.get('ID'));
                if (!Ext.isEmpty(css)) {
                    m.css = 'eb-message-type-' + css;
                    r.invalid = true;
                    //this.ownerCt.setIconClass('eb-message-type-ERROR');
                } else {
                    r.invalid = false;
                }
                return "";
            },
            scope: this,
            align: 'left'
        });
        cols = cols.concat(this.columns);

        var cfg = {
            tbar: []
            , listeners: {}
            , columns: cols
            //, disableSelection: true
            , plugins: plugins
            , sm: new eBook.Worksheets.GridRowSelectionModel({
                singleSelect: true
                        , listeners: {
                            'selectionchange': {
                                fn: this.onRowSelect
                                    , scope: this
                            }
                        }
            })
        };

        var closed = eBook.Interface.currentFile.get('Closed') || this.wsClosed;
        if (!this.readOnly) this.readOnly = closed;

        if (this.allowAdd && !this.readOnly) {
            cfg.tbar.push({
                ref: 'additem',
                text: eBook.Worksheet.GridPanel_AddItem,
                iconCls: 'eBook-icon-add-16',
                scale: 'small',
                iconAlign: 'left',
                handler: this.onAddItemClick,
                scope: this
            });
        }
        if (this.allowEdit && !this.readOnly) {
            cfg.tbar.push({
                ref: 'edititem',
                text: eBook.Worksheet.GridPanel_EditItem,
                iconCls: 'eBook-icon-edit-16',
                scale: 'small',
                iconAlign: 'left',
                disabled: true,
                handler: this.onEditItemClick,
                scope: this
            });
            cfg.listeners.rowdblclick = {
                fn: this.onRowDblClick
                , scope: this
            };
        }
        if (this.allowDelete && !this.readOnly) {
            cfg.tbar.push({
                ref: 'deleteitem',
                text: eBook.Worksheet.GridPanel_DeleteItem,
                iconCls: 'eBook-icon-delete-16',
                scale: 'small',
                iconAlign: 'left',
                disabled: true,
                handler: this.onDeleteItemClick,
                scope: this
            });
        }
        if (cfg.tbar.length == 0) cfg.tbar = null;

        cfg.store = { xtype: 'eb-jsonstore', fields: this.fields, idProperty: 'ID', root: this.collectionName };
        cfg.viewConfig = {
            getRowClass: function(record, index, rowParams, store) {
                var prev = record.get('PreviousImported');
                if (prev) {
                    return 'eb-worksheet-previousimported';
                }
                var a = record.get('AutoLine');
                if (!a) a = record.get('Autoline');
                if (!a) a = record.get('Disabled');
                if (!a) a = record.get('AutoKey');
                if (a) {
                    return 'eb-worksheet-autoline';
                }
                return '';
            }
        };

        this.fields = null;
        Ext.apply(this, cfg);





        eBook.Worksheets.Grid.superclass.initComponent.apply(this, arguments);
    }
    , onAddItemClick: function() {
        this.refOwner.showEdit();
    }
    , onEditItemClick: function() {
        var rec = this.getSelectionModel().getSelected();
        var a = rec.get('AutoLine');
        if (!a) a = rec.get('Autoline');
        if (!a) a = rec.get('Disabled');
        if (!a) a = rec.get('AutoKey');
        if (a) {
            eBook.Interface.showError("This is an automatic eBook line, only manual lines can be edited.", "Automatic line");
            return;
        }
        this.refOwner.showEdit(rec);
    }
    , onDeleteItemClick: function() {

        var rec = this.getSelectionModel().getSelected();
        var idx = this.store.find('ID', rec.get('ID'));
        this.store.removeAt(idx); // TO CHANGE WITH RED MARKING, UPON COMPLETION RELOAD STORES
        var win = this.refOwner.ownerCt.ownerCt; // window
        win.getEl().mask('Removing item');
        eBook.CachedAjax.request({
            url: this.ruleEngine.url + this.ruleEngine.baseMethod + this.collectionName + 'Remove'
                    , method: 'POST'
                    , params: Ext.encode({ cfrdc: {
                        Id: eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
                            , RowId: rec.get('ID')
                    }
                    })
                    , success: this.onDeleteSuccess
                    , failure: this.onDeleteFailure
                    , scope: this
        });


    }
    , onDeleteSuccess: function(resp, opts) {
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";
        this.refOwner.ownerCt.loadData(o[resKey]);
    }
    , onDeleteFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.refOwner.ownerCt.ownerCt.title);
        this.refOwner.ownerCt.ownerCt.getEl().unmask();
    }
    , onRowDblClick: function(grd, ridx, e) {
        if (!this.allowEdit) return;
        var rec = this.store.getAt(ridx);
        if (!Ext.isEmpty(this.rowDisabledField)) {
            if (rec.get(this.rowDisabledField)) {
                return;
            }
        }
        //        var a = rec.get('AutoLine');
        //        if (!a) a = rec.get('Autoline');
        //        if (!a) a = rec.get('Disabled');
        //        if (!a) a = rec.get('AutoKey');
        //        if (a) {
        //            eBook.Interface.showError("This is an automatic eBook line, only manual lines can be edited.", "Automatic line");
        //            return;
        //        }
        this.refOwner.showEdit(rec);
    }
    , highlightRowId: function(row, field) {
        var ridx = this.store.find('ID', row);
        if (ridx > -1) {
            var sm = this.getSelectionModel();
            //sm.clearSelections();
            sm.selectRow(ridx, false);
        }
    }
    , onRowSelect: function(sm) {
        /* TOOLBAR UPDATE */

        if (this.getTopToolbar()) {

            var ed = this.getTopToolbar().edititem;
            var dl = this.getTopToolbar().deleteitem;
            if (!sm.hasSelection()) {
                if (dl) dl.disable();
                if (ed) ed.disable();
            } else {
                if (dl) dl.enable();
                if (ed) ed.enable();
                if (!Ext.isEmpty(this.rowDisabledField)) {
                    var r = sm.getSelected();
                    if (r.get(this.rowDisabledField)) {
                        if (dl) dl.disable();
                        if (ed) ed.disable();
                        sm.clearSelections();
                    }
                }

            }
        }
    }
    , setSheetStatus: function(isClosed) {
        var tb = this.getTopToolbar();
        if (tb) {
            if (this.rendered) {
                tb.show();
                if (isClosed) tb.hide();
            } else {
                tb.hidden = isClosed;
            }
        }
    }
});

Ext.reg('worksheet-grid', eBook.Worksheets.Grid);eBook.Worksheets.Tab = Ext.extend(Ext.Panel, {
    initComponent: function() {
        for (var j = 0; j < this.items.length; j++) {
            this.items[j].wsClosed = this.wsClosed;
        }
        
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
        });

        eBook.Worksheets.Tab.superclass.initComponent.apply(this, arguments);
    }
    , setMsg: function(row, field) {
        if (row) {
            if (this.myView.highlightRowId) {
                this.myView.highlightRowId(row,field);
            }
        }
    }
    , showEdit: function(record) {
        if (this.myEdit) {
            this.layout.setActiveItem(this.myEdit);
            this.myEdit.loadRecord.defer(200, this.myEdit, [record]);
        }
    }
    , showView: function() {
        this.layout.setActiveItem(this.myView);
    }
    , onFieldChange: function(fld, nv, ov) {
        if (this.myEdit) {
            this.myEdit.onFieldChange.call(this.myEdit, fld, nv, ov);
        } else if (this.myView && this.myView.onFieldChange) {
            this.myView.onFieldChange.call(this.myView, fld, nv, ov);
        }
    }
});

Ext.reg('worksheet-tab', eBook.Worksheets.Tab);eBook.Worksheets.TabPanel = Ext.extend(Ext.TabPanel, {
    initComponent: function() {
        this.tpeId = this.ownerCt.typeId;
        for (var i = 0; i < this.tabPanels.length; i++) {
            this.tabPanels[i].wsClosed = this.wsClosed;
            this.tabPanels[i].iconCls = eBook.Interface.center.fileMenu.checkHealthTab(this.tpeId, this.tabPanels[i].xtype.split('-')[3]); // 'eb-message-type-ERROR';
        }
        Ext.apply(this, {
            enableTabScroll: true
            , activeTab: 0
            , items: this.tabPanels
        });
        this.tabPanels = null;
        eBook.Worksheets.TabPanel.superclass.initComponent.apply(this, arguments);
        this.on('beforetabchange', this.onTabChange, this);
        this.on('tabchange', this.onAfterTabChange, this);
    }
    , setMsg: function(tab, row, field) {
        for (var i = 0; i < this.items.getCount(); i++) {
            if (this.items.get(i).xtype.indexOf(tab) > -1) {
                this.setActiveTab(i);
                if (row) {
                    this.items.get(i).setMsg(row, field);
                }
                break;
            }
        }
    }
    , onTabChange: function(tp, newtab, currenttab) {
        return true;
    }
    , onAfterTabChange: function(tp, newtab, currenttab) {
        var form;
        for (var j = 0; j < newtab.items.items.length; j++) {
            if (newtab.items.items[j].form) {
                form = newtab.items.items[j].getForm();
            }
        }
        /*
        var field = form.findField('IRRemainingResultBelgium');
        if (field) {
        field.markInvalid('You dumb shit!');
        }
        */
        var fields = eBook.Interface.center.fileMenu.getFieldsByTab(this.tpeId, form.xtype.split('-')[3]);
        if (fields) {
            for (var i = 0; i < fields.items.length; i++) {
                var field = fields.items[i].data;
                var fieldName = field.Field;
                var item = form.findField(fieldName);
                if (item) {
                    item.markInvalid(field.Text);
                }
            }
        }

        return true;
    }
    , updateTabStatus: function(sts) {
        return true;
    }
    , colStores: {}
    , colStoreIds: []
    , data: null
    , getCollectionsStore: function(collections, displayField, valueField, sid) {
        if (this.colStores[sid]) return this.colStores[sid];

        var flds = [{ name: 'id', mapping: 'ID', type: Ext.data.Types.STRING }
            , { name: 'nl', mapping: displayField, type: Ext.data.Types.STRING }
            , { name: 'fr', mapping: displayField, type: Ext.data.Types.STRING }
            , { name: 'en', mapping: displayField, type: Ext.data.Types.STRING}]
        this.colStores[sid] = new Ext.data.JsonStore({
            fields: flds
            , idProperty: 'id'
            , root: 'cols'
            , collections: collections
            , cfg: { displayField: displayField, collections: collections }
        });
        this.colStoreIds.push(sid);
        if (this.data != null) this.loadColStore[sid];
        return this.colStores[sid];
    }
    , loadColStore: function(sid) {
        var cols = [];
        var str = this.colStores[sid];
        for (var i = 0; i < str.collections.length; i++) {
            if (this.data[str.collections[i]]) {
                cols = cols.concat(this.data[str.collections[i]]);
            }
        }
        if (cols.length > 0) {
            this.colStores[sid].loadData({ cols: cols });
        } else {
            if (this.colStores[sid].getCount() > 0) this.colStores[sid].removeAll();
        }
    }
    , load: function() {
        this.ownerCt.getEl().mask('loading');

        eBook.CachedAjax.request({
            url: this.ruleEngine.url + this.ruleEngine.baseMethod + 'GetData'
                    , method: 'POST'
                    , params: Ext.encode({ cfdc: {
                        FileId: eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
                    }
                    })
                    , success: this.onLoadSuccess
                    , failure: this.onLoadFailure
                    , scope: this
        });
    }
    , onLoadSuccess: function(resp, opts) {
        if(resp.responseText) {
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";

            this.loadData(o[resKey]);
        }
        else
        {
            Ext.Msg.alert("No response from server", "Please contact IT using ebookquestions@be.ey.com.");
            this.ownerCt.getEl().unmask();
        }
    }
    , onLoadFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.refOwner.title);
        this.ownerCt.getEl().unmask();
    }
    , disableRisicoKapitaalHistoriekHistoriekToolbar: function() {
        if (this.ruleEngine.baseMethod == 'RisicoKapitaalHistoriekApp') {
            this.items.each(function(it, idx, len) {
                if (it.myView.collectionName == 'Historiek') {
                    //if (this.data.HistoryTabDisabled)
                    //  it.myView.topToolbar.hide();
                    if (eBook.Interface.currentFile.data.PreviousEndDate)
                        it.myView.topToolbar.hide();
                }
            }, this);
        }
    }
    , loadData: function(dta) {
        this.data = dta;
        this.reloadStores();
        //this.disableRisicoKapitaalHistoriekHistoriekToolbar();
        this.ownerCt.getEl().unmask();
    }
    , reloadStores: function() {
        this.items.each(function(it, idx, len) {
            if (it.myView.store) {
                if (this.data[it.myView.store.root]) {
                    it.myView.store.loadData(this.data);
                } else {
                    it.myView.removeAll();
                }
            } else {
                it.myView.loadDta(this.data[it.myView.collectionName], true);
            }
        }, this);
        for (var i = 0; i < this.colStoreIds.length; i++) {
            this.loadColStore(this.colStoreIds[i]);
        }
    }
    , findCollectionItem: function(collection, field, value) {
        var col = this.data[collection];
        if (col) {
            if (!Ext.isArray(col)) return col;
            for (var i = 0; i < col.length; i++) {
                var fld = col[i][fld];
                if (Ext.isDefined(fld) && fld == value) return col[i];
            }
        }
        return {};
    }
    , tabChange: function(tabPnl, tab) {
        alert('tab clicked');
    }
});

Ext.reg('worksheet-tabpanel', eBook.Worksheets.TabPanel);

eBook.Worksheets.Window = Ext.extend(eBook.Window, {
    wsClosed: false
    , initComponent: function() {
        var tb = [{
            ref: 'recalc',
            text: eBook.Worksheet.Window_Recalc,
            iconCls: 'eBook-icon-refreshworksheet-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onReCalcClick,
            hidden: this.wsClosed,
            scope: this
        }, {
            ref: 'print',
            text: eBook.Worksheet.Window_PrintPreview,
            iconCls: 'eBook-icon-previewworksheet-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onPrintClick,
            scope: this
}];
            if (this.customFunctions) {
                if (this.customFunctions.ProAccImportHistory) {
                    tb.push({
                        ref: 'importhistory',
                        text: eBook.Worksheet.Window_ImportHistory,
                        iconCls: 'eBook-icon-importproacc-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onImportHistoryClick,
                        scope: this
                    });
                }
                if (this.customFunctions.ExportExcel) {
                    tb.push({
                        ref: 'exportexcel',
                        text: 'Export Excel',
                        iconCls: 'eBook-excel-ico-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onExportExcelClick,
                        scope: this
                    });
                }
            }

            tb.push('->');
            tb.push({
                ref: 'help',
                text: eBook.Worksheet.Window_HelpInfo,
                iconCls: 'eBook-icon-helpworksheet-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onHelpClick,
                scope: this
            });
            Ext.apply(this, {
                tbar: tb
            , items: [{ xtype: 'worksheet-tabpanel', wsClosed: this.wsClosed, ruleEngine: this.ruleEngine, tabPanels: this.tabPanels, ref: 'pnls'}]
            , layout: 'fit'
            });
            this.tabPanels = null;
            eBook.Worksheets.Window.superclass.initComponent.apply(this, arguments);
        }
    , show: function(tab, row, field) {
        eBook.Worksheets.Window.superclass.show.call(this);
        this.pnls.load();
        if (tab) {
            this.pnls.setMsg(tab, row, field);
        }
    }

    , onImportHistoryClick: function() {
        this.getEl().mask('importing');
        eBook.CachedAjax.request({
            url: this.ruleEngine.url + this.ruleEngine.baseMethod + 'ProAccImportHistory'
            , method: 'POST'
            , params: Ext.encode({ cfdc: {
                FileId: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
            }
            })
            , success: this.onImportHistorySuccess
            , failure: this.onImportHistoryFailure
            , scope: this
        });
    }
    , onImportHistorySuccess: function(resp, opts) {
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";

        this.pnls.loadData(o[resKey]);
        this.getEl().unmask();
    }
    , onImportHistoryFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.title);
        this.getEl().unmask();
    }
    , onReCalcClick: function() {
        this.pnls.load();
    }
    , onPrintClick: function() {
        this.getEl().mask("Preparing print", 'x-mask-loading');
        var widc = {
            "__type": "WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data"
            , "EndsAt": 0
            , "FooterConfig": { "Enabled": true, "FootNote": null, "ShowFooterNote": true, "ShowPageNr": true, "Style": null }
            , "HeaderConfig": { "Enabled": true, "ShowTitle": true, "Style": null }
            , "NoDraft": false
            , "StartsAt": 0
            , "iTextTemplate": null
            , "iconCls": null
            , "id": eBook.EmptyGuid
            , "indexed": true
            , "locked": false
            , "title": this.title
            , "Culture": eBook.Interface.Culture
            , "Data": null
            , "FileId": eBook.Interface.currentFile.get('Id')
            , "PrintLayout": "default"
            , "RuleApp": this.ruleApp
            , "WorksheetType": this.typeId
            , "man": null
            , "type": null
        };
        Ext.Ajax.request({
            url: eBook.Service.output + 'GetWorksheetPrint'
            , method: 'POST'
            , params: Ext.encode({ widc: widc })
            , callback: this.onPdfGenerated
            , scope: this
        });
    }
    , onPdfGenerated: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetWorksheetPrintResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onExportExcelClick: function() {
        this.getEl().mask("Preparing export", 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.output + 'GetWorksheetExcelExport'
                , method: 'POST'
                , params: Ext.encode({ cewedc: { FileId: eBook.Interface.currentFile.get('Id'), TypeId: this.typeId} })
                , callback: this.onExportExcelCallback
                , scope: this
        });

    }
    , onExportExcelCallback: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open('pickup/' + robj.GetWorksheetExcelExportResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onHelpClick: function() {
        //alert("Check your local champion.");
        eBook.Help.showHelp(this.initialConfig.id);
        //this.initialConfig.id    WORKSHEETID
    }
    , onBeforeClose: function() {
        eBook.Interface.center.fileMenu.updateTaxCalculation();
    }
    });

Ext.reg('worksheet-window', eBook.Worksheets.Window);
eBook.Document.Fields.DefaultField = {
    locationEl: null
    , properties: []
    , parCmpID: ''
    , initComponent: function () {
        //this.addEvents('anchor');
        Ext.apply(this, {
            style: { display: 'inline' }
        });
        //this.locationEl = Ext.get(this.properties.id);
        this.id = this.parCmpID + '_' + this.name;
        if (this.properties.eBook.datatype == 'label') {
            if (Ext.isDefined(this.properties.eBook.defaultValue)) this.setText(this.properties.eBook.defaultValue);
        } else {
            if (Ext.isDefined(this.properties.eBook.defaultValue)) this.setValue(this.properties.eBook.defaultValue);
        }
        /*
        this.on('focus',this.onDocumentFieldFocus,this);
        this.on('blur',this.onDocumentFieldBlur,this);
        */

    }
    /*
    , onDocumentFieldFocus: function() {
    //this.anchor();
    /*
    if(!eBook.Document.Fields.QTip) {
    var cfg = {tag: "div", id: this.id+'_lbldiv', style:"display:inline-block;white-space:normal;padding:5px;z-index:20;background-color:#FFE600;"};
    var el = Ext.get(this.parCmpID);
    eBook.Document.Fields.QTip = el.createChild(cfg);
    }
    eBook.Document.Fields.QTip.update(this.properties.eBook.displayName);
    eBook.Document.Fields.QTip.setWidth(eBook.Document.Fields.QTip.getTextWidth(this.properties.eBook.displayName.replace(' ','_')));
    eBook.Document.Fields.QTip.show();
    eBook.Document.Fields.QTip.anchorTo(this.locationEl,'tl',[0,-this.locationEl.getComputedHeight()]);
        
    }
    ,onDocumentFieldBlur:function() {
    if(eBook.Document.Fields.QTip) {
    eBook.Document.Fields.QTip.hide();
    }
    }
    */
};
    
eBook.Document.Fields.TextField = Ext.extend(Ext.form.TextField,eBook.Document.Fields.DefaultField);
eBook.Document.Fields.NumberField = Ext.extend(Ext.form.NumberField,eBook.Document.Fields.DefaultField);
eBook.Document.Fields.DateField = Ext.extend(Ext.form.DateField,eBook.Document.Fields.DefaultField);
eBook.Document.Fields.DateField = Ext.extend(eBook.Document.Fields.DateField, {
    onTriggerClick : function(){
        this.el = this.locationEl;
        eBook.Document.Fields.DateField.superclass.onTriggerClick.apply(this,arguments);
    }
});
eBook.Document.Fields.TimeField = Ext.extend(Ext.form.TimeField,eBook.Document.Fields.DefaultField);
eBook.Document.Fields.ComboBox = Ext.extend(Ext.form.ComboBox, eBook.Document.Fields.DefaultField);
eBook.Document.Fields.Label = Ext.extend(Ext.form.Label, eBook.Document.Fields.DefaultField);
Ext.QuickTips.init();eBook.Document.Fields.FreeEditor = Ext.extend(Ext.ux.TinyMCE, {
    initComponent: function() {
        Ext.apply(this, {
            tinymceSettings: {
                theme: "advanced",
                plugins: "pagebreak,table,iespell,insertdatetime,searchreplace,paste,directionality,noneditable,xhtmlxtras",
                theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|cut,copy,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,cleanup,code,|,insertdate,inserttime",
                theme_advanced_buttons2: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|",
                //theme_advanced_buttons1 : "bold,italic,underline",
                //theme_advanced_buttons2 : "",
                theme_advanced_buttons3 : "",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                extended_valid_elements: ""
            },
            boxMinHeight: 200
        });
        this.id = this.parCmpID + '_' + this.name;
        
        
        if (this.fullDocument == "true" || this.fullDocument == true) {
                this.height=700;
        }
        // this.id = this.parCmpID + '_' + this.name;
        eBook.Document.Fields.FreeEditor.superclass.initComponent.apply(this, arguments);

    }
    /*,beforeDestroy : function(){
    if(this.monitorTask){
    Ext.TaskMgr.stop(this.monitorTask);
    }
    if (this.rendered) {
    if(!this.getWin()) 
    Ext.destroy(this.tb);
    var doc = this.getDoc();
    if(doc){
    try{
    Ext.EventManager.removeAll(doc);
    for (var prop in doc){
    delete doc[prop];
    }
    }catch(e){}
    }
    if(this.wrap){
    this.wrap.dom.innerHTML = '';
    this.wrap.remove();
    }
    }
    eBook.Document.Fields.FreeEditor.superclass.beforeDestroy.call(this);
    }
    , getWin: function() {
    try {
    return eBook.Document.Fields.FreeEditor.superclass.getWin.call(this);
    } catch (e) {
    return null;
    }
    }
    , getDoc: function() {
    if (Ext.isIE && !this.getWin()) return;
    if (!Ext.isIE && !this.iframe && !this.getWin()) return;
    if (!Ext.isIE && !this.iframe.contentDocument && !this.getWin()) return;
    return Ext.isIE ? this.getWin().document : (this.iframe.contentDocument || this.getWin().document);
    }
    , onRender: function(ct, position) {
    eBook.Document.Fields.FreeEditor.superclass.onRender.call(this, ct, position);
    this.el.setStyle('width', '100%');
    Ext.get(this.iframe).setStyle('width', '100%');
    if (this.fullDocument == "true" || this.fullDocument == true) {
    this.el.setStyle('height', '700px');
    Ext.get(this.iframe).setStyle('height', '700px');
    }
    }*/
    , cleanHtml: function(html) {
        // var cleaned = eBook.Document.Fields.FreeEditor.superclass.cleanHtml.call(this, html);
        //cleaned = html.replace(/<br>/gi, '<br/>');
        //return cleaned;
        return html;
    }
    , setTimedValue: function(v) {
        this.setMeValue.defer(500, this, [v]);
    }
    , setMeValue: function(v) {
        if (!this.rendered) this.setMeValue.defer(500, this, [v]);
        this.setValue(v);
    }
    , getValue: function() {
        //if (!this.getWin()) return "";
        val = eBook.Document.Fields.FreeEditor.superclass.getValue.call(this);
        return this.cleanHtml(val);
    }
    , createToolbar: function(editor) {
        /*
        var items = [];
        var tipsEnabled = Ext.QuickTips && Ext.QuickTips.isEnabled();


        function btn(id, toggle, handler) {
        return {
        itemId: id,
        cls: 'x-btn-icon',
        iconCls: 'x-edit-' + id,
        enableToggle: toggle !== false,
        scope: editor,
        handler: handler || editor.relayBtnCmd,
        clickEvent: 'mousedown',
        tooltip: tipsEnabled ? editor.buttonTips[id] || undefined : undefined,
        overflowText: editor.buttonTips[id].title || undefined,
        tabIndex: -1
        };
        }

        if (this.enableFormat) {
        items.push(
        btn('bold'),
        btn('italic'),
        btn('underline')
        );
        }

        if (this.enableFontSize) {
        items.push(
        '-',
        btn('increasefontsize', false, this.adjustFont),
        btn('decreasefontsize', false, this.adjustFont)
        );
        }


        if (this.enableAlignments) {
        items.push(
        '-',
        btn('justifyleft'),
        btn('justifycenter'),
        btn('justifyright')
        );
        }

        if (!Ext.isSafari2) {
        if (this.enableLinks) {
        items.push(
        '-',
        btn('createlink', false, this.createLink)
        );
        }

            if (this.enableLists) {
        items.push(
        '-',
        btn('insertorderedlist'),
        btn('insertunorderedlist')
        );
        }
        if (this.enableSourceEdit) {
        items.push(
        '-',
        btn('sourceedit', true, function(btn) {
        this.toggleSourceEdit(!this.sourceEditMode);
        })
        );
        }
        }

        //items.push('-', btn('save', false, this.onSave));

        // build the toolbar
        var tb = new Ext.Toolbar({
        renderTo: this.wrap.dom.firstChild,
        items: items
        });



        // stop form submits
        this.mon(tb.el, 'click', function(e) {
        e.preventDefault();
        });

        this.tb = tb;
        this.tb.doLayout();
        */
    }
});

eBook.Document.Fields.BlockEditor = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'vbox'
            , layoutConfig: { align: 'stretch' }
            , height: 300
            , header: false
            , items: [new Ext.form.TextArea({ ref: 'title', height: 60 }), new eBook.Document.Fields.HtmlEditor({ ref: 'text', height: 200 })]
            , tbar: new Ext.Toolbar({
                items: [{
                        ref: 'savechanges',
                        text: eBook.Document.Fields.BlockEditor_Save,
                        iconCls: 'eBook-icon-16-save',
                        scale: 'small',
                        iconAlign: 'top',
                        handler: this.commitEdit,
                        scope: this
                    }, {
                        ref: 'cancel',
                        text: eBook.Document.Fields.BlockEditor_Cancel,
                        iconCls: 'eBook-icon-16-cancel',
                        scale: 'small',
                        iconAlign: 'top',
                        handler: this.cancelEdit,
                        scope: this
                    }]
                })
            });
            eBook.Document.Fields.BlockEditor.superclass.initComponent.apply(this, arguments);
        }
    , startEdit: function(el, rec) {
        if (this.rendered) {
            this.el.remove();
        }
        this.contEl = Ext.get(el);
        this.activeRecord = rec;
        if (this.contEl.child('.eBook-documents-block-title')) {
            this.contEl.child('.eBook-documents-block-title').setDisplayed('none');
        }
        if (this.contEl.child('.eBook-documents-block-txt')) {
            this.contEl.child('.eBook-documents-block-txt').setDisplayed('none');
        }
        this.render(el);
        this.title.setValue(rec.get('Title'));
        this.text.setValue(rec.get('Text'));

    }
    , commitEdit: function() {
        this.activeRecord.set('Title', this.title.getValue());
        this.activeRecord.set('Text', this.text.getValue());
        this.cancelEdit();
    }
    , cancelEdit: function() {
        this.activeRecord.isEditing = false;
        if (this.contEl.child('.eBook-documents-block-title')) {
            this.contEl.child('.eBook-documents-block-title').setDisplayed('');
        }
        if (this.contEl.child('.eBook-documents-block-txt')) {
            this.contEl.child('.eBook-documents-block-txt').setDisplayed('');
        }
        this.destroy();
    }
    
});



eBook.Document.Fields.HtmlEditor = Ext.extend(Ext.ux.TinyMCE, {
    initComponent: function() {
        Ext.apply(this, {
            tinymceSettings: {
                theme: "advanced",
                plugins: "pagebreak,table,iespell,insertdatetime,searchreplace,paste,directionality,noneditable,xhtmlxtras",
                theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|cut,copy,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,cleanup,code,|,insertdate,inserttime",
                theme_advanced_buttons2: "",
                theme_advanced_buttons3: "",
                //theme_advanced_buttons4: ",abbr,acronym,del,ins,|,visualchars,nonbreaking,pagebreak",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: true,
                extended_valid_elements: ""
            },
            boxMinHeight: 200
        });
        eBook.Document.Fields.HtmlEditor.superclass.initComponent.apply(this, arguments);
    }
    , startEdit: function(el, val) {
        if (Ext.isDefined(val)) this.setValue(val);
        if (this.rendered) {
            this.el.remove();
            this.rendered = false;
        }

        this.render(el);
    }
});

eBook.Document.EntryPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            html: 'EntryPanel'
        });
        eBook.Document.EntryPanel.superclass.initComponent.apply(this, arguments);
    }
});
eBook.Document.BlockContextMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            items: [{
                text: 'Move Up',
                iconCls: 'moveup',
                scope: this,
                handler: this.onMoveUp
            }, {
                text: 'Move Down',
                iconCls: 'movedown',
                scope: this,
                handler: this.moveDown
            }, '-', {
                text: 'Edit',
                iconCls: 'editblock',
                scope: this,
                handler: this.onEdit
            }, {
                text: 'Delete',
                iconCls: 'deleteblock',
                scope: this,
                handler: this.onDelete
}]
            });
            eBook.Document.BlockContextMenu.superclass.initComponent.apply(this, arguments);
        }
    , loadBlock: function(rec, str, container, node) {
        this.activeRecord = rec;
        this.activeStore = str;
        this.activeContainer = container;
        this.activeNode = node;
    }
    , onEdit: function() {
        if (this.activeRecord && this.activeContainer && this.activeNode) {
            this.activeContainer.editItem(this.activeNode, this.activeRecord);
        }
    }
    , onDelete: function() {
        if (this.activeRecord && this.activeStore) {
            this.activeStore.remove(this.activeRecord);
            var order = 0;
            this.activeStore.each(function(r) {
                r.set('Order', order);
                order++;
                //if (r.get('Order') >= (order - 1)) r.set('Order', r.get('Order') + 1);
            }, this);
            this.activeStore.commitChanges();
            this.activeStore.sort('Order', 'ASC');
        }
    }
    , onMoveUp: function() {

        if (this.activeRecord && this.activeStore) {
            var order = this.activeRecord.get('Order');
            if (order == 0) return;
            this.activeStore.each(function(r) {
                if (r.get('Order') == (order - 1)) r.set('Order', order);
                //if (r.get('Order') >= (order - 1)) r.set('Order', r.get('Order') + 1);
            }, this);
            this.activeRecord.set('Order', order - 1);
            this.activeRecord.commit();
            this.activeStore.commitChanges();
            this.activeStore.sort('Order', 'ASC');
        }
    }
    , moveDown: function() {
        if (this.activeRecord && this.activeStore) {
            var order = this.activeRecord.get('Order');
            if (order == this.activeStore.getCount()) return;
            this.activeStore.each(function(r) {
                if (r.get('Order') == (order + 1)) r.set('Order', order);
            }, this);
            this.activeRecord.set('Order', order + 1);
            this.activeRecord.commit();
            this.activeStore.commitChanges();
            this.activeStore.sort('Order', 'ASC');
        }
    }
    });

eBook.Document.BlockContainer = Ext.extend(Ext.DataView, {
    blockTypes: []
    , initComponent: function() {
        Ext.apply(this, {
            autoHeight: true,
            autoScroll: true,
            containerMouseEvents: true,
            multiSelect: false,
            overClass: 'x-view-over',
            selectedClass: 'eBook-documents-block-select'
        });

        switch (this.viewType) {
            case 'list':
                Ext.apply(this, {
                    itemSelector: 'li.eBook-documents-block'
                    , tpl: new Ext.XTemplate(
                        '<tpl for=".">',
                        '<ul><li class="eBook-documents-block eBook-documents-block-title" id="' + this.id + '-{Order}">{Title}</li></ul>',
                        '</tpl>')
                });
                break;
            default:
                Ext.apply(this, {
                    itemSelector: 'div.eBook-documents-block'
                    , tpl: new Ext.XTemplate(
                        '<tpl for=".">',
                        '<div class="eBook-documents-block" id="' + this.id + '-{Order}"><div class="eBook-documents-block-title" style="font-weight:bold;">{Title}</div>',
                        '<div class="eBook-documents-block-txt">{Text}</div></div>',
                        '',
                        '</tpl>')
                });
                break;
        }
        eBook.Document.BlockContainer.superclass.initComponent.apply(this, arguments);

        if (!eBook.Interface.currentFile.get('Closed')) this.on('render', this.initializeDropZone, this);
        if (!eBook.Interface.currentFile.get('Closed')) this.on('dblclick', this.onItemDoubleClick, this);
        if (!eBook.Interface.currentFile.get('Closed')) this.on('contextmenu', this.onItemContextmenu, this);
    }
    , destroy: function() {
        if (this.mnuContext) this.mnuContext.destroy();
        eBook.Document.BlockContainer.superclass.destroy.call(this);
    }
    , showMenu: function(rec, store, el, e) {
        if (!this.mnuContext) {
            this.mnuContext = new eBook.Document.BlockContextMenu({});
        }
        this.mnuContext.loadBlock(rec, store, this,el);
        this.mnuContext.showAt(e.getXY());
    }
    , onItemContextmenu: function(dv, idx, n, e) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var rec = this.store.getAt(idx);
        if (rec) this.showMenu(rec, this.store, n, e);
    }
    , onItemDoubleClick: function(dv, idx, n, e) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var rec = this.store.getAt(idx);
        this.editItem(n, rec);
    }
    , editItem: function(n, rec) {
        if (!Ext.isDefined(rec.isEditing) || !rec.isEditing) {
            rec.isEditing = true;
            this.editor = new eBook.Document.Fields.BlockEditor({});
            this.editor.startEdit(n, rec);
        }
    }
    , validateBlockType: function(bt) {
        for (var i = 0; i < this.blockTypes.length; i++) {
            if (bt.toLowerCase() == this.blockTypes[i].toLowerCase()) return true;
        }
        return false;
    }
    , initializeDropZone: function(v) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.dropZone = new Ext.dd.DropZone(v.getEl(), {
            vw: v,
            ddGroup: 'eBook-document-blocks',
            overClass: 'eBook-documents-blocks-over',
            overOkClass: 'eBook-documents-blocks-over-ok',
            overFailClass: 'eBook-documents-blocks-over-fail',
            notifyOut: function(dd, e, data) {
                this.el.removeClass(this.overFailClass);
                this.el.removeClass(this.overOkClass);
            },
            /* notifyEnter: function(dd, e, data) {
            if (this.vw.validateBlockType(data.blockData.blockTypeId)) {
            return Ext.dd.DropTarget.prototype.dropAllowed;
            }
            return Ext.dd.DropTarget.prototype.dropNotAllowed;
            },*/
            onContainerOver: function(dd, e, data) {
                if (this.vw.validateBlockType(data.blockData.blockTypeId)) {
                    this.el.addClass(this.overOkClass);
                    return Ext.dd.DropTarget.prototype.dropAllowed;
                }
                this.el.addClass(this.overFailClass);
                return Ext.dd.DropTarget.prototype.dropNotAllowed;
            },
            onContainerDrop: function(dd, e, data) {
                if (this.vw.validateBlockType(data.blockData.blockTypeId)) {
                    var exel = e.getTarget('.eBook-documents-block');
                    var order = this.vw.store.getCount();
                    if (exel) {
                        order = parseInt(exel.id.replace(this.vw.id + '-', ''));
                        this.vw.store.each(function(r) {
                            if (r.get('Order') >= order) r.set('Order', r.get('Order') + 1);
                        }, this);
                    }
                    this.el.removeClass(this.overFailClass);
                    this.el.removeClass(this.overOkClass);
                    var rt = this.vw.store.recordType;
                    var rec = new rt(data.blockData);
                    rec.set('Order', order);
                    this.vw.store.add(rec);
                    rec.commit();
                    this.vw.store.sort('Order', 'ASC');
                    this.vw.store.commitChanges();
                    data = null;
                    return true;
                }
                return false;
            }
        });
    }
});/*
eBook.Document.BlocksDataView = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            html: 'Blocks dataview'
            ,region:'east'
        });
        eBook.Document.BlocksDataView.superclass.initComponent.apply(this, arguments);
    }
});

*/

eBook.Document.BlocksPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.storeCfg.id = Ext.id();
        Ext.apply(this, {
            items: [new eBook.Document.BlocksDataView({ store: new Ext.data.JsonStore(this.storeCfg), blockTypeId: this.blockTypeId })]
            ,autoScroll:true       
        });
        eBook.Document.BlocksPanel.superclass.initComponent.apply(this, arguments);
    }
});

eBook.Document.BlocksDataView = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.eBook-documents-view-block'
            , tpl: new Ext.XTemplate(
                        '<tpl for=".">',
                        '<div class="eBook-documents-view-block" id="' + this.id + '-{[xindex]}"><div class="eBook-documents-view-block-title">{nameTitle}</div>',
                        '<div class="eBook-documents-view-block-txt">{shorttxt}</div>',
                         '</div>', //<div class="eBook-documents-view-block-txt">{shorttxt}</div>
                        '</tpl>')
            , prepareData: function(obj) {
                obj.nameTitle = Ext.isEmpty(obj.Title) ? Ext.util.Format.stripTags(obj.Text).substr(0, 30) + '...' : obj.Title;
                obj.shorttxt = Ext.util.Format.stripTags(obj.Text).substr(0, 20) + '...';
                return obj;
            }
            , autoScroll: true
        });
        eBook.Document.BlocksDataView.superclass.initComponent.apply(this, arguments);
        if (!eBook.Interface.currentFile.get('Closed')) this.on('render', this.initializeDragZone, this);
    }
    , initializeDragZone: function(v) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.dragZone = new Ext.dd.DragZone(v.getEl(), {
            dragView: v,
            ddGroup: 'eBook-document-blocks',
            getDragData: function(e) {
                var sourceEl = e.getTarget(this.dragView.itemSelector, 10);
                if (sourceEl) {
                    var d = sourceEl.cloneNode(true);
                    d.id = Ext.id();
                    var tdta = {};
                    Ext.apply(tdta, this.dragView.getRecord(sourceEl).data);
                    tdta.blockTypeId = this.dragView.blockTypeId;
                    return this.dragView.dragData = {
                        sourceEl: sourceEl
                        , repairXY: Ext.fly(sourceEl).getXY()
                        , ddel: d
                        , blockData: tdta
                    }

                }
            },
            getRepairXY: function() {
                return this.dragData.repairXY;
            }
        });
    }
});


eBook.Document.Editor = Ext.extend(Ext.BoxComponent, {
    fields: new Ext.util.MixedCollection()
    , blocks: new Ext.util.MixedCollection()
    , blockStores: new Ext.util.MixedCollection()
    , id: 'ebook-document-editor'
    , onRender: function (ct) {
        this.el = ct.createChild({ id: this.id, cls: 'eBook-document-editor', tag: 'div' });
        eBook.Document.Editor.superclass.onRender.apply(this, arguments);


    }
    , getLayoutTarget: function () {
        return this.el;
    }
    , initComponent: function () {
        Ext.apply(this, {
            html: 'editor'
            , region: 'center'
            , style: 'background-color:#D0D0D0;'
            , autoScroll: true
        });
        eBook.Document.Editor.superclass.initComponent.apply(this, arguments);
    }
    , loadTemplate: function (html) {
        this.el.update(html);
        this.bodyEl = Ext.get(Ext.select('div#' + this.id + ' div.eBook-document-body').elements[0]);

        this.processBlockContainers();

    }
    , loadDocument: function (doc) {

        if (doc.bcd != null && Ext.isArray(doc.bcd)) {
            for (var i = 0; i < doc.bcd.length; i++) {
                var str = this.blockStores.get(doc.bcd[i].n);
                if (Ext.isDefined(str) && str != null) {
                    str.loadData(doc.bcd[i].bs);
                }
            }
        }
        if (doc.df != null && Ext.isArray(doc.df)) {
            for (var i = 0; i < doc.df.length; i++) {
                var fid = this.id + '_' + doc.df[i].n;
                var fld = this.fields.get(fid);
                if (fld != null && doc.df[i].v != null && Ext.isDefined(doc.df[i].v)) {
                    if (Ext.isDefined(fld.valRenderer) && Ext.isFunction(fld.valRenderer)) {
                        fld.setValue(fld.valRenderer(doc.df[i].v));
                    } else {
                        fld.setValue(doc.df[i].v);
                    }
                    if (fld.rendered && fld.syncValue) {
                        fld.syncValue();
                        fld.originalValue = doc.df[i].v;
                    }
                }
            }
        }
    }
    , clearAll: function () {
        this.clearFields();
        this.clearBlocks();
        this.el.update("");
    }
    , clearFields: function () {
        this.fields.each(function (fld) {
            fld.destroy();
        }, this);
        this.fields.clear();
    }
    , clearBlocks: function () {
        this.blocks.each(function (bl) {
            bl.destroy();
        }, this);
        this.blocks.clear();

        this.blockStores.eachKey(function (key) {
            this.blockStores.get(key).destroy();
        }, this);
        this.blockStores.clear();
    }
    , getBlockContainers: function () {
        //get array with blockcontainers and data
        var bcd = [];
        this.blockStores.eachKey(function (str) {
            var bs = this.blockStores.get(str);
            var bso = {
                n: str
                , bs: []
            };
            bs.each(function (rec) {
                bso.bs.push({
                    o: rec.get('Order')
                    , ti: rec.get('Title')
                    , text: rec.get('Text')
                });
            }, this);
            bcd.push(bso);
        }, this);
        return bcd;
    }
    , getFields: function () {
        // get array with fields
        var flds = [];
        this.fields.each(function (fld) {
            if (fld.dataType == 'label') {
                var fv = fld.text;
                var o = {
                    did: this.refOwner.activeDocument.id
                    , n: fld.name
                    , dt: fld.dataType
                    , v: fv
                };
                flds.push(o);
            } else {
                var fv = fld.getValue();
                if (fld.isValid() && fv != null && fv != "") {
                    var o = {
                        did: this.refOwner.activeDocument.id
                    , n: fld.name
                    , dt: fld.dataType
                    , v: fv
                    };
                    if (fld.dataType.toLowerCase() == 'datetime' || fld.dataType.toLowerCase() == 'date') {
                        delete o.v;
                        o.dv = fv;
                    }
                    flds.push(o);
                }
            }

        }, this);
        return flds;
    }
    , processBlockContainers: function () {
        // compile fields
        var freeEdits = this.bodyEl.select('.eBook-documents-free-edit').elements;
        for (var i = 0; i < freeEdits.length; i++) {
            this.renderFreeEdit(Ext.get(freeEdits[i]));
        }


        var fieldEls = this.bodyEl.select('.eBook-documents-field').elements;
        for (var i = 0; i < fieldEls.length; i++) {
            this.renderField(Ext.get(fieldEls[i]));
        }

        //this.alignFields();
        var blockdivs = this.bodyEl.select('div.eBook-documents-blocks').elements;
        for (var i = 0; i < blockdivs.length; i++) {
            this.renderBlock(Ext.get(blockdivs[i]));
        }
    }
    , getFieldProperties: function (el) {
        var properties = [];
        el = Ext.get(el);
        properties['eBook'] = [];
        properties['eBook']['name'] = el.getAttributeNS('eBook', 'name');
        properties['eBook']['fullDocument'] = el.getAttributeNS('eBook', 'fullDocument');
        if (properties['eBook']['name'] == null) properties['eBook']['name'] = el.getAttributeNS('eBook', 'Name');
        properties['eBook']['datatype'] = el.getAttributeNS('eBook', 'datatype');
        properties['eBook']['defaultValue'] = el.getAttributeNS('eBook', 'defaultValue');

        properties['eBook']['list'] = el.getAttributeNS('eBook', 'list');
        /* 
        properties['eBook']['displayName'] = el.getAttributeNS('eBook', 'displayName');
        properties['eBook']['value'] = el.getAttributeNS('eBook', 'value');
        properties['eBook']['store'] = el.getAttributeNS('eBook', 'store');
        properties['eBook']['value'] = properties['eBook']['value'] ? properties['eBook']['value'] : '';
        */
        return properties;
    }
    , renderField: function (el) {
        var fProps = this.getFieldProperties(el);
        var fld;
        //if (fProps.eBook.list == eBook.EmptyGuid) {
        switch (fProps.eBook.datatype.toLowerCase()) {
            case 'date':
            case 'datetime':
                fld = new eBook.Document.Fields.DateField({
                    applyTo: el,
                    locationEl: el,
                    //label: fProps.eBook.displayName,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    format: 'd/m/Y',
                    parCmpID: this.id,
                    properties: fProps,
                    valRenderer: Ext.data.Types.WCFDATE.convert
                });
                offs = 16;
                break;
            case 'time':
                fld = new eBook.Document.Fields.TimeField({
                    applyTo: el,
                    locationEl: el,
                    // label: fProps.eBook.displayName,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    parCmpID: this.id,
                    properties: fProps
                });
                offs = 16;
                break;
            case 'number':
            case 'int':
                fld = new eBook.Document.Fields.NumberField({
                    applyTo: el,
                    locationEl: el,
                    //  label: fProps.eBook.displayName,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    parCmpID: this.id,
                    properties: fProps
                });
                break;
            case 'combobox':
                var item = (fProps.eBook.list).split("-");

                for (var i = 0, c = item.length; i < c; i++) {
                    item[i] = [item[i]];
                }

                var items = new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'displayText'
                    ],
                    data: item
                }),

                fld = new eBook.Document.Fields.ComboBox({
                    applyTo: el,
                    locationEl: el,
                    label: "combo",
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    style: 'display:inline',
                    hideLabel: true,
                    enableKeyEvents: true,
                    store: items,
                    displayField: 'displayText',
                    typeAhead: true,
                    mode: 'local',
                    forceSelection: false,
                    editable: false,
                    triggerAction: 'all',
                    selectOnFocus: true,
                    valueField: 'displayText',
                    parCmpID: this.id,
                    properties: fProps,
                    width: 300,
                    listeners: {
                        change: function (t, newv, oldv) {
                            var items = document.querySelectorAll('input[data-name="' + fProps.eBook.name + '"]');
                            for (var i = 0; i < items.length; i++) {
                                items[i].value = newv;
                                items[i].readOnly = true;
                                //items[i].setAttribute("value", newv);
                                //this.setValue(newv);
                                //this.setRawValue(newv);
                            }
                        },
                        afterrender: function (cmp) {
                            cmp.getEl().set({
                                "data-name": fProps.eBook.name
                            });
                        }
                    }


                });
                break;
            case 'label':
                fld = new eBook.Document.Fields.Label({
                    style: 'border: 1px solid orange !important;',
                    cls: 'eBook-document-tooltip',
                    applyTo: el,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    locationEl: el,
                    width: '100%',
                    readOnly: true,
                    listeners: {
                        afterrender: function (c) {
                            c.el.dom.readOnly = true;
                            var text = c.properties.eBook.list.split('((br))').join('<br/>');
                            text = text.split('((i))').join('<i>');
                            text = text.split('((/i))').join('</i>');
                            text = text.split('((b))').join('<b>');
                            text = text.split('((/b))').join('</b>');
                            new Ext.ToolTip({
                                target: c.el.dom,
                                html: text
                            });
                            eBook.Document.Fields.Label.superclass.afterRender.call(this);
                        }
                    },
                    parCmpID: this.id,
                    properties: fProps
                });
                break;
            default:

                parNode = false;
                fld = new eBook.Document.Fields.TextField({
                    applyTo: el,
                    grow: true,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    locationEl: el,
                    //  label: fProps.eBook.displayName,
                    listeners: {
                        'autosize': {
                            fn: function (fld, wd) {
                                fld.setWidth(wd + 20);
                            }, scope: this
                        },
                        afterrender: function (cmp) {
                            cmp.getEl().set({
                                "data-name": fProps.eBook.name
                            });
                        }
                    },
                    parCmpID: this.id,
                    properties: fProps
                });

                break;
        }
        // } else {
        /*
        fld = new eBook.Document.Fields.ComboBox({
        locationEl: el,
        label: fProps.eBook.displayName,
        style: 'display:inline',
        hideLabel: true,
        enableKeyEvents: true,
        store: MetaDataLookup,
        filterInfo: { fnc: function(rec) {
        var key = rc.get("listID");
        return (key == fProps.eBook.list || key == eBook.EmptyGuid);
        }
        },
        displayField: 'displayText',
        typeAhead: true,
        mode: 'local',
        forceSelection: false,
        triggerAction: 'all',
        selectOnFocus: true,
        valueField: 'value',
        parCmpID: this.id,
        properties: fProps
        });
        offs = 16;
        */
        //}


        //el.setWidth(fld.getBox().width+offs);
        fld.render();
        fld.el = el;
        this.fields.add(fld.id, fld);
    }
    , renderFreeEdit: function (el) {
        // create htmlfield
        //set data
        var fProps = this.getFieldProperties(el);
        var fld;
        var defval = el.dom.innerHTML;
        el.update("");
        fld = new eBook.Document.Fields.FreeEditor({ //eBook.Document.Fields.HtmlEditor({
            renderTo: el,
            // locationEl: el,
            //contentEl:el,
            //label: fProps.eBook.displayName,
            fullDocument: fProps.eBook.fullDocument,
            dataType: 'FREEEDIT',
            width: '100%',
            value: defval,
            name: fProps.eBook.name,
            parCmpID: this.id,
            properties: fProps
        });
        fld.render();
        //fld.setValue(defval);

        //fld.el = el;
        this.fields.add(fld.id, fld);

    }
    , renderBlock: function (el) {
        var elc = Ext.get(el);
        var name = elc.getAttributeNS('eBook', 'name');
        var blockTypes = elc.getAttributeNS('eBook', 'typeid');
        var viewt = elc.getAttributeNS('eBook', 'viewType');

        if (blockTypes != null && blockTypes != '') {
            blockTypes = blockTypes.split(",");
        } else {
            blockTypes = [];
        }

        if (!this.blockStores.containsKey(name)) {
            this.blockStores.add(name, new Ext.data.JsonStore({
                id: this.id + '-' + name,
                idProperty: 'Order',
                fields: eBook.data.RecordTypes.DocumentBlock,
                sortInfo: {
                    field: 'Order',
                    direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
                }
            }));
        }

        var bc = new eBook.Document.BlockContainer({
            applyTo: el,
            blockContainerId: name,
            viewType: viewt,
            documentID: this.documentID,
            documentTypeID: this.documentTypeID,
            blockTypes: blockTypes,
            store: this.blockStores.get(name)
        });
        /*var r = new bc.store.recordType({ Order: 1, Title: "Title test", Text: "<b>Body</b> test." }, 1);
        bc.store.add(r);
        bc.store.commitChanges();*/
        bc.render();
        this.blocks.add(bc);

    }

});
Ext.QuickTips.init();/*
eBook.Document.TestTemplate = {
    template: "<div class=\"ReportBody\" xmlns:eBook=\"eBook\"><span style=\"\">&#32;</span><br /><br /><table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;\"><tr><td style=\"text-align:left;font-size:12pt;\"><div class=\"eBook-p-tag\" style=\"font-size:12pt;\">CALA CAPITAL TEST<br />Rue Edith Cavell 128<br />1180 BRUXELLES <br /></div>&#32;\r\n    </td><td style=\"text-align:left;font-size:12pt;\"><div class=\"eBook-p-tag\" style=\"font-size:12pt;\"><input id=\"b7dfafdf-8d8f-b880-f8b0-d5729d1a20d3\" class=\"eBook-documents-field\" eBook:datatype=\"date\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:id=\"8d214632-9e88-fb5a-ddd9-879d8aec82ce\" eBook:parentid=\"f9f25fc6-2383-3961-6453-459e970f0ef7\" eBook:keyid=\"74e02eef-e380-497c-ae4c-c009fdb1ef5f\" eBook:displayName=\"Date de rapport de compilation\" eBook:value=\"\" value=\"\" eBook:store=\"metadata_file\" /></div>&#32;\r\n    </td></tr><tr><td style=\"text-align:left;font-size:13pt;font-weight:bold;\" colspan=\"2\"><br /><br /><div class=\"eBook-p-tag\" style=\"font-size:13pt;font-weight:bold;\">Management letter</div>&#32;\r\n    </td></tr></table><span style=\"\">&#32;</span><br /><br /><span style=\"\">&#32;</span><br /><br /><span style=\"\">&#32;</span><br /><br /><div class=\"eBook-p-tag\" style=\"\"> <input id=\"2434b40f-0b56-67a5-f737-957c6b433a26\" class=\"eBook-documents-field\" eBook:datatype=\"shorttext\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:displayName=\"Titre Management Letter\" eBook:id=\"25fe6ed8-f10c-7472-966b-01e69af2ffea\" eBook:parentid=\"00000000-0000-0000-0000-000000000000\" eBook:keyid=\"3b0b7dd5-4f51-48a0-90d0-6b062a65b3db\" eBook:value=\"Meneer Ward\" value=\"Meneer Ward\" eBook:store=\"metadata_client\" />, </div><br /><div class=\"eBook-documents-blocks\" name=\"ManagementBody\" eBook:typeid=\"BFCECF6C-6A24-4810-8DE7-12CAA5537F70,9230E9F7-58EE-463B-AB00-2DFE983685B9\" eBook:viewType=\"alinea\">\r\n       \r\n    </div><br /><div class=\"eBook-p-tag\" style=\"font-size:10pt;\">Veuillez agréer, <input id=\"39fd1032-b38b-08fe-1f5b-8024c390ab80\" class=\"eBook-documents-field\" eBook:datatype=\"shorttext\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:displayName=\"Titre Management Letter\" eBook:id=\"25fe6ed8-f10c-7472-966b-01e69af2ffea\" eBook:parentid=\"00000000-0000-0000-0000-000000000000\" eBook:keyid=\"3b0b7dd5-4f51-48a0-90d0-6b062a65b3db\" eBook:value=\"Meneer Ward\" value=\"Meneer Ward\" eBook:store=\"metadata_client\" />, mes salutations distinguées,</div><br /><br /><br /><div class=\"eBook-p-tag\" style=\"font-size:10pt;\"><input id=\"0e847b00-7b8d-d66f-9c18-6ded12db7fe2\" class=\"eBook-documents-field\" eBook:datatype=\"shorttext\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:displayName=\"Signature Management Letter\" eBook:id=\"895b6f36-6ae7-3112-1222-c1b0e8b2f131\" eBook:parentid=\"00000000-0000-0000-0000-000000000000\" eBook:keyid=\"568bcdc6-aba5-42b0-a69d-b8ef3b27b0d9\" eBook:value=\"Lena Plas\" value=\"Lena Plas\" eBook:store=\"metadata_client\" /><br /><input id=\"4eae079d-5c44-1fcb-9676-a31dd9bcc6c9\" class=\"eBook-documents-field\" eBook:datatype=\"shorttext\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:displayName=\"Fonction sign. Management Letter \" eBook:id=\"5ffb1951-f9f5-4370-8535-2c49aee2c160\" eBook:parentid=\"00000000-0000-0000-0000-000000000000\" eBook:keyid=\"564b3440-b60b-4d0e-9448-666484735b13\" eBook:value=\"Vennoot\" value=\"Vennoot\" eBook:store=\"metadata_client\" /></div></div>"
    , blockTypes: [
            { title: 'Block type 1', id: 'BFCECF6C-6A24-4810-8DE7-12CAA5537F70', items: [{ ti: 'Title 1-1', te: 'text 1-1' }, { ti: 'Title 1-2', te: 'text 1-2' }, { ti: 'Title 1-3', te: 'text 1-3'}] }
            , { title: 'Block type 2', id: '9230E9F7-58EE-463B-AB00-2DFE983685B9', items: [{ ti: 'Title 2-1', te: 'text 2-1' }, { ti: 'Title 2-2', te: 'text 2-2' }, { ti: 'Title 2-3', te: 'text 2-3'}] }
        ]
};*/

eBook.Document.Builder = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            items: [
                    new eBook.Document.Editor({ ref: 'editor' })
                , new Ext.Panel({ ref: 'blocksPanel', region: 'east', layout: 'accordion', items: [{ title: 'temp', html: 'body'}], width: 200 })]
            , layout: 'border'
            , bodyStyle: 'background-color:#ABABAB;'
        });
        eBook.Document.Builder.superclass.initComponent.apply(this, arguments);
    }
    , documentLoaded: false
    , clearDocument: function() {
        if (this.documentLoaded) {
            this.editor.clearAll();
            this.documentLoaded = false;
        }
    }
    , loadDocumentType: function(data) {
        //data.template
        //data.blockTypes
        this.editor.show();
        this.editor.loadTemplate(data.tmp);
        this.blocksPanel.removeAll(true);
        if (data.bts.length > 0) {
            this.blocksPanel.show();
            this.blocksPanel.expand();
            this.loadBlockTypes(data.bts);
        } else {
            this.blocksPanel.collapse();
            this.blocksPanel.hide();
        }
        data = null;

    }
    , loadDocument: function(data) {
        this.refOwner.setTitle(data.n);
        this.activeDocument = {};
        Ext.apply(this.activeDocument, data);
        this.editor.loadDocument(data);
        data = null;
        this.documentLoaded = true;
    }
    , getDocument: function() {
        if (!Ext.isDefined(this.activeDocument)) return {};
        this.activeDocument.bcd = this.editor.getBlockContainers();
        this.activeDocument.df = this.editor.getFields();
        return this.activeDocument;
    }
    , loadBlockTypes: function(blockTypes) {

        for (var i = 0; i < blockTypes.length; i++) {

            this.blocksPanel.add(new eBook.Document.BlocksPanel({
                title: blockTypes[i].n
                    , storeCfg: {
                        autoDestroy: true
                        , fields: eBook.data.RecordTypes.DocumentBlock
                        , data: blockTypes[i].bt
                    }
                    , blockTypeId: blockTypes[i].id
            }));
        }
        this.blocksPanel.doLayout();
        //this.doLayout();
    }
});
eBook.Document.GroupList = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        this.culture = eBook.Interface.Culture;
        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                        selectAction: 'GetDocumentGroups'
                        , serviceUrl: eBook.Service.document
                        , baseParams: {
                            Culture: eBook.Interface.Culture
                        }
                        , criteriaParameter: 'ccdc'
                        , autoDestroy: true
                        , fields: [
                                    { name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
                                    , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                                    ]
            })
            , minChars: 1
            , displayField: 'Name'
            , valueField: 'Id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: 200
        });
        eBook.Document.GroupList.superclass.initComponent.apply(this, arguments);
    }
    , setCulture: function(cult) {
        this.culture = cult;
        this.store.baseParams = {
            Culture: cult
        };
        this.store.load();
        this.setValue(eBook.EmptyGuid);
    }
});

Ext.reg('eBook.DocumentGroupList', eBook.Document.GroupList);

eBook.Document.TemplateChooser = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            tpl: new Ext.XTemplate(
                        '<tpl for=".">'
                            , '<div class="eBook-templatechooser-item" id="{Id}_{culture}">'
                                , '<div class="eBook-templatechooser-preview"><img src="images/documentTypes/{Id}.png" alt="{Name}"/></div>'
                                , '<div class="eBook-templatechooser-name">{Name}</div>'
            //, '<div class="eBook-templatehooser-description">{Name}</div>'
                        , '</div>',
                        '</tpl>')
            , itemSelector: '.eBook-templatechooser-item'
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetDocumentTypes'
                        , serviceUrl: eBook.Service.document
                        , criteriaParameter: 'cicdc'
                        , autoDestroy: true
                        , fields: eBook.data.RecordTypes.DocumentType
                        , autoLoad: true
                        , baseParams: { Id: eBook.EmptyGuid, Culture: eBook.Interface.Culture }
            })
            , singleSelect: true
            , overClass: 'eBook-templatechooser-over'
            , selectedClass: 'eBook-templatechooser-select'
            , autoScroll: true
            , cls: 'eBook-templatechooser'

        });
        this.groupId = eBook.EmptyGuid;
        this.culture = eBook.Interface.Culture;
        eBook.Document.TemplateChooser.superclass.initComponent.apply(this, arguments);
        this.on('selectionchange', this.onSelectionChanged, this);
    }
    , updateParams: function(culture, groupId) {
        this.culture = culture;
        this.groupId = groupId;
        this.store.baseParams = { Id: groupId, Culture: culture };
        this.store.load();
    }
    , prepareData: function(obj) {
        obj.culture = this.culture;
        return obj;
    }
    , getSelectedId: function() {
        var recs = this.getSelectedRecords();
        return recs[0].get('Id');
    }
    , standardTpl: ''
    , onSelectionChanged: function(dv, sels) {
        var t = this.refOwner.documentName;
        if (this.getSelectedRecords().length > 0) {
            t.setValue(this.getSelectedRecords()[0].get('Name'));
        }
        else {
            t.setValue("");
        }
    }
});

Ext.reg('eBook.TemplateChooser', eBook.Document.TemplateChooser);


eBook.Document.NewDocumentWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'form'
            , maximizable: false
            , title: eBook.Document.NewDocumentWindow_Title
            , resizable: false
            , height: 420
            , modal: true
            , bodyStyle: 'padding:10px;'
            , items: [
                {
                    xtype: 'textfield'
                    , ref: 'documentName'
                    , fieldLabel: eBook.Document.NewDocumentWindow_DocName
                    , allowBlank: false
                    , width: 250
                },
                {
                    xtype: 'languagebox'
                    , ref: 'culture'
                    , fieldLabel: eBook.Document.NewDocumentWindow_Language
                    , allowBlank: false
                    , value: eBook.Interface.Culture
                    , autoSelect: true
                    , listeners: {
                        'select': {
                            fn: this.changedTemplateCriteria
                            , scope: this
                        }
                    }
                }, {
                    xtype: 'eBook.DocumentGroupList'
                    , ref: 'docgroup'
                    , fieldLabel: eBook.Document.NewDocumentWindow_Group
                    , allowBlank: false
                    , autoSelect: true
                    , listeners: {
                        'select': {
                            fn: this.changedTemplateCriteria
                            , scope: this
                        }
                    }
                },
                {
                    xtype: 'label'
                    , text: eBook.Document.NewDocumentWindow_Template
                    , cls: 'x-form-item'
                }
                , {
                    xtype: 'eBook.TemplateChooser'
                    , ref: 'template'
                    , height: 200
                    , loadingText: eBook.Document.NewDocumentWindow_LoadingTemplates
                }
            ]
            , tbar: [{
                ref: 'createdocument',
                text: eBook.Document.NewDocumentWindow_CreateDoc,
                iconCls: 'eBook-icon-createdocument-24 ',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onCreateDocumentClick,
                scope: this
}]
            });
            eBook.Document.NewDocumentWindow.superclass.initComponent.apply(this, arguments);


        }
    , changedTemplateCriteria: function() {
        var cult, gid;
        cult = this.culture.getValue();
        if (cult != this.docgroup.culture) {
            this.docgroup.setCulture(cult);
        }
        gid = this.docgroup.getValue();
        if (Ext.isEmpty(cult)) cult = eBook.Interface.Culture;
        if (Ext.isEmpty(gid)) gid = eBook.EmptyGuid;
        this.template.updateParams(cult, gid);
    }
    , show: function(caller) {
        this.caller = caller;
        eBook.Document.NewDocumentWindow.superclass.show.call(this);
    }
    , onCreateDocumentClick: function() {
        var c = this.culture.getValue();
        var recs = this.template.getSelectedRecords();
        if (recs.length > 0) {
            if (Ext.isDefined(this.caller) && c != null) {
                this.caller.newDocument(recs[0].get('Id'), c, this.documentName.getValue());
                this.close();
            }
        }
        else {
            alert(eBook.Document.NewDocumentWindow_NoTemplateSelected);
        }
    }
    });

eBook.Document.OpenDocumentWindow = Ext.extend(eBook.Window, {
    initComponent:function() {
        Ext.apply(this, {
            height:200
            ,width:200
            ,items:[{
                    xtype:'listview'
                    ,store: new eBook.data.JsonStore({
                                selectAction: 'GetDocuments'
                                , serviceUrl: eBook.Service.document
                                        , criteriaParameter: 'cfdc'
                                        , autoDestroy: true
                                        , fields: eBook.data.RecordTypes.DocumentList
                                        , autoLoad: true
                                        , baseParams: { FileId: eBook.Interface.currentFile.get('Id') }
                            })
                    ,singleSelect:true
                    , emptyText: eBook.Document.OpenDocumentWindow_NoDocuments
                    ,reserveScrollOffset: true
                    ,columns: [{
                                    header: 'Document'
                                    ,width: 1
                                    ,dataIndex: 'Name'
                    }]
                    ,listeners: {
                        'click': {
                                fn:this.onClick
                                ,scope:this
                                }
                    }
              }]
        });
        eBook.Document.OpenDocumentWindow.superclass.initComponent.apply(this,arguments);
    }
    , show: function(caller) {
        this.caller = caller;
        eBook.Document.OpenDocumentWindow.superclass.show.call(this);
    }
    ,onClick:function(dv,idx,n,e) {
        if(Ext.isDefined(this.caller)) this.caller.loadDocument(dv.store.getAt(idx));
        this.close();
    }
});
/*
    eBook.Document.Window
    
    Document builder window. 
    Layout:card.
    
    card 0: choose document type and language (provide preview?)
    card 1: document builder
    
    Upon showing without attribute, show card 0. 
    When selection is made open card 1 for this selection
    
    
*/



eBook.Document.Window = Ext.extend(eBook.Window, {
    constructor: function(config) {
        this.addEvents({
            "documentTypeLoaded": true
            , "documentLoaded": true
        });
        eBook.Document.Window.superclass.constructor.call(this, config);
    }
    , initComponent: function() {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        this.renderMenu();
        Ext.apply(this, {
            items: [new eBook.Document.Builder({ ref: 'builder' })]
            , title: eBook.Document.Window_Title
            , layout: 'fit'
            , bodyStyle: 'background-color:#ABABAB;'
            , tbar: new Ext.Toolbar({
                items: [{
                    ref: 'newdocument',
                    text: eBook.Document.Window_NewDocument,
                    iconCls: 'eBook-icon-newdocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onNewDocumentClick,
                    disabled: fileClosed,
                    scope: this
                }, {
                    ref: 'opendocument',
                    text: eBook.Document.Window_OpenDocument,
                    iconCls: 'eBook-icon-opendocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    menu: this.loadMenu,
                    scope: this
                }, {
                    ref: 'savedocument',
                    text: eBook.Document.Window_SaveDocument,
                    iconCls: 'eBook-icon-savedocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    disabled: true,
                    handler: this.onSaveDocumentClick,
                    scope: this
                },
                {
                    ref: 'printpreview',
                    text: eBook.Document.Window_PrintPreview,
                    iconCls: 'eBook-icon-previewdocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    disabled: true,
                    handler: this.onPrintPreviewClick,
                    scope: this
                }
                ]
            })
        });
        eBook.Document.Window.superclass.initComponent.apply(this, arguments);

    }
    , printPreview: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.getEl().mask(eBook.Document.Window_Loading, 'x-mask-loading');

            var pars = { didc: {
                        __type: 'DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data'
                        , DocumentId: this.documentId
                        , DocumentTypeId: this.documentTypeId
                        , FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: this.documentCulture
                        , type: 'DOCUMENT'
                    }
            };
            eBook.CachedAjax.request({
                url: eBook.Service.output + 'GetDocumentPrint'
                , method: 'POST'
                , params: Ext.encode(pars)
                , callback: this.onPdfGenerated
                , scope: this
            });
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onPdfGenerated: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetDocumentPrintResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onNewDocumentClick: function() {
        //this.builder.loadDocumentType(eBook.Document.TestTemplate);
        if (eBook.Interface.currentFile.get('Closed')) return;
        var wn = new eBook.Document.NewDocumentWindow({});
        wn.show(this);
    }
    , onOpenDocumentClick: function() {
        //this.builder.loadDocumentType(eBook.Document.TestTemplate);
        var wn = new eBook.Document.OpenDocumentWindow({});
        wn.show(this);
    }
    , onSaveDocumentClick: function() {
        var doc = this.builder.getDocument();
        if (eBook.Interface.currentFile.get('Closed')) return;
        doc.Person = eBook.User.getActivePersonDataContract();
        var action = {
            text: String.format(eBook.Document.Window_Saving, doc.n)
            , params: {
                ddc: doc
            }
            , url: eBook.Service.document
            , action: 'SaveDocument'
            , lockWindow: true
        };
        this.addAction(action);
    }
    , onPrintPreviewClick: function() {
        this.getEl().mask('Saving...', 'x-mask-loading');
        var doc = this.builder.getDocument();
        if (eBook.Interface.currentFile.get('Closed')) return;
        doc.Person = eBook.User.getActivePersonDataContract();
        this.documentId = doc.id;
        eBook.CachedAjax.request({
            url: eBook.Service.document + 'SaveDocument'
                , method: 'POST'
                , params: Ext.encode(
                            {
                                ddc: doc
                            })
                , callback: this.printPreview
                , scope: this
        });
    }
    , show: function(documentX) {
        // Active document
        if (!Ext.isDefined(documentX)) this.builder.hide();
        eBook.Document.Window.superclass.show.call(this);
        //this.builder.loadDocumentType(eBook.Document.TestTemplate);
        if (documentX) this.loadDocument(documentX);
    }
    , loadDocument: function(rec) {
        // Load the documentType
        this.builder.clearDocument();
        this.on('documentTypeLoaded', function() { this.loadDocumentById(rec.get('Id'), rec.get('Culture')); }, this, { scope: this, single: true });
        this.loadDocumentTypeById(rec.get('DocumentTypeId'), rec.get('Culture'));
    }
    , loadDocumentById: function(id, culture) {
        this.builder.clearDocument();
        this.documentId = id;
        // this.documentCulture = culture;
        this.getEl().mask(eBook.Document.Window_Loading, 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.document + 'GetDocument'
                , method: 'POST'
                , params: Ext.encode({
                    cicdc: {
                        Id: id
                            , Culture: culture
                    }
                })
                , callback: this.documentLoaded
                , scope: this
        });
    }
    , newDocument: function(typeId, culture, name) {
        this.builder.clearDocument();
        if (eBook.Interface.currentFile.get('Closed')) return;
        var doc = {
            id: eBook.NewGuid()
            , fid: eBook.Interface.currentFile.get('Id')
            , n: name
            , dtid: typeId
            , c: culture
            , bcd: []
            , df: []
        };
        this.documentId = doc.id;
        this.on('documentTypeLoaded', function() { this.builder.loadDocument(doc); }, this, { scope: this, single: true });
        this.loadDocumentTypeById(typeId, culture);
    }
    , loadDocumentTypeById: function(typeId, culture) {
        //  this.layout.setActiveItem(1);
        this.documentTypeId = typeId;
        this.documentCulture = culture;
        this.getEl().mask(eBook.Document.Window_Loading, 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.document + 'GetDocumentType'
            , method: 'POST'
            , params: Ext.encode({
                cddc: {
                    FileId: eBook.Interface.currentFile.get('Id')
                        , DocumentTypeId: typeId
                        , Culture: culture
                }
            })
            , callback: this.documentTypeLoaded
            , scope: this
        });

    }
    , onBeforeClose: function() {
        this.builder.clearDocument();
        return true;
    }
    , documentTypeLoaded: function(opts, s, resp) {

        if (s) {
            var dt = Ext.decode(resp.responseText).GetDocumentTypeResult;
            this.builder.show();
            this.builder.loadDocumentType(dt);
            if (!eBook.Interface.currentFile.get('Closed')) {
                this.getTopToolbar().savedocument.enable();
                this.getTopToolbar().printpreview.enable();
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
        this.fireEvent('documentTypeLoaded');
    }
    , documentLoaded: function(opts, s, resp) {
        if (s) {
            var doc = Ext.decode(resp.responseText).GetDocumentResult;
            this.builder.loadDocument.defer(500, this.builder, [doc]);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }

        this.fireEvent('documentLoaded');
        this.getEl().unmask();
    }
    , onLoadDocumentClick: function(lv, rec) {

        // this.getEl().mask('Loading document ' + rec.get('Name'), 'x-mask-loading');
        this.loadMenu.hide(true);

        this.loadDocument(rec);
    }
    , renderMenu: function() {
        this.loadMenu = new eBook.Menu.ListViewMenu({
            handler: this.onLoadDocumentClick,
            reloadOnShow: true,
            hideOnClick: true,
            scope: this,
            listConfig: {
                loadingText: eBook.Document.Window_LoadingMenu,
                hideHeaders: true,
                store: new Ext.data.JsonStore({
                    autoDestroy: true,
                    root: 'GetDocumentsResult',
                    fields: eBook.data.RecordTypes.DocumentList,
                    baseParams: { FileId: eBook.Interface.currentFile.get('Id') },
                    proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                        url: eBook.Service.document + 'GetDocuments'
                             , criteriaParameter: 'cfdc'
                             , method: 'POST'

                    })
                })
            , width: 300
            , multiSelect: false
            , emptyText: eBook.Document.Window_MenuEmpty
            , reserveScrollOffset: false
                //, autoHeight: true
            , columns: [{
                header: 'name',
                width: 1,
                dataIndex: 'Name'
}]
            }
        });
    }
});

eBook.Excel.ColumnCombo = Ext.extend(Ext.form.ComboBox, {
    initComponent: function () {
        Ext.apply(this, {
            store: "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")
                , triggerAction: 'all'
                , typeAhead: false
                , selectOnFocus: true
                , autoSelect: true
            //, allowBlank: false
                , forceSelection: true
                , editable: false
            //, nullable: false
                , mode: 'local'
                , width: 50
                , listWidth: 50
        });
        eBook.Excel.ColumnCombo.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('eBook.Excel.ColumnCombo', eBook.Excel.ColumnCombo);


eBook.Excel.UploadWindow = Ext.extend(eBook.Window, {

    initComponent: function () {
        mappingPane = this.mappingPane;
        Ext.apply(this, {
            title: eBook.Excel.UploadWindow_Title
            , layout: 'fit'
            , width: 620
            , height: 200
            , modal: true
            , iconCls: 'eBook-Window-excel-ico'
            , items: [new Ext.FormPanel({
                fileUpload: true
                        , items: [{
                            xtype: 'fileuploadfield'
                                    , width: 400
                                    , fieldLabel: eBook.Excel.UploadWindow_SelectFile
                                    , fileTypeRegEx: /(\.xls(x{0,1}))$/i
                                    , ref: 'file'
                        }, {
                            xtype: 'hidden'
                                        , name: 'fileType'
                                        , value: 'excel'
                        }]
                        , ref: 'excelForm'
            })]
            , tbar: [{
                text: eBook.Excel.UploadWindow_Upload,
                ref: '../saveButton',
                iconCls: 'eBook-icon-uploadexcel-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
            }]
        });

        eBook.Excel.UploadWindow.superclass.initComponent.apply(this, arguments);
    }
    , onSaveClick: function (e) {
        var f = this.excelForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Excel.UploadWindow_Invalid);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Excel.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function (fp, o) {
        //process o.result.file
        this.uploaded = true;
        this.newFileName = o.result.file;
        this.originalFileName = o.result.originalFile;
        this.getEl().mask(String.format(eBook.Excel.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.excel + 'ExcelGetBasicInfo'
                , method: 'POST'
                , params: Ext.encode({ cedc: { FileName: o.result.file} })
                , callback: this.handleFileProcessing
                , scope: this
        });
    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, this.title);
    }
    , handleFileProcessing: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var dc = Ext.decode(resp.responseText).ExcelGetBasicInfoResult;
            dc.originalFileName = this.originalFileName;
            if (this.parentCaller && Ext.isFunction(this.parentCaller.setExcelInfo)) {
                this.parentCaller.setExcelInfo(dc);
            } else {
                var wn = new eBook.Excel.Window({
                    title: dc.originalFileName,
                    fileName: dc.fn,
                    sheets: dc.sns,
                    mappingPane: this.mappingPane
                });
                wn.show(this.parentCaller);
            }
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , show: function (caller) {
        this.parentCaller = caller;
        eBook.Excel.UploadWindow.superclass.show.call(this);
    }
});
//eBook.Excel.ImportTypes = [['as', 'Accountschema'], ['bo', 'Booking'], ['ba', 'Balans + accountschema'], ['c', 'Customer'], ['s', 'Supplier']]



eBook.Excel.MappingPanel = Ext.extend(Ext.FormPanel, {
    constructor: function (config) {
        this.addEvents('sheetchange');
        config.items = [{
            xtype: 'combo'
                            , ref: 'sheet'
                            , name: 'sheet'
                            , fieldLabel: eBook.Excel.MappingPanel_Sheet //'Excel werkblad'
                            , triggerAction: 'all'
                            , typeAhead: false
                            , selectOnFocus: true
                            , autoSelect: true
                            , allowBlank: false
                            , forceSelection: true
                            //, editable: false
                            , nullable: false
                            , mode: 'local'
                            , itemCls: 'excel-column-field'
                            , displayField: 'field1'
                            , valueField: 'field1'
                            , store: {
                                xtype: 'simplestore',
                                fields: ['field1'],
                                data: config.sheets ? config.sheets : [''],
                                expandData: true,
                                autoDestroy: true,
                                autoCreated: true

                            }
                            , listeners: {
                                'select': {
                                    fn: this.onSheetSelect
                                        , scope: this
                                }
                            }
        }];

        config.items = config.items.concat(config.columnMappings);

        eBook.Excel.MappingPanel.superclass.constructor.call(this, config);
    },
    initComponent: function () {
        Ext.apply(this, {
            labelWidth: 150
            , labelAlign: 'top'
            , bodyStyle: 'padding:5px;'
        });
        eBook.Excel.MappingPanel.superclass.initComponent.apply(this, arguments);
    }
    , onSheetSelect: function () {
        this.fireEvent('sheetchange', this.sheet.getValue());
    }
    , getMapping: function () {
        var mappings = [];
        this.getForm().items.each(function (fld) {
            if (fld.isMappingField) {
                mappings.push({ name: fld.name, column: fld.getValue() });
            }
        }, this);
        return mappings;
    }
    , loadFile: function (fileName, sheets) {
        this.fileName = fileName;
        this.sheet.store.removeAll();
        this.sheet.store.loadData(sheets, false);

        //this.sheet.expand();
        //this.sheet.select(0);
    }
});

Ext.reg('eBook.Excel.Mapping', eBook.Excel.MappingPanel);



eBook.Excel.GridView = Ext.extend(Ext.grid.GridView, {
    processRows: function(startRow, skipStripe) {
        if (!this.ds || this.ds.getCount() < 1) {
            return;
        }

        var rows = this.getRows(),
            length = rows.length,
            row, i;

        skipStripe = skipStripe || !this.grid.stripeRows;
        startRow = startRow || 0;

        for (i = 0; i < length; i++) {
            row = rows[i];
            if (row) {
                row.rowIndex = i;
                if (!skipStripe) {
                    row.className = row.className.replace(this.rowClsRe, ' ');
                    if ((i + 1) % 2 === 0) {
                        row.className += ' x-grid3-row-alt';
                    }
                }
                if (this.grid.excludedRow.call(this.grid,i)) {
                    row.className += ' eBook-Excel-excluderow'
                }
            }
        }

        // add first/last-row classes
        if (startRow === 0) {
            Ext.fly(rows[0]).addClass(this.firstRowCls);
        }

        Ext.fly(rows[length - 1]).addClass(this.lastRowCls);
    }
});


eBook.Excel.Grid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        this.excelStore = new eBook.data.PagedJsonStore({
            selectAction: 'ExcelGetSheetData',
            serviceUrl: eBook.Service.excel,
            fields: this.getExcelMappings(),
            idField: 'Id',
            baseParams: {
                FileName: this.fileName
                , SheetName: this.sheetName
            },
            criteriaParameter: 'cesdc'
        });
        // TODO:
        this.previewStore = new eBook.data.PagedJsonStore({
            selectAction: this.previewCfg.action,
            serviceUrl: eBook.Service.excel,
            fields: eBook.data.RecordTypes.ExcelPreview,
            baseParams: {
                FileName: this.fileName
                , SheetName: this.sheetName
                , Mapping: null
                , ImportType: this.importType
            },
            criteriaParameter: 'cesmdc'
        });

        Ext.apply(this, {
            columnLines: true
            , deferRowRender: true
            //, view: new eBook.Excel.GridView({})
            , columns: this.getColumns()
            , store: this.excelStore
            , loadMask: true
            , bbar: new Ext.PagingToolbar({
                displayInfo: true,
                pageSize: 40,
                prependButtons: true,
                store: this.excelStore,
                ref: 'pgToolbar'
            })
            , tbar: [{
                ref: '../preview',
                text: eBook.Excel.Grid_Preview,
                enableToggle: true,
                iconCls: 'eBook-magnify-ico-16',
                scale: 'small',
                iconAlign: 'left',
                toggleHandler: this.onTogglePreview,
                scope: this
}]
            });
            eBook.Excel.Grid.superclass.initComponent.apply(this, arguments);
        }
    , getExcelMappings: function() {
        var maps = [];
        var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (var i = 0; i < str.length; i++) {
            var ch = str.charAt(i);
            maps.push({ name: ch, mapping: ch, type: Ext.data.Types.STRING });
        }
        return maps;
    }
    , getColumns: function() {
        var cols = [new Ext.grid.RowNumberer()];
        // loop through alfabet
        var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (var i = 0; i < str.length; i++) {
            var ch = str.charAt(i);
            cols.push({
                header: ch,
                width: 50,
                dataIndex: ch,
                align: 'left'
            });
        }
        return cols;
    }
    , getColumnModel: function() {
        return new Ext.grid.ColumnModel(this.getColumns());
    }
    , loadFile: function(fileName, sheets) {
        this.fileName = fileName;
        this.sheets = sheets;
    }
    , loadSheet: function(sheetName) {
        this.sheetName = sheetName;
        var bp = {
            FileName: this.fileName
               , SheetName: this.sheetName
        };
        if (this.preview.pressed) {
            Ext.apply(bp, this.refOwner.getDataContract());
        }
        bp.FileName = this.fileName;
        this.store.baseParams = bp;
        this.store.load({
            params: {
                Start: 0
                , Limit: 40
            }
        });
    }
    , onTogglePreview: function(btn, pressed) {
        if (this.sheetName == null || this.sheetName == '') {
            btn.toggle(false, true);
            Ext.Msg.alert(eBook.Excel.Grid_NoWorksheetTitle, eBook.Excel.Grid_NoWorksheetMsh);
            return;
        }
        if (pressed) {
            this.reconfigure(this.previewStore, this.getPreviewColumnModel());
            this.getBottomToolbar().bindStore(this.previewStore);
        } else {
            this.reconfigure(this.excelStore, this.getColumnModel());
            this.getBottomToolbar().bindStore(this.excelStore);
        }

        this.loadSheet(this.sheetName);
    }
    , getPreviewColumnModel: function() {

        return new Ext.grid.ColumnModel(this.previewCfg.columns);
    }
    });

Ext.reg('eBook.Excel.Grid', eBook.Excel.Grid);
eBook.Excel.MainPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , items: [{ xtype: 'eBook.Excel.Mapping', ref: 'mapping', region: 'north', height: 150,collapsible:true }, { xtype: 'eBook.Excel.Grid', ref: 'grid', region: 'center'}]
        });
        eBook.Excel.MainPanel.superclass.initComponent.apply(this, arguments);
    }
    , loadExcel: function(excelFile) {
        this.refOwner.getEl().mask('Loading data', 'x-mask-loading');
        this.fileName = excelFile.fn;
        this.refOwner.setTitle(this.fileName);
        this.mapping.loadExcel(excelFile);
        this.refOwner.getEl().unmask();
    }
    , loadSheet: function(sheet) {
        this.grid.loadSheet(this.fileName, sheet);
    }
});

Ext.reg('eBook.Excel.Main', eBook.Excel.MainPanel);


eBook.Excel.CheckTabPanel = Ext.extend(eBook.TabPanel.CheckTabPanel, {
    areEnabledValid: function() {
        var aev = true;
        for (var i = 0; i < this.items.items.length; i++) {
            var pnl = this.items.items[i];
            if (!pnl.disabled) {
                var b = pnl.isValidt();
                aev = aev && b;
            }
        }
        return aev;
    }
    , loadFile: function(fileName, sheets) {
        for (var i = 0; i < this.items.items.length; i++) {
            var pnl = this.items.items[i];
            pnl.loadFile(fileName, sheets);
        }
    }
    , getDataContracts: function() {
        var dcs = {};
        var cnt = 0;
        for (var i = 0; i < this.items.items.length; i++) {
            var pnl = this.items.items[i];
            if (!pnl.disabled) {
                dcs[pnl.initialConfig.cfgName] = pnl.getDataContract();
                cnt++;
            }
        }
        dcs.length = cnt;
        return dcs;
    }
});
Ext.reg('eBook.Excel.CheckTabPanel', eBook.Excel.CheckTabPanel);

eBook.Excel.TabPanel = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        var cfg = {
            layout: 'border'
            , fileName: config.fileName
            , title: config.title
            , disabled: true
            , importType: config.importType
            , cfgName: config.cfgName
            , items: [{ xtype: 'eBook.Excel.Mapping'
                     , columnMappings: config.columnMappings
                     , sheets: config.sheets
                     , fileName: config.fileName
                     , region: 'north'
                     , split: true
                     , ref: 'mappingPanel'
                     , height: config.mappingHeight ? config.mappingHeight : 150
                     , listeners: {
                         'sheetchange': {
                             fn: this.onSheetChange
                                , scope: this
                         }
                     }
            }, {
                xtype: 'eBook.Excel.Grid'
                        , ref: 'grid'
                        , fileName: config.fileName
                        , region: 'center'
                        , previewCfg: config.previewCfg
}]
            };
            eBook.Excel.TabPanel.superclass.constructor.call(this, cfg);
        }

    , loadFile: function(fileName, sheets) {
        this.fileName = fileName;
        this.mappingPanel.loadFile(fileName, sheets);
        this.grid.loadFile(fileName, sheets);
    }
    , importType: null
    , getMapping: function() {
        return this.mappingPanel.getMapping();
    }
    , onSheetChange: function(sheet) {
        this.activeSheet = sheet;
        this.grid.loadSheet(sheet);
    }
        /*,getPreviewColumns:function() {
        var mappings = this.getMapping();
        var cols = [];
        cols = cols.concat(this.previewColumns);
        for(var i=0;i<cols.length;i++) {
        cols[i].dataIndex = mappings[cols[i].mappingItem];
        }
        return cols;
        }*/
    , isDirty: function() {
        return this.mappingPanel.getForm().isDirty();
    }
    , isValidt: function() {
        var vld = this.mappingPanel.getForm().isValid();
        if (!vld) {
            this.markTabInvalid();
        } else {
            this.clearTabInvalid();
        }
        return vld;
    }
     , markTabInvalid: function() {
         Ext.get(this.tabEl).addClass('eBook-tab-invalid');
     }
    , clearTabInvalid: function() {
        Ext.get(this.tabEl).removeClass('eBook-tab-invalid');
    }
    , getDataContract: function() {
        return {
            ImportType: this.initialConfig.importType
            , FileName: this.fileName
            , SheetName: this.activeSheet
            , Culture: eBook.Interface.Culture
            , Mapping: this.getMapping()
            , txt: this.activeSheet
            , active:!this.disabled
        };
    }
    , checkValid: function() {
        if (this.disabled) return true;
        if (!this.disabled) {
            //var dc = this.getDataContract();
            //if (dc.Mapping.length == this.columnMappings.length) {
            //    return true;
            //}
            return this.isValidt();
        }
        return false;
    }
    });



    eBook.Excel.AccountSchemaPanel = Ext.extend(eBook.Excel.TabPanel, {
        constructor: function(config) {
            Ext.apply(config, {
                mappingHeight: 55
            , importType: 'as'
            , cfgName: config.cfgName
            , columnMappings: [{
                xtype: 'eBook.Excel.ColumnCombo'
                                , name: 'accountnr'
                                , ref: 'accountnr'
                                , fieldLabel: eBook.Excel.AccountSchemaPanel_AccountNr
                                , itemCls: 'excel-column-field'
                                , nullable: false
                                , allowBlank: false
                                , isMappingField: true
                                , listeners: {
                                    'change': { fn: this.onChange, scope: this }
                                }
            }, {
                xtype: 'eBook.Excel.ColumnCombo'
                                , name: 'accountdescription'
                                , ref: 'accountdescription'
                                , fieldLabel: eBook.Excel.AccountSchemaPanel_AccountDesc
                                , itemCls: 'excel-column-field'
                                , nullable: false
                                , allowBlank: false
                                , isMappingField: true
                                , listeners: {
                                    'change': { fn: this.onChange, scope: this }
                                }
            }, {
                xtype: 'languagebox'
                                , name: 'culture'
                                , ref: 'culture'
                                , fieldLabel: eBook.Excel.AccountSchemaPanel_Language
                                , itemCls: 'excel-column-field'
                                , nullable: false
                                , allowBlank: false
                                , value: eBook.Interface.Culture
                                , listeners: {
                                    'change': { fn: this.onChange, scope: this }
                                }
}]
            , previewCfg: {
                columns: [{ header: eBook.Excel.AccountSchemaPanel_AccountNr, dataIndex: 'accountnr', mappingItem: 'accountnr' }
                              , { header: eBook.Excel.AccountSchemaPanel_AccountDesc, dataIndex: 'accountdescription', mappingItem: 'accountdescription', width: 400}]
                , action: 'ExcelGetAccountSchema'
            }
            });
            eBook.Excel.AccountSchemaPanel.superclass.constructor.apply(this, arguments);
        }
    , getDataContract: function() {
        var dc = eBook.Excel.AccountSchemaPanel.superclass.getDataContract.call(this);
        dc.Culture = this.mappingPanel.getForm().findField('culture').getValue();
        dc.ImportType = 'as';
        return dc;
    }
    
    , onChange: function() { }
    });

Ext.reg('eBook.Excel.AccountSchemaPanel', eBook.Excel.AccountSchemaPanel);


eBook.Excel.BalansPanel = Ext.extend(eBook.Excel.TabPanel, {
    constructor: function(config) {
        Ext.apply(config, {
            mappingHeight: 55
            , importType: config.importType
            , cfgName: config.cfgName
            , columnMappings: [{
                xtype: 'eBook.Excel.ColumnCombo'
                , name: 'accountnr'
                , ref: 'accountnr'
                , fieldLabel: eBook.Excel.AccountSchemaPanel_AccountNr
                , itemCls: 'excel-column-field'
                , nullable: false
                , allowBlank: false
                , isMappingField: true
                , listeners: {
                    'change': { fn: this.onChange, scope: this }
                }
            }, {
                xtype: 'eBook.Excel.ColumnCombo'
                , name: 'accountdescription'
                , ref: 'accountdescription'
                , fieldLabel: eBook.Excel.AccountSchemaPanel_AccountDesc
                , itemCls: 'excel-column-field'
                , nullable: false
                , allowBlank: false
                , isMappingField: true
                , listeners: {
                    'change': { fn: this.onChange, scope: this }
                }
            }, {
                xtype: 'languagebox'
                , name: 'culture'
                , fieldLabel: eBook.Excel.AccountSchemaPanel_Language
                , itemCls: 'excel-column-field'
                , nullable: false
                , allowBlank: false
                , value: eBook.Interface.Culture
                , listeners: {
                    'change': { fn: this.onChange, scope: this }
                }
            },{
                xtype: 'eBook.Excel.ColumnCombo'
                , name: 'saldo'
                , ref: 'saldo'
                , fieldLabel: eBook.Excel.BalansPanel_Saldo
                , itemCls: 'excel-column-field'
                , nullable: false
                , allowBlank: false
                , isMappingField: true
                , listeners: {
                    'change': { fn: this.onChange, scope: this }
                }
            }]
            , previewCfg: {
            columns: [{ header: eBook.Excel.AccountSchemaPanel_AccountNr, dataIndex: 'accountnr', mappingItem: 'accountnr' }
                              , { header: eBook.Excel.AccountSchemaPanel_AccountDesc, dataIndex: 'accountdescription', mappingItem: 'accountdescription', width: 200 }
                              , { header: eBook.Excel.BalansPanel_Saldo, dataIndex: 'saldo', mappingItem: 'saldo',align:'right'}]
                , action: 'ExcelGetBalans'
            }
            });
            eBook.Excel.BalansPanel.superclass.constructor.apply(this, arguments);
        }
    , getDataContract: function() {
        var dc = eBook.Excel.BalansPanel.superclass.getDataContract.call(this);
        dc.Culture = this.mappingPanel.getForm().findField('culture').getValue();
        dc.ImportType = this.importType;
        return dc;
    }
    , onChange: function() { }
    });

    Ext.reg('eBook.Excel.BalansPanel', eBook.Excel.BalansPanel);
eBook.Excel.Configurator = Ext.extend(eBook.Excel.CheckTabPanel, {
    initComponent: function() {
        Ext.apply(this, {
            items: [{ xtype: 'eBook.Excel.AccountSchemaPanel', title: eBook.Excel.Window_AccountSchema
                        , ref: 'accounts', sheets: this.sheets, fileName: this.fileName, cfgName: 'accountsCfg'
            }
                  , { xtype: 'eBook.Excel.BalansPanel', title: eBook.Excel.Window_CurrentBalans
                        , ref: 'current', importType: 'hb', sheets: this.sheets, fileName: this.fileName, cfgName: 'currentCfg'
                  }
                  , { xtype: 'eBook.Excel.BalansPanel', title: eBook.Excel.Window_PreviousBalans
                        , ref: 'previous', importType: 'vb', sheets: this.sheets, fileName: this.fileName, cfgName: 'previousCfg'
                  }
            ]
            , activeTab: 0
        });
        eBook.Excel.Configurator.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('eBook.Excel.Configurator', eBook.Excel.Configurator); 
// select import method. 
// (accountshema,accountschema+ebook start journal,end balans previous, suppliers, customers
// upload file
// select columns
// -->

eBook.Excel.Window = Ext.extend(eBook.Window, {
    excelDC: null
    , fileId: ''
    , initComponent: function () {
        mappingPane = this.mappingPane;
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , modal: true
            //, title: this.fileName
            , items: [
                    { ref: 'main', xtype: 'eBook.Excel.Configurator', sheets: this.sheets, fileName: this.fileName
                    },
                    { xtype: 'eBook-createFile-actions'
                        , ref: 'create'
                        , title: eBook.Excel.Window_Importing
                    }
                   ]
            , tbar: {
                xtype: 'toolbar',
                items: [{
                    xtype: 'buttongroup',
                    ref: 'generalActions',
                    id: this.id + '-toolbar-general',
                    columns: 4,
                    title: eBook.Excel.Window_Import,
                    items: [{
                        ref: '../import',
                        text: eBook.Excel.Window_ImportData,
                        iconCls: 'eBook-icon-importexcel-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onImportClick,
                        scope: this
                    }]
                }]
            }
        });
        eBook.Excel.Window.superclass.initComponent.apply(this, arguments);
    }
    , onImportClick: function () {
        if (eBook.Interface.currentFile.get('Closed')) return;
        if (this.main.hasEnabledTabs()) {
            if (!this.main.areEnabledValid()) {
                Ext.Msg.alert(eBook.Excel.Window_TabFailedTitle, eBook.Excel.Window_TabFailedMsg);
            } else {
                this.performImports(this.main.getDataContracts());
            }
            //var datacontracts
            //this.main.items.each
        } else {
            Ext.Msg.alert(eBook.Excel.Window_NoDataTitle, eBook.Excel.Window_NoDataMsg);
        }
        //this.getLayout().setActiveItem(1);
        // this.main.grid.preview();
    }
    , performImports: function (dcs) {
        //this.im
        if (eBook.Interface.currentFile.get('Closed')) return;
        var acts = {
            actions: []
            , revertActions: []
            , postActions: []
        };
        var fileID = eBook.Interface.currentFile.get('Id');

        for (var i = 0; i < this.main.items.getCount(); i++) {
            var it = this.main.items.get(i);
            var dc = it.getDataContract();
            if (dc.active) {
                var act = { url: eBook.Service.file, response: null, status: 'ready', revertByFailure: false, params: null, action: '', text: '' };
                dc.FileId = fileID;
                act.params = { cesmdc: dc };
                switch (dc.ImportType) {
                    case 'as':
                        act.action = 'ImportAccountsExcel';
                        act.text = eBook.Excel.Window_ImportAS;  //'Importeren van het rekeningschema';
                        break;
                    case 'hb':
                        act.action = 'ImportCurrentStateExcel';
                        act.text = eBook.Excel.Window_ImportHB;  //'Importeren van de balans van het huidig boekjaar';
                        break;
                    case 'vb':
                        act.action = 'ImportPreviousStateExcel';
                        act.text = eBook.Excel.Window_ImportVB;  //'Importeren van de balans van het vorig boekjaar';
                        break;
                }

                acts.actions.push(act);
            }
        }

        acts.actions.push({
            state: 'ready'
                , response: null
                , text: 'Update cache'
                , url: eBook.Service.file
                , action: 'ForceFileCacheReload'
                , params: { cfdc: { FileId: fileID, Culture: eBook.Interface.Culture} }
        });

        this.getTopToolbar().disable();
        this.layout.setActiveItem(this.create);
        this.create.on('actionsDone', this.onImportDone, this);
        this.create.setActions(acts);
        this.create.start.defer(500, this.create);
    }
    , onImportDone: function () {
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + 'ForceReCalculate'
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
                , callback: this.worksheetsUpdated
                , started: new Date().getTime()
                , scope: this
        });
    }
    , worksheetsUpdated: function () {
        if (this.callingWindow && Ext.isFunction(this.callingWindow.importDone)) this.callingWindow.importDone();
        Ext.Msg.alert(eBook.Excel.Window_ImportDoneTitle, eBook.Excel.Window_ImportDoneMsg);
        this.mappingPane.doRefresh();
        this.close.defer(500, this);
    }
    , show: function (caller) {
        this.callingWindow = caller;
        //if (Ext.isDefined(this.fileName)) {
        if (!eBook.Interface.currentFile.get('Closed')) eBook.Excel.Window.superclass.show.call(this);
        //this.getLayout().setActiveItem(1);
        //this.main.loadExcel(excelFile)
        //}
        if (eBook.Interface.currentFile.get('Closed')) this.destroy();
    }
});

Ext.reg('eBook.Excel.Window', eBook.Excel.Window);


eBook.Exact.UploadWindow = Ext.extend(eBook.Window, {

    initComponent: function () {
        mappingPane = this.mappingPane;
        Ext.apply(this, {
            title: eBook.Excel.UploadWindow_Title
            , layout: 'fit'
            , width: 620
            , height: 200
            , modal: true
            , iconCls: 'eBook-uploadbiz-ico-24'
            , items: [new Ext.FormPanel({
                fileUpload: true
                        , items: [{
                            xtype: 'fileuploadfield'
                                    , width: 400
                                    , fieldLabel: eBook.Exact.UploadWindow_SelectFile
                                    , fileTypeRegEx: /(\.txt())$/i
                                    , ref: 'file'
                        }, {
                            xtype: 'hidden'
                                        , name: 'fileType'
                                        , value: 'txt'
                        }]
                        , ref: 'txtForm'
            })]
            , tbar: [{
                text: eBook.Excel.UploadWindow_Upload,
                ref: '../saveButton',
                iconCls: 'eBook-uploadbiz-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
            }]
        });

        eBook.Exact.UploadWindow.superclass.initComponent.apply(this, arguments);
    }
    , onSaveClick: function (e) {
        var f = this.txtForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Exact.UploadWindow_Invalid);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Excel.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function (fp, o) {
        //process o.result.file
        this.uploaded = true;
        this.newFileName = o.result.file;
        this.originalFileName = o.result.originalFile;
        this.getEl().mask('Loading descriptions');
        Ext.Ajax.request({
            url: eBook.Service.file + 'ImportAccountsTxt'
                , method: 'POST'
                , params: Ext.encode({ cesmdc: { FileName: this.newFileName, FileId: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.currentFile.get('Culture')} })
                , callback: this.handleFileProcessingTxt
                , scope: this
        });

    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, this.title);
    }
    , handleFileProcessingTxt: function (opts, success, resp) {
        this.getEl().unmask();
        this.getEl().mask('Loading numbers');
        if (success) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'ImportCurrentStateTxt'
                , method: 'POST'
                , params: Ext.encode({ cesmdc: { FileName: this.newFileName, FileId: eBook.Interface.currentFile.get('Id')} })
                , callback: this.handleFileProcessingAccountTxt
                , scope: this
            });

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , handleFileProcessingAccountTxt: function (opts, success, resp) {
        this.getEl().unmask();
        this.getEl().mask('Set file in cache');
        if (success) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'SetFileInCache'
                , method: 'POST'
                , params: { cfdc: { FileId: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.Culture} }
                , callback: this.SetFileInCacheDone
                , scope: this
            });

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , SetFileInCacheDone: function (opts, success, resp) {
        this.mappingPane.doRefresh();
        this.getEl().unmask();
        this.getEl().mask('Recalculate worksheets');
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + 'ForceReCalculate'
            , method: 'POST'
            , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
            , callback: this.worksheetsUpdated
            , started: new Date().getTime()
            , scope: this
        });
        this.close();
    }
    , worksheetsUpdated: function (opts, success, resp) {
        this.getEl().unmask();
        
    }
    , show: function (caller) {
        this.parentCaller = caller;
        eBook.Exact.UploadWindow.superclass.show.call(this);
    }
});

eBook.ProAcc.ActionDataView = Ext.extend(Ext.DataView, {
    constructor: function(config) {
        this.addEvents('actionsDone');
        eBook.ProAcc.ActionDataView.superclass.constructor.call(this, config);
    }
    , data: {}
    , title: ''
    , initComponent: function() {
        Ext.apply(this, {
            border: false
            , style: 'padding:10px;background-color:#FFFFFF;'
            , store: new Ext.data.JsonStore({
                autoDestroy: true,
                root: 'actions',
                idProperty: 'id',
                fields: eBook.data.RecordTypes.CreateFileActions
            })
            , itemSelector: 'div.eBook-createFile-action'
            , tpl: new Ext.XTemplate(
                        '<div class="eBook-createFile-action-title">' + this.title + '</div><tpl for=".">',
                        '<div class="eBook-createFile-action eBook-createFile-action-status-{status}" id="{id}">{text}<tpl if="status==\'failure\'"><div class="eBook-createFile-action-msg">{errorMsg}</div></tpl></div>',
                        '</tpl>')
        });
        eBook.ProAcc.ActionDataView.superclass.initComponent.apply(this, arguments);
    }
    , setActions: function(actObj) {
        this.actionsContainer = actObj;
        if (this.actionsContainer.revertActions == null) this.actionsContainer.revertActions = [];
        if (this.actionsContainer.postActions == null) this.actionsContainer.postActions = [];
        for (var i = 0; i < this.actionsContainer.actions.length; i++) {
            this.actionsContainer.actions[i].id = Ext.id();
            this.actionsContainer.actions[i].status = 'ready';
            this.actionsContainer.actions[i].container = 'actions';
        }
        for (var i = 0; i < this.actionsContainer.revertActions.length; i++) {
            this.actionsContainer.revertActions[i].id = Ext.id();
            this.actionsContainer.revertActions[i].status = 'ready';
            this.actionsContainer.revertActions[i].container = 'revertActions';
        }
        for (var i = 0; i < this.actionsContainer.postActions.length; i++) {
            this.actionsContainer.postActions[i].id = Ext.id();
            this.actionsContainer.postActions[i].status = 'ready';
            this.actionsContainer.postActions[i].container = 'postActions';
        }

        this.store.loadData({ actions: this.actionsContainer.actions });
    }
    , start: function() {
        this.data = {};
        this.findParentByType(eBook.Window).locked = true;
        this.actionIndex = -1;
        this.performAction();
    }
    , getActionObject: function(rec) {
        var cnt = this.actionsContainer[rec.get('container')];

        for (var i = 0; i < cnt.length; i++) {
            if (cnt[i].id == rec.get('id')) return cnt[i];
        }
        return null;
    }
    , postAdded: false
    , success: true
    , performAction: function() {
        this.actionIndex++;
        if (this.store.getCount() > this.actionIndex) {
            var rec = this.store.getAt(this.actionIndex);
            var act = this.getActionObject(rec);
            Ext.get(act.id).scrollIntoView(this.findParentByType(eBook.Window).body, false);
            rec.set('status', 'busy');
            rec.commit();
            var params = {};

            if (Ext.isFunction(act.params)) {
                params = act.params.call(this, this);
            } else {
                params = act.params;
            }

            Ext.Ajax.request({
                url: (act.url ? act.url : eBook.Service.url) + act.action
                , method: 'POST'
                , params: Ext.encode(params)
                , success: function(resp, opts) { this.onActionSuccess(resp, opts, act); }
                , failure: function(resp, opts) { this.onActionFailure(resp, opts, act); }
                , scope: this
            });

        } else {
            //this.isBusy = false;
            //this.setStatus('Ready');
            if (!this.postAdded && this.success && this.actionsContainer.postActions.length > 0) {
                //this.store.on('load', this.performAction, this, { single: true });
                this.store.loadData({ actions: this.actionsContainer.postActions }, true);
                this.store.commitChanges();
                this.postAdded = true;
                this.actionIndex--;
                this.performAction.defer(100, this);
            } else {

                this.findParentByType(eBook.Window).locked = false;
                this.fireEvent('actionsDone', true, this.actionsContainer, this.data); //success, actions
            }
        }
    }
    , onActionSuccess: function(resp, opts, act) {

        var rec = this.store.getById(act.id);
        rec.set('status', 'success');
        rec.commit();
        this.getActionObject(rec).status = 'success';
        if (act.response != null && Ext.isFunction(act.response)) {
            var respObj = Ext.decode(resp.responseText);
            act.response.call(this, this, respObj);
        }
        if (act.callback != null && Ext.isFunction(act.callback)) {
            act.callback.call(act.scope || this, this, resp);
        }
        this.performAction.defer(100, this);
    }
    , onActionFailure: function(resp, opts, act) {
        var rec = this.store.getById(act.id);
        rec.set('status', 'failure');
        rec.set('errorMsg', 'Failed to execute action');
        rec.commit();

        this.getActionObject(rec).status = 'failure';
        if (act.revertByFailure) {
            var dels = [];
            this.store.each(function(rec) {
                if (rec.get('status') == 'ready') dels.push(rec);
            }, this);
            if (dels.length > 0) this.store.remove(dels);
            this.store.commitChanges();
            if (this.actionsContainer.revertActions.length > 0) {
                this.store.loadData({ actions: this.actionsContainer.revertActions }, true);
            }
            this.performAction.defer(100, this);
        } else {

            if (act.ignoreFailure) {
                this.performAction.defer(100, this);
            } else {
                this.findParentByType(eBook.Window).locked = false;
                this.fireEvent('actionsDone', false, this.actionsContainer);
            }
        }

        // temp:

    }

});

Ext.reg('eBook-createFile-actions', eBook.ProAcc.ActionDataView);

eBook.ProAcc.Window = Ext.extend(eBook.Window, {
    clientOnly: false
    , includePreviousYear: true
    , initComponent: function() {
        Ext.apply(this, {
            iconCls: ''
            , title: eBook.ProAcc.Window_Title
            , layout: 'card'
            , activeItem: 0
            , modal: true
            , items: [{
                xtype: 'panel'
                        , ref: 'frm'
                        , bodyStyle: 'padding:10px;'
                        , items: [{
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportAccountSchema
                                    , ref: 'accountschema'
                                    , name: 'accountschema'
                                    , hidden: this.clientOnly
                                    , listeners: {
                                        'check': { fn: this.onCheck, scope: this }
                                    }
                        }, {
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportCurrentBalans
                                    , ref: 'currentbookyear'
                                    , name: 'currentbookyear'
                                    , hidden: this.clientOnly
                                    , listeners: {
                                        'check': { fn: this.onCheck, scope: this }
                                    }
                        }, {
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportPreviousBalans
                                    , ref: 'lastbookyear'
                                    , name: 'lastbookyear'
                                    , hidden: !this.clientOnly ? !this.includePreviousYear : true
                                    , listeners: {
                                        'check': { fn: this.onCheck, scope: this }
                                    }
                        }, {
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportCustomers
                                    , ref: 'customers'
                                    , name: 'customers'
                        }, {
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportSuppliers
                                    , ref: 'suppliers'
                                    , name: 'suppliers'
}]
            }, { xtype: 'eBook-createFile-actions'
                        , ref: 'create'
}]
            , tbar: [{
                ref: '../empty',
                text: eBook.ProAcc.Window_StartImport,
                iconCls: 'eBook-empty-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onStartImport,
                scope: this
}]
            });
            eBook.ProAcc.Window.superclass.initComponent.apply(this, arguments);
        }
    , onCheck: function(chk, state) {
        switch (chk.name) {
            case 'accountschema':
                if (!state) {
                    this.frm.currentbookyear.setValue(false);
                    this.frm.lastbookyear.setValue(false);
                }
                break;
            case 'currentbookyear':
            case 'lastbookyear':
                if (state) {
                    this.frm.accountschema.setValue(true);
                }
                break;
        }
    }
    , onStartImport: function() {

        var acts = {
            actions: []
            , revertActions: []
            , postActions: []
        };
        var proAccServer = eBook.Interface.currentClient.get('ProAccServer');
        var proAccDatabase = eBook.Interface.currentClient.get('ProAccDatabase');
        var fileId = eBook.Interface.currentFile.get('Id');
        var clientId = eBook.Interface.currentClient.get('Id');

        var cur = this.frm.currentbookyear.getValue();
        var prev = this.frm.lastbookyear.getValue();

        var cust = this.frm.customers.getValue();
        var supp = this.frm.suppliers.getValue();
        var id = 0;

        if (cust) {
            acts.actions.push({ id: id
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , response: null
                , text: 'Import customers from ProAcc'
                , url: eBook.Service.businessrelation
                , action: 'ImportCustomersProAcc'
                , params: { cidc: { Id: clientId, Person:eBook.User.getActivePersonDataContract()} }
                , paramKey: 'cidc'
                , scope: this
            });
            id++;
        }
        
        if (supp) {
            acts.actions.push({ id: id
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , response: null
                , text: 'Import suppliers from ProAcc'
                , url: eBook.Service.businessrelation
                , action: 'ImportSuppliersProAcc'
                , params: { cidc: { Id: clientId, Person: eBook.User.getActivePersonDataContract()} }
                , paramKey: 'cidc'
                , scope: this
            });
            id++;
        }

        if (cur || prev) {
            acts.actions.push({ id: id
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , response: null
                , text: 'Import (new) accounts from proAcc including current state' + (prev ? ' and previous state' : '')
                , url: eBook.Service.file
                , action: 'ImportAccountsProAcc'
                , params: { iaedc: { fid: fileId, ips: prev} }
                , paramKey: 'iaedc'
                , callback: function(cr, resp) {
                    var robj = Ext.decode(resp.responseText);
                    robj = robj['ImportAccountsProAccResult'];
                    var dte = Ext.data.Types.WCFDATE.convert(robj);
                    eBook.Interface.center.fileMenu.mymenu.setLastImportDate(dte);
                }
                , scope: this
            });
            id++;
        }


        acts.actions.push({ id: 1
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , response: null
                , text: 'Update cache'
                , url: eBook.Service.file
                , action: 'ForceFileCacheReload'
                , params: { cfdc: { FileId: fileId, Culture: eBook.Interface.Culture} }
                , paramKey: 'cfdc'
                , callback: this.onActionReady
                , scope: this
        });
        id++;


        this.getTopToolbar().disable();
        this.layout.setActiveItem(this.create);
        this.create.on('actionsDone', this.onImportDone, this);
        this.create.setActions(acts);
        this.create.start.defer(200, this.create);
    }
    , onImportDone: function() {
        if (this.caller && Ext.isFunction(this.caller.importDone)) this.caller.importDone();
        Ext.Msg.alert(eBook.ProAcc.Window_ImportDoneTitle, eBook.ProAcc.Window_ImportDoneMsg);
        eBook.Interface.center.fileMenu.updateWorksheets();
        this.close.defer(500, this);
    }
    , show: function(caller) {
        this.caller = caller;
        eBook.ProAcc.Window.superclass.show.call(this);
    }
    });

Ext.reg('eBook.ProAcc.Window', eBook.ProAcc.Window);
// fields to add:
// Selection of ProAcc period
// Journal nr
// Start bookingnr

// TODO: How to prevent multiple import in ProAcc?

eBook.ProAcc.ExportWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        this.proAccServer = eBook.Interface.currentClient.get('ProAccServer');
        this.proAccDatabase = eBook.Interface.currentClient.get('ProAccDatabase');
        Ext.apply(this, {
            iconCls: ''
            , title: eBook.ProAcc.ExportWindow_Title
            , modal: true
            , layout: 'fit'
            , width: 420
            , height: 300
            , items: [{
                xtype: 'form',
                bodyStyle: 'padding:10px',
                labelWidth: 200,
                ref: 'detailedForm',
                items: [
                            { xtype: 'combo'
                                , ref: 'period'
                                , fieldLabel: 'ProAcc Period'
                                , name: 'Period'
                                , triggerAction: 'all'
                                , typeAhead: false
                                , selectOnFocus: true
                                , autoSelect: true
                                , allowBlank: false
                                , forceSelection: true
                                , valueField: 'Period'
                                , displayField: 'Name'
                                , editable: false
                                , nullable: false
                                , mode: 'remote'
                                , listWidth:300
                                , store: new eBook.data.JsonStore({
                                    selectAction: 'ProAccGetPeriods'
                                    //, autoLoad: true
                                    , autoDestroy: true
                                    , criteriaParameter: 'cpbdc'
                                    , baseParams: {
                                        Server: this.proAccServer
                                        , Database: this.proAccDatabase
                                    }
                                    , fields: eBook.data.RecordTypes.Periods
                                })
                            },
                            { xtype: 'combo'
                                , ref: 'journal'
                                , fieldLabel: 'Journal'
                                , name: 'Journal'
                                , triggerAction: 'all'
                                , typeAhead: false
                                , selectOnFocus: true
                                , autoSelect: true
                                , allowBlank: false
                                , forceSelection: true
                                , valueField: 'id'
                                , displayField: 'name'
                                , editable: false
                                , nullable: false
                                , mode: 'remote'
                                , listWidth:300
                                , store: new eBook.data.JsonStore({
                                    selectAction: 'ProAccGetJournals'
                                    //, autoLoad: true
                                    , autoDestroy: true
                                    , criteriaParameter: 'cpbdc'
                                    , baseParams: {
                                        Server: this.proAccServer
                                        , Database: this.proAccDatabase
                                    }
                                    , fields: eBook.data.RecordTypes.ProAccJournals
                                })
                            },
                            {
                                xtype: 'numberfield',
                                name: 'BookingNr',
                                fieldLabel: 'Start booking nr at',
                                ref: 'bookingnr',
                                allowDecimals: false,
                                allowNegative: false,
                                decimalPrecision: 0,
                                minValue: 1,
                                value: 1
                            }

                        ]
                        , buttons: [{
                            ref: 'performExport',
                            text: 'Export bookings',
                            iconCls: 'eBook-icon-exportProacc-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onCheckProAccExport,
                            width: 70,
                            scope: this
                        }
                        ]
}]
            });
            eBook.ProAcc.ExportWindow.superclass.initComponent.apply(this, arguments);
        }
    , onPerformExport: function() {
        // perform export to excel
        this.getEl().mask(eBook.Accounts.Mappings.GridPanel_PreparingExcelAdjust, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'ExportProAccBookings'
            , method: 'POST'
            , params: Ext.encode({ cpebdc: {
                fileId: eBook.Interface.currentFile.get('Id'),
                period: this.detailedForm.period.getValue(),
                journal: this.detailedForm.journal.getValue(),
                bookingnr: this.detailedForm.bookingnr.getValue(),
                Person: eBook.User.getActivePersonDataContract()
            }
            })
            , callback: this.onProAccExportComplete
            , scope: this
        });
    }
    , onProAccExportComplete: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open("pickup/" + robj.ExportProAccBookingsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onCheckProAccExport: function() {
        this.getEl().mask('Loading', 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'CheckExportProAcc'
            , method: 'POST'
            , params: Ext.encode({ cicdc: {
                Id: eBook.Interface.currentFile.get('Id'),
                Culture: eBook.Interface.Culture
            }
            })
            , callback: this.onCheckProAccExportComplete
            , scope: this
        });
    }
    , onCheckProAccExportComplete: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var ms = eBook.ProAcc.ExportWindow_LastExportedMessage;
            var robj = Ext.decode(resp.responseText); 
            if (robj.CheckExportProAccResult != null) {
                robj = robj.CheckExportProAccResult;
                robj.d = Ext.data.Types.WCFDATE.convert(robj.d);
                var dte = robj.d.format('l j F Y H:i:s');
                var msg = String.format(ms, robj.p.fn, robj.p.ln, dte);
                Ext.Msg.confirm(eBook.ProAcc.ExportWindow_ExportMessageTitle, msg, this.onCheckProAccExportConfirm, this);
            } else {
                this.onPerformExport();
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onCheckProAccExportConfirm: function(buttonid) {
        if (buttonid == "yes") {
            this.onPerformExport();
        }
    }
    });

eBook.Pdf.WindowMgr = new Ext.WindowGroup();
eBook.Pdf.WindowMgr.closeAll = function() {
    eBook.Pdf.WindowMgr.each(function(lst) { lst.close(); }, eBook.Pdf.WindowMgr);
};
eBook.Pdf.WindowMgr.zseed = 10000;


eBook.Pdf.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        var isClosed = eBook.Interface.currentFile.get('Closed');
        Ext.apply(this, {
            id: 'et-pdf-window',
            title: eBook.Pdf.Window_Title,
            iconCls:'eBook-Pdf-icon',
            items: [{ xtype: 'eBook.Pdf.DataView', ref: 'dataview'}],
            manager: eBook.Pdf.WindowMgr,
            autoScroll: true,
            minimizable: true,
            tbar: [{
                text: eBook.Pdf.Window_AddPdf,
                iconCls: 'eBook-pdf-add-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'addpdf',
                handler: this.onAddPdf,
                hidden: isClosed,
                scope: this
            }, {
                text: eBook.Pdf.Window_ReplacePdf,
                iconCls: 'eBook-pdf-replace-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'replacepdf',
                handler: this.onReplacePdf,
                disabled: true,
                hidden:isClosed,
                scope: this
            }, {
                text: eBook.Pdf.Window_RemovePdf,
                iconCls: 'eBook-pdf-remove-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'removepdf',
                handler: this.onDeletePdf,
                disabled: true,
                hidden: isClosed,
                scope: this
            }, {
                text: eBook.Pdf.Window_ViewPdf,
                iconCls: 'eBook-pdf-view-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'viewpdf',
                handler: this.onViewPdf,
                disabled: true,
                scope: this
            }, {
                text: eBook.Pdf.Window_RefreshList,
                iconCls: 'eBook-pdflist-refresh-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'refreshlist',
                handler: this.onRefreshList,
                scope: this
}],
                width: 460,
                bodyStyle: 'background-color:#FFF;padding:5px;',
                height: 460
            });
            eBook.Pdf.Window.superclass.initComponent.apply(this, arguments);

        }
    , show: function(args) {
        eBook.Pdf.Window.superclass.show.call(this);
        if (args && args.snapRight) {
            this.setPosition(eBook.Interface.viewPort.getWidth() - this.width - 20, 100);
        }
    }
    , minimized: false
    , minimize: function(wn) {
        this.toggleCollapse();
        this.minimized = !this.minimized;
    }
    , maximize: function() {
        if (this.minimized) {
            this.minimize();
        } else {
            eBook.Pdf.Window.superclass.maximize.call(this);
        }
    }
    , onSelect: function() {
        var tb = this.getTopToolbar();
        tb.replacepdf.enable();
        tb.removepdf.enable();
        tb.viewpdf.enable();
    }
    , onDeselect: function() {
        var tb = this.getTopToolbar();
        tb.replacepdf.disable();
        tb.removepdf.disable();
        tb.viewpdf.disable();
    }
    , onAddPdf: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var wn = new eBook.Pdf.UploadWindow({ parentCaller: this });
        wn.show(this);
    }
    , onReplacePdf: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var pdf = this.dataview.getSelectedRecord();
        var wn = new eBook.Pdf.UploadWindow({ parentCaller: this, replacement: pdf });
        wn.show(this);
    }
    , onDeletePdf: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var pdf = this.dataview.getSelectedRecord();
        if (pdf) {

            Ext.WindowMgr.zseed = 20000;
            Ext.Msg.show({
                title: eBook.Bundle.Window_LibraryDeletePdf,
                msg: String.format(eBook.Bundle.Window_DeletePdfMsg, pdf.get('Name')),
                buttons: Ext.Msg.YESNO,
                fn: this.performDelete,
                scope: this,
                icon: Ext.MessageBox.QUESTION
            });
        }
    }
    , performDelete: function(btnid) {
        Ext.WindowMgr.zseed = 9000;
        if (btnid == "yes") {
            var pdf = this.dataview.getSelectedRecord();
            if (!pdf) return;

            var action = {
                text: String.format(eBook.Bundle.Window_DeletingPdfMsg, pdf.get('Name'))
                    , params: { cidc: {
                        Id: pdf.get('Id')
                    }
                    }
                    , action: 'DeletePdf'
            };
            this.dataview.removeSelected();
            this.onDeselect();
            this.addAction(action);
        }
    }
    , onViewPdf: function(pdf) {
        if (!pdf || !pdf.get) pdf = this.dataview.getSelectedRecord();
        window.open('ViewPdf.aspx?id=' + pdf.get('Id') + '&nm=' + pdf.get('Name'));
    }
    , refreshPdfs: function() {
        this.onRefreshList();
    }
    , onRefreshList: function() {
        this.dataview.store.load();
    }
    });

Ext.reg('eBook.Pdf.Window', eBook.Pdf.Window);


eBook.Pdf.DataView = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'et-pdf-view',
            tpl: '<tpl for=".">' +
                    '<div class="et-pdf-item" id="{Id}">' +
                        '<table><tbody>' +
                        '<tr><td style="text-align:left;" unselectable="on">' +
                            '{Name}' +
                        '</td>' +
                        '<td width="50" style="text-align:right;" unselectable="on">' +
                            '{Pages}p.' +
                        '</td></tr></table>' +
                    '</div>' +
                 '</tpl>',
            itemSelector: 'div.et-pdf-item',
            overClass: 'et-pdf-item-over',
            selectedClass: 'et-pdf-item-selected',
            singleSelect: true,
            store: new eBook.data.JsonStore({
                selectAction: 'GetFilePdfFiles'
                , autoDestroy: true
                , autoLoad: true
                , criteriaParameter: 'cidc'
                , fields: eBook.data.RecordTypes.PdfFile
                , baseParams: {
                    'Culture': eBook.Interface.Culture
                    , 'Id': eBook.Interface.currentFile.get('Id')
                }
            }),
            listeners: {
                render: this.initializeDragZone
                , selectionchange: this.onSelectionChange
                , dblclick: this.onDoubleClick
                , scope: this
            }
        });
        eBook.Pdf.DataView.superclass.initComponent.apply(this, arguments);
    }
    , onSelectionChange: function(dv, sels) {
        var selId = null; 
        if(sels && sels.length>0) selId=sels[0].id;

        if (this.selId != selId) {
            this.selId = selId;
            this.refOwner.onSelect();
        } else {
            this.selId = null;
            this.clearSelections(true);
            this.refOwner.onDeselect();
        }
    }
    , onDoubleClick: function(dv, idx, n, e) {
        this.refOwner.onViewPdf(this.store.getAt(idx));
    }
    , getSelectedRecord: function() {
        if (this.selId == null) return null;
        return this.getSelectedRecords()[0];
    }
    , removeSelected: function() {
        var sels = this.getSelectedRecords();
        this.store.remove(sels);
        this.store.commitChanges();
    }
    , initializeDragZone: function(v) {
        this.dragZone = new Ext.dd.DragZone(v.getEl(), {
            getDragData: function(e) {
                var sourceEl = e.getTarget(v.itemSelector, 10);
                if (sourceEl) {
                    d = sourceEl.cloneNode(true);
                    d.className = 'et-pdf-item-drag';
                    d.id = Ext.id();
                    return v.dragData = {
                        sourceEl: sourceEl,
                        repairXY: Ext.fly(sourceEl).getXY(),
                        ddel: d,
                        pdfData: v.getRecord(sourceEl).data
                    }
                }
            },
            getRepairXY: function() {
                return this.dragData.repairXY;
            }
        });
    }
});

Ext.reg('eBook.Pdf.DataView', eBook.Pdf.DataView);

eBook.Pdf.UploadWindow = Ext.extend(eBook.Window, {

    initComponent: function() {
        Ext.apply(this, {
            title: eBook.Pdf.UploadWindow_Title
            , layout: 'fit'
            , width: 650
            , height: 200
            , manager: eBook.Pdf.WindowMgr
            , modal: true
            //, iconCls: 'eBook-Window-account-ico'
            , items: [new Ext.FormPanel({
                        fileUpload: true
                        ,labelWidth:150
                        , items: [
                                {
                                    xtype: 'combo'
                                    , ref: 'replaceFor'
                                    , name: 'replaceFor'
                                    , fieldLabel: eBook.Pdf.UploadWindow_ReplacePdf
                                    , triggerAction: 'all'
                                    , typeAhead: false
                                    , selectOnFocus: true
                                    , autoSelect: true
                                    , allowBlank: true
                                    , forceSelection: true
                                    , valueField: 'Id'
                                    , displayField: 'Name'
                                    , editable: false
                                    , nullable: true
                                    , mode: 'local'
                                    , width: 350
                                    , store: new eBook.data.JsonStore({
                                        selectAction: 'GetFilePdfFiles'
                                        , autoDestroy: true
                                        , autoLoad: true
                                        , criteriaParameter: 'cidc'
                                        , baseParams: {
                                            'Culture': eBook.Interface.Culture
                                            , 'Id': eBook.Interface.currentFile.get('Id')
                                        }
                                        , fields: eBook.data.RecordTypes.PdfFile
                                    })
                                }
                                , new Ext.ux.form.FileUploadField({ width: 350, fieldLabel: eBook.Pdf.UploadWindow_ChoosePDF, fileTypeRegEx: /(\.pdf)$/i })]
                        , ref: 'pdfForm'

            })]
            , tbar: [{
                text: eBook.Pdf.UploadWindow_UploadPDF,
                ref: '../saveButton',
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
}]
            });

            eBook.Pdf.UploadWindow.superclass.initComponent.apply(this, arguments);
        }
    , onSaveClick: function(e) {
        var f = this.pdfForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Pdf.UploadWindow_WrongType);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Pdf.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function(fp, o) {
        //process o.result.file
        this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'ProcessNewPDF'
                , method: 'POST'
                , params: Ext.encode({ cnpdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    PdfFileName: o.result.file,
                    OriginalName: o.result.originalFile,
                    ReplacementFor: this.pdfForm.replaceFor.getValue()
                }
                })
                , callback: this.handleFileProcessing
                , scope: this
        });
    }
    , failedUpload: function(fp, o) {
        eBook.Interface.showError(o.message, this.title);
        this.getEl().unmask();
    }
    , handleFileProcessing: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            if (this.parentCaller) this.parentCaller.refreshPdfs();
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , show: function(caller) {
        this.parentCaller = caller;
        eBook.Pdf.UploadWindow.superclass.show.call(this);
        if (this.replacement) {
            this.pdfForm.replaceFor.setValue(this.replacement.get('Id'));
            this.pdfForm.replaceFor.setDisplayValue(this.replacement.get('Name')); 
        }
    }
});

eBook.Pdf.ManualUploadWindow = Ext.extend(eBook.Window, {

    initComponent: function() {
        Ext.apply(this, {
            title: eBook.Pdf.UploadWindow_Title
            , layout: 'fit'
            , width: 650
            , height: 200
            , manager: eBook.Pdf.WindowMgr
            , modal: true
            //, iconCls: 'eBook-Window-account-ico'
            , items: [new Ext.FormPanel({
                fileUpload: true
                        , labelWidth: 150
                        , items: [
                                new Ext.ux.form.FileUploadField({ width: 350, fieldLabel: eBook.Pdf.UploadWindow_ChoosePDF, fileTypeRegEx: /(\.pdf)$/i })]
                        , ref: 'pdfForm'

            })]
            , tbar: [{
                text: eBook.Pdf.UploadWindow_UploadPDF,
                ref: '../saveButton',
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
}]
            });

            eBook.Pdf.ManualUploadWindow.superclass.initComponent.apply(this, arguments);
        }
    , onSaveClick: function(e) {
        var f = this.pdfForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Pdf.UploadWindow_WrongType);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Pdf.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function(fp, o) {
        //process o.result.file
        this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
        Ext.Ajax.request({
                url: eBook.Service.url + 'ProcessNewManualPDF'
                , method: 'POST'
                , params: Ext.encode({ cnpdc: {
                    ManualId: this.manualId,
                    PdfFileName: o.result.file,
                    OriginalName: o.result.originalFile,
                    ReplacementFor: null
                }
                })
                , callback: this.handleFileProcessing
                , scope: this
        });
    }
    , failedUpload: function(fp, o) {
        eBook.Interface.showError(o.message, this.title)
        this.getEl().unmask();
    }
    , handleFileProcessing: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            if (this.parentCaller) this.parentCaller.refreshPdfs();
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , show: function(caller) {
        this.parentCaller = caller;
        eBook.Pdf.ManualUploadWindow.superclass.show.call(this);
        if (this.replacement) {
            this.pdfForm.replaceFor.setValue(this.replacement.get('Id'));
            this.pdfForm.replaceFor.setDisplayValue(this.replacement.get('Name'));
        }
    }
});
eBook.Help.Window = Ext.extend(eBook.Window, {
    constructor: function(config) {
        this.config = config;
        Ext.apply(this, config || {}, {
            id: 'et-help-window',
            manager: eBook.Help.WindowMgr,
            modal: true,
            width: 1000,
            layout: 'border',
            title: 'Help',
            items: [{ xtype: 'eBook.Help.View', region: 'center', cls:'ebook-help-view', worksheetID: config.worksheetID },
                    { xtype: 'eBook.Help.IndexView', region: 'east', cls:'ebook-help-indexview', width: 300 }]

        });
        //eBook.Help.Window.superclass.initComponent.apply(this, arguments);
        eBook.Help.Window.superclass.constructor.call(this, config);
    },
    getStore: function(){
        var storeCfg = {
            selectAction: 'GetWorksheetHelpByID',
            serviceUrl: eBook.Service.url,
            fields: eBook.data.RecordTypes.WorksheetHelp,
            autoDestroy: true,
            autoLoad: true,
            baseParams: { cicdc:{Id: this.worksheetID, Culture: eBook.Interface.Culture}}
            //,            groupField: 'id'
        };
        return storeCfg;
    }
});

eBook.Help.View = Ext.extend(Ext.DataView, {
    initComponent: function() {
        var storeCfg = this.ownerCt.getStore();

        Ext.apply(this, {
            store: new eBook.data.GroupedJsonStore(storeCfg)
            , loadMask: true
            , autoScroll: true
            ,emptyText:'No help available for this worksheet. Please sent your suggestions to ebookquestions@be.ey.com.'
            , tpl: new Ext.XTemplate(
                
                '<tpl if="(values)">',
                    '<tpl for=".">',
		            '<div class="eBook-help-block">',
		                '<div id="{subject}" class="eBook-help-block-subject">{subject}</div>',
		                '<div class="eBook-help-block-body">{body}</div>',
		                '</br>',
		                '</br>',
		            '</div>',
                '</tpl>',   
                '</tpl>',
		                     
                '<div class="x-clear"></div>'
	        )
            
            });
            eBook.Help.View.superclass.initComponent.apply(this, arguments);
        }

    });
    Ext.reg('eBook.Help.View', eBook.Help.View);
    
    eBook.Help.IndexView = Ext.extend(Ext.DataView, {
    initComponent: function() {
        var storeCfg = this.ownerCt.getStore();

        Ext.apply(this, {
            store: new eBook.data.GroupedJsonStore(storeCfg)
            , tpl: new Ext.XTemplate(
                '<h1 class="ebook-help-index-h1">Index</h1>',
		        '<tpl for=".">',
		            '<div class="ebook-help-index">',
		                '<div class="ebook-help-index-subject"><a href="#{subject}">{subject}</a></div>',
		            '</div>',
                '</tpl>',
                '<div class="x-clear"></div>'
	        )
            });
            eBook.Help.IndexView.superclass.initComponent.apply(this, arguments);
        }

    });
    Ext.reg('eBook.Help.IndexView', eBook.Help.IndexView);
eBook.Help.ChampionsGridView = Ext.extend(Ext.grid.GroupingView, {
    initTemplates: function() {
        Ext.grid.GroupingView.superclass.initTemplates.call(this);
        this.state = {};

        var sm = this.grid.getSelectionModel();
        sm.on(sm.selectRow ? 'beforerowselect' : 'beforecellselect',
        this.onBeforeRowSelect, this);

        if (!this.startGroup) {
            this.startGroup = new Ext.XTemplate(
                '<div id="{groupId}" class=\'x-grid-group {cls} <tpl if="values.gvalue==eBook.User.defaultOffice.n">et-champion-grid-highlight</tpl>\'>',
                '<div id="{groupId}-hd" class="x-grid-group-hd" style="{style}"><div class="x-grid-group-title">', this.groupTextTpl, '</div></div>',
                '<div id="{groupId}-bd" class="x-grid-group-body">'
            );
        }
        this.startGroup.compile();

        if (!this.endGroup) {
            this.endGroup = '</div></div>';
        }
    }
});

eBook.Help.ChampionsGrid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        var storeCfg = {
            selectAction: 'GetAllChampions',
            serviceUrl:eBook.Service.home,
            fields: eBook.data.RecordTypes.Champion,
            autoDestroy: true,
            autoLoad: true,
            groupField: 'officeName'
        };

        Ext.apply(this, {
            store: new eBook.data.GroupedJsonStore(storeCfg)
            , loadMask: true
            , columnLines: true
            , columns: [{
                header: eBook.Help.ChampionsGrid_Office,
                dataIndex: 'officeName',
                align: 'left',
                // hidden: true,
                groupRenderer: function(v, unused, r, ridx, cidx, ds) {
                    return v;
                }
                    , hideable: false
            }, {
                header: eBook.Help.ChampionsGrid_FirstName,
                dataIndex: 'firstName',
                align: 'left',
                hideable: false,
                sortable: true,
                renderer: function(v, m, r, ri, ci, s) {
                    return v + ' ' + r.get('lastName');
                }
            }, {
                header: eBook.Help.ChampionsGrid_Email,
                dataIndex: 'email',
                align: 'left',
                hideable: false,
                renderer: function(v, m, r, ri, ci, s) {
                    return '<a href="mailto:' + v + '">' + v + '</a>';
                },
                sortable: true
}]
            , view: new eBook.Help.ChampionsGridView({
                forceFit: true
                , groupTextTpl: '{group}'
                , showGroupName: false
                , enableGrouping: true
                , enableGroupingMenu: false
                , enableNoGroups: false
                , hideGroupedColumn: true
            })
            });
            eBook.Help.ChampionsGrid.superclass.initComponent.apply(this, arguments);
        }

    });
Ext.reg('eBook.Help.ChampionsGrid', eBook.Help.ChampionsGrid);

eBook.Help.ChampionsWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            id: 'et-help-champions-window',
            manager: eBook.Help.WindowMgr,
            title:'eBook Champions',
            items: [{xtype:'eBook.Help.ChampionsGrid'}],
            modal: true,
            iconCls: 'eBook-Team-icon',
            layout: 'fit',
            width: 500
        });
        eBook.Help.ChampionsWindow.superclass.initComponent.apply(this, arguments);
    }
});

eBook.PMT.TeamGrid = Ext.extend(Ext.grid.GridPanel, {
    includePMT: true,
    includeMail: false,
    includeEbook: true,
    initComponent: function() {
        var storeCfg;
        var cols = [];
        cols.push({
            header: '', //eBook.PMT.TeamGrid_Department,
            dataIndex: 'department',
            align: 'left',
            hidden: true,
            groupRenderer: function(v, unused, r, ridx, cidx, ds) {
                if (!v) return "GLOBAL";
                return v;
            }
                    , hideable: false
        });
        cols.push({
            header: '', //eBook.PMT.TeamGrid_Member,
            dataIndex: 'name',
            align: 'left',
            summaryType: 'count',
            summaryRenderer: function(v, params, data) {
                return ((v === 0 || v > 1) ? '(' + v + ' Members)' : '(1 Member)');
            }
                    , hideable: false
                    , sortable: true
        });
        if (this.includeMail) {
            cols.push({
                header: 'Mail', // eBook.PMT.TeamGrid_PMTRole,
                //width: 50,
                dataIndex: 'mail',
                align: 'left',
                // fixed: true
                hideable: false,
                sortable: true,
                renderer: function(value) {
                    return '<a href="mailto:' + value + '">' + value + '</a>';
                }
            });
        }
        if (this.includePMT) {
            cols.push({
                header: 'PMT', // eBook.PMT.TeamGrid_PMTRole,
                width: 50,
                dataIndex: 'role',
                align: 'left',
                // fixed: true
                hideable: false,
                sortable: true
            });
        }
        if (this.includeEbook) {
            cols.push({
                header: 'eBook', //eBook.PMT.TeamGrid_eBookRole,
                width: 50,
                dataIndex: 'level',
                align: 'left',
                renderer: function(v, m, r, ri, ci, s) {
                    if (v > 3) return eBook.PMT.TeamGrid_Editor;
                    if (v > 1) return eBook.PMT.TeamGrid_Reviewer;
                    if (v == 1) return eBook.PMT.TeamGrid_Administrator;
                },
                hideable: false,
                sortable: true
            });
        }
        if (!this.storeCfg) {
            storeCfg = {
                selectAction: 'GetClientTeam',
                fields: eBook.data.RecordTypes.TeamMember,
                serviceUrl: eBook.Service.client,
                idField: 'Id',
                autoDestroy: true,
                autoLoad: true,
                criteriaParameter: 'cicdc',
                baseParams: {
                    Id: eBook.Interface.currentClient.get('Id')
                        , Culture: eBook.Interface.Culture
                }

                , groupField: 'department'
            };
        } else {
            storeCfg = this.storeCfg
        }

        if (Ext.isDefined(storeCfg.groupField)) {
            Ext.apply(this, {
                store: new eBook.data.GroupedJsonStore(storeCfg)
                , plugins: [new Ext.ux.grid.GridSummary(), new Ext.ux.grid.GroupSummary()]
                , view: new Ext.grid.GroupingView({
                    forceFit: true
                    , groupTextTpl: '{group}'
                    , showGroupName: false
                    , enableGrouping: true
                    , enableGroupingMenu: false
                    , enableNoGroups: false
                    , hideGroupedColumn: true
                })
            });
        } else {
            Ext.apply(this, {
                store: new eBook.data.JsonStore(storeCfg)
                , view: new Ext.grid.GridView({ forceFit: true, templates: { cell: new Ext.Template(
                        '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}>',
                        '<div class="x-grid3-cell-inner x-grid3-col-{id} x-selectable" {attr}>{value}</div>',
                        '</td>'
                        )
                        , row: new Ext.Template(
                            '<div class="x-grid3-row {alt} x-selectable" style="{tstyle}"><table class="x-grid3-row-table x-selectable" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
                            '<tbody><tr>{cells}</tr>',
                            '</tbody></table></div>'
                        )
                }
                })
                , disableSelection: true
                , trackMouseOver: false
            });
        }


        Ext.apply(this, {
            // store: new eBook.data.GroupedJsonStore(storeCfg)
            // , plugins: [new Ext.ux.grid.GridSummary(), new Ext.ux.grid.GroupSummary()]
            loadMask: true
            , columnLines: true

            , columns: cols
            , enableHdMenu: true

        });
        eBook.PMT.TeamGrid.superclass.initComponent.apply(this, arguments);
    }
});

    Ext.reg("eBook.PMT.TeamGrid", eBook.PMT.TeamGrid);eBook.PMT.TeamWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout:'fit',
            modal: false,
            iconCls: 'eBook-Team-icon',
            title:eBook.PMT.TeamWindow_Title,
            width:550,
            items:[{
                xtype: 'eBook.PMT.TeamGrid',
                    ref:'grid'
                    }]
            /*tbar: [{
                text: 'Update from PMT',
                iconCls: 'eBook-pmt-refresh-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'updatepmt',
                handler: this.onUpdatePMT,
                scope: this
            }]*/
        });
        eBook.PMT.TeamWindow.superclass.initComponent.apply(this, arguments);
    }
    ,onUpdatePMT:function() {
    }
});


eBook.Xbrl.ClientDetails = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            labelWidth: 150,
            items: [
            //{ xtype: 'Xbrl.PreformattedContacts', ref: 'preformatted', fieldLabel: 'Preformatted contact' }
            //,
                {xtype: 'textfield', ref: 'name', fieldLabel: eBook.Xbrl.ClientDetails_Name, allowBlank: false, width: 250 }
                , { xtype: 'legaltype', ref: 'legaltype', fieldLabel: eBook.Xbrl.ClientDetails_Legaltype, allowBlank: false }
                , { xtype: 'textfield', ref: 'enterprisenr', fieldLabel: eBook.Xbrl.ClientDetails_EnterpriseNr, allowBlank: false, width: 250 }
                , { xtype: 'fod.address', ref: 'address', fieldLabel: ' ', labelSeparator: '', title:eBook.Xbrl.ClientDetails_Address, width: 550 }
                , { xtype: 'bankdetails', ref: 'bank', fieldLabel: ' ', labelSeparator: '', title: eBook.Xbrl.ClientDetails_BankInfo, width: 550 }
            ]
            , autoScroll: true
            , bodyStyle: 'padding:10px;'
        });
        eBook.Xbrl.ClientDetails.superclass.initComponent.apply(this, arguments);
    }
    , isValid: function(prevMark) {
        var t = this.name.isValid(prevMark)
                || this.legaltype.isValid(prevMark)
                || this.enterprisenr.isValid(prevMark)
                || this.address.isValid(prevMark)
                || this.bank.isValid(prevMark);

        return this.name.isValid(prevMark)
                && this.legaltype.isValid(prevMark)
                && this.enterprisenr.isValid(prevMark)
                && this.bank.isValid(prevMark)
                && this.address.isValid(prevMark);
    }
    , loadContract: function(contract) {
        if (contract) {
            this.name.setValue(contract.n ? contract.n : '');
            this.legaltype.setValue(contract.lti ? contract.lti : '');
            this.enterprisenr.setValue(contract.enr ? contract.enr : '');
            this.address.setValue(contract.foda ? contract.foda : null);
            this.bank.setValue(contract.b ? contract.b : null);
        }
    }
    , getContract: function() {
        return {
            n: this.name.getValue()
            , lti: this.legaltype.getValue()
            , enr: this.enterprisenr.getValue()
            , foda: this.address.getValue()
            , b: this.bank.getValue()
        };
    }
});

Ext.reg('xbrl.client', eBook.Xbrl.ClientDetails);


eBook.Xbrl.AttachmentPanel = Ext.extend(Ext.Panel, {
    contractName: 'attachments'
    , attachments: []
    , defaultInValidMsg: eBook.Xbrl.AttachmentPanel_DefaultInValidMsg //'Volgende bijlagen zijn aangeduid, maar bevatten geen onderdelen:'
    , currentStep: 0
    , noWizardNext: true
    , noWizardPrev: true
    , initializeData: function() {
        if (!this.answers) this.answers = [];

        for (var i = 0; i < this.answers.length; i++) {

            var at = this.answers[i];
            //var j = this.getAnswer(at.id);

            /*
            if (!this.answers[at.idx]) this.answers[at.idx] = at;
            this.answers[at.idx].id = at.id;
            if (!this.answers[at.idx].items) this.answers[at.idx].items = [];
            this.answers[at.idx].name = at.name;
            this.answers[at.idx].automated = at.automated;
            this.answers[at.idx].idx = at.idx;
            if (typeof (this.answers[at.idx].checked) == "undefined") {
            this.answers[at.idx].checked = at.checked;
            }
            */
            for (var j = 0; j < this.answers[at.idx].items.length; j++) {
                switch (this.answers[at.idx].items[j].__type) {
                    case 'PDFDataContract:#EY.com.eBook.API.Contracts.Data':
                        this.answers[at.idx].items[j].iconCls = 'eBook-bundle-tree-pdf';
                        break;
                    case "WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data":
                        this.answers[at.idx].items[j].iconCls = 'eBook-bundle-tree-worksheet';
                        break;
                    case "StatementsDataContract:#EY.com.eBook.API.Contracts.Data":
                        this.answers[at.idx].items[j].iconCls = 'eBook-bundle-tree-statement';
                        break;
                    case "DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data":
                        this.answers[at.idx].items[j].iconCls = 'eBook-bundle-tree-document';
                        break;
                }
            }
            //this.answers[at.id].items.push({ id: 'test', name: 'test pdf' });
        }
    }
    , initComponent: function() {

        this.initializeData();

        this.at_tpl = new Ext.XTemplate('<tpl for="."><tpl for="answers">'
                                    , '<div id="xbrlattach-{id}" class="eBook-Xbr-Attachment" idx="{#}">'
                                    , '<input type="checkbox" id="check{id}"<tpl if="required==1 || automated"> disabled="true"</tpl> value="yes"<tpl if="checked"> checked="true"</tpl>/>&nbsp;<span class="eBook-Xbrl-Attachment-Title"><b>{name}</b></span>'
                                    , '<tpl if="!this.isNullOrEmpty(subText)">'
                                        , '<div class="eBook-Xbrl-SubText">{subText}</div>'
                                    , '</tpl>'
                                    , '<tpl if="automated">'
                                        , '<div class="eBook-Xbrl-Automated">{eBook.Xbrl.AttachmentPanel_automated}</div>'
                                    , '</tpl>'
                                    , '<tpl if="!automated">'
                                        , '<div class="eBook-Xbrl-Attachment-Items"<tpl if="!checked"> style="display:none;"</tpl>>'
                                        , '<div class="eBook-Xbrl-Attachment-Add">' + eBook.Xbrl.AttachmentPanel_Add + '</div>'
                                        , '<div class="eBook-Xbrl-Attachment-List">'
        //, '<tpl if="parent.answers[idx]">'
                                        , '<tpl for="items"><div class="eBook-Xbrl-Attachment-item"><div class="eBook-Xbrl-Attachment-item-text {iconCls}" idx="{#}" itemId="{id}">{title}</div> <div class="eBook-Xbrl-Attachment-Remove">' + eBook.Xbrl.AttachmentPanel_Delete + '</div><div class="x-clear"></div></div></tpl>'
        //, '</tpl>'
                                        , '</div>'
                                        , '</div>'
                                     , '</tpl>'
                                     , '</div>'
                                    , '</tpl></tpl>'
                                    , {
                                        compiled: true
                                        , isNullOrEmpty: function(val) { return (val == '' || !val); }
                                        , getItems: function(idx, par) {
                                            if (par.answers[idx]) {
                                                if (par.answers[idx].items) return par.answers[idx].items;
                                            }
                                            return [];
                                        }
                                        , getAnswer: function(idx, par) {
                                            return par.answers[idx];
                                        }
                                    });

        this.at_tpl.compile();
        Ext.apply(this, {
            html: this.at_tpl.apply({ attachments: this.attachments, answers: this.answers })
            , autoScroll: true
        });
        eBook.Xbrl.AttachmentPanel.superclass.initComponent.apply(this, arguments);
        //this.on('click', this.onItemClick, this);
    }
    , afterRender: function() {
        eBook.Xbrl.AttachmentPanel.superclass.afterRender.call(this);
        this.initItemEvents();
    }
    , isValid: function(prevMark) {
        this.invalidMsg = this.defaultInValidMsg;
        this.invalidMsg += '<ul>';

        var valid = true;
        for (var i = 0; i < this.answers.length; i++) {
            var as = this.answers[i];
            if (as.checked && !as.automated) {
                if (!(as.items != null && as.items.length > 0)) {
                    valid = false;
                    this.invalidMsg += '<li>' + as.name + '</li>';
                }
            }
        }
        this.invalidMsg += '</ul>';
        return valid;
    }
    , purgeItemEvents: function() {
        if (this.el) {
            var its = this.el.select(".eBook-Xbr-Attachment");
            if (its) {
                Ext.EventManager.removeAll(its);
            }
        }
    }
    , initItemEvents: function() {

        var its = this.el.select(".eBook-Xbr-Attachment");
        its.on('click', this.onItemClick, this);
    }
    , onItemClick: function(e, t, o) {
        var it = e.getTarget('.eBook-Xbr-Attachment')
            , chk = e.getTarget('input')
            , titem = e.getTarget('.eBook-Xbrl-Attachment-Title')
            , pitem = e.getTarget('.eBook-Xbrl-Attachment-item')
            , ritem = e.getTarget('.eBook-Xbrl-Attachment-Remove')
            , ditem = e.getTarget('.eBook-Xbrl-Attachment-Add')
            , atitems
            , idx
            , pidx
            , id;

        if (it) {
            it = Ext.get(it);
            var idx = parseInt(it.getAttribute("idx")) - 1;
            atitems = it.child(".eBook-Xbrl-Attachment-Items");
            id = it.id.replace('xbrlattach-', '');
            if (!this.answers[idx]) this.answers[idx] = { answer: "yes" };
            if (chk) {
                this.answers[idx].checked = chk.checked;
                if (chk.checked) {
                    atitems.setStyle('display', '');
                } else {
                    atitems.setStyle('display', 'none');
                }
            } else if (titem) {
                chk = Ext.get(titem).parent().child('input').dom;
                if (!chk.disabled) {
                    chk.checked = !chk.checked;
                    this.answers[idx].checked = chk.checked;
                    if (chk.checked) {
                        atitems.setStyle('display', '');
                    } else {
                        atitems.setStyle('display', 'none');
                    }
                }

            } else if (pitem) {

                pitem = Ext.get(pitem);
                var ul = pitem.parent('ul');
                var pidx = parseInt(pitem.getAttribute("idx")) - 1;
                if (ritem) {
                    pitem.remove();
                    this.answers[idx].items.splice(pidx, 1);
                    var nds = ul.select('li');
                    nds.each(function(el, c, idx) {
                        el.dom.setAttribute('idx', idx);
                    });
                }
            } else if (ditem) {
                this.onAddItemClick(ditem, id, idx);
            } else {
                // console.log(this.answers);
            }
        }

    }
    , onAddItemClick: function(ditem, id, idx) {
        this.addingIdx = idx;
        var t;
        if (this.manual) {
            t = new eBook.Client.ManualResourcesWindow({
                culture: this.ownerCt.ownerCt.manual.File.c
                    , manualId: this.ownerCt.ownerCt.manual.Id
            });
        } else {
            t = new eBook.Bundle.ResourcesWindow({});
        }

        t.show(this);
    }
    , resourcesPicked: function(dta) {
        var dta = this.answers[this.addingIdx].items.concat(dta);
        this.answers[this.addingIdx].items = this.unique(dta);
        this.reload();
    }
    , unique: function(arr) {
        var ret = [],
                collect = {};

        Ext.each(arr, function(v) {
            if (!collect[v.id]) {
                ret.push(v);
            }
            collect[v.id] = true;
        });
        return ret;
    }
    , onValidate: function() {
        // validation check ?
    }
    , getContract: function() {
        return this.answers;
    }
    , reload: function() {
        this.initializeData();
        if (this.el) {
            //var l = Ext.get(this.el);
            this.purgeItemEvents();
            this.update("");
            this.update(this.at_tpl.apply({ attachments: this.attachments, answers: this.answers }));
            this.initItemEvents();
        }
    }
    , loadContract: function(contract) {
        if (contract) {
            this.answers = contract;
        } else {
            //
        }
        this.reload();
    }
});
        
Ext.reg('xbrl.attachments', eBook.Xbrl.AttachmentPanel);
// checks if the file has errors prior to start wizard/overview.
//Dossier wordt gevalideerd


eBook.Xbrl.ValidationCheckPanel = Ext.extend(Ext.Panel, {
    wizardPanel: {
        navigationbar: false
    }
    , data: { Message: 'Dossier wordt gevalideerd', state: 'Valid', loading: -1 }
    , initComponent: function() {
        this.bTpl = new Ext.XTemplate('<tpl for=".">'
                                      , '<div class="eBook-Xbrl-Validation-block">'
                                        , '<tpl if="loading==1">'
                                            , '<div class="eBook-Xbrl-Validation eBook-Xbrl-Validating">' + eBook.Xbrl.ValidationCheckPanel_Validating + '</div>'
                                        , '</tpl>'
                                        , '<tpl if="loading==0">'
                                            , '<div class="eBook-Xbrl-Validation eBook-Xbrl-Validation-Result eBook-Xbrl-Validation-{state}">'
                                            , '{Message}'
                                            , '</div>'
                                        , '</tpl>'
                                      , '</div>'
                                      , '</tpl>', { compiled: true });
        Ext.apply(this, {

            html: ''

        });
        eBook.Xbrl.ValidationCheckPanel.superclass.initComponent.apply(this, arguments);
        this.on('afterlayout', this.onActivate, this);
    }
    , isValid: function() {
        return true;
    }
    , reload: function() {
        if (this.el) {
            this.update("");
            this.update(this.bTpl.apply(this.data));
            this.ownerCt.doLayout();
        }
    }
    , afterRender: function() {
        eBook.Xbrl.ValidationCheckPanel.superclass.afterRender.apply(this, arguments);
        this.reload();
    }
    , onActivate: function(v) {
        if (this.rendered) {

        }

    }
    , performValidation: function() {
        //Ext.Ajax.request({});
        // or from message store.
        this.data.loading = 1;
        this.reload();
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'Validate'
                    , method: 'POST'
                    , params: Ext.encode({ cicdc: {
                        Id: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                    }
                    })
                    , success: this.onPerformValidationSucces
                    , failure: this.onPerformValidationFailure
                    , scope: this
        });

    }
    , onPerformValidationSucces: function(resp, opts) {
        // validation success image
        // show warnings if existing.
        // move next
        var robj = Ext.decode(resp.responseText);
        robj = robj.ValidateResult;
        this.data.loading = 0;
        this.data.Message = robj.Message;
        this.data.Validated = robj.Validated;
        this.data.Errors = robj.Errors;
        this.data.Warnings = robj.Warnings;
        this.data.WorksheetsClosed = robj.WorksheetsClosed;
        this.ownerCt.showNext();
        switch (robj.Validated) {
            case -1:
                this.ownerCt.hideAllButtons();
                this.data.state = 'Errors';
                break;
            case 0:
                this.data.state = 'Warnings';
                break;
            case 1:
                this.data.state = 'Valid';
                this.ownerCt.onNext.defer(1000, this.ownerCt);
        }
        this.reload();
    }
    , onPerformValidationFailure: function(resp, opts) {
        // validation failed image
        // + message
        alert("failed");
    }
});

Ext.reg('Xbrl.ValidationCheckPanel', eBook.Xbrl.ValidationCheckPanel);


eBook.Xbrl.ContactDetails = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            labelWidth: 150,
            items: [
            //{ xtype: 'Xbrl.PreformattedContacts', ref: 'preformatted', fieldLabel: 'Preformatted contact' }
            //,
                {xtype: 'fod.contacttype', ref: 'contacttype', fieldLabel: eBook.Xbrl.ContactDetails_Contacttype, allowBlank: false }
                , { xtype: 'textfield', ref: 'func', fieldLabel: eBook.Xbrl.ContactDetails_Function, allowBlank: false, width: 250 }
                , { xtype: 'textfield', ref: 'name', fieldLabel: eBook.Xbrl.ContactDetails_Name, allowBlank: false, width: 250 }
                , { xtype: 'textfield', ref: 'firstname', fieldLabel: eBook.Xbrl.ContactDetails_FirstName, allowBlank: false, width: 250 }
                 , { xtype: 'email', ref: 'email', fieldLabel: eBook.Xbrl.ContactDetails_Email, allowBlank: true, width: 250 }

               , { xtype: 'fod.address', ref: 'address', fieldLabel: ' ', labelSeparator: '', title: eBook.Xbrl.ContactDetails_Address, width: 550 }
                 , { xtype: 'fod.phone', ref: 'phone', fieldLabel: ' ', labelSeparator: '', title: eBook.Xbrl.ContactDetails_Phone, width: 550, checkboxToggle: true }
//                 , { xtype: 'fod.phone', ref: 'fax', fieldLabel: ' ', labelSeparator: '', title: 'Fax', width: 550, checkboxToggle: true }

            ]
            , autoScroll: true
            , bodyStyle: 'padding:10px;'
        });
        eBook.Xbrl.ContactDetails.superclass.initComponent.apply(this, arguments);
        this.on('activate', this.onActivate, this);
    }
    , onActivate: function() {
        if (this.rendered) {
            this.phone.clearInvalid();
            
        }
    }
    , isValid: function(prevMark) {
        var t = this.contacttype.isValid(prevMark)
                || this.func.isValid(prevMark)
                || this.name.isValid(prevMark)
                || this.firstname.isValid(prevMark)
                || this.email.isValid(prevMark)
                || this.address.isValid(prevMark)
                || this.phone.isValid(prevMark);
        return this.contacttype.isValid(prevMark)
                && this.func.isValid(prevMark)
                && this.name.isValid(prevMark)
                && this.firstname.isValid(prevMark)
                && this.email.isValid(prevMark)
                && this.address.isValid(prevMark)
                && this.phone.isValid(prevMark);
    }
    , loadContract: function(contract) {
        if (contract) {
            this.contacttype.setValue(contract.ct ? contract.ct : '');
            this.func.setValue(contract.f ? contract.f : '');
            this.name.setValue(contract.n ? contract.n : '');
            this.firstname.setValue(contract.fn ? contract.fn : '');
            this.address.setValue(contract.a ? contract.a : null);
            this.phone.setValue(contract.p ? contract.p : null);
        }
    }
    , getContract: function() {
        return {
            ct: this.contacttype.getContract()
            , f: this.func.getValue()
            , n: this.name.getValue()
            , fn: this.firstname.getValue()
            , a: this.address.getValue()
            , p: this.phone.getValue()
        };
    }
});

Ext.reg('xbrl.contact', eBook.Xbrl.ContactDetails);

eBook.Xbrl.Wizard = Ext.extend(Ext.Panel, {
    data: {}
    , xbrlConfig: {}
    , initComponent: function() {
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , autoScroll: true
            , layoutconfig: { layoutOnCardChange: true, deferredRender: true }
            // , items: [{ xtype: 'Xbrl.ValidationCheckPanel'}]
           , items: [
                { xtype: 'Xbrl.ValidationCheckPanel', idx: 0, title: ' '} // Check file on errors and warnings. (when errors, no moving forward 'fix it' links
                , { xtype: 'xbrl.client', idx: 1, title: eBook.Xbrl.ClientDetails_Title, contractName: 'client' }
                , { xtype: 'xbrl.contact', idx: 2, title: eBook.Xbrl.ContactDetails_Title, contractName: 'contact' }
                 , { xtype: 'xbrl.attachments', title: eBook.Xbrl.AttachmentPanel_FichesTitle, idx: 3, contractName: 'fiches', attachments: []} // per fiche pdf aanduiden (behalve voor 275C & 204.3)
                 , { xtype: 'xbrl.attachments', title: eBook.Xbrl.Overview_AttachmentsTitle, idx: 4, contractName: 'divers', attachments: [] }
                 , { xtype: 'xbrl.attachments', title: eBook.Xbrl.AttachmentPanel_InfoTitle, idx: 5, contractName: 'info', attachments: eBook.Xbrl.InformativeAttachments }
                ]
            , bbar: [
                { xtype: 'button', ref: 'prev', hidden: true, text: eBook.Xbrl.Wizard_Previous, handler: this.onPrevious, scope: this, iconCls: 'eBook-back-24',
                    scale: 'medium',
                    iconAlign: 'top'
                }
                , '->'
                , { xtype: 'button', text: eBook.Xbrl.Wizard_Next, ref: 'nxt', handler: this.onNext, scope: this, iconCls: 'eBook-next-24',
                    scale: 'medium',
                    iconAlign: 'top'}]
        });
        eBook.Xbrl.Wizard.superclass.initComponent.apply(this, arguments);
    }
    , hideAllButtons: function() {
        this.getBottomToolbar().prev.hide();
        this.getBottomToolbar().nxt.hide();
    }
    , showNext: function() {
        this.getBottomToolbar().nxt.show();
    }
    , moveFirst: function() {
        this.layout.setActiveItem(0);
        this.hideAllButtons();
        this.getBottomToolbar().prev.hide();
        this.getBottomToolbar().nxt.hide();
        //var ai = this.items.items[0];
        if (this.layout.activeItem.performValidation) {
            this.layout.activeItem.performValidation();
        }
        this.layout.activeItem.doLayout();
        // this.doLayout();
    }
    , onNext: function(e) {
        this.getBottomToolbar().nxt.show();
        if (!this.data.result) this.data.result = {f:null};
        var ai = this.layout.activeItem;
        if (ai.getContract) {
            var c = ai.getContract();
            this.data[ai.contractName] = c;
        }
        if (!ai.isValid()) {
            if (ai.invalidMsg) {
                eBook.Interface.showError(ai.invalidMsg, ai.title);
            } else {
                eBook.Interface.showError("Niet alle velden zijn correct ingevuld", ai.title);
            }
            return;
        }

        var idx = ai.idx;
        if (idx > 0) this.refOwner.saveXbrl();
        if ((idx + 1) > 1) this.getBottomToolbar().prev.show();
        if (idx + 1 < this.items.items.length) {
            ai = this.items.items[idx + 1];
            ai.loadContract(this.data[ai.contractName]);
            this.layout.setActiveItem(idx + 1);
            this.layout.activeItem.doLayout();
            /*if (idx + 1 == 1) {
            var c = { "328K": { checked: true, items: [{ id: 'mypdf', name: 'PDFKE'}]} };
            this.layout.activeItem.loadContract(c);
            }*/
        } else if (idx + 1 == this.items.items.length) {
            this.refOwner.afterWizard();
        }
    }
    , onPrevious: function(e) {
        var idx = this.layout.activeItem.idx;
        if (idx - 1 >= 0) {
            this.layout.setActiveItem(idx - 1);
            if (idx - 1 == 0) this.getBottomToolbar().prev.hide();
        }
        this.layout.activeItem.doLayout();
    }
    , getContract: function() {
        return this.data;
    }
    , setContract: function(data) {
        this.data = data;
    }
    , onPrintClick: function() {
        // create a pdf bundle containing all selected info
    }
    , onSendToFOD: function() {
        // manager/partner only, send XBRL to central admin
    }
    , onReCreate: function() {
        // traverse wizard after an xbrl was already build
    }
    , loadXbrlConfig: function() {
        // load the configuration for the xbrl of the assessmentyear to which this file belongs.
    }
    , loadXbrlData: function() {
        // load existing or non-existing Xbrl Data
        // if existing, start in overview
        // if non-existing, start wizard.
    }
});

Ext.reg('Xbrl.Wizard', eBook.Xbrl.Wizard);eBook.Xbrl.Overview_ResultTitle = 'Resultaat';
eBook.Xbrl.Overview = Ext.extend(Ext.Panel, {
    data: {}
    , initComponent: function() {
        this.dtTpl = new Ext.XTemplate('<tpl for=".">'
                                            , '<div class="eBook-Xbrl-Overview">'

                                                    , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Status eBook-Xbrl-Overview-Status-Color-{[values.status.id]}">'
                                                        , '<tpl for="status">'
                                                            , ' ' + eBook.Xbrl.Overview_Status + ': <span class="eBook-Xbrl-Overview-StatusText">{txt}</span>'
                                                            , ' <br/>' + eBook.Xbrl.Overview_Version + ': <span class="eBook-Xbrl-Overview-Version">{parent.version}</span>'
                                                            , '<br/><input class="eBook-Xbrl-Overview-partner" type="checkbox" value="1"<tpl if="parent.validated"> checked="true"</tpl><tpl if="id==2"> disabled="true"</tpl>/> ' + eBook.Xbrl.Overview_ValidatedPartner
                                                        , '</tpl>'
                                                        , '<div class="eBook-Xbrl-Overview-History-Title">' + eBook.Xbrl.Overview_History + '</div>'
                                                        , '<div class="eBook-Xbrl-Overview-History" style="display:none"> '
                                                        , '<tpl for="history">'
                                                            , '<div class="eBook-Xbrl-Overview-History-item">'
                                                            , '{[fm.date(Ext.data.Types.WCFDATE.convert(values.dte),\'d/m/Y G:i:s\')]} - {atx} - {pfn}'
                                                            , '</div>'
                                                        , '</tpl>'
                                                        , '</div>'
                                                    , '</div>'
                                                    , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Result">'
                                                            , ' <b><u>' + eBook.Xbrl.Overview_ResultTitle + '</u></b><br/><br/>'
                                                            , '<tpl if="this.exists(values,\'result\')">'
                                                            , '<tpl for="result">'
                                                            , '<tpl if="this.exists(values,\'f\') && f!=null">'
                                                            , '<tpl for="f">'
                                                                , '<a href="{[this.getViewPath(values)]}" target="_blank">{file}</a><br/>'
                                                            , '</tpl>'
                                                            , '</tpl>'
                                                            , '</tpl>'
                                                            , '</tpl>'
                                                    , '</div>'

                                                 , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Client">'
                                                    , ' <b><u>' + eBook.Xbrl.Overview_ClientTitle + '</u></b>'
                                                    , '<tpl if="this.exists(values,\'client\')">'
                                                        , '<tpl for="client">'
                                                            , '<br/><b>{n}</b>&nbsp;&nbsp;&nbsp;{lti}'
                                                            , '<br/>' + eBook.Xbrl.Overview_EnterpriseNr + ': {enr}'
                                                            , '<tpl if="this.exists(values, \'foda\') && foda!=null"><tpl for="foda">'
                                                                , '<tpl if="this.exists(values, \'at\') && at!=null"><br/>{values.at.d}</tpl>'
                                                                , '<div class="eBook-Xbrl-Overview-Contact-Address">'
                                                                    , '{s} {n} <tpl if="this.exists(values, \'b\') && b!=\'\'">&nbsp;&nbsp;&nbsp;' + eBook.Xbrl.Overview_Box + ' {b}</tpl>'
                                                                    , '<br/><tpl if="this.exists(values, \'z\') && z!=null">{values.z.d} </tpl>{ci} <tpl if="this.exists(values, \'co\') && z!=null"><br/>{values.co.d} </tpl>'
                                                                , '</div>'
                                                            , '</tpl></tpl>'
                                                        , '</tpl>'
                                                    , '</tpl>'
                                                 , '</div>'
                                                , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Contact">'
                                                    , '<b><u>' + eBook.Xbrl.Overview_ContactTitle + '</u></b><br/>'
                                                    , '<tpl if="this.exists(values, \'contact\')">'
                                                        , '<tpl for="contact">'
                                                            , '<tpl if="this.exists(values, \'ct\') && ct!=null"><br/>{values.ct.d}:</tpl>'
                                                            , '<div class="eBook-Xbrl-Overview-Contact-Info"><b>{n}, {fn}</b>'
                                                                , '<tpl if="this.exists(values, \'f\') && f!=null"><br/><i>{f}</i></tpl>'

                                                                , '<tpl if="this.exists(values, \'a\') && a!=null"><tpl for="a">'
                                                                , '<tpl if="this.exists(values, \'at\') && at!=null"><br/>{values.at.d}</tpl>'
                                                                    , '<div class="eBook-Xbrl-Overview-Contact-Address">'
                                                                        , '{s} {n} <tpl if="this.exists(values, \'b\') && b!=\'\'">&nbsp;&nbsp;&nbsp;' + eBook.Xbrl.Overview_Box + ' {b}</tpl>'
                                                                        , '<br/><tpl if="this.exists(values, \'z\') && z!=null">{values.z.d} </tpl>{ci} <tpl if="this.exists(values, \'co\') && z!=null"><br/>{values.co.d} </tpl>'
                                                                    , '</div>'
                                                                , '</tpl></tpl>'
                                                                , '<tpl if="this.exists(values, \'p\') && p.act">'
                                                                    , '<tpl for="p"><br/>Tel:{cc} {z} {l} <tpl if="this.exists(values, \'e\' && e!=\'\')">' + eBook.Xbrl.Overview_Extension + ':{e}</tpl></tpl>'
                                                                , '</tpl>'
                                                            , '</div>'
                                                        , '</tpl>'
                                                    , '</tpl>'
                                                , '</div>'
                                                , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Calculation">'
                                                    , '<tpl if="this.exists(values, \'calculation\')"><tpl for="calculation">'
                                                        , '<b><u>' + eBook.Xbrl.Overview_CalculationTitle + '</u></b>'
                                                        , ' <br/>' + eBook.Xbrl.Overview_eBookCalculation + ':&nbsp; <span class="eBook-XbrlOverview-Calculated">{eBook}</span>'
                                                        , '<br/>' + eBook.Xbrl.Overview_BizTaxCalculation + ': <span class="eBook-XbrlOverview-Calculated"><tpl if="this.exists(values,\'biztax\') && biztax!=null">{biztax}</tpl><tpl if="!(this.exists(values,\'biztax\') && biztax!=null)">........</tpl></span>'
                                                     , '</tpl></tpl>'
                                                , '</div>'
                                                , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Fiches">'
                                                    , '<b><u>' + eBook.Xbrl.Overview_AutomatedFormsTitle + '</u></b>'
                                                    , '<tpl if="this.exists(values, \'fiches\')">'
                                                        , '<tpl for="fiches">'
                                                            , '<tpl if="checked && automated"><div class="eBook-Xbrl-Divers-item">{name}'
        //, '<tpl for="items"><div class="eBook-Xbrl-Overview-resource {iconCls}">{title}</div></tpl>'
                                                            , '</div></tpl>'
                                                    , '</tpl></tpl>'
                                                , '</div>'
                                                , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Divers">'
                                                    , '<b><u>' + eBook.Xbrl.Overview_AttachmentsTitle + ' </u></b>'
                                                    , '<tpl if="this.exists(values, \'divers\')">'
                                                        , '<tpl for="divers">'
                                                        , '<tpl if="checked"><div class="eBook-Xbrl-Overview-Divers-item">{name}'
                                                        , '<tpl for="items"><div class="eBook-Xbrl-Overview-resource {iconCls}">{title}</div></tpl>'
                                                        , '</div></tpl>'
                                                    , '</tpl>'
                                                        , '<div class="eBook-Xbrl-Overview-Divers-item eBook-Xbrl-Overview-FichesOther">'
                                                        , eBook.Xbrl.Overview_FichesAttachmentsTitle
                                                        , '<tpl if="this.exists(values, \'fiches\')">'
                                                        , '<tpl for="fiches">'
                                                            , '<tpl if="checked && !automated"><div class="eBook-Xbrl-Fiches-other">{name}'
                                                            , '<tpl for="items"><div class="eBook-Xbrl-Overview-resource {iconCls}">{title}</div></tpl>'
                                                            , '</div></tpl>'
                                                        , '</tpl>'
                                                        , '</tpl>'
                                                        , '</div>'
                                                     , '</tpl>'
                                                , '</div>'
                                            , '</div>'
                                        , '</tpl>'
                                        , {
                                            getViewPath: function(vls) {
                                                console.log('test' + vls);
                                                return 'viewFile.aspx?' + Ext.urlEncode(vls);
                                            }
                                        }
                                        );
        this.dtTpl.compile();
        this.data = this.ownerCt.data;
        if (!this.data.result) this.data.result = { f: null };
        Ext.apply(this, {
            html: this.dtTpl.apply(this.ownerCt.data)
            , autoScroll: true
            , tbar: [{
                text: eBook.Xbrl.Overview_Wizard,
                iconCls: 'eBook-wizard-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'wizard',
                handler: this.onWizard,
                scope: this
            }, {
                text: eBook.Xbrl.Overview_Bundle,
                iconCls: 'eBook-biztax-bundle-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'addpdf',
                handler: this.onBundle,
                scope: this
            }, {
                text: eBook.Xbrl.Overview_SendToBizTax,
                iconCls: 'eBook-biztax-send-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'biztax',
                handler: this.onSendToBizTax,
                scope: this
}]
            });
            eBook.Xbrl.Overview.superclass.initComponent.apply(this, arguments);
            this.on('activate', this.onActivate, this);
        }
    , onWizard: function() {
        this.ownerCt.layout.setActiveItem(1);
        this.ownerCt.doLayout();
        this.ownerCt.layout.activeItem.moveFirst.defer(100, this.ownerCt.layout.activeItem);
    }

    , onSendToBizTax: function(btn, e, partner) {

        if (!eBook.User.isActiveClientRolesFullAccess()) {
            eBook.Interface.showError("Only manager or above can send to biztax", this.title);
            return;
        }
        if (this.data.status.id < 2 && this.data.status.id > -1) {
            if (eBook.User.department == 'TAX' && !partner) {
                var wn = new eBook.Xbrl.PartnerSelect({ width: 340, height: 200 });
                wn.show(this);
                return;
            } else {
                this.getEl().mask(eBook.Xbrl.Overview_GeneratingBundle, 'x-mask-loading');
                if (this.ownerCt.manual) {

                    Ext.Ajax.request({
                        url: eBook.Service.bizTax + 'SendToProcessManual'
                    , method: 'POST'
                    , params: Ext.encode({ cxdc: {
                        ManualId: this.ownerCt.manual.Id
                        , Xbrl: this.data
                        , Person: eBook.User.getActivePersonDataContract()
                        , Culture: this.ownerCt.manual.File.c
                        , Partner: partner
                    }
                    })
                    , callback: this.onSendToBizTaxResponse
                    , scope: this
                    });
                } else {
                    Ext.Ajax.request({
                        url: eBook.Service.bizTax + 'SendToProcess'
                        , method: 'POST'
                        , params: Ext.encode({ cxdc: {
                            FileId: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                            , Xbrl: this.data
                            , Person: eBook.User.getActivePersonDataContract()
                            , Culture: eBook.Interface.Culture
                            , Partner: partner
                        }
                        })
                        , callback: this.onSendToBizTaxResponse
                        , scope: this
                    });
                }
            }
        } else {
            alert("Can't declare. Current status of declaration [" + this.data.status.txt + "] does not allow it");
            return;
        }

    }
    , onSendToBizTaxResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                if (robj.SendToProcessResult) {
                    this.ownerCt.data = robj.SendToProcessResult;
                } else {
                    this.ownerCt.data = robj.SendToProcessManualResult;
                }
                if (!this.ownerCt.data.result) this.ownerCt.data.result = { f: null };
                this.reload();
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
    , afterRender: function() {
        eBook.Xbrl.Overview.superclass.afterRender.apply(this, arguments);
        this.setEvents();
        this.handleValidated();
    }
    , clearEvents: function() {
        if (this.chkbox) {
            this.chkbox.purgeAllListeners();
        }
    }
    , setEvents: function() {
        if (this.chkbox) { this.clearEvents(); }
        var el = this.getEl();
        el = Ext.get(el);
        this.chkbox = el.child('.eBook-Xbrl-Overview-partner');
        this.chkbox.on('click', this.onCheckClick, this);

        var histEl = el.child('.eBook-Xbrl-Overview-History-Title');
        histEl.on('click', this.onHistoryClick, this);
        var hist = this.getEl().child('.eBook-Xbrl-Overview-History');
        hist.setVisibilityMode(Ext.Element.DISPLAY);
    }
    , onCheckClick: function() {
        this.onCheckChange.defer(100, this);
    }
    , onHistoryClick: function() {
        var hist = this.getEl().child('.eBook-Xbrl-Overview-History');
        hist = Ext.get(hist);
        if (hist.isDisplayed()) {
            hist.hide();
        } else {
            hist.show();
        }
    }
    , onCheckChange: function() {
        this.data.validated = this.chkbox.dom.checked;
        this.ownerCt.data.validated = this.chkbox.dom.checked;
        this.handleValidated();
    }
    , handleValidated: function() {
        var tb = this.getTopToolbar();

        if (this.data.validated && this.data.status.id == 1) {
            if (eBook.User.isActiveClientRolesFullAccess()) {
                this.chkbox.dom.disabled = false;
                tb.biztax.enable();
            } else {
                this.chkbox.dom.disabled = true;
                tb.biztax.disable();
            }

        } else {
            tb.biztax.disable();
        }
        if (this.data.status.id > 1) {
            this.chkbox.dom.disabled = true;
            tb.wizard.disable();
        } else {
            tb.wizard.enable();
        }

        tb.doLayout();
    }
    , onActivate: function(pnl) {
        this.reload();
    }
    , objectToArray: function(obj) {
        var arr = [];
        Ext.iterate(obj, function(key, val, o) {
            arr[val.idx] = val;
        }, this);
        return arr;
    }
    , transposeData: function(data) {
        return data;
        //        var dta = {};
        //        Ext.apply(dta, data);
        //        var fiches = this.objectToArray(data.fiches);
        //        var divers = this.objectToArray(data.divers);
        //        dta.fiches = fiches;
        //        dta.divers = divers;
        //        return dta;
    }
    , reload: function() {
        this.data = this.transposeData(this.ownerCt.data);
        if (!this.data.result) this.data.result = { f: null };
        if (!this.ownerCt.data.result) this.ownerCt.data.result = { f: null };
        // console.log(this.data);
        if (this.el) {
            // var l = Ext.get(this.el);
            //l.purgeAllListeners();
            this.clearEvents();
            this.update("");
            this.update(this.dtTpl.apply(this.data));
            this.setEvents();
            this.handleValidated();
        }
    }
    , onBundle: function() {
        this.getEl().mask(eBook.Xbrl.Overview_GeneratingBundle, 'x-mask-loading');

        if (this.ownerCt.manual) {
            Ext.Ajax.request({
                url: eBook.Service.bizTax + 'GetManualOverviewBundle'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    ManualId: this.ownerCt.manual.Id //eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: this.ownerCt.manual.File.c
                }
                })
                , callback: this.onBundleResponse
                , scope: this
            });
        } else {
            Ext.Ajax.request({
                url: eBook.Service.bizTax + 'GetOverviewBundle'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    FileId: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: eBook.Interface.currentFile.get('Culture')
                }
                })
                , callback: this.onBundleResponse
                , scope: this
            });
        }
    }
    , onBundleResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                if (robj.GetOverviewBundleResult) {
                    window.open('renderedPdfs/' + robj.GetOverviewBundleResult);
                } else {
                    window.open('renderedPdfs/' + robj.GetManualOverviewBundleResult);
                }
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
    });

    Ext.reg('xbrl.overview', eBook.Xbrl.Overview);

eBook.Xbrl.BizTaxWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        this.data = { fid: eBook.Interface.currentFile.get('Id'), validated: false, contact: {}, calculation: { eBook: null, biztax: null }, status: { id: -1, txt: 'Aanmaak' }, version: 0 };
        Ext.apply(this, {
            title: 'BizTax'
            , layout: 'card'
            , activeItem: 0
            , items: [
                    { xtype: 'panel', ref: 'startpanel', plain: true, html: '' }
                    , { xtype: 'Xbrl.Wizard', ref: 'wizard' }
                    , { xtype: 'xbrl.overview', ref: 'overview'}]
        });
        eBook.Xbrl.BizTaxWindow.superclass.initComponent.apply(this, arguments);
    }
    , show: function() {
        eBook.Xbrl.BizTaxWindow.superclass.show.call(this);
        this.getEl().mask('loading');
        this.loadData()
    }
    , loadData: function() {
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'GetXbrl'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
                            , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onLoadDataCallback
                , scope: this
        });
        //  this.onLoadDataCallback.defer(1000, this);
    }
    , onLoadDataCallback: function(opts, success, resp) {
        this.getEl().unmask(true);
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.wizard.setContract(robj.GetXbrlResult);
                if (robj.GetXbrlResult.status.id == 0) {
                    this.layout.setActiveItem(1);
                    this.layout.activeItem.moveFirst();
                } else {
                    this.data = robj.GetXbrlResult;
                    this.layout.setActiveItem(2);
                }
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();

    }
    , saveXbrl: function() {
        this.data = this.wizard.getContract();
       // this.getEl().mask(eBook.Xbrl.BizTaxWindow_Generating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'SaveXbrl'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    FileId: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: eBook.Interface.currentFile.get('Culture')
                    , ClientId: eBook.Interface.currentClient.get('Id')
                }
                })
                , callback: Ext.emptyFn
                , scope: this
        });
    }
    , afterWizard: function() {
        this.data = this.wizard.getContract();
        if (!this.data.result) this.data.result = {f:null};
        this.getEl().mask(eBook.Xbrl.BizTaxWindow_Generating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'Generate'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    FileId: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: eBook.Interface.currentFile.get('Culture')
                    , ClientId: eBook.Interface.currentClient.get('Id')
                }
                })
                , callback: this.performAfterWizard
                , scope: this
        });
    }
    , performAfterWizard: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.data = robj.GenerateResult;
                this.layout.setActiveItem(2);

            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
});

Ext.reg('xbrl.biztax', eBook.Xbrl.BizTaxWindow);


eBook.Xbrl.ManualWizard = Ext.extend(Ext.Panel, {
    data: {}
    , xbrlConfig: {}
    , initComponent: function() {
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , autoScroll: true
            , layoutconfig: { layoutOnCardChange: true, deferredRender: true }
            // , items: [{ xtype: 'Xbrl.ValidationCheckPanel'}]
           , items: [
            // { xtype: 'Xbrl.ValidationCheckPanel', idx: 0, title: ' '} // Check file on errors and warnings. (when errors, no moving forward 'fix it' links
                {xtype: 'xbrl.client', idx: 0, title: eBook.Xbrl.ClientDetails_Title, contractName: 'client' }
                , { xtype: 'xbrl.contact', idx: 1, title: eBook.Xbrl.ContactDetails_Title, contractName: 'contact' }
                 , { xtype: 'xbrl.attachments',manual:true, title: eBook.Xbrl.AttachmentPanel_FichesTitle, idx: 2, contractName: 'fiches', attachments: []} // per fiche pdf aanduiden (behalve voor 275C & 204.3)
                 , { xtype: 'xbrl.attachments', manual: true, title: eBook.Xbrl.Overview_AttachmentsTitle, idx: 3, contractName: 'divers', attachments: [] }
                 , { xtype: 'xbrl.attachments', manual: true, title: eBook.Xbrl.AttachmentPanel_InfoTitle, idx: 4, contractName: 'info', attachments: eBook.Xbrl.InformativeAttachments }
                ]
            , bbar: [
                { xtype: 'button', ref: 'prev', hidden: true, text: eBook.Xbrl.ManualWizard_Previous, handler: this.onPrevious, scope: this, iconCls: 'eBook-back-24',
                    scale: 'medium',
                    iconAlign: 'top'
                }
                , '->'
                , { xtype: 'button', text: eBook.Xbrl.ManualWizard_Next, ref: 'nxt', handler: this.onNext, scope: this, iconCls: 'eBook-next-24',
                    scale: 'medium',
                    iconAlign: 'top'}]
        });
        eBook.Xbrl.ManualWizard.superclass.initComponent.apply(this, arguments);
    }
    , hideAllButtons: function() {
        this.getBottomToolbar().prev.hide();
        this.getBottomToolbar().nxt.hide();
    }
    , showNext: function() {
        this.getBottomToolbar().nxt.show();
    }
    , moveFirst: function() {
        this.layout.setActiveItem(0);
        this.hideAllButtons();
        this.getBottomToolbar().prev.hide();
        this.getBottomToolbar().nxt.hide();
        //var ai = this.items.items[0];
        if (this.layout.activeItem.performValidation) {
            this.layout.activeItem.performValidation();
        } else {
            ai = this.layout.activeItem
            if (this.data[ai.contractName]) { 
                ai.loadContract(this.data[ai.contractName]);
            }
            
            this.getBottomToolbar().nxt.show();
        }
        this.layout.activeItem.doLayout();
        // this.doLayout();
    }
    , onNext: function(e) {
        this.getBottomToolbar().nxt.show();

        var ai = this.layout.activeItem;
        if (ai.getContract) {
            var c = ai.getContract();
            this.data[ai.contractName] = c;
        }
        if (!ai.isValid()) {
            if (ai.invalidMsg) {
                eBook.Interface.showError(ai.invalidMsg, ai.title);
            } else {
                eBook.Interface.showError("Niet alle velden zijn correct ingevuld", ai.title);
            }
            return;
        }
        this.refOwner.saveXbrl();
        var idx = ai.idx;
        if ((idx + 1) > 1) this.getBottomToolbar().prev.show();
        if (idx + 1 < this.items.items.length) {
            ai = this.items.items[idx + 1];
            ai.loadContract(this.data[ai.contractName]);
            this.layout.setActiveItem(idx + 1);
            this.layout.activeItem.doLayout();
            /*if (idx + 1 == 1) {
            var c = { "328K": { checked: true, items: [{ id: 'mypdf', name: 'PDFKE'}]} };
            this.layout.activeItem.loadContract(c);
            }*/
        } else if (idx + 1 == this.items.items.length) {
            this.refOwner.afterWizard();
        }
    }
    , onPrevious: function(e) {
        var idx = this.layout.activeItem.idx;
        if (idx - 1 >= 0) {
            this.layout.setActiveItem(idx - 1);
            if (idx - 1 == 0) this.getBottomToolbar().prev.hide();
        }
        this.layout.activeItem.doLayout();
    }
    , getContract: function() {
        return this.data;
    }
    , setContract: function(data) {
        this.data = data;
    }
    , onPrintClick: function() {
        // create a pdf bundle containing all selected info
    }
    , onSendToFOD: function() {
        // manager/partner only, send XBRL to central admin
    }
    , onReCreate: function() {
        // traverse wizard after an xbrl was already build
    }
    , loadXbrlConfig: function() {
        // load the configuration for the xbrl of the assessmentyear to which this file belongs.
    }
    , loadXbrlData: function() {
        // load existing or non-existing Xbrl Data
        // if existing, start in overview
        // if non-existing, start wizard.
    }
});

Ext.reg('Xbrl.Wizard.Manual', eBook.Xbrl.ManualWizard);

eBook.Xbrl.BizTaxManualWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        this.data = { fid: eBook.EmptyGuid, validated: false, contact: {}, calculation: { eBook: null, biztax: null }, status: { id: -1, txt: 'Aanmaak' }, version: 0 };
        Ext.apply(this, {
            title: 'BizTax'
            , layout: 'card'
            , activeItem: 0
            , items: [
                    { xtype: 'panel', ref: 'startpanel', plain: true, html: '' }
                    , { xtype: 'Xbrl.Wizard.Manual', ref: 'wizard' }
                    , { xtype: 'xbrl.overview', ref: 'overview'}]
        });
        eBook.Xbrl.BizTaxManualWindow.superclass.initComponent.apply(this, arguments);
    }
    , show: function(manual) {
        this.manual = manual;
        this.culture = manual.File.c;
        
        eBook.Xbrl.BizTaxManualWindow.superclass.show.call(this);
        this.getEl().mask('loading');
        this.loadData()
    }
    , loadData: function() {
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'GetXbrlManual'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: this.manual.Id //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Culture: this.culture
                    , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onLoadDataCallback
                , scope: this
        });
        //  this.onLoadDataCallback.defer(1000, this);
    }
    , onLoadDataCallback: function(opts, success, resp) {
        this.getEl().unmask(true);
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.wizard.setContract(robj.GetXbrlManualResult);
                if (robj.GetXbrlManualResult.status.id == 0) {
                    this.layout.setActiveItem(1);
                    this.layout.activeItem.moveFirst();
                } else {
                    this.data = robj.GetXbrlManualResult;
                    this.layout.setActiveItem(2);
                }
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();

    }
    , saveXbrl: function() {
        this.data = this.wizard.getContract();
        // this.getEl().mask(eBook.Xbrl.BizTaxWindow_Generating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'SaveXbrlManual'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    ManualId: this.manual.Id
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: this.culture
                    , ClientId: eBook.Interface.currentClient.get('Id')
                }
                })
                , callback: Ext.emptyFn
                , scope: this
        });
    }
    , afterWizard: function() {
        this.data = this.wizard.getContract();
        if (!this.data.result) this.data.result = { f: null };
        this.getEl().mask(eBook.Xbrl.BizTaxManualWindow_Generating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'GenerateManual'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    ManualId: this.manual.Id
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: this.culture
                    , ClientId: eBook.Interface.currentClient.get('Id')
                }
                })
                , callback: this.performAfterWizard
                , scope: this
        });
    }
    , performAfterWizard: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.data = robj.GenerateManualResult;
                this.layout.setActiveItem(2);

            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
});

Ext.reg('xbrl.biztax.manual', eBook.Xbrl.BizTaxManualWindow);



eBook.Xbrl.PartnerSelect = Ext.extend(Ext.Window, {
    initComponent: function() {
        Ext.apply(this, {
            title: 'Select partner'
            ,modal:true
            , items: [{ xtype: 'TaxPartners', ref: 'partners', allowBlank: false, autoLoad: true}]
            ,tbar:[{
                text: eBook.Xbrl.Overview_SendToBizTax,
                iconCls: 'eBook-biztax-send-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'biztax',
                handler: this.onSendToBizTax,
                scope: this
            }]

        });
        eBook.Xbrl.PartnerSelect.superclass.initComponent.apply(this, arguments);
    }
    , show: function(caller) {
        this.caller = caller;
        eBook.Xbrl.PartnerSelect.superclass.show.call(this);
    }
    , onSendToBizTax: function(btn, e) {
        if (this.partners.isValid()) {
            this.caller.onSendToBizTax(btn, e, this.partners.getValue());
            this.close();
        } else {
            alert("Select a partner. If the required partner is not in the list, check PMT & eBook team(s) prior to declaration.");
        }
    }
});

Ext.reg('XbrlPartnerSelcet', eBook.Xbrl.PartnerSelect);eBook.Reports.EngagementAgreement.Criteria = Ext.extend(Ext.Panel, {
    initComponent: function () {

        this.isRecAdded = false;
        this.isEmptyTemplateAdded = false;
        Ext.apply(this, {
            loadMask: true,
            title: 'Criteria',
            hideBorders: true,
            layout: 'form',
            ref: 'criteriaGrid',

            items: [
                {
                    layout: 'column',
                    hideBorders: true,
                    ref: 'columnContainer',
                    items: [
                            {
                                columnWidth: .2,
                                layout: 'form',
                                ref: 'column1',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Start date (from)',
                                        format: 'd/m/Y',
                                        name: 'startDateFrom',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Start date (to)',
                                        format: 'd/m/Y',
                                        name: 'startDateTo',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Expiration within',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[30, '1 month'], [60, '2 month'], [90, '3 month'], [120, '4 month'], [150, '5 month'], [180, '6 month'], [356, '1 year'], [712, '2 year'], [1068, '3 year'], [1424, '4 year'], [1780, '5 year']]
                                        }),
                                        name: 'expiration',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: .2,
                                layout: 'form',
                                ref: 'column2',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'End date (from)',
                                        format: 'd/m/Y',
                                        name: 'endDateFrom',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'End date (to)',
                                        format: 'd/m/Y',
                                        name: 'endDateTo',
                                        //value: new Date(),
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Validated by GTH',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [['%', "All"], ['true', 'Yes'], ['false', 'No']]
                                        }),
                                        name: 'gth',
                                        value: '',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: .25,
                                layout: 'form',
                                ref: 'column3',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Client name',
                                        name: 'clientName',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        ref: '../cboStatusList',
                                        fieldLabel: 'Status',
                                        displayField: 'nl',
                                        valueField: 'id',
                                        mode: 'local',
                                        selectOnFocus: true,
                                        autoSelect: true,
                                        editable: false,
                                        forceSelection: true,
                                        triggerAction: 'all',
                                        store: new eBook.data.JsonStore({
                                            selectAction: 'GetStatusList'
                                            , criteriaParameter: 'csdc'
                                            , autoDestroy: true
                                            , serviceUrl: eBook.Service.repository
                                            , storeParams: {}
                                            , disabled: this.readOnly
                                            , fields: eBook.data.RecordTypes.GlobalListItem
                                            , autoLoad: true
                                            , baseParams: {
                                                id: "SENTSIGNEDCLIENT"
                                            }
                                            , sortInfo: {field: 'id', direction:'ASC'}
                                        }),
                                        name: 'status',
                                        listeners: {
                                            expand: function (d, e, i) {
                                                var store = d.store;
                                                //store.load();
                                                if (!this.isRecAdded) {
                                                    var defaultData = {
                                                        id: '64AFAD19-8407-4F9E-AF72-ED37468C4272',
                                                        nl: 'No engagement agreement',
                                                        fr: 'No engagement agreement',
                                                        en: 'No engagement agreement',
                                                        value: 'No engagement agreement'
                                                    };
                                                    var defaultData2 = {
                                                        id: null,//'00000000-0000-0000-0000-000000000000',
                                                        nl: "All",
                                                        fr: 'All',
                                                        en: 'All',
                                                        value: ''
                                                    };
                                                    var recId = 3; // provide unique id
                                                    var p = new store.recordType(defaultData, recId);
                                                    var p2 = new store.recordType(defaultData2, 5);
                                                    store.insert(store.getTotalCount(), p);
                                                    store.insert(0, p2);
                                                    this.isRecAdded = true;
                                                }
                                            }
                                        },
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        //group: 'EL',
                                        fieldLabel: 'Template',
                                        displayField: 'nl',
                                        valueField: 'id',
                                        autoSelect: true,
                                        editable: false,
                                        forceSelection: true,
                                        triggerAction: 'all',
                                        mode: 'local',
                                        store: new eBook.data.JsonStore({
                                            selectAction: 'GetList'
                                            , serviceUrl: eBook.Service.lists
                                            , autoLoad: true
                                            , autoDestroy: true
                                            , criteriaParameter: 'cldc'
                                           , baseParams: {
                                               c: "nl-BE",
                                               lik: "EngagementTemplate"
                                           }
                                            , fields: eBook.data.RecordTypes.GlobalListItem

                                        }),
                                        name: 'template'
                                        , listeners: {
                                            expand: function (d, e, i) {
                                                var store = d.store;
                                                //store.load();
                                                if (!this.isEmptyTemplateAdded) {
                                                    var defaultData2 = {
                                                        id: null,//'00000000-0000-0000-0000-000000000000',
                                                        nl: "All",
                                                        fr: 'All',
                                                        en: 'All',
                                                        value: ''
                                                    };
                                                    var p2 = new store.recordType(defaultData2, 5);
                                                    store.insert(0, p2);
                                                    this.isEmptyTemplateAdded = true;
                                                }
                                            }
                                        },
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: 0.15,
                                layout: 'form',
                                ref: 'column4',
                                cls: 'eBook-reports-column',
                                items: [
                                         {
                                             xtype: 'checkbox',
                                             fieldLabel: 'Show only most recent EA',
                                             checked: true,
                                             name: 'renewal'
                                         },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Client status',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['0', 'Active'], ['1', 'Inactive']]
                                        }),
                                        name: 'activeClient',
                                        value: '',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: .2,
                                layout: 'hbox',
                                layoutConfig: {
                                    align:'stretch',
                                    pack:'end',
                                    defaultMargins:{
                                        top:25,
                                        bottom:0,
                                        right:5,
                                        left:0
                                    }
                                },
                                height:95,
                                hideBorders: true,
                                cls: 'eBook-reports-button-column',
                                ref: 'columnButtonContainer',
                                items: [
                                    {
                                            xtype: 'button',
                                            text: 'Export',
                                            itemId: 'exportBtn',
                                            ref: '../../searchBtn',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-icon-excel-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            width: 100,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var searchFilterValues = btn.refOwner.getSearchValues(),
                                                        grid = btn.refOwner.refOwner.reportEAgrid,
                                                        sortState = grid.store.getSortState(),
                                                        cpeldc = {
                                                            Start: 0,
                                                            Limit: 100000,
                                                            sdt: searchFilterValues.startDateTo,
                                                            sdf: searchFilterValues.startDateFrom,
                                                            edt: searchFilterValues.endDateTo,
                                                            edf: searchFilterValues.endDateFrom,
                                                            sid: searchFilterValues.status,
                                                            tid: searchFilterValues.template,
                                                            cn: searchFilterValues.clientName,
                                                            ex: searchFilterValues.expiration,
                                                            ro: searchFilterValues.renewal,
                                                            gth: searchFilterValues.gth,
                                                            pid: eBook.User.personId,
                                                            ac: searchFilterValues.activeClient,
                                                            srt: sortState
                                                        };

                                                    if(sortState)
                                                    {
                                                        cpeldc.sf = sortState.field;
                                                        cpeldc.so = sortState.direction;
                                                    }

                                                    btn.ownerCt.getEl().mask("Generating excel");
                                                    Ext.Ajax.request({
                                                        url: '/EY.com.eBook.API.5/OutputService.svc/ExportClientEngagementLettersByPerson'
                                                        ,
                                                        method: 'POST'
                                                        ,
                                                        jsonData: {
                                                            cpeldc: cpeldc
                                                        }//{ cceldc: { sd: Ext.Date.format(startDate.getValue(), 'MS'), ed: Ext.Date.format(endDate.getValue(), 'MS'), stid: structureId, sid: statusCombo.getValue(), tid: templateCombo.getValue(), t: templateCombo.getRawValue()} }
                                                        ,
                                                        callback: function (options, success, response) {
                                                            btn.ownerCt.getEl().unmask();
                                                            if (success) {
                                                                var obj = Ext.decode(response.responseText);
                                                                var id = obj.ExportClientEngagementLettersByPersonResult.Id;
                                                                window.open("pickup/" + id + ".xlsx");
                                                                btn.ownerCt.ownerCt.ownerCt.getEl().unmask();
                                                            } else {
                                                                alert('Export failed. ');
                                                            }
                                                        }
                                                        ,
                                                        scope: this
                                                    });
                                                }
                                            },
                                            scope: this
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Search/Refresh',
                                            itemId: 'searchBtn',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-filter-ico-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            ref: '../../searchBtn',
                                            width: 100,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var me = this,
                                                        criteriaGrid = me.refOwner;

                                                    criteriaGrid.search();
                                                }
                                            },
                                            scope: this
                                        }
                                ]
                            }
                        ]
                }
            ]
            ,
            listeners: {
                afterRender: function (thisForm, options) {
                    var me = this;
                    me.keyNav = new Ext.KeyNav(me.el, {
                        //Pressing enter on any field will trigger the search
                        "enter" : function(e){

                            me.search();
                        },
                        scope : me
                    });
                }
            }
        });

        eBook.Reports.EngagementAgreement.Criteria.superclass.initComponent.apply(this, arguments);
    },
    search: function () {
        //retrieve filter settings
        var me = this,
            searchValues = me.getSearchValues();
        //utilize the storeload function in grid panel to reload data
        me.refOwner.reportEAgrid.storeLoad(searchValues);
    },
    exportToExcel: function (btn, e) {

    },
    getSearchValues: function () {
        var columnContainer = this.columnContainer,
            fields = columnContainer.findByType(Ext.form.Field),
            values = {};

        Ext.each(fields,function(fld){
            var val = fld.getValue();
            values[fld.name] = typeof(val) != 'undefined' && val != null && val != "" ? val : null;
        },this);

        return values;
    }
});
Ext.reg('reportEAcriteria', eBook.Reports.EngagementAgreement.Criteria);eBook.Reports.EngagementAgreement.Grid = Ext.extend(eBook.grid.PagedGridPanel, {

    initComponent: function() {
        Ext.apply(this, {
            title: 'Search Result',
            storeConfig: {
                selectAction: 'GetClientEngagementLettersByPerson',
                fields: eBook.data.RecordTypes.ClientEngagementLettersByPerson,
                idField: 'clientId',
                criteriaParameter: 'cpeldc',
                serviceUrl: eBook.Service.client
            },
            pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            },
            loadMask: true,
            columns: [{
                    header: 'Clientname',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'clientName'
                    //editor: 'headerfield'
                },
                {
                    header: 'Client GFIS',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'clientGFIS'
                    //editor: 'headerfield'
                },
                {
                    header: 'Partner',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'partner'
                    //editor: 'headerfield'
                },
                {
                    header: 'Manager',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'manager'
                    //editor: 'headerfield'
                },
                {
                    header: 'Office',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'office'
                },
                {
                    header: 'Date ELEA',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'dateELEA'
                },
                {
                    header: 'Start date',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'startDate',
                    renderer: function(value, e, f, d, s, q, i) { 
                        if (value != null) {
                            return value.format("d/m/Y");
                        }else{
                            return null;  
                        } 
                    }
                },
                {
                    header: 'End date',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'endDate',
                    renderer: function(value, e, f, d, s, q, i) { 
                        if (value != null) {
                            return value.format("d/m/Y");
                        }else{
                            return null;  
                        } 
                    }
                    //editor: 'headerfield'
                },
                {
                    header: 'Template',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'template',
                    renderer: function(value) {
                        if (value != ""){
                            var obj = JSON.parse(value) ;
                            return obj.nl;
                        }else{
                            return "";
                        }
                    }
                },
                {
                    header: 'Status',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'status'
                    //editor: 'headerfield'
                },
                /*{
                    header: 'Filename',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'fileName'
                    //editor: 'headerfield'
                },*/
                {
                    header: 'Validated by GTH',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'gth'
                    //editor: 'headerfield'
                }
                
                                  
            ] ,
            //store: new eBook.data.JsonStore(storeCfg),
            listeners:{
                rowdblclick : function( t, rowIndex, e ){
                    var data = t.store.getAt(rowIndex).data;

                    if(!Ext.isEmpty(data.itemId)) {
                        var wn = new eBook.Repository.FileDetailsWindow({
                            repositoryItemId: data.itemId,
                            readOnly: false,
                            readOnlyMeta: false,
                            clientId: data.clientId
                        });
                        wn.show(this.parentCaller);
                        wn.loadById();
                    }
                },
                afterrender: function(t){

                    var searchValues = t.ownerCt.criteriaGrid.getSearchValues();
                    this.storeLoad(searchValues);

                    /*
                    Ext.Ajax.request({
                    url: '/EY.com.eBook.API.5/ClientService.svc/GetClientEngagementLettersByPerson'
                    , method: 'POST'
                    , jsonData: { cpeldc: { sdt: v.startDateTo, sdf: v.startDateFrom, edt: v.endDateTo, edf: v.endDateFrom, sid: v.status, tid: v.template, cn: v.clientName, ex: v.expiration, ro: v.renewal, gth: v.gth, pid: eBook.User.personId}}//{ cceldc: { sd: Ext.Date.format(startDate.getValue(), 'MS'), ed: Ext.Date.format(endDate.getValue(), 'MS'), stid: structureId, sid: statusCombo.getValue(), tid: templateCombo.getValue(), t: templateCombo.getRawValue()} }
                    , callback: function (options, success, response) {
                        if (success) {
                            var obj = Ext.decode(response.responseText);
                            this.store.loadData(obj.GetClientEngagementLettersByPersonResult);
                            this.getEl().unmask();
                        }
                    }
                    , scope: this
                });*/
                }
            },

            storeLoad : function(searchFilterValues) {
                var me = this,
                    sortState = me.store.getSortState(),
                    params =  {
                        Start: 0,
                        Limit: 25,
                        sdt: searchFilterValues.startDateTo,
                        sdf: searchFilterValues.startDateFrom,
                        edt: searchFilterValues.endDateTo,
                        edf: searchFilterValues.endDateFrom,
                        sid: searchFilterValues.status,
                        tid: searchFilterValues.template,
                        cn: searchFilterValues.clientName,
                        ex: searchFilterValues.expiration,
                        ro: searchFilterValues.renewal,
                        gth: searchFilterValues.gth,
                        ac: searchFilterValues.activeClient,
                        pid: eBook.User.personId
                    };

                //retrieve grid sort
                if(sortState)
                {
                    params.sf = sortState.field;
                    params.so = sortState.direction;
                }

                //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                Ext.apply(this.store.baseParams,params);

                this.store.load(
                    {
                        params: params,
                        callback: this.onLoadSuccess,
                        scope: this
                    }
                )
            },

            onLoadSuccess: function(r,options,success)  {
                this.getEl().unmask();
                if(success) {
                }
                else
                {
                    this.getEl().mask("Loading of data failed");
                }
            }
        });

        eBook.Reports.EngagementAgreement.Grid.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('reportEAgrid', eBook.Reports.EngagementAgreement.Grid);

eBook.Reports.EngagementAgreement.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        Ext.apply(this, {
            title: 'Client - Engagement letter Report',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                { xtype: 'reportEAcriteria', height: 120, ref:'reportEAcriteria' },
                { xtype: 'reportEAgrid', flex: 1, cls: 'eBookAT-GTH-Persongrid', ref: 'reportEAgrid' }
            ]
        });
        eBook.Reports.EngagementAgreement.Window.superclass.initComponent.apply(this, arguments);
    }
});eBook.Reports.PowerOfAttorney.Criteria = Ext.extend(Ext.Panel, {
    initComponent: function () {
        var me = this;
        this.isRecAdded = false;
        this.isEmptyTemplateAdded = false;
        Ext.apply(this, {
            loadMask: true,
            title: 'Criteria',
            hideBorders: true,
            layout: 'form',
            ref: 'criteriaGrid',

            items:
            [
                {
                    layout: 'column',
                    hideBorders: true,
                    ref: 'columnContainer',
                    items: [
                            {
                                columnWidth: 0.24,
                                layout: 'form',
                                ref: 'column1',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Client name',
                                        name: 'clientName',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Status',
                                        displayField: 'Status',
                                        nullable:true,
                                        valueField: 'Id',
                                        mode: 'local',
                                        selectOnFocus: true,
                                        autoSelect: true,
                                        editable: false,
                                        forceSelection: true,
                                        triggerAction: 'all',
                                        store: new eBook.data.JsonStore({
                                            selectAction: 'GetCodaPoaStatusList',
                                            criteriaParameter: 'cibpdc',
                                            autoDestroy: true,
                                            serviceUrl: eBook.Service.client,
                                            baseParams: { Boolean: false, Person : eBook.User.getActivePersonDataContract()},
                                            disabled: this.readOnly,
                                            fields: eBook.data.RecordTypes.Statuses,
                                            autoLoad: true,
                                            sortInfo: {field: 'Rank', direction:'ASC'}
                                        }),
                                        listeners: {
                                            expand: function (d, e, i) {
                                                var store = d.store;
                                                if (!this.isRecAdded) {
                                                    var defaultData = {Id: null,
                                                        Rank: -1,
                                                        Status: "All"};
                                                    var p = new store.recordType(defaultData, -1);
                                                    store.insert(0, p);
                                                    this.isRecAdded = true;
                                                }
                                            }
                                        },
                                        name: 'status',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Status Modified',
                                        format: 'd/m/Y',
                                        name: 'statusModified',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Last Modified',
                                        format: 'd/m/Y',
                                        name: 'dateModified',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: 0.24,
                                layout: 'form',
                                ref: 'column2',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Bank',
                                        displayField: 'Name',
                                        nullable:true,
                                        valueField: 'Id',
                                        mode: 'local',
                                        selectOnFocus: true,
                                        autoSelect: true,
                                        typeAhead:true,
                                        forceSelection: true,
                                        triggerAction: 'all',
                                        store: new eBook.data.JsonStore({
                                            selectAction: 'GetBanks',
                                            criteriaParameter: 'cidc',
                                            autoDestroy: true,
                                            serviceUrl: eBook.Service.client,
                                            storeParams: {},
                                            disabled: this.readOnly,
                                            fields: eBook.data.RecordTypes.BankList,
                                            autoLoad: true
                                        }),
                                        listeners: {
                                            expand: function (d, e, i) {
                                                var store = d.store;
                                                if (!this.isRecAdded) {
                                                    var defaultData = {Id: null, Name: "All"};
                                                    var p = new store.recordType(defaultData, 0);
                                                    store.insert(0, p);
                                                    this.isRecAdded = true;
                                                }
                                            }
                                        },
                                        name: 'bank',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'BIC',
                                        name: 'bic',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Account',
                                        name: 'account',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: 0.24,
                                layout: 'form',
                                ref: 'column3',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'PMT Allow Coda',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['1', 'Yes'], ['0', 'No']]
                                        }),
                                        name: 'allowCoda',
                                        value: '',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Client Status ACR',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['1', 'Active'], ['0', 'Inactive']]
                                        }),
                                        name: 'activeClientACR',
                                        value: '',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Exact EY license',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['1', 'Yes'], ['0', 'No']]
                                        }),
                                        name: 'EOL_EYLicense',
                                        value: '',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Exact Dig Mailbox',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['1', 'Yes'], ['0', 'No']]
                                        }),
                                        name: 'EOL_DigMailbox',
                                        value: '',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: 0.28,
                                layout: 'hbox',
                                layoutConfig: {
                                    align:'stretch',
                                    pack:'end',
                                    defaultMargins:{
                                        top:50,
                                        bottom:0,
                                        right:5,
                                        left:0
                                    }
                                },
                                height:120,
                                hideBorders: true,
                                cls: 'eBook-reports-button-column',
                                ref: 'columnButtonContainer',
                                items: [
                                        {
                                            xtype: 'button',
                                            text: 'Show log',
                                            ref: '../../btnLog',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-icon-checklist-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            width: 100,
                                            disabled: true,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var logView = btn.refOwner.refOwner.reportPOAlog,
                                                        grid = btn.refOwner.refOwner.reportPOAgrid;

                                                    if(logView.collapsed === true) {
                                                        logView.show();
                                                        logView.expand(true);
                                                        btn.setText("Hide log");
                                                    }
                                                    else{
                                                        logView.hide();
                                                        logView.collapse(true);
                                                        btn.setText("Show log");
                                                    }
                                                }
                                            },
                                            scope: this
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Upload file',
                                            ref: '../../btnUpload',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-repository-add-ico-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            width: 100,
                                            disabled: true,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var grid = btn.refOwner.refOwner.reportPOAgrid,
                                                        record = grid.getActiveRecord();

                                                    //open upload window dialog
                                                    grid.uploadDocument(record);
                                                }
                                            },
                                            scope: this
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Export',
                                            itemId: 'exportBtn',
                                            ref: '../../exportBtn',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-icon-excel-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            width: 100,

                                            listeners: {
                                                click: function (btn, e) {
                                                    var searchFilterValues = btn.refOwner.getSearchValues(),
                                                        grid = btn.refOwner.refOwner.reportPOAgrid,
                                                        sortState = grid.store.getSortState(),
                                                        ccpdc = {
                                                            ba: searchFilterValues.account,
                                                            bic: searchFilterValues.bic,
                                                            lm: searchFilterValues.dateModified,
                                                            sid: searchFilterValues.status,
                                                            cn: searchFilterValues.clientName,
                                                            bid: searchFilterValues.bank,
                                                            pid: eBook.User.personId,
                                                            ar: eBook.User.activeRole,
                                                            ac: searchFilterValues.activeRole,
                                                            sm: searchFilterValues.statusModified
                                                        };

                                                    if(sortState)
                                                    {
                                                        ccpdc.sf = sortState.field;
                                                        ccpdc.so = sortState.direction;
                                                    }

                                                    btn.ownerCt.getEl().mask("Generating excel");
                                                    Ext.Ajax.request({
                                                        url: '/EY.com.eBook.API.5/OutputService.svc/ExportCodaPoa',
                                                        method: 'POST',
                                                        jsonData: {
                                                            ccpdc: ccpdc
                                                        },
                                                        callback: function (options, success, response) {
                                                            btn.ownerCt.getEl().unmask();
                                                            if (success) {
                                                                var obj = Ext.decode(response.responseText);
                                                                var id = obj.ExportCodaPoaResult.Id;
                                                                window.open("pickup/" + id + ".xlsx");
                                                                btn.ownerCt.ownerCt.ownerCt.getEl().unmask();
                                                            } else {
                                                                alert('Export failed. ');
                                                            }
                                                        },
                                                        scope: this
                                                    });

                                                }
                                            },
                                            scope: this
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Search/Refresh',
                                            itemId: 'searchBtn',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-filter-ico-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            ref: '../../searchBtn',
                                            height: 18,
                                            width: 100,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var me = this,
                                                        criteriaGrid = me.refOwner;

                                                    criteriaGrid.search();
                                                }
                                            },
                                            scope: this
                                        }
                                ]
                            }
                        ]
                }
            ],
            listeners: {
                afterRender: function (thisForm, options) {
                    var me = this;
                    me.keyNav = new Ext.KeyNav(me.el, {
                        //Pressing enter on any field will trigger the search
                        "enter" : function(e){
                            me.search();
                        },
                        scope : me
                    });
                    //Set default values
                    var columnContainer = me.columnContainer,
                        fields = columnContainer.findByType(Ext.form.Field),
                        values = {};

                    Ext.each(fields,function(fld){
                        var name = fld.getName(),
                            defaultValue = null;

                        switch(name)
                        {
                            case 'allowCoda':
                            case 'activeClientACR':
                                defaultValue = fld.getStore().getAt(1);
                                fld.setValue(defaultValue ? defaultValue.id : null);
                                break;
                        }
                    },me);
                }
            }
        });

        eBook.Reports.PowerOfAttorney.Criteria.superclass.initComponent.apply(this, arguments);
    },
    search: function () {
        var me = this,
            grid = me.refOwner.reportPOAgrid;

        //retrieve filter settings
        var searchValues = me.getSearchValues();
        //utilize the storeload function in grid panel to reload data
        grid.storeLoad(searchValues);
    },
    exportToExcel: function (btn, e) {

    },
    getSearchValues: function () {
        var columnContainer = this.columnContainer,
            fields = columnContainer.findByType(Ext.form.Field),
            values = {};

        Ext.each(fields,function(fld){
            var val = fld.getValue();
            values[fld.name] = typeof(val) != 'undefined' && val !== null && val !== "" ? val : null;
        },this);

        return values;
    },
    enableRowSpecificButtons: function(enable) {
        var me = this;

        if(enable) {
            me.btnUpload.enable();
            me.btnLog.enable();
        }
        else {
           me.btnUpload.disable();
            if(me.btnLog.getText() != "Hide log") {
                me.btnLog.disable();
            }
        }
    }
});

Ext.reg('reportPOAcriteria', eBook.Reports.PowerOfAttorney.Criteria);
eBook.Reports.PowerOfAttorney.Log = Ext.extend(Ext.Panel,{
    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetCodaPoaLog'
                , serviceUrl: eBook.Service.client
                , criteriaParameter: 'ccpldc'
                , fields: eBook.data.RecordTypes.Log
            }),
            tpl = new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="eBook-reports-logEntry">',
                    '<div id="personName" class="eBook-reports-header eBook-reports-icon eBook-icon-checklist-24">{personName}</div>',
                    '<table>',
                        '<tr>',
                            '<td class="eBook-reports-logEntry-details">',
                                '<div class="eBook-reports-header">Actions taken:</div>',
                                '<ul>',
                                    '<tpl if="[this.actionPerformed(actions,\'status\')] &gt; -1"=>',
                                        '<li id="status">{status}</li>',
                                    '</tpl>',
                                    '<tpl if="[this.actionPerformed(actions,\'file\')] &gt; -1">', //check if file was uploaded
                                        '<li id="documentUploaded">Document uploaded</li>',
                                    '</tpl>',
                                    '<tpl if="[this.actionPerformed(actions,\'allowed\')] &gt; -1">', //Action set through PMT job
                                        '<li id="codaAllowed">Coda allowed</li>',
                                    '</tpl>',
                                    '<tpl if="[this.actionPerformed(actions,\'disallowed\')] &gt; -1">', //Action set through PMT job
                                        '<li id="codaDisallowed">Coda disallowed</li>',
                                    '</tpl>',
                                '</ul>',
                            '</td>',
                        '</tr>',
                        '<tpl if="[this.actionPerformed(actions,\'comment\')] &gt; -1"=>',
                            '<tr>',
                                '<td class="eBook-reports-logEntry-details">',
                                '<div class="eBook-reports-header">Comment:</div>',
                                '<div class="eBook-reports-logEntry-comment">{comment}</div>',
                                '</td>',
                            '</tr>',
                        '</tpl>',
                        '<tr>',
                            '<td colspan="2" class="eBook-reports-logEntry-timestamp">',
                                //'<div><div id="currentStatus" style="display: inline-block;" class="eBook-reports-header">Current status:&nbsp;</div>{currentStatus)</div>',
                                '<div><div id="timestamp"  style="display: inline-block;" class="eBook-reports-header">Updated on:&nbsp;</div>{timestamp:date("d/m/Y H:i:s")}</div>',
                            '</td>',
                        '<tr>',
                    '</table>',
                '</div>',
                '</tpl>',{
                actionPerformed: function(actions,action) {
                    actions = actions.split(",");
                    return actions.indexOf(action);
            }}
            );

        Ext.apply(this, {
            title: 'Log entries',
            cls: 'eBook-reports-logEntries',
            width: 400,
            minWidth: 400,
            collapsible: true,
            collapsed: true,
            hidden: true,
            hideCollapseTool: true,
            animCollapse: false,
            //collapsedCls: 'eBook-reports-logEntries-collapsed', not working in Extjs 3 due to border layout?
            autoScroll: true,
            items: new Ext.DataView({
                itemId: 'dataviewLog',
                tpl: tpl,
                store: store,
                autoHeight:true,
                multiSelect: true,
                overClass:'x-view-over',
                itemSelector:'div.thumb-wrap',
                emptyText: 'No log entries to display',
                storeLoad: function (bankAccount) {
                    var me = this,
                        params = {
                            ba: bankAccount
                        };

                    me.getEl().mask("loading data");
                    //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                    Ext.apply(me.store.baseParams, params);

                    me.store.load({
                        params: params,
                        callback: me.onLoadSuccess,
                        scope: me
                    });
                },
                onLoadSuccess: function (r, options, success) {
                    var me = this;
                    console.log(r);
                    if (success) {
                        me.getEl().unmask();
                    }
                    else {
                        me.getEl().mask("Loading of data failed");
                    }
                }
            }),
            listeners: {
                beforeexpand: function (p) {
                    //use to selected record id in order to load the store
                    var me = this,
                        grid = p.refOwner.reportPOAgrid,
                        record = grid.getActiveRecord(),
                        bankAccount = record.get("bankAccount");

                    if(bankAccount) {
                        me.getComponent("dataviewLog").storeLoad(bankAccount);
                    }
                }
            }
        });

        eBook.Reports.PowerOfAttorney.Log.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('reportPOAlog', eBook.Reports.PowerOfAttorney.Log);eBook.Reports.PowerOfAttorney.Grid = Ext.extend(eBook.grid.PagedGridPanel, {
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            title: 'Search Result',
            id: 'poaGrid',
            //override getRowClass in order to add css to rows
            cls: 'eBook-reports',
            draggable: false,
            sortChangeEnabled: false,
            activeRecord: null,
            viewConfig: {
                getRowClass: function (record, index, rowParams, store) {
                    return 'eBook-reports-row';
                }
            },
            storeConfig: {
                selectAction: 'GetCodaPoa',
                fields: eBook.data.RecordTypes.CodaPoa,
                idField: 'bankAccount',
                criteriaParameter: 'ccpdc',
                serviceUrl: eBook.Service.client
            },
            pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            },
            loadMask: true,
            border: false,
            columns: [
                {
                    header: 'Clientname',
                    sortable: true,
                    width: 191,
                    dataIndex: 'clientName'
                },
                {
                    header: 'Client GFIS',
                    sortable: true,
                    width: 68,
                    dataIndex: 'clientGfis'
                },
                {
                    header: 'Bank Account',
                    sortable: true,
                    width: 109,
                    dataIndex: 'bankAccount'
                },
                {
                    sortable: true,
                    header: 'Bic',
                    width: 71,
                    dataIndex: 'bic'
                },
                {
                    header: 'Bank',
                    sortable: true,
                    width: 102,
                    dataIndex: 'bankName'
                },
                {
                    header: 'PMT Allow Coda',
                    sortable: true,
                    width: 88,
                    dataIndex: 'allowCoda',
                    renderer: me.setCheckImage
                },
                {
                    header: 'Active Client ACR',
                    sortable: false,
                    width: 97,
                    dataIndex: 'activeClientACR',
                    renderer: me.setCheckImage
                },
                {
                    header: 'Exact EY License',
                    sortable: true,
                    width: 94,
                    dataIndex: 'EOL_EYLicense',
                    renderer: me.setYesNoValue
                },
                {
                    header: 'Exact Dig Mailbox',
                    sortable: true,
                    width: 98,
                    dataIndex: 'EOL_DigMailbox',
                    renderer: me.setCheckImage
                },
                {
                    header: 'Exact Division',
                    sortable: true,
                    width: 81,
                    dataIndex: 'EOL_Division'
                },
                {
                    header: 'Status',
                    sortable: true,
                    width: 337,
                    columnCssClass: 'eBook-reports-poa-row-button x-btn-mc',
                    dataIndex: 'status',
                    renderer: function (value, e, f, d, s, q, i) {
                        return '<div class="eBook-grid-row-combo-ico" style="font-weight: bold; padding-left: 16px; background-position: 1px 1px; background-repeat: no-repeat; background-size: 14px 14px;">' + value + '</div>';
                    }
                },
                {
                    header: 'Status Modified',
                    sortable: true,
                    width: 110,
                    dataIndex: 'statusModified',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value) {
                            return value.format("d/m/Y H:i:s");
                        } else {
                            return null;
                        }
                    }
                },
                {
                    header: 'CODA Proxies',
                    sortable: true,
                    width: 123,
                    columnCssClass: 'eBook-reports-poa-row-button x-btn-mc',
                    dataIndex: 'repositoryItemId',
                    renderer: function (value, e, f, d, s, q, i) {
                        var text = "Open Document",
                            iconCls = "eBook-repository-add-ico-24";
                        if (Ext.isEmpty(value)) {
                            text = 'Upload Document';
                            iconCls = "eBook-repository-ico-16";
                        }
                        return '<div style="font-weight: bold; padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; background-size: 14px 14px;" class="' + iconCls + '" >' + text + '</div>';
                    }
                },
                {
                    header: 'Last Modified',
                    sortable: true,
                    width: 110,
                    dataIndex: 'lastModified',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value) {
                            return value.format("d/m/Y H:i:s");
                        } else {
                            return null;
                        }
                    }
                }
            ],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    'rowselect': {
                        fn: function (grid, rowIndex) {
                            var me = this,
                                record = me.getStore().getAt(rowIndex);
                            //Set the active record (needed because selection of row is lost)
                            me.setActiveRecord(record);
                            me.refOwner.criteriaGrid.enableRowSpecificButtons(true);
                            me.refreshVisibleCodaPoaLog();
                        },
                        scope: this
                    },
                    'rowdeselect': {
                        fn: function () {
                            var critGridView = me.refOwner.criteriaGrid;

                            critGridView.enableRowSpecificButtons(false);
                        },
                        scope: this
                    }
                }
            }),
            //store: new eBook.data.JsonStore(storeCfg),
            listeners: {
                cellclick: function (grid, rowIndex, columnIndex, e) {
                    var record = grid.getStore().getAt(rowIndex),  // Get the Record
                        fieldName = grid.getColumnModel().getDataIndex(columnIndex), // Get field name
                        fieldValue = record.get(fieldName),
                        clientId = record.get('clientId');

                    if (fieldName == 'repositoryItemId') {
                        //Power of Attorney column clicked

                        if (fieldValue) {
                            grid.openDocument(fieldValue, clientId);
                        }
                        else {
                            grid.uploadDocument(record);
                        }
                    }
                    else if (fieldName == 'status') {
                        //Status column clicked
                        var coords = e.getXY();

                        //Only office validators can change the status
                        if(eBook.User.activeRole == "CODA Admin" || eBook.User.activeRole == "Admin") {
                            grid.showStatusContextMenu(coords, record);
                        }
                        else
                        {
                            Ext.Msg.alert('CODA Admin','Only a CODA Admin may change the status.');
                        }
                    }
                },

                afterrender: function (t) {
                    var me = this,
                        searchValues = t.ownerCt.criteriaGrid.getSearchValues();

                    t.header.dom.onScroll = function(){t.getView().el.dom.scrollLeft = t.header.dom.scrollLeft;};

                    me.storeLoad(searchValues);
                },

                sortchange: function (t) {
                    var me = this;

                    if(me.sortChangeEnabled) {
                        var searchValues = t.ownerCt.criteriaGrid.getSearchValues();
                        me.storeLoad(searchValues);
                    }
                }
            },

            storeLoad: function (searchFilterValues) {
                var me = this,
                    sortState = me.store.getSortState(),
                    params = {
                        Start: 0,
                        Limit: 25,
                        ba: searchFilterValues.account,
                        bic: searchFilterValues.bic,
                        lm: searchFilterValues.dateModified,
                        sid: searchFilterValues.status,
                        cn: searchFilterValues.clientName,
                        bid: searchFilterValues.bank,
                        pid: eBook.User.personId,
                        ar: eBook.User.activeRole,
                        sm: searchFilterValues.statusModified,
                        ac: searchFilterValues.allowCoda,
                        li: searchFilterValues.EOL_EYLicense,
                        dm: searchFilterValues.EOL_DigMailbox,
                        aca: searchFilterValues.activeClientACR
                    };

                //retrieve grid sort
                if(sortState)
                {
                    params.sf = sortState.field;
                    params.so = sortState.direction;
                }

                //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                Ext.apply(me.store.baseParams, params);

                me.store.load(
                    {
                        params: params,
                        callback: this.onLoadSuccess,
                        scope: me
                    }
                );
            },

            onLoadSuccess: function (r, options, success) {
                var me = this;
                if (success) {
                    me.getEl().unmask();
                    me.sortChangeEnabled = true;
                }
                else {
                    me.getEl().mask("Loading of data failed");
                }
            },
            createGridButton: function (value, contentid, record) {
                new Ext.Button({
                    text: value, handler: function (btn, e) {
                        alert("record data=" + record.data);
                    }
                }).render(document.body, contentid);
            }
        });

        eBook.Reports.PowerOfAttorney.Grid.superclass.initComponent.apply(this, arguments);

        me.mon(me.store, {
            load: function () {
                //disable record specific buttons on store load
                me.refOwner.criteriaGrid.enableRowSpecificButtons(false);
            }
        });
    },
    setYesNoValue : function(value)
    {
        if(value === "true")
        {
            return "yes";
        }
        else if(value === "false")
        {
            return "no";
        }
    },
    setCheckImage: function(value)
    {
        var iconCls = "";

        if(value === "true")
        {
            iconCls = "eBook-tickcircle-16";
        }
        else if(value === "false")
        {
            iconCls = "eBook-grid-row-delete-ico";
        }

        return '<div style="background-position: 50% 50%; background-repeat: no-repeat; background-size: 12px 12px; height:20px;" class="' + iconCls + '"></div>';
    },
    setActiveRecord: function(value)
    {
        var me = this;

        me.activeRecord = value;
    },
    getActiveRecord: function()
    {
        var me = this;

        return me.activeRecord;
    },
    getStatusContextMenu: function () {
        var me = this;

        if (me.statusContextMenu)
        {
            return me.statusContextMenu;
        }
        else {
            me.statusContextMenu = new eBook.Reports.PowerOfAttorney.StatusContextMenu({});
        }

        return me.statusContextMenu;
    },
    showStatusContextMenu: function (coords, record) {
        var me = this,
            ctxMnu = me.getStatusContextMenu();

        ctxMnu.activeRecord = record;
        ctxMnu.readyCallback = {fn: me.showCommentPrompt, scope: me};
        ctxMnu.showAt(coords);
    },
    openDocument: function (repositoryItemId, clientId) {
        var wn = new eBook.Repository.FileDetailsWindow({
            repositoryItemId: repositoryItemId,
            readOnly: false,
            readOnlyMeta: false,
            clientId: clientId
        });

        wn.show();
        wn.loadById();
    },
    //open upload window
    uploadDocument: function (record) {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
        wn = new eBook.Repository.SingleUploadWindow({
            clientId: record.get('clientId'),
            standards: {location: 'e611d384-4488-4371-aa2d-22224667aa8e'},
            readycallback: {fn: me.showCommentPrompt, scope: me, record: record},
            existingItemId: record.get('repositoryItemId')
        });

        wn.show(me);
    },
    //coda poa update after successful document upload
    onUploadDocumentSuccess: function (record, repositoryItemId, statusId, status) {
        //Although all three are optional, at least one must be filled in
        if (!repositoryItemId) {
            Ext.Msg.alert('Document upload', 'update of document failed');
        }

        var me = this;
        me.updateCodaPoa(record, statusId, status, repositoryItemId);
    },
    deleteDocument: function(repositoryItemId){
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'DeleteFile',
                callback: Ext.Msg.alert('file removed','File was removed from repository.'),
                scope: this,
                params: Ext.encode({ cidc: { Id: repositoryItemId} })
            });
    },
    showCommentPrompt: function (record, repositoryItemId, statusId, status) {
        var me = this,
            buttons = {yes: "Yes", no: "No", cancel: "Cancel"},
            data = {record: record, repositoryItemId: repositoryItemId, statusId: statusId, status: status};

        if(repositoryItemId)
        {
            buttons.cancel += " & delete file";
        }

        Ext.Msg.show({
            title: 'Add a comment?',
            multiline: 75,
            buttons: buttons,
            data: data,
            fn: function (buttonId, text, opt) {
                var comment = null;
                switch (buttonId) {
                    case 'cancel':
                        //user cancels update
                        if(data.repositoryItemId) {me.deleteDocument(data.repositoryItemId);}
                        break;
                    case 'yes':
                        //user adds comment
                        if (text.length === 0) {return me.showCommentPrompt(record, repositoryItemId, statusId, status);}
                        comment = text;
                    default:
                        //default action is a coda poa record update
                        me.updateCodaPoa(data.record, data.statusId, data.status, data.repositoryItemId, comment);
                }
            }
        });
    },
    //update coda poa record
    updateCodaPoa: function (record, statusId, status, repositoryItemId, comment) {
        //Although all three are optional, at least one must be filled in
        if (!statusId && !repositoryItemId && !comment) {return;}

        var me = this,
        //coda poa update object
            ucpdc = {
                Id: record.get('bankAccount'),
                Person: eBook.User.getActivePersonDataContract()
            };

        //set optional updates
        if (statusId) {ucpdc.Status = statusId;}
        if (repositoryItemId) {ucpdc.RepositoryItem = repositoryItemId;}
        if (comment) {ucpdc.Comment = comment;}

        //call
        Ext.Ajax.request({
            url: eBook.Service.client + '/UpdateCodaPoa',
            method: 'POST',
            jsonData: {
                ucpdc: ucpdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText && Ext.decode(response.responseText).UpdateCodaPoaResult === true){
                    me.onUpdateCodaPoaSuccess(record, statusId, status, repositoryItemId);
                } else {
                    Ext.Msg.alert('Update failed', 'Status was not updated.');
                }
            },
            scope: me
        });
    },
    //handle successful update of coda poa
    onUpdateCodaPoaSuccess: function (record, statusId, status, repositoryItemId) {
        var me = this,
            today = new Date();
        //update record with status change
        if (statusId) {
            record.set('statusId', statusId);
            record.set('status', status);
            record.set('statusModified', today);
            record.set('lastModified', today);
        }
        if (repositoryItemId) {
            record.set('repositoryItemId', repositoryItemId);
        }
        record.commit();
        me.refreshVisibleCodaPoaLog();
        me.setActiveRecord(null);
    },
    //If the log view is visible, update the log store
    refreshVisibleCodaPoaLog: function()
    {
        var me = this,
            logView = me.refOwner.reportPOAlog;

        if(logView && logView.collapsed === false) {
            var record = me.getActiveRecord(),
                bankAccount = record.get("bankAccount");

            //load log entries when log view is visible
            logView.getComponent("dataviewLog").storeLoad(bankAccount);

            return true;
        }

        return false;
    },
    //make sure contextmenu is destroyed when panel is destroyed
    destroy: function () {
        var me = this;

        if (me.statusContextMenu) {me.statusContextMenu.destroy();}
        eBook.Reports.PowerOfAttorney.Grid.superclass.destroy.call(this);
    }
});
Ext.reg('reportPOAgrid', eBook.Reports.PowerOfAttorney.Grid);


eBook.Reports.PowerOfAttorney.StatusContextMenu = Ext.extend(Ext.menu.Menu, {

    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetCodaPoaStatusList',
                criteriaParameter: 'cibpdc',
                autoDestroy: true,
                serviceUrl: eBook.Service.client,
                baseParams: { Boolean: true, Person : eBook.User.getActivePersonDataContract()},
                disabled: this.readOnly,
                fields: eBook.data.RecordTypes.Statuses,
                autoLoad: false
            });

        Ext.apply(me, {
            items: [{text: 'Loading data, please wait', statusId: null}],
            listeners: {
                itemclick: me.handleItemClick, scope: me
            }
        });

        eBook.Reports.PowerOfAttorney.StatusContextMenu.superclass.initComponent.apply(this, arguments);
        store.load({callback: me.loadMeItems, scope: me});


    },
    loadMeItems: function (recs, opts, success) {
        var me = this;
        me.removeAll();
        if(recs.length > 0) {
            Ext.each(recs, function (rec) {
                    me.add({text: rec.get('Status'), statusId: rec.get('Id')});
                },
                me);
        }
        else
        {
            me.destroy();
            Ext.Msg.alert("Status cannot be changed.","Role or status does not allow an update.");
        }
    },
    handleItemClick: function (baseItem) {
        var me = this,
            statusId = baseItem.statusId,
            status = baseItem.text;

        //Use parentCaller function to handle the item click
        if (!me.activeRecord || !me.readyCallback) {return;}

        if (statusId) {
            //trigger function defined in readyCallback property
            me.readyCallback.fn.call(me.readyCallback.scope, me.activeRecord, null, statusId, status);
        }
    }
});eBook.Reports.PowerOfAttorney.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        Ext.apply(this, {
            title: 'Client - CODA Proxies Report',
            layout: {
                type: 'border',
                align: 'stretch'
            },
            width: Ext.getBody().getViewSize().width - 100,
            height: Ext.getBody().getViewSize().height - 100,
            items: [
                {xtype: 'reportPOAcriteria', region: 'north', height: 145, ref:'reportPOAcriteria' },
                { xtype: 'reportPOAgrid', region:'center', cls: 'eBookAT-GTH-Persongrid', ref: 'reportPOAgrid' },
                { xtype: 'reportPOAlog', region: 'east', width: 300, ref: 'reportPOAlog' }
            ]
        });

        eBook.Reports.PowerOfAttorney.Window.superclass.initComponent.apply(this, arguments);
    }
});eBook.Reports.FileService.Criteria = Ext.extend(Ext.Panel, {
    initComponent: function () {
        var me = this;
        this.isRecAdded = false;
        this.isEmptyTemplateAdded = false;
        Ext.apply(this, {
            loadMask: true,
            title: 'Criteria',
            hideBorders: true,
            layout: 'form',
            ref: 'criteriaGrid',
            items: [
                {
                    layout: 'column',
                    hideBorders: true,
                    ref: 'columnContainer',
                    items: [
                        {
                            columnWidth: .2,
                            layout: 'form',
                            ref: 'column1',
                            cls: 'eBook-reports-column',
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Client name',
                                    name: 'clientName',
                                    anchor: '100%'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Office',
                                    name: 'office',
                                    anchor: '100%'
                                }
                            ]
                        },
                        {
                            columnWidth: .25,
                            layout: 'form',
                            ref: 'column2',
                            cls: 'eBook-reports-column',
                            items: [
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'End Date',
                                    format: 'd/m/Y',
                                    name: 'endDate',
                                    anchor: '100%'
                                },
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'Date Modified',
                                    format: 'd/m/Y',
                                    name: 'dateModified',
                                    anchor: '100%'
                                }
                            ]
                        },
                        {
                            columnWidth: .2,
                            layout: 'form',
                            ref: 'column3',
                            cls: 'eBook-reports-column',
                            items: [
                                {
                                    xtype: 'combo',
                                    fieldLabel: 'Status',
                                    displayField: 'Status',
                                    nullable: true,
                                    valueField: 'Id',
                                    mode: 'local',
                                    selectOnFocus: true,
                                    autoSelect: true,
                                    editable: false,
                                    forceSelection: true,
                                    triggerAction: 'all',
                                    store: new eBook.data.JsonStore({
                                        selectAction: 'GetServiceStatusesList'
                                        , criteriaParameter: 'cscdc'
                                        , autoDestroy: true
                                        , serviceUrl: eBook.Service.file
                                        , storeParams: {ServiceId: me.serviceId}
                                        , baseParams: {ServiceId: me.serviceId}
                                        , disabled: this.readOnly
                                        , fields: eBook.data.RecordTypes.Statuses
                                        , autoLoad: true
                                        , sortInfo: {field: 'Rank', direction:'ASC'}
                                    }),
                                    name: 'status',
                                    anchor: '100%',
                                    listWidth: 260,
                                    listeners: {
                                        expand: function (d, e, i) {
                                            var store = d.store;
                                            //store.load();
                                            if (!this.isRecAdded) {
                                                var defaultData = {Id: null, Status: "All", Rank: 0};
                                                var p = new store.recordType(defaultData, 0);
                                                store.insert(0, p);
                                                this.isRecAdded = true;
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'checkbox',
                                    fieldLabel: 'Show only most recent file',
                                    checked: true,
                                    name: 'newestFile'
                                }
                            ]
                        },
                        {
                            columnWidth: .35,
                            layout: 'hbox',
                            layoutConfig: {
                                align: 'stretch',
                                pack: 'end',
                                defaultMargins: {
                                    top: 5,
                                    bottom: 0,
                                    right: 5,
                                    left: 0
                                }
                            },
                            height: 95,
                            hideBorders: true,
                            cls: 'eBook-reports-button-column',
                            ref: 'columnButtonContainer',
                            items: [
                                {
                                    xtype: 'button',
                                    text: 'Show log',
                                    id: 'LogBtn',
                                    cls: 'eBook-reports-button',
                                    iconCls: 'eBook-icon-checklist-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    ref: '../../logBtn',
                                    width: 100,
                                    boxMaxHeight:50,
                                    disabled: true,
                                    listeners: {
                                        click: function (btn, e) {
                                            var logView = btn.refOwner.refOwner.reportFSlog,
                                                grid = btn.refOwner.refOwner.reportFSgrid;

                                            if (logView.collapsed == true) {
                                                logView.show();
                                                logView.expand(true);
                                                logView.collapsed = false;
                                                grid.refreshVisibleFileServiceLog();
                                                btn.setText("Hide log");
                                            }
                                            else {
                                                logView.hide();
                                                logView.collapse(true);
                                                btn.setText("Show log");
                                            }
                                        }
                                    },
                                    scope: this
                                },
                                {
                                    xtype: 'button',
                                    text: 'Export',
                                    itemId: 'exportBtn',
                                    ref: '../../exportBtn',
                                    cls: 'eBook-reports-button',
                                    iconCls: 'eBook-icon-excel-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    width: 100,
                                    boxMaxHeight:50,
                                    listeners: {
                                        click: function (btn, e) {
                                            var searchFilterValues = btn.refOwner.getSearchValues(),
                                                grid = btn.refOwner.refOwner.reportFSgrid,
                                                sortState = grid.store.getSortState(),
                                                ccpfsdc = {
                                                    sv: me.serviceId,
                                                    dp: me.department,
                                                    ed: searchFilterValues.endDate,
                                                    dm: searchFilterValues.dateModified,
                                                    sid: searchFilterValues.status,
                                                    cn: searchFilterValues.clientName,
                                                    of: searchFilterValues.office,
                                                    nf: searchFilterValues.newestFile,
                                                    pid: eBook.User.personId
                                                };

                                            if (sortState) {
                                                ccpfsdc.sf = sortState.field;
                                                ccpfsdc.so = sortState.direction;
                                            }

                                            btn.ownerCt.getEl().mask("Generating excel");
                                            Ext.Ajax.request({
                                                url: '/EY.com.eBook.API.5/OutputService.svc/ExportFileService'
                                                ,
                                                method: 'POST'
                                                ,
                                                jsonData: {
                                                    ccpfsdc: ccpfsdc
                                                },
                                                callback: function (options, success, response) {
                                                    btn.ownerCt.getEl().unmask();
                                                    if (success) {
                                                        var obj = Ext.decode(response.responseText);
                                                        var id = obj.ExportFileServiceResult.Id;
                                                        window.open("pickup/" + id + ".xlsx");
                                                        btn.ownerCt.ownerCt.ownerCt.getEl().unmask();
                                                    } else {
                                                        alert('Export failed. ');
                                                    }
                                                }
                                                ,
                                                scope: this
                                            });

                                        }
                                    },
                                    scope: this
                                },
                                {
                                    xtype: 'button',
                                    text: 'Search/Refresh',
                                    itemId: 'searchBtn',
                                    cls: 'eBook-reports-button',
                                    iconCls: 'eBook-filter-ico-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    ref: '../../searchBtn',
                                    boxMaxHeight:50,
                                    width: 100,
                                    listeners: {
                                        click: function (btn, e) {
                                            var me = this,
                                                criteriaGrid = me.refOwner;

                                            criteriaGrid.search();
                                        }
                                    },
                                    scope: this
                                }
                            ]
                        }
                    ]
                }
            ]
            ,
            listeners: {
                afterRender: function (thisForm, options) {
                    var me = this;
                    me.keyNav = new Ext.KeyNav(me.el, {
                        //Pressing enter on any field will trigger the search
                        "enter": function (e) {
                            me.search();
                        },
                        scope: me
                    });
                }
            }
        });

        eBook.Reports.FileService.Criteria.superclass.initComponent.apply(this, arguments);
    },
    search: function () {
        var me = this,
            grid = me.refOwner.reportFSgrid;

        //retrieve filter settings
        var searchValues = me.getSearchValues();
        //utilize the storeload function in grid panel to reload data
        grid.storeLoad(searchValues);
    },
    exportToExcel: function (btn, e) {

    },
    getSearchValues: function () {
        var columnContainer = this.columnContainer,
            fields = columnContainer.findByType(Ext.form.Field),
            values = {};

        Ext.each(fields, function (fld) {
            var val = fld.getValue();
            values[fld.name] = typeof(val) != 'undefined' && val != null && val != "" ? val : null;
        }, this);

        return values;
    },
    enableRowSpecificButtons: function (enable) {
        var btnLog = Ext.getCmp('LogBtn');

        if (enable) {
            btnLog.enable();
        }
        else {
            if(btnLog.getText() != "Hide log") {
                btnLog.disable();
            }
        }
    }
});

Ext.reg('reportFScriteria', eBook.Reports.FileService.Criteria);
eBook.Reports.FileService.Log = Ext.extend(Ext.Panel,{
    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetFileServiceLog'
                , serviceUrl: eBook.Service.client
                , criteriaParameter: 'ccpldc'
                , fields: eBook.data.RecordTypes.Log
            }),
            tpl = new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="eBook-reports-logEntry">',
                '<div id="personName" class="eBook-reports-header eBook-reports-icon eBook-icon-checklist-24">{personName}</div>',
                '<table>',
                '<tr>',
                '<td class="eBook-reports-logEntry-details">',
                '<div class="eBook-reports-header">Actions taken:</div>',
                '<ul>',
                '<tpl if="[this.actionPerformed(actions,\'status\')] &gt; -1"=>',
                '<li id="status">{status}</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'file\')] &gt; -1">', //check if file was uploaded
                '<li id="documentUploaded">Document uploaded</li>',
                '</tpl>',
                '</ul>',
                '</td>',
                '<tpl if="[this.actionPerformed(actions,\'comment\')] &gt; -1"=>',
                '<td class="eBook-reports-logEntry-comment">',
                '<div class="eBook-reports-header">Comment:</div>',
                '<div>{comment}</div>',
                '</td>',
                '</tpl>',
                '</tr>',
                '<tr>',
                '<td colspan="2" class="eBook-reports-logEntry-timestamp">',
                //'<div><div id="currentStatus" style="display: inline-block;" class="eBook-reports-header">Current status:&nbsp;</div>{currentStatus)</div>',
                '<div><div id="timestamp"  style="display: inline-block;" class="eBook-reports-header">Updated on:&nbsp;</div>{timestamp:date("d/m/Y H:i:s")}</div>',
                '</td>',
                '<tr>',
                '</table>',
                '</div>',
                '</tpl>',{
                    actionPerformed: function(actions,action) {
                        return actions.indexOf(action);
                    }}
            );

        Ext.apply(this, {
            title: 'Log entries',
            cls: 'eBook-reports-logEntries',
            width: 400,
            minWidth: 400,
            collapsible: true,
            collapsed: true,
            hidden: true,
            hideCollapseTool: true,
            animCollapse: false,
            //collapsedCls: 'eBook-reports-logEntries-collapsed', not working in Extjs 3 due to border layout?
            autoScroll: true,
            items: new Ext.DataView({
                itemId: 'dataviewLog',
                tpl: tpl,
                store: store,
                autoHeight:true,
                multiSelect: true,
                overClass:'x-view-over',
                itemSelector:'div.thumb-wrap',
                emptyText: 'No log entries to display',
                storeLoad: function (bankAccount) {
                    var me = this,
                        params = {
                            ba: bankAccount
                        };

                    me.getEl().mask("loading data");
                    //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                    Ext.apply(me.store.baseParams, params);

                    me.store.load({
                        params: params,
                        callback: me.onLoadSuccess,
                        scope: me
                    });
                },
                onLoadSuccess: function (r, options, success) {
                    var me = this;
                    console.log(r);
                    if (success) {
                        me.getEl().unmask();
                    }
                    else {
                        me.getEl().mask("Loading of data failed");
                    }
                }
            }),
            listeners: {
                beforeexpand: function (p) {
                    //use to selected record id in order to load the store
                    var me = this,
                        grid = p.refOwner.reportFSgrid,
                        record = grid.getActiveRecord(),
                        bankAccount = record.get("bankAccount");

                    if(bankAccount) {
                        me.getComponent("dataviewLog").storeLoad(bankAccount);
                    }
                }
            }
        });

        eBook.Reports.FileService.Log.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('reportFSlog', eBook.Reports.FileService.Log);eBook.Reports.FileService.Grid = Ext.extend(eBook.grid.PagedGridPanel, {
    initComponent: function () {
        var me = this,
            serviceId = null;

        Ext.apply(this, {
            title: me.title ? me.title : 'Search Result',
            //id: 'FSGrid',
            //override getRowClass in order to add css to rows
            cls: 'eBook-reports',
            draggable: false,
            sortChangeEnabled: false,
            activeRecord: null,
            viewConfig: {
                getRowClass: function (record, index, rowParams, store) {
                    return 'eBook-reports-row';
                }
            },
            storeConfig: {
                selectAction: 'GetFileServiceReport',
                fields: eBook.data.RecordTypes.ClientPersonFileServiceReport,
                idField: 'itemId',
                criteriaParameter: 'ccpfsdc',
                serviceUrl: eBook.Service.file
            },
            pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            },
            loadMask: true,
            border: false,
            columns: [
                {
                    header: 'Clientname',
                    sortable: true,
                    width: 200,
                    dataIndex: 'clientName'
                },
                {
                    header: 'Client GFIS',
                    sortable: true,
                    width: 80,
                    dataIndex: 'clientGFIS'
                },
                {
                    header: 'End Date',
                    sortable: true,
                    width: 120,
                    dataIndex: 'endDate',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value !== null) {
                            return value.format("d/m/Y");
                        } else {
                            return null;
                        }
                    }
                },
                {
                    header: 'Next Annual Meeting',
                    sortable: true,
                    width: 120,
                    dataIndex: 'nextAnnualMeeting',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value !== null) {
                            return value.format("d/m/Y");
                        } else {
                            return null;
                        }
                    }
                },
                {
                    header: 'Partner',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'partner'
                    //editor: 'headerfield'
                },
                {
                    header: 'Manager',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'manager'
                    //editor: 'headerfield'
                },
                {
                    header: 'Office',
                    sortable: true,
                    width: 120,
                    dataIndex: 'office'
                },
                {
                    header: 'Status',
                    sortable: true,
                    width: 300,
                    dataIndex: 'status',
                    columnCssClass: 'eBook-reports-poa-row-button x-btn-mc',
                    renderer: function (value, e, f, d, s, q, i) {
                        return '<div class="eBook-grid-row-combo-ico" style="font-weight: bold; padding-left: 16px; background-position: 1px 1px; background-repeat: no-repeat; background-size: 14px 14px;">' + value + '</div>';
                    }
                },
                {
                    header: 'Review Notes',
                    sortable: true,
                    width: 140,
                    columnCssClass: 'eBook-reports-YE-row-button x-btn-mc',
                    dataIndex: 'fileId',
                    renderer: function (value, e, f, d, s, q, i) {
                        var text = "Show Review Notes",
                            iconCls = "eBook-icon-checklist-24";
                        return '<div style="font-weight: bold; padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; background-size: 14px 14px;" class="' + iconCls + '" >' + text + '</div>';
                    }
                },
                {
                    header: 'Person',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'person'
                    //editor: 'headerfield'
                },
                {
                    header: 'Date Modified',
                    sortable: true,
                    width: 120,
                    dataIndex: 'dateModified',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value !== null) {
                            return value.format("d/m/Y H:i:s");
                        } else {
                            return null;
                        }
                    }
                },
                {
                    header: 'Days on Status',
                    sortable: true,
                    width: 120,
                    dataIndex: 'daysOnStatus'
                }
            ],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    'rowselect': {
                        fn: function (selectionModel, rowIndex) {
                            var grid = selectionModel.grid,
                                record = grid.getStore().getAt(rowIndex);
                            //Set the active record (needed because selection of row is lost)
                            grid.setActiveRecord(record);
                            if(grid.refOwner && grid.refOwner.criteriaGrid) {
                                grid.refOwner.criteriaGrid.enableRowSpecificButtons(true);
                            }
                            grid.refreshVisibleFileServiceLog();

                        },
                        scope: this
                    },
                    'rowdeselect': {
                        fn: function (selectionModel) {
                            var grid = selectionModel.grid;

                            if(grid.refOwner && grid.refOwner.criteriaGrid) {
                                grid.refOwner.criteriaGrid.enableRowSpecificButtons(false);
                            }
                            grid.setActiveRecord(null);
                        },
                        scope: this
                    }
                }
            }),
            //store: new eBook.data.JsonStore(storeCfg),
            listeners: {
                cellclick: function (grid, rowIndex, columnIndex, e) {
                    var record = grid.getStore().getAt(rowIndex),  // Get the Record
                        fieldName = grid.getColumnModel().getDataIndex(columnIndex), // Get field name
                        fieldValue = record.get(fieldName),
                        clientName = record.get('clientName'),
                        endDate = record.get('endDate');

                    if (fieldName == 'fileId') {
                        if (fieldValue) {
                            grid.showReviewNotes(false, fieldValue, grid.serviceId, clientName, endDate);
                        }
                    }
                    else if (fieldName == 'status') {
                        //Status column clicked
                        var coords = e.getXY();

                        //Only office validators can change the status
                        if(eBook.User.activeRole == "NBB Admin" || eBook.User.activeRole == "Admin") {
                            grid.showStatusContextMenu(coords, record, grid.serviceId);
                        }
                        else
                        {
                            Ext.Msg.alert('NBB Admin','Only a NBB Admin may change the status.');
                        }
                    }
                },
                afterrender: function (t) {
                    var me = this;

                    if(t.ownerCt && t.ownerCt.criteriaGrid) { //utilize the criteriagrid values if supplied
                        me.filterValues = t.ownerCt.criteriaGrid.getSearchValues();
                    }

                    me.storeLoad(me.filterValues);
                },
                sortchange: function (t) {
                    var me = this;

                    if(me.sortChangeEnabled) {
                        if(t.ownerCt && t.ownerCt.criteriaGrid) { //utilize the criteriagrid values if supplied
                            me.filterValues = t.ownerCt.criteriaGrid.getSearchValues();
                        }

                        me.storeLoad(me.filterValues);
                    }
                }
            },

            storeLoad: function (filterValues) {
                var me = this,
                    sortState = me.store.getSortState(),
                    params = {
                        Start: 0,
                        Limit: 25,
                        sv: me.serviceId ? me.serviceId : null,
                        dp: me.department ? me.department : null,
                        pid: eBook.User.personId
                    };

                me.setActiveRecord(null);

                if(filterValues)
                {
                    params.ed = filterValues.endDate ? filterValues.endDate : null;
                    params.dm = filterValues.dateModified ? filterValues.dateModified : null;
                    params.sid = filterValues.status ? filterValues.status.toString() : null; //array
                    params.cn = filterValues.clientName ? filterValues.clientName : null;
                    params.of = filterValues.office ? filterValues.office : null;
                    params.nf = filterValues.newestFile ? filterValues.newestFile : null;
                }

                //retrieve grid sort
                if(sortState)
                {
                    params.sf = sortState.field;
                    params.so = sortState.direction;
                }

                //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                Ext.apply(me.store.baseParams, params);

                me.store.load(
                    {
                        params: params,
                        callback: me.onLoadSuccess,
                        scope: me
                    }
                );
            },

            onLoadSuccess: function (r, options, success) {
                var me = this;
                if (success) {
                    me.getEl().unmask();
                    me.sortChangeEnabled = true;
                }
                else {
                    me.getEl().mask("Loading of data failed");
                }
            },
            createGridButton: function (value, contentid, record) {
                new Ext.Button({
                    text: value, handler: function (btn, e) {
                        alert("record data=" + record.data);
                    }
                }).render(document.body, contentid);
            }
        });

        eBook.Reports.FileService.Grid.superclass.initComponent.apply(this, arguments);

        me.mon(me.store, {
            load: function () {
                //disable record specific buttons on store load
                if(me.refOwner && me.refOwner.criteriaGrid) {
                    me.refOwner.criteriaGrid.enableRowSpecificButtons(false);
                }
            }
        });
    },
    setActiveRecord: function(value)
    {
        var me = this;
        me.activeRecord = value;
    },
    getActiveRecord: function()
    {
        var me = this;
        return me.activeRecord;
    },
    getStatusContextMenu: function (serviceId, status) {
        var me = this;

        me.statusContextMenu = new eBook.Reports.FileService.StatusContextMenu({serviceId : serviceId, status: status});
        return me.statusContextMenu;
    },
    showStatusContextMenu: function (coords, record, serviceId) {
        if(serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') {
            var me = this,
                ctxMnu = me.getStatusContextMenu(serviceId,record.status);

            ctxMnu.activeRecord = record;
            ctxMnu.readyCallback = {fn: me.updateFileService, scope: me};
            ctxMnu.showAt(coords);
        }
    },
    showReviewNotes: function (required, fileId, serviceId, clientName, endDate) {
        //This will open the review notes window
        var obj = {
                required: required,
                fileId: fileId,
                serviceId: serviceId,
                clientName: clientName,
                endDate: endDate,
                readOnly: true
            },
            wn = new eBook.Bundle.ReviewNotesWindow(obj);

        wn.show();
    },
    //If the log view is visible, update the log store
    refreshVisibleFileServiceLog: function()
    {
        var me = this,
            logView = me.refOwner.reportFSlog;

        if(logView && logView.collapsed === false) {
            var record = me.getActiveRecord();

            logView.getComponent("dataviewLog").storeLoad(record.get('fileId'), me.serviceId);
            return true;
        }

        return false;
    },
    updateFileService: function (record, statusId, status) {
        var me = this,
            ufsdc = { //file service update object
                FileId: record.get('fileId'),
                ServiceId: me.serviceId ? me.serviceId : null,
                Person: eBook.User.getActivePersonDataContract()
            };

        //add optionals
        if (statusId) {ufsdc.Status = statusId;}

        //call
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'UpdateFileService',
            method: 'POST',
            jsonData: {
                ufsdc: ufsdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText && Ext.decode(response.responseText).UpdateFileServiceResult) {
                    me.onUpdateFileServiceSuccess(record, statusId, status);
                } else {
                    eBook.Interface.showResponseError(response, me.title);
                    me.getEl().unmask();
                }
            },
            scope: me
        });
    },

    onUpdateFileServiceSuccess: function (record, statusId, status) {
        var me = this;
        //update record with status change
        if (statusId) {
            record.set('statusId', statusId);
            record.set('status', status);
        }
        record.commit();
        me.refreshVisibleFileServiceLog();
        me.setActiveRecord(null);
    }
});
Ext.reg('reportFSgrid', eBook.Reports.FileService.Grid);

eBook.Reports.FileService.StatusContextMenu = Ext.extend(Ext.menu.Menu, {

    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetUserSetStatusesList',
                criteriaParameter: 'cfspdc',
                autoDestroy: true,
                serviceUrl: eBook.Service.file,
                baseParams: { ServiceId: me.serviceId, Status: me.status, Person : eBook.User.getActivePersonDataContract()},
                disabled: this.readOnly,
                fields: eBook.data.RecordTypes.Statuses,
                autoLoad: false
            });

            //NbbFilingItems = [{text: 'Invoice uploaded by GTH', statusId: 11},{text: 'Invoice paid by Finance', statusId: 12},{text: 'Refiling requested by NBB Admin', statusId: 13}];

        Ext.apply(me, {
            items: [{text: 'Loading data, please wait', statusId: null}],
            listeners: {
                itemclick: me.handleItemClick, scope: me
            }
        });

        eBook.Reports.FileService.StatusContextMenu.superclass.initComponent.apply(this, arguments);
        store.load({callback: me.loadMeItems, scope: me});
    },
    loadMeItems: function (recs, opts, success) {
        var me = this;
        me.removeAll();
        if(recs.length > 0) {
            Ext.each(recs, function (rec) {
                    me.add({text: rec.get('Status'), statusId: rec.get('Id')});
                },
                me);
        }
        else
        {
            me.destroy();
            Ext.Msg.alert("Status cannot be changed.","Role or status does not allow an update.");
        }
    },
    handleItemClick: function (baseItem) {
        var me = this,
            statusId = baseItem.statusId,
            status = baseItem.text;

        //Use parentCaller function to handle the item click
        if (!me.activeRecord|| !me.readyCallback) {return;}

        if (statusId) {
            //trigger function defined in readyCallback property
            me.readyCallback.fn.call(me.readyCallback.scope, me.activeRecord, statusId, status);
        }
    }
});eBook.Reports.FileService.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            title: 'Client - Year End Flow',
            layout: {
                type: 'border',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'reportFScriteria',
                    region: 'north',
                    height: 100,
                    ref: 'reportFScriteria',
                    serviceId: me.serviceId,
                    department: me.department
                },
                {
                    xtype: 'reportFSgrid',
                    region: 'center',
                    cls: 'eBookAT-GTH-Persongrid',
                    ref: 'reportFSgrid',
                    serviceId: me.serviceId,
                    department: me.department
                },
                {
                    xtype: 'fileServicelog',
                    region: 'east',
                    width: 300,
                    ref: 'reportFSlog',
                    serviceId: me.serviceId,
                    department: me.department,
                    fileId: null,
                    collapsible: true,
                    collapsed: true,
                    hidden: true,
                    hideCollapseTool: true,
                    animCollapse: false
                    /*                    collapsible: false,
                     collapsed: false,
                     hidden: false,
                     hideCollapseTool: false,
                     animCollapse: true*/
                }
            ]
        });

        eBook.Reports.FileService.Window.superclass.initComponent.apply(this, arguments);
    }
});
eBook.Docstore.Window = Ext.extend(eBook.Window, {
    constructor: function (config) {
        this.config = config;
        Ext.apply(this, config || {}, {
            id: 'docStoreInterfaceWindow',
            modal: true,
            width: 400,
            layout: 'fit',
            title: 'Docstore',
            //html: '<iframe width="100%" height="100%" src="http://defranmceebo01/docstoreinterface/index.html?documentId=' + this.config.documentId + '&culture=' + this.config.culture + '&partnerGpn=' + this.config.partnerGpn + '"></iframe>'
            html: '<iframe width="100%" height="100%" src="' + eBook.DocStorePath + '?documentId=' + this.config.documentId + '&culture=' + this.config.culture + '&partnerGpn=' + this.config.partnerGpn + '&uploaderGpn=' + this.config.uploaderGpn + '"></iframe>'

        });
        eBook.Docstore.Window.superclass.constructor.call(this, config);
    }
});

