/*!
 * 
 */
/*!
 * 
 */
Ext.layout.eBookFormLayout = Ext.extend(Ext.layout.FormLayout, {
    fieldTpl: new Ext.XTemplate(
        '<div class="x-form-item {itemCls}" tabIndex="-1">',
            '<tpl if="!hideLabel"><label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label></tpl>',
            '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
            '</div><div class="{clearCls}"></div>',
        '</div>'
    )
    ,getTemplateArgs: function(field) {
        var noLabelSep = !field.fieldLabel || field.hideLabel;

        return {
            id            : field.id,
            label         : field.fieldLabel,
            itemCls       : (field.itemCls || this.container.itemCls || '') + (field.hideLabel ? ' x-hide-label' : ''),
            clearCls      : field.clearCls || 'x-form-clear-left',
            labelStyle    : this.getLabelStyle(field.labelStyle),
            elementStyle: this.elementStyle || '',
            hideLabel     : field.hideLabel,
            labelSeparator: noLabelSep ? '' : (Ext.isDefined(field.labelSeparator) ? field.labelSeparator : this.labelSeparator)
        };
    }
});
Ext.Container.LAYOUTS["ebookform"] = Ext.layout.eBookFormLayout;


Ext.layout.TableFormLayout = Ext.extend(Ext.layout.TableLayout, {
    monitorResize: true,
    labelAutoWidth: true,
    packFields: false,
    xtraLabelStyle: '',
    fieldTpl: new Ext.XTemplate(
        '<div class="x-form-item {itemCls}" tabIndex="-1">',
            '<tpl if="!hideLabel"><label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label></tpl>',
            '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
            '</div><div class="{clearCls}"></div>',
        '</div>'
    ),
    trackLabels: Ext.layout.FormLayout.prototype.trackLabels,
    setContainer: function(ct) {
        Ext.layout.FormLayout.prototype.setContainer.apply(this, arguments);
        var xtraLabelStyle = '';
        if (ct.labelAlign == 'top') {
            this.labelAutoWidth = false;
            if (this.fieldSpacing)
                this.elementStyle = 'padding-left: ' + this.fieldSpacing + 'px;';
        } else {
            if (this.labelAutoWidth)
                this.xtraLabelStylethis += 'width: auto;';
            if (this.packFields && !ct.labelWidth)
                ct.labelWidth = 1;
        }

        if (this.fieldSpacing)
            this.xtraLabelStyle += 'padding-left: ' + this.fieldSpacing + 'px;';
        this.currentRow = 0;
        this.currentColumn = 0;
        this.cells = [];
    },
    renderItem: function(c, position, target) {
        if (c && !c.rendered) {
            if (this.xtraLabelStyle != '') {
                c.labelStyle += this.xtraLabelStyle;
            }
            var cell = Ext.get(this.getNextCell(c));
            cell.addClass("x-table-layout-column-" + this.currentColumn);
            if (c.anchor)
                c.width = 1;
            Ext.layout.FormLayout.prototype.renderItem.call(this, c, 0, cell);
        }
    },
    getLayoutTargetSize: Ext.layout.AnchorLayout.prototype.getLayoutTargetSize,
    parseAnchorRE: Ext.layout.AnchorLayout.prototype.parseAnchorRE,
    parseAnchor: Ext.layout.AnchorLayout.prototype.parseAnchor,
    getTemplateArgs: Ext.layout.eBookFormLayout.prototype.getTemplateArgs,
    isValidParent: Ext.layout.FormLayout.prototype.isValidParent,
    onRemove: Ext.layout.FormLayout.prototype.onRemove,
    isHide: Ext.layout.FormLayout.prototype.isHide,
    onFieldShow: Ext.layout.FormLayout.prototype.onFieldShow,
    onFieldHide: Ext.layout.FormLayout.prototype.onFieldHide,
    adjustWidthAnchor: Ext.layout.FormLayout.prototype.adjustWidthAnchor,
    adjustHeightAnchor: Ext.layout.FormLayout.prototype.adjustHeightAnchor,
    getLabelStyle: Ext.layout.FormLayout.prototype.getLabelStyle,
    onLayout: function(ct, target) {
        Ext.layout.TableFormLayout.superclass.onLayout.call(this, ct, target);
        if (!target.hasClass("x-table-form-layout-ct")) {
            target.addClass("x-table-form-layout-ct");
        }
        var viewSize = this.getLayoutTargetSize();
        if (this.fieldSpacing)
            viewSize.width -= this.fieldSpacing;
        var aw, ah;
        if (ct.anchorSize) {
            if (Ext.isNumber(ct.anchorSize)) {
                aw = ct.anchorSize;
            } else {
                aw = ct.anchorSize.width;
                ah = ct.anchorSize.height;
            }
        } else {
            aw = ct.initialConfig.width;
            ah = ct.initialConfig.height;
        }
        var cs = this.getRenderedItems(ct), len = cs.length, i, j, c;
        var x, col, columnWidthsPx, w;
        // calculate label widths
        if (this.labelAutoWidth) {
            var labelWidths = new Array(this.columns);
            var pad = ct.labelPad || 5;
            for (i = 0; i < this.columns; i++)
                labelWidths[i] = ct.labelWidth || 0;
            // first pass: determine maximal label width for each column
            for (i = 0; i < len; i++) {
                c = cs[i];
                // get table cell
                x = c.getEl().parent(".x-table-layout-cell");
                // get column
                col = parseInt(x.dom.className.replace(/.*x\-table\-layout\-column\-([\d]+).*/, "$1"));
                // set the label width
                if (c.label && c.label.getWidth() > labelWidths[col])
                    labelWidths[col] = c.label.getWidth();
            }
            // second pass: set the label width
            for (i = 0; i < len; i++) {
                c = cs[i];
                // get table cell
                x = c.getEl().parent(".x-table-layout-cell");
                // get column
                col = parseInt(x.dom.className.replace(/.*x\-table\-layout\-column\-([\d]+).*/, "$1"));
                // get label
                if (c.label) {
                    // set the label width and the element padding
                    c.label.setWidth(labelWidths[col]);
                    c.getEl().parent(".x-form-element").setStyle('paddingLeft', (labelWidths[col] + pad - 3) + 'px');
                }
            }
        }
        if (!this.packFields) {
            var rest = viewSize.width;
            columnWidthsPx = new Array(this.columns);
            // Calculate the widths in pixels
            for (j = 0; j < this.columns; j++) {
                if (this.columnWidths)
                    columnWidthsPx[j] = Math.floor(viewSize.width * parseFloat(this.columnWidths[j]));
                else
                    columnWidthsPx[j] = Math.floor(viewSize.width / this.columns);
                rest -= columnWidthsPx[j];
            }
            // Correct the last column width, if necessary
            if (rest > 0)
                columnWidthsPx[this.columns - 1] += rest;
        }
        for (i = 0; i < len; i++) {
            c = cs[i];
            // get table cell
            x = c.getEl().parent(".x-table-layout-cell");
            if (!this.packFields) {
                // get column
                col = parseInt(x.dom.className.replace(/.*x\-table\-layout\-column\-([\d]+).*/, "$1"));
                // get cell width (based on column widths)
                for (j = col, w = 0; j < (col + (c.colspan || 1)); j++)
                    w += columnWidthsPx[j];
                // set table cell width
                x.setWidth(w);
            }
            // perform anchoring
            if (c.anchor) {
                var a, h, cw, ch;
                if (this.packFields)
                    w = x.getWidth();
                // get cell width (subtract padding for label) & height to be base width of anchored component
                this.labelAdjust = c.getEl().parent(".x-form-element").getPadding('l');
                if (this.labelAdjust && ct.labelAlign == 'top')
                    w -= this.labelAdjust;
                h = x.getHeight();
                a = c.anchorSpec;
                if (!a) {
                    var vs = c.anchor.split(" ");
                    c.anchorSpec = a = {
                        right: this.parseAnchor(vs[0], c.initialConfig.width, aw),
                        bottom: this.parseAnchor(vs[1], c.initialConfig.height, ah)
                    };
                }
                cw = a.right ? this.adjustWidthAnchor(a.right(w), c) : undefined;
                ch = a.bottom ? this.adjustHeightAnchor(a.bottom(h), c) : undefined;
                if (cw || ch) {
                    c.setSize(cw || undefined, ch || undefined);
                }
            }
        }
    }
});

Ext.Container.LAYOUTS["tableform"] = Ext.layout.TableFormLayout;// important strongly typed json serialization:
// "__type":"{datacontractName}:#{namespace}"

// Construct eBook Namespace
Ext.ns('eBook'
        , 'eBook.Repository'
        , 'eBook.Home'
        , 'eBook.File'
        , 'eBook.Health'
        , 'eBook.NewFile'
        , 'eBook.Fields'
        , 'eBook.data'
        , 'eBook.Language'
        , 'eBook.grid'
        , 'eBook.Interface'
        , 'eBook.InterfaceTrans'
        , 'eBook.Menu'
        , 'eBook.Service'
        , 'eBook.Recycle'
        , 'eBook.Message'
        , 'eBook.BusinessRelations'
        , 'eBook.Meta'
        , 'eBook.Client'
        , 'eBook.Create'
        , 'eBook.Create.Empty'
        , 'eBook.Create.Excel'
        , 'eBook.Create.ProAcc'
        , 'eBook.Create.Exact'
        , 'eBook.Accounts'
        , 'eBook.Accounts.Mappings'
        , 'eBook.Accounts.New'
        , 'eBook.Accounts.Adjustments'
        , 'eBook.Accounts.Manage'
        , 'eBook.Journal'
        , 'eBook.Journal.Booking'
        , 'eBook.Worksheet'
        , 'eBook.Worksheet.Rules'
        , 'eBook.Worksheets'
        , 'eBook.Worksheets.Fields'
        , 'eBook.Worksheets.Lists'
        , 'eBook.Document'
        , 'eBook.Document.Fields'
        , 'eBook.Bundle'
        , 'eBook.Bundle.Pdf'
        , 'eBook.Excel'
        , 'eBook.Exact'
        , 'eBook.ProAcc'
        , 'eBook.TabPanel'
        , 'eBook.List'
        , 'eBook.Statistics'
        , 'eBook.ExtraFiles'
        , 'eBook.Closing'
        , 'eBook.Pdf'
        , 'eBook.Help'
        , 'eBook.PMT'
        , 'eBook.Xbrl'
        , 'eBook.GTH'
        , 'eBook.BizTax'
        , 'eBook.BizTax.Declaration'
        , 'eBook.Yearend'
        , 'eBook.Reports.EngagementAgreement'
        , 'eBook.Reports.PowerOfAttorney'
        , 'eBook.Reports.FileService'
        , 'eBook.Docstore'
        , 'eBook.Forms.AnnualAccounts'
        );


Date.patterns = {
    ISO8601Long: "Y-m-d H:i:s",
    ISO8601Short: "Y-m-d",
    ShortDate: "n/j/Y",
    LongDate: "l, F d, Y",
    FullDateTime: "l d F Y, H:i:s",
    MonthDay: "F d",
    ShortTime: "g:i A",
    LongTime: "g:i:s A",
    SortableDateTime: "Y-m-d\\TH:i:s",
    UniversalSortableDateTime: "Y-m-d H:i:sO",
    YearMonth: "F, Y"
};




// Set ajax settings
Ext.lib.Ajax.defaultPostHeader = 'application/json';
Ext.Ajax.timeout = 1200000;

Ext.chart.Chart.CHART_URL = 'resources/charts.swf';

// set state provider.
eBook.state = new Ext.state.CookieProvider();
Ext.state.Manager.setProvider(eBook.state);

Ext.layout.FormLayout.prototype.trackLabels = true;

eBook.CurrentClient = null;
eBook.CurrentFile = null;

// add escape function to regex class
if ('function' !== typeof RegExp.escape) {
    RegExp.escape = function(s) {
        if ('string' !== typeof s) {
            return s;
        }
        return s.replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
    }; // eo function escape
}

// Guid generation, to move to serverside (more trustworthy)
eBook.GuidItem = function() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
};

eBook.NewGuid = function() {
    return (eBook.GuidItem() + eBook.GuidItem() + "-" + eBook.GuidItem() + "-" + eBook.GuidItem() + "-" + eBook.GuidItem() + "-" + eBook.GuidItem() + eBook.GuidItem() + eBook.GuidItem());
};

eBook.isGuid = function(val) {
    var reGuid = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/;
    return reGuid.test(val);
};

// disable / enable log

eBook.disableLog = function () {
    console.log('Disabled console logging, eBook.enableLog() to re-enable.');
    eBook.consoleLog = console.log;
    window['console']['log'] = function () { };
};

eBook.enableLog = function () {

    window['console']['log'] = eBook.consoleLog;
    console.log('Enabled console logging, eBook.disableLog() to disable.');
};

eBook.disableLog();

//console = { log: function() { } };

// Empty guid string representation
eBook.EmptyGuid = "00000000-0000-0000-0000-000000000000";

eBook.getMapping = function(map, tpe) {
    if (tpe == 'VALUE' || !Ext.isDefined(tpe)) {
        return new Function('data', 'var am = data.am; for (var i = 0; i < am.length; i++) { if (am[i].wt == "' + map + '" && am[i].mi) return am[i].mi.id; } return "";');
    } else if(tpe=='NAME'){
        return new Function('data', 'var am = data.am; for (var i = 0; i < am.length; i++) { if (am[i].wt == "' + map + '" && am[i].mi) return am[i].mi.n; } return "";');
    } else if(tpe=='LASTYEAR'){
        return new Function('data', 'var am = data.am; for (var i = 0; i < am.length; i++) { if (am[i].wt == "' + map + '") return am[i].mly; } return false;');
    }       
    else return new Function();
};

eBook.GetShared = function(fileid,file) {
    return 'file://'+eBook.Share+'/'+fileid+'/'+file;
};

// Extension of format providers
Ext.apply(Ext.util.Format, {
    intFormat: function(v) {
        var strV = "" + v;
        strV = strV.replace(",", ".");
        v = parseInt(strV);

        if (isNaN(v)) {
            return "";
        }
        return v;
    },
    decimalFormat: function(v, p) {
        if (!Ext.isNumber(p)) {
            p = 2;
        }
        var strV = "" + v;
        strV = strV.replace(",", ".");
        v = parseFloat(strV);

        if (isNaN(v)) {
            return "";
        }
        mp = Math.pow(10, p);
        v = (Math.round((v - 0) * mp)) / mp;
        v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
        v = String(v);
        var ps = v.split('.');
        var whole = ps[0];
        var sub = ps[1] ? ',' + ps[1] : '.00';
        var r = /(\d+)(\d{3})/;
        while (r.test(whole)) {
            whole = whole.replace(r, '$1' + '.' + '$2');
        }
        v = whole + sub;
        //if(v.charAt(0) == '-'){
        //    return '-$' + v.substr(1);
        //}
        //if(isNaN(v)) { return ""; }
        return v;
    },
    euroMoney: function(v) {
       
        var r = Ext.util.Format.decimalFormat(v);
        if (r != '') {
            return Ext.util.Format.decimalFormat(v) + "&euro;";
        } else {
            return '';
        }
    }
        ,
    euroMoneyJournal: function(v) {
        if (v == 0 || v == '0') return '';
        var r = Ext.util.Format.decimalFormat(v);
        if (r != '') {
            return Ext.util.Format.decimalFormat(v) + "&euro;";
        } else {
            return '';
        }
    }
});

Ext.grid.Column.types.date = Ext.grid.DateColumn;

eBook.removeDocStoreUploadWindow = function () {
    var window = Ext.getCmp('docStoreInterfaceWindow');
    window.close();
    eBook.Interface.center.clientMenu.repository.reload();
}

eBook.Splash = {
    textItem: Ext.get("eBook-start-loader-text")
    , maskItem: Ext.get("eBook-mask")
    , splashItem: Ext.get("eBook-startload")
    , curText : ''
    , setText: function(txt) {
        this.textItem.update(txt);
        this.curText=txt;
    }
    , hide: function() {
        this.maskItem.hide();
        this.splashItem.hide();
    }
    , show: function() {
        this.maskItem.show();
        this.splashItem.show();
    }
    , isVisible:function() {
        return this.splashItem.isVisible();
    }
};

eBook.Splash.maskItem.setVisibilityMode(Ext.Element.DISPLAY);
eBook.Splash.splashItem.setVisibilityMode(Ext.Element.DISPLAY);

String.prototype.rightPad = function(padString, length) {
    var str = this.toString();
    while (str.length < length)
        str = str + padString;
    return str;
}

eBook.CachedAjax = {
    FileCacheAjax: new Ext.data.Connection({ autoAbort: false, serializeForm: function(form) { return Ext.lib.Ajax.serializeForm(form); } })
    , Ajax: new Ext.data.Connection({ autoAbort: false, serializeForm: function(form) { return Ext.lib.Ajax.serializeForm(form); } })
    , request: function(o) {
        var orig = {};
        Ext.apply(orig, o);
        o.originalSettings = orig;
        o.success = null;
        o.failure = null;
        o.callback = this.callback;
        o.scope = this;
        this.Ajax.request(o);
    }
    , callback: function(opts, success, resp) {
        if (success) {
            this.relaySuccessResponse(opts, success, resp);
        } else {
            try {
                var o = Ext.decode(resp.responseText);
                if (o.Message) {
                    if (o.Message == "CACHE-MISS") {
                        var splash = null;
                        if (eBook.Splash.isVisible()) {
                            splash = eBook.Splash.curText;
                        }
                        eBook.Splash.setText("Preparing file...");
                        eBook.Splash.show();
                        this.FileCacheAjax.request({
                            url: eBook.Service.file + 'SetFileInCache'
                            , method: 'POST'
                            , params: Ext.encode({ cfdc: { FileId: o.FileId} })
                            , callback: this.CacheCallback
                            , scope: this
                            , originalSettings: opts.originalSettings
                            , splash: splash
                        });
                        return;
                    } else if (o.Message.indexOf('endpoint') > 0) {
                        if (!opts.retryCounter) opts.retryCounter = 0;
                        opts.retryCounter++;
                        if (opts.retryCounter < 6) {
                            eBook.Splash.setText("Service is restarting, please wait...");
                            eBook.Splash.show();
                            opts.originalSettings.retryCounter = opts.retryCounter;
                            this.request.defer(Math.max(2000 * opts.retryCounter, 8000), this, [opts.originalSettings]);
                            return;
                        } else {
                            this.relayFaultResponse(resp, opts);
                        }
                    }
                }
            } catch (e) { }
            this.relayFaultResponse(resp, opts);
        }
    }
    , CacheCallback: function(opts, success, reps) {
        eBook.Splash.hide();
        if (opts.splash != null) {
            eBook.Splash.setText(opts.splash);
            eBook.Splash.show();
        }
        if (success) {
            this.request(opts.originalSettings);
        } else {
            eBook.Interface.showResponseError(reps, 'FILE CACHE FAILED!!');
        }
    }
    , relaySuccessResponse: function(opts, success, resp) {
        eBook.Splash.hide();
        var origOpts = opts.originalSettings;
        if (origOpts.success) {
            origOpts.success.call(origOpts.scope, resp, origOpts);
        }
        if (origOpts.callback) {
            origOpts.callback.call(origOpts.scope, origOpts, true, resp);
        }
    }
    , relayFaultResponse: function(resp, opts) {
        var origOpts = opts.originalSettings;
        if (origOpts.failure) {
            origOpts.failure.call(origOpts.scope, resp, origOpts);
        }
        if (origOpts.callback) {
            origOpts.callback.call(origOpts.scope, origOpts, false, resp);
        }
    }
    , serializeForm: function(form) {
        return Ext.lib.Ajax.serializeForm(form);
    }
};

var mapRules = [];
function mappingCssCache() {
    var sc = document.styleSheets[6];
    try {
        var ssRules = sc.cssRules || sc.rules;
        for (var j = ssRules.length - 1; j >= 0; --j) {
            mapRules[ssRules[j].selectorText.toLowerCase()] = ssRules[j];
        }
    } catch (e) { }
}

eBook.getGuidPath = function(id) {
    var sid = id.replace(/-/g, "");
    path = "";
    for (var i = 0; i < sid.length; i = i + 2) {
        path += sid.substr(i, 2) + "/";
    }
    return path;
};

Ext.enumerables = ['hasOwnProperty', 'valueOf', 'isPrototypeOf', 'propertyIsEnumerable',
                       'toLocaleString', 'toString', 'constructor'];

Ext.clone = function(item,noEnum) {
    var type,
                i,
                j,
                k,
                clone,
                key;

    if (item === null || item === undefined) {
        return item;
    }

    // DOM nodes
    // TODO proxy this to Ext.Element.clone to handle automatic id attribute changing
    // recursively
    if (item.nodeType && item.cloneNode) {
        return item.cloneNode(true);
    }

    type = toString.call(item);

    // Date
    if (type === '[object Date]') {
        return new Date(item.getTime());
    }


    // Array
    if (type === '[object Array]') {
        i = item.length;

        clone = [];

        while (i--) {
            clone[i] = Ext.clone(item[i], noEnum);
        }
    }
    // Object
    else if (type === '[object Object]' && item.constructor === Object) {
        clone = {};

        for (key in item) {
            clone[key] = Ext.clone(item[key], noEnum);
        }

        if (Ext.enumerables && !noEnum) {
            for (j = Ext.enumerables.length; j--; ) {
                k = Ext.enumerables[j];
                clone[k] = item[k];
            }
        }
    }

    return clone || item;
};


// EXT.DESTROY OVERRIDE TO DESTROY ALL OBJECT MEMBERS AS WELL
/*
Ext.destroyComplete = function(arg, forceAll) {
   
        if (arg) {
            if (Ext.isArray(arg)) {
                Ext.destroy.apply(this, arg);
            } else if (typeof arg.destroy == 'function') {
                arg.destroy();
            } else if (arg.dom) {
                arg.remove();
            } else if (Ext.isObject(arg)) {
                var arr = [];
                for (var att in arg) {
                    arr.push(att);
                }
                Ext.destroyMembersArray(arg, arr, forceAll);
            }
        }
};

Ext.destroyMembersArray = function(o, a, forceAll) {
    for (var i = 0, len = a.length; i < len; i++) {
        if (forceAll) {
            Ext.destroyComplete(o[a[i]],forceAll);
        } else {
            Ext.destroy(o[a[i]]);
        }
        delete o[a[i]];
    }
};
*/

/**
* eBook.lazyLoad
*
* Based upon Ext.ux.LazyLoad by Jerry Sosa
*
*/

eBook.LazyLoad = function() {

    // -- Group: Private Variables -----------------------------------------------

    /*
    Object: d
    Shorthand reference to the browser's *document* object.
    */
    var d = document,

    /*
    Object: pending
    Pending request object, or null if no request is in progress.
    */
  pending = null,

    /*
    Array: queue
    Array of queued load requests.
    */
  queue = [];



    return {
        // -- Group: Public Methods ------------------------------------------------

        /*
        Method: load
        Loads the specified script(s) and runs the specified callback function
        when all scripts have been completely loaded.

    Parameters:
        urls     - URL or array of URLs of scripts to load
        callback - function to call when loading is complete
        obj      - (optional) object to pass to the callback function
        scope    - (optional) if true, *callback* will be executed in the scope
        of *obj* instead of receiving *obj* as an argument.
        */
        load: function(urls, callback, obj, scope) {
            var head = d.getElementsByTagName('body')[0],
          i, script;

            if (urls) {
                // Cast urls to an Array.
                urls = Ext.isArray(urls) ? urls : [urls];

                // Create a request object for each URL. If multiple URLs are specified,
                // the callback will only be executed after the last URL is loaded.
                for (i = 0; i < urls.length; ++i) {
                    queue.push({
                        'url': urls[i],
                        'callback': i === urls.length - 1 ? callback : null,
                        'obj': obj,
                        'scope': scope
                    });
                }
            }

            // If a previous load request is currently in progress, we'll wait our
            // turn. Otherwise, grab the first request object off the top of the
            // queue.
            if (pending) {
                return;
            } else {
                pending = queue.shift();
                if (!pending) return;
            }

            // Determine browser type and version for later use.


            // Load the script.
            script = d.createElement('script');
            script.src = pending.url;

            if (Ext.isIE) {
                // If this is IE, watch the last script's ready state.
                script.onreadystatechange = function() {
                    if (this.readyState === 'loaded' ||
              this.readyState === 'complete') {
                        eBook.LazyLoad.requestComplete();
                    }
                };
            } else { //if (Ext.isGecko || (Ext.isSafari && !Ext.isSafari2)) {
                // Firefox and Safari 3.0+ support the load/error events on script
                // nodes.
                script.onload = eBook.LazyLoad.requestComplete;
                script.onerror = eBook.LazyLoad.requestComplete;
            }

            head.appendChild(script);
            /*
            if (!Ext.isIE && !Ext.isGecko && !(Ext.isSafari && !Ext.isSafari2)) {
                // Try to use script node blocking to figure out when things have
                // loaded. This works well in Opera, but may or may not be reliable in
                // other browsers. It definitely doesn't work in Safari 2.x.
                script = d.createElement('script');
                script.appendChild(d.createTextNode('eBook.LazyLoad.requestComplete.defer(500,eBook.LazyLoad);'));
                head.appendChild(script);
            }
            */
        },

        /*
        Method: loadOnce
        Loads the specified script(s) only if they haven't already been loaded
        and runs the specified callback function when loading is complete. If all
        of the specified scripts have already been loaded, the callback function
        will not be executed unless the *force* parameter is set to true.

    Parameters:
        urls     - URL or array of URLs of scripts to load
        callback - function to call when loading is complete
        obj      - (optional) object to pass to the callback function
        scope    - (optional) if true, *callback* will be executed in the scope
        of *obj* instead of receiving *obj* as an argument
        force    - (optional) if true, *callback* will always be executed, even if
        all specified scripts have already been loaded
        */
        loadOnce: function(urls, callback, obj, scope, force) {
            var newUrls = [],
          scripts = d.getElementsByTagName('script'),
          i, j, loaded, url;

            urls = Ext.isArray(urls) ? urls : [urls];

            for (i = 0; i < urls.length; ++i) {
                loaded = false;
                url = urls[i];

                for (j = 0; j < scripts.length; ++j) {
                    if (url === scripts[j].src) {
                        loaded = true;
                        break;
                    }
                }

                if (!loaded) {
                    newUrls.push(url);
                }
            }

            if (newUrls.length > 0) {
                eBook.LazyLoad.load(newUrls, callback, obj, scope);
            } else if (force) {
                if (obj) {
                    if (scope) {
                        callback.call(scope,obj);
                    } else {
                        callback.call(window, obj);
                    }
                } else {
                    callback.call();
                }
            }
        },

        /*
        Method: requestComplete
        Handles callback execution and cleanup after a request is completed. This
        method should not be called manually.
        */
        requestComplete: function() {
            // Execute the callback.
            
            var pd = pending;
            pending = null;

            // Execute the next load request on the queue (if any).
            if (queue.length) {
                eBook.LazyLoad.load.defer(10, this);
            }
            if (pd.callback) {
                if (Ext.isDefined(pd.obj)) {
                    if (pd.scope) {
                        pd.callback.call(pd.scope, pd.obj);
                    } else {
                        pd.callback.call(window, pd.obj);
                    }
                } else {
                    if (pd.scope) {
                        pd.callback.call(pd.scope);
                    } else {
                        pd.callback.call(window);
                    }
                }
            }


        }
    };
} ();
eBook.Person = Ext.extend(Ext.util.Observable, {
    constructor: function (config) {
        this.windowsAccount = config.winAccount;
        //this.name = config.name;
        this.addEvents({
            "loggingin": true,
            "loggedin": true
        });
        eBook.Person.superclass.constructor.call(this, config);
    },
    windowsAccount: null,
    personId: 0,
    firstName: '',
    lastName: '',
    fullName: '',
    companyRole: '',
    department: '',
    email: '',
    lastLogon: null,
    roles: [],
    isChampion: false,
    isSysAdmin: false,
    offices: [],
    activeRole: '',
    activeClientRoles: '',
    gpn: '',
    login: function () {
        eBook.Splash.setText("Logging in...");
        if (this.windowsAccount != '' && this.windowsAccount != null) {

            this.fireEvent('loggingin', this);
            Ext.Ajax.request({
                url: eBook.Service.home + 'Login',
                method: 'POST',
                params: Ext.encode({ cwadc: { WindowsAccount: this.windowsAccount} }),
                success: this.onLoginSuccess,
                failure: this.onLoginFailure,
                scope: this
            });
        } else {
            return;
        }
    },
    getActivePersonDataContract: function () {
        var p = {
            id: this.personId,
            ar: this.activeRole,
            acr: this.activeClientRoles,
            nm: this.fullName
        };
        var ps = Ext.encode(p);
        return Ext.util.Base64.encode(ps);
    },
    onLoginSuccess: function (resp, opts) {
        var rObj = Ext.decode(resp.responseText);
        var personObj = rObj.LoginResult;
        Ext.apply(this, {
            personId: personObj.id,
            firstName: personObj.fn,
            lastName: personObj.ln,
            fullName: personObj.fn + ' ' + personObj.ln,
            companyRole: personObj.em,
            email: personObj.em,
            lastLogon: Ext.data.Types.WCFDATE.convert(personObj.ll),
            roles: personObj.rs,
            isChampion: personObj.ic,
            isSysAdmin: personObj.isa,
            offices: personObj.os,
            defaultOffice: personObj.dof,
            defaultOfficeObj: personObj.dof,
            department: personObj.dp,
            gpn: personObj.gpn
        });

        if (this.roles.length > 0) {
            for (var i = 0; i < this.roles.length; i++) {
                if ((this.roles[i].r).indexOf('eBookAT') == -1) {
                    this.setActiveRole(this.roles[i].r, true);
                }
                if(this.roles[i].lu == true)
                {
                    this.setActiveRole(this.roles[i].r, true);
                    break;
                }
            }
        }
        var chpstr = personObj.ec;
        var chps = personObj.ecs.split('/');
        if (chps.length > 2) {
            this.chpstr = chps[0] + '/' + chps[1] + '/..';
        }
        if (eBook.Interface.rendered) {
            Ext.get('et-local-champions').update(chpstr);

            Ext.get('eBook.User').update(this.fullName);
            var tmp = 'No office';
            if (this.defaultOffice != null) {tmp = this.defaultOffice.n;}
            Ext.get('eBook.Office').update(tmp);
            // var d = eBook.Language.Roles[this.activeRole] ? eBook.Language.Roles[this.activeRole] : this.activeRole;
            //Ext.get('eBook.Role').update(d);

            eBook.Interface.updateRoles(this.getComboRoles());
            this.fireEvent('loggedin', this);
        } else {
            eBook.Interface.render();
        }
    },
    getOfficeList: function () {
        var lst = [{ id: null, n: 'All'}].concat(this.offices);
        return lst;

    },
    onLoginFailure: function (resp, opts) {
        // message
        eBook.Interface.showResponseError(resp, this.title);

    },
    getComboRoles: function () {
        var rs = [];
        for (var i = 0; i < this.roles.length; i++) {
            if ((this.roles[i].r).indexOf('eBookAT') == -1) {
                var d = eBook.Language.Roles[this.roles[i].r] ? eBook.Language.Roles[this.roles[i].r] : this.roles[i].r;

                //rs[i] = [this.roles[i].r, d];
                rs.push([this.roles[i].r, d]);
            }
        }
        return rs;
    },
    setActiveRole: function (role, noreload) {
        if (!this.isExistingRole(role)) {return;}
        var prevRole = this.activeRole;
        this.activeRole = role;
        var d = eBook.Language.Roles[this.activeRole] ? eBook.Language.Roles[this.activeRole] : this.activeRole;
        if (eBook.Interface.rendered) {
            eBook.Interface.applyActiveRole(role, prevRole);
        }
    },
    isCurrentlyChampion: function () {
        var rexp = /(champion)/i;
        return rexp.test(eBook.User.activeRole);
    },
    isCurrentlyAdmin: function () {
        var rexp = /(ADMIN|developer)/i;
        return rexp.test(eBook.User.activeRole);
    },
    hasRoleLike: function (qry, not) {
        if (!Ext.isDefined(not)) not = false;
        qry = qry.toLowerCase();
        for (var i = 0; i < this.roles.length; i++) {
            if (this.roles[i].r.toLowerCase().indexOf(qry) > -1) {
                if (!not) {return true;}
            } else {
                if (not) {return true;}
            }
        }
        return false;
    },
    isExistingRole: function (role) {
        for (var i = 0; i < this.roles.length; i++) {
            if (this.roles[i].r == role) {return true;}
        }
        return false;
    },
    isActiveClientRolesFullAccess: function () {
        var rexpAdm = /((admin)|(developer)|(champion))/i;
        if (rexpAdm.test(this.activeRole)) {return true;}
        var rexpc = /((partner)|(director)|(manager)|(admin))/i;
        return rexpc.test(this.activeClientRoles);
    },
    checkCloser: function (sheetDepartment, isSharedClient) {
        var hasBasicRights = eBook.User.isActiveClientRolesFullAccess();
        if (!hasBasicRights) {return false;}

        //        if (isSharedClient && !eBook.User.isCurrentlyAdmin() && sheetDepartment != null) {

        //            return sheetDepartment == eBook.User.department;
        //        }
        return true;

    }
});