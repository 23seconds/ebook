/*!
 * 
 */
/*!
 * 
 */

eBook.Help = {
    WindowMgr: new Ext.WindowGroup()
    , showHelp: function(worksheetID) {

        var wn = eBook.Help.WindowMgr.get('et-help-window');
        if (wn) {
            eBook.Help.WindowMgr.bringToFront(wn);
        } else {

        wn = new eBook.Help.Window({ worksheetID: worksheetID });
            wn.worksheetID = worksheetID;
            wn.show();
        }


    }
    , showChampions: function() {
        var wn = eBook.Help.WindowMgr.get('et-help-champions-window');
        if (wn) {
            eBook.Help.WindowMgr.bringToFront(wn);
        } else {
            wn = new eBook.Help.ChampionsWindow({});
            wn.show();
        }
    }
};
eBook.Editor = Ext.extend(Ext.Editor, {
    onRender: function(ct, position) {
        this.el = new Ext.Layer({
            shadow: this.shadow,
            cls: "x-tree-editor",
            parentEl: ct,
            shim: this.shim,
            shadowOffset: this.shadowOffset || 4,
            id: this.id,
            constrain: this.constrain
        });
        if (this.zIndex) {
            this.el.setZIndex(this.zIndex);
        }
        this.el.setStyle("overflow", Ext.isGecko ? "auto" : "hidden");
        if (this.field.msgTarget != 'title') {
            this.field.msgTarget = 'qtip';
        }
        this.field.inEditor = true;
        this.mon(this.field, {
            scope: this,
            blur: this.onBlur,
            specialkey: this.onSpecialKey
        });
        if (this.field.grow) {
            this.mon(this.field, "autosize", this.el.sync, this.el, { delay: 1 });
        }
        this.field.render(this.el).show();
        this.field.getEl().dom.name = '';
        if (this.swallowKeys) {
            this.field.el.swallowEvent([
                    'keypress', // *** Opera
                    'keydown'   // *** all other browsers
                ]);
        }
    }
    , startEdit: function(el, value) {
        if (this.editing) {
            this.completeEdit();
        }
        this.boundEl = Ext.get(el);
        var v = value !== undefined ? value : this.boundEl.dom.innerHTML;
        if (!this.rendered) {
            this.render(this.parentEl || document.body);
        }
        if (this.fireEvent("beforestartedit", this, this.boundEl, v) !== false) {
            this.startValue = v;
            this.field.reset();
            this.field.setValue(v);
            this.show();
            this.realign(true);
            this.editing = true;

        }
    },
    realign: function(autoSize) {
        if (autoSize === true) {
            this.doAutoSize();
        }
        //this.el.alignTo(this.boundEl, this.alignment, this.offsets);
        this.el.setXY(this.boundEl.getXY());
    }
    , onClick: function() { }
    //, onBlur: function() { alert('blur'); }
});eBook.Util = {
    ConstructObjectFromRecord: function(rec) {
        var o = {};
        rec.fields.each(function(fld) {
            o[fld.mapping] = rec.get(fld.name);
        }, this);
        return o;
    }
};
eBook.Window = Ext.extend(Ext.Window, {
    actions: []
    , locked: false
    , constrain: true
    , failedActions: []
    , busyMsg: eBook.Window_Busy
    , isBusy: false
    , haltOnActionError: false
    , actionsShowWait: false
    , actionsAutoStart: true
    , waitWindowCfg: null
    , defaultWaitWindowCfg: {}
    , constructor: function (config) {
        config.closeAction = 'close';
        if (!config.width) config.width = eBook.Interface.getDefaultWidth();
        if (!config.height) config.height = eBook.Interface.getDefaultHeight();
        if (!config.iconCls) config.iconCls = 'eBook-Window-icon';
        Ext.apply(this, config || {}, {
            bbar: new Ext.ux.StatusBar({
                defaultText: 'Ready'
            })
            , cls: 'eBook-def-window'
            , shadow: false
            , maximizable: true
        });
        this.defaultWaitWindowCfg = {
            title: eBook.Window_Busy,
            msg: this.busyMsg
        };
        eBook.Window.superclass.constructor.call(this, config);
        this.on('beforeclose', this.onBeforeClose, this);
    }
    , mask: function (txt, cls) {
        this.getEl().mask(txt, cls);
    }
    , unmask: function () {
        this.getEl().unmask();
    }
    , clearStatus: function (o) {
        if (this.getBottomToolbar() && this.getBottomToolbar().clearStatus) {
            this.getBottomToolbar().clearStatus(o);
        }
    }
    , setStatus: function (msg) {
        this.clearStatus();
        this.getBottomToolbar().setStatus(msg);
    }
    , setStatusBusy: function (msg) {
        this.getBottomToolbar().showBusy(msg);
    }
    , startActions: function (waitWinCfg) {
        if (Ext.isDefined(waitWinCfg)) this.waitWindowCfg = waitWinCfg;
        if (!this.actionsAutoStart && !this.isBusy) {
            this.performAction.defer(200, this);
        }
    }
    , addAction: function (action) {
        this.actions.push(action);
        if (!this.isBusy && this.actionsAutoStart) this.performAction.defer(200, this);
    }
    , addActions: function (actions) {
        if (!Ext.isArray(actions)) return;
        for (var i = 0; i < actions.length; i++) {
            this.actions.push(actions[i]);
        }
        if (!this.isBusy && this.actionsAutoStart) this.performAction.defer(200, this);
    }
    , performAction: function () {
        if (this.actions.length > 0 && !this.isBusy) {
            this.isBusy = true;
            var act = this.actions.shift();
            if (act.action == "CLOSE_WINDOW") {
                this.doClose();
            } else if (act.action == "DUMMY") {
                if (this.actionsShowWait) this.updateWait(act);
                this.onActionSuccess.defer(2000, this, [null, null, act]);
            } else if (Ext.isFunction(act.action)) {
                var res = false;
                if (act.scope) {
                    res = act.action.call(act.scope, act);
                } else {
                    res = act.action(act);
                }
                if (res) {
                    this.onActionSuccess(null, null, act);
                } else {
                    this.onActionFailure(null, null, act);
                }
            } else {
                if (this.actionsShowWait) this.updateWait(act);
                var txt = act.text;
                if (this.actions.length > 0) txt += ' [' + this.actions.length + ' actions left]';
                this.setStatusBusy(txt);
                if (Ext.isDefined(act.lockWindow) && act.lockWindow) this.getEl().mask(act.txt, 'x-mask-loading');
                if (this.enableFileCache) {
                    eBook.CachedAjax.request({
                        url: (act.url ? act.url : eBook.Service.url) + act.action
                        , method: 'POST'
                        , params: Ext.encode(act.params)
                        , success: function (resp, opts) { this.onActionSuccess(resp, opts, act); }
                        , failure: function (resp, opts) { this.onActionFailure(resp, opts, act); }
                        , scope: this
                    });
                } else {
                    Ext.Ajax.request({
                        url: (act.url ? act.url : eBook.Service.url) + act.action
                        , method: 'POST'
                        , params: Ext.encode(act.params)
                        , success: function (resp, opts) { this.onActionSuccess(resp, opts, act); }
                        , failure: function (resp, opts) { this.onActionFailure(resp, opts, act); }
                        , scope: this
                    });
                }
            }
        } else {
            this.isBusy = false;
            this.setStatus('Ready');
            if (this.waitWindow) {
                this.waitWindow.getDialog().close();
                this.waitWindow = null;
                this.waitWindowCfg = null;
            }
        }
    }
    , onActionSuccess: function (resp, opts, action) {
        
        this.isBusy = false;
        this.setStatus(action.text + ' - SUCCESS');
        if (Ext.isDefined(action.lockWindow) && action.lockWindow && this.actions.length == 0) this.getEl().unmask();
        this.performAction.defer(100, this);
    }
    , onActionFailure: function (resp, opts, action) {
        
        this.isBusy = false;
        this.failedActions.push(action);
        if (Ext.isDefined(action.lockWindow) && action.lockWindow) this.getEl().unmask();
        if (this.haltOnActionError) {
            // show messagebox
            if (this.waitWindow) {
                this.waitWindow.getDialog().close();
                this.waitWindow = null;
                this.waitWindowCfg = null;
            }
        } else {
            this.setStatus(action.text + ' - ERROR');
            this.performAction.defer(100, this);
        }
    }
    , show: function () {
        this.actions = [];
        this.failedActions = [];
        //this.actions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        eBook.Window.superclass.show.call(this);
        this.clearStatus();
    }
    , waitWindow: null
    , length: 0
    , updateWait: function (action) {
        if (!this.waitWindow) this.length = this.actions.length + 1;
        if (!this.waitWindow) {
            this.waitWindowCfg = this.waitWindowCfg ? this.waitWindowCfg : this.defaultWaitWindowCfg;
            Ext.apply(this.waitWindowCfg, {
                buttons: false,
                progress: true,
                closable: false,
                minWidth: this.minProgressWidth,
                progressText: null
            });
            this.waitWindow = Ext.Msg.show(this.waitWindowCfg);

            // this.waitWindow.getDialog().setWidth(500);
        }
        var cnt = (this.length - this.actions.length);
        Ext.Msg.updateProgress((1 / this.length) * cnt, cnt + '/' + this.length + ': ' + action.text, this.waitWindowCfg.msg);
    }
    , onBeforeClose: function () {
        if (this.myCloseAction) {
            this.myCloseAction();
        }
        if (!this.waitWindow) this.length = this.actions.length;
        if (this.actions.length > 0) {
            if (!this.waitWindow) this.waitWindow = Ext.Msg.progress(eBook.Window_Busy, this.busyMsg);
            this.actions.shift();
            var cnt = (this.length - this.actions.length);
            Ext.Msg.updateProgress((1 / this.length) * cnt, cnt + '/' + this.length, this.busyMsg);
            this.close.defer(500, this);
            return false;
        } else if (this.isBusy) {
            if (!this.waitWindow) this.waitWindow = Ext.Msg.wait(eBook.Window_Busy, this.busyMsg);
            this.close.defer(200, this);
            return false;
        } else if (this.locked) {
            return false;
        } else {
            if (this.waitWindow) {
                this.waitWindow.getDialog().close();
                this.waitWindow = null;
            }
            this.doClose();
        }

        return true;

    }
});

Ext.reg('eBook.Window', eBook.Window);

//eBook.FileCultureWindow = Ext.extend(Ext.Window, {
//    initComponent: function() {
//        Ext.apply(this, {
//            width: 300,
//            height: 200,
//            title:'SET DEFAULT LANGUAGE',
//            bodyStyle:'padding:10px;',
//            items: [{ xtype: 'label', text: 'The import of this file into the new eBook version could not automatically determine the default language of this file.', style: 'display:block;font-weight:bold;' }
//                , { xtype: 'label', text: 'Please select the default language', style: 'display:block;font-weight:bold;margin-top:5px;' }
//                , { xtype: 'languagebox', allowBlank: false, ref: 'langbox' }
//                    ]
//            , maximizable: false
//            ,modal:true
//            ,buttons:[{  text: 'Save & continue...',
//                    iconCls: 'eBook-window-save-ico',
//                    scale: 'medium',
//                    iconAlign: 'top',
//                    handler: this.onSetFileCulture,
//                    width: 70,
//                    scope: this
//                }]
//        });
//        eBook.FileCultureWindow.superclass.initComponent.apply(this, arguments);
//    }
//    , onSetFileCulture: function() {
//        if(this.langbox.isValid()) {
//            this.getEl().mask('Saving language setting...', 'x-mask-loading');
//            this.record.set('Culture', this.langbox.getValue());
//            this.record.commit();
//            Ext.Ajax.request({
//                url: eBook.Service.url + 'SetFileCulture'
//                , method: 'POST'
//                , params: Ext.encode({ cicdc: {
//                    Id:this.record.get('Id'),
//                    Culture: this.langbox.getValue()
//                }
//                })
//                , callback: this.onSetFileCultureResponse
//                , scope: this
//            });
//        }
//    }
//    , onSetFileCultureResponse: function(opts, success, resp) {
//        eBook.Interface.loadFile(this.record);
//    }
//});
//Ext.reg('eBook.FileCultureWindow', eBook.FileCultureWindow);


eBook.LoadOverlay = {
    el: null
    , setEl: function(el) {
        this.el = el;
        this.el.setVisibilityMode(Ext.Element.DISPLAY);
        this.el.hide();
    }
    , showFor: function(target, offsets) {
        this.el.show();
        this.el.anchorTo(target, 'br',offsets);
    }
    , hide: function() {
        this.el.hide();
    }
};// PRODUCTION VERSION
eBook.Service = {
    url:'/EY.com.eBook.API.5/GlobalService.svc/',
    bizTaxOld: '/EY.com.eBook.API.5/BizTaxService.svc/',
    bundle: '/EY.com.eBook.API.5/BundleService.svc/',
    lists: '/EY.com.eBook.API.5/ListService.svc/',
    home: '/EY.com.eBook.API.5/HomeService.svc/',
    file: '/EY.com.eBook.API.5/FileService.svc/',
    schema: '/EY.com.eBook.API.5/AccountSchemaService.svc/',
    client: '/EY.com.eBook.API.5/ClientService.svc/',
    businessrelation: '/EY.com.eBook.API.5/BusinessRelationService.svc/',
    proacc: '/EY.com.eBook.API.5/ProAccWebService.svc/',
    excel: '/EY.com.eBook.API.5/ExcelService.svc/',
    document: '/EY.com.eBook.API.5/DocumentService.svc/',
    output: '/EY.com.eBook.API.5/OutputService.svc/',
    rule2011: '/EY.com.eBook.RuleEngine.5/RuleEngine2011.svc/',
    rule2012: '/EY.com.eBook.RuleEngine.5/RuleEngine2012.svc/',
    rule2013: '/EY.com.eBook.RuleEngine.5/RuleEngine2013.svc/',
    rule2014: '/EY.com.eBook.RuleEngine.5/RuleEngine2014.svc/',
    rule2015: '/EY.com.eBook.RuleEngine.5/RuleEngine2015.svc/',
    rule2016: '/EY.com.eBook.RuleEngine.5/RuleEngine2016.svc/',
    rule: function(year){return '/EY.com.eBook.RuleEngine.5/RuleEngine'+year+'.svc/'},
    repository: '/EY.com.eBook.API.5/RepositoryService.svc/',
    GTH: '/EY.com.eBook.API.5/GTHTeamService.svc/',
    biztax: '/EY.com.eBook.BizTax.Wcf.5/',
    docstore: '/EY.com.eBook.API.5/DocstoreService.svc/',
    annualAccount: '/EY.com.eBook.API.5/AnnualAccountService.svc/'
};

// DEVELOPMENT VERSION
/*
 eBook.Service = {
 url: '/EY.com.eBook.API.5.DEV/EbookService.svc/'
 };
 */
eBook.data.WcfProxy = function(conn) {
    eBook.data.WcfProxy.superclass.constructor.call(this, conn);
    if (conn.criteriaParameter) this.criteriaParameter = conn.criteriaParameter;
};

Ext.extend(eBook.data.WcfProxy, Ext.data.HttpProxy, {
    criteriaParameter: ''
    ,doRequest : function(action, rs, params, reader, cb, scope, arg) {
        if (this.criteriaParameter != '') {
            var pars = {};
            pars[this.criteriaParameter] = params;
            params  =pars;
        } 
        var  o = {
            method: (this.api[action]) ? this.api[action]['method'] : undefined,
            request: {
                callback : cb,
                scope : scope,
                arg : arg
            },
            reader: reader,
            callback : this.createCallback(action, rs),
            scope: this
        };

        // If possible, transmit data using jsonData || xmlData on Ext.Ajax.request (An installed DataWriter would have written it there.).
        // Use std HTTP params otherwise.
        if (params.jsonData) {
            o.jsonData = params.jsonData;
        } else if (params.xmlData) {
            o.xmlData = params.xmlData;
        } else {
            o.params = params || {};
        }
        // Set the connection url.  If this.conn.url is not null here,
        // the user must have overridden the url during a beforewrite/beforeload event-handler.
        // this.conn.url is nullified after each request.
        this.conn.url = this.buildUrl(action, rs);
        o.params = Ext.encode(params);
        if(this.useAjax){

            Ext.applyIf(o, this.conn);

            // If a currently running request is found for this action, abort it.
            if (this.activeRequest[action]) {
                ////
                // Disabled aborting activeRequest while implementing REST.  activeRequest[action] will have to become an array
                // TODO ideas anyone?
                //
                //Ext.Ajax.abort(this.activeRequest[action]);
            }
            if(this.enableFileCached) {
                this.activeRequest[action] = eBook.CachedAjax.request(o);
            } else {
                this.activeRequest[action] = Ext.Ajax.request(o);
            }
        }else{
            this.conn.request(o);
        }
        // request is sent, nullify the connection url in preparation for the next request
        this.conn.url = null;
    }
   
});

eBook.Language.LanguagesList = [
    ['nl-BE', eBook.Language_Dutch]
    , ['fr-FR', eBook.Language_French]
    , ['de-DE', eBook.Language_German]
    , ['en-US', eBook.Language_English]
];

eBook.Repository.CategoriesList = [
    ['perm', eBook.RepositoryCategory_Permanent]
    , ['period', eBook.RepositoryCategory_Period]
];


eBook.data.RecordTypes = {
    SimpleList: [{ name: 'id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'desc', mapping: 'desc', type: Ext.data.Types.STRING}]
    , GlobalListItem: [{ name: 'id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'nl', mapping: 'nl', type: Ext.data.Types.STRING }
            , { name: 'fr', mapping: 'fr', type: Ext.data.Types.STRING }
            , { name: 'en', mapping: 'en', type: Ext.data.Types.STRING }
            , { name: 'value', mapping: 'value', type: Ext.data.Types.NUMBER}]
    , FodList: [
            { name: 'id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'name', mapping: 'd', type: Ext.data.Types.STRING }
            , { name: 'listid', mapping: 'li', type: Ext.data.Types.STRING }
            , { name: 'meta', mapping: 'm', type: Ext.data.Types.STRING}]
    , RecentFiles: [
        { name: 'file', mapping: 'f', type: Ext.data.Types.AUTO }
        , { name: 'client', mapping: 'c', type: Ext.data.Types.AUTO }
        , { name: 'clientName', mapping: 'cn', type: Ext.data.Types.STRING }
    //, { name: 'assessment', mapping: 'fas', type: Ext.data.Types.INT }
    //, { name: 'start', mapping: 'fsd', type: Ext.data.Types.WCFDATE }
    //, { name: 'end', mapping: 'fed', type: Ext.data.Types.WCFDATE }
        , { name: 'lastaccessed', mapping: 'la', type: Ext.data.Types.WCFDATE }
    ]
    , Form: [
        { name: 'field', mapping: 'f', type: Ext.data.Types.STRING  }
        , { name: 'value', mapping: 'v', type: Ext.data.Types.STRING  }
    ],
    QueryFiles: [
        { name: 'field', mapping: 'f', type: Ext.data.Types.STRING  }
        , { name: 'value', mapping: 'v', type: Ext.data.Types.STRING  }
    ]
    , CreateFileActions: [
                        { name: 'id', mapping: 'id', type: Ext.data.Types.STRING }
                        , { name: 'action', mapping: 'action', type: Ext.data.Types.STRING }
                        , { name: 'status', mapping: 'status', type: Ext.data.Types.STRING }
                        , { name: 'container', mapping: 'container', type: Ext.data.Types.STRING }
                        , { name: 'errorMsg', mapping: 'status', type: Ext.data.Types.STRING }
                        , { name: 'text', mapping: 'text', type: Ext.data.Types.STRING }
                    ]
    , FileType: [
                { name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'Order', mapping: 'o', type: Ext.data.Types.INT }
                ]

    , FileHistory: [
                { name: 'clientId', type: Ext.data.Types.STRING }
                , { name: 'clientGfis', type: Ext.data.Types.STRING }
                , { name: 'clientName', type: Ext.data.Types.STRING }
                , { name: 'fileId', type: Ext.data.Types.STRING }
                , { name: 'fileName', type: Ext.data.Types.STRING }
                , { name: 'startDate', type: Ext.data.Types.DATE }
                , { name: 'endDate', type: Ext.data.Types.DATE }
                , { name: 'lastaccess', type: Ext.data.Types.DATE}]
    , ClientBase: [
                { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'GFISCode', mapping: 'gc', type: Ext.data.Types.STRING }
                , { name: 'Address', mapping: 'a', type: Ext.data.Types.STRING }
                , { name: 'ZipCode', mapping: 'zip', type: Ext.data.Types.STRING }
                , { name: 'City', mapping: 'ci', type: Ext.data.Types.STRING }
                , { name: 'Country', mapping: 'co', type: Ext.data.Types.STRING }
                , { name: 'VatNumber', mapping: 'vnr', type: Ext.data.Types.STRING }
                , { name: 'EnterpriseNumber', mapping: 'enr', type: Ext.data.Types.STRING }
                , { name: 'ProAccServer', mapping: 'pas', type: Ext.data.Types.STRING }
                , { name: 'ProAccDatabase', mapping: 'pad', type: Ext.data.Types.STRING }
                , { name: 'ProAccLinkUpdated', mapping: 'palu', type: Ext.data.Types.WCFDATE }
                , { name: 'Shared', mapping: 'sh', type: Ext.data.Types.BOOLEAN }
                , { name: 'bni', mapping: 'bni', type: Ext.data.Types.BOOLEAN }
                ]
    , FileInfo: [
                { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
                , { name: 'ClientId', mapping: 'cid', type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                , { name: 'StartDate', mapping: 'sd', type: Ext.data.Types.WCFDATE }
                , { name: 'EndDate', mapping: 'ed', type: Ext.data.Types.WCFDATE }
                , { name: 'PreviousStartDate', mapping: 'psd', type: Ext.data.Types.WCFDATE }
                , { name: 'PreviousEndDate', mapping: 'ped', type: Ext.data.Types.WCFDATE }
                , { name: 'AssessmentYear', mapping: 'ay', type: Ext.data.Types.INT }
                , { name: 'Display', mapping: 'dsp', type: Ext.data.Types.STRING }
                , { name: 'Closed', mapping: 'cl', type: Ext.data.Types.BOOLEAN }
                , { name: 'Errors', mapping: 'ers', type: Ext.data.Types.INT }
                , { name: 'Warnings', mapping: 'wrs', type: Ext.data.Types.INT }
                , { name: 'ImportDate', mapping: 'imd', type: Ext.data.Types.WCFDATE }
                , { name: 'ImportType', mapping: 'imt', type: Ext.data.Types.STRING }
                , { name: 'NumbersLocked', mapping: 'nl', type: Ext.data.Types.BOOL }
                , { name: 'Services', mapping: 'svc', type: Ext.data.Types.AUTO }
                ]
    , DistinctCounterFiles: [
                { name: 'StartDate', mapping: 'sd', type: Ext.data.Types.WCFDATE }
                , { name: 'EndDate', mapping: 'ed', type: Ext.data.Types.WCFDATE }
                , { name: 'Counter', mapping: 'cnt', type: Ext.data.Types.INT }
                , { name: 'ClientName', mapping: 'cn', type: Ext.data.Types.STRING }
                , { name: 'ClientId', mapping: 'cid', type: Ext.data.Types.STRING }
                , { name: 'FileId', mapping: 'fid', type: Ext.data.Types.STRING }
    ]
    , FileBase: [
                { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
                , { name: 'ClientId', mapping: 'cid', type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                , { name: 'StartDate', mapping: 'sd', type: Ext.data.Types.WCFDATE }
                , { name: 'EndDate', mapping: 'ed', type: Ext.data.Types.WCFDATE }
                , { name: 'AssessmentYear', mapping: 'ay', type: Ext.data.Types.INT }
    //  , { name: 'LastUpdated', mapping: 'lud', type: Ext.data.Types.WCFDATE }
    // , { name: 'LastUpdatedBy', mapping: 'lub', type: Ext.data.Types.STRING }
                , { name: 'Closed', mapping: 'cl', type: Ext.data.Types.BOOLEAN }
                , { name: 'NumbersLocked', mapping: 'nl', type: Ext.data.Types.BOOLEAN }
    // , { name: 'AccountLength', mapping: 'al', type: Ext.data.Types.INT }
                , { name: 'ImportDate', mapping: 'imd', type: Ext.data.Types.WCFDATE }
                , { name: 'ImportType', mapping: 'imt', type: Ext.data.Types.STRING }
                , { name: 'MarkDelete', mapping: 'md', type: Ext.data.Types.BOOLEAN }
                , { name: 'Deleted', mapping: 'd', type: Ext.data.Types.BOOLEAN }
                , { name: 'Display', mapping: 'dsp', type: Ext.data.Types.STRING }
                , { name: 'PreviousFileId', mapping: 'pfid', type: Ext.data.Types.STRING }
                , { name: 'PreviousStartDate', mapping: 'psd', type: Ext.data.Types.WCFDATE }
                , { name: 'PreviousEndDate', mapping: 'ped', type: Ext.data.Types.WCFDATE }
                , { name: 'Errors', mapping: 'ers', type: Ext.data.Types.INT }
                , { name: 'Warnings', mapping: 'wrs', type: Ext.data.Types.INT }
                ]
    , BusinessRelation: [
                { name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
                , { name: 'ImportedId', mapping: 'iid', type: Ext.data.Types.STRING }
                , { name: 'ClientId', mapping: 'cid', type: Ext.data.Types.STRING }
                , { name: 'LastName', mapping: 'ln', type: Ext.data.Types.STRING }
                , { name: 'FirstName', mapping: 'fn', type: Ext.data.Types.STRING }
                , { name: 'Address', mapping: 'ad', type: Ext.data.Types.STRING }
                , { name: 'Zip', mapping: 'zc', type: Ext.data.Types.STRING }
                , { name: 'City', mapping: 'ci', type: Ext.data.Types.STRING }
                , { name: 'Country', mapping: 'co', type: Ext.data.Types.STRING }
                , { name: 'VatNumber', mapping: 'vt', type: Ext.data.Types.STRING }
                , { name: 'EnterpriseNumber', mapping: 'cn', type: Ext.data.Types.STRING }
                , { name: 'RelationType', mapping: 'bt', type: Ext.data.Types.STRING }
                ]
    , BusinessRelationList: [
                { name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                ]
    , YearList: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.INTEGER}]
    , MonthList: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING}]
    , MetaLookup: [
                 { name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
                  , { name: 'Meta', mapping: 'm', type: Ext.data.Types.STRING }
                  , { name: 'Value', mapping: 'v', type: Ext.data.Types.STRING }
                  , { name: 'Seq', mapping: 's', type: Ext.data.Types.INTEGER }
                  , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                  , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                ]
    , RepositoryItem: [
        { name: 'Id', mapping: 'Id', type: Ext.data.Types.STRING }
        , { name: 'ClientId', mapping: 'ClientId', type: Ext.data.Types.STRING }
        , { name: 'FileName', mapping: 'FileName', type: Ext.data.Types.STRING }
        , { name: 'ContentType', mapping: 'ContentType', type: Ext.data.Types.STRING }
        , { name: 'DocstoreId', mapping: 'DocstoreId', type: Ext.data.Types.STRING }
        , { name: 'PhysicalFileId', mapping: 'PhysicalFileId', type: Ext.data.Types.STRING }
        , { name: 'PeriodStart', mapping: 'PeriodStart', type: Ext.data.Types.WCFDATE }
        , { name: 'PeriodEnd', mapping: 'PeriodEnd', type: Ext.data.Types.WCFDATE  }
        , { name: 'Status', mapping: 'Status', type: Ext.data.Types.STRING }
        , { name: 'StatusText', mapping: 'StatusText', type: Ext.data.Types.STRING }
        , { name: 'Extension', mapping: 'Extension', type: Ext.data.Types.STRING }
        , { name: 'IconCls', mapping: 'IconCls', type: Ext.data.Types.STRING }
    ]
    , RepositoryItemLinks: [
                 { name: 'ClientId', mapping: 'ClientId', type: Ext.data.Types.STRING }
                  , { name: 'PhysicalFileId', mapping: 'PhysicalFileId', type: Ext.data.Types.STRING }
                  , { name: 'ItemId', mapping: 'ItemId', type: Ext.data.Types.STRING }
                  , { name: 'ConnectionType', mapping: 'ConnectionType', type: Ext.data.Types.STRING }
                  , { name: 'ConnectionGuid', mapping: 'ConnectionGuid', type: Ext.data.Types.STRING }
                  , { name: 'ConnectionAccount', mapping: 'ConnectionAccount', type: Ext.data.Types.STRING }
                  , { name: 'ConnectionDetailedPath', mapping: 'ConnectionDetailedPath', type: Ext.data.Types.STRING }
                  , { name: 'Description', mapping: 'Description', type: Ext.data.Types.STRING }

                ]
    , Account: [
         { name: 'Id', mapping: 'inr', index: true, type: Ext.data.Types.STRING }
        , { name: 'nr', mapping: 'vnr', type: Ext.data.Types.STRING }
        , { name: 'description', mapping: 'd', type: Ext.data.Types.STRING }
        , { name: 'taxAdjustment', mapping: 'txd', type: Ext.data.Types.STRING }
        , { name: 'taxParent', mapping: 'txp', type: Ext.data.Types.STRING }
        , { name: 'existedLastYear', mapping: 'ely', type: Ext.data.Types.BOOLEAN }
        , { name: 'hasTaxAdjustments', mapping: 'hta', type: Ext.data.Types.BOOLEAN}]
    , AccountsMappingGrid: [
                { name: 'Id', mapping: 'inr', index: true, type: Ext.data.Types.STRING }
                , { name: 'nr', mapping: 'vnr', type: Ext.data.Types.STRING }
                , { name: 'description', mapping: 'd', type: Ext.data.Types.STRING }
                , { name: 'saldo', mapping: 'ta', type: Ext.data.Types.NUMBER }
                , { name: 'saldoPrevious', mapping: 'tap', type: Ext.data.Types.NUMBER }
                , { name: 'taxAdjustment', mapping: 'txd', type: Ext.data.Types.STRING }
                , { name: 'taxParent', mapping: 'txp', type: Ext.data.Types.STRING }
                , { name: 'existedLastYear', mapping: 'ely', type: Ext.data.Types.BOOLEAN }
                , { name: 'hasTaxAdjustments', mapping: 'hta', type: Ext.data.Types.BOOLEAN }
                , { name: 'BelastingenApp', meta: '10eea163-5e43-4501-9f1c-35ae838be615', mapping: eBook.getMapping('10eea163-5e43-4501-9f1c-35ae838be615', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'BelastingenApp_name', meta: '10eea163-5e43-4501-9f1c-35ae838be615', mapping: eBook.getMapping('10eea163-5e43-4501-9f1c-35ae838be615', 'NAME'), type: Ext.data.Types.STRING }
                , { name: 'PersoneelApp', meta: 'bd2374b4-42c3-4c09-96b2-d8debe3fa1b1', mapping: eBook.getMapping('bd2374b4-42c3-4c09-96b2-d8debe3fa1b1', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'PersoneelApp_name', meta: 'bd2374b4-42c3-4c09-96b2-d8debe3fa1b1', mapping: eBook.getMapping('bd2374b4-42c3-4c09-96b2-d8debe3fa1b1', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'BTWApp', meta: '2ca965ed-eacb-43ff-9a91-cfe7ef09eef9', mapping: eBook.getMapping('2ca965ed-eacb-43ff-9a91-cfe7ef09eef9', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'BTWApp_name', meta: '2ca965ed-eacb-43ff-9a91-cfe7ef09eef9', mapping: eBook.getMapping('2ca965ed-eacb-43ff-9a91-cfe7ef09eef9', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'VoordelenApp', meta: '67855d3d-4e11-4c7b-96a3-e6602ddaaa6f', mapping: eBook.getMapping('67855d3d-4e11-4c7b-96a3-e6602ddaaa6f', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'VoordelenApp_name', meta: '67855d3d-4e11-4c7b-96a3-e6602ddaaa6f', mapping: eBook.getMapping('67855d3d-4e11-4c7b-96a3-e6602ddaaa6f', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'ErelonenHuurApp', meta: '3e9bb601-ea23-4072-bd49-e5dc61a6b5a8', mapping: eBook.getMapping('3e9bb601-ea23-4072-bd49-e5dc61a6b5a8', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'ErelonenHuurApp_name', meta: '3e9bb601-ea23-4072-bd49-e5dc61a6b5a8', mapping: eBook.getMapping('3e9bb601-ea23-4072-bd49-e5dc61a6b5a8', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'VoorschottenApp', meta: '997a80e4-ea39-42c7-95d5-cdf8322c4c11', mapping: eBook.getMapping('997a80e4-ea39-42c7-95d5-cdf8322c4c11', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'VoorschottenApp_name', meta: '997a80e4-ea39-42c7-95d5-cdf8322c4c11', mapping: eBook.getMapping('997a80e4-ea39-42c7-95d5-cdf8322c4c11', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'RVIntrestenApp', meta: '3b260f00-e6e8-4c6b-83ea-69457799a068', mapping: eBook.getMapping('3b260f00-e6e8-4c6b-83ea-69457799a068', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'RVIntrestenApp_name', meta: '3b260f00-e6e8-4c6b-83ea-69457799a068', mapping: eBook.getMapping('3b260f00-e6e8-4c6b-83ea-69457799a068', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'BelasteReservesApp', meta: '7df44f01-2f13-4cd5-ada1-7ad8003a85c1', mapping: eBook.getMapping('7df44f01-2f13-4cd5-ada1-7ad8003a85c1', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'BelasteReservesApp_name', meta: '7df44f01-2f13-4cd5-ada1-7ad8003a85c1', mapping: eBook.getMapping('7df44f01-2f13-4cd5-ada1-7ad8003a85c1', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'DBIApp', meta: '77d8dbbc-feec-478e-9e11-83838e346caf', mapping: eBook.getMapping('77d8dbbc-feec-478e-9e11-83838e346caf', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'DBIApp_name', meta: '77d8dbbc-feec-478e-9e11-83838e346caf', mapping: eBook.getMapping('77d8dbbc-feec-478e-9e11-83838e346caf', 'NAME'), type: Ext.data.Types.STRING }
            ]
   , MappingItem: [
                { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
                , { name: 'meta', mapping: 'm', type: Ext.data.Types.STRING }
                , { name: 'value', mapping: 'v', type: Ext.data.Types.STRING }
                , { name: 'assessmentYear', mapping: 'ay', type: Ext.data.Types.STRING }
                , { name: 'name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'groupname', mapping: 'gn', type: Ext.data.Types.STRING }
            ]
    , AccountDescriptions: [
                 { name: 'AccountId', mapping: 'inr', index: true, type: Ext.data.Types.STRING }
                 , { name: 'culture', mapping: 'c', index: true, type: Ext.data.Types.STRING }
                 , { name: 'description', mapping: 'd', type: Ext.data.Types.STRING }
            ]
    , AccountMapping: [
                { name: 'FileId', mapping: 'ft', type: Ext.data.Types.STRING }
                , { name: 'InternalNr', mapping: 'inr', type: Ext.data.Types.STRING }
                , { name: 'worksheetTypeId', mapping: 'wt.id', index: true, type: Ext.data.Types.STRING }
                , { name: 'worksheetTypeName', mapping: 'wt.n', type: Ext.data.Types.STRING }
                , { name: 'mappingId', mapping: 'mi.id', type: Ext.data.Types.STRING }
                , { name: 'mappingName', mapping: 'mi.n', type: Ext.data.Types.STRING }
            ]
    , Messages: [
                 { name: 'FileId', mapping: 'fid', type: Ext.data.Types.STRING }
                , { name: 'MessageId', mapping: 'mid', index: true, type: Ext.data.Types.STRING }
                , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                , { name: 'Type', mapping: 'tp', type: Ext.data.Types.STRING }
                , { name: 'Text', mapping: 'tx', type: Ext.data.Types.STRING }
                , { name: 'ConnectionType', mapping: 'ct', type: Ext.data.Types.STRING }
                , { name: 'Account', mapping: 'ca', type: Ext.data.Types.STRING }
                , { name: 'Guid', mapping: 'cg', type: Ext.data.Types.STRING }
                , { name: 'Collection', mapping: 'wtc', type: Ext.data.Types.STRING }
                , { name: 'CollectionText', mapping: 'wtct', type: Ext.data.Types.STRING }
                , { name: 'RowIndex', mapping: 'wtrid', type: Ext.data.Types.STRING }
                , { name: 'Field', mapping: 'wtfld', type: Ext.data.Types.STRING }
                , { name: 'FieldText', mapping: 'wtfldt', type: Ext.data.Types.STRING }
                , { name: 'Worksheet', type: Ext.data.Types.STRING }
            ]
   , WorksheetTypes: [
                 { name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
                , { name: 'RuleApp', mapping: 'ra', type: Ext.data.Types.STRING }
                , { name: 'Order', mapping: 'o', index: true, type: Ext.data.Types.INTEGER }
                , { name: 'Type', mapping: 't', type: Ext.data.Types.STRING }
                , { name: 'IsActive', mapping: 'ia', type: Ext.data.Types.BOOL }
                , { name: 'IsFiche', mapping: 'if', type: Ext.data.Types.BOOL }
                , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'Errors', mapping: 'ers', type: Ext.data.Types.INT }
                , { name: 'Closed', mapping: 'cl', type: Ext.data.Types.BOOL }
                , { name: 'Warnings', mapping: 'wrs', type: Ext.data.Types.INT }
            ]
    , BookingLines: [
            { name: 'BookingId', mapping: 'bid', type: Ext.data.Types.STRING }
            , { name: 'CreationDate', mapping: 'bcd', type: Ext.data.Types.WCFDATE }
            , { name: 'GroupNr', mapping: 'gnr', type: Ext.data.Types.INT }
            , { name: 'BookingDescription', mapping: 'bd', type: Ext.data.Types.STRING }
            , { name: 'JournalType', mapping: 'bjt', type: Ext.data.Types.INT }
            , { name: 'WorksheetTypeId', mapping: 'wt', type: Ext.data.Types.STRING }
            , { name: 'WorksheetCollection', mapping: 'wtc', type: Ext.data.Types.STRING }
            , { name: 'WorksheetRowId', mapping: 'wtr', type: Ext.data.Types.STRING }
            , { name: 'WorksheetKey', mapping: 'wbk', type: Ext.data.Types.STRING }
            , { name: 'Hidden', mapping: 'bh', type: Ext.data.Types.BOOL }
            , { name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'Debet', mapping: 'd', type: Ext.data.Types.NUMBER }
            , { name: 'Credit', mapping: 'c', type: Ext.data.Types.NUMBER }
            , { name: 'Account', mapping: 'acc', type: Ext.data.Types.AUTO }
            , { name: 'InternalAccountNr', mapping: 'ianr', type: Ext.data.Types.STRING }
            , { name: 'ExportCount', mapping: 'ec', type: Ext.data.Types.INT }
            , { name: 'ClientSupplierName', mapping: 'csn', type: Ext.data.Types.STRING }
            , { name: 'ClientSupplierId', mapping: 'csi', type: Ext.data.Types.STRING }
            , { name: 'Comment', mapping: 'com', type: Ext.data.Types.STRING }
            , { name: 'Bold', type: Ext.data.Types.STRING }
    ]
    , FinalTrialBalance: [
            { name: 'InternalAccountNr', mapping: 'ian', type: Ext.data.Types.STRING }
            , { name: 'Account', mapping: 'acc', type: Ext.data.Types.AUTO }
            , { name: 'StartBalance', mapping: 'sb', type: Ext.data.Types.NUMBER }
            , { name: 'AdjustmentsDebet', mapping: 'ad', type: Ext.data.Types.NUMBER }
            , { name: 'AdjustmentsCredit', mapping: 'ac', type: Ext.data.Types.NUMBER }
            , { name: 'EndBalance', mapping: 'eb', type: Ext.data.Types.NUMBER }
    ]
    , DocumentBlock: [
        { name: 'Order', mapping: 'o', index: true, type: Ext.data.Types.INTEGER }
        , { name: 'Title', mapping: 'ti', type: Ext.data.Types.STRING }
        , { name: 'Text', mapping: 'text', type: Ext.data.Types.STRING }
        , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
        , { name: 'EditOnStart', mapping: 'eos', type: Ext.data.Types.BOOL}]
    , DocumentType: [
          { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
        , { name: 'Key', mapping: 'k', type: Ext.data.Types.STRING }
        , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
        , { name: 'IsFullyEditable', mapping: 'ife', type: Ext.data.Types.BOOL }
        , { name: 'IsActive', mapping: 'ia', type: Ext.data.Types.BOOL}]
    , DocumentList: [
          { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
        , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
        , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
        , { name: 'DocumentTypeId', mapping: 'dtid', type: Ext.data.Types.STRING}]
    , Bookyear: [{ name: 'Bookyear', mapping: 'Bookyear', index: true, type: Ext.data.Types.INT}]
    , Periods: [{ name: 'Period', mapping: 'Period', index: true, type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'Name', type: Ext.data.Types.STRING}]
    , ExtraFiles: [{ name: 'Name', mapping: 'Name', index: true, type: Ext.data.Types.STRING}]
    , InRuleList: [{ name: 'Display', mapping: 'Display', type: Ext.data.Types.STRING },
                    { name: 'Value', mapping: 'Value', index: true, type: Ext.data.Types.STRING}]
    , FileReportList: [{ name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
                        , { name: 'Name', mapping: 'Name', type: Ext.data.Types.STRING }
                        , { name: 'Culture', mapping: 'Culture', type: Ext.data.Types.STRING }
                        ]
    , ExcelPreview: [{ name: 'accountnr', mapping: 'AccountNr', index: true, type: Ext.data.Types.STRING }
                        , { name: 'accountdescription', mapping: 'AccountDescription', type: Ext.data.Types.STRING }
                        , { name: 'saldo', mapping: 'Saldo', type: Ext.data.Types.NUMBER }
                        , { name: 'debet', mapping: 'Debet', type: Ext.data.Types.NUMBER }
                        , { name: 'credit', mapping: 'Credit', type: Ext.data.Types.NUMBER }
                        ]
    , Statistics: [{ name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING },
                    { name: 'LabelY', mapping: 'LabelY', type: Ext.data.Types.STRING },
                    { name: 'Year', mapping: 'Year', type: Ext.data.Types.INTEGER },
                    { name: 'Month', mapping: 'Month', type: Ext.data.Types.INTEGER },
                    { name: 'MonthName', mapping: 'MonthName', type: Ext.data.Types.STRING },
                    { name: 'Total1', mapping: 'Total1', type: Ext.data.Types.NUMBER },
                    { name: 'Total2', mapping: 'Total2', type: Ext.data.Types.NUMBER}]

    , StatisticsSimple: [{ name: 'LabelY', mapping: 'LabelY', index: true, type: Ext.data.Types.STRING },
                    { name: 'Total1', mapping: 'Total1', type: Ext.data.Types.NUMBER },
                    { name: 'Total2', mapping: 'Total2', type: Ext.data.Types.NUMBER },
                    { name: 'Total3', mapping: 'Total3', type: Ext.data.Types.NUMBER },
                    { name: 'Total4', mapping: 'Total4', type: Ext.data.Types.NUMBER },
                    { name: 'Total5', mapping: 'Total5', type: Ext.data.Types.NUMBER },
                    { name: 'Total6', mapping: 'Total6', type: Ext.data.Types.NUMBER },
                    { name: 'Total7', mapping: 'Total7', type: Ext.data.Types.NUMBER },
                    { name: 'Total8', mapping: 'Total8', type: Ext.data.Types.NUMBER },
                    { name: 'Total9', mapping: 'Total9', type: Ext.data.Types.NUMBER },
                    { name: 'Total10', mapping: 'Total10', type: Ext.data.Types.NUMBER}]

    , Offices: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                    { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING}]
    , PdfFile: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                { name: 'FileId', mapping: 'fid', type: Ext.data.Types.STRING },
                { name: 'Name', mapping: 'd', type: Ext.data.Types.STRING },
                { name: 'Pages', mapping: 'p', type: Ext.data.Types.INT}]
    , Champion: [{ name: 'personId', mapping: 'pid', index: true, type: Ext.data.Types.STRING },
                { name: 'officeId', mapping: 'oid', type: Ext.data.Types.STRING },
                { name: 'firstName', mapping: 'fn', type: Ext.data.Types.STRING },
                { name: 'lastName', mapping: 'ln', type: Ext.data.Types.STRING },
                { name: 'email', mapping: 'em', type: Ext.data.Types.STRING },
                { name: 'officeName', mapping: 'o', type: Ext.data.Types.STRING}]
    , TeamMember: [{ name: 'personId', mapping: 'pid', index: true, type: Ext.data.Types.STRING },
                { name: 'clientId', mapping: 'cid', type: Ext.data.Types.STRING },
                { name: 'department', mapping: 'd', type: Ext.data.Types.STRING },
                { name: 'role', mapping: 'r', type: Ext.data.Types.STRING },
                { name: 'name', mapping: 'n', type: Ext.data.Types.STRING },
                { name: 'mail', mapping: 'm', type: Ext.data.Types.STRING },
                { name: 'level', mapping: 'l', type: Ext.data.Types.INT}]
    , ProAccJournals: [{ name: 'id', mapping: 'id', index: true, type: Ext.data.Types.INT },
                { name: 'name', mapping: 'n', type: Ext.data.Types.STRING}]
    , ManualWorksheets: [{ name: 'id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                { name: 'name', mapping: 'n', type: Ext.data.Types.STRING },
                { name: 'lastUpdated', mapping: 'lu', type: Ext.data.Types.WCFDATE}]
    , LibraryNode: [{ name: 'indexItem', mapping: 'indexItem', type: Ext.data.Types.AUTO}]

    , Resource: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                { name: 'Title', mapping: 'title', type: Ext.data.Types.STRING },
                { name: 'ResourceType', mapping: 'type', type: Ext.data.Types.STRING },
                { name: 'DC', mapping: '__type', type: Ext.data.Types.STRING}]
    , XbrlFiches: [
        { name: 'id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
        { name: 'automated', mapping: 'automated', type: Ext.data.Types.BOOL },
        { name: 'checked', mapping: 'checked', type: Ext.data.Types.BOOL },
        { name: 'pdfid', mapping: 'pdfid', type: Ext.data.Types.STRING },
        { name: 'pdfname', mapping: 'pdfname', type: Ext.data.Types.STRING }
        ]
     , LegalTypes: [
        { name: 'id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
        { name: 'listdesc', mapping: 'ld', type: Ext.data.Types.STRING },
        { name: 'code', mapping: 'c', type: Ext.data.Types.STRING },
        { name: 'name', mapping: 'n', type: Ext.data.Types.STRING }
        ]
    , Partners: [
        { name: 'id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
        , { name: 'name', mapping: 'n', type: Ext.data.Types.STRING }
        , { name: 'department', mapping: 'd', type: Ext.data.Types.STRING }
        ]
    , GTHClientsList: [
        { name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
        , { name: 'ClientName', mapping: 'ClientName', type: Ext.data.Types.STRING }
        , { name: 'ClientGfis', mapping: 'ClientGfis', type: Ext.data.Types.STRING }
        , { name: 'ClientEnterprise', mapping: 'ClientEnterprise', type: Ext.data.Types.STRING }
        , { name: 'StartDate', mapping: 'StartDate', type: Ext.data.Types.WCFDATE }
        , { name: 'EndDate', mapping: 'EndDate', type: Ext.data.Types.WCFDATE }
    ]
     , WorksheetHelp: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                { name: 'worksheetTypeID', mapping: 'wid', index: true, type: Ext.data.Types.STRING },
                { name: 'culture', mapping: 'c', type: Ext.data.Types.STRING },
                { name: 'subject', mapping: 's', type: Ext.data.Types.STRING },
                { name: 'body', mapping: 'b', type: Ext.data.Types.STRING },
                { name: 'order', mapping: 'o', type: Ext.data.Types.INT}]

    , ExactAdmin: [{ name: 'ExactAdminCode', mapping: 'eacode', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminNr', mapping: 'eanr', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminName', mapping: 'eaname', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminAddresLine1', mapping: 'eaal1', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminAddresLine2', mapping: 'eaal2', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminAddresLine3', mapping: 'eaal3', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminZipCode', mapping: 'eazc', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminCity', mapping: 'eacity', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminState', mapping: 'eas', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminCountry', mapping: 'eacountry', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminCurrency', mapping: 'eacurrency', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminVat', mapping: 'eavat', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminClusterTermId', mapping: 'eactid', index: true, type: Ext.data.Types.INT },
                    { name: 'ExactAdminClusterCode', mapping: 'eacc', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminMainPostbox', mapping: 'eamp', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminDisplay', mapping: 'ead', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminHasFinancialPeriods', mapping: 'eahfp', index: true, type: Ext.data.Types.BOOL}]

    , ExactFinYears: [{ name: 'ExactFinYearId', mapping: 'efyid', index: true, type: Ext.data.Types.INT },
                        { name: 'ExactFinYearAdminCode', mapping: 'efyac', index: true, type: Ext.data.Types.STRING },
                        { name: 'ExactFinYearNumber', mapping: 'efyn', index: true, type: Ext.data.Types.INT },
                        { name: 'ExactFinYearStartDate', mapping: 'efysd', index: true, type: Ext.data.Types.WCFDATE },
                        { name: 'ExactFinYearEndDate', mapping: 'efyed', index: true, type: Ext.data.Types.WCFDATE },
                        { name: 'ExactFinYearDisplay', mapping: 'efyd', index: true, type: Ext.data.Types.STRING}]

    , ClientEngagementLettersByPerson: [{ name: 'clientName', mapping: 'c', index: true, type: Ext.data.Types.STRING },
                        { name: 'clientGFIS', mapping: 'cg', index: true, type: Ext.data.Types.STRING },
                        { name: 'manager', mapping: 'm', index: true, type: Ext.data.Types.STRING },
                        { name: 'office', mapping: 'o', index: true, type: Ext.data.Types.STRING },
                        { name: 'partner', mapping: 'p', index: true, type: Ext.data.Types.STRING },
                        { name: 'status', mapping: 's', index: true, type: Ext.data.Types.STRING },
                        { name: 'template', mapping: 't', index: true, type: Ext.data.Types.STRING },
                        { name: 'startDate', mapping: 'sd', index: true, type: Ext.data.Types.WCFDATE },
                        { name: 'endDate', mapping: 'ed', index: true, type: Ext.data.Types.WCFDATE },
                        { name: 'dateELEA', mapping: 'de', index: true, type: Ext.data.Types.STRING },
                        { name: 'fileName', mapping: 'fn', index: true, type: Ext.data.Types.STRING },
                        { name: 'gth', mapping: 'gth', index: true, type: Ext.data.Types.STRING },
                        { name: 'itemId', mapping: 'iid', index: true, type: Ext.data.Types.STRING },
                        { name: 'clientId', mapping: 'cid', index: true, type: Ext.data.Types.STRING }
                        ]

    ,Statuses:[
        { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.INT },
        { name: 'Status', mapping: 'sn', type: Ext.data.Types.STRING },
        { name: 'Rank', mapping: 'rnk', type: Ext.data.Types.INT }
        ]

    ,Engagement: [
        { name: 'Code', mapping: 'id', index: true, type: Ext.data.Types.STRING },
        { name: 'CustomerId', mapping: 'cid', index: true, type: Ext.data.Types.STRING },
        { name: 'Description', mapping: 'd', type: Ext.data.Types.STRING },
        { name: 'Status', mapping: 's', type: Ext.data.Types.STRING },
        { name: 'ServiceLine', mapping: 'sl', type: Ext.data.Types.STRING },
        { name: 'ServiceLineDescription', mapping: 'sld', type: Ext.data.Types.STRING }
    ]

    ,CodaPoa:[
        { name: 'clientName', mapping: 'cn',  type: Ext.data.Types.STRING },
        { name: 'clientGfis', mapping: 'cg', type: Ext.data.Types.STRING },
        { name: 'bic', mapping: 'bic', type: Ext.data.Types.STRING },
        { name: 'lastModified', mapping: 'lm', type: Ext.data.Types.WCFDATE },
        { name: 'bankAccount', mapping: 'ba',index: true, type: Ext.data.Types.STRING },
        { name: 'bankName', mapping: 'bn', type: Ext.data.Types.STRING },
        { name: 'clientId', mapping: 'cid', type: Ext.data.Types.STRING },
        { name: 'status', mapping: 'sn',  type: Ext.data.Types.STRING },
        { name: 'statusId', mapping: 'sid',  type: Ext.data.Types.INT },
        { name: 'docstoreId', mapping: 'dsid', type: Ext.data.Types.STRING },
        { name: 'repositoryItemId', mapping: 'riid', type: Ext.data.Types.STRING },
        { name: 'EOL_EYLicense', mapping: 'eoll',  type: Ext.data.Types.STRING },
        { name: 'EOL_DigMailbox', mapping: 'eolm',  type: Ext.data.Types.STRING },
        { name: 'EOL_Division', mapping: 'eold',  type: Ext.data.Types.STRING },
        { name: 'statusModified', mapping: 'sm',  type: Ext.data.Types.WCFDATE },
        { name: 'allowCoda', mapping: 'ac',  type: Ext.data.Types.STRING },
        { name: 'activeClientACR', mapping: 'aca', type: Ext.data.Types.STRING }
        ]

    ,Log:[
        { name: 'actions', mapping: 'act', type: Ext.data.Types.STRING },
        { name: 'comment', mapping: 'cm', type: Ext.data.Types.STRING },
        { name: 'status', mapping: 'st',  type: Ext.data.Types.STRING },
        { name: 'timestamp', mapping: 'ts', type: Ext.data.Types.WCFDATE },
        { name: 'documentUploaded', mapping: 'du', type: Ext.data.Types.BOOL },
        { name: 'personName', mapping: 'pn', type: Ext.data.Types.STRING },
        { name: 'pdfAttachmentId', mapping: 'pi', type: Ext.data.Types.STRING }
    ]

    ,BankList:[  { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
        { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
        ]

    , ClientPersonFileServiceReport: [{ name: 'clientId', mapping: 'cid', index: true, type: Ext.data.Types.STRING },
        { name: 'clientName', mapping: 'c', index: true, type: Ext.data.Types.STRING },
        { name: 'clientGFIS', mapping: 'cg', index: true, type: Ext.data.Types.STRING },
        { name: 'statusId', mapping: 'sid', index: true, type: Ext.data.Types.INT },
        { name: 'status', mapping: 's', index: true, type: Ext.data.Types.STRING },
        { name: 'startDate', mapping: 'sd', index: true, type: Ext.data.Types.WCFDATE },
        { name: 'endDate', mapping: 'ed', index: true, type: Ext.data.Types.WCFDATE },
        { name: 'nextAnnualMeeting', mapping: 'nam', index: true, type: Ext.data.Types.WCFDATE },
        { name: 'manager', mapping: 'm', index: true, type: Ext.data.Types.STRING },
        { name: 'office', mapping: 'o', index: true, type: Ext.data.Types.STRING },
        { name: 'partner', mapping: 'p', index: true, type: Ext.data.Types.STRING },
        { name: 'person', mapping: 'pe', index: true, type: Ext.data.Types.STRING },
        { name: 'dateModified', mapping: 'dm', index: true, type: Ext.data.Types.WCFDATE },
        { name: 'daysOnStatus', mapping: 'dos', index: true, type: Ext.data.Types.STRING },
        { name: 'fileId', mapping: 'fid', index: true, type: Ext.data.Types.STRING },
        { name: 'fileName', mapping: 'f', index: true, type: Ext.data.Types.STRING }
    ]
};
//



eBook.data.BaseJsonStore = Ext.extend(Ext.data.JsonStore, {
    getRecordTypeDataObject: function(rec, baseObj) {
        var flds = this.reader.meta.fields;
        var obj = {};
        for (var i = 0; i < flds.length; i++) {
            obj[flds[i].mapping] = rec.get(flds[i].name);
            if (Ext.isString(obj[flds[i].mapping]) && Ext.isEmpty(obj[flds[i].mapping])) {
                obj[flds[i].mapping] = null;
            }
        }
        if (Ext.isDefined(baseObj)) Ext.apply(obj, baseObj);
        return obj;
    }
    , getJsonRecords: function(baseObj, validationFn) {
        var recs = [];

        this.each(function(rec) {
            if (!this.isRecordEmpty(rec)) {
                if (Ext.isFunction(validationFn)) {
                    if (validationFn(rec)) {
                        recs.push(this.getRecordTypeDataObject(rec, baseObj));
                    }
                } else {
                    recs.push(this.getRecordTypeDataObject(rec, baseObj));
                }
            }
        }, this);
        return recs;
    }
    , isRecordEmpty: function(rec) {
        var empty = true;
        for (var prop in rec.data) {
            if (rec.data.hasOwnProperty(prop)) {
                if (!Ext.isEmpty(rec.data[prop])) return false;
            }
        }
        return empty;
    }
});

eBook.data.JsonStore = Ext.extend(eBook.data.BaseJsonStore, {
    serviceUrl: eBook.Service.url
    , constructor: function(config) {
        if (config.serviceUrl) this.serviceUrl = config.serviceUrl;
        if (config.selectAction) {
            config.root = config.selectAction + 'Result';
            if (config.rootProp) config.root = config.root + '.' + config.rootProp;
            config.proxy = new eBook.data.WcfProxy({
                url: this.serviceUrl + config.selectAction
                , criteriaParameter: config.criteriaParameter
                , method: 'POST'
            });
            config.proxy.enableFileCached = config.enableFileCached;
        }
        eBook.data.JsonStore.superclass.constructor.call(this, config);
    }
    , loadRecords: function(o, options, success) {
        eBook.data.JsonStore.superclass.loadRecords.apply(this, arguments);
    }
});
Ext.reg('eb-jsonstore', eBook.data.JsonStore);

eBook.data.GroupedJsonStore = Ext.extend(Ext.data.GroupingStore, {
    serviceUrl: eBook.Service.url
    , constructor: function(config) {
        if (config.serviceUrl) this.serviceUrl = config.serviceUrl;
        if (config.selectAction) {
            config.root = config.selectAction + 'Result';

            config.proxy = new eBook.data.WcfProxy({
                url: this.serviceUrl + config.selectAction
                , criteriaParameter: config.criteriaParameter
                , method: 'POST'
                , enableFileCached: config.enableFileCached
            });
            config.proxy.enableFileCached = config.enableFileCached;
        }
        eBook.data.GroupedJsonStore.superclass.constructor.call(this, Ext.apply(config, {
            reader: new Ext.data.JsonReader(config)
        }));
    }
    , loadRecords: function(o, options, success) {
        eBook.data.GroupedJsonStore.superclass.loadRecords.apply(this, arguments);
    }
});


eBook.data.PagedJsonStore = Ext.extend(Ext.data.JsonStore, {
    serviceUrl: eBook.Service.url
    , constructor: function(config) {
        if (config.serviceUrl) this.serviceUrl = config.serviceUrl;
        if (config.selectAction) {
            config.root = config.selectAction + 'Result.Data';
            config.totalProperty = config.selectAction + 'Result.Total',
            config.paramNames = {
                start: 'Start'
                 , limit: 'Limit'
            };
            config.proxy = new eBook.data.WcfProxy({
                url: this.serviceUrl + config.selectAction
                , criteriaParameter: config.criteriaParameter
                , method: 'POST'
            });
            config.proxy.enableFileCached = config.enableFileCached;
        }
        eBook.data.PagedJsonStore.superclass.constructor.call(this, config);
    }
    , load: function(options) {
        eBook.data.PagedJsonStore.superclass.load.call(this, options);
    }
});eBook.History = {
    maxLength: 5
    , cookieName: 'eBook.FileHistory'
    , list: []
    , store: new Ext.data.ArrayStore({
        idIndex: 4
        , fields: eBook.data.RecordTypes.FileHistory
    })
    , addFile: function(fileRec, clientRec) {
        var idx = this.searchFile(fileRec.get('Id'));
        if (idx > -1) this.list.splice(idx);
        if (this.list.length >= 5) this.list.splice(4);
        var frec = [
            clientRec.get('Id')
            , clientRec.get('GFISCode')
            , clientRec.get('Name')
            , fileRec.get('Id')
            , fileRec.get('Name')
            , fileRec.get('StartDate')
            , fileRec.get('EndDate')
            , new Date()
        ];
        this.list.unshift({});
        this.list[0] = frec;
        this.save();
    }
    , searchFile: function(fid) {
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i][3] == fid) return i;
        }
        return -1;
    }
    , load: function() {
        this.list = Ext.decode(Ext.util.Cookies.get(this.cookieName));
        if (this.list == null) this.list = [];
        this.store.loadData(this.list);
    }
    , save: function() {
        Ext.util.Cookies.set(this.cookieName, Ext.encode(this.list));
        //Ext.util.Cookies.set(this.cookieName, Ext.encode([]));
        this.store.loadData(this.list);
    }
};

eBook.TabPanel.CheckTabPanel = Ext.extend(Ext.TabPanel, {
    initComponent: function() {
        this.itemTpl = new Ext.Template(
                 '<li class="{cls}" id="{id}"><a class="x-tab-strip-close"></a>',

                 '<a class="x-tab-right" href="#"><em class="x-tab-left">',
                 '',
                 '<span class="x-tab-strip-inner"><span class="x-tab-strip-text {iconCls}"><input type="checkbox" class="x-tab-check"/> <b>{text}</b></span></span>',
                 '</em></a>',
                 '</li>'
            );
        this.itemTpl.disableFormats = true;
        this.itemTpl.compile();
        eBook.TabPanel.CheckTabPanel.superclass.initComponent.apply(this, arguments);
    }
    , hasEnabledTabs: function() {
        var hase = false;
        this.items.each(function(pnl) {
            hase = hase || !pnl.disabled;
        }, this);
        return hase;
    }
    , findTargets: function(e) {
        var item = null,
            itemEl = e.getTarget('li:not(.x-tab-edge)', this.strip),
            itemCheckEl = e.getTarget('.x-tab-check', this.strip);

        if (itemEl) {
            item = this.getComponent(itemEl.id.split(this.idDelimiter)[1]);
            if (itemCheckEl) {
                itemCheckEl.checked = !itemCheckEl.checked;
                if (itemCheckEl.checked) {
                    item.enable();
                } else {
                    item.disable();
                }
            } else {
                if (item.disabled) {
                    return {
                        close: null,
                        item: null,
                        el: null
                    };
                }
            }
        }
        return {
            close: e.getTarget('.x-tab-strip-close', this.strip),
            check: itemCheckEl,
            item: item,
            el: itemEl
        };
    },
    onItemTitleChanged: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).child('span.x-tab-strip-text>b', true).innerHTML = item.title;
        }
    },
    // private
    onStripMouseDown: function(e) {
        if (e.button !== 0) {
            return;
        }
        e.preventDefault();
        var t = this.findTargets(e);
        if (t.close) {
            if (t.item.fireEvent('beforeclose', t.item) !== false) {
                t.item.fireEvent('close', t.item);
                this.remove(t.item);
            }
            return;
        }
        if (t.item && t.item != this.activeTab) {
            this.setActiveTab(t.item);
        }
    }
});

Ext.reg('eBook.TabPanel.CheckTabPanel', eBook.TabPanel.CheckTabPanel);

eBook.Fields.YesNoField = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            store: [Ext.MessageBox.buttonText.yes, Ext.MessageBox.buttonText.no]
            , typeAhead: false
            , triggerAction: 'all'
        });
        eBook.Fields.YesNoField.superclass.initComponent.apply(this, arguments);
    }
    , getValue: function() {
        var val = eBook.Fields.YesNoField.superclass.getValue.call(this);
        switch (val) {
            case Ext.MessageBox.buttonText.yes:
                return true;
            case Ext.MessageBox.buttonText.no:
                return false;
        }
        return null;
    }
    , setValue: function(val) {
        if (Ext.isBoolean(val)) {
            if (val) {
                eBook.Fields.YesNoField.superclass.setValue.call(this, Ext.MessageBox.buttonText.yes);
            } else {
                eBook.Fields.YesNoField.superclass.setValue.call(this, Ext.MessageBox.buttonText.no);
            }
        } else if (val == Ext.MessageBox.buttonText.yes || val == Ext.MessageBox.buttonText.no) {
            eBook.Fields.YesNoField.superclass.setValue.call(this, val);
        } else {
            this.clearValue();
        }
    }
});

Ext.reg('yesno', eBook.Fields.YesNoField);

eBook.Fields.LanguageBox = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            store: eBook.Language.LanguagesList
            , typeAhead: false
            , mode: 'local'
            , selectOnFocus: true
            , autoSelect: true
            , editable: false
            , forceSelection: true
            , triggerAction: 'all'
        });
        eBook.Fields.LanguageBox.superclass.initComponent.apply(this, arguments);
    }
    , getSelectedDisplay: function() {
        var idx = this.store.findExact(this.valueField, this.getValue());
        var rec = this.store.getAt(idx);
        return rec.get(this.displayField);
    }
});

Ext.reg('languagebox',eBook.Fields.LanguageBox);


eBook.Fields.RepositoryCategory = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            store: eBook.Repository.CategoriesList
            , typeAhead: false
            , mode: 'local'
            , selectOnFocus: true
            , autoSelect: true
            , editable: false
            , forceSelection: true
            , triggerAction: 'all'
        });
        eBook.Fields.RepositoryCategory.superclass.initComponent.apply(this, arguments);
    }
    , getSelectedDisplay: function() {
        var idx = this.store.findExact(this.valueField, this.getValue());
        var rec = this.store.getAt(idx);
        return rec.get(this.displayField);
    }
});

Ext.reg('repository-category', eBook.Fields.RepositoryCategory);

eBook.Fields.IconLabelField = Ext.extend(Ext.BoxComponent, {
    iconCls: ''
    , text: ''
    , autoEl: 'div'
    , cls: ''
    , itemStyle: ''
    , onRender: function(ct, position) {
        eBook.Fields.IconLabelField.superclass.onRender.call(this, ct, position);
        this.el.addClass('eBook-fields-iconlabelfield');
        if (this.cls != '') this.el.addClass(this.cls);
        if(this.itemStyle!='') this.el.setStyle(this.itemStyle);
        if (this.iconCls != '') this.el.addClass(this.iconCls);
        this.el.update(this.text);
    }
    , setIconCls: function(cls) {
        this.el.replaceClass(this.iconCls, cls);
        this.iconCls = cls;
    }
    , setText: function(txt) {
        this.el.update(txt);
        this.text = txt;
    }
    , update: function(txt, iconCls) {
        this.setIconCls(iconCls);
        this.setText(txt);
    }
});

Ext.reg('iconlabelfield', eBook.Fields.IconLabelField);

eBook.Fields.LegalType = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: false
            , width: 300
            ,autoSelect :true
            ,listWidth:300
            , forceSelection: true
            , hideTrigger: false
            , typeAhead: false
            , disableKeyFilter: true
            , editable: false
            , enableKeyEvents: false
            ,triggerAction :'all'
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'listdesc'
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetLegalTypes'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'ccdc'
                        , baseParams: { Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.LegalTypes
                        //, queryParam: 'Query'
            })
        });
        eBook.Fields.LegalType.superclass.initComponent.apply(this, arguments);
    }
//    , getContract: function() {
//        var rec = this.findRecord(this.valueField, this.getValue());
//        if (rec) {
//            return rec.json;
//        } else {
//            return null;
//        }
//    }
//    , setValue: function(v) {
//        if (typeof v == "object") {
//            this.setContract(v);
//            return;
//        }
//        eBook.Fields.LegalType.superclass.setValue.call(this, v);
//    }
//    , setContract: function(obj) {
//    eBook.Fields.LegalType.superclass.setValue.call(this, obj[this.valueField]);
//    }
});

Ext.reg('legaltype', eBook.Fields.LegalType);
eBook.Fields.BankDetails = Ext.extend(Ext.form.FieldSet, {
    initComponent: function() {
        Ext.apply(this, {
        //hideLabel:true,
            autoScroll: false,
            checkboxToggle: true,
            items: [{ xtype: 'textfield', ref: 'bic', fieldLabel: 'BIC', allowBlank: false }
                , { xtype: 'textfield', ref: 'iban', fieldLabel: 'IBAN', allowBlank: true}]
        });
        eBook.Fields.BankDetails.superclass.initComponent.apply(this, arguments);
    }
    , clearInvalid: function() {
        this.bic.clearInvalid();
        this.iban.clearInvalid();
    }
    , isValid: function(prevMark) {
        this.clearInvalid();
        if (this.isChecked()) {
            var v = this.getValue();
            var valid = true;
            if (Ext.isEmpty(v.bic)) {
                this.bic.markInvalid();
                valid = false;
            }
            if (Ext.isEmpty(v.iban)) {
                this.iban.markInvalid();
                valid = false;
            }
            return valid;
        }
        return true;
    }
     , getValue: function() {
         return {
             act: this.isChecked()
            , bic: this.bic.getValue()
            , iban: this.iban.getValue()
         };
     }
    , setValue: function(v) {
        if (v) {
            this.bic.setValue(v.bic ? v.bic : '');
            this.iban.setValue(v.iban ? v.iban : '');
            if (v.act != null && typeof v.act != "undefined" && this.checkbox) { this.checkbox.dom.checked = v.act; }
        }
    }
    , isChecked: function() {
        if (this.checkbox) { return this.checkbox.dom.checked; }
        return true;
    }
});

Ext.reg('bankdetails', eBook.Fields.BankDetails);
eBook.Fields.Email = Ext.extend(Ext.form.TextField, {
    initComponent: function() {
        /*
        Ext.apply(this, {
        });
        */
        eBook.Fields.Email.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('email', eBook.Fields.Email);    
eBook.Fields.FodAddress = Ext.extend(Ext.form.FieldSet, {
    initComponent: function() {
        Ext.apply(this, {
        //hideLabel:true,
            autoScroll:false,
            items: [{ xtype: 'fod.addresstype', ref: 'addresstype', fieldLabel: eBook.Fields.FodAddress_Adrestype, allowBlank: false }
                , {
                    xtype: 'compositefield',
                    //fieldLabel: 'Address',
                    msgTarget: 'side',
                    ref: 'address',
                    // anchor: '-20',
                    items: [{ xtype: 'textfield', ref: 'street', fieldLabel: eBook.Fields.FodAddress_Street, width: 250, allowBlank: false }
                            , { xtype: 'textfield', ref: 'nr', fieldLabel: eBook.Fields.FodAddress_Nr, width: 50, allowBlank: false }
                            , { xtype: 'textfield', ref: 'bus', fieldLabel: eBook.Fields.FodAddress_Box, width: 50 }
                            ]
                }
                , { xtype: 'fod.country', ref: 'country', fieldLabel: eBook.Fields.FodAddress_Country, allowBlank: false }
                , { xtype: 'fod.zipcode', ref: 'zipcode', fieldLabel: eBook.Fields.FodAddress_Zipcode, allowBlank: false }
                , { xtype: 'textfield', ref: 'city', fieldLabel: eBook.Fields.FodAddress_City, allowBlank: true}]
        });
        eBook.Fields.FodAddress.superclass.initComponent.apply(this, arguments);
    }
    , isValid: function(prevmark) {

        var t =this.addresstype.isValid(prevmark)
            || this.address.innerCt.street.isValid(prevmark)
            || this.address.innerCt.nr.isValid(prevmark)
            || this.address.innerCt.bus.isValid(prevmark)
            || this.country.isValid(prevmark)
            || this.zipcode.isValid(prevmark)
            || this.city.isValid(prevmark);
        
        return this.addresstype.isValid(prevmark)
            && this.address.innerCt.street.isValid(prevmark)
            && this.address.innerCt.nr.isValid(prevmark)
            && this.address.innerCt.bus.isValid(prevmark)
            && this.country.isValid(prevmark)
            && this.zipcode.isValid(prevmark)
            && this.city.isValid(prevmark);
    }
    , getValue: function() {
        return {
            at: this.addresstype.getContract()
            , s: this.address.innerCt.street.getValue()
            , n: this.address.innerCt.nr.getValue()
            , b: this.address.innerCt.bus.getValue()
            , co: this.country.getContract()
            , z: this.zipcode.getContract()
            , ci: this.city.getValue()
        };
    }
    , setValue: function(v) {
        if (v) {
            this.addresstype.setValue(v.at ? v.at : '');
            this.address.innerCt.street.setValue(v.s ? v.s : '');
            this.address.innerCt.nr.setValue(v.n ? v.n : '');
            this.address.innerCt.bus.setValue(v.b ? v.b : '');
            this.country.setValue(v.co ? v.co : '');
            this.zipcode.setValue(v.z ? v.z : '');
            this.city.setValue(v.ci ? v.ci : '');
        }
    }
});

Ext.reg('fod.address', eBook.Fields.FodAddress);
eBook.Fields.FodAddressType = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: false
            , width: 300
            ,listWidth:300
            , forceSelection: true
            , hideTrigger: false
            , typeAhead: false
            , disableKeyFilter: true
            , editable: false
            , enableKeyEvents: false
            ,triggerAction :'all'
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'name'
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetFodList'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'cfldc'
                        , baseParams: { ListId: 'ADDRESSTYPE', Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.FodList
            })
        });
        eBook.Fields.FodAddressType.superclass.initComponent.apply(this, arguments);
    }
    , getContract: function() {
        var rec = this.findRecord(this.valueField, this.getValue());
        if (rec) {
            return rec.json;
        } else {
            return null;
        }
    }
    , setValue: function(v) {
        if (typeof v == "object") {
            this.setContract(v);
            return;
        }
        eBook.Fields.FodAddressType.superclass.setValue.call(this, v);
    }
    , setContract: function(obj) {
        eBook.Fields.FodAddressType.superclass.setValue.call(this, obj[this.valueField]);
    }
});

Ext.reg('fod.addresstype', eBook.Fields.FodAddressType);
eBook.Fields.FodContactType = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: false
            , width: 300
            ,autoSelect :true
            ,listWidth:300
            , forceSelection: true
            , hideTrigger: false
            , typeAhead: false
            , disableKeyFilter: true
            , editable: false
            , enableKeyEvents: false
            ,triggerAction :'all'
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'name'
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetFodList'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'cfldc'
                        , baseParams: { ListId: 'CONTACTTYPE', Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.FodList
                        , queryParam: 'Query'
            })
        });
        eBook.Fields.FodContactType.superclass.initComponent.apply(this, arguments);
    }
    , getContract: function() {
        var rec = this.findRecord(this.valueField, this.getValue());
        if (rec) {
            return rec.json;
        } else {
            return null;
        }
    }
    , setValue: function(v) {
        if (typeof v == "object") {
            this.setContract(v);
            return;
        }
        eBook.Fields.FodContactType.superclass.setValue.call(this, v);
    }
    , setContract: function(obj) {
        eBook.Fields.FodContactType.superclass.setValue.call(this, obj[this.valueField]);
    }
});

Ext.reg('fod.contacttype', eBook.Fields.FodContactType);
eBook.Fields.FodCountry = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: true
            , forceSelection: true
            , hideTrigger: true
            , mode: 'local'
            , width: 200
            , valueField: 'id'
            , hideTrigger: true
            , displayField: 'name'
            , queryParam: 'Query'
            , queryDelay: 10
            , minChars:2
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetFodList'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'cfldc'
                        , baseParams: { ListId: 'COUNTRY', Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.FodList
            })
        });
        eBook.Fields.FodCountry.superclass.initComponent.apply(this, arguments);
    }
    , getContract: function() {
        var rec = this.findRecord(this.valueField, this.getValue());
        if (rec) {
            return rec.json;
        } else {
            return null;
        }
    }
    , setValue: function(v) {
        if (typeof v == "object") {
            this.setContract(v);
            return;
        }
        eBook.Fields.FodCountry.superclass.setValue.call(this, v);
    }
    , setContract: function(obj) {
        eBook.Fields.FodCountry.superclass.setValue.call(this, obj[this.valueField]);
    }
});

Ext.reg('fod.country', eBook.Fields.FodCountry);
eBook.Fields.FodPhone = Ext.extend(Ext.form.FieldSet, {
    initComponent: function() {
        Ext.apply(this, {
            items: [
            //{ xtype: 'textfield', ref: 'phonefax', fieldLabel: 'Phone/Fax' }
                {xtype: 'compositefield', msgTarget: 'side', ref: 'countryzone', allowBlank: false
                    , items: [{ xtype: 'textfield', ref: 'country', fieldLabel: eBook.Fields.FodPhone_Country, value: '+32', width: 40, allowBlank: false }
                            , { xtype: 'textfield', ref: 'zone', fieldLabel: eBook.Fields.FodPhone_Zone, width: 40, allowBlank: false }
                          ]
            }
                , { xtype: 'textfield', ref: 'local', fieldLabel: eBook.Fields.FodPhone_Local, allowBlank: false }
                 , { xtype: 'textfield', ref: 'extension', fieldLabel: eBook.Fields.FodPhone_Extension, allowBlank: true }
             ]
        });
        eBook.Fields.FodPhone.superclass.initComponent.apply(this, arguments);
    }
    , clearInvalid: function() {
        this.countryzone.innerCt.country.clearInvalid();
        this.countryzone.innerCt.zone.clearInvalid();
        this.local.clearInvalid();
    }
    , isValid: function(prevMark) {
        this.clearInvalid();
        if (this.isChecked()) {
            var v = this.getValue();
            var valid = true;
            if (Ext.isEmpty(v.cc)) {
                this.countryzone.innerCt.country.markInvalid();
                valid = false;
            }
            if (Ext.isEmpty(v.z)) {
                this.countryzone.innerCt.zone.markInvalid();
                valid = false;
            }
            if (Ext.isEmpty(v.l)) {
                this.local.markInvalid();
                valid = false;
            }
            return valid;
        }
        return true;
    }
    , getValue: function() {
        return {
            act: this.isChecked()
            , cc: this.countryzone.innerCt.country.getValue()
            , z: this.countryzone.innerCt.zone.getValue()
            , l: this.local.getValue()
            , e: this.extension.getValue()
        };
    }
    , setValue: function(v) {
        if (v) {
            this.countryzone.innerCt.country.setValue(v.cc ? v.cc : '');
            this.countryzone.innerCt.zone.setValue(v.z ? v.z : '');
            this.local.setValue(v.l ? v.l : '');
            this.extension.setValue(v.e ? v.e : '');
            if (v.act != null && typeof v.act != "undefined" && this.checkbox) { this.checkbox.dom.checked = v.act; }
        }
    }
    , isChecked: function() {
        if (this.checkbox) { return this.checkbox.dom.checked; }
        return true;
    }
});

Ext.reg('fod.phone', eBook.Fields.FodPhone);
eBook.Fields.FodZipCode = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: true
            , forceSelection: true
            , allowBlank: false
            , hideTrigger: false
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'name'
            , width: 70
            , queryParam: 'Query'
            , queryDelay: 0
            , minChars: 0
            , store: new eBook.data.JsonStore({
                selectAction: 'GetFodList'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'cfldc'
                        , baseParams: { ListId: 'ZIPCODE', Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.FodList

            })
        });
        eBook.Fields.FodZipCode.superclass.initComponent.apply(this, arguments);
    }
    , getContract: function() {
        var rec = this.findRecord(this.valueField, this.getValue());
        if (rec) {
            return rec.json;
        } else {
            return null;
        }
    }
    , setValue: function(v) {
        if (typeof v == "object") {
            this.setContract(v);
            return;
        }
        eBook.Fields.FodZipCode.superclass.setValue.call(this, v);
    }
    , setContract: function(obj) {
        eBook.Fields.FodZipCode.superclass.setValue.call(this, obj[this.valueField]);
    }
});

Ext.reg('fod.zipcode', eBook.Fields.FodZipCode);
eBook.Fields.TaxPartners = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: false
            , width: 300
            , autoSelect: true
            , listWidth: 300
            , forceSelection: true
            , hideTrigger: false
            , typeAhead: false
            , disableKeyFilter: true
            , editable: false
            , enableKeyEvents: false
            , triggerAction: 'all'
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'name'
            , store: new eBook.data.JsonStore({
                selectAction: 'GetPartners'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'ciddc'
                        , baseParams: { Id: eBook.Interface.currentClient.get('Id'), Department:'TAX' }
                        , fields: eBook.data.RecordTypes.Partners
                //, queryParam: 'Query'
            })
        });
        eBook.Fields.TaxPartners.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('TaxPartners', eBook.Fields.TaxPartners);
eBook.Language.Languages = {
    nl_BE: eBook.Language_Dutch
    , fr_FR: eBook.Language_French
    , de_DE: eBook.Language_German
    , en_US: eBook.Language_English
};

eBook.grid.ColumnRenders = {
    Account: function(value, metaData, record, rowIndex, colIndex, store) {
        return record.get('nr') + ' ' + record.get('description');
    }
    , HasValueCheck: function(value, metaData, record, rowIndex, colIndex, store) {
        var wtid = this.editor.worksheetTypeId;
        var fnc = eBook.getMapping(wtid, 'LASTYEAR');
     
        if (fnc(record.json)) {
            metaData.css += 'eBook-mapping-lastyear';
            
        }
        if (store.detailed) {
            if (value != null && value != '') {
                return record.get(this.dataIndex + '_name'); // to be replaced with textual value
            }
        } else {
            if (value != null && value != '') {
                return '<img src="images/icons/16/tick.png" alt="" widht="16" height="16" style="margin:auto;"/>';
            }
        }
        return '';
    }
    , Language: function(value, metaData, record, rowIndew, colIndex, store) {
        return eBook.Language.Languages[value.replace('-', '_')];
    }
};

eBook.grid.numberColumn = Ext.extend(Ext.grid.Column, {
    emptyIfZero: false,
    format: '0,000.00',
    align:'right',
    constructor: function(cfg) {
        eBook.grid.numberColumn.superclass.constructor.call(this, cfg);
        this.renderer = Ext.util.Format.numberRenderer(this.format);
        var emptyIfZero = this.emptyIfZero;
        var format = this.format;
        this.renderer = function(v) {
            if (v == 0 && emptyIfZero) return "";
            return Ext.util.Format.number(v, format);
        };
    }

});

Ext.grid.Column.types.ebooknumbercolumn= eBook.grid.numberColumn;
Ext.reg('ebooknumbercolumn', eBook.grid.numberColumn);

eBook.grid.percentColumn = Ext.extend(Ext.grid.Column, {
    emptyIfZero: false,
    format: '0,000.00%',
    align: 'right',
    constructor: function(cfg) {
        eBook.grid.percentColumn.superclass.constructor.call(this, cfg);
        this.renderer = Ext.util.Format.numberRenderer(this.format);
        var emptyIfZero = this.emptyIfZero;
        var format = this.format;
        this.renderer = function(v) {
            if (v == 0 && emptyIfZero) return "";
            return Ext.util.Format.number(v, format);
        };
    }

});

Ext.grid.Column.types.ebookpercentcolumn = eBook.grid.percentColumn;
Ext.reg('ebookpercentcolumn', eBook.grid.percentColumn);
    eBook.grid.PagedGridPanel = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        this.store = new eBook.data.PagedJsonStore(this.storeConfig);
        Ext.apply(this.pagingConfig, {
            store: this.store
        });
        
        Ext.apply(this, {
            bbar: new Ext.PagingToolbar(this.pagingConfig)
        });
        eBook.grid.PagedGridPanel.superclass.initComponent.apply(this, arguments);
    }
});
eBook.grid.GridViewNote = Ext.extend(Ext.grid.GridView, {
    enableRowBody: true,
    initData: function(ds, cm) {
        
        eBook.grid.GridViewNote.superclass.initData.apply(this, arguments);
        for (var i = 0; i < this.cm.config.length; i++) {
            if (this.cm.config[i].isNote) break;
        }
        this.noteCm = this.cm.config[i];
        this.cm.config.splice(i, 1);
        this.cm = cm;
    },
    initTemplates: function() {
        
        var ts = this.templates || {};

        if (!ts.note) {
            ts.note = new Ext.Template(
            //'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="{style}" tabIndex="0" {cellAttr}>',
                    '<b>{header}:</b><i>{value}</i>'
            //'</td>'
                    );
        }

        if (!ts.row) {
            ts.row = new Ext.XTemplate(
                '<div class="x-grid3-row {alt}" style="{tstyle}"><table class="x-grid3-row-table" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
                '<tbody><tr>{cells}</tr>',
                '<tr class="x-grid3-row-body-tr eBook-gridviewnotes-body {[values.noteColumn.hidden ? "eBook-hidden" : ""]}" style="{bodyStyle}"><td colspan="{cols}" class="x-grid3-body-cell" tabIndex="0" hidefocus="on">{body}</td></tr>',
                '</tbody></table></div>'
            );
        }

        this.templates = ts;
        eBook.grid.GridViewNote.superclass.initTemplates.call(this);
    },
    doRender: function(columns, records, store, startRow, colCount, stripe) {
        
        var templates = this.templates,
            cellTemplate = templates.cell,
            noteTemplate = templates.note,
            rowTemplate = templates.row,
            last = colCount - 1;

        var tstyle = 'width:' + this.getTotalWidth() + ';';

        // buffers
        var rowBuffer = [],
            colBuffer = [],
            rowParams = { tstyle: tstyle },
            meta = {},
            column,
            noteColumn = {},
            record;

        //build up each row's HTML
        for (var j = 0, len = records.length; j < len; j++) {
            record = records[j];
            colBuffer = [];

            var rowIndex = j + startRow;

            //build up each column's HTML
            for (var i = 0; i < colCount; i++) {
                column = columns[i];

                meta.id = column.id;
                meta.css = i === 0 ? 'x-grid3-cell-first ' : (i == last ? 'x-grid3-cell-last ' : '');
                meta.attr = meta.cellAttr = '';
                meta.style = column.style;
                meta.value = column.renderer.call(column.scope, record.data[column.name], meta, record, rowIndex, i, store);

                if (Ext.isEmpty(meta.value)) {
                    meta.value = '&#160;';
                }

                if (this.markDirty && record.dirty && Ext.isDefined(record.modified[column.name])) {
                    meta.css += ' x-grid3-dirty-cell';
                }

                colBuffer[colBuffer.length] = cellTemplate.apply(meta);

            }

            if (this.noteCm) {
                noteColumn.id = this.noteCm.id;
                noteColumn.css = this.noteCm.css;
                noteColumn.attr = this.noteCm.cellAttr = '';
                noteColumn.style = this.noteCm.style;
                noteColumn.value = this.noteCm.renderer.call(this.noteCm.scope, record.data[this.noteCm.dataIndex], noteColumn, record, rowIndex, -1, store);
                noteColumn.header = this.noteCm.header;
                noteColumn.hidden = false;
                if (Ext.isEmpty(noteColumn.value) || noteColumn.value=="&#160;") {
                    noteColumn.hidden = true;
                }

                if (this.markDirty && record.dirty && Ext.isDefined(record.modified[noteColumn.dataIndex])) {
                    noteColumn.css += ' x-grid3-dirty-cell';
                }
            }
            //set up row striping and row dirtiness CSS classes
            var alt = [];

            if (stripe && ((rowIndex + 1) % 2 === 0)) {
                alt[0] = 'x-grid3-row-alt';
            }

            if (record.dirty) {
                alt[1] = ' x-grid3-dirty-row';
            }

            rowParams.cols = colCount;

            if (this.getRowClass) {
                alt[2] = this.getRowClass(record, rowIndex, rowParams, store);
            }

            rowParams.alt = alt.join(' ');
            rowParams.cells = colBuffer.join('');
            rowParams.noteColumn = noteColumn;
            rowParams.body = noteTemplate.apply(noteColumn);
            rowBuffer[rowBuffer.length] = rowTemplate.apply(rowParams);
        }

        return rowBuffer.join('');
    }
});

eBook.grid['gridviewnotes'] = eBook.grid.GridViewNote;/* 'beforeload': {
                                fn: this.maskLists
                                , scope: this
                            }
                            ,
*/

eBook.GTH.ClientList = Ext.extend(Ext.Panel, {
    initComponent: function() {
        var pers = '';
        if (this.statusType != 'TODO') {
            pers = '<div class="eb-gth-client-actor">{[this.getLastUpdate(values.LastUpdate)]}: {PersonName}</div>';
        }
        this.template = new Ext.XTemplate(
                '<tpl for=".">',
                    '<div class="eb-gth-client">',
                    '<div>{ClientName}',
                    '<tpl if="values.StartDate!=null && values.EndDate!=null "><br/><i>{[this.getPeriod(values)]}</i></tpl>',
                    '</div>',
                    pers,
                    '</div>',
                '</tpl>',
                '<div class="x-clear"></div>', {
                        getLastUpdate: function(value) { var dte = Ext.data.Types.WCFDATE.convert(value); return dte.format("d/m/Y H:i:s"); }
                        ,getPeriod:function(values) { var st = Ext.data.Types.WCFDATE.convert(values.StartDate), ed=Ext.data.Types.WCFDATE.convert(values.EndDate);return st.format("d/m/Y") + ' - ' +  ed.format("d/m/Y");}
                }
            );
        this.template.compile();
        Ext.apply(this, {
            html: '<div class="eb-gth-client-list"></div>'
            , title: this.baseTitle
            , autoScroll: true
            , bodyStyle:'background-color:#FFF;'
        });
        eBook.GTH.ClientList.superclass.initComponent.apply(this, arguments);
    }
    , onRender: function(ct, position) {
        eBook.GTH.ClientList.superclass.onRender.apply(this, arguments);
        //        if (this.ownerCt.store && this.ownerCt.store.getCount() > 0) {
        //            
        //        }
        this.dataEl = this.el.child('eb-gth-client-list');
        if (this.startDta) {
            this.updateList(this.startDta);
        }
    }
    , updateList: function(dta) {
        if (this.rendered) {
            this.startDta = null;
            this.template.overwrite(this.body, dta, true);
            this.setTitle(this.baseTitle + ' (' + dta.length + ')');
        } else {
            this.startDta = dta;
        }
    }
});

Ext.reg('GTHClientList', eBook.GTH.ClientList);

eBook.GTH.TeamAccordion = Ext.extend(Ext.Panel, {
    initComponent: function() {
        var accordionItems = [{ xtype: 'GTHClientList', baseTitle: 'Todo', teamId: this.teamId, statusType: 'TODO', ref: 'todos' }
                             , { xtype: 'GTHClientList', baseTitle: 'In Progress', teamId: this.teamId, statusType: 'OTHER', ref: 'progress' }
                             , { xtype: 'GTHClientList', baseTitle: 'Finalized', teamId: this.teamId, statusType: 'FINAL', ref: 'finals'}];
        /*if (this.teamId == 6) {
            accordionItems = [{ xtype: 'GTHClientList', baseTitle: 'Clients', teamId: this.teamId, statusType: 'TODO', ref: 'todos'}];
        }*/

        Ext.apply(this, {
            layout: 'accordion',
            border: false,
            bodyStyle: 'background-color:#FFF;',
            items: accordionItems
        });
        //        this.store = new eBook.data.JsonStore({
        //            selectAction: 'GetClientList'
        //                        , serviceUrl: eBook.Service.GTH
        //                        , autoLoad: true
        //                        , autoDestroy: true
        //                        , criteriaParameter: 'ctdc'
        //                        , baseParams: { TeamId: this.teamId, StatusList: [] }
        //                        , listeners: {
        //                            'load': {
        //                                fn: this.updateLists
        //                                , scope: this
        //                            }
        //                        }
        //                        , fields: eBook.data.RecordTypes.GTHClientsList
        //        });
        eBook.GTH.TeamAccordion.superclass.initComponent.apply(this, arguments);

    }
    , updateLists: function() {

        this.loadData();
    }
    , afterRender: function(ct) {
        eBook.GTH.TeamAccordion.superclass.afterRender.apply(this, arguments);
        this.loadData();
    }
    , loadData: function() {
        if (this.pollTask) {
            //Ext.TaskMgr.stop(this.pollTask);
        }
        if (this.el.parent('.eb-gth-team-container').isVisible()) {
            this.getEl().mask('loading');
            Ext.Ajax.request({
                url: eBook.Service.GTH + 'GetClientStartList'
                , method: 'POST'
                , params: Ext.encode({ ctdc: { TeamId: this.teamId} })
                , callback: this.onLoadDataCallback
                , scope: this

            });
        }
    }
    , onLoadDataCallback: function(opts, success, resp) {
        if (success) {
            var dta = Ext.decode(resp.responseText);
            dta = dta.GetClientStartListResult;
            this.todos.updateList(dta.Todo);
            /*if (this.teamId != 6) {*/
                this.progress.updateList(dta.Progress);
                this.finals.updateList(dta.Final);
            /*}*/
            
        } else {
            this.todos.updateList([{ ClientName: 'test'}]);
            this.progress.updateList([]);
            this.finals.updateList([]);
        }
        this.getEl().unmask();
        //var def = parseInt(Math.random() * 10000);
        //alert(def);
        this.startUpdateTask();
    }
    , stopUpdateTask: function() {
        if (this.pollTask) {
            Ext.TaskMgr.stop(this.pollTask);
            this.pollTask = null;
        }
    }
    , startUpdateTask: function() {
        if (!this.pollTask) this.pollTask = Ext.TaskMgr.start({ run: this.loadData, scope: this, interval: 30000 });  //.defer(def, [this.pollTask]);

    }
});

Ext.reg('GTHTeamAccordion', eBook.GTH.TeamAccordion);eBook.CleanEnterprise = function(v) {
    if (Ext.isEmpty(v) || Ext.isEmpty(v.trim())) return 'NULL';
    v = v.replace('BE', '');
    v = v.replace(/^0+/, '');
    //0xxx.xxx.xxx
    while (v.length < 10) {
        v = '0' + v;
    }
    return v;
};

eBook.GTH.ClientSearch = Ext.extend(eBook.grid.PagedGridPanel, {
    statusList: ['TODO']
    //, getAllClients : false
    , initComponent: function() {
        var storeCfg = {
                        selectAction: 'GetClients', /*'GetAllClients',*/
                        fields: eBook.data.RecordTypes.GTHClientsList,
                        idField: 'Id',
                        criteriaParameter: 'cstdc',
                        baseParams: { TeamId: this.teamId, statusList: this.statusList, PersonId: eBook.User.personId },
                        serviceUrl: eBook.Service.GTH
                       };
        if (this.getAllClients){
            storeCfg = {
                        selectAction: 'GetAllClients', /*'GetAllClients',*/
                        fields: eBook.data.RecordTypes.GTHClientsList,
                        idField: 'Id',
                        criteriaParameter: 'cstdc',
                        baseParams: { TeamId: this.teamId, statusList: this.statusList, PersonId: eBook.User.personId },
                        serviceUrl: eBook.Service.GTH
                       };        
        } 
        
        Ext.apply(this, {
            title: "Search Client"
            , storeConfig: storeCfg
            , sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
            })
            , border: true
            //,bodyStyle:'margin-top:10px;'
            , viewConfig: { forceResize: true }
            , listeners: {
                'rowmousedown': {
                    fn: function(grid, rowIndex, e) {
                        var rec = grid.store.getAt(rowIndex);
                        this.selectClient(rec);
                    },
                    scope: this
                }
            }
            , tbar: []
            , pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            }
            , plugins: [new Ext.ux.grid.Search({
                ref: 'searchfield',
                iconCls: 'eBook-search-ico',
                minChars: 3,
                minLength: 3,
                autoFocus: true,
                position: 'top',
                mode: 'remote',
                width: 250,
                searchText: "Search Client",
                searchTipText: '',
                selectAllText: "Select all fields"
            })]
            , loadMask: true
			, columns: [
			    {
			        header: 'Client Name',
			        width: 300,
			        dataIndex: 'ClientName',
			        sortable: true
			    },
			    {
			        header: 'Company Nr',
			        width: 120,
			        dataIndex: 'ClientEnterprise',
			        renderer: function(v, m, rec, ridx, cidx, str) {
			            return eBook.CleanEnterprise(v);
			        },
			        sortable: true
			    }, {
			        header: 'Period',
			        width: 300,
			        dataIndex: 'StartDate',
			        renderer: function(v, m, rec, ridx, cidx, str) {
			            if(v && rec.get('EndDate')) {
			                return rec.get('StartDate').format('d/m/Y') + ' - ' + rec.get('EndDate').format('d/m/Y');
			            } 
			            return '';
			        }
			    }, {
			        header: 'Client GFIS',
			        width: 300,
			        dataIndex: 'ClientGfis',
			        sortable: true
}]
            , border: false
            , style: 'margin-top:10px'
        });
        eBook.GTH.ClientSearch.superclass.initComponent.apply(this, arguments);
    }
    , selectClient: function(rec) {
        this.ownerCt.selectClient(rec.get('Id'));
    }
});
Ext.reg('GTHClientSearch', eBook.GTH.ClientSearch);


eBook.GTH.ClientSelector = Ext.extend(Ext.Panel, {
searchButton: true
    ,getAllClients:false
    , initComponent: function() {
        var srchBtn = {};
        if (this.searchButton) {
            srchBtn = { xtype: 'panel', border: false, bodyStyle: 'text-align:center;padding:10px;', items: [{ xtype: 'button', ref: 'btnSrch', width: 400, style: 'margin:auto;font-weight:bold', text: 'Select first available client', handler: function() { this.selectClient(null); }, scope: this}], region: 'north', height: 60 }
        }
        Ext.apply(this, {
            layout: 'border'
            , items: [ srchBtn
                      , { xtype: 'GTHClientSearch', getAllClients:this.getAllClients,teamId: this.teamId, ref: 'srch', region: 'center' }
                      , { xtype: 'spacer', region: 'south', height: 20 }
                      ]
        });
        eBook.GTH.ClientSelector.superclass.initComponent.apply(this, arguments);
    }
    , selectClient: function(id) {
        this.getEl().mask('loading');
        Ext.Ajax.request({
            url: eBook.Service.GTH + 'SelectClient'
                    , method: 'POST'
                    , params: Ext.encode({ cctdc: { TeamId: this.teamId, GTHId: Ext.isEmpty(id) ? null : id, Status: this.selectStatus, PersonId: eBook.User.personId} })
                    , callback: this.onSelectClientCallback
                    , scope: this

        });
    }
    , onSelectClientCallback: function(opts, success, resp) {
        if (success) {
            var obj = Ext.decode(resp.responseText).SelectClientResult;
            this.ownerCt.selectClient(obj, this.stepId);
        } else {
            eBook.Interface.showResponseError(resp, "Select client");
        }
        this.getEl().unmask();
    }
    , initStep: function(owner) {
        owner.client = null;

        owner.showStop();
        this.srch.store.load();
    }
});


Ext.reg('GTHClientSelector', eBook.GTH.ClientSelector);/*
eBook.GTH.FileUpload = Ext.extend(Ext.ux.form.FileUploadField, {
    validateValue:function(v) {
        var valid = eBook.GTH.FileUpload.superclass.validateValue.call(this,v);
        this.periodEnd.format('ddMMY');
        var rexp = new RegExp(this.enterprisNr + '_' +  + '/gi')
    }
*/

eBook.GTH.Team1_Upload = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: { type: 'vbox', align: 'stretch' }
            , border: false
            , items: [{ xtype: 'panel', layout: { type: 'hbox', align: 'stretch' }, flex: 1, items: [{ xtype: 'GTHClientDetails', teamId: this.teamId, ref: '../clientDetails', height: 80, flex: 1 }, { xtype: 'GTHTeam4DownloadFiles', teamId: this.teamId, ref: '../downloadfiles', flex: 1}] }
                      , { xtype: 'GTHUploadNBB', teamId: this.teamId, ref: 'uploadNBB', flex: 1 }
                      , { xtype: 'button', text: 'Upload files', handler: this.onUploadClick, scope: this, height: 60, cls: 'eb-massive-btn' }
                      ]
        });
        eBook.GTH.Team1_Upload.superclass.initComponent.apply(this, arguments);
    }
    , initStep: function(owner) {
        owner.hideStop();
        var cl = owner.client;
        this.clientId = cl.Id;
        this.stepTitle = 'STEP 2: Download &amp; upload files for ' + cl.ClientName;
        this.clientDetails.initStep(cl);
        this.uploadNBB.reset(); //getForm().applyToFields({ originalValue: null });
        this.uploadNBB.setClient(owner.client);
        this.downloadfiles.getFiles(cl);
       // this.uploadNBB.getForm().reset();

    }
    , onUploadClick: function(e) {
        var f = this.uploadNBB.getForm();

        if (!this.uploadNBB.isValid()) {
            alert("Some required fields are invalid/missing");
            return;
        }
        f.isValid = function() { return true; };
        f.submit({
            url: 'UploadMultiple.aspx',
            waitMsg: 'Uploading files',
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function(fp, o) {
        this.getEl().mask("Validating &amp; processing files", 'x-mask-loading');
        var dta = this.uploadNBB.getData(o.result.files);
        Ext.Ajax.request({
            url: eBook.Service.GTH + 'ProcessFilesTeam1'
            , method: 'POST'
            , params: Ext.encode({ cpfdc: {  StructureId: '3787f2a4-112d-4589-a231-e9b95bdcfcd9', Files: dta, TeamId: this.teamId, Status: 'FINAL', PersonId: eBook.User.personId, GTHClientId: this.clientId} })
             , callback: this.handleFileProcessing
            , scope: this
        });
        //process o.result.file
        /*this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
       
        ¨*/

    }
    , failedUpload: function(fp, o) {
        eBook.Interface.showError(o.message, "UPLOAD FAILED!!")
        this.getEl().unmask();
    }
    , handleFileProcessing: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.ownerCt.moveNextStep();

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam1UploadFiles', eBook.GTH.Team1_Upload);


eBook.GTH.GTHClientDetails = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.templ = new Ext.XTemplate('<div class="gth-team1-downloadfiles">'
                                            , '<div class="gth-team1-downloadfiles-body">'
                                                , '<div>CLIENT: <b>{ClientName}</b></div>'
                                                , '<div>COMPANY NR: <b>{[eBook.CleanEnterprise(values.ClientEnterprise)]}</b>'
                                                , '<tpl if="values.StartDate!=null"><span style="padding-left:10px;">Period : <b>{StartDate:date("d/m/Y")} &gt; {EndDate:date("d/m/Y")}</b></span></tpl>'
                                                ,'</div>'
                                                
        // , '<i>Download the files from the NBB website (internet explore) and upload them here.</i>'
                                            , '</div>'
        // , '<div class="gth-bottomtoolbar">'
        //     , '<div class="gth-moveNext"></div>'
        // , '</div>'
                                           , ' </div>');
        this.templ.compile();
        Ext.apply(this, {
            html: 'test'
        });
        eBook.GTH.GTHClientDetails.superclass.initComponent.apply(this, arguments);
    }
     , initStep: function(cl) {
        if (cl.StartDate != null) cl.StartDate = Ext.data.Types.WCFDATE.convert(cl.StartDate);
        if (cl.EndDate != null) cl.EndDate = Ext.data.Types.WCFDATE.convert(cl.EndDate);
         this.body.update(this.templ.apply(cl));
     }
});        


Ext.reg('GTHClientDetails', eBook.GTH.GTHClientDetails);


eBook.GTH.GTHUploadNBB = Ext.extend(Ext.form.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            fileUpload: true,
            border: false,
            bodyStyle: 'padding:10px;',
            autoScroll: true,
            layout: 'column',
            items: [{ xtype: 'GTHNbbFrameSet', ref: 'period1', title: 'Period 1', checkboxName: 'period1' }
                    , { xtype: 'GTHNbbFrameSet', ref: 'period2', title: 'Period 2', checkboxName: 'period2' }
            //, { xtype: 'GTHNbbFrameSet', ref: 'period2' }
            ]
        });
        eBook.GTH.GTHUploadNBB.superclass.initComponent.apply(this, arguments);
    }
    , isValid: function() {
        var valid = true;
        if (!this.period1.collapsed) {
            valid = valid && this.period1.isValid();
        }
        if (!this.period2.collapsed) {
            valid = valid && this.period2.isValid();
        }
        return valid;
    }
    , setClient: function(client) {
        client.ClientEnterprise = eBook.CleanEnterprise(client.ClientEnterprise);
        if (client.ClientEnterprise == 'NULL') client.ClientEnterprise = '';
        this.period1.setClient(client);
        this.period2.setClient(client);
    }
    , reset: function() {
        this.period1.reset();
        this.period2.reset();
    }
    , getData: function(uplFiles) {
        var res = [];
        if (!this.period1.collapsed) res = res.concat(this.period1.getData(uplFiles));
        if (!this.period2.collapsed) res = res.concat(this.period2.getData(uplFiles));
        return res;
    }
});
Ext.reg('GTHUploadNBB', eBook.GTH.GTHUploadNBB);

eBook.GTH.GTHNbbFrameSet = Ext.extend(Ext.form.FieldSet, {
    initComponent: function() {
        Ext.apply(this, {
            // collapsible: true,
            autoHeight: true,
            checkboxToggle: true,
            columnWidth: 0.5,
            bodyStyle: 'padding:5px;',
            border: false,
            items: [{ xtype: 'datefield', format: 'd M Y', ref: 'periodStart', name: 'StartPeriod', allowBlank: false, fieldLabel: 'Financial year start' }
                    , { xtype: 'datefield', format: 'd M Y', ref: 'periodEnd', name: 'EndPeriod', allowBlank: false, fieldLabel: 'Financial year end' }
                    , new Ext.ux.form.FileUploadField({ width: 350, ref: 'xbrl', allowBlank: true, fieldLabel: 'XBRL File', fileTypeRegEx: /(\.xbrl)$/i })
                    , new Ext.ux.form.FileUploadField({ width: 350, ref: 'pdf', allowBlank: false, fieldLabel: 'PDF File', fileTypeRegEx: /(\.pdf)$/i })
            ]

        });
        eBook.GTH.GTHNbbFrameSet.superclass.initComponent.apply(this, arguments);
    }
    , reset: function() {
        this.periodStart.reset();
        this.periodEnd.reset();
        this.xbrl.reset();
        this.pdf.reset();
    }
    , getData: function(uplFiles) {
        var start = this.periodStart.getValue();
        var end = this.periodEnd.getValue();
        var vxbrl = this.xbrl.getValue();
        var vpdf = this.pdf.getValue();
        var fles = [];
        for (var i = 0; i < uplFiles.length; i++) {
            var fle = uplFiles[i];
            var fo = { Name: fle.originalFile, Id: fle.id, Extension: fle.extension, ContentType: fle.contentType, fileName: fle.file };
            if (!Ext.isEmpty(vxbrl) && vxbrl.indexOf(fle.originalFile) > -1) {
                fo.Start = start;
                fo.End = end;
                fles.push(fo);
            } else if (!Ext.isEmpty(vpdf) && vpdf.indexOf(fle.originalFile) > -1) {
                fo.Start = start;
                fo.End = end;
                fles.push(fo);
            }
        }
        return fles;

    }
    , setClient: function(client) {
        this.client = client;

    }
    , isValid: function() {
        var valid = true;
        this.items.each(function(it) {
            var vit = it.isValid();
            valid = valid && vit;
        }, this);

        if (valid) {
            if (this.periodStart.getValue() > this.periodEnd.getValue()) {
                this.periodStart.markInvalid("Start of period cannot be after end of period.");
                this.periodEnd.markInvalid("End of period cannot be before start of period.");
                valid = false;
            } else {
                var fmsg = 'Format of the filename needs to be {CompanyNr}_{day}{month}{year}. Day & month must include a leading zero if lower then 10';
                var fileClientRegex = new RegExp(this.client.ClientEnterprise + '_' + this.periodEnd.getValue().format('dmY'));
                var v = this.xbrl.getValue();
                if (!Ext.isEmpty(v)) {
                    if (!fileClientRegex.test(v)) {
                        valid = false;
                        this.xbrl.markInvalid(fmsg);
                    }
                }
                v = this.pdf.getValue();
                if (!Ext.isEmpty(v)) {
                    if (!fileClientRegex.test(v)) {
                        valid = false;
                        this.pdf.markInvalid(fmsg);
                    }
                }
            }


        }
        return valid;
    }
});
Ext.reg('GTHNbbFrameSet', eBook.GTH.GTHNbbFrameSet);            
            

/*
eBook.GTH.FileUpload = Ext.extend(Ext.ux.form.FileUploadField, {
    validateValue:function(v) {
        var valid = eBook.GTH.FileUpload.superclass.validateValue.call(this,v);
        this.periodEnd.format('ddMMY');
        var rexp = new RegExp(this.enterprisNr + '_' +  + '/gi')
    }
*/

eBook.GTH.Team2_Process = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: { type: 'vbox', align: 'stretch' }
            , border: false
              , items: [{ xtype: 'GTHClientDetails', teamId: this.teamId, ref: 'clientDetails', height: 60 }
                        , { xtype: 'panel', layout: 'accordion', cls: 'gth-team2-accord', animate: true, flex: 1, margins: { top: 0, bottom: 10, left: 0, right: 0 },
                            items: [{ xtype: 'panel', step: 1, title: 'Step 1: Excel template, NBB files, Teammembers, File repository', layout: { type: 'hbox', align: 'stretch' }
                                    , listeners: { expand: { fn: this.onAccordionPaneExpand, scope: this} }
                                    , items: [
                                     { xtype: 'panel', title: 'Excel, NBB & Teammembers', flex: 1, layout: { type: 'vbox', align: 'stretch' }

                                        , items: [{ xtype: 'GTHTeam2DownloadFiles', height: 60, teamId: this.teamId, ref: '../../../downloadfiles', flex: 1 }
                                               , { xtype: 'eBook.PMT.TeamGrid', ref: '../../../teamgrid', includeMail: true, includeEbook: false, storeCfg: {
                                                   selectAction: 'GetClientTeamDepartment',
                                                   fields: eBook.data.RecordTypes.TeamMember,
                                                   serviceUrl: eBook.Service.client,
                                                   idField: 'Id',
                                                   autoDestroy: true,
                                                   autoLoad: false,
                                                   criteriaParameter: 'ciddc',
                                                   baseParams: {
                                                       Department: 'ACR'
                                                   }

                                               }, flex: 1
}]
                                     }, { xtype: 'repository', title: 'Repository', ref: '../../repos', readOnly: true, startFrom: { type: 'period', id: '77C537A1-38C6-4868-AAE0-7CD6AA57E66F' }, flex: 1 }
                                    ]
                            }
                                    , { xtype: 'GTHTeam2UploadSupDocs', ref: '../uploadexcel', step: 2, title: 'Step 2: Final: upload definitive excel', listeners: { expand: { fn: this.onAccordionPaneExpand, scope: this}} }
                                ]
                        }
                    , { xtype: 'button', text: 'Start new client', ref: 'newclientstart', handler: this.onNewClientClick, scope: this, height: 60, cls: 'eb-massive-btn'}]
        });
        eBook.GTH.Team2_Process.superclass.initComponent.apply(this, arguments);
    }
    , initStep: function(owner) {
        //owner.hideStop(); can stop in the middle or start new
        this.client = owner.client;
        this.ownerSteps = owner;
        var cl = this.client;
        this.clientId = cl.Id;
        this.stepTitle = 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel for ' + cl.ClientName;
        this.clientDetails.initStep(cl);
        if (this.uploadexcel && this.uploadexcel.rendered) this.uploadexcel.reset();
        this.downloadfiles.getFiles(cl);
        this.teamgrid.store.load({ params: { Id: cl.ClientId} });
        this.repos.getEl().mask("Validating &amp; processing files", 'x-mask-loading');
//        var rt = this.repos.getRootNode();
//        var loader = rt.loader || rt.attributes.loader || this.repos.getLoader();
//        loader.ClientId = cl.ClientId;
//        loader.filter = { ps: cl.StartDate, pe: cl.EndDate };
//        rt.loader = loader;
        //        rt.reload(this.onReloadedRepos, this);
        var rt = this.repos.getRootNode();
        rt.removeAll();
        this.repos.ClientId = cl.ClientId;
        this.repos.filter = { ps: cl.StartDate, pe: cl.EndDate };
        this.repos.getLoader().load(rt, this.onReloadedRepos, this);
    }
    , onAccordionPaneExpand: function(panel) {
        this.newclientstart.enable();
        if (panel.step == 2) {
            this.newclientstart.disable();
        }
    }
    , onReloadedRepos: function() {
        this.repos.getEl().unmask();
    }
    , onNewClientClick: function() {
        this.ownerSteps.moveNextStep();
    }
});
Ext.reg('GTHTeam2Process', eBook.GTH.Team2_Process);


// download empty excel template
// list files team 1 of this period.
eBook.GTH.Team2_DownloadFiles = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.templ = new Ext.XTemplate('<div class="gth-team2-fileslist">'
                                        , '<div class="gth-team2-exceltempl"><a href="Repository/GTHTeam2_Template_v8.xlsx" target="_blank">Download empty Excel template</a></div>'
                                        , '<div class="gth-team2-repo1">TEAM 1 Files:<ul>'
                                        , '<tpl for="files"><li><a href="{[this.getLink(values)]}" target="_blank">{text} <i><tpl for="Item">({Extension})</tpl></i></a></li></tpl>'
                                        , '</ul></div>', {
                                            getLink: function(values) {
                                                return "Repository/" + eBook.getGuidPath(values.Item.ClientId) + values.Item.PhysicalFileId + values.Item.Extension
                                            }
                                        });
        this.templ.compile();
        Ext.apply(this, { html: 'loading data' });
        eBook.GTH.Team2_DownloadFiles.superclass.initComponent.apply(this, arguments);
    }
    , getFiles: function(cl) {
        if (cl.StartDate != null && !Ext.isDate(cl.StartDate)) cl.StartDate = Ext.data.Types.WCFDATE.convert(cl.StartDate);
        if (cl.EndDate != null && !Ext.isDate(cl.EndDate)) cl.EndDate = Ext.data.Types.WCFDATE.convert(cl.EndDate);
        this.getEl().mask("Loading data team 1", 'x-mask-loading');

        Ext.Ajax.request({
            url: eBook.Service.repository + 'QueryFiles'
            , method: 'POST'
            , params: Ext.encode({ cqfdc: { cid: cl.ClientId, sid: '3787F2A4-112D-4589-A231-E9B95BDCFCD9', ps: cl.StartDate, pe: cl.EndDate} })
             , callback: this.onFilesObtained
            , scope: this
        });
    }
    , onFilesObtained: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var o = Ext.decode(resp.responseText).QueryFilesResult;
            this.body.update(this.templ.apply({ files: o }));
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam2DownloadFiles', eBook.GTH.Team2_DownloadFiles);

eBook.GTH.GTHUploadSupDocs = Ext.extend(Ext.form.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            fileUpload: true,
            border: false,
            bodyStyle: 'padding:10px;',
            //layout: { type: 'vbox', align: 'stretch' },
            autoScroll: true,
            items: [
                new Ext.ux.form.FileUploadField({ width: 350, ref: 'excel', allowBlank: false, fieldLabel: 'Final Excel Template', fileTypeRegEx: /((\.xls)(x){0,1})$/i })
            //new Ext.ux.form.FileUploadField({ width: 350, ref: 'pdf', allowBlank: false, fieldLabel: 'Executive summary', fileTypeRegEx: /((\.pdf))$/i })
                , { xtype: 'label', text: '/!\\ IMPORTANT: This is the final step for each Client/period. When uploading the excel and pdf file, you will mark this job as "finalized".', style: 'font-weight:bold;' }
                , { xtype: 'button', width: 350, text: 'Upload file', handler: this.onUploadClick, scope: this, height: 60, cls: 'eb-massive-btn', style: 'margin-top:20px;' }
            ]
        });
        eBook.GTH.GTHUploadSupDocs.superclass.initComponent.apply(this, arguments);
    }
    , reset: function() {
        if (this.excel && this.excel.rendered) this.excel.reset();
        //if (this.pdf && this.pdf.rendered) this.pdf.reset();
    }
//    , onUploadClick: function() {
//        var f = this.getForm();
//        if (this.excel.isValid() && this.pdf.isValid()) {
//            f.isValid = function() { return true; };
//            this.ownerCt.ownerCt.ownerSteps.hideStop();
//            f.submit({
//                url: 'UploadMultiple.aspx',
//                waitMsg: 'Uploading files',
//                success: this.successUpload,
//                failure: this.failedUpload,
//                scope: this
//            });
//        } else {
//            alert("Selected file/upload field contains invalid or no data.");
//        }
    //    }
    , onUploadClick: function() {
        var f = this.getForm();
        if (this.excel.isValid()) {
            f.isValid = function() { return true; };
            this.ownerCt.ownerCt.ownerSteps.hideStop();
            f.submit({
                url: 'UploadMultiple.aspx',
                waitMsg: 'Uploading files',
                success: this.successUpload,
                failure: this.failedUpload,
                scope: this
            });
        } else {
            alert("Selected file/upload field contains invalid or no data.");
        }
    }
     , successUpload: function(fp, o) {
         var exc = o.result.files[0];
         var cl = this.ownerCt.ownerCt.client;
         var fle;
         this.ownerCt.ownerCt.getEl().mask("Validating &amp; processing files", 'x-mask-loading');

         var fls = [];
         for (var i = 0; i < o.result.files.length; i++) {
             fle = o.result.files[i];
             fls.push({ Name: fle.originalFile, Start: cl.StartDate, End: cl.EndDate, Id: fle.id, Extension: fle.extension, ContentType: fle.contentType, fileName: fle.file });
         }


         Ext.Ajax.request({
             url: eBook.Service.GTH + 'ProcessFilesTeam2'
            , method: 'POST'
            , params: Ext.encode({ cpfdc: { StructureId: 'e2e2440a-6e08-444a-9494-052d8e8c3dc2', Files: fls, TeamId: this.teamId, Status: 'FINAL', PersonId: eBook.User.personId, GTHClientId: this.ownerCt.ownerCt.clientId} })
             , callback: this.handleFileProcessing
            , scope: this
         });

         //process o.result.file
         /*this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
       
        ¨*/

     }
    , failedUpload: function(fp, o) {
        eBook.Interface.showError(o.message, "UPLOAD FAILED!!")
        this.ownerCt.ownerCt.ownerSteps.showStop();
        this.ownerCt.ownerCt.getEl().unmask();
    }
    , handleFileProcessing: function(opts, success, resp) {
        this.ownerCt.ownerCt.getEl().unmask();
        this.ownerCt.ownerCt.ownerSteps.showStop();
        if (success) {
            this.ownerCt.ownerCt.ownerCt.moveNextStep();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});

Ext.reg('GTHTeam2UploadSupDocs', eBook.GTH.GTHUploadSupDocs);/*
 eBook.GTH.FileUpload = Ext.extend(Ext.ux.form.FileUploadField, {
 validateValue:function(v) {
 var valid = eBook.GTH.FileUpload.superclass.validateValue.call(this,v);
 this.periodEnd.format('ddMMY');
 var rexp = new RegExp(this.enterprisNr + '_' +  + '/gi')
 }
 */


eBook.GTH.Team4_Process = Ext.extend(Ext.Panel, {
    initComponent: function () {
        Ext.apply(this, {
            layout: {type: 'vbox', align: 'stretch'}
            , border: false
            , items: [{xtype: 'GTHClientDetails', teamId: this.teamId, ref: 'clientDetails', height: 60}
                , {
                    xtype: 'panel',
                    layout: 'accordion',
                    cls: 'gth-team2-accord',
                    animate: true,
                    flex: 1,
                    margins: {top: 0, bottom: 10, left: 0, right: 0},
                    items:
                    [
                        {
                            xtype: 'panel',
                            step: 1,
                            title: 'Step 1: Excel template, Teammembers, eBook files',
                            layout: {type: 'hbox', align: 'stretch'}
                            ,
                            listeners: {expand: {fn: this.onAccordionPaneExpand, scope: this}}
                            ,
                            items: [
                                {
                                    xtype: 'panel',
                                    title: 'Excel, NBB & Teammembers',
                                    flex: 1,
                                    layout: {type: 'vbox', align: 'stretch'}

                                    ,
                                    items: [{
                                        xtype: 'GTHTeam4DownloadFiles',
                                        height: 80,
                                        autoScroll: true,
                                        teamId: this.teamId,
                                        ref: '../../../downloadfiles',
                                        flex: 1
                                    }
                                        , {
                                            xtype: 'eBook.PMT.TeamGrid',
                                            ref: '../../../teamgrid',
                                            includeMail: true,
                                            includeEbook: false,
                                            storeCfg: {
                                                selectAction: 'GetClientTeamDepartment',
                                                fields: eBook.data.RecordTypes.TeamMember,
                                                serviceUrl: eBook.Service.client,
                                                idField: 'Id',
                                                autoDestroy: true,
                                                autoLoad: false,
                                                criteriaParameter: 'ciddc',
                                                baseParams: {
                                                    Department: 'ACR'
                                                }

                                            },
                                            flex: 1
                                        }]
                                }
                                // REMOVE REPOSITORY
                                , {
                                    xtype: 'panel',
                                    layout: {type: 'vbox', align: 'stretch'},
                                    flex: 1,
                                    items: [
                                        /*{ //Removed on 2015/10/16 per request
                                         xtype: 'GTHTeam4Files',
                                         title: 'eBook files',
                                         ref: '../../../filesInfo',
                                         flex: 2, autoScroll: true
                                         },*/
                                        {
                                            xtype: 'GTHFilesMeta',
                                            title: 'eBook files meta (Peergroup)',
                                            ref: '../../../GTHFilesMeta',
                                            flex: 1,
                                            autoScroll: true
                                        }
                                    ]
                                }
                            ]
                        }
                        ,{
                            xtype: 'GTHTeam4UploadFinHealthDocs',
                            ref: '../uploadexcel',
                            step: 2,
                            title: 'Step 2: Final: upload definitive excel',
                            listeners: {expand: {fn: this.onAccordionPaneExpand, scope: this}}
                        }
                    ]
                }
                , {
                    xtype: 'button',
                    text: 'Start new client',
                    ref: 'newclientstart',
                    handler: this.onNewClientClick,
                    scope: this,
                    height: 60,
                    cls: 'eb-massive-btn'
                }]
        });
        eBook.GTH.Team4_Process.superclass.initComponent.apply(this, arguments);
    }
    , initStep: function (owner) {
        //owner.hideStop(); can stop in the middle or start new
        this.client = owner.client;
        this.ownerSteps = owner;
        var cl = this.client;
        this.clientId = cl.Id; // not actual client id => is GTH_team_status ID
        this.stepTitle = 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel for ' + cl.ClientName;
        this.clientDetails.initStep(cl);
        if (this.uploadexcel && this.uploadexcel.rendered) this.uploadexcel.reset();
        this.downloadfiles.getFiles(cl);
        this.teamgrid.store.load({params: {Id: cl.ClientId}});

        //this.filesInfo.getFiles(cl.ClientId);

        // deleted repository for this team so removed mask
        //this.repos.getEl().mask("Validating &amp; processing files", 'x-mask-loading');
        //var rt = this.repos.getRootNode();
        //var loader = rt.loader || rt.attributes.loader || this.repos.getLoader();
        //loader.ClientId = cl.ClientId;
        //loader.filter = { ps: cl.StartDate, pe: cl.EndDate };
        //rt.loader = loader;
        //rt.reload(this.onReloadedRepos, this);
    }
    , onAccordionPaneExpand: function (panel) {
        this.newclientstart.enable();
        if (panel.step == 2) {
            this.newclientstart.disable();
        }
    }
    , onReloadedRepos: function () {
        this.repos.getEl().unmask();
    }
    , onNewClientClick: function () {
        this.ownerSteps.moveNextStep();
    }
});
Ext.reg('GTHTeam4Process', eBook.GTH.Team4_Process);


// download empty excel template
// list files team 1 of this period.
// Either globalize (excel via config) or team 4
eBook.GTH.Team4_DownloadFiles = Ext.extend(Ext.Panel, {
    initComponent: function () {
        this.templTeam4 = new Ext.XTemplate('<div class="gth-team2-fileslist">'
            // WAITING FOR EXCEL TEMPLATE GTHTeam4_Template !!!!!!!!!!!!!!!!!!!!!
            , '<div class="gth-team2-exceltempl"><a href="Repository/FinancialHealth_20151119_TemplateInclCompetitorAnalysis.xlsm" target="_blank">Download empty Excel template</a></div>',
            /*, '<div class="gth-team2-repo1">TEAM 1 Files:<ul>'
             , '<tpl for="files"><li><a href="{[this.getLink(values)]}" target="_blank">{text} <i><tpl for="Item">({Extension})</tpl></i></a></li></tpl>'
             , '</ul></div>'*/
            {
                getLink: function (values) {
                    return "Repository/" + eBook.getGuidPath(values.Item.ClientId) + values.Item.PhysicalFileId + values.Item.Extension
                }
            });

        this.templTeam1 = new Ext.XTemplate('<div class="gth-team2-fileslist">'
            // WAITING FOR EXCEL TEMPLATE GTHTeam4_Template !!!!!!!!!!!!!!!!!!!!!
            , '<div class="gth-team2-repo1">Present Files:<ul>'
            , '<tpl for="files"><li><a href="{[this.getLink(values)]}" target="_blank">{text} <i><tpl for="Item">({Extension})</tpl></i></a></li></tpl>'
            , '</ul></div>', {
                getLink: function (values) {
                    return "Repository/" + eBook.getGuidPath(values.Item.ClientId) + values.Item.PhysicalFileId + values.Item.Extension
                }
            });
        if (this.teamId == "1") {
            this.templTeam1.compile();
        } else if (this.teamId == "4") {
            this.templTeam4.compile();
        }

        Ext.apply(this, {html: 'loading data'});
        eBook.GTH.Team4_DownloadFiles.superclass.initComponent.apply(this, arguments);
    }
    , getFiles: function (cl) {
        //if (cl.StartDate != null && !Ext.isDate(cl.StartDate)) cl.StartDate = Ext.data.Types.WCFDATE.convert(cl.StartDate);
        //if (cl.EndDate != null && !Ext.isDate(cl.EndDate)) cl.EndDate = Ext.data.Types.WCFDATE.convert(cl.EndDate);
        //cl.StartDate = null;
        //cl.EndDate = null;
        this.getEl().mask("Loading data team 1", 'x-mask-loading');

        Ext.Ajax.request({
            url: eBook.Service.repository + 'QueryFiles'
            ,
            method: 'POST'
            ,
            params: Ext.encode({
                cqfdc: {
                    cid: cl.ClientId,
                    sid: '3787F2A4-112D-4589-A231-E9B95BDCFCD9',
                    ps: cl.StartDate,
                    pe: cl.EndDate
                }
            })
            ,
            callback: this.onFilesObtained
            ,
            scope: this
        });
    }
    , onFilesObtained: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var o = Ext.decode(resp.responseText).QueryFilesResult;
            if (this.teamId == "1") {
                this.body.update(this.templTeam1.apply({files: o}));
            } else if (this.teamId == "4") {
                this.body.update(this.templTeam4.apply({files: o}));
            }

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam4DownloadFiles', eBook.GTH.Team4_DownloadFiles);


//eBook.GTH.GTHUploadSupDocs = Ext.extend(Ext.form.FormPanel, {
eBook.GTH.GTHUploadFinHealthDocs = Ext.extend(Ext.form.FormPanel, {
    initComponent: function () {
        Ext.apply(this, {
            fileUpload: true,
            border: false,
            bodyStyle: 'padding:10px;',
            //layout: { type: 'vbox', align: 'stretch' },
            autoScroll: true,
            items: [
                new Ext.ux.form.FileUploadField({
                    width: 350,
                    labelWidth: 50,
                    ref: 'excel',
                    allowBlank: false,
                    fieldLabel: 'Final Excel Template',
                    fileTypeRegEx: /((\.xls)(x|m){0,1})$/i
                })
                , {
                    xtype: 'combo',
                    ref: 'competitionAnalysis',
                    fieldLabel: 'Competition analysis',
                    name: 'competitionAnalysis',
                    triggerAction: 'all',
                    typeAhead: false,
                    selectOnFocus: true,
                    autoSelect: true,
                    allowBlank: true,
                    forceSelection: true,
                    valueField: 'field1',
                    displayField: 'field1',
                    editable: false,
                    listWidth: 300,
                    labelWidth: 50,
                    mode: 'local',
                    store: ["Not included","Included"],
                    width: 300
                }
                , {
                    xtype: 'label',
                    text: '/!\\ IMPORTANT: This is the final step for each Client/period. When uploading the excel and pdf file, you will mark this job as "finalized".',
                    style: 'font-weight:bold;'
                }
                , {
                    xtype: 'button',
                    width: 350,
                    text: 'Upload file',
                    handler: this.onUploadClick,
                    scope: this,
                    height: 60,
                    cls: 'eb-massive-btn',
                    style: 'margin-top:20px;'
                }
            ]
        });
        eBook.GTH.GTHUploadFinHealthDocs.superclass.initComponent.apply(this, arguments);
    }
    , reset: function () {
        var me = this;
        if (me.excel && me.excel.rendered) me.excel.reset();
        if (me.pdf && me.pdf.rendered) me.pdf.reset();
        if (me.competitionAnalysis)
        {
            var store = me.competitionAnalysis.getStore(),
                record = store ? store.getAt(0) : null;

            me.competitionAnalysis.setValue(record.get("field1"));
        }
    }
    , onUploadClick: function () {
        var f = this.getForm();
        if (this.excel.isValid()) {
            f.isValid = function () {
                return true;
            };
            this.ownerCt.ownerCt.ownerSteps.hideStop();
            f.submit({
                url: 'UploadMultiple.aspx',
                waitMsg: 'Uploading files',
                success: this.successUpload,
                failure: this.failedUpload,
                scope: this
            });
        } else {
            alert("Selected file/upload field contains invalid or no data.");
        }
    }
    , successUpload: function (fp, o) {
        var exc = o.result.files[0];
        var cl = this.ownerCt.ownerCt.client;
        var fle;
        this.ownerCt.ownerCt.getEl().mask("Validating &amp; processing files", 'x-mask-loading');

        var fls = [];
        for (var i = 0; i < o.result.files.length; i++) {
            fle = o.result.files[i];
            fls.push({
                Name: fle.originalFile,
                Start: cl.StartDate,
                End: cl.EndDate,
                Id: fle.id,
                Extension: fle.extension,
                ContentType: fle.contentType,
                fileName: fle.file,
                fileNameParentheticalRemark: fp.getFieldValues().competitionAnalysis == "Not included" ? null : "Competition analysis included"
            });
        }


        Ext.Ajax.request({
            url: eBook.Service.GTH + 'ProcessFilesTeam2' // saves to repository GTH folder team 4 - financial health
            ,
            method: 'POST'
            ,
            params: Ext.encode({
                cpfdc: {
                    StructureId: '1b444980-2d74-45a0-95e6-d9bc196f07f5',
                    Files: fls,
                    TeamId: this.teamId,
                    Status: 'FINAL',
                    PersonId: eBook.User.personId,
                    GTHClientId: this.ownerCt.ownerCt.clientId
                }
            })
            ,
            callback: this.handleFileProcessing
            ,
            scope: this
        });

        //process o.result.file
        /*this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');

         ¨*/

    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, "UPLOAD FAILED!!")
        this.ownerCt.ownerCt.ownerSteps.showStop();
        this.ownerCt.ownerCt.getEl().unmask();
    }
    , handleFileProcessing: function (opts, success, resp) {
        this.ownerCt.ownerCt.getEl().unmask();
        this.ownerCt.ownerCt.ownerSteps.showStop();
        if (success) {
            this.ownerCt.ownerCt.ownerCt.moveNextStep();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});

Ext.reg('GTHTeam4UploadFinHealthDocs', eBook.GTH.GTHUploadFinHealthDocs);


eBook.GTHFiles = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
        , '<tpl for="GetFileInfosResult">'
        , '<div id={id} class="eBook-GTH-team4-file">'
        , '<div> {n} </div>'
        , '<tpl><i>{[this.getPeriod(values)]}</i></tpl>'
        , '</div>'
        , '</tpl>'
        , '</tpl>'
        , '<div class="x-clear"></div>', {
            getPeriod: function (values) {
                var sDate = Ext.data.Types.WCFDATE.convert(values.sd),
                    eDate = Ext.data.Types.WCFDATE.convert(values.ed);
                return sDate.format("d/m/Y") + ' - ' + eDate.format("d/m/Y");
            }
        }
    )

    , initComponent: function () {
        Ext.apply(this, {
            border: false
        });
        eBook.GTHFiles.superclass.initComponent.apply(this, arguments);
    }
    , onRender: function (ct, pos) {
        eBook.GTHFiles.superclass.onRender.call(this, ct, pos);
        //        this.body.update(this.mainTpl.apply(this.data));
    }
    , afterRender: function () {
        eBook.GTHFiles.superclass.afterRender.call(this);

        // set events
        this.el.on('click', this.onBodyClick, this);
    }

    , onBodyClick: function (e, t, o) {
        var file = e.getTarget('.eBook-GTH-team4-file');

        var cicdc = {
            cicdc: {
                Id: file.id
                , Culture: 'en-US'
            }
        }

        this.getEl().mask("Loading files", 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.output + 'GetAnnualAccountsBundle'
            , method: 'POST'
            , FileId: cicdc.Id
            , params: Ext.encode(cicdc)
            , scope: this
            , callback: function (opts, success, resp) {
                this.getEl().unmask();

                if (success) {
                    // alert(Ext.decode(resp.responseText).GetAnnualAccountsBundleResult);
                    window.open(Ext.decode(resp.responseText).GetAnnualAccountsBundleResult);
                } else {
                    eBook.Interface.showResponseError(resp, this.title);
                }

            }
        });
    }

    , getFiles: function (cliendID) {
        this.getEl().mask("Loading files", 'x-mask-loading');

        var cfdc = {
            cfdc: {
                ClientId: cliendID
                , MarkedForDeletion: 'false'
                , Deleted: 'false'
                , Closed: null
                , query: null
                , StartDate: null
            }
        };

        Ext.Ajax.request({
            url: eBook.Service.file + 'GetFileInfos'
            , method: 'POST'
            , params: Ext.encode(cfdc)
            , scope: this
            , callback: this.handleFileProcessing
        });
    }

    , handleFileProcessing: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.data = resp.responseText;
            this.body.update(this.mainTpl.apply(Ext.decode(this.data)));

            if (Ext.decode(this.data).GetFileInfosResult.length > 0) {
                this.refOwner.GTHFilesMeta.getFilesMeta(Ext.decode(this.data).GetFileInfosResult[0].cid);
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam4Files', eBook.GTHFiles);

eBook.GTHFilesMeta = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
        , '<div  class="eBook-GTH-team4-file">'
        , '<div> {n} </div>'
        , '<tpl><i>{ond} {nme}</i></tpl>'
        , '</div>'
        , '</tpl>'
        , '<div class="x-clear"></div>'
    )

    , initComponent: function () {
        Ext.apply(this, {
            border: false
        });
        eBook.GTHFilesMeta.superclass.initComponent.apply(this, arguments);
    }
    , onRender: function (ct, pos) {
        eBook.GTHFilesMeta.superclass.onRender.call(this, ct, pos);
        //        this.body.update(this.mainTpl.apply(this.data));
    }


    , getFilesMeta: function (clientId) {
        this.getEl().mask("Loading files", 'x-mask-loading');

        var cidc = {
            cidc: {Id: clientId}
        };

        Ext.Ajax.request({
            url: eBook.Service.file + 'GetMetaByClientId'
            , method: 'POST'
            , params: Ext.encode(cidc)
            , scope: this
            , callback: this.handleFileProcessing
        });
    }

    , handleFileProcessing: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.data = resp.responseText;
            var arr = [];
            var filesMeta = Ext.decode(this.data).GetMetaByClientIdResult;
            if (filesMeta.length > 0) {
                for (var i = 0; i < filesMeta.length; i++) {
                    if (filesMeta[i].dta.pgroup.length > 0) {
                        for (var j = 0; j < filesMeta[i].dta.pgroup.length; j++) {
                            var x = {}
                            x.ond = filesMeta[i].dta.pgroup[j].ond;
                            x.nme = filesMeta[i].dta.pgroup[j].nme;
                            arr.push(x);
                        }
                    }
                }
            }
            this.body.update(this.mainTpl.apply(arr));
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHFilesMeta', eBook.GTHFilesMeta);

eBook.GTH.Team5 = Ext.extend(Ext.Panel, {
    id: 'GthTeam5MainPanel',
    period: null,
    serviceId: '1ABF0836-7D7B-48E7-9006-F03DB97AC28B',

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            bodyCssClass: 'eBook-GTH-Team5',
            ref: 'eBook-GTH-Team5',
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack: 'start'
            },
            defaults: {
                bodyStyle: 'padding:15px'
            },
            items: [
                {
                    xtype: 'panel',
                    title: 'Annual Accounts awaiting filing',
                    ref: 'AwaitingFilingPanel',
                    //height: 250,
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    padding: 0,
                    items: [
                        {
                            xtype: 'button',
                            ref: 'btnPickupAwaitingFiling',
                            text: 'Pick up item for filing<br>(from list or allow system to choose latest)',
                            width: 220,
                            //height: 250,
                            record: {action: 'Filing'}, //record obj
                            cls: 'eBook-GTH-Team5-btnPickupFirstOrSelectedRecord', //padding-top increase for button icon and text
                            iconCls: 'eBook-repository-select-ico',
                            iconAlign: 'top',
                            listeners: {
                                click: {fn: me.onBtnPickUpRecordClicked, scope: me}
                            }
                        },
                        {
                            xtype: 'reportFSgrid',
                            flex: 1,
                            //height: 250,
                            cls: 'eBookAT-GTH-Persongrid',
                            ref: 'pickupGrid',
                            serviceId: me.serviceId,
                            title: 'List of annual accounts to be filed with the NBB filing application',
                            filterValues: {status: [5,10,13], field: 'daysOnStatus', direction: 'DSC'} //Only Approved by Partner, Returned to pool by GTH or Refiling requested by NBB Admin (hence the ones that need NBB filing)
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    title: 'Annual Accounts awaiting invoice',
                    ref: 'AwaitingInvoicePanel',
                    //height: 250,
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    padding: 0,
                    items: [
                        {
                            xtype: 'button',
                            ref: 'btnPickupAwaitingInvoice',
                            text: 'Pick up item for invoice info input<br>(from list)',
                            width: 220,
                            //height: 250,
                            record: {action: 'Input invoice info'}, //record obj
                            cls: 'eBook-GTH-Team5-btnPickupFirstOrSelectedRecord', //padding-top increase for button icon and text
                            iconCls: 'eBook-invoice-select-ico',
                            iconAlign: 'top',
                            listeners: {
                                click: {fn: me.onBtnPickUpRecordClicked, scope: me}
                            }
                        },
                        {
                            xtype: 'reportFSgrid',
                            flex: 1,
                            height: 250,
                            cls: 'eBookAT-GTH-Persongrid',
                            ref: 'pickupGrid',
                            serviceId: me.serviceId,
                            title: 'List of annual accounts awaiting invoice information from NBB',
                            filterValues: {status: 9, field: 'daysOnStatus', direction: 'DSC'} //Only Filed by GTH  (hence the ones that awaiting invoice feedback from NBB)
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    ref: 'recordPnl',
                    record: null, //custom record prop
                    title: 'Use the above button to pick a client',
                    padding: 0,
                    flex: 1,
                    boxMinHeight: 200,
                    disabled: true,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    items: [
                        {
                            xtype: 'formOverview',
                            ref: 'formOverview',
                            flex: 1,
                            serviceUrl: eBook.Service.annualAccount
                        },
                        {
                            xtype: 'panel',
                            ref: 'actionPnl',
                            title: 'Actions',
                            flex: 2,
                            padding: 0,
                            layout: {
                                type: 'vbox',
                                align: 'stretch',
                                pack: 'start',
                                defaultMargins: {
                                    top: 10,
                                    bottom: 10,
                                    right: 5,
                                    left: 10
                                }
                            },
                            items: [{
                                xtype: 'button',
                                text: 'Copy folder URL<br>(folder contains file required for upload on NBB website)',
                                ref: 'btnCopyUrl',
                                iconCls: 'eBook-icon-openreport-24',
                                flex: 1,
                                scale: 'medium',
                                iconAlign: 'top',
                                margins: {top: 10, right: 10, bottom: 10, left: 10},
                                listeners: {
                                    click: {fn: me.onBtnCopyUrl, scope: me}
                                }
                            },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch',
                                        pack: 'start',
                                        defaultMargins: {
                                            top: 0,
                                            bottom: 0,
                                            right: 5,
                                            left: 0
                                        }
                                    },
                                    ref: 'submitBtnsCon',
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: 'success', //dynamically updated through status
                                            ref: '../btnSuccess',
                                            iconCls: 'eBook-yes-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            flex: 1,
                                            listeners: {
                                                click: {fn: me.onBtnSuccessClicked, scope: me}
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'failed', //dynamically updated through status
                                            ref: '../btnFailure',
                                            iconCls: 'eBook-grid-row-delete-ico',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            flex: 1,
                                            listeners: {
                                                click: {fn: me.onBtnFailedClicked, scope: me}
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                afterrender: function () {
                    me.getEl().mask("Checking if a filing item was already picked up and if so retrieving it");
                    //load any picked up item
                    if (me.recordPnl) {me.recordPnl.record = {statusId: 7, action: 'Filing'};}
                    me.getLongestOnStatusFileServiceReportRecord(7, eBook.User.personId);
                }
            }
        });

        eBook.GTH.Team5.superclass.initComponent.apply(this, arguments);
    },

    ///show override
    /*
     show: function ()
     {
     var me = this;

     me.AwaitingFilingPanel.pickupGrid.storeLoad({status: 5, field: 'daysOnStatus', direction: 'DSC'});
     eBook.GTH.Team5.Window.superclass.show.call(me);
     },
     */
    refreshPickupGrid: function () {
        var me = this;

        me.AwaitingFilingPanel.pickupGrid.storeLoad({status: [5,10,13], field: 'daysOnStatus', direction: 'DSC'});
        me.AwaitingInvoicePanel.pickupGrid.storeLoad({status: 9, field: 'daysOnStatus', direction: 'DSC'});
    },

    ///User has clicked the pick up button (FIFO or selected item), which calls the GetFileServiceReport function allowing the retrieval of one record, sorted by daysOnStatus
    onBtnPickUpRecordClicked: function (btn) {
        var me = this,
            grid = btn.refOwner.pickupGrid,
            selectedRecord = grid.getActiveRecord();

        if (me.recordPnl) {
            if (me.recordPnl.record) { //record already loaded?
                Ext.MessageBox.show({
                    title: "Item already picked up. Move item back to pool?",
                    msg: "An item was already selected. Do you want to return this item to the pool and select a new one?",
                    buttons: Ext.MessageBox.YESNO,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            me.getEl().mask("Moving item to awaiting filing list");
                            if (me.recordPnl.record.statusId == 7) {
                                me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 10, null); //Updates service status to 'Returned to pool by GTH'
                            }
                            else {
                                me.unloadFileServiceRecord();
                            }
                        }
                    }
                });
            } else {
                me.recordPnl.record = {};
                /*for (var prop in btn.record) {
                    me.recordPnl.record[prop] = btn.record[prop]; //Add pre-defined tn record props to recordPnl record obj
                }*/

                if (selectedRecord) //user selected record?
                {
                    me.getEl().mask("Retrieving selected Annual Accounts item");
                    var recordLoaded = me.loadFileServiceRecord(selectedRecord.data);

                    if (recordLoaded) {
                        grid.getSelectionModel().clearSelections();
                        if (selectedRecord.get("statusId") == 5 || selectedRecord.get("statusId") == 10 || selectedRecord.get("statusId") == 13) {
                            me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 7, null); //Updates service status to 'Picked up by GTH'
                        }
                        else {
                            me.updateRecordPnl();
                        }
                    }
                }
                else if (btn.record.action == "Filing") { //auto pick up record if action is filing
                    me.getEl().mask("Automatically retrieving oldest Annual Accounts item that requires filing");
                        me.getLongestOnStatusFileServiceReportRecord([5,10,13], null); //get longest on status for any filing pick up status
                }
                else {
                    me.recordPnl.record = null;
                    Ext.Msg.alert("Select an item", "Please select an item.");
                }
            }
        }
    },

    ///Get the record that has been the most days on a certain status and optionally for a certain person
    getLongestOnStatusFileServiceReportRecord: function (statusIds, lastModifiedBy) {
        var me = this,
            storeConfig = {
                selectAction: 'GetFileServiceReport',
                fields: eBook.data.RecordTypes.ClientPersonFileServiceReport,
                idField: 'itemId',
                criteriaParameter: 'ccpfsdc',
                serviceUrl: eBook.Service.file
            },
            jsonStore = new eBook.data.PagedJsonStore(storeConfig),
            params = {
                Start: 0,
                Limit: 1,
                sv: me.serviceId ? me.serviceId : null,
                dp: me.department ? me.department : null,
                pid: eBook.User.personId,
                lmb: lastModifiedBy,
                sid: statusIds.toString(),
                sf: 'daysOnStatus',
                so: 'DSC'
            }; //Get the latest item (hence get top 1 on status 5 ordered by daysOnStatus desc

        jsonStore.load(
            {
                params: params,
                callback: me.onFileServiceRecordRetrieved,
                scope: me
            }
        );
    },

    ///Process response from GetFileServiceReport and UpdateFileService and load the record
    onFileServiceRecordRetrieved: function (response, opts, success) {
        var me = this,
            fsRecord = null;

        if (success) {
            if (response.length == 1) {
                fsRecord = response[0].data;

                if (me.recordPnl) {
                    var recordLoaded = me.loadFileServiceRecord(fsRecord);

                    if (recordLoaded) {
                        if (fsRecord.statusId === 5) { //approved by partner
                            me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 7, null); //Updates service status to 'Picked up by GTH'
                        }
                        else {
                            me.updateRecordPnl();
                        }
                    }
                }
            }
            else {
                if (me.recordPnl.record) {me.recordPnl.record = null;}
                me.getEl().unmask();
            }
        }
        else {
            eBook.Interface.showResponseError(response, me.title);
        }
    },

    loadFileServiceRecord: function (fsRecord) {
        var me = this;

        if (me.recordPnl) {
            for (var prop in fsRecord) {
                me.recordPnl.record[prop] = fsRecord[prop]; //Add retrieved prop to recordPnl record obj
            }
        }

        eBook.Interface.currentFile = { //set currentFile
            gthTeam5FileId: fsRecord.fileId,
            gthTeam5FileName: fsRecord.fileName,
            gthTeam5StartDate: fsRecord.startDate,
            gthTeam5EndDate: fsRecord.endDate
        };

        eBook.Interface.currentClient = { //set currentClient
            gthTeam5ClientId: fsRecord.clientId,
            gthTeam5ClientName: fsRecord.clientName
        };

        me.getEl().unmask();

        return true;
    },

    ///Clear current file and client objects and hide the record panel
    unloadFileServiceRecord: function () {
        var me = this,
            recordPnl = me.recordPnl;

        eBook.Interface.currentFile = null;
        eBook.Interface.currentClient = null;

        if (recordPnl) {
            recordPnl.record = null;
            recordPnl.disable();
            recordPnl.setTitle('Use the above button to pick a client');
            if (recordPnl.formOverview) {
                recordPnl.formOverview.clearStore();
            }
        }
        me.getEl().unmask();
        me.refreshPickupGrid();
    },

    ///Set current file and client objects using the records data and load the record panel
    updateRecordPnl: function () {
        var me = this,
            recordPnl = me.recordPnl,
            actionPnl = recordPnl.actionPnl,
            btnSuccess = actionPnl.btnSuccess,
            btnFailure = actionPnl.btnFailure,
            btnCopyUrl = actionPnl.btnCopyUrl,
            fsRecord = recordPnl.record;

        if (fsRecord) {

            //hide/show/update buttons
            if (btnSuccess && fsRecord.action) {
                btnSuccess.btnEl.update(fsRecord.action + " success" || '&#160;'); //not using setText as it uses autowidth
            }
            if (btnFailure && fsRecord.action) {
                btnFailure.btnEl.update(fsRecord.action + " failed" || '&#160;');
            }
            if (btnCopyUrl && fsRecord.statusId == 7) {
                btnCopyUrl.show();
            }
            if (recordPnl.formOverview) {
                recordPnl.formOverview.loadStore(fsRecord.fileId); //load form overview of record
            }

            recordPnl.setTitle(fsRecord.clientName + ': ' + fsRecord.fileName);
            recordPnl.enable();
            me.getEl().unmask();
        }

        me.refreshPickupGrid();
    },

    ///Creates the URL to the NBB file using file and client data
    onBtnCopyUrl: function () {
        var destinationFolderPath = "C:\\ACR Central Processing\\GTH\\NBB\\TODO", //To be supplied by server
            currentClient = eBook.Interface.currentClient,
            currentFile = eBook.Interface.currentFile;

        if (currentClient && currentFile) {
            //Client folder
            destinationFolderPath += '\\' + currentClient.gthTeam5ClientName.replace(/[^a-z\d\-]+/gi, "") + '-' + currentClient.gthTeam5ClientId; //Keep only letters, numbers and -
            //File folder
            destinationFolderPath += '\\' + currentFile.gthTeam5FileName.substring(4).replace(/[^a-z\d\-]+/gi, "") + '-' + currentFile.gthTeam5FileId; //Removed the year prefix and keep only letters, numbers and -

            window.prompt("Copy the URL to your clipboard by pressing Ctrl+C or right click and choose copy. Close this window with enter.", destinationFolderPath);
        }
        else {
            Ext.Msg.alert("No client selected", "Select a client.");
        }
    },

    onBtnSuccessClicked: function (successButton,e,invoiceAmount,invoiceAmountInvalidText,invoiceStructuredMessage,invoiceStructuredMessageInvalidText) { //optional params on selfcall
        var me = this,
            record = me.recordPnl.record,
            msg = '',
            buttons = null;

        if (record.statusId == 7) //Picked up for filing
        {
            msg += 'Was the annual accounts file successfully uploaded on the NBB website?';
            buttons = Ext.MessageBox.YESNO;
        }
        else if (record.statusId == 9) //Invoice
        {
            msg += 'Amount to be paid:<br><input type="text" id="invoiceAmount" ' + (invoiceAmount ? (' value="' + invoiceAmount + '"') : '') + '>'; //add if given
            msg += '<div style="color:red;" id="invoiceAmountInvalidText">' + (invoiceAmountInvalidText ? invoiceAmountInvalidText : '') + '</div>';
            msg += 'Invoice structured message:<br><input type="text" id="invoiceStructuredMessage" maxlength="20" ' + (invoiceStructuredMessage ? (' value="' + invoiceStructuredMessage + '" ') : '') + 'oninput="Ext.getCmp(&quot;GthTeam5MainPanel&quot;).autoMarkupStructuredMessage()">'; //add if given, auto-fill-function on input
            msg += '<div style="color:red;" id="invoiceStructuredMessageInvalidText">' + (invoiceStructuredMessageInvalidText ? invoiceStructuredMessageInvalidText : '') + '</div>';
            buttons = {ok: "Submit", cancel: "Cancel"};
        }

        Ext.MessageBox.show({
            title: record.action,
            msg: msg,
            width: 200,
            buttons: buttons,
            fn: function(btn) {
                if (btn == 'ok') {
                    //get form input
                    invoiceAmount =  Ext.get('invoiceAmount').getValue();
                    invoiceStructuredMessage = Ext.get('invoiceStructuredMessage').getValue();
                    invoiceAmountInvalidText = null;
                    invoiceStructuredMessageInvalidText = null;

                    var pInvoiceAmount = parseFloat(invoiceAmount.replace(/[^0-9-,]/g, '').replace(/\,/g, '.')), //capture and replace any currency formatting
                        numbers = me.getNumbersFromString(invoiceStructuredMessage),
                        sequence = numbers.substring(0, numbers.length - 2),
                        check = numbers.slice(-2); //last 2 figures

                    //validation
                    if (pInvoiceAmount.toFixed(2) != pInvoiceAmount) { //invoiceAmount validation
                        invoiceAmountInvalidText = "Amount invalid. Check numbers after comma.";
                    }
                    if (!(sequence % 97 == check || (sequence % 97 === 0 && check == 97))) { //structured message validation
                        invoiceStructuredMessageInvalidText = "Structured message invalid. Please verify.";
                    }

                    //followup
                    if (pInvoiceAmount && invoiceStructuredMessage && eBook.Interface.currentFile.gthTeam5FileId && !invoiceAmountInvalidText && !invoiceStructuredMessageInvalidText) {
                        record.invoiceAmount = pInvoiceAmount;
                        record.invoiceStructuredMessage = invoiceStructuredMessage;
                        me.openUploadWindow('b8266c9e-8dcd-414d-bcf7-6014e5e76e1d', 'Invoice NBB ' + eBook.Interface.currentClient.gthTeam5ClientName, 'c6165544-80e3-4390-bc8b-bc159f2cc880');
                    }
                    else {
                        return me.onBtnSuccessClicked(successButton,e,invoiceAmount,invoiceAmountInvalidText,invoiceStructuredMessage,invoiceStructuredMessageInvalidText); //ropen alert with validation explanation (yes I should have used a window)
                    }
                }
                else if (btn == 'yes') {
                    me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 9, null); //Updates service status to 'NBB Filed by GTH'
                    me.getEl().mask("Moving item to awaiting invoice list");
                }
            }
        });
    },

    getNumbersFromString: function (text) {
        return text.replace(/[^0-9]/g, '');
    },

    autoMarkupStructuredMessage: function () {
        var me = this,
            field = Ext.get("invoiceStructuredMessage");

        if (field) {
            var fieldValue = field.getValue(),
                insertString = function (target, source, index) {
                    return (target.slice(0, index) + source + target.slice(index));
                };

            fieldValue = me.getNumbersFromString(fieldValue); //remove anything but numbers

            if (fieldValue.length < 18) { //waterfall through and auto-add chars (if's are faster than switch)
                switch (true) {
                    case fieldValue.length > 11:
                        fieldValue += "+++";
                    case fieldValue.length > 6:
                        fieldValue = insertString(fieldValue, "/", 7);
                    case fieldValue.length > 2:
                        fieldValue = insertString(fieldValue, "/", 3);
                    case fieldValue.length > 0:
                        fieldValue = "+++" + fieldValue;
                        break;
                }
            }
            else {
                Ext.Msg.alert("Too many characters", "Structured message contains too many characters.");
            }

            field.dom.value = fieldValue;
        }
    },

    onBtnFailedClicked: function () {
        var me = this,
            buttons = {yes: "Submit", cancel: "Cancel"};

        Ext.Msg.show({
            title: 'Explain why the filing failed',
            width: 300,
            multiline: 75,
            buttons: buttons,
            fn: function (buttonId, text, opt) {
                var comment = null;
                switch (buttonId) {
                    case 'cancel':
                        //user cancels update
                        break;
                    case 'yes':
                        //user adds comment
                        if (text.length === 0) {return me.onBtnFilingFailedClicked();}
                        comment = text;
                    //notice no break!
                    default:
                        me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 8, comment); //Updates service status to 'Rejected by GTH' and adds review note
                        me.getEl().mask("Informing team of failure");
                }
            }
        });
    },

    openUploadWindow: function (location, filename, repositoryStatusId, periodStart, periodEnd) {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
            standards = {
                location: location,
                periodStart: periodStart,
                periodEnd: periodEnd
            };

        //optionals
        if (filename) {standards.fileName = filename;}
        if (repositoryStatusId) {standards.statusid = repositoryStatusId;}

        var wn = new eBook.Repository.SingleUploadWindow({
            clientId: eBook.Interface.currentClient.gthTeam5ClientId,
            changeFileName: true,
            standards: standards,
            parentCaller: me.ref,
            readycallback: {
                fn: me.onUploadSuccess,
                scope: me
            }
        });

        wn.show(me);
    },
    ///after upload, update annual accounts invoice info & update the status to "Invoice uploaded by GTH"
    onUploadSuccess: function (repositoryItemId) {
        var me = this,
            record = me.recordPnl ? me.recordPnl.record : null;

        record.invoiceRepositoryItemId = repositoryItemId;

        if(record.invoiceAmount && record.invoiceStructuredMessage && record.invoiceRepositoryItemId  && eBook.Interface.currentFile.gthTeam5FileId) {
            Ext.Ajax.request({
                url: eBook.Service.annualAccount + 'UpdateAnnualAccounts',
                method: 'POST',
                params: Ext.encode({
                    cfaadc: {
                        FileId: eBook.Interface.currentFile.gthTeam5FileId,
                        InvoiceAmount: record.invoiceAmount,
                        InvoiceStructuredMessage: record.invoiceStructuredMessage,
                        InvoiceRepositoryItemId: record.invoiceRepositoryItemId
                    }
                }),
                callback: me.onUpdateAnnualAccountsSuccess, //Invoice uploaded by GTH
                scope: me
            });
        }
    },

    ///once the annual accounts record is up to date, update status of service
    onUpdateAnnualAccountsSuccess : function(options, success, response)
    {
        var me = this;

        if (success && response.responseText) {
            me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 11, null);
        }
    },

    updateFileService: function (fileId, serviceId, statusId, comment) {
        //All must be filled in, except for comment
        if (!fileId || !serviceId) {return;}
        //At least one of these must be filled in
        if (!statusId && !comment) {return;}

        var me = this,
            ufsdc = { //file service update object
                FileId: fileId,
                ServiceId: serviceId,
                Person: eBook.User.getActivePersonDataContract()
            };

        me.getEl().mask("Updating...");

        //add optionals
        if (statusId) {ufsdc.Status = statusId;}
        if (comment) {ufsdc.Comment = comment;}

        //call
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'UpdateFileService',
            method: 'POST',
            jsonData: {
                ufsdc: ufsdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText && Ext.decode(response.responseText).UpdateFileServiceResult) {
                    var fsRecord = Ext.decode(response.responseText).UpdateFileServiceResult;
                    if (fsRecord.si == 7) //Picked up by GTH (in-between status)
                    {
                        if (me.recordPnl) {
                            me.recordPnl.record.statusId = fsRecord.si;
                        }
                        me.updateRecordPnl();
                    }
                    else {
                        me.unloadFileServiceRecord();
                    }
                } else {
                    eBook.Interface.showResponseError(response, me.title);
                    me.getEl().unmask();
                }
            },
            scope: me
        });
    },

    ///Notify the team that the files are missing
    onBtnNotifyTeamClicked: function (btn, e) {
        var me = this;
        me.getEl().mask("Sending mail");
        var clientId = eBook.Interface.currentClient.gthTeam5ClientId;
        Ext.Ajax.request({
            url: eBook.Service.GTH + 'Team5NotifyTeam',
            method: 'POST',
            jsonData: {ciacdc: {Id: clientId, Culture: this.ownerCt.ownerCt.period}},
            callback: function (options, success, response) {
                if (success) {
                    Ext.getCmp('GthTeam5MainPanel').getEl().unmask();
                    Ext.Msg.show({
                        title: 'Info',
                        msg: 'Mail sent',
                        buttons: Ext.Msg.OK,
                        fn: function (btn) {
                            if (btn == 'ok') {
                                Ext.getCmp('GthTeam5RepoPanel').standardFilter.pe = null;
                                Ext.getCmp('GthTeam5RepoPanel').standardFilter.ps = null;
                                Ext.getCmp('GthTeam5RepoPanel').hide();
                                Ext.getCmp('GthTeam5NotifyTeamBtn').hide();
                                Ext.getCmp('GthTeam5FilesMissingBtn').hide();
                                Ext.getCmp('GthTeam5FilesPanel').filelist.store.removeAll();
                                Ext.getCmp('GthTeam5TextFieldGfis').setValue("");
                                Ext.getCmp('GthTeam5FilesPanel').setTitle("Files");
                            }
                        },
                        icon: Ext.MessageBox.INFO,
                        scope: this
                    });
                } else {
                    alert('Mail not sent. Please contact an administrator');
                }
            },
            scope: this
        });
    }
});
Ext.reg('GTHTeam5', eBook.GTH.Team5);

eBook.GTH.Team5.FilesList = Ext.extend(Ext.list.ListView, {
    initComponent: function () {
        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                autoDestroy: true,
                root: 'GetDistinctFilesWithCounterResult',
                fields: eBook.data.RecordTypes.DistinctCounterFiles,
                baseParams: {
                    String: null
                },
                proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                    url: eBook.Service.file + 'GetDistinctFilesWithCounter',
                    criteriaParameter: 'csdc',
                    method: 'POST'
                }),
                listeners: {
                    load: function (store, records, options) {
                        Ext.getCmp('GthTeam5FilesPanel').setTitle('Files (' + records[0].get('ClientName') + ')');
                    }
                }
            }),
            multiSelect: false,
            loadingText: 'Loading files...',
            emptyText: eBook.Menu.Client_NoOpenFiles,
            reserveScrollOffset: false,
            cls: 'client-home-openFiles',
            autoHeight: true,
            autoScroll: true,
            singleSelect: true,
            listeners: {
                click: function (listv, index, node, e) {
                    this.ownerCt.ownerCt.period = listv.selected.elements[0].innerText;
                    if (listv.store.getAt(index).get('Counter') > 1) {
                        listv.ownerCt.ownerCt.repo.hide();
                        listv.ownerCt.ownerCt.buttonPnl.btnFilesMissing.hide();
                        listv.ownerCt.ownerCt.btnNotifyTeam.show();
                    } else {

                        eBook.Interface.currentFile = {
                            gthTeam5FileId: listv.store.getAt(index).get("FileId"),
                            gthTeam5StartDate: listv.store.getAt(index).get('StartDate'),
                            gthTeam5EndDate: listv.store.getAt(index).get('EndDate')
                        };
                        eBook.Interface.currentClient = {
                            gthTeam5ClientId: listv.store.getAt(index).get('ClientId')
                        };
                        listv.ownerCt.ownerCt.btnNotifyTeam.hide();
                        listv.ownerCt.ownerCt.repo.standardFilter.pe = listv.store.getAt(index).get('EndDate');
                        listv.ownerCt.ownerCt.repo.standardFilter.ps = listv.store.getAt(index).get('StartDate');
                        listv.ownerCt.ownerCt.repo.show();
                        listv.ownerCt.ownerCt.repo.reload();
                        listv.ownerCt.ownerCt.buttonPnl.btnFilesMissing.show();
                    }
                }
            },
            columns: [{
                header: '#Files',
                dataIndex: 'Counter',
                width: 0.10
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                dataIndex: 'StartDate',
                width: 0.45
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                dataIndex: 'EndDate',
                width: 0.45
            }]


        });
        eBook.GTH.Team5.FilesList.superclass.initComponent.apply(this, arguments);
    }
    /*
     , onMyBodyContext: function(dv, idx, nde, e) {
     //var el = e.getTarget();
     if (dv.ref == 'openFiles') {
     //eBook.Interface.center.clientMenu.mymenu.unHighlightTrashCan();
     if (!this.contextMenu) {
     this.contextMenu = new eBook.Client.ContextMenu({ scope: this });
     this.contextMenu.addItem({
     text: 'Delete',
     iconCls: 'eBook-icon-delete-16',
     handler: function() {
     this.scope.onDeleteClick(this.activeRecord);
     },
     ref: 'deleteFile',
     scope: this.contextMenu
     });
     }
     this.contextMenu.activeRecord = dv.store.getAt(idx);



     this.contextMenu.showMeAt(e);
     }

     }
     , onRestoreClickCallback: function(opts, success, resp) {
     if (success) {
     this.store.load({ params: { ClientId: this.activeClient} });
     }
     }
     */
});

Ext.reg('team5filelist', eBook.GTH.Team5.FilesList);
eBook.GTH.Team6_Process = Ext.extend(Ext.Panel, {
    initComponent: function () {
        Ext.apply(this, {
            layout: { type: 'vbox', align: 'stretch' }
            , border: false
            , items: [{
                xtype: 'GTHClientDetails'
                        , teamId: this.teamId
                        , ref: 'clientDetails'
                        , height: 60
            }, {
                xtype: 'panel'
                        , layout: 'accordion'
                        , cls: 'gth-team2-accord'
                        , animate: true
                        , flex: 1
                        , margins: { top: 0, bottom: 10, left: 0, right: 0 }
                        , items: [{
                            xtype: 'panel'
                                    , step: 1, title: 'Step 1: Excel template, NBB files, Teammembers, File repository'
                                    , layout: { type: 'hbox', align: 'stretch' }
                                    , listeners: { expand: { fn: this.onAccordionPaneExpand, scope: this} }
                                    , items: [{
                                        xtype: 'panel'
                                                , title: 'Excel, NBB & Teammembers'
                                                , flex: 1
                                                , layout: { type: 'vbox', align: 'stretch' }
                                                , items: [{
                                                    xtype: 'eBook.PMT.TeamGrid'
                                                            , ref: '../../../teamgrid'
                                                            , includeMail: true
                                                            , includeEbook: false
                                                            , storeCfg: {
                                                                selectAction: 'GetClientTeamDepartment',
                                                                fields: eBook.data.RecordTypes.TeamMember,
                                                                serviceUrl: eBook.Service.client,
                                                                idField: 'Id',
                                                                autoDestroy: true,
                                                                autoLoad: false,
                                                                criteriaParameter: 'ciddc',
                                                                baseParams: {
                                                                    Department: 'ACR'
                                                                }
                                                            }
                                                            , flex: 1
                                                }]
                                    }, {
                                        xtype: 'panel', layout: { type: 'vbox', align: 'stretch' }, flex: 1, items: [
                                                {
                                                    xtype: 'repository'
                                                    , title: 'Repository'
                                                    , id: 'GthTeam6Repository'
                                                    , ref: '../../../repos'
                                                    , readOnly: true
                                                    , readOnlyMeta: false
                                                    , startFrom: { type: 'perm', id: '32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1' }
                                                    , flex: 3
                                                    , clearOnLoad: true
                                                }, {
                                                    xtype: 'button', flex: 1, handler: this.onAddFileClick, text:'Upload file' , cls: 'eb-massive-btn'
                                                }

                                            ]
                                    }, ]
                        }]
            }
                    , { xtype: 'button', text: 'Finalize', ref: 'finalize', handler: this.onNewClientClick, scope: this, height: 60, cls: 'eb-massive-btn'}]
        });

        eBook.GTH.Team6_Process.superclass.initComponent.apply(this, arguments);
    }
    , initStep: function (owner) {
        //owner.hideStop(); can stop in the middle or start new
        this.client = owner.client;
        this.ownerSteps = owner;
        var cl = this.client;
        //this.clientId = cl.Id;
        this.stepTitle = 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel for ' + cl.ClientName;
        this.clientDetails.initStep(cl);
        if (this.uploadexcel && this.uploadexcel.rendered) this.uploadexcel.reset();
        //this.downloadfiles.getFiles(cl);
        this.teamgrid.store.load({ params: { Id: cl.ClientId} });
        this.loadRepo();
    }
    , loadRepo: function () {
        var cl = this.client;
        this.repos.getEl().mask("Validating &amp; processing files", 'x-mask-loading');
        var rt = this.repos.getRootNode();
        rt.removeAll();
        this.repos.ClientId = cl.ClientId;
        this.repos.filter = { ps: cl.StartDate, pe: cl.EndDate };

        this.repos.getLoader().load(rt, this.onReloadedRepos, this);
        //rt.reload(this.onReloadedRepos, this);
    }
    , onAccordionPaneExpand: function (panel) {
        this.newclientstart.enable();
        if (panel.step == 2) {
            this.newclientstart.disable();
        }
    }
    , onReloadedRepos: function () {
        this.repos.getEl().unmask();
    }
    , onNewClientClick: function () {
        var rt = this.repos.getRootNode();
        var allFilesChecked = true;
        if (rt != null && rt.childNodes.length > 0) {
            var files = rt.childNodes;
            for (var i = 0; i < files.length; i++) {
                var meta = files[i].attributes.Item.Meta;
                if (meta != null && meta.length > 0) {
                    var metaFound = false;
                    for (var j = 0; j < meta.length; j++) {
                        if (meta[j].Id == 'GTHValidated') {
                            metaFound = true;
                            if (meta[j].Value != 'true') {
                                allFilesChecked = false;
                            }
                        }
                    }
                    if (!metaFound) {
                        allFilesChecked = false;
                    }
                } else {
                    Ext.Msg.alert('Error', 'Engagement letter has no meta data');
                }
            }
        }

        if (allFilesChecked) {
            // continue
            this.ownerCt.ownerCt.getEl().mask();
            Ext.Ajax.request({
                url: eBook.Service.GTH + 'ProcessTeam6'
                , method: 'POST'
                , params: Ext.encode({ cpfdc: { StructureId: 'e2e2440a-6e08-444a-9494-052d8e8c3dc2', Files: [], TeamId: this.teamId, Status: 'FINAL', PersonId: eBook.User.personId, GTHClientId: this.client.ClientId} })
                 , callback: this.handleFileProcessing
                , scope: this
            });
        } else {
            Ext.Msg.alert('Error', 'Not all engagement letters have been modified.');
        }
        /*
        Ext.Ajax.request({
        url: eBook.Service.repository + 'GthCheckEngagementLetters'
        , method: 'POST'
        , params: this.repos.getLoader().getSendingParam(rt)
        , callback: this.onSuccess
        , scope: this
        });
        */
    }
    , handleFileProcessing: function (opts, success, resp) {

        this.ownerCt.ownerCt.getEl().unmask();
        //this.ownerCt.ownerCt.ownerSteps.showStop();
        this.ownerSteps.showStop()
        if (success) {
            //this.ownerCt.ownerCt.ownerCt.moveNextStep();
            this.ownerCt.moveNextStep();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }

    }
    , onSuccess: function () {
        alert('success');
    }

    , onAddFileClick: function () {
        var wn = new eBook.Repository.SingleUploadWindow({ clientId: this.ownerCt.ownerCt.ownerCt.ownerCt.client.ClientId });
        wn.show(this);
    }
});
Ext.reg('GTHTeam6Process', eBook.GTH.Team6_Process);


// download empty excel template
// list files team 1 of this period.
eBook.GTH.Team6_DownloadFiles = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.templ = new Ext.XTemplate('<div class="gth-team2-fileslist">'
                                        , '<div class="gth-team2-exceltempl"><a href="Repository/GTHTeam2_Template_v8.xlsx" target="_blank">Download empty Excel template</a></div>'
                                        , '<div class="gth-team2-repo1">TEAM 1 Files:<ul>'
                                        , '<tpl for="files"><li><a href="{[this.getLink(values)]}" target="_blank">{text} <i><tpl for="Item">({Extension})</tpl></i></a></li></tpl>'
                                        , '</ul></div>', {
                                            getLink: function(values) {
                                                return "Repository/" + eBook.getGuidPath(values.Item.ClientId) + values.Item.PhysicalFileId + values.Item.Extension
                                            }
                                        });
        this.templ.compile();
        Ext.apply(this, { html: 'loading data' });
        eBook.GTH.Team6_DownloadFiles.superclass.initComponent.apply(this, arguments);
    }
    , getFiles: function(cl) {
        if (cl.StartDate != null && !Ext.isDate(cl.StartDate)) cl.StartDate = Ext.data.Types.WCFDATE.convert(cl.StartDate);
        if (cl.EndDate != null && !Ext.isDate(cl.EndDate)) cl.EndDate = Ext.data.Types.WCFDATE.convert(cl.EndDate);
        this.getEl().mask("Loading data team 1", 'x-mask-loading');

        Ext.Ajax.request({
            url: eBook.Service.repository + 'QueryFiles'
            , method: 'POST'
            , params: Ext.encode({ cqfdc: { cid: cl.ClientId, sid: '3787F2A4-112D-4589-A231-E9B95BDCFCD9', ps: cl.StartDate, pe: cl.EndDate} })
             , callback: this.onFilesObtained
            , scope: this
        });
    }
    , onFilesObtained: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var o = Ext.decode(resp.responseText).QueryFilesResult;
            this.body.update(this.templ.apply({ files: o }));
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam6DownloadFiles', eBook.GTH.Team6_DownloadFiles);

eBook.GTH.GTHUploadSupDocs = Ext.extend(Ext.form.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            fileUpload: true,
            border: false,
            bodyStyle: 'padding:10px;',
            //layout: { type: 'vbox', align: 'stretch' },
            autoScroll: true,
            /*items: [
                { xtype: 'button', width: 350, text: 'Finalize', handler: this.onFinalizeClick, scope: this, height: 60, cls: 'eb-massive-btn', style: 'margin-top:20px;' }
            ]*/
            items: [
                new Ext.ux.form.FileUploadField({ width: 350, ref: 'pdf', allowBlank: false, fieldLabel: 'Engagement letter', fileTypeRegEx: /((\.pdf)(x){0,1})$/i })
            //new Ext.ux.form.FileUploadField({ width: 350, ref: 'pdf', allowBlank: false, fieldLabel: 'Executive summary', fileTypeRegEx: /((\.pdf))$/i })
                //, { xtype: 'label', text: '/!\\ IMPORTANT: This is the final step for each Client/period. When uploading the excel and pdf file, you will mark this job as "finalized".', style: 'font-weight:bold;' }
                , { xtype: 'button', text: 'Upload file', handler: this.onUploadClick, scope: this/*, cls: 'eb-massive-btn'*/, style: 'margin-top:20px;' }
            ]
        });
        eBook.GTH.GTHUploadSupDocs.superclass.initComponent.apply(this, arguments);
    }
    , onUploadClick: function () {
        var f = this.getForm();
        if (this.pdf.isValid()) {
            f.isValid = function () { return true; };
            this.ownerCt.ownerCt.ownerCt.ownerCt.ownerSteps.hideStop();
            f.submit({
                url: 'UploadMultiple.aspx',
                waitMsg: 'Uploading files',
                success: this.successUpload,
                failure: this.failedUpload,
                scope: this
            });
        } else {
            alert("Selected file/upload field contains invalid or no data.");
        }
    }
     , successUpload: function (fp, o) {
         var exc = o.result.files[0];
         var cl = this.ownerCt.ownerCt.ownerCt.ownerCt.client;
         var fle;
         this.ownerCt.ownerCt.getEl().mask("Validating &amp; processing files", 'x-mask-loading');

         var fls = [];
         for (var i = 0; i < o.result.files.length; i++) {
             fle = o.result.files[i];
             fls.push({ Name: fle.originalFile, Start: cl.StartDate, End: cl.EndDate, Id: fle.id, Extension: fle.extension, ContentType: fle.contentType, fileName: fle.file });
         }


         Ext.Ajax.request({
             url: eBook.Service.GTH + 'ProcessFilesTeam2'
            , method: 'POST'
            , params: Ext.encode({ cpfdc: { StructureId: 'e2e2440a-6e08-444a-9494-052d8e8c3dc2', Files: fls, TeamId: this.ownerCt.ownerCt.ownerCt.ownerCt.teamId, Status: 'FINAL', PersonId: eBook.User.personId, GTHClientId: this.ownerCt.ownerCt.ownerCt.ownerCt.client.ClientId} })
             , callback: this.handleFileProcessing
            , scope: this
         });

         //process o.result.file
         /*this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
       
         �*/

     }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, "UPLOAD FAILED!!")
        this.ownerCt.ownerCt.ownerSteps.showStop();
        this.ownerCt.ownerCt.getEl().unmask();
    }
    , handleFileProcessing: function (opts, success, resp) {
        this.ownerCt.ownerCt.getEl().unmask();
        this.ownerCt.ownerCt.ownerSteps.showStop();
        if (success) {
            this.ownerCt.ownerCt.ownerCt.moveNextStep();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , reset: function() {
        if (this.excel && this.excel.rendered) this.excel.reset();
        if (this.pdf && this.pdf.rendered) this.pdf.reset();
    }
    , onFinalizeClick: function() {
        this.ownerCt.ownerCt.getEl().mask();
        Ext.Ajax.request({
            url: eBook.Service.GTH + 'ProcessTeam6'
                , method: 'POST'
                , params: Ext.encode({ cpfdc: { StructureId: 'e2e2440a-6e08-444a-9494-052d8e8c3dc2', Files: [], TeamId: this.teamId, Status: 'FINAL', PersonId: eBook.User.personId, GTHClientId: this.ownerCt.ownerCt.clientId} })
                 , callback: this.handleFileProcessing
                , scope: this
        });
    }
    
});

Ext.reg('GTHTeam6UploadSupDocs', eBook.GTH.GTHUploadSupDocs);eBook.GTH.TeamExecutor = Ext.extend(Ext.Container, {

    baseCls: 'x-panel',
    collapsedCls: 'x-panel-collapsed',
    maskDisabled: true,
    animCollapse: Ext.enableFx,
    headerAsText: true,
    buttonAlign: 'right',
    collapsed: false,
    collapseFirst: true,
    minButtonWidth: 75,
    preventBodyReset: false,
    padding: undefined,
    resizeEvent: 'bodyresize',
    toolTarget: 'header',
    collapseEl: 'bwrap',
    slideAnchor: 't',
    disabledClass: '',
    deferHeight: true,
    expandDefaults: {
        duration: 0.25
    },
    // private
    collapseDefaults: {
        duration: 0.25
    },

    // private
    initComponent: function() {
        eBook.GTH.TeamExecutor.superclass.initComponent.call(this);
        //        Ext.Panel.superclass.initComponent.call(this);

        //        this.addEvents(
        //            'bodyresize',
        //            'titlechange',
        //            'iconchange'
        //            'collapse',
        //            'expand',
        //            'beforecollapse',
        //            'beforeexpand',
        //            'beforeclose',
        //            'close',
        //            'activate',
        //            'deactivate'
        //        );

        //        if (this.unstyled) {
        //            this.baseCls = 'x-plain';
        //        }


        //        this.toolbars = [];
        //        // shortcuts
        //        if (this.tbar) {
        //            this.elements += ',tbar';
        //            this.topToolbar = this.createToolbar(this.tbar);
        //            this.tbar = null;

        //        }
        //        if (this.bbar) {
        //            this.elements += ',bbar';
        //            this.bottomToolbar = this.createToolbar(this.bbar);
        //            this.bbar = null;
        //        }

        //        if (this.header === true) {
        //            this.elements += ',header';
        //            this.header = null;
        //        } else if (this.headerCfg || (this.title && this.header !== false)) {
        //            this.elements += ',header';
        //        }

        //        if (this.footerCfg || this.footer === true) {
        //            this.elements += ',footer';
        //            this.footer = null;
        //        }

        //        if (this.buttons) {
        //            this.fbar = this.buttons;
        //            this.buttons = null;
        //        }
        //        if (this.fbar) {
        //            this.createFbar(this.fbar);
        //        }
        //        if (this.autoLoad) {
        //            this.on('render', this.doAutoLoad, this, { delay: 10 });
        //        }
    },

    // private
    createFbar: function(fbar) {
        var min = this.minButtonWidth;
        this.elements += ',footer';
        this.fbar = this.createToolbar(fbar, {
            buttonAlign: this.buttonAlign,
            toolbarCls: 'x-panel-fbar',
            enableOverflow: false,
            defaults: function(c) {
                return {
                    minWidth: c.minWidth || min
                };
            }
        });
        // @compat addButton and buttons could possibly be removed
        // @target 4.0
        /**
        * This Panel's Array of buttons as created from the <code>{@link #buttons}</code>
        * config property. Read only.
        * @type Array
        * @property buttons
        */
        this.fbar.items.each(function(c) {
            c.minWidth = c.minWidth || this.minButtonWidth;
        }, this);
        this.buttons = this.fbar.items.items;
    },

    // private
    createToolbar: function(tb, options) {
        var result;
        // Convert array to proper toolbar config
        if (Ext.isArray(tb)) {
            tb = {
                items: tb
            };
        }
        result = tb.events ? Ext.apply(tb, options) : this.createComponent(Ext.apply({}, tb, options), 'toolbar');
        this.toolbars.push(result);
        return result;
    },

    // private
    createElement: function(name, pnode) {
        if (this[name]) {
            pnode.appendChild(this[name].dom);
            return;
        }

        if (name === 'bwrap' || this.elements.indexOf(name) != -1) {
            if (this[name + 'Cfg']) {
                this[name] = Ext.fly(pnode).createChild(this[name + 'Cfg']);
            } else {
                var el = document.createElement('div');
                el.className = this[name + 'Cls'];
                this[name] = Ext.get(pnode.appendChild(el));
            }
            if (this[name + 'CssClass']) {
                this[name].addClass(this[name + 'CssClass']);
            }
            if (this[name + 'Style']) {
                this[name].applyStyles(this[name + 'Style']);
            }
        }
    },
    workpanelRendered: false,
    workpanelRender: function(w, h) {
        if (!this.workpanelRendered && this.workPanelEl) {
            // render step 1
            if (this.steps) {
                this.steps[0] = this.stepRender(this.steps[0], w, h);
                this.activeStep = 0;
                this.setTitle(this.steps[0].stepTitle);
            }
            this.workpanelRendered = true;
        }
    },
    moveNextStep: function() {


        var st = this.activeStep;

        var p = this.steps[st].el.parent();
        p.setVisibilityMode(Ext.Element.DISPLAY);
        p.hide();
        if (st + 1 < this.steps.length) {
            st++;
        } else {
            st = 0;
        }
        var sz = this.workPanelEl.getSize();
        var sh = this.steps[st].rendered;
        this.steps[st] = this.stepRender(this.steps[st], sz.width - 41, sz.height);
        if (sh) {
            p = this.steps[st].el.parent();
            p.setVisibilityMode(Ext.Element.DISPLAY);
            p.show();
        }
        this.steps[st].initStep(this);
        this.setTitle(this.steps[st].stepTitle);
        this.activeStep = st;

    },
    stepRender: function(stepCfg, w, h) {
        if (!stepCfg.rendered) {
            var dv = document.createElement('div');
            dv.className = 'eb-gth-team-workpane-step';

            var wpBody = this.workPanelEl.child('.eb-gth-workpane-body');
            wpBody.appendChild(dv);
            stepCfg.renderTo = dv;
            stepCfg.teamId = this.teamId;
            stepCfg = this.createComponent(stepCfg);
            stepCfg.ownerCt = this;
            //stepCfg.render(dv);
            // stepCfg.rendered = true;
            //stepCfg.el = Ext.get(dv);
            //stepCfg.el.setHeight(wpBody.getHeight());
            stepCfg.setSize(wpBody.getWidth() - 10, wpBody.getHeight());
        }
        return stepCfg;
    },

    // private
    onRender: function(ct, position) {
        if (!this.el) {
            var containingDiv = document.createElement('div');
            containingDiv.className = 'eb-gth-team-container';
            var body = document.createElement('div');
            body.className = 'eb-gth-team-body';
            var listDiv = document.createElement('div');
            listDiv.className = 'eb-gth-team-todo-list';
            var workpane = document.createElement('div');
            workpane.className = 'eb-gth-team-workpane';
            body.appendChild(listDiv);
            body.appendChild(workpane);
            body.id = Ext.id();
            containingDiv.appendChild(body);
            containingDiv.id = Ext.id();
            this.el = Ext.get(containingDiv);
            this.body = Ext.get(body);
            this.listEl = Ext.get(listDiv);
            this.workPanelEl = Ext.get(workpane);

            if (this.title) {
                var listTitle = document.createElement('div');
                listTitle.className = 'eb-gth-todo-title';
                listTitle.innerHTML = this.title;
                this.listEl.appendChild(listTitle);
            }
            var listBody = document.createElement('div');
            listBody.className = 'eb-gth-todo-body';

            this.activeTeam = false;
            if (eBook.User.isExistingRole('GTH Team ' + this.teamId)) {
                var startBtn = document.createElement('div');
                startBtn.className = 'eb-gth-team-startbtn';
                startBtn.innerHTML = 'START WORK';
                listBody.appendChild(startBtn);
                this.activeTeam = true;
            }

            var accord = document.createElement('div');
            accord.className = 'eb-gth-team-accordion';
            listBody.appendChild(accord);

            this.listEl.appendChild(listBody);
            this.accordionEl = Ext.get(accord);
            if (this.activeTeam) this.startEl = Ext.get(startBtn);

            var wpTitle = document.createElement('div');
            wpTitle.className = 'eb-gth-workpane-title';
            wpTitle.innerHTML = 'Klantnaam - ondernemingsnummer - huidige stap';
            var wpBody = document.createElement('div');
            wpBody.className = 'eb-gth-workpane-body';
            this.workPanelEl.appendChild(wpTitle);
            this.workPanelEl.appendChild(wpBody);

            this.workpanelRendered = false;


            this.accordion = new eBook.GTH.TeamAccordion({ teamId: this.teamId, renderTo: this.accordionEl });
            /*{ border: false, renderTo: this.accordionEl,
            /*layout: {
            type:'vbox',
            // padding:'5',
            align:'stretch'
            }**
            layout:'accordion'
            , items: [{ xtype: 'panel', border: false, title: 'test 1', flex:1,html: 'test' }, { xtype: 'panel',flex:1, border: false, title: 'test 2', html: 'test'}] });
            */


        }

        this.workPanelEl.setStyle('display', 'none');

        //        if(!this.el && this.autoEl){
        //            if(Ext.isString(this.autoEl)){
        //                this.el = document.createElement(this.autoEl);
        //            }else{
        //                var div = document.createElement('div');
        //                Ext.DomHelper.overwrite(div, this.autoEl);
        //                this.el = div.firstChild;
        //            }
        //            if (!this.el.id) {
        //                this.el.id = this.getId();
        //            }
        //        }
        //        if(this.el){
        //            this.el = Ext.get(this.el);
        //            if(this.allowDomMove !== false){
        //                ct.dom.insertBefore(this.el.dom, position);
        //                if (div) {
        //                    Ext.removeNode(div);
        //                    div = null;
        //                }
        //            }
        //        }

        //        Ext.Panel.superclass.onRender.call(this, ct, position);
        //        this.createClasses();

        //        var el = this.el,
        //            d = el.dom,
        //            bw,
        //            ts;


        //        if (this.collapsible && !this.hideCollapseTool) {
        //            this.tools = this.tools ? this.tools.slice(0) : [];
        //            this.tools[this.collapseFirst ? 'unshift' : 'push']({
        //                id: 'toggle',
        //                handler: this.toggleCollapse,
        //                scope: this
        //            });
        //        }

        //        if (this.tools) {
        //            ts = this.tools;
        //            this.elements += (this.header !== false) ? ',header' : '';
        //        }
        //        this.tools = {};

        //        el.addClass(this.baseCls);
        //        if (d.firstChild) { // existing markup
        //            this.header = el.down('.' + this.headerCls);
        //            this.bwrap = el.down('.' + this.bwrapCls);
        //            var cp = this.bwrap ? this.bwrap : el;
        //            this.tbar = cp.down('.' + this.tbarCls);
        //            this.body = cp.down('.' + this.bodyCls);
        //            this.bbar = cp.down('.' + this.bbarCls);
        //            this.footer = cp.down('.' + this.footerCls);
        //            this.fromMarkup = true;
        //        }
        //        if (this.preventBodyReset === true) {
        //            el.addClass('x-panel-reset');
        //        }
        //        if (this.cls) {
        //            el.addClass(this.cls);
        //        }

        //        if (this.buttons) {
        //            this.elements += ',footer';
        //        }

        //        // This block allows for maximum flexibility and performance when using existing markup

        //        // framing requires special markup
        //        if (this.frame) {
        //            el.insertHtml('afterBegin', String.format(Ext.Element.boxMarkup, this.baseCls));

        //            this.createElement('header', d.firstChild.firstChild.firstChild);
        //            this.createElement('bwrap', d);

        //            // append the mid and bottom frame to the bwrap
        //            bw = this.bwrap.dom;
        //            var ml = d.childNodes[1], bl = d.childNodes[2];
        //            bw.appendChild(ml);
        //            bw.appendChild(bl);

        //            var mc = bw.firstChild.firstChild.firstChild;
        //            this.createElement('tbar', mc);
        //            this.createElement('body', mc);
        //            this.createElement('bbar', mc);
        //            this.createElement('footer', bw.lastChild.firstChild.firstChild);

        //            if (!this.footer) {
        //                this.bwrap.dom.lastChild.className += ' x-panel-nofooter';
        //            }
        //            /*
        //            * Store a reference to this element so:
        //            * a) We aren't looking it up all the time
        //            * b) The last element is reported incorrectly when using a loadmask
        //            */
        //            this.ft = Ext.get(this.bwrap.dom.lastChild);
        //            this.mc = Ext.get(mc);
        //        } else {
        //            this.createElement('header', d);
        //            this.createElement('bwrap', d);

        //            // append the mid and bottom frame to the bwrap
        //            bw = this.bwrap.dom;
        //            this.createElement('tbar', bw);
        //            this.createElement('body', bw);
        //            this.createElement('bbar', bw);
        //            this.createElement('footer', bw);

        //            if (!this.header) {
        //                this.body.addClass(this.bodyCls + '-noheader');
        //                if (this.tbar) {
        //                    this.tbar.addClass(this.tbarCls + '-noheader');
        //                }
        //            }
        //        }

        //        if (Ext.isDefined(this.padding)) {
        //            this.body.setStyle('padding', this.body.addUnits(this.padding));
        //        }

        //        if (this.border === false) {
        //            this.el.addClass(this.baseCls + '-noborder');
        //            this.body.addClass(this.bodyCls + '-noborder');
        //            if (this.header) {
        //                this.header.addClass(this.headerCls + '-noborder');
        //            }
        //            if (this.footer) {
        //                this.footer.addClass(this.footerCls + '-noborder');
        //            }
        //            if (this.tbar) {
        //                this.tbar.addClass(this.tbarCls + '-noborder');
        //            }
        //            if (this.bbar) {
        //                this.bbar.addClass(this.bbarCls + '-noborder');
        //            }
        //        }

        //        if (this.bodyBorder === false) {
        //            this.body.addClass(this.bodyCls + '-noborder');
        //        }

        //        this.bwrap.enableDisplayMode('block');

        //        if (this.header) {
        //            this.header.unselectable();

        //            // for tools, we need to wrap any existing header markup
        //            if (this.headerAsText) {
        //                this.header.dom.innerHTML =
        //                    '<span class="' + this.headerTextCls + '">' + this.header.dom.innerHTML + '</span>';

        //                if (this.iconCls) {
        //                    this.setIconClass(this.iconCls);
        //                }
        //            }
        //        }

        //        if (this.floating) {
        //            this.makeFloating(this.floating);
        //        }

        //        if (this.collapsible && this.titleCollapse && this.header) {
        //            this.mon(this.header, 'click', this.toggleCollapse, this);
        //            this.header.setStyle('cursor', 'pointer');
        //        }
        //        if (ts) {
        //            this.addTool.apply(this, ts);
        //        }

        //        // Render Toolbars.
        //        if (this.fbar) {
        //            this.footer.addClass('x-panel-btns');
        //            this.fbar.ownerCt = this;
        //            this.fbar.render(this.footer);
        //            this.footer.createChild({ cls: 'x-clear' });
        //        }
        //        if (this.tbar && this.topToolbar) {
        //            this.topToolbar.ownerCt = this;
        //            this.topToolbar.render(this.tbar);
        //        }
        //        if (this.bbar && this.bottomToolbar) {
        //            this.bottomToolbar.ownerCt = this;
        //            this.bottomToolbar.render(this.bbar);
        //        }
    },

    /**
    * Sets the CSS class that provides the icon image for this panel.  This method will replace any existing
    * icon class if one has already been set and fire the {@link #iconchange} event after completion.
    * @param {String} cls The new CSS class name
    */
    setIconClass: function(cls) {
        var old = this.iconCls;
        this.iconCls = cls;
        if (this.rendered && this.header) {
            if (this.frame) {
                this.header.addClass('x-panel-icon');
                this.header.replaceClass(old, this.iconCls);
            } else {
                var hd = this.header,
                    img = hd.child('img.x-panel-inline-icon');
                if (img) {
                    Ext.fly(img).replaceClass(old, this.iconCls);
                } else {
                    var hdspan = hd.child('span.' + this.headerTextCls);
                    if (hdspan) {
                        Ext.DomHelper.insertBefore(hdspan.dom, {
                            tag: 'img', src: Ext.BLANK_IMAGE_URL, cls: 'x-panel-inline-icon ' + this.iconCls
                        });
                    }
                }
            }
        }
        this.fireEvent('iconchange', this, cls, old);
    },

    // private
    makeFloating: function(cfg) {
        this.floating = true;
        this.el = new Ext.Layer(Ext.apply({}, cfg, {
            shadow: Ext.isDefined(this.shadow) ? this.shadow : 'sides',
            shadowOffset: this.shadowOffset,
            constrain: false,
            shim: this.shim === false ? false : undefined
        }), this.el);
    },

    /**
    * Returns the {@link Ext.Toolbar toolbar} from the top (<code>{@link #tbar}</code>) section of the panel.
    * @return {Ext.Toolbar} The toolbar
    */
    getTopToolbar: function() {
        return this.topToolbar;
    },

    /**
    * Returns the {@link Ext.Toolbar toolbar} from the bottom (<code>{@link #bbar}</code>) section of the panel.
    * @return {Ext.Toolbar} The toolbar
    */
    getBottomToolbar: function() {
        return this.bottomToolbar;
    },

    /**
    * Returns the {@link Ext.Toolbar toolbar} from the footer (<code>{@link #fbar}</code>) section of the panel.
    * @return {Ext.Toolbar} The toolbar
    */
    getFooterToolbar: function() {
        return this.fbar;
    },

    /**
    * Adds a button to this panel.  Note that this method must be called prior to rendering.  The preferred
    * approach is to add buttons via the {@link #buttons} config.
    * @param {String/Object} config A valid {@link Ext.Button} config.  A string will become the text for a default
    * button config, an object will be treated as a button config object.
    * @param {Function} handler The function to be called on button {@link Ext.Button#click}
    * @param {Object} scope The scope (<code>this</code> reference) in which the button handler function is executed. Defaults to the Button.
    * @return {Ext.Button} The button that was added
    */
    addButton: function(config, handler, scope) {
        if (!this.fbar) {
            this.createFbar([]);
        }
        if (handler) {
            if (Ext.isString(config)) {
                config = { text: config };
            }
            config = Ext.apply({
                handler: handler,
                scope: scope
            }, config);
        }
        return this.fbar.add(config);
    },

    // private
    addTool: function() {
        if (!this.rendered) {
            if (!this.tools) {
                this.tools = [];
            }
            Ext.each(arguments, function(arg) {
                this.tools.push(arg);
            }, this);
            return;
        }
        // nowhere to render tools!
        if (!this[this.toolTarget]) {
            return;
        }
        if (!this.toolTemplate) {
            // initialize the global tool template on first use
            var tt = new Ext.Template(
                 '<div class="x-tool x-tool-{id}">&#160;</div>'
            );
            tt.disableFormats = true;
            tt.compile();
            eBook.GTH.TeamExecutor.prototype.toolTemplate = tt;
        }
        for (var i = 0, a = arguments, len = a.length; i < len; i++) {
            var tc = a[i];
            if (!this.tools[tc.id]) {
                var overCls = 'x-tool-' + tc.id + '-over';
                var t = this.toolTemplate.insertFirst(this[this.toolTarget], tc, true);
                this.tools[tc.id] = t;
                t.enableDisplayMode('block');
                this.mon(t, 'click', this.createToolHandler(t, tc, overCls, this));
                if (tc.on) {
                    this.mon(t, tc.on);
                }
                if (tc.hidden) {
                    t.hide();
                }
                if (tc.qtip) {
                    if (Ext.isObject(tc.qtip)) {
                        Ext.QuickTips.register(Ext.apply({
                            target: t.id
                        }, tc.qtip));
                    } else {
                        t.dom.qtip = tc.qtip;
                    }
                }
                t.addClassOnOver(overCls);
            }
        }
    },

    onLayout: function(shallow, force) {
        eBook.GTH.TeamExecutor.superclass.onLayout.apply(this, arguments);
        //        if (this.hasLayout && this.toolbars.length > 0) {
        //            Ext.each(this.toolbars, function(tb) {
        //                tb.doLayout(undefined, force);
        //            });
        //            this.syncHeight();
        //        }
    },

    syncHeight: function() {
        var h = this.toolbarHeight,
                bd = this.body,
                lsh = this.lastSize.height,
                sz;

        if (this.autoHeight || !Ext.isDefined(lsh) || lsh == 'auto') {
            return;
        }


        if (h != this.getToolbarHeight()) {
            h = Math.max(0, lsh - this.getFrameHeight());
            bd.setHeight(h);
            sz = bd.getSize();
            this.toolbarHeight = this.getToolbarHeight();
            this.onBodyResize(sz.width, sz.height);
        }
    },

    // private
    onShow: function() {
        if (this.floating) {
            return this.el.show();
        }
        eBook.GTH.TeamExecutor.superclass.onShow.call(this);
    },

    // private
    onHide: function() {
        if (this.floating) {
            return this.el.hide();
        }
        eBook.GTH.TeamExecutor.superclass.onHide.call(this);
    },

    // private
    createToolHandler: function(t, tc, overCls, panel) {
        return function(e) {
            t.removeClass(overCls);
            if (tc.stopEvent !== false) {
                e.stopEvent();
            }
            if (tc.handler) {
                tc.handler.call(tc.scope || t, e, t, panel, tc);
            }
        };
    },
    hideStop: function() {
        if (this.startEl) {
            this.startEl.setVisibilityMode(Ext.Element.DISPLAY);
            this.startEl.hide();
        }
    },
    showStop: function() {
        if (this.startEl) {
            this.startEl.setVisibilityMode(Ext.Element.DISPLAY);
            this.startEl.show();
        }
    },
    stopWork: function() {
        //this.el.setX(this.prevX);
        this.workPanelEl.setWidth(0);
        this.workPanelEl.setStyle.defer(1900, this.workPanelEl, ['display', 'none']);
        this.el.setX.defer(2000, this.el, [this.prevX]);
        this.ownerCt.stopItem.defer(2500, this.ownerCt, [this.ownerCt.items.indexOf(this)]);
        this.working = false;
        if (this.startEl) {
            this.startEl.update('START WORK');
            this.startEl.show();
        }
    },
    startWork: function() {
        if (!this.working) {
            this.working = true;
            if (this.startEl) this.startEl.update('STOP WORK');
            var listSz = this.listEl.getSize();
            var ownerCT = this.ownerCt.getSize(true);
            var w = ownerCT.width - listSz.width - 30;
            var h = listSz.height - 40;
            var lx = this.ownerCt.el.getLeft(false) + 8; //+this.ownerCt.el.getPadding('l') + this.body.getPadding('l');

            this.workSizes = {
                l: { x: lx + 5 }
                , wp: { h: listSz.height - 40, w: ownerCT.width - listSz.width - 20
                         , x: listSz.width//this.listEl.getComputedWidth()
                         , y: this.listEl.getY()
                }
                , gw: ownerCT.width - 20
            };
            this.workSizes.wpb = { h: this.workSizes.wp.h - 41, w: this.workSizes.wp.w};
            //this.workPanelEl.setWidth(w);
            this.workPanelEl.setHeight(this.workSizes.wp.h);
            wpBody = this.workPanelEl.child('.eb-gth-workpane-body');
            wpBody.setHeight(this.workSizes.wpb.h);
            wpBody.setWidth(this.workSizes.wpb.w);
            this.workpanelRender(this.workSizes.wp.w, this.workSizes.wp.h);
            this.prevX = this.el.getX();

            this.el.setX(lx);

            this.startWorkStep2.defer(500, this);
        } else {
            this.stopWork();
        }
        //this.workPanelEl.move('r', listSz.width + w, true);
    },
    startWorkStep2: function() {

        this.el.setWidth(this.workSizes.gw);
        this.body.setWidth(this.workSizes.gw);
        this.listEl.setStyle('position', 'absolute');


        this.workPanelEl.setStyle('-webkit-transition', 'none');
        this.workPanelEl.setStyle('-moz-transition', 'none');
        this.workPanelEl.setStyle('position', 'absolute');
        this.workPanelEl.setStyle('left', (this.workSizes.wp.x) + 'px');
        this.workPanelEl.setStyle('top', '20' + 'px');
        this.workPanelEl.setStyle('width', '0px');
        this.workPanelEl.setStyle('display', 'block');
        this.workPanelEl.setStyle('-moz-transition', 'all 2s ease-in-out');
        this.workPanelEl.setStyle('-webkit-transition', 'all 2s ease-in-out');
        this.workPanelEl.setStyle.defer(500, this.workPanelEl, ['width', this.workSizes.wp.w + 'px']);
        if (this.steps) {
            var st = this.activeStep;
            if (st != 0) {
                var p = this.steps[st].el.parent();
                p.setVisibilityMode(Ext.Element.DISPLAY);
                p.hide();

            }
            st = 0;
            var sh = this.steps[st].rendered;
            //this.steps[st] = this.stepRender(this.steps[st], sz.width - 41, sz.height);
            if (sh) {
                p = this.steps[st].el.parent();
                p.setVisibilityMode(Ext.Element.DISPLAY);
                p.show();
            }
            
            this.setTitle(this.steps[st].stepTitle);
            this.activeStep = st;

            this.steps[this.activeStep].setSize(this.workSizes.wpb.w, this.workSizes.wpb.h);
            if(this.steps[this.activeStep].initStep) {
                this.steps[this.activeStep].initStep(this);
            }
        }
    },
    // private
    afterRender: function() {
        //        if (this.floating && !this.hidden) {
        //            this.el.show();
        //        }
        //        if (this.title) {
        //            this.setTitle(this.title);
        //        }
        eBook.GTH.TeamExecutor.superclass.afterRender.call(this); // do sizing calcs last
        //        if (this.collapsed) {
        //            this.collapsed = false;
        //            this.collapse(false);
        //        }
        this.initEvents();
    },

    // private
    getKeyMap: function() {
        if (!this.keyMap) {
            this.keyMap = new Ext.KeyMap(this.el, this.keys);
        }
        return this.keyMap;
    },

    // private
    initEvents: function() {
        //        if (this.keys) {
        //            this.getKeyMap();
        //        }
        //        if (this.draggable) {
        //            this.initDraggable();
        //        }
        //        if (this.toolbars.length > 0) {
        //            Ext.each(this.toolbars, function(tb) {
        //                tb.doLayout();
        //                tb.on({
        //                    scope: this,
        //                    afterlayout: this.syncHeight,
        //                    remove: this.syncHeight
        //                });
        //            }, this);
        //            this.syncHeight();
        //        }
        if (this.startEl) {
            this.startEl.on('click', function() {
                this.ownerCt.startItem(this.ownerCt.items.indexOf(this));
            }, this);
        }

    },

    //    // private
    //    initDraggable: function() {
    //        /**
    //        * <p>If this Panel is configured {@link #draggable}, this property will contain
    //        * an instance of {@link Ext.dd.DragSource} which handles dragging the Panel.</p>
    //        * The developer must provide implementations of the abstract methods of {@link Ext.dd.DragSource}
    //        * in order to supply behaviour for each stage of the drag/drop process. See {@link #draggable}.
    //        * @type Ext.dd.DragSource.
    //        * @property dd
    //        */
    //        this.dd = new Ext.Panel.DD(this, Ext.isBoolean(this.draggable) ? null : this.draggable);
    //    },

    // private
    beforeEffect: function(anim) {
        if (this.floating) {
            this.el.beforeAction();
        }
        if (anim !== false) {
            this.el.addClass('x-panel-animated');
        }
    },

    // private
    afterEffect: function(anim) {
        this.syncShadow();
        this.el.removeClass('x-panel-animated');
    },

    // private - wraps up an animation param with internal callbacks
    createEffect: function(a, cb, scope) {
        var o = {
            scope: scope,
            block: true
        };
        if (a === true) {
            o.callback = cb;
            return o;
        } else if (!a.callback) {
            o.callback = cb;
        } else { // wrap it up
            o.callback = function() {
                cb.call(scope);
                Ext.callback(a.callback, a.scope);
            };
        }
        return Ext.applyIf(o, a);
    },

    /**
    * Collapses the panel body so that it becomes hidden.  Fires the {@link #beforecollapse} event which will
    * cancel the collapse action if it returns false.
    * @param {Boolean} animate True to animate the transition, else false (defaults to the value of the
    * {@link #animCollapse} panel config)
    * @return {Ext.Panel} this
    */
    collapse: function(animate) {
        if (this.collapsed || this.el.hasFxBlock() || this.fireEvent('beforecollapse', this, animate) === false) {
            return;
        }
        var doAnim = animate === true || (animate !== false && this.animCollapse);
        this.beforeEffect(doAnim);
        this.onCollapse(doAnim, animate);
        return this;
    },

    // private
    onCollapse: function(doAnim, animArg) {
        if (doAnim) {
            this[this.collapseEl].slideOut(this.slideAnchor,
                    Ext.apply(this.createEffect(animArg || true, this.afterCollapse, this),
                        this.collapseDefaults));
        } else {
            this[this.collapseEl].hide(this.hideMode);
            this.afterCollapse(false);
        }
    },

    // private
    afterCollapse: function(anim) {
        this.collapsed = true;
        this.el.addClass(this.collapsedCls);
        if (anim !== false) {
            this[this.collapseEl].hide(this.hideMode);
        }
        this.afterEffect(anim);

        // Reset lastSize of all sub-components so they KNOW they are in a collapsed container
        this.cascade(function(c) {
            if (c.lastSize) {
                c.lastSize = { width: undefined, height: undefined };
            }
        });
        this.fireEvent('collapse', this);
    },

    /**
    * Expands the panel body so that it becomes visible.  Fires the {@link #beforeexpand} event which will
    * cancel the expand action if it returns false.
    * @param {Boolean} animate True to animate the transition, else false (defaults to the value of the
    * {@link #animCollapse} panel config)
    * @return {Ext.Panel} this
    */
    expand: function(animate) {
        if (!this.collapsed || this.el.hasFxBlock() || this.fireEvent('beforeexpand', this, animate) === false) {
            return;
        }
        var doAnim = animate === true || (animate !== false && this.animCollapse);
        this.el.removeClass(this.collapsedCls);
        this.beforeEffect(doAnim);
        this.onExpand(doAnim, animate);
        return this;
    },

    // private
    onExpand: function(doAnim, animArg) {
        if (doAnim) {
            this[this.collapseEl].slideIn(this.slideAnchor,
                    Ext.apply(this.createEffect(animArg || true, this.afterExpand, this),
                        this.expandDefaults));
        } else {
            this[this.collapseEl].show(this.hideMode);
            this.afterExpand(false);
        }
    },

    // private
    afterExpand: function(anim) {
        this.collapsed = false;
        if (anim !== false) {
            this[this.collapseEl].show(this.hideMode);
        }
        this.afterEffect(anim);
        if (this.deferLayout) {
            delete this.deferLayout;
            this.doLayout(true);
        }
        this.fireEvent('expand', this);
    },

    /**
    * Shortcut for performing an {@link #expand} or {@link #collapse} based on the current state of the panel.
    * @param {Boolean} animate True to animate the transition, else false (defaults to the value of the
    * {@link #animCollapse} panel config)
    * @return {Ext.Panel} this
    */
    toggleCollapse: function(animate) {
        this[this.collapsed ? 'expand' : 'collapse'](animate);
        return this;
    },

    // private
    onDisable: function() {
        if (this.rendered && this.maskDisabled) {
            this.el.mask();
        }
        eBook.GTH.TeamExecutor.superclass.onDisable.call(this);
    },

    // private
    onEnable: function() {
        if (this.rendered && this.maskDisabled) {
            this.el.unmask();
        }
        eBook.GTH.TeamExecutor.superclass.onEnable.call(this);
    },
    working: false,
    // private
    onResize: function(adjWidth, adjHeight, rawWidth, rawHeight) {
        var w = adjWidth,
            h = adjHeight;

        if (Ext.isDefined(w) || Ext.isDefined(h)) {
            if (!this.collapsed) {
                // First, set the the Panel's body width.
                // If we have auto-widthed it, get the resulting full offset width so we can size the Toolbars to match
                // The Toolbars must not buffer this resize operation because we need to know their heights.

                if (Ext.isNumber(w)) {
                    this.body.setWidth(w = this.adjustBodyWidth(w)); //- this.getFrameWidth()
                } else if (w == 'auto') {
                    w = this.body.setWidth('auto').dom.offsetWidth;
                } else {
                    w = this.body.dom.offsetWidth;
                }
                if (!this.working) {
                    this.listEl.setWidth(w);
                }

                //                if (this.tbar) {
                //                    this.tbar.setWidth(w);
                //                    if (this.topToolbar) {
                //                        this.topToolbar.setSize(w);
                //                    }
                //                }
                //                if (this.bbar) {
                //                    this.bbar.setWidth(w);
                //                    if (this.bottomToolbar) {
                //                        this.bottomToolbar.setSize(w);
                //                        // The bbar does not move on resize without this.
                //                        if (Ext.isIE) {
                //                            this.bbar.setStyle('position', 'static');
                //                            this.bbar.setStyle('position', '');
                //                        }
                //                    }
                //                }
                //                if (this.footer) {
                //                    this.footer.setWidth(w);
                //                    if (this.fbar) {
                //                        this.fbar.setSize(Ext.isIE ? (w - this.footer.getFrameWidth('lr')) : 'auto');
                //                    }
                //                }

                // At this point, the Toolbars must be layed out for getFrameHeight to find a result.
                if (Ext.isNumber(h)) {
                    h = Math.max(0, h); //- this.getFrameHeight()
                    //h = Math.max(0, h - (this.getHeight() - this.body.getHeight()));
                    this.body.setHeight(h);
                } else if (h == 'auto') {
                    this.body.setHeight(h);
                }
                // if (!this.working) {
                this.listEl.setHeight(h);
                var hh = this.listEl.child('.eb-gth-todo-title').getHeight();
                var sbh = this.startEl ? this.listEl.child('.eb-gth-team-startbtn').getHeight() : 0;
                this.accordionEl.setHeight(h - sbh - hh);
                var w = this.accordion.el.getWidth();
                this.accordion.setSize(w, h - hh - sbh);
                // }

                if (this.disabled && this.el._mask) {
                    this.el._mask.setSize(this.el.dom.clientWidth, this.el.getHeight());
                }
            } else {
                // Adds an event to set the correct height afterExpand.  This accounts for the deferHeight flag in panel
                this.queuedBodySize = { width: w, height: h };
                if (!this.queuedExpand && this.allowQueuedExpand !== false) {
                    this.queuedExpand = true;
                    this.on('expand', function() {
                        delete this.queuedExpand;
                        this.onResize(this.queuedBodySize.width, this.queuedBodySize.height);
                    }, this, { single: true });
                }
            }
            this.onBodyResize(w, h);
        }
        // this.syncShadow();
        eBook.GTH.TeamExecutor.superclass.onResize.call(this, adjWidth, adjHeight, rawWidth, rawHeight);

    },

    // private
    onBodyResize: function(w, h) {
        this.fireEvent('bodyresize', this, w, h);
    },

    // private
    getToolbarHeight: function() {
        var h = 0;
        if (this.rendered) {
            Ext.each(this.toolbars, function(tb) {
                h += tb.getHeight();
            }, this);
        }
        return h;
    },

    // deprecate
    adjustBodyHeight: function(h) {
        return h;
    },

    // private
    adjustBodyWidth: function(w) {
        return w;
    },

    // private
    onPosition: function() {
        this.syncShadow();
    },

    /**
    * Returns the width in pixels of the framing elements of this panel (not including the body width).  To
    * retrieve the body width see {@link #getInnerWidth}.
    * @return {Number} The frame width
    */
    getFrameWidth: function() {
        var w = this.el.getFrameWidth('lr');

        if (this.frame) {
            var l = this.body.dom;
            w += (Ext.fly(l).getFrameWidth('l') + Ext.fly(l.firstChild).getFrameWidth('r'));
            w += this.mc.getFrameWidth('lr');
        }
        return w;
    },

    /**
    * Returns the height in pixels of the framing elements of this panel (including any top and bottom bars and
    * header and footer elements, but not including the body height).  To retrieve the body height see {@link #getInnerHeight}.
    * @return {Number} The frame height
    */
    getFrameHeight: function() {
        var h = Math.max(0, this.getHeight() - this.body.getHeight());

        if (isNaN(h)) {
            h = 0;
        }
        return h;

        /* Deprecate
        var h  = this.el.getFrameWidth('tb') + this.bwrap.getFrameWidth('tb');
        h += (this.tbar ? this.tbar.getHeight() : 0) +
        (this.bbar ? this.bbar.getHeight() : 0);

            if(this.frame){
        h += this.el.dom.firstChild.offsetHeight + this.ft.dom.offsetHeight + this.mc.getFrameWidth('tb');
        }else{
        h += (this.header ? this.header.getHeight() : 0) +
        (this.footer ? this.footer.getHeight() : 0);
        }
        return h;
        */
    },

    /**
    * Returns the width in pixels of the body element (not including the width of any framing elements).
    * For the frame width see {@link #getFrameWidth}.
    * @return {Number} The body width
    */
    getInnerWidth: function() {
        return this.getSize().width - this.getFrameWidth();
    },

    /**
    * Returns the height in pixels of the body element (not including the height of any framing elements).
    * For the frame height see {@link #getFrameHeight}.
    * @return {Number} The body height
    */
    getInnerHeight: function() {
        return this.body.getHeight();
        /* Deprecate
        return this.getSize().height - this.getFrameHeight();
        */
    },

    // private
    syncShadow: function() {
        if (this.floating) {
            this.el.sync(true);
        }
    },

    // private
    getLayoutTarget: function() {
        return this.body;
    },

    // private
    getContentTarget: function() {
        return this.body;
    },

    /**
    * <p>Sets the title text for the panel and optionally the {@link #iconCls icon class}.</p>
    * <p>In order to be able to set the title, a header element must have been created
    * for the Panel. This is triggered either by configuring the Panel with a non-blank <code>{@link #title}</code>,
    * or configuring it with <code><b>{@link #header}: true</b></code>.</p>
    * @param {String} title The title text to set
    * @param {String} iconCls (optional) {@link #iconCls iconCls} A user-defined CSS class that provides the icon image for this panel
    */
    setTitle: function(title, iconCls) {
        this.title = title;
        var wpTitle = this.el.child('.eb-gth-workpane-title');
        wpTitle.update(title);
        return this;
    },

    /**
    * Get the {@link Ext.Updater} for this panel. Enables you to perform Ajax updates of this panel's body.
    * @return {Ext.Updater} The Updater
    */
    getUpdater: function() {
        return this.body.getUpdater();
    },

    /**
    * Loads this content panel immediately with content returned from an XHR call.
    * @param {Object/String/Function} config A config object containing any of the following options:
    <pre><code>
    panel.load({
    url: 'your-url.php',
    params: {param1: 'foo', param2: 'bar'}, // or a URL encoded string
    callback: yourFunction,
    scope: yourObject, // optional scope for the callback
    discardUrl: false,
    nocache: false,
    text: 'Loading...',
    timeout: 30,
    scripts: false
    });
    </code></pre>
    * The only required property is url. The optional properties nocache, text and scripts
    * are shorthand for disableCaching, indicatorText and loadScripts and are used to set their
    * associated property on this panel Updater instance.
    * @return {Ext.Panel} this
    */
    load: function() {
        var um = this.body.getUpdater();
        um.update.apply(um, arguments);
        return this;
    },

    // private
    beforeDestroy: function() {
        Ext.Panel.superclass.beforeDestroy.call(this);
        if (this.header) {
            this.header.removeAllListeners();
        }
        if (this.tools) {
            for (var k in this.tools) {
                Ext.destroy(this.tools[k]);
            }
        }
        if (this.toolbars.length > 0) {
            Ext.each(this.toolbars, function(tb) {
                tb.un('afterlayout', this.syncHeight, this);
                tb.un('remove', this.syncHeight, this);
            }, this);
        }
        if (Ext.isArray(this.buttons)) {
            while (this.buttons.length) {
                Ext.destroy(this.buttons[0]);
            }
        }
        if (this.rendered) {
            Ext.destroy(
                this.ft,
                this.header,
                this.footer,
                this.tbar,
                this.bbar,
                this.body,
                this.mc,
                this.bwrap,
                this.dd
            );
            if (this.fbar) {
                Ext.destroy(
                    this.fbar,
                    this.fbar.el
                );
            }
        }
        Ext.destroy(this.toolbars);
    },

    // private
    createClasses: function() {
        this.headerCls = this.baseCls + '-header';
        this.headerTextCls = this.baseCls + '-header-text';
        this.bwrapCls = this.baseCls + '-bwrap';
        this.tbarCls = this.baseCls + '-tbar';
        this.bodyCls = this.baseCls + '-body';
        this.bbarCls = this.baseCls + '-bbar';
        this.footerCls = this.baseCls + '-footer';
    },

    // private
    createGhost: function(cls, useShim, appendTo) {
        var el = document.createElement('div');
        el.className = 'x-panel-ghost ' + (cls ? cls : '');
        if (this.header) {
            el.appendChild(this.el.dom.firstChild.cloneNode(true));
        }
        Ext.fly(el.appendChild(document.createElement('ul'))).setHeight(this.bwrap.getHeight());
        el.style.width = this.el.dom.offsetWidth + 'px'; ;
        if (!appendTo) {
            this.container.dom.appendChild(el);
        } else {
            Ext.getDom(appendTo).appendChild(el);
        }
        if (useShim !== false && this.el.useShim !== false) {
            var layer = new Ext.Layer({ shadow: false, useDisplay: true, constrain: false }, el);
            layer.show();
            return layer;
        } else {
            return new Ext.Element(el);
        }
    },

    // private
    doAutoLoad: function() {
        var u = this.body.getUpdater();
        if (this.renderer) {
            u.setRenderer(this.renderer);
        }
        u.update(Ext.isObject(this.autoLoad) ? this.autoLoad : { url: this.autoLoad });
    },

    /**
    * Retrieve a tool by id.
    * @param {String} id
    * @return {Object} tool
    */
    getTool: function(id) {
        return this.tools[id];
    }

    , selectClient: function(client, stepid) {
        this.client = client;
        this.moveNextStep();
    }
    , stopUpdateTask: function() {
        if (this.accordion) this.accordion.stopUpdateTask();
    }
    , startUpdateTask: function() {
        if (this.accordion) this.accordion.startUpdateTask();

    }

});
Ext.reg('TeamExecutor', eBook.GTH.TeamExecutor);

//eBook.GTH.ToDoList = Ext.extend(Ext.list.ListView, {

/*

eBook.GTH.ClientSelector = Ext.extend(Ext.Panel, {
initComponent: function() {
Ext.apply(this, {
layout: {
type: 'vbox',
// padding:'5',
align: 'stretch'
}
, items: [{ xtype: 'panel', border: false,bodyStyle:'text-align:center;padding:10px;', items: [{ xtype: 'button',width:400,style:'margin:auto;font-weight:bold', text: 'Select first available client', handler: this.selectFirstAvailable}], region: 'north', height: 60 }
, { xtype: 'panel', border: false, flex: 1, html: 'fjezifjezofjezo jezo fjezofjez f', region: 'center' }
]
});
eBook.GTH.ClientSelector.superclass.initComponent.apply(this, arguments);
}
});

Ext.reg('GTHClientSelector', eBook.GTH.ClientSelector);
*/



eBook.GTH.Home = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'hbox'
            , layoutConfig: {
                padding: '5',
                defaultMargins: { top: 0, left: 0, right: 5, bottom: 0 },
                align: 'stretch'
            }
            , cls: 'gth-home'
            , items: [
                { xtype: 'TeamExecutor', title: 'Team 1 <br/> Statutory Accounts - NBB Filing', flex: 1
                    , teamId: 1
                    , steps: [
                            {
                                stepId: 0,
                                xtype: 'GTHClientSelector',
                                stepTitle: 'STEP 1: Select a client',
                                selectStatus: 'INPROGRESS'
                            }, {
                                stepId: 1,
                                xtype: 'GTHTeam1UploadFiles',
                                stepTitle: 'STEP 2: Download files',
                                selectStatus: 'INPROGRESS'
}]
                }
                , { xtype: 'TeamExecutor', teamId: 2, title: 'Team 2 <br/> Statutory Accounts - Supporting Documents', flex: 1
                    , steps: [
                            {
                                stepId: 0,
                                xtype: 'GTHClientSelector',
                                stepTitle: 'STEP 1: Select a client',
                                selectStatus: 'INPROGRESS',
                                statusList: []
                            }, {
                                stepId: 1,
                                xtype: 'GTHTeam2Process',
                                stepTitle: 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel',
                                selectStatus: 'INPROGRESS'
}]
                }
               , { xtype: 'TeamExecutor', teamId: 3, title: 'Team 3 <br/> Statutory Accounts - Reconciliation', flex: 1 }
               , { xtype: 'TeamExecutor', teamId: 4, title: 'Team 4 <br/> Financial Reporting - Financial Health Analysis', flex: 1
                    , steps: [
                    {
                        stepId: 0,
                        xtype: 'GTHClientSelector',
                        stepTitle: 'STEP 1: Select a client',
                        selectStatus: 'INPROGRESS',
                        statusList: []
                    }, {
                        stepId: 1,
                        xtype: 'GTHTeam4Process',
                        stepTitle: 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel',
                        selectStatus: 'INPROGRESS'
                    }]
            }
                , { xtype: 'TeamExecutor', teamId: 5, title: 'Team 5 <br/> Filing annual accounts', flex: 1 , steps: [
                            {
                                stepId: 0,
                                xtype: 'GTHTeam5',
                                stepTitle: 'STEP 1: Select a client',
                                selectStatus: 'INPROGRESS'
                            }]}
                , { xtype: 'TeamExecutor', teamId: 6, title: 'Team 6 <br/> Engagement Agreement', flex: 1 
                    , steps: [
                            {
                                stepId: 0,
                                xtype: 'GTHClientSelector',
                                stepTitle: 'STEP 1: Select a client',
                                selectStatus: 'INPROGRESS',
                                statusList: []
                                //searchButton:false,
                                //getAllClients: true
                            }, {
                                stepId: 1,
                                xtype: 'GTHTeam6Process',
                                stepTitle: 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel',
                                selectStatus: 'INPROGRESS'
}]
                }
            ]
            });
            
        eBook.GTH.Home.superclass.initComponent.apply(this, arguments);
    }
   , startUpdates: function() {
       if (this.rendered) this.items.each(function(it) { it.startUpdateTask(); }, this);
   }
   , stopUpdates: function() {
       if (this.rendered) this.items.each(function(it) { it.stopUpdateTask(); }, this);
   }
   , startItem: function(index) {
       var items = this.items.items;
       for (var i = 0; i < items.length; i++) {
           if (i != index) {
               items[i].el.setStyle('opacity', '0');
               items[i].el.setStyle.defer(500, items[i].el, ['display', 'none']);
           }
       }
       items[index].startWork.defer(500, items[index]);
   }
   , stopItem: function(index) {
       var items = this.items.items;
       for (var i = 0; i < items.length; i++) {
           if (i != index) {
               items[i].el.setStyle('display', 'block');
               items[i].el.setStyle('opacity', '1');

           }
       }
       //items[index].startWork.defer(500, items[index]);
   }
});

Ext.reg('gthhome', eBook.GTH.Home);

/*
eBook.Home.Charts = Ext.extend(Ext.ux.Raphael.Chart.Pie, {
    initComponent:function() {
        Ext.apply(this, {
            layout:'fit',
            store: new Ext.data.JsonStore({
                fields: ['season', 'total'],
                data: [{
                    season: 'Summer',
                    total: 150
                },
                {
                    season: 'Fall',
                    total: 245
                },
                {
                    season: 'Winter',
                    total: 117
                },
                {
                    season: 'Spring',
                    total: 184
                }]
            })
            , dataField: 'total'
            , categoryField: 'season'
            , radius: 80
            , listeners: {resize:{fn:this.onResize,scope:this}}
        });
        eBook.Home.Charts.superclass.initComponent.apply(this,arguments);
    }
    , setSize: function() { 
        
    }
    , onResize: function(adjWidth, adjHeight, rawWidth, rawHeight) {
        eBook.Home.Charts.superclass.onResize.apply(this, arguments);
        this.drawIt();
    }
    , draw: function(p) {
        this.x = this.el.getWidth() / 3;
        this.y = this.el.getHeight() / 3;
        this.radius = this.x < this.y ? this.x * 0.7 : this.y * 0.7;
        eBook.Home.Charts.superclass.draw.apply(this, arguments);
    }
});

Ext.reg('home-charts', eBook.Home.Charts);
*/


eBook.Home.Charts = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit',
            html: 'coming soon'
            , listeners: { afterlayout: { fn: this.onResize, scope: this} }
        });
        eBook.Home.Charts.superclass.initComponent.apply(this, arguments);
    }
     , onResize: function(adjWidth, adjHeight, rawWidth, rawHeight) {
         //  eBook.Home.Charts.superclass.onResize.apply(this, arguments);
         var sze = this.ownerCt.getSize();
         this.tmlEl.setSize(sze);
         if (this.paper) this.paper.clear();
         this.paper = Raphael(this.tmlEl.dom, sze.width, sze.height);
                  this.paper.image('images/chart.png', (sze.width / 2) - 64, (sze.height / 3) - 64, 128, 128);
                  this.paper.text((sze.width / 2), (sze.height / 3) + 70, 'COMING SOON');
         //this.paper.print(0,0, 'COMING SOON', this.paper.getFont('Arial')).attr({'color':'#000'});

/*
         var lwd = 24;

         var sx = lwd;
         var sy = lwd;

         var ex = lwd;
         var ey = sze.height - lwd - lwd;

         var cp1x = sze.width * 1.2; // 70%;
         var cp1y = sy - (ey * 0.2);

         var cp2x = sze.width * 1.2; // 70%;
         var cp2y = ey + (ey * 0.2);



         var wd = sze.width;
         var hg = sze.height - 24;
         var hgdf = hg / 4;
         //rdp = this.paper.path('M48,24C' + sze.width + ',48,' + sze.width + ',' + (sze.height - 48) + ',48,' + (sze.height - 48));
         //rdp = this.paper.path('M48,24,' + ((wd / 2) + ',24S') + (wd + ',' + (hgdf) + ',') + (wd + ',' + (hg - hgdf)));
         rdp = this.paper.path('M' + sx + ',' + sy
            + ' C ' + cp1x + ',' + cp1y  // control point 1
            + ',' + cp2x + ',' + cp2y // control point 2
            + ',' + ex + ',' + ey);

         rdp.attr({ "arrow-end": "block-midium-short", stroke: "#000", "stroke-width": 24, "stroke-linecap": "round" });

         var len = Raphael.getTotalLength(rdp.attr('path'));
         var cnt = 10;
         var step = len / (cnt + 2);
         var mlen = step;
         for (var i = 0; i < cnt; i++) {
             
             var p = Raphael.getPointAtLength(rdp.attr('path'), mlen);
             var c = this.paper.circle(p.x, p.y, lwd / 3);
             c.attr("fill", "#FFE600");
             c.attr("opacity", "0.6");
             c.mouseover(function() { this.animate({ "r": this.attr('r') * 2, "opacity": "1" }, 500, "<>"); });
             c.mouseout(function() { this.animate({ "r": this.attr('r') / 2, "opacity": "0.6" }, 500, "<>"); });
             mlen += (step);
         }
         */

     }
    , afterRender: function() {
        eBook.Home.Charts.superclass.afterRender.apply(this, arguments);
        this.body.update('<div class="rph-timeline"></div>');
        this.tmlEl = this.body.child('.rph-timeline');
        // this.tmlEl.update('TIMELINE');
        var sze = this.ownerCt.getSize();
        this.tmlEl.setSize(sze);
        this.tmlEl.setStyle('opacity', '0.5');
        /*
        this.paper = Raphael(this.tmlEl.dom, 200, 50);
        var rc = this.paper.rect(0, 0, 400, 50);
        rc.attr('fill', '#CCC');
        rc.attr('stroke-width', 0);

        this.selectionBox = this.paper.rect(10, 0, 180, 50);
        this.selectionBox.attr('fill', '#FFE600');
        this.selectionBox.attr('stroke-width', 0);
        this.selectionBox.attr('opacity', 0.4);
        this.selectionBox.attr('cursor', 'move');

        this.penStart = this.paper.rect(10, 0, 2, 50);
        this.penStart.attr('fill', '#FFE600');
        this.penStart.attr('stroke-width', 0);
        this.penStart.attr('cursor', 'col-resize');
        this.penStart.ox = 0;
        this.penStart.drag(this.onMove, null, this.onPenDragEnd, { pen: this.penStart, ref: 'start', owner: this }, null, { pen: this.penStart, owner: this });

        this.penEnd = this.paper.rect(190, 0, 2, 50);
        this.penEnd.attr('fill', '#FFE600');
        this.penEnd.attr('stroke-width', 0);
        this.penEnd.attr('cursor', 'col-resize');
        this.penEnd.ox = 0;
        this.penEnd.drag(this.onMove, null, this.onPenDragEnd, { pen: this.penEnd, ref: 'end', owner: this }, null, { pen: this.penEnd, owner: this });
        */
    }
    , onMove: function(dx, dy, x, y, evt) {
        this.pen.transform('...T' + (dx - this.pen.ox) + ',0');
        var wd = this.owner.selectionBox.attr('width');

        if (this.ref == 'start') {
            wd = wd - (dx - this.pen.ox);
            this.owner.selectionBox.attr('width', wd);
            this.owner.selectionBox.transform('...T' + (dx - this.pen.ox) + ',0');
        } else {
            wd = wd + (dx - this.pen.ox);
            this.owner.selectionBox.attr('width', wd)
        }

        this.pen.ox = dx;
    }
    , onPenDragEnd: function() {
        this.pen.ox = 0;
    }
});

    Ext.reg('home-charts', eBook.Home.Charts);
eBook.Home.RecentFiles = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        Ext.apply(this, {
            title: eBook.InterfaceTrans_MyLatestFiles
            ,loadMask:true
            , store: new eBook.data.JsonStore({
                selectAction: 'GetRecentFiles'
                    , criteriaParameter: 'ccdc'
                    , autoDestroy: true
                    , serviceUrl: eBook.Service.home
                    , fields: eBook.data.RecordTypes.RecentFiles
                    , autoLoad: false
            })
            , listeners: {
                'rowmousedown': {
                    fn: function(grid, rowIndex, e) {
                        var rec = grid.store.getAt(rowIndex);
                        eBook.Interface.openShortCut(rec);
                        //eBook.Interface.loadShortcut({ 'shortcut': rec, 'client': lsti[8], 'file': lsti[9] });
                    },
                    scope: this
                }
            }
            , columns: [{
                header: String.format("{0} {1}", eBook.InterfaceTrans_Client, eBook.InterfaceTrans_ClientName),
                width: 300,
                dataIndex: 'client',
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return value.n;
                }
            }, {
                header: '',
                width: 150,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    var sd = Ext.data.Types.WCFDATE.convert(value.sd);
                    var ed = Ext.data.Types.WCFDATE.convert(value.ed);
                    var ay = value.ay;
                    var str = "" + ay;
                    str += " " + sd.format('d/m/Y');
                    str += " &gt; " + ed.format('d/m/Y');

                    return str;
                }
                   , dataIndex: 'file'
            }, {
                header: eBook.InterfaceTrans_LastAccessed,
                xtype: 'datecolumn',
                format: Date.patterns.FullDateTime,
                //width: 220,
                dataIndex: 'lastaccessed'
}]
            });
            eBook.Home.RecentFiles.superclass.initComponent.apply(this, arguments);
        }
    });
    Ext.reg('ebook-home-recentfiles', eBook.Home.RecentFiles);
    eBook.Home.MyClients = Ext.extend(eBook.grid.PagedGridPanel, {

    initComponent:function() {
        Ext.apply(this, {
             title: eBook.InterfaceTrans_ClientSearch
            , storeConfig: {
                selectAction: 'GetClients',
                fields: eBook.data.RecordTypes.ClientBase,
                idField: 'id',
                criteriaParameter: 'ccdc',
                serviceUrl: eBook.Service.home
            }
            , sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
            })
            ,border:true
            //,bodyStyle:'margin-top:10px;'
            , viewConfig: { forceResize: true }
            , listeners: {
                'rowmousedown': {
                    fn: function(grid, rowIndex, e) {
                        var rec = grid.store.getAt(rowIndex);

                        eBook.Interface.openClient(rec);
                    },
                    scope: this
                }
            }
            , tbar: []
            , pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            }
            , plugins: [new Ext.ux.grid.Search({
                ref: 'searchfield',
                iconCls: 'eBook-search-ico',
                minChars: 3,
                minLength: 3,
                autoFocus: true,
                position: 'top',
                mode: 'remote',
                width: 250,
                searchText: eBook.InterfaceTrans_ClientSearchText,
                searchTipText: '',
                selectAllText: eBook.InterfaceTrans_SelectAllFields
            })]
            , loadMask: true
			, columns: [
			    {
			        header: eBook.InterfaceTrans_GFISCode,
			        width: 120,
			        dataIndex: 'GFISCode',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientName,
			        width: 300,
			        dataIndex: 'Name',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientAddress,
			        width: 160,
			        dataIndex: 'Address',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientZipCode,
			        width: 120,
			        dataIndex: 'ZipCode',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientCity,
			        width: 120,
			        dataIndex: 'City',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientCountry,
			        width: 120,
			        dataIndex: 'Country',
			        sortable: true
            }]
            , anchor: '100% 50%'
            , border: false
            , style: 'margin-top:10px'
        });
        eBook.Home.MyClients.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('ebook-home-myclients', eBook.Home.MyClients);eBook.Home.Cards = Ext.extend(Ext.Panel, {
    initComponent: function () {
        this.GTH = eBook.User.hasRoleLike('GTH');
        this.eBookUser = eBook.User.hasRoleLike('GTH', true); // roles NOT like GTH
        var its = [],
            reportItems = [
                {
                    xtype: 'button'
                    , text: "My client's engagement agreements"
                    , listeners: {
                    click: this.openEngagementAgreementView
                },
                    flex: 1,
                    iconCls: 'eBook-icon-excel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    width: 100
                },
                {
                    xtype: 'button'
                    , text: "My client's CODA proxies"
                    , listeners: {
                    click: this.openPowerOfAttorneyView
                },
                    flex: 1,
                    iconCls: 'eBook-icon-excel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    width: 100
                },
                {
                    xtype: 'button',
                    text: "My client's year end deliverable",
                    handler: this.openYearEndReport,
                    scope: this,
                    flex: 1,
                    iconCls: 'eBook-icon-excel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    width: 100
                }
                /*{
                 xtype: 'home-charts', flex: 3
                 }*/
            ];

        var currentUser = eBook.User.personId;
        var allowedUsers = ['29EA3413-4930-4B20-A22F-35B11B4864F7','6BF61892-D9A0-495E-A07B-185DC7B03F54','A2AA22FF-EAA3-42F7-8B10-26423F8F581C','18BC4A5D-A798-4D20-B097-E2F6E1A66E3E','BF723AA4-4974-4DCE-B01D-643764805FB7','AC7B9975-423F-4EB4-86E2-7EFBFA93AF47']; //Kim,Timothy,Theo,Lieve,Mieke,Frédéric
        for(var i = 0;i < allowedUsers.length; i++)
        {
            if(currentUser.toUpperCase() == allowedUsers[i].toUpperCase())
            {
                reportItems.push({
                    xtype: 'button',
                    text: "My client's NBB filing",
                    handler: this.openNBBfilingReport,
                    scope: this,
                    flex: 1,
                    iconCls: 'eBook-icon-excel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    width: 100
                });
            }
        }

        reportItems.push({
            xtype: 'spacer',
            flex: 3
        });

        if (this.eBookUser) {
            // START PAGE eBOOK USER
            its.push({
                xtype: 'panel'
                , id: 'eBook-mainPage'
                , layout: 'anchor'
                , anchorSize: { width: 800, height: 600 }
                , items: [{
                    xtype: 'panel', anchor: '100% 50%', layout: 'hbox'
                             , layoutConfig: { align: 'stretch' },
                    items: [{ xtype: 'ebook-home-recentfiles', ref: '../../lastFiles', flex: 1, anchor: '50% 100%' },
                                    { ref: '../statistics'
                                    , xtype: 'panel'
                                    , flex: 1
                                    , layout: {
                                        type: 'vbox',
                                        padding: '5',
                                        align: 'stretch'
                                    }
                                    , items: reportItems
                                    , anchor: '50% 100%'
                                    }
                             ]
                }
                          , { xtype: 'ebook-home-myclients', ref: '../clientSearch', anchor: '100% 50%', id: 'eBook-clients-grid' }
                        ]
                , border: false
                , ref: 'eBookStart'
            });

            // CLIENT OVERVIEW
            its.push({ xtype: 'clienthome', ref: 'clientMenu' });

            // FILE OVERVIEW
            its.push({ xtype: 'filehome', ref: 'fileMenu' });
        }
        if (this.GTH) {
            this.GTHIndex = its.length;
            its.push({ xtype: 'gthhome', ref: 'gthHome', listeners: { 'activate': { fn: this.onGTHActive, scope: this }, 'deactivate': { fn: this.onGTHInactive, scope: this}} });
        }

        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , layoutConfig: {
                deferredRender: true
            }
            , items: its
            , border: false
        });
        eBook.Home.Cards.superclass.initComponent.apply(this, arguments);
    }
    , onGTHActive: function () {
        this.gthHome.startUpdates();
    }
    , onGTHInactive: function () {
        this.gthHome.stopUpdates();
    }
    , showGth: function () {
        if (this.GTH) this.layout.setActiveItem(this.GTHIndex);
    }
    , showeBook: function () {
        if (this.eBookUser) {
            this.layout.setActiveItem(0);
            this.lastFiles.store.load({
                params: {
                    Person: eBook.User.getActivePersonDataContract()
                }
            });
        }
    }
    , openClient: function (record) {
        if (this.eBookUser) {
            this.layout.setActiveItem(1);
            this.clientMenu.repository.reload();
            this.clientMenu.loadRec(record);
        }
    }
    , openFile: function (record) {
        if (this.eBookUser) {
            this.layout.setActiveItem(2);
            this.fileMenu.loadRec(record);
        }
    }
    , setTax: function (o) {
        this.fileMenu.setTax(o);
    }
    , openEngagementAgreementView: function (btn) {
        var window = new eBook.Reports.EngagementAgreement.Window({});
        window.show();
    }
    , openPowerOfAttorneyView: function (btn) {
        var window = new eBook.Reports.PowerOfAttorney.Window({});
        window.show();
    }
    , openYearEndReport: function(btn) {
        var me = this,
            serviceId = 'C3B8C8DB-3822-4263-9098-541FAE897D02',
            department = 'ACR';

        me.openFileServiceView(serviceId,department);
    }
    ,
    openNBBfilingReport: function(btn) {
        var me = this,
            serviceId = '1ABF0836-7D7B-48E7-9006-F03DB97AC28B',
            department = 'ACR';

        me.openFileServiceView(serviceId,department);
    }
    ,
    openFileServiceView: function (serviceId,department) {
        var window = new eBook.Reports.FileService.Window({serviceId: serviceId, department: 'ACR'});
        window.show();
    }
});

Ext.reg('ebook-home-cards', eBook.Home.Cards);
eBook.Menu.ListViewMenu = Ext.extend(Ext.menu.Menu, {
    enableScrolling: false,
    reloadOnShow: false,
    initComponent: function() {

        /*
        if (this.strict = (Ext.isIE7 && Ext.isStrict)) {
        this.on('show', this.onShow, this, { single: true, delay: 20 });
        }*/
        Ext.apply(this, {
            plain: true,
            showSeparator: false,
            boxMinHeight: 20,
            items: this.listView = new Ext.list.ListView(Ext.applyIf({
        }, this.listConfig))
    });
    this.listView.purgeListeners();
    eBook.Menu.ListViewMenu.superclass.initComponent.call(this);
    // this.relayEvents(this.listView, ['click']);
    //this.on('show', this.picker.focus, this.picker);
    this.listView.on('click', this._onlistItemClick, this);
    if (this.handler) {
        this.on('itemclick', this.handler, this.scope || this);
    }
}
    , _onlistItemClick: function(dv, i, n, e) {
        var rec = dv.getStore().getAt(i);
        //this.onListItemClick(this, rec);
        this.fireEvent('itemclick', this, rec);
    }
    , menuHide: function() {
        if (this.hideOnClick) {
            this.hide(true);
        }
    }
    , show: function(el, pos, pmenu) {
        if (this.reloadOnShow) this.listView.store.reload();
        eBook.Menu.ListViewMenu.superclass.show.call(this, el, pos, pmenu);
    }
    , onShow: function() {
        //alert("show");
        this.listView.reload();
    }
    , reload: function() {
        if (this.listView) {
            this.listView.store.reload();
        }
    }
});
eBook.Recycle.IconContextMenu_Empty

eBook.Recycle.IconContextMenu = Ext.extend(Ext.menu.Menu, {
    listId: null
    , initComponent: function() {
        Ext.apply(this, {
            items: [{
                iconCls: 'eBook-recycle-menu-context-icon',
                text: eBook.Recycle.IconContextMenu_Empty,
                scale: 'medium',
                scope: this,
                handler: this.emptyBin
                }]
            });
            eBook.Recycle.IconContextMenu.superclass.initComponent.apply(this, arguments);
        }
    , emptyBin: function() {
        this.hide();
        var lst = Ext.getCmp(this.listId);
        var ids = [];
        lst.store.each(function(rec) {
            ids.push(rec.get('Id'));
        }, this);
        Ext.Ajax.request({
            url: eBook.Service.url + 'DeleteMarkedFiles'
            , method: 'POST'
            , params: Ext.encode({ cicdc: { Id: eBook.CurrentClient} })
            , success: this.onBatchFileDeleteSuccess
            , failure: this.onBatchFileDeleteFailure
            , scope: this
        });
        eBook.LoadOverlay.showFor(Ext.get('eBook-Client-files-deleted-icon'), [-30, -30]);

        //this.onBatchFileDeleteSuccess.defer(5000, this);
    }
    , onBatchFileDeleteSuccess: function(resp, opts, action) {
        var res = Ext.decode(resp.responseText).DeleteMarkedFilesResult;
        if (res) {
            Ext.getCmp(this.listId).store.reload();
        }

        //
        //alert("done");
        eBook.LoadOverlay.hide();
    }
    , onBatchFileDeleteFailure: function() {
        eBook.LoadOverlay.hide();
        //alert("batch deletion failed");
    }
});


eBook.List.FileIconColumn = Ext.extend(Ext.list.Column, {
    icons: {
        'xls': 'eBook-icon-xls'
        , 'xlsx': 'eBook-icon-xls'
        , 'doc': 'eBook-icon-doc'
        , 'docx': 'eBook-icon-doc'
        , 'pdf': 'eBook-icon-pdf'
    }
    , defaultIconCls: 'eBook-icon-general-16'
    , constructor: function(c) {
        c.tpl = new Ext.XTemplate('<div class="eBook-list-icon-column {' + c.dataIndex + ':this.format}">&nbsp;</div>');
        var icons = this.icons;
        var dics = this.defaultIconCls;
        c.tpl.format = function(v) {
            var ext = /[^.]+$/.exec(v);
            if (icons[ext]) return icons[ext];
            return dics;
        };
        eBook.List.FileIconColumn.superclass.constructor.call(this, c);
    }
});

Ext.reg('eBook.List.FileIconColumn', eBook.List.FileIconColumn);


eBook.List.QualityColumn = Ext.extend(Ext.list.Column, {
   constructor: function(c) {
   c.tpl = new Ext.XTemplate('<div class="eBook-list-icon-column <tpl if="Closed">eBook-lock-16</tpl><tpl if="!Closed && Errors &gt; 0">eBook-icon-16-error</tpl><tpl if="!Closed && Errors==0 && Warnings &gt; 0">eBook-icon-16-warning</tpl>">&nbsp;</div>');

        eBook.List.QualityColumn.superclass.constructor.call(this, c);
    }
});

Ext.reg('eBook.List.QualityColumn', eBook.List.QualityColumn);/*
Ext.extend(Ext.list.ListView, {
    getStore: function() {
        return this.store;
    }
});*/

eBook.List.ListView = Ext.extend(Ext.Panel, {
    initComponent:function() {
        Ext.apply(this, {
            items: [new Ext.list.ListView(this.listConfig)]
            , height: 150
            , border:false
            ,autoScroll:true
        });
        eBook.List.ListView.superclass.initComponent.apply(this,arguments);
    }
    , getStore: function() {
        return this.items.get(0).getStore();
    }
});

Ext.reg('eBookListView', eBook.List.ListView);//<div id="{id}-edit" class="eBook-icon-16 eBook-menu-title-block-edit-ico"></div>
eBook.Menu.ACTION = 'action';
eBook.Menu.GROUP = 'group';
eBook.Menu.LIST = 'list';

eBook.AlertLevel = {
    ERROR:'error',
    WARNING:'warning',
    INFORMATION:'info',
    NONE:''
}

eBook.Menu.menu = Ext.extend(Ext.BoxComponent, {
    overCls: 'eBook-menu-over'
    , menuCloseSelector: 'eBook-menu-title-block-close-ico'
    , titleToolBarItemSelector: '.eBook-menu-title-block-toolbar-item'
    , titleSelector: '.eBook-menu-title-block'
    , itemSelector: '.eBook-menu-item'
    , listSelector: '.eBook-menu-group'
    , groupSelector: '.eBook-menu-group'
    , proxySelector: '.eBook-menu-item-proxy'
    , listItemSelector: '.eBook-menu-group-item' // ? or real list objects (!= lightweight)
    , titleTemplate: new Ext.XTemplate('<tpl for="."><div id="{id}-toolbar" class="eBook-menu-title-block-toolbar"> <div id="{id}-close" class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div></div><div id="{id}-title" class="eBook-menu-title">{name}</div><div id="{id}-subtext" class="eBook-menu-subtext">{subtext}</div></tpl>', { compiled: true })
    , template: new Ext.XTemplate('<tpl for="."><div>'
            , '<div id="{id}-titleblock" class="eBook-menu-title-block {IconCls}"></div>'
                , '<div id="{id}-children" class="eBook-menu-childcontainer">'
                    , '<tpl for="items">'
                            , '<div id="{id}" class="eBook-menu-item {[values[\"align\"]!=null ? \"eBook-float-\" + values[\"align\"] : \"\"]}">'
                                , '<div id="{id}-icon" class="eBook-menu-item-icon {IconCls}{[values[\'Title\']!=\'\'? \'\' : \' eBook-notitle\']}"><div id="{id}-alerticon" class="eBook-menu-item-alert-icon"></div></div>'
                                , '<tpl if="Title!=\'\'"><div id="{id}-title" class="eBook-menu-item-title">{Title}</div></tpl>'
                            , '</div>'
                    , '</tpl>'
                    , '<div class="eBook-menu-groups">'
                    , '<tpl for="items">'
                        , '<tpl if="type==eBook.Menu.GROUP">'
                            , '<div id="{id}-group" class="eBook-menu-group" style="display:none"><div id="{id}-group-parent" class="eBook-menu-group-parent"></div><div id="{id}-group-content" class="eBook-menu-group-content">'
                                , '<tpl for="items">'
                                    , '<tpl if="type==eBook.Menu.LIST">'
                                        , '<div id="{id}" class="eBook-menu-list">'
                                        , '</div>'
                                    , '</tpl>'
                                    , '<tpl if="type==eBook.Menu.ACTION || type==eBook.Menu.GROUP">'
                                        , '<div id="{id}" class="eBook-menu-item">'
                                            , '<div id="{id}-icon" class="eBook-menu-item-icon {IconCls}"><div id="{id}-alerticon" class="eBook-menu-item-alert-icon"></div></div>'
                                            , '<div id="{id}-title" class="eBook-menu-item-title">{Title}</div>'
                                        , '</div>'
                                    , '</tpl>'
                                , '</tpl>'
                                , '<tpl for="items">'
                                    , '<tpl if="type==eBook.Menu.GROUP">'
    //, '<div class="eBook-menu-groups">'
                                            , '<div id="{id}-group" class="eBook-menu-group" style="display:none"><div id="{id}-group-parent" class="eBook-menu-group-parent"></div><div id="{id}-group-content" class="eBook-menu-group-content">'

                                                , '<tpl for="items">'
                                                    , '<tpl if="type==eBook.Menu.LIST">'
                                                        , '<div id="{id}" class="eBook-menu-list">'
                                                        , '</div>'
                                                    , '</tpl>'
                                                    , '<tpl if="type==eBook.Menu.ACTION">'
                                                        , '<div id="{id}" class="eBook-menu-item">'
                                                            , '<div id="{id}-icon" class="eBook-menu-item-icon {IconCls}"><div id="{id}-alerticon" class="eBook-menu-item-alert-icon"></div></div>'
                                                            , '<div id="{id}-title" class="eBook-menu-item-title">{Title}</div>'
                                                        , '</div>'
                                                    , '</tpl>'
                                                , '</tpl>'
                                            , '</div></div>'
    //, '</div>'
                                    , '</tpl>'
                                , '</tpl>'
                            , '</div></div>'
                        , '</tpl>'
                    , '</tpl>'
                    , '</div>'
                , '</div>'
        , '</div>'
        , '</tpl>'

        , { compiled: true })
    , lists: []
    , getData: function() { return {}; }
    , constuctor: function(config) {
        this.addEvents({
            "loaded": true,
            "closed": true
        });
        eBook.Menu.menu.superclass.constructor.call(this, config);
    }
    , initComponent: function() {
        if (!this.id) { this.id = this.getId() };
        Ext.apply(this, {
            autoEl: {
                tag: 'div'
                , cls: 'eBook-menu'
            }
        });
        this.data = this.getData();
        this.data.id = this.getId();
        eBook.Menu.menu.superclass.initComponent.apply(this, arguments);
    }
    , data: {}
    , dynamicLists: []
    , refresh: function(reloadData) {
        this.removeEvents();
        if (reloadData) this.data = this.getData();
        this.data.id = this.getId();

        this.removeLists();
        this.el.update('');
        this.afterRender();
        //this.setTitle();
    }
     , onRender: function(ct, position) {
         if (!this.el) {
             this.el = document.createElement('div');
             if (Ext.isString(this.autoEl)) {
                 this.el = document.createElement(this.autoEl);
             } else {
                 var div = document.createElement('div');
                 Ext.DomHelper.overwrite(div, this.autoEl);
                 this.el = div.firstChild;
             }
             if (!this.el.id) {
                 this.el.id = this.getId();
             }
         }
         if (this.el) {
             this.el = Ext.get(this.el);
             if (this.allowDomMove !== false) {
                 ct.dom.insertBefore(this.el.dom, position);
                 if (div) {
                     Ext.removeNode(div);
                     div = null;
                 }
             }
         }

         var mxHeight = 72;

     }
    , afterRender: function(ct) {

        this.template.overwrite(this.el, this.data, true);
        this.setEvents();
        this.renderLists();
    }
    , removeLists: function() {
        for (var i = 0; i < this.renderedLists.length; i++) {
            this.renderedLists[i].purgeListeners();
            if (this.renderedLists[i].data) this.renderedLists[i].destroy();
        }
        this.renderedLists = [];
    }
    , renderLists: function() {
        this.renderedLists = [];
        for (var i = 0; i < this.lists.length; i++) {
            var cfg = this.lists[i];
            Ext.apply(cfg, {
                renderTo: this.lists[i].listId
            });
            this.renderedLists.push(
                Ext.create(cfg, 'listview')
                );
            //this.lists[i].render();
        }
    }
    , removeEvents: function() {
        this.mun(this.el, {
            "click": this.onClick,
            "dblclick": this.onDblClick,
            "contextmenu": this.onContextMenu,
            "mouseover": this.onMouseOver,
            "mouseout": this.onMouseOut
        });

    }
    , setEvents: function() {
        this.mon(this.el, {
            "click": this.onClick,
            "dblclick": this.onDblClick,
            "contextmenu": this.onContextMenu,
            "mouseover": this.onMouseOver,
            "mouseout": this.onMouseOut,
            scope: this
        });

    }
    , clearAllAlerts: function() {
        var nds = this.el.query('.eBook-menu-item-alert-icon');
        for (var i = 0; i < nds.length; i++) {
            var nel = Ext.get(nds[i]);
            nel.removeClassLike("eBook-icon-16-");
            //nel.removeClass("eBook-icon-16-warning");
            var par = nel.up('.eBook-menu-item', 5);
            if (par) {
                Ext.get(par).setQtip('');
            }
        }
    }
    , clearMenuItemAlertLevel: function(itemEl, iconEl) {
        iconEl.removeClassLike('eBook-icon-16-');
        itemEl.setQtip("");
    }
    , setMenuItemAlertLevel: function(idOrEl, level, cnt) {
        var itemEl = Ext.get(idOrEl);
        var iconEl = Ext.get(itemEl.child(".eBook-menu-item-alert-icon"));
        if (iconEl) {
            this.clearMenuItemAlertLevel(itemEl, iconEl);
            iconEl.addClass("eBook-icon-16-" + level);
            if (Ext.isDefined(cnt)) itemEl.setQtip(cnt);
        }
    }

    , onDblClick: function(e) { }
    , onContextMenu: function(e) { }
    , onMouseOut: function(e) {
        var title = e.getTarget(this.titleSelector, this.el);
        var item = e.getTarget(this.itemSelector, this.el);
        var proxy = e.getTarget(this.proxySelector, this.el);

        if (title) {
            Ext.get(title).removeClass(this.overCls);
        } else if (item) {
            Ext.get(item).removeClass(this.overCls);
        } else if (proxy) {
            Ext.get(proxy).removeClass(this.overCls);
        }
    }
    , onMouseOver: function(e) {
        var title = e.getTarget(this.titleSelector, this.el);
        var item = e.getTarget(this.itemSelector, this.el);
        var proxy = e.getTarget(this.proxySelector, this.el);

        if (title) {
            Ext.get(title).addClass(this.overCls);
        } else if (item) {
            Ext.get(item).addClass(this.overCls);
        } else if (proxy) {
            Ext.get(proxy).addClass(this.overCls);
        }
    }
    , onClick: function(e) {
        var titleTB = e.getTarget(this.titleToolBarItemSelector, this.el);
        var title = e.getTarget(this.titleSelector, this.el);
        var item = e.getTarget(this.itemSelector, this.el);
        var proxy = e.getTarget(this.proxySelector, this.el);
        //var listItem = e.getTarget(this.

        if (titleTB) {
            if (Ext.get(titleTB).hasClass(this.menuCloseSelector)) {
                this.onClose();
            } else {
                this.onTitleToolBarItemClick(e, titleTB);
            }
        } else if (title) {
            this.onTitleClick(e, title);
        } else if (proxy) {
            this.onItemProxyClick(e, proxy);
            //this.onContainerClick(e);
        } else if (item) {

            this.onItemClick(e, item);

            //this.onItemClick(item, this.getDataItem(item), e);

        } else {
            if (this.onContainerClick) this.onContainerClick(e);
        }
    }
    , onTitleToolBarItemClick: function(e, toolbar) { }
    , onTitleClick: function(e, title) {
        this.toggleChildren(this.id);
    }
    , onItemClick: function(e, item) {
        var itemData = this.getItemData(this.data, item.id);
        if (itemData && itemData.type == eBook.Menu.GROUP) {
            this.toggleGroup(item, itemData);
        } else {
            this.onItemActionClick(item, itemData);
            //this.toggleChildren(item.id);
        }
    }
    , onItemActionClick: function(item, itemData) {
        //alert("open " + itemData.Title);
    }
    , onItemProxyClick: function(e, proxy) {
        var itemId = Ext.get(proxy).getAttributeNS('eBook', 'itemId');
        this.onItemClick(e, Ext.get(itemId));
    }
    , onListItemClick: function(e, listname, listRecord) {

    }
    , getItemData: function(cont, id) {
        for (var i = 0; i < cont.items.length; i++) {
            var it = cont.items[i];
            if (it.id == id) return it;
            if (it.items.length > 0) {
                var itdeep = this.getItemData(it, id);
                if (itdeep) return itdeep;
            }
        }
        return null;
    }
    , animate: true
    , toggleGroupBy: function(itemid) {
        var item = Ext.get(itemid);
        var itemData = this.getItemData(this.data, item.id);
        if (itemData && itemData.type == eBook.Menu.GROUP) {
            this.toggleGroup(item, itemData);
        }
    }
    , toggleGroup: function(item, itemData) {
        var item = Ext.get(item);
        var lst = Ext.get(item.id + '-group');
        if (lst) {
            var lstIcon = Ext.get(item.id + '-group-parent');
            lst.setVisibilityMode(Ext.Element.DISPLAY);
            if (!lst.isVisible()) {


                var proxy = this.getProxy(item, itemData, lst, lstIcon);
                item.orgLocation = item.getXY();
                if (!this.animate) {

                    proxy.show();
                    item.setOpacity(0.3);
                    lst.show();
                } else {
                    proxy.show();
                    item.setOpacity(0.3);
                    lst.show();
                    proxy.moveTo(lstIcon.getX(), lstIcon.getY(), {
                        duration: .5
                , callback: function() { this.clearPositioning(); }
                    });
                }

                // lst.show();


            } else {

                //if (item.orgLocation) item.moveTo(item.orgLocation[0], item.orgLocation[1], true);
                var proxy = this.getProxy(item, itemData);
                // proxy.remove();
                if (!this.animate) {
                    item.clearOpacity();
                    proxy.hide();
                    lst.hide();
                } else {
                    proxy.position('absolute');
                    proxy.moveTo(item.getX(), item.getY(), {
                        duration: .5
                , callback: function() { item.clearOpacity(); lst.hide(); this.hide(); }
                    });
                }

                //lst.hide();

            }
        }
    }
    , getProxy: function(item, itemData, lst) {
        var proxy = Ext.get(item.id + '-proxy');
        if (proxy) return proxy;
        proxy = Ext.DomHelper.createDom({
            tag: 'div'
            , cls: 'eBook-menu-item eBook-menu-item-proxy'
            , id: item.id + '-proxy'
            , itemId: item.id
            , style: 'display:none'
            , children: [{
                tag: 'div'
                    , cls: 'eBook-menu-item-icon ' + itemData.IconCls
                    , html: ''
            }, {
                tag: 'div'
                    , cls: 'eBook-menu-item-title'
                    , html: itemData.Title
            }
            ]
        });
        //Ext.getBody().appendChild(proxy);
        Ext.get(item.id + '-group-parent').appendChild(proxy);
        proxy = Ext.get(proxy);
        proxy.set({ 'eBook:itemId': item.id });
        proxy.show();
        proxy.position('absolute');
        proxy.moveTo(item.getX(), item.getY());
        return proxy;
    }

    , toggleChildren: function(id) {
        var children = Ext.get(id + '-children');
        if (children) {
            children.setVisibilityMode(Ext.Element.DISPLAY);
            if (children.isVisible()) {
                children.hide();
            } else {
                children.show();
            }
        }
    }
    , show: function() {
        if (!this.el) return;
        this.el.show();
    }
    , hide: function() {
        if (this.el) {
            this.el.hide();
        }
    }
    , load: function(record) {
        this.record = record;
        this.loadData(record);
    }
    , reload: function() {
        if (this.record) {
            this.loadData(record);
        }
    }
    , loadData: Ext.emptyFn // override
    , listLoadCount: 0
    , listLoaded: function() {
        this.listLoadCount++;
        if (this.listLoadCount == this.lists.length) this.fireEvent('loaded', this);
    }
    , setTitle: function(data) {
        if (data) this.titleData = data;
        this.titleTemplate.overwrite(Ext.get(this.id + "-titleblock"), this.titleData, true);
    }
    , syncSizes: function() {
        var menuItems = this.el.query('.eBook-menu-item');
        Ext.each(menuItems, function(it) {
            Ext.get(it).autoHeight();
        }, this);
    }
    , onClose: function() {
        eBook.Pdf.WindowMgr.closeAll();
        this.hide();
    }
    , refreshList: function(listId) {
        for (var i = 0; i < this.renderedLists.length; i++) {
            if (this.renderedLists[i].listId == listId) {
                this.renderedLists[i].getStore().load({ params: this.renderedLists[i].getMyParams(), scope: this });
                break;
            }
        }
    }
});
/*
{
            id: 'eBook-Client-manualworksheets'
                    , IconCls: 'eBook-worksheet-group-extra-menu-ico'
                    , Title: eBook.Client.PreWorksheetWindow_Title //eBook.Menu.File_ExtraFiles //eBook.Language.Menu.WorksheetsAL
                    , type: eBook.Menu.GROUP
                    , items: [{
                        id: 'eBook-Client-manualworksheets-list'
                                , listActions: []
                                , type: eBook.Menu.LIST
                                , items: []
                            }]
        },
        
        
        , {
                    id: 'eBook-Client-manualworksheets-list-id'
            , listId: 'eBook-Client-manualworksheets-list'
            , xtype: 'eBookListView'
                    //, getMyParams: function() { return { ClientId: eBook.Interface.currentFile.get('Id') }; }
            , width: 500
            , tbar: [{
                    text: eBook.Client.PreWorksheetWindow_New,
                iconCls: 'eBook-worksheets-add-ico',
                scale: 'small',
                iconAlign: 'left',
                handler: function() {
                    var wn = new eBook.Client.PreWorksheetWindow({
                        ruleApp: 'AangifteVenBManualApp'
                        , clientId: eBook.CurrentClient
                        , typeName: 'Manuele aangifte venb'
                    });
                    wn.show();
                },
                scope: this
            }]
            , listConfig: {
                store:
                 new Ext.data.JsonStore({
                     autoDestroy: true,
                     root: 'GetClientManualWorksheetsResult',
                     fields: eBook.data.RecordTypes.ManualWorksheets,
                     proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                         url: eBook.Service.url + 'GetClientManualWorksheets'
                        , criteriaParameter: 'cfdc'
                        , method: 'POST'
                     })
                 })
                , width: 450
                , multiSelect: false
                , emptyText: 'No manual worksheets'
                , reserveScrollOffset: false
                , autoHeight: true
                , listeners: {
                    'click': {
                        fn: function(lv, idx, nd, e) {
                            var rec = lv.getStore().getAt(idx);
                            var wn = new eBook.Client.PreWorksheetWindow({
                                ruleApp: 'AangifteVenBManualApp'
                                , clientId: eBook.CurrentClient
                                , typeName: 'Manuele aangifte venb'
                            });
                            wn.show(rec);

                        }
                        , scope: this
                    }
                }
                , hideHeaders: true
                , columns: [{
                    header: 'Name',
                    width: .9,
                    dataIndex: 'name'
}]
                }
                }
*/

eBook.Menu.Client = Ext.extend(eBook.Menu.menu, {
    getData: function() {
        return {
            IconCls: 'eBook-Client-menu-ico'
        , items: [{
            id: 'eBook-Client-data'
                , IconCls: 'eBook-meta-menu-ico'
                , Title: eBook.Menu.Client_ClientData//'Klantgegevens (- TT)' //eBook.Language.Menu.PermanentMeta
                , items: []
                , type: eBook.Menu.ACTION
        }, 
         {
             id: 'eBook-Client-PMT'
                    , IconCls: 'eBook-pmt-menu-ico'
                    , Title: eBook.PMT.TeamWindow_Title //'Klantgegevens (- TT)' //eBook.Language.Menu.PermanentMeta
                    , items: []
                    , type: eBook.Menu.ACTION
         }, {
             id: 'eBook-Client-suppliers'
                , IconCls: 'eBook-suppliers-menu-ico'
                , Title: eBook.Menu.Client_Suppliers
                , items: []
                , type: eBook.Menu.ACTION
         }
            , {
                id: 'eBook-Client-customers'
                , IconCls: 'eBook-customers-menu-ico'
                , Title: eBook.Menu.Client_Customers
                , items: []
                , type: eBook.Menu.ACTION
            }
            /* , {
            id: 'eBook-Client-personnel'
            , IconCls: 'eBook-personnel-menu-ico'
            , Title: eBook.Language.Menu.Personnel
            , type: eBook.Menu.ACTION
            , items: []
            }*/
            , {
                id: 'eBook-Client-files-add'
                , Title: eBook.Menu.Client_NewFile//eBook.Language.Menu.NewFile
                , IconCls: 'eBook-createfile-menu-ico'
                , type: eBook.Menu.ACTION
                , items: []
            }, {
                id: 'eBook-Client-files-openlist'
                , Title: eBook.Menu.Client_OpenFiles
                , IconCls: 'eBook-existing-open-files-menu-ico'
                , type: eBook.Menu.GROUP
                , items: [{
                    id: 'eBook-Client-files-list'
                            , listActions: []
                            , type: eBook.Menu.LIST
                            , items: []
}]
                }, {
                    id: 'eBook-Client-files-closed'
                , Title: eBook.Menu.Client_ClosedFiles
                , IconCls: 'eBook-existing-closed-files-menu-ico'
                , type: eBook.Menu.GROUP
                , items: [{
                    id: 'eBook-Client-files-closed-list'
                            , listActions: []
                            , type: eBook.Menu.LIST
                            , items: []
}]
                }
                , {
                    id: 'eBook-Client-files-deleted'
                    , Title: eBook.Menu.Client_Deleted
                    , IconCls: 'eBook-marked-deletion-files-menu-ico '
                    , type: eBook.Menu.GROUP
                    , align: 'right'
                    , items: [{
                        id: 'eBook-Client-files-deleted-list'
                            , listActions: []
                            , type: eBook.Menu.LIST
                            , items: []
}]
                    }
            ]
                };
            }
    , lists: [
       {
           id: 'eBook-Client-files-openlist-list'
            , listId: 'eBook-Client-files-list'
            , store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetFileInfosResult',
                 fields: eBook.data.RecordTypes.FileInfo,
                 baseParams: {
                     MarkedForDeletion: false
                    , Deleted: false
                    , Closed: false
                 },
                 //idField: 'Id',
                 // data: [{Id:'1','n':'Naam','sd': new Date(), 'ed':new Date()}]
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.file + 'GetFileInfos'
                     , criteriaParameter: 'cfdc'
                     , method: 'POST'
                 })
             })
            , width: 600
            , multiSelect: false
            , emptyText: eBook.Menu.Client_NoOpenFiles
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        eBook.Interface.openFile(lv.getStore().getAt(idx),true);
                        eBook.Interface.center.clientMenu.toggleGroupBy('eBook-Client-files-openlist');
                    }
                    , scope: this
                }
            }
            , columns: [new eBook.List.QualityColumn({

                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: eBook.Menu.Client_FileName,
                width: .55,
                dataIndex: 'Name'
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'StartDate'
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'EndDate'
}]
       }
        , {
            id: 'eBook-Client-files-closedlist'
            , listId: 'eBook-Client-files-closed-list'
            , store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetFileInfosResult',
                 fields: eBook.data.RecordTypes.FileInfo,
                 baseParams: {
                     MarkedForDeletion: false
                    , Deleted: false
                    , Closed: true
                 },
                 //idField: 'Id',
                 // data: [{Id:'1','n':'Naam','sd': new Date(), 'ed':new Date()}]
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.file + 'GetFileInfos'
                     , criteriaParameter: 'cfdc'
                     , method: 'POST'
                 })
             })
            , width: 500
            , multiSelect: false
            , emptyText: eBook.Menu.Client_NoClosedFiles
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        eBook.Interface.loadFile(lv.getStore().getAt(idx),true);
                        eBook.Interface.center.clientMenu.toggleGroupBy('eBook-Client-files-closed');
                    }
                    , scope: this
                }
            }
            , columns: [{
                header: eBook.Menu.Client_FileName,
                width: .6,
                dataIndex: 'Name'
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'StartDate'
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'EndDate'
}]
            }
            , {
                id: 'eBook-Client-files-deleted-list-list'
                , listId: 'eBook-Client-files-deleted-list'
                , store:
                     new Ext.data.JsonStore({
                         autoDestroy: true,
                         root: 'GetFileInfosResult',
                         fields: eBook.data.RecordTypes.FileInfo,
                         baseParams: {
                             MarkedForDeletion: true
                            , Deleted: false
                         },
                         //idField: 'Id',
                         // data: [{Id:'1','n':'Naam','sd': new Date(), 'ed':new Date()}]
                         proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                             url: eBook.Service.file + 'GetFileInfos'
                             , criteriaParameter: 'cfdc'
                             , method: 'POST'
                         })
                     })
                , width: 500
                , multiSelect: false
                , emptyText: eBook.Menu.Client_NoDeletedFiles
                , reserveScrollOffset: false
                , autoHeight: true
                , columns: [{
                    header: eBook.Menu.ClientFileName_,
                    width: .6,
                    dataIndex: 'Name'
                }, {
                    header: eBook.Menu.Client_StartDate,
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    width: .2,
                    dataIndex: 'StartDate'
                }, {
                    header: eBook.Menu.Client_EndDate,
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    width: .2,
                    dataIndex: 'EndDate'
}]
                }
                
    ]

    , loadData: function(record) {
        this.listLoadCount = 0;
        eBook.CurrentClient = record.get('Id');
        var subtext = record.get('Address') + ', ' + record.get('ZipCode') + ' ' + record.get('City');
        subtext += '<br/>PMT ROLLEN: <b>' + eBook.User.activeClientRoles + '</b>';
        if (record.get('ProAccDatabase')) {
            subtext += '<br/>ProAcc database: <b>' + record.get('ProAccDatabase') + '</b>';
            if (record.get('ProAccLinkUpdated' != null)) subtext += ' (last updated: ' + record.get('ProAccLinkUpdated').format('d/m/Y') + ')';
        }
        if (record.get('Shared')) {
            subtext += '<br/><span class="eBook-client-shared-txt">Gedeeld dossier ACR & TAX</span>';
        }
        this.setTitle({
            id: this.id
            , name: record.get('Name')
            , subtext: subtext
        });
        for (var i = 0; i < this.lists.length; i++) {
            //params
            if (this.lists[i].rendered) {
                this.lists[i].store.load({ params: {
                    ClientId: eBook.CurrentClient
                }, callback: this.listLoaded, scope: this
                });
            } else {
                if (this.lists[i].store) {
                    this.lists[i].store.load({ params: { ClientId: eBook.CurrentClient }, callback: this.listLoaded, scope: this });
                } else {
                    this.lists[i].listConfig.store.load({ params: { ClientId: eBook.CurrentClient }, callback: this.listLoaded, scope: this });
                }
            }
        }
        //this.refresh();
    }
    , updateList: function(listId) {
        for (var i = 0; i < this.lists.length; i++) {
            //params
            if (this.lists[i].listId == listId) {
                if (this.lists[i].rendered) {
                    this.lists[i].store.load({ params: {
                        ClientId: eBook.CurrentClient
                    }, scope: this
                    });
                } else {
                    if (this.lists[i].store) {
                        this.lists[i].store.load({ params: { ClientId: eBook.CurrentClient }, scope: this });
                    } else {
                        this.lists[i].listConfig.store.load({ params: { ClientId: eBook.CurrentClient }, scope: this });
                    }
                }
            }
        }
    }
    , show: function(tp) {
        eBook.Menu.Client.superclass.show.call(this);
    }
    , renderLists: function() {

        eBook.Menu.Client.superclass.renderLists.call(this);
        this.initFileDragZone(this.renderedLists[0]);
        this.mon(this.renderedLists[2].store, 'datachanged', this.fileRecycleBinChanged);
        this.initRecycleBinDropZone(Ext.get('eBook-Client-files-deleted'), this.lists[2]); //
    }
    , reloadFiles: function() {
        this.renderedLists[0].store.reload();
    }
    , reloadFilesAndOpen: function(id, importPrevious) {
        this.renderedLists[0].store.load({ params: {
            ClientId: eBook.CurrentClient
        }, callback: function() { this.openFile(id, importPrevious) }, scope: this
        });
        this.renderedLists[1].store.load({ params: {
            ClientId: eBook.CurrentClient
        }, callback: function() { this.openFile(id, importPrevious) }, scope: this
        });
    }
    , openFile: function(id, importPrevious) {
        var rec = this.renderedLists[0].store.getById(id);
        var recCl = this.renderedLists[1].store.getById(id);
        if (rec) eBook.Interface.loadFile(rec, importPrevious);
        if (recCl) eBook.Interface.loadFile(recCl, importPrevious);
    }
    , fileRecycleBinChanged: function(store) {
        var icon = Ext.get('eBook-Client-files-deleted-icon');
        if (store.getCount() > 0) {
            icon.addClass('eBook-recyclebin-full');
        } else {
            icon.removeClass('eBook-recyclebin-full');
        }
    }
    , initFileDragZone: function(v) {

        v.dragZone = new Ext.dd.DragZone(v.innerBody, {
            ddGroup: 'eBook-files',
            getDragData: function(e) {
                var sourceEl = e.getTarget(v.itemSelector, 10);
                if (sourceEl) {
                    d = sourceEl.cloneNode(true);
                    d.id = Ext.id();
                    var rec = v.getRecord(sourceEl);
                    return v.dragData = {
                        sourceEl: sourceEl,
                        repairXY: Ext.fly(sourceEl).getXY(),
                        ddel: d,
                        storeIdx: v.store.indexOf(rec),
                        fileRecord: rec
                    }
                }
            },
            getRepairXY: function() {
                return this.dragData.repairXY;
            }
        });
    }
                /*,refresh:function() {
                this.iconDropZone.destroy();
                eBook.Menu.Client.superclass.refresh.apply(this);
                }*/
    , initRecycleBinDropZone: function(iconEl, listItem) {
        //var list =Ext.getCmp()

        // init icon drop
        this.iconDropZone = new Ext.dd.DropTarget(iconEl, {
            ddGroup: 'eBook-files',
            notifyDrop: function(ddSource, e, data) {
                eBook.Interface.center.clientMenu.markFileForDeletion(data.fileRecord.get('Id'));
                var lst = Ext.getCmp(Ext.get(data.sourceEl).parent(".x-list-wrap").id);
                lst.store.removeAt(data.storeIdx);
                listItem.store.add(data.fileRecord);
                lst.store.commitChanges();
                listItem.store.commitChanges();
                listItem.store.fireEvent('datachanged', listItem.store);
                // get marked deleted store and add record.
                // get source store and remove record.
                return true;
            }
        });
    }
    , markFileForDeletion: function(id) {
        Ext.Ajax.request({
            url: eBook.Service.url + 'MarkFileForDeletion'
            , method: 'POST'
            , params: Ext.encode({ id: id })
            , success: this.onMarkFileForDeletionSuccess
            , failure: this.onMarkFileForDeletionFailure
            , scope: this
        });
    }
    , onMarkFileForDeletionSuccess: function(resp, opts) {
        //alert("marked for deletion");
    }
    , onMarkFileForDeletionFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.title);
    }
    , onContextMenu: function(e) {
        var it = e.getTarget(".eBook-marked-deletion-files-menu-ico", this.el);
        if (it) {
            if (!this.iconRecycleContext) {
                this.iconRecycleContext = new eBook.Recycle.IconContextMenu({ id: 'eBook-recycle-file-context', listId: this.lists[2].id });
            }
            e.stopEvent();
            this.iconRecycleContext.showAt(e.getXY());
        }
    }
    , onItemActionClick: function(item, itemData) {
        switch (itemData.id) {
            case 'eBook-Client-TEST':
                var wn = new eBook.Client.PreWorksheetWindow({
                    ruleApp: 'AangifteVenBManualApp'
                    , clientId: eBook.CurrentClient
                    , typeName: 'Manuele aangifte venb'
                });
                wn.show();
                break;
            case 'eBook-Client-data':
                var w = new eBook.Client.Window({ title: itemData.Title });
                w.show();
                break;
            case 'eBook-Client-PMT':
                var wn = new eBook.PMT.TeamWindow({});
                wn.show();
                break;
            case 'eBook-Client-customers':
                var wn = new eBook.BusinessRelations.Window({
                    businessRelationType: 'C'
                    , title: eBook.Menu.Client_Customers
                });
                wn.show();
                break;
            case 'eBook-Client-suppliers':
                var wn = new eBook.BusinessRelations.Window({
                    businessRelationType: 'S'
                    , title: eBook.Menu.Client_Suppliers
                });
                wn.show();
                break;
            case 'eBook-Client-files-add':
               // var wn = new eBook.Create.Window({});
                //wn.show();
                var eb = new eBook.NewFile.Window({});
                eb.show();
                break;
        }
    }
    , onClose: function() {
        eBook.CurrentClient = null;
        this.setTitle({
            id: this.id
                , name: ''
                , subtext: ''
        });
        for (var i = 0; i < this.lists.length; i++) {
            //params
            if (this.lists[i].store) this.lists[i].store.removeAll();
            if (this.lists[i].listConfig) this.lists[i].listConfig.store.removeAll();
        }
        eBook.Menu.Client.superclass.onClose.call(this);
        eBook.Interface.home();
    }
    , load: function(record) {
        if (eBook.User.isCurrentlyAdmin()) {
            // load client
            eBook.User.activeClientRoles.push('ADMINISTRATOR');
            eBook.Menu.Client.superclass.load.apply(this, arguments);
        } else if (eBook.User.isCurrentlyChampion()) {
            eBook.User.activeClientRoles.push('eBook Champion');
            eBook.Menu.Client.superclass.load.apply(this, arguments);
        } else {
            Ext.Ajax.request({
                url: eBook.Service.url + 'GetClientRoles'
                , method: 'POST'
                , params: Ext.encode({ cipdc: {
                    Id: record.get('Id'),
                    Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: function(opts, success, resp) { this.onRolesCallback(opts, success, resp, record); }
                , scope: this
            });
        }
    }
    , onRolesCallback: function(opts, success, resp, record) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            var lst = robj.GetClientRolesResult;

            if (lst != null && Ext.isArray(lst) && lst.length > 0) {
                eBook.User.activeClientRoles = lst;
                eBook.Menu.Client.superclass.load.apply(this, [record]);
            } else {
                this.onClose();
                eBook.Interface.showError('You don\'t have access to client ' + record.get('Name'), 'Client');
            }
        } else {
            this.onClose();
            eBook.Interface.showResponseError(resp, 'Client');
        }
    }
});


/*
*/
eBook.Menu.File = Ext.extend(eBook.Menu.menu, {
    getData: function() {
        return {
            IconCls: 'eBook-File-menu-ico'
        , items: [{
            id: 'eBook-File-Meta'
                , IconCls: 'eBook-file-meta-menu-ico'
                , Title: 'Metadata' // eBook.Menu.File_Meta
                , type: eBook.Menu.ACTION
                , items: []
        },{
                    id: 'eBook-File-accountmappings'
                        , Title: eBook.Menu.File_AccountsMapping //eBook.Language.Menu.AccountMeta
                        , IconCls: 'eBook-accountmappings-menu-ico'
                        , type: eBook.Menu.ACTION
                        , items: []
                }
            , {
                id: 'eBook-File-journals'
                , IconCls: 'eBook-journals-menu-ico'
                , Title: eBook.Menu.File_Journals
                , type: eBook.Menu.GROUP
                , items: [{
                    id: 'eBook-File-journal-manual'
                        , Title: eBook.Menu.File_JournalManual
                        , IconCls: 'eBook-adjustments-manual-menu-ico'
                        , type: eBook.Menu.ACTION
                        , items: []
                }, {
                    id: 'eBook-File-journal-auto'
                        , Title: eBook.Menu.File_JournalWorksheets
                        , IconCls: 'eBook-adjustments-auto-menu-ico'
                        , type: eBook.Menu.ACTION
                        , items: []
                }, {
                    id: 'eBook-File-finalTrialBalance'
                        , Title: eBook.Menu.File_FinalTrialBalance
                        , IconCls: 'eBook-final-trialbalance-menu-ico'
                        , type: eBook.Menu.ACTION
                        , items: []
}]
                }
              , {
                  id: 'eBook-File-leadsheets'
                , IconCls: 'eBook-leadsheets-menu-ico'
                , Title: 'Leadsheets'//eBook.Menu.File_LeadSheets
                , type: eBook.Menu.ACTION //eBook.Menu.GROUP
                , items: []

              }
            , {
                id: 'eBook-File-worksheets'
                , IconCls: 'eBook-worksheets-menu-ico'
                , Title: eBook.Menu.File_Worksheets
                , type: eBook.Menu.GROUP
                , items: [{
                    id: 'eBook-File-worksheets-general'
                            , IconCls: 'eBook-worksheet-group-menu-ico'
                            , Title: eBook.Menu.File_WorksheetsAL
                            , type: eBook.Menu.GROUP
                            , items: [{
                                id: 'eBook-File-worksheets-general-list'
                                        , listActions: []
                                        , type: eBook.Menu.LIST
                                        , items: []
}]
                }
                        , {
                            id: 'eBook-File-worksheets-financial'
                            , IconCls: 'eBook-worksheet-group-tax-menu-ico'
                            , Title: eBook.Menu.File_WorksheetsFI
                            , type: eBook.Menu.GROUP
                           , items: [{
                               id: 'eBook-File-worksheets-financial-list'
                                        , listActions: []
                                        , type: eBook.Menu.LIST
                                        , items: []
}]
                        }
                        ]
            }
            , {
                id: 'eBook-File-documents'
                , IconCls: 'eBook-documents-menu-ico'
                , Title: eBook.Menu.File_Documents
                , type: eBook.Menu.ACTION //eBook.Menu.GROUP
                , items: []

            }, {
                id: 'eBook-File-PDFFiles'
                , IconCls: 'eBook-pdffiles-menu-ico'
                , Title: eBook.Pdf.Window_Title
                , type: eBook.Menu.ACTION
                , items: []
            }, {
                id: 'eBook-File-ReportGeneration'
                , IconCls: 'eBook-reports-generated'
                , Title: eBook.Menu.File_Reporting
                , type: eBook.Menu.ACTION
                , items: []
            }
          /*  , {
                id: 'eBook-File-ClosingProcedure'
                , IconCls: 'eBook-closingprocedure-menu-ico'
                , Title: eBook.Closing.Window_Title
                , type: eBook.Menu.ACTION
                , items: []
            }
            , {
                id: 'eBook-File-BizTax'
                , IconCls: 'eBook-biztax-menu-ico'
                , Title: "BizTax indiening" // eBook.Closing.Window_Title
                , type: eBook.Menu.ACTION
                , items: []
            }*/
            ]
            };
        }
    , lists: [
       {
           id: 'eBook-File-worksheets-general-list-id'
            , listId: 'eBook-File-worksheets-general-list'
            , store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetWorksheetTypesResult',
                 fields: eBook.data.RecordTypes.WorksheetTypes,
                 baseParams: {
                     Type: 'AL'
                 },
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.url + 'GetWorksheetTypes'
                    , criteriaParameter: 'cwtdc'
                    , method: 'POST'
                 })
                 , listeners: {
                     'load': {
                         fn: function(str, recs, opts) {
                             eBook.Interface.center.fileMenu.setWorksheetTypeAlerts(recs, 'eBook-File-worksheets-general', 'eBook-File-worksheets');
                         }
                    , scope: this
                     }
                 }
             })
            , getMyParams: function() { return { Culture: eBook.Interface.Culture, FileId: eBook.Interface.currentFile.get('Id') }; }
            , width: 600
            , multiSelect: false
            , emptyText: ''
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        var rec = lv.getStore().getAt(idx);
                        eBook.Worksheet.show(rec.get('RuleApp'), rec.get('Name'));
                    }
                    , scope: this
                }
            }
            , hideHeaders: true
            , columns: [new eBook.List.QualityColumn({
                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: 'Name',
                width: .6,
                dataIndex: 'Name'
}]
       }
       , {
           id: 'eBook-File-worksheets-financial-list-id'
            , listId: 'eBook-File-worksheets-financial-list'
            , store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetWorksheetTypesResult',
                 fields: eBook.data.RecordTypes.WorksheetTypes,
                 baseParams: {
                     Type: 'FI'
                 },
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.url + 'GetWorksheetTypes'
                    , criteriaParameter: 'cwtdc'
                    , method: 'POST'
                 })
                 , listeners: {
                     'load': {
                         fn: function(str, recs, opts) {
                             eBook.Interface.center.fileMenu.setWorksheetTypeAlerts(recs, 'eBook-File-worksheets-financial', 'eBook-File-worksheets');
                         }
                    , scope: this
                     }
                 }
             })
            , getMyParams: function() { return { Culture: eBook.Interface.Culture, FileId: eBook.Interface.currentFile.get('Id') }; }
            , width: 600
            , multiSelect: false
            , emptyText: ''
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        var rec = lv.getStore().getAt(idx);
                        eBook.Worksheet.show(rec.get('RuleApp'), rec.get('Name'));
                    }
                    , scope: this
                }
            }
            , hideHeaders: true
            , columns: [new eBook.List.QualityColumn({
                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: 'Name',
                width: .6,
                dataIndex: 'Name'
                }]
      
       }
    ]
    , load: function(record, importPrevious) {
        if (importPrevious) this.on('loaded', this.ImportPreviousWorksheets, this, { scope: this, single: true });
        eBook.Menu.File.superclass.load.apply(this, arguments);
    }
    , ImportPreviousWorksheets: function() {
        var wn = new eBook.Worksheet.Rules.Window({});
        wn.show(eBook.Worksheet.Rules.IMPORTPREVIOUS_RULES);
    }
    , loadData: function(record) {
        if (record) {
            this.setFileState(record.get('Closed'), record.get('NumbersLocked'));
            this.listLoadCount = 0;
            eBook.CurrentFile = record.get('Id');
            var subtext = '';
            subtext = String.format('[{0} - {1}]', record.get('StartDate').format('d/m/Y'), record.get('EndDate').format('d/m/Y'));
            if (record.get('ImportDate') != null) {
                subtext = '<br/>' + String.format(eBook.Menu.File_LastImported, record.get('ImportDate'));
            }
            this.setTitle({
                id: this.id
                , name: String.format(eBook.Menu.File_FileTitle, record.get('Name'))
                , subtext: subtext
            });
            for (var i = 0; i < this.lists.length; i++) {
                //params
                if (this.lists[i].rendered) {
                    this.lists[i].getStore().load({ params: this.lists[i].getMyParams(), callback: this.listLoaded, scope: this });
                } else {
                    if (this.lists[i].store) {
                        this.lists[i].store.load({ params: this.lists[i].getMyParams(), callback: this.listLoaded, scope: this });
                    } else {
                        this.lists[i].listConfig.store.load({ params: this.lists[i].getMyParams(), callback: this.listLoaded, scope: this });
                    }
                }

            }
            //this.fireEvent('loaded', this);
        }
    }
    , show: function(tp) {

        eBook.Menu.File.superclass.show.call(this);
        
    }

    , setWorksheetTypeAlerts: function(recs, id, parentId) {
        var errs = 0;
        var warns = 0;
        for (var i = 0; i < recs.length; i++) {
            errs += recs[i].get('Errors');
            warns += recs[i].get('Warnings');
        }
        if (errs > 0) {
            this.setMenuItemAlertLevel(id, "error", errs);
            if (parentId) this.setMenuItemAlertLevel(parentId, "error");
            return;
        }
        if (warns > 0) {
            this.setMenuItemAlertLevel(id, "warning", warns);
            if (parentId) this.setMenuItemAlertLevel(parentId, "warning");
            return;
        }
    }
    , renderLists: function() {
        this.loadData();
        // this.mon(this.lists[0], 'render', this.initFileDragZone);
        //this.mon(this.lists[2].store, 'datachanged', this.fileRecycleBinChanged);
        eBook.Menu.File.superclass.renderLists.call(this);

        // this.initRecycleBinDropZone(Ext.get('eBook-Client-files-deleted'), this.lists[2]); //
    }
    , onItemActionClick: function(item, itemData) {
        var wn;
        switch (itemData.id) {
            case 'eBook-File-Meta':
                wn = new eBook.Meta.Window({});
                wn.show();
                break;
            case 'eBook-File-accountmappings':
                wn = new eBook.Accounts.Mappings.Window({});
                wn.show();
                break;
            case 'eBook-File-accounts':
                wn = new eBook.Accounts.Manage.Window({});
                wn.show();
                break;
            case 'eBook-File-Close':
                Ext.get('eBook-File-worksheets-alerticon').toggleClass('eBook-icon-16-error');
                Ext.get('eBook-File-worksheets-general-alerticon').toggleClass('eBook-icon-16-error');
                break;
            case 'eBook-File-journal-manual':
                wn = new eBook.Journal.Window({ journal: 'MANUAL' });
                wn.show();
                break;
            case 'eBook-File-journal-auto':
                wn = new eBook.Journal.Window({ journal: 'AUTOMATIC' });
                wn.show();
                break;
            case 'eBook-File-journal-start':
                wn = new eBook.Journal.Window({ journal: 'OPENING' });
                wn.show();
                break;
            case 'eBook-File-journal-lastyear':
                wn = new eBook.Journal.Window({ journal: 'LASTYEAR' });
                wn.show();
                break;
            case 'eBook-File-finalTrialBalance':
                wn = new eBook.Journal.Window({ journal: 'FINAL' });
                wn.show();
                break;
            case 'eBook-File-documents':
                wn = new eBook.Document.Window({});
                wn.show();
                break;
            case 'eBook-File-ReportGeneration':
                wn = new eBook.Bundle.Window({});
                wn.show();
                break;
            case 'eBook-File-PDFFiles':
                wn = eBook.Pdf.WindowMgr.get('et-pdf-window');
                if (wn) {
                    eBook.Pdf.WindowMgr.bringToFront(wn);
                } else {
                    wn = new eBook.Pdf.Window({});
                    wn.show({ snapRight: true });
                }
                break;
            case 'eBook-File-ClosingProcedure':
                wn = new eBook.Closing.Window({});
                wn.show();
                break;
            case 'eBook-File-BizTax':
                wn = new eBook.Xbrl.BizTaxWindow({});
                wn.show();
                break;
            case 'eBook-File-leadsheets':
                eBook.Splash.setText("Generating leadsheets");
                eBook.Splash.show();
                Ext.Ajax.request({
                    url: eBook.Service.bundle + 'GenerateLeadSheets'
                    , method: 'POST'
                    , params: Ext.encode({
                        cidc: {
                            Culture: eBook.Interface.currentFile.get('Culture')
                                , Id: eBook.Interface.currentFile.get('Id')
                        }
                    })
                    , callback: this.onLeadSheetsReady
                    , scope: this
                });
        }
    }
    , onLeadSheetsReady: function(opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open('pickup/' + robj.GenerateLeadSheetsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , setFileState: function(closed, numberslocked) {
        var el = Ext.get(this.el);
        el.removeClass("eBook-File-Closed");
        el.removeClass("eBook-File-NumbersLocked");
        if (closed) {
            el.addClass("eBook-File-Closed");
            return;
        }
        if (numberslocked) {
            el.addClass("eBook-File-NumbersLocked");
            return;
        }
    }

    , onClose: function() {
        eBook.Interface.currentFile = null;
        this.setTitle({
            id: this.id
                , name: ''
                , subtext: ''
        });

        for (var i = 0; i < this.lists.length; i++) {
            //params 
            if (this.lists[i].rendered) this.lists[i].getStore().removeAll();
        }
        eBook.Menu.File.superclass.onClose.call(this);
    }
    , refreshList: function(listId) {
        for (var i = 0; i < this.renderedLists.length; i++) {
            if (this.renderedLists[i].listId == listId) {
                this.renderedLists[i].getStore().load({ params: this.renderedLists[i].getMyParams(), scope: this });
                break;
            }
        }

    }
    });


/*{
                    id: 'eBook-File-worksheets-messages'
                            , IconCls: 'eBook-check-worksheets-menu-ico'
                            , Title: eBook.Language.Menu.MessageOverview
			    , type: eBook.Menu.ACTION
                            , items: []
                }
                        , */eBook.Interface = {
    viewPort: null,
    header: null,
    footer: null,
    center: null,
    mainPage: null,
    subPage: null,
    Culture: 'nl-BE',
    updatedPanels: [],
    counter: null,
    setCultureHighlight: function () {
        var el = Ext.get('eBook-Culture-' + this.Culture);
        el.setStyle('background-color', 'rgb(204, 204, 204)');
        el.setStyle('font-weight', 'bold');
    },
    setUpHeader: function (el, r) {
        this.setCultureHighlight(el, r);
        Ext.get('eBook-Title').update('eBook ' + eBook.Version);


    },
    showEBook: function (b, e) {
        /*if (EY && EY.Notification) {
         EY.Notification.alert("Er is (was) er eentje...", "JARIG!! Congrats Lena Lake!");
         }*/
        this.headerMenu.ebook.addClass('eBook-header-menu-btn-active');
        this.headerMenu.statistics.removeClass('eBook-header-menu-btn-active');
        this.viewPort.contentPanel.getLayout().setActiveItem(0);
    },
    showStatistics: function (b, e) {
        this.headerMenu.ebook.removeClass('eBook-header-menu-btn-active');
        this.headerMenu.statistics.addClass('eBook-header-menu-btn-active');
        this.viewPort.contentPanel.getLayout().setActiveItem(1);
    },
    onRepoStructureCallback: function (opts, success, resp) {
        if (success) {
            var o = Ext.decode(resp.responseText);
            eBook.Interface.RepoStructure = o.GetStructureTreeResult;
        } else {
            eBook.Interface.RepoStructure = null;
            eBook.Interface.showResponseError(resp, "REPOSITORY STRUCTURE");
        }
    },
    GetRepoStructureById: function (tpe, id) {
        var r = Ext.encode(eBook.Interface.RepoStructure[tpe]);
        var rs = Ext.decode(r);
        return this.findRepoId(rs, id);
    },
    GetRepoStructureByName: function (folderText) {
        //combine perm and period into a new object
        if(eBook.Interface.RepoStructure.period && eBook.Interface.RepoStructure.perm) {
            var folders = JSON.parse(JSON.stringify(eBook.Interface.RepoStructure.period.concat(eBook.Interface.RepoStructure.perm)));//Ext.util.clone();
            return this.findRepoText(folders, folderText);
        }
        return null;
    },
    repoId: function (nds, id) {
        for (var i = 0; i < nds.length; i++) {
            if (nds[i].id.toLowerCase() == id.toLowerCase()) {return nds[i];}
            if (nds[i].children) {
                var res = this.findRepoId(nds[i].children, id);
                if (res) {return res;}
            }
        }
        return null;
    },
    findRepoText: function (nds, text) {
        var me = this,
            matchedNodes = [];
        console.log(nds);
        for (var i = 0; i < nds.length; i++) {
            //console.log(nds[i].text.toLowerCase() + "==" + text);
            if (nds[i].text.toLowerCase().indexOf(text.toLowerCase()) != -1) //matches text?
            {
                var foundNode = nds[i];
                if(foundNode.text.indexOf("\t") != -1)
                {
                    foundNode.text = foundNode.text.slice(0,foundNode.text.indexOf("\t")); //remove an already added path added after a tab
                }

                if(foundNode.Path.lastIndexOf("\\") > 0) { //foundNode is child node
                    foundNode.text = foundNode.text + "\t(" + foundNode.Path.substring(0, foundNode.Path.lastIndexOf("\\")) + ")"; //add tab and path to text
                }
                matchedNodes.push(foundNode);
            }
            if (nds[i].children) { //has children?
                var foundChildNodes = me.findRepoText(nds[i].children, text);
                if (foundChildNodes.length > 0) {
                    matchedNodes = matchedNodes.concat(foundChildNodes);
                }
            }
        }

        return matchedNodes;
    },
    GetRepoStructure: function (tpe) {
        var me = this;
        if(eBook.Interface.RepoStructure == null) {
            if(me.viewPort) {
                me.viewPort.getEl().mask('Retrieving repository tree');
            }
            me.counter++;
            if (me.counter < 20) {
                me.GetRepoStructure.defer(500, me, [tpe]);
                return;
            }
        }

        me.viewPort.getEl().unmask();

        var r = Ext.encode(eBook.Interface.RepoStructure[tpe]);
        return Ext.decode(r);
    },
    GetSelectionRepoStructure: function () {
        var ret = [],
            o = eBook.Interface.GetRepoStructure('perm'),
            res = [],
            containers = [],
            periods = [];

        for (var i = 0; i < o.length; i++) {
            if (o[i].Key.indexOf('CONTAINER') == -1) {
                res.push(Ext.apply({}, o[i]));
            } else {
                containers.push(Ext.apply({}, o[i]));
            }
        }

        o = eBook.Interface.GetRepoStructure('period');
        for (i = 0; i < o.length; i++) {
            periods.push(Ext.apply({}, o[i]));
        }

        ret.push({
            nodeType: 'async',
            text: 'Client Permanent',
            draggable: false,
            id: 'Permanent',
            children: res,
            expanded: true
        });
        //        ret.push({
        //            nodeType: 'async',
        //            text: 'Existing period/ebook File',
        //            draggable: false,
        //            id: 'Existingperiods',
        //            children: containers,
        //            expanded: true
        //        });
        ret.push({
            nodeType: 'async',
            text: 'Period/file based',
            draggable: false,
            id: 'Period',
            children: periods,
            expanded: true
        });

        return ret;
    },
    render: function () {

        // LOAD REPO STRUCTURES IN BACKGROUND
        Ext.Ajax.request({
            method: 'POST',
            url: eBook.Service.repository + 'GetStructureTree',
            callback: this.onRepoStructureCallback,
            scope: this,
            params: Ext.encode({ccdc: {Culture: eBook.Interface.Culture}})
        });

        eBook.Splash.setText("Building interface...");
        this.header = new Ext.Panel({
            region: 'north',
            autoLoad: 'header.html?dc=' + eBook.SpecificVersion,
            height: 90,
            style: '',
            border: false
        });
        this.header.on('afterrender', function (pnl) {
            pnl.getUpdater().on('update', this.setUpHeader, this);
        }, this);

        this.footer = new Ext.Panel({
            region: 'south',
            autoLoad: 'footer.html?dc=' + eBook.SpecificVersion,
            border: false,
            height: 28
        });

        // construc center pane
        this.constructCenter();

        // this.statistics = new eBook.Statistics.DashBoard({});

        this.viewPort = new Ext.Viewport({
            layout: 'border',
            renderTo: Ext.getBody(),
            border: false,
            items: [
                this.header,
                new Ext.Panel({
                    id: 'contentPanel',
                    ref: 'contentPanel',
                    layout: 'card',
                    layoutConfig: {
                        deferredRender: true
                    },
                    activeItem: 0,
                    autoScroll: true,
                    region: 'center',
                    items: [this.center],
                    border: false,
                    height: '100%'
                }), this.footer
            ]
        });


        this.checkPanels.defer(100, this);
    },
    constructCenter: function () {
        this.center = new eBook.Home.Cards({});
    },
    checkPanels: function () {
        if (!this.header.getUpdater().isUpdating() && !this.footer.getUpdater().isUpdating()) {
            this.rendered = true;
            this.afterRendered.defer(500, this);
        } else {
            this.checkPanels.defer(200, this);
        }
    },
    rendered: false,
    setLogin: function () {
        Ext.get('et-local-champions').update(eBook.User.chpstr);

        Ext.get('eBook.User').update(eBook.User.fullName);
        var tmp = 'No office';
        if (eBook.User.defaultOffice !== null) {tmp = eBook.User.defaultOffice.n;}
        Ext.get('eBook.Office').update(tmp);
        // var d = eBook.Language.Roles[this.activeRole] ? eBook.Language.Roles[this.activeRole] : this.activeRole;
        //Ext.get('eBook.Role').update(d);

        eBook.Interface.updateRoles(eBook.User.getComboRoles());
        eBook.Interface.applyActiveRole(eBook.User.activeRole, '');
    },
    applyActiveRole: function (role, previousRole) {
        var p = eBook.User.getActivePersonDataContract();
        if (eBook.Interface.center.clientSearch) {
            eBook.Interface.center.clientSearch.store.baseParams.Person = p;
            if (eBook.Interface.center.clientSearch.isVisible()) {
                eBook.Interface.center.clientSearch.store.load({
                    params: {
                        Start: 0,
                        Limit: 25
                    }
                });
            }
        }

        if (role.indexOf('GTH') > -1) {
            // show GTH
            if (eBook.Interface.currentFile) {eBook.Interface.closeFile();}
            if (eBook.Interface.currentClient) {eBook.Interface.closeClient();}
            eBook.Interface.center.showGth();
        } else if (previousRole && previousRole.indexOf('GTH') > -1) {
            eBook.Interface.center.showeBook();
        } else {
            eBook.Interface.center.showeBook();
        }
        var d = eBook.Language.Roles[role] ? eBook.Language.Roles[role] : role;
        Ext.get('eBook.Role').update(d);

        //Mark role as last used
        if(previousRole) {
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.home + 'SetLastUsedRole',
                //callback: Ext.Msg.alert('file removed','File was removed from repository.'),
                scope: this,
                params: Ext.encode({cprdc: {pid: eBook.User.personId, r: role}})
            });
        }
    },
    //returns a bool verifying that the active role is part of a value in the given array
    isActiveRoleAllowed: function(arrAllowedClientRoles) {
        for(var role = 0; role < arrAllowedClientRoles.length; role++)
        {
            if(eBook.User.activeRole.indexOf(arrAllowedClientRoles[role]) != -1)
            {
                return true;
            }
        }
        return false;
    },
    afterRendered: function () {
        eBook.Interface.setLogin();
        eBook.Interface.setEYLogo();
        eBook.Splash.hide.defer(1000, eBook.Splash);
        // eBook.User.on('loggedin', this.loadClients, this);
        // eBook.Splash.setText(eBook.InterfaceTrans_LoggingIn);
        // eBook.User.login();
        //        eBook.Splash.hide();
        //        eBook.Splash.hide.defer(1000, eBook.Splash);
        //        var t = new eBook.Xbrl.BizTaxWindow({});
        //        //var t = new eBook.Bundle.ResourcesWindow({});
        //        //{ layout: 'fit', title: 'BizTax test', items: [{ xtype: 'Xbrl.Wizard'}], width: 900 });
        //        t.show();

    },
    updateRoles: function (roles) {
        if (this.roleSelector) this.roleSelector.destroy();
        if (Ext.isArray(roles) && roles.length > 1) {
            this.roleSelector = new Ext.Editor({
                cls: 'x-small-editor',
                alignment: 'bl-bl?',
                offsets: [0, 3],
                listeners: {
                    complete: function (ed, value, oldValue) {
                        if (value != '' && value != null && value != oldValue) eBook.User.setActiveRole(value);
                    }
                },
                field: {
                    width: 110,
                    triggerAction: 'all',
                    xtype: 'combo',
                    editable: false,
                    forceSelection: true,
                    store: roles
                }
            });
            this.roleSelector.mon(Ext.get('eBook.Role'), 'click', function (e, t) {
                this.roleSelector.startEdit(t);
            }, this);

        }
    },
    getDefaultWidth: function () {
        return Ext.getBody().getWidth(true) * 0.9;
    },
    getDefaultHeight: function () {
        return Ext.getBody().getHeight(true) * 0.9;
    },
    openShortCut: function (record) {
        var me = this;

        if(eBook.Interface.RepoStructure == null) {
            if(me.viewPort) {
                me.viewPort.getEl().mask('Retrieving repository tree');
            }
            me.counter++;
            if (me.counter < 20) {
                me.openShortCut.defer(500, this, [record]);
                return;
            }
        }

        me.viewPort.getEl().unmask();

        eBook.User.activeClientRoles = record.json.c.roles;
        if (eBook.User.isCurrentlyChampion() && !Ext.Array.contains(eBook.User.activeClientRoles,'Champion')) eBook.User.activeClientRoles.push('Champion');
        if (eBook.User.isCurrentlyAdmin() && !Ext.Array.contains(eBook.User.activeClientRoles,'Administrator')) eBook.User.activeClientRoles.push('Administrator');

        var creader = new Ext.data.JsonReader({
            root: 'root'
            , fields: eBook.data.RecordTypes.ClientBase
        });
        var freader = new Ext.data.JsonReader({
            root: 'root'
            , fields: eBook.data.RecordTypes.FileInfo
        });

        var crec = creader.readRecords({root: [record.get('client')]});
        var frec = freader.readRecords({root: [record.get('file')]});
        crec = crec.records[0];
        frec = frec.records[0];
        eBook.Interface.openClient(crec, frec);
    },
    openClient: function (record, fileRecord) {
        // GTH team 5 reset filter period
        Ext.getCmp('GthTeam5RepoPanel') ? Ext.getCmp('GthTeam5RepoPanel').standardFilter.pe = null : null;
        Ext.getCmp('GthTeam5RepoPanel') ? Ext.getCmp('GthTeam5RepoPanel').standardFilter.ps = null : null;

        eBook.Splash.setText(String.format(eBook.InterfaceTrans_LoadingClient, record.get('Name')));
        this.currentClient = record;
        eBook.CurrentClient = record.get('Id');
        eBook.User.activeClientRoles = record.json.roles;
        if (eBook.User.isCurrentlyChampion() && !Ext.Array.contains(eBook.User.activeClientRoles,'Champion')) eBook.User.activeClientRoles.push('Champion');
        if (eBook.User.isCurrentlyAdmin() && !Ext.Array.contains(eBook.User.activeClientRoles,'Administrator')) eBook.User.activeClientRoles.push('Administrator');
        this.center.openClient(record);


        //        eBook.Splash.show();

        //        this.currentFile = null;
        //        if (this.fileMenu.isVisible()) this.fileMenu.onClose();

        //        this.clientMenu.load(record);
        if (fileRecord) {
            eBook.Interface.openFile(fileRecord, true);
        }
    },
    isClientRole: function(clientId,role) {
        var me = this;

        Ext.Ajax.request({
            url: eBook.Service.client + 'GetClientBase',
            method: 'POST',
            jsonData: {
                cidc: {
                    'Id' : clientId
                }
            },
            callback: function (options, success, resp) {
                if (success && resp.responseText) {
                    var me = this,
                        robj = Ext.decode(resp.responseText);

                    if (robj.GetClientBaseResult.role.indexOf(role) != -1){
                        return true;
                    }
                    return false;
                }
                else {
                    Ext.Msg.alert('Could not retrieve client role.', 'Something went wrong');
                }
            },
            scope: me
        });
    },
    openFile: function (idOrRecord, tocache) {
        var id = idOrRecord;
        if (!Ext.isString(idOrRecord)) {
            id = idOrRecord.get('Id');
        }

        if (tocache) {
            eBook.Splash.setText("Preparing file...");
            eBook.Splash.show();
            Ext.Ajax.request({
                url: eBook.Service.file + 'SetFileInCache'
                , method: 'POST'
                , params: Ext.encode({cfdc: {FileId: id}})
                , callback: function () {
                    eBook.Interface.openFile(idOrRecord, false);
                }
                , scope: this
            });
        } else {
            eBook.Splash.setText("Opening file");
            eBook.Splash.show();

            if (!Ext.isString(idOrRecord)) {
                eBook.Splash.setText(String.format(eBook.InterfaceTrans_LoadingFile, idOrRecord.get('Name')));
                this.currentFile = idOrRecord;
                //eBook.CurrentFile = idOrRecord;
                this.getTax();
                var msgs = eBook.Interface.reloadHealth();
                eBook.Interface.redrawWorksheetsView();
                eBook.Interface.loadRepoLinks();
            } else {
                this.currentFile = id;

                eBook.Interface.center.clientMenu.files.openFiles.store.load({
                    params: {ClientId: this.currentClient.get('Id')}, callback: function (r, opts, succ) {
                        var str = eBook.Interface.center.clientMenu.files.openFiles.store;
                        var idx = str.find('Id', eBook.Interface.currentFile);
                        if (idx > -1) {
                            eBook.Interface.openFile(str.getAt(idx), false);
                        }
                    }
                });
            }

        }

    },
    updateFile: function (idOrRecord, tocache) {
        var id = idOrRecord;
        if (!Ext.isString(idOrRecord)) {
            id = idOrRecord.get('Id');
        }

        if (!Ext.isString(idOrRecord)) {
            this.currentFile = idOrRecord;
            this.getTax();
            var msgs = eBook.Interface.reloadHealth();
            eBook.Interface.redrawWorksheetsView();
            eBook.Interface.loadRepoLinks();
        } else {
            this.currentFile = id;

            eBook.Interface.center.clientMenu.files.openFiles.store.load({
                params: {ClientId: this.currentClient.get('Id')}, callback: function (r, opts, succ) {
                    var str = eBook.Interface.center.clientMenu.files.openFiles.store;
                    var idx = str.find('Id', eBook.Interface.currentFile);
                    if (idx > -1) {
                        eBook.Interface.updateFile(str.getAt(idx), false);
                    }
                }
            });
        }
    },
    redrawWorksheetsView: function () {
        eBook.Interface.center.fileMenu.redrawWorksheetsView();
    },
    reloadHealth: function () {
        return Ext.getCmp('eBook-file-healthmonitor').store.load({
            params: {
                FileId: this.currentFile.get('Id'),
                Culture: eBook.Interface.Culture
            }
        });
    },
    reloadHealthUpdateFields: function (form) {
        this.form = form;
        Ext.getCmp('eBook-file-healthmonitor').store.load({
            params: {
                FileId: this.currentFile.get('Id'),
                Culture: eBook.Interface.Culture
            }, callback: this.reloadHealthUpdateFieldsCallback, scope: this
        });
    },
    reloadHealthUpdateFieldsCallback: function (r, options, success) {
        if (success) {
            var fields = eBook.Interface.center.fileMenu.getFieldsByTab(this.form.xtype.split('-')[3]);
            if (fields) {
                for (var i = 0; i < fields.items.length; i++) {
                    var field = fields.items[i].data;
                    var fieldName = field.Field;
                    var item = this.form.findField(fieldName);
                    if (item) {
                        item.markInvalid(field.Text);
                    }
                }
            } else {
                this.form.clearInvalid();
            }
        }
    },
    reloadHealthUpdateTabs: function (tabs) {
        this.tabs = tabs;
        Ext.getCmp('eBook-file-healthmonitor').store.load({
            params: {
                FileId: this.currentFile.get('Id'),
                Culture: eBook.Interface.Culture
            }, callback: this.reloadHealthUpdateTabsCallback, scope: this
        });
    },
    reloadHealthUpdateTabsCallback: function (r, options, success) {
        if (success) {
            for (var i = 0; i < this.tabs.length; i++) {
                this.tabs[i].setIconClass(eBook.Interface.center.fileMenu.checkHealthTab(this.tabs[i].xtype.split('-')[3]));
                //this.tabs[i].setTitle = 'test';
            }
        }
    },
    getTax: function () {
        var fid = this.currentFile.get('Id');
        var ass = this.currentFile.get('AssessmentYear');
        Ext.Ajax.request({
            url: eBook.Service.rule(ass) + 'GetTaxCalculation',
            method: 'POST',
            params: Ext.encode({cfdc: {FileId: fid, Culture: eBook.Interface.Culture}}),
            callback: this.getTaxCallback,
            scope: this
        });
    },
    showRepoLinksToolTip: function (el, tpe, linkId, caller) {
        if (eBook.Interface.RepoLinks) {
            eBook.Interface.RepoLinks.showAt(el, tpe, linkId, caller);
        }

    },
    showRepoItem: function (id) {
        var wn = new eBook.Repository.FileDetailsWindow({repositoryItemId: id});
        wn.show();
        wn.loadById();
        eBook.Interface.RepoLinks.hideTask.cancel();
        eBook.Interface.RepoLinks.hide();
    },
    loadRepoLinks: function () {
        eBook.Interface.fileRepository = {};
        if (!eBook.Interface.RepoLinks) {
            eBook.Interface.RepoLinks = new eBook.Repository.ContextLinkList({});
        }
        eBook.Interface.RepoLinks.dv.store.load();

    },
    getTaxCallback: function (opts, success, resp) {
        this.center.openFile(this.currentFile);
        if (success) {
            try {
                var o = Ext.decode(resp.responseText);
                o = o.GetTaxCalculationResult;
                this.center.setTax(o);
            } catch (e) {
                this.center.setTax(null);
            }
        } else {
            this.center.setTax(null);
        }
        eBook.Splash.hide.defer(500, eBook.Splash);
    },
    setFileCulture: function (rec) {

        var wn = new eBook.FileCultureWindow({record: rec});
        wn.record = rec;
        wn.show();
    },
    shortcutClientLoaded: function () {
        this.loadClient(this.clientSearch.store.getAt(0));
    },
    onShortcutLoaded: function () {
        this.shortcut = null;
        eBook.Splash.hide();
    },
    home: function () {
        this.currentClient = null;
        this.currentFile = null;
        if (this.fileMenu.isVisible()) this.fileMenu.onClose();
        if (this.clientMenu.isVisible()) this.clientMenu.onClose();
        this.center.layout.setActiveItem(0);
    },
    setEYLogo: function()
    {
        var imgFolder = "images/ey/",
            imgFileName = "eyLogo-2.png";

        switch(window.location.hostname)
        {
            case "BEBE-IT05238":
            case "localhost":
                imgFileName = "eyLogo-2-dev.png";
                break;
            case "DEFRANMCETBO01":
                imgFileName = "eyLogo-2-test.png";
                break;
        }
        if(Ext.get('eyLogo')) {Ext.get('eyLogo').set({src: imgFolder + imgFileName});}
    },
    showExcelImport: function (caller) {
        var wn = new eBook.Excel.UploadWindow({});
        wn.show(caller);
    },
    showExcelMapping: function (dc, caller) {
        var wn = new eBook.Excel.Window({
            title: dc.originalFileName,
            fileName: dc.fn,
            sheets: dc.sns
        });
        wn.show(caller);
    },
    showProAccImport: function (caller) {
        if (this.currentClient != null && this.currentClient.get('ProAccServer') != null && this.currentClient.get('ProAccServer') != '') {
            var wn = new eBook.ProAcc.Window({
                clientOnly: this.currentFile == null,
                includePreviousYear: this.currentFile != null ? this.currentFile.get('PreviousStartDate') != null : false,
                width: 450,
                height: 320
            });
            wn.show(caller);
        } else {

            Ext.Msg.alert(eBook.InterfaceTrans_NoProAccTitle, eBook.InterfaceTrans_NoProAccMsg);
        }
    },
    isProAccClient: function () {
        if (this.currentClient == null) return false;
        return (this.currentClient.get('ProAccServer') != null && this.currentClient.get('ProAccServer') != '');
    },
    isExactFile: function () {
        if (this.currentFile.get('ImportType') == 'exact') {
            return true;
        } else {
            return false;
        }
    },
    EmptyField: function (dta) {
        var dtas = dta.split(";");
        for (var i = 0; i < dtas.length; i++) {

        }
    },
    showResponseError: function (response, title) {
        eBook.Splash.hide();
        var msg = "";

        try {
            var fo = Ext.decode(response.responseText);
            msg = fo.Message;
        } catch (e) {
            msg = response.responseText;
            if (Ext.isEmpty(msg)) msg = response.status + ' - ' + response.statusText;
        }
        this.showError(msg, title);

    },
    showError: function (message, title) {
        eBook.Splash.hide();
        if (title === "" || title === null) {title = "Error";}
        Ext.MessageBox.show({
            title: title,
            msg: message,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR
        });
    },
    openWindow: function (id) {
        var me = this,
            serviceId = null,
            wn = null,
            fileId = null;

        switch (id) {
            case 'clientteam':
                wn = new eBook.PMT.TeamWindow({});
                wn.show();
                break;
            case 'eBook-Client-customers':
            case 'eBook-File-customers':
                wn = new eBook.BusinessRelations.Window({
                    businessRelationType: 'C'
                    , title: eBook.Menu.Client_Customers
                });
                wn.show();
                break;
            case 'eBook-Client-suppliers':
            case 'eBook-File-suppliers':
                wn = new eBook.BusinessRelations.Window({
                    businessRelationType: 'S',
                    title: eBook.Menu.Client_Suppliers
                });
                wn.show();
                break;
            case 'eBook-Client-files-add':
                var eb = new eBook.NewFile.Window({});
                eb.show();
                break;
            case 'eBook-Client-files-deleted':
                wn = new eBook.Client.TrashcanWindow({});
                wn.show();
                break;
            case 'eBook-File-repository':
                wn = new eBook.Repository.FileWindow({});
                wn.show();
                break;
            case 'eBook-File-Meta':
                wn = new eBook.Meta.Window({});
                wn.show();
                break;
            case 'eBook-File-Schema':
                wn = new eBook.Accounts.Mappings.Window({});
                wn.show();
                break;
            case 'eBook-File-journal':
                wn = new eBook.Journal.Window({});
                wn.show();
                break;
            case 'eBook-File-finalTrialBalance':
                wn = new eBook.Journal.FTBWindow({});
                wn.show();
                break;
            case 'eBook-File-documents':
                wn = new eBook.Document.Window({});
                wn.show();
                break;
            case 'eBook-File-ReportGeneration-yearend':
                serviceId = 'C3B8C8DB-3822-4263-9098-541FAE897D02'; //yearend
                fileId = eBook.Interface.currentFile.get('Id');

                if (this.hasService(serviceId) && fileId) {
                        me.handleDeliverable(serviceId, fileId);
                } else {
                    alert("This file has no yearend service.");
                }
                break;
            case 'eBook-File-ReportGeneration-AnnualAccounts':
                serviceId = '1ABF0836-7D7B-48E7-9006-F03DB97AC28B'; //AnnualAccounts NBB Filing
                fileId = eBook.Interface.currentFile.get('Id');

                if (this.hasService(serviceId)) {
                    if(serviceId && fileId) {
                        me.handleDeliverable(serviceId, fileId);
                    }
                } else {
                    Ext.Msg.show({
                        title: 'Activate Annual Accounts Filing',
                        msg: 'This file does not yet have the annual accounts service activated.<br> Do you want to activate this service now?<br><br>This wil create an Annual Accounts deliverable and allow you<br>to start the proces towards filing.',
                        buttons: Ext.Msg.YESNO,
                        fn: me.addService,
                        scope: me,
                        icon: Ext.MessageBox.QUESTION,
                        serviceId: serviceId,
                        fileId: fileId
                    });
                    //alert("gThis file has no annual accounts service.");
                }

                break;
            case 'eBook-File-Bundles':
                wn = new eBook.Bundle.Window({});
                wn.show();
                break;
            case 'eBook-File-ReportGeneration-biztax':
                var service = null;
                if (this.hasService('BD7C2EAE-8500-4103-8984-0131E73D07FA')) {
                    service = 'BD7C2EAE-8500-4103-8984-0131E73D07FA';
                } else if (this.hasService('60CA9DF9-C549-4A61-A2AD-3FDB1705CF55')) {
                    service = '60CA9DF9-C549-4A61-A2AD-3FDB1705CF55';
                } else {
                    alert("This file has no biztax service.");
                }
                wn = new eBook.Bundle.Window({serviceId: service});
                wn.show();
                break;
            case 'eBook-File-BizTax':
                wn = new eBook.Xbrl.BizTaxWindow({});
                wn.show();
                break;
            case 'eBook-File-Close-yearend':
                eBook.Interface.verifyCloseService('C3B8C8DB-3822-4263-9098-541FAE897D02');
                break;
            case 'eBook-File-Settings':
                wn = new eBook.File.ServicesWindow({});
                wn.show();
                break;
            case 'eBook-File-quickprint':
                wn = new eBook.File.QuickprintWindow({});
                wn.show();
                break;
            case 'eBook-File-leadsheets':
                eBook.Splash.setText("Generating leadsheets");
                eBook.Splash.show();
                Ext.Ajax.request({
                    url: eBook.Service.output + 'GenerateLeadSheets'
                    , method: 'POST'
                    , params: Ext.encode({
                        cidc: {
                            Culture: eBook.Interface.currentFile.get('Culture')
                            , Id: eBook.Interface.currentFile.get('Id')
                        }
                    })
                    , callback: this.onLeadSheetsReady
                    , scope: this
                });
        }
    },
    ///Retrieve any existing deliverables (service linked bundles) or supply user with option to create one when none are found
    handleDeliverable: function(serviceId,fileId)
    {
        var me = this,
        bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');

        if(me.viewPort) {
            me.viewPort.getEl().mask('Retrieving deliverable');
        }

        if(bundleEditWindow) //making sure only one instance exists, as users tend to open this window multiple times.
        {
            bundleEditWindow.destroy();
        }

        //check existence of the service and its deliverables (=serviceBundles)
        Ext.Ajax.request({
            url: eBook.Service.bundle + 'GetServiceBundles'
            , method: 'POST'
            , jsonData: {
                cfsdc: {
                    'ServiceId': serviceId
                    , 'FileId': fileId
                    , 'Culture': eBook.Interface.Culture
                }
            }
            , callback: function (options, success, resp) {
                var me = this;
                if (success && resp.responseText) {
                    var robj = Ext.decode(resp.responseText),
                        serviceBundlesCount = robj.GetServiceBundlesResult.length;

                    if (eBook.User.activeClientRoles.indexOf('Administrator') > 0) {
                        serviceBundlesCount = 2; //Admin always sees the bundle list window with all options
                    }

                    switch (serviceBundlesCount) {
                        case 0: //open create bundle window
                            var wn = new eBook.Bundle.Window({serviceId: serviceId});
                            var nwn = new eBook.Bundle.NewWindow(
                                {
                                    serviceId: serviceId,
                                    readyCallback: serviceId ? {
                                        fn: wn.onGetFileServiceStatus,
                                        scope: wn
                                    } : null, //only retrieve fileservice if service set
                                    maskedCmp: me.viewPort
                                });
                            nwn.show();
                            break;
                        case 1: //open existing bundle in edit window
                            wn = new eBook.Bundle.Window({
                                serviceId: serviceId,
                                id: 'bundleWindow',
                                maskedCmp:  me.viewPort
                            });
                            wn.getBundle(robj.GetServiceBundlesResult[0]);
                            break;
                        default: //open bundle list window
                            wn = new eBook.Bundle.Window({serviceId: serviceId, maskedCmp:  me.viewPort});
                            wn.show();
                            break;
                    }
                }
                else {
                    Ext.Msg.alert('Could not retrieve existing deliverables', 'Something went wrong');
                }
            }
            , scope: me
        });
    },
    showRepositorySelection: function (callingBtn, show) {
        if (!eBook.Interface.RepositoryMenu) {
            eBook.Interface.RepositoryMenu = new eBook.Repository.SelectTree({renderTo: Ext.getBody()});
        }
        if (show) {
            eBook.Interface.RepositoryMenu.showMenu(callingBtn);
        } else {
            eBook.Interface.RepositoryMenu.hide();
        }
    },
    onLeadSheetsReady: function (opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open('pickup/' + robj.GenerateLeadSheetsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    },
    closeClient: function () {
        var me = this;
        if (me.currentFile) me.closeFile();
        me.currentClient = null;
        me.center.layout.setActiveItem(0);
        me.closeWindows();
    },
    closeFile: function () {
        var me = this;
        me.currentFile = null;
        me.center.fileMenu.tabs.setActiveTab(0);
        me.center.fileMenu.tabs.module.clearAll();
        me.center.layout.setActiveItem(1);
        me.closeWindows();
    },
    closeWindows: function()
    {
        //Ext.WindowMgr.each(function(cmp) {cmp.destroy();});
    },
    isFileClosed: function () {
        if (this.currentFile != null && this.currentFile.get('Closed')) {
            return true;
        } else {
            return false;
        }
    },
    hasService: function (service) {
        var svcs = eBook.Interface.currentFile.get('Services');
        if (!Ext.isArray(svcs)) return;
        for (var i = 0; i < svcs.length; i++) {
            if (svcs[i].sid.toLowerCase() == service.toLowerCase()) return true;
        }
        return false;
    },
    addService: function (btnid, txt, opts)
    {
        var me = this,
            serviceId = opts.serviceId,
            fileId = opts.fileId;

        if (serviceId && btnid == 'yes') {
            if(me.viewPort) {
                me.viewPort.getEl().mask('Retrieving deliverable');
            }
            Ext.Ajax.request({
                url: eBook.Service.file + 'AddService'
                , method: 'POST'
                , params: Ext.encode({
                    cfspdc: {
                        FileId: fileId
                        , ServiceId: serviceId
                        , Person: eBook.User.getActivePersonDataContract()
                    }
                })
                , callback: this.onServiceAdded.createDelegate(me, [serviceId], true)
                , scope: this
            });
        }
    },
    onServiceAdded: function (opts, success, resp, serviceId) {
        var me = this,
            serviceId = serviceId, //AnnualAccounts
            fileId = eBook.Interface.currentFile.get('Id');

        if(me.viewPort) {
            me.viewPort.getEl().unmask();
        }

        if (success) {
            var addedService = Ext.decode(resp.responseText).AddServiceResult;

            if(addedService)
            {
                //add service client-side
                var currentFileServices = eBook.Interface.currentFile.get('Services');
                if (!Ext.isArray(currentFileServices)) {return;}
                currentFileServices.push(addedService);

                //Open deliverable after service creation
                me.handleDeliverable(serviceId, fileId);
            }
        }
        else{
            eBook.Interface.showResponseError(resp, "Could not create service");
        }
    },
    verifyCloseService: function (serviceId) {
        if (eBook.User.isActiveClientRolesFullAccess()) {
            var wn = new eBook.Yearend.Window({});
            wn.show(eBook.Interface.serviceYearend.s);
        }

    },
    closeService: function (btnid, txt, opts) {
        if (btnid == 'yes') {
            eBook.Splash.setText("Closing service...");
            eBook.Splash.show();
            Ext.Ajax.request({
                url: eBook.Service.file + 'CloseService'
                , method: 'POST'
                , params: Ext.encode({
                    cfspdc: {
                        FileId: eBook.Interface.currentFile.get('Id')
                        , ServiceId: opts.serviceId
                        , Person: eBook.User.getActivePersonDataContract()
                    }
                })
                , callback: this.onServiceClosed
                , scope: this
            });
        }
    },
    onServiceClosed: function () {
        var clientRec = eBook.Interface.currentClient;
        var fid = eBook.Interface.currentFile.get('Id');
        this.closeClient();
        this.openClient(clientRec, fid);
    }
};

eBook.BusinessRelations.FormPanel = Ext.extend(Ext.form.FormPanel, {
    performAction: ''
    , initComponent: function() {
        Ext.apply(this, {
            labelAlign: 'top',
            bodyStyle: 'padding:5px 5px 0',
            frame: true,
            border: false,
            autoScroll: true,
            buttons: [{
                //text:'Bewaar gegevens',
                ref: '../saveButton',
                iconCls: 'eBook-window-save-ico',
                scale: 'large',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
            }, {
                //text: 'Annuleer',
                ref: '../cancelButton',
                iconCls: 'eBook-window-cancel-ico',
                scale: 'large',
                iconAlign: 'top',
                handler: this.onCancelClick,
                scope: this
}],
                items: [
                {
                    border: false,
                    layout: 'column'
                    , items: [
                        {
                            columnWidth: .5
                            , border: false
                            , layout: 'form'
                            , items: [
                                {
                                    xtype: 'hidden'
                                    , name: 'Id'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_LastName
                                    , name: 'LastName'
                                    , anchor: '95%'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_FirstName
                                    , name: 'FirstName'
                                    , anchor: '95%'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_VatNumber
                                    , name: 'VatNumber'
                                    , anchor: '95%'
                                }
                                , {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_EnterpriseNumber
                                    , name: 'EnterpriseNumber'
                                    , anchor: '95%'
                                }
                            ]
                        }, {
                            columnWidth: .5
                            , border: false
                            , layout: 'form'
                            , items: [
                                {
                                    xtype: 'hidden'
                                    , name: 'ImportedId'
                                }
                                , {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_Address
                                    , name: 'Address'
                                    , anchor: '95%'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_ZipCode
                                    , name: 'Zip'
                                    , anchor: '95%'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_City
                                    , name: 'City'
                                    , anchor: '95%'
                                }
                                , {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_Country
                                    , name: 'Country'
                                    , anchor: '95%'
                                }
                            ]
                        }
                    ]
                }
            ]
            });
            eBook.BusinessRelations.FormPanel.superclass.initComponent.apply(this, arguments);
        }
    , loadRecord: function(rec) {
        this.getForm().reset();
        this.activeRecord = rec;
        this.getForm().loadRecord(rec);
    }
    , onCancelClick: function() {
        this.activeRecord = null;
        this.refOwner.layout.setActiveItem(0);
    }
    , onSaveClick: function() {
        this.getForm().updateRecord(this.activeRecord);
        if (!this.activeRecord.dirty) return this.onCancelClick();

        this.refOwner.setStatusBusy(String.format(eBook.BusinessRelations.FormPanel_Saving, this.activeRecord.get('LastName')));
        var o = {};
        o[this.refOwner.criteriaParameter] = eBook.Util.ConstructObjectFromRecord(this.activeRecord);
        Ext.Ajax.request({
            url: eBook.Service.businessrelation + this.refOwner.updateAction
            , method: 'POST'
            , params: Ext.encode(o)
            , success: this.onSaveSuccess
            , failure: this.onSaveFailure
            , scope: this
        });

    }
    , onSaveSuccess: function(resp, opts) {
        if (this.performAction == "ADD") {
            var resp = Ext.decode(resp.responseText);
            this.activeRecord.id = resp[this.refOwner.updateAction + 'Result'].Id;
            this.activeRecord.set('Id', this.activeRecord.id);
            this.refOwner.grid.store.add(this.activeRecord);
        }
        this.refOwner.setStatus("Done");
        this.onCancelClick();
    }
    , onSaveFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.title);
        this.refOwner.setStatus("!! Failed saving changes !!");
    }
});


eBook.BusinessRelations.Window = Ext.extend(eBook.Window, {
    businessRelationType: 'C'
    , selectAction: ''
    , deleteAction: ''
    , updateAction: ''
    , criteriaParameter: ''
    , deleting: []
    , initComponent: function() {
        var action = '';
        var trans = '';
        switch (this.businessRelationType) {
            case 'C':
                action = 'Customer';
                trans = eBook.BusinessRelations.Window_Customer;
                this.criteriaParameter = 'brdc';
                break;
            case 'S':
                action = 'Supplier';
                this.criteriaParameter = 'brdc';
                trans = eBook.BusinessRelations.Window_Supplier;
                break;
            case 'P':
                action = 'Personnel';
                trans = 'Personnel';
                break;
        }
        this.translation = trans;
        this.selectAction = 'Get' + action + 'sRange';
        this.selectRoot = 'Get' + action + 'sRangeResult';
        this.deleteAction = 'DeleteBusinessRelation';
        this.updateAction = 'SaveBusinessRelationDataContract';
        this.languageSelector = action;

        Ext.apply(this, {
            layout: 'card'
            , layoutConfig: { deferredRender: false }
            , iconCls: 'eBook-Window-contacts-ico'
            , activeItem: 0
            , items: [
                new eBook.grid.PagedGridPanel({
                    id: this.id + '-grid'
                    , ref: 'grid'
                    , storeConfig: {
                        serviceUrl: eBook.Service.businessrelation,
                        selectAction: this.selectAction,
                        fields: eBook.data.RecordTypes.BusinessRelation,
                        idField: 'id',
                        criteriaParameter: 'cbidc',
                        baseParams: {
                            Id: eBook.CurrentClient
                        }
                    }
                    , tbar: new Ext.Toolbar({
                    disabled: eBook.Interface.isFileClosed(),
                        items: [{
                            xtype: 'buttongroup',
                            ref: 'editgroup',
                            id: this.id + '-toolbar-editgroup',
                            columns: 3,
                            // height:90,
                            title: String.format(eBook.BusinessRelations.Window_Manage,trans),
                            
                            items: [{
                                ref: '../addButton',
                                text: String.format(eBook.BusinessRelations.Window_New,trans),
                                iconCls: 'eBook-businessrelation-add-ico',
                                scale: 'medium',
                                iconAlign: 'top',
                                handler: this.onAddClick,
                                scope: this
                            }, {
                                ref: '../editButton',
                                text: String.format(eBook.BusinessRelations.Window_Edit,trans),
                                iconCls: 'eBook-businessrelation-edit-ico',
                                scale: 'medium',
                                iconAlign: 'top',
                                disabled: true,
                                handler: this.onEditClick,
                                scope: this
                            }, {
                                ref: '../deleteButton',
                                text: String.format(eBook.BusinessRelations.Window_Delete,trans),
                                iconCls: 'eBook-businessrelation-delete-ico',
                                scale: 'medium',
                                iconAlign: 'top',
                                disabled: true,
                                handler: this.onDeleteClick,
                                scope: this
}]
                            }, {
                                xtype: 'buttongroup',
                                ref: 'createtypes',
                                id: this.id + '-toolbar-createtypes',
                                columns: 4,
                                // height:90,
                                title: eBook.BusinessRelations.Window_Import,
                                items: [{
                                    ref: '../proaccImport',
                                    text: eBook.BusinessRelations.Window_ImportProAcc,
                                    iconCls: 'eBook-icon-importproacc-24 ',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    handler: this.onProAccImportClick,
                                    disabled: true,
                                    scope: this
                                }, {
                                    ref: '../excelImport',
                                    text: eBook.BusinessRelations.Window_ImportExcel,
                                    iconCls: 'eBook-icon-importexcel-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    handler: this.onExcelImportClick,
                                    scope: this, hidden: true
}]
}]
                                })
                    , loadMask: true
                    , pagingConfig: {
                        displayInfo: true,
                        pageSize: 25,
                        prependButtons: true
                    }
                    , sm: new Ext.grid.RowSelectionModel({
                        singleSelect: true
                        , listeners: {
                            'selectionchange': {
                                fn: this.onRowSelect
                                , scope: this
                            }
                        }
                    })
                    , columns: [{
                        header: eBook.BusinessRelations.FormPanel_LastName,
                        width: 300,
                        dataIndex: 'Name',
                        sortable: true,
                        renderer: function(v, m, r, ri, ci, s) {
                            if (('' + r.get('LastName')).length > 0) {
                                if (('' + r.get('FirstName')).length > 0) {
                                    return r.get('LastName') + ', ' + r.get('FirstName');
                                }
                                return r.get('LastName');
                            } else {
                                return r.get('FirstName');
                            }
                        }
                    },
			            {
			                header: eBook.BusinessRelations.FormPanel_Address,
			                width: 160,
			                dataIndex: 'Address',
			                sortable: true
			            },
			            {
			                header: eBook.BusinessRelations.FormPanel_ZipCode,
			                width: 120,
			                dataIndex: 'Zip',
			                sortable: true
			            },
			            {
			                header: eBook.BusinessRelations.FormPanel_City,
			                width: 120,
			                dataIndex: 'City',
			                sortable: true
			            },
			            {
			                header: eBook.BusinessRelations.FormPanel_Country,
			                width: 120,
			                dataIndex: 'Country',
			                sortable: true
			            }
			            ,
			            {
			                header: eBook.BusinessRelations.FormPanel_VatNumber,
			                width: 120,
			                dataIndex: 'VatNumber',
			                sortable: true
			            }
                    ]
                     , plugins: [
                        new Ext.ux.grid.Search({
                            iconCls: 'eBook-search-ico',
                            minChars: 2,
                            minLength: 2,
                            showMenu: false,
                            // buttonGroup: this.id + '-grid-toolbar-view',
                            autoFocus: true,
                            position: 'bottom',
                            mode: 'remote',
                            width: 210,
                            tbPosition: 11,
                            searchText: '',
                            searchTipText: '',
                            selectAllText: ''
                            //  fieldConfig: {colspan:3}
                        })]
                            })
                , new eBook.BusinessRelations.FormPanel({ ref: 'form' })
            ]
        });
        eBook.BusinessRelations.Window.superclass.initComponent.apply(this, arguments);
    }
    , show: function() {
        eBook.BusinessRelations.Window.superclass.show.apply(this, arguments);
        this.grid.store.load({ params: { Start: 0, Limit: this.grid.pagingConfig.pageSize} });
        if (eBook.Interface.currentClient.get('ProAccDatabase')) {
            this.grid.getTopToolbar().proaccImport.enable();
        }
    }
    , onRowSelect: function(sm) {
        if (this.activeRecord == sm.getSelected() || sm.getSelected() == null) {
            this.activeRecord = null;
            this.grid.getTopToolbar().editButton.disable();
            this.grid.getTopToolbar().deleteButton.disable();

        } else {
            this.activeRecord = sm.getSelected();
            this.grid.getTopToolbar().editButton.enable();
            this.grid.getTopToolbar().deleteButton.enable();
        }
        // if (sm.isSelected(idx)) return false; //this.onRowDeselect(sm, idx, r);
        // this.form.getForm().loadRecord(r);
        // this.layout.setActiveItem(1);
        //return true;
    }
    , onAddClick: function() {
        this.form.performAction = "ADD";
        this.form.loadRecord(new this.grid.store.recordType({ Id: eBook.EmptyGuid, ClientId: eBook.CurrentClient, RelationType: this.businessRelationType }));
        this.layout.setActiveItem(1);
    }
    , onEditClick: function() {
        this.form.performAction = "UPDATE";
        this.form.loadRecord(this.grid.getSelectionModel().getSelected());
        this.layout.setActiveItem(1);
    }
    , onDeleteClick: function() {
        var rec = this.grid.getSelectionModel().getSelected();
        var name = '' + rec.get('LastName');
        if (rec.get('FirstName')) {
            if (name && name.length > 0) name += ', ';
            name += rec.get('FirstName');
        }
        Ext.Msg.show({
            animEl: this.grid.getTopToolbar().deleteButton.el // if animation is on
            , buttons: Ext.MessageBox.YESNO
            , closable: false
            , fn: this.onConfirmDelete
            , scope: this
            , icon: Ext.MessageBox.WARNING
            , modal: true
            , msg: String.format(eBook.BusinessRelations.Window_DeleteMsg, name)
            , title: String.format(eBook.BusinessRelations.Window_DeleteTitle, this.translation)
        });


    }
    , onConfirmDelete: function(btn) {
        if (btn == 'yes') this.onPerformDelete();
    }
    , onPerformDelete: function() {
        var rec = this.grid.getSelectionModel().getSelected();
        var id = rec.get('Id');
        var name = '' + rec.get('LastName');
        if (rec.get('FirstName')) {
            if (name && name.length > 0) name += ', ';
            name += rec.get('FirstName');
        }
        this.grid.store.remove(rec);
        var action = {
            url: eBook.Service.businessrelation
            , text: String.format(eBook.BusinessRelations.Window_Deleting,name)
            , params: { cidc: { Id: id} }
            , action: this.deleteAction
            , record: rec
        };
        this.addAction(action);
    }
    , onProAccImportClick: function() {
        this.getEl().mask(eBook.BusinessRelations.Window_Importing, 'x-mask-loading');
        var importAction = this.businessRelationType == 'C' ? 'ImportCustomersProAcc' : 'ImportSuppliersProAcc';
        var pars = {
            cidc: {
                Id: eBook.Interface.currentClient.get('Id')
                , Culture: ''
                , Person: eBook.User.getActivePersonDataContract()
            }
        };
        Ext.Ajax.request({
                url: eBook.Service.businessrelation + importAction
                , method: 'POST'
                , params: Ext.encode(pars)
                , callback: this.onProAccImportSuccess
                , scope: this
                 
        });
    }
    , onProAccImportSuccess: function(opts, s, r) {
        if (s) {
            this.grid.store.load({ params: { Start: 0, Limit: this.grid.pagingConfig.pageSize} });
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }

});
eBook.Meta.Grid = Ext.extend(Ext.Panel, {
    propertyName: ''
    , initComponent: function() {
        this.editorPanel = Ext.apply(this.editorPanel, { ref: 'editor', gridEditor: true });
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , items: [{
                xtype: 'metagridpanel'
                       , ref: 'gridpanel'
                       , columns: this.columnsCfg
                       , fieldsCfg: this.fieldsCfg
            }, this.editorPanel]
        });
        eBook.Meta.Grid.superclass.initComponent.apply(this, arguments);
    }
    , onEditItem: function(idx) {
        var rec = this.gridpanel.store.getAt(idx);
        this.editor.loadLine(rec);
        this.getLayout().setActiveItem(1);
    }
    , onAddRow: function() {
        this.editor.clearAll();
        this.getLayout().setActiveItem(1);
    }
    , onSaveItem: function() {
        this.getLayout().setActiveItem(0);
        this.editor.clearAll();
    }
    , loadData: function(parentObj) {
        this.gridpanel.store.loadData(parentObj[this.propertyName]);
    }
    , getData: function(parentObj) {
        var arr = [];
        var g = this;
        this.gridpanel.store.each(function(rec) {
            var o = rec.data;
            arr.push(o);
        });
        return arr;
    }
});
Ext.reg('metagrid', eBook.Meta.Grid);

eBook.Meta.GridPanel = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        Ext.apply(this, {
            tbar: new Ext.Toolbar({
                items: [
                                {
                                    ref: 'additem',
                                    text: eBook.Meta.GridPanel_AddItem,
                                    iconCls: 'eBook-icon-add-16',
                                    scale: 'small',
                                    iconAlign: 'left',
                                    handler: this.onAddRow,
                                    disabled: eBook.Interface.isFileClosed(),
                                    scope: this
                                }, {
                                    ref: 'edititem',
                                    text: eBook.Meta.GridPanel_EditItem,
                                    iconCls: 'eBook-icon-edit-16',
                                    scale: 'small',
                                    iconAlign: 'left',
                                    disabled: true,
                                    handler: this.onEditRow,
                                    
                                    scope: this
                                }, {
                                    ref: 'deleteitem',
                                    text:  eBook.Meta.GridPanel_DeleteItem,
                                    iconCls: 'eBook-icon-delete-16',
                                    scale: 'small',
                                    iconAlign: 'left',
                                    disabled: true,
                                    handler: this.onDeleteRow,
                                    scope: this
                                }
                            ]
            })
            , store: { xtype:'jsonstore',fields: this.fieldsCfg }
            , sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
                        , listeners: {
                            'selectionchange': {
                                fn: this.onRowSelect
                                    , scope: this
                            }
                        }
            })
        });

        eBook.Meta.GridPanel.superclass.initComponent.apply(this, arguments);
        this.on('rowdblclick', this.onEditRow);
    }
    , onRowSelect: function(sm) {
        /* TOOLBAR UPDATE */
        if (this.getTopToolbar()) {

            var ed = this.getTopToolbar().edititem;
            var dl = this.getTopToolbar().deleteitem;
            if (!sm.hasSelection()) {
                dl.disable();
                ed.disable();
            } else {
                dl.enable();
                ed.enable();
                if (!Ext.isEmpty(this.rowDisabledField)) {
                    var r = sm.getSelected();
                    if (r.get(this.rowDisabledField)) {
                        dl.disable();
                        ed.disable();
                        sm.clearSelections();
                    }
                }

            }
        }
    }
    , onEditRow: function(grid, ridx, e) {
        this.refOwner.onEditItem(ridx);
    }
    , onAddRow: function() {
        this.refOwner.onAddRow(); //bubble
    }
    , onDeleteRow: function() {
        var r = this.getSelectionModel().getSelected();
        this.store.remove(r);
        this.getSelectionModel().clearSelections(true);
    }
});

Ext.reg('metagridpanel', eBook.Meta.GridPanel);
eBook.Meta.PersonEditor = Ext.extend(Ext.Panel, {
    classType: 'MetaPersonDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName: 'Person'
    , gridEditor: false
    , initComponent: function() {
        Ext.apply(this, {
            border: false,
            layout: 'form',
            labelWidth: 200,
            items: [
                {
                    xtype: 'label',
                    text: this.fieldText
                    , disabled: eBook.Interface.isFileClosed()
                },
                { //eBook.Meta.PersonEditor_Male eBook.Meta.PersonEditor_Female
                    xtype: 'combo'
                    , ref: 'gr'
                    , name: 'gr'
                    , value: 'M'
                    , lazyRender: true
                    , mode: 'local'
                    , disabled: eBook.Interface.isFileClosed()
                    , store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: [
                            'id',
                            'display'
                        ],
                        data: [['M', eBook.Meta.PersonEditor_Male ], ['F', eBook.Meta.PersonEditor_Female]]
                    })
                    , valueField: 'id'
                    , displayField: 'display'
                    , typeAhead: false
                    , triggerAction: 'all'
                    , fieldLabel:eBook.Meta.PersonEditor_Gender // eBook.Meta.PersonEditor_Gender
                }, {

                xtype: 'textfield'
                    , disabled: eBook.Interface.isFileClosed()
                    , width: 250
                    , ref: 'nme'
                    , name: 'nme'
                    , value: ''
                    , fieldLabel:eBook.Meta.PersonEditor_Name // eBook.Meta.PersonEditor_Name
}]
        });

        if (this.gridEditor) {
            Ext.apply(this, {
                buttons: [{
                    //text:'Bewaar gegevens',
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onSaveClick,
                    scope: this
                }, {
                    //text: 'Annuleer',
                    ref: '../cancelButton',
                    iconCls: 'eBook-window-cancel-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onCancelClick,
                    scope: this
                }
                ]
            });
        }
        eBook.Meta.PersonEditor.superclass.initComponent.apply(this, arguments);
    }
    , onSaveClick: function() {
        this.saveLine();
        return;
    }
    , onCancelClick: function() {
        if (!this.refOwner) return;
        if (this.refOwner.onSaveItem) {
            this.refOwner.onSaveItem();
        }
        return;
    }
    , loadLine: function(rec) {
        this.activeRecord = rec;
        this.loadObject(rec.data);
    }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        this.loadObject(mobj);
    }
    , loadObject: function(obj) {
        if (obj) {
            this.gr.setValue(obj.gr);
            this.nme.setValue(obj.nme);
        }
    }
    , getData: function() {
        return {
            gr: this.gr.getValue()
            , nme: this.nme.getValue()
        };
    }
    , saveLine: function() {
        if (!this.refOwner) return;
        
        if (this.activeRecord) {
            this.activeRecord.data = this.getData();
            this.activeRecord.markDirty();
            this.activeRecord.commit();
            this.refOwner.onSaveItem();
        } else {
            if (!this.refOwner) return;
            if (this.refOwner.gridpanel) {
                var rect = this.refOwner.gridpanel.store.recordType;
                var rec = new rect(this.getData());
                this.refOwner.gridpanel.store.add(rec);
                this.refOwner.onSaveItem();
            }
        }
    }
    , reset: function() {
        this.clearAll();
    }
    , clearAll: function() {
        this.gr.reset();
        this.nme.reset();
        this.activeRecord = null;
    }
});

Ext.reg('metapersoneditor', eBook.Meta.PersonEditor);

eBook.Meta.MeetingEditor = Ext.extend(Ext.form.FormPanel, {
    classType: 'MeetingDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName: 'Meeting'
    , initComponent: function() {
        Ext.apply(this, {
            labelWidth: 200
            , labelPad: 10
            , border: 0
            , bodyStyle: 'padding:10px;'
            
            , items: [{
                xtype: 'datefield'
                    , ref: 'dte'
                    , name: 'dte'
                    , value: ''
                    , format: 'd/m/Y'
                    , fieldLabel: eBook.Meta.MeetingEditor_Date // eBook.Meta.MeetingEditor_Date
                    , disabled: eBook.Interface.isFileClosed()
            }, {
                xtype: 'timefield'
                    , ref: 'time'
                    , name: 'time'
                    , value: ''
                    , format: 'H:i'
                    , fieldLabel: eBook.Meta.MeetingEditor_Time // eBook.Meta.MeetingEditor_Time
                    , disabled: eBook.Interface.isFileClosed()
            }, {
                xtype: 'metapersoneditor'
                    , ref: 'chm'
                    , fieldText: eBook.Meta.MeetingEditor_Chairman// eBook.Meta.MeetingEditor_Chairman
                    , propertyName: 'chm'
                    //, disabled: eBook.Interface.currentFile != null && eBook.Interface.currentFile.get('Closed')
            }, { xtype: 'fieldset'
                     , ref: 'del'
                     , checkboxToggle: true
                     , checkboxName: 'del'
                     , collapsed: false
                     , title: eBook.Meta.MeetingEditor_Delayed //eBook.Meta.MeetingEditor_Delayed
                     , autoHeight: true                     
                     , items: [{
                         xtype: 'datefield'
                                    , name: 'deldte'
                                    , ref: '../deldte'
                                    , value: ''
                                    , format: 'd/m/Y'
                                    , fieldLabel: eBook.Meta.MeetingEditor_Date// eBook.Meta.MeetingEditor_Date
                                    , disabled: eBook.Interface.isFileClosed()
                     }, {
                         xtype: 'timefield'
                                    , ref: '../deltime'
                                    , name: 'deltime'
                                    , value: ''
                                    , format: 'H:i'
                                    , fieldLabel: eBook.Meta.MeetingEditor_Time// eBook.Meta.MeetingEditor_Time
                                    , disabled: eBook.Interface.isFileClosed()
}]
}]
            });

            eBook.Meta.MeetingEditor.superclass.initComponent.apply(this, arguments);
        }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        if (mobj) {
            if (mobj.dte) this.dte.setValue(Ext.data.Types.WCFDATE.convert(mobj.dte));
            this.time.setValue(mobj.time);
            this.chm.loadData(mobj);
            if (this.del.checkbox) {
                this.del.checkbox.dom.checked = mobj.del;
            } else {
                this.del.collapsed = !mobj.del;
            }
            if (mobj.deldte) this.deldte.setValue(Ext.data.Types.WCFDATE.convert(mobj.deldte));
            this.deltime.setValue(mobj.deltime);
        }
    }
    , getData: function() {
        return {
            dte: !Ext.isEmpty(this.dte.getValue()) ? this.dte.getValue() : null
            , time: this.time.getValue()
            , chm: this.chm.getData()
            , del: this.del.checkbox ? this.del.checkbox.dom.checked : false
            , deldte: !Ext.isEmpty(this.deldte.getValue()) ? this.deldte.getValue() : null
            , deltime: this.deltime.getValue()
        };
    }
    });

Ext.reg('metameetingeditor', eBook.Meta.MeetingEditor);

eBook.Meta.ShareHolderEditor = Ext.extend(Ext.form.FormPanel, {
    classType: 'ShareHolderDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName: ''
    , initComponent: function() {
        Ext.apply(this, {
            labelWidth: 200
            , labelPad: 10
            , border: 0
            , bodyStyle: 'padding:10px;'
            , items: [{  
                xtype: 'combo'
                    , ref: 'gr'
                    , name: 'gr'
                    , value: 'M'
                    , lazyRender: true
                    , mode: 'local'
                    , store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: [
                            'id',
                            'display'
                        ],
                        data: [['M', eBook.Meta.PersonEditor_Male], ['F', eBook.Meta.PersonEditor_Female]]
                    })
                    , valueField: 'id'
                    , displayField: 'display'
                    , typeAhead: false
                    , triggerAction: 'all'
                    , fieldLabel: eBook.Meta.PersonEditor_Gender // eBook.Meta.PersonEditor_Gender
            }, {
                xtype: 'textfield'
                    , width: 250
                    , ref: 'nme'
                    , name: 'nme'
                    , value: ''
                    , fieldLabel: eBook.Meta.PersonEditor_Name// eBook.Meta.PersonEditor_Name
            }, {
                xtype: 'numberfield'
                    , width: 250
                    , ref: 'shrs'
                    , name: 'shrs'
                    , allowDecimals: false
                    , value: 0
                    , fieldLabel: eBook.Meta.ShareHolderEditor_Shares
            }, {
                xtype: 'metapersoneditor'
                    , ref: 'repr'
                    , fieldLabel: eBook.Meta.ShareHolderEditor_RepresentedBy
                    , propertyName: 'repr'
}]
            });

            if (this.gridEditor) {
                Ext.apply(this, {
                    buttons: [{
                        //text:'Bewaar gegevens',
                        ref: '../saveButton',
                        iconCls: 'eBook-window-save-ico',
                        scale: 'large',
                        iconAlign: 'top',
                        handler: this.onSaveClick,
                        scope: this
                    }, {
                        //text: 'Annuleer',
                        ref: '../cancelButton',
                        iconCls: 'eBook-window-cancel-ico',
                        scale: 'large',
                        iconAlign: 'top',
                        handler: this.onCancelClick,
                        scope: this
                    }
                ]
                });
            }
            eBook.Meta.ShareHolderEditor.superclass.initComponent.apply(this, arguments);
        }
   , onSaveClick: function() {
       this.saveLine();
       return;
   }
    , onCancelClick: function() {
        if (!this.refOwner) return;
        if (this.refOwner.onSaveItem) {
            this.refOwner.onSaveItem();
        }
        return;
    }
    , loadLine: function(rec) {
        this.activeRecord = rec;
        this.loadObject(rec.data);
    }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        if (mobj) {
            this.loadObject(mobj);
        }

    }
    , loadObject: function(obj) {
        if (obj) {
            //his.itemid.setValue(obj.id);
            this.gr.setValue(obj.gr);
            this.nme.setValue(obj.nme);
            this.shrs.setValue(obj.shrs);
            this.repr.loadData(obj);
        }
    }
    , getData: function() {
        return {
            gr: this.gr.getValue()
            , nme: this.nme.getValue()
            , shrs: this.shrs.getValue()
            , repr: this.repr.getData()
        };
    }
    , getDataUn: function() {
        var repr = this.repr.getData();
        return {
            gr: this.gr.getValue()
            , nme: this.nme.getValue()
            , shrs: this.shrs.getValue()
            , repr_nme: repr.nme
            , repr_gr: repr.gr
            , repr: repr
        };
    }
    , saveLine: function() {
        if (!this.refOwner) return;

        if (this.activeRecord) {
            this.activeRecord.data = this.getData();
            this.activeRecord.markDirty();
            this.activeRecord.commit();
            this.refOwner.onSaveItem();
        } else {
            if (!this.refOwner) return;
            if (this.refOwner.gridpanel) {
                var rect = this.refOwner.gridpanel.store.recordType;
                var rec = new rect(this.getDataUn());
                rec.json = this.getData();
                this.refOwner.gridpanel.store.add(rec);
                this.refOwner.gridpanel.store.commitChanges();
                this.refOwner.onSaveItem();
            }
        }
    }
    , reset: function() {
        this.clearAll();
    }
    , clearAll: function() {
        // this.itemid.reset();
        this.gr.reset();
        this.nme.reset();
        this.shrs.reset();
        this.repr.reset();
        this.activeRecord = null;
    }
    });

Ext.reg('shareholdereditor', eBook.Meta.ShareHolderEditor);


eBook.Meta.PeerGroupEditor = Ext.extend(Ext.form.FormPanel, {
classType: 'PeerGroupItemDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName: ''
    , initComponent: function() {
        Ext.apply(this, {
            labelWidth: 200
            , labelPad: 10
            , border: 0
            , bodyStyle: 'padding:10px;'
            , items: [ {
                xtype: 'textfield'
                    , width: 250
                    , ref: 'ond'
                    , name: 'ond'
                    , value: ''
                    ,allowBlank:false
                    , fieldLabel: eBook.BusinessRelations.FormPanel_EnterpriseNumber// eBook.Meta.PersonEditor_Name
            }, {
                xtype: 'textfield'
                    , width: 250
                    , ref: 'nme'
                    , name: 'nme'
                    , value: ''
                    , allowBlank: false
                    , fieldLabel: eBook.Meta.PersonEditor_Name// eBook.Meta.PersonEditor_Name
                }]
            });

            if (this.gridEditor) {
                Ext.apply(this, {
                    buttons: [{
                        //text:'Bewaar gegevens',
                        ref: '../saveButton',
                        iconCls: 'eBook-window-save-ico',
                        scale: 'large',
                        iconAlign: 'top',
                        handler: this.onSaveClick,
                        scope: this
                    }, {
                        //text: 'Annuleer',
                        ref: '../cancelButton',
                        iconCls: 'eBook-window-cancel-ico',
                        scale: 'large',
                        iconAlign: 'top',
                        handler: this.onCancelClick,
                        scope: this
                    }
                ]
                });
            }
            eBook.Meta.PeerGroupEditor.superclass.initComponent.apply(this, arguments);
        }
   , onSaveClick: function() {
       this.saveLine();
       return;
   }
    , onCancelClick: function() {
        if (!this.refOwner) return;
        if (this.refOwner.onSaveItem) {
            this.refOwner.onSaveItem();
        }
        return;
    }
    , loadLine: function(rec) {
        this.activeRecord = rec;
        this.loadObject(rec.data);
    }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        if (mobj) {
            this.loadObject(mobj);
        }

    }
    , loadObject: function(obj) {
        if (obj) {
            //his.itemid.setValue(obj.id);
            this.ond.setValue(obj.ond);
            this.nme.setValue(obj.nme);
        }
    }
    , getData: function() {
        return {
            ond: this.ond.getValue()
            , nme: this.nme.getValue()
        };
    }
    , getDataUn: function() {
        
        return {
            ond: this.ond.getValue()
            , nme: this.nme.getValue()
        };
    }
    , saveLine: function() {
        if (!this.refOwner) return;

        if (this.activeRecord) {
            this.activeRecord.data = this.getData();
            this.activeRecord.markDirty();
            this.activeRecord.commit();
            this.refOwner.onSaveItem();
        } else {
            if (!this.refOwner) return;
            if (this.refOwner.gridpanel) {
                var rect = this.refOwner.gridpanel.store.recordType;
                var rec = new rect(this.getDataUn());
                rec.json = this.getData();
                this.refOwner.gridpanel.store.add(rec);
                this.refOwner.gridpanel.store.commitChanges();
                this.refOwner.onSaveItem();
            }
        }
    }
    , reset: function() {
        this.clearAll();
    }
    , clearAll: function() {
        // this.itemid.reset();
        this.ond.reset();
        this.nme.reset();
        this.activeRecord = null;
    }
    });

    Ext.reg('peergroupeditor', eBook.Meta.PeerGroupEditor);

eBook.Meta.ShareholdersMeetingEditor = Ext.extend(Ext.form.FormPanel, {
    classType: 'ShareHoldersMeetingDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName:'Meeting'
    , initComponent: function() {
        Ext.apply(this, {
            labelWidth: 200
            , labelPad: 10
            , border: 0
            , bodyStyle: 'padding:10px;'
            ,items:[{
                    xtype:'datefield'
                    , ref:'dte'
                    , name:'dte'
                    , value:''
                    , format:'d/m/Y'
                    , fieldLabel: eBook.Meta.MeetingEditor_Date
                    , disabled: eBook.Interface.isFileClosed()
                   },{
                    xtype:'timefield'
                    , ref: 'time'
                    , name:'time'
                    , value:''
                    , format:'H:i'
                    , fieldLabel: eBook.Meta.MeetingEditor_Time
                    , disabled: eBook.Interface.isFileClosed()
                   }, {
                    xtype:'metapersoneditor'
                    , ref: 'chm'
                    , fieldLabel: eBook.Meta.MeetingEditor_Chairman // eBook.Meta.MeetingEditor_Chairman
                    , propertyName:'chm'
                   }, {
                    xtype: 'metapersoneditor'
                    , ref: 'secr'
                    , fieldLabel: eBook.Meta.MeetingEditor_Secretary
                    , propertyName: 'secr'
                   }, {
                    xtype: 'metapersoneditor'
                    , ref: 'tell'
                    , fieldLabel: eBook.Meta.MeetingEditor_Teller
                    , propertyName: 'tell'
                   },{
                        xtype: 'numberfield'
                        , width: 250
                        , ref: 'shrs'
                        , name: 'shrs'
                        , allowDecimals: false
                        , value: 0
                        , fieldLabel: eBook.Meta.ShareHolderEditor_Shares
                        , disabled: eBook.Interface.isFileClosed()
                    }, { 
                    xtype: 'fieldset'
                     , ref: 'del'
                     , checkboxToggle:true
                     , checkboxName: 'del'
                     , title: eBook.Meta.MeetingEditor_Delayed
                     , autoHeight:true
                     , items : [{
                                    xtype:'datefield'
                                    , name: 'deldte'
                                    , ref: '../deldte'
                                    , value:''
                                    , format:'d/m/Y'
                                    , fieldLabel: eBook.Meta.MeetingEditor_Date// eBook.Meta.MeetingEditor_Date
                                    , disabled: eBook.Interface.isFileClosed()
                                   },{
                                    xtype:'timefield'
                                    , ref: '../deltime'
                                    , name: 'deltime'
                                    , value:''
                                    , format:'H:i'
                                    , fieldLabel: eBook.Meta.MeetingEditor_Time // eBook.Meta.MeetingEditor_Time
                                    , disabled: eBook.Interface.isFileClosed()
                                   }]
                    }]
        });

        eBook.Meta.ShareholdersMeetingEditor.superclass.initComponent.apply(this, arguments);
    }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        if (mobj) {
            if (mobj.dte) this.dte.setValue(Ext.data.Types.WCFDATE.convert(mobj.dte));
            this.time.setValue(mobj.time);
            this.chm.loadData(mobj);
            this.secr.loadData(mobj);
            this.tell.loadData(mobj);
            if (this.del.checkbox) {
                this.del.checkbox.dom.checked = mobj.del;
            } else {
                this.del.collapsed = !mobj.del;
            }
            if (mobj.deldte) this.deldte.setValue(Ext.data.Types.WCFDATE(mobj.deldte));
            this.deltime.setValue(mobj.deltime);
        }
    }
    , getData: function() {
        return {
            dte: !Ext.isEmpty(this.dte.getValue()) ? this.dte.getValue() : null
            ,time:this.time.getValue()
            , chm: this.chm.getData()
            , secr: this.secr.getData()
            , tell: this.tell.getData()
            , del: this.del.checkbox ? this.del.checkbox.dom.checked : false
            , deldte: !Ext.isEmpty(this.deldte.getValue()) ? this.deldte.getValue() : null
            ,deltime:this.deltime.getValue()
        };
    }
});

Ext.reg('metashareholdersmeetingeditor', eBook.Meta.ShareholdersMeetingEditor);
eBook.Meta.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        Ext.apply(this, {
             title: eBook.Meta.Window_Title
            , layout: 'fit'
            , width: 866
            , height: 450
            
            , tbar: [{
                //text:'Bewaar gegevens',
                ref: '../saveButton',
                iconCls: 'eBook-window-save-ico',
                scale: 'large',
                iconAlign: 'top',
                handler: this.onSaveClick,
                disabled: eBook.Interface.isFileClosed(),
                scope: this
}]
            , items: [{ xtype: 'tabpanel'
                     , activeTab: 0
                     , ref: 'tabpnl'
                     ,deferredRender :true
                     , items: [ {
                        
                         xtype: 'metagrid'
                                    , title: 'Peer group'
                                    , propertyName: 'pgroup'
                                    , columnsCfg: [{ mapping: 'ond', header: eBook.BusinessRelations.FormPanel_EnterpriseNumber }, { mapping: 'nme', header: 'Company name'}]
                                    , fieldsCfg: [{ name: 'ond' }, { name: 'nme'}]
                                    , editorPanel: {
                                            xtype: 'peergroupeditor'
                                            , title: 'Peer group - company'
                                            , propertyName: ''
                                            , disabled: eBook.Interface.isFileClosed()
                                     }
                     },{
                         xtype: 'metagrid'
                                    , title: 'Managers'
                                    , propertyName: 'mgrs'
                                    , columnsCfg: [{ mapping: 'gr', header: eBook.Meta.PersonEditor_Gender }, { mapping: 'nme', header: 'Manager'}]
                                    , fieldsCfg: [{ name: 'gr' }, { name: 'nme'}]
                                    , editorPanel: {
                                        xtype: 'metapersoneditor'
                                        , classType: 'MetaPersonDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
                                        , propertyName: ''
                                    }
                     }, {
                         xtype: 'metagrid'
                                    , title: 'Shareholders'
                                    , propertyName: 'shrhls'
                                    , columnsCfg: [{ mapping: 'gr', header: eBook.Meta.PersonEditor_Gender }, { mapping: 'nme', header: 'Shareholder' }
                                                   , { mapping: 'shrs', header: 'Shares'}
                                                   , { mapping: 'repr_nme', header: 'Represented by' }, { mapping: 'repr_gr', header: 'Representor Gender'}]
                                    , fieldsCfg: [{ name: 'gr' }, { name: 'nme' }, { name: 'shrs' }, { name: 'repr_nme', mapping: 'repr.nme' }, { name: 'repr_gr', mapping: 'repr.gr'}]
                                    , editorPanel: {
                                        xtype: 'shareholdereditor'
                                        , propertyName: ''
                                    }
                     }, {
                         xtype: 'metameetingeditor'
                                    , title: 'Board Meeting'
                                    , propertyName: 'bmeet'
                     }, {
                         xtype: 'metashareholdersmeetingeditor'
                                    , title: 'ShareHolders Meeting'
                                    , propertyName: 'smeet'
                     }, {
                         xtype: 'metameetingeditor'
                                    , title: 'Managers Meeting'
                                    , propertyName: 'mmeet'
                     }, {
                         xtype: 'metameetingeditor'
                                    , title: 'Partners Meeting'
                                    , propertyName: 'pmeet'
                     }
                     ]
}]
            });
            eBook.Meta.Window.superclass.initComponent.apply(this, arguments);
        }
    , show: function() {
        eBook.Meta.Window.superclass.show.call(this);
        this.load();
    }
    , load: function() {
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'GetFileMeta'
                    , method: 'POST'
                    , params: Ext.encode({
                        cipdc: {
                            Id: eBook.Interface.currentFile.get('Id')
                            , Person: eBook.User.getActivePersonDataContract()
                        }
                    })
                    , callback: this.onLoadResponse
                    , scope: this
        });
    }
    , onLoadResponse: function(opts, success, resp) {
       // try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                var rc = robj.GetFileMetaResult;
                this.loadObject(rc);
                this.getEl().unmask();
            } else {
                eBook.Interface.showResponseError(resp, this.title);
                this.getEl().unmask();
            }
       /* } catch (e) {
            //console.log(e);
            this.getEl().unmask();
            eBook.Interface.showError(e.message, this.title);
        }*/

    }
    , loadObject: function(robj) {
        robj = robj.dta;
        this.tabpnl.items.each(function(it) {
            it.loadData(robj);
        }, this);
    }
    , onSaveClick: function() {
        this.getEl().mask(eBook.Meta.Window._Saving, 'x-mask-loading');
        var o = {};
        this.tabpnl.items.each(function(it) {
            o[it.propertyName] = it.getData();
        }, this);

        var f = {
            fid: eBook.Interface.currentFile.get('Id')
            , dta: o
        };
        eBook.CachedAjax.request({
                url: eBook.Service.file + 'SaveFileMeta'
                , method: 'POST'
                , params: Ext.encode({
                    cfmdc: {
                        fmdc: f
                        , Person: eBook.User.getActivePersonDataContract()
                    }
                })
                , callback: this.onSaveResponse
                , scope: this
        });

    }
    , onSaveResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                var rc = robj.SaveFileMetaResult;
                this.getEl().unmask();
            } else {
                eBook.Interface.showResponseError(resp, this.title);
                this.getEl().unmask();
            }
        } catch (e) {
            //console.log(e);
            this.getEl().unmask();
            eBook.Interface.showError(e.message, this.title);
        }

    }

    });    


eBook.Health.Messages = Ext.extend(Ext.DataView, {
    initComponent: function() {
        //var worksheetName = eBook.Interface.center.fileMenu.getWorksheetRecord
        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                autoDestroy: true,
                root: 'GetMessagesWithoutNotesResult',
                fields: eBook.data.RecordTypes.Messages,
                proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                    url: eBook.Service.file + 'GetMessagesWithoutNotes'
                     , criteriaParameter: 'cfdc'
                     , method: 'POST'
                    //                     , baseParams: { FileId: eBook.currentFile.get('Id')
                    //                                    , Culture: eBook.Interface.Culture
                    //                     }
                })
                , listeners: { datachanged: { fn: this.onDataChanged, scope: this }
                            , afterrender: { fn: this.updateHealthClass, scope: this }
                            , click: { fn: this.onItemClick, scope: this }
                }
            })
            , id: 'eBook-file-healthmonitor'
            , tpl: new Ext.XTemplate(
		        '<tpl for=".">',
		            '<div class="ebook-message">',
		                '<tpl if="this.test(values)"><div><b>Worksheet:</b> {Worksheet}</div></tpl>', //
		                '<tpl if="CollectionText"><div><b>Tab:</b> {CollectionText}</div></tpl>', //
		                '<tpl if="FieldText"><div><b>Field:</b> {FieldText}</div></tpl>', //
                        '<div class="ebook-message-source-align ebook-message-source-{ConnectionType}-type-{Type}" id="{MessageId}">',
		                    '{Text}',
		                    
		                '</div>',
		            '</div>',
                '</tpl>',
                '<div id="eBook-health-joint" class="x-tab-joint" style="height: 100px; left: -2px; top: 11px;"></div>',
                '<div class="x-clear"></div>',
                {
                    test: function(values) {
                        var record = eBook.Interface.center.fileMenu.getWorksheetRecord(values.Guid);
                        values.Worksheet = record.data.name;
                        return values;
                    }
                }
	        )
	        , itemSelector: 'div.ebook-message'
	        , overClass: 'ebook-message-over'
            , cls: 'health-view'
            , data: []
            , border: false
            , autoScroll: true
        });
        eBook.Health.Messages.superclass.initComponent.apply(this, arguments);
        Ext.ComponentMgr.register(this);
    }
    , healthClass: ''
    , onItemClick: function(dv, idx, nde, e) {
        var rec = this.store.getAt(idx);
        if (rec.get('ConnectionType') == '0') {

            eBook.Interface.center.fileMenu.openWorksheetSh(rec.get('Guid'), rec.get('Collection'), rec.get('RowIndex'), rec.get('Field'));
        }
    }
    , onDataChanged: function() {

        if (this.store.query('Type', '0').length > 0) {
            this.healthClass = 'ebook-health-icon-ERROR';
        } else if (this.store.query('Type', '1').length > 0) {
            this.healthClass = 'ebook-health-icon-WARNING';
        } else if (this.store.query('Type', '2').length > 0) {
            this.healthClass = 'ebook-health-icon-INFO';
        }
        this.updateHealthClass();
        eBook.Interface.redrawWorksheetsView();
        this.refresh();
    }
    , updateHealthClass: function() {
        if (this.rendered) {
            var oel = this.ownerCt.getEl().parent();
            var icon = oel.child('.ebook-health-icon');
            icon.removeClass('ebook-health-icon-ERROR');
            icon.removeClass('ebook-health-icon-WARNING');
            icon.removeClass('ebook-health-icon-INFO');
            icon.addClass(this.healthClass);
        }
    }
    , getItemMessageType: function(id) {
        var its = this.store.query('Guid', id);
        if (its.getCount() == 0) {
            its = this.store.query('RowIndex', id);
        }
        var err = false, warn = false, info = false;
        its.each(function(it) {
            if (it.get('Type') == '0') err = true;
            if (it.get('Type') == '1') warn = true;
            if (it.get('Type') == '2') info = true;
        }, this);
        if (err) return 'ERROR';
        if (warn) return 'WARNING';
        if (info) return 'INFO';
        return '';
    }
    , checkHealthTab: function(tpeId, name) {
        var tabs = this.store.queryBy(function(rec) {
            return rec.get('Collection') == name && rec.get('Guid').toLowerCase() == tpeId.toLowerCase();
        }, this);


        if (tabs.getCount() > 0) {
            var err = false, warn = false, info = false;
            tabs.each(function(tab) {
                if (tab.get('Type') == '0') err = true;
                if (tab.get('Type') == '1') warn = true;
                if (tab.get('Type') == '2') info = true;
            }, this);
            if (err) { return 'eb-message-type-ERROR'; }
            else if (warn) { return 'eb-message-type-WARNING'; }
            else if (info) { return 'eb-message-type-INFO'; }
        }
        return '';
    }
    , getFieldsByTab: function(tpeId, tab) {
        var fields = new Array();
        fields = this.store.queryBy(function(rec) {
            return rec.get('Collection') == tab && rec.get('Guid').toLowerCase() == tpeId.toLowerCase();
        });
        if (fields.getCount() > 0) {
            return fields;
        }
        return '';
    }
});
Ext.reg('health-messages', eBook.Health.Messages);

eBook.Health.Panel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , cls: 'eBook-health-region-border'
            , items: [{
                xtype: 'panel'
                         , cls: 'eBook-health-center'
                        , border: false
                        , layout: 'fit', region: 'center'
                        , items: [{ xtype: 'health-messages',
                            style: 'border:1px; solid #999;',
                            ref: '../messages',
                            border: false}]
            }
                    , { xtype: 'panel'
                        , cls: 'eBook-health-selector'
                        , bodyStyle: 'background-color:transparent;'
                         , border: false,
                        html: '<div class="ebook-health-icon ebook-health-selected"></div>'
                         , region: 'east', width: 110
                    }
                    ]
        });
        eBook.Health.Panel.superclass.initComponent.apply(this, arguments);
    }
    , getItemMessageType: function(id) {
        return this.messages.getItemMessageType(id);
    }
    , checkHealthTab: function(tpeId,name) {
        return this.messages.checkHealthTab(tpeId, name);
    }
    , getFieldsByTab: function(tpeId, tab) {
        return this.messages.getFieldsByTab(tpeId, tab);
    }
});

Ext.reg('health', eBook.Health.Panel);




eBook.Repository.SingleUploadWindow = Ext.extend(eBook.Window, {
    existingItemId: null,
    changeFileName: false,
    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            title: eBook.Pdf.UploadWindow_Title
            , layout: 'fit'
            , width: 650
            , height: 200
            , modal: true
            //, iconCls: 'eBook-Window-account-ico'
            , items: [new Ext.FormPanel({
                fileUpload: true
                , labelWidth: 150
                , items: [
                    //new Ext.ux.form.FileUploadField({ width: 350, fieldLabel: eBook.Pdf.UploadWindow_ChoosePDF, fileTypeRegEx: /(\.pdf)$/i })
                    {xtype: 'fileuploadfield'
                        , width: 400
                        , fieldLabel: eBook.Pdf.UploadWindow_ChoosePDF
                        , fileTypeRegEx: /(\..*)$/i
                        , ref: 'file'
                    }
                    /* , {
                     xtype: 'hidden'
                     , name: 'fileType'
                     , value: 'pdf'
                     }*/]

                , ref: 'pdfForm'

            })]
            , tbar: [{
                text: eBook.Pdf.UploadWindow_UploadPDF,
                ref: '../saveButton',
                iconCls: 'eBook-icon-24-save',
                scale: 'medium',
                iconAlign: 'top',
                handler: me.onSaveClick,
                scope: me
            }]
        });

        eBook.Repository.SingleUploadWindow.superclass.initComponent.apply(me, arguments);
    }
    , onSaveClick: function (e) {
        var me = this;
        var f = me.pdfForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Pdf.UploadWindow_WrongType);
            return;
        }

        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Pdf.UploadWindow_Uploading,
            success: me.successUpload,
            failure: me.failedUpload,
            scope: me
        });
    }
    , successUpload: function (fp, o) {
        var me = this;
        //process o.result.file
        if (!this.existingItemId) {
            var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: o.result.id
                , standards: this.standards
                , readycallback: this.readycallback
                , clientId: me.clientId
            });
            if (!me.parentCaller) {
                wn.show()
            }
            else
            {
                wn.show(me.parentCaller);
            }
            wn.loadNewFile({ id: o.result.id, newName: o.result.file, oldName: o.result.originalFile, extension: o.result.extension, contentType: o.result.contentType });
            me.close();
        } else {
            me.getEl().mask("Replacing file", 'x-mask-loading');
            Ext.Ajax.request({
                url: eBook.Service.repository + 'ReplaceItem'
                , method: 'POST'
                , params: Ext.encode({
                    crridc: {
                        NewFileId: o.result.id,
                        Extension: o.result.extension,
                        Contenttype: o.result.contentType,
                        ItemId: me.existingItemId,
                        NewFileName: me.changeFileName ? o.result.file : null
                    }
                })
                , callback: me.onReplaceCallback
                , scope: me
            });
        }
    }
    , onReplaceCallback: function (opts, success, resp) {
        var me = this;
        me.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            if (robj.ReplaceItemResult) {
                var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: this.existingItemId, readOnly: false, readOnlyMeta: false, clientId: this.ClientId,readycallback: this.readycallback });

                //if parent defined
                if (!me.parentCaller) {
                    wn.show();
                }
                else
                {
                    wn.show(me.parentCaller);
                }
                wn.loadById();
                this.close();
            } else {
                eBook.Interface.showError("Failed to replace item", "Replace item");
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , failedUpload: function (fp, o) {
        var me = this;
        eBook.Interface.showError(o.message, me.title);
        me.getEl().unmask();
    }
    , show: function (caller) {
        var me = this;
        me.parentCaller = caller;
        eBook.Repository.SingleUploadWindow.superclass.show.call(me);
    }
});

Ext.override(Ext.tree.AsyncTreeNode, {
    isExpandable: function () {
        return this.attributes.isDynamic || this.attributes.expandable || this.hasChildNodes();
    },
    hasChildNodes: function () {
        if ((!this.isLeaf() && !this.loaded) || this.isDynamic) {
            return true;
        } else {
            return Ext.tree.AsyncTreeNode.superclass.hasChildNodes.call(this);
        }
    },
    loadComplete: function (deep, anim, callback, scope) {
        this.loading = false;
        this.loaded = true;
        this.completedRun = true;
        this.ui.afterLoad(this);
        this.fireEvent("load", this);
        this.expand(deep, anim, callback, scope);
        this.completedRun = false;
    },
    expand: function (deep, anim, callback, scope) {
        if (this.loading) { // if an async load is already running, waiting til it's done
            var timer;
            var f = function () {
                if (!this.loading) { // done loading
                    clearInterval(timer);
                    this.expand(deep, anim, callback, scope);
                }
            } .createDelegate(this);
            timer = setInterval(f, 200);
            return;
        }
        if (this.attributes && this.attributes.isDynamic && !this.completedRun) {
            delete this.attributes.children;
            this.attributes.expandable = true;
            this.loaded = false;
        }
        if (!this.loaded && !this.completedRun) {
            if (this.fireEvent("beforeload", this) === false) {
                return;
            }
            this.loading = true;
            this.ui.beforeLoad(this);
            var loader = this.loader || this.attributes.loader || this.getOwnerTree().getLoader();
            if (loader) {
                loader.load(this, this.loadComplete.createDelegate(this, [deep, anim, callback, scope]), this);
                return;
            }
        }
        Ext.tree.AsyncTreeNode.superclass.expand.call(this, deep, anim, callback, scope);
    }
});  

Ext.tree.AsyncTreeNode.prototype.findParent= function(attribute, value) {
        return this.findParent(function() {
            return this.attributes[attribute] == value;
        }, null);
};
Ext.tree.AsyncTreeNode.prototype.findParentBy = function(fn, scope) {
        // include self
        if (fn.call(scope || this, this) === true) { 
            return this;
        }
        if (this.parentNode) {
            if (fn.call(scope || this.parentNode, this.parentNode) === true) {
                return this.parentNode;
            } else {
                return this.parentNode.findParentBy(fn, scope);
            }
        }
        return null;
};


eBook.Repository.TreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    filter: {
        ps: null
        , pe: null
        , stat: null
    }
    , load: function (node, callback, scope) {
        if (this.clearOnLoad) {
            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
        }
        if (this.doPreload(node)) { // preloaded json children
            this.runCallback(callback, scope || node, [node]);
        } else if (this.directFn || this.dataUrl || this.url) {
            this.requestData(node, callback, scope || node);
        }
    }
    , doPreload: function (node) {
        if (node.attributes.children) {
            if (node.childNodes.length < 1) { // preloaded?
                var cs = node.attributes.children;
                node.beginUpdate();
                for (var i = 0, len = cs.length; i < len; i++) {
                    var cn = node.appendChild(this.createNode(cs[i]));
                    if (this.preloadChildren) {
                        this.doPreload(cn);
                    }
                }
                node.endUpdate();
            }
            return true;
        }
        // alert("false dopreload");
        return false;
    }
    , selectionMode: false
    , createNode: function (attr, parent) {
        // apply baseAttrs, nice idea Corey!
        if (!Ext.isDefined(attr.originalConfig)) {
            attr.originalConfig = Ext.decode(Ext.encode(attr));
        }
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (!attr.leaf) {
            if (!Ext.isDefined(attr.StructureId)) {
                Ext.apply(attr, { StructureId: attr.id });
            }
            attr.id = Ext.id();
        }

        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        if (Ext.isString(attr.uiProvider)) {
            attr.uiProvider = this.uiProviders[attr.uiProvider] || eval(attr.uiProvider);
        }
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                        new Ext.tree.TreeNode(attr) :
                        new Ext.tree.AsyncTreeNode(attr);
        }
    }
    , requestData: function (node, callback, scope) {
        if (node.attributes.StructureId == 'root') {
            this.processNodes(node, eBook.Interface.GetRepoStructure('perm'), callback, scope);
        } else {
            if (node.attributes.Key == 'PERIOD_ITEM') {
                this.processNodes(node, eBook.Interface.GetRepoStructure('period'), callback, scope);
            } else {
                // if (!this.selectionMode) {
                this.transId = Ext.Ajax.request({
                    method: 'POST',
                    url: this.dataUrl || this.url,
                    success: this.handleResponse,
                    failure: this.handleFailure,
                    scope: this,
                    argument: { callback: callback, node: node, scope: scope },
                    params: this.getSendingParam(node)
                });
                //} else {
                //    this.runCallback(callback, scope || node, [node]);
                //}
            }
        }

    }
    , processNodes: function (node, o, callback, scope) {
        node.beginUpdate();
        for (var i = 0, len = o.length; i < len; i++) {
            var n = this.createNode(o[i], node);
            if (n) {
                if (n.attributes.Files == false && this.selectionMode) {
                    if (!Ext.isEmpty(n.attributes.cls) && n.attributes.cls.length > 0) {
                        n.attributes.cls += ' ';
                    } else {
                        n.attributes.cls = '';
                    }
                    n.attributes.cls += "tree-node-unselectable";
                }

                node.appendChild(n);

            }
        }
        node.endUpdate();
        this.runCallback(callback, scope || node, [node]);
    }
    , processResponse: function (response, node, callback, scope) {
        var json = response.responseText;
        try {
            var o = response.responseData || Ext.decode(json);
            o = o.GetChildrenResult;
            this.processNodes(node, o, callback, scope);
        } catch (e) {
            this.handleFailure(response);
        }
    }
    , getSendingParam: function (node) {
        var ot = node.getOwnerTree();
        // file
        var fileId = null;        
        if (eBook.Interface.currentFile) {
            if (eBook.Interface.currentFile.gthTeam5FileId) {
                fileId = eBook.Interface.currentFile.gthTeam5FileId;
            } else {
                fileId = eBook.Interface.currentFile.get('Id');
            }
        }

        // client
        var clientId = null;
        if (eBook.Interface.currentClient) {
            if (eBook.Interface.currentClient.gthTeam5ClientId) {
                clientId = eBook.Interface.currentClient.gthTeam5ClientId;
            } else {
                clientId = eBook.Interface.currentClient.get('Id');
            }
        }

        var o = { crcdc: {
                cid: clientId
                //cid: eBook.Interface.currentClient ? eBook.Interface.currentClient.get('Id') : ot.ClientId
                , cult: eBook.Interface.Culture
                , strucId: node.attributes.StructureId == 'root' ? null : node.attributes.StructureId
                , key: node.attributes.Key
                , strucTp: node.attributes.StructuralType
                , showFiles: this.showFiles
                , fileId: fileId
            //, fileId: eBook.Interface.currentFile ? eBook.Interface.currentFile.get('Id') : null
        }
        };

        Ext.apply(o.crcdc, this.filter);
        if (ot.filter) {
            Ext.apply(o.crcdc, ot.filter);
        }
        var periodParent = null;
        if (node.attributes.StructuralType == "FILE") {

            periodParent = node.findParentBy(function () {
                return this.attributes.PeriodStart && !Ext.isEmpty(this.attributes.PeriodStart);
            }, null);
        }
        if (periodParent) {
            o.crcdc.ps = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodStart);
            o.crcdc.pe = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodEnd);
        }
        return Ext.encode(o);
    }
});


eBook.Repository.Tree = Ext.extend(Ext.tree.TreePanel, {
    readOnly: false,
    readOnlyMeta: false,
    standardFilter: {
        ps: null
            , pe: null
            , stat: null
    },
    initComponent: function () {
        if (eBook.Interface.currentFile) {
            this.standardFilter = {
                ps: eBook.Interface.currentFile.get('StartDate')
                , pe: eBook.Interface.currentFile.get('EndDate')
                , stat: null
            };
        }

        var loader = new eBook.Repository.TreeLoader({
            dataUrl: eBook.Service.repository + 'GetChildren',
            requestMethod: "POST", showFiles: true, filter: this.standardFilter
        });

        var root = {
            nodeType: 'async',
            text: 'Repository',
            draggable: false,
            id: 'root'//,
            // expanded: true
        };
        if (this.startFrom) {
            //'period'
            root = loader.createNode(eBook.Interface.GetRepoStructureById(this.startFrom.type, this.startFrom.id));
        }

        if (!this.readOnly) {
            Ext.apply(this, {
                tbar: [{
                    ref: 'addfile',
                    text: 'Upload file', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onAddFileClick,
                    width: 70,
                    scope: this,
                    disabled: eBook.Interface.isFileClosed()
                }, {
                    ref: 'actFilter',
                    text: 'Filter', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-filter-ico-24',
                    scale: 'medium',
                    enableToggle: true,
                    pressed: false,
                    iconAlign: 'top',
                    toggleHandler: this.onFilterClick,
                    width: 70,
                    hidden: eBook.Interface.currentFile != null,
                    scope: this
                },
                    {
                        xtype: 'buttongroup',
                        ref: 'dateFilters',
                        disabled: true,
                        columns: 2,
                        hidden: eBook.Interface.currentFile != null,
                        title: 'Date/period filter',
                        items: [
                        { xtype: 'datefield', format: 'd/m/Y', name: 'StartDate', ref: 'startDate', tooltip: 'Start date', listeners: { select: { fn: this.onDateFilterChange, scope: this }, change: { fn: this.onDateFilterChange, scope: this}} }
                        , { xtype: 'datefield', format: 'd/m/Y', name: 'EndDate', ref: 'endDate', tooltip: 'End date', listeners: { select: { fn: this.onDateFilterChange, scope: this }, change: { fn: this.onDateFilterChange, scope: this}} }
                        ]
                    }
               , {
                   ref: 'replacefile',
                   text: 'Replace file', // eBook.Accounts.Manage.Window_AddAccount,
                   iconCls: 'eBook-replace-menu-context-icon',
                   scale: 'medium',
                   iconAlign: 'top',
                   handler: this.onReplaceItemClick,
                   width: 70,
                   scope: this,
                   disabled: true
               }
               , {
                   ref: 'deletefile',
                   text: 'Delete file', // eBook.Accounts.Manage.Window_AddAccount,
                   iconCls: 'eBook-recycle-menu-context-icon ',
                   scale: 'medium',
                   iconAlign: 'top',
                   handler: this.onDeleteItemClick,
                   width: 70,
                   scope: this,
                   disabled: true
               }
               , {
                   ref: 'sentFileToDocstore',
                   text: 'Send file to DocStore', // eBook.Accounts.Manage.Window_AddAccount,
                   iconCls: 'eBook-Docstore-24-ico',
                   scale: 'medium',
                   iconAlign: 'top',
                   handler: this.sentToDocstore,
                   width: 70,
                   scope: this
               }

                    ]
            });
        }
        Ext.apply(this, {
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDD: false,
            containerScroll: true,
            //border: false,
            // auto create TreeLoader
            // dataUrl: 'get-nodes.php',
            loader: loader,
            rootVisible: false,
            root: root
            , selModel: new Ext.tree.DefaultSelectionModel({ listeners: { selectionchange: { fn: this.onNodeSelect, scope: this}} })
            , listeners: {
                dblclick: { fn: this.onNodeDblClick, scope: this }
                , contextmenu: { fn: this.onNodeContextClick, scope: this }
                , expandnode: { fn: this.expandNode, scope: this }
                , collapsenode: { fn: this.expandNode, scope: this }
            }
            , keys: [
	            {
	                key: Ext.EventObject.DELETE
	                , fn: this.onDeleteItemClick
	                , scope: this
	            }
	        ]
        });
        eBook.Repository.Tree.superclass.initComponent.apply(this, arguments);
    }
    , expandNode: function (node) {
        if (this.topToolbar) {
            if (node.attributes.DocstoreAccess) {
                this.topToolbar.sentFileToDocstore.show();
                this.topToolbar.sentFileToDocstore.disable();
            } else {
                this.topToolbar.sentFileToDocstore.hide();
            }
        }
    }
    , sentToDocstore: function (t, e) {
        var node = t.scope.getSelectionModel().selNode;
        if (node.attributes.Item.Extension == ".pdf") {
            var wn = new eBook.Docstore.Window({ documentId: node.attributes.Item.Id, clientId: node.attributes.Item.ClientId, culture: eBook.Interface.Culture, partnerGpn: node.attributes.Item.PartnerGpnAcr, uploaderGpn: eBook.User.gpn });
            wn.show();
        } else {
            Ext.Msg.show({
                title: 'Error',
                msg: 'You can only upload PDF files to docstore',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                scope: this
            });
        }


        //        var par = {
        //            ciddc: {
        //                'Id': node.attributes.Item.ClientId
        //                , 'Department': 'ACR'//eBook.Interface.center.clientMenu.files.getLatestId()
        //            }
        //        };
        //        Ext.Ajax.request({
        //            method: 'POST',
        //            url: eBook.Service.client + 'GetClientPartnerByDepartment',
        //            callback: this.sentToDocstoreCallback,
        //            scope: this,
        //            node: node,
        //            params: Ext.encode(par)
        //        });


        /*       
        Ext.Ajax.request({
        method: 'POST',
        url: eBook.Service.docstore + 'GetDocumentInfo',
        callback: this.sentToDocstoreCallback,
        scope: this,
        params: Ext.encode(par)
        });
        */
    }
    //    , sentToDocstoreCallback: function (opts, success, resp) {
    //        var node = opts.node;
    //        var partnerGpn = Ext.decode(resp.responseText).GetClientPartnerByDepartmentResult[0].gpn
    //        var wn = new eBook.Docstore.Window({ documentId: node.attributes.Item.Id, clientId: node.attributes.Item.ClientId, culture: eBook.Interface.Culture, partnerGpn: partnerGpn });
    //        wn.show();
    //    }
    , onNodeContextClick: function (nde, e) {
        if (this.readOnly) return;
        if (!Ext.isDefined(nde.attributes.Item)) return;
        if (!nde.isLeaf()) return;
        nde.select();
        if (!this.contextMenu) {
            this.contextMenu = new eBook.Repository.ContextMenu({ scope: this });
        }
        if (!nde.attributes.Item.DocstoreAccess) {
            this.contextMenu.docstoreItem.hide();
        } else {
            this.contextMenu.docstoreItem.show();
        }
        this.contextMenu.showAt(e.getXY());

    }
    , onNodeSelect: function (sm, nd) {
        if (this.readOnly) return;
        var tb = this.getTopToolbar();
        tb.deletefile.disable();
        tb.replacefile.disable();
        if (nd && nd.leaf) {
            tb.deletefile.enable();
            tb.replacefile.enable();
        }
        if (!this.contextMenu) {
            this.contextMenu = new eBook.Repository.ContextMenu({ scope: this });
        }

        if (nd.attributes.Item) {
            if (nd.attributes.Item.DocstoreAccess) {
                tb.sentFileToDocstore.show();
                tb.sentFileToDocstore.enable();
            } else {
                tb.sentFileToDocstore.hide();
            }
            if (nd.attributes.Item.DocstoreId) {
                tb.deletefile.disable();
                tb.replacefile.disable();
                tb.sentFileToDocstore.disable();
                this.contextMenu.docstoreItem.disable();
                this.contextMenu.copyItem.disable();
                this.contextMenu.moveItem.disable();
                this.contextMenu.deleteItem.disable();
            }
        } else {
            if (nd.attributes.DocstoreAccess) {
                tb.sentFileToDocstore.show();
                tb.sentFileToDocstore.disable();
            } else {
                tb.sentFileToDocstore.hide();

            }
        }

    }
    , getNewId: function (callback, scope, extraparams) {
        var cscope = scope || this;
        this.getEl().mask("Generating new id.", 'x-mask-loading');
        Ext.Ajax.request({
            method: 'POST',
            url: eBook.Service.url + 'GetNewId',
            callback: callback,
            scope: cscope,
            extras: extraparams
        });
    }
    , onMoveItem: function () {
        var sm = this.getSelectionModel();
        var n = sm.getSelectedNode();
        if (!n) return;
        var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: n.attributes.Item.Id, tpe: 'MOVE' });
        wn.show(this.parentCaller);
        wn.loadExistingFile(n);
    }
    , onCopyItem: function () {
        this.getNewId(this.onLoadCopied, this);
    }
    , onLoadCopied: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            var id = robj.GetNewIdResult;
            var sm = this.getSelectionModel();
            var n = sm.getSelectedNode();
            if (!n) return;

            var attr = Ext.decode(Ext.encode(n.attributes.originalConfig));
            attr.id = id;
            attr.text = 'Copy of ' + attr.text;
            attr.Item.FileName = 'Copy of ' + attr.Item.FileName;
            attr.Item.Id = id;
            n = new Ext.tree.AsyncTreeNode(attr);
            var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: n.attributes.Item.Id, tpe: 'COPY' });
            wn.show(this.parentCaller);
            wn.loadExistingFile(n);
        } else {
            eBook.Interface.showResponseError(resp, "New Id");
        }
    }
    , onViewItem: function () {
        var sm = this.getSelectionModel();
        var n = sm.getSelectedNode();
        if (!n) return;
        var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: n.attributes.Item.Id, tpe: 'VIEWFILE' });
        wn.show(this.parentCaller);
        wn.loadExistingFile(n);
    }
    , onReplaceItemClick: function () {
        var sm = this.getSelectionModel(),
            n = sm.getSelectedNode();

        if (!n || !n.leaf) {
            Ext.Msg.show({
                title: 'Not a file',
                msg: 'No file was selected. Please select a file first',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING,
                scope: this
            });
            return;
        } else {
            //replace window
            var wn = new eBook.Repository.SingleUploadWindow({ existingItemId: n.attributes.Item.Id });
            wn.show(this);
        }
    }
    , onDeleteItem: function () { this.onDeleteItemClick(); }
    , onDeleteItemClick: function () {
        var sm = this.getSelectionModel();
        var n = sm.getSelectedNode()
        if (!n || !n.leaf) {
            Ext.Msg.show({
                title: 'Not a file',
                msg: 'No file was selected. Please select a file first',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING,
                scope: this
            });
        } else {
            this.checkLinksBeforeDeleting(n);
        }
    }
    , checkLinksBeforeDeleting: function (node) {
        this.getEl().mask("Checking for active links on " + node.text, 'x-mask-loading');
        var par = {
            cildc: {
                'ItemId': node.attributes.Item.Id
                , 'FileId': eBook.Interface.center.clientMenu.files.getLatestId()
                , 'Culture': eBook.Interface.Culture
            }
        };
        Ext.Ajax.request({
            method: 'POST',
            url: eBook.Service.repository + 'GetItemLinks',
            callback: this.checkLinksBeforeDeletingCallback,
            scope: this,
            node: node,
            params: Ext.encode(par)
        });


    }
    , checkLinksBeforeDeletingCallback: function (opts, success, resp) {

        if (success) {
            var robj = Ext.decode(resp.responseText);
            robj = robj.GetItemLinksResult;
            if (robj.length > 0) {
                Ext.Msg.show({
                    title: 'Links detected',
                    msg: 'The file \'' + opts.node.text + '\' wich you are trying to delete has ' + robj.length + ' active links.<br/>Are you sure you want to delete this file and <u>all</u> links?',
                    buttons: Ext.Msg.YESNO,
                    fn: this.onAcceptDelete,
                    icon: Ext.MessageBox.WARNING,
                    node: opts.node,
                    scope: this
                });
            } else {
                Ext.Msg.show({
                    title: 'No Links detected',
                    msg: 'The file \'' + opts.node.text + '\' doesn\'t have any active link.<br/>Do you wish to delete this file?',
                    buttons: Ext.Msg.YESNO,
                    fn: this.onAcceptDelete,
                    icon: Ext.MessageBox.QUESTION,
                    node: opts.node,
                    scope: this
                });
            }
        } else {
            //alert('Could not retrieve file links. If persists, contact champion.');
            // means no file links
            Ext.getBody().unmask();
        }
    }
    , onAcceptDelete: function (btnid, txt, opts) {
        this.getEl().unmask();
        if (btnid == "yes") {
            this.getEl().mask("Deleting file " + opts.node.text, 'x-mask-loading');
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'DeleteFile',
                callback: this.deletePerformed,
                scope: this,
                node: opts.node,
                params: Ext.encode({ cidc: { Id: opts.node.attributes.Item.Id} })
            });
        } else {
            Ext.Msg.show({
                title: 'File deletion CANCELLED',
                msg: 'You have cancelled the deletion of the file <u>\'' + opts.node.text + '\'</u>.<br/> <b>The file was not deleted</b>',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO,
                scope: this
            });
        }
    }
    , deletePerformed: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            opts.node.remove(true);
        }
    }
    , onFilterClick: function (btn, state) {
        var tb = this.getTopToolbar();
        if (state) {
            tb.dateFilters.enable();
            this.onDateFilterChange();
        } else {
            tb.dateFilters.disable();
            this.getLoader().filter.ps = null;
            this.getLoader().filter.pe = null;
            this.reload();
            //tb.dateFilters.startDate.setValue();
            //tb.dateFilters.endDate.setValue();
        }

    }
    , onDateFilterChange: function () {
        var tb = this.getTopToolbar();
        var st = tb.dateFilters.startDate.getValue();
        var ed = tb.dateFilters.endDate.getValue();
        if (this.standardFilter.ps && st < this.standardFilter.ps) {
            st = this.standardFilter.ps;
            tb.dateFilters.startDate.setValue(st);
        }
        if (this.standardFilter.pe && ed > this.standardFilter.pe) {
            ed = this.standardFilter.pe;
            tb.dateFilters.endDate.setValue(ed);
        }

        this.getLoader().filter.ps = st == '' ? null : st;
        this.getLoader().filter.pe = ed == '' ? null : ed;
        this.reload();
    }
    , reload: function () {
        this.setRootNode({
            nodeType: 'async',
            text: 'Repository',
            draggable: false,
            id: 'root',
            expanded: true
        });
    }
    , reloadRepo: function () {
        var loader = new eBook.Repository.TreeLoader({
            dataUrl: eBook.Service.repository + 'GetChildren',
            requestMethod: "POST", showFiles: true, filter: this.standardFilter
        });
        loader.requestData();
        var root = {
            nodeType: 'async',
            text: 'Repository',
            draggable: false,
            id: 'root'//,
            // expanded: true
        };
        if (this.startFrom) {
            //'period'
            root = loader.createNode(eBook.Interface.GetRepoStructureById(this.startFrom.type, this.startFrom.id));
        }
    }
    , onAddFileClick: function () {
        // var wn = new eBook.Repository.LocalFileBrowser({});
        //var wn = new eBook.Window({layout:'fit',items:[new eBook.Repository.FileDetailsPanel({})]});
        var wn = new eBook.Repository.SingleUploadWindow({});
        wn.show(this);
    }
    , onNodeDblClick: function (n, e) {
        if (n.leaf) {
            var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: n.attributes.Item.Id, readOnly: this.readOnly, readOnlyMeta: this.readOnlyMeta, clientId: this.ClientId });
            wn.show(this.parentCaller);
            wn.loadExistingFile(n);

            wn.on('beforeclose', function () {
                if (this.ownerCt.ownerCt.ownerCt.ownerCt.repos != null) {
                    this.ownerCt.ownerCt.ownerCt.ownerCt.loadRepo();
                }
            }, this);
        }
    }
});

Ext.reg('repository', eBook.Repository.Tree);



eBook.Repository.ContextMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'eBook-repository-context-menu'
            , shadow: false
            , items: [{
                text: 'Sent to DocStore',
                iconCls: 'eBook-Docstore-16-ico',
                handler: this.scope.sentToDocstore,
                ref: 'docstoreItem',
                scope: this.scope
                //,hidden:true
            },'-', {
                text: 'Copy...',
                iconCls: 'eBook-icon-copy-16',
                handler: this.scope.onCopyItem,
                ref: 'copyItem',
                scope: this.scope
            }, {
                text: 'Move...',
                iconCls: 'eBook-icon-move-16',
                handler: this.scope.onMoveItem,
                ref: 'moveItem',
                scope: this.scope
            },'-', {
                text: 'Delete...',
                iconCls: 'eBook-icon-delete-16',
                handler: this.scope.onDeleteItem,
                ref:'deleteItem',
                scope: this.scope
            }]

            });
            eBook.Repository.ContextMenu.superclass.initComponent.apply(this, arguments);
        }
//    , showMe: function(el, accountNr, hta) {
//        this.activeEl = el;
//        this.accountNr = accountNr;
//        eBook.Accounts.ContextMenu.superclass.show.apply(this, [el, 'c']);
//        if (!hta) {
//            this.editAdjustments.disable();
//        } else {
//            this.editAdjustments.enable();
//        }
//    }
//    , showMeAt: function(el, accountNr, hta, xy) {
//        this.activeEl = el;
//        this.accountNr = accountNr;
//        eBook.Accounts.ContextMenu.superclass.showAt.apply(this, [xy]);
//        if (!hta) {
//            this.editAdjustments.disable();
//        } else {
//            this.editAdjustments.enable();
//        }
//    }
});

Ext.reg('repositorycontextmenu', eBook.Repository.ContextMenu);




eBook.Repository.ItemLinksFileNameColumn = Ext.extend(Ext.list.Column, {
    
    constructor : function(c) {
        c.tpl = new Ext.XTemplate('{[this.getFileName(values.' + c.dataIndex + ')]}'
                    , {getFileName:function(id) {
                        if (id == eBook.EmptyGuid) return eBook.Repository.ItemLinks_DefaultFile;
                        return eBook.Interface.center.clientMenu.files.getFileName(id);
                    }});      
        eBook.Repository.ItemLinksFileNameColumn.superclass.constructor.call(this, c);
    }
});
Ext.reg('lvreposfilenamecolumn', eBook.Repository.ItemLinksFileNameColumn);

eBook.Repository.ListLinkTypeColumn = Ext.extend(Ext.list.Column, {
    constructor: function(c) {
        c.tpl = c.tpl || new Ext.XTemplate('<div class="eb-repos-link-type eb-repos-link-type-{' + c.dataIndex + '}"></div>');

        eBook.Repository.ListLinkTypeColumn.superclass.constructor.call(this, c);
    }
});

Ext.reg('lvlinktype', eBook.Repository.ListLinkTypeColumn);


eBook.Repository.ItemLinksDataView = Ext.extend(Ext.list.ListView, {
    initComponent: function() {
        Ext.apply(this, {
            emptyText: 'No links to display'
            , loadingText: 'Please Wait...'
            , multiSelect: false
            , singleSelect:true
            , store: new eBook.data.JsonStore({
                selectAction: 'GetItemLinks'
                            , serviceUrl: eBook.Service.repository
                            , autoDestroy: true
                            , autoLoad: true
                            , criteriaParameter: 'cildc'
                            , fields: eBook.data.RecordTypes.RepositoryItemLinks
                            , baseParams: {
                                'ItemId': this.repositoryItemId
                                , 'FileId': this.fileId
                                , 'Culture': eBook.Interface.Culture

                            }
            })
            , columns: [{
                        xtype:'lvlinktype',
                        header: 'Type',
                        width: .05,
                        dataIndex: 'ConnectionType'
                    }, {
                        header: 'Dossier',
                        xtype: 'lvreposfilenamecolumn',
                        width: .2,
                        dataIndex: 'FileId'
                    }, {
                        header: 'Link',
                        dataIndex: 'Description'
                }]
            
            });
            eBook.Repository.ItemLinksDataView.superclass.initComponent.apply(this, arguments);
        }
    });

Ext.reg('RepositoryItemLinksDataView', eBook.Repository.ItemLinksDataView);

eBook.Repository.ItemLinksPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.fileId = (eBook.Interface && eBook.Interface.center && eBook.Interface.center.clientMenu && eBook.Interface.center.clientMenu.files) ? eBook.Interface.center.clientMenu.files.getLatestId() : null;
        if (this.fileId == null) {
            Ext.apply(this, {
                html: 'Client does not contain any file. <br/>At least 1 file is needed in order to be able to select an account. <br/>Default linking is disabled.'
            });

        } else {
            Ext.apply(this, {
                items: [{ xtype: 'RepositoryItemLinksDataView', fileId: this.fileId, repositoryItemId: this.repositoryItemId, ref: 'myView', listeners: { selectionchange: { fn: this.onSelect, scope: this}}}]
            , layout: 'fit'
            , tbar: [
                {
                    xtype: 'combo'
                    , store: new eBook.data.JsonStore({
                        selectAction: 'GetQueriedAccountsList'
                        , baseParams: {
                            FileId: this.fileId
                                , Culture: eBook.Interface.Culture
                        }
                        , serviceUrl: eBook.Service.schema
                        , criteriaParameter: 'cadc'
                        , autoDestroy: true
                        , fields: eBook.data.RecordTypes.GlobalListItem
                    })
                , displayField: eBook.Interface.Culture.substr(0, 2)
                , valueField: 'id'
                , minChars: 1
                , hideTrigger: true
                , typeAhead: true
                , lazyRender: true
                , forceSelect: true
                , listWidth: 300
                , queryParam: 'Query'
                , selectOnFocus: true
                , ref: 'accountSel'
                }
                , {
                    ref: 'addLnk',
                    text: 'Add account link',
                    iconCls: 'eBook-icon-add-16',
                    scale: 'small',
                    iconAlign: 'left',
                    disabled: false,
                    handler: this.onAddLinkClick,
                    scope: this
                }, {
                    ref: 'delLnk',
                    text: 'Delete link',
                    iconCls: 'eBook-icon-delete-16',
                    scale: 'small',
                    iconAlign: 'left',
                    disabled: true,
                    handler: this.onDeleteLinkClick,
                    scope: this
}]
                });
            }

            eBook.Repository.ItemLinksPanel.superclass.initComponent.apply(this, arguments);
        }
    , onSelect: function(ls, sel) {
        var tb = this.getTopToolbar();
        tb.delLnk.disable();
        this.selected = null
        if (sel && sel.length > 0) {
            tb.delLnk.enable();
            this.selected = ls.getRecord(sel[0]);
        }
    }
    , onDeleteLinkClick: function() {
        if (this.selected) {
            Ext.Msg.show({
                title: 'Delete link',
                msg: 'Are you sure you want to delete this link?',
                buttons: Ext.Msg.YESNO,
                fn: this.onAcceptDelete,
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        }
    }
    , onAcceptDelete: function(btnid, txt, opts) {
        this.getEl().unmask();
        if (btnid == "yes") {
            this.getEl().mask("Deleting file " + this.selected.get('Description'), 'x-mask-loading');
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'DeleteLink',
                callback: this.deletePerformed,
                scope: this,
                params: Ext.encode({ rlidc: this.selected.json })
            });
            this.selected = null;
            var tb = this.getTopToolbar();
            tb.delLnk.disable();
        }
    }
    , deletePerformed: function() {
        this.myView.store.load();
        this.getEl().unmask();
        if (eBook.Interface.currentFile) {
            eBook.Interface.loadRepoLinks();
        }
    }
    , onAddLinkClick: function() {
        var tb = this.getTopToolbar();
        if (!Ext.isEmpty(tb.accountSel.getValue())) {
            Ext.Ajax.request({
                url: eBook.Service.repository + 'AddLink'
                    , method: 'POST'
                    , params: Ext.encode({ rldc: {
                        ClientId: eBook.Interface.currentClient.get('Id')
                            , FileId: eBook.EmptyGuid
                            , ItemId: this.repositoryItemId
                            , ConnectionType: 'ACCOUNT'
                            , ConnectionGuid: eBook.EmptyGuid
                            , ConnectionAccount: tb.accountSel.getValue()
                             , ConnectionDetailedPath: ''
                    }
                    })
                    , callback: this.onAddLinkResponse
                    , scope: this
            });
        }
    }
    , onAddLinkResponse: function(opts, success, resp) {
        if (success) {
            this.myView.store.load();
            var tb = this.getTopToolbar();
            tb.accountSel.reset();
        }
    }
    , allowDefaultLinking: function(allow) {
        var tb = this.getTopToolbar();
        if (tb) {
            if (allow) {
                tb.show();
            }
            else {
                tb.hide();
            }
        }
    }
    });


Ext.reg('RepositoryItemLinks', eBook.Repository.ItemLinksPanel);


eBook.Repository.ViewPdfPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            html: ' LOADING... '

        });
        eBook.Repository.ViewPdfPanel.superclass.initComponent.apply(this, arguments);
    }
    , afterRender: function() {
        var re = /(?:\.([^.]+))?$/;
        eBook.Repository.ViewPdfPanel.superclass.afterRender.call(this)
        if (this.fileUrl) {
            var exten = re.exec(this.fileUrl)[1];
            if (exten == "pdf") {
                this.pdfEl = new PDFObject({ url: this.fileUrl }).embed(this.body.id);
            } else {
                this.body.update("No preview, <a href='" + this.fileUrl + "'>Click here to download the file</a>");
            }
        }
        var test = 3.3;
    },
    loadFile: function(url) {
        var me = this;
        var re = /(?:\.([^.]+))?$/;
        this.fileUrl = url;
        if (me.rendered) {
            me.unloadFile();
            var exten = re.exec(this.fileUrl)[1];
            if (exten == "pdf") {
                me.pdfEl = new PDFObject({ url: me.fileUrl + "?dc=" + eBook.NewGuid() }).embed(this.body.id);
            } else {
                me.body.update("No preview, <a href='" + me.fileUrl + "'>Click here to download the file</a>");
            }
        }

    },
    unloadFile: function() {
        var me = this;
        if (me.pdfEl) Ext.get(me.pdfEl).remove();
    }
});

Ext.reg('RepositoryPreview', eBook.Repository.ViewPdfPanel);
function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months;
}

eBook.Fields.ReposHide = function() {

    var pel = this.getEl().parent('.eb-repos-filemeta-field');
    pel.setVisibilityMode('display');
    pel.hide();
};

eBook.Fields.ReposShow = function() {

    var pel = this.getEl().parent('.eb-repos-filemeta-field');
    pel.setVisibilityMode('display');
    pel.show();
};

eBook.Repository.RepoCommentsView = Ext.extend(Ext.Component, {

    initComponent: function () {

        Ext.apply(this, {
           /* store: new Ext.data.JsonStore({
                fields: [{ name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
                , { name: 'PersonId', mapping: 'pid', type: Ext.data.Types.STRING }
                 , { name: 'PersonName', mapping: 'pn', type: Ext.data.Types.STRING }
                , { name: 'Comment', mapping: 'cmt', type: Ext.data.Types.STRING }
                , { name: 'PostDate', mapping: 'pd', type: Ext.data.Types.WCFDATE }
               ],
                data: [{ Id: 'test-1', PersonId: 'Pers-1', PersonName: 'Test Person', Comment: 'rgjrogjzrogjzrogjreogjg g rg gzjerg', PostDate: "/Date(1418379755867+0100)/"}]
            }),*/
            autoHeight: true,
            autoScroll: true,
            autoEl:{
                tag:'div',
                cls:'eBook-repo-comments'

            }
            , mytpl: new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="eBook-repo-comment">',
                    '<div class="eBook-repo-comment-txt">{Comment}</div>',
                    '<div class="eBook-repo-comment-by"> <tpl for="status">{en}<br/></tpl>{PersonName} {PostDateString}</div>',
                '</div>',
                '</tpl>')

        });
        eBook.Repository.RepoCommentsView.superclass.initComponent.apply(this, arguments);
    }

    , setData: function (dta) {
        var me = this;


        me.getEl().update(me.mytpl.applyTemplate(dta));
    }
});

Ext.reg('repoComments-view', eBook.Repository.RepoCommentsView);

eBook.Repository.RepoCommentsAdd = Ext.extend(Ext.Container, {

    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            /* store: new Ext.data.JsonStore({
            fields: [{ name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'PersonId', mapping: 'pid', type: Ext.data.Types.STRING }
            , { name: 'PersonName', mapping: 'pn', type: Ext.data.Types.STRING }
            , { name: 'Comment', mapping: 'cmt', type: Ext.data.Types.STRING }
            , { name: 'PostDate', mapping: 'pd', type: Ext.data.Types.WCFDATE }
            ],
            data: [{ Id: 'test-1', PersonId: 'Pers-1', PersonName: 'Test Person', Comment: 'rgjrogjzrogjzrogjreogjg g rg gzjerg', PostDate: "/Date(1418379755867+0100)/"}]
            }),*/
            // html: 'I can add here'
            items: [{ xtype: 'textarea', ref: 'textar', width: '100%' }, { xtype: 'button', text: 'Add', handler: me.onAddClick, scope: me}]

        });
        eBook.Repository.RepoCommentsAdd.superclass.initComponent.apply(this, arguments);
    }
    , onAddClick: function (e, evt) {
        var me = this,
            txt = me.textar.getValue(),
            owner = me.refOwner;
        if (Ext.isEmpty(txt)) return;
        me.textar.setValue('');
        owner.addComment(txt);

    }
    , setData: function (dta) {

    }
});

Ext.reg('repoComments-add', eBook.Repository.RepoCommentsAdd);


eBook.Repository.RepoComments = Ext.extend(Ext.Container, {

    initComponent: function () {

        Ext.apply(this, {
            cls: 'eBook-repo-comments-cont',
            items: [{ xtype: 'repoComments-add', height: 100, ref: 'commentAdder', width: '100%' }
                , { xtype: 'repoComments-view', ref: 'commentViewer', width: '100%' }
            ]
        });
        eBook.Repository.RepoComments.superclass.initComponent.apply(this, arguments);

    }
    , myData: []
    , setValue: function (dta) {
        var me = this;
        if (!Ext.isArray(dta)) {
            dta = [dta];
        }
        me.myData = dta;
        me.commentViewer.setData(dta);
        // me.getEl().update(me.mytpl.applyTemplate(dta));
    }
    , getValue: function () {
        return this.myData;
    }
    , addComment: function (txtComment) {
        var me = this,
             obj = { PersonId: eBook.User.personId, PersonName: eBook.User.fullName, Comment: txtComment, PostDate: new Date(), PostDateString: Ext.util.Format.date(new Date(), 'd/m/Y H:i:s') };
        obj.status = me.myOwner.refOwner.fileDetails.statuslist.getRecord().data;
        me.myData.unshift(obj);
        me.setValue(me.myData);

        //          data: [{ Id: 'test-1', PersonId: 'Pers-1', PersonName: 'Test Person', Comment: 'rgjrogjzrogjzrogjreogjg g rg gzjerg', PostDate: "/Date(1418379755867+0100)/"}]

    },
    isValid: function () {
        return true;
    }
});

Ext.reg('repoComments', eBook.Repository.RepoComments);



eBook.Fields.ReposRadioGroup = Ext.extend(Ext.form.RadioGroup, {
    initComponent: function() {
        var itsVals = [];
        var its = [];
        for (var i = 0; i < this.fldAttributes.length; i++) {
            if (this.fldAttributes[i].k == eBook.Interface.Culture) {
                var itsVals = eval(this.fldAttributes[i].v);
                break;
            }
        }
        for (var i = 0; i < itsVals.length; i++) {
            var arr = itsVals[i];
            its.push({ name: this.name + '_rd', width: 200, boxLabel: arr[0], inputValue: arr[1], checked: i == 0
                , listeners: { check: { fn: this.onRadioCheck, scope: this} }
            });
        }
        Ext.apply(this, {
            items: its
            , columns: 1
            , width: 250
        });
        eBook.Fields.ReposRadioGroup.superclass.initComponent.apply(this, arguments);
    }
    , onRadioCheck: function(fld, chk) {
        if (chk) this.fireEvent('change', this, this);
    }
    , getValue: function() {
        var rd = eBook.Fields.ReposRadioGroup.superclass.getValue.call(this);
        if (rd == null) return null;
        return rd.inputValue;
    }
});
Ext.reg('rpradiogroup', eBook.Fields.ReposRadioGroup);

eBook.Fields.LocalInputList = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        var lst = this.attributes[0].v;
        for (var i = 0; i < this.attributes.length; i++) {
            if (this.attributes[i].k == eBook.Interface.Culture) {
                lst = this.attributes[i].v;
            }
        }
        lst = Ext.util.JSON.decode(lst);

        Ext.apply(this, {
            store: lst
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , minChars: 1
            , autLoad: true
            , hideTrigger: true
            , typeAhead: false
            , lazyRender: false
            , forceSelect: false
            , listWidth: 300
            , triggerAction: 'all'
            , selectOnFocus: false
            , disableKeyFilter :true
            , listeners: {
                focus: { fn: function(gr) {
                    if (!gr.list) gr.initList();

                    gr.expand();
                    gr.doQuery('',true);// .clearFilter();
                }, scope: this
                }
            }
        });
        eBook.Fields.LocalInputList.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('localinputlist', eBook.Fields.LocalInputList);


eBook.Repository.FileDetailsWindow = Ext.extend(eBook.Window, {

    readOnly: false,
    readOnlyMeta: false,
    initComponent: function () {
        Ext.apply(this, { width: 607, modal: true, layout: 'fit', items: [new eBook.Repository.FileDetailsPanel({ ref: 'detailPanel', repositoryItemId: this.repositoryItemId, readOnly: this.readOnly, readOnlyMeta: this.readOnlyMeta })]
        });
        if (!this.readOnly || !this.readOnlyMeta) {
            Ext.apply(this, {
                bbar: ['->', {
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onSaveClick,
                    scope: this
                }
            ]
            , tbar: [{
                ref: '../copyItem',
                iconCls: 'eBook-window-copy-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onCopyClick,
                scope: this
            }, {
                ref: '../moveItem',
                iconCls: 'eBook-window-move-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onMoveClick,
                scope: this
            }, {
                ref: '../replaceItem',
                iconCls: 'eBook-window-move-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onReplaceClick,
                scope: this
            }]
            });
        }

        eBook.Repository.FileDetailsWindow.superclass.initComponent.apply(this, arguments);
    }
    , onCopyClick: function () {
        eBook.Interface.center.clientMenu.repository.onCopyItem();
        this.close();
    }
     , onMoveClick: function () {
         eBook.Interface.center.clientMenu.repository.onMoveItem();
         this.close();
     }
     , onReplaceClick: function () {
         var wn = new eBook.Repository.SingleUploadWindow({ existingItemId: this.repositoryItemId });
         wn.show();
         this.close();
     }
    , loadById: function () {
        this.getEl().mask("Loading", 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.repository + 'GetFileNode'
            , method: 'POST'
            , params: Ext.encode({
                cicdc: {
                    Culture: eBook.Interface.currentFile ? eBook.Interface.currentFile.get('Culture') : eBook.Interface.Culture
                     , Id: this.repositoryItemId
                }
            })
            , callback: this.onLoadedById
            , scope: this
        });
    }
    , onLoadedById: function (opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            this.loadExistingFile({ attributes: robj.GetFileNodeResult });
            this.detailPanel.tbpanel.setActiveTab(2);
        } else {
            this.getEl().unmask();
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , loadExistingFile: function (fobj) {
        if (this.tpe == 'COPY') {
            this.isNew = true;

        } else {
            this.isNew = false;
        }
        if (this.tpe == 'COPY' || this.tpe == 'MOVE') this.getTopToolbar().hide();
        this.detailPanel.loadExistingFile(fobj, this.tpe);
    }
    , loadNewFile: function (fobj) {
        this.getTopToolbar().hide();
        this.isNew = true;
        this.detailPanel.loadNewFile(fobj, this.standards);
    }
    , onSaveClick: function () {
        var sid = this.detailPanel.fileDetails.locationTree.getValue();
        if (Ext.isEmpty(sid)) {
            this.detailPanel.fileDetails.locationTree.markInvalid();
            return;
        }
        this.getEl().mask("Saving", 'x-mask-loading');
        if (this.detailPanel.metaPanel.validateData()) {
            var mtaData = this.detailPanel.metaPanel.getMetaData();
            var o = { IsNew: this.isNew
                , Item: {
                    ClientId: this.clientId ? this.clientId : eBook.Interface.currentClient.get('Id')
                    , StructureId: this.detailPanel.fileDetails.locationTree.getValue()
                    , Id: this.detailPanel.fileDetails.fileId
                    , PhysicalFileId: this.detailPanel.fileDetails.physFileId
                    , FileName: this.detailPanel.fileDetails.fileName.getValue()
                    , Extension: this.detailPanel.fileDetails.extension
                    , ContentType: this.detailPanel.fileDetails.contentType
                    , Meta: mtaData
                    , PeriodStart: this.detailPanel.metaPanel.meta.periodStart
                    , PeriodEnd: this.detailPanel.metaPanel.meta.periodEnd
                    , TimeZoneOffset: this.detailPanel.metaPanel.meta.periodEnd.getTimezoneOffset()
                    , Prequel: this.detailPanel.metaPanel.meta.preQuel == null ? '' : this.detailPanel.metaPanel.meta.preQuel
                    , Status: this.detailPanel.fileDetails.statuslist.getValue()
                }
            };
            //if (Ext.isEmpty(o.Item.Status)) o.Item.Status = null;
            if (!this.detailPanel.fileDetails.statuslist.isVisible()) { o.Item.Status = null; }
            else if (Ext.isEmpty(o.Item.Status)) { this.detailPanel.fileDetails.statuslist.markInvalid(); this.getEl().unmask(); return; }
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'SaveItem',
                callback: this.onSaveCallback,
                scope: this,
                params: Ext.encode({ csridc: o })
            });
        } else {
            this.getEl().unmask();
        }


    }
    , onSaveCallback: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            //if the parent caller isn't defined, refresh repository tree
            if (!this.parentCaller) eBook.Interface.center.clientMenu.repository.reload();


            if (this.detailPanel.fileDetails.statuslist.getValue() == "b50ff824-3ccf-4ba5-b628-f8dbf6cd2b78") { //I/f status == To be send to client DS 
                if (this.detailPanel.fileDetails.extension == ".pdf") {
                    var o = { Id: eBook.Interface.currentClient.id, Department: "ACR" };
                    Ext.Ajax.request({
                        method: 'POST',
                        url: eBook.Service.client + 'GetClientPartnerByDepartment',
                        callback: this.onGetClientPartnerByDepartmentCallback,
                        scope: this,
                        params: Ext.encode({ ciddc: o })
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'You can only upload PDF files to docstore',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        scope: this
                    });
                }
            }

            /* GTH */
            var Team6Repo = Ext.getCmp('GthTeam6Repository');
            if (Team6Repo != null) {
                Team6Repo.refOwner.loadRepo();
            }

            if (this.readycallback) {

                if(resp.responseText)
                {
                    var scope = this.readycallback.scope,
                        repositoryItem = Ext.decode(resp.responseText).SaveItemResult,
                        status = '',
                        statusId = this.detailPanel.fileDetails.statuslist.getValue();

                    //Check if caller is specified else check argument length (to be improved)
                    if(this.parentCaller && this.parentCaller.ref)
                    {
                        if(this.parentCaller.ref.indexOf("bundleEditWindow") != -1) {
                            //A certain step within the year end bundle flow requires a file upload and is then followed by a success callback function
                            this.readycallback.fn.call(scope, this.readycallback.required, this.readycallback.bundle, this.readycallback.fileId, this.readycallback.serviceId, this.readycallback.statusId, repositoryItem, this.readycallback.filename);
                        }
                        if(this.parentCaller.ref.indexOf("reviewNotes") != -1 || this.parentCaller.ref.indexOf("eBook-GTH-Team5") != -1)
                        {
                            this.readycallback.fn.call(scope,repositoryItem);
                        }
                        if(this.parentCaller.ref.indexOf("library") != -1)
                        {
                            this.readycallback.fn.call(scope);
                        }
                        if(this.parentCaller.ref.indexOf("reportPOAgrid") != -1)
                        {
                            this.readycallback.fn.call(scope, this.readycallback.record, repositoryItem, statusId, status);
                        }
                    }
                    else {
                        if(this.readycallback.callerCallback) {
                            this.readycallback.fn.call(scope,this.readycallback.callerCallback,this.readycallback.callerCallbackScope);
                        }
                        else {
                            this.readycallback.fn.call(scope);
                        }
                    }
                }
                else
                {
                    eBook.Interface.showResponseError(resp, this.title);
                }
            }
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onGetClientPartnerByDepartmentCallback: function (opts, success, resp) {
        //var wn = new eBook.Docstore.Window({ documentId: this.detailPanel.activeNode.attributes.Item.Id, clientId: this.detailPanel.activeNode.attributes.Item.ClientId, culture: eBook.Interface.Culture, partnerGpn: this.detailPanel.activeNode.attributes.Item.PartnerGpnAcr });
        var wn = new eBook.Docstore.Window({ documentId: opts.scope.repositoryItemId, clientId: eBook.Interface.currentClient.id, culture: eBook.Interface.Culture, partnerGpn: Ext.decode(resp.responseText).GetClientPartnerByDepartmentResult[0].gpn, uploaderGpn: eBook.User.gpn });

        wn.show();
    }
    , show: function (caller) {
        this.parentCaller = caller;
        eBook.Repository.FileDetailsWindow.superclass.show.call(this);
        if (this.tpe == "MOVE") {
            Ext.Msg.alert("Move file", "<b>Warning:</b><br/>Moving a file can result in loss of metadata (if present).<br/>To cancel, exit the window without saving.");
        }
    }
});


eBook.Repository.FileDetailsPanel = Ext.extend(Ext.Panel, {
    readOnly: false,
    readOnlyMeta: true,
    initComponent: function () {
        Ext.apply(this, {
            layout: 'border'
            , items: [
                        { xtype: 'panel', layout: 'form', region: 'north', height: 100, ref: 'fileDetails'
                            , bodyStyle: 'padding:10px;', readOnly: this.readOnly
                            , items: [
                                { xtype: 'textfield', maxLength: 250, fieldLabel: eBook.Repository.FileMetaPanel_FileName
                                    , ref: 'fileName', width: 420, disabled: this.readOnly
                                }
                                , { xtype: 'TreeSelect'
                                    , ref: 'locationTree'
                                    , width: 420
                                    , fieldLabel: eBook.Repository.FileMetaPanel_Location
						            , listWidth: 420
						            , lazyInit: false
						            , rootVisible: true
						            , loader: new eBook.Repository.TreeLoader({
						                dataUrl: eBook.Service.repository + 'GetChildren',
						                requestMethod: "POST",
						                showFiles: false,
						                selectionMode: true,
						                preloadChildren: true,
						                noLoad: true
						            })
						            , typeAhead: false
                                    , selectOnFocus: true
                                    , editable: false
                                    , forceSelection: true
                                    , disabled: this.readOnly
                                    , root: {
                                        nodeType: 'async',
                                        text: 'Repository',
                                        draggable: false,
                                        id: 'root',
                                        children: eBook.Interface.GetSelectionRepoStructure(),
                                        expanded: true
                                    }
                                    , listeners: {
                                        select: { fn: this.onSelect, scope: this }
                                    }

                                }, { xtype: 'combo'
                                , store: new eBook.data.JsonStore({
                                    selectAction: 'GetStatusList'
                                    , criteriaParameter: 'csdc'
                                    , autoDestroy: true
                                    , serviceUrl: eBook.Service.repository
                                    , storeParams: {}
                                    , disabled: this.readOnly
                                    , fields: eBook.data.RecordTypes.GlobalListItem
                                    , autoLoad: false
                                })
                                , fieldLabel: eBook.Repository.FileMetaPanel_Status
                                , allowBlank: false
                                , ref: 'statuslist'
                                , typeAhead: false
                                , mode: 'local'
                                , selectOnFocus: true
                                , autoSelect: true
                                , editable: false
                                , forceSelection: true
                                , triggerAction: 'all'
                                , nullable: false
                                , width: 200
                                , listWidth: 200
                                , displayField: eBook.Interface.Culture.substr(0, 2)
                                , valueField: 'id'
                                , hidden: true
                                , listeners: { select: { fn: this.onStatusChange, scope: this }
                                    //, disabled: this.readOnly
                                , disabled: this.readOnlyMeta == false ? this.readOnlyMeta : this.readOnly
                                , expand: { fn: this.onComboExpand, scope: this }
                                }
                                }]
                        }
                        , { xtype: 'tabpanel', ref: 'tbpanel', items: [
                                { xtype: 'reposFileMeta', readOnly: this.readOnly, readOnlyMeta: this.readOnlyMeta, autoScroll: true, title: eBook.Repository.FileDetailsPanel_MetaTitle, region: 'center', ref: '../metaPanel' }
                                , { xtype: 'RepositoryItemLinks', readOnly: this.readOnly, title: eBook.Repository.FileDetailsPanel_LinksTitle, ref: '../repolinks', repositoryItemId: this.repositoryItemId }
                                , { xtype: 'RepositoryPreview', ref: '../previewPanel', title: eBook.Repository.FileDetailsPanel_PreviewTitle}]
                            , region: 'center', activeTab: 0
                        }
                ]
        });
        eBook.Repository.FileDetailsPanel.superclass.initComponent.apply(this, arguments);
    }
    , onComboExpand: function (combo) {

        // Disable some statusses
        this.fileDetails.statuslist.store.filterBy(function (rec, id) {
            if (id != 'c0009211-8936-49f0-a2b4-e0f70a0a2a96' //Sent to DocStore
                && id != 'd54bcd3c-6b1f-46b5-b0c4-d66d307a7869' //Sent to client DS
                && id != 'eb72b2a6-8862-4544-ac7a-75c99a166ea4') { //Signed by client DS
                return rec;
            }
        }, this);
    }
    , onStatusChange: function (el) {
        var val = el.getValue();
        this.metaPanel.onStatusChange(val);
    }
    , onSelect: function (treeSel) {
        if (treeSel.selectedNode.attributes.Key == 'ENGAGEMENT') {
            this.treeSel = treeSel;
            this.userHasGTHrole = false;
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'PersonHasGTHrole',
                callback: this.onPersonHasGTHroleCallback,
                scope: this,
                params: Ext.encode({ cidc: { Id: eBook.User.personId} })
            });
        } else {
            this.onSelectProcess(treeSel);
        }

    }
    , onSelectProcess: function (treeSel) {
        if (treeSel.selectedNode) {
            this.fileDetails.statuslist.clearValue();
            if (!Ext.isEmpty(treeSel.selectedNode.attributes.StatusType)) {
                this.fileDetails.statuslist.store.baseParams.id = treeSel.selectedNode.attributes.StatusType;
                this.fileDetails.statuslist.store.load();
                this.fileDetails.statuslist.show();
            } else {
                this.fileDetails.statuslist.hide();
            }
            this.repolinks.allowDefaultLinking(treeSel.selectedNode.attributes.AllowLinks);
            var mc = { flds: [] };
            if (treeSel.selectedNode.attributes.MetaConfig != null) {
                if (treeSel.selectedNode.attributes.MetaConfig.ft == "CLIENTBANK") {
                    Ext.Ajax.request({
                        method: 'POST',
                        url: eBook.Service.client + 'GetClientExactBanks',
                        callback: this.loadExactMeta,
                        scope: this,
                        params: Ext.encode({ cidc: { Key: eBook.Interface.currentClient.get('EnterpriseNumber'), Id: eBook.Interface.currentClient.get('Id')} })
                    });
                } /*else if (treeSel.selectedNode.attributes.MetaConfig.ft == "") {
                   

                } */
                else {
                    mc = treeSel.selectedNode.attributes.MetaConfig;
                    this.metaPanel.loadConfig(mc, treeSel.selectedNode);
                }
            } else {
                this.metaPanel.loadConfig(mc, treeSel.selectedNode);
            }

        }
    }

    , loadExactMeta: function (opts, success, resp) {
        if (success) {
            var mc = { flds: [] };


            this.metaPanel.loadConfig(Ext.decode(resp.responseText).GetClientExactBanksResult, this.fileDetails.locationTree.selectedNode);
        }
    }
    , onPersonHasGTHroleCallback: function (opts, success, resp) {
        if (success) {
            if (Ext.decode(resp.responseText).PersonHasGTHroleResult == true) {
                this.userHasGTHrole = true;
            } else {
                this.userHasGTHrole = false;
            }
        }
        this.onSelectProcess(this.treeSel)
    }
    , loadExistingFile: function (n, nw) {
        // TODO
        if (!nw) this.fileDetails.locationTree.disable();
        this.activeNode = n;
        this.ownerCt.getEl().mask("Loading", 'x-mask-loading');
        var fobj = n.attributes.Item;
        this.fileDetails.fileId = fobj.Id;
        this.fileDetails.physFileId = fobj.PhysicalFileId;
        this.fileDetails.fileName.setValue(fobj.FileName);
        this.fileDetails.extension = fobj.Extension;
        this.fileDetails.contentType = fobj.ContentType;
        this.fileDetails.statuslist.clearValue();
        this.fileDetails.locationTree.clearValue();
        this.fileDetails.locationTree.setValue(fobj.StructureId); // bank is unknown & dynamic.
        if (!this.fileDetails.locationTree.selectedNode) {
            this.fileDetails.locationTree.hide();
        }
        this.continueLoadExisting(this.fileDetails.locationTree.selectedNode ? this.fileDetails.locationTree.selectedNode : n.parentNode, true);

    }
    , continueLoadExisting: function (nde, success) {
        if (success) {
            if (!Ext.isEmpty(this.activeNode.attributes.Item.Status)) {
                this.fileDetails.statuslist.disable(this.readOnlyMeta == false ? this.readOnlyMeta : this.readOnly);
                this.onSelect({ selectedNode: nde });
                this.fileDetails.statuslist.store.on('load', function (st, rcs, opts) {
                    this.continueLoadExistingDefered.defer(500, this, [nde]);
                }, this, { single: true, scope: this });
                this.fileDetails.statuslist.store.load();

            } else {
                if (nde) this.onSelect({ selectedNode: nde });
                this.metaPanel.loadItem(this.activeNode.attributes.Item);
                this.previewPanel.loadFile("Repository/" + eBook.getGuidPath(this.activeNode.attributes.Item.ClientId) + this.activeNode.attributes.Item.PhysicalFileId + this.activeNode.attributes.Item.Extension);
                if (this.readOnly) {
                    //this.metaPanel.disable();
                    this.tbpanel.setActiveTab(2);
                }

                this.ownerCt.getEl().unmask();
            }

            //            if (this.fileDetails.statuslist.isVisible()) this.fileDetails.statuslist.store.load();
            //            this.fileDetails.statuslist.setValue(this.activeNode.attributes.Item.Status);

            //if (this.fileDetails.statuslist.isVisible()) 

        } else {
            alert("Failed loading the tree");
            this.ownerCt.getEl().unmask();
        }

    }
    , continueLoadExistingDefered: function (nde) {

        !this.readOnly ? this.fileDetails.statuslist.enable() : null;
        !this.readOnlyMeta ? this.fileDetails.statuslist.enable() : null;

        this.fileDetails.statuslist.setValue(this.activeNode.attributes.Item.Status);
        this.onStatusChange(this.fileDetails.statuslist);
        this.metaPanel.loadItem(this.activeNode.attributes.Item);
        this.previewPanel.loadFile("Repository/" + eBook.getGuidPath(this.activeNode.attributes.Item.ClientId) + this.activeNode.attributes.Item.PhysicalFileId + ".pdf");
        if (this.readOnly) {
            //this.metaPanel.disable();
            this.tbpanel.setActiveTab(2);
        }
        this.ownerCt.getEl().unmask();
    }
    , loadNewFile: function (fobj, standards) {
        this.fileDetails.locationTree.enable();
        this.fileDetails.fileId = fobj.id;
        this.fileDetails.physFileId = fobj.id;
        this.fileDetails.fileName.setValue(fobj.oldName);
        this.fileDetails.extension = fobj.extension;
        this.fileDetails.contentType = fobj.contentType;
        this.fileDetails.statuslist.clearValue();
        this.fileDetails.locationTree.clearValue();
        this.metaPanel.loadConfig({ flds: [] }, null);
        if (standards)
        {
            //When a location given, lock location tree and utilize this as repository structure location
            //this.detailPanel.fileDetails.fileName
            if(!Ext.isEmpty(standards.location)) {
                this.fileDetails.locationTree.setValue(standards.location);
                this.fileDetails.locationTree.disable();
                this.onSelect({selectedNode: this.fileDetails.locationTree.selectedNode});
                //What status to set
                if (!Ext.isEmpty(standards.statusid)) {
                    this.fileDetails.statuslist.store.on('load', function (st, rcs, opts) {
                        this.fileDetails.statuslist.setValue(standards.statusid);
                        this.fileDetails.statuslist.disable();
                    }, this, {single: true, scope: this});
                    this.fileDetails.statuslist.store.load();

                }
            }

            //When filename given, set this as default filename
            if(!Ext.isEmpty(standards.fileName))
            {
                this.fileDetails.fileName.setValue(standards.fileName);
                this.fileDetails.fileName.disable();
            }

            //Periood might be given as well and utilized at saveItem function
        }

        if (fobj.extension == ".pdf") {
            this.previewPanel.loadFile("upload/" + fobj.id + fobj.extension);
        } else {
            // not a pdf... can't preview ?
            //this.previewPanel
        }
    }

    , getData: function () {

    }
});



eBook.Repository.FileMetaPanel = Ext.extend(Ext.Panel, {
    cfgTpl: '<tpl for="."><div><div class="eb-repos-filemeta-field" fieldId="_PERIOD"><div class="eb-repos-filemeta-field-label"></div><div class="eb-repos-filemeta-field-item"></div></div><tpl for="flds"><div class="eb-repos-filemeta-field" fieldId="{Id}"><div class="eb-repos-filemeta-field-label"></div><div class="eb-repos-filemeta-field-item"></div></div></tpl></div></tpl>'
    , initComponent: function () {
        Ext.apply(this, {
            html: '<div>Select category</div>'
            , frame: false
        });
        this.cfgTpl = new Ext.XTemplate(this.cfgTpl);
        this.cfgTpl.compile();
        eBook.Repository.FileMetaPanel.superclass.initComponent.apply(this, arguments);
    }
    , fields: []
    , periodFields: []
    , metaConfig: {}
    , periodType: 'PERIOD'
    , categoryPeriod: {
        start: null
        , end: null
    }
    , clearFields: function () {
        for (var i = 0; i < this.fields.length; i++) {
            this.fields[i].destroy();
        }
        this.fields = [];
        for (var i = 0; i < this.periodFields.length; i++) {
            this.periodFields[i].destroy();
        }
        this.periodFields = [];
    }
    , meta: {//set period supplied through standards object or set null
        periodStart: this.standards && this.standards.periodStart  ? Ext.data.Types.WCFDATE.convert(this.standards.periodStart) : null
        , periodEnd: this.standards && this.standards.periodEnd ? Ext.data.Types.WCFDATE.convert(this.standards.periodEnd) : null
        , preQuel: ''
    }
    , loadItem: function (item) {
        this.loadMeta(item.Meta);
        this.meta.preQuel = item.preQuel || '';
        this.meta.periodStart = Ext.data.Types.WCFDATE.convert(item.PeriodStart);
        this.meta.periodEnd = Ext.data.Types.WCFDATE.convert(item.PeriodEnd);
        switch (this.periodType) {
            case "YEAR":
                this.periodFields[0].setValue(this.meta.periodStart.getFullYear());
                break;
            case "QUARTERMONTH":

                if (monthDiff(this.meta.periodStart, this.meta.periodEnd) > 1) {
                    this.periodFields[0].setValue('Q');
                    var q = (this.meta.periodEnd.getMonth() + 1) / 3;

                    this.periodFields[1].setValue('Q0' + q + '/' + this.meta.periodEnd.getFullYear())
                } else {
                    this.periodFields[0].setValue('M');
                    var m = (this.meta.periodEnd.getMonth() + 1);
                    if (m < 10) m = '0' + m;
                    this.periodFields[1].setValue(m + '/' + this.meta.periodEnd.getFullYear())
                }
                break;
            default:
                if (this.periodFields && this.periodFields.length > 0) {
                    this.periodFields[0].setValue(this.meta.periodStart);
                    this.periodFields[1].setValue(this.meta.periodEnd);
                    var el = this.periodFields[1];
                    if (el.fiscalYearEl) {
                        if (this.meta.periodEnd) {
                            var fy = new Date(this.meta.periodEnd.getFullYear, this.meta.periodEnd.getMonth(), this.meta.periodEnd.getDate());
                            fy.setDate(fy.getDate() + 1);
                            fy = fy.getFullYear();
                            el.fiscalYearEl.update(fy + ' &gt; ');
                        } else {
                            el.fiscalYearEl.update('&nbsp;');
                        }
                    }
                }
                break;
        }
        if (this.readOnly) {
            if (this.periodFields[0]) this.periodFields[0].disable();
            if (this.periodFields[1]) this.periodFields[1].disable();
        }
        if (this.readOnlyMeta) {
            for (var i = 0; i < this.fields.length; i++) {
                this.fields[i].disable();
            }
        }
    }
    , loadMeta: function (meta) {
        if (meta == null) return;
        for (var i = 0; i < meta.length; i++) {
            var idx = this.fieldIdx[meta[i].Id];
            if (Ext.isDefined(idx) && idx != null) {
                // check for date.
                var fld = this.fields[idx];
                var val = meta[i].Value;
                //if (fld.xtype == "datefield")

                if (fld.xtype == "datefield" && Ext.isString(val)) {
                    var dte = Date.parseDate(val, "j/n/Y"); // fix for non-leading zeroes: j(day)/n(month)
                    fld.setValue(dte);

                } else if (fld.xtype == "ewList_Global") {
                    try {
                        val = Ext.util.JSON.decode(val);
                    } catch (e) { }
                    fld.setActiveRecord(val);
                } if (fld.xtype == "repoComments" && Ext.isString(val)) {
                    fld.setValue(Ext.util.JSON.decode(val));
                } else {
                    fld.setValue(val);

                }
            }
        }
    }
    , validateData: function () {
        var hasValues = false;
        for (var i = 0; i < this.periodFields.length; i++) {
            if (!this.periodFields[i].isValid()) return false;
        }

        if (this.fields.length == 0 || !this.feature == "CLIENTBANK") hasValues = true;

        for (var i = 0; i < this.fields.length; i++) {
            if (!this.fields[i].isValid() && this.fields[i].isVisible()) return false;
            if (!hasValues) {
                if (Ext.isBoolean(this.fields[i].getValue())) {
                    hasValues = this.fields[i].getValue();
                } else {
                    hasValues = !Ext.isEmpty(this.fields[i].getValue());
                }
            }
        }


        return hasValues;
    }
    , getMetaData: function () {
        var o = [], i;
        for (i = 0; i < this.fields.length; i++) {
            if (this.fields[i].isMetaField) {
                // check for date.
                val = this.fields[i].getValue();
                if (this.fields[i].xtype == "datefield") {
                    val = val.format('d/m/Y');
                } else if (this.fields[i].xtype == "ewList_Global") {
                    val = Ext.util.JSON.encode(val);
                } if (this.fields[i].xtype == "repoComments") {
                    val = Ext.util.JSON.encode(val);
                } else {
                    if (val != null) val = val.toString();
                }
                o.push({ Id: this.fields[i].name, Value: val });
            }
        }

        //update periods
        for (i = 0; i < this.periodFields.length; i++) {
            this.periodChange(this.periodFields[i], null, null);
        }

        return o;
    }
    , periodChange: function (el, nw, old) {
        this.meta.preQuel = '';
        switch (el.periodType) {
            case "YEAR":
                this.meta.periodStart = new Date(el.getValue(), 0, 1);
                this.meta.periodEnd = new Date(el.getValue(), 11, 31);
                break;
            case "QUARTERMONTH_TYPE":
                var val = el.getValue();
                var fldVal = this.periodFields[1];
                fldVal.store.baseParams.qm = el.getValue();
                fldVal.clearValue();
                fldVal.store.load();
                break;
            case "QUARTERMONTH_VALUE":
                var val = el.getValue();
                var endM, endY;
                //var fldVal = this.fields[el.linkedFldIdx];
                if (!Ext.isEmpty(val)) {
                    if (val.substr(0, 1) == 'Q') {
                        val = val.substr(1, 7).split('/');
                        endM = (parseInt(val[0]) * 3) - 1; // javascript months: 0-11
                        endY = parseInt(val[1]);
                        this.meta.periodStart = new Date(endY, (endM - 2), 1);
                        this.meta.periodEnd = new Date(endY, (endM + 1), 1);
                        this.meta.periodEnd.setDate(this.meta.periodEnd.getDate() - 1);
                    } else {
                        var sval = val.split('/');
                        endM = (parseInt(sval[0])) - 1; // javascript months: 0-11
                        endY = parseInt(sval[1]);
                        this.meta.periodStart = new Date(endY, (endM), 1);
                        this.meta.periodEnd = new Date(endY, (endM + 1), 1);
                        this.meta.periodEnd.setDate(this.meta.periodEnd.getDate() - 1);

                    }
                    this.meta.preQuel = val;
                }
                break;
            default:
                if (el.name == "StartDate") {
                    this.meta.periodStart = el.getValue();
                }
                if (el.name == "EndDate") {
                    this.meta.periodEnd = el.getValue();
                    if (Ext.isEmpty(this.meta.periodEnd)) {
                        this.meta.periodEnd = new Date(2100, 11, 31);
                    }
                }
                if (el.fiscalYearEl) {
                    if (this.meta.periodEnd) {
                        var fy = new Date(this.meta.periodEnd.getFullYear, this.meta.periodEnd.getMonth(), this.meta.periodEnd.getDate());
                        fy.setDate(fy.getDate() + 1);
                        fy = fy.getFullYear();
                        el.fiscalYearEl.update(fy + ' &gt; ');
                    } else {
                        el.fiscalYearEl.update('&nbsp;');
                    }
                }
                break;
        }

        if(this.periodFields[0].isValid(false)) {
            this.setDefaultEndDate(this.periodFields[0]);
        }
    }
    , renderPeriod: function (fel, nde) {
        fel = Ext.get(fel);
        var lel = fel.child('.eb-repos-filemeta-field-label');
        var ffel = fel.child('.eb-repos-filemeta-field-item');

        var ps = this.categoryPeriod ? this.categoryPeriod.start : null;
        var pe = this.categoryPeriod ? this.categoryPeriod.end : null;
        var fyEl = null;

        lel.update(eBook.Repository['FileMetaPanel_' + nde.attributes.DateType]);
        switch (nde.attributes.DateType) {
            case "CALENDERYEAR":
            case "YEAR":
                var fld = new Ext.form.ComboBox({
                    store: new eBook.data.JsonStore({
                        selectAction: 'GetYearsInt'
                        , criteriaParameter: 'cpydc'
                        , autoDestroy: true
                        , serviceUrl: eBook.Service.repository
                        , storeParams: { ps: ps, pe: pe }
                        , fields: eBook.data.RecordTypes.YearList
                        , autoLoad: true
                    })
                    , allowBlank: false
                    , renderTo: ffel
                    , typeAhead: false
                    , mode: 'local'
                    , selectOnFocus: true
                    , autoSelect: true
                    , editable: false
                    , forceSelection: true
                    , triggerAction: 'all'
                    , nullable: false
                    , width: 100
                    , listWidth: 200
                    , displayField: 'Id'
                    , valueField: 'Id'
                    , value: pe != null ? pe.getFullYear() : (new Date()).getFullYear()
                    , periodType: nde.attributes.DateType
                    , listeners: {
                        select: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                break;
            case "QUARTERMONTH":
                // ffel.createChild({ tag: 'span', html: 'Periodtype ' });
                var cbCfg = {
                    store: [['M', eBook.Repository.FileMetaPanel_MONTHLY], ['Q', eBook.Repository.FileMetaPanel_QUARTERLY]]
                    , allowBlank: false
                    , renderTo: ffel
                    , typeAhead: false
                    , mode: 'local'
                    , selectOnFocus: true
                    , autoSelect: true
                    , editable: false
                    , forceSelection: true
                    , triggerAction: 'all'
                    , nullable: false
                    , width: 100
                    , listWidth: 200
                    , periodType: "QUARTERMONTH_TYPE"
                    , linkedFldIdx: this.fields.length + 1
                    , listeners: {
                        select: { fn: this.periodChange, scope: this }
                    }
                };
                var perVal = null;
                cbCfg.value = 'M';
                if (this.meta.periodStart != null && this.meta.periodEnd != null) {

                    perVal = '' + (this.meta.periodStart.getMonth() + 1);
                    if (perVal.length == 1) perVal = '0' + perVal;

                    if (monthDiff(this.meta.periodStart, this.meta.periodEnd) > 1) {
                        cbCfg.value = 'Q';
                        sval = 'Q';
                        perVal = "Q0" + ((this.meta.periodEnd.getMonth() + 1) / 3);
                    }
                    perVal += '/' + this.meta.periodStart.getFullYear();
                }
                var fld = new Ext.form.ComboBox(cbCfg);
                this.periodFields.push(fld);
                // ffel.createChild({ tag: 'span', html: ' Period ' });
                var fcfg2 = {
                    store: new eBook.data.JsonStore({
                        selectAction: 'GetQuarterOrMonthList'
                            , criteriaParameter: 'cqmdc'
                            , autoDestroy: true
                            , serviceUrl: eBook.Service.repository
                            , baseParams: { qm: cbCfg.value, ps: ps, pe: pe }
                            , fields: eBook.data.RecordTypes.MonthList
                            , autoLoad: true
                    })
                    , periodType: "QUARTERMONTH_VALUE"
                    , allowBlank: false
                    , renderTo: ffel
                    , typeAhead: false
                    , mode: 'local'
                    , selectOnFocus: true
                    , autoSelect: true
                    , editable: false
                    , forceSelection: true
                    , triggerAction: 'all'
                    , nullable: false
                    , width: 100
                    , listWidth: 200
                    , displayField: 'Id'
                    , valueField: 'Id'
                    , linkedFldIdx: this.fields.length
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                    }
                };
                if (perVal) fcfg2.value = perVal;
                var fld2 = new Ext.form.ComboBox(fcfg2);
                this.periodFields.push(fld2);
                break;
            case "INFINITEPERIOD":
                //ffel.createChild({ tag: 'span', html: '' + eBook.Create.Empty.PreviousFileFieldSet_Startdate });
               
                var fld = new Ext.form.DateField({
                    allowBlank: false
                    , format: 'd/m/Y'
                    , name: 'StartDate'
                    , renderTo: ffel
                   // , hidden: true
                    , value: new Date()
                    , periodType: nde.attributes.DateType
                    , listeners: {
                       // change: { fn: this.periodChange, scope: this },
                       // valid: { fn: this.setDefaultEndDate, scope: this }
                    }
                });
                this.periodFields.push(fld);
               // ffel.createChild({ tag: 'span', html: ' <div class="x-clear"></div>' + eBook.Create.Empty.PreviousFileFieldSet_Enddate });

                var fld = new Ext.form.DateField({
                    allowBlank: true
                    , format: 'd/m/Y'
                    , name: 'EndDate'
                    //, hidden: true
                    , value: new Date(2099, 12, 31) // upon req. of Fred. Godinho
                    , renderTo: ffel
                    , periodType: nde.attributes.DateType
                    , listeners: {
                       // change: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                break;
            case "PERIOD":
                ffel.createChild({ tag: 'span', html: '' + eBook.Create.Empty.PreviousFileFieldSet_Startdate });

                var fld = new Ext.form.DateField({
                    allowBlank: false
                    , format: 'd/m/Y'
                    , name: 'StartDate'
                    , renderTo: ffel
                    , periodType: nde.attributes.DateType
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                        //valid: { fn: this.setDefaultEndDate, scope: this }
                    }
                });
                this.periodFields.push(fld);
                ffel.createChild({ tag: 'span', html: ' <div class="x-clear"></div>' + eBook.Create.Empty.PreviousFileFieldSet_Enddate });

                var fld = new Ext.form.DateField({
                    allowBlank: true
                    , format: 'd/m/Y'
                    , name: 'EndDate'
                    , renderTo: ffel
                    , periodType: nde.attributes.DateType
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                break;
            case "FISCALYEAR":
                var d = '';
                if (pe) {
                    d = new Date(pe.getFullYear(), pe.getMonth(), pe.getDate());
                    d.setDate(d.getDate() + 1)
                    d = d.getFullYear();
                }

                //ffel.createChild({ tag: 'span', html: d + ' &gt; ' });
                var par = ffel.parent().parent();
                par.createChild({ tag: 'div', cls: 'eb-repos-filemeta-field',
                    children: [{ tag: 'div', cls: 'eb-repos-filemeta-field-label', html: 'Aanslagjaar' }
                                             , { tag: 'div', cls: 'eb-repos-filemeta-field-item', html: d}]
                });


            case "BOOKYEAR":
                var fld = new Ext.form.DateField({
                    allowBlank: false
                    , format: 'd/m/Y'
                    , name: 'StartDate'
                    , renderTo: ffel
                    , value: ps
                    , disabled: this.categoryPeriod != null // nde.attributes.StructuralType == "FILE"
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                ffel.createChild({ tag: 'span', html: ' to ' });

                var fld = new Ext.form.DateField({
                    allowBlank: true
                    , format: 'd/m/Y'
                    , name: 'EndDate'
                    , fiscalYearEl: fyEl
                    , renderTo: ffel
                    , value: pe
                    , disabled: this.categoryPeriod != null //nde.attributes.StructuralType == "FILE"
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                if (nde.attributes.StructuralType == "FILE") {
                    this.meta.periodStart = ps;
                    this.meta.periodEnd = pe;
                }
                break;
        }

    }
    , fieldIdx: {}
    , statusChangeFields: []
    , renderField: function (fel, fcfg) {
        fel = Ext.get(fel);
        var lel = fel.child('.eb-repos-filemeta-field-label');
        var ffel = fel.child('.eb-repos-filemeta-field-item');
        var cfg = { xtype: '' };

        var clientSideVisibilitySource = false;
        var clientSideVisibilitySourceElements = null;
        var clientSideVisibilitySourceValues = null;
        var statusChange = [];

        for (var i = 0; i < fcfg.Attributes.length; i++) {
            switch (fcfg.Attributes[i].k) {
                case "CLIENT_VISIBILITY":
                    clientSideVisibilitySource = true;
                    break;
                case "CLIENT_VISIBILITY_TARGETS":
                    clientSideVisibilitySourceElements = Ext.util.JSON.decode(fcfg.Attributes[i].v);
                    break;
                case "CLIENT_VISIBILITY_VALUES":
                    clientSideVisibilitySourceValues = Ext.util.JSON.decode(fcfg.Attributes[i].v);
                    break;
                case "STATUS_VISIBILITY_VALUES":
                    statusChange = Ext.util.JSON.decode(fcfg.Attributes[i].v);
                    break;

            }
        }
        var listeners = {};
        lel.update(fcfg.Names[eBook.Interface.Culture.substr(0, 2)]);
        switch (fcfg.FieldType) {
            case "TextField":
                cfg = { xtype: 'textfield', name: fcfg.Id };
                break;
            case "Comments":
                cfg = { xtype: 'repoComments', name: fcfg.Id, myOwner: this };
                break;
            case "RadioGroup":
                cfg = { xtype: 'rpradiogroup', name: fcfg.Id };
                break;
            case "NumberField":
                cfg = { xtype: 'numberfield', name: fcfg.Id };
                break;
            case "EuroField":
                cfg = { xtype: 'numberfield', name: fcfg.Id };
                break;
            case "TextArea":
                cfg = { xtype: 'textarea', name: fcfg.Id };
                break;
            case "LocalInputList":
                cfg = { xtype: 'localinputlist', name: fcfg.Id };
                break;
            case "GlobalList":
                cfg = { xtype: 'ewList_Global', name: fcfg.Id, width: 300, autoLoad: true };
                cfg.attributes = [];
                for (var j = 0; j < fcfg.Attributes.length; j++) {
                    cfg.attributes[fcfg.Attributes[j].k] = fcfg.Attributes[j].v;
                }
                break;
            case "Date":
                cfg = { xtype: 'datefield', format: 'd/m/Y', name: fcfg.Id };
                break;
            case "Checkbox":
                cfg = { xtype: 'checkbox', name: fcfg.Id };
                cfg.attributes = [];
                for (var j = 0; j < fcfg.Attributes.length; j++) {
                    if (fcfg.Attributes[j].k == 'checked' && fcfg.Attributes[j].v == 'true') {
                        cfg.checked = true;
                    }
                    if (fcfg.Attributes[j].k == 'GTHonly' && fcfg.Attributes[j].v == 'true') {
                        if (!this.ownerCt.ownerCt.userHasGTHrole) {
                            cfg.disabled = true;
                        }
                    }


                }
                break;
            case "Label":
                cfg = { xtype: 'component', name: fcfg.Id, html: fcfg.Names };
                break;
        }
        cfg.fldAttributes = fcfg.Attributes;
        cfg.show = eBook.Fields.ReposShow;
        cfg.hide = eBook.Fields.ReposHide;
        if (clientSideVisibilitySource) {
            var lcfg = { fn: this.onFieldVisibilityChange, scope: this };
            listeners.change = lcfg;
            listeners.check = lcfg;
            listeners.select = lcfg;
            cfg.visibilityChange = {
                targets: clientSideVisibilitySourceElements
                , values: clientSideVisibilitySourceValues
            };
        }
        cfg.statusChange = statusChange;
        cfg.listeners = listeners;
        cfg.allowBlank = !fcfg.Required;
        cfg.isMetaField = true;
        cfg.renderTo = ffel;
        var fld = Ext.ComponentMgr.create(cfg);
        if (fcfg.FieldType != 'Label') {
            this.fields.push(fld);
            this.fieldIdx[fld.name] = this.fields.length - 1;
        }

        if (statusChange.length > 0) {
            this.statusChangeFields.push(fld.name);
        }
    }
    , setDefaultEndDate: function (startDateField) {
        //If a interval is defined within the repository structure table, use interval for period, other wise default value is 5
        var me = this,
            periodInterval = me.periodInterval ? me.periodInterval : 5,
            datefield = me.periodFields[1];

        if (datefield && datefield.name == 'EndDate' && !datefield.value) {
            this.periodFields[1].setValue(startDateField.getValue().add(Date.YEAR, periodInterval));
        }
    }
    , onFieldVisibilityChange: function (src) {
        var me = this,
            val = src.getValue(),
            prefix = '',fn,
            statusVal = me.refOwner.refOwner.detailPanel.fileDetails.statuslist.getValue();
        //if(src.xtype=="checkbox") val = src.
        if (src.xtype == "ewList_Global") {

            // prefix = 'G';
        }
        if (Ext.isObject(val)) val = val.id;
        val = prefix + val;
        for (var i = 0; i < src.visibilityChange.targets.length; i++) {
            fn = src.visibilityChange.targets[i];
            var vals = src.visibilityChange.values[fn];
            var fld = this.fields[this.fieldIdx[fn]];
            var foundMatch = false;
            for (var j = 0; j < vals.length; j++) {
                var showMe = !fld.statusChange || fld.statusChange.length == 0;
                if (fld.statusChange) {

                    for (var k = 0; k < fld.statusChange.length; k++) {
                        if (statusVal.toLowerCase() == fld.statusChange[k].toString().toLowerCase()) {
                            showMe=true;
                        }
                    }
                }
                if (showMe && val.toLowerCase() == (prefix + vals[j]).toLowerCase()) {

                    fld.allowBlank = false;
                    fld.show();
                    foundMatch = true;

                } //else {
                if (!foundMatch) {
                    fld.allowBlank = true;
                    fld.hide();
                }
            }
        }

    }
    , onStatusChange: function (val) {
        for (var i = 0; i < this.statusChangeFields.length; i++) {
            var fn = this.statusChangeFields[i];
            var fld = this.fields[this.fieldIdx[fn]];
            if (fld.statusChange) {
                fld.hide();
                for (var j = 0; j < fld.statusChange.length; j++) {
                    if (val.toLowerCase() == fld.statusChange[j].toString().toLowerCase()) {
                        fld.show();
                    }
                }
            }
        }
    }
    , loadConfig: function (cfg, selNode) {
        this.metaConfig = cfg;
        this.statusChangeFields = [];
        this.fieldIdx = {};
        this.clearFields();
        this.feature = cfg.ft;
        this.body.update(this.cfgTpl.apply(cfg));
        if (selNode == null) return;
        this.periodType = selNode.attributes.DateType;
        this.periodInterval = selNode.attributes.DateInterval;
        var periodParent = null;
        if (selNode.attributes.StructuralType == "FILE") {
            periodParent = selNode.findParentBy(function () {
                return this.attributes.PeriodStart && !Ext.isEmpty(this.attributes.PeriodStart)
                       && this.attributes.PeriodEnd && !Ext.isEmpty(this.attributes.PeriodEnd);
            }, null);
        }
        if (periodParent) {
            this.categoryPeriod = {
                start: Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodStart)
                    , end: Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodEnd)
            };
        } else {
            this.categoryPeriod = null;
            if (eBook.Interface.currentFile) {
                if (!eBook.Interface.currentFile.gthTeam5FileId) {
                    this.categoryPeriod = { start: eBook.Interface.currentFile.get('StartDate')
                        , end: eBook.Interface.currentFile.get('EndDate')
                    };
                } else {
                    this.categoryPeriod = { start: eBook.Interface.currentFile.gthTeam5StartDate
                        , end: eBook.Interface.currentFile.gthTeam5EndDate
                    };
                }
            }
        }

        var flds = this.body.query('.eb-repos-filemeta-field');
        var minIdx = 0;

        for (var i = 0; i < flds.length; i++) {
            var fid = flds[i].getAttribute('fieldId');
            if (fid.indexOf('_') == 0) {
                this.renderPeriod(flds[i], selNode);
                minIdx += 1;
            } else {
                this.renderField(flds[i], cfg.flds[i - minIdx]);
            }

        }
    }
});

Ext.reg('reposFileMeta',eBook.Repository.FileMetaPanel);

// ONLY TO BE USED IN CUSTOM ENTERNET-BROWSER !!


eBook.Repository.DirectoryTreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    createNode: function(attr) {
        // apply baseAttrs, nice idea Corey!
        attr.leaf=false;
        attr.text = attr.name;
        attr.iconCls = 'filestruct-' + attr.type;
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                            new Ext.tree.TreeNode(attr) :
                            new Ext.tree.AsyncTreeNode(attr);
        }
    }
    ,load : function(node, callback, scope){
        if(this.clearOnLoad){
            while(node.firstChild){
                node.removeChild(node.firstChild);
            }
        }
        if(this.doPreload(node)){ // preloaded json children
            this.runCallback(callback, scope || node, [node]);
        }else {
            this.requestData(node, callback, scope || node);
        }
    }
    ,requestData : function(node, callback, scope){
        if(this.fireEvent("beforeload", this, node, callback) !== false){
            var json = EY.GetDirectoryStructure(node.attributes.path);
            try {
                var o = json;
                node.beginUpdate();
                for (var i = 0, len = o.length; i < len; i++) {
                    var n = this.createNode(o[i]);
                    if (n) {
                        node.appendChild(n);
                    }
                }
                node.endUpdate();
                this.runCallback(callback, scope || node, [node]);
                this.fireEvent("load", this, node, null);
            } catch (e) {
                this.fireEvent("loadexception", this, node, null);
            }
        }else{
            // if the load is cancelled, make sure we notify
            // the node that we are done
            this.runCallback(callback, scope || node, []);
        }
    }
});

eBook.Repository.DirectoryTree = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        Ext.apply(this, {
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDD: false,
            containerScroll: true,
            border: false,
            // auto create TreeLoader
            loader: new eBook.Repository.DirectoryTreeLoader({}),
            selModel: new Ext.tree.DefaultSelectionModel({
                listeners: {
                    'selectionchange': {
                        fn: this.onSelectionChange
	                    , scope: this
                    }
                }
            }),
            rootVisible: false,
            root: new Ext.tree.AsyncTreeNode()
            
        });
        eBook.Repository.DirectoryTree.superclass.initComponent.apply(this, arguments);

    }
    , onSelectionChange: function(sel, node) {
        this.refOwner.contents.load(node.attributes.path);
    }
});
Ext.reg('directorytree', eBook.Repository.DirectoryTree);

eBook.Repository.DirectoyContents = Ext.extend(Ext.DataView, {
    initComponent: function() {
        this.listTpl = new Ext.XTemplate('<tpl for=".">',
                    '<div class="eb-dc-file-wrap" id="{path}">',
                        '<tpl if="type==\'File\'">',
		                    '<div class="eb-dc-file-ico eb-dc-file-ico-{extension}">{name}</div>',
		                    '<div class="eb-dc-file-created">{created:date("d/m/Y")}</div>',
		                    '<div class="eb-dc-file-lastupdated">{lastupdated:date("d/m/Y")}</div>',
		                '</tpl>',
		                '<tpl if="type==\'Directory\'">',
		                    '<div class="eb-dc-file-ico eb-dc-file-ico-directory">{name}</div>',
		                '</tpl>',
		            '</div>',
                    '<div class="x-clear"></div>',
                '</tpl>',
                { compiled: true }
                );


        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                url: 'get-images.php',
                root: 'contents',
                fields: ['name', 'path', 'extension', 'type', { name: 'created', type: 'date', dateFormat: 'timestamp' }, { name: 'lastupdated', type: 'date', dateFormat: 'timestamp'}]
            }),
            tpl: this.listTpl,
            bodyStyle:'background-color:#fff;',
            //autoHeight: true,
            autoSCroll:true,
            multiSelect: true,
            overClass: 'x-view-over',
            itemSelector: 'div.eb-dc-file-wrap',
            emptyText: 'No items'
        });
        eBook.Repository.DirectoyContents.superclass.initComponent.apply(this, arguments);
    }
    , load: function(path) {
        this.getEl().mask();
        var o = { contents: EY.GetDirectoryContents(path) };
        this.store.loadData(o);
        this.getEl().unmask();
    }
});
Ext.reg('directorycontents', eBook.Repository.DirectoyContents);



eBook.Repository.LocalFileBrowser = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border',
            
            items: [{ xtype: 'directorytree', ref: 'dirtree', region: 'west',split:true,width:150 },
                    { xtype: 'directorycontents', ref: 'contents', region: 'center'}]
        });
        eBook.Repository.LocalFileBrowser.superclass.initComponent.apply(this, arguments);
    }
});

eBook.Repository.FileWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        var repository = new eBook.Repository.Tree();
        //repository.treeFilter = new eBook.Repository.TreeFilter(repository);
        Ext.apply(this, {
            title: 'Repository',
            items: [repository],
            layout: 'fit'
        });
        eBook.Repository.FileWindow.superclass.initComponent.apply(this, arguments);
    }
});



eBook.Repository.ContextLinkListDataView = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: 'GetFileLinks'
                , serviceUrl: eBook.Service.repository
                //, autoDestroy: true
                //, autoLoad: true
                , criteriaParameter: 'cicdc'
                , fields: eBook.data.RecordTypes.RepositoryItemLinks
                , baseParams: {
                    'Id': eBook.Interface.currentFile.get('Id')
                    , 'Culture': eBook.Interface.Culture
                }
                , listeners: { load: { fn: this.onSetFileLinks, scope: this} }
            })

            , tpl: new Ext.XTemplate(
		        '<tpl for=".">',
                    '<div class="ebook-repo-file-link eb-rep-node-pdf">',
		                '<div class="ebook-repo-file-link-txt"><a href="javascript:eBook.Interface.showRepoItem(\'{ItemId}\');">{Description}</a></div>',
		            '<div class="ebook-repo-file-link-delete"></div>',
		            '</div>',
                '</tpl>',
                '<div class="x-clear"></div>'
	        )

	        , itemSelector: 'div.ebook-repo-file-link'
	        , overClass: 'ebook-repo-file-link-over'
            , cls: 'ebook-repo-file-links'
            , data: []
            , border: false
            , prepareData: function(dta) {
                return dta;
            }
            , listeners: { click: { fn: this.onMyItemClick, scope: this} }

        });
        eBook.Repository.ContextLinkListDataView.superclass.initComponent.apply(this, arguments);
    }
    , onMyItemClick: function(dv, idx, n, e) {
        if (e) {
            var tg = e.getTarget('.ebook-repo-file-link-delete');
            if (tg) {
                var rec = this.store.getAt(idx);
                Ext.Msg.show({
                    title: 'Delete link',
                    msg: 'Are you sure you want to delete this link to \'' + rec.get('Description') + '\'?',
                    buttons: Ext.Msg.YESNO,
                    rec: rec,
                    fn: this.onAcceptDelete,
                    icon: Ext.MessageBox.QUESTION,
                    scope: this
                });
            }
        }
    }
    , onAcceptDelete: function(btnid, txt, opts) {
        if (btnid == "yes") {
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'DeleteLink',
                callback: this.deletePerformed,
                scope: this,
                params: Ext.encode({ rlidc: opts.rec.json })
            });
        }
    }
    , deletePerformed: function() {
        this.store.load();
        if (this.refOwner.caller && this.refOwner.caller.doRefresh) {
            this.refOwner.caller.doRefresh();
        }
    }
    , onSetFileLinks: function() {
        var st = this.store;
        var accs = st.collect('ConnectionAccount', false);
        var guids = st.collect('ConnectionGuid', false);
        var its = guids.concat(accs);
        var lnks = {};
        for (var i = 0; i < its.length; i++) {
            lnks['L' + its[i]] = true;
        }
        eBook.Interface.fileRepository = lnks;

        eBook.Repository.ContextLinkListDataView.superclass.onDataChanged.apply(this, arguments);
    }

});

eBook.Repository.ContextLinkList = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit'
            , id: 'eBook-file-links'
            , maxWidth: 200
            , maxHeight: 120
            , delay: 1
            , floating: true
            , autoScroll: true
            , renderTo: Ext.getBody()
            , items: [new eBook.Repository.ContextLinkListDataView({ ref: 'dv' })]
        });
        eBook.Repository.ContextLinkList.superclass.initComponent.apply(this, arguments);
    }
    , hide: function() {
        eBook.Repository.ContextLinkList.superclass.hide.call(this);
    }
    , startHide: function(delay) {
        this.hideTask.cancel();
        this.hideTask.delay((delay || this.delay) * 1000);
    }
    , showAt: function(el, connectiontype, connectionId, caller) {
        this.caller = caller;
        this.dv.store.clearFilter();
        this.dv.store.filterBy(function(rec, id) {
            return rec.get('ConnectionType') == connectiontype
                    && (rec.get('ConnectionGuid') == connectionId
                        || rec.get('ConnectionAccount') == connectionId);
        });
        this.show();
        this.setSize(this.maxWidth, this.dv.getHeight() + 10);
        this.getEl().alignTo(Ext.get(el), 'tl-bl?');
        this.startHide();

    }
    , initEvents: function() {
        eBook.Repository.ContextLinkList.superclass.initEvents.apply(this, arguments);
        /*this.mon(this.getEl(),'mouseout',this.onPanelMouseOut,this);
        this.mon(this.getEl(),'mouseover',this.onPanelMouseOver);*/
        //this.showTask = new Ext.util.DelayedTask(this.show, this);
        this.hideTask = new Ext.util.DelayedTask(function() {
            // this.showTask.cancel();
            this.hide();
        }, this);

        this.el.hover(function() {
            this.hideTask.cancel();
        }, function() {
            this.hideTask.delay(this.delay * 1000);
        }, this);
        this.makeFloating(true);
    }
})
eBook.Repository.SelectTree = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        Ext.apply(this, {
            root: {
                nodeType: 'async',
                text: 'Repository',
                draggable: false,
                id: 'root',
                expanded: true
            },
            cls: 'eb-repos-selection-tree',
            rootVisible: false,
            floating: true,
            autoScroll: true,
            minWidth: 200,
            minHeight: 200,
            singleExpand: true,
            enableDrag: true,
            enableDrop: false,
            ddGroup: 'repository',
            width: 250,
            height: 400,
            delay: 2,
            loader: new eBook.Repository.TreeLoader({
                dataUrl: eBook.Service.repository + 'GetChildren',
                requestMethod: "POST",
                showFiles: true,
                selectionMode: true,
                filter: { ps: eBook.Interface.currentFile.get('StartDate')
                            , pe: eBook.Interface.currentFile.get('EndDate')
                            , stat: null
                }
            })
        });
        eBook.Repository.SelectTree.superclass.initComponent.apply(this, arguments);
    }
    , initEvents: function() {
        eBook.Repository.SelectTree.superclass.initEvents.apply(this, arguments);
        /*this.mon(this.getEl(),'mouseout',this.onPanelMouseOut,this);
        this.mon(this.getEl(),'mouseover',this.onPanelMouseOver);*/
        //this.showTask = new Ext.util.DelayedTask(this.show, this);
        this.hideTask = new Ext.util.DelayedTask(function() {
            // this.showTask.cancel();
            this.hide();
        }, this);

        this.el.hover(function() {
            this.hideTask.cancel();
        }, function() {
            this.hideTask.delay(this.delay * 1000);
        }, this);
    }
    , hide: function() {
        eBook.Repository.SelectTree.superclass.hide.call(this);
        if (this.btn) this.btn.toggle(false, true);
    }
    , showMenu: function(btn) {
        this.show();
        this.getEl().alignTo(btn.getEl(), 'tl-bl?');
        this.btn = btn;
        this.startHide();
    }
    , startHide: function(delay) {
        this.hideTask.cancel();
        this.hideTask.delay((delay || this.delay) * 1000);
    }

});///Gives the user the option to choose a file for replacement.
///store, repofilepath and caller are supplied by caller
eBook.Repository.ReplaceChoiceWindow = Ext.extend(eBook.Window, {
    repoFilePath: null, //supplied by caller
    store: null, //supplied by caller
    caller: null,//supplied by caller
    repoLocation: null, //supplied by caller

    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            title: 'Folder overview',
            ref: 'replaceChoiceWindow',
            modal: true,
            layout: 'vbox',
            layoutConfig: {
                align: 'stretch'
            },
            bodyStyle: 'background-color:#FFF;',
            width: 800,
            height: 287,
            y: 75, //half of height difference with body viewsize when preview added (see loadPreview)
            minimizable: true,
            items: [
                {
                    xtype: 'container',
                    cls: 'x-toolbar x-box-item x-toolbar',
                    height: 30,
                    ref: 'pnlFolder',
                    html: '<img style="padding-left: 2px; margin-top: 3px; float: left;" src="images/icons/16/tree/folder-open.gif" style="padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; background-size: 14px 14px;" alt="Folder"/><div style ="margin-left: 22px; margin-top: 7px; font: 11px arial, tahoma, helvetica, sans-serif; font-weight: bold;">' + (me.repoFilePath ? me.repoFilePath : '') + '</div>'
                },
                {
                    xtype: 'fileGrid',
                    ref: 'fileGrid',
                    height: 177,
                    store: me.store ? me.store : null
                },
                {
                    xtype: 'RepositoryPreview',
                    title: 'Preview',
                    ref: 'previewPanel',
                    flex: 1,
                    hidden: true
                }
            ],
            bbar: [
                {
                    xtype: 'tbfill'
                },
                {
                    text: eBook.Repository.ReplaceFile,
                    ref: '../btnReplace',
                    iconCls: 'eBook-replace-menu-context-icon',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.replaceFile,
                    tooltip: 'Replaces the current selected file.',
                    width: 100,
                    scope: this
                },
                {
                    text: eBook.Repository.NewFile,
                    ref: '../btnNew',
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.addFile,
                    tooltip: 'Allows you to add a new file.',
                    width: 100,
                    scope: this
                },
                {
                    text: eBook.Cancel,
                    ref: '../btnCancel',
                    iconCls: 'eBook-window-cancel-ico eBook-background-image-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.close,
                    width: 100,
                    scope: this
                }]
        });

        eBook.Repository.ReplaceChoiceWindow.superclass.initComponent.apply(me, arguments);
    },
    ///add a new file, repoLocation is optional
    addFile: function () {
        var me = this,
            standards = null,
            wn = null;

        if (me.repoLocation) {
            standards = {};
            standards.location = me.repoLocation;
        }

        wn = new eBook.Repository.SingleUploadWindow({
            clientId: eBook.CurrentClient,
            standards: standards,
            readycallback: {fn: me.reload, scope: me}
        });

        wn.show(me);
        me.close();
    },
    ///replace the given file
    replaceFile: function () {
        var me = this,
            wn = null,
            record = me.fileGrid ? me.fileGrid.getActiveRecord() : null,
            id = record ? record.get("Id") : null;

        if (id) {
            wn = new eBook.Repository.SingleUploadWindow({
                existingItemId: id,
                readycallback: {fn: me.reload, scope: me}
            });

            wn.show(me);
        }
        me.close();
    },
    enableRowSpecificButtons: function(enable) {
        var me = this;

        if(me.btnReplace)
        {
            if(enable) {
                me.btnReplace.enable();
            } else {
                me.btnReplace.disable();
            }
        }
    },
    loadPreview: function(clientId,physicalFileId){
        var me = this;
        if(clientId && physicalFileId) {
            me.previewPanel.loadFile("Repository/" + eBook.getGuidPath(clientId) + physicalFileId + ".pdf");
            me.previewPanel.show();
            me.doLayout();
            me.setHeight(Ext.getBody().getViewSize().height - 150);
            me.center();
        }
    },
    unloadPreview: function(){
        var me = this;
        me.previewPanel.unloadFile();
        me.previewPanel.hide();
        me.doLayout();
        me.setHeight(287);
        me.setPosition(me.getPosition()[0],75);
    }
});

Ext.reg('replaceChoiceWindow', eBook.Repository.ReplaceChoiceWindow);///The FileGrid is simple grid panel showing a flat list (name, status and path)
///Store is supplied by caller
eBook.Repository.FileGrid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            title: "Files",
            loadMask: true,
            cls: "ebook-fileGrid",
            activeRecord: null,
            autoExpandColumn:2,
            store: me.store ? me.store : null,
            viewConfig: {
                scrollOffset: 0
            },
            listeners: {
                beforerender: function (gridPanel) {
                    var store = gridPanel.getStore();
                    var result = store.queryBy(function(record){return record.get("Status") === null || record.get("Status") === "";}); //Check if status is empty. Query function couldn't differentiate empty strings therefore I used queryBy with custom function
                    if(store.getCount() == result.getCount()) //no status defined, hide column
                    {
                        gridPanel.getColumnModel().setHidden(1, true);
                    }
                }
            },
            columns: [{
                header: "FileName",
                dataIndex: 'FileName',
                renderer: function (value, metaData, record) { //add pdf icon and css
                    return '<div style="font-weight: bold; padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; background-size: 14px 14px;" class="' + record.get("IconCls") + '" >' + value + '</div>';
                },
                width: 300
            }, {
                header: "Status",
                dataIndex: 'StatusText',
                width: 300
            },
            {
                header: "Extension",
                dataIndex: 'Extension',
                renderer: function (value, metaData, record) {
                    return value.replace(".","");
                },
                width: 50
            }],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    'rowselect': {
                        fn: function (grid, rowIndex) {
                            var me = this,
                                record = me.getStore().getAt(rowIndex);

                            me.setActiveRecord(record); //Set the active record (needed because selection of row is lost)
                            me.refOwner.enableRowSpecificButtons(true);
                            me.refOwner.loadPreview(record.get("ClientId"),record.get("PhysicalFileId"));
                        },
                        scope: me
                    },
                    'rowdeselect': {
                        fn: function () {
                            me.setActiveRecord(null);
                            me.refOwner.enableRowSpecificButtons(false);
                            me.refOwner.unloadPreview();
                        },
                        scope: this
                    }
                }
            })
        });
        eBook.Bundle.FormOverview.superclass.initComponent.apply(me, arguments);
    },
    clearStore: function ()
    {
        var me = this;

        me.store.removeAll();
    },
    setActiveRecord: function(value)
    {
        var me = this;

        me.activeRecord = value;
    },
    getActiveRecord: function()
    {
        var me = this;

        return me.activeRecord;
    }
});
Ext.reg('fileGrid', eBook.Repository.FileGrid);

eBook.File.ServicesPanel = Ext.extend(Ext.form.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'hbox', layoutConfig: { align: 'stretch' },
            defaults: { margins: { top: 5, left: 5, right: 5, bottom: 5} },
            disabled: eBook.Interface.isFileClosed(),
            items: [
                 { xtype: 'fieldset', flex: 1, title: 'Periods', ref: 'periods'
                    , items: [{ xtype: 'fieldset', title: 'Current period', ref: 'curperiod'
                                                , items: [{
                                                    xtype: 'datefield'
                                                                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                                                                , allowBlank: false
                                                                , format: 'd/m/Y'
                                                                , name: 'StartDate'
                                                                , ref: 'startdate'
                                                                , width: 100
                                                                , ctx: 'current'
                                                }, {
                                                    xtype: 'datefield'
                                                                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                                                                , allowBlank: false
                                                                , format: 'd/m/Y'
                                                                , name: 'EndDate'
                                                                , ref: 'enddate'
                                                                , ctx: 'current'
                                                                , width: 100
                                                                , listeners: {
                                                                    'change': {
                                                                        fn: this.onEndDateChanged
                                                                        , scope: this
                                                                    }
                                                                }
                                                }, {
                                                    xtype: 'displayfield'
                                                                , name: 'Assessmentyear'
                                                                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Assessmentyear
                                                                , ref: 'assessmentyear'
}]
                    }, { xtype: 'fieldset', title: 'Previous period', ref: 'prevperiod'
                            , items: [{
                                xtype: 'datefield'
                                    , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                                    , allowBlank: false
                                    , format: 'd/m/Y'
                                    , name: 'StartDate'
                                    , ref: 'startdate'
                                    , ctx: 'previous'
                                    , width: 100
                            }, {
                                xtype: 'datefield'
                                    , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                                    , allowBlank: false
                                    , format: 'd/m/Y'
                                    , name: 'EndDate'
                                    , ref: 'enddate'
                                    , ctx: 'previous'
                                    , width: 100
                                    , listeners: {
                                        'change': {
                                            fn: this.onEndDateChanged
                                            , scope: this
                                        }
                                    }
                            }, {
                                xtype: 'displayfield'
                                    , name: 'Assessmentyear'
                                    , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Assessmentyear
                                    , ref: 'assessmentyear'
}]
}]
                 },
                    { xtype: 'fieldset', flex: 1, title: 'Settings', ref: 'sets'
                            , items: [
                    {
                        xtype: 'languagebox'
                         , ref: 'language'
                         , name: 'language'
                         , allowBlank: false
                         , fieldLabel: 'Language'
                         , width: 200
                         , listWidth: 200
                    }
                        , { xtype: 'checkbox', ref: 'first', name: 'first', boxLabel: 'First bookyear', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallPrevBef', boxLabel: 'Before previous period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallPrev', boxLabel: 'Previous period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallCur', boxLabel: 'Current period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'oneof', boxLabel: 'Current period is one of the first 3 periods', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'taxshelter', boxLabel: 'Tax shelter?', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
}]
                        }
                    ]
        });
        eBook.File.ServicesPanel.superclass.initComponent.apply(this, arguments);
    }
    , onEndDateChanged: function(lfd, n, o) {
        var par = lfd.ownerCt;
        if (Ext.isDate(n)) {
            yr = n.getFullYear();
            if (n.getMonth() == 11 && n.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
            var ay = par.ownerCt.ownerCt['ay' + lfd.ctx];
            this.valid = true;
            if (ay != yr) {
                Ext.Msg.show({
                    title: 'INVALID',
                    msg: 'Changing assessmentyear is not allowed',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                lfd.markInvalid('Changing assessmentyear is not allowed');
                this.valid = false;
            }
            par.assessmentyear.setValue(yr);
            par.assessmentyear.originalValue = yr;
        } else {
            par.assessmentyear.setValue('');
            par.assessmentyear.originalValue = '';
        }
    }
    , checkChanged: function(chk, checked) {
        if (chk.name == 'first') {
            if (checked) {
                this.sets.smallPrevBef.setValue(false);
                this.sets.smallPrev.setValue(false);
                this.sets.oneof.setValue(true);
                this.sets.smallPrevBef.disable();
                this.sets.smallPrev.disable();
                this.sets.oneof.disable();
                this.periods.prevperiod.startdate.allowBlank = true;
                this.periods.prevperiod.enddate.allowBlank = true;
                this.periods.prevperiod.startdate.setValue(null);
                this.periods.prevperiod.enddate.setValue(null);
                this.periods.prevperiod.assessmentyear.setValue('');
                this.periods.prevperiod.disable();

            } else {

                this.periods.prevperiod.startdate.allowBlank = false;
                this.periods.prevperiod.enddate.allowBlank = false;
                this.periods.prevperiod.enable();
                this.sets.smallPrevBef.enable();
                this.sets.smallPrev.enable();
                this.sets.oneof.enable();
            }
        }
    }
    , loadData: function(robj) {
        this.periods.curperiod.startdate.setValue(Ext.data.Types.WCFDATE.convert(robj.StartDate));
        this.periods.curperiod.enddate.setValue(Ext.data.Types.WCFDATE.convert(robj.EndDate));

        var dte = Ext.data.Types.WCFDATE.convert(robj.EndDate);
        if (dte) {
            this.aycurrent = dte.getFullYear();
            if (dte.getMonth() == 11 && dte.getDate() == 31) this.aycurrent++;
            this.onEndDateChanged(this.periods.curperiod.enddate, this.periods.curperiod.enddate.getValue());
        }
        this.periods.prevperiod.startdate.setValue(Ext.data.Types.WCFDATE.convert(robj.PreviousStartDate));
        this.periods.prevperiod.enddate.setValue(Ext.data.Types.WCFDATE.convert(robj.PreviousEndDate));


        dte = Ext.data.Types.WCFDATE.convert(robj.PreviousEndDate);
        if (dte) {
            this.ayprevious = dte.getFullYear();
            if (dte.getMonth() == 11 && dte.getDate() == 31) this.ayprevious++;

            this.onEndDateChanged(this.periods.prevperiod.enddate, this.periods.prevperiod.enddate.getValue());
        }
        this.sets.language.setValue(robj.Culture);
        this.sets.smallPrevBef.setValue(robj.Settings.bps);
        this.sets.smallPrev.setValue(robj.Settings.ps);
        this.sets.smallCur.setValue(robj.Settings.cs);

        this.sets.oneof.setValue(robj.Settings.oft);
        this.sets.first.setValue(robj.Settings.fby);
        this.checkChanged(this.sets.first, robj.Settings.fby);
        this.sets.taxshelter.setValue(robj.Settings.ts);

        this.resetOriginals();
    }
    , detectChanges: function() {
        return this.getForm().isDirty();

    }
    , resetOriginals: function() {
        this.periods.curperiod.startdate.originalValue = this.periods.curperiod.startdate.getValue();
        this.periods.curperiod.enddate.originalValue = this.periods.curperiod.enddate.getValue();
        this.periods.prevperiod.startdate.originalValue = this.periods.prevperiod.startdate.getValue();
        this.periods.prevperiod.enddate.originalValue = this.periods.prevperiod.enddate.getValue();
        this.sets.language.originalValue = this.sets.language.getValue();
        this.sets.smallPrevBef.originalValue = this.sets.smallPrevBef.getValue();
        this.sets.smallPrev.originalValue = this.sets.smallPrev.getValue();
        this.sets.smallCur.originalValue = this.sets.smallCur.getValue();
        this.sets.oneof.originalValue = this.sets.oneof.getValue();
        this.sets.first.originalValue = this.sets.first.getValue();
        this.sets.taxshelter.originalValue = this.sets.taxshelter.getValue();
    }
    , getData: function(robj) {
        var obj = {
            FileId: eBook.Interface.currentFile.get('Id')
            , Culture: this.sets.language.getValue()
            , StartDate: this.periods.curperiod.startdate.getValue()
            , EndDate: this.periods.curperiod.enddate.getValue()
            , PreviousStartDate: this.periods.prevperiod.startdate.getValue()
            , PreviousEndDate: this.periods.prevperiod.enddate.getValue()
            , Settings: {
                fid: eBook.Interface.currentFile.get('Id')
                , fby: this.sets.first.getValue()
                , oft: this.sets.oneof.getValue()
                , bps: this.sets.smallPrevBef.getValue()
                , ps: this.sets.smallPrev.getValue()
                , cs: this.sets.smallCur.getValue()
                , ts: this.sets.taxshelter.getValue()
            }
        };
        if (!Ext.isDefined(obj.PreviousStartDate) || Ext.isEmpty(obj.PreviousStartDate)) obj.PreviousStartDate = null;
        if (!Ext.isDefined(obj.PreviousEndDate) || Ext.isEmpty(obj.PreviousEndDate)) obj.PreviousEndDate = null;
        return obj;
    }
});
Ext.reg('file-services-panel',eBook.File.ServicesPanel);

eBook.File.ServicesWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            items: [{ xtype: 'panel', html: '!IMPORTANT! Changing these settings will result in a complete recalculation of this file and all content. <span style="color:#FF0;">Review your changes <u>before saving</u>!</span>', height: 24
                        , border: false, region: 'north', bodyStyle: 'background-color:#FE0000;padding:5px;font-weight:bold;color:#FFF;'
            },
                    { xtype: 'file-services-panel', region: 'center', ref: 'servicePanel'}]
            , layout: 'border'
            , bbar: ['->', {
                ref: 'savechanges',
                text: "Save changes",
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                disabled: eBook.Interface.isFileClosed(),
                scope: this
}]
            , title: 'File settings'
            , width: 880
            , height: 410
            , modal: true
            });
            eBook.File.ServicesWindow.superclass.initComponent.apply(this, arguments);
        }
    , show: function() {
        eBook.File.ServicesWindow.superclass.show.call(this);
        this.loadData();
    }
    , loadData: function() {
        this.getEl().mask('Loading data', 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'GetFileSettings'
                , method: 'POST'
                , params: Ext.encode({
                    cidc: {
                        Id: eBook.Interface.currentFile.get('Id')
                    }
                })
                , callback: this.onDataObtained
                , scope: this
        });
    }
    , onDataObtained: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText).GetFileSettingsResult;
            this.servicePanel.loadData(robj);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , detectChanges: function() {
        return this.servicePanel.detectChanges();
    }
    , onBeforeClose: function() {
        if (this.detectChanges()) {
            Ext.Msg.show({
                title: '!! Changes detected !!',
                msg: 'You made changes to the settings. Are you sure you wish to <b>CANCEL ALL CHANGES</b>?',
                buttons: Ext.Msg.YESNO,
                fn: this.onChangesDetected,
                scope: this,
                icon: Ext.MessageBox.QUESTION
            });
            return false;
        }
        return eBook.File.ServicesWindow.superclass.onBeforeClose.call(this);
    }
    , onChangesDetected: function(btnid, txt, opts) {
        if (btnid == 'yes') {
            this.servicePanel.resetOriginals();
            this.close();
        }
    }
    , onSaveClick: function() {
        if (this.servicePanel.valid) {
            this.saveChanges();
        } else {
            Ext.Msg.show({
                title: 'INVALID',
                msg: 'The changes you\'ve made are invalid. Review!',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }
    , saveChanges: function(force) {
        if (!this.detectChanges() && !force) {
            Ext.Msg.show({
                title: 'No changes detected',
                msg: 'No changes to the settings were detected, do you want to force-save changes anyway?',
                buttons: Ext.Msg.YESNO,
                fn: this.onNoChanges,
                scope: this,
                icon: Ext.MessageBox.QUESTION
            });
        } else {
            this.getEl().mask('Saving changes', 'x-mask-loading');
            this.servicePanel.resetOriginals();
            eBook.CachedAjax.request({
                url: eBook.Service.file + 'SaveFileSettings'
                    , method: 'POST'
                    , params: Ext.encode({
                        csfsdc: {
                            Settings: this.servicePanel.getData()
                            , Person: eBook.User.getActivePersonDataContract()
                        }
                    })
                    , callback: this.onSavedChanges
                    , scope: this
            });

        }
    }
    , onNoChanges: function(btnid, txt, opts) {
        if (btnid == 'yes') {
            this.saveChanges(true);
        } else {
            this.close();
        }
    }
    , onSavedChanges: function(opts, success, resp) {
        if (success) {
            eBook.Interface.center.fileMenu.updateWorksheets(true);
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }

    });        /*!
* Ext JS Library 3.2.1
* Copyright(c) 2006-2010 Ext JS, Inc.
* licensing@extjs.com
* http://www.extjs.com/license
*/
Ext.ns('Ext.ux');

Ext.ux.GroupTabPanel = Ext.extend(Ext.TabPanel, {
    tabPosition: 'left',

    alternateColor: false,

    alternateCls: 'x-grouptabs-panel-alt',

    defaultType: 'grouptab',

    deferredRender: false,

    activeGroup: null,

    initComponent: function() {
        Ext.ux.GroupTabPanel.superclass.initComponent.call(this);

        this.addEvents(
            'beforegroupchange',
            'groupchange'
        );
        this.elements = 'body,header';
        this.stripTarget = 'header';

        this.tabPosition = this.tabPosition == 'right' ? 'right' : 'left';

        this.addClass('x-grouptabs-panel');

        if (this.tabStyle && this.tabStyle != '') {
            this.addClass('x-grouptabs-panel-' + this.tabStyle);
        }

        if (this.alternateColor) {
            this.addClass(this.alternateCls);
        }

        this.on('beforeadd', function(gtp, item, index) {
            this.initGroup(item, index);
        });
        this.items.each(function(item) {
            item.on('tabchange', function(item) {
                this.fireEvent('tabchange', this, item.activeTab);
            }, this);
        }, this);
    },

    initEvents: function() {
        this.mon(this.strip, 'mousedown', this.onStripMouseDown, this);
    },

    onRender: function(ct, position) {
        Ext.TabPanel.superclass.onRender.call(this, ct, position);
        if (this.plain) {
            var pos = this.tabPosition == 'top' ? 'header' : 'footer';
            this[pos].addClass('x-tab-panel-' + pos + '-plain');
        }

        var st = this[this.stripTarget];

        this.stripWrap = st.createChild({ cls: 'x-tab-strip-wrap ', cn: {
            tag: 'ul', cls: 'x-grouptabs-strip x-grouptabs-tab-strip-' + this.tabPosition}
        });

        var beforeEl = (this.tabPosition == 'bottom' ? this.stripWrap : null);
        this.strip = new Ext.Element(this.stripWrap.dom.firstChild);

        this.header.addClass('x-grouptabs-panel-header');
        this.bwrap.addClass('x-grouptabs-bwrap');
        this.body.addClass('x-tab-panel-body-' + this.tabPosition + ' x-grouptabs-panel-body');

        if (!this.groupTpl) {
            var tt = new Ext.Template(
                '<li class="{cls}" id="{id}">',
                '<a class="x-grouptabs-expand" onclick="return false;"></a>',
                '<a class="x-grouptabs-text {iconCls}" href="#" onclick="return false;">',
                '<span>{text}</span></a>',
                '</li>'
            );
            tt.disableFormats = true;
            tt.compile();
            Ext.ux.GroupTabPanel.prototype.groupTpl = tt;
        }
        this.items.each(this.initGroup, this);
    },

    afterRender: function() {
        Ext.ux.GroupTabPanel.superclass.afterRender.call(this);

        this.tabJoint = Ext.fly(this.body.dom.parentNode).createChild({
            cls: 'x-tab-joint'
        });

        this.addClass('x-tab-panel-' + this.tabPosition);
        this.header.setWidth(this.tabWidth);

        if (this.activeGroup !== undefined) {
            var group = (typeof this.activeGroup == 'object') ? this.activeGroup : this.items.get(this.activeGroup);
            delete this.activeGroup;
            this.setActiveGroup(group);
            group.setActiveTab(group.getMainItem());
        }
    },

    getGroupEl: Ext.TabPanel.prototype.getTabEl,

    // private
    findTargets: function(e) {
        var item = null,
            itemEl = e.getTarget('li', this.strip);
        if (itemEl) {
            item = this.findById(itemEl.id.split(this.idDelimiter)[1]);
            if (item.disabled) {
                return {
                    expand: null,
                    item: null,
                    el: null
                };
            }
        }
        return {
            expand: e.getTarget('.x-grouptabs-expand', this.strip),
            isGroup: !e.getTarget('ul.x-grouptabs-sub', this.strip),
            item: item,
            el: itemEl
        };
    },

    // private
    onStripMouseDown: function(e) {
        if (e.button != 0) {
            return;
        }
        e.preventDefault();
        var t = this.findTargets(e);
        if (t.expand) {
            this.toggleGroup(t.el);
        }
        else if (t.item) {
            if (t.isGroup) {
                t.item.setActiveTab(t.item.getMainItem());
            }
            else {
                t.item.ownerCt.setActiveTab(t.item);
            }
        }
    },

    expandGroup: function(groupEl) {
        if (groupEl.isXType) {
            groupEl = this.getGroupEl(groupEl);
        }
        Ext.fly(groupEl).addClass('x-grouptabs-expanded');
        this.syncTabJoint();
    },

    toggleGroup: function(groupEl) {
        if (groupEl.isXType) {
            groupEl = this.getGroupEl(groupEl);
        }
        Ext.fly(groupEl).toggleClass('x-grouptabs-expanded');
        this.syncTabJoint();
    },

    collapseGroup: function(groupEl) {
        if (groupEl.isXType) {
            groupEl = this.getGroupEl(groupEl);
        }
        Ext.fly(groupEl).removeClass('x-grouptabs-expanded');
        this.syncTabJoint();
    },

    syncTabJoint: function(groupEl) {
        if (!this.tabJoint) {
            return;
        }

        groupEl = groupEl || this.getGroupEl(this.activeGroup);
        if (groupEl) {
            this.tabJoint.setHeight(Ext.fly(groupEl).getHeight() - 2);

            var y = Ext.isGecko2 ? 0 : 1;
            if (this.tabPosition == 'left') {
                this.tabJoint.alignTo(groupEl, 'tl-tr', [-2, y]);
            }
            else {
                this.tabJoint.alignTo(groupEl, 'tr-tl', [1, y]);
            }
        }
        else {
            this.tabJoint.hide();
        }
    },

    getActiveTab: function() {
        if (!this.activeGroup) return null;
        return this.activeGroup.getTabEl(this.activeGroup.activeTab) || null;
    },

    onResize: function() {
        Ext.ux.GroupTabPanel.superclass.onResize.apply(this, arguments);
        this.syncTabJoint();
    },

    createCorner: function(el, pos) {
        return Ext.fly(el).createChild({
            cls: 'x-grouptabs-corner x-grouptabs-corner-' + pos
        });
    },

    initGroup: function(group, index) {
        var before = this.strip.dom.childNodes[index],
            p = this.getTemplateArgs(group);
        if (index === 0) {
            p.cls += ' x-tab-first';
        }
        p.cls += ' x-grouptabs-main';
        p.text = group.groupTitle;

        var el = before ? this.groupTpl.insertBefore(before, p) : this.groupTpl.append(this.strip, p),
            tl = this.createCorner(el, 'top-' + this.tabPosition),
            bl = this.createCorner(el, 'bottom-' + this.tabPosition);

        group.tabEl = el;
        if (group.expanded) {
            this.expandGroup(el);
        }

        if (Ext.isIE6 || (Ext.isIE && !Ext.isStrict)) {
            bl.setLeft('-10px');
            bl.setBottom('-5px');
            tl.setLeft('-10px');
            tl.setTop('-5px');
        }

        this.mon(group, {
            scope: this,
            changemainitem: this.onGroupChangeMainItem,
            beforetabchange: this.onGroupBeforeTabChange
        });
    },

    setActiveGroup: function(group) {
        group = this.getComponent(group);
        if (!group) {
            return false;
        }
        if (!this.rendered) {
            this.activeGroup = group;
            return true;
        }
        if (this.activeGroup != group && this.fireEvent('beforegroupchange', this, group, this.activeGroup) !== false) {
            if (this.activeGroup) {
                this.activeGroup.activeTab = null;
                var oldEl = this.getGroupEl(this.activeGroup);
                if (oldEl) {
                    Ext.fly(oldEl).removeClass('x-grouptabs-strip-active');
                }
            }

            var groupEl = this.getGroupEl(group);
            Ext.fly(groupEl).addClass('x-grouptabs-strip-active');

            this.activeGroup = group;
            this.stack.add(group);

            this.layout.setActiveItem(group);
            this.syncTabJoint(groupEl);

            this.fireEvent('groupchange', this, group);
            return true;
        }
        return false;
    },

    onGroupBeforeTabChange: function(group, newTab, oldTab) {
        if (group !== this.activeGroup || newTab !== oldTab) {
            this.strip.select('.x-grouptabs-sub > li.x-grouptabs-strip-active', true).removeClass('x-grouptabs-strip-active');
        }
        this.expandGroup(this.getGroupEl(group));
        if (group !== this.activeGroup) {
            return this.setActiveGroup(group);
        }
    },

    getFrameHeight: function() {
        var h = this.el.getFrameWidth('tb');
        h += (this.tbar ? this.tbar.getHeight() : 0) +
        (this.bbar ? this.bbar.getHeight() : 0);

        return h;
    },

    adjustBodyWidth: function(w) {
        return w - this.tabWidth;
    }
});


/*!
* Ext JS Library 3.2.1
* Copyright(c) 2006-2010 Ext JS, Inc.
* licensing@extjs.com
* http://www.extjs.com/license
*/
Ext.ux.GroupTab = Ext.extend(Ext.Container, {
    mainItem: 0,

    expanded: true,

    deferredRender: true,

    activeTab: null,

    idDelimiter: '__',

    headerAsText: false,

    frame: false,

    hideBorders: true,

    initComponent: function(config) {
        Ext.apply(this, config);
        this.frame = false;

        Ext.ux.GroupTab.superclass.initComponent.call(this);

        this.addEvents('activate', 'deactivate', 'changemainitem', 'beforetabchange', 'tabchange');

        this.setLayout(new Ext.layout.CardLayout({
            deferredRender: this.deferredRender
        }));

        if (!this.stack) {
            this.stack = Ext.TabPanel.AccessStack();
        }

        this.initItems();

        this.on('beforerender', function() {
            this.groupEl = this.ownerCt.getGroupEl(this);
        }, this);

        this.on('add', this.onAdd, this, {
            target: this
        });
        this.on('remove', this.onRemove, this, {
            target: this
        });

        if (this.mainItem !== undefined) {
            var item = (typeof this.mainItem == 'object') ? this.mainItem : this.items.get(this.mainItem);
            delete this.mainItem;
            this.setMainItem(item);
        }
    },

    /**
    * Sets the specified tab as the active tab. This method fires the {@link #beforetabchange} event which
    * can return false to cancel the tab change.
    * @param {String/Panel} tab The id or tab Panel to activate
    */
    setActiveTab: function(item) {
        item = this.getComponent(item);
        if (!item) {
            return false;
        }
        if (!this.rendered) {
            this.activeTab = item;
            return true;
        }
        if (this.activeTab != item && this.fireEvent('beforetabchange', this, item, this.activeTab) !== false) {
            if (this.activeTab && this.activeTab != this.mainItem) {
                var oldEl = this.getTabEl(this.activeTab);
                if (oldEl) {
                    Ext.fly(oldEl).removeClass('x-grouptabs-strip-active');
                }
            }
            var el = this.getTabEl(item);
            Ext.fly(el).addClass('x-grouptabs-strip-active');
            this.activeTab = item;
            this.stack.add(item);

            this.layout.setActiveItem(item);
            if (this.layoutOnTabChange && item.doLayout) {
                item.doLayout();
            }
            if (this.scrolling) {
                this.scrollToTab(item, this.animScroll);
            }

            this.fireEvent('tabchange', this, item);
            return true;
        }
        return false;
    },

    getTabEl: function(item) {
        if (item == this.mainItem) {
            return this.groupEl;
        }
        return Ext.TabPanel.prototype.getTabEl.call(this, item);
    },

    onRender: function(ct, position) {
        Ext.ux.GroupTab.superclass.onRender.call(this, ct, position);

        this.strip = Ext.fly(this.groupEl).createChild({
            tag: 'ul',
            cls: 'x-grouptabs-sub'
        });

        this.tooltip = new Ext.ToolTip({
            target: this.groupEl,
            delegate: 'a.x-grouptabs-text',
            trackMouse: true,
            renderTo: document.body,
            listeners: {
                beforeshow: function(tip) {
                    var item = (tip.triggerElement.parentNode === this.mainItem.tabEl)
                       ? this.mainItem
                       : this.findById(tip.triggerElement.parentNode.id.split(this.idDelimiter)[1]);

                    if (!item.tabTip) {
                        return false;
                    }
                    tip.body.dom.innerHTML = item.tabTip;
                },
                scope: this
            }
        });

        if (!this.itemTpl) {
            var tt = new Ext.Template('<li class="{cls}" id="{id}">', '<a onclick="return false;" class="x-grouptabs-text {iconCls}">{text}</a>', '</li>');
            tt.disableFormats = true;
            tt.compile();
            Ext.ux.GroupTab.prototype.itemTpl = tt;
        }

        this.items.each(this.initTab, this);
    },

    afterRender: function() {
        Ext.ux.GroupTab.superclass.afterRender.call(this);

        if (this.activeTab !== undefined) {
            var item = (typeof this.activeTab == 'object') ? this.activeTab : this.items.get(this.activeTab);
            delete this.activeTab;
            this.setActiveTab(item);
        }
    },

    // private
    initTab: function(item, index) {
        var before = this.strip.dom.childNodes[index];
        var p = Ext.TabPanel.prototype.getTemplateArgs.call(this, item);

        if (item === this.mainItem) {
            item.tabEl = this.groupEl;
            p.cls += ' x-grouptabs-main-item';
        }

        var el = before ? this.itemTpl.insertBefore(before, p) : this.itemTpl.append(this.strip, p);

        item.tabEl = item.tabEl || el;

        item.on('disable', this.onItemDisabled, this);
        item.on('enable', this.onItemEnabled, this);
        item.on('titlechange', this.onItemTitleChanged, this);
        item.on('iconchange', this.onItemIconChanged, this);
        item.on('beforeshow', this.onBeforeShowItem, this);
    },

    setMainItem: function(item) {
        item = this.getComponent(item);
        if (!item || this.fireEvent('changemainitem', this, item, this.mainItem) === false) {
            return;
        }

        this.mainItem = item;
    },

    getMainItem: function() {
        return this.mainItem || null;
    },

    // private
    onBeforeShowItem: function(item) {
        if (item != this.activeTab) {
            this.setActiveTab(item);
            return false;
        }
    },

    // private
    onAdd: function(gt, item, index) {
        if (this.rendered) {
            this.initTab.call(this, item, index);
        }
    },

    // private
    onRemove: function(tp, item) {
        Ext.destroy(Ext.get(this.getTabEl(item)));
        this.stack.remove(item);
        item.un('disable', this.onItemDisabled, this);
        item.un('enable', this.onItemEnabled, this);
        item.un('titlechange', this.onItemTitleChanged, this);
        item.un('iconchange', this.onItemIconChanged, this);
        item.un('beforeshow', this.onBeforeShowItem, this);
        if (item == this.activeTab) {
            var next = this.stack.next();
            if (next) {
                this.setActiveTab(next);
            }
            else if (this.items.getCount() > 0) {
                this.setActiveTab(0);
            }
            else {
                this.activeTab = null;
            }
        }
    },

    // private
    onBeforeAdd: function(item) {
        var existing = item.events ? (this.items.containsKey(item.getItemId()) ? item : null) : this.items.get(item);
        if (existing) {
            this.setActiveTab(item);
            return false;
        }
        Ext.TabPanel.superclass.onBeforeAdd.apply(this, arguments);
        var es = item.elements;
        item.elements = es ? es.replace(',header', '') : es;
        item.border = (item.border === true);
    },

    // private
    onItemDisabled: Ext.TabPanel.prototype.onItemDisabled,
    onItemEnabled: Ext.TabPanel.prototype.onItemEnabled,

    // private
    onItemTitleChanged: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).child('a.x-grouptabs-text', true).innerHTML = item.title;
        }
    },

    //private
    onItemIconChanged: function(item, iconCls, oldCls) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).child('a.x-grouptabs-text').replaceClass(oldCls, iconCls);
        }
    },

    beforeDestroy: function() {
        Ext.TabPanel.prototype.beforeDestroy.call(this);
        this.tooltip.destroy();
    }
});

Ext.reg('grouptab', Ext.ux.GroupTab);
Ext.reg('grouptabpanel', Ext.ux.GroupTabPanel);


eBook.File.ListMessageColumn = Ext.extend(Ext.list.Column, {
    constructor: function(c) {
        c.tpl = new Ext.XTemplate('<div class="eb-message-column eb-message-type-{[this.getMessageType(values)]}"></div>',
                                {
                                    getMessageType: function(values) {
                                        if (eBook.Interface.currentFile) {
                                            if (eBook.Interface.serviceYearend && eBook.Interface.serviceYearend.c && values.type == "AL") {
                                                return 'lock';
                                            }
                                            if (eBook.Interface.serviceBizTax && eBook.Interface.serviceBizTax.c && values.type == "FI") {
                                                return 'lock';
                                            }
                                            return eBook.Interface.center.fileMenu.getItemMessageType(values.id);
                                        }
                                        return '';
                                    }
                                });

        eBook.File.ListMessageColumn.superclass.constructor.call(this, c);
    }
});

Ext.reg('lvmessagestatus', eBook.File.ListMessageColumn);

eBook.File.WorksheetsList = Ext.extend(Ext.list.ListView, {
    initComponent: function () {
        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                url: 'js/json/worksheettypes-' + this.wtype + '-' + eBook.Interface.Culture + '.json',
                method: 'GET',
                fields: [
		                { name: 'id' },
		                { name: 'name' },
		                { name: 'ruleApp' },
		                { name: 'type' },
                         { name: 'AJ', type: 'int' },
	                ],
                root: 'types', autoLoad: true
            })
            , margins: { top: 5, right: 10, bottom: 5, left: 10 }
            , layout: 'fit'
            , autoScroll: true
            , multiSelect: false
            , emptyText: "No worksheets"
            , reserveScrollOffset: false
            , hideHeaders: true
            , border: false
            , listeners: {
                'click': {
                    fn: function (lv, idx, nd, e) {
                        var rec = lv.getStore().getAt(idx);
                        var el = Ext.get(nd).child('.eb-message-type-lock');
                        if (el) rec.closed = true;
                        eBook.Interface.center.fileMenu.openWorksheet(rec);
                    }
                    , scope: this
                }
            }
            , columns: [{ xtype: 'lvmessagestatus', width: 0.05, dataIndex: 'id' }, {
                header: "",
                // width: 1,
                dataIndex: 'name'
            }]
        });
        eBook.File.WorksheetsList.superclass.initComponent.apply(this, arguments);
    },
    getData: function () {
        console.log(this);
    }
    , refreshMe: function () {
        var aj = eBook.Interface.currentFile.get('AssessmentYear');

        this.store.clearFilter();
        this.store.filterBy(function (rec, id) {
            var raj = rec.get('AJ');
            if (!raj) return true;
            if (raj == 0) return true;
            if (raj >= aj) return true;
            return false;
        }, this);
        this.getStore().reload();
        this.refresh();
    }
});
/*
{ width: .1, dataIndex: 'id' }, new eBook.List.QualityColumn({
                            header: '',
                            width: .05,
                            dataIndex: 'id'
                        }),*/
        
Ext.reg('worksheetlist', eBook.File.WorksheetsList);


eBook.File.WorksheetsTab = Ext.extend(Ext.ux.GroupTabPanel, {
    initComponent: function() {
        Ext.apply(this, {
            tabWidth: 130,
    		activeGroup: 0,
    		items: [{
    			        mainItem: 0,
    			        groupTitle: 'Worksheets',
    			        iconCls: 'eBook-bundle-tree-worksheet ',
    			        items: [{ xtype: 'worksheetlist', wtype: '', title: 'Worksheets', iconCls: 'eBook-bundle-tree-worksheet' }
    			                , { xtype: 'worksheetlist', wtype: 'AL', title: eBook.Language.YEAREND, iconCls: 'eBook-bundle-tree-worksheet' }
    			                , { xtype: 'worksheetlist', wtype: 'FI', title: eBook.Language.BIZTAXPREP, iconCls: 'eBook-bundle-tree-worksheet-tax'}]
    			        , expanded:true
                    }]
            
        });
        eBook.File.WorksheetsTab.superclass.initComponent.apply(this, arguments);
    }
    , onItemClick: function(dataview, nodeindex, node, evt) {
    }
    , getStore: function() {
        return this.items.items[0].items.items[0].getStore();
    }
    , redrawWorksheetsView: function() {
        var its = this.items.items[0].items.items;
        for (var i = 0; i < its.length; i++) {
            if(its[i].rendered) its[i].refreshMe();
        }
    }
});


Ext.reg('worksheetstab', eBook.File.WorksheetsTab);







Ext.ns('Ext.ux');

Ext.ux.IconTabPanel = Ext.extend(Ext.Panel, {
    deferredRender: true,
    tabWidth: 120,
    minTabWidth: 30,
    resizeTabs: false,
    enableTabScroll: false,
    scrollIncrement: 0,
    scrollRepeatInterval: 400,
    scrollDuration: 0.35,
    animScroll: true,
    tabPosition: 'left', // left or right
    baseCls: 'ux-icontab-panel',
    autoTabs: false,
    autoTabSelector: 'div.ux-icontab',
    activeTab: undefined,
    tabMargin: 2,
    plain: false,
    wheelIncrement: 20,
    idDelimiter: '__',
    itemCls: 'ux-icontab-item',
    elements: 'body',
    headerAsText: false,
    frame: false,
    hideBorders: true,
    initComponent: function() {
        this.frame = false;
        Ext.apply(this, { defaults: { headerAsText: false, header: false} });
        Ext.ux.IconTabPanel.superclass.initComponent.apply(this, arguments);
        this.addEvents(
            'beforetabchange',
            'tabchange',
            'contextmenu'
        );
        this.setLayout(new Ext.layout.CardLayout(Ext.apply({
            layoutOnCardChange: this.layoutOnTabChange,
            deferredRender: this.deferredRender
        }, this.layoutConfig)));

        if (!this.stack) {
            this.stack = Ext.ux.IconTabPanel.AccessStack();
        }
        this.initItems();
    },
    getLayoutTarget: function() {
        return this.innerBody;
    },

    // private
    getContentTarget: function() {
        return this.innerBody;
    },
    // private
    onRender: function(ct, position) {
        Ext.ux.IconTabPanel.superclass.onRender.call(this, ct, position);


        if (this.enableTabScroll) {
            this.tabStripWrap = this.body.createChild({ cls: 'ux-icontab-tabstrip-wrap', style: 'width:' + this.tabWidth + 'px;' });
            this.tabStripWrapScrollUp = this.tabStripWrap.createChild({ cls: 'ux-icontab-tabstrip-scroll-up' });
            this.tabStripWrapBody = this.tabStripWrap.createChild({ cls: 'ux-icontab-tabstrip-body' });
            this.tabStrip = this.tabStripWrapBody.createChild({ cls: 'ux-icontab-tabstrip', style: 'width:100%;' }); // float left and/or fix width;
            this.tabStripWrapScrollDown = this.tabStripWrap.createChild({ cls: 'ux-icontab-tabstrip-scroll-down' });

            this.upRepeater = new Ext.util.ClickRepeater(this.tabStripWrapScrollUp, {
                interval: this.scrollRepeatInterval,
                handler: this.onScrollUp,
                scope: this
            });
            this.downRepeater = new Ext.util.ClickRepeater(this.tabStripWrapScrollDown, {
                interval: this.scrollRepeatInterval,
                handler: this.onScrollDown,
                scope: this

            });
        } else {
            this.tabStrip = this.body.createChild({ cls: 'ux-icontab-tabstrip', style: 'width:' + this.tabWidth + 'px;' }); // float left and/or fix width;
        }
        this.innerBody = this.body.createChild({ cls: 'ux-icontab-innerbody' }); // float right and/or fix width;

        if (this.tabStripTitle) {
            this.tabStrip.createChild({ cls: 'ux-icontab-tabstrip-title', html: this.tabStripTitle });
        }

        this.body.addClass('ux-icontab-panel-body-' + this.tabPosition);

        if (!this.itemTpl) {
            var tt = new Ext.XTemplate(
                 '<div class="ux-icontab-tabItem {cls}" id="{id}" idx="{idx}">',
                 '<tpl if="values.tabIconCls"><div class="ux-icontab-tabItem-icon {tabIconCls}"></div></tpl>',
                 '<div class="ux-icontab-tabItem-txt"><span class="ux-icontab-strip-text">{text}</span></div>',
                 '</div>'
            );
            tt.disableFormats = true;
            tt.compile();
            Ext.ux.IconTabPanel.prototype.itemTpl = tt;
        }

        this.items.each(this.initTab, this);
    },

    // private
    afterRender: function() {
        Ext.ux.IconTabPanel.superclass.afterRender.call(this);

        if (this.activeTab !== undefined) {
            var item = Ext.isObject(this.activeTab) ? this.activeTab : this.items.get(this.activeTab);
            delete this.activeTab;
            this.setActiveTab(item);
        }
    },

    // private
    initEvents: function() {
        Ext.ux.IconTabPanel.superclass.initEvents.call(this);

        this.mon(this.tabStrip, {
            scope: this,
            mousedown: this.onStripMouseDown,
            contextmenu: this.onStripContextMenu
        });

        if (this.enableTabScroll) {
            this.mon(this.tabStripWrapBody, 'mousewheel', this.onWheel, this);
        }
    },

    // private
    findTargets: function(e) {
        var item = null,
            itemEl = e.getTarget('div.ux-icontab-tabItem ', this.tabStrip);

        if (itemEl) {
            item = this.getComponent(itemEl.id.split(this.idDelimiter)[1]);
            if (item.disabled) {
                return {
                    close: null,
                    item: null,
                    el: null
                };
            }
        }
        return {
            close: e.getTarget('.ux-icontab-strip-close', this.tabStrip),
            item: item,
            el: itemEl
        };
    },

    // private
    onStripMouseDown: function(e) {
        if (e.button !== 0) {
            return;
        }
        e.preventDefault();
        var t = this.findTargets(e);
        if (t.close) {
            if (t.item.fireEvent('beforeclose', t.item) !== false) {
                t.item.fireEvent('close', t.item);
                this.remove(t.item);
            }
            return;
        }
        if (t.item && t.item != this.activeTab) {
            this.setActiveTab(t.item);
        }
    },

    // private
    onStripContextMenu: function(e) {
        e.preventDefault();
        var t = this.findTargets(e);
        if (t.item) {
            this.fireEvent('contextmenu', this, t.item, e);
        }
    },

    /**
    * True to scan the markup in this tab panel for <tt>{@link #autoTabs}</tt> using the
    * <tt>{@link #autoTabSelector}</tt>
    * @param {Boolean} removeExisting True to remove existing tabs
    */
    readTabs: function(removeExisting) {
        if (removeExisting === true) {
            this.items.each(function(item) {
                this.remove(item);
            }, this);
        }
        var tabs = this.el.query(this.autoTabSelector);
        for (var i = 0, len = tabs.length; i < len; i++) {
            var tab = tabs[i],
                title = tab.getAttribute('title');
            if(!this.enableTabTitles) tab.removeAttribute('title');
            this.add({
                title: title,
                contentEl: tab
            });
        }
    },

    // private
    initTab: function(item, index) {
        var childIdx = this.tabStripTitle ? index + 1 : index;
        var before = this.tabStrip.dom.childNodes[childIdx],
            p = this.getTemplateArgs(item);
        p.idx = index;
        var el = before ?
                 this.itemTpl.insertBefore(before, p) :
                 this.itemTpl.append(this.tabStrip, p),
            cls = 'ux-icontab-strip-over',
            tabEl = Ext.get(el);

        tabEl.hover(function() {
            if (!item.disabled) {
                tabEl.addClass(cls);
            }
        }, function() {
            tabEl.removeClass(cls);
        });

        if (item.tabTip) {
            tabEl.qtip = item.tabTip;
        }
        item.tabEl = el;

        // Route *keyboard triggered* click events to the tab strip mouse handler.
        /*
        tabEl.select('a').on('click', function(e) {
        if (!e.getPageX()) {
        this.onStripMouseDown(e);
        }
        }, this, { preventDefault: true });
        */
        item.on({
            scope: this,
            disable: this.onItemDisabled,
            enable: this.onItemEnabled,
            titlechange: this.onItemTitleChanged,
            iconchange: this.onItemIconChanged,
            beforeshow: this.onBeforeShowItem
        });
    },

    getTemplateArgs: function(item) {

        var cls = item.closable ? 'ux-icontab-strip-closable' : '';
        if (item.disabled) {
            cls += ' x-item-disabled';
        }
        if (item.tabIconCls) {
            cls += ' ux-icontab-with-icon';
        }
        if (item.tabCls) {
            cls += ' ' + item.tabCls;
        }
        if (this.itemsCls) {
            cls += ' ' + this.itemsCls;
        }

        return {
            id: this.id + this.idDelimiter + item.getItemId(),
            text: item.title,
            cls: cls,
            iconCls: item.iconCls || '',
            tabIconCls: item.tabIconCls
        };
        return item;
    },

    // private
    onAdd: function(c) {
        Ext.ux.IconTabPanel.superclass.onAdd.call(this, c);
        if (this.rendered) {
            var items = this.items;
            this.initTab(c, items.indexOf(c));
            this.delegateUpdates();
        }
    },

    // private
    onBeforeAdd: function(item) {
        var existing = item.events ? (this.items.containsKey(item.getItemId()) ? item : null) : this.items.get(item);
        if (existing) {
            this.setActiveTab(item);
            return false;
        }
        Ext.ux.IconTabPanel.superclass.onBeforeAdd.apply(this, arguments);
        var es = item.elements;
        item.elements = es ? es.replace(',left', '') : es;
        item.border = (item.border === true);
    },

    // private
    onRemove: function(c) {
        var te = Ext.get(c.tabEl);
        // check if the tabEl exists, it won't if the tab isn't rendered
        if (te) {
            te.select('a').removeAllListeners();
            Ext.destroy(te);
        }
        Ext.ux.IconTabPanel.superclass.onRemove.call(this, c);
        this.stack.remove(c);
        delete c.tabEl;
        c.un('disable', this.onItemDisabled, this);
        c.un('enable', this.onItemEnabled, this);
        c.un('titlechange', this.onItemTitleChanged, this);
        c.un('iconchange', this.onItemIconChanged, this);
        c.un('beforeshow', this.onBeforeShowItem, this);
        if (c == this.activeTab) {
            var next = this.stack.next();
            if (next) {
                this.setActiveTab(next);
            } else if (this.items.getCount() > 0) {
                this.setActiveTab(0);
            } else {
                this.setActiveTab(null);
            }
        }
        if (!this.destroying) {
            this.delegateUpdates();
        }
    },

    // private
    onBeforeShowItem: function(item) {
        if (item != this.activeTab) {
            this.setActiveTab(item);
            return false;
        }
    },

    // private
    onItemDisabled: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).addClass('x-item-disabled');
        }
        this.stack.remove(item);
    },

    // private
    onItemEnabled: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).removeClass('x-item-disabled');
        }
    },

    // private
    onItemTitleChanged: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).child('span.ux-icontab-strip-text', true).innerHTML = item.title;
        }
    },

    //private
    onItemIconChanged: function(item, iconCls, oldCls) {
        var el = this.getTabEl(item);
        if (el) {
            el = Ext.get(el);
            el.child('span.ux-icontab-strip-text').replaceClass(oldCls, iconCls);
            el[Ext.isEmpty(iconCls) ? 'removeClass' : 'addClass']('ux-icontab-with-icon');
        }
    },

    getTabEl: function(item) {
        var c = this.getComponent(item);
        return c ? c.tabEl : null;
    },

    // private
    onResize: function(adjWidth, adjHeight, rawWidth, rawHeight) {
        var w = adjWidth,
            h = adjHeight;

        if (Ext.isDefined(w) || Ext.isDefined(h)) {
            if (!this.collapsed) {
                // First, set the the Panel's body width.
                // If we have auto-widthed it, get the resulting full offset width so we can size the Toolbars to match
                // The Toolbars must not buffer this resize operation because we need to know their heights.

                if (Ext.isNumber(w)) {
                    this.body.setWidth(w = this.adjustBodyWidth(w - this.getFrameWidth()));
                } else if (w == 'auto') {
                    w = this.body.setWidth('auto').dom.offsetWidth;
                } else {
                    w = this.body.dom.offsetWidth;
                }
                var iw = this.body.getBox(true).width - this.tabStrip.getWidth();
                this.innerBody.setWidth(iw);

                if (this.tbar) {
                    this.tbar.setWidth(w);
                    if (this.topToolbar) {
                        this.topToolbar.setSize(w);
                    }
                }
                if (this.bbar) {
                    this.bbar.setWidth(w);
                    if (this.bottomToolbar) {
                        this.bottomToolbar.setSize(w);
                        // The bbar does not move on resize without this.
                        if (Ext.isIE) {
                            this.bbar.setStyle('position', 'static');
                            this.bbar.setStyle('position', '');
                        }
                    }
                }
                if (this.footer) {
                    this.footer.setWidth(w);
                    if (this.fbar) {
                        this.fbar.setSize(Ext.isIE ? (w - this.footer.getFrameWidth('lr')) : 'auto');
                    }
                }

                // At this point, the Toolbars must be layed out for getFrameHeight to find a result.
                if (Ext.isNumber(h)) {
                    h = Math.max(0, h - this.getFrameHeight());
                    //h = Math.max(0, h - (this.getHeight() - this.body.getHeight()));
                    this.body.setHeight(h);
                } else if (h == 'auto') {
                    this.body.setHeight(h);
                }

                if (this.disabled && this.el._mask) {
                    this.el._mask.setSize(this.el.dom.clientWidth, this.el.getHeight());
                }
            } else {
                // Adds an event to set the correct height afterExpand.  This accounts for the deferHeight flag in panel
                this.queuedBodySize = { width: w, height: h };
                if (!this.queuedExpand && this.allowQueuedExpand !== false) {
                    this.queuedExpand = true;
                    this.on('expand', function() {
                        delete this.queuedExpand;
                        this.onResize(this.queuedBodySize.width, this.queuedBodySize.height);
                    }, this, { single: true });
                }
            }
            this.onBodyResize(w, h);
        }
        this.syncShadow();
        if (this.tabStripWrapBody) {
            var h = this.tabStripWrap.getHeight() - this.tabStripWrapScrollUp.getHeight() - this.tabStripWrapScrollDown.getHeight();
            this.tabStripWrapBody.setHeight(h);
        }
        //Ext.ux.IconTabPanel.superclass.superclass.onResize.call(this, adjWidth, adjHeight, rawWidth, rawHeight);
        // panel since we've completly override panels onResize
        Ext.Panel.superclass.onResize.call(this, adjWidth, adjHeight, rawWidth, rawHeight);
        this.delegateUpdates();

    },

    /**
    * Suspends any internal calculations or scrolling while doing a bulk operation. See {@link #endUpdate}
    */
    beginUpdate: function() {
        this.suspendUpdates = true;
    },

    /**
    * Resumes calculations and scrolling at the end of a bulk operation. See {@link #beginUpdate}
    */
    endUpdate: function() {
        this.suspendUpdates = false;
        this.delegateUpdates();
    },

    /**
    * Hides the tab strip item for the passed tab
    * @param {Number/String/Panel} item The tab index, id or item
    */
    hideTabStripItem: function(item) {
        item = this.getComponent(item);
        var el = this.getTabEl(item);
        this.stack.remove(item);
        if (el) {
            Ext.get(el).addClass('x-hide-display');
            this.delegateUpdates();
        }
        
    },

    /**
    * Unhides the tab strip item for the passed tab
    * @param {Number/String/Panel} item The tab index, id or item
    */
    unhideTabStripItem: function(item) {
        item = this.getComponent(item);
        var el = this.getTabEl(item);
        if (el) {
            Ext.get(el).removeClass('x-hide-display');
            this.delegateUpdates();
        }
    },

    // private
    delegateUpdates: function() {
        if (this.suspendUpdates) {
            return;
        }
        if (this.resizeTabs && this.rendered) {
            this.autoSizeTabs();
        }
        if (this.enableTabScroll && this.rendered) {
            this.autoScrollTabs();
        }
    },

    // private
    autoSizeTabs: function() {
        var count = this.items.length,
            ce = this.tabPosition != 'bottom' ? 'header' : 'footer',
            ow = this[ce].dom.offsetWidth,
            aw = this[ce].dom.clientWidth;

        if (!this.resizeTabs || count < 1 || !aw) { // !aw for display:none
            return;
        }

        var each = Math.max(Math.min(Math.floor((aw - 4) / count) - this.tabMargin, this.tabWidth), this.minTabWidth); // -4 for float errors in IE
        this.lastTabWidth = each;
        var lis = this.strip.query('li:not(.x-tab-edge)');
        for (var i = 0, len = lis.length; i < len; i++) {
            var li = lis[i],
                inner = Ext.fly(li).child('.x-tab-strip-inner', true),
                tw = li.offsetWidth,
                iw = inner.offsetWidth;
            inner.style.width = (each - (tw - iw)) + 'px';
        }
    },

    // private
    adjustBodyWidth: function(w) {
        if (this.header) {
            this.header.setWidth(w);
        }
        if (this.footer) {
            this.footer.setWidth(w);
        }
        return w;
    },

    /**
    * Sets the specified tab as the active tab. This method fires the {@link #beforetabchange} event which
    * can <tt>return false</tt> to cancel the tab change.
    * @param {String/Number} item
    * The id or tab Panel to activate. This parameter may be any of the following:
    * <div><ul class="mdetail-params">
    * <li>a <b><tt>String</tt></b> : representing the <code>{@link Ext.Component#itemId itemId}</code>
    * or <code>{@link Ext.Component#id id}</code> of the child component </li>
    * <li>a <b><tt>Number</tt></b> : representing the position of the child component
    * within the <code>{@link Ext.Container#items items}</code> <b>property</b></li>
    * </ul></div>
    * <p>For additional information see {@link Ext.util.MixedCollection#get}.
    */
    setActiveTab: function(item) {
        item = this.getComponent(item);
        if (this.fireEvent('beforetabchange', this, item, this.activeTab) === false) {
            return;
        }
        if (!this.rendered) {
            this.activeTab = item;
            return;
        }
        if (this.activeTab != item) {
            if (this.activeTab) {
                var oldEl = this.getTabEl(this.activeTab);
                if (oldEl) {
                    Ext.fly(oldEl).removeClass('ux-icontab-strip-active');
                }
            }
            this.activeTab = item;
            if (item) {
                var el = this.getTabEl(item);
                Ext.fly(el).addClass('ux-icontab-strip-active');
                this.stack.add(item);

                this.layout.setActiveItem(item);
                if (this.scrolling) {
                    this.scrollToTab(item, this.animScroll);
                }
            }
            this.fireEvent('tabchange', this, item);
        }
    },

    /**
    * Returns the Component which is the currently active tab. <b>Note that before the TabPanel
    * first activates a child Component, this method will return whatever was configured in the
    * {@link #activeTab} config option.</b>
    * @return {BoxComponent} The currently active child Component if one <i>is</i> active, or the {@link #activeTab} config value.
    */
    getActiveTab: function() {
        return this.activeTab || null;
    },

    /**
    * Gets the specified tab by id.
    * @param {String} id The tab id
    * @return {Panel} The tab
    */
    getItem: function(item) {
        return this.getComponent(item);
    },

    // private
    autoScrollTabs: function() {
        /*
        this.tabStripWrap 
        this.tabStripWrapScrollUp 
        this.tabStripWrapBody
        this.tabStrip
        this.tabStripWrapScrollDown 
        */

        this.pos = this.tabStrip; //this.tabPosition == 'bottom' ? this.footer : this.header;
        var count = this.items.length,
            ow = this.pos.dom.offsetHeight,
            tw = this.pos.dom.clientHeight,
            wrap = this.tabStripWrapBody,
            wd = wrap.dom,
            cw = wd.offsetHeight,
            pos = this.getScrollPos(),
            l = this.tabStripWrapBody.getOffsetsTo(this.tabStrip)[1] + pos;


        if (!this.enableTabScroll || count < 1 || cw < 20) { // 20 to prevent display:none issues
            return;
        }
        if (l <= tw) {
            wd.scrollTop = 0;
            wrap.setHeight(tw);
            if (this.scrolling) {
                this.scrolling = false;
                this.pos.removeClass('ux-icontab-scrolling');
                this.tabStripWrapScrollUp.hide();
                this.tabStripWrapScrollDown.hide();
                // See here: http://extjs.com/forum/showthread.php?t=49308&highlight=isSafari
                if (Ext.isAir || Ext.isWebKit) {
                    wd.style.marginLeft = '';
                    wd.style.marginRight = '';
                }
            }
        } else {
            if (!this.scrolling) {
                this.pos.addClass('ux-icontab-scrolling');
                // See here: http://extjs.com/forum/showthread.php?t=49308&highlight=isSafari
                /* if (Ext.isAir || Ext.isWebKit) {
                wd.style.marginLeft = '18px';
                wd.style.marginRight = '18px';
                }*/
            }
            tw -= wrap.getMargins('tb');
            wrap.setWidth(tw > 20 ? tw : 20);
            if (!this.scrolling) {

                this.tabStripWrapScrollUp.show();
                this.tabStripWrapScrollDown.show();

            }
            this.scrolling = true;
            if (pos > (l - tw)) { // ensure it stays within bounds
                wd.scrollTop = l - tw;
            } else { // otherwise, make sure the active tab is still visible
                this.scrollToTab(this.activeTab, false);
            }
            this.updateScrollButtons();
        }
    },

    // private
    createScrollers: function() {
        this.pos.addClass('ux-icontab-scrolling-' + this.tabPosition);
        var h = this.stripWrap.dom.offsetHeight;

        // left
        var sl = this.pos.insertFirst({
            cls: 'ux-icontab-scroller-left'
        });
        sl.setHeight(h);
        sl.addClassOnOver('ux-icontab-scroller-left-over');
        this.leftRepeater = new Ext.util.ClickRepeater(sl, {
            interval: this.scrollRepeatInterval,
            handler: this.onScrollLeft,
            scope: this
        });
        this.scrollLeft = sl;

        // right
        var sr = this.pos.insertFirst({
            cls: 'ux-icontab-scroller-right'
        });
        sr.setHeight(h);
        sr.addClassOnOver('ux-icontab-scroller-right-over');
        this.rightRepeater = new Ext.util.ClickRepeater(sr, {
            interval: this.scrollRepeatInterval,
            handler: this.onScrollRight,
            scope: this
        });
        this.scrollRight = sr;
    },

    // private
    getScrollWidth: function() {
        return this.edge.getOffsetsTo(this.tabStrip)[0] + this.getScrollPos();
    },
    getScrollHeight: function() {
        return this.tabStripWrapBody.getOffsetsTo(this.tabStrip)[1] + this.getScrollPos();
    },
    // private
    getScrollPos: function() {
        return parseInt(this.tabStrip.dom.scrollTop, 10) || 0;
    },

    // private
    getScrollArea: function() {
        return parseInt(this.tabStrip.dom.clientHeight, 10) || 0;
    },

    // private
    getScrollAnim: function() {
        return { duration: this.scrollDuration, callback: this.updateScrollButtons, scope: this };
    },

    // private
    getScrollIncrement: function() {
        return this.scrollIncrement || (this.resizeTabs ? this.lastTabHeight + 2 : 100);
    },

    /**
    * Scrolls to a particular tab if tab scrolling is enabled
    * @param {Panel} item The item to scroll to
    * @param {Boolean} animate True to enable animations
    */

    scrollToTab: function(item, animate) {
        if (!item) {
            return;
        }
        var el = this.getTabEl(item),
            pos = this.getScrollPos(),
            area = this.getScrollArea(),
            left = Ext.fly(el).getOffsetsTo(this.tabStrip)[1] + pos,
            right = left + el.offsetHeight;
        if (left < pos) {
            this.scrollTo(left, animate);
        } else if (right > (pos + area)) {
            this.scrollTo(right - area, animate);
        }
    },

    // private
    scrollTo: function(pos, animate) {
        this.tabStrip.scrollTo('top', pos, animate ? this.getScrollAnim() : false);
        if (!animate) {
            this.updateScrollButtons();
        }
    },

    onWheel: function(e) {
        var d = e.getWheelDelta() * this.wheelIncrement * -1;
        e.stopEvent();

        var pos = this.getScrollPos(),
            newpos = pos + d;
        //,
        //sh = this.getScrollHeight(); // -this.getScrollArea();

        var s = newpos; // Math.max(0, Math.min(sh, newpos));
        if (s != pos) {
            this.scrollTo(s, false);
        }
    },

    // private
    onScrollDown: function() {
        var sw = this.getScrollHeight(), //- this.getScrollArea()
            pos = this.getScrollPos(),
            s = pos + this.getScrollIncrement(); //Math.min(sw, pos + this.getScrollIncrement());
        if (s != pos) {
            this.scrollTo(s, this.animScroll);
        }
    },

    // private
    onScrollUp: function() {
        var pos = this.getScrollPos(),
            s = Math.max(0, pos - this.getScrollIncrement());
        if (s != pos) {
            this.scrollTo(s, this.animScroll);
        }
    },

    // private
    updateScrollButtons: function() {
        /* TODO
        var pos = this.getScrollPos();
        this.scrollLeft[pos === 0 ? 'addClass' : 'removeClass']('x-tab-scroller-left-disabled');
        this.scrollRight[pos >= (this.getScrollWidth() - this.getScrollArea()) ? 'addClass' : 'removeClass']('x-tab-scroller-right-disabled');
        */
    },

    // private
    beforeDestroy: function() {
        Ext.destroy(this.upRepeater, this.downRepeater); // TODO
        this.deleteMembers('strip', 'edge', 'scrollLeft', 'scrollRight', 'stripWrap');
        this.activeTab = null;
        Ext.ux.IconTabPanel.superclass.beforeDestroy.apply(this);
    }


});
Ext.reg('icontabpanel', Ext.ux.IconTabPanel);

/**
* See {@link #setActiveTab}. Sets the specified tab as the active tab. This method fires
* the {@link #beforetabchange} event which can <tt>return false</tt> to cancel the tab change.
* @param {String/Panel} tab The id or tab Panel to activate
* @method activate
*/
Ext.ux.IconTabPanel.prototype.activate = Ext.ux.IconTabPanel.prototype.setActiveTab;

// private utility class used by IconTabPanel (copy from tabpanel)
Ext.ux.IconTabPanel.AccessStack = function() {
    var items = [];
    return {
        add: function(item) {
            items.push(item);
            if (items.length > 10) {
                items.shift();
            }
        },

        remove: function(item) {
            var s = [];
            for (var i = 0, len = items.length; i < len; i++) {
                if (items[i] != item) {
                    s.push(items[i]);
                }
            }
            items = s;
        },

        next: function() {
            return items.pop();
        }
    };
};

// split up

eBook.File.HomeHeaderPanel = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for="."></tpl>')
      , initComponent: function () {
          Ext.apply(this, {
              html: '<div>open client</div>'
            , border: false
          });
          eBook.File.HomeHeaderPanel.superclass.initComponent.apply(this, arguments);
      }
    , setLastImportDate: function (dte) {
        var lid = this.body.child('.proacc-importdate');
        if (lid) {
            lid.update(dte.format('d/m/Y H:i:s'));
        }
    }
    , updatingTax: function () {
        var txe = this.body.child('.eBook-menu-info-tax');
        if (txe) {
            txe.addClass('eBook-loading-ico');
        }
    }
    , setTax: function (tx) {
        var txe = this.body.child('.eBook-menu-info-tax');
        if (txe) {
            txe.removeClass('eBook-loading-ico');
            txe.update(tx == null ? '' : Ext.util.Format.number(tx, '0.000,00/i') + '€ (eBook)');
        }
        // this.mymenu.setTax(tx);
    }

    , setBizTax: function (tx) {
        var txe = this.body.child('.eBook-menu-info-tax-biztax');
        if (txe) {
            txe.removeClass('eBook-loading-ico');
            if (Ext.isString(tx)) {
                txe.update(tx);
            } else {
                txe.update(tx == null ? '' : Ext.util.Format.number(tx, '0.000,00/i') + '€ (BizTax)');
            }
        }
        // this.mymenu.setTax(tx);
    }

    , enableBizTax: function (yesOrNo) {
        var txe = this.body.child('.eBook-menu-info-tax-biztax');
        if (txe) {
            if (yesOrNo) {
                txe.show();
            } else {
                txe.hide();
            }
        }
    }
    , enableTax: function (yesOrNo) {
        var txe = this.body.child('.eBook-menu-info-tax');
        if (txe) {
            if (yesOrNo) {
                txe.show();
            } else {
                txe.hide();
            }
        }
    }
    , updatingWorksheets: function () {
        var ael = this.body.child('.eBook-activity');
        ael.removeClass('ebook-opacity-0');
        ael.addClass('ebook-opacity-1');
        ael.dom.setAttribute('busy', 'true');
        ael.update('Updating all worksheets');
    }
    , updatingWorksheetsDone: function (time) {
        var ael = this.body.child('.eBook-activity');
        ael.dom.setAttribute('busy', 'false');
        var sec = Math.floor(time / 1000);
        var ms = time - (sec * 1000);
        var timing = sec + 's, ' + ms + 'ms)';
        ael.update('All worksheets updated (' + timing);
        ael.addClass('ebook-opacity-0');
    }
    , afterRender: function () {
        eBook.File.HomeHeaderPanel.superclass.afterRender.call(this);

        // set events
        this.el.on('mouseover', this.onBodyMouseOver, this);
        this.el.on('mouseout', this.onBodyMouseOut, this);
        this.el.on('click', this.onBodyClick, this);
        //this.el.on('contextmenu', this.onBodyContext, this);
    }
    , onBodyMouseOver: function (e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            Ext.get(mnuItem).addClass('eBook-menu-item-over');
            //   Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '90%');
        } else if (mnuBlock && !mnuBlockContent) {
            Ext.get(mnuBlock).addClass('eBook-menu-item-over');
        }
    }
    , onBodyMouseOut: function (e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            Ext.get(mnuItem).removeClass('eBook-menu-item-over');
            // Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '100%');
        } else if (mnuBlock && !mnuBlockContent) {
            Ext.get(mnuBlock).removeClass('eBook-menu-item-over');
        }
    }
    , onBodyClick: function (e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var close = e.getTarget('.eBook-menu-close');
        var team = e.getTarget('.eBook-menu-info-PMT-Team');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            eBook.Interface.openWindow(mnuItem.id);
        } else if (close) {
            var xclose = Ext.get(close);
            if (xclose.parent().hasClass('eBook-Client')) {
                eBook.Interface.closeClient();
            } else if (xclose.parent().hasClass('eBook-File')) {
                eBook.Interface.closeFile();
            }
        } else if (team) {
            eBook.Interface.openWindow('clientteam');
        } else if (mnuBlock && !mnuBlockContent) {
            mnuBlockContent = Ext.get(mnuBlock).child('.eBook-menu-info-block-item-content');
            mnuBlockContent.toggleClass('x-hide-display');
        }
    }
    , updateMe: function (rec, svcs) {
        var dta = {};
        Ext.apply(dta, svcs);
        Ext.apply(dta, {
            roles: eBook.User.activeClientRoles
            , teamleden: eBook.PMT.TeamWindow_Title
            , NoProAcc: 'GEEN PROACC'
            , leveranciers: eBook.Menu.Client_Suppliers
            , klanten: eBook.Menu.Client_Customers
            , nieuwdossier: eBook.Menu.Client_NewFile
            , settings: 'Settings'
            , metadata: 'Metadata'
            , schema: 'Schema'
            , adjustments: 'Adjustments'
            , finaltrial: eBook.Menu.File_FinalTrialBalance
            , leadsheets: 'Leadsheets'
            , quickprint: 'Quickprint'
            , documents: eBook.Menu.File_Documents
            , deliverables: eBook.Bundle.Window_Deliverable_Title
            , bundles: eBook.Bundle.Window_Bundle_Title
            , annualAccountsTitle: eBook.Bundle.Window_AnnualAccounts_Title
            , closeyearend: 'Pre-close year-end service'
            , closeyearendSoft: 'Definitive close year-end service'
            , closeyearendHard: 'Year-end service closed'
            , biztax: 'BizTax'
            , client: eBook.Interface.currentClient.data
            , yearend: eBook.Interface.hasService('C3B8C8DB-3822-4263-9098-541FAE897D02')
            , yearendStatus:eBook.Interface.hasService("C3B8C8DB-3822-4263-9098-541FAE897D02")?eBook.Interface.serviceYearend.s:null
            , biztaxAuto: eBook.Interface.hasService('BD7C2EAE-8500-4103-8984-0131E73D07FA')
            , biztaxManual: eBook.Interface.hasService('60CA9DF9-C549-4A61-A2AD-3FDB1705CF55')
            , annualAccounts: eBook.Interface.hasService('1ABF0836-7D7B-48E7-9006-F03DB97AC28B')
        });
        //  Ext.apply(dta, eBook.Interface.currentClient.data);
        Ext.apply(dta, rec.data);
        this.body.update(this.mainTpl.apply(dta));

    }
});

eBook.File.HomeHeader = Ext.extend(eBook.File.HomeHeaderPanel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
                , '<div class="boxSides">'
                    , '<div id="eyMenuNav" style="">'
                        , '<div class="eBook-Client">'
                            , '<div class="eBook-menu-text"><tpl for="client">{Name}</tpl></div>'
                            , '<div class="eBook-menu-close"> '
                                , '<div class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div>'
                            , '</div>		'
       	                , '</div>'
       	                , '<div class="eBook-File <tpl if="values.Closed">eBook-File-Closed</tpl>">'
                            , '<div class="eBook-menu-text">{StartDate:date("d/m/Y")} &gt; {EndDate:date("d/m/Y")}</div>'
                            , '<div class="eBook-menu-close"> '
                                , '<div class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div>'
                            , '</div>		'
       	                , '</div>'
       	                , '<div class="eBook-activity"></div>'
                    , '</div>'
                , '</div>	'
                , '<div class="eBook-menu-info-block">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-address">'
                         , '<div class="eBook-menu-info-block-item-content"><tpl for="client">{Address}'
                        , '<br>{ZipCode} {City}</tpl>'
                        , '<tpl if="client.Shared"><div class="eBook-menu-info-shared">GEDEELD DOSSIER ACR/TAX</div></tpl>'
                        , '<tpl if="client.bni"><div class="eBook-menu-info-shared">BNI</div></tpl>'
                     , '</div></div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-PMT">'
                        , '<div class="eBook-menu-info-block-item-content">' + eBook.File.HomeHeader_Role + ': <br/><b>{roles}</b>'
                        , '<div class="eBook-menu-info-PMT-Team">{teamleden}</div>'
                    , '</div></div>'
                    , '<tpl if="ImportType==\'exact\'">'
                        , '<div class="eBook-menu-info-block-item eBook-menu-info-ProAcc">'
                            , '<div class="eBook-menu-info-block-item-content"><b><u>Exact</u></b><br></div>'
                        , '</div>'
                    , '</tpl>'
                    , '<tpl if="ImportType==null || ImportType==\'\'">'
                        , '<div class="eBook-menu-info-block-item eBook-menu-info-ProAcc">'
                            , '<div class="eBook-menu-info-block-item-content"><b><u>ProAcc</u></b><br>'
                            , '<tpl if="client.ProAccServer==null || client.ProAccServer==\'\'">{NoProAcc}</tpl>'
                            , '<tpl if="client.ProAccServer!=null && client.ProAccServer!=\'\'">'
                                , '<tpl for="client">Database: <b>{ProAccDatabase}</b>'
                                , '</tpl>'//, '<br/>Server: <b>{ProAccServer}</b></tpl>'
                                , '<br/>Last import: <span class="proacc-importdate">{ImportDate:date("d/m/Y H:i:s")}</span>'
                            , '</tpl>'
                        , '</div></div>'
                    , '</tpl>'
                    , '<tpl if="Ext.isDefined(eBook.Interface.serviceYearend) && eBook.Interface.serviceYearend!=null">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Services">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>Year-end service</u></b>'
                        , '<tpl for="eBook.Interface.serviceYearend"><br/><tpl if="c==null">In progress</tpl><tpl if="c!=null">Closed: {c:date("d/m/Y")}<br/> {cb} </tpl></tpl>'
                    , '</div></div>'
                    , '</tpl>'
                     , '<tpl if="eBook.Interface.serviceBizTax!=null">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-BizTax">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>BizTax service</u></b>'
                        , '<tpl if="Ext.isDefined(eBook.Interface.serviceYearend) && eBook.Interface.serviceYearend!=null"><tpl if="eBook.Interface.serviceYearend.c==null"><br/> Awaiting closing of year-end service.</tpl><tpl if="eBook.Interface.serviceYearend.c!=null"><br/><tpl for="eBook.Interface.serviceBizTax"><br/><tpl if="c==null">In progress</tpl><tpl if="c!=null">Closed: {c:date("d/m/Y")}<br/> {cb} </tpl></tpl></tpl></tpl>'
                        , '<tpl if="!(Ext.isDefined(eBook.Interface.serviceYearend) && eBook.Interface.serviceYearend!=null)"><tpl for="eBook.Interface.serviceBizTax"><br/><tpl if="c==null">In progress</tpl><tpl if="c!=null">Closed: {c:date("d/m/Y")}<br/> {cb} </tpl></tpl></tpl>'
                        , '<tpl for="eBook.Interface.serviceBizTax"><br/><tpl if="c==null">In progress</tpl><tpl if="c!=null">Closed: {c:date("d/m/Y")}<br/> {cb} </tpl></tpl>'
                    , '</div></div>'
                    ,'</tpl>'
    //                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Health">'
    //                        , '<div class="eBook-menu-info-block-item-content"><b><u>Health</u></b>'
    //                        , '<br/>Errors: 0'
    //                        , '<br/>Warnings: 0'
    //    // , 'Errors: 0'
    //    //, 'Errors: 0'
    //                    , '</div></div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Money">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>Tax calculation</u></b>'
                        , '<div class="eBook-menu-info-tax">€</div>'
                        , '<div class="eBook-menu-info-tax-biztax">€</div>'
                    , '</div></div>'
                 , '</div>'

            , '</tpl>',
        {
            compiled: true
        })
   
});


Ext.reg('filehomeheader', eBook.File.HomeHeader);


eBook.File.HomeMenu = Ext.extend(eBook.File.HomeHeaderPanel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
                , '<div style="visibility: visible;" class="eBook-menu">'
                    , '<div class="eBook-menu-childcontainer">'
                        , '<!--div class="eBook-menu-item" id="eBook-File-Wizard">'
                            , '<div class="eBook-menu-item-icon eBook-wizard-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{jaarrekening}</div>'
                        , '</div-->'
                         , '<div class="eBook-menu-item" id="eBook-File-repository">'
                            , '<div class="eBook-menu-item-icon eBook-repository-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">Repository</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-suppliers">'
                            , '<div class="eBook-menu-item-icon eBook-suppliers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{leveranciers}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-customers">'
                            , '<div class="eBook-menu-item-icon eBook-customers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{klanten}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Settings" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-file-settings-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{settings}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Meta" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-file-meta-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{metadata}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Schema" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-accountmappings-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{schema}</div>'
                        , '</div>'
                        //, '<tpl if="ImportType==\'exact\'"><div class="eBook-menu-item eBook-menu-item-disabled" id="eBook-File-journal" qtip=""></tpl>'
                        //, '<tpl if="ImportType==null || ImportType!=\'exact\'"><div class="eBook-menu-item" id="eBook-File-journal" qtip=""></tpl>'
                        , '<div class="eBook-menu-item" id="eBook-File-journal" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-journals-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{adjustments}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-finalTrialBalance" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-final-trialbalance-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{finaltrial}</div>'
                        , '</div>'
//                        , '<div class="eBook-menu-item" id="eBook-File-leadsheets" qtip="">'
//                            , '<div class="eBook-menu-item-icon eBook-leadsheets-menu-ico">'
//                                , '<div class="eBook-menu-item-alert-icon"></div>'
//                            , '</div>'
//                            , '<div class="eBook-menu-item-title">{leadsheets}</div>'
//                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-quickprint" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-quickprint-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{quickprint}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-documents" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-documents-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{documents}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Bundles" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-reports-generated">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{bundles}</div>'
                        , '</div>'
                        , '<tpl if="yearend"><div class="eBook-menu-item" id="eBook-File-ReportGeneration-yearend" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-services-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">Year-end {deliverables}</div>'
                        , '</div></tpl>'
                        , '<tpl if="(yearend || annualAccounts) && [this.userAllowed()] == 1"><div class="eBook-menu-item" id="eBook-File-ReportGeneration-AnnualAccounts" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-services-ico">'
                            , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{annualAccountsTitle}</div>'
                        , '</div></tpl>'
                        //    , '<div class="eBook-menu-item " id="eBook-File-BizTax" qtip="">'
                        //        , '<div class="eBook-menu-item-icon eBook-biztax-menu-ico">'
                        //            , '<div class="eBook-menu-item-alert-icon"></div>'
                        //        , '</div>'
                        //    , '<div class="eBook-menu-item-title">{biztax}</div>'
                        //, '</div>'
                    , '</div>'
                , '</div>'
                ,
        {
            compiled: true,
            userAllowed: function() {
                var currentUser = eBook.User.personId;
                var allowedUsers = ['29EA3413-4930-4B20-A22F-35B11B4864F7','6BF61892-D9A0-495E-A07B-185DC7B03F54','A2AA22FF-EAA3-42F7-8B10-26423F8F581C','18BC4A5D-A798-4D20-B097-E2F6E1A66E3E','BF723AA4-4974-4DCE-B01D-643764805FB7','AC7B9975-423F-4EB4-86E2-7EFBFA93AF47']; //Kim,Timothy,Theo,Lieve,Mieke,Frédéric
                for(var i = 0;i < allowedUsers.length; i++)
                {
                    if(currentUser.toUpperCase() == allowedUsers[i].toUpperCase())
                    {
                        return 1
                    }
                }
                return 0;
            }
        })
});
Ext.reg('filehomemenu', eBook.File.HomeMenu);

//, style: 'background-color:#FFF;margin-left:60px;margin-right:60px;'
//, bodyStyle:'background-color:transparent;margin-left:60px;margin-right:60px;'
eBook.File.Home = Ext.extend(Ext.Panel, {
    initComponent: function () {
        var fileClosed = false;
        Ext.apply(this, {
            layout: 'vbox', layoutConfig: { align: 'stretch', defaultMargins: { top: 0, bottom: 10, left: 0, right: 0} }

            , items: [
                { xtype: 'filehomeheader', height: 124, ref: 'mymenu' }
                , { xtype: 'icontabpanel', flex: 1,
                    border: false,
                    ref: 'tabs',
                    cls: 'ebook-file-main-tab',
                    //padding: { top: 100, bottom: 10, right: 10, left: 10 },
                    activeTab: 0,
                    //deferredRender: true,
                    //tabStripTitle: 'SERVICES',
                    items: [
                        { xtype: 'panel', ref: '../../yearend', title: 'Jaarrekening &amp; voorbereiding BizTax', tabIconCls: 'eBook-menu-info-Services ',
                            layout: 'vbox', layoutConfig: { align: 'stretch' }
                            , items: [{ xtype: 'filehomemenu', height: 200, ref: '../../myiconmenu' }
                                    , { xtype: 'panel', flex: 1, padding: '0 60 0 60', layout: 'border', border: false
                                            , items: [{ split: true, ref: '../../../worksheets', xtype: 'worksheetstab', region: 'west', width: '50%' }, { xtype: 'health', ref: '../../../health', region: 'center'}]
                                    }]
                        }
                        ,
                    /*{ xtype: 'panel', title: 'BizTax indiening', tabIconCls: 'eBook-biztax-menu-ico '
                    , tbar: [
                    {
                    ref: 'reImportWorksheets',
                    text: 'Re-import/refresh worksheets',
                    iconCls: 'eBook-icon-import-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    },
                    {
                    ref: 'startValidation',
                    text: 'Validate',
                    iconCls: 'eBook-icon-checklist-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    }
                    , {
                    ref: 'clientBundle',
                    text: 'Client Bundle',
                    iconCls: 'eBook-bundle-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    }
                    , {
                    ref: 'reviewBundle',
                    text: 'Internal Review Bundle',
                    iconCls: 'eBook-biztax-bundle-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    }
                    , {
                    ref: 'declaration',
                    text: 'Declare',
                    iconCls: 'eBook-biztax-send-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    }
                    ]
                    }
                 
                    */
                {xtype: 'biztax-module', items: [], ref: '../../module', enableTabScroll: true, title: eBook.Language.BIZTAXDECLARATION, tabIconCls: 'eBook-biztax-menu-ico ' }

                    ]
                }

                ]
        });
        eBook.File.Home.superclass.initComponent.apply(this, arguments);
    }
    , redrawWorksheetsView: function () {
        this.worksheets.redrawWorksheetsView();
    }
    , getItemMessageType: function (id) {
        return this.health.getItemMessageType(id);
    }
    , checkHealthTab: function (tpeId, name) {
        return this.health.checkHealthTab(tpeId, name);
    }
    , getFieldsByTab: function (tpeId, tab) {
        return this.health.getFieldsByTab(tpeId, tab);
    }
    , loadRec: function (rec) {
        eBook.Interface.serviceYearend = null;
        eBook.Interface.serviceBizTax = null;
        eBook.Interface.serviceAnnualAccounts = null;
        var yearend = false,
            autobiztax = false,
            manbiztax = false,
            annualAccounts = false,
            svcTxts = {};
        eBook.Interface.hasService('BD7C2EAE-8500-4103-8984-0131E73D07FA');
        var svcs = rec.get('Services');
        for (var i = 0; i < svcs.length; i++) {
            switch (svcs[i].sid.toUpperCase()) {
                case 'C3B8C8DB-3822-4263-9098-541FAE897D02':
                    eBook.Interface.serviceYearend = svcs[i];
                    if (eBook.Interface.serviceYearend.c) eBook.Interface.serviceYearend.c = Ext.data.Types.WCFDATE.convert(eBook.Interface.serviceYearend.c);

                    break;
                case 'BD7C2EAE-8500-4103-8984-0131E73D07FA':
                    eBook.Interface.serviceBizTax = svcs[i];
                    if (eBook.Interface.serviceBizTax.c) eBook.Interface.serviceBizTax.c = Ext.data.Types.WCFDATE.convert(eBook.Interface.serviceBizTax.c);
                    eBook.Interface.serviceBizTax.auto = true;
                    autobiztax = true;
                    break;
                case '60CA9DF9-C549-4A61-A2AD-3FDB1705CF55':
                    eBook.Interface.serviceBizTax = svcs[i];
                    if (eBook.Interface.serviceBizTax.c) eBook.Interface.serviceBizTax.c = Ext.data.Types.WCFDATE.convert(eBook.Interface.serviceBizTax.c);
                    eBook.Interface.serviceBizTax.auto = false;
                    break;
                case '1ABF0836-7D7B-48E7-9006-F03DB97AC28B':
                    eBook.Interface.serviceAnnualAccounts = svcs[i];
                    if (eBook.Interface.serviceAnnualAccounts.c) eBook.Interface.serviceAnnualAccounts.c = Ext.data.Types.WCFDATE.convert(eBook.Interface.serviceAnnualAccounts.c);
                    eBook.Interface.serviceAnnualAccounts.auto = false;
                    break;
            }
        }

        var title0 = '';
        if (eBook.Interface.serviceYearend) {
            title0 += eBook.Language.YEAREND;
        }


        //autoBiztax
        if (eBook.Interface.serviceBizTax && eBook.Interface.serviceBizTax.auto) {
            if (title0.length > 0) title0 += ' &amp; ';
            title0 += eBook.Language.BIZTAXPREP;
        }
        this.tabs.get(0).setTitle(title0);
        //manualBiztax
        if (eBook.Interface.serviceBizTax && !eBook.Interface.serviceBizTax.auto) {
            this.tabs.setActiveTab(1);
            this.tabs.hideTabStripItem(0);

        } else {
            this.tabs.setActiveTab(0);
            this.tabs.unhideTabStripItem(0);
        }

        //No Biztax
        if (!eBook.Interface.serviceBizTax) {
            this.tabs.hideTabStripItem(1);
        } else {
            this.tabs.unhideTabStripItem(1);
        }


        this.mymenu.updateMe(rec);
        this.myiconmenu.updateMe(rec);
        this.mymenu.enableBizTax(eBook.Interface.serviceBizTax != null);
        if (eBook.Interface.serviceBizTax != null && !eBook.Interface.serviceBizTax.auto) {
            this.mymenu.setBizTax("open biztax tab");
        }
        this.mymenu.enableTax(eBook.Interface.serviceYearend != null);
        this.redrawWorksheetsView();
        //this.files.loadTabs(rec.get('Id'));
    }
    , setTax: function (tx) {
        this.mymenu.setTax(tx);
    }
    , setBizTax: function (tx) {
        this.mymenu.setBizTax(tx);
    }
    , openWorksheetSh: function (typeId, tab, row, field) {
        this.openWorksheet(this.getWorksheetRecord(typeId), tab, row, field);
    }
    , openWorksheet: function (rec, tab, row, field) {
        var cfg = this.getWorksheetWindowCfg(rec);
        if (cfg == null) {
            var ocfg = {
                rec: rec
                , tab: tab
                , row: row
                , field: field
            }; var ass = eBook.Interface.currentFile.get('AssessmentYear');
            eBook.Splash.setText("Loading worksheets " + ass);
            eBook.Splash.show();
            eBook.LazyLoad.loadOnce.defer(50, this, ["js/Worksheets/" + ass + "-" + eBook.Interface.Culture + ".js?" + eBook.SpecificVersion, this.onWorksheetsLoaded, ocfg, this, true]);
        } else {
            var wn = new cfg({ title: rec.get('name'), wsClosed: rec.closed, id: rec.get('id') });
            wn.show(tab, row, field);
        }
    }
    , getWorksheetWindowCfg: function (rec) {
        var cfg = eBook.Worksheets['AY' + eBook.Interface.currentFile.get('AssessmentYear')];
        if (cfg == null) return null;
            return cfg[rec.get('ruleApp')].Window;
    }
    , onWorksheetsLoaded: function (ocfg) {
        this.openWorksheet(ocfg.rec, ocfg.tab, ocfg.row, ocfg.field);
        eBook.Splash.hide();
    }
    , getWorksheetRecord: function (typeid) {
        var str = this.worksheets.getStore();
        var idx = str.find('id', typeid);
        if (idx < 0) return null;
        return str.getAt(idx);
    }
    , updateWorksheets: function (force) {
        this.mymenu.updatingWorksheets();
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        if (force) {
            eBook.Splash.setText("Force recalculating all worksheets " + ass);
            eBook.Splash.show();
        }
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + (force ? 'ForceReCalculate' : 'ReCalculate')
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
                , callback: this.worksheetsUpdated
                , started: new Date().getTime()
                , scope: this
                , forced: force
        });
    }
    , worksheetsUpdated: function (opts, success, resp) {
        var end = new Date().getTime();
        this.mymenu.updatingWorksheetsDone((end - opts.started));
        if (opts.forced) {
            eBook.Splash.hide();
        }
        this.updateTaxCalculation();
    }
    , updateTaxCalculation: function () {
        this.mymenu.updatingTax();
        eBook.Interface.reloadHealth();
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + 'GetTaxCalculation'
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
                , callback: this.taxCalculationCallback
                , scope: this
        });
    }
    , taxCalculationCallback: function (opts, success, resp) {
        if (success) {
            try {
                var o = Ext.decode(resp.responseText);
                o = o.GetTaxCalculationResult;
                this.setTax(o);
            } catch (e) {
                this.setTax(null);
            }
        } else { this.setTax(null); }

    }
});

Ext.reg('filehome', eBook.File.Home);eBook.File.QuickprintWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        if (eBook.Interface.Culture == 'fr-FR') {
            this.leadsheets = 'Leadsheets';
            this.balans = 'Bilan';
            this.balansV = 'Bilan - comparant';
            this.activa = 'Activa';
            this.passiva = 'Passiva';
            this.resultatenrekening = 'Compte de résultat';
            this.resultaatverwerking = 'Resultaatverwerking';
            this.correctieboeking = 'Operations diverses';
        } else {
            this.leadsheets = 'Leadsheets';
            this.balans = 'Balans';
            this.balansV = 'Balans - vergelijkend';
            this.activa = 'Activa';
            this.passiva = 'Passiva';
            this.resultatenrekening = 'Resultatenrekening';
            this.resultaatverwerking = 'Resultaatverwerking';
            this.correctieboeking = 'Correctieboeking';
        }
        /* TO IMPLEMENT
        var leadsheets,
        balans,
        balansV,
        activa,
        passiva,
        resultatenrekening,
        resultaatverwerking,
        correctieboeking;
        this.leadsheets = null;
        Ext.Ajax.request({
        url: eBook.Service.lists + '/GetList'//eBook.Service.url + action
        , method: 'POST'
        , jsonData: { cldc: { lik: 'Quickprint'} }
        , success: function(resp) {
        console.log(this);
        alert(resp);
        var array = Ext.decode(resp.responseText);
        //                    for (var i = 0; i < array.length; i++) {
        //                        switch (array[i].id) {
        //                            case '16AE9C19-A0F8-45AB-A470-0068843F6218':
        //                                this.leadsheets = array[i].nl;
        //                                break;
        //                            case '94741A7E-8650-4127-9ACA-70DA2A7D83B8':
        //                                balans = array[i].nl;
        //                                break;
        //                            case '710C3D60-8F3D-4FDC-AB0C-85A3CCEC13FE':
        //                                activa = array[i].nl;
        //                                break;
        //                            case 'F015D7E6-51F9-4DED-B6AE-E1646636FE14':
        //                                passiva = array[i].nl;
        //                                break;
        //                            case '31F9CA3C-1BBB-4C78-84C2-5A41DA099026':
        //                                resultatenrekening = array[i].nl;
        //                                break;
        //                            case '7597CEBD-33C1-45C6-A9B3-3A57D6DFC997':
        //                                resultaatverwerking = array[i].nl;
        //                                break;
        //                            case '7725E891-13DE-4E9C-98C1-47FF449271F0':
        //                                correctieboeking = array[i].nl;
        //                                break;
        //                            case 'E28CF7AC-0AB3-4169-83D6-1DE815CF0FE8':
        //                                balansV = array[i].nl;
        //                                break;
        //                        }
        //                    }
        //                    leadsheets,
        //                    balans,
        //                    activa,
        //                    passiva,
        //                    resultatenrekening,
        //                    resultaatverwerking;
        }

                , scope: this
        });
        */
        Ext.apply(this, {
            items: [
            {
                xtype: 'fieldset',
                region: 'center',
                title: 'Options',
                margins: '0 8 8 8',
                items: [{ xtype: 'checkbox', boxLabel: this.leadsheets, tag: 'leadsheets', itemCls: 'eBook-File-Quickprints-checkbox-box' },
                        {
                            xtype: 'fieldset',
                            checkboxToggle: true,
                            collapsed: true,
                            title: this.balans,
                            defaults: {
                                xtype: 'checkbox',
                                itemCls: 'eBook-File-Quickprints-checkbox-fieldset-box'
                            },
                            items: [
                                { boxLabel: this.activa, tag: 'jaarrekening_activa' }
                                , { boxLabel: this.passiva, tag: 'jaarrekening_passiva' }
                                , { boxLabel: this.resultatenrekening, tag: 'jaarrekening_resultatenrekening' }
                                , { boxLabel: this.resultaatverwerking, tag: 'jaarrekening_resultaatverwerking' }
                            ],
                            listeners: {
                                'expand': {
                                    fn: this.autoCheckCheckboxes,
                                    scope: this
                                },
                                'collapse': {
                                    fn: this.autoUncheckCheckboxes,
                                    scope: this
                                }
                            }
                        },
                            {
                                xtype: 'fieldset',
                                checkboxToggle: true,
                                collapsed: true,
                                title: this.balansV,
                                defaults: {
                                    xtype: 'checkbox',
                                    itemCls: 'eBook-File-Quickprints-checkbox-fieldset-box'
                                },
                                items: [
                                { boxLabel: this.activa, tag: 'jaarrekening_activa_v' }
                                , { boxLabel: this.passiva, tag: 'jaarrekening_passiva_v' }
                                , { boxLabel: this.resultatenrekening, tag: 'jaarrekening_resultatenrekening_v' }
                                , { boxLabel: this.resultaatverwerking, tag: 'jaarrekening_resultaatverwerking_v' }
                            ],
                                listeners: {
                                    'expand': {
                                        fn: this.autoCheckCheckboxes,
                                        scope: this
                                    },
                                    'collapse': {
                                        fn: this.autoUncheckCheckboxes,
                                        scope: this
                                    }
                                }

                            },
                        { xtype: 'checkbox', boxLabel: this.correctieboeking, tag: 'correctieboekingen', itemCls: 'eBook-File-Quickprints-checkbox-box' },
                        { xtype: 'combo',
                            mode: 'local',
                            store: new Ext.data.ArrayStore({
                                id: 0,
                                fields: [
                                    'myId',
                                    'displayText'
                                ],
                                data: [['nl-BE', 'Nederlands'], ['fr-FR', 'Frans'], ['en-US', 'Engels']],
                                autoLoad: true
                            }),
                            valueField: 'myId',
                            displayField: 'displayText',
                            fieldLabel: 'Language',
                            itemCls: 'eBook-File-Quickprints-checkbox-combo',
                            allowBlank: false,
                            id: 'quickprintLanguagueCombo',
                            value: 'all',
                            listeners: {
                                'afterrender': {
                                    fn: this.loadUserLanguage,
                                    scope: this
                                },
                                'expand': {
                                    fn: this.loadComboOptions,
                                    scope: this
                                }
                            }
                        }
                    ]
            }
            ]
            , layout: 'border'
            , bbar: ['->', {
                ref: 'savechanges',
                text: "Save changes",
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                //disabled: eBook.Interface.isFileClosed(),
                scope: this
}]
            , title: 'Quickprint'
            , width: 350
            , height: 550
            , modal: true
            });

            eBook.File.QuickprintWindow.superclass.initComponent.apply(this, arguments);
        }
        , loadComboOptions: function(combo) {
            combo.store.loadData([['nl-BE', 'Nederlands'], ['fr-FR', 'Frans'], ['en-US', 'Engels']]);
        }

    , loadUserLanguage: function(combo) {
        combo.setValue(combo.store.getAt('0').get('myId'))
        //Ext.getCmp('quickprintLanguagueCombo').setValue(store.getAt('0').get('displayText'));
        //combo.selectByValue(1, false);
    }

    , autoCheckCheckboxes: function(fieldset) {
        this.handleCheckboxes(fieldset, true);
    }

    , autoUncheckCheckboxes: function(fieldset) {
        this.handleCheckboxes(fieldset, false);
    }

    , handleCheckboxes: function(fieldset, bool) {
        var checkboxes = fieldset.items.items;
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].setValue(bool);
        }
    }
    , show: function() {
        eBook.File.QuickprintWindow.superclass.show.call(this);
        this.loadData();
    }
    , loadData: function() {
        this.getEl().mask('Loading data', 'x-mask-loading');
        this.getEl().unmask();
    }
    , onSaveClick: function(btn) {
        var window = btn.ownerCt.ownerCt;
        var checkedCheckboxes = new Array();
        var components = window.findBy(function(component, window) {
            if (component.isXType('checkbox') && component.checked && component.tag) {
                checkedCheckboxes.push(component.tag);
            }
        });
        var combo = Ext.getCmp('quickprintLanguagueCombo');
        eBook.Splash.setText("Generating quickprint");
        eBook.Splash.show();
        eBook.CachedAjax.request({
            url: eBook.Service.output + 'GenerateQuickprint'
            , method: 'POST'
            , params: Ext.encode({
                cqdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    Culture: combo.getValue(),
                    Options: checkedCheckboxes
                }
            })
            , callback: this.onQuickprintReady
            , scope: this
        });

        //this.saveChanges();

    }
    , onQuickprintReady: function(opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GenerateQuickprintResult);
            //window.open('pickup/' + robj.GenerateLeadSheetsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , saveChanges: function(force) {
        alert('saving changes');

        //            this.getEl().mask('Saving changes', 'x-mask-loading');
        //            this.servicePanel.resetOriginals();
        //            eBook.CachedAjax.request({
        //                url: eBook.Service.file + 'SaveFileSettings'
        //                    , method: 'POST'
        //                    , params: Ext.encode({
        //                        csfsdc: {
        //                            Settings: this.servicePanel.getData()
        //                            , Person: eBook.User.getActivePersonDataContract()
        //                        }
        //                    })
        //                    , callback: this.onSavedChanges
        //                    , scope: this
        //            });


    }
    , onSavedChanges: function(opts, success, resp) {
        if (success) {
            eBook.Interface.center.fileMenu.updateWorksheets(true);
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }

    });//This form is, as a panel, not stand-alone.
//It does not have the server-contact capability. It does however contain all the functions and info for the parent to utilize this form.
//Its functions allow form interaction, data validation and retrieval.
eBook.Forms.AnnualAccounts.Staff = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        //Vars
        var me = this,
            companyTypes = ["Enterprise","Non-profit institution","Foreign enterprise","Foreign non-profit institution"],
            models = ["Full presentation","Abbreviated presentation","Consolidated accounts","correction","Other"];

        //Set properties that will be used by parent
        me.changesAfterGeneralAssemblyDescriptionValue = null;
        me.serviceUrl = eBook.Service.annualAccount;
        me.action = 'UpdateAnnualAccounts';
        me.criteriaParameter = 'cfaadc';

        //Form
        Ext.apply(this, {
            layout: 'form',
            labelWidth: 200,
            title: 'Annual Accounts information form',
            bodyStyle: 'padding:10px;',
            padding:10,
            ref: 'formPanel',
            items: [
                /*{ Currently not auto-filled in
                    xtype:'label',
                    fieldLabel: 'Is the estimated general assembly date accurate? If not add the definitive date',
                    labelStyle: 'width: 800px;'
                },*/
                {
                    xtype: 'datefield',
                    ref: 'definitiveGeneralAssembly',
                    anchor: '95%',
                    name: 'definitiveGeneralAssembly',
                    fieldLabel: 'Definitive general assembly date',
                    maxValue: new Date(),
                    format: 'd/m/Y'
                },
                {
                    xtype: 'combo'
                    , ref: 'companyType'
                    , fieldLabel: 'Type of company'
                    , name: 'companyType'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Name'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: companyTypes
                    , anchor: '95%'
                }  ,
                {
                    xtype: 'combo'
                    , ref: 'model'
                    , fieldLabel: 'Model'
                    , name: 'model'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Name'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: models
                    , anchor: '95%'
                },
                {
                    xtype:'label',
                    fieldLabel: 'Can you confirm that no adjustments, by EY, have been made to the annual accounts after the date of the AGM',
                    labelStyle: 'width: 800px;'
                },
                {
                    xtype: 'radiogroup',
                    ref: 'changesAfterGAD',
                    name: 'changesAfterGeneralAssemblyGroup',
                    columns: 1,
                    hideLabel: true,
                    items: [
                        {
                            boxLabel: 'I confirm',
                            ref: 'changesAfterGADNo',
                            name: 'changesAfterGeneralAssembly',
                            inputValue: false
                        },
                        {
                            boxLabel: 'I do not confirm',
                            ref: 'changesAfterGADYes',
                            name: 'changesAfterGeneralAssembly',
                            inputValue: true
                        }
                    ],
                    listeners: {
                    'change': {
                        fn: me.toggleChangesAfterGADDescription,
                        scope: this
                        }
                    }
                },
                {
                    xtype:'label',
                    ref: 'changesAfterGADlabel',
                    fieldLabel: 'Please add a note as to why adjustments were made',
                    labelStyle: 'width: 800px;'
                }
            ],
            listeners: {
                afterrender: function () {
                    //disable editor of parent as its availability depends on a form component
                    me.parent.htmlEditor.disable();
                    me.changesAfterGADlabel.hide();
                }
            }
        });
        eBook.Forms.AnnualAccounts.Staff.superclass.initComponent.apply(this, arguments);
    }
    //Check if form is valid
    , isValid: function()
    {
        //vars
        var me = this,
            invalid = null;

        //validation
        if(!me.model.isValid() || !me.model.getValue()) {invalid = "model";}
        if(!me.companyType.isValid() || !me.companyType.getValue()) {invalid = "company type";}
        if(!me.definitiveGeneralAssembly.isValid() || !me.companyType.getValue()) {invalid = "general assembly date";}
        if(me.parent) { //review notes window
            if (me.changesAfterGAD.getValue().inputValue && me.parent && me.parent.htmlEditor.getValue() == "") {invalid = "text area";}
        }

        //return en alert
        if(invalid) {
            Ext.Msg.alert("Not filled in", "Please fill in the " + invalid + ".");
            return false;
        }

        return true;
    }
    //Get json data of form
    , getData: function() {
        //vars
        var me = this,
            cfaadc = {};

        //create contract
        cfaadc.DefinitiveGeneralAssembly = me.definitiveGeneralAssembly.getValue();
        cfaadc.CompanyType = me.companyType.getValue();
        cfaadc.Model = me.model.getValue();
        cfaadc.ChangesAfterGeneralAssembly = me.changesAfterGAD.getValue().inputValue;
        if(cfaadc.ChangesAfterGeneralAssembly === true && me.parent && me.parent.htmlEditor)
        {
            cfaadc.ChangesAfterGeneralAssemblyDescription = me.parent.htmlEditor.getValue();
        }

        return cfaadc;
    }
    , setData: function(values) {
        var me = this;

        if(values) {
            me.getForm().setValues(values);
        }
        if(values.definitiveGeneralAssembly) //Date not being properly filled in through setValues as it is supplied in millisenconds since 1970, format u
        {
            me.definitiveGeneralAssembly.setValue(Ext.data.Types.WCFDATE.convert(values.definitiveGeneralAssembly));
        }
        if(values.changesAfterGeneralAssembly)
        {
            //Could not get the setValue on the group to work so I went with the filter function on the items as a dynamic solution
            me.changesAfterGAD.items.items.filter(function(obj){return obj.inputValue == values.changesAfterGeneralAssembly})[0].setValue(true);
        }
        if(values.changesAfterGeneralAssemblyDescription)
        {
            me.changesAfterGeneralAssemblyDescriptionValue = values.changesAfterGeneralAssemblyDescription;
            me.parent.htmlEditor.setValue(me.changesAfterGeneralAssemblyDescriptionValue);
        }

    }
    , show: function(parent) {
        var me = this;
        me.callingParent = parent;
        eBook.Forms.AnnualAccounts.Staff.superclass.show.call(me);
    }
    , toggleChangesAfterGADDescription: function(buttongroup, newValue)
    {
        //Toggle review notes editor on and off
        var me = this;

        if(me.parent) {
            if (newValue.inputValue) {
                me.parent.htmlEditor.enable();
                me.parent.htmlEditor.setValue(me.changesAfterGeneralAssemblyDescriptionValue);
                me.changesAfterGADlabel.show();
            }
            else {
                me.parent.htmlEditor.disable();
                me.parent.htmlEditor.setValue(null);
                me.changesAfterGADlabel.hide();
            }
        }
    }
});//This form is, as a panel, not stand-alone.
//It does not have the server-contact capability. It does however contain all the functions and info for the parent to utilize this form.
//Its functions allow form interaction, data validation and retrieval.
eBook.Forms.AnnualAccounts.Manager = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        //Vars
        var me = this,
            invoiceRechargeFilingExpenses = ["Filing costs + supplementary cost for late filing","Only filing costs","Only supplementary costs"],
            invoiceLanguage = ["Dutch","French","English"];
            /*invoiceLanguage = Ext.create('Ext.data.Store', {
            fields: ['value', 'display'],
            data : [
                {"value":"nl-BE", "display":"Dutch"},
                {"value":"fr-FR", "display":"French"},
                {"value":"en-GB", "display":"English"}
                //...
            ]
        })*/
        //Set properties that will be used by parent

        //Form
        Ext.apply(this, {
            layout: 'form',
            labelWidth: 200,
            title: 'Annual Accounts information form',
            bodyStyle: 'padding:10px;',
            //, resizable: true,
            padding:10,
            ref: 'formPanel',
            items: [
                {
                    xtype: 'combo'
                    , ref: 'engagementCode'
                    , fieldLabel: 'Engagement code'
                    , name: 'engagementCode'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , forceSelection: true
                    , valueField: 'Code'
                    , displayField: 'Code'
                    ,tpl: new Ext.XTemplate(
                    '<tpl for="."><div class="x-combo-list-item">{Code} - {Description}</div></tpl>')
                    , editable: false
                    , mode: 'local'
                    , anchor: '95%'
                    , store: new eBook.data.JsonStore({
                    selectAction: 'GetEngagementCodes'
                    , criteriaParameter: 'cidc'
                    , autoDestroy: true
                    , serviceUrl: eBook.Service.annualAccount
                    , storeParams: {Id: eBook.CurrentClient}
                    , baseParams: {Id: eBook.CurrentClient}
                    , disabled: this.readOnly
                    , fields: eBook.data.RecordTypes.Engagement
                    , autoLoad: true
                    })
                },
                {
                    xtype: 'combo'
                    , ref: 'invoiceRechargeFilingExpense'
                    , fieldLabel: 'Invoice recharge filing expense'
                    , name: 'invoiceRechargeFilingExpense'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Name'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: invoiceRechargeFilingExpenses
                    , anchor: '95%'
                }  ,
                {
                    xtype: 'combo'
                    , ref: 'invoiceLanguage'
                    , fieldLabel: 'Invoice language'
                    , name: 'invoiceLanguage'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Name'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: invoiceLanguage
                    , anchor: '95%'
                }
            ],
            listeners: {
                afterrender: function () {
                    //remove mask if there is one
                    if(me.maskedCmp) {
                        me.maskedCmp.getEl().unmask();
                    }
                }
            }
        });
        eBook.Forms.AnnualAccounts.Manager.superclass.initComponent.apply(this, arguments);
    },
    //Check if form is valid
    isValid: function()
    {
        //vars
        var me = this,
            invalid = null;

        //validation
        if(!me.engagementCode.isValid() || !me.engagementCode.getValue()) invalid = " engagement code";
        if(!me.invoiceRechargeFilingExpense.isValid() || !me.invoiceRechargeFilingExpense.getValue()) {invalid = " invoice recharge filing expense";}
        if(!me.invoiceLanguage.isValid() || !me.invoiceLanguage.getValue()) {invalid = "invoice language";}

        //return en alert
        if(invalid) {
            Ext.Msg.alert("Not filled in", "Please fill in the " + invalid + ".");
            return false;
        }

        return true;
    },
    //Get json data of form
    getData: function() {
        //vars
        var me = this,
            cfaadc = {},
            combo = me.engagementCode,
            store = combo ? combo.getStore() : null,
            record = store ? store.getById(combo.getValue()): null; //get record

        //create contract
        cfaadc.InvoiceRechargeFilingExpense = me.invoiceRechargeFilingExpense.getValue();
        cfaadc.InvoiceLanguage = me.invoiceLanguage.getValue();
        cfaadc.EngagementCode = record ? record.get("Code") : null;
        cfaadc.ServiceLine = record ? record.get("ServiceLine"): null;

        return cfaadc;
    },
    setData: function(values) {
        var me = this,
            combo = me.engagementCode,
            store = combo ? combo.getStore() : null,
            record = store ? store.getAt(0) : null; //get first record

        if(values) {
            me.getForm().setValues(values);
        }

        if(!values.engagementCode) //pre-set the first engagement code if it wasn't already set
        {
            combo.setValue(record ? record.id : null);
        }
    }
    , show: function(parent) {
        var me = this;
        me.callingParent = parent;
        eBook.Forms.AnnualAccounts.Manager.superclass.show.call(me);
    }
});//This form is, as a panel, not stand-alone.
//It does not have the server-contact capability. It does however contain all the functions and info for the parent to utilize this form.
//Its functions allow form interaction, data validation and retrieval.
eBook.Forms.AnnualAccounts.Partner = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        //Vars
        var me = this;
        //Set properties that will be used by parent

        //Form
        Ext.apply(this, {
            layout: 'form'
            , labelWidth: 200
            , title: 'Annual Accounts information form'
            , bodyStyle: 'padding:10px;'
            //, resizable: true
            , padding:10
            , ref: 'formPanel'
            , items: [
                {
                    xtype: 'combo'
                    , ref: 'engagementCode'
                    , fieldLabel: 'Engagement code'
                    , name: 'engagementCode'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , forceSelection: true
                    , valueField: 'Code'
                    , displayField: 'Code'
                    ,tpl: new Ext.XTemplate(
                        '<tpl for="."><div class="x-combo-list-item">{Code} - {Description}</div></tpl>')
                    , editable: false
                    , mode: 'local'
                    , anchor: '95%'
                    , store: new eBook.data.JsonStore({
                        selectAction: 'GetEngagementCodes'
                        , criteriaParameter: 'cidc'
                        , autoDestroy: true
                        , serviceUrl: eBook.Service.annualAccount
                        , storeParams: {Id: eBook.CurrentClient}
                        , baseParams: {Id: eBook.CurrentClient}
                        , disabled: this.readOnly
                        , fields: eBook.data.RecordTypes.Engagement
                        , autoLoad: true
                    })
                }
            ],
            listeners: {
                afterrender: function (combo) {
                    //remove mask if there is one
                    if(me.maskedCmp) {
                        me.maskedCmp.getEl().unmask();
                    }
                }
            }
        });
        eBook.Forms.AnnualAccounts.Partner.superclass.initComponent.apply(this, arguments);
    },
    //Check if form is valid
    isValid: function()
    {
        //vars
        var me = this,
            invalid = null;

        //validation
        if(!me.engagementCode.isValid() || !me.engagementCode.getValue()) invalid = " engagement code";

        //return en alert
        if(invalid) {
            Ext.Msg.alert("Not filled in", "Please fill in the " + invalid + ".");
            return false;
        }

        return true;
    },
    //Get json data of form
    getData: function() {
        //vars
        var me = this,
            cfaadc = {},
            combo = me.engagementCode,
            store = combo ? combo.getStore() : null,
            record = store ? store.getById(combo.getValue()): null; //get record

        //create contract
        cfaadc.EngagementCode = record ? record.get("Code") : null;
        cfaadc.ServiceLine = record ? record.get("ServiceLine"): null;

        return cfaadc;
    }
    , setData: function(values) {
        var me = this,
            combo = me.engagementCode,
            store = combo ? combo.getStore() : null,
            record = store ? store.getAt(0) : null; //get first record

        if(values) {
            me.getForm().setValues(values);
        }

        if(!values.engagementCode) //pre-set the first engagement code if it wasn't already set
        {
            combo.setValue(record ? record.id : null);
        }
    }
    , show: function(parent) {
        var me = this;
        me.callingParent = parent;
        eBook.Forms.AnnualAccounts.Partner.superclass.show.call(me);
    }
});

eBook.Client.HomeHeader = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
                , '<div class="boxSides">'
                    , '<div id="eyMenuNav" style="">'
                        , '<div class="eBook-Client">'
                            , '<div class="eBook-menu-text">{Name}</div>'
                            , '<div class="eBook-menu-close"> '
                                , '<div class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div>'
                            , '</div>		'
       	                , '</div>'
                    , '</div>'
                , '</div>	'
                , '<div class="eBook-menu-info-block">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-address">'
                         , '{Address}'
                        , '<br>{ZipCode} {City}'
                        , '<tpl if="Shared"><div class="eBook-menu-info-shared">GEDEELD DOSSIER ACR/TAX</div></tpl>'
                        , '<tpl if="bni"><div class="eBook-menu-info-shared">BNI</div></tpl>'
                     , '</div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-PMT">'
                        , 'Current role(s): <br/><b>{rollen}</b>'
                        , '<div class="eBook-menu-info-PMT-Team">{teamleden}</div>'
                    , '</div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-ProAcc">'
                        , '<b><u>ProAcc</u></b><br>'
                        , '<tpl if="ProAccServer==null || ProAccServer==\'\'">{NoProAcc}</tpl>'
                        , '<tpl if="ProAccServer!=null && ProAccServer!=\'\'">'
                            , 'Database: <b>{ProAccDatabase}</b>'
                            , '<br/>Server: <b>{ProAccServer}</b>'
    // , '<br/>Link updated: <b>{ProAccLinkUpdated:date("d/m/Y H:i:s")}</b>'
                        , '</tpl>'
                    , '</div>'
                 , '</div>'
                , '<div style="visibility: visible;" class="eBook-menu">'
                    , '<div class="eBook-menu-childcontainer">'
                        , '<div class="eBook-menu-item" id="eBook-Client-suppliers">'
                            , '<div class="eBook-menu-item-icon eBook-suppliers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{leveranciers}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-Client-customers">'
                            , '<div class="eBook-menu-item-icon eBook-customers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{klanten}</div>'
                        , '</div>'
                        , '<div id="eBook-Client-files-add" class="eBook-menu-item ">'
                            , '<div class="eBook-menu-item-icon eBook-createfile-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{nieuwdossier}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-Client-files-deleted">'
                            , '<div class="eBook-menu-item-icon eBook-marked-deletion-files-menu-ico  eBook-notitle eBook-recyclebin-full">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                        , '</div>'
                    , '</div>'
                , '</div>'
            , '</tpl>', { compiled: true })
    , initComponent: function() {

        Ext.apply(this, {
            html: '<div>open client</div>'
            , border: false
        });
        eBook.Client.HomeHeader.superclass.initComponent.apply(this, arguments);
    }
    , afterRender: function() {
        eBook.Client.HomeHeader.superclass.afterRender.call(this);

        // set events
        this.el.on('mouseover', this.onBodyMouseOver, this);
        this.el.on('mouseout', this.onBodyMouseOut, this);
        this.el.on('click', this.onBodyClick, this);
        //this.el.on('contextmenu', this.onBodyContext, this);
    }
    , onBodyMouseOver: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        if (mnuItem) {
            Ext.get(mnuItem).addClass('eBook-menu-item-over');
            //   Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '90%');
        }
    }
    , onBodyMouseOut: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        if (mnuItem) {
            Ext.get(mnuItem).removeClass('eBook-menu-item-over');
            // Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '100%');
        }
    }
    , onBodyClick: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var closeClient = e.getTarget('.eBook-menu-close');
        var team = e.getTarget('.eBook-menu-info-PMT-Team');

        if (mnuItem) {
            eBook.Interface.openWindow(mnuItem.id);
        } else if (closeClient) {
            eBook.Interface.closeClient();
        } else if (team) {
            eBook.Interface.openWindow('clientteam');
        }
    }
    , updateMe: function(rec) {
        if (this.trashcanDDTarget) this.trashcanDDTarget.destroy();
        var dta = {};
        Ext.apply(dta, {
            rollen: eBook.User.activeClientRoles
            , teamleden: eBook.PMT.TeamWindow_Title
            , NoProAcc: 'GEEN PROACC CONNECTIE'
            , leveranciers: eBook.Menu.Client_Suppliers
            , klanten: eBook.Menu.Client_Customers
            , nieuwdossier: eBook.Menu.Client_NewFile
        });
        Ext.apply(dta, rec.data);
        this.body.update(this.mainTpl.apply(dta));
        this.trashcanEl = this.body.child('#eBook-Client-files-deleted');
        this.trashcanDDTarget = new Ext.dd.DropTarget(this.trashcanEl, {
            ddGroup: 'filesDDGroup'
            , notifyDrop: function(ddSource, e, data) {
                eBook.Interface.center.clientMenu.files.openFiles.onDeleteClick(data.draggedRecord);                
                return true;
            }
        });
    }
    /*
    , highlightTrashCan: function() {
    if (this.trashcanEl) {
    this.trashcanEl.addClass('eBook-menu-item-over');
    //this.trashcanEl.setStyle('background-color','red');
    }
    }
    , unHighlightTrashCan: function() {
    if (this.trashcanEl) {
    this.trashcanEl.removeClass('eBook-menu-item-over');
    //this.trashcanEl.setStyle('background-color', 'white');
    }
    }
    */
});


Ext.reg('clienthomeheader', eBook.Client.HomeHeader);

eBook.Client.HomeFiles = Ext.extend(Ext.TabPanel, {
    initComponent: function() {
        Ext.apply(this, {
            items: [
                { xtype: 'filelist', openFiles: true, closedFiles: false, deletedFiles: false
                    , title: 'Open files', ref: 'openFiles'
                }
                , { xtype: 'filelist', openFiles: false, closedFiles: true, deletedFiles: false
                    , title: 'Closed files', ref: 'closedFiles'
                }
                ]
            , activeTab: 0
        });
        eBook.Client.HomeFiles.superclass.initComponent.apply(this, arguments);
    }
    , loadTabs: function(clientId) {
        this.openFiles.activeClient = clientId;
        this.closedFiles.activeClient = clientId;
        this.openFiles.store.load({ params: { ClientId: clientId} });
        this.closedFiles.store.load({ params: { ClientId: clientId} });
        this.trashcanFiles = this.getCountTrashcanFiles(clientId);
        
    }
    , reloadTabs: function() {
        if (this.openFiles.activeClient) {
            this.loadTabs(this.openFiles.activeClient);
            this.dragAndDrop();
        }
    }
    , getCountTrashcanFiles: function(clientId) {
        Ext.Ajax.request({
            url: eBook.Service.file + 'GetFileInfos'
            , method: 'POST'
            , params: Ext.encode({ cfdc: { ClientId: clientId, MarkedForDeletion: true, Deleted: false, Closed: false} })
            , callback: this.handleCallbackTrashcanFiles
            , scope: this
        });
    }
    , handleCallbackTrashcanFiles: function(opts, success, resp) {
        var o = Ext.decode(resp.responseText);
        var parent = Ext.get('eBook-Client-files-deleted');
        var icon = parent.select('div.eBook-marked-deletion-files-menu-ico');
        if (o.GetFileInfosResult.length > 0) {
            icon.addClass('eBook-recyclebin-full');
        } else {
            icon.removeClass('eBook-recyclebin-full');
        }
    }
    , getFileName: function(id) {
        var idx = this.openFiles.store.find('Id', id);
        if (idx > -1) {
            return this.openFiles.store.getAt(idx).get('Name');
        } else {
            var idx = this.closedFiles.store.find('Id', id);
            if (idx > -1) {
                return this.closedFiles.store.getAt(idx).get('Name');
            }
        }
        return 'deleted file';
    }
    , getLatestId: function() {

        var ro, rc;
        if (this.openFiles.store.getCount() > 0) {
            ro = this.openFiles.store.getAt(0);
        }
        if (this.closedFiles.store.getCount() > 0) {
            rc = this.closedFiles.store.getAt(0);
            if (ro) {
                if (ro.get('EndDate') < rc.get('EndDate')) ro = rc;
            } else {
                ro = rc;
            }
        }
        if (ro) {
            return ro.get('Id');
        }
        return null;
    }
    , dragAndDrop: function() {
        this.get


        /*
        // Configure the cars to be draggable
        var carElements = Ext.get('cars').select('div');
        Ext.each(carElements.elements, function(el) {
        var dd = new Ext.dd.DD(el, 'carsDDGroup', {
        isTarget  : false
        });
        Ext.apply(dd, overrides);
        });
        */
    }
});
Ext.reg('clienthomefiles', eBook.Client.HomeFiles);


eBook.Client.FilesList = Ext.extend(Ext.list.ListView, {
    initComponent: function() {
        Ext.apply(this, {
            store:
                 new Ext.data.JsonStore({
                     autoDestroy: true,
                     root: 'GetFileInfosResult',
                     fields: eBook.data.RecordTypes.FileInfo,
                     baseParams: {
                         MarkedForDeletion: this.deletedFiles
                        , Deleted: false
                        , Closed: this.closedFiles
                     },
                     proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                         url: eBook.Service.file + 'GetFileInfos'
                         , criteriaParameter: 'cfdc'
                         , method: 'POST'
                     }),
                     listeners: {
                     /*

                         'load': function(store, records, options) {

                             // Create an object that we'll use to implement and override drag behaviors a little later
                     var overrides = {
                     // Called the instance the element is dragged.
                     b4StartDrag: function() {
                     // Cache the drag element
                     if (!this.el) {
                     this.el = Ext.get(this.getEl());
                     }
                     this.el.setStyle('z-index', 8000);
                     this.el.setStyle('border', '1px solid');



                                     //Cache the original XY Coordinates of the element, we'll use this later.
                     this.originalXY = this.el.getXY();
                     },
                     // Called when element is dropped not anything other than a dropzone with the same ddgroup
                     onInvalidDrop: function() {
                     // Set a flag to invoke the animated repair
                     this.invalidDrop = true;
                     },
                     // Called when the drag operation completes
                     endDrag: function() {
                     // Invoke the animation if the invalidDrop flag is set to true
                     if (this.invalidDrop === true) {
                     // Remove the drop invitation
                     this.el.removeClass('dropOK');

                                         // Create the animation configuration object
                     var animCfgObj = {
                     easing: 'elasticOut',
                     duration: 1,
                     scope: this,
                     callback: function() {
                     // Remove the position attribute
                     this.el.dom.style.position = '';
                     }
                     };

                                         // Apply the repair animation
                     this.el.moveTo(this.originalXY[0], this.originalXY[1], animCfgObj);
                     delete this.invalidDrop;
                     this.el.addClass('cancelOnClick');
                     }

                                 },
                     // Called upon successful drop of an element on a DDTarget with the same
                     onDragDrop: function(evtObj, targetElId) {
                     // Wrap the drop target element with Ext.Element
                     var dropEl = Ext.get(targetElId);

                                     // Perform the node move only if the drag element's 
                     // parent is not the same as the drop target
                     if (this.el.dom.parentNode.id != targetElId) {

                                         // Move the element
                     dropEl.appendChild(this.el);

                                         // Remove the drag invitation
                     this.onDragOut(evtObj, targetElId);

                                         // Clear the styles
                     this.el.dom.style.position = '';
                     this.el.dom.style.top = '';
                     this.el.dom.style.left = '';
                     }
                     else {
                     // This was an invalid drop, initiate a repair
                     this.onInvalidDrop();
                     }
                     }
                     };

                             // Configure the cars to be draggable
                     var fileElements = Ext.select('.client-home-openFiles dl');
                     Ext.each(fileElements.elements, function(el) {
                     var dd = new Ext.dd.DD(el, 'filesDDGroup', {
                     isTarget: false
                     });
                     Ext.get(el).setStyle('z-index', '80000');

                                 //Apply the overrides object to the newly created instance of DD
                     Ext.apply(dd, overrides);
                     });

                         }*/
                 }
             })
            , multiSelect: false
            , loadingText: 'Loading files...'
            , emptyText: eBook.Menu.Client_NoOpenFiles
            , reserveScrollOffset: false
            , cls: 'client-home-openFiles'
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        //eBook.Interface.center.clientMenu.mymenu.unHighlightTrashCan();
                        if ((nd.className).indexOf('cancelOnClick') == -1) {
                            eBook.Interface.openFile(lv.getStore().getAt(idx), true);
                        } else {
                            Ext.get(nd).removeClass('cancelOnClick');
                        }
                        //eBook.Interface.clientMenu.toggleGroupBy('eBook-Client-files-openlist');
                    }
                , scope: this
                }
                ,
                'contextmenu': {
                    fn: this.onMyBodyContext
                    /*function() {
                    eBook.Client.FilesList.superclass.afterRender.call(this);
                    this.innerBody.on('contextmenu', this.onBodyContext, this);
                    }*/
                    , scope: this

                }

            }
            , columns: [new eBook.List.QualityColumn({
                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: eBook.Menu.Client_FileName,
                width: .55,
                dataIndex: 'Name'
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'StartDate'
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'EndDate'
}]


            });
            eBook.Client.FilesList.superclass.initComponent.apply(this, arguments);
        }
        , afterRender: function() {

            var v = this;
            if (v.ref == 'openFiles') {
                v.dragZone = new Ext.dd.DragZone(v.innerBody, {
                    ddGroup: 'filesDDGroup',
                    afterInvalidDrop: function() {
                        //eBook.Interface.center.clientMenu.mymenu.unHighlightTrashCan();
                    },
                    getDragData: function(e) {


                        var sourceEl = e.getTarget('dl', 10);


                        if (sourceEl) {
                            
                            d = sourceEl.cloneNode(true);
                            //  d.setStyle('z-index', 8000);
                            //d = Ext.DomHelper.createDom("<div> What the... you're dragging me!</div>");
                            //v.getEl().appendChild(d);
                            d.id = Ext.id();                            

                            return v.dragData = {
                                ddel: d,
                                sourceEl: sourceEl,
                                repairXY: Ext.fly(sourceEl).getXY(),
                                sourceStore: v.store,
                                draggedRecord: v.getRecord(sourceEl)
                            };
                            
                        }
                    },
                    getRepairXY: function() {
                        return this.dragData.repairXY;
                    }
                });
            }
            eBook.Client.FilesList.superclass.afterRender.apply(this, arguments);
        }
        , onMyBodyContext: function(dv, idx, nde, e) {
            //var el = e.getTarget();
            if (dv.ref == 'openFiles') {
                //eBook.Interface.center.clientMenu.mymenu.unHighlightTrashCan();
                if (!this.contextMenu) {
                    this.contextMenu = new eBook.Client.ContextMenu({ scope: this });
                    this.contextMenu.addItem({
                        text: 'Delete',
                        iconCls: 'eBook-icon-delete-16',
                        handler: function() {
                            this.scope.onDeleteClick(this.activeRecord);
                        },
                        ref: 'deleteFile',
                        scope: this.contextMenu
                    });
                }
                this.contextMenu.activeRecord = dv.store.getAt(idx);



                this.contextMenu.showMeAt(e);
            }

        }
        , onDeleteClick: function(activeRecord) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'SetFileMarkDeleted'
                , method: 'POST'
                , params: Ext.encode({ cidc: { Id: activeRecord.get('Id')} })
                 , callback: this.onRestoreClickCallback
                , scope: this
            });
        }
        , onRestoreClickCallback: function(opts, success, resp) {
            if (success) {
                this.store.load({ params: { ClientId: this.activeClient} });
                eBook.Interface.center.clientMenu.files.reloadTabs();
            }
        }
    });

Ext.reg('filelist', eBook.Client.FilesList);

 //, style: 'background-color:#FFF;margin-left:60px;margin-right:60px;'
                        //, bodyStyle:'background-color:transparent;margin-left:60px;margin-right:60px;'
eBook.Client.Home = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , items: [{ xtype: 'clienthomeheader', region: 'north', height: 250,ref:'mymenu' }
                      , {xtype:'panel', region:'center'
                            , bodyStyle: 'padding-left:60px;padding-right:60px'
                            , layout: 'fit'
                            , border:false
                            ,items: [
                                { xtype: 'panel', layout: 'border', border: false
                                     ,items : [
                                        {ref:'../../repository', xtype: 'repository', region: 'center', title:'Repository',border:true }
                                        , { ref: '../../files', xtype: 'clienthomefiles', region: 'east', width: 400, title: 'Files' }
                                        ]
                                }]
                      }]
        });
        eBook.Client.Home.superclass.initComponent.apply(this, arguments);
    }
    , loadRec: function(rec) {
        this.mymenu.updateMe(rec);
        this.files.loadTabs(rec.get('Id'));
    }
});

Ext.reg('clienthome', eBook.Client.Home);

eBook.Client.Line = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'hbox'
            , border: false
            , bodyStyle: 'padding:5px;border-bottom:1px dashed #999999;vertical-align:middle'
            , layoutConfig: {
                defaultMargins: { top: 0, right: 0, bottom: 0, left: 10 }
            }
            , overCls: 'eBook-over'
            , items: [{
                    xtype: 'label',
                    ref:'label',
                    text: this.fieldName,
                    flex: 1,
                    style: 'padding-top:5px;'
                }, {
                    xtype: this.fieldType,
                    ref: 'gfis',
                    disabled:true,
                    flex: 1
                }, {
                    xtype: this.fieldType,
                    ref: 'override',
                    flex: 1
            }]
        });
        eBook.Client.Line.superclass.initComponent.apply(this, arguments);
        
        
        if (Ext.isDefined(this.gfisValue) || Ext.isDefined(this.eBookValue)) {
            this.setValue(this.gfisValue,this.eBookValue);
        }
        
        this.on('afterrender', function(pnl) {
             this.getEl().on('click',function(el) {
                pnl.override.focus();
             },pnl);
        },this);
    }
    , setValue: function(gfis, override) {
        this.gfis.reset();
        this.gfis.setValue(gfis);
        this.override.reset();
        this.override.setValue(override);
    }
    ,getValue:function() {
        var val = this.override.getValue();
        if (Ext.isEmpty(val)) return null;
        return val;
    }
});

Ext.reg('eBook.Client.Line', eBook.Client.Line);




eBook.Client.HeadingLine = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'hbox'
            , border: false
            , bodyStyle: 'padding:5px;border-bottom:1px solid #000000;vertical-align:middle'
            , layoutConfig: {
                defaultMargins: { top: 0, right: 0, bottom: 0, left: 10 }
            }
            , items: [{
                        xtype: 'label',
                        ref: 'label',
                        text: this.heading1,
                        flex: 1,
                        style: 'padding-top:5px;font-weight:bold;font-size:12px;'
                    }, {
                        xtype: 'label',
                        ref: 'label',
                        text: this.heading2,
                        flex: 1,
                        style: 'padding-top:5px;font-weight:bold;font-size:12px;'
                    }, {
                        xtype: 'label',
                        ref: 'label',
                        text: this.heading3,
                        flex: 1,
                        style: 'padding-top:5px;font-weight:bold;font-size:12px;'
                    }]
        });
        eBook.Client.HeadingLine.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('eBook.Client.HeadingLine', eBook.Client.HeadingLine);

eBook.Client.Panel = Ext.extend(Ext.FormPanel, {
    lines: []
    , initComponent: function() {

        Ext.apply(this, {
            border: false
            , items: this.lines
        });
        eBook.Client.Panel.superclass.initComponent.apply(this, arguments);
    }
    , loadData: function(gfisObject, overruleObject) {
        for (var i = 0; i < this.lines.length; i++) {
            var line = this.lines[i];
            if (line.xtype == "eBook.Client.Line") this[line.ref].setValue(gfisObject[line.mapping], overruleObject[line.mapping]);
        }
        this.clientOverrule = overruleObject;
    }
    , getData: function() {
        for (var i = 0; i < this.lines.length; i++) {
            if (this.lines[i].xtype == "eBook.Client.Line") {
                this.clientOverrule[this.lines[i].mapping] = this[this.lines[i].ref].getValue();
            }
        }
        return this.clientOverrule;
    }
    , hasChanges: function() {
        for (var i = 0; i < this.lines.length; i++) {
            if (this.lines[i].xtype == "eBook.Client.Line") {
                var b = this.clientOverrule[this.lines[i].mapping] == this[this.lines[i].ref].getValue();
                if (!b) return true;
            }
        }
        return false;
    }
});

Ext.reg('eBook.Client.Panel', eBook.Client.Panel);
        


eBook.Client.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            bodyStyle: 'padding:10px;background-color:#FFFFFF;'
            , autoScroll: true
            , tbar: [{
                ref: 'save',
                text: eBook.Client.Window_SaveChanges,
                iconCls: 'eBook-icon-tablesave-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveChangesClick,
                scope: this
}]
            , items: [
                    { xtype: 'eBook.Client.Panel'
                    , ref: 'panel'
                    , lines: [
                        {
                            xtype: 'eBook.Client.HeadingLine'
                            , heading1: ''
                            , heading2: eBook.Client.Window_GFIS
                            , heading3: eBook.Client.Window_eBookOverrule
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Name
                            , fieldType: 'textfield'
                            , ref: 'name'
                            , mapping: 'n'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Address
                            , fieldType: 'textfield'
                            , ref: 'address'
                            , mapping: 'a'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_ZipCode
                            , fieldType: 'textfield'
                            , ref: 'zipcode'
                            , mapping: 'zip'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_City
                            , fieldType: 'textfield'
                            , ref: 'city'
                            , mapping: 'ci'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Country
                            , fieldType: 'textfield'
                            , ref: 'country'
                            , mapping: 'co'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Vat
                            , fieldType: 'textfield'
                            , ref: 'vat'
                            , mapping: 'vnr'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_EnterpriseNr
                            , fieldType: 'textfield'
                            , ref: 'enterprise'
                            , mapping: 'enr'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Phone
                            , fieldType: 'textfield'
                            , ref: 'phone'
                            , mapping: 'ph'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Fax
                            , fieldType: 'textfield'
                            , ref: 'fax'
                            , mapping: 'fx'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Email
                            , fieldType: 'textfield'
                            , ref: 'email'
                            , mapping: 'em'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_LegalStruct
                            , fieldType: 'textfield'
                            , ref: 'legalstructure'
                            , mapping: 'ls'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Rpr
                            , fieldType: 'textfield'
                            , ref: 'rpr'
                            , mapping: 'rpr'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Director
                            , fieldType: 'textfield'
                            , ref: 'director'
                            , mapping: 'dn'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_VatElegible
                            , fieldType: 'yesno'
                            , ref: 'isvat'
                            , mapping: 'iv'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Active
                            , fieldType: 'yesno'
                            , ref: 'isactive'
                            , mapping: 'ia'
}]
}]
            });
            eBook.Client.Window.superclass.initComponent.apply(this, arguments);
        }
    , show: function() {
        eBook.Client.Window.superclass.show.call(this);
        this.getEl().mask(eBook.Client.Window_Loading, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.client + 'GetClientInfo'
                , method: 'POST'
                , params: Ext.encode({ cidc: {
                    Id: eBook.Interface.currentClient.get('Id')
                    }
                })
                , callback: this.onClientLoaded
                , scope: this
        });
    }
    , onClientLoaded: function(opts, s, resp) {
        if (s) {
            var o = Ext.decode(resp.responseText).GetClientInfoResult;
            this.panel.loadData(o.gfis, o.overrule);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onSaveChangesClick: function(b, e, l) {
        if (!Ext.isDefined(l)) l = false;
        var action = {
            text: eBook.Client.Window_Saving
            , params: {
                cdc: this.panel.getData()
            }
            , action: 'SaveClientOverrule'
            , url: eBook.Service.client
            , lockWindow: l
        };
        this.addAction(action);
    }
    , onBeforeClose: function() {
        if (this.panel.hasChanges()) {

            Ext.MessageBox.show({
                title: eBook.Client.Window_SaveTitle,
                msg: eBook.Client.Window_SaveMsg,
                buttons: Ext.MessageBox.YESNO,
                animEl: this.getEl(),
                fn: this.closingWithChanges,
                scope: this,
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }
        return eBook.Client.Window.superclass.onBeforeClose.call(this);
    }
    , closingWithChanges: function(b) {
        if (b == 'yes') {
            this.onSaveChangesClick(null, null, true);
            this.addAction({ action: 'CLOSE_WINDOW' });
            return;
        }
        this.doClose();
    }

});


eBook.Client.PreWorksheetWindow = Ext.extend(eBook.Window, {
    ruleApp: ''
    , typeName: ''
    , clientId: ''
    , initComponent: function() {
        Ext.apply(this, {
            title: eBook.Client.PreWorksheetWindow_Title
            , tbar: [{
                ref: 'opensheet',
                text: eBook.Client.PreWorksheetWindow_Create,
                iconCls: 'eBook-icon-tablesave-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onOpenSheet,
                scope: this
}]
            , width: 450
            , height: 300
            , items: [
               { xtype: 'form'
                   , ref: 'formitems'
                   , labelWidth: 200
                   , items: [
                       {
                           xtype: 'textfield'
                            , ref: 'filename'
                            , name: 'filename'
                            , allowBlank: false
                            , fieldLabel: eBook.Client.PreWorksheetWindow_Description
                            , anchor: '90%'
                       }, {
                           xtype: 'languagebox'
                             , ref: 'language'
                             , name: 'language'
                             , allowBlank: false
                             , fieldLabel: eBook.Create.Empty.FileInfoFieldSet_Language
                             , anchor: '90%'
                       }, {
                           xtype: 'datefield'
                            , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                            , allowBlank: false
                            , format: 'd/m/Y'
                            , name: 'StartDate'
                            , ref: 'startdate'
                       }, {
                           xtype: 'datefield'
                            , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                            , allowBlank: false
                            , format: 'd/m/Y'
                            , name: 'EndDate'
                            , ref: 'enddate'
                       }, {
                           xtype: 'datefield'
                            , fieldLabel: eBook.Client.PreWorksheetWindow_PreviousEnddate
                            , allowBlank: true
                            , format: 'd/m/Y'
                            , name: 'PreviousEndDate'
                            , ref: 'previousenddate'
                       }

                    ]
               }
            ]
            });
            eBook.Client.PreWorksheetWindow.superclass.initComponent.apply(this, arguments);
        }
    , onOpenSheet: function() {
        this.showWorksheet(this.worksheetId);
    }
    , showNew: function() {
        this.show();
    }
    , showWorksheet: function(id) {
        if (this.checkValid()) {
            var manual = this.createManual(id);
            var ed = this.formitems.enddate.getValue();
            var ass = ed.getFullYear();
            if (ed.getDate() == 31 && ed.getMonth() == 11) ass++;
            eBook.Worksheet.showManual(this.ruleApp, this.typeName, ass, manual)
            this.close();
        }
    }
    , checkValid: function() {
        return this.formitems.filename.isValid() &&
                this.formitems.language.isValid() &&
                this.formitems.startdate.isValid() &&
                this.formitems.enddate.isValid();
    }
    , show: function(rec) {
        eBook.Client.PreWorksheetWindow.superclass.show.call(this);
        if (rec) {
            this.getTopToolbar().opensheet.setText(eBook.Client.PreWorksheetWindow_Open);
            this.worksheetId = rec.json.id;
            this.formitems.filename.setValue(rec.json.n);
            this.formitems.language.setValue(rec.json.m.File.c);
            this.formitems.startdate.setValue(Ext.data.Types.WCFDATE.convert(rec.json.m.File.sd));
            this.formitems.enddate.setValue(Ext.data.Types.WCFDATE.convert(rec.json.m.File.ed));
            this.formitems.previousenddate.setValue(Ext.data.Types.WCFDATE.convert(rec.json.m.File.ped));
        }
    }
    , createManual: function(id) {
        if (!id) id = eBook.EmptyGuid;
        var ped = this.formitems.previousenddate.getValue();
        if (ped == '') {
            ped = null;
        }
        return {
            Client: null
            , File: {
                id: eBook.EmptyGuid
                , ft: null
                , cid: eBook.CurrentClient
                , n: this.formitems.filename.getValue()
                , c: this.formitems.language.getValue()
                , sd: this.formitems.startdate.getValue()
                , ed: this.formitems.enddate.getValue()
                , ped: ped
                , nl: false
                , al: 8
                , md: false
                , d: false
                , ers: 0
                , wrs: 0
                , dsp: 'display'
            }
            , Id: id
            , Description: this.formitems.filename.getValue()
        };
    }
    });
eBook.Client.ContextMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'eBook-client-context-menu'
            , shadow: false
            // Add items on creation            
            });
            eBook.Client.ContextMenu.superclass.initComponent.apply(this, arguments);
        }
    
    , showMeAt: function(el) {
        eBook.Client.ContextMenu.superclass.showAt.apply(this, [el.xy]);
    }
    });

    Ext.reg('clientcontextmenu', eBook.Client.ContextMenu);
eBook.Client.TrashcanList = Ext.extend(Ext.list.ListView, {
    initComponent: function() {
        var contextMenuListener;
        if (this.mgrRole) {
            contextMenuListener = {
                'contextmenu': {
                    fn: this.onMyBodyContext
                    , scope: this
                }
            };
        }
        Ext.apply(this, {
            store:
                 new Ext.data.JsonStore({
                     autoDestroy: true,
                     autoLoad: true,
                     root: 'GetFileInfosResult',
                     fields: eBook.data.RecordTypes.FileInfo,
                     baseParams: {
                         MarkedForDeletion: this.deletedFiles
                        , Deleted: false
                        , Closed: false
                        , ClientId: eBook.CurrentClient
                     },
                     proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                         url: eBook.Service.file + 'GetFileInfos'
                         , criteriaParameter: 'cfdc'
                         , method: 'POST'
                     })
                 })
            , multiSelect: false
            , loadingText: 'Loading files...'
            , emptyText: eBook.Menu.Client_NoOpenFiles
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: contextMenuListener
            , columns: [new eBook.List.QualityColumn({
                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: eBook.Menu.Client_FileName,
                width: .55,
                dataIndex: 'Name'
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'StartDate'
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'EndDate'
}]


            });
            eBook.Client.TrashcanList.superclass.initComponent.apply(this, arguments);
        }
        , onMyBodyContext: function(dv, idx, nde, e) {
            //var el = e.getTarget();

            if (!this.contextMenu) {
                this.contextMenu = new eBook.Client.ContextMenu({ scope: this });
                this.contextMenu.addItem({
                    text: 'Delete permanently',
                    iconCls: 'eBook-icon-delete-16',
                    handler: function() {
                        this.scope.onPermanentDeleteClick(this.activeRecord);
                    },
                    ref: 'deleteFile'
                    , scope: this.contextMenu
                });
                //if (this.admRole) {
                if (this.mgrRole) {
                    this.contextMenu.addItem({
                        text: 'Restore',
                        iconCls: 'eBook-icon-arrowrefresh-16',
                        handler: function() {
                            this.scope.onRestoreClick(this.activeRecord);
                        },
                        ref: 'restoreFile',
                        scope: this.contextMenu
                    });
                }
            }
            this.contextMenu.activeRecord = dv.store.getAt(idx);


            this.contextMenu.showMeAt(e);

        }
        , onPermanentDeleteClick: function(activeRecord) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'DeleteFile'
                    , method: 'POST'
                    , params: Ext.encode({ cidc: { Id: activeRecord.get('Id')} })
                     , callback: function(opts, success, resp) { success ? this.store.load({ params: { ClientId: this.activeClient} }) : alert('failure') }
                    , scope: this
            });
        }
        , onRestoreClick: function(activeRecord) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'SetFileUnMarkDeleted'
                        , method: 'POST'
                        , params: Ext.encode({ cidc: { Id: activeRecord.get('Id')} })
                         , callback: this.onRestoreClickCallback
                        , scope: this
            });
        }
        , onRestoreClickCallback: function(opts, success, resp) {
            if (success) {
                this.store.load({ params: { ClientId: this.activeClient} });
                eBook.Interface.center.clientMenu.files.reloadTabs();
            }
        }
    });

    Ext.reg('trashcanlist', eBook.Client.TrashcanList);
eBook.Client.TrashcanWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        var tb = [];
        var mgrRole = eBook.User.isActiveClientRolesFullAccess();
        var admRole = eBook.User.isCurrentlyChampion() || eBook.User.isCurrentlyAdmin();
        /*
        if (mgrRole) {
            tb = [{
                text: 'Empty trashcan',
                iconCls: 'eBook-pmt-refresh-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'updatepmt',
                handler: this.onEmptyTrashcan,
                scope: this
            }];
        }
        */
        Ext.apply(this, {
            layout: 'fit',
            modal: false,
            iconCls: 'eBook-recycle-menu-context-icon',
            title: 'Trashcan',
            width: 550,
            items: [{
                xtype: 'trashcanlist',
                ref: 'list',
                openFiles: true, closedFiles: false, deletedFiles: true
            , mgrRole: mgrRole, admRole: admRole
        }]
        , tbar: tb
            });
            eBook.Client.TrashcanWindow.superclass.initComponent.apply(this, arguments);
        }
    , onEmptyTrashcan: function() {
        alert('Empty list');

    }
});
eBook.NewFile.Wizard = Ext.extend(Ext.Panel, {
    data: {}
    , initComponent: function () {

        Ext.apply(this, {
            layout: 'card'
            , layoutConfig: {
                // layout-specific configs go here
                titleCollapse: false,
                animate: true,
                activeOnTop: false,
                hideCollapseTool: true
            }
            , activeItem: 0
            , items: [
                      { xtype: 'eBook.NewFile.FileServices', ref: 'step1', title: 'Services' }
                    , { xtype: 'eBook.NewFile.ImportType', txtStep: 10, excelStep: 9, proaccStep: 8, exactAdminStep: 3, exactFinYearStep: 4, defineBookyearStep: 5, ref: 'step2', title: 'Select file type' }
                    , { xtype: 'eBook.NewFile.ExactSettings', ref: 'step3', title: 'Exact: Administrations', inactive: true }
                    , { xtype: 'eBook.NewFile.ExactFinYears', ref: 'step4', title: 'Exact: Financial years', inactive: true }
                    , { xtype: 'eBook.NewFile.StartEnd', ref: 'step5', title: 'Define bookyear', inactive: false }
                    , { xtype: 'eBook.NewFile.SelectPrevious', ref: 'step6', title: 'Select previous eBook File' }
                    , { xtype: 'eBook.NewFile.PreviousStartEnd', ref: 'step7', title: 'Define previous bookyear' }
                    , { xtype: 'eBook.NewFile.ProAccSettings', ref: 'step8', title: 'ProAcc import settings', inactive: true }
                    , { xtype: 'eBook.NewFile.UploadExcel', ref: 'step9', title: 'Upload Excel file', inactive: true }
                    , { xtype: 'eBook.NewFile.UploadTxt', ref: 'step10', title: 'Upload Textfile', inactive: true }
                    , { xtype: 'eBook.NewFile.FileSettings', ref: 'step11', title: 'Settings/Meta' }
                    , { xtype: 'eBook.NewFile.CreationOverview', ref: 'step12', title: 'Creation overview' }
                    , { xtype: 'eBook.NewFile.CreateFile', ref: 'step13', title: 'Create new file' }
            //, { xtype: 'eBook.NewFile.StartEnd', ref: 'step8', title: '&gt; Review mappings' }

                ]
        });
        eBook.NewFile.Wizard.superclass.initComponent.apply(this, arguments);
    }
    , previousStep: 0
    , currentStep: 1
    , maxStep: 13
    , movePrevious: function () {
        var curStep = this.getLayout().activeItem;
        var currentStep = curStep.stepId;
        if (!curStep.inactive) this.ownerCt.updateState(currentStep, curStep.validate());
        if (currentStep > 1) {
            currentStep--;

            this.getLayout().setActiveItem(this['step' + currentStep]);
            //this.previousStep--;
            curStep = this['step' + currentStep];
            if (curStep.inactive == true) {
                this.ownerCt.updateState(currentStep, 'inactive');
                this.movePrevious();
            } else {
                this.ownerCt.updateState(currentStep, null);
            }
            curStep.doLayout();
        }

    }
    , moveNext: function () {
        var curStep = this.getLayout().activeItem;
        var currentStep = curStep.stepId;
        if (!curStep.inactive) this.ownerCt.updateState(currentStep, curStep.validate());
        if (currentStep < this.maxStep) {
            currentStep++;
            //this.previousStep++;
            this.getLayout().setActiveItem(this['step' + currentStep]);
            curStep = this['step' + currentStep];
            if (curStep.inactive == true) {
                this.ownerCt.updateState(currentStep, 'inactive');
                this.moveNext();
            } else {
                this.ownerCt.updateState(currentStep, null);
            }
            curStep.doLayout();
        }

    }
    , stepStates: {}
});

Ext.reg('eBook.NewFile.Wizard', eBook.NewFile.Wizard);

eBook.NewFile.Step = {
    autoNext: true
    , applyInit: function() {
        this.stepId = parseInt(this.ref.replace('step', ''));
        if (!this.disableMove) {
            Ext.apply(this, {
                bbar: [
                    { xtype: 'button'
                        , ref: 'prev'
                        , hidden: this.stepId < 2
                        , text: eBook.Xbrl.Wizard_Previous
                        , handler: this.onPrevious
                        , scope: this
                        , iconCls: 'eBook-back-24'
                        , scale: 'medium'
                        , iconAlign: 'top'
                    }
                    , '->'
                    , { xtype: 'button'
                        , text: eBook.Xbrl.Wizard_Next
                        , ref: 'nxt'
                        , handler: this.onNext
                        , scope: this
                        , iconCls: 'eBook-next-24'
                        , scale: 'medium'
                        , iconAlign: 'top'
                    }]
                
            });
        }
        Ext.apply(this, {
            cls: 'eBook-wizard-step-inactive eBook-wizard-step',
            iconCls: 'dummy'
            , labelWidth: 200
            , bodyStyle: 'padding:20px;'
            , listeners: {
                'activate': {
                    fn: this.checkLayout
                        , scope: this
                }
                }
            });
    }
    , checkLayout: function() {
        if (this.beforeCheckLayout) this.beforeCheckLayout();
        var bb = this.getBottomToolbar();
        if (bb && !this.disableMove) {
            if (this.stepId < 2) {
                bb.prev.disable();
            } else {
                bb.prev.enable();
            }
            if (this.stepId >= this.refOwner.maxStep) {
                bb.nxt.disable();
            } else {
                bb.nxt.enable();
            }
        }
        if (this.afterCheckLayout) this.afterCheckLayout();
    }
    , setIcon: function() {

    }
    , onPrevious: function() {
        this.refOwner.movePrevious();
    }
    , onNext: function() {
        if (this.validate()) {
            this.refOwner.moveNext();
        }
    }
    , validate: Ext.emptyFn
};

eBook.NewFile.StepPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.applyInit();
        eBook.NewFile.StepPanel.superclass.initComponent.apply(this, arguments);
    }
});

Ext.override(eBook.NewFile.StepPanel, eBook.NewFile.Step);

eBook.NewFile.ImportType = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            items: [{
                xtype: 'button',
                ref: 'proacc',
                text: eBook.Create.Window_ProAcc, // 'Import from ProAcc',
                iconCls: 'eBook-proacc-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                toggleHandler: function (btn, pressed) { if (pressed) { this.onSelectType('proacc'); } },
                enableToggle: true,
                toggleGroup: 'importtype',
                style: 'margin-bottom:10px;',
                width: 200,
                scope: this
            }, {
                xtype: 'button',
                ref: 'excel',
                text: eBook.Create.Window_Excel, //'Import from Excel',
                iconCls: 'eBook-excel-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                toggleHandler: function (btn, pressed) { if (pressed) { this.onSelectType('excel'); } },
                enableToggle: true,
                toggleGroup: 'importtype',
                style: 'margin-bottom:10px;',
                width: 200,
                scope: this,
                disabled: false
            }, {
                xtype: 'button',
                ref: 'exact',
                text: eBook.Create.Window_Exact, //'Import from Excel',
                iconCls: 'eBook-exact-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                toggleHandler: function (btn, pressed) { if (pressed) { this.onSelectType('exact'); } },
                enableToggle: true,
                toggleGroup: 'importtype',
                width: 200,
                scope: this,
                disabled: false
            }]
        });
        eBook.NewFile.ImportType.superclass.initComponent.apply(this, arguments);
    }
    , selectionMade: false
    , onSelectType: function (importType) {
        if (!this.proacc.pressed && !this.excel.pressed && !this.exact.pressed) {
            this.selectionMade = false;
        } else {
            var esobj = this.refOwner['step' + this.excelStep];
            var txobj = this.refOwner['step' + this.txtStep]
            var probj = this.refOwner['step' + this.proaccStep];
            var exaobj = this.refOwner['step' + this.exactAdminStep];
            var exfyobj = this.refOwner['step' + this.exactFinYearStep];
            var dbobj = this.refOwner['step' + this.defineBookyearStep];
            var noAdminsFound = false;
            if (importType == 'proacc') {
                esobj.inactive = true;
                this.ownerCt.ownerCt.updateState(this.excelStep, 'inactive');
                probj.inactive = false
                this.ownerCt.ownerCt.updateState(this.proaccStep, 0);
                this.importType = 'ProAcc';
            } else if (importType == 'excel') {
                esobj.inactive = false;
                this.ownerCt.ownerCt.updateState(this.excelStep, 0);
                probj.inactive = true
                this.ownerCt.ownerCt.updateState(this.proaccStep, 'inactive');
                this.importType = 'Excel';
            } else if (importType == 'exact') {
                //esobj.inactive = true;
                probj.inactive = true;
                dbobj.inactive = true;
                esobj.inactive = true;
                this.ownerCt.ownerCt.updateState(this.proaccStep, 'inactive');
                this.ownerCt.ownerCt.updateState(this.excelStep, 'inactive');
                this.ownerCt.ownerCt.updateState(this.defineBookyearStep, 'inactive');

                switch (this.refOwner.step3.GetCountAdmins()) {
                    case 0:
                        alert("No Administrations found. Make sure the client has a VAT number.");
                        noAdminsFound = true;
                        break;
                    case 1:
                        exaobj.inactive = true;
                        this.ownerCt.ownerCt.updateState(this.exactAdminStep, 'inactive');
                        this.refOwner.step3.selectedExactAdmin = this.refOwner.step3.GetAdmin();
                        this.refOwner.step4.loadStore();
                        break;
                    default:
                        exaobj.inactive = false;
                        this.ownerCt.ownerCt.updateState(this.exactAdminStep, 0);
                        break;
                }

                exfyobj.inactive = false;
                this.ownerCt.ownerCt.updateState(this.exactFinYearStep, 0);
                /*
                esobj.inactive = false;
                this.ownerCt.ownerCt.updateState(this.excelStep, 0);
                */
                txobj.inactive = false;
                this.ownerCt.ownerCt.updateState(this.txtStep, 0);

                this.importType = "Exact";

                //excel
                var checkboxedTabs = Ext.query('.x-tab-check');
                for (var i = 0; i < checkboxedTabs.length; i++) {
                    checkboxedTabs[i].checked = true;
                }
            }
            if (!noAdminsFound) {
                this.importType = importType;
                this.selectionMade = true;
                if (importType == 'excel') {
                    Ext.Msg.show({
                        title: '',
                        msg: 'If this file has an existing file in exact. Please create a file by pressing on the Exact button. Do you want to continue?',
                        buttons: Ext.Msg.YESNO,
                        fn: function(btnid){ if (btnid == 'yes') { if (this.autoNext) this.onNext(); } },
                        scope: this,
                        icon: Ext.MessageBox.QUESTION
                    });
                }else{
                    if (this.autoNext) this.onNext();
                }
                
            }
        }
    }
    , validate: function () {
        this.ownerCt.ownerCt.updateState(this.stepId, this.selectionMade);
        if (!this.selectionMade && !this.inactive) {
            alert("Select one importtype");
        }
        return this.selectionMade;
    }
});

Ext.reg('eBook.NewFile.ImportType', eBook.NewFile.ImportType);


eBook.NewFile.ExactSettings = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , layoutCfg: { labelWidth: 150 }
            , items: [{
                xtype: 'combo'
                        , ref: 'exactAdmins'
                        , fieldLabel: eBook.Create.Empty.PreviousFileFieldSet_EbookFile
                        , name: 'ExactAdmins'
                        , triggerAction: 'all'
                        , typeAhead: false
                        , selectOnFocus: true
                        , autoSelect: true
                        , allowBlank: true
                        , forceSelection: true
                        , valueField: 'ExactAdminCode'
                        , displayField: 'ExactAdminDisplay'
                        , editable: false
                        , nullable: true
                        , mode: 'local'
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetExactAdmins'
                            , serviceUrl: eBook.Service.file
                            , autoLoad: true
                            , autoDestroy: true
                            , criteriaParameter: 'cidc'
                           , baseParams: {
                               Id: eBook.CurrentClient

                           }
                            , fields: eBook.data.RecordTypes.ExactAdmin
                        })
                        , width: 300
                        , listWidth: 400
            }]
        });
        eBook.NewFile.ExactSettings.superclass.initComponent.apply(this, arguments);
    },
    GetCountAdmins: function () {
        return this.exactAdmins.store.getCount();
    },

    GetAdmin: function () {
        return this.exactAdmins.store.getAt(0);
    }

});

Ext.reg('eBook.NewFile.ExactSettings', eBook.NewFile.ExactSettings);


eBook.NewFile.ExactFinYears = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , layoutCfg: { labelWidth: 150 }
            , items: [{
                xtype: 'combo'
                        , ref: 'exactFinYears'
                        , fieldLabel: eBook.Create.Exact.FinancialYears
                        , name: 'ExactFinYears'
                        , triggerAction: 'all'
                        , typeAhead: false
                        , selectOnFocus: true
                        , autoSelect: true
                        , allowBlank: true
                        , forceSelection: true
                        , valueField: 'ExactFinYearId'
                        , displayField: 'ExactFinYearDisplay'
                        , editable: false
                        , nullable: true
                , mode: 'local'
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetExactFinYears'
                            , serviceUrl: eBook.Service.file
                            //  , autoLoad: true
                            // , autoDestroy: true
                            , criteriaParameter: 'csdc'
                            , fields: eBook.data.RecordTypes.ExactFinYears
                        })
                        , width: 300
                        , listWidth: 400
            }]
        });
        eBook.NewFile.ExactFinYears.superclass.initComponent.apply(this, arguments);
    },

    loadStore: function () {
        var i = 0;
        this.exactFinYears.store.load({ params: { String: this.refOwner.step3.selectedExactAdmin.data.ExactAdminCode} });



        //this.items.items[0].store.load({ String: this.ownerCt.ownerCt.selectedExactAdmin.data.ExactAdminVat });
    }

    , validate: function () {
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        if (this.exactFinYears.getRawValue() != "") {
            return true;
        } else {
            alert("Please select a financial year in order to continue");
        }

    }
    , onNext: function () {
        if (this.validate()) {

            this.selectedExactFinYear = this.exactFinYears.getRawValue();

            var res = this.selectedExactFinYear.split(" - ");
            var resStart = res[0].split("/");
            var resEnd = res[1].split("/");
            this.selectedExactFinYearStartdate = new Date(resStart[1] + "/" + resStart[0] + "/" + resStart[2]);
            this.selectedExactFinYearEnddate = new Date(resEnd[1] + "/" + resEnd[0] + "/" + resEnd[2]);

            if (Ext.isDate(this.selectedExactFinYearEnddate)) {
                yr = this.selectedExactFinYearEnddate.getFullYear();
                if (this.selectedExactFinYearEnddate.getMonth() == 11 && this.selectedExactFinYearEnddate.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
                this.selectedExactFinYearassessmentyear = yr;
            } else {
                this.selectedExactFinYearassessmentyear = "";
            }

            eBook.NewFile.ExactFinYears.superclass.onNext.call(this);
        }

    }

});

       Ext.reg('eBook.NewFile.ExactFinYears', eBook.NewFile.ExactFinYears);


eBook.NewFile.StartEnd = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , layoutCfg: { labelWidth: 150 }
            , items: [{
                xtype: 'datefield'
                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                , allowBlank: false
                , format: 'd/m/Y'
                , name: 'StartDate'
                , ref: 'startdate'
                , width: 100
            }, {
                xtype: 'datefield'
                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                , allowBlank: false
                , format: 'd/m/Y'
                , name: 'EndDate'
                , ref: 'enddate'
                , width: 100
                , listeners: {
                    'change': {
                        fn: this.onEndDateChanged
                        , scope: this
                    }
                }
            }, {
                xtype: 'displayfield'
                , name: 'Assessmentyear'
                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Assessmentyear
                , ref: 'assessmentyear'
}]
            });
            eBook.NewFile.StartEnd.superclass.initComponent.apply(this, arguments);
        }
    , onEndDateChanged: function(lfd, n, o) {
        if (Ext.isDate(n)) {
            yr = n.getFullYear();
            if (n.getMonth() == 11 && n.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
            this.assessmentyear.setValue(yr);
        } else {
            this.assessmentyear.setValue('');
        }
    }
    , validate: function() {
        var sd = this.startdate.isValid();
        var ed = this.enddate.isValid();
        if (sd && ed) {
            sd = this.startdate.getValue();
            ed = this.enddate.getValue();
            if (sd >= ed) {
                this.enddate.markInvalid(eBook.Create.Empty.FileDatesFieldSet_EnddateInvalid);
            } if(ed < new Date(2011,11,31)) {
                this.enddate.markInvalid('eBook 5 only handles assessmentyear 2012, assesmentyear 2011 and earlier are to be finalised in eBook 4.4 !');
            }else {
                this.ownerCt.ownerCt.updateState(this.stepId, true);
                return true;
            }
        }
        this.ownerCt.ownerCt.updateState(this.stepId, false);
        return false;
    }
    , onNext: function() {
        if (this.validate()) {
            Ext.apply(this.refOwner.data, {
                startDate: this.startdate.getValue()
                    , endDate: this.enddate.getValue()
                    ,assessmentyear: this.assessmentyear.getValue()
            });
            eBook.NewFile.StartEnd.superclass.onNext.call(this);
        }

    }
    });

Ext.reg('eBook.NewFile.StartEnd', eBook.NewFile.StartEnd);


eBook.NewFile.PreviousStartEnd = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , layoutCfg: { labelWidth: 150 }
            , items: [{
                xtype: 'checkbox'
                        , ref: 'first'
                        , id: 'PreviousStartEnd-first'
                        , boxLabel: 'First bookyear'
                        , checked: false
                        , labelWidth: 200
                        , listeners: {
                            'check': {
                                fn: this.firstChanged
                                , scope: this
                            }
                        }
            }
                    , {
                        xtype: 'datefield'
                        , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                        , allowBlank: false
                        , format: 'd/m/Y'
                        , name: 'StartDate'
                        , ref: 'startdate'
                        , width: 100
                    }, {
                        xtype: 'datefield'
                        , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                        , allowBlank: false
                        , format: 'd/m/Y'
                        , name: 'EndDate'
                        , ref: 'enddate'
                        , width: 100
                        , listeners: {
                            'change': {
                                fn: this.onEndDateChanged
                                , scope: this
                            }
                        }
                    }, {
                        xtype: 'displayfield'
                        , name: 'Assessmentyear'
                        , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Assessmentyear
                        , ref: 'assessmentyear'
}]
        });
        eBook.NewFile.PreviousStartEnd.superclass.initComponent.apply(this, arguments);
    }
    , firstChanged: function(chk, checked) {
        if (checked) {
            this.startdate.allowBlank = true;
            this.enddate.allowBlank = true;
            this.startdate.setValue(null);
            this.enddate.setValue(null);
            this.startdate.disable();
            this.enddate.disable();
            this.assessmentyear.setValue('-');
        } else {
            this.startdate.allowBlank = false;
            this.enddate.allowBlank = false;
            this.beforeCheckLayout();
        }
    }
    , onEndDateChanged: function(lfd, n, o) {
        if (Ext.isDate(n)) {
            yr = n.getFullYear();
            if (n.getMonth() == 11 && n.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
            this.assessmentyear.setValue(yr);
        } else {
            this.assessmentyear.setValue('');
        }
    }
    , validate: function() {
        if (this.first.checked) {
            this.ownerCt.ownerCt.updateState(this.stepId, true);
            return true;
        }
        var sd = this.startdate.isValid();
        var ed = this.enddate.isValid();
        if (sd && ed) {
            sd = this.startdate.getValue();
            ed = this.enddate.getValue();
            if (sd >= ed) {
                this.enddate.markInvalid(eBook.Create.Empty.FileDatesFieldSet_EnddateInvalid);
            }
            var ok = true;
            if (sd >= this.refOwner.data.startDate) {
                this.startdate.markInvalid("Must be before " + this.refOwner.data.startDate);
                ok = false;
            }
            if (ed >= this.refOwner.data.startDate) {
                this.enddate.markInvalid("Must be before " + this.refOwner.data.startDate);
                ok = false;
            }
            this.ownerCt.ownerCt.updateState(this.stepId, ok);
            return ok;
        }
        this.ownerCt.ownerCt.updateState(this.stepId, false);
        return false;
    }
    , onNext: function() {
        if (this.validate()) {
            Ext.apply(this.refOwner.data, {
                previousStartDate: this.startdate.getValue()
                    , previousEndDate: this.enddate.getValue()
            });
            this.refOwner.moveNext();
        }

    }
    , hasPreviousFile: function() {
        var step6 = this.refOwner.step6;
        if (this.refOwner.rendered && step6 && step6.rendered) {
            if (step6.checker.checked) {
                var val = step6.previousfile.getValue();
                var rec = step6.previousfile.store.getById(val);
                return rec;
            }
            return false;
        }
        return false;
    }
    , beforeCheckLayout: function() {
        var rec = this.hasPreviousFile();
        if (rec) {
            this.first.setValue(false);
            this.first.disable();
            this.startdate.setValue(rec.get('StartDate'));
            this.enddate.setValue(rec.get('EndDate'));
            this.startdate.disable();
            this.enddate.disable();
            this.onEndDateChanged(this.enddate, this.enddate.getValue(), null);
        } else {

            this.first.enable();
            this.startdate.enable();
            this.enddate.enable();
        }
    }
});

Ext.reg('eBook.NewFile.PreviousStartEnd', eBook.NewFile.PreviousStartEnd);

eBook.NewFile.SelectPrevious = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , items: [
                    { xtype: 'checkbox', ref: 'checker', boxLabel: 'Has previous eBook file', checked: true
                        , labelWidth: 200
                        , listeners: {
                            'check': {
                                fn: function (bx, chkd) {
                                    if (chkd) {
                                        this.previousfile.enable();
                                        this.accounts.show();
                                        this.mappings.show();
                                        this.worksheets.show();
                                        this.previousimport.show();
                                        this.accounts.setValue(true);
                                        this.mappings.setValue(true);
                                        this.worksheets.setValue(true);
                                    } else {
                                        this.previousfile.disable();
                                        this.accounts.setValue(false);
                                        this.mappings.setValue(false);
                                        this.worksheets.setValue(false);
                                        this.previousimport.setValue(false);
                                        this.accounts.hide();
                                        this.mappings.hide();
                                        this.worksheets.hide();
                                        this.previousimport.hide();
                                    }
                                }
                                , scope: this
                            }
                        }
                    }, {
                        xtype: 'combo'
                        , ref: 'previousfile'
                        , fieldLabel: eBook.Create.Empty.PreviousFileFieldSet_EbookFile
                        , name: 'PreviousFile'
                        , triggerAction: 'all'
                        , typeAhead: false
                        , selectOnFocus: true
                        , autoSelect: true
                        , allowBlank: true
                        , forceSelection: true
                        , valueField: 'Id'
                        , displayField: 'Display'
                        , editable: false
                        , nullable: true
                        , mode: 'local'
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetFileInfos'
                            , serviceUrl: eBook.Service.file
                            , autoLoad: true
                            , autoDestroy: true
                            , criteriaParameter: 'cfdc'
                           , baseParams: {
                               MarkedForDeletion: false
                                , Deleted: false
                               , ClientId: eBook.Interface.currentClient.get('Id')
                                , Closed: null
                           }
                            , fields: eBook.data.RecordTypes.FileBase
                        })
                        , width: 300
                        , listWidth: 400
                    }
                , { xtype: 'checkbox', ref: 'accounts'
                    , boxLabel: 'Import accountshema &amp; descriptions'
                    , checked: true, disabled: true
                }
                , { xtype: 'checkbox', ref: 'mappings'
                    , boxLabel: 'Import mappings'
                    , checked: true, disabled: true
                }
                , { xtype: 'checkbox', ref: 'worksheets'
                    , boxLabel: 'Import worksheets (history)'
                    , checked: true, disabled: true
                }
                , { xtype: 'checkbox', ref: 'previousimport'
                            , boxLabel: 'Import end-state previous period from eBook previous file'
                            , checked: false, labelWidth: 200
                }]
        });
        eBook.NewFile.SelectPrevious.superclass.initComponent.apply(this, arguments);
    }
    , checkLayout: function () {
        var step5 = this.refOwner.step5;
        if (this.refOwner.rendered && step5 && step5.rendered && this.previousfile && !step5.inactive) {
            var val = step5.startdate.getValue();
            if (val && this.previousfile.store.baseParams.StartDate != val) {
                this.previousfile.store.baseParams.StartDate = val;
                this.previousfile.store.load();
            }
        } else if (step5.inactive) { // EXACT so get date from fin year
            var val = this.refOwner.step4.selectedExactFinYearStartdate;
            if (val && this.previousfile.store.baseParams.StartDate != val) {
                
                this.previousfile.store.baseParams.StartDate = val;
                this.previousfile.store.load();
            }
        }
        eBook.NewFile.SelectPrevious.superclass.checkLayout.call(this);
    }
    , onEndDateChanged: function (lfd, n, o) {
        if (Ext.isDate(n)) {
            yr = n.getFullYear();
            if (n.getMonth() == 11 && n.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
            this.assessmentyear.setValue(yr);
        } else {
            this.assessmentyear.setValue('');
        }
    }
    , getSettings: function () {
        if (this.inactive) return {};
        if (!this.checker.checked) return {};
        return this.mySettings;
    }
    , validate: function () {
        var sel = this.previousfile.getValue();
        var chkd = this.checker.checked;
        if (chkd && (!sel || Ext.isEmpty(sel))) {
            this.previousfile.markInvalid();
            this.ownerCt.ownerCt.updateState(this.stepId, false);
            return false;
        }
        this.ownerCt.ownerCt.updateState(this.stepId, true);

        this.mySettings = {
            importAccounts: this.accounts.checked
            , importMappings: this.mappings.checked
            , importWorksheets: this.worksheets.checked
            , importPreviousState: this.previousimport.checked
        };
        return true;
    }
    , onNext: function () {
        if (this.validate()) {
            var chkd = this.checker.checked;
            if (chkd) {
                this.refOwner.data.previousFile = { id: this.previousfile.getValue() };
            } else {
                this.refOwner.data.previousFile = null;
            }
            eBook.NewFile.SelectPrevious.superclass.onNext.call(this);
        }

    }
});

Ext.reg('eBook.NewFile.SelectPrevious', eBook.NewFile.SelectPrevious);

eBook.NewFile.FileServices = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , items: [
                        { xtype: 'checkbox', ref: 'yearend', id: 'FileServices-yearend', boxLabel: eBook.Language.YEAREND, checked: true
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'biztaxAuto', id: 'FileServices-biztax', boxLabel: eBook.Language.BIZTAX_AUTO, checked: true
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'biztaxManual', id: 'FileServices-biztax-manual', boxLabel: eBook.Language.BIZTAX_MANUAL, checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                    ]
        });
        eBook.NewFile.FileServices.superclass.initComponent.apply(this, arguments);
    }
    , checkLayout: function() {

        eBook.NewFile.FileServices.superclass.checkLayout.call(this);
    }
    , checkChanged: function(chk, checked) {
       if(this.biztaxManual.checked) {
            this.yearend.setValue(false);
            this.biztaxAuto.setValue(false);
            this.yearend.disable();
            this.biztaxAuto.disable();
       } else {
            this.yearend.enable();
            this.biztaxAuto.enable();
       }
    
       if(!this.yearend.checked && !this.biztaxAuto.checked && this.biztaxManual.checked) {
            this.refOwner.step2.importType='none';
            this.refOwner.step2.inactive=true;
            this.ownerCt.ownerCt.updateState(2, 'inactive');
            this.refOwner.step2.inactive=true;
            this.ownerCt.ownerCt.updateState(6, 'inactive');
            this.refOwner.step2.inactive=true;
            this.ownerCt.ownerCt.updateState(7, 'inactive');
       } else {
            this.refOwner.step2.importType='none';
            this.refOwner.step2.inactive=false;
            this.ownerCt.ownerCt.updateState(2, 0);
       }
    }
    , validate: function() {
        if (!this.yearend.checked && !this.biztaxManual.checked && !this.biztaxAuto.checked) {
            alert("At least one service is required");
            this.ownerCt.ownerCt.updateState(stepId, false);
            return false;
        }
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        return true;
    }
    , onNext: function() {
        if (this.validate()) {
            
            eBook.NewFile.FileServices.superclass.onNext.call(this);
        }

    }
});

Ext.reg('eBook.NewFile.FileServices', eBook.NewFile.FileServices);


eBook.NewFile.FileSettings = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , items: [{
                             xtype:'languagebox'
                             ,ref:'language'
                             , name: 'language'
                             ,allowBlank:false
                             , fieldLabel: eBook.Create.Empty.FileInfoFieldSet_Language
                             ,width:200
                             ,listWidth:200
                        }
                        , { xtype: 'checkbox', ref: 'first', id: 'FileSettings-first', boxLabel: 'First bookyear', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallPrevBef', id: 'FileSettings-smallPrevBef', boxLabel: 'Before previous period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallPrev', id: 'FileSettings-smallPrev', boxLabel: 'Previous period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallCur', id: 'FileSettings-smallCur', boxLabel: 'Current period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'oneof', id: 'FileSettings-oneof', boxLabel: 'Current period is one of the first 3 periods', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'taxshelter', id: 'FileSettings-taxshelter', boxLabel: 'Tax shelter?', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                    ]
        });
        eBook.NewFile.FileSettings.superclass.initComponent.apply(this, arguments);
    }
    , beforeCheckLayout: function() {
        var step7 = this.refOwner.step7;
        this.first.setValue(step7.first.checked);
        this.first.disable();
        this.checkChanged(this.first, step7.first.checked);
    }
    , checkChanged: function(chk, checked) {
        if (chk.id == 'FileSettings-first') {
            if (checked) {
                this.smallPrevBef.setValue(false);
                this.smallPrev.setValue(false);
                this.oneof.setValue(true);
                this.smallPrevBef.disable();
                this.smallPrev.disable();
                this.oneof.disable();

            } else {
                this.smallPrevBef.enable();
                this.smallPrev.enable();
                this.oneof.enable();
            }
        }
//        else if (chk.id.indexOf('smallPrev') > -1) {
//            var mcheck = this.smallPrev.getValue() || this.smallPrevBef.getValue();
//            if (mcheck) {
//                this.first.setValue(false);
//                this.first.disable();
//            } else {
//                this.first.enable();
//            }
//        }
    }
    , validate: function() {
        if(!this.language.isValid()) {
            this.ownerCt.ownerCt.updateState(this.stepId, false);
            return false;
        }
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        return true;
    }
    , onNext: function() {
        if (this.validate()) {

            eBook.NewFile.FileSettings.superclass.onNext.call(this);
        }

    }
});

Ext.reg('eBook.NewFile.FileSettings', eBook.NewFile.FileSettings);



eBook.NewFile.ProAccSettings = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , items: [
                        { xtype: 'checkbox', ref: 'accounts', id: 'ProAccSettings-accounts'
                            , boxLabel: 'Import new accounts &amp; descriptions'
                            , checked: true, labelWidth: 200,disabled:true
                        }
                        ,{ xtype: 'checkbox', ref: 'current', id: 'ProAccSettings-current'
                            , boxLabel: 'Import state current period'
                            , checked: true, labelWidth: 200,disabled:true
                        }
                         ,{ xtype: 'checkbox', ref: 'previous', id: 'ProAccSettings-current'
                            , boxLabel: 'Import end-state previous period'
                            , checked: true, labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkPreviousPeriodChanged
                                    , scope: this
                                }
                            }
                        }
                        ,{ xtype: 'radiogroup', ref:'previousImportGroup',columns: 1,cls:'eBook-newfile-proaccsettings-previmporttype'
                            ,items: [
                                {boxLabel: 'From ProAcc',  id:'nf-pit-proacc', name: 'previousImportType', inputValue: 'proacc', checked: true},
                                {boxLabel: 'From previous eBook file',id:'nf-pit-ebook', name: 'previousImportType', inputValue: 'ebook'}
                            ]
                        }
                        ,{ xtype: 'checkbox', ref: 'clientsuppliers', id: 'ProAccSettings-clientsuppliers'
                            , boxLabel: 'Update clients &amp; suppliers'
                            , checked: true, labelWidth: 200
                        }
                    ]
        });
        eBook.NewFile.ProAccSettings.superclass.initComponent.apply(this, arguments);
    }
    , checkPreviousPeriodChanged: function(chk, checked) {
        var rec = this.hasPreviousFile();
        if(checked && rec) {
            this.previousImportGroup.enable();
        } else {
            this.previousImportGroup.disable();
        }
    }
     , hasPreviousFile: function() {
        var step6 = this.refOwner.step6;
        if (this.refOwner.rendered && step6 && step6.rendered) {
            if (step6.checker.checked) {
                var val = step6.previousfile.getValue();
                var rec = step6.previousfile.store.getById(val);
                return rec;
            }
            return false;
        }
        return false;
    }
    ,beforeCheckLayout:function() {
        var rec = this.hasPreviousFile();
        if(!rec) {
            this.previousImportGroup.onSetValue('nf-pit-proacc', true);
            this.previousImportGroup.disable();
            if(this.refOwner.step7.first.checked) {
                this.previous.setValue(false);
                this.previous.disable();
            } else {
                this.previous.enable();
            }
        } else {
            if(this.refOwner.step6.previousimport.checked) {
                this.previous.setValue(true);
                this.checkPreviousPeriodChanged();
                this.previousImportGroup.onSetValue('nf-pit-ebook', true);
                this.previous.disable();
            } else {
                this.previousImportGroup.onSetValue('nf-pit-proacc', true);
                this.previousImportGroup.enable();
                this.previous.enable();
            }
        }
    }
    , getSettings:function() {
        if(this.inactive) return {};
        return this.mySettings;
    }
    , validate: function() {
        if(!this.previous.disabled && this.previous.checked && this.previousImportGroup.getValue().inputValue=='ebook') {
            this.refOwner.step6.previousimport.setValue(true);
            this.refOwner.step6.validate();
        } else if(this.previous.checked && this.previousImportGroup.getValue().inputValue=='proacc') {
            this.refOwner.step6.previousimport.setValue(false);
            this.refOwner.step6.validate();
        }
        this.mySettings = { 
            database: eBook.Interface.currentClient.get('ProAccDatabase')
            ,server: eBook.Interface.currentClient.get('ProAccServer')
            ,importAccounts:this.accounts.checked
            ,importCurrentState:this.current.checked
            ,importPreviousState:this.previous.checked && this.previousImportGroup.getValue().inputValue=='proacc'
            , updateBusinessRelations:this.clientsuppliers.checked
        };
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        return true;
    }
    , onNext: function() {
        if (this.validate()) {
            
            eBook.NewFile.ProAccSettings.superclass.onNext.call(this);
        }

    }
});

Ext.reg('eBook.NewFile.ProAccSettings', eBook.NewFile.ProAccSettings);

eBook.NewFile.UploadExcel = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            layout: 'border'
             , items: [{ xtype: 'form'
                        , fileUpload: true
                        , bodyStyle: 'padding:10px;'
                        , items: [{
                            xtype: 'fileuploadfield'
                                    , width: 400
                                    , fieldLabel: eBook.Excel.UploadWindow_SelectFile
                                    , fileTypeRegEx: /(\.xls(x{0,1}))$/i
                                    , ref: 'file'
                        }, {
                            xtype: 'hidden'
                                        , name: 'fileType'
                                        , value: 'excel'
                        }, {
                            xtype: 'button',
                            text: eBook.Excel.UploadWindow_Upload,
                            ref: '../saveButton',
                            iconCls: 'eBook-icon-uploadexcel-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onUploadClick,
                            scope: this,
                            style: 'margin-left:300px;'
                        }]
                        , ref: 'uploadForm'
                        , region: 'north'
             }
                    , { xtype: 'eBook.Excel.Configurator', region: 'center', ref: 'configurator', disabled: true }
            ]
        });
        eBook.NewFile.UploadExcel.superclass.initComponent.apply(this, arguments);
    }
    , beforeCheckLayout: function () {
        var fin = this.uploadForm.el.child('input.x-form-file-text');
        fin.setStyle('width', '250px');
        this.configurator.items.items[0].mappingPanel.sheet.setWidth(100);
        this.configurator.items.items[0].mappingPanel.culture.setWidth(100);
        this.configurator.items.items[0].enable();
        this.configurator.items.items[1].enable();
        if (this.refOwner.step2.importType == "exact") {
            this.configurator.items.items[2].enable();
        }
        //this.configurator.items.items[0].mappingPanel.items.items[0].setValue('test');  
    }
    , onUploadClick: function (e) {
        this.configurator.disable();
        var f = this.uploadForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Excel.UploadWindow_Invalid);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Excel.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function (fp, o) {
        //process o.result.file
        this.uploaded = true;
        this.newFileName = o.result.file;
        this.originalFileName = o.result.originalFile;
        this.getEl().mask(String.format(eBook.Excel.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.excel + 'ExcelGetBasicInfo'
                , method: 'POST'
                , params: Ext.encode({ cedc: { FileName: o.result.file} })
                , callback: this.handleFileProcessing
                , scope: this
        });
    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, this.title);
    }
    , handleFileProcessing: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var dc = Ext.decode(resp.responseText).ExcelGetBasicInfoResult;
            dc.originalFileName = this.originalFileName;
            this.configurator.setTitle(this.originalFileName);
            this.configurator.loadFile(dc.fn, dc.sns);
            if (this.refOwner.step2.importType == "exact") {
                // Tab rekeningschema
                var mappingRekSchema = this.configurator.items.items[0].mappingPanel;
                mappingRekSchema.sheet.setValue(mappingRekSchema.sheet.store.getAt(0).data.field1);
                mappingRekSchema.onSheetSelect();
                mappingRekSchema.accountnr.setValue('A');
                mappingRekSchema.accountdescription.setValue('B');

                // Tab balans
                var mappingBalans = this.configurator.items.items[1].mappingPanel;
                mappingBalans.sheet.setValue(mappingBalans.sheet.store.getAt(1).data.field1);
                mappingBalans.onSheetSelect();
                mappingBalans.accountnr.setValue('A');
                mappingBalans.accountdescription.setValue('B');
                mappingBalans.saldo.setValue('C');

                // Tab balans vorig
                var mappingBalansVorig = this.configurator.items.items[2].mappingPanel;
                mappingBalansVorig.sheet.setValue(mappingBalansVorig.sheet.store.getAt(2).data.field1);
                mappingBalansVorig.onSheetSelect();
                mappingBalansVorig.accountnr.setValue('A');
                mappingBalansVorig.accountdescription.setValue('B');
                mappingBalansVorig.saldo.setValue('C');

            }
            this.configurator.enable();

            //this.configurator.items.items[0].mappingPanel.items.items[0].expand();
            //this.configurator.items.items[0].mappingPanel.items.items[0].select(0);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , setExcelInfo: function (dc) {
        this.dataContract = dc;
        this.filename.setText(dc.originalFileName);
        this.filesheets.setText(dc.sns.join(","));
    }
    , getSettings: function () {
        if (this.inactive) return {};
        var dcs = this.configurator.getDataContracts();
        return { importAccounts: dcs.accountsCfg && dcs.accountsCfg.active
                , importCurrentState: dcs.currentCfg && dcs.currentCfg.active
                , importPreviousState: dcs.previousCfg && dcs.previousCfg.active
                , originalFileName: this.originalFileName
                , newFileName: this.newFileName
            , datacontracts: dcs
        };

    }
    , validate: function () {
       // return true; // TO DELETE!
        if (!this.uploaded) {
            this.ownerCt.ownerCt.updateState(this.stepId, false);
            return false;
        }
        var ok = !this.configurator.items.items[0].disabled && !this.configurator.items.items[1].disabled;
        ok = ok && this.configurator.getDataContracts().length > 1;
        ok = ok && this.configurator.items.items[0].checkValid();
        ok = ok && this.configurator.items.items[1].checkValid();
        ok = ok && this.configurator.items.items[2].checkValid();
        this.ownerCt.ownerCt.updateState(this.stepId, ok);
        return ok;
    }
});

Ext.reg('eBook.NewFile.UploadExcel', eBook.NewFile.UploadExcel);


eBook.NewFile.UploadTxt = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        this.importFile = false;
        Ext.apply(this, {
            layout: 'border'
             , items: [

                        { xtype: 'form'
                        , fileUpload: true
                        , bodyStyle: 'padding:10px;'
                        , items: [
                            { xtype: 'checkbox',
                                boxLabel: 'Import data',
                                checked: false,
                                listeners: {
                                    'check': {
                                        fn: this.firstChanged
                                        , scope: this
                                    }
                                }
                            },
                            {
                                xtype: 'fileuploadfield'
                                        , width: 400
                                        , fieldLabel: eBook.Exact.UploadWindow_SelectFile
                                        , fileTypeRegEx: /(\.txt())$/i
                                        , ref: '../file'
                                        , disabled: true
                            }, {
                                xtype: 'hidden'
                                            , name: 'fileType'
                                            , value: 'txt'
                            }, {
                                xtype: 'button',
                                text: eBook.Excel.UploadWindow_Upload,
                                ref: '../saveButton',
                                iconCls: 'eBook-uploadbiz-ico-24 ',
                                scale: 'medium',
                                iconAlign: 'top',
                                handler: this.onUploadClick,
                                scope: this,
                                style: 'margin-left:300px;',
                                disabled: true
                            }]
                        , ref: 'uploadForm'
                        , region: 'center'
                        }
            //, { xtype: 'eBook.Excel.Configurator', region: 'center', ref: 'configurator', disabled: true }
            ]
        });
        eBook.NewFile.UploadTxt.superclass.initComponent.apply(this, arguments);
    }
    , beforeCheckLayout: function () {
        var fin = this.uploadForm.el.child('input.x-form-file-text');
        fin.setStyle('width', '250px');
    }
    , firstChanged: function (chk, checked) {
        if (checked) {
            this.file.enable();
            this.saveButton.enable();
            this.importFile = true;
        } else {
            this.file.disable();
            this.saveButton.disable();
            this.importFile = false;
        }
    }
    , onUploadClick: function (e) {
        var f = this.uploadForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Excel.UploadWindow_Invalid);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Excel.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function (fp, o) {
        this.uploaded = true;
        this.newFileName = o.result.file;
        this.originalFileName = o.result.originalFile;
        Ext.Msg.show({
            title: '',
            msg: 'Your file was successfully uploaded. Press OK to continue.',
            buttons: Ext.Msg.OK,
            fn: function (btnid) { if (btnid == 'ok') { this.ownerCt.moveNext() } },
            scope: this,
            icon: Ext.MessageBox.QUESTION
        });
    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, this.title);
    }
    , setExcelInfo: function (dc) {
        this.dataContract = dc;
        this.filename.setText(dc.originalFileName);
        this.filesheets.setText(dc.sns.join(","));
    }
    , getSettings: function () {
        if (this.inactive) return {};
        return { originalFileName: this.originalFileName
                , newFileName: this.newFileName
                , importFile: this.importFile
        };

    }
    , validate: function () {
        // return true; // TO DELETE!
        if (this.importFile) {
            if (this.uploaded) {
                this.ownerCt.ownerCt.updateState(this.stepId, true);
                return true;
            } else {
                this.ownerCt.ownerCt.updateState(this.stepId, false);
                return false;
            }
        } else {
            this.ownerCt.ownerCt.updateState(this.stepId, true);
            return true;
        }

        


    }
});

Ext.reg('eBook.NewFile.UploadTxt', eBook.NewFile.UploadTxt);


eBook.NewFile.CreationOverview = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        this.applyInit();
        this.tpl = new Ext.XTemplate('<div class="eBook-newfile-overview">'
                        , '<h2>Create File {assessmentYear}: {startDate:date("d/m/Y")} &gt; {endDate:date("d/m/Y")}</h2>'
                        , '<div class="eBook-newfile-overview-info">'
                                , 'Client default language: {language}'
                                , '<tpl if="!settings.first"><br/>Previous  period: <i>\[{previousAssessmentYear}\] {previousStartDate:date("d/m/Y")} - {previousEndDate:date("d/m/Y")}</i></tpl>'
                                , '<tpl if="settings.first"><br/><b>First bookyear</b>, no previous period.</tpl>'
                        , '</div>'
                        , '<div class="eBook-newfile-overview-tab">'
                            , '<div class="eBook-newfile-overview-tab-title">Import details</div>'
                            , '<div class="eBook-newfile-overview-tab-body">'
                                , '<tpl if="previouseBook">'
                                    , '<u>Previous eBook File</u>'
                                    , '<ul>'
                                        , '<tpl if="importConfig.ebook.importAccounts"><li>Import accountschema</li></tpl>'
                                        , '<tpl if="importConfig.ebook.importMappings"><li>Import mappings</li></tpl>'
                                        , '<tpl if="importConfig.ebook.importWorksheets"><li>Import worksheets (history)</li></tpl>'
                                        , '<tpl if="importConfig.ebook.importPreviousState"><li>Import state previous period</li></tpl>'
                                    , '</ul>'
                                    , '<br/>'
                                , '</tpl>'
                                , '<tpl if="importConfig.type==\'none\'">'
                                    , '<u><b>NO IMPORT</b></u>'
                                    , 'No data needs to be imported when performing manual BizTax declaration only'
                                 , '</tpl>'
                                , '<tpl if="importConfig.type==\'proacc\'">'
                                    , '<tpl for="importConfig.proacc"><u>ProAcc database - {database}</u></tpl>'
                                    , '<ul>'
                                        , '<tpl if="importConfig.proacc.importAccounts"><li>Import new accounts & descriptions</li></tpl>'
                                        , '<tpl if="importConfig.proacc.importPreviousState"><li>Import state previous period</li></tpl>'
                                        , '<tpl if="importConfig.proacc.importCurrentState"><li>Import state current period</li></tpl>'
                                        , '<tpl if="importConfig.proacc.updateBusinessRelations"><li>Update clients &amp; suppliers</li></tpl>'
                                    , '</ul>'
                               , '</tpl>'
                               , '<tpl if="importConfig.type==\'excel\'">'
                                    , '<tpl for="importConfig.excel"><u><u>Excel file {originalFileName}</u>'
                                    , '<ul>'
                                        , '<tpl if="importAccounts"><li>Import accounts & descriptions <br/>(<i><tpl for="datacontracts.accountsCfg">{txt}</tpl></i>) </li></tpl>'
                                        , '<tpl if="importPreviousState"><li>Import state previous period <br/>(<i><tpl for="datacontracts.previousCfg">{txt}</tpl></i>)</li></tpl>'
                                        , '<tpl if="importCurrentState"><li>Import state current period <br/>(<i><tpl for="datacontracts.currentCfg">{txt}</tpl></i>)</li></tpl>'
                                    , '</ul>'
                                    , '</tpl>'
                               , '</tpl>'
                               , '<tpl if="importConfig.type==\'exact\'">'
                                    , '<tpl if="importConfig.exact.importFile">'
                                        , '<tpl for="importConfig.exact"><u>Text file: {originalFileName}</u>'
                                        , '</tpl>'
                                    , '</tpl>'
                                    , '<tpl if="importConfig.exact.importFile==\'false\'">'
                                        , '<tpl for="importConfig.exact"><u>None</u>'
                                        , '</tpl>'
                                    , '</tpl>'
                               , '</tpl>'
                            , '</div>'
                        , '</div>'
                        , '<div class="eBook-newfile-overview-tab">'
                            , '<div class="eBook-newfile-overview-tab-title">Setting/Meta details</div>'
                            , '<div class="eBook-newfile-overview-tab-body">'
                                , '<u>Services</u>'
                                , '<ul>'
                                    , '<tpl if="services.yearend"><li>Yearend closing</li></tpl>'
                                    , '<tpl if="services.biztaxAuto"><li>Automated BizTax declaraion (client ledger available)</li></tpl>'
                                    , '<tpl if="services.biztaxManual"><li>MANUAL BizTax declaraion (client ledger NOT available)</li></tpl>'
                                , '</ul>'
                                , '<br/>'
                                , '<u>Meta</u>'
                                , '<ul>'
                                    , '<tpl if="settings.first"><li><b>First bookyear</b></li></tpl>'
                                    , '<tpl if="settings.oneof"><li>Current period is one of the first 3 periods</li></tpl>'
                                    , '<tpl if="settings.smallPrevBef"><li>Before previous period small company</li></tpl>'
                                    , '<tpl if="settings.smallPrev"><li>Previous period small company</li></tpl>'
                                    , '<tpl if="settings.smallCur"><li>Current period small company</li></tpl>'
                                    , '<tpl if="settings.taxshelter"><li>Tax Shelter</li></tpl>'
                                , '</ul>'
                            , '</div>'
                        , '</div>'
                    , '</div>');
        Ext.apply(this, {
            items: [{ xtype: 'panel', html: 'updating...', ref: 'overview'}]
            , disableMove: true
            , bbar: [
                    { xtype: 'button'
                        , ref: 'prev'
                        , hidden: this.stepId < 2
                        , text: 'Change settings'
                        , handler: this.onPrevious
                        , scope: this
                        , iconCls: 'eBook-back-24'
                        , scale: 'medium'
                        , iconAlign: 'top'
                    }
                    , '->'
                    , { xtype: 'button'
                        , text: eBook.Create.Empty.MainPanel_Create
                        , ref: 'nxt'
                        , handler: this.onNext
                        , scope: this
                        , iconCls: 'eBook-createFile-createButton'
                        , scale: 'medium'
                        , iconAlign: 'top'
                    }]
        });
        eBook.NewFile.CreationOverview.superclass.initComponent.apply(this, arguments);
    }
    , beforeCheckLayout: function () {
//        
        this.dta = {
            startDate: this.refOwner.step2.importType == "exact" ? this.refOwner.step4.selectedExactFinYearStartdate : this.refOwner.step5.startdate.getValue()
            , endDate: this.refOwner.step2.importType == "exact" ? this.refOwner.step4.selectedExactFinYearEnddate : this.refOwner.step5.enddate.getValue()
            , assessmentYear: this.refOwner.step2.importType == "exact" ? this.refOwner.step4.selectedExactFinYearassessmentyear : this.refOwner.step5.assessmentyear.getValue()
            , previouseBook: this.refOwner.step6.checker.checked
            , previousFileId: this.refOwner.step6.previousfile.getValue()
            , previousStartDate: this.refOwner.step7.startdate.getValue()
            , previousEndDate: this.refOwner.step7.enddate.getValue()
            , previousAssessmentYear: this.refOwner.step7.assessmentyear.getValue()
            , culture: this.refOwner.step11.language.getValue()
            , language: this.refOwner.step11.language.getSelectedDisplay()
            , importConfig: {
                type: this.refOwner.step2.importType
                , proacc: this.refOwner.step8.getSettings()
                , excel: this.refOwner.step9.getSettings()
                , ebook: this.refOwner.step6.getSettings()
                , exact: this.refOwner.step10.getSettings()
            }
            , services: {
                yearend: this.refOwner.step1.yearend.checked
                , biztaxAuto: this.refOwner.step1.biztaxAuto.checked
                , biztaxManual: this.refOwner.step1.biztaxManual.checked
            }
            , settings: {
                first: this.refOwner.step7.first.checked
                , smallPrevBef: this.refOwner.step11.smallPrevBef.checked
                , smallPrev: this.refOwner.step11.smallPrev.checked
                , smallCur: this.refOwner.step11.smallCur.checked
                , oneof: this.refOwner.step11.oneof.checked
                , taxshelter: this.refOwner.step11.taxshelter.checked
            }
        };
        this.ownerCt.creationData = this.dta;
        this.overview.el.update(this.tpl.apply(this.dta));
    }
    , validate: function () {
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        return true;
    }
});

//Ext.override(eBook.NewFile.CreationOverview, eBook.NewFile.Step);

Ext.reg('eBook.NewFile.CreationOverview', eBook.NewFile.CreationOverview);



eBook.NewFile.CreateFile = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        this.disableMove = true;
        this.applyInit();
        this.tpl = new Ext.XTemplate('<div class="eBook-newfile-overview">'
                        , '<h2>Create File {assessmentYear}: {startDate:date("d/m/Y")} &gt; {endDate:date("d/m/Y")}</h2>'
                        , '<tpl if="!settings.first"><div class="eBook-newfile-overview-info">Previous  period: <i>\[{previousAssessmentYear}\] {previousStartDate:date("d/m/Y")} - {previousEndDate:date("d/m/Y")}</i></div></tpl>'
                        , '<tpl if="settings.first"><div class="eBook-newfile-overview-info"><b>First bookyear</b>, no previous period.</div></tpl>'
                        , '<div class="eBook-newfile-actionsCT"><div class="eBook-newfile-actions">'
                            , '<ul>'
                            , '<tpl for="actions">'
                                , '<li class="{stateCls}" id="eBook-newfile-action-{id}">{description}<div class="eBook-newfile-action-errMsg" style="display:none"></div></li>'
                            , '</tpl>'
                            , '</ul>'
                        , '</div></div>'
                    , '</div>');
        Ext.apply(this, {
            items: [{ xtype: 'panel', html: 'updating...', ref: 'overview'}]
            , disableMove: true
        });
        eBook.NewFile.CreateFile.superclass.initComponent.apply(this, arguments);
    }
     , beforeCheckLayout: function () {
         this.settingsLog = this.refOwner.creationData;

         this.dta = {
             assessmentYear: this.settingsLog.assessmentYear
            , startDate: this.settingsLog.startDate
            , endDate: this.settingsLog.endDate
            , settings: this.settingsLog.settings
            , previousAssessmentYear: this.settingsLog.previousAssessmentYear
            , previousStartDate: this.settingsLog.previousStartDate
            , previousEndDate: this.settingsLog.previousEndDate
            , actions: []
         };

         var services = [];
         if (this.settingsLog.services.yearend) services.push('c3b8c8db-3822-4263-9098-541fae897d02');
         if (this.settingsLog.services.biztaxAuto) services.push('bd7c2eae-8500-4103-8984-0131e73d07fa');
         if (this.settingsLog.services.biztaxManual) services.push('60ca9df9-c549-4a61-a2ad-3fdb1705cf55');

         actions = [];

         idx = 0;
         actions.push({
             id: idx
            , stateCls: 'eBook-newfile-ready'
            , state: 'ready'
            , description: 'Create new file'
            , url: eBook.Service.file
            , action: 'CreateFile'
            , params: { ccfdc: {
                cid: eBook.CurrentClient
                            , as: services
                            , c: this.settingsLog.culture
                            , s: this.settingsLog.startDate
                            , e: this.settingsLog.endDate
                            , ps: !this.settingsLog.settings.first ? this.settingsLog.previousStartDate : null
                            , pe: !this.settingsLog.settings.first ? this.settingsLog.previousEndDate : null
                            , pfid: this.settingsLog.previouseBook ? this.settingsLog.previousFileId : null
                            , fs: {
                                fid: eBook.EmptyGuid
                                , fby: this.settingsLog.settings.first
                                , oft: this.settingsLog.settings.oneof
                                , bps: this.settingsLog.settings.smallPrevBef
                                , ps: this.settingsLog.settings.smallPrev
                                , cs: this.settingsLog.settings.smallCur
                                , ts: this.settingsLog.settings.taxshelter
                            }
                            , Person: eBook.User.getActivePersonDataContract()
                            , it: this.refOwner.step2.importType
            }
            }
            , callback: this.onFileCreated
            , scope: this
         });
         idx++;

         if (this.settingsLog.previouseBook) {
             actions.push({
                 id: idx
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , description: 'Import accounts previous eBook ' + (this.settingsLog.importConfig.ebook.importPreviousState ? ' (including previous state)' : '')
                , url: eBook.Service.file
                , action: 'ImportAccountseBook'
                , params: { iaedc: { fid: '', ips: this.settingsLog.importConfig.ebook.importPreviousState} }
                , paramKey: 'iaedc'
                , callback: this.onActionReady
                , scope: this
             });
             idx++;
             actions.push({
                 id: idx
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , description: 'Import mappings previous eBook'
                , url: eBook.Service.file
                , action: 'ImportMappingseBook'
                , params: { cfdc: { FileId: '', Culture: eBook.Interface.Culture} }
                , paramKey: 'cfdc'
                , parmFileKey: 'FileId'
                , callback: this.onActionReady
                , scope: this
             });
             idx++;
         }

         if (this.settingsLog.importConfig.type == 'proacc') {
             actions.push({
                 id: idx
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , description: 'Import (new) accounts from proAcc inlcuding current state' + (this.settingsLog.importConfig.proacc.importPreviousState ? ' and previous state' : '')
                , url: eBook.Service.file
                , action: 'ImportAccountsProAcc'
                , params: { iaedc: { fid: '', ips: this.settingsLog.importConfig.proacc.importPreviousState} }
                , paramKey: 'iaedc'
                , callback: this.onActionReady
                , scope: this
             });
             idx++;


             if (this.settingsLog.importConfig.proacc.updateBusinessRelations) {
                 actions.push({
                     id: idx
                     , stateCls: 'eBook-newfile-ready'
                     , state: 'ready'
                     , description: 'Update clients &amp; suppliers from proAcc'
                     , url: eBook.Service.businessrelation
                     , action: 'ImportBusinessRelationsProAcc'
                     , params: { cidc: { Id: eBook.Interface.currentClient.get('Id'), Culture: '', Person: eBook.User.getActivePersonDataContract()} }
                     , paramKey: 'cidc'
                     , callback: this.onActionReady
                     , scope: this
                 });
                 idx++;
             }


         }

         if (this.settingsLog.importConfig.type == 'excel') {
             /* TO DELETE start */
             var dcs = this.settingsLog.importConfig.excel.datacontracts;
             var par = dcs.accountsCfg;
             par.FileId = '';
             actions.push({
                 id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import (new) accounts from Excel file'
                 , url: eBook.Service.file
                 , action: 'ImportAccountsExcel'
                 , params: { cesmdc: par }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
             });
             idx++;

             if (this.settingsLog.importConfig.excel.importCurrentState) {
                 par = dcs.currentCfg;
                 par.FileId = '';
                 actions.push({
                     id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import current state from Excel file'
                 , url: eBook.Service.file
                 , action: 'ImportCurrentStateExcel'
                     //, action: 'ImportCurrentStateTxt'
                 , params: { cesmdc: par }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
                 });
                 idx++;

             }

             if (this.settingsLog.importConfig.excel.importPreviousState && !this.settingsLog.settings.first) {
                 par = dcs.previousCfg;
                 par.FileId = '';
                 actions.push({
                     id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import previous state from Excel file'
                 , url: eBook.Service.file
                 , action: 'ImportPreviousStateExcel'
                 , params: { cesmdc: par }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
                 });
                 idx++;

             }
             /* end */
         }

         if (this.settingsLog.importConfig.type == 'exact' && this.refOwner.step10.importFile && this.refOwner.step10.uploaded) {
             var dcs = this.settingsLog.importConfig.exact;

             actions.push({
                 id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import (new) accounts from Excel file'
                 , url: eBook.Service.file
                 , action: 'ImportAccountsTxt'
                 , params: { cesmdc: { FileName: dcs.newFileName, Culture: eBook.Interface.Culture} }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
             });
             idx++;

             actions.push({
                 id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import current state from Text file'
                 , url: eBook.Service.file
                 //, action: 'ImportCurrentStateExcel'
                 , action: 'ImportCurrentStateTxt'
                 , params: { cesmdc: { FileName: dcs.newFileName} }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
             });
             idx++;
         }

         actions.push({
             id: idx
            , stateCls: 'eBook-newfile-ready'
            , state: 'ready'
            , description: 'Preparing new file for fast usage...'
            , url: eBook.Service.file
            , action: 'SetFileInCache'
            , params: { cfdc: { FileId: '', Culture: eBook.Interface.Culture} }
            , paramKey: 'cfdc'
            , parmFileKey: 'FileId'
            , callback: this.onActionReady
            , scope: this
         });
         idx++;

         if (this.settingsLog.previouseBook) {

             actions.push({
                 id: idx
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , description: 'Import previous worksheets'
                , url: eBook.Service.rule(this.settingsLog.assessmentYear)
                , action: 'ImportPrevious'
                , params: { cfdc: { FileId: '', Culture: eBook.Interface.Culture} }
                , paramKey: 'cfdc'
                , parmFileKey: 'FileId'
                , callback: this.onActionReady
                , scope: this
             });
             idx++;

             /*
             actions.push({
             id: idx
             , stateCls: 'eBook-newfile-ready'
             , state: 'ready'
             , description: 'Update worksheets containing ProAcc history'
             , url: eBook.Service.file
             , action: 'ImportWorksheetsProAccHistory'
             , params: { ccfdc: {} }
             , paramKey: 'ccfdc'
             , callback: this.onActionReady
             , scope: this
             });
             idx++;
             */
         }



         actions.push({
             id: idx
            , stateCls: 'eBook-newfile-ready'
            , state: 'ready'
            , description: 'Open newly created file'
            , fn: this.openFile
            , callback: this.onFileOpened
            , scope: this
         });

         actions.push({
             state: 'ready'
                , response: null
                , text: 'Update cache'
                , url: eBook.Service.file
                , action: 'ForceFileCacheReload'
                , params: { cfdc: { FileId: this.fileId, Culture: eBook.Interface.Culture} }
         });
         this.dta.actions = actions;

         this.overview.el.update(this.tpl.apply(this.dta));
         this.startActions.defer(200, this);
     }
     , getStateClass: function (state) {
         // eBook-newfile-busy
         // eBook-newfile-ready
         // eBook-newfile-done
         // eBook-newfile-error
         return 'eBook-newfile-' + state;
     }
     , updateActionStatus: function (idx, prevstate, state) {
         var cls = this.getStateClass(state);
         var prevCls = this.getStateClass(prevstate);
         var elId = 'eBook-newfile-action-' + idx;
         var el = Ext.get(elId);
         if (el) {
             el.replaceClass(prevCls, cls);
         }

     }
     , startActions: function () {
         this.ownerCt.ownerCt.locked = true;
         this.actionIdx = 0;
         this.executeAction(this.actionIdx);
     }
     , executeAction: function (actIdx) {
         if (actIdx >= this.dta.actions.length) {
             this.ownerCt.ownerCt.locked = false;
             this.actionIdx = 0;
             return;
         }

         this.updateActionStatus(actIdx, 'ready', 'busy');

         var act = this.dta.actions[actIdx];
         if (!act.fn) {
             if (act.paramKey && this.fileId) {
                 if (act.parmFileKey) {
                     act.params[act.paramKey][act.parmFileKey] = this.fileId;
                 } else {
                     act.params[act.paramKey].fid = this.fileId;
                 }
             }
             Ext.Ajax.request({
                 url: act.url + act.action
                , method: 'POST'
                , params: Ext.encode(act.params)
                , callback: act.callback
                , scope: act.scope
                , actionIdx: actIdx
                , action: act.action
             });
         } else {
             act.fn.call(act.scope);
             //this.onActionReady({ actionIdx: actIdx }, true, null);
         }
     }
     , handleError: function (opts, resp) {
         this.updateActionStatus(opts.actionIdx, 'busy', 'error');
         var dtl = '';
         try {
             var respObj = Ext.decode(response.responseText);
             dtl = respObj.Message;
         } catch (e) {
             dtl = 'Status: ' + resp.status + ' - ' + resp.statusText;
         }
         var elId = 'eBook-newfile-action-' + opts.actionIdx;
         var el = Ext.get(elId);
         var errEl = el.child('.eBook-newfile-action-errMsg');
         errEl.setStyle('display', 'block');
         errEl.update(dtl);
         this.ownerCt.ownerCt.locked = false;
     }
     , onFileCreated: function (opts, success, resp) {
         if (!success) {
             this.handleError(opts, resp);
         } else {
             try {
                 var respObj = Ext.decode(resp.responseText);
                 this.fileId = respObj.CreateFileResult;
                 this.updateActionStatus(opts.actionIdx, 'busy', 'done');
                 this.executeAction(opts.actionIdx + 1);
             } catch (e) {
                 alert(e);
             }
         }
     }
     , onActionReady: function (opts, success, resp) {
         if (success) {
             this.updateActionStatus(opts.actionIdx, 'busy', 'done');
             this.executeAction(opts.actionIdx + 1);
         } else {
             this.handleError(opts, resp);
         }
     }
     , openFile: function () {
         eBook.Interface.openFile(this.fileId, true);
         this.ownerCt.ownerCt.locked = false;
         // open mappings?
         this.ownerCt.ownerCt.close();
     }
     , onFileOpened: function () {
         this.ownerCt.ownerCt.locked = false;
         // open mappings?
         this.ownerCt.ownerCt.close();
     }
});

Ext.reg('eBook.NewFile.CreateFile', eBook.NewFile.CreateFile);


eBook.NewFile.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            items: [
                { xtype: 'eBook.NewFile.State', ref: 'state', region: 'west', width: 250 }
                , { xtype: 'eBook.NewFile.Wizard', ref: 'wizard', region: 'center'}]
            , modal: true
            , layout: 'border'
            , listeners: {
                'render': {
                    fn: function() { this.state.loadSteps(this.wizard); }
                        , scope: this
                }
            }

        });
        eBook.NewFile.Window.superclass.initComponent.apply(this, arguments);
    }
    , updateState: function(stepId, state) {
        var istate = -1;
        if (state === true) { istate = 2; }
        if (state === null) { istate = 1; }
        if (state === false) { istate = -99; }
        if (state === 0) { istate = 0; }
        this.state.updateState(stepId - 1, istate);
    }
});


eBook.NewFile.State = Ext.extend(Ext.Panel, {
    dta: []
    , initComponent: function() {
        this.tpl = new Ext.XTemplate(
            '<div class="eBook-wizard-steps-body">'
            , '<tpl for=".">'

                , '<div class="eBook-wizard-step eBook-wizard-step-state-{state}"><div class="eBook-wizard-step-icon">{title}</div></div>'

            , '</tpl>'
            , '</div>'
        );
        this.tpl.compile();
        Ext.apply(this, {
            html: this.tpl.apply(this.dta)
            ,bodyStyle:'background-color:transparent;'
        });
        eBook.NewFile.State.superclass.initComponent.apply(this, arguments);
    }
    , loadSteps: function(wizard) {
        this.dta = [];

        wizard.items.each(function(it) {
            this.dta.push({ id: it.initialConfig.ref.replace('step', ''), title: it.title, cref: it.initialConfig.ref, state: 0 });
        }, this);
        this.dta[0].state = 1;
        if (this.rendered) { this.body.update(this.tpl.apply(this.dta)); }
        else { this.html = this.tpl.apply(this.dta); }
    }
    , updateState: function(stepId, state) {
        this.dta[stepId].state = state;
        if (this.rendered) { this.body.update(this.tpl.apply(this.dta)); }
        else { this.html = this.tpl.apply(this.dta); }
    }
});

Ext.reg('eBook.NewFile.State', eBook.NewFile.State);
eBook.Accounts.ContextMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'eBook-account-context-menu'
            , shadow: false
            ,items:[{
                    text: 'Edit account',
                    iconCls: 'eBook-icon-edit-16',
                    handler: this.scope.onEditAccountClick,
                    ref:'editAccount',
                    scope: this.scope
                }, '-', {
                    text: 'Add Tax Adjustment',
                    iconCls: 'eBook-icon-add-16',
                    handler: this.scope.onAddAjdustmentClick,
                    ref: 'addAdjustment',
                    scope: this.scope
                }, {
                    text: 'Edit Tax Adjustments',
                    iconCls: 'eBook-icon-edit-16',
                    handler: this.scope.onEditAjdustmentsClick,
                    ref: 'editAdjustments',
                    scope: this.scope
                }]
                
        });
        eBook.Accounts.ContextMenu.superclass.initComponent.apply(this, arguments);
    }
    , showMe: function(el, accountNr, hta) {
        this.activeEl = el;
        this.accountNr = accountNr;
        eBook.Accounts.ContextMenu.superclass.show.apply(this, [el,'c']);
        if (!hta) {
            this.editAdjustments.disable();
        } else {
            this.editAdjustments.enable();
        }
    }
    , showMeAt: function(el, accountNr, hta, xy) {
        this.activeEl = el;
        this.accountNr = accountNr;
        eBook.Accounts.ContextMenu.superclass.showAt.apply(this, [xy]);
        if (!hta) {
            this.editAdjustments.disable();
        } else {
            this.editAdjustments.enable();
        }
    }
});

Ext.reg('accountcontextmenu', eBook.Accounts.ContextMenu);
/*
Get all MappingGroups and generate a recordtype as well as a columnmodel
Launch at load

*/
eBook.Accounts.Util = {
    getMappingsGroups:''
    ,createMappingRecordType: function() {
    
    }
    , createMappingColumnModel: function() {
    }
};String.prototype.rpad = function(padString, length) {
	var str = this;
    while (str.length < length)
        str = str + padString;
    return str;
}
String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

eBook.Accounts.TranslationPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        var fcult = eBook.Interface.currentFile.get('Culture');
        this.defaultCult = fcult;
        var cults = [fcult];
        if (fcult != 'nl-BE') cults.push('nl-BE');
        if (fcult != 'fr-FR') cults.push('fr-FR');
        if (fcult != 'en-US') cults.push('en-US');
        if (fcult != 'de-DE') cults.push('de-DE');
        var flds = [];
        var cultListener = { change: { fn: this.onCurrentTranslationChange, scope: this} };
        flds.push(new Ext.form.TextField({
            fieldLabel: eBook.Accounts.New.Window_AccountNr
                            , name: 'nr'
                            , ref: 'nr'
                            , allowBlank: false
                            , disabled: this.actionMode == 'EDIT'
                            , maskRe: new RegExp('[0123456789]') 
                            , listeners: this.actionMode == 'ADD' ? cultListener : {}
        }));
        for (var i = 0; i < cults.length; i++) {
            flds.push(new Ext.form.TextField({
                fieldLabel: String.format(eBook.Accounts.New.Window_AccountDesc, eBook.Language.Languages[cults[i].replace('-', '_')])
                , name: cults[i]
                , width: 300
                , allowBlank: cults[i] != fcult
                , ref: cults[i].replace('-', '_')
                , listeners: cults[i] == eBook.Interface.Culture ? cultListener : {}
            }));
        }
        Ext.apply(this, {
            labelWidth: 160
            , layout: 'form'
            , ref: 'form'
            , monitorValid: true
            , frame: true
            , bodyStyle: 'padding:5px 5px 0'
            , items: flds

        });
        eBook.Accounts.TranslationPanel.superclass.initComponent.apply(this, arguments);
    }
    , getVisibilityEl: function() {
        return this.el;
    }
    , onCurrentTranslationChange: function(el, nw, old) {
        var orignr = ""+this.nr.getValue();
        var splittedOrignr = orignr.split('.');
        var nr = ("" + splittedOrignr[0].rpad('0', 6)).substring(0, 6);
        if (splittedOrignr.length > 1) {
            nr += ("." + splittedOrignr[1].lpad('0',3).substring(0, 3));
        }
      
        this.nr.setValue(nr);
        var ttl = this[eBook.Interface.Culture.replace('-', '_')].getValue();
        this.refOwner.refOwner.setTitle(nr + ' ' + ttl);
    }
    , loadAccount: function(account) {
        this.account = account;
        for (var i = 0; i < this.account.ads.length; i++) {
            var obj = this.account.ads[i];
            if (this[obj.c.replace('-', '_')]) {
                this[obj.c.replace('-', '_')].setValue(obj.d);
            }
        }
        
        this.nr.setValue(this.account.vnr);
        this.onCurrentTranslationChange();
    }
    , getTranslationObject: function(culture, internalNr) {
        return {
            fid: eBook.Interface.currentFile.get('Id')
            , inr: internalNr
            , c: culture
            , d: this[culture.replace('-', '_')].getValue()
            , ch: true
        };
    }
    , getResult: function() {
        var ac;
        var internalNr;
        if (this.account) {
            internalNr = this.account.inr;
            ac = {
                fid: eBook.Interface.currentFile.get('Id')
                , inr: this.account.inr
                , vnr: "" + this.nr.getValue()
                , iid: this.account.iid
                , hta: false
                , ely: this.account.ely
                , s: this.account.s
                , ss: this.account.ss
                , ps: this.account.ps
                , ita: false
                , dd: ''
                , c: eBook.Interface.Culture
                , lc: new Date()
                , ads: []
                , ams: []
                , adj: []
            };
        } else {
            internalNr = ("" + this.nr.getValue()).rpad('0', 12);
            ac = {
                fid: eBook.Interface.currentFile.get('Id')
                , inr: internalNr
                , vnr: "" + this.nr.getValue()
                , iid: ""
                , hta: false
                , ely: false
                , s: 0
                , ss: 0
                , ps: 0
                , ita: false
                , dd: ''
                , c: eBook.Interface.Culture
                , lc: new Date()
                , ads: []
                , ams: []
                , adj: []
            };
        }
        ac.ads.push(this.getTranslationObject('nl-BE', internalNr));
        ac.ads.push(this.getTranslationObject('fr-FR', internalNr));
        ac.ads.push(this.getTranslationObject('en-US', internalNr));
        ac.ads.push(this.getTranslationObject('de-DE', internalNr));
        return ac;
    }
});

Ext.reg('AccountTranslationsPanel', eBook.Accounts.TranslationPanel);
eBook.Accounts.Adjustments.MainTpl = new Ext.XTemplate('<tpl for=".">'
                    , '<div class="eb-adjs-container">'
                        , '<div class="eb-adjs-item-header">'
                            , '<div class="eb-adjs-item-header-nr">ADJ</div>'
                            , '<div class="eb-adjs-item-header-trans">DESCRIPTIONS</div>'
                            , '<div class="eb-adjs-item-header-SaldoN-1">SALDO N-1<br/>{[fm.decimalFormat(values.ps,2)]}</div>'
                            , '<div class="eb-adjs-item-header-Saldo">SALDO N<br/>{[fm.decimalFormat(values.s,2)]}</div>'
                        , '</div>'
                         , '<div class="eb-adjs-items-body">'
                        ,'<tpl for="adj">'
                            , '{[this.renderAdjustment(values)]}'
                        , '</tpl>'
                        , '<tpl if="values.adj.length==0">'
                        , '<div class="eb-adjs-items-nodata">ACCOUNT HAS NO ADJUSTMENTS</div>'
                        , '</tpl>'
                        , '</div>'
                   ,'</div>'
                 ,'</tpl>'
                 , {
                    renderAdjustment:function(values) {
                        return eBook.Accounts.Adjustments.AdjustmentTpl.apply(values);
                    }
                });

eBook.Accounts.Adjustments.AdjustmentTpl = new Ext.XTemplate(
                        '<tpl for=".">'
                            , '<div class="eb-adjs-item" id="eb-adjs-item-{adjNr}" adjustment="{adjNr}">'
                                , '<div class="eb-adjs-item-body">'
                                    , '<div class="eb-adjs-item-nr">{adjNr}</div>'
                                    , '<div class="eb-adjs-item-trans">'
                                        , '<tpl for="this.getDefaultDesc(values.ads)">'
                                            , '<div class="eb-adjs-item-trans-header">'
                                                , '<div id="eb-adjs-item-{parent.adjNr}-trans-header-txt" class="eb-adjs-item-trans-header-txt">{d}</div>'
                                                , '<div class="eb-adjs-item-trans-header-edit"></div>'
                                            , '</div>'
                                        , '</tpl>'
                                        , '<div class="eb-adjs-item-trans-body">'
                                            , '<div class="eb-adjs-item-trans-edit">'
                                                , '<tpl for="ads">'
                                                    , '<div class="eb-adjs-item-trans-row">'
                                                            , '<div class="eb-adjs-item-trans-row-language">{[this.getLanguage(values.c)]}</div>'
                                                            , '<div class="eb-adjs-item-trans-row-description">'
                                                                , '<input type="text" class="eb-adjs-item-trans-row-desc" id="eb-adj-desc-{parent.adjNr}-{c}" value="{d}"  adjustment="{parent.adjNr}" culture="{c}" />'
                                                            , '</div>'
                                                    , '</div>'
                                                , '</tpl>'
                                            , '</div>'
                                            , '<div class="eb-adjs-item-trans-edit-close"></div>'
                                        , '</div>'
                                    , '</div>'
                                    , '<div class="eb-adjs-item-SaldoN-1"><input class="eb-adjs-item-Saldo-input"  id="eb-adj-desc-{adjNr}-previous" type="text" value="{ps}"  adjustment="{adjNr}"/></div>'
                                    , '<div class="eb-adjs-item-Saldo"><input class="eb-adjs-item-Saldo-input"  id="eb-adj-desc-{adjNr}-current" type="text" value="{s}" adjustment="{adjNr}"/></div>'
                                , '</div>'
                                , '<tpl if="values.adjNr!=\'999\'"><div class="eb-adjs-item-delete"></div></tpl>'
                            , '</div>'
                        , '</tpl>'
                        , {
                            getDefaultDesc: function(descs) {
                                var o = descs[0];
                                for (var i = 0; i < descs.length; i++) {
                                    if (descs[i].c == eBook.Interface.Culture) {
                                        return descs[i];
                                    }
                                }
                                return o;
                            }
                            , getLanguage: function(culture) {
                                return eBook.Language.Languages[culture.replace('-', '_')];
                            }
                        });

                        eBook.Accounts.Adjustments.Panel = Ext.extend(Ext.Panel, {
                            initComponent: function() {
                                // Ext.apply(this,{html:'<div class="eb-adjs-container">&nbsp;</div>' });
                                Ext.apply(this, {
                                    layout: 'fit', tbar: [{
                                        ref: 'addadj',
                                        text: 'Add Adjustment',
                                        iconCls: 'eBook-icon-add-16',
                                        scale: 'small',
                                        iconAlign: 'left',
                                        disabled: false,
                                        handler: this.onAddAdjustmentClick,
                                        scope: this
                                    }, {
                                        ref: 'delAll',
                                        text: 'Clear all',
                                        iconCls: 'eBook-icon-delete-16',
                                        scale: 'small',
                                        iconAlign: 'left',
                                        disabled: false,
                                        handler: this.onClearAllClick,
                                        scope: this
}]

                                    });

                                    eBook.Accounts.Adjustments.Panel.superclass.initComponent.apply(this, arguments);
                                }
    , onRender: function(ct, pos) {
        eBook.Accounts.Adjustments.Panel.superclass.onRender.apply(this, arguments);
        if (this.dta == null) {
            this.dta = { adj: [] };
        }
        //        this.el.dom.innerHTML = '<div class="eb-adjs-container">&nbsp;</div>';
        //        this.contEl = this.el.child('.eb-adjs-container');
        this.loadAccount(this.dta);

        this.el.on('click', this.onBodyClick, this);
        this.el.on('mouseover', this.onBodyOver, this);
        this.el.on('mouseout', this.onBodyOut, this);

    }
    , onBodyOver: function(e, t, o) {
        var isDelete = e.getTarget('.eb-adjs-item-delete');

        if (isDelete) {
            var par = Ext.get(e.getTarget('.eb-adjs-item'));
            par.setStyle('background-color', 'rgba(255,0,0,0.1)');
        }
    }
    , onBodyOut: function(e, t, o) {
        var isDelete = e.getTarget('.eb-adjs-item-delete');

        if (isDelete) {
            var par = Ext.get(e.getTarget('.eb-adjs-item'));
            par.setStyle('background-color', '');

        }
    }
    , onBodyClick: function(e, t, o) {
        var isEdit = e.getTarget('.eb-adjs-item-trans-header-edit');
        var isSave = e.getTarget('.eb-adjs-item-trans-edit-close');
        var isDelete = e.getTarget('.eb-adjs-item-delete');

        var par = Ext.get(e.getTarget('.eb-adjs-item'));

        if (isEdit) {
            par.addClass('eb-adjs-item-editing');
        } else if (isSave) {
            par.removeClass('eb-adjs-item-editing');
        } else if (isDelete) {
            var adj = par.getAttribute("adjustment");
            var idx = parseInt(adj) - 1;
            this.dta.adj.remove(this.dta.adj[idx]);
            this.reIndexData();
        }

    }
    , reIndexData: function() {
        if (this.dta.adj[0].adjNr == "999") {
            this.dta.adj = [];
        } else {
            this.calculateFinal();
            var nl, fr, en;
            for (var j = 0; j < this.dta.ads.length; j++) {
                switch (this.dta.ads[j].c) {
                    case 'nl-BE':
                        nl = this.dta.ads[j].d;
                        break;
                    case 'fr-FR':
                        fr = this.dta.ads[j].d;
                        break;
                    case 'en-US':
                        en = this.dta.ads[j].d;
                        break;
                }
            }
            for (var i = 0; i < this.dta.adj.length; i++) {
                if (this.dta.adj[i].adjNr != '999') {

                    var adjNr = String.leftPad('' + (i + 1), 3, '0');
                    this.dta.adj[i].adjNr = adjNr;
                    this.dta.adj[i].inr = this.dta.inr + '.' + adjNr;
                    for (var j = 0; j < this.dta.adj[i].ads.length; j++) {
                        this.dta.adj[i].ads[j].inr = this.dta.inr + '.' + adjNr;
                        if (this.dta.adj[i].ads[j].d.indexOf('ADJ-') > -1) {
                            switch (this.dta.ads[j].c) {
                                case 'nl-BE':
                                    this.dta.adj[i].ads[j].d = nl;
                                    break;
                                case 'fr-FR':
                                    this.dta.adj[i].ads[j].d = fr;
                                    break;
                                case 'en-US':
                                    this.dta.adj[i].ads[j].d = en;
                                    break;
                            }
                            this.dta.adj[i].ads[j].d += ' ADJ-' + adjNr;
                        }
                    }
                }
            }

        }
        this.loadAccount(this.dta);

    }
    , onClearAllClick: function() {
        this.dta.adj = [];
        this.loadAccount(this.dta);
    }
    , onAddAdjustmentClick: function() {
        var pAdj = String.leftPad('' + this.maxAdj, 3, '0');
        this.maxAdj++;
        var sAdj = String.leftPad('' + this.maxAdj, 3, '0');
        var fnl = {
            fid: this.dta.fid
                        , inr: this.dta.inr + '.' + sAdj
                        , adjNr: sAdj
                        , ads: []
                        , s: 0
                        , ps: 0
        };
        for (var i = 0; i < this.dta.ads.length; i++) {
            fnl.ads.push({
                fid: this.dta.fid,
                inr: this.dta.inr + '.' + sAdj,
                c: this.dta.ads[i].c,
                d: this.dta.ads[i].d + ' ADJ-' + sAdj
            });
        }
        this.dta.adj.push(fnl);
        this.dta.adj.sort(function(a, b) {
            if (a.adjNr < b.adjNr) return -1;
            if (a.adjNr > b.adjNr) return 1;
            return 0;
        });
        var el;
        if (this.maxAdj == 1) {
            this.renderFinal(0, 0, -1);
            this.contEl = eBook.Accounts.Adjustments.MainTpl.overwrite(this.body, this.dta, true);
        } else {
            el = eBook.Accounts.Adjustments.AdjustmentTpl.insertAfter('eb-adjs-item-' + pAdj, fnl, true);
        }
        this.renderFormFields(el);
    }
    , onDescriptionChange: function(el, nw, old) {
        var idx = parseInt(el.adjustment) - 1;
        if (el.adjustment == "999") idx = this.dta.adj.length - 1;
        for (var i = 0; i < this.dta.adj[idx].ads.length; i++) {
            if (this.dta.adj[idx].ads[i].c == el.culture) {
                this.dta.adj[idx].ads[i].d = nw;
            }
        }

        if (el.culture == eBook.Interface.Culture) {
            var hd = Ext.get('eb-adjs-item-' + el.adjustment + '-trans-header-txt');
            hd.update(nw);
        }
    }
    , onSaldiChanged: function(el, nw, old) {
        var idx = parseInt(el.adjustment) - 1;
        if (el.id.indexOf('previous') > -1) {
            this.dta.adj[idx].ps = nw;
        } else {
            this.dta.adj[idx].s = nw;
        }
        this.calculateFinal();
    }
    , calculateFinal: function() {
        var it = Ext.getDom('eb-adjs-item-999');
        var idx = this.dta.adj.length - 1;
        var ttlS = 0;
        var ttlPs = 0;
        for (var i = 0; i < idx; i++) {
            ttlS += this.dta.adj[i].s;
            ttlPs += this.dta.adj[i].ps;
        }
        this.dta.adj[idx].s = Math.round((this.dta.s - ttlS) * 100) / 100;
        this.dta.adj[idx].ps = Math.round((this.dta.ps - ttlPs) * 100) / 100;
        var cp = Ext.getCmp('eb-adj-desc-999-previous');
        var cs = Ext.getCmp('eb-adj-desc-999-current');
        cp.setValue(this.dta.adj[idx].ps);
        cs.setValue(this.dta.adj[idx].s);

    }
    , renderFinal: function(s, ps, idx) {
        if (this.dta.adj.length > 0) {
            if (idx == -1) {
                var fnl = {
                    fid: this.dta.fid
                    , inr: this.dta.inr + '.999'
                    , adjNr: '999'
                    , ads: []
                };
                for (var i = 0; i < this.dta.ads.length; i++) {
                    fnl.ads.push({
                        fid: this.dta.fid,
                        inr: this.dta.inr + '.999',
                        c: this.dta.ads[i].c,
                        d: this.dta.ads[i].d + ' ADJ-999'
                    });
                }
                this.dta.adj.push(fnl);
                idx = this.dta.adj.length - 1;
            }
            this.dta.adj[idx].s = Math.round((this.dta.s - s) * 100) / 100;
            this.dta.adj[idx].ps = Math.round((this.dta.ps - ps) * 100) / 100;
        }

    }
    , prepareData: function() {
        if (!this.dta.adj) this.dta.adj = [];
        var hasFinal = -1;
        this.maxAdj = 0;
        var s = 0;
        var ps = 0;
        for (var i = 0; i < this.dta.adj.length; i++) {
            this.dta.adj[i].adjNr = this.dta.adj[i].inr.split('.')[1];
            if (this.dta.adj[i].adjNr == '999') {
                hasFinal = i;
            }
            else {
                var inr = parseInt(this.dta.adj[i].adjNr);
                if (inr > this.maxAdj) this.maxAdj = inr;
                s += this.dta.adj[i].s;
                ps += this.dta.adj[i].ps;
            }
        }
        this.renderFinal(s, ps, hasFinal);
        this.dta.adj.sort(function(a, b) {
            if (a.adjNr < b.adjNr) return -1;
            if (a.adjNr > b.adjNr) return 1;
            return 0;
        });
    }
    , getVisibilityEl: function() {
        return this.el;
    }
    , loadAccount: function(dta) {

        this.dta = dta; // to clean, get adj nr
        this.prepareData();
        // this.clearBodyEvents();
        // destroy form fields?
        if (this.rendered) {
            this.contEl = eBook.Accounts.Adjustments.MainTpl.overwrite(this.body, this.dta, true);
            var hght = this.ownerCt.getHeight(true);
            this.contEl.setStyle('height', (hght) + 'px');
            this.renderFormFields();
        }
    }
    , renderFormFields: function(el) {
        var hght = this.body.getHeight(true);
        this.contEl.setStyle('height', (hght) + 'px');
        this.contEl.child('.eb-adjs-items-body').setStyle('height', (hght - 40) + 'px');

        if (!el) el = this.contEl
        var descs = el.query('.eb-adjs-item-trans-row-desc');
        var saldi = el.query('.eb-adjs-item-Saldo-input');
        for (var i = 0; i < descs.length; i++) {
            //var id = descs.findParent('.eb-adjs-item').getAttribute("adjustment");
            var adj = descs[i].getAttribute("adjustment");
            new Ext.form.TextField({ applyTo: descs[i], width: 300, adjustment: adj
                , culture: descs[i].getAttribute("culture")
                , listeners: {
                    change: {
                        fn: this.onDescriptionChange
                        , scope: this
                    }
                }
            });
        }
        var lsts = {
            change: {
                fn: this.onSaldiChanged
                    , scope: this
            }
        };
        for (var i = 0; i < saldi.length; i++) {
            var adj = saldi[i].getAttribute("adjustment");

            new Ext.form.NumberField({ applyTo: saldi[i]
                    , width: 100
                    , align: 'right'
                    , adjustment: adj
                    , disabled: adj == "999"
                    , listeners: adj == "999" ? {} : lsts
                    , id: saldi[i].id
            });
        }
    }
    , getResult: function() {
        var dta = this.dta;
        for (var i = 0; i < this.dta.adj.length; i++) {
            var nr = dta.adj[i].adjNr;
            delete dta.adj[i].adjNr;
            dta.adj[i].vnr = this.dta.vnr + '.' + nr;
        }
        return dta.adj;
    }
                            });
      
Ext.reg('adjustmentsPanel', eBook.Accounts.Adjustments.Panel);
        
/*
 dta: {
        fid: ''
        , inr: '100000000000'
        , s: 12000
        , ps: 2343.56
        , ss: 1000
        , ads: [
            { fid: '', inr: '100000000000', c: 'nl-BE', d: 'parent omschrijving NL' }
            , { fid: '', inr: '100000000000', c: 'fr-FR', d: 'parent omschrijving FR' }
            , { fid: '', inr: '100000000000', c: 'en-US', d: 'parent omschrijving EN' }
        ]
        , adjs: [
                                //            {
                                //                fid: ''
                                //                        , inr: '100000000000.002'
                                //                        , s: 4354
                                //                        , ps: 343.12
                                //                        , ads: [
                                //                            { fid: '', inr: '100000000000.002', c: 'nl-BE', d: 'omschrijving 2 NL' }
                                //                            , { fid: '', inr: '100000000000.002', c: 'fr-FR', d: 'omschrijving 2 FR' }
                                //                            , { fid: '', inr: '100000000000.002', c: 'en-US', d: 'omschrijving 2 EN' }
                                //                        ]
                                //            }, {
                                //                fid: ''
                                //                        , inr: '100000000000.001'
                                //                        , s: 12.30
                                //                        , ps: 34.4
                                //                        , ads: [
                                //                            { fid: '', inr: '100000000000.001', c: 'nl-BE', d: 'omschrijving NL' }
                                //                            , { fid: '', inr: '100000000000.001', c: 'fr-FR', d: 'omschrijving FR' }
                                //                            , { fid: '', inr: '100000000000.001', c: 'en-US', d: 'omschrijving EN' }
                                //                            ]
                                //            }
        ]
                            }
    , 
*/
eBook.Accounts.Manage.Window = Ext.extend(eBook.Window, {
    
    initComponent: function() {
        var its = [{ xtype: 'AccountTranslationsPanel', title: 'Descriptions', ref: '../descs'
                    , actionMode: this.internalNr ? "EDIT" : "ADD"}];
            this.adjustmentsPanelAdded = false;
            if (!Ext.isEmpty(this.internalNr) && (this.internalNr.indexOf('.') == -1)) {
                its.push({ xtype: 'adjustmentsPanel', title: 'Adjustments', ref: '../adjs' });
                this.adjustmentsPanelAdded = true;
            }

            Ext.apply(this, {
                width: 758
            , height: 454
            , layout: 'fit'
            , maximizable: false
            , items: [
            { xtype: 'tabpanel', activeItem: 0, ref: 'tab'
                , items: its
            }
            ]
            , bbar: ['->', {
                ref: 'savechanges',
                text: "Save changes",
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
}]
            });
            eBook.Accounts.Manage.Window.superclass.initComponent.apply(this, arguments);
        }
    , onSaveClick: function() {
        var serviceMethod = 'SaveNewAccount'
        var account = this.tab.items.items[0].getResult();
        if (this.internalNr) {
            account.adj = this.tab.items.items[1].getResult();
            serviceMethod = 'SaveAccount'
        }
        this.getEl().mask('Saving');
        eBook.CachedAjax.request({
            url: eBook.Service.schema + serviceMethod
            , serviceMethod: serviceMethod
            , method: 'POST'
            , params: Ext.encode({ adc: account })
            , callback: this.onSaveAccountCallback
            , scope: this
        });
    }
    , onSaveAccountCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            //var robj = resp.respons
            this.parent.hasChanges = true;
            this.parent.doRefresh();
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , show: function(parent, ctx) {
        this.parent = parent;
        this.callingCtx = ctx;
        eBook.Accounts.Manage.Window.superclass.show.call(this);
        if (this.internalNr) {
            this.loadAccount(this.internalNr);
        }

    }
    , loadAccount: function(nr) {
        this.getEl().mask('loading');
        eBook.CachedAjax.request({
            url: eBook.Service.schema + 'GetAccount'
                , method: 'POST'
                , params: Ext.encode({ cadc: { FileId: eBook.Interface.currentFile.get('Id')
                                                , InternalNr: '' + nr
                                                , Culture: eBook.Interface.Culture
                                                , IncludeMappingDetails: false
                                                , IncludeAllDescriptions: true
                }
                })
                , callback: this.onLoadAccountCallback
                , scope: this

        });
    }
    , onLoadAccountCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            this.account = robj.GetAccountResult;
            this.tab.items.items[0].loadAccount(this.account);
            if (this.adjustmentsPanelAdded) {
                this.tab.items.items[1].loadAccount(this.account);
            }
            
            switch (this.callingCtx) {
                case "DESC":
                    //nothing, default tab
                    break;
                case "ADJ":
                    this.tab.setActiveTab(1);
                    break;
                case "ADDADJ":
                    this.tab.setActiveTab(1);
                    this.tab.items.items[1].onAddAdjustmentClick();
                    break;
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    }); 

/*
eBook.Accounts.New.Window = Ext.extend(eBook.Window, {
    actionMode: 'ADD'
    , initComponent: function() {
        var fcult = eBook.Interface.currentFile.get('Culture');
        this.defaultCult = fcult;
        var cults = [fcult];
        if (fcult != 'nl-BE') cults.push('nl-BE');
        if (fcult != 'fr-FR') cults.push('fr-FR');
        if (fcult != 'en-US') cults.push('en-US');
        if (this.internalNr) this.actionMode = 'EDIT';
        var flds = [];
        flds.push(new Ext.form.NumberField({
            fieldLabel: eBook.Accounts.New.Window_AccountNr
                            , name: 'nr'
                            , ref: 'nr'
                            , allowBlank: false
                            , disabled: this.actionMode == 'EDIT'
        }));
        for (var i = 0; i < cults.length; i++) {
            flds.push(new Ext.form.TextField({
                fieldLabel: String.format(eBook.Accounts.New.Window_AccountDesc, cults[i])
                            , name: cults[i]
                            , width: 300
                            , allowBlank: cults[i] != fcult
                            , ref: cults[i].replace('-', '_')
            }));
        }

        Ext.apply(this, {
            title: ''
            , busyMsg: ''
            , layout: 'fit'
            , width: 620
            , height: 300
            , modal: true
            , iconCls: 'eBook-Window-account-ico'
            , items: [new Ext.form.FormPanel({
                labelWidth: 160
                , ref: 'form'
                , monitorValid: true
                , frame: true
                , bodyStyle: 'padding:5px 5px 0'
                , items: flds
                , buttons: [{
                    //text:'Bewaar gegevens',
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onSaveClick,
                    formBind: true,
                    scope: this
                }, {
                    //text: 'Annuleer',
                    ref: '../cancelButton',
                    iconCls: 'eBook-window-cancel-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onCancelClick,
                    scope: this
}]
                })]
            });
            eBook.Accounts.New.Window.superclass.initComponent.apply(this, arguments);
        }
    , show: function(parent) {
        this.calledFrom = parent ? parent : null;
        eBook.Accounts.New.Window.superclass.show.call(this);
        if (this.internalNr) this.loadAccount(this.internalNr);
    }
    , loadAccount: function(nr) {
        this.getEl().mask('loading');
        Ext.Ajax.request({
            url: eBook.Service.schema + 'GetAccount'
                , method: 'POST'
                , params: Ext.encode({ adc: { fileId: eBook.Interface.currentFile.get('Id'), inr: nr} })
                , callback: this.onLoadAccountCallback
                , scope: this

        });
    }
    , onLoadAccountCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , correctVisualNr: function(nr) {
        nr = '' + nr;
        nr = nr.trim();
        return nr.rightPad('0', 6); // get file visual account length
    }
    , getInternalNr: function(nr) {
        nr = '' + nr;
        nr = nr.trim();
        return nr.rightPad('0', 12);
    }
    , onCancelClick: function() {
        this.close();
    }
    , onSaveClick: function() {
        //mask
        var ct = this.defaultCult.replace('-', '_');
        var t = {
            fid: eBook.Interface.currentFile.get('Id')
            , inr: this.getInternalNr(this.form.nr.getValue())
            , vnr: this.correctVisualNr(this.form.nr.getValue())
            , nl: '' + this.form.nl_BE.getValue()
            , fr: '' + this.form.fr_FR.getValue()
            , en: '' + this.form.en_US.getValue()
            , Person: eBook.User.getActivePersonDataContract()
        };
        var def = this.form[ct].getValue();
        if (Ext.isEmpty(t.nl)) t.nl = def;
        if (Ext.isEmpty(t.fr)) t.fr = def;
        if (Ext.isEmpty(t.en)) t.en = def;

        this.getEl().mask('Saving');
        Ext.Ajax.request({
            url: eBook.Service.schema + (this.actionMode == 'ADD' ? 'AddAccount' : 'EditAccount')
            , method: 'POST'
            , params: Ext.encode({ adc: t })
            , callback: this.onAddCallback
            , scope: this

        });

    }
    , onAddCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            if (this.calledFrom) {
                if (this.calledFrom.doRefresh) {
                    this.calledFrom.doRefresh();
                } else if (this.calledFrom.store) { this.calledFrom.store.reload(); }
            }
            // unmask
            this.close.defer(300, this);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    });
                            
                                
*/eBook.Accounts.Mappings.handleClick = function(menuItem, b, c, d, e, f, g) {
    var mnu = menuItem;
    while (mnu.parentMenu != null) {
        mnu = mnu.parentMenu;
    }
    if (mnu.showType == "mapping") {
        var mpane = Ext.WindowMgr.get('eBook-accountmapping-window');
        mpane.changeMapping(menuItem, mnu.activeEl);
    } else {
        var cmp = Ext.getCmp(mnu.activeEl.id);
        cmp.setValue(menuItem.listObj);
    }
    //Ext.get(mnu.activeEl);  //.setStyle('background-color', '#F00');
};

eBook.Accounts.MappingMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            cls:'eBook-mapping-menu'
            , shadow:false
        });
        eBook.Accounts.MappingMenu.superclass.initComponent.apply(this, arguments);
    }
    , show: function(el) {
        this.showType = "mapping";
        this.activeEl = el;
        eBook.Accounts.MappingMenu.superclass.show.apply(this, arguments);
    }
    , showField: function(el) {
        this.showType = "field";
        this.activeEl = el;
        eBook.Accounts.MappingMenu.superclass.show.apply(this, arguments);
    }
});

Ext.reg('mappingmenu', eBook.Accounts.MappingMenu);