﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page 
{
    public string Culture;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!string.IsNullOrEmpty(Request.Params["culture"]))
        {
            Culture = Request.Params["culture"];
        }
        else
        {

            HttpCookie languageCookie = Request.Cookies["eBook.Language"];
            if (languageCookie != null)
            {
                Culture = languageCookie.Value;
            }
            else
            {
                if (Request.UserLanguages == null)
                {
                    Culture = "nl-BE";
                }
                else
                {
                    string Lang = Request.UserLanguages[0];
                    if (Lang != null)
                    {
                        if (Lang.Length < 3)
                            Lang = Lang + "-" + Lang.ToUpper();
                        //Culture = Lang == "en-US" ? "nl-BE":Lang;
                        Culture = Lang;
                    }
                }
            }
        }
        if (Culture.StartsWith("en")) Culture = "en-US";
       // if (Culture == "en-US") Culture = "nl-BE";
        Response.Cookies["eBook.Language"].Value = Culture;
        Response.Cookies["eBook.Language"].Expires = DateTime.Now.AddYears(1);
        string[] parts = Culture.Split(new char[] { '-' });
        if(parts.Length==2) Culture = string.Format("{0}-{1}", parts[0].ToLower(), parts[1].ToUpper());
    }
}
