﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Configuration;

public partial class UploadMultiple : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        MessagesOb results = new MessagesOb();
        results.files = new List<MessageOb>();
        HttpContext context = HttpContext.Current;


        if (context.Request.Files.Count > 0)
        {
            bool cont = true;

             HttpFileCollection hfc = Request.Files;
             foreach (String h in hfc.AllKeys)
            {
                // Add an item to the BulletedList if a file
                // was specified for the corresponding control.
                if (hfc[h].ContentLength > 0)
                {

                    HttpPostedFile file = hfc[h];
                    MessageOb result = new MessageOb();
                    result.originalFile = Path.GetFileName(file.FileName);
                    result.id = Guid.NewGuid();
                    result.extension = Path.GetExtension(file.FileName);
                    result.contentType = file.ContentType;
                    result.file = string.Format("{0}{1}", result.id.ToString(), Path.GetExtension(file.FileName));


                    byte[] binaryWriteArray = new byte[file.InputStream.Length];
                    file.InputStream.Read(binaryWriteArray, 0, (int)file.InputStream.Length);
                    string path = Path.Combine(EY.com.eBook.Core.Config.UploadFolder, result.file);
                    FileStream objfilestream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);

                    //Write the file and close it
                    objfilestream.Write(binaryWriteArray, 0, binaryWriteArray.Length);
                    objfilestream.Close();
                    result.message = "";
                    results.success = true;
                    results.files.Add(result);
                }
                //else
                //{
                //    results.success = false;
                //    results.message = "FILE " + h + " IS EMPTY.";
                //    break;
                //}
            }



        }



        // prepare response
        
        Literal1.Text = JSONHelper.Serialize<MessagesOb>(results);



    }

}


public class MessagesOb
{

    public bool success { get; set; }

    public string message { get; set; }

    public List<MessageOb> files { get; set; }

}


public class MessageOb
{

    public bool success { get; set; }

    public string message { get; set; }

    public string file { get; set; }

    public string extension {get;set;}

    public string contentType {get;set;}

    public string originalFile { get; set; }

    public Guid id { get; set; }

}



public class JSONHelper
{

    public static string Serialize<T>(T obj)
    {

        System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());

        MemoryStream ms = new MemoryStream();

        serializer.WriteObject(ms, obj);

        string retVal = Encoding.UTF8.GetString(ms.ToArray());

        return retVal;

    }



    public static T Deserialize<T>(string json)
    {

        T obj = Activator.CreateInstance<T>();

        MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));

        System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());

        obj = (T)serializer.ReadObject(ms);

        ms.Close();

        return obj;

    }

}

