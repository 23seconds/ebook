﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

     <title>eBook <%=((HttpContext.Current.IsDebuggingEnabled || HttpContext.Current.Request.QueryString["debug"]=="y") ? System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"] : System.Configuration.ConfigurationManager.AppSettings["eBook.Version"])%></title>
	<link rel="SHORTCUT ICON" href="http://localhost/eBook30/favicon.ico"/>
	<link rel="apple-touch-icon" href="http://defranmcetbo01/apple-touch-icon.png" />
<% string specVersion = System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]; %>

<link rel="stylesheet" type="text/css" href="css/ext-all.css" />
        <link rel="stylesheet" type="text/css" href="css/xtheme-EY.css" />
         <link rel="stylesheet" type="text/css" href="css/typography.css" />
		    <link rel="stylesheet" type="text/css" href="css/eyNav.css" />
		    <link rel="stylesheet" type="text/css" href="css/structure.css" />
		    
    <link rel="stylesheet" type="text/css" href="css/ey.css" />
    <link rel="stylesheet" type="text/css" href="css/mappings.css" />
		<link rel="stylesheet" type="text/css" href="css/eBook.css?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>" />
		<link rel="stylesheet" type="text/css" href="css/eBook-icons.css?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>" />
		<link rel="stylesheet" type="text/css" href="css/ux/GroupSummary.css" />
		<link rel="stylesheet" type="text/css" href="css/ux/GridSummary.css" />
		<link rel="stylesheet" type="text/css" href="css/ux/Ext.ux.grid.RowActions.css" />
		<link rel="Stylesheet" type="text/css" href="css/Ext.ux.tot2ivn.VrTabPanel.css" />
		<link rel="stylesheet" type="text/css" href="css/Ext.ux.icontabpanel.css" />
		<link rel="stylesheet" type="text/css" href="css/eBook-biztax.css" />
		<script type="text/javascript" src="js/tiny_mce/tiny_mce.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
</head>
<body>

    <form id="form1" runat="server">
    </form>
    		<div id="eBook-mask"></div>
		<div id="eBook-startload" class="eBook-masked-msg-container">&nbsp;
			<div class="eBook-masked-msg-centered">
				<div id="eBook-splash" class="eBook-masked-msg-window">
					<div id="eBook-splash-header" class="eBook-masked-msg-header">
						<img height="42"  alt="Ernst &amp; Young" src="<%=((Environment.MachineName == "BEBE-IT05238") ? "images/ey/eyLogo-2-dev.png" : ((Environment.MachineName == "DEFRANMCETBO01") ? "images/ey/eyLogo-2-test.png" : "images/ey/eyLogo-2.png"))%>" class="eylogo" title="Site name home" id="ImgHome">
					</div>
					<div id="eBook-icon" class="eBook-masked-msg-icon"></div>
					<div id="eBook-startload-title">eBook <%=((HttpContext.Current.IsDebuggingEnabled || HttpContext.Current.Request.QueryString["debug"]=="y") ? System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"] : System.Configuration.ConfigurationManager.AppSettings["eBook.Version"])%></div>
					<div id="eBook-startload-loader">
						<div id="eBook-start-loader-loading"></div>
						<div id="eBook-start-loader-text">Loading ExtJS...</div>
					</div>
				</div>
 			</div>
 		</div>
 		<!--div class="eBook-masked-msg-container">&nbsp;
					<div class="eBook-masked-msg-centered">
						<div class="eBook-masked-msg-window">
							<div class="eBook-masked-msg-header">
								<img height="42" width="185" alt="Ernst &amp; Young" src="images/ey/eyLogo-en.png" runat="server" class="eylogo" title="Site name home" id="Img1">
							</div>
							<div class="eBook-masked-msg-icon eBook-masked-msg-gerenating-pdf"></div>
							<div class="eBook-masked-msg">
								Bundel wordt gegenereerd.
							</div>
						</div>
		 			</div>
 		</div-->
        <div id="getPersonResponse"></div>
        
<%if (HttpContext.Current.IsDebuggingEnabled || HttpContext.Current.Request.QueryString["debug"]=="y")
  { %>
  <script type="text/javascript" src="js/pdf-min.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
  <script type="text/javascript">
      // Specify the main script used to create a new PDF.JS web worker.
      // In production, change this to point to the combined `pdf.js` file.
      //PDFJS.workerSrc = 'js/worker_loader.js';
  </script>
        <script type="text/javascript" src="js/ext/ext-base-debug.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ext-all-debug.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/util/base64.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/util/clone.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/overrides/Ext.grid.RowSelectionModel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/overrides/Ext.form.ComboBox.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/overrides/Ext.grid.GridView.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/extensions/Ext.data.Types.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/extensions/Ext.Array.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/extensions/Ext.Element.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/grid/search.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/grid/LockingGridView.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/grid/GridSummary.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/grid/GroupSummary.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/grid/RowActions.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/grid/RowExpander.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/StatusBar.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/tree/TreePanel.toOBject.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/tree/TreePanel.CopyDropNode.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/tree/Ext.ux.TreeCombo.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/form/FileUploadField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/form/CurrencyField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ux/Ext.ux.TinyMCE.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/Ext-ux-raphael-min.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/Ext/ux/Ext.ux.tot2ivn.VrTabPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript">
        	document.getElementById("eBook-start-loader-text").innerText="Loading library...";
        </script>
		
		<script type="text/javascript" src="js/eBook/Layout/TableFormLayout.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/eBook.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/LazyLoad.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Person.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Language/ext-lang-<%=Culture %>.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Help.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Editor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Util.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/LoadOverlay.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Service.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/data/WcfProxy.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/data/RecordTypes.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/data/JsonStore.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/History.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/TabPanel/CheckTabPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/YesNoField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/LanguageBox.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/RepositoryCategory.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Fields/IconLabelField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Fields/LegalType.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/BankDetails.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/EmailField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/FodAddress.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/FodAddressType.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/FodContactType.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/FodCountry.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/FodPhone.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/FodZipCode.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Fields/Partners.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/grid/ColumnRenders.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/grid/PagedGridPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/grid/GridViewNote.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/GTH/TeamAccordion.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/GTH/ClientSelector.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/GTH/Team1.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/GTH/Team2.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/GTH/Team4.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/GTH/Team5.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/GTH/Team6.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/GTH/TeamExecutor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/GTH/Home.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Home/Charts.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Home/RecentFiles.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Home/MyClients.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Home/Home.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		
		<script type="text/javascript" src="js/eBook/Menu/ListViewMenu.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Docstore/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Recycle/IconContextMenu.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/List/FileIconColumn.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/List/ListView.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Menu/menu.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Menu/Client.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Menu/File.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Interface.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/BusinessRelations/FormPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/BusinessRelations/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/Meta/Grid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Meta/PersonEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Meta/MeetingEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Meta/ShareHolderEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Meta/PeerGroupEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Meta/ShareholdersMeetingEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Meta/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Health/Panel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		
		<script type="text/javascript" src="js/eBook/Repository/SingleUploadWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Repository/Tree.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Repository/RepositoryItemLinks.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Repository/FileDetailsPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Repository/LocalFileBrowser.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Repository/ReplaceChoiceWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/Repository/FileGrid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Repository/FileWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Repository/ContextLinkList.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Repository/SelectTree.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/File/Services.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/File/WorksheetsView.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/File/ServicesTabPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/File/NewHome.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/File/Quickprint.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Client/Home.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Client/Line.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Client/Panel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Client/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Client/PreWorksheet.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Client/ContextMenu.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Client/TrashcanList.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Client/TrashcanWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/NewFile/Wizard.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/NewFile/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Accounts/ContextMenu.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Accounts/Util.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Accounts/TranslationPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Accounts/AdjustmentPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Accounts/New/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
			
		<script type="text/javascript" src="js/eBook/Accounts/Mappings/Handler.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/mappings-<%=Culture %>.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Accounts/Mappings/MappingPane.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Accounts/Mappings/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/Accounts/Mappings/Adjustments/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/Accounts/Mappings/Adjustments/BookingLinesPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>

		<script type="text/javascript" src="js/eBook/Forms/AnnualAccounts/Staff.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Forms/AnnualAccounts/Manager.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Forms/AnnualAccounts/Partner.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>


		<script type="text/javascript" src="js/eBook/Journal/Grid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Journal/GroupedGrid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Journal/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Journal/Booking/AccountEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Journal/Booking/AccountEditorColumn.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Journal/Booking/Panel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Journal/Booking/Grid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Journal/Booking/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
	
	    <script type="text/javascript" src="js/eBook/Bundle/NewWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
	    <script type="text/javascript" src="js/eBook/Bundle/EditWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/Editor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/ItemSettings.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/BundleSettings.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/Library.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/Log.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/ReviewNotes.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/Form.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/FormOverview.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Bundle/HtmlEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>



		<script type="text/javascript" src="js/eBook/BizTax/Store.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/BizTax/Toolbar.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/BizTax/ContextGrid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/BizTax/ValuelistOrOther.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/BizTax/Module.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/BizTax/Tab.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<!--script type="text/javascript" src="js/eBook/BizTax/Pane.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script-->
		<script type="text/javascript" src="js/eBook/BizTax/UploadField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<!--script type="text/javascript" src="js/eBook/BizTax/ValidationOverview.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script-->
		<script type="text/javascript" src="js/eBook/BizTax/Declaration/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		
		<script type="text/javascript" src="js/eBook/BizTax/ItemSettings.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/BizTax/UploadEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/BizTax/UploadEditorWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
	
		<script type="text/javascript" src="js/eBook/Client/ManualResourcesWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		
		<!-- WORKSHEET SCRIPTS //-->
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/Field.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/NumberField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/CurrencyField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/TextField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/DateField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/Checkbox.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/Booking.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/Label.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/SpacerField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/PercentField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Worksheets/Fields/Dropdown.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
	
        <script type="text/javascript" src="js/eBook/Worksheets/Lists/Global.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/InnerRuleAppList.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/Accounts.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/AccountsFromMapping.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/Mappings.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/BusinessRelations.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/Years.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/BookyearMonths.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/Collections.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Lists/DropDownFieldsCopy.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Worksheets/Form.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Grid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Tab.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/TabPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Worksheets/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		
		
		<script type="text/javascript" src="js/eBook/Document/Fields/DefaultField.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/Fields/BlockEditor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/EntryPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/BlockContextMenu.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/BlockContainer.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/BlocksDataView.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/Editor.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/Builder.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/DocumentGroupList.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/TemplateChooser.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/NewDocumentWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/OpenDocumentWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Document/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Excel/ColumnCombo.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Excel/UploadWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Excel/MappingPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Excel/GridView.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Excel/Grid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Excel/MainPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Excel/TabPanels.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Excel/Configurator.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Excel/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>

        <script type="text/javascript" src="js/eBook/Exact/UploadWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/ProAcc/ActionDataView.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/ProAcc/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/ProAcc/ExportWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
        <script type="text/javascript" src="js/eBook/Reports/EngagementAgreement/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/Reports/EngagementAgreement/Grid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/eBook/Reports/EngagementAgreement/CriteriaContainer.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>

		<script type="text/javascript" src="js/eBook/Reports/PowerOfAttorney/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Reports/PowerOfAttorney/Grid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Reports/PowerOfAttorney/CriteriaContainer.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Reports/PowerOfAttorney/Log.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>

		<script type="text/javascript" src="js/eBook/Reports/FileService/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Reports/FileService/Grid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Reports/FileService/CriteriaContainer.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Reports/FileService/Log.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>

		<script type="text/javascript" src="js/eBook/Pdf/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Pdf/UploadWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Pdf/ManualUploadWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/Help/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Help/ChampionsWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>

        <script type="text/javascript" src="js/eBook/Yearend/Window.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<script type="text/javascript" src="js/eBook/PMT/TeamGrid.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/PMT/TeamWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		<!-- XBRL SCRIPTS //-->
		<script type="text/javascript" src="js/eBook/Xbrl/ClientDetails.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/AttachmentPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/ValidationCheckPanel.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/ContactDetails.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/Wizard.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/Overview.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/BizTaxWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/ManualWizard.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/BizTaxManualWindow.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		<script type="text/javascript" src="js/eBook/Xbrl/PartnerSelect.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
		
		
		<% }  else
                   {%>
        <script type="text/javascript" src="js/ext/ext-base.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="js/ext/ext-all.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript">
            document.getElementById("eBook-start-loader-text").innerText = "Loading library...";
        </script>  
        <script type="text/javascript" src="jsDeploy/ebook-ext.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        <script type="text/javascript" src="jsDeploy/ebook-<%=Culture %>.js?dc=<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>"></script>
        
        
        
                   
        <%
                   }%>
 
		
		<script type="text/javascript">
		    eBook.Splash.setText("Building interface...");
        </script>
        <div id="eBook-loading-overlay" style="">&nbsp;</div>
        <script type="text/javascript">
		<!--
		    eBook.Debug = '<%=(HttpContext.Current.IsDebuggingEnabled || HttpContext.Current.Request.QueryString["debug"]=="y").ToString().ToLower() %>';
            Ext.ux.TinyMCE.initTinyMCE();
            eBook.Version = '<%=((HttpContext.Current.IsDebuggingEnabled || HttpContext.Current.Request.QueryString["debug"]=="y") ? System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"] : System.Configuration.ConfigurationManager.AppSettings["eBook.Version"])%>';
            eBook.SpecificVersion = '<%=System.Configuration.ConfigurationManager.AppSettings["eBook.SpecificVersion"]%>';
            eBook.Share = '<%=System.Configuration.ConfigurationManager.AppSettings["eBook.Folders.Shared.Path"].Replace(@"\",@"/") %>';
            eBook.DocStorePath = '<%=System.Configuration.ConfigurationManager.AppSettings["eBook.DocStore"] %>';
            eBook.LoadOverlay.setEl(Ext.get('eBook-loading-overlay'));
            
            Ext.onReady(function() {
                Ext.getBody().on("contextmenu", Ext.emptyFn, null, { preventDefault: true });
                
                Ext.QuickTips.init();
                eBook.User = new eBook.Person({ winAccount: '<%=Context.User.Identity.Name.Replace(@"\",@"\\") %>' }); /*'EURW\BEBXTAXPKER'*/
               // eBook.Splash.hide();
                eBook.Interface.Culture = '<%=Culture %>';
                eBook.User.login();
//                eBook.Interface.render();

//               var wn = new eBook.Repository.LocalFileBrowser({});
//               wn.show();
               // wn = new eBook.Accounts.Mappings.Window({});
                //wn.show();
              
               
                
            });



		//-->
		</script>
		
</body>
</html>
