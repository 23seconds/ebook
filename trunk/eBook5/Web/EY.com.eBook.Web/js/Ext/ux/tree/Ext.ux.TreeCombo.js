
Ext.ns('Ext.ux', 'Ext.ux.form');

Ext.ux.form.TreeCombo = Ext.extend(Ext.form.TriggerField, {
    id: Ext.id(),

    triggerClass: 'x-form-tree-trigger',
    useCheckboxes: false,
    initComponent: function() {
        this.readOnly = false;
        this.isExpanded = false;

        if (!this.sepperator) {
            this.sepperator = ','
        }

        if (!Ext.isDefined(this.singleCheck)) {
            this.singleCheck = true;
        }

        Ext.ux.form.TreeCombo.superclass.initComponent.call(this);
        this.on('specialkey', function(f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTriggerClick();
            }
        }, this);
        this.on('show', function() {
            this.setRawValue('');
            this.getTree();

            if (this.treePanel.loader.isLoading()) {
                this.treePanel.loader.on('load', function(c, n) {
                    n.expandChildNodes(true);
                    if (this.setValueToTree()) this.getValueFromTree();
                }, this);
            } else {
                if (this.setValueToTree()) this.getValueFromTree();
            }
        });
    },

    onTriggerClick: function() {
        if (this.isExpanded) {
            this.collapse();
        } else {
            this.expand();
        }
    },
    clearValue: function() {
        if (this.hiddenField) {
            this.hiddenField.value = '';
        }
        this.setRawValue('');
        this.lastSelectionText = '';
        this.applyEmptyText();
        this.value = '';
        if (this.selectedNode) this.selectedNode.attributes.checked = false;
        this.selectedNode = null;
    },
    // was called combobox was collapse
    collapse: function() {
        this.isExpanded = false;
        this.getTree().hide();
        if (this.resizer) this.resizer.resizeTo(this.treeWidth, this.treeHeight);
        this.getValueFromTree();
    },

    // was called combobox was expand
    expand: function() {
        this.isExpanded = true;
        this.getTree().show();
        this.getTree().getEl().alignTo(this.wrap, 'tl-bl?');

        this.setValueToTree();
    },
    setValue: function(v) {
        this.value = v;
        this.setValueToTree();
        if (this.selectedNode) this.valueText = this.selectedNode.attributes.Path;
        this.setRawValue(this.valueText);
    },

    getValue: function() {
        if (!this.value) {
            return '';
        } else {
            return this.value;
        }
    },
    setValueToTree: function() {
        // check for tree ist exist
        if (!this.treePanel) this.getTree();
        var rootNode = this.treePanel.getRootNode();

        if (!this.selectedNode && !Ext.isEmpty(this.value)) {
            //find node
            var nde = rootNode.findChild('StructureId', this.value, true);
            if (nde == null) return false;
            this.selectedNode = nde;
        }
        //        // split this.value to array with sepperate value-elements
        //        var arrVal = new Array();
        //        try {
        //            arrVal = this.value.split(this.sepperator);
        //        } catch (e) { };

        // find root-element of treepanel, and expand all childs

        //rootNode.collapseChildNodes(true);
        //node.expandChildNodes(false);

        //        var subNode = this.selectedNode;
        //        while (subNode.parentNode != null) {
        //            subNode.parentNode.expand();
        //            subNode = subNode.parentNode;
        //        }
        // this.selectedNode.parentNode.collapse();
        if (this.selectedNode) {
            try { this.selectedNode.ensureVisible(); } catch(ex) {}
            this.selectedNode.select();
            this.selectedNode.getUI().toggleCheck(true);
        }

        // this.selectedNode.parentNode.expand();


        return true;
    }

    , getNodePath: function(node) {
        return node.attributes.Path;
    }
    //    , setTreePath: function(path, callback, scope) {
    //        var ids = path.split('##');
    //        //this.expand();
    //        var node = this.getTree().getRootNode();
    //        this.loadTreePath = {
    //            ids: ids
    //            , callback: callback
    //            , scope: scope
    //        };
    //        node.expand(false, false, this.setTreePathInner, this);

    //    }
    //    , setTreePathInner: function(nde) {
    //        if (!nde || this.loadTreePath == null) return;
    //        if (this.loadTreePath.ids.length == 0) {
    //            this.clearValue();
    //            nde.select();
    //            nde.getUI().toggleCheck(true);
    //            nde.attributes.checked = true;
    //            this.getValueFromTree();
    //            if (this.loadTreePath.callback) {
    //                this.loadTreePath.callback.defer(200, this.loadTreePath.scope || this, [nde, true]);
    //            }
    //            this.loadTreePath = null;

    //        } else {
    //            var id = this.loadTreePath.ids.shift();
    //            var subNode = nde.findChild('id', id, false);
    //            if (!subNode) {
    //                if (this.loadTreePath.callback) {
    //                    this.loadTreePath.callback.call(this.loadTreePath.scope || this, nde, false);
    //                }
    //            } else {
    //                if (this.loadTreePath.ids.length == 0) {
    //                    this.setTreePathInner(subNode);
    //                } else {
    //                    subNode.expand(false, false, this.setTreePathInner, this);
    //                }
    //            }
    //        }
    //    }
    , getValueFromTree: function() {
        this.ArrVal = new Array();
        this.ArrDesc = new Array();

        if (this.singleCheck) {
            var ns = this.treePanel.getChecked();

            if (ns && ns.length > 0) {
                var n = ns[0];
                this.value = n.attributes.StructureId;
                this.valueText = n.attributes.Path;
                if (this.selectedNode != n) this.selectedNode = n;
                this.selectedPath = this.getNodePath(n);
                this.fireEvent('select', this, n);
            }
        } else {
            //if (this.useCheckboxes) {
            Ext.each(this.treePanel.getChecked(), function(item) {
                if (!item.attributes.value) {
                    this.ArrVal.push(item.attributes.StructureId);
                } else {
                    this.ArrVal.push(item.attributes.value);
                }
                this.ArrDesc.push(item.attributes.text);
            }, this);

            this.fireEvent('select', this, this.value);
        }

        this.setRawValue(this.valueText);

        // GET PATH


        //        } else {
        //            this.setRawValue(node.text);
        //            this.value = node.id;
        //            //console.debug(node);
        //            this.fireEvent('select', this, node);
        //        }
    },

    validateBlur: function() {
        return !this.treePanel || !this.treePanel.isVisible();
    },

    /*
    * following functions are using by treePanel
    */
    setRawValue: function(v) {
        if (this.rendered) {
            if (this.metrics == null) {
                this.metrics = Ext.util.TextMetrics.createInstance(this.el.dom);
                this.pointsSize = this.metrics.getWidth('...');
                this.characterSize = this.metrics.getWidth('a');
            }
            var elWd = this.el.getWidth();
            var strSize = elWd - 2 - this.pointsSize;
            var wd = this.metrics.getWidth(v);
            if (wd > (elWd - 2)) {
                var mxChars = Math.round(strSize / this.characterSize);
                var v = '...' + v.substring(v.length - mxChars, v.length);
            }
            return this.el.dom.value = Ext.isEmpty(v) ? '' : v;
        }
        return '';
        return this.rendered ? (this.el.dom.value = (Ext.isEmpty(v) ? '' : v)) : '';
    },
    getTree: function() {
        if (!this.treePanel) {
            if (!this.treeWidth) {
                this.treeWidth = Math.max(200, this.listWidth || 200);
            }
            if (!this.treeHeight) {
                this.treeHeight = 200;
            }
            this.treePanel = new Ext.tree.TreePanel({
                renderTo: Ext.getBody(),
                loader: this.loader,
                root: this.root,
                rootVisible: false,
                floating: true,
                autoScroll: true,
                minWidth: 200,
                minHeight: 200,
                singleExpand: true,
                width: this.treeWidth,
                height: this.treeHeight,
                listeners: {
                    hide: this.onTreeHide,
                    show: this.onTreeShow,
                    click: this.onTreeNodeClick,
                    checkchange: this.onTreeCheckChange,
                    expandnode: this.onExpandOrCollapseNode,
                    collapsenode: this.onExpandOrCollapseNode,
                    resize: this.onTreeResize,
                    scope: this
                }
            });
            this.treePanel.show();
//            this.treePanel.getRootNode().cascade(function(n) {
//                // preload
//                n.expand();
//            }, this);
            this.treePanel.hide();
            this.relayEvents(this.treePanel.loader, ['beforeload', 'load', 'loadexception']);
            if (this.resizable) {
                this.resizer = new Ext.Resizable(this.treePanel.getEl(), {
                    pinned: true, handles: 'se'
                });
                this.mon(this.resizer, 'resize', function(r, w, h) {
                    this.treePanel.setSize(w, h);
                }, this);
            }
        }
        return this.treePanel;
    },

    onExpandOrCollapseNode: function() {
        if (!this.maxHeight || this.resizable)
            return;  // -----------------------------> RETURN
        var treeEl = this.treePanel.getTreeEl();
        var heightPadding = treeEl.getHeight() - treeEl.dom.clientHeight;
        var ulEl = treeEl.child('ul');  // Get the underlying tree element
        var heightRequired = ulEl.getHeight() + heightPadding;
        if (heightRequired > this.maxHeight)
            heightRequired = this.maxHeight;
        this.treePanel.setHeight(heightRequired);
    },

    onTreeResize: function() {
        if (this.treePanel)
            this.treePanel.getEl().alignTo(this.wrap, 'tl-bl?');
    },

    onTreeShow: function() {
        Ext.getDoc().on('mousewheel', this.collapseIf, this);
        Ext.getDoc().on('mousedown', this.collapseIf, this);
    },

    onTreeHide: function() {
        Ext.getDoc().un('mousewheel', this.collapseIf, this);
        Ext.getDoc().un('mousedown', this.collapseIf, this);
    },

    collapseIf: function(e) {
        if (!e.within(this.wrap) && !e.within(this.getTree().getEl())) {
            this.collapse();
        }
    },

    onTreeNodeClick: function(node, e) {
        if (node.attributes.Files != false) {
            var sm = this.getTree().getSelectionModel();
            if (this.singleCheck) {
                Ext.each(this.treePanel.getChecked(), function(item) {
                    item.attributes.checked = false;
                }, this);
                sm.clearSelections();
            }
            sm.select(node);
            if (this.selectedNode) this.selectedNode.attributes.checked = false;
            node.attributes.checked = true;
            this.selectedNode = node;
            // console.debug(this.singleSelect);
            //        this.setRawValue(node.text);
            //        this.value = node.id;
            //        //console.debug(node);
            //        this.fireEvent('select', this, node);
            this.collapse();
        } else {
            if (this.selectedNode) this.selectedNode.attributes.checked = false;
            this.selectedNode = null;
        }

    },
    disable: function() {
        Ext.ux.form.TreeCombo.superclass.disable.call(this);
        this.getTree().disable();
    },
    enable: function() {
        Ext.ux.form.TreeCombo.superclass.enable.call(this);
        this.getTree().enable();
    },
    onTreeCheckChange: function(node, value) {
        if (this.singleCheck) {
            // temporary disable event-listeners on treePanel-object 
            this.treePanel.suspendEvents(false);

            // disable all tree-checkboxes, there checked at the moment			
            Ext.each(this.treePanel.getChecked(), function(arrVal) {
                arrVal.getUI().toggleCheck(false);
            });

            // re-check the selected node on treePanel-object
            node.getUI().toggleCheck(true);

            // activate event-listeners on treePanel-object
            this.treePanel.resumeEvents();
        }
    },

    getRendererFunction: function(value) {
        var out = new Array();

        //console.info(value);
        //console.debug(this.getEditor().field.loader);
        if (String.trim(value) != "") {
            var TreePanel = this.getEditor().field.getTree();
            var Sepperator = this.getEditor().field.sepperator;


            // split this.value to array with sepperate value-elements
            var arrVal = new Array();
            try {
                arrVal = value.split(Sepperator);
            } catch (e) { };


            TreePanel.getRootNode().cascade(function(n) {
                Ext.each(arrVal, function(arrVal_Item) {
                    if (String.trim(arrVal_Item) == n.attributes.value) {
                        out.push(n.attributes.text);
                    }
                }, this);
            });

            

            //console.debug("OUT",out);		
            //console.debug("TreePanel",TreePanel);
        }

        if (out.length != 0) {
            return out.join(Sepperator + ' ');
        } else {
            return value;
        }

    }

});

Ext.reg('TreeSelect', Ext.ux.form.TreeCombo);
