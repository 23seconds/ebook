/*
original author: dpasichnik
modified by: Will Ferrer
date: 09/03/09
history:
09/03/09 -- posted to ExtJS forums
*/
/**
This code is based on dpasichnik's treeSerializer found at:
http://www.extjs.com/forum/showthread.php?t=20793
*/
/**
* Returns a object that represents the tree
* @param {Function} nodeFilter (optional) A function, which when passed the node, returns true or false to include
* or exclude the node.
* @param {Function} attributeFilter (optional) A function, which when passed an attribute name, and an attribute value,
* returns true or false to include or exclude the attribute.
* @param {Object} attributeMapping (optional) An associative array which can you use to remap attribute names on the nodes as they are converted to an object. Keys in the array represent attributes to be remapped, and their associated values represent the new keys that those attributes will be remapped onto in the returned object.
* @return {String}
*/
Ext.tree.TreePanel.prototype.toObject = function(nodeFilter, attributeFilter, attributeMapping) {
    return this.getRootNode().toObject(nodeFilter, attributeFilter, attributeMapping);
};

/**
* Returns an object that represents the node
* @param {Function} nodeFilter (optional) A function, which when passed the node, returns true or false to include
* or exclude the node.
* @param {Function} attributeFilter (optional) A function, which when passed an attribute name, and an attribute value,
* returns true or false to include or exclude the attribute.
* @param {Object} attributeMapping (optional) An associative array which can you use to remap attribute names on the nodes as they are converted to an object. Keys in the array represent attributes to be remapped, and their associated values represent the new keys that those attributes will be remapped onto in the returned object.
* @return {String}
*/

Ext.tree.TreeNode.prototype.toObject = function(nodeFilter, attributeFilter, attributeMapping) {
    //    Exclude nodes based on caller-supplied filtering function
    if (nodeFilter && (nodeFilter(this) == false)) {
        return {};
    }
    var c = false, returnObj = {};

    //    Add the id attribute unless the attribute filter rejects it.
    if (!attributeFilter || attributeFilter("id", this.id)) {
        returnObj.id = this.id;
        c = true;
    }

    //    Add all user-added attributes unless rejected by the attributeFilter.
    for (var key in this.attributes) {
        if ((key != 'id') && (!attributeFilter || attributeFilter(key, this.attributes[key]))) {
            if (attributeMapping && attributeMapping[key]) {
                thisKey = attributeMapping[key];
            } else {
                thisKey = key;
            }
            returnObj[thisKey] = this.attributes[key];
            c = true;
        }
    }
    //    Add child nodes if any
    var children = this.childNodes;
    var clen = children.length;
    if (clen != 0) {
        returnObj['children'] = [];
        for (var i = 0; i < clen; i++) {
            returnObj['children'][i] = children[i].toObject(nodeFilter, attributeFilter, attributeMapping);
        }
    }
    return returnObj;
}