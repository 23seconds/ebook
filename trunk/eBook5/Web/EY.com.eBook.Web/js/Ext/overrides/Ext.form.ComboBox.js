Ext.override(Ext.Component, {
    findParentByType: function(xtype, shallow) {
        return this.findParentBy(function(c) {
            return c.isXType(xtype, shallow);
        });
    }
});


Ext.override(Ext.form.ComboBox, {
    nullable: false
    , initComponent: Ext.form.ComboBox.prototype.initComponent.createSequence(function() {
        if (this.nullable) {
            //                TODO => ADD alert client/supplier from worksheet            
            //            if (this.addable && this.onAddClick) {
            //                this.triggerConfig = {
            //                    tag: 'span', cls: 'x-form-triple-triggers', cn: [
            //                        { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger ' },
            //                        { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger x-form-clear-trigger' },
            //                        { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger x-form-add-trigger' }
            //                    ]
            //                };
            //            } else {
            this.triggerConfig = {
                tag: 'span', cls: 'x-form-twin-triggers', cn: [
                        { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger ' },
                        { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger x-form-clear-trigger' }
                    ]
            };
            //}
        }

        this.addEvents(
            'clear',
            'change'
        );
        this.on('show', function() {
            if (this.trigger_clear && this.nullable) this.trigger_clear.show();
            this.fireEvent('change', this);
        });
    })
    , setActiveRecord: function(dta) {
        if (dta == null || dta == '') {
            this.clearValue();
        } else {

            var r = this.findRecord(this.valueField || this.displayField, dta[this.valueField || this.displayField]);
            if (!r) {
                this.standardValue = new this.store.recordType(dta);
                this.store.add(this.standardValue);
            } else {
                this.standardValue = r;
            }
            this.setValue(dta[this.valueField || this.displayField]);
        }

    }
    , getRecord: function() {
        var v = this.getValue();
        var r = this.findRecord(this.valueField || this.displayField, v);
        if (!r) return this.standardValue;
        return r;
    }
    , setDisplayValue: function(dValue) {
        if (this.el) {
            this.el.dom.value = dValue;
            return;
        }
        this.dValue = dValue;
    }
    , isDirty: function() {
        if (this.disabled || !this.rendered) {
            return false;
        }
        // if (this.standardValue) return this.getRecord() !== this.standardValue;
        return this.getValue() !== this.originalValue;

    }
    , getDisplayValue: function() {
        if (this.el) {
            return this.el.dom.value;
        }
        return this.dValue;
    }
    , reset: function() {
        this.clearValue();
    }
    , clearValueClicked: function() {
        this.clearValue();
        this.standardValue = null;
    }
    , clearValue: Ext.form.ComboBox.prototype.clearValue.createSequence(function() {
        if (this.trigger_clear) {
            //this.trigger_clear.hide();
        }
        this.fireEvent('clear', this);
        this.fireEvent('change', this);
        this.fireEvent('select', this);
        this.value = null;
        this.setRawValue('');
        if (this.el && this.el.dom) this.el.dom.value = '';
        if (this.hiddenField) {
            this.hiddenField.value = '';
        }

        this.dValue = '';
    })
    , onRender: Ext.form.ComboBox.prototype.onRender.createSequence(function(ct, position) {
        var self = this;
        var triggers = this.trigger.select('.x-form-trigger', true).elements;
        for (var i; i < triggers.length; i++) {
            triggers[i].addClassOnOver('x-form-trigger-over');
            triggers[i].addClassOnOver('x-form-trigger-over');
        }
        if (this.nullable) {
            this.trigger_clear = triggers[1];
            this.trigger_clear.hide = function() {
                var w = self.wrap.getWidth();
                this.dom.style.display = 'none';
                self.el.setWidth(w - self.trigger.getWidth());
            };
            this.trigger_clear.show = function() {
                var w = self.wrap.getWidth();
                this.dom.style.display = '';
                self.el.setWidth(w - self.trigger.getWidth());
            };
            this.mon(this.trigger_clear, 'click', this.clearValueClicked, this, { stopPropagation: true });
        }

        //if (!this.getValue())
        //this.trigger_clear.hide();
    })
    , onDestroy: Ext.form.ComboBox.prototype.onDestroy.createInterceptor(function() {
        Ext.destroy(this.triggers);
        return [];
    })
    , getParams: function(q) {
        var p = {};

        if (this.pageSize) {
            var parnames = this.pageTb.getParams();
            p[parnames['start']] = 0;
            p[parnames['limit']] = this.pageSize;
        }
        return p;
    }
    , initValue: function() {
        Ext.form.ComboBox.superclass.initValue.call(this);
        if (this.hiddenField) {
            this.hiddenField.value =
                Ext.value(Ext.isDefined(this.hiddenValue) ? this.hiddenValue : this.value, '');
        }
        if (this.el && this.dValue != null) this.el.dom.value = this.dValue;
    }
    , isVisible: function(deep) {
        return this.rendered && this.getVisibilityEl().isVisible(deep);
    }
});