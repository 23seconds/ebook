Ext.Element.prototype.getClasses = function() {
    return this.dom.className.replace(/^\s+|\s+$/g, '').split(/\s+/);
    };
Ext.Element.prototype.removeClassLike= function(like) {
        var cls = this.getClasses();
        for (var i = 0; i < cls.length; i++) {
            if (cls[i].indexOf(like) == 0) this.removeClass(cls[i]);
        }
    };

Ext.Element.prototype.setAttributeNS=function(ns,name,value) {
    this.dom.setAttributeNS(ns,name,value);
};

Ext.Element.prototype.setQtip = function(msg) {
this.dom.setAttribute("qtip", msg);
};

Ext.override(Ext.XTemplate, {
    exists: function(o, name) {
        return o.hasOwnProperty(name);
    }
});