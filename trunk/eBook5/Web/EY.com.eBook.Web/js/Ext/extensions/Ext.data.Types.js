
Ext.data.Types.WCFDATE = {
    convert: function(v) {
        if (v == null) return v;
        if (Ext.isDate(v)) return v;
        v = v.replace("Date", "new Date");
        v = v.replace(/\//g, "");
        v = eval(v);
        return Ext.data.Types.DATE.convert(v);
    },
    sortType: Ext.data.SortTypes.asDate,
    type: 'date'
};

Ext.util.JSON.encodeDate = function(d) {
    if (d == null) return null;
    var v = d.format('M$');
    var offs = d.getGMTOffset();
    v = v.replace(")", offs+")");
    v = '"' + v.replace(/\\/gi, "") + '"';
    return v;
};

Ext.override(Ext.form.Field, {
    getErrorCt :function (){
        return this.el.findParent('.x-form-element', 5, true) || 
            this.el.findParent('.x-form-field-wrap', 5, true) || 
            this.el.findParent('.biztax-field-wrapper', 5, true);   
    } 
    });


    Ext.form.MessageTargets.sidebiztax = {
        mark: function (field, msg) {
            field.el.addClass(field.invalidClass);
            field.el.addClass("fld-biztax-error");
            if (!field.errorIcon) {
                var elp = field.getErrorCt();
                // field has no container el
                if (!elp) {
                    field.el.dom.title = msg;
                    return;
                }
                field.errorIcon = elp.createChild({ cls: 'x-form-invalid-icon' });
              //  field.errorIcon.on('click', function () { }, field);
                if (field.ownerCt) {
                    field.ownerCt.on('afterlayout', field.alignErrorIcon, field);
                    field.ownerCt.on('expand', field.alignErrorIcon, field);
                }
                field.on('resize', field.alignErrorIcon, field);
                field.on('destroy', function () {
                    Ext.destroy(this.errorIcon);
                }, field);
            }
            field.alignErrorIcon = function () {
                field.errorIcon.alignTo(field.el, 'tl-tl', [-10, 0]);
            };
            field.alignErrorIcon();
            
            field.errorIcon.dom.qtip = msg;
            field.errorIcon.dom.qclass = 'x-form-invalid-tip';
            field.errorIcon.show();

            var tr = field.el.findParent('tr', 5, true);
            if (tr) tr.setStyle('color', 'rgba(255,0,0,1)');
        },
        clear: function (field) {
            field.el.removeClass(field.invalidClass);
            field.el.removeClass("fld-biztax-error");
            if (field.errorIcon) {
                field.errorIcon.dom.qtip = '';
                field.errorIcon.hide();
            } else {
                field.el.dom.title = '';
            }
            var tr = field.el.findParent('tr', 5, true);
            if (tr) tr.setStyle('color', 'rgba(0,0,0,1)');
        }
    };