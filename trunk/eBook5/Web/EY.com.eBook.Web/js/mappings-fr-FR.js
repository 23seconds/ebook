
    
    eBook.Accounts.Mappings.Menus = {
        Erelonen : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-Erelonen'
        ,items:[
         {
         text:'Nature'
          ,meta:'aard'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Avantages de toute nature'
          ,meta: 'aard'
          ,key:'Erelonen'
          ,mapId:'4b6fea84-dae6-4d50-b666-080a8085dd35'
          ,listObj: {
              id:'4b6fea84-dae6-4d50-b666-080a8085dd35'
              ,nl:'Voordelen van alle aard'
              ,fr:'Avantages de toute nature'
              ,en:'Voordelen van alle aard'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Commissions, courtages, ristournes commerciales, etc.'
          ,meta: 'aard'
          ,key:'Erelonen'
          ,mapId:'eb417ec7-1207-497e-a8b0-cb9af71a7bce'
          ,listObj: {
              id:'eb417ec7-1207-497e-a8b0-cb9af71a7bce'
              ,nl:'Commissies, makelaarslonen, handelsrestornos, enz.'
              ,fr:'Commissions, courtages, ristournes commerciales, etc.'
              ,en:'Commissies, makelaarslonen, handelsrestornos, enz.'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais exposés pour compte du bénéficiaire'
          ,meta: 'aard'
          ,key:'Erelonen'
          ,mapId:'60551e91-59df-4b70-b5f2-efc98b15416b'
          ,listObj: {
              id:'60551e91-59df-4b70-b5f2-efc98b15416b'
              ,nl:'Kosten gedaan voor rekening van de verkrijger'
              ,fr:'Frais exposés pour compte du bénéficiaire'
              ,en:'Kosten gedaan voor rekening van de verkrijger'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Honoraires ou vacations'
          ,meta: 'aard'
          ,key:'Erelonen'
          ,mapId:'200adb0a-b150-46e9-b572-96f21ba431bd'
          ,listObj: {
              id:'200adb0a-b150-46e9-b572-96f21ba431bd'
              ,nl:'Erelonen of vacatiegelden'
              ,fr:'Honoraires ou vacations'
              ,en:'Erelonen of vacatiegelden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  
        ]
    })
  ,VerworpenUitgaven : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-VerworpenUitgaven'
        ,items:[
          {text:'Amendes, pénalités et confiscations de toute nature'
          ,meta: 'boete'
          ,key:'VerworpenUitgaven'
          ,mapId:'7c5a87d8-61de-4550-95cb-fbe6dec24514'
          ,listObj: {
              id:'7c5a87d8-61de-4550-95cb-fbe6dec24514'
              ,nl:'Geldboeten, verbeurdverklaringen en straffen van alle aard'
              ,fr:'Amendes, pénalités et confiscations de toute nature'
              ,en:'Fines'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Autres'
          ,meta: 'andere'
          ,key:'VerworpenUitgaven'
          ,mapId:'b7aadf3f-5631-4418-a7b3-f77f7e5b9deb'
          ,listObj: {
              id:'b7aadf3f-5631-4418-a7b3-f77f7e5b9deb'
              ,nl:'Andere'
              ,fr:'Autres'
              ,en:'other'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Avantages anormaux ou bénévoles'
          ,meta: 'agv'
          ,key:'VerworpenUitgaven'
          ,mapId:'62afa7e6-e973-4185-9940-cb4cfe1fe12c'
          ,listObj: {
              id:'62afa7e6-e973-4185-9940-cb4cfe1fe12c'
              ,nl:'Abnormale of goedgunstige voordelen'
              ,fr:'Avantages anormaux ou bénévoles'
              ,en:'Granted abnormal or benevolent advantages'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Avantages de titres-repas, chèques sport/culture ou éco-chèques'
          ,meta: 'maaltijdsportcultuureco'
          ,key:'VerworpenUitgaven'
          ,mapId:'bc27c4a1-3ffb-470d-b21c-56f34231548b'
          ,listObj: {
              id:'bc27c4a1-3ffb-470d-b21c-56f34231548b'
              ,nl:'Maaltijd-, sport-, cultuur- en ecocheques'
              ,fr:'Avantages de titres-repas, chèques sport/culture ou éco-chèques'
              ,en:'Maaltijd-, sport-, cultuur- en ecocheques'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Bénéfices sociale'
          ,meta: 'voordeel'
          ,key:'VerworpenUitgaven'
          ,mapId:'3d98ba0d-907a-4f1d-a7d0-caa280420baf'
          ,listObj: {
              id:'3d98ba0d-907a-4f1d-a7d0-caa280420baf'
              ,nl:'Sociale voordelen'
              ,fr:'Bénéfices sociale'
              ,en:'Social advantages'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Cout tax shelter  DESC'
          ,meta: 'taxshelter'
          ,key:'VerworpenUitgaven'
          ,mapId:'a86fc507-fc68-44d0-b4fe-f7f95d1464e4'
          ,listObj: {
              id:'a86fc507-fc68-44d0-b4fe-f7f95d1464e4'
              ,nl:'Kosten Tax Shelter'
              ,fr:'Cout tax shelter  DESC'
              ,en:'Kosten Tax Shelter'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Donation accepté fiscal / Liberalité'
          ,meta: 'fingift'
          ,key:'VerworpenUitgaven'
          ,mapId:'87216df4-5ccd-469e-8bc9-ad067fa2ef8a'
          ,listObj: {
              id:'87216df4-5ccd-469e-8bc9-ad067fa2ef8a'
              ,nl:'Fiscaal aanvaarde gift / Liberaliteit'
              ,fr:'Donation accepté fiscal / Liberalité'
              ,en:'Gift -  deductable'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Donation non accepté fiscal / Liberalité'
          ,meta: 'gift'
          ,key:'VerworpenUitgaven'
          ,mapId:'5a6f6c1c-ddbf-4930-8fd1-29ba567cf69a'
          ,listObj: {
              id:'5a6f6c1c-ddbf-4930-8fd1-29ba567cf69a'
              ,nl:'Fiscaal niet aanvaarde gift / Liberaliteit'
              ,fr:'Donation non accepté fiscal / Liberalité'
              ,en:'Gift -  non deductable'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
         {
         text:'Frais dautomobile'
          ,meta:'auto'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Frais de voiture non déductible  (50%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'5820cf02-cef5-4b96-a350-fd2542c291cf'
          ,listObj: {
              id:'5820cf02-cef5-4b96-a350-fd2542c291cf'
              ,nl:'Niet aftrekbare autokosten (50%)'
              ,fr:'Frais de voiture non déductible  (50%)'
              ,en:'Non deductable car costs  (50%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (10% de 70%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'2168e25f-286e-4955-ace0-cf62124105a2'
          ,listObj: {
              id:'2168e25f-286e-4955-ace0-cf62124105a2'
              ,nl:'Niet aftrekbare autokosten (10% van 70%)'
              ,fr:'Frais de voiture non déductible (10% de 70%)'
              ,en:'Niet aftrekbare autokosten (10% van 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (10%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'37743faf-f687-4176-a16a-683fa4ca1124'
          ,listObj: {
              id:'37743faf-f687-4176-a16a-683fa4ca1124'
              ,nl:'Niet aftrekbare autokosten (10%)'
              ,fr:'Frais de voiture non déductible (10%)'
              ,en:'Non deductable car costs  (10%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (20% de 70%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'d63d18b5-bdb8-4d0b-a796-4fa76ca854ec'
          ,listObj: {
              id:'d63d18b5-bdb8-4d0b-a796-4fa76ca854ec'
              ,nl:'Niet aftrekbare autokosten (20% van 70%)'
              ,fr:'Frais de voiture non déductible (20% de 70%)'
              ,en:'Niet aftrekbare autokosten (20% van 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (20%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'842d79fd-0e34-445e-8cfb-95f5f41f4aef'
          ,listObj: {
              id:'842d79fd-0e34-445e-8cfb-95f5f41f4aef'
              ,nl:'Niet aftrekbare autokosten (20%)'
              ,fr:'Frais de voiture non déductible (20%)'
              ,en:'Non deductable car costs  (20%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (25% de 60%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'b8e033e1-f19c-4f9e-8010-99b99874a9f8'
          ,listObj: {
              id:'b8e033e1-f19c-4f9e-8010-99b99874a9f8'
              ,nl:'Niet aftrekbare autokosten (25% van 60%)'
              ,fr:'Frais de voiture non déductible (25% de 60%)'
              ,en:'Non deductable car costs '
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (25% de 70%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'2bc7d196-714e-47da-b427-45fb49373e3c'
          ,listObj: {
              id:'2bc7d196-714e-47da-b427-45fb49373e3c'
              ,nl:'Niet aftrekbare autokosten (25% van 70%)'
              ,fr:'Frais de voiture non déductible (25% de 70%)'
              ,en:'Non deductable car costs  (25% van 60%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (25% de 75%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'5d3272a6-4473-4d57-95b0-63d8a715d973'
          ,listObj: {
              id:'5d3272a6-4473-4d57-95b0-63d8a715d973'
              ,nl:'Niet aftrekbare autokosten (25% van 75%)'
              ,fr:'Frais de voiture non déductible (25% de 75%)'
              ,en:'Non deductable car costs  (25% van 75%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (25% de 90%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'164303dc-c36b-4033-b2e4-b492e0139e1c'
          ,listObj: {
              id:'164303dc-c36b-4033-b2e4-b492e0139e1c'
              ,nl:'Niet aftrekbare autokosten (25% van 90%)'
              ,fr:'Frais de voiture non déductible (25% de 90%)'
              ,en:'Non deductable car costs  (25% van 90%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (25%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'8c06d1ea-43cd-4b01-a591-21a2f9d20318'
          ,listObj: {
              id:'8c06d1ea-43cd-4b01-a591-21a2f9d20318'
              ,nl:'Niet aftrekbare autokosten (25%)'
              ,fr:'Frais de voiture non déductible (25%)'
              ,en:'Non deductable car costs  (25%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (30% de 70%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'a945a5fd-130e-4264-b1a0-8419a9721a5f'
          ,listObj: {
              id:'a945a5fd-130e-4264-b1a0-8419a9721a5f'
              ,nl:'Niet aftrekbare autokosten (30% van 70%)'
              ,fr:'Frais de voiture non déductible (30% de 70%)'
              ,en:'Niet aftrekbare autokosten (30% van 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (30%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'69ed6f97-6c87-46c7-89bc-4d934e3fe54a'
          ,listObj: {
              id:'69ed6f97-6c87-46c7-89bc-4d934e3fe54a'
              ,nl:'Niet aftrekbare autokosten (30%)'
              ,fr:'Frais de voiture non déductible (30%)'
              ,en:'Non deductable car costs  (30%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (40% de 70%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'6a616fdc-76d3-476a-8ee4-ab40fb9a1523'
          ,listObj: {
              id:'6a616fdc-76d3-476a-8ee4-ab40fb9a1523'
              ,nl:'Niet aftrekbare autokosten (40% van 70%)'
              ,fr:'Frais de voiture non déductible (40% de 70%)'
              ,en:'Niet aftrekbare autokosten (40% van 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (40%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'041f575f-e3ff-40c8-9f08-3356b923cad5'
          ,listObj: {
              id:'041f575f-e3ff-40c8-9f08-3356b923cad5'
              ,nl:'Niet aftrekbare autokosten (40%)'
              ,fr:'Frais de voiture non déductible (40%)'
              ,en:'Non deductable car costs  (40%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (50% de 70%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'cf60b185-08a7-4ac5-b17b-ae38ec2af1a0'
          ,listObj: {
              id:'cf60b185-08a7-4ac5-b17b-ae38ec2af1a0'
              ,nl:'Niet aftrekbare autokosten (50 van 70%)'
              ,fr:'Frais de voiture non déductible (50% de 70%)'
              ,en:'Non deductable car costs  (50% of 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible 25% de 80%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'d958084a-dd72-4f1c-90f3-430c418da23e'
          ,listObj: {
              id:'d958084a-dd72-4f1c-90f3-430c418da23e'
              ,nl:'Niet aftrekbare autokosten (25% van 80%)'
              ,fr:'Frais de voiture non déductible 25% de 80%)'
              ,en:'Non deductable car costs  (25% van 80%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Non déductible frais dessence (25%)'
          ,meta: 'auto'
          ,key:'VerworpenUitgaven'
          ,mapId:'8cf1942c-7bc2-40ec-8b93-82967a44d5e5'
          ,listObj: {
              id:'8cf1942c-7bc2-40ec-8b93-82967a44d5e5'
              ,nl:'Niet aftrekbare brandstofkosten (25%)'
              ,fr:'Non déductible frais dessence (25%)'
              ,en:'Non deductable fuel costs  (25%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Frais dautomobile y compris section du carburant (30%)'
          ,meta:'auto70'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Frais de voiture non déductible (10% de 70%)'
          ,meta: 'auto70'
          ,key:'VerworpenUitgaven'
          ,mapId:'c581b67c-2fa1-4751-932a-96056e42d19d'
          ,listObj: {
              id:'c581b67c-2fa1-4751-932a-96056e42d19d'
              ,nl:'Niet aftrekbare autokosten (10% van 70%)'
              ,fr:'Frais de voiture non déductible (10% de 70%)'
              ,en:'Niet aftrekbare autokosten (10% van 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (20% de 70%)'
          ,meta: 'auto70'
          ,key:'VerworpenUitgaven'
          ,mapId:'fb7a8eab-c1eb-4068-9182-ff6d22acaa4c'
          ,listObj: {
              id:'fb7a8eab-c1eb-4068-9182-ff6d22acaa4c'
              ,nl:'Niet aftrekbare autokosten (20% van 70%)'
              ,fr:'Frais de voiture non déductible (20% de 70%)'
              ,en:'Niet aftrekbare autokosten (20% van 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (25% de 70%)'
          ,meta: 'auto70'
          ,key:'VerworpenUitgaven'
          ,mapId:'5e70313e-fb08-46f1-b76c-7196ac8b3d0e'
          ,listObj: {
              id:'5e70313e-fb08-46f1-b76c-7196ac8b3d0e'
              ,nl:'Niet aftrekbare autokosten (25% van 70%)'
              ,fr:'Frais de voiture non déductible (25% de 70%)'
              ,en:'Non deductable car costs  (25% van 60%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (30% de 70%)'
          ,meta: 'auto70'
          ,key:'VerworpenUitgaven'
          ,mapId:'1ba344e6-2d4b-41bf-8188-4f4881330e29'
          ,listObj: {
              id:'1ba344e6-2d4b-41bf-8188-4f4881330e29'
              ,nl:'Niet aftrekbare autokosten (30% van 70%)'
              ,fr:'Frais de voiture non déductible (30% de 70%)'
              ,en:'Niet aftrekbare autokosten (30% van 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (40% de 70%)'
          ,meta: 'auto70'
          ,key:'VerworpenUitgaven'
          ,mapId:'fcfafd32-9775-4b3b-81f4-9b0868a0ad24'
          ,listObj: {
              id:'fcfafd32-9775-4b3b-81f4-9b0868a0ad24'
              ,nl:'Niet aftrekbare autokosten (40% van 70%)'
              ,fr:'Frais de voiture non déductible (40% de 70%)'
              ,en:'Niet aftrekbare autokosten (40% van 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture non déductible (50% de 70%)'
          ,meta: 'auto70'
          ,key:'VerworpenUitgaven'
          ,mapId:'8052c5fb-dd87-46ac-b0dc-fe9a7acbcc27'
          ,listObj: {
              id:'8052c5fb-dd87-46ac-b0dc-fe9a7acbcc27'
              ,nl:'Niet aftrekbare autokosten (50% van 70%)'
              ,fr:'Frais de voiture non déductible (50% de 70%)'
              ,en:'Non deductable car costs  (50% of 70%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
          {text:'Frais de vêtements professionnels non-spécifiques'
          ,meta: 'kledij'
          ,key:'VerworpenUitgaven'
          ,mapId:'68b9c809-2c7b-4f49-83b7-ff0df302a9b8'
          ,listObj: {
              id:'68b9c809-2c7b-4f49-83b7-ff0df302a9b8'
              ,nl:'Kosten voor niet-specifieke beroepskledij (100%)'
              ,fr:'Frais de vêtements professionnels non-spécifiques'
              ,en:'Costs for non specific professional dress'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais de voiture parti Avantages et libéralités (17%)'
          ,meta: 'autoVAA'
          ,key:'VerworpenUitgaven'
          ,mapId:'c8b41c86-f115-4c65-b0fc-0e6413b5f6fe'
          ,listObj: {
              id:'c8b41c86-f115-4c65-b0fc-0e6413b5f6fe'
              ,nl:'Autokosten deel VAA (17%)'
              ,fr:'Frais de voiture parti Avantages et libéralités (17%)'
              ,en:'Car part advantages (17%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Indémnité pour coupon manquant'
          ,meta: 'coupon'
          ,key:'VerworpenUitgaven'
          ,mapId:'6cf63c4b-3df9-403b-9431-a10161fdba10'
          ,listObj: {
              id:'6cf63c4b-3df9-403b-9431-a10161fdba10'
              ,nl:'Vergoeding ontbrekende coupon'
              ,fr:'Indémnité pour coupon manquant'
              ,en:'Vergoeding ontbrekende coupon'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Intérêts éxagérés'
          ,meta: 'intresten'
          ,key:'VerworpenUitgaven'
          ,mapId:'6b500962-b80c-46c5-b615-66c58529c31e'
          ,listObj: {
              id:'6b500962-b80c-46c5-b615-66c58529c31e'
              ,nl:'Overdreven interesten'
              ,fr:'Intérêts éxagérés'
              ,en:'Excess interest'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Intérêts relatifs à une partie de certains emprunts'
          ,meta: 'intrestlening'
          ,key:'VerworpenUitgaven'
          ,mapId:'473e73e8-0578-4505-9e02-90568b2da589'
          ,listObj: {
              id:'473e73e8-0578-4505-9e02-90568b2da589'
              ,nl:'Interesten met betrekking tot een gedeelte van bepaalde leningen'
              ,fr:'Intérêts relatifs à une partie de certains emprunts'
              ,en:'Interest relating to a portion of certain loans'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Les impôts, taxes et rétributions régionaux '
          ,meta: 'belasting'
          ,key:'VerworpenUitgaven'
          ,mapId:'31fb1d70-6c6e-40e9-a9a8-ca919e72e7a6'
          ,listObj: {
              id:'31fb1d70-6c6e-40e9-a9a8-ca919e72e7a6'
              ,nl:'Gewestelijke belastingen, heffingen en retributies (100%)'
              ,fr:'Les impôts, taxes et rétributions régionaux '
              ,en:'Local taxes'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Non déductible frais de réception et de cadeaux (50%)'
          ,meta: 'receptie'
          ,key:'VerworpenUitgaven'
          ,mapId:'a25e863f-ec46-424d-a3e2-572b6abf1c89'
          ,listObj: {
              id:'a25e863f-ec46-424d-a3e2-572b6abf1c89'
              ,nl:'Niet aftrekbare receptiekosten en relatiegeschenken (50%)'
              ,fr:'Non déductible frais de réception et de cadeaux (50%)'
              ,en:'Non deductable restaurant costs and business giftes (50%)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Non déductible frais de restaurant (31%)'
          ,meta: 'restaurant'
          ,key:'VerworpenUitgaven'
          ,mapId:'e7c1c17d-9ecb-4d77-93b6-5a4e2ac75131'
          ,listObj: {
              id:'e7c1c17d-9ecb-4d77-93b6-5a4e2ac75131'
              ,nl:'Niet aftrekbare restaurantkosten (31%)'
              ,fr:'Non déductible frais de restaurant (31%)'
              ,en:'Non deductable restaurant costs (37,5)'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
      }



          ,
          { text: 'Dépenses ou avantages de toute nature non justifiés'
          , meta: 'nietverantwoord'
          , key: 'VerworpenUitgaven'
          , mapId: 'F50FC02C-3E78-4E99-98F6-F955BF7C4F46'
          , listObj: {
              id: 'e7c1c17d-9ecb-4d77-93b6-5a4e2ac75131'
              , nl: 'Niet-verantwoorde kosten kosten of voordelen van alle aard'
              , fr: 'Dépenses ou avantages de toute nature non justifiés'
              , en: 'Non justified costs or benefits in kind'
          }
          , handler: eBook.Accounts.Mappings.handleClick
          , scope: this
          }
  ,
          {text:'Non déductible impôts'
          ,meta: 'nbelasting'
          ,key:'VerworpenUitgaven'
          ,mapId:'6ba6a64a-0415-405d-a0b3-1930e28b9bbe'
          ,listObj: {
              id:'6ba6a64a-0415-405d-a0b3-1930e28b9bbe'
              ,nl:'Niet aftrekbare belastingen'
              ,fr:'Non déductible impôts'
              ,en:'Non decuctable taxes'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Non déductible pensions et contribution de pension'
          ,meta: 'pensioenen'
          ,key:'VerworpenUitgaven'
          ,mapId:'89baa6b1-92c5-4902-8236-13bfa3813d34'
          ,listObj: {
              id:'89baa6b1-92c5-4902-8236-13bfa3813d34'
              ,nl:'Niet aftrekbare pensioenen en pensioenbijdragen'
              ,fr:'Non déductible pensions et contribution de pension'
              ,en:'Non deductable pensions and pensionscontributions'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Paiements non déductibles vers certains Etats'
          ,meta: 'bepaaldestaten'
          ,key:'VerworpenUitgaven'
          ,mapId:'8a567601-f2d5-4469-b160-03867e2f0f76'
          ,listObj: {
              id:'8a567601-f2d5-4469-b160-03867e2f0f76'
              ,nl:'Niet aftrekbare betalingen naar bepaalde staten'
              ,fr:'Paiements non déductibles vers certains Etats'
              ,en:'Niet aftrekbare betalingen naar bepaalde staten'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Participation des travailleurs'
          ,meta: 'werknemersparticipatie'
          ,key:'VerworpenUitgaven'
          ,mapId:'cafcb0f9-95cc-491e-a51e-5c5c8e7f740f'
          ,listObj: {
              id:'cafcb0f9-95cc-491e-a51e-5c5c8e7f740f'
              ,nl:'Werknemersparticipatie'
              ,fr:'Participation des travailleurs'
              ,en:'Employee participation in capital'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Primes, subsides en capital et en intérêt régionaux'
          ,meta: 'gewestpremies'
          ,key:'VerworpenUitgaven'
          ,mapId:'6932dd3f-2e1f-4332-90b0-a7e6f1739907'
          ,listObj: {
              id:'6932dd3f-2e1f-4332-90b0-a7e6f1739907'
              ,nl:'Gewestelijke premies en kapitaal- en interestsubsidies'
              ,fr:'Primes, subsides en capital et en intérêt régionaux'
              ,en:'Regional premiums, capital and interest subsidies'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Réductions de valeur et moins-values sur actions ou parts'
          ,meta: 'aandelen'
          ,key:'VerworpenUitgaven'
          ,mapId:'d92229e3-ed8f-41fb-a714-80f5d3f54a1f'
          ,listObj: {
              id:'d92229e3-ed8f-41fb-a714-80f5d3f54a1f'
              ,nl:'Waardeverminderingen en minderwaarden op aandelen'
              ,fr:'Réductions de valeur et moins-values sur actions ou parts'
              ,en:'Waardeverminderingen en minderwaarden op aandelen'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Reprises dexonérations antérieures'
          ,meta: 'vrijstellingen'
          ,key:'VerworpenUitgaven'
          ,mapId:'20986c04-c081-4198-85eb-c37148e53b5f'
          ,listObj: {
              id:'20986c04-c081-4198-85eb-c37148e53b5f'
              ,nl:'Terugnemingen van vroegere vrijstellingen'
              ,fr:'Reprises dexonérations antérieures'
              ,en:'Reversals of previous tax exemptions'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
        ]
    })
  ,Personeel : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-Personeel'
        ,items:[
         {
         text:'Avantages'
          ,meta:'vaa'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de résultats avantages - employé'
          ,meta: 'vaa'
          ,key:'Personeel'
          ,mapId:'8e9de920-ad97-4edd-91b0-0cb35ed67602'
          ,listObj: {
              id:'8e9de920-ad97-4edd-91b0-0cb35ed67602'
              ,nl:'Resultatenrekening VAA Bedienden'
              ,fr:'Compte de résultats avantages - employé'
              ,en:'Resultatenrekening VAA Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de résultats avantages - ouvrier'
          ,meta: 'vaa'
          ,key:'Personeel'
          ,mapId:'b9e4820c-0a1a-4284-9f2e-35064255294c'
          ,listObj: {
              id:'b9e4820c-0a1a-4284-9f2e-35064255294c'
              ,nl:'Resultatenrekening VAA Arbeiders'
              ,fr:'Compte de résultats avantages - ouvrier'
              ,en:'Resultatenrekening VAA Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Brut'
          ,meta:'bruto'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte des résultats brut employés'
          ,meta: 'bruto'
          ,key:'Personeel'
          ,mapId:'f8cd8522-c3b1-4867-b89d-efad88942092'
          ,listObj: {
              id:'f8cd8522-c3b1-4867-b89d-efad88942092'
              ,nl:'Resultatenrekening bruto bedienden'
              ,fr:'Compte des résultats brut employés'
              ,en:'Resultatenrekening bruto bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte des résultats brut ouvriers'
          ,meta: 'bruto'
          ,key:'Personeel'
          ,mapId:'00cf8a7c-7c0b-4286-9be7-ff74aaa5112f'
          ,listObj: {
              id:'00cf8a7c-7c0b-4286-9be7-ff74aaa5112f'
              ,nl:'Resultatenrekening bruto arbeiders'
              ,fr:'Compte des résultats brut ouvriers'
              ,en:'Resultatenrekening bruto arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Chèques-repas'
          ,meta:'maaltijdcheques'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de résultats - chèques-repas - ouvrier'
          ,meta: 'maaltijdcheques'
          ,key:'Personeel'
          ,mapId:'2f1c1df0-7b05-439b-845d-f036ab765eec'
          ,listObj: {
              id:'2f1c1df0-7b05-439b-845d-f036ab765eec'
              ,nl:'Resultatenrekening Maaltijdcheques Arbeiders'
              ,fr:'Compte de résultats - chèques-repas - ouvrier'
              ,en:'Resultatenrekening Maaltijdcheques Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de résultats Chèque-repas - employé'
          ,meta: 'maaltijdcheques'
          ,key:'Personeel'
          ,mapId:'4f5e8ffb-031c-476b-a154-0efc048508dd'
          ,listObj: {
              id:'4f5e8ffb-031c-476b-a154-0efc048508dd'
              ,nl:'Resultatenrekening Maaltijdcheques Bedienden'
              ,fr:'Compte de résultats Chèque-repas - employé'
              ,en:'Resultatenrekening Maaltijdcheques Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
          {text:'Compte de résultats - dirigeant dentreprise'
          ,meta: 'bedrijfsleider'
          ,key:'Personeel'
          ,mapId:'2978d3e2-2189-44b8-b369-55e0bf881c99'
          ,listObj: {
              id:'2978d3e2-2189-44b8-b369-55e0bf881c99'
              ,nl:'Resultatenrekening Bedrijfsleider'
              ,fr:'Compte de résultats - dirigeant dentreprise'
              ,en:'Resultatenrekening Bedrijfsleider'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
         {
         text:'Cotisations de lemployeur'
          ,meta:'wg_bijdragen'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de résultats  - Cotisation patronale - employé'
          ,meta: 'wg_bijdragen'
          ,key:'Personeel'
          ,mapId:'0331ed91-102f-4ab6-a889-8b3cb4d37c11'
          ,listObj: {
              id:'0331ed91-102f-4ab6-a889-8b3cb4d37c11'
              ,nl:'Resultatenrekening Werkgeversbijdragen Bedienden'
              ,fr:'Compte de résultats  - Cotisation patronale - employé'
              ,en:'Resultatenrekening Werkgeversbijdragen Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de résultats - Cotisation patronale - ouvrier'
          ,meta: 'wg_bijdragen'
          ,key:'Personeel'
          ,mapId:'cdc8b4da-52ae-40cc-a445-ee77b73c3b82'
          ,listObj: {
              id:'cdc8b4da-52ae-40cc-a445-ee77b73c3b82'
              ,nl:'Resultatenrekening Werkgeversbijdragen Arbeiders'
              ,fr:'Compte de résultats - Cotisation patronale - ouvrier'
              ,en:'Resultatenrekening Werkgeversbijdragen Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Frais'
          ,meta:'kosten'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de résultats - frais demployeur - employé'
          ,meta: 'kosten'
          ,key:'Personeel'
          ,mapId:'c1f79aee-2e17-4879-893d-59da44d5e93a'
          ,listObj: {
              id:'c1f79aee-2e17-4879-893d-59da44d5e93a'
              ,nl:'Resultatenrekening Kosten eigen aan de werkgever Bedienden'
              ,fr:'Compte de résultats - frais demployeur - employé'
              ,en:'Resultatenrekening Kosten eigen aan de werkgever Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de résultats - frais demployeur - ouvrier'
          ,meta: 'kosten'
          ,key:'Personeel'
          ,mapId:'a37af5c9-e5ed-4ed0-85e5-5f48f30001d4'
          ,listObj: {
              id:'a37af5c9-e5ed-4ed0-85e5-5f48f30001d4'
              ,nl:'Resultatenrekening Kosten eigen aan de werkgever Arbeiders'
              ,fr:'Compte de résultats - frais demployeur - ouvrier'
              ,en:'Resultatenrekening Kosten eigen aan de werkgever Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'léquilibre de vacances'
          ,meta:'vakantiegeld_balans'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de bilan de clôture - Pécule de vacances - employé'
          ,meta: 'vakantiegeld_balans'
          ,key:'Personeel'
          ,mapId:'4cc7d998-4820-4a32-9fcb-efce61cb2c89'
          ,listObj: {
              id:'4cc7d998-4820-4a32-9fcb-efce61cb2c89'
              ,nl:'Balansrekening Vakantiegeld Bedienden'
              ,fr:'Compte de bilan de clôture - Pécule de vacances - employé'
              ,en:'Balansrekening Vakantiegeld Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de bilan de clôture - Pécule de vacances - ouvrier'
          ,meta: 'vakantiegeld_balans'
          ,key:'Personeel'
          ,mapId:'24757ec4-b43b-4e93-a7f4-3bf938552403'
          ,listObj: {
              id:'24757ec4-b43b-4e93-a7f4-3bf938552403'
              ,nl:'Balansrekening Vakantiegeld Arbeiders'
              ,fr:'Compte de bilan de clôture - Pécule de vacances - ouvrier'
              ,en:'Balansrekening Vakantiegeld Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Net'
          ,meta:'netto'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de bilan de clôture - Net - employé'
          ,meta: 'netto'
          ,key:'Personeel'
          ,mapId:'a5425f72-7fcc-4999-8fe0-bb95c2b5f18e'
          ,listObj: {
              id:'a5425f72-7fcc-4999-8fe0-bb95c2b5f18e'
              ,nl:'Balansrekening Netto Bedienden'
              ,fr:'Compte de bilan de clôture - Net - employé'
              ,en:'Balansrekening Netto Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de bilan de clôture - Net - ouvrier'
          ,meta: 'netto'
          ,key:'Personeel'
          ,mapId:'59f4e092-e1e7-43a2-bda4-45c47aac8dbe'
          ,listObj: {
              id:'59f4e092-e1e7-43a2-bda4-45c47aac8dbe'
              ,nl:'Balansrekening Netto Arbeiders'
              ,fr:'Compte de bilan de clôture - Net - ouvrier'
              ,en:'Balansrekening Netto Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Résidence-effort'
          ,meta:'woon_werk'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de résultats  - Résidence-effort - ouvrier'
          ,meta: 'woon_werk'
          ,key:'Personeel'
          ,mapId:'57020c49-52ca-4a23-b01e-a15987e75dac'
          ,listObj: {
              id:'57020c49-52ca-4a23-b01e-a15987e75dac'
              ,nl:'Resultatenrekening Woon-werk Arbeiders'
              ,fr:'Compte de résultats  - Résidence-effort - ouvrier'
              ,en:'Resultatenrekening Woon-werk Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de résultats - Résidentiel-effort - employé'
          ,meta: 'woon_werk'
          ,key:'Personeel'
          ,mapId:'430f1419-5dfe-4893-8e3d-ff42a68c2d9d'
          ,listObj: {
              id:'430f1419-5dfe-4893-8e3d-ff42a68c2d9d'
              ,nl:'Resultatenrekening Woon-werk Bedienden'
              ,fr:'Compte de résultats - Résidentiel-effort - employé'
              ,en:'Resultatenrekening Woon-werk Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Retenue à la source'
          ,meta:'bv'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de bilan de clôture - retenue à  la source sur rémunération - employé'
          ,meta: 'bv'
          ,key:'Personeel'
          ,mapId:'2c278b87-47e9-4f3f-a155-d065eb2f8552'
          ,listObj: {
              id:'2c278b87-47e9-4f3f-a155-d065eb2f8552'
              ,nl:'Balansrekening BV Bedienden'
              ,fr:'Compte de bilan de clôture - retenue à  la source sur rémunération - employé'
              ,en:'Balansrekening BV Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de bilan de clôture - retenue à  la source sur rémunération - ouvrier'
          ,meta: 'bv'
          ,key:'Personeel'
          ,mapId:'08861715-ab4b-4ebe-9431-38f6784f6970'
          ,listObj: {
              id:'08861715-ab4b-4ebe-9431-38f6784f6970'
              ,nl:'Balansrekening BV Arbeiders'
              ,fr:'Compte de bilan de clôture - retenue à  la source sur rémunération - ouvrier'
              ,en:'Balansrekening BV Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'RSZ'
          ,meta:'rsz'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de bilan de clôture - RSZ - employé'
          ,meta: 'rsz'
          ,key:'Personeel'
          ,mapId:'d387dfa4-7da3-4b70-ad19-92fc7afa9981'
          ,listObj: {
              id:'d387dfa4-7da3-4b70-ad19-92fc7afa9981'
              ,nl:'Balansrekening RSZ Bedienden'
              ,fr:'Compte de bilan de clôture - RSZ - employé'
              ,en:'Balansrekening RSZ Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de bilan de clôture - RSZ - ouvrier'
          ,meta: 'rsz'
          ,key:'Personeel'
          ,mapId:'671092f6-1ca2-49a2-8709-5c527ab03bf0'
          ,listObj: {
              id:'671092f6-1ca2-49a2-8709-5c527ab03bf0'
              ,nl:'Balansrekening RSZ Arbeiders'
              ,fr:'Compte de bilan de clôture - RSZ - ouvrier'
              ,en:'Balansrekening RSZ Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Vacances - résultats'
          ,meta:'vakantiegeld_result'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Compte de résultats  Pécule de vacances - ouvrier'
          ,meta: 'vakantiegeld_result'
          ,key:'Personeel'
          ,mapId:'a389669c-92a8-4b19-bcae-2cee5bc72b83'
          ,listObj: {
              id:'a389669c-92a8-4b19-bcae-2cee5bc72b83'
              ,nl:'Resultatenrekening Vakantiegeld Arbeiders'
              ,fr:'Compte de résultats  Pécule de vacances - ouvrier'
              ,en:'Resultatenrekening Vakantiegeld Arbeiders'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Compte de résultats Pécule de vacances - employé'
          ,meta: 'vakantiegeld_result'
          ,key:'Personeel'
          ,mapId:'6f65243e-bb70-4662-9ef3-0f5ba7af3cb7'
          ,listObj: {
              id:'6f65243e-bb70-4662-9ef3-0f5ba7af3cb7'
              ,nl:'Resultatenrekening Vakantiegeld Bedienden'
              ,fr:'Compte de résultats Pécule de vacances - employé'
              ,en:'Resultatenrekening Vakantiegeld Bedienden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  
        ]
    })
  ,BTW : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-BTW'
        ,items:[
          {text:'C/C inscrit'
          ,meta: 'RC'
          ,key:'BTW'
          ,mapId:'b12e1513-d2e5-4e35-b6e5-526bef1b37e4'
          ,listObj: {
              id:'b12e1513-d2e5-4e35-b6e5-526bef1b37e4'
              ,nl:'R/C geboekt'
              ,fr:'C/C inscrit'
              ,en:'R/C geboekt'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Ventes inscrit'
          ,meta: 'Omzet'
          ,key:'BTW'
          ,mapId:'914e2289-3a7a-4a2e-9add-b26515ecdfbe'
          ,listObj: {
              id:'914e2289-3a7a-4a2e-9add-b26515ecdfbe'
              ,nl:'Omzet geboekt'
              ,fr:'Ventes inscrit'
              ,en:'Omzet geboekt'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
        ]
    })
  ,VoordelenAlleAard : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-VoordelenAlleAard'
        ,items:[
          {text:'Autre'
          ,meta: 'andere'
          ,key:'VoordelenAlleAard'
          ,mapId:'0c87df9a-d763-4958-b225-a19b3be25571'
          ,listObj: {
              id:'0c87df9a-d763-4958-b225-a19b3be25571'
              ,nl:'Andere'
              ,fr:'Autre'
              ,en:'other'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Autre services dutilité'
          ,meta: 'OverigeNuts'
          ,key:'VoordelenAlleAard'
          ,mapId:'64b192f4-a80f-4034-b78a-c4eebde2122c'
          ,listObj: {
              id:'64b192f4-a80f-4034-b78a-c4eebde2122c'
              ,nl:'Overige nutsvoorzieningen'
              ,fr:'Autre services dutilité'
              ,en:'Overige nutsvoorzieningen'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Chauffage/électricité'
          ,meta: 'utilities'
          ,key:'VoordelenAlleAard'
          ,mapId:'f833b6f6-a928-43e1-b1fb-24b02e0b0653'
          ,listObj: {
              id:'f833b6f6-a928-43e1-b1fb-24b02e0b0653'
              ,nl:'Verwarming/Elektriciteit'
              ,fr:'Chauffage/électricité'
              ,en:'Utilities'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Cotisations sociales dirigeant dentreprise'
          ,meta: 'socialebijdragen'
          ,key:'VoordelenAlleAard'
          ,mapId:'1b05fc38-4bf5-411b-8def-dda56325e6f5'
          ,listObj: {
              id:'1b05fc38-4bf5-411b-8def-dda56325e6f5'
              ,nl:'Sociale bijdragen bedrijfsleider'
              ,fr:'Cotisations sociales dirigeant dentreprise'
              ,en:'Sociale bijdragen bedrijfsleider'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Immeuble/logement'
          ,meta: 'huur'
          ,key:'VoordelenAlleAard'
          ,mapId:'57a4d21d-a142-4b6f-bddf-e4b9ffa3d3be'
          ,listObj: {
              id:'57a4d21d-a142-4b6f-bddf-e4b9ffa3d3be'
              ,nl:'Onroerend goed'
              ,fr:'Immeuble/logement'
              ,en:'Rent'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Intérêts sur c/c dirigeant dentreprise'
          ,meta: 'interestenrc'
          ,key:'VoordelenAlleAard'
          ,mapId:'ccd3da15-c36d-49b4-b74c-5580a1bdb5f0'
          ,listObj: {
              id:'ccd3da15-c36d-49b4-b74c-5580a1bdb5f0'
              ,nl:'Intresten r/c bedrijfsleider'
              ,fr:'Intérêts sur c/c dirigeant dentreprise'
              ,en:'Intresten r/c bedrijfsleider'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Pc/Internet'
          ,meta: 'pc'
          ,key:'VoordelenAlleAard'
          ,mapId:'a52a74c1-4b7a-4bd6-91bb-3ff11c10d591'
          ,listObj: {
              id:'a52a74c1-4b7a-4bd6-91bb-3ff11c10d591'
              ,nl:'Pc/Internet'
              ,fr:'Pc/Internet'
              ,en:'Pc/Internet'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Requalification loyer'
          ,meta: 'huurherkwalificatie'
          ,key:'VoordelenAlleAard'
          ,mapId:'0de62cf6-b708-4e79-896f-3af3f8e66fcf'
          ,listObj: {
              id:'0de62cf6-b708-4e79-896f-3af3f8e66fcf'
              ,nl:'Huurherkwalificatie'
              ,fr:'Requalification loyer'
              ,en:'Huurherkwalificatie'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Téléphone/Gsm'
          ,meta: 'telefonie'
          ,key:'VoordelenAlleAard'
          ,mapId:'ca8932c9-2382-4470-a5a7-63ffd4d94ebe'
          ,listObj: {
              id:'ca8932c9-2382-4470-a5a7-63ffd4d94ebe'
              ,nl:'Telefoon/Gsm'
              ,fr:'Téléphone/Gsm'
              ,en:'Telephone/Mobile'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Voiture'
          ,meta: 'auto'
          ,key:'VoordelenAlleAard'
          ,mapId:'22e2f8e4-dad3-429f-a1a7-9151ed8a1564'
          ,listObj: {
              id:'22e2f8e4-dad3-429f-a1a7-9151ed8a1564'
              ,nl:'Auto'
              ,fr:'Voiture'
              ,en:'Car'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
        ]
    })
  ,RentegevendeVoorschotten : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-RentegevendeVoorschotten'
        ,items:[
          {text:'Actif'
          ,meta: 'actief'
          ,key:'RentegevendeVoorschotten'
          ,mapId:'0158dd9d-1a23-4d92-9ef6-a1a0bf0a47a1'
          ,listObj: {
              id:'0158dd9d-1a23-4d92-9ef6-a1a0bf0a47a1'
              ,nl:'Actief'
              ,fr:'Actif'
              ,en:'Actief'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Frais dintérêts'
          ,meta: 'intrestkost'
          ,key:'RentegevendeVoorschotten'
          ,mapId:'01f667d4-1745-487e-a494-a815f240a9f0'
          ,listObj: {
              id:'01f667d4-1745-487e-a494-a815f240a9f0'
              ,nl:'Intrest kosten'
              ,fr:'Frais dintérêts'
              ,en:'Intrest kosten'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Passif'
          ,meta: 'passief'
          ,key:'RentegevendeVoorschotten'
          ,mapId:'4044cee6-9a31-4908-ad52-db448fa7f4fd'
          ,listObj: {
              id:'4044cee6-9a31-4908-ad52-db448fa7f4fd'
              ,nl:'Passief'
              ,fr:'Passif'
              ,en:'Passief'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Taux de rendement dintérêts'
          ,meta: 'intrestopbrengst'
          ,key:'RentegevendeVoorschotten'
          ,mapId:'41c87701-9cb7-49fb-ac0e-62b3e806622c'
          ,listObj: {
              id:'41c87701-9cb7-49fb-ac0e-62b3e806622c'
              ,nl:'Intrest opbrengsten'
              ,fr:'Taux de rendement dintérêts'
              ,en:'Intrest opbrengsten'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
        ]
    })
  ,RVIntresten : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-RVIntresten'
        ,items:[
          {text:'Intérêts reçu'
          ,meta: 'intrest'
          ,key:'RVIntresten'
          ,mapId:'92a7ca66-345e-4fbb-bf3f-761b3df4f94b'
          ,listObj: {
              id:'92a7ca66-345e-4fbb-bf3f-761b3df4f94b'
              ,nl:'Ontvangen intresten'
              ,fr:'Intérêts reçu'
              ,en:'Ontvangen intresten'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Précompte mobilier'
          ,meta: 'rv'
          ,key:'RVIntresten'
          ,mapId:'6ebb55f6-5657-4e05-abe5-00a7bdf25339'
          ,listObj: {
              id:'6ebb55f6-5657-4e05-abe5-00a7bdf25339'
              ,nl:'Roerende voorheffing'
              ,fr:'Précompte mobilier'
              ,en:'Roerende voorheffing'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
        ]
    })
  ,Reserves : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-Reserves'
        ,items:[
          {text:'Exonération définitive des bénéfices provenant de lhomologation dun plan de réorganisation et de la constatation dun accord amiable'
          ,meta: 'brvrij_winsthomologatie'
          ,key:'Reserves'
          ,mapId:'371178c4-8b3a-41ad-91b6-5ea12dbe7d18'
          ,listObj: {
              id:'371178c4-8b3a-41ad-91b6-5ea12dbe7d18'
              ,nl:'Winst uit de homologatie van reorganisatieplan en minnelijk akkoord'
              ,fr:'Exonération définitive des bénéfices provenant de lhomologation dun plan de réorganisation et de la constatation dun accord amiable'
              ,en:'Winst uit de homologatie van reorganisatieplan en minnelijk akkoord'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
         {
         text:'Exonération dimpôt: Autres éléments exonérés'
          ,meta:'brvrij_anderevb'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'120 % frais de sécurisation'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'9a493a8b-28d7-4bca-9d74-e74891fb83e1'
          ,listObj: {
              id:'9a493a8b-28d7-4bca-9d74-e74891fb83e1'
              ,nl:'120% kosten van beveiliging'
              ,fr:'120 % frais de sécurisation'
              ,en:'120% kosten van beveiliging'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'120 % frais de transport collectif organisés'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'e8be7d97-c1ad-4dcd-87d2-e4f08d14c6ba'
          ,listObj: {
              id:'e8be7d97-c1ad-4dcd-87d2-e4f08d14c6ba'
              ,nl:'120% kosten voor georganiseerd gemeenschappelijk v'
              ,fr:'120 % frais de transport collectif organisés'
              ,en:'120% kosten voor georganiseerd gemeenschappelijk v'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'120 % frais vélos dentreprise'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'3c6daeca-12ba-4cdf-97dd-2df426759e39'
          ,listObj: {
              id:'3c6daeca-12ba-4cdf-97dd-2df426759e39'
              ,nl:'120% kosten van bedrijfsfietsen'
              ,fr:'120 % frais vélos dentreprise'
              ,en:'120% kosten van bedrijfsfietsen'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'120% déduction pour véhicules sans émissions de CO2'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'af272fd3-07f2-4382-bc2d-1ddb58e2005c'
          ,listObj: {
              id:'af272fd3-07f2-4382-bc2d-1ddb58e2005c'
              ,nl:'120% aftrek voor CO2-uitstootloze autos'
              ,fr:'120% déduction pour véhicules sans émissions de CO2'
              ,en:'120% aftrek voor CO2-uitstootloze autos'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Actif net'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'7eb8b1a7-d395-4bb5-b426-103093dec93d'
          ,listObj: {
              id:'7eb8b1a7-d395-4bb5-b426-103093dec93d'
              ,nl:'Netto-actief'
              ,fr:'Actif net'
              ,en:'Netto-actief'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Actualisation du stock de diamantaires agréés'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'6ce2122c-0a05-4e1b-ac11-aa2b1c44c8ba'
          ,listObj: {
              id:'6ce2122c-0a05-4e1b-ac11-aa2b1c44c8ba'
              ,nl:'Voorraadactualisering erkende diamanthandelaars'
              ,fr:'Actualisation du stock de diamantaires agréés'
              ,en:'Voorraadactualisering erkende diamanthandelaars'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Amortissements fiscalement admis au dessus de la valeur dacquisition'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'ed435274-fac7-425d-9d47-043bcd2886bd'
          ,listObj: {
              id:'ed435274-fac7-425d-9d47-043bcd2886bd'
              ,nl:'Fiscaal toegelaten afschrijvingen boven de aanscha'
              ,fr:'Amortissements fiscalement admis au dessus de la valeur dacquisition'
              ,en:'Fiscaal toegelaten afschrijvingen boven de aanscha'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Autres'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'b6b11987-62df-4380-bff0-543e1c0f0c29'
          ,listObj: {
              id:'b6b11987-62df-4380-bff0-543e1c0f0c29'
              ,nl:'Andere'
              ,fr:'Autres'
              ,en:'Andere'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Bénéfice exonéré dune société dinnovations'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'446f5ea4-9765-496b-9864-c24b5364e8de'
          ,listObj: {
              id:'446f5ea4-9765-496b-9864-c24b5364e8de'
              ,nl:'Vrijgestelde winst van een innovatievennootschap'
              ,fr:'Bénéfice exonéré dune société dinnovations'
              ,en:'Vrijgestelde winst van een innovatievennootschap'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Exonérations de cotisations patronales de sécurité sociale'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'4fc1f563-8637-41d1-a63b-913a03e44bec'
          ,listObj: {
              id:'4fc1f563-8637-41d1-a63b-913a03e44bec'
              ,nl:'Vrijstelling werkgeversbijdragen inzake sociale ze'
              ,fr:'Exonérations de cotisations patronales de sécurité sociale'
              ,en:'Vrijstelling werkgeversbijdragen inzake sociale ze'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Provisions pour avantages sociaux'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'28cd6aee-27e3-43c0-88b0-f440db788001'
          ,listObj: {
              id:'28cd6aee-27e3-43c0-88b0-f440db788001'
              ,nl:'Provisies voor sociale voordelen'
              ,fr:'Provisions pour avantages sociaux'
              ,en:'Provisies voor sociale voordelen'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Provisions pour passif social'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'be71ff83-148a-4728-94e5-56d6e0962777'
          ,listObj: {
              id:'be71ff83-148a-4728-94e5-56d6e0962777'
              ,nl:'Voorziening voor sociaal passief'
              ,fr:'Provisions pour passif social'
              ,en:'Voorziening voor sociaal passief'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Rachat dactions - F.R.I. dune société de reconversion proprement dite'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'b7da956a-d70a-4606-a246-054f9689963d'
          ,listObj: {
              id:'b7da956a-d70a-4606-a246-054f9689963d'
              ,nl:'Afkoop van F.I.V- aandelen van een eigenlijke reco'
              ,fr:'Rachat dactions - F.R.I. dune société de reconversion proprement dite'
              ,en:'Afkoop van F.I.V- aandelen van een eigenlijke reco'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Réserve dinvestissement de lexercice imposable'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'93dfceb7-ab6b-40f2-85e1-f98caed36f2e'
          ,listObj: {
              id:'93dfceb7-ab6b-40f2-85e1-f98caed36f2e'
              ,nl:'Investeringsreserve met betrekking tot aanslagjaar'
              ,fr:'Réserve dinvestissement de lexercice imposable'
              ,en:'Investeringsreserve met betrekking tot aanslagjaar'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Subsides en capital'
          ,meta: 'brvrij_anderevb'
          ,key:'Reserves'
          ,mapId:'afa2062f-d033-4223-b995-f8e97c089e3b'
          ,listObj: {
              id:'afa2062f-d033-4223-b995-f8e97c089e3b'
              ,nl:'Kapitaalsubsidies'
              ,fr:'Subsides en capital'
              ,en:'Kapitaalsubsidies'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Exonération dimpôt: Plus-value réalisée'
          ,meta:'brvrij_verwmw'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Apport dune ou plusieurs branches dactivité ou dune universalité de biens'
          ,meta: 'brvrij_verwmw'
          ,key:'Reserves'
          ,mapId:'6858bfc2-0edf-49bc-a6e9-b6c2bf9547bb'
          ,listObj: {
              id:'6858bfc2-0edf-49bc-a6e9-b6c2bf9547bb'
              ,nl:'Inbreng van bedrijfsafdelingen of takke'
              ,fr:'Apport dune ou plusieurs branches dactivité ou dune universalité de biens'
              ,en:'Inbreng van bedrijfsafdelingen of takke'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Autres'
          ,meta: 'brvrij_verwmw'
          ,key:'Reserves'
          ,mapId:'f5982a3e-f2ac-44a4-8a3d-5c1870d4b712'
          ,listObj: {
              id:'f5982a3e-f2ac-44a4-8a3d-5c1870d4b712'
              ,nl:'Andere'
              ,fr:'Autres'
              ,en:'Andere'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Considéré comme non-réalisé'
          ,meta: 'brvrij_verwmw'
          ,key:'Reserves'
          ,mapId:'514703c4-2ed5-4f24-a9ca-b8141ff62f5e'
          ,listObj: {
              id:'514703c4-2ed5-4f24-a9ca-b8141ff62f5e'
              ,nl:'Als niet verwezenlijkt beschouwd'
              ,fr:'Considéré comme non-réalisé'
              ,en:'Als niet verwezenlijkt beschouwd'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Domaine à problème et exonération économiques'
          ,meta: 'brvrij_verwmw'
          ,key:'Reserves'
          ,mapId:'6f484ffa-ea68-426a-a8f7-2a08e21892e3'
          ,listObj: {
              id:'6f484ffa-ea68-426a-a8f7-2a08e21892e3'
              ,nl:'Probleemgebieden en economische vrijstellingen'
              ,fr:'Domaine à problème et exonération économiques'
              ,en:'Probleemgebieden en economische vrijstellingen'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Entrepreneurs immobliers'
          ,meta: 'brvrij_verwmw'
          ,key:'Reserves'
          ,mapId:'7b9f56a8-3f27-46d0-bec6-4337049dccc2'
          ,listObj: {
              id:'7b9f56a8-3f27-46d0-bec6-4337049dccc2'
              ,nl:'Vastgoedhandelaars'
              ,fr:'Entrepreneurs immobliers'
              ,en:'Vastgoedhandelaars'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Plus-values forcées'
          ,meta: 'brvrij_verwmw'
          ,key:'Reserves'
          ,mapId:'28241dbf-722a-448f-88d7-f8d41a9e6593'
          ,listObj: {
              id:'28241dbf-722a-448f-88d7-f8d41a9e6593'
              ,nl:'Gedwongen meerwaarden'
              ,fr:'Plus-values forcées'
              ,en:'Gedwongen meerwaarden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Plus-values non monétaire'
          ,meta: 'brvrij_verwmw'
          ,key:'Reserves'
          ,mapId:'ad83ddcd-67e3-4dd1-95cf-188b35771d75'
          ,listObj: {
              id:'ad83ddcd-67e3-4dd1-95cf-188b35771d75'
              ,nl:'Niet monetaire meerwaarden'
              ,fr:'Plus-values non monétaire'
              ,en:'Niet monetaire meerwaarden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Quotité monétaire'
          ,meta: 'brvrij_verwmw'
          ,key:'Reserves'
          ,mapId:'4624cd9f-492f-481a-a646-05d02d307f9a'
          ,listObj: {
              id:'4624cd9f-492f-481a-a646-05d02d307f9a'
              ,nl:'Monetair gedeelte'
              ,fr:'Quotité monétaire'
              ,en:'Monetair gedeelte'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Exonération dimpôt: Plus-values exprimées mais non réalisées'
          ,meta:'brvrij_nietverwmw'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Apport dune plus-value'
          ,meta: 'brvrij_nietverwmw'
          ,key:'Reserves'
          ,mapId:'6c629fa6-eb79-4727-a9b8-4fc97984cd48'
          ,listObj: {
              id:'6c629fa6-eb79-4727-a9b8-4fc97984cd48'
              ,nl:'Inbrengmeerwaarden'
              ,fr:'Apport dune plus-value'
              ,en:'Inbrengmeerwaarden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Autres'
          ,meta: 'brvrij_nietverwmw'
          ,key:'Reserves'
          ,mapId:'f55be2c9-c14b-4d1b-a677-d15235ac76c6'
          ,listObj: {
              id:'f55be2c9-c14b-4d1b-a677-d15235ac76c6'
              ,nl:'Andere'
              ,fr:'Autres'
              ,en:'Andere'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Plus-values de réévaluation'
          ,meta: 'brvrij_nietverwmw'
          ,key:'Reserves'
          ,mapId:'d9ae297d-0294-4319-b704-f9c35d86bae7'
          ,listObj: {
              id:'d9ae297d-0294-4319-b704-f9c35d86bae7'
              ,nl:'Herschattingsmeerwaarden'
              ,fr:'Plus-values de réévaluation'
              ,en:'Herschattingsmeerwaarden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Plus-values exprimées mais non réalisées'
          ,meta: 'brvrij_nietverwmw'
          ,key:'Reserves'
          ,mapId:'67e6d44c-b46f-4aa4-9fc1-a7eb4c9c7931'
          ,listObj: {
              id:'67e6d44c-b46f-4aa4-9fc1-a7eb4c9c7931'
              ,nl:'Niet verwezenlijkte meerwaarden'
              ,fr:'Plus-values exprimées mais non réalisées'
              ,en:'Niet verwezenlijkte meerwaarden'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
         {
         text:'Exonération dimpôt: Taxation étalée des plus-values réalisées'
          ,meta:'brvrij_gesprverwmw'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[
          {text:'Certains titres'
          ,meta: 'brvrij_gesprverwmw'
          ,key:'Reserves'
          ,mapId:'d8314000-8b2c-4157-a550-3ba74d3c95c2'
          ,listObj: {
              id:'d8314000-8b2c-4157-a550-3ba74d3c95c2'
              ,nl:'Bepaalde effecten'
              ,fr:'Certains titres'
              ,en:'Bepaalde effecten'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Immobilisations corporelles et incorporelles'
          ,meta: 'brvrij_gesprverwmw'
          ,key:'Reserves'
          ,mapId:'eaef62cc-ff45-49c1-b371-744e14142561'
          ,listObj: {
              id:'eaef62cc-ff45-49c1-b371-744e14142561'
              ,nl:'Materiële en immateriële vaste activa'
              ,fr:'Immobilisations corporelles et incorporelles'
              ,en:'Materiële en immateriële vaste activa'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
            ] }
          }
  ,
          {text:'Modification de la situation du début des réserves : autres'
          ,meta: 'aanpassingBT'
          ,key:'Reserves'
          ,mapId:'6eaaaf3b-7a31-46b7-9a34-c435203a5939'
          ,listObj: {
              id:'6eaaaf3b-7a31-46b7-9a34-c435203a5939'
              ,nl:'Aanpassing BT: Andere'
              ,fr:'Modification de la situation du début des réserves : autres'
              ,en:'Aanpassing BT: Andere'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Oeuvres audio-visuel agrées Tax Shelter'
          ,meta: 'brvrij_taxshelter'
          ,key:'Reserves'
          ,mapId:'4c67ea9e-6fdd-4658-bbb6-e34b33b1dbd3'
          ,listObj: {
              id:'4c67ea9e-6fdd-4658-bbb6-e34b33b1dbd3'
              ,nl:'Tax shelter erkende audiovisuele werken'
              ,fr:'Oeuvres audio-visuel agrées Tax Shelter'
              ,en:'Tax shelter erkende audiovisuele werken'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Plus-value sur bateaux de navigation intérieure'
          ,meta: 'brvrij_mwbinnensch'
          ,key:'Reserves'
          ,mapId:'04106d37-50be-4a8a-a761-5b73f6746c4f'
          ,listObj: {
              id:'04106d37-50be-4a8a-a761-5b73f6746c4f'
              ,nl:'Meerwaarden op binnenschepen'
              ,fr:'Plus-value sur bateaux de navigation intérieure'
              ,en:'Meerwaarden op binnenschepen'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Plus-values sur navires'
          ,meta: 'brvrij_mwzeeesch'
          ,key:'Reserves'
          ,mapId:'798337d7-93bd-4b7d-b32d-e5bc66a532f5'
          ,listObj: {
              id:'798337d7-93bd-4b7d-b32d-e5bc66a532f5'
              ,nl:'Meerwaarden op zeeschepen'
              ,fr:'Plus-values sur navires'
              ,en:'Meerwaarden op zeeschepen'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Plus-values sur véhicules dentreprises'
          ,meta: 'brvrij_mwbedrvoert'
          ,key:'Reserves'
          ,mapId:'96f82deb-c464-45b0-b68f-780da3a1e724'
          ,listObj: {
              id:'96f82deb-c464-45b0-b68f-780da3a1e724'
              ,nl:'Meerwaarden op bedrijfsvoertuigen'
              ,fr:'Plus-values sur véhicules dentreprises'
              ,en:'Meerwaarden op bedrijfsvoertuigen'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Réserve dinvestissement'
          ,meta: 'brvrij_investres'
          ,key:'Reserves'
          ,mapId:'7b10a74c-6c24-470e-8284-a678f8cf38cb'
          ,listObj: {
              id:'7b10a74c-6c24-470e-8284-a678f8cf38cb'
              ,nl:'Investeringsreserve'
              ,fr:'Réserve dinvestissement'
              ,en:'Investeringsreserve'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Réserves imposables'
          ,meta: 'belastereserve'
          ,key:'Reserves'
          ,mapId:'5bf76cf9-16e8-4ff0-a8bf-4c9b8209e547'
          ,listObj: {
              id:'5bf76cf9-16e8-4ff0-a8bf-4c9b8209e547'
              ,nl:'Belaste reserves'
              ,fr:'Réserves imposables'
              ,en:'Charged reserves'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
        ]
    })
  ,DBI : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-DBI'
        ,items:[
          {text:'Dividende'
          ,meta: 'dividend'
          ,key:'DBI'
          ,mapId:'402e7e0e-e51e-4413-b78e-cf9113f716e6'
          ,listObj: {
              id:'402e7e0e-e51e-4413-b78e-cf9113f716e6'
              ,nl:'Dividend'
              ,fr:'Dividende'
              ,en:'Dividend'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  ,
          {text:'Participation'
          ,meta: 'participatie'
          ,key:'DBI'
          ,mapId:'b022f480-2450-4fa9-a153-c400e3a45f5c'
          ,listObj: {
              id:'b022f480-2450-4fa9-a153-c400e3a45f5c'
              ,nl:'Participatie'
              ,fr:'Participation'
              ,en:'Participation'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
        ]
    })
  ,meerwaardeaandelen : new eBook.Accounts.MappingMenu({
        id:'eb-mapping-2012-meerwaardeaandelen'
        ,items:[
          {text:'Plus-values sur actions ou parts'
          ,meta: 'meerwaardeaandelen'
          ,key:'meerwaardeaandelen'
          ,mapId:'77479167-d6c9-49e0-a60b-3056ca49f1dd'
          ,listObj: {
              id:'77479167-d6c9-49e0-a60b-3056ca49f1dd'
              ,nl:'Meerwaarden op aandelen'
              ,fr:'Plus-values sur actions ou parts'
              ,en:'Caiptal Gains Shares'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  
        ]
    })
  
    };

    eBook.Accounts.Mappings.Descs = {
    'M4b6fea84-dae6-4d50-b666-080a8085dd35':'Avantages de toute nature'
    ,'Meb417ec7-1207-497e-a8b0-cb9af71a7bce':'Commissions, courtages, ristournes commerciales, etc.'
    ,'M60551e91-59df-4b70-b5f2-efc98b15416b':'Frais exposés pour compte du bénéficiaire'
    ,'M200adb0a-b150-46e9-b572-96f21ba431bd':'Honoraires ou vacations'
    ,'M7c5a87d8-61de-4550-95cb-fbe6dec24514':'Amendes, pénalités et confiscations de toute nature'
    ,'Mb7aadf3f-5631-4418-a7b3-f77f7e5b9deb':'Autres'
    ,'M62afa7e6-e973-4185-9940-cb4cfe1fe12c':'Avantages anormaux ou bénévoles'
    ,'Mbc27c4a1-3ffb-470d-b21c-56f34231548b':'Avantages de titres-repas, chèques sport/culture ou éco-chèques'
    ,'M3d98ba0d-907a-4f1d-a7d0-caa280420baf':'Bénéfices sociale'
    ,'Ma86fc507-fc68-44d0-b4fe-f7f95d1464e4':'Cout tax shelter  DESC'
    ,'M87216df4-5ccd-469e-8bc9-ad067fa2ef8a':'Donation accepté fiscal / Liberalité'
    ,'M5a6f6c1c-ddbf-4930-8fd1-29ba567cf69a':'Donation non accepté fiscal / Liberalité'
    ,'M5820cf02-cef5-4b96-a350-fd2542c291cf':'Frais de voiture non déductible  (50%)'
    ,'M2168e25f-286e-4955-ace0-cf62124105a2':'Frais de voiture non déductible (10% de 70%)'
    ,'M37743faf-f687-4176-a16a-683fa4ca1124':'Frais de voiture non déductible (10%)'
    ,'Md63d18b5-bdb8-4d0b-a796-4fa76ca854ec':'Frais de voiture non déductible (20% de 70%)'
    ,'M842d79fd-0e34-445e-8cfb-95f5f41f4aef':'Frais de voiture non déductible (20%)'
    ,'Mb8e033e1-f19c-4f9e-8010-99b99874a9f8':'Frais de voiture non déductible (25% de 60%)'
    ,'M2bc7d196-714e-47da-b427-45fb49373e3c':'Frais de voiture non déductible (25% de 70%)'
    ,'M5d3272a6-4473-4d57-95b0-63d8a715d973':'Frais de voiture non déductible (25% de 75%)'
    ,'M164303dc-c36b-4033-b2e4-b492e0139e1c':'Frais de voiture non déductible (25% de 90%)'
    ,'M8c06d1ea-43cd-4b01-a591-21a2f9d20318':'Frais de voiture non déductible (25%)'
    ,'Ma945a5fd-130e-4264-b1a0-8419a9721a5f':'Frais de voiture non déductible (30% de 70%)'
    ,'M69ed6f97-6c87-46c7-89bc-4d934e3fe54a':'Frais de voiture non déductible (30%)'
    ,'M6a616fdc-76d3-476a-8ee4-ab40fb9a1523':'Frais de voiture non déductible (40% de 70%)'
    ,'M041f575f-e3ff-40c8-9f08-3356b923cad5':'Frais de voiture non déductible (40%)'
    ,'Mcf60b185-08a7-4ac5-b17b-ae38ec2af1a0':'Frais de voiture non déductible (50% de 70%)'
    ,'Md958084a-dd72-4f1c-90f3-430c418da23e':'Frais de voiture non déductible 25% de 80%)'
    ,'M8cf1942c-7bc2-40ec-8b93-82967a44d5e5':'Non déductible frais dessence (25%)'
    ,'Mc581b67c-2fa1-4751-932a-96056e42d19d':'Frais de voiture non déductible (10% de 70%)'
    ,'Mfb7a8eab-c1eb-4068-9182-ff6d22acaa4c':'Frais de voiture non déductible (20% de 70%)'
    ,'M5e70313e-fb08-46f1-b76c-7196ac8b3d0e':'Frais de voiture non déductible (25% de 70%)'
    ,'M1ba344e6-2d4b-41bf-8188-4f4881330e29':'Frais de voiture non déductible (30% de 70%)'
    ,'Mfcfafd32-9775-4b3b-81f4-9b0868a0ad24':'Frais de voiture non déductible (40% de 70%)'
    ,'M8052c5fb-dd87-46ac-b0dc-fe9a7acbcc27':'Frais de voiture non déductible (50% de 70%)'
    ,'M68b9c809-2c7b-4f49-83b7-ff0df302a9b8':'Frais de vêtements professionnels non-spécifiques'
    ,'Mc8b41c86-f115-4c65-b0fc-0e6413b5f6fe':'Frais de voiture parti Avantages et libéralités (17%)'
    ,'M6cf63c4b-3df9-403b-9431-a10161fdba10':'Indémnité pour coupon manquant'
    ,'M6b500962-b80c-46c5-b615-66c58529c31e':'Intérêts éxagérés'
    ,'M473e73e8-0578-4505-9e02-90568b2da589':'Intérêts relatifs à une partie de certains emprunts'
    ,'M31fb1d70-6c6e-40e9-a9a8-ca919e72e7a6':'Les impôts, taxes et rétributions régionaux '
    ,'Ma25e863f-ec46-424d-a3e2-572b6abf1c89':'Non déductible frais de réception et de cadeaux (50%)'
    ,'Me7c1c17d-9ecb-4d77-93b6-5a4e2ac75131':'Non déductible frais de restaurant (31%)'
    ,'M6ba6a64a-0415-405d-a0b3-1930e28b9bbe':'Non déductible impôts'
    ,'M89baa6b1-92c5-4902-8236-13bfa3813d34':'Non déductible pensions et contribution de pension'
    ,'M8a567601-f2d5-4469-b160-03867e2f0f76':'Paiements non déductibles vers certains Etats'
    ,'Mcafcb0f9-95cc-491e-a51e-5c5c8e7f740f':'Participation des travailleurs'
    ,'M6932dd3f-2e1f-4332-90b0-a7e6f1739907':'Primes, subsides en capital et en intérêt régionaux'
    ,'Md92229e3-ed8f-41fb-a714-80f5d3f54a1f':'Réductions de valeur et moins-values sur actions ou parts'
    ,'M20986c04-c081-4198-85eb-c37148e53b5f':'Reprises dexonérations antérieures'
    ,'M8e9de920-ad97-4edd-91b0-0cb35ed67602':'Compte de résultats avantages - employé'
    ,'Mb9e4820c-0a1a-4284-9f2e-35064255294c':'Compte de résultats avantages - ouvrier'
    ,'Mf8cd8522-c3b1-4867-b89d-efad88942092':'Compte des résultats brut employés'
    ,'M00cf8a7c-7c0b-4286-9be7-ff74aaa5112f':'Compte des résultats brut ouvriers'
    ,'M2f1c1df0-7b05-439b-845d-f036ab765eec':'Compte de résultats - chèques-repas - ouvrier'
    ,'M4f5e8ffb-031c-476b-a154-0efc048508dd':'Compte de résultats Chèque-repas - employé'
    ,'M2978d3e2-2189-44b8-b369-55e0bf881c99':'Compte de résultats - dirigeant dentreprise'
    ,'M0331ed91-102f-4ab6-a889-8b3cb4d37c11':'Compte de résultats  - Cotisation patronale - employé'
    ,'Mcdc8b4da-52ae-40cc-a445-ee77b73c3b82':'Compte de résultats - Cotisation patronale - ouvrier'
    ,'Mc1f79aee-2e17-4879-893d-59da44d5e93a':'Compte de résultats - frais demployeur - employé'
    ,'Ma37af5c9-e5ed-4ed0-85e5-5f48f30001d4':'Compte de résultats - frais demployeur - ouvrier'
    ,'M4cc7d998-4820-4a32-9fcb-efce61cb2c89':'Compte de bilan de clôture - Pécule de vacances - employé'
    ,'M24757ec4-b43b-4e93-a7f4-3bf938552403':'Compte de bilan de clôture - Pécule de vacances - ouvrier'
    ,'Ma5425f72-7fcc-4999-8fe0-bb95c2b5f18e':'Compte de bilan de clôture - Net - employé'
    ,'M59f4e092-e1e7-43a2-bda4-45c47aac8dbe':'Compte de bilan de clôture - Net - ouvrier'
    ,'M57020c49-52ca-4a23-b01e-a15987e75dac':'Compte de résultats  - Résidence-effort - ouvrier'
    ,'M430f1419-5dfe-4893-8e3d-ff42a68c2d9d':'Compte de résultats - Résidentiel-effort - employé'
    ,'M2c278b87-47e9-4f3f-a155-d065eb2f8552':'Compte de bilan de clôture - retenue à  la source sur rémunération - employé'
    ,'M08861715-ab4b-4ebe-9431-38f6784f6970':'Compte de bilan de clôture - retenue à  la source sur rémunération - ouvrier'
    ,'Md387dfa4-7da3-4b70-ad19-92fc7afa9981':'Compte de bilan de clôture - RSZ - employé'
    ,'M671092f6-1ca2-49a2-8709-5c527ab03bf0':'Compte de bilan de clôture - RSZ - ouvrier'
    ,'Ma389669c-92a8-4b19-bcae-2cee5bc72b83':'Compte de résultats  Pécule de vacances - ouvrier'
    ,'M6f65243e-bb70-4662-9ef3-0f5ba7af3cb7':'Compte de résultats Pécule de vacances - employé'
    ,'Mb12e1513-d2e5-4e35-b6e5-526bef1b37e4':'C/C inscrit'
    ,'M914e2289-3a7a-4a2e-9add-b26515ecdfbe':'Ventes inscrit'
    ,'M0c87df9a-d763-4958-b225-a19b3be25571':'Autre'
    ,'M64b192f4-a80f-4034-b78a-c4eebde2122c':'Autre services dutilité'
    ,'Mf833b6f6-a928-43e1-b1fb-24b02e0b0653':'Chauffage/électricité'
    ,'M1b05fc38-4bf5-411b-8def-dda56325e6f5':'Cotisations sociales dirigeant dentreprise'
    ,'M57a4d21d-a142-4b6f-bddf-e4b9ffa3d3be':'Immeuble/logement'
    ,'Mccd3da15-c36d-49b4-b74c-5580a1bdb5f0':'Intérêts sur c/c dirigeant dentreprise'
    ,'Ma52a74c1-4b7a-4bd6-91bb-3ff11c10d591':'Pc/Internet'
    ,'M0de62cf6-b708-4e79-896f-3af3f8e66fcf':'Requalification loyer'
    ,'Mca8932c9-2382-4470-a5a7-63ffd4d94ebe':'Téléphone/Gsm'
    ,'M22e2f8e4-dad3-429f-a1a7-9151ed8a1564':'Voiture'
    ,'M0158dd9d-1a23-4d92-9ef6-a1a0bf0a47a1':'Actif'
    ,'M01f667d4-1745-487e-a494-a815f240a9f0':'Frais dintérêts'
    ,'M4044cee6-9a31-4908-ad52-db448fa7f4fd':'Passif'
    ,'M41c87701-9cb7-49fb-ac0e-62b3e806622c':'Taux de rendement dintérêts'
    ,'M92a7ca66-345e-4fbb-bf3f-761b3df4f94b':'Intérêts reçu'
    ,'M6ebb55f6-5657-4e05-abe5-00a7bdf25339':'Précompte mobilier'
    ,'M371178c4-8b3a-41ad-91b6-5ea12dbe7d18':'Exonération définitive des bénéfices provenant de lhomologation dun plan de réorganisation et de la constatation dun accord amiable'
    ,'M9a493a8b-28d7-4bca-9d74-e74891fb83e1':'120 % frais de sécurisation'
    ,'Me8be7d97-c1ad-4dcd-87d2-e4f08d14c6ba':'120 % frais de transport collectif organisés'
    ,'M3c6daeca-12ba-4cdf-97dd-2df426759e39':'120 % frais vélos dentreprise'
    ,'Maf272fd3-07f2-4382-bc2d-1ddb58e2005c':'120% déduction pour véhicules sans émissions de CO2'
    ,'M7eb8b1a7-d395-4bb5-b426-103093dec93d':'Actif net'
    ,'M6ce2122c-0a05-4e1b-ac11-aa2b1c44c8ba':'Actualisation du stock de diamantaires agréés'
    ,'Med435274-fac7-425d-9d47-043bcd2886bd':'Amortissements fiscalement admis au dessus de la valeur dacquisition'
    ,'Mb6b11987-62df-4380-bff0-543e1c0f0c29':'Autres'
    ,'M446f5ea4-9765-496b-9864-c24b5364e8de':'Bénéfice exonéré dune société dinnovations'
    ,'M4fc1f563-8637-41d1-a63b-913a03e44bec':'Exonérations de cotisations patronales de sécurité sociale'
    ,'M28cd6aee-27e3-43c0-88b0-f440db788001':'Provisions pour avantages sociaux'
    ,'Mbe71ff83-148a-4728-94e5-56d6e0962777':'Provisions pour passif social'
    ,'Mb7da956a-d70a-4606-a246-054f9689963d':'Rachat dactions - F.R.I. dune société de reconversion proprement dite'
    ,'M93dfceb7-ab6b-40f2-85e1-f98caed36f2e':'Réserve dinvestissement de lexercice imposable'
    ,'Mafa2062f-d033-4223-b995-f8e97c089e3b':'Subsides en capital'
    ,'M6858bfc2-0edf-49bc-a6e9-b6c2bf9547bb':'Apport dune ou plusieurs branches dactivité ou dune universalité de biens'
    ,'Mf5982a3e-f2ac-44a4-8a3d-5c1870d4b712':'Autres'
    ,'M514703c4-2ed5-4f24-a9ca-b8141ff62f5e':'Considéré comme non-réalisé'
    ,'M6f484ffa-ea68-426a-a8f7-2a08e21892e3':'Domaine à problème et exonération économiques'
    ,'M7b9f56a8-3f27-46d0-bec6-4337049dccc2':'Entrepreneurs immobliers'
    ,'M28241dbf-722a-448f-88d7-f8d41a9e6593':'Plus-values forcées'
    ,'Mad83ddcd-67e3-4dd1-95cf-188b35771d75':'Plus-values non monétaire'
    ,'M4624cd9f-492f-481a-a646-05d02d307f9a':'Quotité monétaire'
    ,'M6c629fa6-eb79-4727-a9b8-4fc97984cd48':'Apport dune plus-value'
    ,'Mf55be2c9-c14b-4d1b-a677-d15235ac76c6':'Autres'
    ,'Md9ae297d-0294-4319-b704-f9c35d86bae7':'Plus-values de réévaluation'
    ,'M67e6d44c-b46f-4aa4-9fc1-a7eb4c9c7931':'Plus-values exprimées mais non réalisées'
    ,'Md8314000-8b2c-4157-a550-3ba74d3c95c2':'Certains titres'
    ,'Meaef62cc-ff45-49c1-b371-744e14142561':'Immobilisations corporelles et incorporelles'
    ,'M6eaaaf3b-7a31-46b7-9a34-c435203a5939':'Modification de la situation du début des réserves : autres'
    ,'M4c67ea9e-6fdd-4658-bbb6-e34b33b1dbd3':'Oeuvres audio-visuel agrées Tax Shelter'
    ,'M04106d37-50be-4a8a-a761-5b73f6746c4f':'Plus-value sur bateaux de navigation intérieure'
    ,'M798337d7-93bd-4b7d-b32d-e5bc66a532f5':'Plus-values sur navires'
    ,'M96f82deb-c464-45b0-b68f-780da3a1e724':'Plus-values sur véhicules dentreprises'
    ,'M7b10a74c-6c24-470e-8284-a678f8cf38cb':'Réserve dinvestissement'
    ,'M5bf76cf9-16e8-4ff0-a8bf-4c9b8209e547':'Réserves imposables'
    ,'M402e7e0e-e51e-4413-b78e-cf9113f716e6':'Dividende'
    ,'Mb022f480-2450-4fa9-a153-c400e3a45f5c':'Participation'
    , 'M77479167-d6c9-49e0-a60b-3056ca49f1dd': 'Plus-values sur actions ou parts'
    , 'Mf50fc02c-3e78-4e99-98f6-f955bf7c4f46': 'Dépenses ou avantages de toute nature non justifiés'
    
    };

  