

Ext = {

    /**
    * The version of the framework
    * @type String
    */
    version: '3.2.1',
    versionDetail: {
        major: 3,
        minor: 2,
        patch: 1
    }
};

Ext.apply = function(o, c, defaults) {
    // no "this" reference for friendly out of scope calls
    if (defaults) {
        Ext.apply(o, defaults);
    }
    if (o && c && typeof c == 'object') {
        for (var p in c) {
            o[p] = c[p];
        }
    }
    return o;
};


Ext.apply(Ext, {

    USE_NATIVE_JSON: false,
    applyIf: function(o, c) {
        if (o) {
            for (var p in c) {
                if (!Ext.isDefined(o[p])) {
                    o[p] = c[p];
                }
            }
        }
        return o;
    },
    extend: function() {
        // inline overrides
        var io = function(o) {
            for (var m in o) {
                this[m] = o[m];
            }
        };
        var oc = Object.prototype.constructor;

        return function(sb, sp, overrides) {
            if (typeof sp == 'object') {
                overrides = sp;
                sp = sb;
                sb = overrides.constructor != oc ? overrides.constructor : function() { sp.apply(this, arguments); };
            }
            var F = function() { },
      sbp,
      spp = sp.prototype;

            F.prototype = spp;
            sbp = sb.prototype = new F();
            sbp.constructor = sb;
            sb.superclass = spp;
            if (spp.constructor == oc) {
                spp.constructor = sp;
            }
            sb.override = function(o) {
                Ext.override(sb, o);
            };
            sbp.superclass = sbp.supr = (function() {
                return spp;
            });
            sbp.override = io;
            Ext.override(sb, overrides);
            sb.extend = function(o) { return Ext.extend(sb, o); };
            return sb;
        };
    } (),

    override: function(origclass, overrides) {
        if (overrides) {
            var p = origclass.prototype;
            Ext.apply(p, overrides);
            if (false && overrides.hasOwnProperty('toString')) {
                p.toString = overrides.toString;
            }
        }
    },
    toArray: function() {
        return false ?
      function(a, i, j, res) {
          res = [];
          for (var x = 0, len = a.length; x < len; x++) {
              res.push(a[x]);
          }
          return res.slice(i || 0, j || res.length);
      } :
                 function(a, i, j) {
                     return Array.prototype.slice.call(a, i || 0, j || a.length);
                 }
    } (),


    isEmpty: function(v, allowBlank) {
        return v === null || v === undefined || ((Ext.isArray(v) && !v.length)) || (!allowBlank ? v === '' : false);
    },
    isArray: function(v) {
        return toString.apply(v) === '[object Array]';
    },
    isDate: function(v) {
        return toString.apply(v) === '[object Date]';
    },
    isObject: function(v) {
        return !!v && Object.prototype.toString.call(v) === '[object Object]';
    },
    isPrimitive: function(v) {
        return Ext.isString(v) || Ext.isNumber(v) || Ext.isBoolean(v);
    },
    isFunction: function(v) {
        return toString.apply(v) === '[object Function]';
    },
    isNumber: function(v) {
        return typeof v === 'number' && isFinite(v);
    },
    isString: function(v) {
        return typeof v === 'string';
    },
    isBoolean: function(v) {
        return typeof v === 'boolean';
    },
    isElement: function(v) {
        return v ? !!v.tagName : false;
    },
    isDefined: function(v) {
        return typeof v !== 'undefined';
    },


    /**
    * True if the detected browser is Opera.
    * @type Boolean
    */
    isOpera: false,

    /**
    * True if the detected browser uses WebKit.
    * @type Boolean
    */
    isWebKit: true,

    /**
    * True if the detected browser is Chrome.
    * @type Boolean
    */
    isChrome: true,

    /**
    * True if the detected browser is Safari.
    * @type Boolean
    */
    isSafari: false,

    /**
    * True if the detected browser is Safari 3.x.
    * @type Boolean
    */
    isSafari3: false,

    /**
    * True if the detected browser is Safari 4.x.
    * @type Boolean
    */
    isSafari4: false,

    /**
    * True if the detected browser is Safari 2.x.
    * @type Boolean
    */
    isSafari2: false,

    /**
    * True if the detected browser is Internet Explorer.
    * @type Boolean
    */
    isIE: false,

    /**
    * True if the detected browser is Internet Explorer 6.x.
    * @type Boolean
    */
    isIE6: false,

    /**
    * True if the detected browser is Internet Explorer 7.x.
    * @type Boolean
    */
    isIE7: false,

    /**
    * True if the detected browser is Internet Explorer 8.x.
    * @type Boolean
    */
    isIE8: false,

    /**
    * True if the detected browser uses the Gecko layout engine (e.g. Mozilla, Firefox).
    * @type Boolean
    */
    isGecko: false,

    /**
    * True if the detected browser uses a pre-Gecko 1.9 layout engine (e.g. Firefox 2.x).
    * @type Boolean
    */
    isGecko2: false,

    /**
    * True if the detected browser uses a Gecko 1.9+ layout engine (e.g. Firefox 3.x).
    * @type Boolean
    */
    isGecko3: false,

    /**
    * True if the detected browser is Internet Explorer running in non-strict mode.
    * @type Boolean
    */
    isBorderBox: false,

    /**
    * True if the detected platform is Linux.
    * @type Boolean
    */
    isLinux: false,

    /**
    * True if the detected platform is Windows.
    * @type Boolean
    */
    isWindows: true,

    /**
    * True if the detected platform is Mac OS.
    * @type Boolean
    */
    isMac: false,

    /**
    * True if the detected platform is Adobe Air.
    * @type Boolean
    */
    isAir: false
});


self.storeFunctions = {
    registerField: function(fieldId, elementId, path, xtype) {
        var eid = path ? xtype != 'combo' ? path + '/' + elementId : path : elementId;
        if (!this.elementsByFields[fieldId]) this.elementsByFields[fieldId] = eid;
        if (!this.fieldsByElements[elementId]) this.fieldsByElements[elementId] = [];
        this.fieldsByElements[elementId].push(fieldId);
        return this.getElementValueById(elementId, xtype);
    }
      , setValueByField: function(fld) {
          var id = this.elementsByFields[fld.id];
          if (id.indexOf('/') > -1) {
              // structured node
              var xtype = fld.getXType();
              var lst = id.split('/');
              var mi = this.elementsByNamePeriodContext[lst[0]];
              if (!mi) {
                  var nmsplit = lst[0].split('_');
                  this.addElement(this.createStructuredElement(nmsplit[0], nmsplit[1]));
                  mi = this.elementsByNamePeriodContext[lst[0]];
              }
              var midx = mi[0];
              var el = this.elements[midx];
              for (var i = 1; i < lst.length; i++) {
                  if (el.Children == null) el.Children = [];
                  var nel = this.getElementChildById(el, lst[i]);
                  if (nel == null) {
                      if (i < lst.length - 1) {
                          var its = lst[i].split('_');
                          nel = this.createStructuredElement(its[0], its[1]);
                          el.Children.push(nel);

                      } else {
                          if (xtype == 'combo') {
                              var its = lst[i].split('_');
                              nel = this.createStructuredElement(its[0], its[1]);
                              el.Children.push(nel);
                          } else {
                              nel = this.createElementFromField(fld);
                              el.Children.push(nel);
                          }
                      }
                  }
                  el = nel;
              }
              if (el == null) return null;
              this.updateElement(el, fld.getValue(), xtype, fld);
              return this.retrieveElementValue(el);
          }
          else {

              if (id == null) {
                  // NEW CREATION
                  this.addElementByField(fld);
                  return;
              }
              var idx = this.elementsByNamePeriodContext[id];
              if (idx) {
                  this.updateElement(idx[0], fld.getValue(), fld.getXType(), fld);
              } else {
                  this.addElementByField(fld);
                  return;
              }
          }
      }
    , createElementByCalcCfg: function(cfg) {
        var cref = cfg.period;
        if (cfg.context != null && cfg.context != '') cref += ('__' + cref.context);
        var el = {
            AutoRendered: false
      , Children: null
      , Context: cfg.context
      , ContextRef: cref
      , Decimals: "INF" // to determine?
      , Id: cfg.id
      , Name: cfg.name
      , NameSpace: ""
      , Period: cfg.period
      , Prefix: cfg.prefix
      , UnitRef: "EUR" // to determine?
      , Value: "" + (cfg.value == null ? "" : cfg.value)
        };
        return el;
    }
      , createComboValueElement: function(fld) {

          var val = fld.getValue(); // is id like pfs-vl_XCode_LegalFormCode_001
          if (Ext.isEmpty(val)) return null;
          var splitted = val.split('_');
          var prefix = splitted[0];
          var value = splitted[splitted.length - 1];
          //splitted.splice(splitted.length - 1,1);
          splitted.splice(0, 1);
          var name = splitted.join('_');
          return {
              AutoRendered: false
      , Children: null
      , BinaryValue: null
      , Context: fld.XbrlDef.Context
      , ContextRef: fld.XbrlDef.ContextRef
      , Decimals: fld.XbrlDef.Decimals
      , Id: ''
      , Name: name
      , NameSpace: ""
      , Period: fld.XbrlDef.Period
      , Prefix: prefix
      , UnitRef: fld.XbrlDef.UnitRef
      , Value: value
          };
      }
      , createElementFromField: function(fld) {
          var value = fld.getValue();
          var xtype = fld.getXType();
          var el;
          if (xtype == 'combo') {
              var child = this.createComboValueElement(fld);
              el = {
                  AutoRendered: false
      , Children: child ? [child] : []
      , BinaryValue: null
      , Context: null
      , ContextRef: null
      , Decimals: null
      , Id: fld.XbrlDef.Prefix + '_' + fld.XbrlDef.Name
      , Name: fld.XbrlDef.Name
      , NameSpace: ""
      , Period: null
      , Prefix: fld.XbrlDef.Prefix
      , UnitRef: fld.XbrlDef.UnitRef
      , Value: null
              }
          } else {
              el = {
                  AutoRendered: false
      , Children: null
      , BinaryValue: xtype == 'biztax-uploadfield' ? Ext.decode(Ext.encode(value)) : null
      , Context: fld.XbrlDef.Context
      , ContextRef: fld.XbrlDef.ContextRef
      , Decimals: fld.XbrlDef.Decimals
      , Id: fld.XbrlDef.Id
      , Name: fld.XbrlDef.Name
      , NameSpace: ""
      , Period: fld.XbrlDef.Period
      , Prefix: fld.XbrlDef.Prefix
      , UnitRef: fld.XbrlDef.UnitRef
      , Value: xtype != 'biztax-uploadfield' ? "" + (value == null ? "" : value) : null
              };

          }
          return el;
      }
      , addElementByField: function(fld) {

          this.addElement(this.createElementFromField(fld));
      }
      , createStructuredElement: function(prefix, name) {
          return {
              AutoRendered: false
      , Children: []
      , Context: ''
      , ContextRef: ''
      , Decimals: null
      , Id: prefix + '_' + name
      , Name: name
      , NameSpace: ""
      , Period: ''
      , Prefix: prefix
      , UnitRef: ''
      , Value: null
          };
      }
      , getValueForField: function(fieldId, xtype) {
          var id = this.elementsByFields[fieldId];
          if (id == null) return null;
          if (id.indexOf('/') > -1) {
              // structured node
              var lst = id.split('/');
              var el = this.getElementById(lst[0]);
              if (el == null) return null;
              for (var i = 1; i < lst.length; i++) {
                  el = this.getElementChildById(el, lst[i]);
                  if (el == null) return null;
              }
              if (el == null) return null;
              return this.retrieveElementValue(el, xtype);
          }
          else {
              return this.getElementValueById(id, xtype);
          }
      }
    , getElementChildById: function(mainEl, id) {
        if (mainEl.Children == null) return null;
        if (!Ext.isArray(mainEl.Children)) return null;
        for (var j = 0; j < mainEl.Children.length; j++) {
            if (mainEl.Children[j].Id == id) return mainEl.Children[j];
        }
        return null;
    }
    , addContexts: function(contexts) {
        for (var i = 0; i < contexts.length; i++) {
            this.contexts[contexts[i].Id] = contexts[i];
        }
    }
    , createContext: function() { }
    , removeContexts: function(refs) {
        if (!Ext.isDefined(refs)) return;
        if (!Ext.isArray(refs)) refs = [refs];
        if (refs.length == 0) return;
        var args = [this.contexts];
        args = args.concat(refs);
        Ext.destroyMembers(args);
        for (var i = 0; i < refs.length; i++) {
            refs[i] = refs[i].replace('D__', '').replace('I-Start__', '').replace('I-End__', '');
        }
        refs = Ext.unique(refs);
        var els = [];
        for (var i = 0; i < refs.length; i++) {
            var arrs = this.elementsByContext[refs[i]];
            if (arrs != null) els = els.concat(arrs);
        }
        if (els.length == 0) return;

        els = Ext.unique(els).sort(function(a, b) { return b - a; }); // reversed to not affect indexes while removing

        for (var i = 0; i < els.length; i++) {
            this.elements.splice(els[i], 1);
        }
        //alert("reindex");
        this.indexElements();
    }
    , createElement: function() { }
    , getDataContract: function() {
        var dc = {};
        Ext.apply(dc, this.biztaxGeneral);
        dc.Elements = this.elements;
        dc.Contexts = [];
        for (var att in this.contexts) {
            if (!Ext.isFunction(this.contexts[att]) && this.contexts[att].Period) dc.Contexts.push(this.contexts[att]);
        }
        return dc;
    }
      , loadData: function(callback, scope) {
          eBook.CachedAjax.request({
              url: eBook.Service.rule2012 + 'GetBizTax'
      , method: 'POST'
      , params: Ext.encode({ cfdc: {
          FileId: eBook.Interface.currentFile.get('Id')
      , Culture: eBook.Interface.Culture
      }
      })
      , callback: this.onDataRetreived
      , scope: this
      , callerCallback: { fn: callback, scope: scope }
          });
      }
      , onDataRetreived: function(opts, success, resp) {
          if (!success) {
              alert("failed!");
              opts.callerCallback.fn.call(opts.callerCallback.scope || this);
              return null;
          }
          var robj = Ext.decode(resp.responseText);
          var dta = robj.GetBizTaxResult;
          this.clearData(); // clear existing data
          // copy global info
          Ext.copyTo(this.biztaxGeneral, dta, ["AssessmentYear", "EntityIdentifier", "Units"]);

          // load in contexts by Id
          this.contexts = {};
          for (var i = 0; i < dta.Contexts.length; i++) {
              this.contexts[dta.Contexts[i].Id] = dta.Contexts[i];
          }

          // load in elements 
          this.elements = dta.Elements;

          // index elements
          this.indexElements();

          opts.callerCallback.fn.call(opts.callerCallback.scope || this);

      }
    , addIdx: function(idxName, id, idx) {
        if (!this[idxName][id]) this[idxName][id] = [];
        this[idxName][id].push(idx);
    }
    , indexElements: function() {
        this.clearElementIndexes();
        for (var i = 0; i < this.elements.length; i++) {
            var el = this.elements[i];
            this.addElementToIndex(el, i);
        }
    }
    , addElement: function(el) {
        if (!this.elementsByNamePeriodContext[el.id]) {
            this.elements.push(el);
            this.addElementToIndex(el, this.elements.length - 1);
            return this.elements.length - 1;
        } else {
            var idx = this.elementsByNamePeriodContext[el.id];
            this.elements[idx] = el;
            return idx;
        }
    }
    , addElementToIndex: function(el, i) {
        var id = el.Id,
            fullName = el.Prefix + '_' + el.Name;

        // unique idx
        // id = fullName + '_' + el.Period + '_' + el.Context;
        this.addIdx('elementsByNamePeriodContext', id, i);

        // non-unique idx's
        this.addIdx('elementsByName', fullName, i);

        id = el.Period;
        this.addIdx('elementsByPeriod', id, i);

        id = fullName + '_' + el.Period;
        this.addIdx('elementsByNamePeriod', id, i);

        id = el.Context;
        this.addIdx('elementsByContext', id, i);

        id = fullName + '_' + el.Context;
        this.addIdx('elementsByNameContext', id, i);
    }
    , clearElementIndexes: function() {
        this.elementsByPeriod = {};
        this.elementsByNamePeriod = {};
        this.elementsByContext = {};
        this.elementsByNameContext = {};
        this.elementsByNamePeriodContext = {}; //unique
        this.elementsByName = {};
    }
    , clearData: function() {
        this.biztaxGeneral = {};
        this.contexts = {};
        this.elements = [];
        this.clearElementIndexes();
        //        this.elementsByFields = {};
        //        this.fieldsByElements = {};
    }
    , getElementsByIndexes: function(idxs) {
        var res = [];
        if (!Ext.isArray(idxs)) idxs = [idxs];
        for (var i = 0; i < idxs.length; i++) {
            res.push(this.elements[idxs[i]]);
        }
        return res;
    }
    , getElementValueById: function(id, xtype) {
        var el = this.getElementById(id);
        return this.retrieveElementValue(el, xtype);
    }
    , retrieveElementValue: function(el, xtype) {
        if (!el) return null;
        if (!Ext.isEmpty(el.Decimals)) {
            return Ext.isEmpty(el.Value) ? 0 : parseFloat(el.Value);
        }
        if (xtype == "biztax-uploadfield") {
            return el.BinaryValue;
        }
        if (xtype == "combo") {
            if (el.Children != null && el.Children.length > 0) {
                return el.Children[0].Prefix + '_' + el.Children[0].Name;
            }
        }
        //  if (el.Value == "true" || el.Value == "false") return eval(el.Value);
        return el.Value;
    }
      , getNumericElementValueById: function(id) {
          var val = this.getElementValueById(id);
          if (val == null) return 0;
          return val;
      }
      , addOrUpdateElement: function(cfg) {
          if (!this.elementsByNamePeriodContext[cfg.id]) {
              this.addElement(this.createElementByCalcCfg(cfg));
          } else {
              this.updateElement(cfg.id, cfg.value, '', null, true);
          }
      }
      , updateElement: function(idxorEl, value, xtype, fld, donotoverwrite) {
          if (Ext.isString(idxorEl)) {
              idxorEl = this.elementsByNamePeriodContext[idxorEl];
              if (idxorEl == null || !Ext.isDefined(idxorEl)) return;
          }
          if (Ext.isObject(idxorEl)) {
              // for child elements.
              if (idxorEl.AutoRendered && donotoverwrite) return;
              if (xtype == "biztax-uploadfield") {
                  idxorEl.BinaryValue = Ext.decode(Ext.encode(value));
                  idxorEl.Value = null;
              } if (xtype == "combo") {
                  var child = this.createComboValueElement(fld);
                  idxorEl.Children = child ? [child] : [];
              } else {
                  idxorEl.Value = "" + (value == null ? "" : value);
              }
              idxorEl.AutoRendered = false;
              return idxorEl;
          } else {
              if (this.elements[idxorEl].AutoRendered && donotoverwrite) return;
              if (xtype == "biztax-uploadfield") {
                  this.elements[idxorEl].BinaryValue = Ext.decode(Ext.encode(value));
                  this.elements[idxorEl].Value = null;
              } if (xtype == "combo") {
                  var child = this.createComboValueElement(value);
                  this.elements[idxorEl].Children = child ? [child] : [];
              } else {
                  this.elements[idxorEl].Value = "" + (value == null ? "" : value);
              }
              this.elements[idxorEl].AutoRendered = false;
          }
      }
      , findElementsByIndex: function(idxName, id) {
          if (!this[idxName]) return [];
          if (!this[idxName][id]) return [];
          return this.getElementsByIndexes(this[idxName][id]);
      }
      , getElementById: function(id) {
          var res = this.findElementsForId(id);
          if (res.length == 0) return null;
          return res[0];
      }
      , findElementsForId: function(id) {
          return this.findElementsByIndex('elementsByNamePeriodContext', id);
      }
      , findElementsForName: function(fullname) {
          return this.findElementsByIndex('elementsByName', fullname);
      }
      , findElementsForPeriod: function(period) {
          return this.findElementsByIndex('elementsByPeriod', period);
      }
      , findElementsForNamePeriod: function(fullname, period) {
          return this.findElementsByIndex('elementsByNamePeriod', fullname + '_' + period);
      }
      , findElementsForContext: function(context) {
          return this.findElementsByIndex('elementsByContext', context);
      }
      , findElementsForNameContext: function(fullname, context) {
          return this.findElementsByIndex('elementsByNameContext', fullname + '_' + context);
      }
      , getContextGridData: function(targetField) {
          var target = this.findElementsForName(targetField);
          if (target != null && target.length > 0) {
              var result = [];
              for (var i = 0; i < target.length; i++) {
                  var ctx = target[i].Context;
                  var context = this.contexts['D__' + ctx];
                  result.push({
                      elements: this.findElementsForContext(ctx)
                    , scenarios: context.Scenario
                    , context: ctx
                  });
              }

              return result;
          }
          return null;
      }
    , destroy: function() {
        delete this.biztaxGeneral;
        delete this.contexts;
        delete this.elements;
        delete this.elementsByPeriod;
        delete this.elementsByNamePeriod;
        delete this.elementsByContext;
        delete this.elementsByNameContext;
        delete this.elementsByNamePeriodContext;
        delete this.elementsByName;
        delete this.elementsByFields;
        delete this.fieldsByElements;
    }
};

self.getElIdx = function(elId, data) {
    if (data.elementsByNamePeriodContext[elId]) {
        return data.elementsByNamePeriodContext[elId];
    }
    return -1;
}

self.getEl = function(elId, data) {
    var idx = self.getElIdx(elId, data);
    if (idx > -1) {
        var idx = data.elementsByNamePeriodContext[elId];
        return data.elements[idx];
    }
    return null;
};

self.getElValue = function(elId, data, def) {
    var el = self.getEl(elId, data);
    return self.getValueOfEl(el, def);
};

self.getValueOfEl = function(el, def) {
    if (el) {
        var res = 0;
        try {
            res = parseFloat(el.Value);
        } catch (e) {
        }
        if (isNaN(res)) return def | 0;
        return res;
    } else {
        return def | 0;
    }
};

self.getSumOf = function(elPrefix, elName, testCtx, data) {
    var elId = elPrefix + '_' + elName;
    var idxs = data.elementsByName[elId];
    //return idxs;
    if (idxs != null) {
        var res = 0;
        for (var i = 0; i < idxs.length; i++) {
            if (testCtx) {
                if (data.elements[idxs[i]].ContextRef.indexOf(testCtx) > -1) {
                    res += self.getValueOfEl(data.elements[idxs[i]]);
                }
            } else {
                res += self.getValueOfEl(data.elements[idxs[i]]);
            }
        }
        return res;
    }
    return 0;
}

self.checkStore = function(result, store) {
    // context should always exist. (are fixed contexts only?)
    //    var contextId = results[i].period;
    //    if (results[i].context != '' && results[i].context != null) contextId += ('__' + results[i].context);
    //    if (!store.contexts[contextId]) {
    //        store.createContextByCfg(result);
    //    }
    if (store.elementsByNamePeriodContext[result.id] == undefined) {
        store.createElementByCalcCfg(result);
    }
    return store;
};


self.addEventListener('message', function(e) {
    var store = e.data.store, idx = -1;
    var results = [];
    var re = /(I-Start)/;
    Ext.apply(store, self.storeFunctions);


    res = { id: 'tax-inc_TaxableReserves_I-Start', prefix: 'tax-inc', name: 'TaxableReserves', period: 'I-Start', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_TaxableReservesCapitalSharePremiums_I-Start', store) + self.getElValue('tax-inc_TaxablePortionRevaluationSurpluses_I-Start', store) + self.getElValue('tax-inc_LegalReserve_I-Start', store) + self.getElValue('tax-inc_UnavailableReserves_I-Start', store) + self.getElValue('tax-inc_AvailableReserves_I-Start', store) + self.getElValue('tax-inc_AccumulatedProfitsLosses_I-Start', store) + self.getElValue('tax-inc_TaxableProvisions_I-Start', store) + self.getSumOf('tax-inc', 'OtherReserves', 'I-Start', store) + self.getSumOf('tax-inc', 'OtherTaxableReserves', 'I-Start', store) + self.getElValue('tax-inc_TaxableWriteDownsUndisclosedReserve_I-Start', store) + self.getElValue('tax-inc_ExaggeratedDepreciationsUndisclosedReserve_I-Start', store) + self.getElValue('tax-inc_OtherUnderestimationsAssetsUndisclosedReserve_I-Start', store) + self.getElValue('tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve_I-Start', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_TaxableReserves_I-End', prefix: 'tax-inc', name: 'TaxableReserves', period: 'I-End', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_TaxableReservesCapitalSharePremiums_I-End', store) + self.getElValue('tax-inc_TaxablePortionRevaluationSurpluses_I-End', store) + self.getElValue('tax-inc_LegalReserve_I-End', store) + self.getElValue('tax-inc_UnavailableReserves_I-End', store) + self.getElValue('tax-inc_AvailableReserves_I-End', store) + self.getElValue('tax-inc_AccumulatedProfitsLosses_I-End', store) + self.getElValue('tax-inc_TaxableProvisions_I-End', store) + self.getSumOf('tax-inc', 'OtherReserves', 'I-End', store) + self.getSumOf('tax-inc', 'OtherTaxableReserves', 'I-End', store) + self.getElValue('tax-inc_TaxableWriteDownsUndisclosedReserve_I-End', store) + self.getElValue('tax-inc_ExaggeratedDepreciationsUndisclosedReserve_I-End', store) + self.getElValue('tax-inc_OtherUnderestimationsAssetsUndisclosedReserve_I-End', store) + self.getElValue('tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve_I-End', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    var AdjustmentsReservesPlus = self.getElValue('tax-inc_CapitalGainsShares_D', store) + self.getElValue('tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus_D', store) + self.getElValue('tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus_D', store) + self.getElValue('tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus_D', store) + self.getElValue('tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement_D', store) + self.getElValue('tax-inc_OtherAdjustmentsReservesPlus_D', store);

    var AdjustmentsReservesMinus = self.getElValue('tax-inc_AdjustmentsReservesMinus_D', store);

    res = { id: 'tax-inc_TaxableReservesAfterAdjustments_I-Start', prefix: 'tax-inc', name: 'TaxableReservesAfterAdjustments', period: 'I-Start', context: '', value: Math.round(Math.pow(10, 2) * (AdjustmentsReservesMinus == 0 ? self.getElValue('tax-inc_TaxableReserves_I-Start', store) + AdjustmentsReservesPlus : self.getElValue('tax-inc_TaxableReserves_I-Start', store) + -AdjustmentsReservesMinus)) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_TaxableReservedProfit_D', prefix: 'tax-inc', name: 'TaxableReservedProfit', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_TaxableReserves_I-End', store) + -self.getElValue('tax-inc_TaxableReservesAfterAdjustments_I-Start', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_ExemptWriteDownDebtClaim_I-Start', prefix: 'tax-inc', name: 'ExemptWriteDownDebtClaim', period: 'I-Start', context: '', value: Math.round(Math.pow(10, 2) * (self.getSumOf('tax-inc', 'ExemptWriteDownDebtClaim', 'I-Start_', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_ExemptWriteDownDebtClaim_I-End', prefix: 'tax-inc', name: 'ExemptWriteDownDebtClaim', period: 'I-End', context: '', value: Math.round(Math.pow(10, 2) * (self.getSumOf('tax-inc', 'ExemptWriteDownDebtClaim', 'I-End_', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_ExemptProvisionRisksExpenses_I-Start', prefix: 'tax-inc', name: 'ExemptProvisionRisksExpenses', period: 'I-Start', context: '', value: Math.round(Math.pow(10, 2) * (self.getSumOf('tax-inc', 'ExemptProvisionRisksExpenses', 'I-Start_', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_ExemptProvisionRisksExpenses_I-End', prefix: 'tax-inc', name: 'ExemptProvisionRisksExpenses', period: 'I-End', context: '', value: Math.round(Math.pow(10, 2) * (self.getSumOf('tax-inc', 'ExemptProvisionRisksExpenses', 'I-End_', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_ExemptReservedProfit_I-Start', prefix: 'tax-inc', name: 'ExemptReservedProfit', period: 'I-Start', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_ExemptWriteDownDebtClaim_I-Start', store) + self.getElValue('tax-inc_ExemptProvisionRisksExpenses_I-Start', store) + self.getElValue('tax-inc_UnrealisedExpressedCapitalGainsExemptReserve_I-Start', store) + self.getElValue('tax-inc_CapitalGainsSpecificSecuritiesExemptReserve_I-Start', store) + self.getElValue('tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve_I-Start', store) + self.getElValue('tax-inc_CapitalGainsCorporateVehiclesExemptReserve_I-Start', store) + self.getElValue('tax-inc_CapitalGainsRiverVesselExemptReserve_I-Start', store) + self.getElValue('tax-inc_CapitalGainsSeaVesselExemptReserve_I-Start', store) + self.getElValue('tax-inc_OtherRealisedCapitalGainsExemptReserve_I-Start', store) + self.getElValue('tax-inc_ExemptInvestmentReserve_I-Start', store) + self.getElValue('tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve_I-Start', store) + self.getElValue('tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve_I-Start', store) + self.getElValue('tax-inc_OtherExemptReserves_I-Start', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_ExemptReservedProfit_I-End', prefix: 'tax-inc', name: 'ExemptReservedProfit', period: 'I-End', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_ExemptWriteDownDebtClaim_I-End', store) + self.getElValue('tax-inc_ExemptProvisionRisksExpenses_I-End', store) + self.getElValue('tax-inc_UnrealisedExpressedCapitalGainsExemptReserve_I-End', store) + self.getElValue('tax-inc_CapitalGainsSpecificSecuritiesExemptReserve_I-End', store) + self.getElValue('tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve_I-End', store) + self.getElValue('tax-inc_CapitalGainsCorporateVehiclesExemptReserve_I-End', store) + self.getElValue('tax-inc_CapitalGainsRiverVesselExemptReserve_I-End', store) + self.getElValue('tax-inc_CapitalGainsSeaVesselExemptReserve_I-End', store) + self.getElValue('tax-inc_OtherRealisedCapitalGainsExemptReserve_I-End', store) + self.getElValue('tax-inc_ExemptInvestmentReserve_I-End', store) + self.getElValue('tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve_I-End', store) + self.getElValue('tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve_I-End', store) + self.getElValue('tax-inc_OtherExemptReserves_I-End', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_DisallowedExpenses_D', prefix: 'tax-inc', name: 'DisallowedExpenses', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_NonDeductibleTaxes_D', store) + self.getElValue('tax-inc_NonDeductibleRegionalTaxesDutiesRetributions_D', store) + self.getElValue('tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind_D', store) + self.getElValue('tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums_D', store) + self.getElValue('tax-inc_NonDeductibleCarExpensesLossValuesCars_D', store) + self.getElValue('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind_D', store) + self.getElValue('tax-inc_NonDeductibleReceptionBusinessGiftsExpenses_D', store) + self.getElValue('tax-inc_NonDeductibleRestaurantExpenses_D', store) + self.getElValue('tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses_D', store) + self.getElValue('tax-inc_ExaggeratedInterests_D', store) + self.getElValue('tax-inc_NonDeductibleParticularPortionInterestsLoans_D', store) + self.getElValue('tax-inc_AbnormalBenevolentAdvantages_D', store) + self.getElValue('tax-inc_ProfitTransferBelgiumAbroad_D', store) + self.getElValue('tax-inc_NonDeductibleSocialAdvantages_D', store) + self.getElValue('tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers_D', store) + self.getElValue('tax-inc_Liberalities_D', store) + self.getElValue('tax-inc_WriteDownsLossValuesShares_D', store) + self.getElValue('tax-inc_ReversalPreviousExemptions_D', store) + self.getElValue('tax-inc_EmployeeParticipation_D', store) + self.getElValue('tax-inc_IndemnityMissingCoupon_D', store) + self.getElValue('tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork_D', store) + self.getElValue('tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies_D', store) + self.getElValue('tax-inc_NonDeductiblePaymentsCertainStates_D', store) + self.getElValue('tax-inc_OtherDisallowedExpenses_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_TaxableDividendsPaid_D', prefix: 'tax-inc', name: 'TaxableDividendsPaid', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_OrdinaryDividends_D', store) + self.getElValue('tax-inc_AcquisitionOwnShares_D', store) + self.getElValue('tax-inc_DeceaseDepartureExclusionPartner_D', store) + self.getElValue('tax-inc_DistributionCompanyAssets_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_FiscalResult_D', prefix: 'tax-inc', name: 'FiscalResult', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_TaxableReservedProfit_D', store) + self.getElValue('tax-inc_DisallowedExpenses_D', store) + self.getElValue('tax-inc_TaxableDividendsPaid_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_DeductionLimitElementsFiscalResult_D', prefix: 'tax-inc', name: 'DeductionLimitElementsFiscalResult', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind_D', store) + self.getElValue('tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve_D', store) + self.getElValue('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind_D', store) + self.getElValue('tax-inc_EmployeeParticipation_D', store) + self.getElValue('tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_ShippingResultNotTonnageBased_D', prefix: 'tax-inc', name: 'ShippingResultNotTonnageBased', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_FiscalResult_D', store) + -self.getElValue('tax-inc_ShippingResultTonnageBased_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_RemainingFiscalResultBeforeOriginDistribution_D', prefix: 'tax-inc', name: 'RemainingFiscalResultBeforeOriginDistribution', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_ShippingResultNotTonnageBased_D', store) + -self.getElValue('tax-inc_DeductionLimitElementsFiscalResult_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    var generalVariable_BalanceRemainingResultBelgium_AfterAllowanceInvestmentDeductionBelgium = self.getElValue('tax-inc_RemainingFiscalResult_D__BelgiumMember', store) + -self.getElValue('tax-inc_MiscellaneousExemptions_D__BelgiumMember', store) + -self.getElValue('tax-inc_PEExemptIncomeMovableAssets_D__BelgiumMember', store) + -self.getElValue('tax-inc_DeductionPatentsIncome_D__BelgiumMember', store) + -self.getElValue('tax-inc_AllowanceCorporateEquity_D__BelgiumMember', store) + -self.getElValue('tax-inc_CompensatedTaxLosses_D__BelgiumMember', store) + -self.getElValue('tax-inc_AllowanceInvestmentDeduction_D__BelgiumMember', store);

    var generalVariable_BalanceRemainingResultNoTaxTreatyMember = self.getElValue('tax-inc_RemainingFiscalResult_D__NoTaxTreatyMember', store) + -self.getElValue('tax-inc_MiscellaneousExemptions_D__NoTaxTreatyMember', store) + -self.getElValue('tax-inc_PEExemptIncomeMovableAssets_D__NoTaxTreatyMember', store) + -self.getElValue('tax-inc_DeductionPatentsIncome_D__NoTaxTreatyMember', store) + -self.getElValue('tax-inc_AllowanceCorporateEquity_D__NoTaxTreatyMember', store) + -self.getElValue('tax-inc_CompensatedTaxLosses_D__NoTaxTreatyMember', store);

    res = { id: 'tax-inc_RemainingFiscalProfitCommonRate_D__BelgiumMember', prefix: 'tax-inc', name: 'RemainingFiscalProfitCommonRate', period: 'D', context: 'BelgiumMember', value: Math.round(Math.pow(10, 2) * (generalVariable_BalanceRemainingResultBelgium_AfterAllowanceInvestmentDeductionBelgium > 0 ? generalVariable_BalanceRemainingResultBelgium_AfterAllowanceInvestmentDeductionBelgium : 0)) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_RemainingFiscalProfitCommonRate_D__NoTaxTreatyMember', prefix: 'tax-inc', name: 'RemainingFiscalProfitCommonRate', period: 'D', context: 'NoTaxTreatyMember', value: Math.round(Math.pow(10, 2) * (generalVariable_BalanceRemainingResultNoTaxTreatyMember > 0 ? generalVariable_BalanceRemainingResultNoTaxTreatyMember : 0)) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_RemainingFiscalProfitCommonRate_D', prefix: 'tax-inc', name: 'RemainingFiscalProfitCommonRate', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_RemainingFiscalProfitCommonRate_D__BelgiumMember', store) + self.getElValue('tax-inc_RemainingFiscalProfitCommonRate_D__NoTaxTreatyMember', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    var BasicTaxableAmountCommonRate_ItemsTotal = self.getElValue('tax-inc_RemainingFiscalProfitCommonRate_D', store) + self.getElValue('tax-inc_ShippingProfitTonnageBased_D', store) + self.getElValue('tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind_D', store) + self.getElValue('tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve_D', store) + self.getElValue('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind_D', store) + self.getElValue('tax-inc_EmployeeParticipation_D', store);

    var BasicTaxableAmountCommonRate_OtherTariffs = self.getElValue('tax-inc_BasicTaxableAmountExitTaxRate_D', store) + self.getElValue('tax-inc_CapitalGainsSharesRate2500_D', store);

    res = { id: 'tax-inc_BasicTaxableAmountCommonRate_D', prefix: 'tax-inc', name: 'BasicTaxableAmountCommonRate', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (BasicTaxableAmountCommonRate_OtherTariffs > 0 ? BasicTaxableAmountCommonRate_ItemsTotal + -BasicTaxableAmountCommonRate_OtherTariffs : BasicTaxableAmountCommonRate_ItemsTotal)) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_DeductibleMiscellaneousExemptions_D', prefix: 'tax-inc', name: 'DeductibleMiscellaneousExemptions', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_ExemptGifts_D', store) + self.getElValue('tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions_D', store) + self.getElValue('tax-inc_ExemptionTrainingPeriodBonus_D', store) + self.getElValue('tax-inc_OtherMiscellaneousExemptions_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_GrossPEExemptIncomeMovableAssets_D__BelgianBranchMember', prefix: 'tax-inc', name: 'GrossPEExemptIncomeMovableAssets', period: 'D', context: 'BelgianBranchMember', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_OtherExemptIncomeMovableAssets_D__BelgianBranchMember', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_GrossPEExemptIncomeMovableAssets_D__ForeignBranchMember', prefix: 'tax-inc', name: 'GrossPEExemptIncomeMovableAssets', period: 'D', context: 'ForeignBranchMember', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_OtherExemptIncomeMovableAssets_D__ForeignBranchMember', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_NetPEExemptIncomeMovableAssets_D__BelgianBranchMember', prefix: 'tax-inc', name: 'NetPEExemptIncomeMovableAssets', period: 'D', context: 'BelgianBranchMember', value: Math.round(Math.pow(10, 2) * (Math.max((self.getElValue('tax-inc_GrossPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + -self.getElValue('tax-inc_ExpensesSharesPEExemptIncomeMovableAssets_D__BelgianBranchMember', store)), (0)))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_NetPEExemptIncomeMovableAssets_D__ForeignBranchMember', prefix: 'tax-inc', name: 'NetPEExemptIncomeMovableAssets', period: 'D', context: 'ForeignBranchMember', value: Math.round(Math.pow(10, 2) * (Math.max((self.getElValue('tax-inc_GrossPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + -self.getElValue('tax-inc_ExpensesSharesPEExemptIncomeMovableAssets_D__ForeignBranchMember', store)), (0)))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_NetPEExemptIncomeMovableAssets_D__BelgianBranchMember', prefix: 'tax-inc', name: 'NetPEExemptIncomeMovableAssets', period: 'D', context: 'BelgianBranchMember', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_NetPEExemptIncomeMovableAssets_D__BelgianBranchMember', store) + self.getElValue('tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState_D__BelgianBranchMember', store) + self.getElValue('tax-inc_ExemptIncomeMovableAssetsRefinancingLoans_D__BelgianBranchMember', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_NetPEExemptIncomeMovableAssets_D__ForeignBranchMember', prefix: 'tax-inc', name: 'NetPEExemptIncomeMovableAssets', period: 'D', context: 'ForeignBranchMember', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_NetPEExemptIncomeMovableAssets_D__ForeignBranchMember', store) + self.getElValue('tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState_D__ForeignBranchMember', store) + self.getElValue('tax-inc_ExemptIncomeMovableAssetsRefinancingLoans_D__ForeignBranchMember', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets_D', prefix: 'tax-inc', name: 'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (Math.max((self.getElValue('tax-inc_AccumulatedPEExemptIncomeMovableAssets_D', store) + self.getElValue('tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod_D', store) + -self.getElValue('tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod_D', store)), (0)))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_Prepayments_D', prefix: 'tax-inc', name: 'Prepayments', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_PrepaymentFirstQuarter_D', store) + self.getElValue('tax-inc_PrepaymentSecondQuarter_D', store) + self.getElValue('tax-inc_PrepaymentThirdQuarter_D', store) + self.getElValue('tax-inc_PrepaymentFourthQuarter_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_NonRepayableAdvanceLevies_D', prefix: 'tax-inc', name: 'NonRepayableAdvanceLevies', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_NonRepayableFictiousWitholdingTax_D', store) + self.getElValue('tax-inc_NonRepayableLumpSumForeignTaxes_D', store) + self.getElValue('tax-inc_TaxCreditResearchDevelopment_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_RepayableAdvanceLevies_D', prefix: 'tax-inc', name: 'RepayableAdvanceLevies', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium_D', store) + self.getElValue('tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign_D', store) + self.getElValue('tax-inc_RepayableWithholdingTaxOtherPEForeign_D', store) + self.getElValue('tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares_D', store) + self.getElValue('tax-inc_RepayableWithholdingTaxOtherDividends_D', store) + self.getElValue('tax-inc_OtherRepayableWithholdingTaxes_D', store) + self.getElValue('tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty_D', store) + self.getElValue('tax-inc_WithholdingTaxEarnedIncomeIncomePartner_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_DeductionsEquityAllowanceCorporateEquity_I-Start', prefix: 'tax-inc', name: 'DeductionsEquityAllowanceCorporateEquity', period: 'I-Start', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_OwnSharesFiscalValue_I-Start', store) + self.getElValue('tax-inc_FinancialFixedAssetsParticipationsOtherShares_I-Start', store) + self.getElValue('tax-inc_SharesInvestmentCorporations_I-Start', store) + self.getElValue('tax-inc_BranchesCountryTaxTreaty_I-Start', store) + self.getElValue('tax-inc_ImmovablePropertyCountryTaxTreaty_I-Start', store) + self.getElValue('tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts_I-Start', store) + self.getElValue('tax-inc_InvestmentsNoPeriodicalIncome_I-Start', store) + self.getElValue('tax-inc_ImmovablePropertyUseManager_I-Start', store) + self.getElValue('tax-inc_UnrealisedExpressedCapitalGains_I-Start', store) + self.getElValue('tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity_I-Start', store) + self.getElValue('tax-inc_InvestmentGrants_I-Start', store) + self.getElValue('tax-inc_ActualisationStockRecognisedDiamondTraders_I-Start', store) + self.getElValue('tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch_I-Start', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity_D', prefix: 'tax-inc', name: 'MovementEquityAfterDeductionsAllowanceCorporateEquity', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getSumOf('tax-inc', 'MovementEquityAfterDeductionsAllowanceCorporateEquity', 'D_', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_AllowanceCorporateEquityCurrentTaxPeriod_D', prefix: 'tax-inc', name: 'AllowanceCorporateEquityCurrentTaxPeriod', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_Equity_I-Start', store) + -self.getElValue('tax-inc_DeductionsEquityAllowanceCorporateEquity_I-Start', store) + self.getElValue('tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    res = { id: 'tax-inc_AllowanceCorporateEquity_D', prefix: 'tax-inc', name: 'AllowanceCorporateEquity', period: 'D', context: '', value: Math.round(Math.pow(10, 2) * (self.getElValue('tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear_D', store) + self.getElValue('tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears_D', store))) / Math.pow(10, 2) };
    store.addOrUpdateElement(res);
    results.push(res);

    var res = { result: results, scopeId: e.data.scopeId };
    self.postMessage(res);
}, false);
    