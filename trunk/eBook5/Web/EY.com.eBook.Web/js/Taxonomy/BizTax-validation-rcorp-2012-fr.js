
    Ext = {

    /**
    * The version of the framework
    * @type String
    */
    version: '3.2.1',
    versionDetail: {
    major: 3,
    minor: 2,
    patch: 1
    }
    };

    Ext.apply = function(o, c, defaults) {
    // no "this" reference for friendly out of scope calls
    if (defaults) {
    Ext.apply(o, defaults);
    }
    if (o && c && typeof c == 'object') {
    for (var p in c) {
    o[p] = c[p];
    }
    }
    return o;
    };


    Ext.apply(Ext, {

    USE_NATIVE_JSON: false,
    applyIf: function(o, c) {
    if (o) {
    for (var p in c) {
    if (!Ext.isDefined(o[p])) {
    o[p] = c[p];
    }
    }
    }
    return o;
    },
    extend: function() {
    // inline overrides
    var io = function(o) {
    for (var m in o) {
    this[m] = o[m];
    }
    };
    var oc = Object.prototype.constructor;

    return function(sb, sp, overrides) {
    if (typeof sp == 'object') {
    overrides = sp;
    sp = sb;
    sb = overrides.constructor != oc ? overrides.constructor : function() { sp.apply(this, arguments); };
    }
    var F = function() { },
    sbp,
    spp = sp.prototype;

    F.prototype = spp;
    sbp = sb.prototype = new F();
    sbp.constructor = sb;
    sb.superclass = spp;
    if (spp.constructor == oc) {
    spp.constructor = sp;
    }
    sb.override = function(o) {
    Ext.override(sb, o);
    };
    sbp.superclass = sbp.supr = (function() {
    return spp;
    });
    sbp.override = io;
    Ext.override(sb, overrides);
    sb.extend = function(o) { return Ext.extend(sb, o); };
    return sb;
    };
    } (),

    override: function(origclass, overrides) {
    if (overrides) {
    var p = origclass.prototype;
    Ext.apply(p, overrides);
    if (false && overrides.hasOwnProperty('toString')) {
    p.toString = overrides.toString;
    }
    }
    },
    toArray: function() {
    return false ?
    function(a, i, j, res) {
    res = [];
    for (var x = 0, len = a.length; x < len; x++) {
    res.push(a[x]);
    }
    return res.slice(i || 0, j || res.length);
    } :
    function(a, i, j) {
    return Array.prototype.slice.call(a, i || 0, j || a.length);
    }
    } (),


    isEmpty: function(v, allowBlank) {
    return v === null || v === undefined || ((Ext.isArray(v) && !v.length)) || (!allowBlank ? v === '' : false);
    },
    isArray: function(v) {
    return toString.apply(v) === '[object Array]';
    },
    isDate: function(v) {
    return toString.apply(v) === '[object Date]';
    },
    isObject: function(v) {
    return !!v && Object.prototype.toString.call(v) === '[object Object]';
    },
    isPrimitive: function(v) {
    return Ext.isString(v) || Ext.isNumber(v) || Ext.isBoolean(v);
    },
    isFunction: function(v) {
    return toString.apply(v) === '[object Function]';
    },
    isNumber: function(v) {
    return typeof v === 'number' && isFinite(v);
    },
    isString: function(v) {
    return typeof v === 'string';
    },
    isBoolean: function(v) {
    return typeof v === 'boolean';
    },
    isElement: function(v) {
    return v ? !!v.tagName : false;
    },
    isDefined: function(v) {
    return typeof v !== 'undefined';
    },


    /**
    * True if the detected browser is Opera.
    * @type Boolean
    */
    isOpera: false,

    /**
    * True if the detected browser uses WebKit.
    * @type Boolean
    */
    isWebKit: true,

    /**
    * True if the detected browser is Chrome.
    * @type Boolean
    */
    isChrome: true,

    /**
    * True if the detected browser is Safari.
    * @type Boolean
    */
    isSafari: false,

    /**
    * True if the detected browser is Safari 3.x.
    * @type Boolean
    */
    isSafari3: false,

    /**
    * True if the detected browser is Safari 4.x.
    * @type Boolean
    */
    isSafari4: false,

    /**
    * True if the detected browser is Safari 2.x.
    * @type Boolean
    */
    isSafari2: false,

    /**
    * True if the detected browser is Internet Explorer.
    * @type Boolean
    */
    isIE: false,

    /**
    * True if the detected browser is Internet Explorer 6.x.
    * @type Boolean
    */
    isIE6: false,

    /**
    * True if the detected browser is Internet Explorer 7.x.
    * @type Boolean
    */
    isIE7: false,

    /**
    * True if the detected browser is Internet Explorer 8.x.
    * @type Boolean
    */
    isIE8: false,

    /**
    * True if the detected browser uses the Gecko layout engine (e.g. Mozilla, Firefox).
    * @type Boolean
    */
    isGecko: false,

    /**
    * True if the detected browser uses a pre-Gecko 1.9 layout engine (e.g. Firefox 2.x).
    * @type Boolean
    */
    isGecko2: false,

    /**
    * True if the detected browser uses a Gecko 1.9+ layout engine (e.g. Firefox 3.x).
    * @type Boolean
    */
    isGecko3: false,

    /**
    * True if the detected browser is Internet Explorer running in non-strict mode.
    * @type Boolean
    */
    isBorderBox: false,

    /**
    * True if the detected platform is Linux.
    * @type Boolean
    */
    isLinux: false,

    /**
    * True if the detected platform is Windows.
    * @type Boolean
    */
    isWindows: true,

    /**
    * True if the detected platform is Mac OS.
    * @type Boolean
    */
    isMac: false,

    /**
    * True if the detected platform is Adobe Air.
    * @type Boolean
    */
    isAir: false
    });


    self.storeFunctions = {
    registerField: function(fieldId, elementId, path, xtype) {
    var eid = path ? xtype != 'combo' ? path + '/' + elementId : path : elementId;
    if (!this.elementsByFields[fieldId]) this.elementsByFields[fieldId] = eid;
    if (!this.fieldsByElements[elementId]) this.fieldsByElements[elementId] = [];
    this.fieldsByElements[elementId].push(fieldId);
    return this.getElementValueById(elementId, xtype);
    }
    , setValueByField: function(fld) {
    var id = this.elementsByFields[fld.id];
    if (id.indexOf('/') > -1) {
    // structured node
    var xtype = fld.getXType();
    var lst = id.split('/');
    var mi = this.elementsByNamePeriodContext[lst[0]];
    if (!mi) {
    var nmsplit = lst[0].split('_');
    this.addElement(this.createStructuredElement(nmsplit[0], nmsplit[1]));
    mi = this.elementsByNamePeriodContext[lst[0]];
    }
    var midx = mi[0];
    var el = this.elements[midx];
    for (var i = 1; i < lst.length; i++) {
    if (el.Children == null) el.Children = [];
    var nel = this.getElementChildById(el, lst[i]);
    if (nel == null) {
    if (i < lst.length - 1) {
    var its = lst[i].split('_');
    nel = this.createStructuredElement(its[0], its[1]);
    el.Children.push(nel);

    } else {
    if (xtype == 'combo') {
    var its = lst[i].split('_');
    nel = this.createStructuredElement(its[0], its[1]);
    el.Children.push(nel);
    } else {
    nel = this.createElementFromField(fld);
    el.Children.push(nel);
    }
    }
    }
    el = nel;
    }
    if (el == null) return null;
    this.updateElement(el, fld.getValue(), xtype, fld);
    return this.retrieveElementValue(el);
    }
    else {

    if (id == null) {
    // NEW CREATION
    this.addElementByField(fld);
    return;
    }
    var idx = this.elementsByNamePeriodContext[id];
    if (idx) {
    this.updateElement(idx[0], fld.getValue(), fld.getXType(), fld);
    } else {
    this.addElementByField(fld);
    return;
    }
    }
    }
    , createElementByCalcCfg: function(cfg) {
    var cref = cfg.period;
    if (cfg.context != null && cfg.context != '') cref += ('__' + cref.context);
    var el = {
    AutoRendered: false
    , Children: null
    , Context: cfg.context
    , ContextRef: cref
    , Decimals: "INF" // to determine?
    , Id: cfg.id
    , Name: cfg.name
    , NameSpace: ""
    , Period: cfg.period
    , Prefix: cfg.prefix
    , UnitRef: "EUR" // to determine?
    , Value: "" + (cfg.value == null ? "" : cfg.value)
    };
    return el;
    }
    , createComboValueElement: function(fld) {

    var val = fld.getValue(); // is id like pfs-vl_XCode_LegalFormCode_001
    if (Ext.isEmpty(val)) return null;
    var splitted = val.split('_');
    var prefix = splitted[0];
    var value = splitted[splitted.length - 1];
    //splitted.splice(splitted.length - 1,1);
    splitted.splice(0, 1);
    var name = splitted.join('_');
    return {
    AutoRendered: false
    , Children: null
    , BinaryValue: null
    , Context: fld.XbrlDef.Context
    , ContextRef: fld.XbrlDef.ContextRef
    , Decimals: fld.XbrlDef.Decimals
    , Id: ''
    , Name: name
    , NameSpace: ""
    , Period: fld.XbrlDef.Period
    , Prefix: prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: value
    };
    }
    , createElementFromField: function(fld) {
    var value = fld.getValue();
    var xtype = fld.getXType();
    var el;
    if (xtype == 'combo') {
    var child = this.createComboValueElement(fld);
    el = {
    AutoRendered: false
    , Children: child ? [child] : []
    , BinaryValue: null
    , Context: null
    , ContextRef: null
    , Decimals: null
    , Id: fld.XbrlDef.Prefix + '_' + fld.XbrlDef.Name
    , Name: fld.XbrlDef.Name
    , NameSpace: ""
    , Period: null
    , Prefix: fld.XbrlDef.Prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: null
    }
    } else {
    el = {
    AutoRendered: false
    , Children: null
    , BinaryValue: xtype == 'biztax-uploadfield' ? Ext.decode(Ext.encode(value)) : null
    , Context: fld.XbrlDef.Context
    , ContextRef: fld.XbrlDef.ContextRef
    , Decimals: fld.XbrlDef.Decimals
    , Id: fld.XbrlDef.Id
    , Name: fld.XbrlDef.Name
    , NameSpace: ""
    , Period: fld.XbrlDef.Period
    , Prefix: fld.XbrlDef.Prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: xtype != 'biztax-uploadfield' ? "" + (value == null ? "" : value) : null
    };

    }
    return el;
    }
    , addElementByField: function(fld) {

    this.addElement(this.createElementFromField(fld));
    }
    , createStructuredElement: function(prefix, name) {
    return {
    AutoRendered: false
    , Children: []
    , Context: ''
    , ContextRef: ''
    , Decimals: null
    , Id: prefix + '_' + name
    , Name: name
    , NameSpace: ""
    , Period: ''
    , Prefix: prefix
    , UnitRef: ''
    , Value: null
    };
    }
    , getValueForField: function(fieldId, xtype) {
    var id = this.elementsByFields[fieldId];
    if (id == null) return null;
    if (id.indexOf('/') > -1) {
    // structured node
    var lst = id.split('/');
    var el = this.getElementById(lst[0]);
    if (el == null) return null;
    for (var i = 1; i < lst.length; i++) {
    el = this.getElementChildById(el, lst[i]);
    if (el == null) return null;
    }
    if (el == null) return null;
    return this.retrieveElementValue(el, xtype);
    }
    else {
    return this.getElementValueById(id, xtype);
    }
    }
    , getElementChildById: function(mainEl, id) {
    if (mainEl.Children == null) return null;
    if (!Ext.isArray(mainEl.Children)) return null;
    for (var j = 0; j < mainEl.Children.length; j++) {
    if (mainEl.Children[j].Id == id) return mainEl.Children[j];
    }
    return null;
    }
    , addContexts: function(contexts) {
    for (var i = 0; i < contexts.length; i++) {
    this.contexts[contexts[i].Id] = contexts[i];
    }
    }
    , createContext: function() { }
    , removeContexts: function(refs) {
    if (!Ext.isDefined(refs)) return;
    if (!Ext.isArray(refs)) refs = [refs];
    if (refs.length == 0) return;
    var args = [this.contexts];
    args = args.concat(refs);
    Ext.destroyMembers(args);
    for (var i = 0; i < refs.length; i++) {
    refs[i] = refs[i].replace('D__', '').replace('I-Start__', '').replace('I-End__', '');
    }
    refs = Ext.unique(refs);
    var els = [];
    for (var i = 0; i < refs.length; i++) {
    var arrs = this.elementsByContext[refs[i]];
    if (arrs != null) els = els.concat(arrs);
    }
    if (els.length == 0) return;

    els = Ext.unique(els).sort(function(a, b) { return b - a; }); // reversed to not affect indexes while removing

    for (var i = 0; i < els.length; i++) {
    this.elements.splice(els[i], 1);
    }
    //alert("reindex");
    this.indexElements();
    }
    , createElement: function() { }
    , getDataContract: function() {
    var dc = {};
    Ext.apply(dc, this.biztaxGeneral);
    dc.Elements = this.elements;
    dc.Contexts = [];
    for (var att in this.contexts) {
    if (!Ext.isFunction(this.contexts[att]) && this.contexts[att].Period) dc.Contexts.push(this.contexts[att]);
    }
    return dc;
    }
    , loadData: function(callback, scope) {
    eBook.CachedAjax.request({
    url: eBook.Service.rule2012 + 'GetBizTax'
    , method: 'POST'
    , params: Ext.encode({ cfdc: {
    FileId: eBook.Interface.currentFile.get('Id')
    , Culture: eBook.Interface.Culture
    }
    })
    , callback: this.onDataRetreived
    , scope: this
    , callerCallback: { fn: callback, scope: scope }
    });
    }
    , onDataRetreived: function(opts, success, resp) {
    if (!success) {
    alert("failed!");
    opts.callerCallback.fn.call(opts.callerCallback.scope || this);
    return null;
    }
    var robj = Ext.decode(resp.responseText);
    var dta = robj.GetBizTaxResult;
    this.clearData(); // clear existing data
    // copy global info
    Ext.copyTo(this.biztaxGeneral, dta, ["AssessmentYear", "EntityIdentifier", "Units"]);

    // load in contexts by Id
    this.contexts = {};
    for (var i = 0; i < dta.Contexts.length; i++) {
    this.contexts[dta.Contexts[i].Id] = dta.Contexts[i];
    }

    // load in elements
    this.elements = dta.Elements;

    // index elements
    this.indexElements();

    opts.callerCallback.fn.call(opts.callerCallback.scope || this);

    }
    , addIdx: function(idxName, id, idx) {
    if (!this[idxName][id]) this[idxName][id] = [];
    this[idxName][id].push(idx);
    }
    , indexElements: function() {
    this.clearElementIndexes();
    for (var i = 0; i < this.elements.length; i++) {
    var el = this.elements[i];
    this.addElementToIndex(el, i);
    }
    }
    , addElement: function(el) {
    if (!this.elementsByNamePeriodContext[el.id]) {
    this.elements.push(el);
    this.addElementToIndex(el, this.elements.length - 1);
    return this.elements.length - 1;
    } else {
    var idx = this.elementsByNamePeriodContext[el.id];
    this.elements[idx] = el;
    return idx;
    }
    }
    , addElementToIndex: function(el, i) {
    var id = el.Id,
    fullName = el.Prefix + '_' + el.Name;

    // unique idx
    // id = fullName + '_' + el.Period + '_' + el.Context;
    this.addIdx('elementsByNamePeriodContext', id, i);

    // non-unique idx's
    this.addIdx('elementsByName', fullName, i);

    id = el.Period;
    this.addIdx('elementsByPeriod', id, i);

    id = fullName + '_' + el.Period;
    this.addIdx('elementsByNamePeriod', id, i);

    id = el.Context;
    this.addIdx('elementsByContext', id, i);

    id = fullName + '_' + el.Context;
    this.addIdx('elementsByNameContext', id, i);
    }
    , clearElementIndexes: function() {
    this.elementsByPeriod = {};
    this.elementsByNamePeriod = {};
    this.elementsByContext = {};
    this.elementsByNameContext = {};
    this.elementsByNamePeriodContext = {}; //unique
    this.elementsByName = {};
    }
    , clearData: function() {
    this.biztaxGeneral = {};
    this.contexts = {};
    this.elements = [];
    this.clearElementIndexes();
    //        this.elementsByFields = {};
    //        this.fieldsByElements = {};
    }
    , getElementsByIndexes: function(idxs) {
    var res = [];
    if (!Ext.isArray(idxs)) idxs = [idxs];
    for (var i = 0; i < idxs.length; i++) {
    res.push(this.elements[idxs[i]]);
    }
    return res;
    }
    , getElementValueById: function(id, xtype) {
    var el = this.getElementById(id);
    return this.retrieveElementValue(el, xtype);
    }
    , retrieveElementValue: function(el, xtype) {
    if (!el) return null;
    if (!Ext.isEmpty(el.Decimals)) {
    return Ext.isEmpty(el.Value) ? 0 : parseFloat(el.Value);
    }
    if (xtype == "biztax-uploadfield") {
    return el.BinaryValue;
    }
    if (xtype == "combo") {
    if (el.Children != null && el.Children.length > 0) {
    return el.Children[0].Prefix + '_' + el.Children[0].Name;
    }
    }
    //  if (el.Value == "true" || el.Value == "false") return eval(el.Value);
    return el.Value;
    }
    , getNumericElementValueById: function(id) {
    var val = this.getElementValueById(id);
    if (val == null) return 0;
    return val;
    }
    , addOrUpdateElement: function(cfg) {
    if (!this.elementsByNamePeriodContext[cfg.id]) {
    this.addElement(this.createElementByCalcCfg(cfg));
    } else {
    this.updateElement(cfg.id, cfg.value, '', null, true);
    }
    }
    , updateElement: function(idxorEl, value, xtype, fld, donotoverwrite) {
    if (Ext.isString(idxorEl)) {
    idxorEl = this.elementsByNamePeriodContext[idxorEl];
    if (idxorEl == null || !Ext.isDefined(idxorEl)) return;
    }
    if (Ext.isObject(idxorEl)) {
    // for child elements.
    if (idxorEl.AutoRendered && donotoverwrite) return;
    if (xtype == "biztax-uploadfield") {
    idxorEl.BinaryValue = Ext.decode(Ext.encode(value));
    idxorEl.Value = null;
    } if (xtype == "combo") {
    var child = this.createComboValueElement(fld);
    idxorEl.Children = child ? [child] : [];
    } else {
    idxorEl.Value = "" + (value == null ? "" : value);
    }
    idxorEl.AutoRendered = false;
    return idxorEl;
    } else {
    if (this.elements[idxorEl].AutoRendered && donotoverwrite) return;
    if (xtype == "biztax-uploadfield") {
    this.elements[idxorEl].BinaryValue = Ext.decode(Ext.encode(value));
    this.elements[idxorEl].Value = null;
    } if (xtype == "combo") {
    var child = this.createComboValueElement(value);
    this.elements[idxorEl].Children = child ? [child] : [];
    } else {
    this.elements[idxorEl].Value = "" + (value == null ? "" : value);
    }
    this.elements[idxorEl].AutoRendered = false;
    }
    }
    , findElementsByIndex: function(idxName, id) {
    if (!this[idxName]) return [];
    if (!this[idxName][id]) return [];
    return this.getElementsByIndexes(this[idxName][id]);
    }
    , getElementById: function(id) {
    var res = this.findElementsForId(id);
    if (res.length == 0) return null;
    return res[0];
    }
    , findElementsForId: function(id) {
    return this.findElementsByIndex('elementsByNamePeriodContext', id);
    }
    , findElementsForName: function(fullname) {
    return this.findElementsByIndex('elementsByName', fullname);
    }
    , findElementsForPeriod: function(period) {
    return this.findElementsByIndex('elementsByPeriod', period);
    }
    , findElementsForNamePeriod: function(fullname, period) {
    return this.findElementsByIndex('elementsByNamePeriod', fullname + '_' + period);
    }
    , findElementsForContext: function(context) {
    return this.findElementsByIndex('elementsByContext', context);
    }
    , findElementsForNameContext: function(fullname, context) {
    return this.findElementsByIndex('elementsByNameContext', fullname + '_' + context);
    }
    , getContextGridData: function(targetField) {
    var target = this.findElementsForName(targetField);
    if (target != null && target.length > 0) {
    var result = [];
    for (var i = 0; i < target.length; i++) {
    var ctx = target[i].Context;
    var context = this.contexts['D__' + ctx];
    result.push({
    elements: this.findElementsForContext(ctx)
    , scenarios: context.Scenario
    , context: ctx
    });
    }

    return result;
    }
    return null;
    }
    , destroy: function() {
    delete this.biztaxGeneral;
    delete this.contexts;
    delete this.elements;
    delete this.elementsByPeriod;
    delete this.elementsByNamePeriod;
    delete this.elementsByContext;
    delete this.elementsByNameContext;
    delete this.elementsByNamePeriodContext;
    delete this.elementsByName;
    delete this.elementsByFields;
    delete this.fieldsByElements;
    }

    };

    self.getElIdx = function(elId, data) {
    if (data.elementsByNamePeriodContext[elId]) {
    return data.elementsByNamePeriodContext[elId];
    }
    return -1;
    }

    self.getEl = function(elId, data) {
    var idx = self.getElIdx(elId, data);
    if (idx>-1) {
    var idx = data.elementsByNamePeriodContext[elId];
    return data.elements[idx];
    }
    return null;
    };

    self.getElValue = function(elId, data, def) {
    var el = self.getEl(elId, data);
    return self.getValueOfEl(el,def);
    };

    self.getValueOfEl = function(el, def) {
    if (el) {
    var res = 0;
    try {
    res = parseFloat(el.Value);
    } catch (e) {
    }
    if (isNaN(res)) return def | 0;
    return res;
    } else {
    return def | 0;
    }
    };

    self.getSumOf = function(elPrefix, elName, testCtx, data) {
    var elId = elPrefix + '_' + elName;
    var idxs = data.elementsByName[elId];
    //return idxs;
    if (idxs != null) {
    var res = 0;
    for (var i = 0; i < idxs.length; i++) {
    if (testCtx) {
    if (data.elements[idxs[i]].ContextRef.indexOf(testCtx) > -1) {
    res += self.getValueOfEl(data.elements[idxs[i]]);
    }
    } else {
    res += self.getValueOfEl(data.elements[idxs[i]]);
    }
    }
    return res;
    }
    return 0;
    }

    self.checkStore = function(result, store) {
    // context should always exist. (are fixed contexts only?)
    //    var contextId = results[i].period;
    //    if (results[i].context != '' && results[i].context != null) contextId += ('__' + results[i].context);
    //    if (!store.contexts[contextId]) {
    //        store.createContextByCfg(result);
    //    }
    if (store.elementsByNamePeriodContext[result.id] == undefined) {
    store.createElementByCalcCfg(result);
    }
    return store;
    };

    self.getElementValidationValue = function(el,tpe) {
      var res;
      if(tpe!='binary' && Ext.isEmpty(el.Value)) return null;
      if(tpe=='binary' && (Ext.isEmpty(el.BinaryValue) || !Ext.isArray(el.BinaryValue) || (Ext.isArray(el.BinaryValue) && el.BinaryValue.length==0))) return null;
      switch(tpe) {
        case 'binary':
          res= el.BinaryValue;
        case 'numeric':
          try {
            res = parseFloat(el.Value);
          } catch (e) {
            res = null;
          }
          break;
        case 'text':
          res = el.Value;
          break;
        case 'bool':
          res = el.Value=='true';
          break;
        case 'date':
          try {
            res = new Date(Date.parse(el.Value));
          } catch(e) {
            return null;
          }
          break;
      }
      return res;
    };


    self.addEventListener('message', function(e) {
    var store = e.data.store, idx = -1, i=0, value=null, valid=true;
    var results = [], fields=[];
    var re = /(I-Start)/;
    Ext.apply(store,self.storeFunctions);
    var msgs= [],fieldMsgs={};

    
    fields = store.findElementsForName('tax-inc_TaxableReservesCapitalSharePremiums');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesCapitalSharePremiums', fieldName:'Réserves incorporées au capital et primes d’émission imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'])  {
            fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesCapitalSharePremiums', fieldName:'Réserves incorporées au capital et primes d’émission imposables', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'])  {
            fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesCapitalSharePremiums', fieldName:'Réserves incorporées au capital et primes d’émission imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'])  {
            fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesCapitalSharePremiums'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxablePortionRevaluationSurpluses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxablePortionRevaluationSurpluses', fieldName:'Quotité imposable des plus-values de réévaluation', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'])  {
            fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] = '';
        }
        fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxablePortionRevaluationSurpluses', fieldName:'Quotité imposable des plus-values de réévaluation', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'])  {
            fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] = '';
        }
        fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxablePortionRevaluationSurpluses', fieldName:'Quotité imposable des plus-values de réévaluation', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'])  {
            fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] = '';
        }
        fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_LegalReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'LegalReserve', fieldName:'Réserve légale', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_LegalReserve'])  {
            fieldMsgs['tax-inc_LegalReserve'] = '';
        }
        fieldMsgs['tax-inc_LegalReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'LegalReserve', fieldName:'Réserve légale', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_LegalReserve'])  {
            fieldMsgs['tax-inc_LegalReserve'] = '';
        }
        fieldMsgs['tax-inc_LegalReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'LegalReserve', fieldName:'Réserve légale', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_LegalReserve'])  {
            fieldMsgs['tax-inc_LegalReserve'] = '';
        }
        fieldMsgs['tax-inc_LegalReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_UnavailableReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'UnavailableReserves', fieldName:'Réserves indisponibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_UnavailableReserves'])  {
            fieldMsgs['tax-inc_UnavailableReserves'] = '';
        }
        fieldMsgs['tax-inc_UnavailableReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'UnavailableReserves', fieldName:'Réserves indisponibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_UnavailableReserves'])  {
            fieldMsgs['tax-inc_UnavailableReserves'] = '';
        }
        fieldMsgs['tax-inc_UnavailableReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'UnavailableReserves', fieldName:'Réserves indisponibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_UnavailableReserves'])  {
            fieldMsgs['tax-inc_UnavailableReserves'] = '';
        }
        fieldMsgs['tax-inc_UnavailableReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AvailableReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AvailableReserves', fieldName:'Réserves disponibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AvailableReserves'])  {
            fieldMsgs['tax-inc_AvailableReserves'] = '';
        }
        fieldMsgs['tax-inc_AvailableReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AvailableReserves', fieldName:'Réserves disponibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AvailableReserves'])  {
            fieldMsgs['tax-inc_AvailableReserves'] = '';
        }
        fieldMsgs['tax-inc_AvailableReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AvailableReserves', fieldName:'Réserves disponibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AvailableReserves'])  {
            fieldMsgs['tax-inc_AvailableReserves'] = '';
        }
        fieldMsgs['tax-inc_AvailableReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AccumulatedProfitsLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedProfitsLosses', fieldName:'Bénéfice (Perte) reporté(e)', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AccumulatedProfitsLosses'])  {
            fieldMsgs['tax-inc_AccumulatedProfitsLosses'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedProfitsLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedProfitsLosses', fieldName:'Bénéfice (Perte) reporté(e)', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_AccumulatedProfitsLosses'])  {
            fieldMsgs['tax-inc_AccumulatedProfitsLosses'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedProfitsLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedProfitsLosses', fieldName:'Bénéfice (Perte) reporté(e)', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AccumulatedProfitsLosses'])  {
            fieldMsgs['tax-inc_AccumulatedProfitsLosses'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedProfitsLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableProvisions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableProvisions', fieldName:'Provisions imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableProvisions'])  {
            fieldMsgs['tax-inc_TaxableProvisions'] = '';
        }
        fieldMsgs['tax-inc_TaxableProvisions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableProvisions', fieldName:'Provisions imposables', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableProvisions'])  {
            fieldMsgs['tax-inc_TaxableProvisions'] = '';
        }
        fieldMsgs['tax-inc_TaxableProvisions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableProvisions', fieldName:'Provisions imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableProvisions'])  {
            fieldMsgs['tax-inc_TaxableProvisions'] = '';
        }
        fieldMsgs['tax-inc_TaxableProvisions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherReserves', fieldName:'Autres réserves figurant au bilan', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherReserves'])  {
            fieldMsgs['tax-inc_OtherReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherReserves', fieldName:'Autres réserves figurant au bilan', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherReserves'])  {
            fieldMsgs['tax-inc_OtherReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherReserves', fieldName:'Autres réserves figurant au bilan', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherReserves'])  {
            fieldMsgs['tax-inc_OtherReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherTaxableReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherTaxableReserves', fieldName:'Autres réserves imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherTaxableReserves'])  {
            fieldMsgs['tax-inc_OtherTaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherTaxableReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherTaxableReserves', fieldName:'Autres réserves imposables', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherTaxableReserves'])  {
            fieldMsgs['tax-inc_OtherTaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherTaxableReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherTaxableReserves', fieldName:'Autres réserves imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherTaxableReserves'])  {
            fieldMsgs['tax-inc_OtherTaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherTaxableReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableWriteDownsUndisclosedReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableWriteDownsUndisclosedReserve', fieldName:'Réductions de valeur imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableWriteDownsUndisclosedReserve', fieldName:'Réductions de valeur imposables', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableWriteDownsUndisclosedReserve', fieldName:'Réductions de valeur imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExaggeratedDepreciationsUndisclosedReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciationsUndisclosedReserve', fieldName:'Excédents d’amortissements', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciationsUndisclosedReserve', fieldName:'Excédents d’amortissements', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciationsUndisclosedReserve', fieldName:'Excédents d’amortissements', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherUnderestimationsAssetsUndisclosedReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherUnderestimationsAssetsUndisclosedReserve', fieldName:'Autres sous-évaluations d’actif', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherUnderestimationsAssetsUndisclosedReserve', fieldName:'Autres sous-évaluations d’actif', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherUnderestimationsAssetsUndisclosedReserve', fieldName:'Autres sous-évaluations d’actif', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherOverestimationsLiabilitiesUndisclosedReserve', fieldName:'Surestimations du passif', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherOverestimationsLiabilitiesUndisclosedReserve', fieldName:'Surestimations du passif', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherOverestimationsLiabilitiesUndisclosedReserve', fieldName:'Surestimations du passif', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReserves', fieldName:'Réserves imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReserves'])  {
            fieldMsgs['tax-inc_TaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_TaxableReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReserves', fieldName:'Réserves imposables', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReserves'])  {
            fieldMsgs['tax-inc_TaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_TaxableReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReserves', fieldName:'Réserves imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReserves'])  {
            fieldMsgs['tax-inc_TaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_TaxableReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsShares', fieldName:'Plus-values sur actions ou parts', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsShares'])  {
            fieldMsgs['tax-inc_CapitalGainsShares'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsShares', fieldName:'Plus-values sur actions ou parts', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsShares'])  {
            fieldMsgs['tax-inc_CapitalGainsShares'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsShares', fieldName:'Plus-values sur actions ou parts', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsShares'])  {
            fieldMsgs['tax-inc_CapitalGainsShares'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus', fieldName:'Reprises de réductions de valeur sur actions ou parts antérieurement imposées à titre de dépenses non admises', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus', fieldName:'Reprises de réductions de valeur sur actions ou parts antérieurement imposées à titre de dépenses non admises', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus', fieldName:'Reprises de réductions de valeur sur actions ou parts antérieurement imposées à titre de dépenses non admises', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus', fieldName:'Exonération définitive œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus', fieldName:'Exonération définitive œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus', fieldName:'Exonération définitive œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus', fieldName:'Exonération des primes et subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus', fieldName:'Exonération des primes et subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus', fieldName:'Exonération des primes et subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement', fieldName:'Exonération définitive des bénéfices provenant de l\'homologation d\'un plan de réorganisation et de la constatation d\'un accord amiable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'])  {
            fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] = '';
        }
        fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement', fieldName:'Exonération définitive des bénéfices provenant de l\'homologation d\'un plan de réorganisation et de la constatation d\'un accord amiable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'])  {
            fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] = '';
        }
        fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement', fieldName:'Exonération définitive des bénéfices provenant de l\'homologation d\'un plan de réorganisation et de la constatation d\'un accord amiable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'])  {
            fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] = '';
        }
        fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherAdjustmentsReservesPlus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherAdjustmentsReservesPlus', fieldName:'Autres', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'])  {
            fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherAdjustmentsReservesPlus', fieldName:'Autres', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'])  {
            fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherAdjustmentsReservesPlus', fieldName:'Autres', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'])  {
            fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AdjustmentsReservesMinus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AdjustmentsReservesMinus', fieldName:'Diminutions de la situation de début des réserves', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AdjustmentsReservesMinus'])  {
            fieldMsgs['tax-inc_AdjustmentsReservesMinus'] = '';
        }
        fieldMsgs['tax-inc_AdjustmentsReservesMinus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AdjustmentsReservesMinus', fieldName:'Diminutions de la situation de début des réserves', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AdjustmentsReservesMinus'])  {
            fieldMsgs['tax-inc_AdjustmentsReservesMinus'] = '';
        }
        fieldMsgs['tax-inc_AdjustmentsReservesMinus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AdjustmentsReservesMinus', fieldName:'Diminutions de la situation de début des réserves', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AdjustmentsReservesMinus'])  {
            fieldMsgs['tax-inc_AdjustmentsReservesMinus'] = '';
        }
        fieldMsgs['tax-inc_AdjustmentsReservesMinus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReservesAfterAdjustments');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesAfterAdjustments', fieldName:'Réserves imposables après adaptation de la situation de début des réserves', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'])  {
            fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesAfterAdjustments', fieldName:'Réserves imposables après adaptation de la situation de début des réserves', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'])  {
            fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesAfterAdjustments', fieldName:'Réserves imposables après adaptation de la situation de début des réserves', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'])  {
            fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReservedProfit');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Bénéfices réservés imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Bénéfices réservés imposables', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Bénéfices réservés imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptWriteDownDebtClaim');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Réduction de valeur exonérée', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Réduction de valeur exonérée', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Réduction de valeur exonérée', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptProvisionRisksExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Provision exonérée', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Provision exonérée', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Provision exonérée', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_UnrealisedExpressedCapitalGainsExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGainsExemptReserve', fieldName:'Plus-values exprimées mais non réalisées', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGainsExemptReserve', fieldName:'Plus-values exprimées mais non réalisées', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGainsExemptReserve', fieldName:'Plus-values exprimées mais non réalisées', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsSpecificSecuritiesExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSpecificSecuritiesExemptReserve', fieldName:'Taxation étalée des plus-values sur certains titres', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSpecificSecuritiesExemptReserve', fieldName:'Taxation étalée des plus-values sur certains titres', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSpecificSecuritiesExemptReserve', fieldName:'Taxation étalée des plus-values sur certains titres', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsTangibleIntangibleFixedAssetsExemptReserve', fieldName:'Taxation étalée des plus-values sur immobilisations corporelles et incorporelles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsTangibleIntangibleFixedAssetsExemptReserve', fieldName:'Taxation étalée des plus-values sur immobilisations corporelles et incorporelles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsTangibleIntangibleFixedAssetsExemptReserve', fieldName:'Taxation étalée des plus-values sur immobilisations corporelles et incorporelles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherRealisedCapitalGainsExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherRealisedCapitalGainsExemptReserve', fieldName:'Autres plus-values réalisées', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherRealisedCapitalGainsExemptReserve', fieldName:'Autres plus-values réalisées', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherRealisedCapitalGainsExemptReserve', fieldName:'Autres plus-values réalisées', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsCorporateVehiclesExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsCorporateVehiclesExemptReserve', fieldName:'Plus-values sur véhicules d’entreprises', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsCorporateVehiclesExemptReserve', fieldName:'Plus-values sur véhicules d’entreprises', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsCorporateVehiclesExemptReserve', fieldName:'Plus-values sur véhicules d’entreprises', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsRiverVesselExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsRiverVesselExemptReserve', fieldName:'Plus-values sur bateaux de navigation intérieure', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsRiverVesselExemptReserve', fieldName:'Plus-values sur bateaux de navigation intérieure', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsRiverVesselExemptReserve', fieldName:'Plus-values sur bateaux de navigation intérieure', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsSeaVesselExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSeaVesselExemptReserve', fieldName:'Plus-values sur navires', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSeaVesselExemptReserve', fieldName:'Plus-values sur navires', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSeaVesselExemptReserve', fieldName:'Plus-values sur navires', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptInvestmentReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptInvestmentReserve', fieldName:'Réserve d’investissement', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptInvestmentReserve'])  {
            fieldMsgs['tax-inc_ExemptInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ExemptInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptInvestmentReserve', fieldName:'Réserve d’investissement', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptInvestmentReserve'])  {
            fieldMsgs['tax-inc_ExemptInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ExemptInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptInvestmentReserve', fieldName:'Réserve d’investissement', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptInvestmentReserve'])  {
            fieldMsgs['tax-inc_ExemptInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ExemptInvestmentReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxShelterAuthorisedAudiovisualWorkExemptReserve', fieldName:'Œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'])  {
            fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxShelterAuthorisedAudiovisualWorkExemptReserve', fieldName:'Œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'])  {
            fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxShelterAuthorisedAudiovisualWorkExemptReserve', fieldName:'Œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'])  {
            fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve', fieldName:'Bénéfices provenant de l\'homologation d\'un plan de réorganisation et de la constatation d\'un accord amiable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'])  {
            fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve', fieldName:'Bénéfices provenant de l\'homologation d\'un plan de réorganisation et de la constatation d\'un accord amiable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'])  {
            fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve', fieldName:'Bénéfices provenant de l\'homologation d\'un plan de réorganisation et de la constatation d\'un accord amiable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'])  {
            fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherExemptReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptReserves', fieldName:'Autres éléments exonérés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherExemptReserves'])  {
            fieldMsgs['tax-inc_OtherExemptReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptReserves', fieldName:'Autres éléments exonérés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherExemptReserves'])  {
            fieldMsgs['tax-inc_OtherExemptReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptReserves', fieldName:'Autres éléments exonérés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherExemptReserves'])  {
            fieldMsgs['tax-inc_OtherExemptReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptReservedProfit');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptReservedProfit', fieldName:'Bénéfices réservés exonérés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptReservedProfit'])  {
            fieldMsgs['tax-inc_ExemptReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_ExemptReservedProfit'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptReservedProfit', fieldName:'Bénéfices réservés exonérés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptReservedProfit'])  {
            fieldMsgs['tax-inc_ExemptReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_ExemptReservedProfit'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptReservedProfit', fieldName:'Bénéfices réservés exonérés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptReservedProfit'])  {
            fieldMsgs['tax-inc_ExemptReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_ExemptReservedProfit'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Impôts non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Impôts non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Impôts non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleRegionalTaxesDutiesRetributions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Impôts, taxes et rétributions régionaux', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Impôts, taxes et rétributions régionaux', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Impôts, taxes et rétributions régionaux', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Amendes, pénalités et confiscations de toute nature', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Amendes, pénalités et confiscations de toute nature', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Amendes, pénalités et confiscations de toute nature', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Pensions, capitaux, cotisations et primes patronales non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Pensions, capitaux, cotisations et primes patronales non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Pensions, capitaux, cotisations et primes patronales non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesLossValuesCars');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Frais de voiture et moins-values sur véhicules automobiles non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Frais de voiture et moins-values sur véhicules automobiles non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Frais de voiture et moins-values sur véhicules automobiles non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleReceptionBusinessGiftsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Frais de réception et de cadeaux d’affaires non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Frais de réception et de cadeaux d’affaires non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Frais de réception et de cadeaux d’affaires non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleRestaurantExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Frais de restaurant non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Frais de restaurant non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Frais de restaurant non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Frais de vêtements professionnels non spécifiques', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Frais de vêtements professionnels non spécifiques', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Frais de vêtements professionnels non spécifiques', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExaggeratedInterests');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Intérêts exagérés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Intérêts exagérés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Intérêts exagérés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleParticularPortionInterestsLoans');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Intérêts relatifs à une partie de certains emprunts', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Intérêts relatifs à une partie de certains emprunts', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Intérêts relatifs à une partie de certains emprunts', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AbnormalBenevolentAdvantages');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Avantages anormaux ou bénévoles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Avantages anormaux ou bénévoles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Avantages anormaux ou bénévoles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleSocialAdvantages');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Avantages sociaux', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Avantages sociaux', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Avantages sociaux', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Avantages de titres-repas, chèques sport/culture ou éco-chèques', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Avantages de titres-repas, chèques sport/culture ou éco-chèques', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Avantages de titres-repas, chèques sport/culture ou éco-chèques', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_Liberalities');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Libéralités', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Libéralités', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Libéralités', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WriteDownsLossValuesShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Réductions de valeur et moins-values sur actions ou parts', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Réductions de valeur et moins-values sur actions ou parts', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Réductions de valeur et moins-values sur actions ou parts', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ReversalPreviousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Reprises d’exonérations antérieures', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Reprises d’exonérations antérieures', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Reprises d’exonérations antérieures', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EmployeeParticipation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IndemnityMissingCoupon');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Indemnités pour coupon manquant', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Indemnités pour coupon manquant', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Indemnités pour coupon manquant', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Frais d\'œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Frais d\'œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Frais d\'œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Primes, subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Primes, subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Primes, subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductiblePaymentsCertainStates');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Paiements non déductibles vers certains Etats', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Paiements non déductibles vers certains Etats', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Paiements non déductibles vers certains Etats', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherDisallowedExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Autres dépenses non admises', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Autres dépenses non admises', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Autres dépenses non admises', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DisallowedExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Dépenses non admises', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Dépenses non admises', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Dépenses non admises', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OrdinaryDividends');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OrdinaryDividends', fieldName:'Dividendes ordinaires', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OrdinaryDividends'])  {
            fieldMsgs['tax-inc_OrdinaryDividends'] = '';
        }
        fieldMsgs['tax-inc_OrdinaryDividends'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OrdinaryDividends', fieldName:'Dividendes ordinaires', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OrdinaryDividends'])  {
            fieldMsgs['tax-inc_OrdinaryDividends'] = '';
        }
        fieldMsgs['tax-inc_OrdinaryDividends'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OrdinaryDividends', fieldName:'Dividendes ordinaires', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OrdinaryDividends'])  {
            fieldMsgs['tax-inc_OrdinaryDividends'] = '';
        }
        fieldMsgs['tax-inc_OrdinaryDividends'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AcquisitionOwnShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionOwnShares', fieldName:'Acquisition d’actions ou parts propres', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_AcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionOwnShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionOwnShares', fieldName:'Acquisition d’actions ou parts propres', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_AcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionOwnShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionOwnShares', fieldName:'Acquisition d’actions ou parts propres', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_AcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionOwnShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeceaseDepartureExclusionPartner');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeceaseDepartureExclusionPartner', fieldName:'Décès, démission ou exclusion d’un associé', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'])  {
            fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'] = '';
        }
        fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeceaseDepartureExclusionPartner', fieldName:'Décès, démission ou exclusion d’un associé', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'])  {
            fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'] = '';
        }
        fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeceaseDepartureExclusionPartner', fieldName:'Décès, démission ou exclusion d’un associé', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'])  {
            fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'] = '';
        }
        fieldMsgs['tax-inc_DeceaseDepartureExclusionPartner'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DistributionCompanyAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DistributionCompanyAssets', fieldName:'Partage de l’avoir social', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DistributionCompanyAssets'])  {
            fieldMsgs['tax-inc_DistributionCompanyAssets'] = '';
        }
        fieldMsgs['tax-inc_DistributionCompanyAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DistributionCompanyAssets', fieldName:'Partage de l’avoir social', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DistributionCompanyAssets'])  {
            fieldMsgs['tax-inc_DistributionCompanyAssets'] = '';
        }
        fieldMsgs['tax-inc_DistributionCompanyAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DistributionCompanyAssets', fieldName:'Partage de l’avoir social', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DistributionCompanyAssets'])  {
            fieldMsgs['tax-inc_DistributionCompanyAssets'] = '';
        }
        fieldMsgs['tax-inc_DistributionCompanyAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableDividendsPaid');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableDividendsPaid', fieldName:'Dividendes distribués', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableDividendsPaid'])  {
            fieldMsgs['tax-inc_TaxableDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_TaxableDividendsPaid'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableDividendsPaid', fieldName:'Dividendes distribués', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableDividendsPaid'])  {
            fieldMsgs['tax-inc_TaxableDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_TaxableDividendsPaid'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableDividendsPaid', fieldName:'Dividendes distribués', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableDividendsPaid'])  {
            fieldMsgs['tax-inc_TaxableDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_TaxableDividendsPaid'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReservedProfit');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Bénéfices réservés imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Bénéfices réservés imposables', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Bénéfices réservés imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DisallowedExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Dépenses non admises', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Dépenses non admises', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Dépenses non admises', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableDividendsPaid');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableDividendsPaid', fieldName:'Dividendes distribués', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableDividendsPaid'])  {
            fieldMsgs['tax-inc_TaxableDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_TaxableDividendsPaid'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableDividendsPaid', fieldName:'Dividendes distribués', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableDividendsPaid'])  {
            fieldMsgs['tax-inc_TaxableDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_TaxableDividendsPaid'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableDividendsPaid', fieldName:'Dividendes distribués', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableDividendsPaid'])  {
            fieldMsgs['tax-inc_TaxableDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_TaxableDividendsPaid'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_FiscalResult');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'FiscalResult', fieldName:'Résultat de la période imposable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_FiscalResult'])  {
            fieldMsgs['tax-inc_FiscalResult'] = '';
        }
        fieldMsgs['tax-inc_FiscalResult'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'FiscalResult', fieldName:'Résultat de la période imposable', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_FiscalResult'])  {
            fieldMsgs['tax-inc_FiscalResult'] = '';
        }
        fieldMsgs['tax-inc_FiscalResult'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'FiscalResult', fieldName:'Résultat de la période imposable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_FiscalResult'])  {
            fieldMsgs['tax-inc_FiscalResult'] = '';
        }
        fieldMsgs['tax-inc_FiscalResult'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ShippingResultTonnageBased');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultTonnageBased', fieldName:'Résultat effectif des activités de la navigation maritime, pour lesquelles le bénéfice est déterminé sur base du tonnage', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ShippingResultTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultTonnageBased'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultTonnageBased', fieldName:'Résultat effectif des activités de la navigation maritime, pour lesquelles le bénéfice est déterminé sur base du tonnage', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingResultTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultTonnageBased'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultTonnageBased', fieldName:'Résultat effectif des activités de la navigation maritime, pour lesquelles le bénéfice est déterminé sur base du tonnage', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingResultTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultTonnageBased'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ShippingResultNotTonnageBased');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultNotTonnageBased', fieldName:'Résultat effectif des activités pour lesquelles le bénéfice n’est pas déterminé sur base du tonnage', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ShippingResultNotTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultNotTonnageBased', fieldName:'Résultat effectif des activités pour lesquelles le bénéfice n’est pas déterminé sur base du tonnage', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingResultNotTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultNotTonnageBased', fieldName:'Résultat effectif des activités pour lesquelles le bénéfice n’est pas déterminé sur base du tonnage', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingResultNotTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Avantages anormaux ou bénévoles obtenus et avantages financiers ou de toute nature obtenus', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Avantages anormaux ou bénévoles obtenus et avantages financiers ou de toute nature obtenus', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Avantages anormaux ou bénévoles obtenus et avantages financiers ou de toute nature obtenus', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Non-respect de l’obligation d’investir ou de la condition d’intangibilité relatives à la réserve d’investissement', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Non-respect de l’obligation d’investir ou de la condition d’intangibilité relatives à la réserve d’investissement', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Non-respect de l’obligation d’investir ou de la condition d’intangibilité relatives à la réserve d’investissement', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EmployeeParticipation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupport', fieldName:'Subsides en capital et en intérêts dans le cadre de l\'aide à l\'agriculture', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupport', fieldName:'Subsides en capital et en intérêts dans le cadre de l\'aide à l\'agriculture', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupport', fieldName:'Subsides en capital et en intérêts dans le cadre de l\'aide à l\'agriculture', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RemainingFiscalResultBeforeOriginDistribution');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResultBeforeOriginDistribution', fieldName:'Résultat subsistant', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'])  {
            fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResultBeforeOriginDistribution', fieldName:'Résultat subsistant', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'])  {
            fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResultBeforeOriginDistribution', fieldName:'Résultat subsistant', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'])  {
            fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RemainingFiscalResult');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResult', fieldName:'Résultat subsistant suivant sa provenance', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResult'])  {
            fieldMsgs['tax-inc_RemainingFiscalResult'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResult'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResult', fieldName:'Résultat subsistant suivant sa provenance', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResult'])  {
            fieldMsgs['tax-inc_RemainingFiscalResult'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResult'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResult', fieldName:'Résultat subsistant suivant sa provenance', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResult'])  {
            fieldMsgs['tax-inc_RemainingFiscalResult'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResult'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_MiscellaneousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'MiscellaneousExemptions', fieldName:'Eléments non imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_MiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_MiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_MiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'MiscellaneousExemptions', fieldName:'Eléments non imposables', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_MiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_MiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_MiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'MiscellaneousExemptions', fieldName:'Eléments non imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_MiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_MiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_MiscellaneousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductionPatentsIncome');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductionPatentsIncome', fieldName:'Déduction pour revenus de brevets', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductionPatentsIncome'])  {
            fieldMsgs['tax-inc_DeductionPatentsIncome'] = '';
        }
        fieldMsgs['tax-inc_DeductionPatentsIncome'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductionPatentsIncome', fieldName:'Déduction pour revenus de brevets', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductionPatentsIncome'])  {
            fieldMsgs['tax-inc_DeductionPatentsIncome'] = '';
        }
        fieldMsgs['tax-inc_DeductionPatentsIncome'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductionPatentsIncome', fieldName:'Déduction pour revenus de brevets', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductionPatentsIncome'])  {
            fieldMsgs['tax-inc_DeductionPatentsIncome'] = '';
        }
        fieldMsgs['tax-inc_DeductionPatentsIncome'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Déduction pour capital à risque déduite au cours de l\'exercice d\'imposition actuel', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Déduction pour capital à risque déduite au cours de l\'exercice d\'imposition actuel', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Déduction pour capital à risque déduite au cours de l\'exercice d\'imposition actuel', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensatedTaxLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Pertes récupérées', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Pertes récupérées', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Pertes récupérées', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AllowanceInvestmentDeduction');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AllowanceInvestmentDeduction', fieldName:'Déduction pour investissement', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AllowanceInvestmentDeduction'])  {
            fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] = '';
        }
        fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AllowanceInvestmentDeduction', fieldName:'Déduction pour investissement', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AllowanceInvestmentDeduction'])  {
            fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] = '';
        }
        fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AllowanceInvestmentDeduction', fieldName:'Déduction pour investissement', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AllowanceInvestmentDeduction'])  {
            fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] = '';
        }
        fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RemainingFiscalProfitCommonRate');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Bénéfice subsistant suivant sa provenance', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Bénéfice subsistant suivant sa provenance', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Bénéfice subsistant suivant sa provenance', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RemainingFiscalProfitCommonRate');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Bénéfice subsistant suivant sa provenance', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Bénéfice subsistant suivant sa provenance', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Bénéfice subsistant suivant sa provenance', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ShippingProfitTonnageBased');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ShippingProfitTonnageBased', fieldName:'Bénéfice provenant de la navigation maritime, déterminé sur base du tonnage', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ShippingProfitTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingProfitTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingProfitTonnageBased'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ShippingProfitTonnageBased', fieldName:'Bénéfice provenant de la navigation maritime, déterminé sur base du tonnage', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ShippingProfitTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingProfitTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingProfitTonnageBased'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingProfitTonnageBased', fieldName:'Bénéfice provenant de la navigation maritime, déterminé sur base du tonnage', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingProfitTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingProfitTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingProfitTonnageBased'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Avantages anormaux ou bénévoles obtenus et avantages financiers ou de toute nature obtenus', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Avantages anormaux ou bénévoles obtenus et avantages financiers ou de toute nature obtenus', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Avantages anormaux ou bénévoles obtenus et avantages financiers ou de toute nature obtenus', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Non-respect de l’obligation d’investir ou de la condition d’intangibilité relatives à la réserve d’investissement', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Non-respect de l’obligation d’investir ou de la condition d’intangibilité relatives à la réserve d’investissement', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Non-respect de l’obligation d’investir ou de la condition d’intangibilité relatives à la réserve d’investissement', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EmployeeParticipation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsSharesRate2500');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesRate2500', fieldName:'Plus-values sur actions ou parts imposables à 25%', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesRate2500'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesRate2500', fieldName:'Plus-values sur actions ou parts imposables à 25%', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesRate2500'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesRate2500', fieldName:'Plus-values sur actions ou parts imposables à 25%', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesRate2500'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BasicTaxableAmountExitTaxRate');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BasicTaxableAmountExitTaxRate', fieldName:'Imposable au taux de l\'exit tax (16,5%)', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'])  {
            fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] = '';
        }
        fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BasicTaxableAmountExitTaxRate', fieldName:'Imposable au taux de l\'exit tax (16,5%)', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'])  {
            fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] = '';
        }
        fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BasicTaxableAmountExitTaxRate', fieldName:'Imposable au taux de l\'exit tax (16,5%)', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'])  {
            fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] = '';
        }
        fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500', fieldName:'Subsides en capital et en intérêts dans le cadre de l\'aide à l\'agriculture, imposables à 5 %', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500', fieldName:'Subsides en capital et en intérêts dans le cadre de l\'aide à l\'agriculture, imposables à 5 %', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500', fieldName:'Subsides en capital et en intérêts dans le cadre de l\'aide à l\'agriculture, imposables à 5 %', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind', fieldName:'Dépenses ou avantages de toute nature non justifiés, bénéfices dissimulés et avantages financiers ou de toute nature', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind', fieldName:'Dépenses ou avantages de toute nature non justifiés, bénéfices dissimulés et avantages financiers ou de toute nature', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind', fieldName:'Dépenses ou avantages de toute nature non justifiés, bénéfices dissimulés et avantages financiers ou de toute nature', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions', fieldName:'Cotisation distincte sur les réserves taxées dans le chef des sociétés de crédit agréées', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'])  {
            fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'] = '';
        }
        fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions', fieldName:'Cotisation distincte sur les réserves taxées dans le chef des sociétés de crédit agréées', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'])  {
            fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'] = '';
        }
        fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions', fieldName:'Cotisation distincte sur les réserves taxées dans le chef des sociétés de crédit agréées', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'])  {
            fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'] = '';
        }
        fieldMsgs['tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation', fieldName:'Cotisation distincte dans le chef des sociétés admises à offrir des crédits à l\'outillage artisanal et des sociétés de logement sur les dividendes distribués', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'])  {
            fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'] = '';
        }
        fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation', fieldName:'Cotisation distincte dans le chef des sociétés admises à offrir des crédits à l\'outillage artisanal et des sociétés de logement sur les dividendes distribués', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'])  {
            fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'] = '';
        }
        fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation', fieldName:'Cotisation distincte dans le chef des sociétés admises à offrir des crédits à l\'outillage artisanal et des sociétés de logement sur les dividendes distribués', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'])  {
            fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'] = '';
        }
        fieldMsgs['tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300', fieldName:'Partage total ou partiel de l’avoir social, imposable à 33%', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300', fieldName:'Partage total ou partiel de l’avoir social, imposable à 33%', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300', fieldName:'Partage total ou partiel de l’avoir social, imposable à 33%', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650', fieldName:'Partage total ou partiel de l’avoir social, imposable à 16,5%', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650', fieldName:'Partage total ou partiel de l’avoir social, imposable à 16,5%', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650', fieldName:'Partage total ou partiel de l’avoir social, imposable à 16,5%', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation', fieldName:'Avantages de toute nature accordés par des sociétés en liquidation', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation', fieldName:'Avantages de toute nature accordés par des sociétés en liquidation', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation', fieldName:'Avantages de toute nature accordés par des sociétés en liquidation', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'])  {
            fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'] = '';
        }
        fieldMsgs['tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AdditionalDutiesDiamondTraders');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AdditionalDutiesDiamondTraders', fieldName:'Cotisation supplémentaire des diamantaires agréés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'])  {
            fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AdditionalDutiesDiamondTraders', fieldName:'Cotisation supplémentaire des diamantaires agréés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'])  {
            fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AdditionalDutiesDiamondTraders', fieldName:'Cotisation supplémentaire des diamantaires agréés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'])  {
            fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RetributionTaxCreditResearchDevelopment');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RetributionTaxCreditResearchDevelopment', fieldName:'Remboursement d\'une quotité du crédit d\'impôt pour recherche et développement antérieurement accordé', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RetributionTaxCreditResearchDevelopment', fieldName:'Remboursement d\'une quotité du crédit d\'impôt pour recherche et développement antérieurement accordé', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RetributionTaxCreditResearchDevelopment', fieldName:'Remboursement d\'une quotité du crédit d\'impôt pour recherche et développement antérieurement accordé', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptGifts');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptGifts', fieldName:'Libéralités exonérées', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptGifts'])  {
            fieldMsgs['tax-inc_ExemptGifts'] = '';
        }
        fieldMsgs['tax-inc_ExemptGifts'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptGifts', fieldName:'Libéralités exonérées', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptGifts'])  {
            fieldMsgs['tax-inc_ExemptGifts'] = '';
        }
        fieldMsgs['tax-inc_ExemptGifts'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptGifts', fieldName:'Libéralités exonérées', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptGifts'])  {
            fieldMsgs['tax-inc_ExemptGifts'] = '';
        }
        fieldMsgs['tax-inc_ExemptGifts'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelMiscellaneousExemptions', fieldName:'Exonération pour personnel supplémentaire', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelMiscellaneousExemptions', fieldName:'Exonération pour personnel supplémentaire', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelMiscellaneousExemptions', fieldName:'Exonération pour personnel supplémentaire', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptionAdditionalPersonnelSMEs');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelSMEs', fieldName:'Exonération pour personnel supplémentaire PME', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelSMEs', fieldName:'Exonération pour personnel supplémentaire PME', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelSMEs', fieldName:'Exonération pour personnel supplémentaire PME', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptionTrainingPeriodBonus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionTrainingPeriodBonus', fieldName:'Exonération pour bonus de tutorat', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'])  {
            fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionTrainingPeriodBonus', fieldName:'Exonération pour bonus de tutorat', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'])  {
            fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionTrainingPeriodBonus', fieldName:'Exonération pour bonus de tutorat', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'])  {
            fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherMiscellaneousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherMiscellaneousExemptions', fieldName:'Autres éléments non imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherMiscellaneousExemptions', fieldName:'Autres éléments non imposables', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherMiscellaneousExemptions', fieldName:'Autres éléments non imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductibleMiscellaneousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductibleMiscellaneousExemptions', fieldName:'Eléments non imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductibleMiscellaneousExemptions', fieldName:'Eléments non imposables', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductibleMiscellaneousExemptions', fieldName:'Eléments non imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus belges', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus belges', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus belges', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus belges', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus belges', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus belges', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus étrangers', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus étrangers', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus étrangers', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus étrangers', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus étrangers', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus étrangers', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus belges', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus belges', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus belges', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus belges', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus belges', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus belges', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus étrangers', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus étrangers', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Montant net, revenus étrangers', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus étrangers', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus étrangers', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Précompte mobilier, revenus étrangers', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptIncomeMovableAssets', fieldName:'Autres revenus mobiliers exonérés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptIncomeMovableAssets', fieldName:'Autres revenus mobiliers exonérés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptIncomeMovableAssets', fieldName:'Autres revenus mobiliers exonérés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_GrossPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'GrossPEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés avant déduction des frais', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'GrossPEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés avant déduction des frais', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'GrossPEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés avant déduction des frais', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExpensesSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExpensesSharesPEExemptIncomeMovableAssets', fieldName:'Frais', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExpensesSharesPEExemptIncomeMovableAssets', fieldName:'Frais', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExpensesSharesPEExemptIncomeMovableAssets', fieldName:'Frais', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetPEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés après déduction des frais', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetPEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés après déduction des frais', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetPEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés après déduction des frais', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState', fieldName:'Revenus d\'apports effectués à l\'occasion d\'une opération de fusion, scission ou d\'une opération y assimilée, non rémunérés en raison de la détention par la société absorbante ou bénéficiaire d\'actions ou parts de la société absorbée ou scindée ou de dispositions d\'effet analogue dans un autre état membre de l\'UE', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'])  {
            fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] = '';
        }
        fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState', fieldName:'Revenus d\'apports effectués à l\'occasion d\'une opération de fusion, scission ou d\'une opération y assimilée, non rémunérés en raison de la détention par la société absorbante ou bénéficiaire d\'actions ou parts de la société absorbée ou scindée ou de dispositions d\'effet analogue dans un autre état membre de l\'UE', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'])  {
            fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] = '';
        }
        fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState', fieldName:'Revenus d\'apports effectués à l\'occasion d\'une opération de fusion, scission ou d\'une opération y assimilée, non rémunérés en raison de la détention par la société absorbante ou bénéficiaire d\'actions ou parts de la société absorbée ou scindée ou de dispositions d\'effet analogue dans un autre état membre de l\'UE', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'])  {
            fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] = '';
        }
        fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptIncomeMovableAssetsRefinancingLoans');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptIncomeMovableAssetsRefinancingLoans', fieldName:'Revenus mobiliers exonérés des titres d’emprunts de refinancement déterminés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'])  {
            fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] = '';
        }
        fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptIncomeMovableAssetsRefinancingLoans', fieldName:'Revenus mobiliers exonérés des titres d’emprunts de refinancement déterminés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'])  {
            fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] = '';
        }
        fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptIncomeMovableAssetsRefinancingLoans', fieldName:'Revenus mobiliers exonérés des titres d’emprunts de refinancement déterminés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'])  {
            fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] = '';
        }
        fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductiblePEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductiblePEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductiblePEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductiblePEExemptIncomeMovableAssets', fieldName:'Revenus définitivement taxés et revenus mobiliers exonérés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AccumulatedPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedPEExemptIncomeMovableAssets', fieldName:'Solde reporté de la déduction des revenus définitivement taxés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedPEExemptIncomeMovableAssets', fieldName:'Solde reporté de la déduction des revenus définitivement taxés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedPEExemptIncomeMovableAssets', fieldName:'Solde reporté de la déduction des revenus définitivement taxés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod', fieldName:'Déduction revenus définitivement taxés de la période imposable reportable sur la période imposable suivante', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod', fieldName:'Déduction revenus définitivement taxés de la période imposable reportable sur la période imposable suivante', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod', fieldName:'Déduction revenus définitivement taxés de la période imposable reportable sur la période imposable suivante', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod', fieldName:'Déduction revenus définitivement taxés reportée qui a été effectivement déduite durant la période imposable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'])  {
            fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod', fieldName:'Déduction revenus définitivement taxés reportée qui a été effectivement déduite durant la période imposable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'])  {
            fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod', fieldName:'Déduction revenus définitivement taxés reportée qui a été effectivement déduite durant la période imposable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'])  {
            fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets', fieldName:'Solde de la déduction revenus définitivement taxés reportable sur la période imposable suivante', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets', fieldName:'Solde de la déduction revenus définitivement taxés reportable sur la période imposable suivante', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets', fieldName:'Solde de la déduction revenus définitivement taxés reportable sur la période imposable suivante', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AccumulatedAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedAllowanceCorporateEquity', fieldName:'Solde reporté de la déduction pour capital à risque', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedAllowanceCorporateEquity', fieldName:'Solde reporté de la déduction pour capital à risque', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedAllowanceCorporateEquity', fieldName:'Solde reporté de la déduction pour capital à risque', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque qui est reportable sur la période imposable suivante', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque qui est reportable sur la période imposable suivante', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque qui est reportable sur la période imposable suivante', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensableTaxLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensableTaxLosses', fieldName:'Solde des pertes antérieures récupérables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensableTaxLosses'])  {
            fieldMsgs['tax-inc_CompensableTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensableTaxLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensableTaxLosses', fieldName:'Solde des pertes antérieures récupérables', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensableTaxLosses'])  {
            fieldMsgs['tax-inc_CompensableTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensableTaxLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 0) {
      msgs.push({prefix:'tax-inc',name:'CompensableTaxLosses', fieldName:'Solde des pertes antérieures récupérables', msg:'Fieldvalue is too great, maximum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensableTaxLosses'])  {
            fieldMsgs['tax-inc_CompensableTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensableTaxLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensatedTaxLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Pertes récupérées', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Pertes récupérées', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Pertes récupérées', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_LossCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'LossCurrentTaxPeriod', fieldName:'Perte de la période imposable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_LossCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_LossCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_LossCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'LossCurrentTaxPeriod', fieldName:'Perte de la période imposable', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_LossCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_LossCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_LossCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 0) {
      msgs.push({prefix:'tax-inc',name:'LossCurrentTaxPeriod', fieldName:'Perte de la période imposable', msg:'Fieldvalue is too great, maximum allowed value is 0'});
       if(!fieldMsgs['tax-inc_LossCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_LossCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_LossCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverTaxLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverTaxLosses', fieldName:'Perte à reporter sur la période imposable suivante', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverTaxLosses'])  {
            fieldMsgs['tax-inc_CarryOverTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CarryOverTaxLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverTaxLosses', fieldName:'Perte à reporter sur la période imposable suivante', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverTaxLosses'])  {
            fieldMsgs['tax-inc_CarryOverTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CarryOverTaxLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverTaxLosses', fieldName:'Perte à reporter sur la période imposable suivante', msg:'Fieldvalue is too great, maximum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverTaxLosses'])  {
            fieldMsgs['tax-inc_CarryOverTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CarryOverTaxLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentFirstQuarter');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarter', fieldName:'Versement anticipé, premier trimestre', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarter'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarter', fieldName:'Versement anticipé, premier trimestre', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarter'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarter', fieldName:'Versement anticipé, premier trimestre', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarter'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentSecondQuarter');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarter', fieldName:'Versement anticipé, deuxième trimestre', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarter'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarter', fieldName:'Versement anticipé, deuxième trimestre', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarter'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarter', fieldName:'Versement anticipé, deuxième trimestre', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarter'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentThirdQuarter');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarter', fieldName:'Versement anticipé, troisième trimestre', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarter'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarter', fieldName:'Versement anticipé, troisième trimestre', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarter'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarter', fieldName:'Versement anticipé, troisième trimestre', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarter'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentFourthQuarter');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarter', fieldName:'Versement anticipé, quatrième trimestre', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarter'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarter', fieldName:'Versement anticipé, quatrième trimestre', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarter'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarter', fieldName:'Versement anticipé, quatrième trimestre', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarter'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
        
    }
    
    fields = store.findElementsForName('tax-inc_NonRepayableFictiousWitholdingTax');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTax', fieldName:'Précompte mobilier fictif', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTax', fieldName:'Précompte mobilier fictif', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTax', fieldName:'Précompte mobilier fictif', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonRepayableLumpSumForeignTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxes', fieldName:'Quotité forfaitaire d’impôt étranger', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxes', fieldName:'Quotité forfaitaire d’impôt étranger', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxes', fieldName:'Quotité forfaitaire d’impôt étranger', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxCreditResearchDevelopment');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopment', fieldName:'Crédit d’impôt pour recherche et développement imputable en principe', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopment', fieldName:'Crédit d’impôt pour recherche et développement imputable en principe', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopment', fieldName:'Crédit d’impôt pour recherche et développement imputable en principe', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium', fieldName:'Précompte mobilier réel ou fictif sur revenus définitivement taxés et sur revenus mobiliers exonérés d’origine belge d’actions ou parts, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium', fieldName:'Précompte mobilier réel ou fictif sur revenus définitivement taxés et sur revenus mobiliers exonérés d’origine belge d’actions ou parts, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium', fieldName:'Précompte mobilier réel ou fictif sur revenus définitivement taxés et sur revenus mobiliers exonérés d’origine belge d’actions ou parts, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign', fieldName:'Précompte mobilier sur bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign', fieldName:'Précompte mobilier sur bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign', fieldName:'Précompte mobilier sur bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherPEForeign');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeign', fieldName:'Précompte mobilier sur revenus définitivement taxés d’origine étrangère, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeign', fieldName:'Précompte mobilier sur revenus définitivement taxés d’origine étrangère, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeign', fieldName:'Précompte mobilier sur revenus définitivement taxés d’origine étrangère, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares', fieldName:'Précompte mobilier sur autres bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares', fieldName:'Précompte mobilier sur autres bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares', fieldName:'Précompte mobilier sur autres bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherDividends');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividends', fieldName:'Précompte mobilier sur autres dividendes', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividends', fieldName:'Précompte mobilier sur autres dividendes', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividends', fieldName:'Précompte mobilier sur autres dividendes', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherRepayableWithholdingTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxes', fieldName:'Autre précompte mobilier remboursable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxes', fieldName:'Autre précompte mobilier remboursable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxes', fieldName:'Autre précompte mobilier remboursable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear', fieldName:'Crédit d\'impôt pour recherche et développement à restituer pour la présente période imposable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear', fieldName:'Crédit d\'impôt pour recherche et développement à restituer pour la présente période imposable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear', fieldName:'Crédit d\'impôt pour recherche et développement à restituer pour la présente période imposable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 8) {
      msgs.push({prefix:'tax-inc',name:'AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod', fieldName:'Nombre de travailleurs occupés, en moyenne annuelle', msg:'Fieldvalue is too long, maximum length is 8'});
       if(!fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod', fieldName:'Nombre de travailleurs occupés, en moyenne annuelle', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 999999.99) {
      msgs.push({prefix:'tax-inc',name:'AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod', fieldName:'Nombre de travailleurs occupés, en moyenne annuelle', msg:'Fieldvalue is too great, maximum allowed value is 999999.99'});
       if(!fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod', fieldName:'Chiffre d\'affaires annuel, hors TVA', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod', fieldName:'Chiffre d\'affaires annuel, hors TVA', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod', fieldName:'Chiffre d\'affaires annuel, hors TVA', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BalanceSheetTotalCorporationCodeCurrentTaxPeriod', fieldName:'Total du bilan', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BalanceSheetTotalCorporationCodeCurrentTaxPeriod', fieldName:'Total du bilan', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BalanceSheetTotalCorporationCodeCurrentTaxPeriod', fieldName:'Total du bilan', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_GeneralMeetingMinutesDecisions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'binary');
        

      if(value==null) {
      msgs.push({prefix:'tax-inc',name:'GeneralMeetingMinutesDecisions', fieldName:'Rapports à l\'assemblée générale et délibérations de celle-ci', msg:'This field is required!'});
       if(!fieldMsgs['tax-inc_GeneralMeetingMinutesDecisions'])  {
            fieldMsgs['tax-inc_GeneralMeetingMinutesDecisions'] = '';
        }
        fieldMsgs['tax-inc_GeneralMeetingMinutesDecisions'] =msgs[msgs.length-1].msg;
      }

  
        
    }
    
      if(fields.length==0) {
        msgs.push({prefix:'tax-inc',name:'GeneralMeetingMinutesDecisions', fieldName:'Rapports à l\'assemblée générale et délibérations de celle-ci', msg:'This field is required!'});
        if(!fieldMsgs['tax-inc_GeneralMeetingMinutesDecisions'])  {
            fieldMsgs['tax-inc_GeneralMeetingMinutesDecisions'] = '';
        }
        fieldMsgs['tax-inc_GeneralMeetingMinutesDecisions'] =msgs[msgs.length-1].msg;
      }
    
    fields = store.findElementsForName('tax-inc_AnnuityLinearDepreciation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AnnuityLinearDepreciation', fieldName:'Taux d\'amortissement linéaire', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AnnuityLinearDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityLinearDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityLinearDepreciation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'AnnuityLinearDepreciation', fieldName:'Taux d\'amortissement linéaire', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_AnnuityLinearDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityLinearDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityLinearDepreciation'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'AnnuityLinearDepreciation', fieldName:'Taux d\'amortissement linéaire', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_AnnuityLinearDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityLinearDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityLinearDepreciation'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_AnnuityDegressiveDepreciation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AnnuityDegressiveDepreciation', fieldName:'Taux d\'amortissement dégressif', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'AnnuityDegressiveDepreciation', fieldName:'Taux d\'amortissement dégressif', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'AnnuityDegressiveDepreciation', fieldName:'Taux d\'amortissement dégressif', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_AcquisitionInvestmentValue');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionInvestmentValue', fieldName:'Valeur d\'acquisition ou d\'investissement', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AcquisitionInvestmentValue'])  {
            fieldMsgs['tax-inc_AcquisitionInvestmentValue'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionInvestmentValue'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionInvestmentValue', fieldName:'Valeur d\'acquisition ou d\'investissement', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AcquisitionInvestmentValue'])  {
            fieldMsgs['tax-inc_AcquisitionInvestmentValue'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionInvestmentValue'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionInvestmentValue', fieldName:'Valeur d\'acquisition ou d\'investissement', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AcquisitionInvestmentValue'])  {
            fieldMsgs['tax-inc_AcquisitionInvestmentValue'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionInvestmentValue'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_Revaluations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'Revaluations', fieldName:'Réévaluation', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_Revaluations'])  {
            fieldMsgs['tax-inc_Revaluations'] = '';
        }
        fieldMsgs['tax-inc_Revaluations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'Revaluations', fieldName:'Réévaluation', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_Revaluations'])  {
            fieldMsgs['tax-inc_Revaluations'] = '';
        }
        fieldMsgs['tax-inc_Revaluations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Revaluations', fieldName:'Réévaluation', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_Revaluations'])  {
            fieldMsgs['tax-inc_Revaluations'] = '';
        }
        fieldMsgs['tax-inc_Revaluations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DepreciationsNoRevaluations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsNoRevaluations', fieldName:'Amortissements, sans réévaluation', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DepreciationsNoRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsNoRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsNoRevaluations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsNoRevaluations', fieldName:'Amortissements, sans réévaluation', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DepreciationsNoRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsNoRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsNoRevaluations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsNoRevaluations', fieldName:'Amortissements, sans réévaluation', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DepreciationsNoRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsNoRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsNoRevaluations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DepreciationsRevaluations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsRevaluations', fieldName:'Amortissement sur réévaluation', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DepreciationsRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsRevaluations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsRevaluations', fieldName:'Amortissement sur réévaluation', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DepreciationsRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsRevaluations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsRevaluations', fieldName:'Amortissement sur réévaluation', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DepreciationsRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsRevaluations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExaggeratedDepreciations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciations', fieldName:'Excédents d’amortissements', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciations'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciations'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciations', fieldName:'Excédents d’amortissements', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciations'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciations'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciations', fieldName:'Excédents d’amortissements', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciations'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciations'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTaxNonDeductibleTaxes', fieldName:'Précompte mobilier fictif', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTaxNonDeductibleTaxes', fieldName:'Précompte mobilier fictif', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTaxNonDeductibleTaxes', fieldName:'Précompte mobilier fictif', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxesNonDeductibleTaxes', fieldName:'Quotité forfaitaire d’impôt étranger', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxesNonDeductibleTaxes', fieldName:'Quotité forfaitaire d’impôt étranger', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxesNonDeductibleTaxes', fieldName:'Quotité forfaitaire d’impôt étranger', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes', fieldName:'Précompte mobilier réel ou fictif sur revenus définitivement taxés et sur revenus mobiliers exonérés d’origine belge d’actions ou parts, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres définitivement taxés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes', fieldName:'Précompte mobilier réel ou fictif sur revenus définitivement taxés et sur revenus mobiliers exonérés d’origine belge d’actions ou parts, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres définitivement taxés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes', fieldName:'Précompte mobilier réel ou fictif sur revenus définitivement taxés et sur revenus mobiliers exonérés d’origine belge d’actions ou parts, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres définitivement taxés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes', fieldName:'Précompte mobilier sur bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes', fieldName:'Précompte mobilier sur bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes', fieldName:'Précompte mobilier sur bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes', fieldName:'Précompte mobilier sur revenus définitivement taxés d’origine étrangère, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes', fieldName:'Précompte mobilier sur revenus définitivement taxés d’origine étrangère, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes', fieldName:'Précompte mobilier sur revenus définitivement taxés d’origine étrangère, autres que bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres, définitivement taxés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes', fieldName:'Précompte mobilier sur autres bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes', fieldName:'Précompte mobilier sur autres bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes', fieldName:'Précompte mobilier sur autres bonis de liquidation ou bonis en cas d’acquisition d’actions ou parts propres', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes', fieldName:'Précompte mobilier sur autres dividendes', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes', fieldName:'Précompte mobilier sur autres dividendes', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes', fieldName:'Précompte mobilier sur autres dividendes', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxesNonDeductibleTaxes', fieldName:'Autre précompte mobilier remboursable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxesNonDeductibleTaxes', fieldName:'Autre précompte mobilier remboursable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxesNonDeductibleTaxes', fieldName:'Autre précompte mobilier remboursable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, premier trimestre', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, premier trimestre', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, premier trimestre', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, deuxième trimestre', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, deuxième trimestre', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, deuxième trimestre', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, troisième trimestre', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, troisième trimestre', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, troisième trimestre', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, quatrième trimestre', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, quatrième trimestre', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarterNonDeductibleTaxes', fieldName:'Versement anticipé, quatrième trimestre', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests', fieldName:'Impôt des sociétés y compris majoration, accroissement et intérêts de retard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'])  {
            fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] = '';
        }
        fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests', fieldName:'Impôt des sociétés y compris majoration, accroissement et intérêts de retard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'])  {
            fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] = '';
        }
        fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests', fieldName:'Impôt des sociétés y compris majoration, accroissement et intérêts de retard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'])  {
            fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] = '';
        }
        fieldMsgs['tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EstimatedCorporateIncomeTaxDebt');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EstimatedCorporateIncomeTaxDebt', fieldName:'Montant estimé des dettes d’impôts', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'])  {
            fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] = '';
        }
        fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EstimatedCorporateIncomeTaxDebt', fieldName:'Montant estimé des dettes d’impôts', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'])  {
            fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] = '';
        }
        fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EstimatedCorporateIncomeTaxDebt', fieldName:'Montant estimé des dettes d’impôts', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'])  {
            fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] = '';
        }
        fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxDividendsPaid');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxDividendsPaid', fieldName:'Précompte mobilier supporté par la société sur dividendes distribués', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'])  {
            fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxDividendsPaid', fieldName:'Précompte mobilier supporté par la société sur dividendes distribués', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'])  {
            fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxDividendsPaid', fieldName:'Précompte mobilier supporté par la société sur dividendes distribués', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'])  {
            fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxDividendsPaid'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxOtherIncomePaid');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherIncomePaid', fieldName:'Précompte mobilier supporté par la société sur autres revenus attribués', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherIncomePaid', fieldName:'Précompte mobilier supporté par la société sur autres revenus attribués', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherIncomePaid', fieldName:'Précompte mobilier supporté par la société sur autres revenus attribués', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherIncomePaid'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherNonDeductibleTaxes', fieldName:'Autres impôts non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherNonDeductibleTaxes', fieldName:'Autres impôts non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherNonDeductibleTaxes', fieldName:'Autres impôts non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxesNoReimbursementsRegularizations', fieldName:'Impôts non déductibles avant déduction des remboursements et régularisations', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxesNoReimbursementsRegularizations', fieldName:'Impôts non déductibles avant déduction des remboursements et régularisations', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxesNoReimbursementsRegularizations', fieldName:'Impôts non déductibles avant déduction des remboursements et régularisations', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ReimbursementsRegularizationsNonDeductibleTaxes', fieldName:'Remboursements et régularisations', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ReimbursementsRegularizationsNonDeductibleTaxes', fieldName:'Remboursements et régularisations', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ReimbursementsRegularizationsNonDeductibleTaxes', fieldName:'Remboursements et régularisations', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Impôts non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Impôts non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Impôts non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleRegionalTaxesDutiesRetributions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Impôts, taxes et rétributions régionaux', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Impôts, taxes et rétributions régionaux', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Impôts, taxes et rétributions régionaux', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Amendes, pénalités et confiscations de toute nature', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Amendes, pénalités et confiscations de toute nature', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Amendes, pénalités et confiscations de toute nature', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Pensions, capitaux, cotisations et primes patronales non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Pensions, capitaux, cotisations et primes patronales non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Pensions, capitaux, cotisations et primes patronales non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarExpense');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarExpense', fieldName:'Frais', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarExpense'])  {
            fieldMsgs['tax-inc_CarExpense'] = '';
        }
        fieldMsgs['tax-inc_CarExpense'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarExpense', fieldName:'Frais', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarExpense'])  {
            fieldMsgs['tax-inc_CarExpense'] = '';
        }
        fieldMsgs['tax-inc_CarExpense'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarExpense', fieldName:'Frais', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarExpense'])  {
            fieldMsgs['tax-inc_CarExpense'] = '';
        }
        fieldMsgs['tax-inc_CarExpense'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_LossValueCar');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'LossValueCar', fieldName:'Moins-value', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_LossValueCar'])  {
            fieldMsgs['tax-inc_LossValueCar'] = '';
        }
        fieldMsgs['tax-inc_LossValueCar'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'LossValueCar', fieldName:'Moins-value', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_LossValueCar'])  {
            fieldMsgs['tax-inc_LossValueCar'] = '';
        }
        fieldMsgs['tax-inc_LossValueCar'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'LossValueCar', fieldName:'Moins-value', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_LossValueCar'])  {
            fieldMsgs['tax-inc_LossValueCar'] = '';
        }
        fieldMsgs['tax-inc_LossValueCar'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesLossValuesCars');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Frais de voiture et moins-values sur véhicules automobiles non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Frais de voiture et moins-values sur véhicules automobiles non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Frais de voiture et moins-values sur véhicules automobiles non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensationCarRepairCosts');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensationCarRepairCosts', fieldName:'Indemnité perçue à titre de dédommagement pour des frais de réparation', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensationCarRepairCosts'])  {
            fieldMsgs['tax-inc_CompensationCarRepairCosts'] = '';
        }
        fieldMsgs['tax-inc_CompensationCarRepairCosts'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CompensationCarRepairCosts', fieldName:'Indemnité perçue à titre de dédommagement pour des frais de réparation', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensationCarRepairCosts'])  {
            fieldMsgs['tax-inc_CompensationCarRepairCosts'] = '';
        }
        fieldMsgs['tax-inc_CompensationCarRepairCosts'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensationCarRepairCosts', fieldName:'Indemnité perçue à titre de dédommagement pour des frais de réparation', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensationCarRepairCosts'])  {
            fieldMsgs['tax-inc_CompensationCarRepairCosts'] = '';
        }
        fieldMsgs['tax-inc_CompensationCarRepairCosts'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Frais de voiture à concurrence d’une quotité de l’avantage de toute nature', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ReceptionBusinessGiftsExpense');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ReceptionBusinessGiftsExpense', fieldName:'Frais', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'])  {
            fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] = '';
        }
        fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ReceptionBusinessGiftsExpense', fieldName:'Frais', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'])  {
            fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] = '';
        }
        fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ReceptionBusinessGiftsExpense', fieldName:'Frais', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'])  {
            fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] = '';
        }
        fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleReceptionBusinessGiftsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Frais de réception et de cadeaux d’affaires non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Frais de réception et de cadeaux d’affaires non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Frais de réception et de cadeaux d’affaires non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RestaurantExpense');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RestaurantExpense', fieldName:'Frais', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RestaurantExpense'])  {
            fieldMsgs['tax-inc_RestaurantExpense'] = '';
        }
        fieldMsgs['tax-inc_RestaurantExpense'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RestaurantExpense', fieldName:'Frais', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RestaurantExpense'])  {
            fieldMsgs['tax-inc_RestaurantExpense'] = '';
        }
        fieldMsgs['tax-inc_RestaurantExpense'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RestaurantExpense', fieldName:'Frais', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RestaurantExpense'])  {
            fieldMsgs['tax-inc_RestaurantExpense'] = '';
        }
        fieldMsgs['tax-inc_RestaurantExpense'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses', fieldName:'Pourcentage non déductible', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleRestaurantExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Frais de restaurant non déductibles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Frais de restaurant non déductibles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Frais de restaurant non déductibles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Frais de vêtements professionnels non spécifiques', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Frais de vêtements professionnels non spécifiques', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Frais de vêtements professionnels non spécifiques', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExaggeratedInterests');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Intérêts exagérés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Intérêts exagérés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Intérêts exagérés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleParticularPortionInterestsLoans');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Intérêts relatifs à une partie de certains emprunts', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Intérêts relatifs à une partie de certains emprunts', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Intérêts relatifs à une partie de certains emprunts', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AbnormalBenevolentAdvantages');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Avantages anormaux ou bénévoles', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Avantages anormaux ou bénévoles', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Avantages anormaux ou bénévoles', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleSocialAdvantages');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Avantages sociaux', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Avantages sociaux', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Avantages sociaux', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Avantages de titres-repas, chèques sport/culture ou éco-chèques', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Avantages de titres-repas, chèques sport/culture ou éco-chèques', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Avantages de titres-repas, chèques sport/culture ou éco-chèques', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_Liberalities');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Libéralités', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Libéralités', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Libéralités', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WriteDownsLossValuesShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Réductions de valeur et moins-values sur actions ou parts', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Réductions de valeur et moins-values sur actions ou parts', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Réductions de valeur et moins-values sur actions ou parts', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ReversalPreviousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Reprises d’exonérations antérieures', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Reprises d’exonérations antérieures', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Reprises d’exonérations antérieures', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EmployeeParticipation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Participation des travailleurs', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IndemnityMissingCoupon');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Indemnités pour coupon manquant', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Indemnités pour coupon manquant', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Indemnités pour coupon manquant', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Frais d\'œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Frais d\'œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Frais d\'œuvres audiovisuelles agréées tax shelter', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Primes, subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Primes, subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Primes, subsides en capital et en intérêt régionaux', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductiblePaymentsCertainStates');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Paiements non déductibles vers certains Etats', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Paiements non déductibles vers certains Etats', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Paiements non déductibles vers certains Etats', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherDisallowedExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Autres dépenses non admises', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Autres dépenses non admises', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Autres dépenses non admises', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DebtClaim');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DebtClaim', fieldName:'Créance', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DebtClaim'])  {
            fieldMsgs['tax-inc_DebtClaim'] = '';
        }
        fieldMsgs['tax-inc_DebtClaim'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DebtClaim', fieldName:'Créance', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DebtClaim'])  {
            fieldMsgs['tax-inc_DebtClaim'] = '';
        }
        fieldMsgs['tax-inc_DebtClaim'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DebtClaim', fieldName:'Créance', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DebtClaim'])  {
            fieldMsgs['tax-inc_DebtClaim'] = '';
        }
        fieldMsgs['tax-inc_DebtClaim'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptWriteDownDebtClaim');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Réduction de valeur exonérée', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Réduction de valeur exonérée', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Réduction de valeur exonérée', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales correspondant aux pertes qui sont devenues définitives au cours de l\'exercice comptable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales correspondant aux pertes qui sont devenues définitives au cours de l\'exercice comptable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales correspondant aux pertes qui sont devenues définitives au cours de l\'exercice comptable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales résultant de l\'encaissement total ou partiel de la créance au cours de l\'exercice comptable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales résultant de l\'encaissement total ou partiel de la créance au cours de l\'exercice comptable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales résultant de l\'encaissement total ou partiel de la créance au cours de l\'exercice comptable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales résultant d\'une nouvelle estimation de la perte probable au cours de l\'exercice comptable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales résultant d\'une nouvelle estimation de la perte probable au cours de l\'exercice comptable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss', fieldName:'Diminution de la réduction de valeur exonérée sur créances commerciales résultant d\'une nouvelle estimation de la perte probable au cours de l\'exercice comptable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IncreaseExemptWriteDownDebtClaim');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptWriteDownDebtClaim', fieldName:'Augmentation de la réduction de valeur exonérée au cours de l\'exercice comptable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptWriteDownDebtClaim', fieldName:'Augmentation de la réduction de valeur exonérée au cours de l\'exercice comptable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptWriteDownDebtClaim', fieldName:'Augmentation de la réduction de valeur exonérée au cours de l\'exercice comptable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ProbableCost');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ProbableCost', fieldName:'Charge probable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ProbableCost'])  {
            fieldMsgs['tax-inc_ProbableCost'] = '';
        }
        fieldMsgs['tax-inc_ProbableCost'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ProbableCost', fieldName:'Charge probable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ProbableCost'])  {
            fieldMsgs['tax-inc_ProbableCost'] = '';
        }
        fieldMsgs['tax-inc_ProbableCost'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ProbableCost', fieldName:'Charge probable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ProbableCost'])  {
            fieldMsgs['tax-inc_ProbableCost'] = '';
        }
        fieldMsgs['tax-inc_ProbableCost'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptProvisionRisksExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Provision exonérée', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Provision exonérée', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Provision exonérée', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod', fieldName:'Diminution de la provision exonérée résultant de la prise en charge effective au cours de l\'exercice comptable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod', fieldName:'Diminution de la provision exonérée résultant de la prise en charge effective au cours de l\'exercice comptable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod', fieldName:'Diminution de la provision exonérée résultant de la prise en charge effective au cours de l\'exercice comptable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses', fieldName:'Diminution de la provision exonérée résultant d\'une nouvelle estimation de la charge probable au cours de l\'exercice comptable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses', fieldName:'Diminution de la provision exonérée résultant d\'une nouvelle estimation de la charge probable au cours de l\'exercice comptable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses', fieldName:'Diminution de la provision exonérée résultant d\'une nouvelle estimation de la charge probable au cours de l\'exercice comptable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IncreaseExemptProvisionRisksExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptProvisionRisksExpenses', fieldName:'Augmentation de la provision exonérée', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptProvisionRisksExpenses', fieldName:'Augmentation de la provision exonérée', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptProvisionRisksExpenses', fieldName:'Augmentation de la provision exonérée', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_Equity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'Equity', fieldName:'Capitaux propres', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_Equity'])  {
            fieldMsgs['tax-inc_Equity'] = '';
        }
        fieldMsgs['tax-inc_Equity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Equity', fieldName:'Capitaux propres', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_Equity'])  {
            fieldMsgs['tax-inc_Equity'] = '';
        }
        fieldMsgs['tax-inc_Equity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Equity', fieldName:'Capitaux propres', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_Equity'])  {
            fieldMsgs['tax-inc_Equity'] = '';
        }
        fieldMsgs['tax-inc_Equity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OwnSharesFiscalValue');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OwnSharesFiscalValue', fieldName:'Actions ou parts propres', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OwnSharesFiscalValue'])  {
            fieldMsgs['tax-inc_OwnSharesFiscalValue'] = '';
        }
        fieldMsgs['tax-inc_OwnSharesFiscalValue'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OwnSharesFiscalValue', fieldName:'Actions ou parts propres', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OwnSharesFiscalValue'])  {
            fieldMsgs['tax-inc_OwnSharesFiscalValue'] = '';
        }
        fieldMsgs['tax-inc_OwnSharesFiscalValue'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OwnSharesFiscalValue', fieldName:'Actions ou parts propres', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OwnSharesFiscalValue'])  {
            fieldMsgs['tax-inc_OwnSharesFiscalValue'] = '';
        }
        fieldMsgs['tax-inc_OwnSharesFiscalValue'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_FinancialFixedAssetsParticipationsOtherShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'FinancialFixedAssetsParticipationsOtherShares', fieldName:'Immobilisations financières consistant en participations et autres actions ou parts', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'])  {
            fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] = '';
        }
        fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'FinancialFixedAssetsParticipationsOtherShares', fieldName:'Immobilisations financières consistant en participations et autres actions ou parts', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'])  {
            fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] = '';
        }
        fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'FinancialFixedAssetsParticipationsOtherShares', fieldName:'Immobilisations financières consistant en participations et autres actions ou parts', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'])  {
            fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] = '';
        }
        fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_SharesInvestmentCorporations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'SharesInvestmentCorporations', fieldName:'Actions ou parts émises par des sociétés d\'investissement', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_SharesInvestmentCorporations'])  {
            fieldMsgs['tax-inc_SharesInvestmentCorporations'] = '';
        }
        fieldMsgs['tax-inc_SharesInvestmentCorporations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'SharesInvestmentCorporations', fieldName:'Actions ou parts émises par des sociétés d\'investissement', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_SharesInvestmentCorporations'])  {
            fieldMsgs['tax-inc_SharesInvestmentCorporations'] = '';
        }
        fieldMsgs['tax-inc_SharesInvestmentCorporations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'SharesInvestmentCorporations', fieldName:'Actions ou parts émises par des sociétés d\'investissement', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_SharesInvestmentCorporations'])  {
            fieldMsgs['tax-inc_SharesInvestmentCorporations'] = '';
        }
        fieldMsgs['tax-inc_SharesInvestmentCorporations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BranchesCountryTaxTreaty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BranchesCountryTaxTreaty', fieldName:'Etablissements situés dans un pays avec convention', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BranchesCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BranchesCountryTaxTreaty', fieldName:'Etablissements situés dans un pays avec convention', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BranchesCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BranchesCountryTaxTreaty', fieldName:'Etablissements situés dans un pays avec convention', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BranchesCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ImmovablePropertyCountryTaxTreaty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyCountryTaxTreaty', fieldName:'Immeubles situés dans un pays avec convention', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyCountryTaxTreaty', fieldName:'Immeubles situés dans un pays avec convention', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyCountryTaxTreaty', fieldName:'Immeubles situés dans un pays avec convention', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TangibleFixedAssetsUnreasonableRelatedCosts', fieldName:'Actifs corporels dans la mesure où les frais y afférents sont déraisonnables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'])  {
            fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] = '';
        }
        fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TangibleFixedAssetsUnreasonableRelatedCosts', fieldName:'Actifs corporels dans la mesure où les frais y afférents sont déraisonnables', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'])  {
            fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] = '';
        }
        fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TangibleFixedAssetsUnreasonableRelatedCosts', fieldName:'Actifs corporels dans la mesure où les frais y afférents sont déraisonnables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'])  {
            fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] = '';
        }
        fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_InvestmentsNoPeriodicalIncome');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'InvestmentsNoPeriodicalIncome', fieldName:'Eléments détenus à titre de placement non productifs de revenus périodiques imposables', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'])  {
            fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] = '';
        }
        fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'InvestmentsNoPeriodicalIncome', fieldName:'Eléments détenus à titre de placement non productifs de revenus périodiques imposables', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'])  {
            fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] = '';
        }
        fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'InvestmentsNoPeriodicalIncome', fieldName:'Eléments détenus à titre de placement non productifs de revenus périodiques imposables', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'])  {
            fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] = '';
        }
        fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ImmovablePropertyUseManager');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyUseManager', fieldName:'Biens immobiliers dont les dirigeants ont l\'usage', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyUseManager'])  {
            fieldMsgs['tax-inc_ImmovablePropertyUseManager'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyUseManager'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyUseManager', fieldName:'Biens immobiliers dont les dirigeants ont l\'usage', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyUseManager'])  {
            fieldMsgs['tax-inc_ImmovablePropertyUseManager'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyUseManager'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyUseManager', fieldName:'Biens immobiliers dont les dirigeants ont l\'usage', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyUseManager'])  {
            fieldMsgs['tax-inc_ImmovablePropertyUseManager'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyUseManager'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_UnrealisedExpressedCapitalGains');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGains', fieldName:'Plus-values exprimées mais non réalisées', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGains', fieldName:'Plus-values exprimées mais non réalisées', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGains', fieldName:'Plus-values exprimées mais non réalisées', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity', fieldName:'Crédit d’impôt pour recherche et développement', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity', fieldName:'Crédit d’impôt pour recherche et développement', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity', fieldName:'Crédit d’impôt pour recherche et développement', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_InvestmentGrants');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'InvestmentGrants', fieldName:'Subsides en capital', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_InvestmentGrants'])  {
            fieldMsgs['tax-inc_InvestmentGrants'] = '';
        }
        fieldMsgs['tax-inc_InvestmentGrants'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'InvestmentGrants', fieldName:'Subsides en capital', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_InvestmentGrants'])  {
            fieldMsgs['tax-inc_InvestmentGrants'] = '';
        }
        fieldMsgs['tax-inc_InvestmentGrants'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'InvestmentGrants', fieldName:'Subsides en capital', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_InvestmentGrants'])  {
            fieldMsgs['tax-inc_InvestmentGrants'] = '';
        }
        fieldMsgs['tax-inc_InvestmentGrants'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ActualisationStockRecognisedDiamondTraders');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ActualisationStockRecognisedDiamondTraders', fieldName:'Actualisation des stocks des diamantaires agréés', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'])  {
            fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ActualisationStockRecognisedDiamondTraders', fieldName:'Actualisation des stocks des diamantaires agréés', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'])  {
            fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ActualisationStockRecognisedDiamondTraders', fieldName:'Actualisation des stocks des diamantaires agréés', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'])  {
            fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch', fieldName:'Moyens empruntés dans le chef du siège principal dont les intérêts sont à charge du résultat imposable de l\'établissement belge', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'])  {
            fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] = '';
        }
        fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch', fieldName:'Moyens empruntés dans le chef du siège principal dont les intérêts sont à charge du résultat imposable de l\'établissement belge', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'])  {
            fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] = '';
        }
        fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch', fieldName:'Moyens empruntés dans le chef du siège principal dont les intérêts sont à charge du résultat imposable de l\'établissement belge', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'])  {
            fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] = '';
        }
        fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Variations en cours de période imposable des capitaux propres et des éléments à déduire', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Variations en cours de période imposable des capitaux propres et des éléments à déduire', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Variations en cours de période imposable des capitaux propres et des éléments à déduire', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AllowanceCorporateEquityCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityCurrentTaxPeriod', fieldName:'Capital à risque de la période imposable', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityCurrentTaxPeriod', fieldName:'Capital à risque de la période imposable', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityCurrentTaxPeriod', fieldName:'Capital à risque de la période imposable', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductibleAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'Déductible en principe', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductibleAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'Déductible en principe', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductibleAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'Déductible en principe', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductionAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'Déduction pour capital à risque de l\'exercice d\'imposition actuel, effectivement déduite', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductionAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'Déduction pour capital à risque de l\'exercice d\'imposition actuel, effectivement déduite', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductionAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'Déduction pour capital à risque de l\'exercice d\'imposition actuel, effectivement déduite', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque de l\'exercice d\'imposition actuel à reporter sur les exercices d\'imposition ultérieurs', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque de l\'exercice d\'imposition actuel à reporter sur les exercices d\'imposition ultérieurs', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque de l\'exercice d\'imposition actuel à reporter sur les exercices d\'imposition ultérieurs', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AllowanceCorporateEquityPreviousAssessmentYears');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityPreviousAssessmentYears', fieldName:'Solde déductible de la déduction pour capital à risque constituée au cours d\'exercices d\'imposition antérieurs', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityPreviousAssessmentYears', fieldName:'Solde déductible de la déduction pour capital à risque constituée au cours d\'exercices d\'imposition antérieurs', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityPreviousAssessmentYears', fieldName:'Solde déductible de la déduction pour capital à risque constituée au cours d\'exercices d\'imposition antérieurs', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductionAllowanceCorporateEquityPreviousAssessmentYears', fieldName:'Déduction pour capital à risque antérieure effectivement déduite', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'])  {
            fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'] = '';
        }
        fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductionAllowanceCorporateEquityPreviousAssessmentYears', fieldName:'Déduction pour capital à risque antérieure effectivement déduite', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'])  {
            fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'] = '';
        }
        fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductionAllowanceCorporateEquityPreviousAssessmentYears', fieldName:'Déduction pour capital à risque antérieure effectivement déduite', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'])  {
            fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'] = '';
        }
        fieldMsgs['tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Déduction pour capital à risque déduite au cours de l\'exercice d\'imposition actuel', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Déduction pour capital à risque déduite au cours de l\'exercice d\'imposition actuel', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Déduction pour capital à risque déduite au cours de l\'exercice d\'imposition actuel', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque qui est reportable sur la période imposable suivante', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque qui est reportable sur la période imposable suivante', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Solde de la déduction pour capital à risque qui est reportable sur la période imposable suivante', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Variations en cours de période imposable des capitaux propres et des éléments à déduire', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Variations en cours de période imposable des capitaux propres et des éléments à déduire', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Variations en cours de période imposable des capitaux propres et des éléments à déduire', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    

    var res = { result: results, scopeId: e.data.scopeId,msgs:msgs,fieldMsgs:fieldMsgs };
    self.postMessage(res);
    }, false);
  