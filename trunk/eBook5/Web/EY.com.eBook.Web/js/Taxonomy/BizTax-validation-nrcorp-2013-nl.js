
    Ext = {

    /**
    * The version of the framework
    * @type String
    */
    version: '3.2.1',
    versionDetail: {
    major: 3,
    minor: 2,
    patch: 1
    }
    };

    Ext.apply = function(o, c, defaults) {
    // no "this" reference for friendly out of scope calls
    if (defaults) {
    Ext.apply(o, defaults);
    }
    if (o && c && typeof c == 'object') {
    for (var p in c) {
    o[p] = c[p];
    }
    }
    return o;
    };


    Ext.apply(Ext, {

    USE_NATIVE_JSON: false,
    applyIf: function(o, c) {
    if (o) {
    for (var p in c) {
    if (!Ext.isDefined(o[p])) {
    o[p] = c[p];
    }
    }
    }
    return o;
    },
    extend: function() {
    // inline overrides
    var io = function(o) {
    for (var m in o) {
    this[m] = o[m];
    }
    };
    var oc = Object.prototype.constructor;

    return function(sb, sp, overrides) {
    if (typeof sp == 'object') {
    overrides = sp;
    sp = sb;
    sb = overrides.constructor != oc ? overrides.constructor : function() { sp.apply(this, arguments); };
    }
    var F = function() { },
    sbp,
    spp = sp.prototype;

    F.prototype = spp;
    sbp = sb.prototype = new F();
    sbp.constructor = sb;
    sb.superclass = spp;
    if (spp.constructor == oc) {
    spp.constructor = sp;
    }
    sb.override = function(o) {
    Ext.override(sb, o);
    };
    sbp.superclass = sbp.supr = (function() {
    return spp;
    });
    sbp.override = io;
    Ext.override(sb, overrides);
    sb.extend = function(o) { return Ext.extend(sb, o); };
    return sb;
    };
    } (),

    override: function(origclass, overrides) {
    if (overrides) {
    var p = origclass.prototype;
    Ext.apply(p, overrides);
    if (false && overrides.hasOwnProperty('toString')) {
    p.toString = overrides.toString;
    }
    }
    },
    toArray: function() {
    return false ?
    function(a, i, j, res) {
    res = [];
    for (var x = 0, len = a.length; x < len; x++) {
    res.push(a[x]);
    }
    return res.slice(i || 0, j || res.length);
    } :
    function(a, i, j) {
    return Array.prototype.slice.call(a, i || 0, j || a.length);
    }
    } (),


    isEmpty: function(v, allowBlank) {
    return v === null || v === undefined || ((Ext.isArray(v) && !v.length)) || (!allowBlank ? v === '' : false);
    },
    isArray: function(v) {
    return toString.apply(v) === '[object Array]';
    },
    isDate: function(v) {
    return toString.apply(v) === '[object Date]';
    },
    isObject: function(v) {
    return !!v && Object.prototype.toString.call(v) === '[object Object]';
    },
    isPrimitive: function(v) {
    return Ext.isString(v) || Ext.isNumber(v) || Ext.isBoolean(v);
    },
    isFunction: function(v) {
    return toString.apply(v) === '[object Function]';
    },
    isNumber: function(v) {
    return typeof v === 'number' && isFinite(v);
    },
    isString: function(v) {
    return typeof v === 'string';
    },
    isBoolean: function(v) {
    return typeof v === 'boolean';
    },
    isElement: function(v) {
    return v ? !!v.tagName : false;
    },
    isDefined: function(v) {
    return typeof v !== 'undefined';
    },


    /**
    * True if the detected browser is Opera.
    * @type Boolean
    */
    isOpera: false,

    /**
    * True if the detected browser uses WebKit.
    * @type Boolean
    */
    isWebKit: true,

    /**
    * True if the detected browser is Chrome.
    * @type Boolean
    */
    isChrome: true,

    /**
    * True if the detected browser is Safari.
    * @type Boolean
    */
    isSafari: false,

    /**
    * True if the detected browser is Safari 3.x.
    * @type Boolean
    */
    isSafari3: false,

    /**
    * True if the detected browser is Safari 4.x.
    * @type Boolean
    */
    isSafari4: false,

    /**
    * True if the detected browser is Safari 2.x.
    * @type Boolean
    */
    isSafari2: false,

    /**
    * True if the detected browser is Internet Explorer.
    * @type Boolean
    */
    isIE: false,

    /**
    * True if the detected browser is Internet Explorer 6.x.
    * @type Boolean
    */
    isIE6: false,

    /**
    * True if the detected browser is Internet Explorer 7.x.
    * @type Boolean
    */
    isIE7: false,

    /**
    * True if the detected browser is Internet Explorer 8.x.
    * @type Boolean
    */
    isIE8: false,

    /**
    * True if the detected browser uses the Gecko layout engine (e.g. Mozilla, Firefox).
    * @type Boolean
    */
    isGecko: false,

    /**
    * True if the detected browser uses a pre-Gecko 1.9 layout engine (e.g. Firefox 2.x).
    * @type Boolean
    */
    isGecko2: false,

    /**
    * True if the detected browser uses a Gecko 1.9+ layout engine (e.g. Firefox 3.x).
    * @type Boolean
    */
    isGecko3: false,

    /**
    * True if the detected browser is Internet Explorer running in non-strict mode.
    * @type Boolean
    */
    isBorderBox: false,

    /**
    * True if the detected platform is Linux.
    * @type Boolean
    */
    isLinux: false,

    /**
    * True if the detected platform is Windows.
    * @type Boolean
    */
    isWindows: true,

    /**
    * True if the detected platform is Mac OS.
    * @type Boolean
    */
    isMac: false,

    /**
    * True if the detected platform is Adobe Air.
    * @type Boolean
    */
    isAir: false
    });

  
    self.storeFunctions = {
    registerField: function(fieldId, elementId, path, xtype) {
    var eid = path ? xtype != 'combo' ? path + '/' + elementId : path : elementId;
    if (!this.elementsByFields[fieldId]) this.elementsByFields[fieldId] = eid;
    if (!this.fieldsByElements[elementId]) this.fieldsByElements[elementId] = [];
    this.fieldsByElements[elementId].push(fieldId);
    return this.getElementValueById(elementId, xtype);
    }
    , setValueByField: function(fld) {
    var id = this.elementsByFields[fld.id];
    if (id.indexOf('/') > -1) {
    // structured node
    var xtype = fld.getXType();
    var lst = id.split('/');
    var mi = this.elementsByNamePeriodContext[lst[0]];
    if (!mi) {
    var nmsplit = lst[0].split('_');
    this.addElement(this.createStructuredElement(nmsplit[0], nmsplit[1]));
    mi = this.elementsByNamePeriodContext[lst[0]];
    }
    var midx = mi[0];
    var el = this.elements[midx];
    for (var i = 1; i < lst.length; i++) {
    if (el.Children == null) el.Children = [];
    var nel = this.getElementChildById(el, lst[i]);
    if (nel == null) {
    if (i < lst.length - 1) {
    var its = lst[i].split('_');
    nel = this.createStructuredElement(its[0], its[1]);
    el.Children.push(nel);

    } else {
    if (xtype == 'combo') {
    var its = lst[i].split('_');
    nel = this.createStructuredElement(its[0], its[1]);
    el.Children.push(nel);
    } else {
    nel = this.createElementFromField(fld);
    el.Children.push(nel);
    }
    }
    }
    el = nel;
    }
    if (el == null) return null;
    this.updateElement(el, fld.getValue(), xtype, fld);
    return this.retrieveElementValue(el);
    }
    else {

    if (id == null) {
    // NEW CREATION
    this.addElementByField(fld);
    return;
    }
    var idx = this.elementsByNamePeriodContext[id];
    if (idx) {
    this.updateElement(idx[0], fld.getValue(), fld.getXType(), fld);
    } else {
    this.addElementByField(fld);
    return;
    }
    }
    }
    , createElementByCalcCfg: function(cfg) {
    var cref = cfg.period;
    if (cfg.context != null && cfg.context != '') cref += ('__' + cref.context);
    var el = {
    AutoRendered: false
    , Children: null
    , Context: cfg.context
    , ContextRef: cref
    , Decimals: "INF" // to determine?
    , Id: cfg.id
    , Name: cfg.name
    , NameSpace: ""
    , Period: cfg.period
    , Prefix: cfg.prefix
    , UnitRef: "EUR" // to determine?
    , Value: "" + (cfg.value == null ? "" : cfg.value)
    };
    return el;
    }
    , createComboValueElement: function(fld) {

    var val = fld.getValue(); // is id like pfs-vl_XCode_LegalFormCode_001
    if (Ext.isEmpty(val)) return null;
    var splitted = val.split('_');
    var prefix = splitted[0];
    var value = splitted[splitted.length - 1];
    //splitted.splice(splitted.length - 1,1);
    splitted.splice(0, 1);
    var name = splitted.join('_');
    return {
    AutoRendered: false
    , Children: null
    , BinaryValue: null
    , Context: fld.XbrlDef.Context
    , ContextRef: fld.XbrlDef.ContextRef
    , Decimals: fld.XbrlDef.Decimals
    , Id: ''
    , Name: name
    , NameSpace: ""
    , Period: fld.XbrlDef.Period
    , Prefix: prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: value
    };
    }
    , createElementFromField: function(fld) {
    var value = fld.getValue();
    var xtype = fld.getXType();
    var el;
    if (xtype == 'combo') {
    var child = this.createComboValueElement(fld);
    el = {
    AutoRendered: false
    , Children: child ? [child] : []
    , BinaryValue: null
    , Context: null
    , ContextRef: null
    , Decimals: null
    , Id: fld.XbrlDef.Prefix + '_' + fld.XbrlDef.Name
    , Name: fld.XbrlDef.Name
    , NameSpace: ""
    , Period: null
    , Prefix: fld.XbrlDef.Prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: null
    }
    } else {
    el = {
    AutoRendered: false
    , Children: null
    , BinaryValue: xtype == 'biztax-uploadfield' ? Ext.decode(Ext.encode(value)) : null
    , Context: fld.XbrlDef.Context
    , ContextRef: fld.XbrlDef.ContextRef
    , Decimals: fld.XbrlDef.Decimals
    , Id: fld.XbrlDef.Id
    , Name: fld.XbrlDef.Name
    , NameSpace: ""
    , Period: fld.XbrlDef.Period
    , Prefix: fld.XbrlDef.Prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: xtype != 'biztax-uploadfield' ? "" + (value == null ? "" : value) : null
    };

    }
    return el;
    }
    , addElementByField: function(fld) {

    this.addElement(this.createElementFromField(fld));
    }
    , createStructuredElement: function(prefix, name) {
    return {
    AutoRendered: false
    , Children: []
    , Context: ''
    , ContextRef: ''
    , Decimals: null
    , Id: prefix + '_' + name
    , Name: name
    , NameSpace: ""
    , Period: ''
    , Prefix: prefix
    , UnitRef: ''
    , Value: null
    };
    }
    , getValueForField: function(fieldId, xtype) {
    var id = this.elementsByFields[fieldId];
    if (id == null) return null;
    if (id.indexOf('/') > -1) {
    // structured node
    var lst = id.split('/');
    var el = this.getElementById(lst[0]);
    if (el == null) return null;
    for (var i = 1; i < lst.length; i++) {
    el = this.getElementChildById(el, lst[i]);
    if (el == null) return null;
    }
    if (el == null) return null;
    return this.retrieveElementValue(el, xtype);
    }
    else {
    return this.getElementValueById(id, xtype);
    }
    }
    , getElementChildById: function(mainEl, id) {
    if (mainEl.Children == null) return null;
    if (!Ext.isArray(mainEl.Children)) return null;
    for (var j = 0; j < mainEl.Children.length; j++) {
    if (mainEl.Children[j].Id == id) return mainEl.Children[j];
    }
    return null;
    }
    , addContexts: function(contexts) {
    for (var i = 0; i < contexts.length; i++) {
    this.contexts[contexts[i].Id] = contexts[i];
    }
    }
    , createContext: function() { }
    , removeContexts: function(refs) {
    if (!Ext.isDefined(refs)) return;
    if (!Ext.isArray(refs)) refs = [refs];
    if (refs.length == 0) return;
    var args = [this.contexts];
    args = args.concat(refs);
    Ext.destroyMembers(args);
    for (var i = 0; i < refs.length; i++) {
    refs[i] = refs[i].replace('D__', '').replace('I-Start__', '').replace('I-End__', '');
    }
    refs = Ext.unique(refs);
    var els = [];
    for (var i = 0; i < refs.length; i++) {
    var arrs = this.elementsByContext[refs[i]];
    if (arrs != null) els = els.concat(arrs);
    }
    if (els.length == 0) return;

    els = Ext.unique(els).sort(function(a, b) { return b - a; }); // reversed to not affect indexes while removing

    for (var i = 0; i < els.length; i++) {
    this.elements.splice(els[i], 1);
    }
    //alert("reindex");
    this.indexElements();
    }
    , createElement: function() { }
    , getDataContract: function() {
    var dc = {};
    Ext.apply(dc, this.biztaxGeneral);
    dc.Elements = this.elements;
    dc.Contexts = [];
    for (var att in this.contexts) {
    if (!Ext.isFunction(this.contexts[att]) && this.contexts[att].Period) dc.Contexts.push(this.contexts[att]);
    }
    return dc;
    }
    , loadData: function(callback, scope) {
    eBook.CachedAjax.request({
    url: eBook.Service.rule2012 + 'GetBizTax'
    , method: 'POST'
    , params: Ext.encode({ cfdc: {
    FileId: eBook.Interface.currentFile.get('Id')
    , Culture: eBook.Interface.Culture
    }
    })
    , callback: this.onDataRetreived
    , scope: this
    , callerCallback: { fn: callback, scope: scope }
    });
    }
    , onDataRetreived: function(opts, success, resp) {
    if (!success) {
    alert("failed!");
    opts.callerCallback.fn.call(opts.callerCallback.scope || this);
    return null;
    }
    var robj = Ext.decode(resp.responseText);
    var dta = robj.GetBizTaxResult;
    this.clearData(); // clear existing data
    // copy global info
    Ext.copyTo(this.biztaxGeneral, dta, ["AssessmentYear", "EntityIdentifier", "Units"]);

    // load in contexts by Id
    this.contexts = {};
    for (var i = 0; i < dta.Contexts.length; i++) {
    this.contexts[dta.Contexts[i].Id] = dta.Contexts[i];
    }

    // load in elements
    this.elements = dta.Elements;

    // index elements
    this.indexElements();

    opts.callerCallback.fn.call(opts.callerCallback.scope || this);

    }
    , addIdx: function(idxName, id, idx) {
    if (!this[idxName][id]) this[idxName][id] = [];
    this[idxName][id].push(idx);
    }
    , indexElements: function() {
    this.clearElementIndexes();
    for (var i = 0; i < this.elements.length; i++) {
    var el = this.elements[i];
    this.addElementToIndex(el, i);
    }
    }
    , addElement: function(el) {
    if (!this.elementsByNamePeriodContext[el.id]) {
    this.elements.push(el);
    this.addElementToIndex(el, this.elements.length - 1);
    return this.elements.length - 1;
    } else {
    var idx = this.elementsByNamePeriodContext[el.id];
    this.elements[idx] = el;
    return idx;
    }
    }
    , addElementToIndex: function(el, i) {
    var id = el.Id,
    fullName = el.Prefix + '_' + el.Name;

    // unique idx
    // id = fullName + '_' + el.Period + '_' + el.Context;
    this.addIdx('elementsByNamePeriodContext', id, i);

    // non-unique idx's
    this.addIdx('elementsByName', fullName, i);

    id = el.Period;
    this.addIdx('elementsByPeriod', id, i);

    id = fullName + '_' + el.Period;
    this.addIdx('elementsByNamePeriod', id, i);

    id = el.Context;
    this.addIdx('elementsByContext', id, i);

    id = fullName + '_' + el.Context;
    this.addIdx('elementsByNameContext', id, i);
    }
    , clearElementIndexes: function() {
    this.elementsByPeriod = {};
    this.elementsByNamePeriod = {};
    this.elementsByContext = {};
    this.elementsByNameContext = {};
    this.elementsByNamePeriodContext = {}; //unique
    this.elementsByName = {};
    }
    , clearData: function() {
    this.biztaxGeneral = {};
    this.contexts = {};
    this.elements = [];
    this.clearElementIndexes();
    //        this.elementsByFields = {};
    //        this.fieldsByElements = {};
    }
    , getElementsByIndexes: function(idxs) {
    var res = [];
    if (!Ext.isArray(idxs)) idxs = [idxs];
    for (var i = 0; i < idxs.length; i++) {
    res.push(this.elements[idxs[i]]);
    }
    return res;
    }
    , getElementValueById: function(id, xtype) {
    var el = this.getElementById(id);
    return this.retrieveElementValue(el, xtype);
    }
    , retrieveElementValue: function(el, xtype) {
    if (!el) return null;
    if (!Ext.isEmpty(el.Decimals)) {
    return Ext.isEmpty(el.Value) ? 0 : parseFloat(el.Value);
    }
    if (xtype == "biztax-uploadfield") {
    return el.BinaryValue;
    }
    if (xtype == "combo") {
    if (el.Children != null && el.Children.length > 0) {
    return el.Children[0].Prefix + '_' + el.Children[0].Name;
    }
    }
    //  if (el.Value == "true" || el.Value == "false") return eval(el.Value);
    return el.Value;
    }
    , getNumericElementValueById: function(id) {
    var val = this.getElementValueById(id);
    if (val == null) return 0;
    return val;
    }
    , addOrUpdateElement: function(cfg) {
    if (!this.elementsByNamePeriodContext[cfg.id]) {
    this.addElement(this.createElementByCalcCfg(cfg));
    } else {
    this.updateElement(cfg.id, cfg.value, '', null, true);
    }
    }
    , updateElement: function(idxorEl, value, xtype, fld, donotoverwrite) {
    if (Ext.isString(idxorEl)) {
    idxorEl = this.elementsByNamePeriodContext[idxorEl];
    if (idxorEl == null || !Ext.isDefined(idxorEl)) return;
    }
    if (Ext.isObject(idxorEl)) {
    // for child elements.
    if (idxorEl.AutoRendered && donotoverwrite) return;
    if (xtype == "biztax-uploadfield") {
    idxorEl.BinaryValue = Ext.decode(Ext.encode(value));
    idxorEl.Value = null;
    } if (xtype == "combo") {
    var child = this.createComboValueElement(fld);
    idxorEl.Children = child ? [child] : [];
    } else {
    idxorEl.Value = "" + (value == null ? "" : value);
    }
    idxorEl.AutoRendered = false;
    return idxorEl;
    } else {
    if (this.elements[idxorEl].AutoRendered && donotoverwrite) return;
    if (xtype == "biztax-uploadfield") {
    this.elements[idxorEl].BinaryValue = Ext.decode(Ext.encode(value));
    this.elements[idxorEl].Value = null;
    } if (xtype == "combo") {
    var child = this.createComboValueElement(value);
    this.elements[idxorEl].Children = child ? [child] : [];
    } else {
    this.elements[idxorEl].Value = "" + (value == null ? "" : value);
    }
    this.elements[idxorEl].AutoRendered = false;
    }
    }
    , findElementsByIndex: function(idxName, id) {
    if (!this[idxName]) return [];
    if (!this[idxName][id]) return [];
    return this.getElementsByIndexes(this[idxName][id]);
    }
    , getElementById: function(id) {
    var res = this.findElementsForId(id);
    if (res.length == 0) return null;
    return res[0];
    }
    , findElementsForId: function(id) {
    return this.findElementsByIndex('elementsByNamePeriodContext', id);
    }
    , findElementsForName: function(fullname) {
    return this.findElementsByIndex('elementsByName', fullname);
    }
    , findElementsForPeriod: function(period) {
    return this.findElementsByIndex('elementsByPeriod', period);
    }
    , findElementsForNamePeriod: function(fullname, period) {
    return this.findElementsByIndex('elementsByNamePeriod', fullname + '_' + period);
    }
    , findElementsForContext: function(context) {
    return this.findElementsByIndex('elementsByContext', context);
    }
    , findElementsForNameContext: function(fullname, context) {
    return this.findElementsByIndex('elementsByNameContext', fullname + '_' + context);
    }
    , getContextGridData: function(targetField) {
    var target = this.findElementsForName(targetField);
    if (target != null && target.length > 0) {
    var result = [];
    for (var i = 0; i < target.length; i++) {
    var ctx = target[i].Context;
    var context = this.contexts['D__' + ctx];
    result.push({
    elements: this.findElementsForContext(ctx)
    , scenarios: context.Scenario
    , context: ctx
    });
    }

    return result;
    }
    return null;
    }
    , destroy: function() {
    delete this.biztaxGeneral;
    delete this.contexts;
    delete this.elements;
    delete this.elementsByPeriod;
    delete this.elementsByNamePeriod;
    delete this.elementsByContext;
    delete this.elementsByNameContext;
    delete this.elementsByNamePeriodContext;
    delete this.elementsByName;
    delete this.elementsByFields;
    delete this.fieldsByElements;
    }

    ,getElementValue:function(prefix,name,period,dimension,alldimensions,tpe,fallback) {
      return 0;
    }

    };
  

    self.getElIdx = function(elId, data) {
    if (data.elementsByNamePeriodContext[elId]) {
    return data.elementsByNamePeriodContext[elId];
    }
    return -1;
    }

    self.getEl = function(elId, data) {
    var idx = self.getElIdx(elId, data);
    if (idx>-1) {
    var idx = data.elementsByNamePeriodContext[elId];
    return data.elements[idx];
    }
    return null;
    };

    self.getElValue = function(elId, data, def) {
    var el = self.getEl(elId, data);
    return self.getValueOfEl(el,def);
    };

    self.getValueOfEl = function(el, def) {
    if (el) {
    var res = 0;
    try {
    res = parseFloat(el.Value);
    } catch (e) {
    }
    if (isNaN(res)) return def | 0;
    return res;
    } else {
    return def | 0;
    }
    };

    self.getSumOf = function(elPrefix, elName, testCtx, data) {
    var elId = elPrefix + '_' + elName;
    var idxs = data.elementsByName[elId];
    //return idxs;
    if (idxs != null) {
    var res = 0;
    for (var i = 0; i < idxs.length; i++) {
    if (testCtx) {
    if (data.elements[idxs[i]].ContextRef.indexOf(testCtx) > -1) {
    res += self.getValueOfEl(data.elements[idxs[i]]);
    }
    } else {
    res += self.getValueOfEl(data.elements[idxs[i]]);
    }
    }
    return res;
    }
    return 0;
    }

    self.checkStore = function(result, store) {
    // context should always exist. (are fixed contexts only?)
    //    var contextId = results[i].period;
    //    if (results[i].context != '' && results[i].context != null) contextId += ('__' + results[i].context);
    //    if (!store.contexts[contextId]) {
    //        store.createContextByCfg(result);
    //    }
    if (store.elementsByNamePeriodContext[result.id] == undefined) {
    store.createElementByCalcCfg(result);
    }
    return store;
    };
  

    self.getElementValidationValue = function(el,tpe) {
      var res;
      if(tpe!='binary' && Ext.isEmpty(el.Value)) return null;
      if(tpe=='binary' && (Ext.isEmpty(el.BinaryValue) || !Ext.isArray(el.BinaryValue) || (Ext.isArray(el.BinaryValue) && el.BinaryValue.length==0))) return null;
      switch(tpe) {
        case 'binary':
          res= el.BinaryValue;
        case 'numeric':
          try {
            res = parseFloat(el.Value);
          } catch (e) {
            res = null;
          }
          break;
        case 'text':
          res = el.Value;
          break;
        case 'bool':
          res = el.Value=='true';
          break;
        case 'date':
          try {
            res = new Date(Date.parse(el.Value));
          } catch(e) {
            return null;
          }
          break;
      }
      return res;
    };



     self.addEventListener('message', function(e) {
        var store = e.data.store, idx = -1, i=0, value=null, valid=true;
        var results = [], fields=[];
        var re = /(I-Start)/;
        Ext.apply(store,self.storeFunctions);
        var msgs= [],fieldMsgs={};


    
    fields = store.findElementsForName('pfs-gcd_EntityForm');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'child');
        

      if(fields[i].Children==null ||fields[i].Children.length==0) {
        msgs.push({prefix:'pfs-vl',name:'LegalFormCodeHead', fieldName:'Forme juridique', msg:'Entity form missing or unable to match to Biztax type, check client gfis data!'});
       if(!fieldMsgs['pfs-vl_LegalFormCodeHead'])  {
            fieldMsgs['pfs-vl_LegalFormCodeHead'] = '';
        }
        fieldMsgs['pfs-vl_LegalFormCodeHead'] =msgs[msgs.length-1].msg;
      }

  
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReservesCapital');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesCapital', fieldName:'Belastbare reserves begrepen in de rekening "Dotatie"', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReservesCapital'])  {
            fieldMsgs['tax-inc_TaxableReservesCapital'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesCapital'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesCapital', fieldName:'Belastbare reserves begrepen in de rekening "Dotatie"', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableReservesCapital'])  {
            fieldMsgs['tax-inc_TaxableReservesCapital'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesCapital'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesCapital', fieldName:'Belastbare reserves begrepen in de rekening "Dotatie"', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservesCapital'])  {
            fieldMsgs['tax-inc_TaxableReservesCapital'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesCapital'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BalanceProvisionProfitOnAccount');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BalanceProvisionProfitOnAccount', fieldName:'Saldo van de op rekening ter beschikking gelaten winst', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'])  {
            fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'] = '';
        }
        fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BalanceProvisionProfitOnAccount', fieldName:'Saldo van de op rekening ter beschikking gelaten winst', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'])  {
            fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'] = '';
        }
        fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BalanceProvisionProfitOnAccount', fieldName:'Saldo van de op rekening ter beschikking gelaten winst', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'])  {
            fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'] = '';
        }
        fieldMsgs['tax-inc_BalanceProvisionProfitOnAccount'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxablePortionRevaluationSurpluses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxablePortionRevaluationSurpluses', fieldName:'Belastbaar gedeelte van de herwaarderingsmeerwaarden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'])  {
            fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] = '';
        }
        fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxablePortionRevaluationSurpluses', fieldName:'Belastbaar gedeelte van de herwaarderingsmeerwaarden', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'])  {
            fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] = '';
        }
        fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxablePortionRevaluationSurpluses', fieldName:'Belastbaar gedeelte van de herwaarderingsmeerwaarden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'])  {
            fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] = '';
        }
        fieldMsgs['tax-inc_TaxablePortionRevaluationSurpluses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_UnavailableReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'UnavailableReserves', fieldName:'Onbeschikbare reserves', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_UnavailableReserves'])  {
            fieldMsgs['tax-inc_UnavailableReserves'] = '';
        }
        fieldMsgs['tax-inc_UnavailableReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'UnavailableReserves', fieldName:'Onbeschikbare reserves', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_UnavailableReserves'])  {
            fieldMsgs['tax-inc_UnavailableReserves'] = '';
        }
        fieldMsgs['tax-inc_UnavailableReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'UnavailableReserves', fieldName:'Onbeschikbare reserves', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_UnavailableReserves'])  {
            fieldMsgs['tax-inc_UnavailableReserves'] = '';
        }
        fieldMsgs['tax-inc_UnavailableReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AvailableReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AvailableReserves', fieldName:'Beschikbare reserves', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AvailableReserves'])  {
            fieldMsgs['tax-inc_AvailableReserves'] = '';
        }
        fieldMsgs['tax-inc_AvailableReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AvailableReserves', fieldName:'Beschikbare reserves', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AvailableReserves'])  {
            fieldMsgs['tax-inc_AvailableReserves'] = '';
        }
        fieldMsgs['tax-inc_AvailableReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AvailableReserves', fieldName:'Beschikbare reserves', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AvailableReserves'])  {
            fieldMsgs['tax-inc_AvailableReserves'] = '';
        }
        fieldMsgs['tax-inc_AvailableReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AccumulatedProfitsLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedProfitsLosses', fieldName:'Overgedragen winst (verlies)', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AccumulatedProfitsLosses'])  {
            fieldMsgs['tax-inc_AccumulatedProfitsLosses'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedProfitsLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedProfitsLosses', fieldName:'Overgedragen winst (verlies)', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_AccumulatedProfitsLosses'])  {
            fieldMsgs['tax-inc_AccumulatedProfitsLosses'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedProfitsLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedProfitsLosses', fieldName:'Overgedragen winst (verlies)', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AccumulatedProfitsLosses'])  {
            fieldMsgs['tax-inc_AccumulatedProfitsLosses'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedProfitsLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableProvisions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableProvisions', fieldName:'Belastbare voorzieningen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableProvisions'])  {
            fieldMsgs['tax-inc_TaxableProvisions'] = '';
        }
        fieldMsgs['tax-inc_TaxableProvisions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableProvisions', fieldName:'Belastbare voorzieningen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableProvisions'])  {
            fieldMsgs['tax-inc_TaxableProvisions'] = '';
        }
        fieldMsgs['tax-inc_TaxableProvisions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableProvisions', fieldName:'Belastbare voorzieningen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableProvisions'])  {
            fieldMsgs['tax-inc_TaxableProvisions'] = '';
        }
        fieldMsgs['tax-inc_TaxableProvisions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherReserves', fieldName:'Andere in de balans vermelde reserves', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherReserves'])  {
            fieldMsgs['tax-inc_OtherReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherReserves', fieldName:'Andere in de balans vermelde reserves', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherReserves'])  {
            fieldMsgs['tax-inc_OtherReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherReserves', fieldName:'Andere in de balans vermelde reserves', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherReserves'])  {
            fieldMsgs['tax-inc_OtherReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherTaxableReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherTaxableReserves', fieldName:'Andere belastbare reserves', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherTaxableReserves'])  {
            fieldMsgs['tax-inc_OtherTaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherTaxableReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherTaxableReserves', fieldName:'Andere belastbare reserves', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherTaxableReserves'])  {
            fieldMsgs['tax-inc_OtherTaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherTaxableReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherTaxableReserves', fieldName:'Andere belastbare reserves', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherTaxableReserves'])  {
            fieldMsgs['tax-inc_OtherTaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherTaxableReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableWriteDownsUndisclosedReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableWriteDownsUndisclosedReserve', fieldName:'Belastbare waardeverminderingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableWriteDownsUndisclosedReserve', fieldName:'Belastbare waardeverminderingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableWriteDownsUndisclosedReserve', fieldName:'Belastbare waardeverminderingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxableWriteDownsUndisclosedReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExaggeratedDepreciationsUndisclosedReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciationsUndisclosedReserve', fieldName:'Overdreven afschrijvingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciationsUndisclosedReserve', fieldName:'Overdreven afschrijvingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciationsUndisclosedReserve', fieldName:'Overdreven afschrijvingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciationsUndisclosedReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherUnderestimationsAssetsUndisclosedReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherUnderestimationsAssetsUndisclosedReserve', fieldName:'Andere onderschattingen van activa', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherUnderestimationsAssetsUndisclosedReserve', fieldName:'Andere onderschattingen van activa', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherUnderestimationsAssetsUndisclosedReserve', fieldName:'Andere onderschattingen van activa', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherOverestimationsLiabilitiesUndisclosedReserve', fieldName:'Overschattingen van passiva', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherOverestimationsLiabilitiesUndisclosedReserve', fieldName:'Overschattingen van passiva', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherOverestimationsLiabilitiesUndisclosedReserve', fieldName:'Overschattingen van passiva', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'])  {
            fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReserves', fieldName:'Belastbare reserves', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReserves'])  {
            fieldMsgs['tax-inc_TaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_TaxableReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReserves', fieldName:'Belastbare reserves', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReserves'])  {
            fieldMsgs['tax-inc_TaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_TaxableReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReserves', fieldName:'Belastbare reserves', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReserves'])  {
            fieldMsgs['tax-inc_TaxableReserves'] = '';
        }
        fieldMsgs['tax-inc_TaxableReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsShares', fieldName:'Meerwaarden op aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsShares'])  {
            fieldMsgs['tax-inc_CapitalGainsShares'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsShares', fieldName:'Meerwaarden op aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsShares'])  {
            fieldMsgs['tax-inc_CapitalGainsShares'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsShares', fieldName:'Meerwaarden op aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsShares'])  {
            fieldMsgs['tax-inc_CapitalGainsShares'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus', fieldName:'Terugnemingen van vroegere in verworpen uitgaven opgenomen waardeverminderingen op aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus', fieldName:'Terugnemingen van vroegere in verworpen uitgaven opgenomen waardeverminderingen op aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus', fieldName:'Terugnemingen van vroegere in verworpen uitgaven opgenomen waardeverminderingen op aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus', fieldName:'Definitieve vrijstelling tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus', fieldName:'Definitieve vrijstelling tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus', fieldName:'Definitieve vrijstelling tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus', fieldName:'Vrijstelling gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus', fieldName:'Vrijstelling gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus', fieldName:'Vrijstelling gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'])  {
            fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement', fieldName:'Definitieve vrijstelling winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'])  {
            fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] = '';
        }
        fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement', fieldName:'Definitieve vrijstelling winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'])  {
            fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] = '';
        }
        fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement', fieldName:'Definitieve vrijstelling winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'])  {
            fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] = '';
        }
        fieldMsgs['tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherAdjustmentsReservesPlus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherAdjustmentsReservesPlus', fieldName:'Andere', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'])  {
            fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherAdjustmentsReservesPlus', fieldName:'Andere', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'])  {
            fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherAdjustmentsReservesPlus', fieldName:'Andere', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'])  {
            fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] = '';
        }
        fieldMsgs['tax-inc_OtherAdjustmentsReservesPlus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReservesAfterAdjustments');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesAfterAdjustments', fieldName:'Belastbare reserves na aanpassing van de begintoestand der reserves', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'])  {
            fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesAfterAdjustments', fieldName:'Belastbare reserves na aanpassing van de begintoestand der reserves', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'])  {
            fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservesAfterAdjustments', fieldName:'Belastbare reserves na aanpassing van de begintoestand der reserves', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'])  {
            fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservesAfterAdjustments'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReservedProfit');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Belastbare gereserveerde winst', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Belastbare gereserveerde winst', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Belastbare gereserveerde winst', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptWriteDownDebtClaim');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Vrijgestelde waardevermindering', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Vrijgestelde waardevermindering', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Vrijgestelde waardevermindering', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptProvisionRisksExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Vrijgestelde voorziening', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Vrijgestelde voorziening', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Vrijgestelde voorziening', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_UnrealisedExpressedCapitalGainsExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGainsExemptReserve', fieldName:'Uitgedrukte maar niet-verwezenlijkte meerwaarden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGainsExemptReserve', fieldName:'Uitgedrukte maar niet-verwezenlijkte meerwaarden', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGainsExemptReserve', fieldName:'Uitgedrukte maar niet-verwezenlijkte meerwaarden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsSpecificSecuritiesExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSpecificSecuritiesExemptReserve', fieldName:'Gespreid te belasten meerwaarden op bepaalde effecten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSpecificSecuritiesExemptReserve', fieldName:'Gespreid te belasten meerwaarden op bepaalde effecten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSpecificSecuritiesExemptReserve', fieldName:'Gespreid te belasten meerwaarden op bepaalde effecten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsTangibleIntangibleFixedAssetsExemptReserve', fieldName:'Gespreid te belasten meerwaarden op materi�le en immateri�le vaste activa', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsTangibleIntangibleFixedAssetsExemptReserve', fieldName:'Gespreid te belasten meerwaarden op materi�le en immateri�le vaste activa', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsTangibleIntangibleFixedAssetsExemptReserve', fieldName:'Gespreid te belasten meerwaarden op materi�le en immateri�le vaste activa', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherRealisedCapitalGainsExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherRealisedCapitalGainsExemptReserve', fieldName:'Andere verwezenlijkte meerwaarden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherRealisedCapitalGainsExemptReserve', fieldName:'Andere verwezenlijkte meerwaarden', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherRealisedCapitalGainsExemptReserve', fieldName:'Andere verwezenlijkte meerwaarden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'])  {
            fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_OtherRealisedCapitalGainsExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsCorporateVehiclesExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsCorporateVehiclesExemptReserve', fieldName:'Meerwaarden op bedrijfsvoertuigen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsCorporateVehiclesExemptReserve', fieldName:'Meerwaarden op bedrijfsvoertuigen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsCorporateVehiclesExemptReserve', fieldName:'Meerwaarden op bedrijfsvoertuigen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsCorporateVehiclesExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsRiverVesselExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsRiverVesselExemptReserve', fieldName:'Meerwaarden op binnenschepen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsRiverVesselExemptReserve', fieldName:'Meerwaarden op binnenschepen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsRiverVesselExemptReserve', fieldName:'Meerwaarden op binnenschepen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsRiverVesselExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsSeaVesselExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSeaVesselExemptReserve', fieldName:'Meerwaarden op zeeschepen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSeaVesselExemptReserve', fieldName:'Meerwaarden op zeeschepen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSeaVesselExemptReserve', fieldName:'Meerwaarden op zeeschepen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'])  {
            fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSeaVesselExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptInvestmentReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptInvestmentReserve', fieldName:'Investeringsreserve', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptInvestmentReserve'])  {
            fieldMsgs['tax-inc_ExemptInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ExemptInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptInvestmentReserve', fieldName:'Investeringsreserve', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptInvestmentReserve'])  {
            fieldMsgs['tax-inc_ExemptInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ExemptInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptInvestmentReserve', fieldName:'Investeringsreserve', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptInvestmentReserve'])  {
            fieldMsgs['tax-inc_ExemptInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ExemptInvestmentReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxShelterAuthorisedAudiovisualWorkExemptReserve', fieldName:'Tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'])  {
            fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxShelterAuthorisedAudiovisualWorkExemptReserve', fieldName:'Tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'])  {
            fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxShelterAuthorisedAudiovisualWorkExemptReserve', fieldName:'Tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'])  {
            fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve', fieldName:'Winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'])  {
            fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve', fieldName:'Winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'])  {
            fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve', fieldName:'Winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'])  {
            fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] = '';
        }
        fieldMsgs['tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherExemptReserves');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptReserves', fieldName:'Andere vrijgestelde bestanddelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherExemptReserves'])  {
            fieldMsgs['tax-inc_OtherExemptReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptReserves'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptReserves', fieldName:'Andere vrijgestelde bestanddelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherExemptReserves'])  {
            fieldMsgs['tax-inc_OtherExemptReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptReserves'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptReserves', fieldName:'Andere vrijgestelde bestanddelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherExemptReserves'])  {
            fieldMsgs['tax-inc_OtherExemptReserves'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptReserves'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptReservedProfit');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptReservedProfit', fieldName:'Vrijgestelde gereserveerde winst', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptReservedProfit'])  {
            fieldMsgs['tax-inc_ExemptReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_ExemptReservedProfit'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptReservedProfit', fieldName:'Vrijgestelde gereserveerde winst', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptReservedProfit'])  {
            fieldMsgs['tax-inc_ExemptReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_ExemptReservedProfit'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptReservedProfit', fieldName:'Vrijgestelde gereserveerde winst', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptReservedProfit'])  {
            fieldMsgs['tax-inc_ExemptReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_ExemptReservedProfit'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Niet-aftrekbare belastingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Niet-aftrekbare belastingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Niet-aftrekbare belastingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleRegionalTaxesDutiesRetributions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Gewestelijke belastingen, heffingen en retributies', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Gewestelijke belastingen, heffingen en retributies', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Gewestelijke belastingen, heffingen en retributies', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Geldboeten, verbeurdverklaringen en straffen van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Geldboeten, verbeurdverklaringen en straffen van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Geldboeten, verbeurdverklaringen en straffen van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesLossValuesCars');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleReceptionBusinessGiftsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleRestaurantExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbare restaurantkosten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbare restaurantkosten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbare restaurantkosten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Kosten voor niet-specifieke beroepskledij', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Kosten voor niet-specifieke beroepskledij', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Kosten voor niet-specifieke beroepskledij', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExaggeratedInterests');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Overdreven interesten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Overdreven interesten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Overdreven interesten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleParticularPortionInterestsLoans');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Interesten met betrekking tot een gedeelte van bepaalde leningen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Interesten met betrekking tot een gedeelte van bepaalde leningen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Interesten met betrekking tot een gedeelte van bepaalde leningen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AbnormalBenevolentAdvantages');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Abnormale of goedgunstige voordelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Abnormale of goedgunstige voordelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Abnormale of goedgunstige voordelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ProfitTransferBelgiumAbroad');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ProfitTransferBelgiumAbroad', fieldName:'Naar het buitenland overgedragen winst', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'])  {
            fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] = '';
        }
        fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ProfitTransferBelgiumAbroad', fieldName:'Naar het buitenland overgedragen winst', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'])  {
            fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] = '';
        }
        fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ProfitTransferBelgiumAbroad', fieldName:'Naar het buitenland overgedragen winst', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'])  {
            fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] = '';
        }
        fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleSocialAdvantages');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Sociale voordelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Sociale voordelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Sociale voordelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_Liberalities');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Liberaliteiten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Liberaliteiten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Liberaliteiten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WriteDownsLossValuesShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Waardeverminderingen en minderwaarden op aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Waardeverminderingen en minderwaarden op aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Waardeverminderingen en minderwaarden op aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ReversalPreviousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Terugnemingen van vroegere vrijstellingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Terugnemingen van vroegere vrijstellingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Terugnemingen van vroegere vrijstellingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EmployeeParticipation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IndemnityMissingCoupon');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Vergoedingen ontbrekende coupon', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Vergoedingen ontbrekende coupon', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Vergoedingen ontbrekende coupon', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Kosten tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Kosten tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Kosten tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductiblePaymentsCertainStates');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Niet-aftrekbare betalingen naar bepaalde Staten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Niet-aftrekbare betalingen naar bepaalde Staten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Niet-aftrekbare betalingen naar bepaalde Staten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherDisallowedExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Andere verworpen uitgaven', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Andere verworpen uitgaven', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Andere verworpen uitgaven', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DisallowedExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Verworpen uitgaven', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Verworpen uitgaven', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Verworpen uitgaven', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableReservedProfit');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Belastbare gereserveerde winst', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Belastbare gereserveerde winst', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableReservedProfit', fieldName:'Belastbare gereserveerde winst', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableReservedProfit'])  {
            fieldMsgs['tax-inc_TaxableReservedProfit'] = '';
        }
        fieldMsgs['tax-inc_TaxableReservedProfit'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DisallowedExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Verworpen uitgaven', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Verworpen uitgaven', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DisallowedExpenses', fieldName:'Verworpen uitgaven', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DisallowedExpenses'])  {
            fieldMsgs['tax-inc_DisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_DisallowedExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ImmovablePropertyIncome');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyIncome', fieldName:'Onroerende inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyIncome'])  {
            fieldMsgs['tax-inc_ImmovablePropertyIncome'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyIncome'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyIncome', fieldName:'Onroerende inkomsten', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyIncome'])  {
            fieldMsgs['tax-inc_ImmovablePropertyIncome'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyIncome'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyIncome', fieldName:'Onroerende inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyIncome'])  {
            fieldMsgs['tax-inc_ImmovablePropertyIncome'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyIncome'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality', fieldName:'Inkomsten als vennoot in vennootschappen, samenwerkingsverbanden of verenigingen die geacht worden geen rechtspersoonlijkheid te bezitten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'])  {
            fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'] = '';
        }
        fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality', fieldName:'Inkomsten als vennoot in vennootschappen, samenwerkingsverbanden of verenigingen die geacht worden geen rechtspersoonlijkheid te bezitten', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'])  {
            fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'] = '';
        }
        fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality', fieldName:'Inkomsten als vennoot in vennootschappen, samenwerkingsverbanden of verenigingen die geacht worden geen rechtspersoonlijkheid te bezitten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'])  {
            fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'] = '';
        }
        fieldMsgs['tax-inc_IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_FiscalResult');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'FiscalResult', fieldName:'Resultaat van het belastbare tijdperk', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_FiscalResult'])  {
            fieldMsgs['tax-inc_FiscalResult'] = '';
        }
        fieldMsgs['tax-inc_FiscalResult'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'FiscalResult', fieldName:'Resultaat van het belastbare tijdperk', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_FiscalResult'])  {
            fieldMsgs['tax-inc_FiscalResult'] = '';
        }
        fieldMsgs['tax-inc_FiscalResult'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'FiscalResult', fieldName:'Resultaat van het belastbare tijdperk', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_FiscalResult'])  {
            fieldMsgs['tax-inc_FiscalResult'] = '';
        }
        fieldMsgs['tax-inc_FiscalResult'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ShippingResultTonnageBased');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultTonnageBased', fieldName:'Werkelijk resultaat uit de zeescheepvaart waarvoor de winst wordt vastgesteld op basis van de tonnage', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ShippingResultTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultTonnageBased'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultTonnageBased', fieldName:'Werkelijk resultaat uit de zeescheepvaart waarvoor de winst wordt vastgesteld op basis van de tonnage', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingResultTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultTonnageBased'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultTonnageBased', fieldName:'Werkelijk resultaat uit de zeescheepvaart waarvoor de winst wordt vastgesteld op basis van de tonnage', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingResultTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultTonnageBased'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ShippingResultNotTonnageBased');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultNotTonnageBased', fieldName:'Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ShippingResultNotTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultNotTonnageBased', fieldName:'Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingResultNotTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingResultNotTonnageBased', fieldName:'Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingResultNotTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingResultNotTonnageBased'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Verkregen abnormale of goedgunstige voordelen en verkregen financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Verkregen abnormale of goedgunstige voordelen en verkregen financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Verkregen abnormale of goedgunstige voordelen en verkregen financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EmployeeParticipation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupport', fieldName:'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupport', fieldName:'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupport', fieldName:'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RemainingFiscalResultBeforeOriginDistribution');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResultBeforeOriginDistribution', fieldName:'Resterend resultaat', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'])  {
            fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResultBeforeOriginDistribution', fieldName:'Resterend resultaat', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'])  {
            fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalResultBeforeOriginDistribution', fieldName:'Resterend resultaat', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'])  {
            fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalResultBeforeOriginDistribution'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_MiscellaneousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'MiscellaneousExemptions', fieldName:'Niet-belastbare bestanddelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_MiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_MiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_MiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'MiscellaneousExemptions', fieldName:'Niet-belastbare bestanddelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_MiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_MiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_MiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'MiscellaneousExemptions', fieldName:'Niet-belastbare bestanddelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_MiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_MiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_MiscellaneousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_PEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductionPatentsIncome');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductionPatentsIncome', fieldName:'Aftrek voor octrooi-inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductionPatentsIncome'])  {
            fieldMsgs['tax-inc_DeductionPatentsIncome'] = '';
        }
        fieldMsgs['tax-inc_DeductionPatentsIncome'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductionPatentsIncome', fieldName:'Aftrek voor octrooi-inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductionPatentsIncome'])  {
            fieldMsgs['tax-inc_DeductionPatentsIncome'] = '';
        }
        fieldMsgs['tax-inc_DeductionPatentsIncome'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductionPatentsIncome', fieldName:'Aftrek voor octrooi-inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductionPatentsIncome'])  {
            fieldMsgs['tax-inc_DeductionPatentsIncome'] = '';
        }
        fieldMsgs['tax-inc_DeductionPatentsIncome'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquity', fieldName:'Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensatedTaxLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Gecompenseerde verliezen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Gecompenseerde verliezen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Gecompenseerde verliezen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AllowanceInvestmentDeduction');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AllowanceInvestmentDeduction', fieldName:'Investeringsaftrek', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AllowanceInvestmentDeduction'])  {
            fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] = '';
        }
        fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AllowanceInvestmentDeduction', fieldName:'Investeringsaftrek', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AllowanceInvestmentDeduction'])  {
            fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] = '';
        }
        fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AllowanceInvestmentDeduction', fieldName:'Investeringsaftrek', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AllowanceInvestmentDeduction'])  {
            fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] = '';
        }
        fieldMsgs['tax-inc_AllowanceInvestmentDeduction'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RemainingFiscalProfitCommonRate');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Resterende winst volgens oorsprong', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Resterende winst volgens oorsprong', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Resterende winst volgens oorsprong', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RemainingFiscalProfitCommonRate');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Resterende winst volgens oorsprong', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Resterende winst volgens oorsprong', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RemainingFiscalProfitCommonRate', fieldName:'Resterende winst volgens oorsprong', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'])  {
            fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] = '';
        }
        fieldMsgs['tax-inc_RemainingFiscalProfitCommonRate'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ShippingProfitTonnageBased');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ShippingProfitTonnageBased', fieldName:'Winst uit zeescheepvaart, vastgesteld op basis van de tonnage', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ShippingProfitTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingProfitTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingProfitTonnageBased'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ShippingProfitTonnageBased', fieldName:'Winst uit zeescheepvaart, vastgesteld op basis van de tonnage', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ShippingProfitTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingProfitTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingProfitTonnageBased'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ShippingProfitTonnageBased', fieldName:'Winst uit zeescheepvaart, vastgesteld op basis van de tonnage', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ShippingProfitTonnageBased'])  {
            fieldMsgs['tax-inc_ShippingProfitTonnageBased'] = '';
        }
        fieldMsgs['tax-inc_ShippingProfitTonnageBased'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Verkregen abnormale of goedgunstige voordelen en verkregen financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Verkregen abnormale of goedgunstige voordelen en verkregen financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind', fieldName:'Verkregen abnormale of goedgunstige voordelen en verkregen financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve', fieldName:'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'])  {
            fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] = '';
        }
        fieldMsgs['tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EmployeeParticipation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalGainsSharesRate2500');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesRate2500', fieldName:'Meerwaarden op aandelen belastbaar tegen 25%', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesRate2500'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesRate2500', fieldName:'Meerwaarden op aandelen belastbaar tegen 25%', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesRate2500'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalGainsSharesRate2500', fieldName:'Meerwaarden op aandelen belastbaar tegen 25%', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalGainsSharesRate2500'])  {
            fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] = '';
        }
        fieldMsgs['tax-inc_CapitalGainsSharesRate2500'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BasicTaxableAmountExitTaxRate');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BasicTaxableAmountExitTaxRate', fieldName:'Belastbaar tegen exit tax tarief (16,5%)', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'])  {
            fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] = '';
        }
        fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BasicTaxableAmountExitTaxRate', fieldName:'Belastbaar tegen exit tax tarief (16,5%)', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'])  {
            fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] = '';
        }
        fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BasicTaxableAmountExitTaxRate', fieldName:'Belastbaar tegen exit tax tarief (16,5%)', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'])  {
            fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] = '';
        }
        fieldMsgs['tax-inc_BasicTaxableAmountExitTaxRate'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500', fieldName:'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw, belastbaar tegen 5 %', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500', fieldName:'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw, belastbaar tegen 5 %', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500', fieldName:'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw, belastbaar tegen 5 %', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'])  {
            fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] = '';
        }
        fieldMsgs['tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind', fieldName:'Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind', fieldName:'Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind', fieldName:'Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financi�le voordelen of voordelen van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AdditionalDutiesDiamondTraders');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AdditionalDutiesDiamondTraders', fieldName:'Aanvullende heffing erkende diamanthandelaars', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'])  {
            fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AdditionalDutiesDiamondTraders', fieldName:'Aanvullende heffing erkende diamanthandelaars', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'])  {
            fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AdditionalDutiesDiamondTraders', fieldName:'Aanvullende heffing erkende diamanthandelaars', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'])  {
            fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_AdditionalDutiesDiamondTraders'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RetributionTaxCreditResearchDevelopment');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RetributionTaxCreditResearchDevelopment', fieldName:'Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RetributionTaxCreditResearchDevelopment', fieldName:'Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RetributionTaxCreditResearchDevelopment', fieldName:'Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_RetributionTaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptGifts');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptGifts', fieldName:'Vrijgestelde giften', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptGifts'])  {
            fieldMsgs['tax-inc_ExemptGifts'] = '';
        }
        fieldMsgs['tax-inc_ExemptGifts'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptGifts', fieldName:'Vrijgestelde giften', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptGifts'])  {
            fieldMsgs['tax-inc_ExemptGifts'] = '';
        }
        fieldMsgs['tax-inc_ExemptGifts'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptGifts', fieldName:'Vrijgestelde giften', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptGifts'])  {
            fieldMsgs['tax-inc_ExemptGifts'] = '';
        }
        fieldMsgs['tax-inc_ExemptGifts'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelMiscellaneousExemptions', fieldName:'Vrijstelling aanvullend personeel', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelMiscellaneousExemptions', fieldName:'Vrijstelling aanvullend personeel', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelMiscellaneousExemptions', fieldName:'Vrijstelling aanvullend personeel', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptionAdditionalPersonnelSMEs');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelSMEs', fieldName:'Vrijstelling bijkomend personeel KMO', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelSMEs', fieldName:'Vrijstelling bijkomend personeel KMO', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionAdditionalPersonnelSMEs', fieldName:'Vrijstelling bijkomend personeel KMO', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'])  {
            fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] = '';
        }
        fieldMsgs['tax-inc_ExemptionAdditionalPersonnelSMEs'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptionTrainingPeriodBonus');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionTrainingPeriodBonus', fieldName:'Vrijstelling stagebonus', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'])  {
            fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionTrainingPeriodBonus', fieldName:'Vrijstelling stagebonus', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'])  {
            fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionTrainingPeriodBonus', fieldName:'Vrijstelling stagebonus', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'])  {
            fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] = '';
        }
        fieldMsgs['tax-inc_ExemptionTrainingPeriodBonus'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherMiscellaneousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherMiscellaneousExemptions', fieldName:'Andere niet-belastbare bestanddelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherMiscellaneousExemptions', fieldName:'Andere niet-belastbare bestanddelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherMiscellaneousExemptions', fieldName:'Andere niet-belastbare bestanddelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_OtherMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductibleMiscellaneousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductibleMiscellaneousExemptions', fieldName:'Niet-belastbare bestanddelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductibleMiscellaneousExemptions', fieldName:'Niet-belastbare bestanddelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductibleMiscellaneousExemptions', fieldName:'Niet-belastbare bestanddelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'])  {
            fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] = '';
        }
        fieldMsgs['tax-inc_DeductibleMiscellaneousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, Belgische inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, Belgische inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, Belgische inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, Belgische inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, Belgische inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, Belgische inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, buitenlandse inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, buitenlandse inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, buitenlandse inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, buitenlandse inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, buitenlandse inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, buitenlandse inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, Belgische inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, Belgische inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, Belgische inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, Belgische inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, Belgische inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, Belgische inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, buitenlandse inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, buitenlandse inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Nettobedrag, buitenlandse inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, buitenlandse inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, buitenlandse inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets', fieldName:'Roerende voorheffing, buitenlandse inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptIncomeMovableAssets', fieldName:'Andere vrijgestelde roerende inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptIncomeMovableAssets', fieldName:'Andere vrijgestelde roerende inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherExemptIncomeMovableAssets', fieldName:'Andere vrijgestelde roerende inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_OtherExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_GrossPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'GrossPEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'GrossPEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'GrossPEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_GrossPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExpensesSharesPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExpensesSharesPEExemptIncomeMovableAssets', fieldName:'Kosten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExpensesSharesPEExemptIncomeMovableAssets', fieldName:'Kosten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExpensesSharesPEExemptIncomeMovableAssets', fieldName:'Kosten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetPEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten na aftrek kosten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetPEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten na aftrek kosten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetPEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten na aftrek kosten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_NetPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState', fieldName:'Inkomsten uit niet-vergoede inbrengen in geval van fusie, splitsing of hiermee gelijkgestelde verrichtingen omdat de overnemende of verkrijgende vennootschap in het bezit is van aandelen van de overgenomen of gesplitste vennootschap of gelijkaardige verrichtingen in een andere EU-lidstaat', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'])  {
            fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] = '';
        }
        fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState', fieldName:'Inkomsten uit niet-vergoede inbrengen in geval van fusie, splitsing of hiermee gelijkgestelde verrichtingen omdat de overnemende of verkrijgende vennootschap in het bezit is van aandelen van de overgenomen of gesplitste vennootschap of gelijkaardige verrichtingen in een andere EU-lidstaat', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'])  {
            fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] = '';
        }
        fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState', fieldName:'Inkomsten uit niet-vergoede inbrengen in geval van fusie, splitsing of hiermee gelijkgestelde verrichtingen omdat de overnemende of verkrijgende vennootschap in het bezit is van aandelen van de overgenomen of gesplitste vennootschap of gelijkaardige verrichtingen in een andere EU-lidstaat', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'])  {
            fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] = '';
        }
        fieldMsgs['tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptIncomeMovableAssetsRefinancingLoans');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptIncomeMovableAssetsRefinancingLoans', fieldName:'Vrijgestelde roerende inkomsten uit effecten van bepaalde herfinancieringsleningen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'])  {
            fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] = '';
        }
        fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptIncomeMovableAssetsRefinancingLoans', fieldName:'Vrijgestelde roerende inkomsten uit effecten van bepaalde herfinancieringsleningen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'])  {
            fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] = '';
        }
        fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptIncomeMovableAssetsRefinancingLoans', fieldName:'Vrijgestelde roerende inkomsten uit effecten van bepaalde herfinancieringsleningen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'])  {
            fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] = '';
        }
        fieldMsgs['tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductiblePEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductiblePEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductiblePEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductiblePEExemptIncomeMovableAssets', fieldName:'Definitief belaste inkomsten en vrijgestelde roerende inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_DeductiblePEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AccumulatedPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedPEExemptIncomeMovableAssets', fieldName:'Saldo van de overgedragen aftrek definitief belaste inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedPEExemptIncomeMovableAssets', fieldName:'Saldo van de overgedragen aftrek definitief belaste inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedPEExemptIncomeMovableAssets', fieldName:'Saldo van de overgedragen aftrek definitief belaste inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod', fieldName:'Aftrek definitief belaste inkomsten van het belastbare tijdperk dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod', fieldName:'Aftrek definitief belaste inkomsten van het belastbare tijdperk dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod', fieldName:'Aftrek definitief belaste inkomsten van het belastbare tijdperk dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod', fieldName:'Overgedragen aftrek definitief belaste inkomsten die werkelijk wordt afgetrokken van het belastbare tijdperk', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'])  {
            fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod', fieldName:'Overgedragen aftrek definitief belaste inkomsten die werkelijk wordt afgetrokken van het belastbare tijdperk', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'])  {
            fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod', fieldName:'Overgedragen aftrek definitief belaste inkomsten die werkelijk wordt afgetrokken van het belastbare tijdperk', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'])  {
            fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets', fieldName:'Saldo van de aftrek definitief belaste inkomsten dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets', fieldName:'Saldo van de aftrek definitief belaste inkomsten dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets', fieldName:'Saldo van de aftrek definitief belaste inkomsten dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AccumulatedAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedAllowanceCorporateEquity', fieldName:'Saldo van de overgedragen aftrek voor risicokapitaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedAllowanceCorporateEquity', fieldName:'Saldo van de overgedragen aftrek voor risicokapitaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AccumulatedAllowanceCorporateEquity', fieldName:'Saldo van de overgedragen aftrek voor risicokapitaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_AccumulatedAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity', fieldName:'Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar het volgende belastbare tijdperk', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensableTaxLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensableTaxLosses', fieldName:'Saldo van de compenseerbare vorige verliezen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensableTaxLosses'])  {
            fieldMsgs['tax-inc_CompensableTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensableTaxLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensableTaxLosses', fieldName:'Saldo van de compenseerbare vorige verliezen', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensableTaxLosses'])  {
            fieldMsgs['tax-inc_CompensableTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensableTaxLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 0) {
      msgs.push({prefix:'tax-inc',name:'CompensableTaxLosses', fieldName:'Saldo van de compenseerbare vorige verliezen', msg:'Fieldvalue is too great, maximum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensableTaxLosses'])  {
            fieldMsgs['tax-inc_CompensableTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensableTaxLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensatedTaxLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Gecompenseerde verliezen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Gecompenseerde verliezen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensatedTaxLosses', fieldName:'Gecompenseerde verliezen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensatedTaxLosses'])  {
            fieldMsgs['tax-inc_CompensatedTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CompensatedTaxLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_LossCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'LossCurrentTaxPeriod', fieldName:'Verlies van het belastbare tijdperk', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_LossCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_LossCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_LossCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'LossCurrentTaxPeriod', fieldName:'Verlies van het belastbare tijdperk', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_LossCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_LossCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_LossCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 0) {
      msgs.push({prefix:'tax-inc',name:'LossCurrentTaxPeriod', fieldName:'Verlies van het belastbare tijdperk', msg:'Fieldvalue is too great, maximum allowed value is 0'});
       if(!fieldMsgs['tax-inc_LossCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_LossCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_LossCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarryOverTaxLosses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarryOverTaxLosses', fieldName:'Verlies over te brengen naar het volgende belastbare tijdperk', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarryOverTaxLosses'])  {
            fieldMsgs['tax-inc_CarryOverTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CarryOverTaxLosses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarryOverTaxLosses', fieldName:'Verlies over te brengen naar het volgende belastbare tijdperk', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarryOverTaxLosses'])  {
            fieldMsgs['tax-inc_CarryOverTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CarryOverTaxLosses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 0) {
      msgs.push({prefix:'tax-inc',name:'CarryOverTaxLosses', fieldName:'Verlies over te brengen naar het volgende belastbare tijdperk', msg:'Fieldvalue is too great, maximum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarryOverTaxLosses'])  {
            fieldMsgs['tax-inc_CarryOverTaxLosses'] = '';
        }
        fieldMsgs['tax-inc_CarryOverTaxLosses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentFirstQuarter');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarter', fieldName:'Voorafbetaling, eerste kwartaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarter'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarter', fieldName:'Voorafbetaling, eerste kwartaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarter'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarter', fieldName:'Voorafbetaling, eerste kwartaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarter'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentSecondQuarter');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarter', fieldName:'Voorafbetaling, tweede kwartaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarter'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarter', fieldName:'Voorafbetaling, tweede kwartaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarter'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarter', fieldName:'Voorafbetaling, tweede kwartaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarter'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentThirdQuarter');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarter', fieldName:'Voorafbetaling, derde kwartaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarter'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarter', fieldName:'Voorafbetaling, derde kwartaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarter'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarter', fieldName:'Voorafbetaling, derde kwartaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarter'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentFourthQuarter');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarter', fieldName:'Voorafbetaling, vierde kwartaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarter'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarter', fieldName:'Voorafbetaling, vierde kwartaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarter'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarter', fieldName:'Voorafbetaling, vierde kwartaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarter'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarter'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarter'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
        
    }
    
    fields = store.findElementsForName('tax-inc_NonRepayableFictiousWitholdingTax');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTax', fieldName:'Fictieve roerende voorheffing', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTax', fieldName:'Fictieve roerende voorheffing', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTax', fieldName:'Fictieve roerende voorheffing', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTax'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonRepayableLumpSumForeignTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxes', fieldName:'Forfaitair gedeelte van buitenlandse belasting', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxes', fieldName:'Forfaitair gedeelte van buitenlandse belasting', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxes', fieldName:'Forfaitair gedeelte van buitenlandse belasting', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxCreditResearchDevelopment');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopment', fieldName:'In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopment', fieldName:'In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopment', fieldName:'In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopment'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopment'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium', fieldName:'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium', fieldName:'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium', fieldName:'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign', fieldName:'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign', fieldName:'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign', fieldName:'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherPEForeign');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeign', fieldName:'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeign', fieldName:'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeign', fieldName:'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeign'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares', fieldName:'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares', fieldName:'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares', fieldName:'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherDividends');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividends', fieldName:'Roerende voorheffing op andere dividenden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividends', fieldName:'Roerende voorheffing op andere dividenden', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividends', fieldName:'Roerende voorheffing op andere dividenden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividends'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherRepayableWithholdingTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxes', fieldName:'Andere terugbetaalbare roerende voorheffing', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxes', fieldName:'Andere terugbetaalbare roerende voorheffing', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxes', fieldName:'Andere terugbetaalbare roerende voorheffing', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty', fieldName:'Bedrijfsvoorheffing op meerwaarden op onroerende goederen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty', fieldName:'Bedrijfsvoorheffing op meerwaarden op onroerende goederen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty', fieldName:'Bedrijfsvoorheffing op meerwaarden op onroerende goederen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxEarnedIncomeIncomePartner');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeIncomePartner', fieldName:'Bedrijfsvoorheffing op inkomsten als vennoot', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeIncomePartner', fieldName:'Bedrijfsvoorheffing op inkomsten als vennoot', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeIncomePartner', fieldName:'Bedrijfsvoorheffing op inkomsten als vennoot', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartner'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 8) {
      msgs.push({prefix:'tax-inc',name:'AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod', fieldName:'Jaargemiddelde van het personeelsbestand', msg:'Fieldvalue is too long, maximum length is 8'});
       if(!fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod', fieldName:'Jaargemiddelde van het personeelsbestand', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 999999.99) {
      msgs.push({prefix:'tax-inc',name:'AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod', fieldName:'Jaargemiddelde van het personeelsbestand', msg:'Fieldvalue is too great, maximum allowed value is 999999.99'});
       if(!fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod', fieldName:'Jaaromzet, exclusief btw', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod', fieldName:'Jaaromzet, exclusief btw', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod', fieldName:'Jaaromzet, exclusief btw', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BalanceSheetTotalCorporationCodeCurrentTaxPeriod', fieldName:'Balanstotaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BalanceSheetTotalCorporationCodeCurrentTaxPeriod', fieldName:'Balanstotaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BalanceSheetTotalCorporationCodeCurrentTaxPeriod', fieldName:'Balanstotaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AnnuityLinearDepreciation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AnnuityLinearDepreciation', fieldName:'Lineair afschrijvingspercentage', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AnnuityLinearDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityLinearDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityLinearDepreciation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'AnnuityLinearDepreciation', fieldName:'Lineair afschrijvingspercentage', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_AnnuityLinearDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityLinearDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityLinearDepreciation'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'AnnuityLinearDepreciation', fieldName:'Lineair afschrijvingspercentage', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_AnnuityLinearDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityLinearDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityLinearDepreciation'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_AnnuityDegressiveDepreciation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AnnuityDegressiveDepreciation', fieldName:'Degressief afschrijvingspercentage', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'AnnuityDegressiveDepreciation', fieldName:'Degressief afschrijvingspercentage', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'AnnuityDegressiveDepreciation', fieldName:'Degressief afschrijvingspercentage', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'])  {
            fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] = '';
        }
        fieldMsgs['tax-inc_AnnuityDegressiveDepreciation'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_AcquisitionInvestmentValue');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionInvestmentValue', fieldName:'Aanschaffings- of beleggingswaarde', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AcquisitionInvestmentValue'])  {
            fieldMsgs['tax-inc_AcquisitionInvestmentValue'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionInvestmentValue'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionInvestmentValue', fieldName:'Aanschaffings- of beleggingswaarde', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AcquisitionInvestmentValue'])  {
            fieldMsgs['tax-inc_AcquisitionInvestmentValue'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionInvestmentValue'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AcquisitionInvestmentValue', fieldName:'Aanschaffings- of beleggingswaarde', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AcquisitionInvestmentValue'])  {
            fieldMsgs['tax-inc_AcquisitionInvestmentValue'] = '';
        }
        fieldMsgs['tax-inc_AcquisitionInvestmentValue'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_Revaluations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'Revaluations', fieldName:'Herwaarderingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_Revaluations'])  {
            fieldMsgs['tax-inc_Revaluations'] = '';
        }
        fieldMsgs['tax-inc_Revaluations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'Revaluations', fieldName:'Herwaarderingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_Revaluations'])  {
            fieldMsgs['tax-inc_Revaluations'] = '';
        }
        fieldMsgs['tax-inc_Revaluations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Revaluations', fieldName:'Herwaarderingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_Revaluations'])  {
            fieldMsgs['tax-inc_Revaluations'] = '';
        }
        fieldMsgs['tax-inc_Revaluations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DepreciationsNoRevaluations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsNoRevaluations', fieldName:'Afschrijvingen, zonder herwaarderingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DepreciationsNoRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsNoRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsNoRevaluations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsNoRevaluations', fieldName:'Afschrijvingen, zonder herwaarderingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DepreciationsNoRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsNoRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsNoRevaluations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsNoRevaluations', fieldName:'Afschrijvingen, zonder herwaarderingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DepreciationsNoRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsNoRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsNoRevaluations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DepreciationsRevaluations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsRevaluations', fieldName:'Afschrijvingen op herwaarderingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DepreciationsRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsRevaluations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsRevaluations', fieldName:'Afschrijvingen op herwaarderingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DepreciationsRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsRevaluations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DepreciationsRevaluations', fieldName:'Afschrijvingen op herwaarderingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DepreciationsRevaluations'])  {
            fieldMsgs['tax-inc_DepreciationsRevaluations'] = '';
        }
        fieldMsgs['tax-inc_DepreciationsRevaluations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExaggeratedDepreciations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciations', fieldName:'Overdreven afschrijvingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciations'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciations'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciations', fieldName:'Overdreven afschrijvingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciations'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciations'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedDepreciations', fieldName:'Overdreven afschrijvingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExaggeratedDepreciations'])  {
            fieldMsgs['tax-inc_ExaggeratedDepreciations'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedDepreciations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTaxNonDeductibleTaxes', fieldName:'Fictieve roerende voorheffing', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTaxNonDeductibleTaxes', fieldName:'Fictieve roerende voorheffing', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableFictiousWitholdingTaxNonDeductibleTaxes', fieldName:'Fictieve roerende voorheffing', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxesNonDeductibleTaxes', fieldName:'Forfaitair gedeelte van buitenlandse belasting', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxesNonDeductibleTaxes', fieldName:'Forfaitair gedeelte van buitenlandse belasting', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonRepayableLumpSumForeignTaxesNonDeductibleTaxes', fieldName:'Forfaitair gedeelte van buitenlandse belasting', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes', fieldName:'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes', fieldName:'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes', fieldName:'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes', fieldName:'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes', fieldName:'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes', fieldName:'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes', fieldName:'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes', fieldName:'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes', fieldName:'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes', fieldName:'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes', fieldName:'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes', fieldName:'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes', fieldName:'Roerende voorheffing op andere dividenden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes', fieldName:'Roerende voorheffing op andere dividenden', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes', fieldName:'Roerende voorheffing op andere dividenden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxesNonDeductibleTaxes', fieldName:'Andere terugbetaalbare roerende voorheffing', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxesNonDeductibleTaxes', fieldName:'Andere terugbetaalbare roerende voorheffing', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherRepayableWithholdingTaxesNonDeductibleTaxes', fieldName:'Andere terugbetaalbare roerende voorheffing', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes', fieldName:'Bedrijfsvoorheffing op meerwaarden op onroerende goederen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes', fieldName:'Bedrijfsvoorheffing op meerwaarden op onroerende goederen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes', fieldName:'Bedrijfsvoorheffing op meerwaarden op onroerende goederen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeCapitalGainsImmovablePropertyNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes', fieldName:'Bedrijfsvoorheffing op inkomsten als vennoot', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes', fieldName:'Bedrijfsvoorheffing op inkomsten als vennoot', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes', fieldName:'Bedrijfsvoorheffing op inkomsten als vennoot', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_WithholdingTaxEarnedIncomeIncomePartnerNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, eerste kwartaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, eerste kwartaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFirstQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, eerste kwartaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, tweede kwartaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, tweede kwartaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentSecondQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, tweede kwartaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, derde kwartaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, derde kwartaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentThirdQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, derde kwartaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, vierde kwartaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, vierde kwartaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'PrepaymentFourthQuarterNonDeductibleTaxes', fieldName:'Voorafbetaling, vierde kwartaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests', fieldName:'Belasting niet-inwoners vennootschappen inclusief vermeerdering, verhoging en nalatigheidsinteresten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'])  {
            fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] = '';
        }
        fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests', fieldName:'Belasting niet-inwoners vennootschappen inclusief vermeerdering, verhoging en nalatigheidsinteresten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'])  {
            fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] = '';
        }
        fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests', fieldName:'Belasting niet-inwoners vennootschappen inclusief vermeerdering, verhoging en nalatigheidsinteresten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'])  {
            fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] = '';
        }
        fieldMsgs['tax-inc_NonResidentCorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EstimatedCorporateIncomeTaxDebt');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EstimatedCorporateIncomeTaxDebt', fieldName:'Geraamd bedrag der belastingschulden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'])  {
            fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] = '';
        }
        fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EstimatedCorporateIncomeTaxDebt', fieldName:'Geraamd bedrag der belastingschulden', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'])  {
            fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] = '';
        }
        fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EstimatedCorporateIncomeTaxDebt', fieldName:'Geraamd bedrag der belastingschulden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'])  {
            fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] = '';
        }
        fieldMsgs['tax-inc_EstimatedCorporateIncomeTaxDebt'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonResidentWithholdingTaxIncomePaidIncomeAttributed', fieldName:'Door de inrichting gedragen roerende voorheffing op uitgekeerde of toegekende inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'])  {
            fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'] = '';
        }
        fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonResidentWithholdingTaxIncomePaidIncomeAttributed', fieldName:'Door de inrichting gedragen roerende voorheffing op uitgekeerde of toegekende inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'])  {
            fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'] = '';
        }
        fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonResidentWithholdingTaxIncomePaidIncomeAttributed', fieldName:'Door de inrichting gedragen roerende voorheffing op uitgekeerde of toegekende inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'])  {
            fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'] = '';
        }
        fieldMsgs['tax-inc_NonResidentWithholdingTaxIncomePaidIncomeAttributed'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherNonDeductibleTaxes', fieldName:'Andere niet-aftrekbare belastingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherNonDeductibleTaxes', fieldName:'Andere niet-aftrekbare belastingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherNonDeductibleTaxes', fieldName:'Andere niet-aftrekbare belastingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_OtherNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxesNoReimbursementsRegularizations', fieldName:'Niet-aftrekbare belastingen voor aftrek van terugbetalingen en regulariseringen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxesNoReimbursementsRegularizations', fieldName:'Niet-aftrekbare belastingen voor aftrek van terugbetalingen en regulariseringen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxesNoReimbursementsRegularizations', fieldName:'Niet-aftrekbare belastingen voor aftrek van terugbetalingen en regulariseringen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ReimbursementsRegularizationsNonDeductibleTaxes', fieldName:'Terugbetalingen en regulariseringen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ReimbursementsRegularizationsNonDeductibleTaxes', fieldName:'Terugbetalingen en regulariseringen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ReimbursementsRegularizationsNonDeductibleTaxes', fieldName:'Terugbetalingen en regulariseringen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleTaxes');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Niet-aftrekbare belastingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Niet-aftrekbare belastingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleTaxes', fieldName:'Niet-aftrekbare belastingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleTaxes'])  {
            fieldMsgs['tax-inc_NonDeductibleTaxes'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleTaxes'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleRegionalTaxesDutiesRetributions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Gewestelijke belastingen, heffingen en retributies', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Gewestelijke belastingen, heffingen en retributies', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRegionalTaxesDutiesRetributions', fieldName:'Gewestelijke belastingen, heffingen en retributies', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'])  {
            fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Geldboeten, verbeurdverklaringen en straffen van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Geldboeten, verbeurdverklaringen en straffen van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleFinesConfiscationsPenaltiesAllKind', fieldName:'Geldboeten, verbeurdverklaringen en straffen van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums', fieldName:'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CarExpense');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CarExpense', fieldName:'Kost', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CarExpense'])  {
            fieldMsgs['tax-inc_CarExpense'] = '';
        }
        fieldMsgs['tax-inc_CarExpense'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CarExpense', fieldName:'Kost', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CarExpense'])  {
            fieldMsgs['tax-inc_CarExpense'] = '';
        }
        fieldMsgs['tax-inc_CarExpense'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CarExpense', fieldName:'Kost', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CarExpense'])  {
            fieldMsgs['tax-inc_CarExpense'] = '';
        }
        fieldMsgs['tax-inc_CarExpense'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_LossValueCar');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'LossValueCar', fieldName:'Minderwaarde', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_LossValueCar'])  {
            fieldMsgs['tax-inc_LossValueCar'] = '';
        }
        fieldMsgs['tax-inc_LossValueCar'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'LossValueCar', fieldName:'Minderwaarde', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_LossValueCar'])  {
            fieldMsgs['tax-inc_LossValueCar'] = '';
        }
        fieldMsgs['tax-inc_LossValueCar'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'LossValueCar', fieldName:'Minderwaarde', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_LossValueCar'])  {
            fieldMsgs['tax-inc_LossValueCar'] = '';
        }
        fieldMsgs['tax-inc_LossValueCar'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesLossValuesCars');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesLossValuesCars', fieldName:'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesLossValuesCars'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensationCarRepairCosts');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensationCarRepairCosts', fieldName:'Vergoeding als schadeloosstelling voor herstellingskosten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensationCarRepairCosts'])  {
            fieldMsgs['tax-inc_CompensationCarRepairCosts'] = '';
        }
        fieldMsgs['tax-inc_CompensationCarRepairCosts'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CompensationCarRepairCosts', fieldName:'Vergoeding als schadeloosstelling voor herstellingskosten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensationCarRepairCosts'])  {
            fieldMsgs['tax-inc_CompensationCarRepairCosts'] = '';
        }
        fieldMsgs['tax-inc_CompensationCarRepairCosts'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensationCarRepairCosts', fieldName:'Vergoeding als schadeloosstelling voor herstellingskosten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensationCarRepairCosts'])  {
            fieldMsgs['tax-inc_CompensationCarRepairCosts'] = '';
        }
        fieldMsgs['tax-inc_CompensationCarRepairCosts'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleCarExpensesPartBenefitsAllKind', fieldName:'Autokosten ten belope van een gedeelte van het voordeel van alle aard', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'])  {
            fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ReceptionBusinessGiftsExpense');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ReceptionBusinessGiftsExpense', fieldName:'Kost', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'])  {
            fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] = '';
        }
        fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ReceptionBusinessGiftsExpense', fieldName:'Kost', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'])  {
            fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] = '';
        }
        fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ReceptionBusinessGiftsExpense', fieldName:'Kost', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'])  {
            fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] = '';
        }
        fieldMsgs['tax-inc_ReceptionBusinessGiftsExpense'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleReceptionBusinessGiftsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleReceptionBusinessGiftsExpenses', fieldName:'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RestaurantExpense');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RestaurantExpense', fieldName:'Kost', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RestaurantExpense'])  {
            fieldMsgs['tax-inc_RestaurantExpense'] = '';
        }
        fieldMsgs['tax-inc_RestaurantExpense'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RestaurantExpense', fieldName:'Kost', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RestaurantExpense'])  {
            fieldMsgs['tax-inc_RestaurantExpense'] = '';
        }
        fieldMsgs['tax-inc_RestaurantExpense'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RestaurantExpense', fieldName:'Kost', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RestaurantExpense'])  {
            fieldMsgs['tax-inc_RestaurantExpense'] = '';
        }
        fieldMsgs['tax-inc_RestaurantExpense'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 1) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too great, maximum allowed value is 1'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
      }
    
    if(value!=null && value.toString().length > 5) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbaar percentage', msg:'Fieldvalue is too long, maximum length is 5'});
       if(!fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
  
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleRestaurantExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbare restaurantkosten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbare restaurantkosten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleRestaurantExpenses', fieldName:'Niet-aftrekbare restaurantkosten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleRestaurantExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Kosten voor niet-specifieke beroepskledij', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Kosten voor niet-specifieke beroepskledij', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleNonSpecificProfessionalClothsExpenses', fieldName:'Kosten voor niet-specifieke beroepskledij', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'])  {
            fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExaggeratedInterests');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Overdreven interesten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Overdreven interesten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExaggeratedInterests', fieldName:'Overdreven interesten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExaggeratedInterests'])  {
            fieldMsgs['tax-inc_ExaggeratedInterests'] = '';
        }
        fieldMsgs['tax-inc_ExaggeratedInterests'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleParticularPortionInterestsLoans');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Interesten met betrekking tot een gedeelte van bepaalde leningen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Interesten met betrekking tot een gedeelte van bepaalde leningen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleParticularPortionInterestsLoans', fieldName:'Interesten met betrekking tot een gedeelte van bepaalde leningen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'])  {
            fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleParticularPortionInterestsLoans'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AbnormalBenevolentAdvantages');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Abnormale of goedgunstige voordelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Abnormale of goedgunstige voordelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AbnormalBenevolentAdvantages', fieldName:'Abnormale of goedgunstige voordelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'])  {
            fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] = '';
        }
        fieldMsgs['tax-inc_AbnormalBenevolentAdvantages'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ProfitTransferBelgiumAbroad');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ProfitTransferBelgiumAbroad', fieldName:'Naar het buitenland overgedragen winst', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'])  {
            fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] = '';
        }
        fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ProfitTransferBelgiumAbroad', fieldName:'Naar het buitenland overgedragen winst', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'])  {
            fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] = '';
        }
        fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ProfitTransferBelgiumAbroad', fieldName:'Naar het buitenland overgedragen winst', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'])  {
            fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] = '';
        }
        fieldMsgs['tax-inc_ProfitTransferBelgiumAbroad'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleSocialAdvantages');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Sociale voordelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Sociale voordelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleSocialAdvantages', fieldName:'Sociale voordelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'])  {
            fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleSocialAdvantages'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers', fieldName:'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'])  {
            fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] = '';
        }
        fieldMsgs['tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_Liberalities');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Liberaliteiten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Liberaliteiten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Liberalities', fieldName:'Liberaliteiten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_Liberalities'])  {
            fieldMsgs['tax-inc_Liberalities'] = '';
        }
        fieldMsgs['tax-inc_Liberalities'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_WriteDownsLossValuesShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Waardeverminderingen en minderwaarden op aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Waardeverminderingen en minderwaarden op aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'WriteDownsLossValuesShares', fieldName:'Waardeverminderingen en minderwaarden op aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_WriteDownsLossValuesShares'])  {
            fieldMsgs['tax-inc_WriteDownsLossValuesShares'] = '';
        }
        fieldMsgs['tax-inc_WriteDownsLossValuesShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ReversalPreviousExemptions');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Terugnemingen van vroegere vrijstellingen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Terugnemingen van vroegere vrijstellingen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ReversalPreviousExemptions', fieldName:'Terugnemingen van vroegere vrijstellingen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ReversalPreviousExemptions'])  {
            fieldMsgs['tax-inc_ReversalPreviousExemptions'] = '';
        }
        fieldMsgs['tax-inc_ReversalPreviousExemptions'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_EmployeeParticipation');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'EmployeeParticipation', fieldName:'Werknemersparticipatie', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_EmployeeParticipation'])  {
            fieldMsgs['tax-inc_EmployeeParticipation'] = '';
        }
        fieldMsgs['tax-inc_EmployeeParticipation'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IndemnityMissingCoupon');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Vergoedingen ontbrekende coupon', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Vergoedingen ontbrekende coupon', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IndemnityMissingCoupon', fieldName:'Vergoedingen ontbrekende coupon', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IndemnityMissingCoupon'])  {
            fieldMsgs['tax-inc_IndemnityMissingCoupon'] = '';
        }
        fieldMsgs['tax-inc_IndemnityMissingCoupon'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Kosten tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Kosten tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExpensesTaxShelterAuthorisedAudiovisualWork', fieldName:'Kosten tax shelter erkende audiovisuele werken', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'])  {
            fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] = '';
        }
        fieldMsgs['tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RegionalPremiumCapitalSubsidiesInterestSubsidies', fieldName:'Gewestelijke premies en kapitaal- en interestsubsidies', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'])  {
            fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] = '';
        }
        fieldMsgs['tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NonDeductiblePaymentsCertainStates');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Niet-aftrekbare betalingen naar bepaalde Staten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Niet-aftrekbare betalingen naar bepaalde Staten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NonDeductiblePaymentsCertainStates', fieldName:'Niet-aftrekbare betalingen naar bepaalde Staten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'])  {
            fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] = '';
        }
        fieldMsgs['tax-inc_NonDeductiblePaymentsCertainStates'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OtherDisallowedExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Andere verworpen uitgaven', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Andere verworpen uitgaven', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OtherDisallowedExpenses', fieldName:'Andere verworpen uitgaven', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OtherDisallowedExpenses'])  {
            fieldMsgs['tax-inc_OtherDisallowedExpenses'] = '';
        }
        fieldMsgs['tax-inc_OtherDisallowedExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_GrossRentRentalBenefitsBelgium');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'GrossRentRentalBenefitsBelgium', fieldName:'Brutobedrag van de huurprijs en de huurvoordelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'GrossRentRentalBenefitsBelgium', fieldName:'Brutobedrag van de huurprijs en de huurvoordelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'GrossRentRentalBenefitsBelgium', fieldName:'Brutobedrag van de huurprijs en de huurvoordelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_GrossRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductibleCostsRentRentalBenefitsBelgium');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductibleCostsRentRentalBenefitsBelgium', fieldName:'Aftrekbare kosten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductibleCostsRentRentalBenefitsBelgium', fieldName:'Aftrekbare kosten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductibleCostsRentRentalBenefitsBelgium', fieldName:'Aftrekbare kosten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_DeductibleCostsRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetRentRentalBenefitsBelgium');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetRentRentalBenefitsBelgium', fieldName:'Netto-inkomsten uit verhuurde onroerende goederen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'NetRentRentalBenefitsBelgium', fieldName:'Netto-inkomsten uit verhuurde onroerende goederen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetRentRentalBenefitsBelgium', fieldName:'Netto-inkomsten uit verhuurde onroerende goederen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'])  {
            fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'] = '';
        }
        fieldMsgs['tax-inc_NetRentRentalBenefitsBelgium'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IndexedCadastralIncomeBelgium');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IndexedCadastralIncomeBelgium', fieldName:'Ge�ndexeerd kadastraal inkomen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'])  {
            fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'] = '';
        }
        fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IndexedCadastralIncomeBelgium', fieldName:'Ge�ndexeerd kadastraal inkomen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'])  {
            fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'] = '';
        }
        fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IndexedCadastralIncomeBelgium', fieldName:'Ge�ndexeerd kadastraal inkomen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'])  {
            fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'] = '';
        }
        fieldMsgs['tax-inc_IndexedCadastralIncomeBelgium'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium', fieldName:'Bedragen verkregen bij vestiging of overdracht van een recht van erfpacht of van opstal of van gelijkaardige onroerende rechten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'])  {
            fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'] = '';
        }
        fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium', fieldName:'Bedragen verkregen bij vestiging of overdracht van een recht van erfpacht of van opstal of van gelijkaardige onroerende rechten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'])  {
            fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'] = '';
        }
        fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium', fieldName:'Bedragen verkregen bij vestiging of overdracht van een recht van erfpacht of van opstal of van gelijkaardige onroerende rechten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'])  {
            fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'] = '';
        }
        fieldMsgs['tax-inc_IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableImmovablePropertyIncomeBelgium');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableImmovablePropertyIncomeBelgium', fieldName:'Belastbare Belgische inkomsten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'])  {
            fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'] = '';
        }
        fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableImmovablePropertyIncomeBelgium', fieldName:'Belastbare Belgische inkomsten', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'])  {
            fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'] = '';
        }
        fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableImmovablePropertyIncomeBelgium', fieldName:'Belastbare Belgische inkomsten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'])  {
            fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'] = '';
        }
        fieldMsgs['tax-inc_TaxableImmovablePropertyIncomeBelgium'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableFinancialAdvantagesBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableFinancialAdvantagesBenefitsAllKind', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableFinancialAdvantagesBenefitsAllKind', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableFinancialAdvantagesBenefitsAllKind', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_TaxableFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxablePensionsCapitalsEmployerContributionsEmployerPremiums', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxablePensionsCapitalsEmployerContributionsEmployerPremiums', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxablePensionsCapitalsEmployerContributionsEmployerPremiums', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'])  {
            fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'] = '';
        }
        fieldMsgs['tax-inc_TaxablePensionsCapitalsEmployerContributionsEmployerPremiums'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind', fieldName:'Belastbaar bedrag', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'])  {
            fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'] = '';
        }
        fieldMsgs['tax-inc_TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods', fieldName:'Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'])  {
            fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] = '';
        }
        fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods', fieldName:'Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'])  {
            fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] = '';
        }
        fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods', fieldName:'Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'])  {
            fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] = '';
        }
        fieldMsgs['tax-inc_CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300', fieldName:'Meerwaarden belastbaar tegen 33%', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300', fieldName:'Meerwaarden belastbaar tegen 33%', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300', fieldName:'Meerwaarden belastbaar tegen 33%', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate3300'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650', fieldName:'Meerwaarden belastbaar tegen 16,5%', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650', fieldName:'Meerwaarden belastbaar tegen 16,5%', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650', fieldName:'Meerwaarden belastbaar tegen 16,5%', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovablePropertyRate1650'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belasting die door de ontvanger van de registratie is gevestigd', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belasting die door de ontvanger van de registratie is gevestigd', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belasting die door de ontvanger van de registratie is gevestigd', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods', fieldName:'Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'])  {
            fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] = '';
        }
        fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods', fieldName:'Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'])  {
            fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] = '';
        }
        fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods', fieldName:'Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'])  {
            fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] = '';
        }
        fieldMsgs['tax-inc_CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belastbare meerwaarden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belastbare meerwaarden', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belastbare meerwaarden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belasting die door de ontvanger van de registratie is gevestigd', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belasting die door de ontvanger van de registratie is gevestigd', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty', fieldName:'Belasting die door de ontvanger van de registratie is gevestigd', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'])  {
            fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'] = '';
        }
        fieldMsgs['tax-inc_RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DebtClaim');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DebtClaim', fieldName:'Schuldvordering', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DebtClaim'])  {
            fieldMsgs['tax-inc_DebtClaim'] = '';
        }
        fieldMsgs['tax-inc_DebtClaim'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DebtClaim', fieldName:'Schuldvordering', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DebtClaim'])  {
            fieldMsgs['tax-inc_DebtClaim'] = '';
        }
        fieldMsgs['tax-inc_DebtClaim'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DebtClaim', fieldName:'Schuldvordering', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DebtClaim'])  {
            fieldMsgs['tax-inc_DebtClaim'] = '';
        }
        fieldMsgs['tax-inc_DebtClaim'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptWriteDownDebtClaim');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Vrijgestelde waardevermindering', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Vrijgestelde waardevermindering', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptWriteDownDebtClaim', fieldName:'Vrijgestelde waardevermindering', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_ExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen die overeenstemt met verliezen die tijdens het boekjaar definitief geworden zijn', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen die overeenstemt met verliezen die tijdens het boekjaar definitief geworden zijn', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen die overeenstemt met verliezen die tijdens het boekjaar definitief geworden zijn', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge de gehele of gedeeltelijke inning van de vordering tijdens het boekjaar', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge de gehele of gedeeltelijke inning van de vordering tijdens het boekjaar', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge de gehele of gedeeltelijke inning van de vordering tijdens het boekjaar', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge een nieuwe schatting van het waarschijnlijke verlies tijdens het boekjaar', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge een nieuwe schatting van het waarschijnlijke verlies tijdens het boekjaar', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss', fieldName:'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge een nieuwe schatting van het waarschijnlijke verlies tijdens het boekjaar', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'])  {
            fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IncreaseExemptWriteDownDebtClaim');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptWriteDownDebtClaim', fieldName:'Verhoging vrijgestelde waardevermindering tijdens het boekjaar', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptWriteDownDebtClaim', fieldName:'Verhoging vrijgestelde waardevermindering tijdens het boekjaar', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptWriteDownDebtClaim', fieldName:'Verhoging vrijgestelde waardevermindering tijdens het boekjaar', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'])  {
            fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptWriteDownDebtClaim'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ProbableCost');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ProbableCost', fieldName:'Waarschijnlijke kost', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ProbableCost'])  {
            fieldMsgs['tax-inc_ProbableCost'] = '';
        }
        fieldMsgs['tax-inc_ProbableCost'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ProbableCost', fieldName:'Waarschijnlijke kost', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ProbableCost'])  {
            fieldMsgs['tax-inc_ProbableCost'] = '';
        }
        fieldMsgs['tax-inc_ProbableCost'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ProbableCost', fieldName:'Waarschijnlijke kost', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ProbableCost'])  {
            fieldMsgs['tax-inc_ProbableCost'] = '';
        }
        fieldMsgs['tax-inc_ProbableCost'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ExemptProvisionRisksExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Vrijgestelde voorziening', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Vrijgestelde voorziening', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptProvisionRisksExpenses', fieldName:'Vrijgestelde voorziening', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_ExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod', fieldName:'Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod', fieldName:'Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod', fieldName:'Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses', fieldName:'Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses', fieldName:'Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses', fieldName:'Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'])  {
            fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] = '';
        }
        fieldMsgs['tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_IncreaseExemptProvisionRisksExpenses');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptProvisionRisksExpenses', fieldName:'Verhoging van de vrijgestelde voorziening', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptProvisionRisksExpenses', fieldName:'Verhoging van de vrijgestelde voorziening', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'IncreaseExemptProvisionRisksExpenses', fieldName:'Verhoging van de vrijgestelde voorziening', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'])  {
            fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] = '';
        }
        fieldMsgs['tax-inc_IncreaseExemptProvisionRisksExpenses'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_Equity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'Equity', fieldName:'Eigen vermogen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_Equity'])  {
            fieldMsgs['tax-inc_Equity'] = '';
        }
        fieldMsgs['tax-inc_Equity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Equity', fieldName:'Eigen vermogen', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_Equity'])  {
            fieldMsgs['tax-inc_Equity'] = '';
        }
        fieldMsgs['tax-inc_Equity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'Equity', fieldName:'Eigen vermogen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_Equity'])  {
            fieldMsgs['tax-inc_Equity'] = '';
        }
        fieldMsgs['tax-inc_Equity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_OwnSharesFiscalValue');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'OwnSharesFiscalValue', fieldName:'Eigen aandelen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_OwnSharesFiscalValue'])  {
            fieldMsgs['tax-inc_OwnSharesFiscalValue'] = '';
        }
        fieldMsgs['tax-inc_OwnSharesFiscalValue'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'OwnSharesFiscalValue', fieldName:'Eigen aandelen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_OwnSharesFiscalValue'])  {
            fieldMsgs['tax-inc_OwnSharesFiscalValue'] = '';
        }
        fieldMsgs['tax-inc_OwnSharesFiscalValue'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'OwnSharesFiscalValue', fieldName:'Eigen aandelen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_OwnSharesFiscalValue'])  {
            fieldMsgs['tax-inc_OwnSharesFiscalValue'] = '';
        }
        fieldMsgs['tax-inc_OwnSharesFiscalValue'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_FinancialFixedAssetsParticipationsOtherShares');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'FinancialFixedAssetsParticipationsOtherShares', fieldName:'Financi�le vaste activa die uit deelnemingen en andere aandelen bestaan', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'])  {
            fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] = '';
        }
        fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'FinancialFixedAssetsParticipationsOtherShares', fieldName:'Financi�le vaste activa die uit deelnemingen en andere aandelen bestaan', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'])  {
            fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] = '';
        }
        fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'FinancialFixedAssetsParticipationsOtherShares', fieldName:'Financi�le vaste activa die uit deelnemingen en andere aandelen bestaan', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'])  {
            fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] = '';
        }
        fieldMsgs['tax-inc_FinancialFixedAssetsParticipationsOtherShares'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_SharesInvestmentCorporations');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'SharesInvestmentCorporations', fieldName:'Aandelen van beleggingsvennootschappen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_SharesInvestmentCorporations'])  {
            fieldMsgs['tax-inc_SharesInvestmentCorporations'] = '';
        }
        fieldMsgs['tax-inc_SharesInvestmentCorporations'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'SharesInvestmentCorporations', fieldName:'Aandelen van beleggingsvennootschappen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_SharesInvestmentCorporations'])  {
            fieldMsgs['tax-inc_SharesInvestmentCorporations'] = '';
        }
        fieldMsgs['tax-inc_SharesInvestmentCorporations'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'SharesInvestmentCorporations', fieldName:'Aandelen van beleggingsvennootschappen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_SharesInvestmentCorporations'])  {
            fieldMsgs['tax-inc_SharesInvestmentCorporations'] = '';
        }
        fieldMsgs['tax-inc_SharesInvestmentCorporations'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BranchesCountryTaxTreaty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BranchesCountryTaxTreaty', fieldName:'Inrichtingen gelegen in een land met verdrag', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BranchesCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BranchesCountryTaxTreaty', fieldName:'Inrichtingen gelegen in een land met verdrag', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BranchesCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BranchesCountryTaxTreaty', fieldName:'Inrichtingen gelegen in een land met verdrag', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BranchesCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_BranchesCountryTaxTreaty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ImmovablePropertyCountryTaxTreaty');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyCountryTaxTreaty', fieldName:'Onroerende goederen gelegen in een land met verdrag', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyCountryTaxTreaty', fieldName:'Onroerende goederen gelegen in een land met verdrag', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyCountryTaxTreaty', fieldName:'Onroerende goederen gelegen in een land met verdrag', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'])  {
            fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyCountryTaxTreaty'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TangibleFixedAssetsUnreasonableRelatedCosts', fieldName:'Materi�le vaste activa in zover de erop betrekking hebbende kosten onredelijk zijn', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'])  {
            fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] = '';
        }
        fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TangibleFixedAssetsUnreasonableRelatedCosts', fieldName:'Materi�le vaste activa in zover de erop betrekking hebbende kosten onredelijk zijn', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'])  {
            fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] = '';
        }
        fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TangibleFixedAssetsUnreasonableRelatedCosts', fieldName:'Materi�le vaste activa in zover de erop betrekking hebbende kosten onredelijk zijn', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'])  {
            fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] = '';
        }
        fieldMsgs['tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_InvestmentsNoPeriodicalIncome');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'InvestmentsNoPeriodicalIncome', fieldName:'Bestanddelen die als belegging worden gehouden en geen belastbaar periodiek inkomen voortbrengen', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'])  {
            fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] = '';
        }
        fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'InvestmentsNoPeriodicalIncome', fieldName:'Bestanddelen die als belegging worden gehouden en geen belastbaar periodiek inkomen voortbrengen', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'])  {
            fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] = '';
        }
        fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'InvestmentsNoPeriodicalIncome', fieldName:'Bestanddelen die als belegging worden gehouden en geen belastbaar periodiek inkomen voortbrengen', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'])  {
            fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] = '';
        }
        fieldMsgs['tax-inc_InvestmentsNoPeriodicalIncome'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ImmovablePropertyUseManager');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyUseManager', fieldName:'Onroerende goederen waarvan bedrijfsleiders het gebruik hebben', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyUseManager'])  {
            fieldMsgs['tax-inc_ImmovablePropertyUseManager'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyUseManager'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyUseManager', fieldName:'Onroerende goederen waarvan bedrijfsleiders het gebruik hebben', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyUseManager'])  {
            fieldMsgs['tax-inc_ImmovablePropertyUseManager'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyUseManager'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ImmovablePropertyUseManager', fieldName:'Onroerende goederen waarvan bedrijfsleiders het gebruik hebben', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ImmovablePropertyUseManager'])  {
            fieldMsgs['tax-inc_ImmovablePropertyUseManager'] = '';
        }
        fieldMsgs['tax-inc_ImmovablePropertyUseManager'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_UnrealisedExpressedCapitalGains');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGains', fieldName:'Uitgedrukte maar niet-verwezenlijkte meerwaarden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGains', fieldName:'Uitgedrukte maar niet-verwezenlijkte meerwaarden', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'UnrealisedExpressedCapitalGains', fieldName:'Uitgedrukte maar niet-verwezenlijkte meerwaarden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'])  {
            fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] = '';
        }
        fieldMsgs['tax-inc_UnrealisedExpressedCapitalGains'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity', fieldName:'Belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity', fieldName:'Belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity', fieldName:'Belastingkrediet voor onderzoek en ontwikkeling', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_InvestmentGrants');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'InvestmentGrants', fieldName:'Kapitaalsubsidies', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_InvestmentGrants'])  {
            fieldMsgs['tax-inc_InvestmentGrants'] = '';
        }
        fieldMsgs['tax-inc_InvestmentGrants'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'InvestmentGrants', fieldName:'Kapitaalsubsidies', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_InvestmentGrants'])  {
            fieldMsgs['tax-inc_InvestmentGrants'] = '';
        }
        fieldMsgs['tax-inc_InvestmentGrants'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'InvestmentGrants', fieldName:'Kapitaalsubsidies', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_InvestmentGrants'])  {
            fieldMsgs['tax-inc_InvestmentGrants'] = '';
        }
        fieldMsgs['tax-inc_InvestmentGrants'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_ActualisationStockRecognisedDiamondTraders');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ActualisationStockRecognisedDiamondTraders', fieldName:'Voorraadactualisering erkende diamanthandelaars', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'])  {
            fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ActualisationStockRecognisedDiamondTraders', fieldName:'Voorraadactualisering erkende diamanthandelaars', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'])  {
            fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ActualisationStockRecognisedDiamondTraders', fieldName:'Voorraadactualisering erkende diamanthandelaars', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'])  {
            fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] = '';
        }
        fieldMsgs['tax-inc_ActualisationStockRecognisedDiamondTraders'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch', fieldName:'Ten name van de hoofdzetel ontleende middelen met betrekking tot dewelke de interesten ten laste van het belastbaar resultaat van de Belgische inrichting wordt gelegd', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'])  {
            fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] = '';
        }
        fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch', fieldName:'Ten name van de hoofdzetel ontleende middelen met betrekking tot dewelke de interesten ten laste van het belastbaar resultaat van de Belgische inrichting wordt gelegd', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'])  {
            fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] = '';
        }
        fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch', fieldName:'Ten name van de hoofdzetel ontleende middelen met betrekking tot dewelke de interesten ten laste van het belastbaar resultaat van de Belgische inrichting wordt gelegd', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'])  {
            fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] = '';
        }
        fieldMsgs['tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < -99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden', msg:'Fieldvalue is too small, minimum allowed value is -99999999999999.99'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'MovementEquityAfterDeductionsAllowanceCorporateEquity', fieldName:'Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'])  {
            fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] = '';
        }
        fieldMsgs['tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_AllowanceCorporateEquityCurrentTaxPeriod');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityCurrentTaxPeriod', fieldName:'Risicokapitaal van het belastbaar tijdperk', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityCurrentTaxPeriod', fieldName:'Risicokapitaal van het belastbaar tijdperk', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'AllowanceCorporateEquityCurrentTaxPeriod', fieldName:'Risicokapitaal van het belastbaar tijdperk', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'])  {
            fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] = '';
        }
        fieldMsgs['tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    fields = store.findElementsForName('tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'DeductibleAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'In principe aftrekbaar', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'DeductibleAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'In principe aftrekbaar', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'DeductibleAllowanceCorporateEquityCurrentAssessmentYear', fieldName:'In principe aftrekbaar', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'])  {
            fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] = '';
        }
        fieldMsgs['tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    
    
    
    fields = store.findElementsForName('tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012');
    
    for(i=0;i<fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'numeric');
        
    if(value!=null && value.toString().length > 16) {
      msgs.push({prefix:'tax-inc',name:'ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal', msg:'Fieldvalue is too long, maximum length is 16'});
       if(!fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
            fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
        }
        fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
    }
  
    if(value!=null && value < 0) {
      msgs.push({prefix:'tax-inc',name:'ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
       if(!fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
            fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
        }
        fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
    }
    
      if(value!=null && value > 99999999999999.99) {
      msgs.push({prefix:'tax-inc',name:'ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
       if(!fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
            fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
        }
        fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
      }
    
        
    }
    fields = store.findElementsForName('tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012');
        
        for(i=0;i<fields.length;i++) 
        {
            value = self.getElementValidationValue(fields[i],'numeric');
            
        if(value!=null && value.toString().length > 16) {
          msgs.push({prefix:'tax-inc',name:'ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal', msg:'Fieldvalue is too long, maximum length is 16'});
           if(!fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
            }
            fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
        }
      
        if(value!=null && value < 0) {
          msgs.push({prefix:'tax-inc',name:'ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal', msg:'Fieldvalue is too small, minimum allowed value is 0'});
           if(!fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
            }
            fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
        }
        
          if(value!=null && value > 99999999999999.99) {
          msgs.push({prefix:'tax-inc',name:'ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
           if(!fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
            }
            fieldMsgs['tax-inc_ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
          }
        
            
        }
        
        
        fields = store.findElementsForName('tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012');
            
            for(i=0;i<fields.length;i++) 
            {
                value = self.getElementValidationValue(fields[i],'numeric');
                
            if(value!=null && value.toString().length > 16) {
              msgs.push({prefix:'tax-inc',name:'DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken', msg:'Fieldvalue is too long, maximum length is 16'});
               if(!fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                    fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
                }
                fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
            }
          
            if(value!=null && value < 0) {
              msgs.push({prefix:'tax-inc',name:'DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken', msg:'Fieldvalue is too small, minimum allowed value is 0'});
               if(!fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                    fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
                }
                fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
            }
            
              if(value!=null && value > 99999999999999.99) {
              msgs.push({prefix:'tax-inc',name:'DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
               if(!fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                    fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
                }
                fieldMsgs['tax-inc_DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
              }
            
                
        }
        
        fields = store.findElementsForName('tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012');
            
            for(i=0;i<fields.length;i++) 
            {
                value = self.getElementValidationValue(fields[i],'numeric');
                
            if(value!=null && value.toString().length > 16) {
              msgs.push({prefix:'tax-inc',name:'CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Saldo van de overgedragen vrijstellingen voor risicokapitaal dat aftrekbaar is tijdens latere aanslagjaren', msg:'Fieldvalue is too long, maximum length is 16'});
               if(!fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                    fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
                }
                fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
            }
          
            if(value!=null && value < 0) {
              msgs.push({prefix:'tax-inc',name:'CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Saldo van de overgedragen vrijstellingen voor risicokapitaal dat aftrekbaar is tijdens latere aanslagjaren', msg:'Fieldvalue is too small, minimum allowed value is 0'});
               if(!fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                    fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
                }
                fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
            }
            
              if(value!=null && value > 99999999999999.99) {
              msgs.push({prefix:'tax-inc',name:'CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012', fieldName:'Saldo van de overgedragen vrijstellingen voor risicokapitaal dat aftrekbaar is tijdens latere aanslagjaren', msg:'Fieldvalue is too great, maximum allowed value is 99999999999999.99'});
               if(!fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'])  {
                    fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] = '';
                }
                fieldMsgs['tax-inc_CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012'] =msgs[msgs.length-1].msg;
              }
            
                
    }

    var res = { result: results, scopeId: e.data.scopeId,msgs:msgs,fieldMsgs:fieldMsgs };
    self.postMessage(res);
    }, false);
  