

eBook.List.FileIconColumn = Ext.extend(Ext.list.Column, {
    icons: {
        'xls': 'eBook-icon-xls'
        , 'xlsx': 'eBook-icon-xls'
        , 'doc': 'eBook-icon-doc'
        , 'docx': 'eBook-icon-doc'
        , 'pdf': 'eBook-icon-pdf'
    }
    , defaultIconCls: 'eBook-icon-general-16'
    , constructor: function(c) {
        c.tpl = new Ext.XTemplate('<div class="eBook-list-icon-column {' + c.dataIndex + ':this.format}">&nbsp;</div>');
        var icons = this.icons;
        var dics = this.defaultIconCls;
        c.tpl.format = function(v) {
            var ext = /[^.]+$/.exec(v);
            if (icons[ext]) return icons[ext];
            return dics;
        };
        eBook.List.FileIconColumn.superclass.constructor.call(this, c);
    }
});

Ext.reg('eBook.List.FileIconColumn', eBook.List.FileIconColumn);


eBook.List.QualityColumn = Ext.extend(Ext.list.Column, {
   constructor: function(c) {
   c.tpl = new Ext.XTemplate('<div class="eBook-list-icon-column <tpl if="Closed">eBook-lock-16</tpl><tpl if="!Closed && Errors &gt; 0">eBook-icon-16-error</tpl><tpl if="!Closed && Errors==0 && Warnings &gt; 0">eBook-icon-16-warning</tpl>">&nbsp;</div>');

        eBook.List.QualityColumn.superclass.constructor.call(this, c);
    }
});

Ext.reg('eBook.List.QualityColumn', eBook.List.QualityColumn);