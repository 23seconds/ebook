/*
Ext.extend(Ext.list.ListView, {
    getStore: function() {
        return this.store;
    }
});*/

eBook.List.ListView = Ext.extend(Ext.Panel, {
    initComponent:function() {
        Ext.apply(this, {
            items: [new Ext.list.ListView(this.listConfig)]
            , height: 150
            , border:false
            ,autoScroll:true
        });
        eBook.List.ListView.superclass.initComponent.apply(this,arguments);
    }
    , getStore: function() {
        return this.items.get(0).getStore();
    }
});

Ext.reg('eBookListView', eBook.List.ListView);