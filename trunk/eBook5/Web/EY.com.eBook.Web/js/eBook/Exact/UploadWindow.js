


eBook.Exact.UploadWindow = Ext.extend(eBook.Window, {

    initComponent: function () {
        mappingPane = this.mappingPane;
        Ext.apply(this, {
            title: eBook.Excel.UploadWindow_Title
            , layout: 'fit'
            , width: 620
            , height: 200
            , modal: true
            , iconCls: 'eBook-uploadbiz-ico-24'
            , items: [new Ext.FormPanel({
                fileUpload: true
                        , items: [{
                            xtype: 'fileuploadfield'
                                    , width: 400
                                    , fieldLabel: eBook.Exact.UploadWindow_SelectFile
                                    , fileTypeRegEx: /(\.txt())$/i
                                    , ref: 'file'
                        }, {
                            xtype: 'hidden'
                                        , name: 'fileType'
                                        , value: 'txt'
                        }]
                        , ref: 'txtForm'
            })]
            , tbar: [{
                text: eBook.Excel.UploadWindow_Upload,
                ref: '../saveButton',
                iconCls: 'eBook-uploadbiz-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
            }]
        });

        eBook.Exact.UploadWindow.superclass.initComponent.apply(this, arguments);
    }
    , onSaveClick: function (e) {
        var f = this.txtForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Exact.UploadWindow_Invalid);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Excel.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function (fp, o) {
        //process o.result.file
        this.uploaded = true;
        this.newFileName = o.result.file;
        this.originalFileName = o.result.originalFile;
        this.getEl().mask('Loading descriptions');
        Ext.Ajax.request({
            url: eBook.Service.file + 'ImportAccountsTxt'
                , method: 'POST'
                , params: Ext.encode({ cesmdc: { FileName: this.newFileName, FileId: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.currentFile.get('Culture')} })
                , callback: this.handleFileProcessingTxt
                , scope: this
        });

    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, this.title);
    }
    , handleFileProcessingTxt: function (opts, success, resp) {
        this.getEl().unmask();
        this.getEl().mask('Loading numbers');
        if (success) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'ImportCurrentStateTxt'
                , method: 'POST'
                , params: Ext.encode({ cesmdc: { FileName: this.newFileName, FileId: eBook.Interface.currentFile.get('Id')} })
                , callback: this.handleFileProcessingAccountTxt
                , scope: this
            });

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , handleFileProcessingAccountTxt: function (opts, success, resp) {
        this.getEl().unmask();
        this.getEl().mask('Set file in cache');
        if (success) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'SetFileInCache'
                , method: 'POST'
                , params: { cfdc: { FileId: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.Culture} }
                , callback: this.SetFileInCacheDone
                , scope: this
            });

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , SetFileInCacheDone: function (opts, success, resp) {
        this.mappingPane.doRefresh();
        this.getEl().unmask();
        this.getEl().mask('Recalculate worksheets');
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + 'ForceReCalculate'
            , method: 'POST'
            , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
            , callback: this.worksheetsUpdated
            , started: new Date().getTime()
            , scope: this
        });
        this.close();
    }
    , worksheetsUpdated: function (opts, success, resp) {
        this.getEl().unmask();
        
    }
    , show: function (caller) {
        this.parentCaller = caller;
        eBook.Exact.UploadWindow.superclass.show.call(this);
    }
});