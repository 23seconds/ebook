
eBook.Person = Ext.extend(Ext.util.Observable, {
    constructor: function (config) {
        this.windowsAccount = config.winAccount;
        //this.name = config.name;
        this.addEvents({
            "loggingin": true,
            "loggedin": true
        });
        eBook.Person.superclass.constructor.call(this, config);
    },
    windowsAccount: null,
    personId: 0,
    firstName: '',
    lastName: '',
    fullName: '',
    companyRole: '',
    department: '',
    email: '',
    lastLogon: null,
    roles: [],
    isChampion: false,
    isSysAdmin: false,
    offices: [],
    activeRole: '',
    activeClientRoles: '',
    gpn: '',
    login: function () {
        eBook.Splash.setText("Logging in...");
        if (this.windowsAccount != '' && this.windowsAccount != null) {

            this.fireEvent('loggingin', this);
            Ext.Ajax.request({
                url: eBook.Service.home + 'Login',
                method: 'POST',
                params: Ext.encode({ cwadc: { WindowsAccount: this.windowsAccount} }),
                success: this.onLoginSuccess,
                failure: this.onLoginFailure,
                scope: this
            });
        } else {
            return;
        }
    },
    getActivePersonDataContract: function () {
        var p = {
            id: this.personId,
            ar: this.activeRole,
            acr: this.activeClientRoles,
            nm: this.fullName
        };
        var ps = Ext.encode(p);
        return Ext.util.Base64.encode(ps);
    },
    onLoginSuccess: function (resp, opts) {
        var rObj = Ext.decode(resp.responseText);
        var personObj = rObj.LoginResult;
        Ext.apply(this, {
            personId: personObj.id,
            firstName: personObj.fn,
            lastName: personObj.ln,
            fullName: personObj.fn + ' ' + personObj.ln,
            companyRole: personObj.em,
            email: personObj.em,
            lastLogon: Ext.data.Types.WCFDATE.convert(personObj.ll),
            roles: personObj.rs,
            isChampion: personObj.ic,
            isSysAdmin: personObj.isa,
            offices: personObj.os,
            defaultOffice: personObj.dof,
            defaultOfficeObj: personObj.dof,
            department: personObj.dp,
            gpn: personObj.gpn
        });

        if (this.roles.length > 0) {
            for (var i = 0; i < this.roles.length; i++) {
                if ((this.roles[i].r).indexOf('eBookAT') == -1) {
                    this.setActiveRole(this.roles[i].r, true);
                }
                if(this.roles[i].lu == true)
                {
                    this.setActiveRole(this.roles[i].r, true);
                    break;
                }
            }
        }
        var chpstr = personObj.ec;
        var chps = personObj.ecs.split('/');
        if (chps.length > 2) {
            this.chpstr = chps[0] + '/' + chps[1] + '/..';
        }
        if (eBook.Interface.rendered) {
            Ext.get('et-local-champions').update(chpstr);

            Ext.get('eBook.User').update(this.fullName);
            var tmp = 'No office';
            if (this.defaultOffice != null) {tmp = this.defaultOffice.n;}
            Ext.get('eBook.Office').update(tmp);
            // var d = eBook.Language.Roles[this.activeRole] ? eBook.Language.Roles[this.activeRole] : this.activeRole;
            //Ext.get('eBook.Role').update(d);

            eBook.Interface.updateRoles(this.getComboRoles());
            this.fireEvent('loggedin', this);
        } else {
            eBook.Interface.render();
        }
    },
    getOfficeList: function () {
        var lst = [{ id: null, n: 'All'}].concat(this.offices);
        return lst;

    },
    onLoginFailure: function (resp, opts) {
        // message
        eBook.Interface.showResponseError(resp, this.title);

    },
    getComboRoles: function () {
        var rs = [];
        for (var i = 0; i < this.roles.length; i++) {
            if ((this.roles[i].r).indexOf('eBookAT') == -1) {
                var d = eBook.Language.Roles[this.roles[i].r] ? eBook.Language.Roles[this.roles[i].r] : this.roles[i].r;

                //rs[i] = [this.roles[i].r, d];
                rs.push([this.roles[i].r, d]);
            }
        }
        return rs;
    },
    setActiveRole: function (role, noreload) {
        if (!this.isExistingRole(role)) {return;}
        var prevRole = this.activeRole;
        this.activeRole = role;
        var d = eBook.Language.Roles[this.activeRole] ? eBook.Language.Roles[this.activeRole] : this.activeRole;
        if (eBook.Interface.rendered) {
            eBook.Interface.applyActiveRole(role, prevRole);
        }
    },
    isCurrentlyChampion: function () {
        var rexp = /(champion)/i;
        return rexp.test(eBook.User.activeRole);
    },
    isCurrentlyAdmin: function () {
        var rexp = /(ADMIN|developer)/i;
        return rexp.test(eBook.User.activeRole);
    },
    hasRoleLike: function (qry, not) {
        if (!Ext.isDefined(not)) not = false;
        qry = qry.toLowerCase();
        for (var i = 0; i < this.roles.length; i++) {
            if (this.roles[i].r.toLowerCase().indexOf(qry) > -1) {
                if (!not) {return true;}
            } else {
                if (not) {return true;}
            }
        }
        return false;
    },
    isExistingRole: function (role) {
        for (var i = 0; i < this.roles.length; i++) {
            if (this.roles[i].r == role) {return true;}
        }
        return false;
    },
    isActiveClientRolesFullAccess: function () {
        var rexpAdm = /((admin)|(developer)|(champion))/i;
        if (rexpAdm.test(this.activeRole)) {return true;}
        var rexpc = /((partner)|(director)|(manager)|(admin))/i;
        return rexpc.test(this.activeClientRoles);
    },
    checkCloser: function (sheetDepartment, isSharedClient) {
        var hasBasicRights = eBook.User.isActiveClientRolesFullAccess();
        if (!hasBasicRights) {return false;}

        //        if (isSharedClient && !eBook.User.isCurrentlyAdmin() && sheetDepartment != null) {

        //            return sheetDepartment == eBook.User.department;
        //        }
        return true;

    }
});