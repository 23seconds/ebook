
eBook.Help = {
    WindowMgr: new Ext.WindowGroup()
    , showHelp: function(worksheetID) {

        var wn = eBook.Help.WindowMgr.get('et-help-window');
        if (wn) {
            eBook.Help.WindowMgr.bringToFront(wn);
        } else {

        wn = new eBook.Help.Window({ worksheetID: worksheetID });
            wn.worksheetID = worksheetID;
            wn.show();
        }


    }
    , showChampions: function() {
        var wn = eBook.Help.WindowMgr.get('et-help-champions-window');
        if (wn) {
            eBook.Help.WindowMgr.bringToFront(wn);
        } else {
            wn = new eBook.Help.ChampionsWindow({});
            wn.show();
        }
    }
};