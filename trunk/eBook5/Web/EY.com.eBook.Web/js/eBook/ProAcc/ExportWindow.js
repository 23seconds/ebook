
// fields to add:
// Selection of ProAcc period
// Journal nr
// Start bookingnr

// TODO: How to prevent multiple import in ProAcc?

eBook.ProAcc.ExportWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        this.proAccServer = eBook.Interface.currentClient.get('ProAccServer');
        this.proAccDatabase = eBook.Interface.currentClient.get('ProAccDatabase');
        Ext.apply(this, {
            iconCls: ''
            , title: eBook.ProAcc.ExportWindow_Title
            , modal: true
            , layout: 'fit'
            , width: 420
            , height: 300
            , items: [{
                xtype: 'form',
                bodyStyle: 'padding:10px',
                labelWidth: 200,
                ref: 'detailedForm',
                items: [
                            { xtype: 'combo'
                                , ref: 'period'
                                , fieldLabel: 'ProAcc Period'
                                , name: 'Period'
                                , triggerAction: 'all'
                                , typeAhead: false
                                , selectOnFocus: true
                                , autoSelect: true
                                , allowBlank: false
                                , forceSelection: true
                                , valueField: 'Period'
                                , displayField: 'Name'
                                , editable: false
                                , nullable: false
                                , mode: 'remote'
                                , listWidth:300
                                , store: new eBook.data.JsonStore({
                                    selectAction: 'ProAccGetPeriods'
                                    //, autoLoad: true
                                    , autoDestroy: true
                                    , criteriaParameter: 'cpbdc'
                                    , baseParams: {
                                        Server: this.proAccServer
                                        , Database: this.proAccDatabase
                                    }
                                    , fields: eBook.data.RecordTypes.Periods
                                })
                            },
                            { xtype: 'combo'
                                , ref: 'journal'
                                , fieldLabel: 'Journal'
                                , name: 'Journal'
                                , triggerAction: 'all'
                                , typeAhead: false
                                , selectOnFocus: true
                                , autoSelect: true
                                , allowBlank: false
                                , forceSelection: true
                                , valueField: 'id'
                                , displayField: 'name'
                                , editable: false
                                , nullable: false
                                , mode: 'remote'
                                , listWidth:300
                                , store: new eBook.data.JsonStore({
                                    selectAction: 'ProAccGetJournals'
                                    //, autoLoad: true
                                    , autoDestroy: true
                                    , criteriaParameter: 'cpbdc'
                                    , baseParams: {
                                        Server: this.proAccServer
                                        , Database: this.proAccDatabase
                                    }
                                    , fields: eBook.data.RecordTypes.ProAccJournals
                                })
                            },
                            {
                                xtype: 'numberfield',
                                name: 'BookingNr',
                                fieldLabel: 'Start booking nr at',
                                ref: 'bookingnr',
                                allowDecimals: false,
                                allowNegative: false,
                                decimalPrecision: 0,
                                minValue: 1,
                                value: 1
                            }

                        ]
                        , buttons: [{
                            ref: 'performExport',
                            text: 'Export bookings',
                            iconCls: 'eBook-icon-exportProacc-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onCheckProAccExport,
                            width: 70,
                            scope: this
                        }
                        ]
}]
            });
            eBook.ProAcc.ExportWindow.superclass.initComponent.apply(this, arguments);
        }
    , onPerformExport: function() {
        // perform export to excel
        this.getEl().mask(eBook.Accounts.Mappings.GridPanel_PreparingExcelAdjust, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'ExportProAccBookings'
            , method: 'POST'
            , params: Ext.encode({ cpebdc: {
                fileId: eBook.Interface.currentFile.get('Id'),
                period: this.detailedForm.period.getValue(),
                journal: this.detailedForm.journal.getValue(),
                bookingnr: this.detailedForm.bookingnr.getValue(),
                Person: eBook.User.getActivePersonDataContract()
            }
            })
            , callback: this.onProAccExportComplete
            , scope: this
        });
    }
    , onProAccExportComplete: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open("pickup/" + robj.ExportProAccBookingsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onCheckProAccExport: function() {
        this.getEl().mask('Loading', 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'CheckExportProAcc'
            , method: 'POST'
            , params: Ext.encode({ cicdc: {
                Id: eBook.Interface.currentFile.get('Id'),
                Culture: eBook.Interface.Culture
            }
            })
            , callback: this.onCheckProAccExportComplete
            , scope: this
        });
    }
    , onCheckProAccExportComplete: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var ms = eBook.ProAcc.ExportWindow_LastExportedMessage;
            var robj = Ext.decode(resp.responseText); 
            if (robj.CheckExportProAccResult != null) {
                robj = robj.CheckExportProAccResult;
                robj.d = Ext.data.Types.WCFDATE.convert(robj.d);
                var dte = robj.d.format('l j F Y H:i:s');
                var msg = String.format(ms, robj.p.fn, robj.p.ln, dte);
                Ext.Msg.confirm(eBook.ProAcc.ExportWindow_ExportMessageTitle, msg, this.onCheckProAccExportConfirm, this);
            } else {
                this.onPerformExport();
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onCheckProAccExportConfirm: function(buttonid) {
        if (buttonid == "yes") {
            this.onPerformExport();
        }
    }
    });