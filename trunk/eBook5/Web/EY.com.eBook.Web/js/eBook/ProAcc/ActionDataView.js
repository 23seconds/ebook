

eBook.ProAcc.ActionDataView = Ext.extend(Ext.DataView, {
    constructor: function(config) {
        this.addEvents('actionsDone');
        eBook.ProAcc.ActionDataView.superclass.constructor.call(this, config);
    }
    , data: {}
    , title: ''
    , initComponent: function() {
        Ext.apply(this, {
            border: false
            , style: 'padding:10px;background-color:#FFFFFF;'
            , store: new Ext.data.JsonStore({
                autoDestroy: true,
                root: 'actions',
                idProperty: 'id',
                fields: eBook.data.RecordTypes.CreateFileActions
            })
            , itemSelector: 'div.eBook-createFile-action'
            , tpl: new Ext.XTemplate(
                        '<div class="eBook-createFile-action-title">' + this.title + '</div><tpl for=".">',
                        '<div class="eBook-createFile-action eBook-createFile-action-status-{status}" id="{id}">{text}<tpl if="status==\'failure\'"><div class="eBook-createFile-action-msg">{errorMsg}</div></tpl></div>',
                        '</tpl>')
        });
        eBook.ProAcc.ActionDataView.superclass.initComponent.apply(this, arguments);
    }
    , setActions: function(actObj) {
        this.actionsContainer = actObj;
        if (this.actionsContainer.revertActions == null) this.actionsContainer.revertActions = [];
        if (this.actionsContainer.postActions == null) this.actionsContainer.postActions = [];
        for (var i = 0; i < this.actionsContainer.actions.length; i++) {
            this.actionsContainer.actions[i].id = Ext.id();
            this.actionsContainer.actions[i].status = 'ready';
            this.actionsContainer.actions[i].container = 'actions';
        }
        for (var i = 0; i < this.actionsContainer.revertActions.length; i++) {
            this.actionsContainer.revertActions[i].id = Ext.id();
            this.actionsContainer.revertActions[i].status = 'ready';
            this.actionsContainer.revertActions[i].container = 'revertActions';
        }
        for (var i = 0; i < this.actionsContainer.postActions.length; i++) {
            this.actionsContainer.postActions[i].id = Ext.id();
            this.actionsContainer.postActions[i].status = 'ready';
            this.actionsContainer.postActions[i].container = 'postActions';
        }

        this.store.loadData({ actions: this.actionsContainer.actions });
    }
    , start: function() {
        this.data = {};
        this.findParentByType(eBook.Window).locked = true;
        this.actionIndex = -1;
        this.performAction();
    }
    , getActionObject: function(rec) {
        var cnt = this.actionsContainer[rec.get('container')];

        for (var i = 0; i < cnt.length; i++) {
            if (cnt[i].id == rec.get('id')) return cnt[i];
        }
        return null;
    }
    , postAdded: false
    , success: true
    , performAction: function() {
        this.actionIndex++;
        if (this.store.getCount() > this.actionIndex) {
            var rec = this.store.getAt(this.actionIndex);
            var act = this.getActionObject(rec);
            Ext.get(act.id).scrollIntoView(this.findParentByType(eBook.Window).body, false);
            rec.set('status', 'busy');
            rec.commit();
            var params = {};

            if (Ext.isFunction(act.params)) {
                params = act.params.call(this, this);
            } else {
                params = act.params;
            }

            Ext.Ajax.request({
                url: (act.url ? act.url : eBook.Service.url) + act.action
                , method: 'POST'
                , params: Ext.encode(params)
                , success: function(resp, opts) { this.onActionSuccess(resp, opts, act); }
                , failure: function(resp, opts) { this.onActionFailure(resp, opts, act); }
                , scope: this
            });

        } else {
            //this.isBusy = false;
            //this.setStatus('Ready');
            if (!this.postAdded && this.success && this.actionsContainer.postActions.length > 0) {
                //this.store.on('load', this.performAction, this, { single: true });
                this.store.loadData({ actions: this.actionsContainer.postActions }, true);
                this.store.commitChanges();
                this.postAdded = true;
                this.actionIndex--;
                this.performAction.defer(100, this);
            } else {

                this.findParentByType(eBook.Window).locked = false;
                this.fireEvent('actionsDone', true, this.actionsContainer, this.data); //success, actions
            }
        }
    }
    , onActionSuccess: function(resp, opts, act) {

        var rec = this.store.getById(act.id);
        rec.set('status', 'success');
        rec.commit();
        this.getActionObject(rec).status = 'success';
        if (act.response != null && Ext.isFunction(act.response)) {
            var respObj = Ext.decode(resp.responseText);
            act.response.call(this, this, respObj);
        }
        if (act.callback != null && Ext.isFunction(act.callback)) {
            act.callback.call(act.scope || this, this, resp);
        }
        this.performAction.defer(100, this);
    }
    , onActionFailure: function(resp, opts, act) {
        var rec = this.store.getById(act.id);
        rec.set('status', 'failure');
        rec.set('errorMsg', 'Failed to execute action');
        rec.commit();

        this.getActionObject(rec).status = 'failure';
        if (act.revertByFailure) {
            var dels = [];
            this.store.each(function(rec) {
                if (rec.get('status') == 'ready') dels.push(rec);
            }, this);
            if (dels.length > 0) this.store.remove(dels);
            this.store.commitChanges();
            if (this.actionsContainer.revertActions.length > 0) {
                this.store.loadData({ actions: this.actionsContainer.revertActions }, true);
            }
            this.performAction.defer(100, this);
        } else {

            if (act.ignoreFailure) {
                this.performAction.defer(100, this);
            } else {
                this.findParentByType(eBook.Window).locked = false;
                this.fireEvent('actionsDone', false, this.actionsContainer);
            }
        }

        // temp:

    }

});

Ext.reg('eBook-createFile-actions', eBook.ProAcc.ActionDataView);