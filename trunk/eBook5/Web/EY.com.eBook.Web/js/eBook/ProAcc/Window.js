

eBook.ProAcc.Window = Ext.extend(eBook.Window, {
    clientOnly: false
    , includePreviousYear: true
    , initComponent: function() {
        Ext.apply(this, {
            iconCls: ''
            , title: eBook.ProAcc.Window_Title
            , layout: 'card'
            , activeItem: 0
            , modal: true
            , items: [{
                xtype: 'panel'
                        , ref: 'frm'
                        , bodyStyle: 'padding:10px;'
                        , items: [{
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportAccountSchema
                                    , ref: 'accountschema'
                                    , name: 'accountschema'
                                    , hidden: this.clientOnly
                                    , listeners: {
                                        'check': { fn: this.onCheck, scope: this }
                                    }
                        }, {
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportCurrentBalans
                                    , ref: 'currentbookyear'
                                    , name: 'currentbookyear'
                                    , hidden: this.clientOnly
                                    , listeners: {
                                        'check': { fn: this.onCheck, scope: this }
                                    }
                        }, {
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportPreviousBalans
                                    , ref: 'lastbookyear'
                                    , name: 'lastbookyear'
                                    , hidden: !this.clientOnly ? !this.includePreviousYear : true
                                    , listeners: {
                                        'check': { fn: this.onCheck, scope: this }
                                    }
                        }, {
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportCustomers
                                    , ref: 'customers'
                                    , name: 'customers'
                        }, {
                            xtype: 'checkbox'
                                    , boxLabel: eBook.ProAcc.Window_ImportSuppliers
                                    , ref: 'suppliers'
                                    , name: 'suppliers'
}]
            }, { xtype: 'eBook-createFile-actions'
                        , ref: 'create'
}]
            , tbar: [{
                ref: '../empty',
                text: eBook.ProAcc.Window_StartImport,
                iconCls: 'eBook-empty-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onStartImport,
                scope: this
}]
            });
            eBook.ProAcc.Window.superclass.initComponent.apply(this, arguments);
        }
    , onCheck: function(chk, state) {
        switch (chk.name) {
            case 'accountschema':
                if (!state) {
                    this.frm.currentbookyear.setValue(false);
                    this.frm.lastbookyear.setValue(false);
                }
                break;
            case 'currentbookyear':
            case 'lastbookyear':
                if (state) {
                    this.frm.accountschema.setValue(true);
                }
                break;
        }
    }
    , onStartImport: function() {

        var acts = {
            actions: []
            , revertActions: []
            , postActions: []
        };
        var proAccServer = eBook.Interface.currentClient.get('ProAccServer');
        var proAccDatabase = eBook.Interface.currentClient.get('ProAccDatabase');
        var fileId = eBook.Interface.currentFile.get('Id');
        var clientId = eBook.Interface.currentClient.get('Id');

        var cur = this.frm.currentbookyear.getValue();
        var prev = this.frm.lastbookyear.getValue();

        var cust = this.frm.customers.getValue();
        var supp = this.frm.suppliers.getValue();
        var id = 0;

        if (cust) {
            acts.actions.push({ id: id
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , response: null
                , text: 'Import customers from ProAcc'
                , url: eBook.Service.businessrelation
                , action: 'ImportCustomersProAcc'
                , params: { cidc: { Id: clientId, Person:eBook.User.getActivePersonDataContract()} }
                , paramKey: 'cidc'
                , scope: this
            });
            id++;
        }
        
        if (supp) {
            acts.actions.push({ id: id
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , response: null
                , text: 'Import suppliers from ProAcc'
                , url: eBook.Service.businessrelation
                , action: 'ImportSuppliersProAcc'
                , params: { cidc: { Id: clientId, Person: eBook.User.getActivePersonDataContract()} }
                , paramKey: 'cidc'
                , scope: this
            });
            id++;
        }

        if (cur || prev) {
            acts.actions.push({ id: id
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , response: null
                , text: 'Import (new) accounts from proAcc including current state' + (prev ? ' and previous state' : '')
                , url: eBook.Service.file
                , action: 'ImportAccountsProAcc'
                , params: { iaedc: { fid: fileId, ips: prev} }
                , paramKey: 'iaedc'
                , callback: function(cr, resp) {
                    var robj = Ext.decode(resp.responseText);
                    robj = robj['ImportAccountsProAccResult'];
                    var dte = Ext.data.Types.WCFDATE.convert(robj);
                    eBook.Interface.center.fileMenu.mymenu.setLastImportDate(dte);
                }
                , scope: this
            });
            id++;
        }


        acts.actions.push({ id: 1
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , response: null
                , text: 'Update cache'
                , url: eBook.Service.file
                , action: 'ForceFileCacheReload'
                , params: { cfdc: { FileId: fileId, Culture: eBook.Interface.Culture} }
                , paramKey: 'cfdc'
                , callback: this.onActionReady
                , scope: this
        });
        id++;


        this.getTopToolbar().disable();
        this.layout.setActiveItem(this.create);
        this.create.on('actionsDone', this.onImportDone, this);
        this.create.setActions(acts);
        this.create.start.defer(200, this.create);
    }
    , onImportDone: function() {
        if (this.caller && Ext.isFunction(this.caller.importDone)) this.caller.importDone();
        Ext.Msg.alert(eBook.ProAcc.Window_ImportDoneTitle, eBook.ProAcc.Window_ImportDoneMsg);
        eBook.Interface.center.fileMenu.updateWorksheets();
        this.close.defer(500, this);
    }
    , show: function(caller) {
        this.caller = caller;
        eBook.ProAcc.Window.superclass.show.call(this);
    }
    });

Ext.reg('eBook.ProAcc.Window', eBook.ProAcc.Window);