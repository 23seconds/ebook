

eBook.Document.TemplateChooser = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            tpl: new Ext.XTemplate(
                        '<tpl for=".">'
                            , '<div class="eBook-templatechooser-item" id="{Id}_{culture}">'
                                , '<div class="eBook-templatechooser-preview"><img src="images/documentTypes/{Id}.png" alt="{Name}"/></div>'
                                , '<div class="eBook-templatechooser-name">{Name}</div>'
            //, '<div class="eBook-templatehooser-description">{Name}</div>'
                        , '</div>',
                        '</tpl>')
            , itemSelector: '.eBook-templatechooser-item'
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetDocumentTypes'
                        , serviceUrl: eBook.Service.document
                        , criteriaParameter: 'cicdc'
                        , autoDestroy: true
                        , fields: eBook.data.RecordTypes.DocumentType
                        , autoLoad: true
                        , baseParams: { Id: eBook.EmptyGuid, Culture: eBook.Interface.Culture }
            })
            , singleSelect: true
            , overClass: 'eBook-templatechooser-over'
            , selectedClass: 'eBook-templatechooser-select'
            , autoScroll: true
            , cls: 'eBook-templatechooser'

        });
        this.groupId = eBook.EmptyGuid;
        this.culture = eBook.Interface.Culture;
        eBook.Document.TemplateChooser.superclass.initComponent.apply(this, arguments);
        this.on('selectionchange', this.onSelectionChanged, this);
    }
    , updateParams: function(culture, groupId) {
        this.culture = culture;
        this.groupId = groupId;
        this.store.baseParams = { Id: groupId, Culture: culture };
        this.store.load();
    }
    , prepareData: function(obj) {
        obj.culture = this.culture;
        return obj;
    }
    , getSelectedId: function() {
        var recs = this.getSelectedRecords();
        return recs[0].get('Id');
    }
    , standardTpl: ''
    , onSelectionChanged: function(dv, sels) {
        var t = this.refOwner.documentName;
        if (this.getSelectedRecords().length > 0) {
            t.setValue(this.getSelectedRecords()[0].get('Name'));
        }
        else {
            t.setValue("");
        }
    }
});

Ext.reg('eBook.TemplateChooser', eBook.Document.TemplateChooser);