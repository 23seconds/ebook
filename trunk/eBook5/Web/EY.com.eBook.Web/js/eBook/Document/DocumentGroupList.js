
eBook.Document.GroupList = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        this.culture = eBook.Interface.Culture;
        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                        selectAction: 'GetDocumentGroups'
                        , serviceUrl: eBook.Service.document
                        , baseParams: {
                            Culture: eBook.Interface.Culture
                        }
                        , criteriaParameter: 'ccdc'
                        , autoDestroy: true
                        , fields: [
                                    { name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
                                    , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                                    ]
            })
            , minChars: 1
            , displayField: 'Name'
            , valueField: 'Id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: 200
        });
        eBook.Document.GroupList.superclass.initComponent.apply(this, arguments);
    }
    , setCulture: function(cult) {
        this.culture = cult;
        this.store.baseParams = {
            Culture: cult
        };
        this.store.load();
        this.setValue(eBook.EmptyGuid);
    }
});

Ext.reg('eBook.DocumentGroupList', eBook.Document.GroupList);