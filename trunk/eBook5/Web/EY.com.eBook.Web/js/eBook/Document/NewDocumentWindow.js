


eBook.Document.NewDocumentWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'form'
            , maximizable: false
            , title: eBook.Document.NewDocumentWindow_Title
            , resizable: false
            , height: 420
            , modal: true
            , bodyStyle: 'padding:10px;'
            , items: [
                {
                    xtype: 'textfield'
                    , ref: 'documentName'
                    , fieldLabel: eBook.Document.NewDocumentWindow_DocName
                    , allowBlank: false
                    , width: 250
                },
                {
                    xtype: 'languagebox'
                    , ref: 'culture'
                    , fieldLabel: eBook.Document.NewDocumentWindow_Language
                    , allowBlank: false
                    , value: eBook.Interface.Culture
                    , autoSelect: true
                    , listeners: {
                        'select': {
                            fn: this.changedTemplateCriteria
                            , scope: this
                        }
                    }
                }, {
                    xtype: 'eBook.DocumentGroupList'
                    , ref: 'docgroup'
                    , fieldLabel: eBook.Document.NewDocumentWindow_Group
                    , allowBlank: false
                    , autoSelect: true
                    , listeners: {
                        'select': {
                            fn: this.changedTemplateCriteria
                            , scope: this
                        }
                    }
                },
                {
                    xtype: 'label'
                    , text: eBook.Document.NewDocumentWindow_Template
                    , cls: 'x-form-item'
                }
                , {
                    xtype: 'eBook.TemplateChooser'
                    , ref: 'template'
                    , height: 200
                    , loadingText: eBook.Document.NewDocumentWindow_LoadingTemplates
                }
            ]
            , tbar: [{
                ref: 'createdocument',
                text: eBook.Document.NewDocumentWindow_CreateDoc,
                iconCls: 'eBook-icon-createdocument-24 ',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onCreateDocumentClick,
                scope: this
}]
            });
            eBook.Document.NewDocumentWindow.superclass.initComponent.apply(this, arguments);


        }
    , changedTemplateCriteria: function() {
        var cult, gid;
        cult = this.culture.getValue();
        if (cult != this.docgroup.culture) {
            this.docgroup.setCulture(cult);
        }
        gid = this.docgroup.getValue();
        if (Ext.isEmpty(cult)) cult = eBook.Interface.Culture;
        if (Ext.isEmpty(gid)) gid = eBook.EmptyGuid;
        this.template.updateParams(cult, gid);
    }
    , show: function(caller) {
        this.caller = caller;
        eBook.Document.NewDocumentWindow.superclass.show.call(this);
    }
    , onCreateDocumentClick: function() {
        var c = this.culture.getValue();
        var recs = this.template.getSelectedRecords();
        if (recs.length > 0) {
            if (Ext.isDefined(this.caller) && c != null) {
                this.caller.newDocument(recs[0].get('Id'), c, this.documentName.getValue());
                this.close();
            }
        }
        else {
            alert(eBook.Document.NewDocumentWindow_NoTemplateSelected);
        }
    }
    });