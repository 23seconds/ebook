eBook.Document.Fields.FreeEditor = Ext.extend(Ext.ux.TinyMCE, {
    initComponent: function() {
        Ext.apply(this, {
            tinymceSettings: {
                theme: "advanced",
                plugins: "pagebreak,table,iespell,insertdatetime,searchreplace,paste,directionality,noneditable,xhtmlxtras",
                theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|cut,copy,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,cleanup,code,|,insertdate,inserttime",
                theme_advanced_buttons2: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|",
                //theme_advanced_buttons1 : "bold,italic,underline",
                //theme_advanced_buttons2 : "",
                theme_advanced_buttons3 : "",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: false,
                extended_valid_elements: ""
            },
            boxMinHeight: 200
        });
        this.id = this.parCmpID + '_' + this.name;
        
        
        if (this.fullDocument == "true" || this.fullDocument == true) {
                this.height=700;
        }
        // this.id = this.parCmpID + '_' + this.name;
        eBook.Document.Fields.FreeEditor.superclass.initComponent.apply(this, arguments);

    }
    /*,beforeDestroy : function(){
    if(this.monitorTask){
    Ext.TaskMgr.stop(this.monitorTask);
    }
    if (this.rendered) {
    if(!this.getWin()) 
    Ext.destroy(this.tb);
    var doc = this.getDoc();
    if(doc){
    try{
    Ext.EventManager.removeAll(doc);
    for (var prop in doc){
    delete doc[prop];
    }
    }catch(e){}
    }
    if(this.wrap){
    this.wrap.dom.innerHTML = '';
    this.wrap.remove();
    }
    }
    eBook.Document.Fields.FreeEditor.superclass.beforeDestroy.call(this);
    }
    , getWin: function() {
    try {
    return eBook.Document.Fields.FreeEditor.superclass.getWin.call(this);
    } catch (e) {
    return null;
    }
    }
    , getDoc: function() {
    if (Ext.isIE && !this.getWin()) return;
    if (!Ext.isIE && !this.iframe && !this.getWin()) return;
    if (!Ext.isIE && !this.iframe.contentDocument && !this.getWin()) return;
    return Ext.isIE ? this.getWin().document : (this.iframe.contentDocument || this.getWin().document);
    }
    , onRender: function(ct, position) {
    eBook.Document.Fields.FreeEditor.superclass.onRender.call(this, ct, position);
    this.el.setStyle('width', '100%');
    Ext.get(this.iframe).setStyle('width', '100%');
    if (this.fullDocument == "true" || this.fullDocument == true) {
    this.el.setStyle('height', '700px');
    Ext.get(this.iframe).setStyle('height', '700px');
    }
    }*/
    , cleanHtml: function(html) {
        // var cleaned = eBook.Document.Fields.FreeEditor.superclass.cleanHtml.call(this, html);
        //cleaned = html.replace(/<br>/gi, '<br/>');
        //return cleaned;
        return html;
    }
    , setTimedValue: function(v) {
        this.setMeValue.defer(500, this, [v]);
    }
    , setMeValue: function(v) {
        if (!this.rendered) this.setMeValue.defer(500, this, [v]);
        this.setValue(v);
    }
    , getValue: function() {
        //if (!this.getWin()) return "";
        val = eBook.Document.Fields.FreeEditor.superclass.getValue.call(this);
        return this.cleanHtml(val);
    }
    , createToolbar: function(editor) {
        /*
        var items = [];
        var tipsEnabled = Ext.QuickTips && Ext.QuickTips.isEnabled();


        function btn(id, toggle, handler) {
        return {
        itemId: id,
        cls: 'x-btn-icon',
        iconCls: 'x-edit-' + id,
        enableToggle: toggle !== false,
        scope: editor,
        handler: handler || editor.relayBtnCmd,
        clickEvent: 'mousedown',
        tooltip: tipsEnabled ? editor.buttonTips[id] || undefined : undefined,
        overflowText: editor.buttonTips[id].title || undefined,
        tabIndex: -1
        };
        }

        if (this.enableFormat) {
        items.push(
        btn('bold'),
        btn('italic'),
        btn('underline')
        );
        }

        if (this.enableFontSize) {
        items.push(
        '-',
        btn('increasefontsize', false, this.adjustFont),
        btn('decreasefontsize', false, this.adjustFont)
        );
        }


        if (this.enableAlignments) {
        items.push(
        '-',
        btn('justifyleft'),
        btn('justifycenter'),
        btn('justifyright')
        );
        }

        if (!Ext.isSafari2) {
        if (this.enableLinks) {
        items.push(
        '-',
        btn('createlink', false, this.createLink)
        );
        }

            if (this.enableLists) {
        items.push(
        '-',
        btn('insertorderedlist'),
        btn('insertunorderedlist')
        );
        }
        if (this.enableSourceEdit) {
        items.push(
        '-',
        btn('sourceedit', true, function(btn) {
        this.toggleSourceEdit(!this.sourceEditMode);
        })
        );
        }
        }

        //items.push('-', btn('save', false, this.onSave));

        // build the toolbar
        var tb = new Ext.Toolbar({
        renderTo: this.wrap.dom.firstChild,
        items: items
        });



        // stop form submits
        this.mon(tb.el, 'click', function(e) {
        e.preventDefault();
        });

        this.tb = tb;
        this.tb.doLayout();
        */
    }
});

eBook.Document.Fields.BlockEditor = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'vbox'
            , layoutConfig: { align: 'stretch' }
            , height: 300
            , header: false
            , items: [new Ext.form.TextArea({ ref: 'title', height: 60 }), new eBook.Document.Fields.HtmlEditor({ ref: 'text', height: 200 })]
            , tbar: new Ext.Toolbar({
                items: [{
                        ref: 'savechanges',
                        text: eBook.Document.Fields.BlockEditor_Save,
                        iconCls: 'eBook-icon-16-save',
                        scale: 'small',
                        iconAlign: 'top',
                        handler: this.commitEdit,
                        scope: this
                    }, {
                        ref: 'cancel',
                        text: eBook.Document.Fields.BlockEditor_Cancel,
                        iconCls: 'eBook-icon-16-cancel',
                        scale: 'small',
                        iconAlign: 'top',
                        handler: this.cancelEdit,
                        scope: this
                    }]
                })
            });
            eBook.Document.Fields.BlockEditor.superclass.initComponent.apply(this, arguments);
        }
    , startEdit: function(el, rec) {
        if (this.rendered) {
            this.el.remove();
        }
        this.contEl = Ext.get(el);
        this.activeRecord = rec;
        if (this.contEl.child('.eBook-documents-block-title')) {
            this.contEl.child('.eBook-documents-block-title').setDisplayed('none');
        }
        if (this.contEl.child('.eBook-documents-block-txt')) {
            this.contEl.child('.eBook-documents-block-txt').setDisplayed('none');
        }
        this.render(el);
        this.title.setValue(rec.get('Title'));
        this.text.setValue(rec.get('Text'));

    }
    , commitEdit: function() {
        this.activeRecord.set('Title', this.title.getValue());
        this.activeRecord.set('Text', this.text.getValue());
        this.cancelEdit();
    }
    , cancelEdit: function() {
        this.activeRecord.isEditing = false;
        if (this.contEl.child('.eBook-documents-block-title')) {
            this.contEl.child('.eBook-documents-block-title').setDisplayed('');
        }
        if (this.contEl.child('.eBook-documents-block-txt')) {
            this.contEl.child('.eBook-documents-block-txt').setDisplayed('');
        }
        this.destroy();
    }
    
});



eBook.Document.Fields.HtmlEditor = Ext.extend(Ext.ux.TinyMCE, {
    initComponent: function() {
        Ext.apply(this, {
            tinymceSettings: {
                theme: "advanced",
                plugins: "pagebreak,table,iespell,insertdatetime,searchreplace,paste,directionality,noneditable,xhtmlxtras",
                theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|cut,copy,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,cleanup,code,|,insertdate,inserttime",
                theme_advanced_buttons2: "",
                theme_advanced_buttons3: "",
                //theme_advanced_buttons4: ",abbr,acronym,del,ins,|,visualchars,nonbreaking,pagebreak",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: true,
                extended_valid_elements: ""
            },
            boxMinHeight: 200
        });
        eBook.Document.Fields.HtmlEditor.superclass.initComponent.apply(this, arguments);
    }
    , startEdit: function(el, val) {
        if (Ext.isDefined(val)) this.setValue(val);
        if (this.rendered) {
            this.el.remove();
            this.rendered = false;
        }

        this.render(el);
    }
});