eBook.Document.Fields.DefaultField = {
    locationEl: null
    , properties: []
    , parCmpID: ''
    , initComponent: function () {
        //this.addEvents('anchor');
        Ext.apply(this, {
            style: { display: 'inline' }
        });
        //this.locationEl = Ext.get(this.properties.id);
        this.id = this.parCmpID + '_' + this.name;
        if (this.properties.eBook.datatype == 'label') {
            if (Ext.isDefined(this.properties.eBook.defaultValue)) this.setText(this.properties.eBook.defaultValue);
        } else {
            if (Ext.isDefined(this.properties.eBook.defaultValue)) this.setValue(this.properties.eBook.defaultValue);
        }
        /*
        this.on('focus',this.onDocumentFieldFocus,this);
        this.on('blur',this.onDocumentFieldBlur,this);
        */

    }
    /*
    , onDocumentFieldFocus: function() {
    //this.anchor();
    /*
    if(!eBook.Document.Fields.QTip) {
    var cfg = {tag: "div", id: this.id+'_lbldiv', style:"display:inline-block;white-space:normal;padding:5px;z-index:20;background-color:#FFE600;"};
    var el = Ext.get(this.parCmpID);
    eBook.Document.Fields.QTip = el.createChild(cfg);
    }
    eBook.Document.Fields.QTip.update(this.properties.eBook.displayName);
    eBook.Document.Fields.QTip.setWidth(eBook.Document.Fields.QTip.getTextWidth(this.properties.eBook.displayName.replace(' ','_')));
    eBook.Document.Fields.QTip.show();
    eBook.Document.Fields.QTip.anchorTo(this.locationEl,'tl',[0,-this.locationEl.getComputedHeight()]);
        
    }
    ,onDocumentFieldBlur:function() {
    if(eBook.Document.Fields.QTip) {
    eBook.Document.Fields.QTip.hide();
    }
    }
    */
};
    
eBook.Document.Fields.TextField = Ext.extend(Ext.form.TextField,eBook.Document.Fields.DefaultField);
eBook.Document.Fields.NumberField = Ext.extend(Ext.form.NumberField,eBook.Document.Fields.DefaultField);
eBook.Document.Fields.DateField = Ext.extend(Ext.form.DateField,eBook.Document.Fields.DefaultField);
eBook.Document.Fields.DateField = Ext.extend(eBook.Document.Fields.DateField, {
    onTriggerClick : function(){
        this.el = this.locationEl;
        eBook.Document.Fields.DateField.superclass.onTriggerClick.apply(this,arguments);
    }
});
eBook.Document.Fields.TimeField = Ext.extend(Ext.form.TimeField,eBook.Document.Fields.DefaultField);
eBook.Document.Fields.ComboBox = Ext.extend(Ext.form.ComboBox, eBook.Document.Fields.DefaultField);
eBook.Document.Fields.Label = Ext.extend(Ext.form.Label, eBook.Document.Fields.DefaultField);
Ext.QuickTips.init();