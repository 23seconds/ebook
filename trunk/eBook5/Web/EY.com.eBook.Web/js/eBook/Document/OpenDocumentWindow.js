

eBook.Document.OpenDocumentWindow = Ext.extend(eBook.Window, {
    initComponent:function() {
        Ext.apply(this, {
            height:200
            ,width:200
            ,items:[{
                    xtype:'listview'
                    ,store: new eBook.data.JsonStore({
                                selectAction: 'GetDocuments'
                                , serviceUrl: eBook.Service.document
                                        , criteriaParameter: 'cfdc'
                                        , autoDestroy: true
                                        , fields: eBook.data.RecordTypes.DocumentList
                                        , autoLoad: true
                                        , baseParams: { FileId: eBook.Interface.currentFile.get('Id') }
                            })
                    ,singleSelect:true
                    , emptyText: eBook.Document.OpenDocumentWindow_NoDocuments
                    ,reserveScrollOffset: true
                    ,columns: [{
                                    header: 'Document'
                                    ,width: 1
                                    ,dataIndex: 'Name'
                    }]
                    ,listeners: {
                        'click': {
                                fn:this.onClick
                                ,scope:this
                                }
                    }
              }]
        });
        eBook.Document.OpenDocumentWindow.superclass.initComponent.apply(this,arguments);
    }
    , show: function(caller) {
        this.caller = caller;
        eBook.Document.OpenDocumentWindow.superclass.show.call(this);
    }
    ,onClick:function(dv,idx,n,e) {
        if(Ext.isDefined(this.caller)) this.caller.loadDocument(dv.store.getAt(idx));
        this.close();
    }
});