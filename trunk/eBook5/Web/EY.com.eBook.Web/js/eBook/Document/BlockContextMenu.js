
eBook.Document.BlockContextMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            items: [{
                text: 'Move Up',
                iconCls: 'moveup',
                scope: this,
                handler: this.onMoveUp
            }, {
                text: 'Move Down',
                iconCls: 'movedown',
                scope: this,
                handler: this.moveDown
            }, '-', {
                text: 'Edit',
                iconCls: 'editblock',
                scope: this,
                handler: this.onEdit
            }, {
                text: 'Delete',
                iconCls: 'deleteblock',
                scope: this,
                handler: this.onDelete
}]
            });
            eBook.Document.BlockContextMenu.superclass.initComponent.apply(this, arguments);
        }
    , loadBlock: function(rec, str, container, node) {
        this.activeRecord = rec;
        this.activeStore = str;
        this.activeContainer = container;
        this.activeNode = node;
    }
    , onEdit: function() {
        if (this.activeRecord && this.activeContainer && this.activeNode) {
            this.activeContainer.editItem(this.activeNode, this.activeRecord);
        }
    }
    , onDelete: function() {
        if (this.activeRecord && this.activeStore) {
            this.activeStore.remove(this.activeRecord);
            var order = 0;
            this.activeStore.each(function(r) {
                r.set('Order', order);
                order++;
                //if (r.get('Order') >= (order - 1)) r.set('Order', r.get('Order') + 1);
            }, this);
            this.activeStore.commitChanges();
            this.activeStore.sort('Order', 'ASC');
        }
    }
    , onMoveUp: function() {

        if (this.activeRecord && this.activeStore) {
            var order = this.activeRecord.get('Order');
            if (order == 0) return;
            this.activeStore.each(function(r) {
                if (r.get('Order') == (order - 1)) r.set('Order', order);
                //if (r.get('Order') >= (order - 1)) r.set('Order', r.get('Order') + 1);
            }, this);
            this.activeRecord.set('Order', order - 1);
            this.activeRecord.commit();
            this.activeStore.commitChanges();
            this.activeStore.sort('Order', 'ASC');
        }
    }
    , moveDown: function() {
        if (this.activeRecord && this.activeStore) {
            var order = this.activeRecord.get('Order');
            if (order == this.activeStore.getCount()) return;
            this.activeStore.each(function(r) {
                if (r.get('Order') == (order + 1)) r.set('Order', order);
            }, this);
            this.activeRecord.set('Order', order + 1);
            this.activeRecord.commit();
            this.activeStore.commitChanges();
            this.activeStore.sort('Order', 'ASC');
        }
    }
    });