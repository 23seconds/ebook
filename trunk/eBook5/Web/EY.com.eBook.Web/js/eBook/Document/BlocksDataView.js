/*
eBook.Document.BlocksDataView = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            html: 'Blocks dataview'
            ,region:'east'
        });
        eBook.Document.BlocksDataView.superclass.initComponent.apply(this, arguments);
    }
});

*/

eBook.Document.BlocksPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.storeCfg.id = Ext.id();
        Ext.apply(this, {
            items: [new eBook.Document.BlocksDataView({ store: new Ext.data.JsonStore(this.storeCfg), blockTypeId: this.blockTypeId })]
            ,autoScroll:true       
        });
        eBook.Document.BlocksPanel.superclass.initComponent.apply(this, arguments);
    }
});

eBook.Document.BlocksDataView = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.eBook-documents-view-block'
            , tpl: new Ext.XTemplate(
                        '<tpl for=".">',
                        '<div class="eBook-documents-view-block" id="' + this.id + '-{[xindex]}"><div class="eBook-documents-view-block-title">{nameTitle}</div>',
                        '<div class="eBook-documents-view-block-txt">{shorttxt}</div>',
                         '</div>', //<div class="eBook-documents-view-block-txt">{shorttxt}</div>
                        '</tpl>')
            , prepareData: function(obj) {
                obj.nameTitle = Ext.isEmpty(obj.Title) ? Ext.util.Format.stripTags(obj.Text).substr(0, 30) + '...' : obj.Title;
                obj.shorttxt = Ext.util.Format.stripTags(obj.Text).substr(0, 20) + '...';
                return obj;
            }
            , autoScroll: true
        });
        eBook.Document.BlocksDataView.superclass.initComponent.apply(this, arguments);
        if (!eBook.Interface.currentFile.get('Closed')) this.on('render', this.initializeDragZone, this);
    }
    , initializeDragZone: function(v) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.dragZone = new Ext.dd.DragZone(v.getEl(), {
            dragView: v,
            ddGroup: 'eBook-document-blocks',
            getDragData: function(e) {
                var sourceEl = e.getTarget(this.dragView.itemSelector, 10);
                if (sourceEl) {
                    var d = sourceEl.cloneNode(true);
                    d.id = Ext.id();
                    var tdta = {};
                    Ext.apply(tdta, this.dragView.getRecord(sourceEl).data);
                    tdta.blockTypeId = this.dragView.blockTypeId;
                    return this.dragView.dragData = {
                        sourceEl: sourceEl
                        , repairXY: Ext.fly(sourceEl).getXY()
                        , ddel: d
                        , blockData: tdta
                    }

                }
            },
            getRepairXY: function() {
                return this.dragData.repairXY;
            }
        });
    }
});
