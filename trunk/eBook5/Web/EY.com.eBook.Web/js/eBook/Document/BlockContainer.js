

eBook.Document.BlockContainer = Ext.extend(Ext.DataView, {
    blockTypes: []
    , initComponent: function() {
        Ext.apply(this, {
            autoHeight: true,
            autoScroll: true,
            containerMouseEvents: true,
            multiSelect: false,
            overClass: 'x-view-over',
            selectedClass: 'eBook-documents-block-select'
        });

        switch (this.viewType) {
            case 'list':
                Ext.apply(this, {
                    itemSelector: 'li.eBook-documents-block'
                    , tpl: new Ext.XTemplate(
                        '<tpl for=".">',
                        '<ul><li class="eBook-documents-block eBook-documents-block-title" id="' + this.id + '-{Order}">{Title}</li></ul>',
                        '</tpl>')
                });
                break;
            default:
                Ext.apply(this, {
                    itemSelector: 'div.eBook-documents-block'
                    , tpl: new Ext.XTemplate(
                        '<tpl for=".">',
                        '<div class="eBook-documents-block" id="' + this.id + '-{Order}"><div class="eBook-documents-block-title" style="font-weight:bold;">{Title}</div>',
                        '<div class="eBook-documents-block-txt">{Text}</div></div>',
                        '',
                        '</tpl>')
                });
                break;
        }
        eBook.Document.BlockContainer.superclass.initComponent.apply(this, arguments);

        if (!eBook.Interface.currentFile.get('Closed')) this.on('render', this.initializeDropZone, this);
        if (!eBook.Interface.currentFile.get('Closed')) this.on('dblclick', this.onItemDoubleClick, this);
        if (!eBook.Interface.currentFile.get('Closed')) this.on('contextmenu', this.onItemContextmenu, this);
    }
    , destroy: function() {
        if (this.mnuContext) this.mnuContext.destroy();
        eBook.Document.BlockContainer.superclass.destroy.call(this);
    }
    , showMenu: function(rec, store, el, e) {
        if (!this.mnuContext) {
            this.mnuContext = new eBook.Document.BlockContextMenu({});
        }
        this.mnuContext.loadBlock(rec, store, this,el);
        this.mnuContext.showAt(e.getXY());
    }
    , onItemContextmenu: function(dv, idx, n, e) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var rec = this.store.getAt(idx);
        if (rec) this.showMenu(rec, this.store, n, e);
    }
    , onItemDoubleClick: function(dv, idx, n, e) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var rec = this.store.getAt(idx);
        this.editItem(n, rec);
    }
    , editItem: function(n, rec) {
        if (!Ext.isDefined(rec.isEditing) || !rec.isEditing) {
            rec.isEditing = true;
            this.editor = new eBook.Document.Fields.BlockEditor({});
            this.editor.startEdit(n, rec);
        }
    }
    , validateBlockType: function(bt) {
        for (var i = 0; i < this.blockTypes.length; i++) {
            if (bt.toLowerCase() == this.blockTypes[i].toLowerCase()) return true;
        }
        return false;
    }
    , initializeDropZone: function(v) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.dropZone = new Ext.dd.DropZone(v.getEl(), {
            vw: v,
            ddGroup: 'eBook-document-blocks',
            overClass: 'eBook-documents-blocks-over',
            overOkClass: 'eBook-documents-blocks-over-ok',
            overFailClass: 'eBook-documents-blocks-over-fail',
            notifyOut: function(dd, e, data) {
                this.el.removeClass(this.overFailClass);
                this.el.removeClass(this.overOkClass);
            },
            /* notifyEnter: function(dd, e, data) {
            if (this.vw.validateBlockType(data.blockData.blockTypeId)) {
            return Ext.dd.DropTarget.prototype.dropAllowed;
            }
            return Ext.dd.DropTarget.prototype.dropNotAllowed;
            },*/
            onContainerOver: function(dd, e, data) {
                if (this.vw.validateBlockType(data.blockData.blockTypeId)) {
                    this.el.addClass(this.overOkClass);
                    return Ext.dd.DropTarget.prototype.dropAllowed;
                }
                this.el.addClass(this.overFailClass);
                return Ext.dd.DropTarget.prototype.dropNotAllowed;
            },
            onContainerDrop: function(dd, e, data) {
                if (this.vw.validateBlockType(data.blockData.blockTypeId)) {
                    var exel = e.getTarget('.eBook-documents-block');
                    var order = this.vw.store.getCount();
                    if (exel) {
                        order = parseInt(exel.id.replace(this.vw.id + '-', ''));
                        this.vw.store.each(function(r) {
                            if (r.get('Order') >= order) r.set('Order', r.get('Order') + 1);
                        }, this);
                    }
                    this.el.removeClass(this.overFailClass);
                    this.el.removeClass(this.overOkClass);
                    var rt = this.vw.store.recordType;
                    var rec = new rt(data.blockData);
                    rec.set('Order', order);
                    this.vw.store.add(rec);
                    rec.commit();
                    this.vw.store.sort('Order', 'ASC');
                    this.vw.store.commitChanges();
                    data = null;
                    return true;
                }
                return false;
            }
        });
    }
});