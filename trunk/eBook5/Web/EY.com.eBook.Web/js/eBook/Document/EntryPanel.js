

eBook.Document.EntryPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            html: 'EntryPanel'
        });
        eBook.Document.EntryPanel.superclass.initComponent.apply(this, arguments);
    }
});