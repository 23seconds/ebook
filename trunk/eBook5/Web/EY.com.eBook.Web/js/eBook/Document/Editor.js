

eBook.Document.Editor = Ext.extend(Ext.BoxComponent, {
    fields: new Ext.util.MixedCollection()
    , blocks: new Ext.util.MixedCollection()
    , blockStores: new Ext.util.MixedCollection()
    , id: 'ebook-document-editor'
    , onRender: function (ct) {
        this.el = ct.createChild({ id: this.id, cls: 'eBook-document-editor', tag: 'div' });
        eBook.Document.Editor.superclass.onRender.apply(this, arguments);


    }
    , getLayoutTarget: function () {
        return this.el;
    }
    , initComponent: function () {
        Ext.apply(this, {
            html: 'editor'
            , region: 'center'
            , style: 'background-color:#D0D0D0;'
            , autoScroll: true
        });
        eBook.Document.Editor.superclass.initComponent.apply(this, arguments);
    }
    , loadTemplate: function (html) {
        this.el.update(html);
        this.bodyEl = Ext.get(Ext.select('div#' + this.id + ' div.eBook-document-body').elements[0]);

        this.processBlockContainers();

    }
    , loadDocument: function (doc) {

        if (doc.bcd != null && Ext.isArray(doc.bcd)) {
            for (var i = 0; i < doc.bcd.length; i++) {
                var str = this.blockStores.get(doc.bcd[i].n);
                if (Ext.isDefined(str) && str != null) {
                    str.loadData(doc.bcd[i].bs);
                }
            }
        }
        if (doc.df != null && Ext.isArray(doc.df)) {
            for (var i = 0; i < doc.df.length; i++) {
                var fid = this.id + '_' + doc.df[i].n;
                var fld = this.fields.get(fid);
                if (fld != null && doc.df[i].v != null && Ext.isDefined(doc.df[i].v)) {
                    if (Ext.isDefined(fld.valRenderer) && Ext.isFunction(fld.valRenderer)) {
                        fld.setValue(fld.valRenderer(doc.df[i].v));
                    } else {
                        fld.setValue(doc.df[i].v);
                    }
                    if (fld.rendered && fld.syncValue) {
                        fld.syncValue();
                        fld.originalValue = doc.df[i].v;
                    }
                }
            }
        }
    }
    , clearAll: function () {
        this.clearFields();
        this.clearBlocks();
        this.el.update("");
    }
    , clearFields: function () {
        this.fields.each(function (fld) {
            fld.destroy();
        }, this);
        this.fields.clear();
    }
    , clearBlocks: function () {
        this.blocks.each(function (bl) {
            bl.destroy();
        }, this);
        this.blocks.clear();

        this.blockStores.eachKey(function (key) {
            this.blockStores.get(key).destroy();
        }, this);
        this.blockStores.clear();
    }
    , getBlockContainers: function () {
        //get array with blockcontainers and data
        var bcd = [];
        this.blockStores.eachKey(function (str) {
            var bs = this.blockStores.get(str);
            var bso = {
                n: str
                , bs: []
            };
            bs.each(function (rec) {
                bso.bs.push({
                    o: rec.get('Order')
                    , ti: rec.get('Title')
                    , text: rec.get('Text')
                });
            }, this);
            bcd.push(bso);
        }, this);
        return bcd;
    }
    , getFields: function () {
        // get array with fields
        var flds = [];
        this.fields.each(function (fld) {
            if (fld.dataType == 'label') {
                var fv = fld.text;
                var o = {
                    did: this.refOwner.activeDocument.id
                    , n: fld.name
                    , dt: fld.dataType
                    , v: fv
                };
                flds.push(o);
            } else {
                var fv = fld.getValue();
                if (fld.isValid() && fv != null && fv != "") {
                    var o = {
                        did: this.refOwner.activeDocument.id
                    , n: fld.name
                    , dt: fld.dataType
                    , v: fv
                    };
                    if (fld.dataType.toLowerCase() == 'datetime' || fld.dataType.toLowerCase() == 'date') {
                        delete o.v;
                        o.dv = fv;
                    }
                    flds.push(o);
                }
            }

        }, this);
        return flds;
    }
    , processBlockContainers: function () {
        // compile fields
        var freeEdits = this.bodyEl.select('.eBook-documents-free-edit').elements;
        for (var i = 0; i < freeEdits.length; i++) {
            this.renderFreeEdit(Ext.get(freeEdits[i]));
        }


        var fieldEls = this.bodyEl.select('.eBook-documents-field').elements;
        for (var i = 0; i < fieldEls.length; i++) {
            this.renderField(Ext.get(fieldEls[i]));
        }

        //this.alignFields();
        var blockdivs = this.bodyEl.select('div.eBook-documents-blocks').elements;
        for (var i = 0; i < blockdivs.length; i++) {
            this.renderBlock(Ext.get(blockdivs[i]));
        }
    }
    , getFieldProperties: function (el) {
        var properties = [];
        el = Ext.get(el);
        properties['eBook'] = [];
        properties['eBook']['name'] = el.getAttributeNS('eBook', 'name');
        properties['eBook']['fullDocument'] = el.getAttributeNS('eBook', 'fullDocument');
        if (properties['eBook']['name'] == null) properties['eBook']['name'] = el.getAttributeNS('eBook', 'Name');
        properties['eBook']['datatype'] = el.getAttributeNS('eBook', 'datatype');
        properties['eBook']['defaultValue'] = el.getAttributeNS('eBook', 'defaultValue');

        properties['eBook']['list'] = el.getAttributeNS('eBook', 'list');
        /* 
        properties['eBook']['displayName'] = el.getAttributeNS('eBook', 'displayName');
        properties['eBook']['value'] = el.getAttributeNS('eBook', 'value');
        properties['eBook']['store'] = el.getAttributeNS('eBook', 'store');
        properties['eBook']['value'] = properties['eBook']['value'] ? properties['eBook']['value'] : '';
        */
        return properties;
    }
    , renderField: function (el) {
        var fProps = this.getFieldProperties(el);
        var fld;
        //if (fProps.eBook.list == eBook.EmptyGuid) {
        switch (fProps.eBook.datatype.toLowerCase()) {
            case 'date':
            case 'datetime':
                fld = new eBook.Document.Fields.DateField({
                    applyTo: el,
                    locationEl: el,
                    //label: fProps.eBook.displayName,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    format: 'd/m/Y',
                    parCmpID: this.id,
                    properties: fProps,
                    valRenderer: Ext.data.Types.WCFDATE.convert
                });
                offs = 16;
                break;
            case 'time':
                fld = new eBook.Document.Fields.TimeField({
                    applyTo: el,
                    locationEl: el,
                    // label: fProps.eBook.displayName,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    parCmpID: this.id,
                    properties: fProps
                });
                offs = 16;
                break;
            case 'number':
            case 'int':
                fld = new eBook.Document.Fields.NumberField({
                    applyTo: el,
                    locationEl: el,
                    //  label: fProps.eBook.displayName,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    parCmpID: this.id,
                    properties: fProps
                });
                break;
            case 'combobox':
                var item = (fProps.eBook.list).split("-");

                for (var i = 0, c = item.length; i < c; i++) {
                    item[i] = [item[i]];
                }

                var items = new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'displayText'
                    ],
                    data: item
                }),

                fld = new eBook.Document.Fields.ComboBox({
                    applyTo: el,
                    locationEl: el,
                    label: "combo",
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    style: 'display:inline',
                    hideLabel: true,
                    enableKeyEvents: true,
                    store: items,
                    displayField: 'displayText',
                    typeAhead: true,
                    mode: 'local',
                    forceSelection: false,
                    editable: false,
                    triggerAction: 'all',
                    selectOnFocus: true,
                    valueField: 'displayText',
                    parCmpID: this.id,
                    properties: fProps,
                    width: 300,
                    listeners: {
                        change: function (t, newv, oldv) {
                            var items = document.querySelectorAll('input[data-name="' + fProps.eBook.name + '"]');
                            for (var i = 0; i < items.length; i++) {
                                items[i].value = newv;
                                items[i].readOnly = true;
                                //items[i].setAttribute("value", newv);
                                //this.setValue(newv);
                                //this.setRawValue(newv);
                            }
                        },
                        afterrender: function (cmp) {
                            cmp.getEl().set({
                                "data-name": fProps.eBook.name
                            });
                        }
                    }


                });
                break;
            case 'label':
                fld = new eBook.Document.Fields.Label({
                    style: 'border: 1px solid orange !important;',
                    cls: 'eBook-document-tooltip',
                    applyTo: el,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    locationEl: el,
                    width: '100%',
                    readOnly: true,
                    listeners: {
                        afterrender: function (c) {
                            c.el.dom.readOnly = true;
                            var text = c.properties.eBook.list.split('((br))').join('<br/>');
                            text = text.split('((i))').join('<i>');
                            text = text.split('((/i))').join('</i>');
                            text = text.split('((b))').join('<b>');
                            text = text.split('((/b))').join('</b>');
                            new Ext.ToolTip({
                                target: c.el.dom,
                                html: text
                            });
                            eBook.Document.Fields.Label.superclass.afterRender.call(this);
                        }
                    },
                    parCmpID: this.id,
                    properties: fProps
                });
                break;
            default:

                parNode = false;
                fld = new eBook.Document.Fields.TextField({
                    applyTo: el,
                    grow: true,
                    dataType: fProps.eBook.datatype,
                    name: fProps.eBook.name,
                    locationEl: el,
                    //  label: fProps.eBook.displayName,
                    listeners: {
                        'autosize': {
                            fn: function (fld, wd) {
                                fld.setWidth(wd + 20);
                            }, scope: this
                        },
                        afterrender: function (cmp) {
                            cmp.getEl().set({
                                "data-name": fProps.eBook.name
                            });
                        }
                    },
                    parCmpID: this.id,
                    properties: fProps
                });

                break;
        }
        // } else {
        /*
        fld = new eBook.Document.Fields.ComboBox({
        locationEl: el,
        label: fProps.eBook.displayName,
        style: 'display:inline',
        hideLabel: true,
        enableKeyEvents: true,
        store: MetaDataLookup,
        filterInfo: { fnc: function(rec) {
        var key = rc.get("listID");
        return (key == fProps.eBook.list || key == eBook.EmptyGuid);
        }
        },
        displayField: 'displayText',
        typeAhead: true,
        mode: 'local',
        forceSelection: false,
        triggerAction: 'all',
        selectOnFocus: true,
        valueField: 'value',
        parCmpID: this.id,
        properties: fProps
        });
        offs = 16;
        */
        //}


        //el.setWidth(fld.getBox().width+offs);
        fld.render();
        fld.el = el;
        this.fields.add(fld.id, fld);
    }
    , renderFreeEdit: function (el) {
        // create htmlfield
        //set data
        var fProps = this.getFieldProperties(el);
        var fld;
        var defval = el.dom.innerHTML;
        el.update("");
        fld = new eBook.Document.Fields.FreeEditor({ //eBook.Document.Fields.HtmlEditor({
            renderTo: el,
            // locationEl: el,
            //contentEl:el,
            //label: fProps.eBook.displayName,
            fullDocument: fProps.eBook.fullDocument,
            dataType: 'FREEEDIT',
            width: '100%',
            value: defval,
            name: fProps.eBook.name,
            parCmpID: this.id,
            properties: fProps
        });
        fld.render();
        //fld.setValue(defval);

        //fld.el = el;
        this.fields.add(fld.id, fld);

    }
    , renderBlock: function (el) {
        var elc = Ext.get(el);
        var name = elc.getAttributeNS('eBook', 'name');
        var blockTypes = elc.getAttributeNS('eBook', 'typeid');
        var viewt = elc.getAttributeNS('eBook', 'viewType');

        if (blockTypes != null && blockTypes != '') {
            blockTypes = blockTypes.split(",");
        } else {
            blockTypes = [];
        }

        if (!this.blockStores.containsKey(name)) {
            this.blockStores.add(name, new Ext.data.JsonStore({
                id: this.id + '-' + name,
                idProperty: 'Order',
                fields: eBook.data.RecordTypes.DocumentBlock,
                sortInfo: {
                    field: 'Order',
                    direction: 'ASC' // or 'DESC' (case sensitive for local sorting)
                }
            }));
        }

        var bc = new eBook.Document.BlockContainer({
            applyTo: el,
            blockContainerId: name,
            viewType: viewt,
            documentID: this.documentID,
            documentTypeID: this.documentTypeID,
            blockTypes: blockTypes,
            store: this.blockStores.get(name)
        });
        /*var r = new bc.store.recordType({ Order: 1, Title: "Title test", Text: "<b>Body</b> test." }, 1);
        bc.store.add(r);
        bc.store.commitChanges();*/
        bc.render();
        this.blocks.add(bc);

    }

});
Ext.QuickTips.init();