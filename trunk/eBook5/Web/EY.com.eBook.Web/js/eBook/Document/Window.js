
/*
    eBook.Document.Window
    
    Document builder window. 
    Layout:card.
    
    card 0: choose document type and language (provide preview?)
    card 1: document builder
    
    Upon showing without attribute, show card 0. 
    When selection is made open card 1 for this selection
    
    
*/



eBook.Document.Window = Ext.extend(eBook.Window, {
    constructor: function(config) {
        this.addEvents({
            "documentTypeLoaded": true
            , "documentLoaded": true
        });
        eBook.Document.Window.superclass.constructor.call(this, config);
    }
    , initComponent: function() {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        this.renderMenu();
        Ext.apply(this, {
            items: [new eBook.Document.Builder({ ref: 'builder' })]
            , title: eBook.Document.Window_Title
            , layout: 'fit'
            , bodyStyle: 'background-color:#ABABAB;'
            , tbar: new Ext.Toolbar({
                items: [{
                    ref: 'newdocument',
                    text: eBook.Document.Window_NewDocument,
                    iconCls: 'eBook-icon-newdocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onNewDocumentClick,
                    disabled: fileClosed,
                    scope: this
                }, {
                    ref: 'opendocument',
                    text: eBook.Document.Window_OpenDocument,
                    iconCls: 'eBook-icon-opendocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    menu: this.loadMenu,
                    scope: this
                }, {
                    ref: 'savedocument',
                    text: eBook.Document.Window_SaveDocument,
                    iconCls: 'eBook-icon-savedocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    disabled: true,
                    handler: this.onSaveDocumentClick,
                    scope: this
                },
                {
                    ref: 'printpreview',
                    text: eBook.Document.Window_PrintPreview,
                    iconCls: 'eBook-icon-previewdocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    disabled: true,
                    handler: this.onPrintPreviewClick,
                    scope: this
                }
                ]
            })
        });
        eBook.Document.Window.superclass.initComponent.apply(this, arguments);

    }
    , printPreview: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.getEl().mask(eBook.Document.Window_Loading, 'x-mask-loading');

            var pars = { didc: {
                        __type: 'DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data'
                        , DocumentId: this.documentId
                        , DocumentTypeId: this.documentTypeId
                        , FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: this.documentCulture
                        , type: 'DOCUMENT'
                    }
            };
            eBook.CachedAjax.request({
                url: eBook.Service.output + 'GetDocumentPrint'
                , method: 'POST'
                , params: Ext.encode(pars)
                , callback: this.onPdfGenerated
                , scope: this
            });
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onPdfGenerated: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetDocumentPrintResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onNewDocumentClick: function() {
        //this.builder.loadDocumentType(eBook.Document.TestTemplate);
        if (eBook.Interface.currentFile.get('Closed')) return;
        var wn = new eBook.Document.NewDocumentWindow({});
        wn.show(this);
    }
    , onOpenDocumentClick: function() {
        //this.builder.loadDocumentType(eBook.Document.TestTemplate);
        var wn = new eBook.Document.OpenDocumentWindow({});
        wn.show(this);
    }
    , onSaveDocumentClick: function() {
        var doc = this.builder.getDocument();
        if (eBook.Interface.currentFile.get('Closed')) return;
        doc.Person = eBook.User.getActivePersonDataContract();
        var action = {
            text: String.format(eBook.Document.Window_Saving, doc.n)
            , params: {
                ddc: doc
            }
            , url: eBook.Service.document
            , action: 'SaveDocument'
            , lockWindow: true
        };
        this.addAction(action);
    }
    , onPrintPreviewClick: function() {
        this.getEl().mask('Saving...', 'x-mask-loading');
        var doc = this.builder.getDocument();
        if (eBook.Interface.currentFile.get('Closed')) return;
        doc.Person = eBook.User.getActivePersonDataContract();
        this.documentId = doc.id;
        eBook.CachedAjax.request({
            url: eBook.Service.document + 'SaveDocument'
                , method: 'POST'
                , params: Ext.encode(
                            {
                                ddc: doc
                            })
                , callback: this.printPreview
                , scope: this
        });
    }
    , show: function(documentX) {
        // Active document
        if (!Ext.isDefined(documentX)) this.builder.hide();
        eBook.Document.Window.superclass.show.call(this);
        //this.builder.loadDocumentType(eBook.Document.TestTemplate);
        if (documentX) this.loadDocument(documentX);
    }
    , loadDocument: function(rec) {
        // Load the documentType
        this.builder.clearDocument();
        this.on('documentTypeLoaded', function() { this.loadDocumentById(rec.get('Id'), rec.get('Culture')); }, this, { scope: this, single: true });
        this.loadDocumentTypeById(rec.get('DocumentTypeId'), rec.get('Culture'));
    }
    , loadDocumentById: function(id, culture) {
        this.builder.clearDocument();
        this.documentId = id;
        // this.documentCulture = culture;
        this.getEl().mask(eBook.Document.Window_Loading, 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.document + 'GetDocument'
                , method: 'POST'
                , params: Ext.encode({
                    cicdc: {
                        Id: id
                            , Culture: culture
                    }
                })
                , callback: this.documentLoaded
                , scope: this
        });
    }
    , newDocument: function(typeId, culture, name) {
        this.builder.clearDocument();
        if (eBook.Interface.currentFile.get('Closed')) return;
        var doc = {
            id: eBook.NewGuid()
            , fid: eBook.Interface.currentFile.get('Id')
            , n: name
            , dtid: typeId
            , c: culture
            , bcd: []
            , df: []
        };
        this.documentId = doc.id;
        this.on('documentTypeLoaded', function() { this.builder.loadDocument(doc); }, this, { scope: this, single: true });
        this.loadDocumentTypeById(typeId, culture);
    }
    , loadDocumentTypeById: function(typeId, culture) {
        //  this.layout.setActiveItem(1);
        this.documentTypeId = typeId;
        this.documentCulture = culture;
        this.getEl().mask(eBook.Document.Window_Loading, 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.document + 'GetDocumentType'
            , method: 'POST'
            , params: Ext.encode({
                cddc: {
                    FileId: eBook.Interface.currentFile.get('Id')
                        , DocumentTypeId: typeId
                        , Culture: culture
                }
            })
            , callback: this.documentTypeLoaded
            , scope: this
        });

    }
    , onBeforeClose: function() {
        this.builder.clearDocument();
        return true;
    }
    , documentTypeLoaded: function(opts, s, resp) {

        if (s) {
            var dt = Ext.decode(resp.responseText).GetDocumentTypeResult;
            this.builder.show();
            this.builder.loadDocumentType(dt);
            if (!eBook.Interface.currentFile.get('Closed')) {
                this.getTopToolbar().savedocument.enable();
                this.getTopToolbar().printpreview.enable();
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
        this.fireEvent('documentTypeLoaded');
    }
    , documentLoaded: function(opts, s, resp) {
        if (s) {
            var doc = Ext.decode(resp.responseText).GetDocumentResult;
            this.builder.loadDocument.defer(500, this.builder, [doc]);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }

        this.fireEvent('documentLoaded');
        this.getEl().unmask();
    }
    , onLoadDocumentClick: function(lv, rec) {

        // this.getEl().mask('Loading document ' + rec.get('Name'), 'x-mask-loading');
        this.loadMenu.hide(true);

        this.loadDocument(rec);
    }
    , renderMenu: function() {
        this.loadMenu = new eBook.Menu.ListViewMenu({
            handler: this.onLoadDocumentClick,
            reloadOnShow: true,
            hideOnClick: true,
            scope: this,
            listConfig: {
                loadingText: eBook.Document.Window_LoadingMenu,
                hideHeaders: true,
                store: new Ext.data.JsonStore({
                    autoDestroy: true,
                    root: 'GetDocumentsResult',
                    fields: eBook.data.RecordTypes.DocumentList,
                    baseParams: { FileId: eBook.Interface.currentFile.get('Id') },
                    proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                        url: eBook.Service.document + 'GetDocuments'
                             , criteriaParameter: 'cfdc'
                             , method: 'POST'

                    })
                })
            , width: 300
            , multiSelect: false
            , emptyText: eBook.Document.Window_MenuEmpty
            , reserveScrollOffset: false
                //, autoHeight: true
            , columns: [{
                header: 'name',
                width: 1,
                dataIndex: 'Name'
}]
            }
        });
    }
});