/*
eBook.Document.TestTemplate = {
    template: "<div class=\"ReportBody\" xmlns:eBook=\"eBook\"><span style=\"\">&#32;</span><br /><br /><table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%;\"><tr><td style=\"text-align:left;font-size:12pt;\"><div class=\"eBook-p-tag\" style=\"font-size:12pt;\">CALA CAPITAL TEST<br />Rue Edith Cavell 128<br />1180 BRUXELLES <br /></div>&#32;\r\n    </td><td style=\"text-align:left;font-size:12pt;\"><div class=\"eBook-p-tag\" style=\"font-size:12pt;\"><input id=\"b7dfafdf-8d8f-b880-f8b0-d5729d1a20d3\" class=\"eBook-documents-field\" eBook:datatype=\"date\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:id=\"8d214632-9e88-fb5a-ddd9-879d8aec82ce\" eBook:parentid=\"f9f25fc6-2383-3961-6453-459e970f0ef7\" eBook:keyid=\"74e02eef-e380-497c-ae4c-c009fdb1ef5f\" eBook:displayName=\"Date de rapport de compilation\" eBook:value=\"\" value=\"\" eBook:store=\"metadata_file\" /></div>&#32;\r\n    </td></tr><tr><td style=\"text-align:left;font-size:13pt;font-weight:bold;\" colspan=\"2\"><br /><br /><div class=\"eBook-p-tag\" style=\"font-size:13pt;font-weight:bold;\">Management letter</div>&#32;\r\n    </td></tr></table><span style=\"\">&#32;</span><br /><br /><span style=\"\">&#32;</span><br /><br /><span style=\"\">&#32;</span><br /><br /><div class=\"eBook-p-tag\" style=\"\"> <input id=\"2434b40f-0b56-67a5-f737-957c6b433a26\" class=\"eBook-documents-field\" eBook:datatype=\"shorttext\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:displayName=\"Titre Management Letter\" eBook:id=\"25fe6ed8-f10c-7472-966b-01e69af2ffea\" eBook:parentid=\"00000000-0000-0000-0000-000000000000\" eBook:keyid=\"3b0b7dd5-4f51-48a0-90d0-6b062a65b3db\" eBook:value=\"Meneer Ward\" value=\"Meneer Ward\" eBook:store=\"metadata_client\" />, </div><br /><div class=\"eBook-documents-blocks\" name=\"ManagementBody\" eBook:typeid=\"BFCECF6C-6A24-4810-8DE7-12CAA5537F70,9230E9F7-58EE-463B-AB00-2DFE983685B9\" eBook:viewType=\"alinea\">\r\n       \r\n    </div><br /><div class=\"eBook-p-tag\" style=\"font-size:10pt;\">Veuillez agréer, <input id=\"39fd1032-b38b-08fe-1f5b-8024c390ab80\" class=\"eBook-documents-field\" eBook:datatype=\"shorttext\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:displayName=\"Titre Management Letter\" eBook:id=\"25fe6ed8-f10c-7472-966b-01e69af2ffea\" eBook:parentid=\"00000000-0000-0000-0000-000000000000\" eBook:keyid=\"3b0b7dd5-4f51-48a0-90d0-6b062a65b3db\" eBook:value=\"Meneer Ward\" value=\"Meneer Ward\" eBook:store=\"metadata_client\" />, mes salutations distinguées,</div><br /><br /><br /><div class=\"eBook-p-tag\" style=\"font-size:10pt;\"><input id=\"0e847b00-7b8d-d66f-9c18-6ded12db7fe2\" class=\"eBook-documents-field\" eBook:datatype=\"shorttext\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:displayName=\"Signature Management Letter\" eBook:id=\"895b6f36-6ae7-3112-1222-c1b0e8b2f131\" eBook:parentid=\"00000000-0000-0000-0000-000000000000\" eBook:keyid=\"568bcdc6-aba5-42b0-a69d-b8ef3b27b0d9\" eBook:value=\"Lena Plas\" value=\"Lena Plas\" eBook:store=\"metadata_client\" /><br /><input id=\"4eae079d-5c44-1fcb-9676-a31dd9bcc6c9\" class=\"eBook-documents-field\" eBook:datatype=\"shorttext\" eBook:list=\"00000000-0000-0000-0000-000000000000\" eBook:displayName=\"Fonction sign. Management Letter \" eBook:id=\"5ffb1951-f9f5-4370-8535-2c49aee2c160\" eBook:parentid=\"00000000-0000-0000-0000-000000000000\" eBook:keyid=\"564b3440-b60b-4d0e-9448-666484735b13\" eBook:value=\"Vennoot\" value=\"Vennoot\" eBook:store=\"metadata_client\" /></div></div>"
    , blockTypes: [
            { title: 'Block type 1', id: 'BFCECF6C-6A24-4810-8DE7-12CAA5537F70', items: [{ ti: 'Title 1-1', te: 'text 1-1' }, { ti: 'Title 1-2', te: 'text 1-2' }, { ti: 'Title 1-3', te: 'text 1-3'}] }
            , { title: 'Block type 2', id: '9230E9F7-58EE-463B-AB00-2DFE983685B9', items: [{ ti: 'Title 2-1', te: 'text 2-1' }, { ti: 'Title 2-2', te: 'text 2-2' }, { ti: 'Title 2-3', te: 'text 2-3'}] }
        ]
};*/

eBook.Document.Builder = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            items: [
                    new eBook.Document.Editor({ ref: 'editor' })
                , new Ext.Panel({ ref: 'blocksPanel', region: 'east', layout: 'accordion', items: [{ title: 'temp', html: 'body'}], width: 200 })]
            , layout: 'border'
            , bodyStyle: 'background-color:#ABABAB;'
        });
        eBook.Document.Builder.superclass.initComponent.apply(this, arguments);
    }
    , documentLoaded: false
    , clearDocument: function() {
        if (this.documentLoaded) {
            this.editor.clearAll();
            this.documentLoaded = false;
        }
    }
    , loadDocumentType: function(data) {
        //data.template
        //data.blockTypes
        this.editor.show();
        this.editor.loadTemplate(data.tmp);
        this.blocksPanel.removeAll(true);
        if (data.bts.length > 0) {
            this.blocksPanel.show();
            this.blocksPanel.expand();
            this.loadBlockTypes(data.bts);
        } else {
            this.blocksPanel.collapse();
            this.blocksPanel.hide();
        }
        data = null;

    }
    , loadDocument: function(data) {
        this.refOwner.setTitle(data.n);
        this.activeDocument = {};
        Ext.apply(this.activeDocument, data);
        this.editor.loadDocument(data);
        data = null;
        this.documentLoaded = true;
    }
    , getDocument: function() {
        if (!Ext.isDefined(this.activeDocument)) return {};
        this.activeDocument.bcd = this.editor.getBlockContainers();
        this.activeDocument.df = this.editor.getFields();
        return this.activeDocument;
    }
    , loadBlockTypes: function(blockTypes) {

        for (var i = 0; i < blockTypes.length; i++) {

            this.blocksPanel.add(new eBook.Document.BlocksPanel({
                title: blockTypes[i].n
                    , storeCfg: {
                        autoDestroy: true
                        , fields: eBook.data.RecordTypes.DocumentBlock
                        , data: blockTypes[i].bt
                    }
                    , blockTypeId: blockTypes[i].id
            }));
        }
        this.blocksPanel.doLayout();
        //this.doLayout();
    }
});