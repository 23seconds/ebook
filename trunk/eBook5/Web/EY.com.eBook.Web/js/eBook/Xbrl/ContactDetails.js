


eBook.Xbrl.ContactDetails = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            labelWidth: 150,
            items: [
            //{ xtype: 'Xbrl.PreformattedContacts', ref: 'preformatted', fieldLabel: 'Preformatted contact' }
            //,
                {xtype: 'fod.contacttype', ref: 'contacttype', fieldLabel: eBook.Xbrl.ContactDetails_Contacttype, allowBlank: false }
                , { xtype: 'textfield', ref: 'func', fieldLabel: eBook.Xbrl.ContactDetails_Function, allowBlank: false, width: 250 }
                , { xtype: 'textfield', ref: 'name', fieldLabel: eBook.Xbrl.ContactDetails_Name, allowBlank: false, width: 250 }
                , { xtype: 'textfield', ref: 'firstname', fieldLabel: eBook.Xbrl.ContactDetails_FirstName, allowBlank: false, width: 250 }
                 , { xtype: 'email', ref: 'email', fieldLabel: eBook.Xbrl.ContactDetails_Email, allowBlank: true, width: 250 }

               , { xtype: 'fod.address', ref: 'address', fieldLabel: ' ', labelSeparator: '', title: eBook.Xbrl.ContactDetails_Address, width: 550 }
                 , { xtype: 'fod.phone', ref: 'phone', fieldLabel: ' ', labelSeparator: '', title: eBook.Xbrl.ContactDetails_Phone, width: 550, checkboxToggle: true }
//                 , { xtype: 'fod.phone', ref: 'fax', fieldLabel: ' ', labelSeparator: '', title: 'Fax', width: 550, checkboxToggle: true }

            ]
            , autoScroll: true
            , bodyStyle: 'padding:10px;'
        });
        eBook.Xbrl.ContactDetails.superclass.initComponent.apply(this, arguments);
        this.on('activate', this.onActivate, this);
    }
    , onActivate: function() {
        if (this.rendered) {
            this.phone.clearInvalid();
            
        }
    }
    , isValid: function(prevMark) {
        var t = this.contacttype.isValid(prevMark)
                || this.func.isValid(prevMark)
                || this.name.isValid(prevMark)
                || this.firstname.isValid(prevMark)
                || this.email.isValid(prevMark)
                || this.address.isValid(prevMark)
                || this.phone.isValid(prevMark);
        return this.contacttype.isValid(prevMark)
                && this.func.isValid(prevMark)
                && this.name.isValid(prevMark)
                && this.firstname.isValid(prevMark)
                && this.email.isValid(prevMark)
                && this.address.isValid(prevMark)
                && this.phone.isValid(prevMark);
    }
    , loadContract: function(contract) {
        if (contract) {
            this.contacttype.setValue(contract.ct ? contract.ct : '');
            this.func.setValue(contract.f ? contract.f : '');
            this.name.setValue(contract.n ? contract.n : '');
            this.firstname.setValue(contract.fn ? contract.fn : '');
            this.address.setValue(contract.a ? contract.a : null);
            this.phone.setValue(contract.p ? contract.p : null);
        }
    }
    , getContract: function() {
        return {
            ct: this.contacttype.getContract()
            , f: this.func.getValue()
            , n: this.name.getValue()
            , fn: this.firstname.getValue()
            , a: this.address.getValue()
            , p: this.phone.getValue()
        };
    }
});

Ext.reg('xbrl.contact', eBook.Xbrl.ContactDetails);