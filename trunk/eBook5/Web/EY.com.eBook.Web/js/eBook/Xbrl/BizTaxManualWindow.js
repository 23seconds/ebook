

eBook.Xbrl.BizTaxManualWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        this.data = { fid: eBook.EmptyGuid, validated: false, contact: {}, calculation: { eBook: null, biztax: null }, status: { id: -1, txt: 'Aanmaak' }, version: 0 };
        Ext.apply(this, {
            title: 'BizTax'
            , layout: 'card'
            , activeItem: 0
            , items: [
                    { xtype: 'panel', ref: 'startpanel', plain: true, html: '' }
                    , { xtype: 'Xbrl.Wizard.Manual', ref: 'wizard' }
                    , { xtype: 'xbrl.overview', ref: 'overview'}]
        });
        eBook.Xbrl.BizTaxManualWindow.superclass.initComponent.apply(this, arguments);
    }
    , show: function(manual) {
        this.manual = manual;
        this.culture = manual.File.c;
        
        eBook.Xbrl.BizTaxManualWindow.superclass.show.call(this);
        this.getEl().mask('loading');
        this.loadData()
    }
    , loadData: function() {
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'GetXbrlManual'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: this.manual.Id //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Culture: this.culture
                    , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onLoadDataCallback
                , scope: this
        });
        //  this.onLoadDataCallback.defer(1000, this);
    }
    , onLoadDataCallback: function(opts, success, resp) {
        this.getEl().unmask(true);
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.wizard.setContract(robj.GetXbrlManualResult);
                if (robj.GetXbrlManualResult.status.id == 0) {
                    this.layout.setActiveItem(1);
                    this.layout.activeItem.moveFirst();
                } else {
                    this.data = robj.GetXbrlManualResult;
                    this.layout.setActiveItem(2);
                }
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();

    }
    , saveXbrl: function() {
        this.data = this.wizard.getContract();
        // this.getEl().mask(eBook.Xbrl.BizTaxWindow_Generating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'SaveXbrlManual'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    ManualId: this.manual.Id
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: this.culture
                    , ClientId: eBook.Interface.currentClient.get('Id')
                }
                })
                , callback: Ext.emptyFn
                , scope: this
        });
    }
    , afterWizard: function() {
        this.data = this.wizard.getContract();
        if (!this.data.result) this.data.result = { f: null };
        this.getEl().mask(eBook.Xbrl.BizTaxManualWindow_Generating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'GenerateManual'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    ManualId: this.manual.Id
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: this.culture
                    , ClientId: eBook.Interface.currentClient.get('Id')
                }
                })
                , callback: this.performAfterWizard
                , scope: this
        });
    }
    , performAfterWizard: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.data = robj.GenerateManualResult;
                this.layout.setActiveItem(2);

            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
});

Ext.reg('xbrl.biztax.manual', eBook.Xbrl.BizTaxManualWindow);
