


eBook.Xbrl.PartnerSelect = Ext.extend(Ext.Window, {
    initComponent: function() {
        Ext.apply(this, {
            title: 'Select partner'
            ,modal:true
            , items: [{ xtype: 'TaxPartners', ref: 'partners', allowBlank: false, autoLoad: true}]
            ,tbar:[{
                text: eBook.Xbrl.Overview_SendToBizTax,
                iconCls: 'eBook-biztax-send-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'biztax',
                handler: this.onSendToBizTax,
                scope: this
            }]

        });
        eBook.Xbrl.PartnerSelect.superclass.initComponent.apply(this, arguments);
    }
    , show: function(caller) {
        this.caller = caller;
        eBook.Xbrl.PartnerSelect.superclass.show.call(this);
    }
    , onSendToBizTax: function(btn, e) {
        if (this.partners.isValid()) {
            this.caller.onSendToBizTax(btn, e, this.partners.getValue());
            this.close();
        } else {
            alert("Select a partner. If the required partner is not in the list, check PMT & eBook team(s) prior to declaration.");
        }
    }
});

Ext.reg('XbrlPartnerSelcet', eBook.Xbrl.PartnerSelect);