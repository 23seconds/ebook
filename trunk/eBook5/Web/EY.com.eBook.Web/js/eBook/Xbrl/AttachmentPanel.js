


eBook.Xbrl.AttachmentPanel = Ext.extend(Ext.Panel, {
    contractName: 'attachments'
    , attachments: []
    , defaultInValidMsg: eBook.Xbrl.AttachmentPanel_DefaultInValidMsg //'Volgende bijlagen zijn aangeduid, maar bevatten geen onderdelen:'
    , currentStep: 0
    , noWizardNext: true
    , noWizardPrev: true
    , initializeData: function() {
        if (!this.answers) this.answers = [];

        for (var i = 0; i < this.answers.length; i++) {

            var at = this.answers[i];
            //var j = this.getAnswer(at.id);

            /*
            if (!this.answers[at.idx]) this.answers[at.idx] = at;
            this.answers[at.idx].id = at.id;
            if (!this.answers[at.idx].items) this.answers[at.idx].items = [];
            this.answers[at.idx].name = at.name;
            this.answers[at.idx].automated = at.automated;
            this.answers[at.idx].idx = at.idx;
            if (typeof (this.answers[at.idx].checked) == "undefined") {
            this.answers[at.idx].checked = at.checked;
            }
            */
            for (var j = 0; j < this.answers[at.idx].items.length; j++) {
                switch (this.answers[at.idx].items[j].__type) {
                    case 'PDFDataContract:#EY.com.eBook.API.Contracts.Data':
                        this.answers[at.idx].items[j].iconCls = 'eBook-bundle-tree-pdf';
                        break;
                    case "WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data":
                        this.answers[at.idx].items[j].iconCls = 'eBook-bundle-tree-worksheet';
                        break;
                    case "StatementsDataContract:#EY.com.eBook.API.Contracts.Data":
                        this.answers[at.idx].items[j].iconCls = 'eBook-bundle-tree-statement';
                        break;
                    case "DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data":
                        this.answers[at.idx].items[j].iconCls = 'eBook-bundle-tree-document';
                        break;
                }
            }
            //this.answers[at.id].items.push({ id: 'test', name: 'test pdf' });
        }
    }
    , initComponent: function() {

        this.initializeData();

        this.at_tpl = new Ext.XTemplate('<tpl for="."><tpl for="answers">'
                                    , '<div id="xbrlattach-{id}" class="eBook-Xbr-Attachment" idx="{#}">'
                                    , '<input type="checkbox" id="check{id}"<tpl if="required==1 || automated"> disabled="true"</tpl> value="yes"<tpl if="checked"> checked="true"</tpl>/>&nbsp;<span class="eBook-Xbrl-Attachment-Title"><b>{name}</b></span>'
                                    , '<tpl if="!this.isNullOrEmpty(subText)">'
                                        , '<div class="eBook-Xbrl-SubText">{subText}</div>'
                                    , '</tpl>'
                                    , '<tpl if="automated">'
                                        , '<div class="eBook-Xbrl-Automated">{eBook.Xbrl.AttachmentPanel_automated}</div>'
                                    , '</tpl>'
                                    , '<tpl if="!automated">'
                                        , '<div class="eBook-Xbrl-Attachment-Items"<tpl if="!checked"> style="display:none;"</tpl>>'
                                        , '<div class="eBook-Xbrl-Attachment-Add">' + eBook.Xbrl.AttachmentPanel_Add + '</div>'
                                        , '<div class="eBook-Xbrl-Attachment-List">'
        //, '<tpl if="parent.answers[idx]">'
                                        , '<tpl for="items"><div class="eBook-Xbrl-Attachment-item"><div class="eBook-Xbrl-Attachment-item-text {iconCls}" idx="{#}" itemId="{id}">{title}</div> <div class="eBook-Xbrl-Attachment-Remove">' + eBook.Xbrl.AttachmentPanel_Delete + '</div><div class="x-clear"></div></div></tpl>'
        //, '</tpl>'
                                        , '</div>'
                                        , '</div>'
                                     , '</tpl>'
                                     , '</div>'
                                    , '</tpl></tpl>'
                                    , {
                                        compiled: true
                                        , isNullOrEmpty: function(val) { return (val == '' || !val); }
                                        , getItems: function(idx, par) {
                                            if (par.answers[idx]) {
                                                if (par.answers[idx].items) return par.answers[idx].items;
                                            }
                                            return [];
                                        }
                                        , getAnswer: function(idx, par) {
                                            return par.answers[idx];
                                        }
                                    });

        this.at_tpl.compile();
        Ext.apply(this, {
            html: this.at_tpl.apply({ attachments: this.attachments, answers: this.answers })
            , autoScroll: true
        });
        eBook.Xbrl.AttachmentPanel.superclass.initComponent.apply(this, arguments);
        //this.on('click', this.onItemClick, this);
    }
    , afterRender: function() {
        eBook.Xbrl.AttachmentPanel.superclass.afterRender.call(this);
        this.initItemEvents();
    }
    , isValid: function(prevMark) {
        this.invalidMsg = this.defaultInValidMsg;
        this.invalidMsg += '<ul>';

        var valid = true;
        for (var i = 0; i < this.answers.length; i++) {
            var as = this.answers[i];
            if (as.checked && !as.automated) {
                if (!(as.items != null && as.items.length > 0)) {
                    valid = false;
                    this.invalidMsg += '<li>' + as.name + '</li>';
                }
            }
        }
        this.invalidMsg += '</ul>';
        return valid;
    }
    , purgeItemEvents: function() {
        if (this.el) {
            var its = this.el.select(".eBook-Xbr-Attachment");
            if (its) {
                Ext.EventManager.removeAll(its);
            }
        }
    }
    , initItemEvents: function() {

        var its = this.el.select(".eBook-Xbr-Attachment");
        its.on('click', this.onItemClick, this);
    }
    , onItemClick: function(e, t, o) {
        var it = e.getTarget('.eBook-Xbr-Attachment')
            , chk = e.getTarget('input')
            , titem = e.getTarget('.eBook-Xbrl-Attachment-Title')
            , pitem = e.getTarget('.eBook-Xbrl-Attachment-item')
            , ritem = e.getTarget('.eBook-Xbrl-Attachment-Remove')
            , ditem = e.getTarget('.eBook-Xbrl-Attachment-Add')
            , atitems
            , idx
            , pidx
            , id;

        if (it) {
            it = Ext.get(it);
            var idx = parseInt(it.getAttribute("idx")) - 1;
            atitems = it.child(".eBook-Xbrl-Attachment-Items");
            id = it.id.replace('xbrlattach-', '');
            if (!this.answers[idx]) this.answers[idx] = { answer: "yes" };
            if (chk) {
                this.answers[idx].checked = chk.checked;
                if (chk.checked) {
                    atitems.setStyle('display', '');
                } else {
                    atitems.setStyle('display', 'none');
                }
            } else if (titem) {
                chk = Ext.get(titem).parent().child('input').dom;
                if (!chk.disabled) {
                    chk.checked = !chk.checked;
                    this.answers[idx].checked = chk.checked;
                    if (chk.checked) {
                        atitems.setStyle('display', '');
                    } else {
                        atitems.setStyle('display', 'none');
                    }
                }

            } else if (pitem) {

                pitem = Ext.get(pitem);
                var ul = pitem.parent('ul');
                var pidx = parseInt(pitem.getAttribute("idx")) - 1;
                if (ritem) {
                    pitem.remove();
                    this.answers[idx].items.splice(pidx, 1);
                    var nds = ul.select('li');
                    nds.each(function(el, c, idx) {
                        el.dom.setAttribute('idx', idx);
                    });
                }
            } else if (ditem) {
                this.onAddItemClick(ditem, id, idx);
            } else {
                // console.log(this.answers);
            }
        }

    }
    , onAddItemClick: function(ditem, id, idx) {
        this.addingIdx = idx;
        var t;
        if (this.manual) {
            t = new eBook.Client.ManualResourcesWindow({
                culture: this.ownerCt.ownerCt.manual.File.c
                    , manualId: this.ownerCt.ownerCt.manual.Id
            });
        } else {
            t = new eBook.Bundle.ResourcesWindow({});
        }

        t.show(this);
    }
    , resourcesPicked: function(dta) {
        var dta = this.answers[this.addingIdx].items.concat(dta);
        this.answers[this.addingIdx].items = this.unique(dta);
        this.reload();
    }
    , unique: function(arr) {
        var ret = [],
                collect = {};

        Ext.each(arr, function(v) {
            if (!collect[v.id]) {
                ret.push(v);
            }
            collect[v.id] = true;
        });
        return ret;
    }
    , onValidate: function() {
        // validation check ?
    }
    , getContract: function() {
        return this.answers;
    }
    , reload: function() {
        this.initializeData();
        if (this.el) {
            //var l = Ext.get(this.el);
            this.purgeItemEvents();
            this.update("");
            this.update(this.at_tpl.apply({ attachments: this.attachments, answers: this.answers }));
            this.initItemEvents();
        }
    }
    , loadContract: function(contract) {
        if (contract) {
            this.answers = contract;
        } else {
            //
        }
        this.reload();
    }
});
        
Ext.reg('xbrl.attachments', eBook.Xbrl.AttachmentPanel);