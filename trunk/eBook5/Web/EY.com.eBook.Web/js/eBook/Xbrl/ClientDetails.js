

eBook.Xbrl.ClientDetails = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            labelWidth: 150,
            items: [
            //{ xtype: 'Xbrl.PreformattedContacts', ref: 'preformatted', fieldLabel: 'Preformatted contact' }
            //,
                {xtype: 'textfield', ref: 'name', fieldLabel: eBook.Xbrl.ClientDetails_Name, allowBlank: false, width: 250 }
                , { xtype: 'legaltype', ref: 'legaltype', fieldLabel: eBook.Xbrl.ClientDetails_Legaltype, allowBlank: false }
                , { xtype: 'textfield', ref: 'enterprisenr', fieldLabel: eBook.Xbrl.ClientDetails_EnterpriseNr, allowBlank: false, width: 250 }
                , { xtype: 'fod.address', ref: 'address', fieldLabel: ' ', labelSeparator: '', title:eBook.Xbrl.ClientDetails_Address, width: 550 }
                , { xtype: 'bankdetails', ref: 'bank', fieldLabel: ' ', labelSeparator: '', title: eBook.Xbrl.ClientDetails_BankInfo, width: 550 }
            ]
            , autoScroll: true
            , bodyStyle: 'padding:10px;'
        });
        eBook.Xbrl.ClientDetails.superclass.initComponent.apply(this, arguments);
    }
    , isValid: function(prevMark) {
        var t = this.name.isValid(prevMark)
                || this.legaltype.isValid(prevMark)
                || this.enterprisenr.isValid(prevMark)
                || this.address.isValid(prevMark)
                || this.bank.isValid(prevMark);

        return this.name.isValid(prevMark)
                && this.legaltype.isValid(prevMark)
                && this.enterprisenr.isValid(prevMark)
                && this.bank.isValid(prevMark)
                && this.address.isValid(prevMark);
    }
    , loadContract: function(contract) {
        if (contract) {
            this.name.setValue(contract.n ? contract.n : '');
            this.legaltype.setValue(contract.lti ? contract.lti : '');
            this.enterprisenr.setValue(contract.enr ? contract.enr : '');
            this.address.setValue(contract.foda ? contract.foda : null);
            this.bank.setValue(contract.b ? contract.b : null);
        }
    }
    , getContract: function() {
        return {
            n: this.name.getValue()
            , lti: this.legaltype.getValue()
            , enr: this.enterprisenr.getValue()
            , foda: this.address.getValue()
            , b: this.bank.getValue()
        };
    }
});

Ext.reg('xbrl.client', eBook.Xbrl.ClientDetails);