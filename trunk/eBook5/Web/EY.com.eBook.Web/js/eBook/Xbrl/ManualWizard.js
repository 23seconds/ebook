

eBook.Xbrl.ManualWizard = Ext.extend(Ext.Panel, {
    data: {}
    , xbrlConfig: {}
    , initComponent: function() {
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , autoScroll: true
            , layoutconfig: { layoutOnCardChange: true, deferredRender: true }
            // , items: [{ xtype: 'Xbrl.ValidationCheckPanel'}]
           , items: [
            // { xtype: 'Xbrl.ValidationCheckPanel', idx: 0, title: ' '} // Check file on errors and warnings. (when errors, no moving forward 'fix it' links
                {xtype: 'xbrl.client', idx: 0, title: eBook.Xbrl.ClientDetails_Title, contractName: 'client' }
                , { xtype: 'xbrl.contact', idx: 1, title: eBook.Xbrl.ContactDetails_Title, contractName: 'contact' }
                 , { xtype: 'xbrl.attachments',manual:true, title: eBook.Xbrl.AttachmentPanel_FichesTitle, idx: 2, contractName: 'fiches', attachments: []} // per fiche pdf aanduiden (behalve voor 275C & 204.3)
                 , { xtype: 'xbrl.attachments', manual: true, title: eBook.Xbrl.Overview_AttachmentsTitle, idx: 3, contractName: 'divers', attachments: [] }
                 , { xtype: 'xbrl.attachments', manual: true, title: eBook.Xbrl.AttachmentPanel_InfoTitle, idx: 4, contractName: 'info', attachments: eBook.Xbrl.InformativeAttachments }
                ]
            , bbar: [
                { xtype: 'button', ref: 'prev', hidden: true, text: eBook.Xbrl.ManualWizard_Previous, handler: this.onPrevious, scope: this, iconCls: 'eBook-back-24',
                    scale: 'medium',
                    iconAlign: 'top'
                }
                , '->'
                , { xtype: 'button', text: eBook.Xbrl.ManualWizard_Next, ref: 'nxt', handler: this.onNext, scope: this, iconCls: 'eBook-next-24',
                    scale: 'medium',
                    iconAlign: 'top'}]
        });
        eBook.Xbrl.ManualWizard.superclass.initComponent.apply(this, arguments);
    }
    , hideAllButtons: function() {
        this.getBottomToolbar().prev.hide();
        this.getBottomToolbar().nxt.hide();
    }
    , showNext: function() {
        this.getBottomToolbar().nxt.show();
    }
    , moveFirst: function() {
        this.layout.setActiveItem(0);
        this.hideAllButtons();
        this.getBottomToolbar().prev.hide();
        this.getBottomToolbar().nxt.hide();
        //var ai = this.items.items[0];
        if (this.layout.activeItem.performValidation) {
            this.layout.activeItem.performValidation();
        } else {
            ai = this.layout.activeItem
            if (this.data[ai.contractName]) { 
                ai.loadContract(this.data[ai.contractName]);
            }
            
            this.getBottomToolbar().nxt.show();
        }
        this.layout.activeItem.doLayout();
        // this.doLayout();
    }
    , onNext: function(e) {
        this.getBottomToolbar().nxt.show();

        var ai = this.layout.activeItem;
        if (ai.getContract) {
            var c = ai.getContract();
            this.data[ai.contractName] = c;
        }
        if (!ai.isValid()) {
            if (ai.invalidMsg) {
                eBook.Interface.showError(ai.invalidMsg, ai.title);
            } else {
                eBook.Interface.showError("Niet alle velden zijn correct ingevuld", ai.title);
            }
            return;
        }
        this.refOwner.saveXbrl();
        var idx = ai.idx;
        if ((idx + 1) > 1) this.getBottomToolbar().prev.show();
        if (idx + 1 < this.items.items.length) {
            ai = this.items.items[idx + 1];
            ai.loadContract(this.data[ai.contractName]);
            this.layout.setActiveItem(idx + 1);
            this.layout.activeItem.doLayout();
            /*if (idx + 1 == 1) {
            var c = { "328K": { checked: true, items: [{ id: 'mypdf', name: 'PDFKE'}]} };
            this.layout.activeItem.loadContract(c);
            }*/
        } else if (idx + 1 == this.items.items.length) {
            this.refOwner.afterWizard();
        }
    }
    , onPrevious: function(e) {
        var idx = this.layout.activeItem.idx;
        if (idx - 1 >= 0) {
            this.layout.setActiveItem(idx - 1);
            if (idx - 1 == 0) this.getBottomToolbar().prev.hide();
        }
        this.layout.activeItem.doLayout();
    }
    , getContract: function() {
        return this.data;
    }
    , setContract: function(data) {
        this.data = data;
    }
    , onPrintClick: function() {
        // create a pdf bundle containing all selected info
    }
    , onSendToFOD: function() {
        // manager/partner only, send XBRL to central admin
    }
    , onReCreate: function() {
        // traverse wizard after an xbrl was already build
    }
    , loadXbrlConfig: function() {
        // load the configuration for the xbrl of the assessmentyear to which this file belongs.
    }
    , loadXbrlData: function() {
        // load existing or non-existing Xbrl Data
        // if existing, start in overview
        // if non-existing, start wizard.
    }
});

Ext.reg('Xbrl.Wizard.Manual', eBook.Xbrl.ManualWizard);