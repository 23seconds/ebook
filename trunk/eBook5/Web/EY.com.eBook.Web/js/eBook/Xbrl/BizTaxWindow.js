

eBook.Xbrl.BizTaxWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        this.data = { fid: eBook.Interface.currentFile.get('Id'), validated: false, contact: {}, calculation: { eBook: null, biztax: null }, status: { id: -1, txt: 'Aanmaak' }, version: 0 };
        Ext.apply(this, {
            title: 'BizTax'
            , layout: 'card'
            , activeItem: 0
            , items: [
                    { xtype: 'panel', ref: 'startpanel', plain: true, html: '' }
                    , { xtype: 'Xbrl.Wizard', ref: 'wizard' }
                    , { xtype: 'xbrl.overview', ref: 'overview'}]
        });
        eBook.Xbrl.BizTaxWindow.superclass.initComponent.apply(this, arguments);
    }
    , show: function() {
        eBook.Xbrl.BizTaxWindow.superclass.show.call(this);
        this.getEl().mask('loading');
        this.loadData()
    }
    , loadData: function() {
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'GetXbrl'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
                            , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onLoadDataCallback
                , scope: this
        });
        //  this.onLoadDataCallback.defer(1000, this);
    }
    , onLoadDataCallback: function(opts, success, resp) {
        this.getEl().unmask(true);
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.wizard.setContract(robj.GetXbrlResult);
                if (robj.GetXbrlResult.status.id == 0) {
                    this.layout.setActiveItem(1);
                    this.layout.activeItem.moveFirst();
                } else {
                    this.data = robj.GetXbrlResult;
                    this.layout.setActiveItem(2);
                }
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();

    }
    , saveXbrl: function() {
        this.data = this.wizard.getContract();
       // this.getEl().mask(eBook.Xbrl.BizTaxWindow_Generating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'SaveXbrl'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    FileId: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: eBook.Interface.currentFile.get('Culture')
                    , ClientId: eBook.Interface.currentClient.get('Id')
                }
                })
                , callback: Ext.emptyFn
                , scope: this
        });
    }
    , afterWizard: function() {
        this.data = this.wizard.getContract();
        if (!this.data.result) this.data.result = {f:null};
        this.getEl().mask(eBook.Xbrl.BizTaxWindow_Generating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'Generate'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    FileId: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: eBook.Interface.currentFile.get('Culture')
                    , ClientId: eBook.Interface.currentClient.get('Id')
                }
                })
                , callback: this.performAfterWizard
                , scope: this
        });
    }
    , performAfterWizard: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.data = robj.GenerateResult;
                this.layout.setActiveItem(2);

            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
});

Ext.reg('xbrl.biztax', eBook.Xbrl.BizTaxWindow);
