
// checks if the file has errors prior to start wizard/overview.
//Dossier wordt gevalideerd


eBook.Xbrl.ValidationCheckPanel = Ext.extend(Ext.Panel, {
    wizardPanel: {
        navigationbar: false
    }
    , data: { Message: 'Dossier wordt gevalideerd', state: 'Valid', loading: -1 }
    , initComponent: function() {
        this.bTpl = new Ext.XTemplate('<tpl for=".">'
                                      , '<div class="eBook-Xbrl-Validation-block">'
                                        , '<tpl if="loading==1">'
                                            , '<div class="eBook-Xbrl-Validation eBook-Xbrl-Validating">' + eBook.Xbrl.ValidationCheckPanel_Validating + '</div>'
                                        , '</tpl>'
                                        , '<tpl if="loading==0">'
                                            , '<div class="eBook-Xbrl-Validation eBook-Xbrl-Validation-Result eBook-Xbrl-Validation-{state}">'
                                            , '{Message}'
                                            , '</div>'
                                        , '</tpl>'
                                      , '</div>'
                                      , '</tpl>', { compiled: true });
        Ext.apply(this, {

            html: ''

        });
        eBook.Xbrl.ValidationCheckPanel.superclass.initComponent.apply(this, arguments);
        this.on('afterlayout', this.onActivate, this);
    }
    , isValid: function() {
        return true;
    }
    , reload: function() {
        if (this.el) {
            this.update("");
            this.update(this.bTpl.apply(this.data));
            this.ownerCt.doLayout();
        }
    }
    , afterRender: function() {
        eBook.Xbrl.ValidationCheckPanel.superclass.afterRender.apply(this, arguments);
        this.reload();
    }
    , onActivate: function(v) {
        if (this.rendered) {

        }

    }
    , performValidation: function() {
        //Ext.Ajax.request({});
        // or from message store.
        this.data.loading = 1;
        this.reload();
        Ext.Ajax.request({
            url: eBook.Service.bizTax + 'Validate'
                    , method: 'POST'
                    , params: Ext.encode({ cicdc: {
                        Id: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                    }
                    })
                    , success: this.onPerformValidationSucces
                    , failure: this.onPerformValidationFailure
                    , scope: this
        });

    }
    , onPerformValidationSucces: function(resp, opts) {
        // validation success image
        // show warnings if existing.
        // move next
        var robj = Ext.decode(resp.responseText);
        robj = robj.ValidateResult;
        this.data.loading = 0;
        this.data.Message = robj.Message;
        this.data.Validated = robj.Validated;
        this.data.Errors = robj.Errors;
        this.data.Warnings = robj.Warnings;
        this.data.WorksheetsClosed = robj.WorksheetsClosed;
        this.ownerCt.showNext();
        switch (robj.Validated) {
            case -1:
                this.ownerCt.hideAllButtons();
                this.data.state = 'Errors';
                break;
            case 0:
                this.data.state = 'Warnings';
                break;
            case 1:
                this.data.state = 'Valid';
                this.ownerCt.onNext.defer(1000, this.ownerCt);
        }
        this.reload();
    }
    , onPerformValidationFailure: function(resp, opts) {
        // validation failed image
        // + message
        alert("failed");
    }
});

Ext.reg('Xbrl.ValidationCheckPanel', eBook.Xbrl.ValidationCheckPanel);