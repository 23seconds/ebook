eBook.Xbrl.Overview_ResultTitle = 'Resultaat';
eBook.Xbrl.Overview = Ext.extend(Ext.Panel, {
    data: {}
    , initComponent: function() {
        this.dtTpl = new Ext.XTemplate('<tpl for=".">'
                                            , '<div class="eBook-Xbrl-Overview">'

                                                    , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Status eBook-Xbrl-Overview-Status-Color-{[values.status.id]}">'
                                                        , '<tpl for="status">'
                                                            , ' ' + eBook.Xbrl.Overview_Status + ': <span class="eBook-Xbrl-Overview-StatusText">{txt}</span>'
                                                            , ' <br/>' + eBook.Xbrl.Overview_Version + ': <span class="eBook-Xbrl-Overview-Version">{parent.version}</span>'
                                                            , '<br/><input class="eBook-Xbrl-Overview-partner" type="checkbox" value="1"<tpl if="parent.validated"> checked="true"</tpl><tpl if="id==2"> disabled="true"</tpl>/> ' + eBook.Xbrl.Overview_ValidatedPartner
                                                        , '</tpl>'
                                                        , '<div class="eBook-Xbrl-Overview-History-Title">' + eBook.Xbrl.Overview_History + '</div>'
                                                        , '<div class="eBook-Xbrl-Overview-History" style="display:none"> '
                                                        , '<tpl for="history">'
                                                            , '<div class="eBook-Xbrl-Overview-History-item">'
                                                            , '{[fm.date(Ext.data.Types.WCFDATE.convert(values.dte),\'d/m/Y G:i:s\')]} - {atx} - {pfn}'
                                                            , '</div>'
                                                        , '</tpl>'
                                                        , '</div>'
                                                    , '</div>'
                                                    , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Result">'
                                                            , ' <b><u>' + eBook.Xbrl.Overview_ResultTitle + '</u></b><br/><br/>'
                                                            , '<tpl if="this.exists(values,\'result\')">'
                                                            , '<tpl for="result">'
                                                            , '<tpl if="this.exists(values,\'f\') && f!=null">'
                                                            , '<tpl for="f">'
                                                                , '<a href="{[this.getViewPath(values)]}" target="_blank">{file}</a><br/>'
                                                            , '</tpl>'
                                                            , '</tpl>'
                                                            , '</tpl>'
                                                            , '</tpl>'
                                                    , '</div>'

                                                 , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Client">'
                                                    , ' <b><u>' + eBook.Xbrl.Overview_ClientTitle + '</u></b>'
                                                    , '<tpl if="this.exists(values,\'client\')">'
                                                        , '<tpl for="client">'
                                                            , '<br/><b>{n}</b>&nbsp;&nbsp;&nbsp;{lti}'
                                                            , '<br/>' + eBook.Xbrl.Overview_EnterpriseNr + ': {enr}'
                                                            , '<tpl if="this.exists(values, \'foda\') && foda!=null"><tpl for="foda">'
                                                                , '<tpl if="this.exists(values, \'at\') && at!=null"><br/>{values.at.d}</tpl>'
                                                                , '<div class="eBook-Xbrl-Overview-Contact-Address">'
                                                                    , '{s} {n} <tpl if="this.exists(values, \'b\') && b!=\'\'">&nbsp;&nbsp;&nbsp;' + eBook.Xbrl.Overview_Box + ' {b}</tpl>'
                                                                    , '<br/><tpl if="this.exists(values, \'z\') && z!=null">{values.z.d} </tpl>{ci} <tpl if="this.exists(values, \'co\') && z!=null"><br/>{values.co.d} </tpl>'
                                                                , '</div>'
                                                            , '</tpl></tpl>'
                                                        , '</tpl>'
                                                    , '</tpl>'
                                                 , '</div>'
                                                , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Contact">'
                                                    , '<b><u>' + eBook.Xbrl.Overview_ContactTitle + '</u></b><br/>'
                                                    , '<tpl if="this.exists(values, \'contact\')">'
                                                        , '<tpl for="contact">'
                                                            , '<tpl if="this.exists(values, \'ct\') && ct!=null"><br/>{values.ct.d}:</tpl>'
                                                            , '<div class="eBook-Xbrl-Overview-Contact-Info"><b>{n}, {fn}</b>'
                                                                , '<tpl if="this.exists(values, \'f\') && f!=null"><br/><i>{f}</i></tpl>'

                                                                , '<tpl if="this.exists(values, \'a\') && a!=null"><tpl for="a">'
                                                                , '<tpl if="this.exists(values, \'at\') && at!=null"><br/>{values.at.d}</tpl>'
                                                                    , '<div class="eBook-Xbrl-Overview-Contact-Address">'
                                                                        , '{s} {n} <tpl if="this.exists(values, \'b\') && b!=\'\'">&nbsp;&nbsp;&nbsp;' + eBook.Xbrl.Overview_Box + ' {b}</tpl>'
                                                                        , '<br/><tpl if="this.exists(values, \'z\') && z!=null">{values.z.d} </tpl>{ci} <tpl if="this.exists(values, \'co\') && z!=null"><br/>{values.co.d} </tpl>'
                                                                    , '</div>'
                                                                , '</tpl></tpl>'
                                                                , '<tpl if="this.exists(values, \'p\') && p.act">'
                                                                    , '<tpl for="p"><br/>Tel:{cc} {z} {l} <tpl if="this.exists(values, \'e\' && e!=\'\')">' + eBook.Xbrl.Overview_Extension + ':{e}</tpl></tpl>'
                                                                , '</tpl>'
                                                            , '</div>'
                                                        , '</tpl>'
                                                    , '</tpl>'
                                                , '</div>'
                                                , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Calculation">'
                                                    , '<tpl if="this.exists(values, \'calculation\')"><tpl for="calculation">'
                                                        , '<b><u>' + eBook.Xbrl.Overview_CalculationTitle + '</u></b>'
                                                        , ' <br/>' + eBook.Xbrl.Overview_eBookCalculation + ':&nbsp; <span class="eBook-XbrlOverview-Calculated">{eBook}</span>'
                                                        , '<br/>' + eBook.Xbrl.Overview_BizTaxCalculation + ': <span class="eBook-XbrlOverview-Calculated"><tpl if="this.exists(values,\'biztax\') && biztax!=null">{biztax}</tpl><tpl if="!(this.exists(values,\'biztax\') && biztax!=null)">........</tpl></span>'
                                                     , '</tpl></tpl>'
                                                , '</div>'
                                                , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Fiches">'
                                                    , '<b><u>' + eBook.Xbrl.Overview_AutomatedFormsTitle + '</u></b>'
                                                    , '<tpl if="this.exists(values, \'fiches\')">'
                                                        , '<tpl for="fiches">'
                                                            , '<tpl if="checked && automated"><div class="eBook-Xbrl-Divers-item">{name}'
        //, '<tpl for="items"><div class="eBook-Xbrl-Overview-resource {iconCls}">{title}</div></tpl>'
                                                            , '</div></tpl>'
                                                    , '</tpl></tpl>'
                                                , '</div>'
                                                , '<div class="eBook-Xbrl-Overview-Block eBook-Xbrl-Overview-Divers">'
                                                    , '<b><u>' + eBook.Xbrl.Overview_AttachmentsTitle + ' </u></b>'
                                                    , '<tpl if="this.exists(values, \'divers\')">'
                                                        , '<tpl for="divers">'
                                                        , '<tpl if="checked"><div class="eBook-Xbrl-Overview-Divers-item">{name}'
                                                        , '<tpl for="items"><div class="eBook-Xbrl-Overview-resource {iconCls}">{title}</div></tpl>'
                                                        , '</div></tpl>'
                                                    , '</tpl>'
                                                        , '<div class="eBook-Xbrl-Overview-Divers-item eBook-Xbrl-Overview-FichesOther">'
                                                        , eBook.Xbrl.Overview_FichesAttachmentsTitle
                                                        , '<tpl if="this.exists(values, \'fiches\')">'
                                                        , '<tpl for="fiches">'
                                                            , '<tpl if="checked && !automated"><div class="eBook-Xbrl-Fiches-other">{name}'
                                                            , '<tpl for="items"><div class="eBook-Xbrl-Overview-resource {iconCls}">{title}</div></tpl>'
                                                            , '</div></tpl>'
                                                        , '</tpl>'
                                                        , '</tpl>'
                                                        , '</div>'
                                                     , '</tpl>'
                                                , '</div>'
                                            , '</div>'
                                        , '</tpl>'
                                        , {
                                            getViewPath: function(vls) {
                                                console.log('test' + vls);
                                                return 'viewFile.aspx?' + Ext.urlEncode(vls);
                                            }
                                        }
                                        );
        this.dtTpl.compile();
        this.data = this.ownerCt.data;
        if (!this.data.result) this.data.result = { f: null };
        Ext.apply(this, {
            html: this.dtTpl.apply(this.ownerCt.data)
            , autoScroll: true
            , tbar: [{
                text: eBook.Xbrl.Overview_Wizard,
                iconCls: 'eBook-wizard-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'wizard',
                handler: this.onWizard,
                scope: this
            }, {
                text: eBook.Xbrl.Overview_Bundle,
                iconCls: 'eBook-biztax-bundle-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'addpdf',
                handler: this.onBundle,
                scope: this
            }, {
                text: eBook.Xbrl.Overview_SendToBizTax,
                iconCls: 'eBook-biztax-send-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'biztax',
                handler: this.onSendToBizTax,
                scope: this
}]
            });
            eBook.Xbrl.Overview.superclass.initComponent.apply(this, arguments);
            this.on('activate', this.onActivate, this);
        }
    , onWizard: function() {
        this.ownerCt.layout.setActiveItem(1);
        this.ownerCt.doLayout();
        this.ownerCt.layout.activeItem.moveFirst.defer(100, this.ownerCt.layout.activeItem);
    }

    , onSendToBizTax: function(btn, e, partner) {

        if (!eBook.User.isActiveClientRolesFullAccess()) {
            eBook.Interface.showError("Only manager or above can send to biztax", this.title);
            return;
        }
        if (this.data.status.id < 2 && this.data.status.id > -1) {
            if (eBook.User.department == 'TAX' && !partner) {
                var wn = new eBook.Xbrl.PartnerSelect({ width: 340, height: 200 });
                wn.show(this);
                return;
            } else {
                this.getEl().mask(eBook.Xbrl.Overview_GeneratingBundle, 'x-mask-loading');
                if (this.ownerCt.manual) {

                    Ext.Ajax.request({
                        url: eBook.Service.bizTax + 'SendToProcessManual'
                    , method: 'POST'
                    , params: Ext.encode({ cxdc: {
                        ManualId: this.ownerCt.manual.Id
                        , Xbrl: this.data
                        , Person: eBook.User.getActivePersonDataContract()
                        , Culture: this.ownerCt.manual.File.c
                        , Partner: partner
                    }
                    })
                    , callback: this.onSendToBizTaxResponse
                    , scope: this
                    });
                } else {
                    Ext.Ajax.request({
                        url: eBook.Service.bizTax + 'SendToProcess'
                        , method: 'POST'
                        , params: Ext.encode({ cxdc: {
                            FileId: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                            , Xbrl: this.data
                            , Person: eBook.User.getActivePersonDataContract()
                            , Culture: eBook.Interface.Culture
                            , Partner: partner
                        }
                        })
                        , callback: this.onSendToBizTaxResponse
                        , scope: this
                    });
                }
            }
        } else {
            alert("Can't declare. Current status of declaration [" + this.data.status.txt + "] does not allow it");
            return;
        }

    }
    , onSendToBizTaxResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                if (robj.SendToProcessResult) {
                    this.ownerCt.data = robj.SendToProcessResult;
                } else {
                    this.ownerCt.data = robj.SendToProcessManualResult;
                }
                if (!this.ownerCt.data.result) this.ownerCt.data.result = { f: null };
                this.reload();
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
    , afterRender: function() {
        eBook.Xbrl.Overview.superclass.afterRender.apply(this, arguments);
        this.setEvents();
        this.handleValidated();
    }
    , clearEvents: function() {
        if (this.chkbox) {
            this.chkbox.purgeAllListeners();
        }
    }
    , setEvents: function() {
        if (this.chkbox) { this.clearEvents(); }
        var el = this.getEl();
        el = Ext.get(el);
        this.chkbox = el.child('.eBook-Xbrl-Overview-partner');
        this.chkbox.on('click', this.onCheckClick, this);

        var histEl = el.child('.eBook-Xbrl-Overview-History-Title');
        histEl.on('click', this.onHistoryClick, this);
        var hist = this.getEl().child('.eBook-Xbrl-Overview-History');
        hist.setVisibilityMode(Ext.Element.DISPLAY);
    }
    , onCheckClick: function() {
        this.onCheckChange.defer(100, this);
    }
    , onHistoryClick: function() {
        var hist = this.getEl().child('.eBook-Xbrl-Overview-History');
        hist = Ext.get(hist);
        if (hist.isDisplayed()) {
            hist.hide();
        } else {
            hist.show();
        }
    }
    , onCheckChange: function() {
        this.data.validated = this.chkbox.dom.checked;
        this.ownerCt.data.validated = this.chkbox.dom.checked;
        this.handleValidated();
    }
    , handleValidated: function() {
        var tb = this.getTopToolbar();

        if (this.data.validated && this.data.status.id == 1) {
            if (eBook.User.isActiveClientRolesFullAccess()) {
                this.chkbox.dom.disabled = false;
                tb.biztax.enable();
            } else {
                this.chkbox.dom.disabled = true;
                tb.biztax.disable();
            }

        } else {
            tb.biztax.disable();
        }
        if (this.data.status.id > 1) {
            this.chkbox.dom.disabled = true;
            tb.wizard.disable();
        } else {
            tb.wizard.enable();
        }

        tb.doLayout();
    }
    , onActivate: function(pnl) {
        this.reload();
    }
    , objectToArray: function(obj) {
        var arr = [];
        Ext.iterate(obj, function(key, val, o) {
            arr[val.idx] = val;
        }, this);
        return arr;
    }
    , transposeData: function(data) {
        return data;
        //        var dta = {};
        //        Ext.apply(dta, data);
        //        var fiches = this.objectToArray(data.fiches);
        //        var divers = this.objectToArray(data.divers);
        //        dta.fiches = fiches;
        //        dta.divers = divers;
        //        return dta;
    }
    , reload: function() {
        this.data = this.transposeData(this.ownerCt.data);
        if (!this.data.result) this.data.result = { f: null };
        if (!this.ownerCt.data.result) this.ownerCt.data.result = { f: null };
        // console.log(this.data);
        if (this.el) {
            // var l = Ext.get(this.el);
            //l.purgeAllListeners();
            this.clearEvents();
            this.update("");
            this.update(this.dtTpl.apply(this.data));
            this.setEvents();
            this.handleValidated();
        }
    }
    , onBundle: function() {
        this.getEl().mask(eBook.Xbrl.Overview_GeneratingBundle, 'x-mask-loading');

        if (this.ownerCt.manual) {
            Ext.Ajax.request({
                url: eBook.Service.bizTax + 'GetManualOverviewBundle'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    ManualId: this.ownerCt.manual.Id //eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: this.ownerCt.manual.File.c
                }
                })
                , callback: this.onBundleResponse
                , scope: this
            });
        } else {
            Ext.Ajax.request({
                url: eBook.Service.bizTax + 'GetOverviewBundle'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    FileId: eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC' //eBook.Interface.currentFile.get('Id')
                    , Xbrl: this.data
                    , Person: eBook.User.getActivePersonDataContract()
                    , Culture: eBook.Interface.currentFile.get('Culture')
                }
                })
                , callback: this.onBundleResponse
                , scope: this
            });
        }
    }
    , onBundleResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                if (robj.GetOverviewBundleResult) {
                    window.open('renderedPdfs/' + robj.GetOverviewBundleResult);
                } else {
                    window.open('renderedPdfs/' + robj.GetManualOverviewBundleResult);
                }
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
    });

    Ext.reg('xbrl.overview', eBook.Xbrl.Overview);