
/*
*/
eBook.Menu.File = Ext.extend(eBook.Menu.menu, {
    getData: function() {
        return {
            IconCls: 'eBook-File-menu-ico'
        , items: [{
            id: 'eBook-File-Meta'
                , IconCls: 'eBook-file-meta-menu-ico'
                , Title: 'Metadata' // eBook.Menu.File_Meta
                , type: eBook.Menu.ACTION
                , items: []
        },{
                    id: 'eBook-File-accountmappings'
                        , Title: eBook.Menu.File_AccountsMapping //eBook.Language.Menu.AccountMeta
                        , IconCls: 'eBook-accountmappings-menu-ico'
                        , type: eBook.Menu.ACTION
                        , items: []
                }
            , {
                id: 'eBook-File-journals'
                , IconCls: 'eBook-journals-menu-ico'
                , Title: eBook.Menu.File_Journals
                , type: eBook.Menu.GROUP
                , items: [{
                    id: 'eBook-File-journal-manual'
                        , Title: eBook.Menu.File_JournalManual
                        , IconCls: 'eBook-adjustments-manual-menu-ico'
                        , type: eBook.Menu.ACTION
                        , items: []
                }, {
                    id: 'eBook-File-journal-auto'
                        , Title: eBook.Menu.File_JournalWorksheets
                        , IconCls: 'eBook-adjustments-auto-menu-ico'
                        , type: eBook.Menu.ACTION
                        , items: []
                }, {
                    id: 'eBook-File-finalTrialBalance'
                        , Title: eBook.Menu.File_FinalTrialBalance
                        , IconCls: 'eBook-final-trialbalance-menu-ico'
                        , type: eBook.Menu.ACTION
                        , items: []
}]
                }
              , {
                  id: 'eBook-File-leadsheets'
                , IconCls: 'eBook-leadsheets-menu-ico'
                , Title: 'Leadsheets'//eBook.Menu.File_LeadSheets
                , type: eBook.Menu.ACTION //eBook.Menu.GROUP
                , items: []

              }
            , {
                id: 'eBook-File-worksheets'
                , IconCls: 'eBook-worksheets-menu-ico'
                , Title: eBook.Menu.File_Worksheets
                , type: eBook.Menu.GROUP
                , items: [{
                    id: 'eBook-File-worksheets-general'
                            , IconCls: 'eBook-worksheet-group-menu-ico'
                            , Title: eBook.Menu.File_WorksheetsAL
                            , type: eBook.Menu.GROUP
                            , items: [{
                                id: 'eBook-File-worksheets-general-list'
                                        , listActions: []
                                        , type: eBook.Menu.LIST
                                        , items: []
}]
                }
                        , {
                            id: 'eBook-File-worksheets-financial'
                            , IconCls: 'eBook-worksheet-group-tax-menu-ico'
                            , Title: eBook.Menu.File_WorksheetsFI
                            , type: eBook.Menu.GROUP
                           , items: [{
                               id: 'eBook-File-worksheets-financial-list'
                                        , listActions: []
                                        , type: eBook.Menu.LIST
                                        , items: []
}]
                        }
                        ]
            }
            , {
                id: 'eBook-File-documents'
                , IconCls: 'eBook-documents-menu-ico'
                , Title: eBook.Menu.File_Documents
                , type: eBook.Menu.ACTION //eBook.Menu.GROUP
                , items: []

            }, {
                id: 'eBook-File-PDFFiles'
                , IconCls: 'eBook-pdffiles-menu-ico'
                , Title: eBook.Pdf.Window_Title
                , type: eBook.Menu.ACTION
                , items: []
            }, {
                id: 'eBook-File-ReportGeneration'
                , IconCls: 'eBook-reports-generated'
                , Title: eBook.Menu.File_Reporting
                , type: eBook.Menu.ACTION
                , items: []
            }
          /*  , {
                id: 'eBook-File-ClosingProcedure'
                , IconCls: 'eBook-closingprocedure-menu-ico'
                , Title: eBook.Closing.Window_Title
                , type: eBook.Menu.ACTION
                , items: []
            }
            , {
                id: 'eBook-File-BizTax'
                , IconCls: 'eBook-biztax-menu-ico'
                , Title: "BizTax indiening" // eBook.Closing.Window_Title
                , type: eBook.Menu.ACTION
                , items: []
            }*/
            ]
            };
        }
    , lists: [
       {
           id: 'eBook-File-worksheets-general-list-id'
            , listId: 'eBook-File-worksheets-general-list'
            , store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetWorksheetTypesResult',
                 fields: eBook.data.RecordTypes.WorksheetTypes,
                 baseParams: {
                     Type: 'AL'
                 },
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.url + 'GetWorksheetTypes'
                    , criteriaParameter: 'cwtdc'
                    , method: 'POST'
                 })
                 , listeners: {
                     'load': {
                         fn: function(str, recs, opts) {
                             eBook.Interface.center.fileMenu.setWorksheetTypeAlerts(recs, 'eBook-File-worksheets-general', 'eBook-File-worksheets');
                         }
                    , scope: this
                     }
                 }
             })
            , getMyParams: function() { return { Culture: eBook.Interface.Culture, FileId: eBook.Interface.currentFile.get('Id') }; }
            , width: 600
            , multiSelect: false
            , emptyText: ''
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        var rec = lv.getStore().getAt(idx);
                        eBook.Worksheet.show(rec.get('RuleApp'), rec.get('Name'));
                    }
                    , scope: this
                }
            }
            , hideHeaders: true
            , columns: [new eBook.List.QualityColumn({
                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: 'Name',
                width: .6,
                dataIndex: 'Name'
}]
       }
       , {
           id: 'eBook-File-worksheets-financial-list-id'
            , listId: 'eBook-File-worksheets-financial-list'
            , store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetWorksheetTypesResult',
                 fields: eBook.data.RecordTypes.WorksheetTypes,
                 baseParams: {
                     Type: 'FI'
                 },
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.url + 'GetWorksheetTypes'
                    , criteriaParameter: 'cwtdc'
                    , method: 'POST'
                 })
                 , listeners: {
                     'load': {
                         fn: function(str, recs, opts) {
                             eBook.Interface.center.fileMenu.setWorksheetTypeAlerts(recs, 'eBook-File-worksheets-financial', 'eBook-File-worksheets');
                         }
                    , scope: this
                     }
                 }
             })
            , getMyParams: function() { return { Culture: eBook.Interface.Culture, FileId: eBook.Interface.currentFile.get('Id') }; }
            , width: 600
            , multiSelect: false
            , emptyText: ''
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        var rec = lv.getStore().getAt(idx);
                        eBook.Worksheet.show(rec.get('RuleApp'), rec.get('Name'));
                    }
                    , scope: this
                }
            }
            , hideHeaders: true
            , columns: [new eBook.List.QualityColumn({
                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: 'Name',
                width: .6,
                dataIndex: 'Name'
                }]
      
       }
    ]
    , load: function(record, importPrevious) {
        if (importPrevious) this.on('loaded', this.ImportPreviousWorksheets, this, { scope: this, single: true });
        eBook.Menu.File.superclass.load.apply(this, arguments);
    }
    , ImportPreviousWorksheets: function() {
        var wn = new eBook.Worksheet.Rules.Window({});
        wn.show(eBook.Worksheet.Rules.IMPORTPREVIOUS_RULES);
    }
    , loadData: function(record) {
        if (record) {
            this.setFileState(record.get('Closed'), record.get('NumbersLocked'));
            this.listLoadCount = 0;
            eBook.CurrentFile = record.get('Id');
            var subtext = '';
            subtext = String.format('[{0} - {1}]', record.get('StartDate').format('d/m/Y'), record.get('EndDate').format('d/m/Y'));
            if (record.get('ImportDate') != null) {
                subtext = '<br/>' + String.format(eBook.Menu.File_LastImported, record.get('ImportDate'));
            }
            this.setTitle({
                id: this.id
                , name: String.format(eBook.Menu.File_FileTitle, record.get('Name'))
                , subtext: subtext
            });
            for (var i = 0; i < this.lists.length; i++) {
                //params
                if (this.lists[i].rendered) {
                    this.lists[i].getStore().load({ params: this.lists[i].getMyParams(), callback: this.listLoaded, scope: this });
                } else {
                    if (this.lists[i].store) {
                        this.lists[i].store.load({ params: this.lists[i].getMyParams(), callback: this.listLoaded, scope: this });
                    } else {
                        this.lists[i].listConfig.store.load({ params: this.lists[i].getMyParams(), callback: this.listLoaded, scope: this });
                    }
                }

            }
            //this.fireEvent('loaded', this);
        }
    }
    , show: function(tp) {

        eBook.Menu.File.superclass.show.call(this);
        
    }

    , setWorksheetTypeAlerts: function(recs, id, parentId) {
        var errs = 0;
        var warns = 0;
        for (var i = 0; i < recs.length; i++) {
            errs += recs[i].get('Errors');
            warns += recs[i].get('Warnings');
        }
        if (errs > 0) {
            this.setMenuItemAlertLevel(id, "error", errs);
            if (parentId) this.setMenuItemAlertLevel(parentId, "error");
            return;
        }
        if (warns > 0) {
            this.setMenuItemAlertLevel(id, "warning", warns);
            if (parentId) this.setMenuItemAlertLevel(parentId, "warning");
            return;
        }
    }
    , renderLists: function() {
        this.loadData();
        // this.mon(this.lists[0], 'render', this.initFileDragZone);
        //this.mon(this.lists[2].store, 'datachanged', this.fileRecycleBinChanged);
        eBook.Menu.File.superclass.renderLists.call(this);

        // this.initRecycleBinDropZone(Ext.get('eBook-Client-files-deleted'), this.lists[2]); //
    }
    , onItemActionClick: function(item, itemData) {
        var wn;
        switch (itemData.id) {
            case 'eBook-File-Meta':
                wn = new eBook.Meta.Window({});
                wn.show();
                break;
            case 'eBook-File-accountmappings':
                wn = new eBook.Accounts.Mappings.Window({});
                wn.show();
                break;
            case 'eBook-File-accounts':
                wn = new eBook.Accounts.Manage.Window({});
                wn.show();
                break;
            case 'eBook-File-Close':
                Ext.get('eBook-File-worksheets-alerticon').toggleClass('eBook-icon-16-error');
                Ext.get('eBook-File-worksheets-general-alerticon').toggleClass('eBook-icon-16-error');
                break;
            case 'eBook-File-journal-manual':
                wn = new eBook.Journal.Window({ journal: 'MANUAL' });
                wn.show();
                break;
            case 'eBook-File-journal-auto':
                wn = new eBook.Journal.Window({ journal: 'AUTOMATIC' });
                wn.show();
                break;
            case 'eBook-File-journal-start':
                wn = new eBook.Journal.Window({ journal: 'OPENING' });
                wn.show();
                break;
            case 'eBook-File-journal-lastyear':
                wn = new eBook.Journal.Window({ journal: 'LASTYEAR' });
                wn.show();
                break;
            case 'eBook-File-finalTrialBalance':
                wn = new eBook.Journal.Window({ journal: 'FINAL' });
                wn.show();
                break;
            case 'eBook-File-documents':
                wn = new eBook.Document.Window({});
                wn.show();
                break;
            case 'eBook-File-ReportGeneration':
                wn = new eBook.Bundle.Window({});
                wn.show();
                break;
            case 'eBook-File-PDFFiles':
                wn = eBook.Pdf.WindowMgr.get('et-pdf-window');
                if (wn) {
                    eBook.Pdf.WindowMgr.bringToFront(wn);
                } else {
                    wn = new eBook.Pdf.Window({});
                    wn.show({ snapRight: true });
                }
                break;
            case 'eBook-File-ClosingProcedure':
                wn = new eBook.Closing.Window({});
                wn.show();
                break;
            case 'eBook-File-BizTax':
                wn = new eBook.Xbrl.BizTaxWindow({});
                wn.show();
                break;
            case 'eBook-File-leadsheets':
                eBook.Splash.setText("Generating leadsheets");
                eBook.Splash.show();
                Ext.Ajax.request({
                    url: eBook.Service.bundle + 'GenerateLeadSheets'
                    , method: 'POST'
                    , params: Ext.encode({
                        cidc: {
                            Culture: eBook.Interface.currentFile.get('Culture')
                                , Id: eBook.Interface.currentFile.get('Id')
                        }
                    })
                    , callback: this.onLeadSheetsReady
                    , scope: this
                });
        }
    }
    , onLeadSheetsReady: function(opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open('pickup/' + robj.GenerateLeadSheetsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , setFileState: function(closed, numberslocked) {
        var el = Ext.get(this.el);
        el.removeClass("eBook-File-Closed");
        el.removeClass("eBook-File-NumbersLocked");
        if (closed) {
            el.addClass("eBook-File-Closed");
            return;
        }
        if (numberslocked) {
            el.addClass("eBook-File-NumbersLocked");
            return;
        }
    }

    , onClose: function() {
        eBook.Interface.currentFile = null;
        this.setTitle({
            id: this.id
                , name: ''
                , subtext: ''
        });

        for (var i = 0; i < this.lists.length; i++) {
            //params 
            if (this.lists[i].rendered) this.lists[i].getStore().removeAll();
        }
        eBook.Menu.File.superclass.onClose.call(this);
    }
    , refreshList: function(listId) {
        for (var i = 0; i < this.renderedLists.length; i++) {
            if (this.renderedLists[i].listId == listId) {
                this.renderedLists[i].getStore().load({ params: this.renderedLists[i].getMyParams(), scope: this });
                break;
            }
        }

    }
    });


/*{
                    id: 'eBook-File-worksheets-messages'
                            , IconCls: 'eBook-check-worksheets-menu-ico'
                            , Title: eBook.Language.Menu.MessageOverview
			    , type: eBook.Menu.ACTION
                            , items: []
                }
                        , */