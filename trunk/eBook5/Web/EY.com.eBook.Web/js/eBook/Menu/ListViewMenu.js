
eBook.Menu.ListViewMenu = Ext.extend(Ext.menu.Menu, {
    enableScrolling: false,
    reloadOnShow: false,
    initComponent: function() {

        /*
        if (this.strict = (Ext.isIE7 && Ext.isStrict)) {
        this.on('show', this.onShow, this, { single: true, delay: 20 });
        }*/
        Ext.apply(this, {
            plain: true,
            showSeparator: false,
            boxMinHeight: 20,
            items: this.listView = new Ext.list.ListView(Ext.applyIf({
        }, this.listConfig))
    });
    this.listView.purgeListeners();
    eBook.Menu.ListViewMenu.superclass.initComponent.call(this);
    // this.relayEvents(this.listView, ['click']);
    //this.on('show', this.picker.focus, this.picker);
    this.listView.on('click', this._onlistItemClick, this);
    if (this.handler) {
        this.on('itemclick', this.handler, this.scope || this);
    }
}
    , _onlistItemClick: function(dv, i, n, e) {
        var rec = dv.getStore().getAt(i);
        //this.onListItemClick(this, rec);
        this.fireEvent('itemclick', this, rec);
    }
    , menuHide: function() {
        if (this.hideOnClick) {
            this.hide(true);
        }
    }
    , show: function(el, pos, pmenu) {
        if (this.reloadOnShow) this.listView.store.reload();
        eBook.Menu.ListViewMenu.superclass.show.call(this, el, pos, pmenu);
    }
    , onShow: function() {
        //alert("show");
        this.listView.reload();
    }
    , reload: function() {
        if (this.listView) {
            this.listView.store.reload();
        }
    }
});