/*
{
            id: 'eBook-Client-manualworksheets'
                    , IconCls: 'eBook-worksheet-group-extra-menu-ico'
                    , Title: eBook.Client.PreWorksheetWindow_Title //eBook.Menu.File_ExtraFiles //eBook.Language.Menu.WorksheetsAL
                    , type: eBook.Menu.GROUP
                    , items: [{
                        id: 'eBook-Client-manualworksheets-list'
                                , listActions: []
                                , type: eBook.Menu.LIST
                                , items: []
                            }]
        },
        
        
        , {
                    id: 'eBook-Client-manualworksheets-list-id'
            , listId: 'eBook-Client-manualworksheets-list'
            , xtype: 'eBookListView'
                    //, getMyParams: function() { return { ClientId: eBook.Interface.currentFile.get('Id') }; }
            , width: 500
            , tbar: [{
                    text: eBook.Client.PreWorksheetWindow_New,
                iconCls: 'eBook-worksheets-add-ico',
                scale: 'small',
                iconAlign: 'left',
                handler: function() {
                    var wn = new eBook.Client.PreWorksheetWindow({
                        ruleApp: 'AangifteVenBManualApp'
                        , clientId: eBook.CurrentClient
                        , typeName: 'Manuele aangifte venb'
                    });
                    wn.show();
                },
                scope: this
            }]
            , listConfig: {
                store:
                 new Ext.data.JsonStore({
                     autoDestroy: true,
                     root: 'GetClientManualWorksheetsResult',
                     fields: eBook.data.RecordTypes.ManualWorksheets,
                     proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                         url: eBook.Service.url + 'GetClientManualWorksheets'
                        , criteriaParameter: 'cfdc'
                        , method: 'POST'
                     })
                 })
                , width: 450
                , multiSelect: false
                , emptyText: 'No manual worksheets'
                , reserveScrollOffset: false
                , autoHeight: true
                , listeners: {
                    'click': {
                        fn: function(lv, idx, nd, e) {
                            var rec = lv.getStore().getAt(idx);
                            var wn = new eBook.Client.PreWorksheetWindow({
                                ruleApp: 'AangifteVenBManualApp'
                                , clientId: eBook.CurrentClient
                                , typeName: 'Manuele aangifte venb'
                            });
                            wn.show(rec);

                        }
                        , scope: this
                    }
                }
                , hideHeaders: true
                , columns: [{
                    header: 'Name',
                    width: .9,
                    dataIndex: 'name'
}]
                }
                }
*/

eBook.Menu.Client = Ext.extend(eBook.Menu.menu, {
    getData: function() {
        return {
            IconCls: 'eBook-Client-menu-ico'
        , items: [{
            id: 'eBook-Client-data'
                , IconCls: 'eBook-meta-menu-ico'
                , Title: eBook.Menu.Client_ClientData//'Klantgegevens (- TT)' //eBook.Language.Menu.PermanentMeta
                , items: []
                , type: eBook.Menu.ACTION
        }, 
         {
             id: 'eBook-Client-PMT'
                    , IconCls: 'eBook-pmt-menu-ico'
                    , Title: eBook.PMT.TeamWindow_Title //'Klantgegevens (- TT)' //eBook.Language.Menu.PermanentMeta
                    , items: []
                    , type: eBook.Menu.ACTION
         }, {
             id: 'eBook-Client-suppliers'
                , IconCls: 'eBook-suppliers-menu-ico'
                , Title: eBook.Menu.Client_Suppliers
                , items: []
                , type: eBook.Menu.ACTION
         }
            , {
                id: 'eBook-Client-customers'
                , IconCls: 'eBook-customers-menu-ico'
                , Title: eBook.Menu.Client_Customers
                , items: []
                , type: eBook.Menu.ACTION
            }
            /* , {
            id: 'eBook-Client-personnel'
            , IconCls: 'eBook-personnel-menu-ico'
            , Title: eBook.Language.Menu.Personnel
            , type: eBook.Menu.ACTION
            , items: []
            }*/
            , {
                id: 'eBook-Client-files-add'
                , Title: eBook.Menu.Client_NewFile//eBook.Language.Menu.NewFile
                , IconCls: 'eBook-createfile-menu-ico'
                , type: eBook.Menu.ACTION
                , items: []
            }, {
                id: 'eBook-Client-files-openlist'
                , Title: eBook.Menu.Client_OpenFiles
                , IconCls: 'eBook-existing-open-files-menu-ico'
                , type: eBook.Menu.GROUP
                , items: [{
                    id: 'eBook-Client-files-list'
                            , listActions: []
                            , type: eBook.Menu.LIST
                            , items: []
}]
                }, {
                    id: 'eBook-Client-files-closed'
                , Title: eBook.Menu.Client_ClosedFiles
                , IconCls: 'eBook-existing-closed-files-menu-ico'
                , type: eBook.Menu.GROUP
                , items: [{
                    id: 'eBook-Client-files-closed-list'
                            , listActions: []
                            , type: eBook.Menu.LIST
                            , items: []
}]
                }
                , {
                    id: 'eBook-Client-files-deleted'
                    , Title: eBook.Menu.Client_Deleted
                    , IconCls: 'eBook-marked-deletion-files-menu-ico '
                    , type: eBook.Menu.GROUP
                    , align: 'right'
                    , items: [{
                        id: 'eBook-Client-files-deleted-list'
                            , listActions: []
                            , type: eBook.Menu.LIST
                            , items: []
}]
                    }
            ]
                };
            }
    , lists: [
       {
           id: 'eBook-Client-files-openlist-list'
            , listId: 'eBook-Client-files-list'
            , store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetFileInfosResult',
                 fields: eBook.data.RecordTypes.FileInfo,
                 baseParams: {
                     MarkedForDeletion: false
                    , Deleted: false
                    , Closed: false
                 },
                 //idField: 'Id',
                 // data: [{Id:'1','n':'Naam','sd': new Date(), 'ed':new Date()}]
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.file + 'GetFileInfos'
                     , criteriaParameter: 'cfdc'
                     , method: 'POST'
                 })
             })
            , width: 600
            , multiSelect: false
            , emptyText: eBook.Menu.Client_NoOpenFiles
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        eBook.Interface.openFile(lv.getStore().getAt(idx),true);
                        eBook.Interface.center.clientMenu.toggleGroupBy('eBook-Client-files-openlist');
                    }
                    , scope: this
                }
            }
            , columns: [new eBook.List.QualityColumn({

                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: eBook.Menu.Client_FileName,
                width: .55,
                dataIndex: 'Name'
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'StartDate'
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'EndDate'
}]
       }
        , {
            id: 'eBook-Client-files-closedlist'
            , listId: 'eBook-Client-files-closed-list'
            , store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetFileInfosResult',
                 fields: eBook.data.RecordTypes.FileInfo,
                 baseParams: {
                     MarkedForDeletion: false
                    , Deleted: false
                    , Closed: true
                 },
                 //idField: 'Id',
                 // data: [{Id:'1','n':'Naam','sd': new Date(), 'ed':new Date()}]
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.file + 'GetFileInfos'
                     , criteriaParameter: 'cfdc'
                     , method: 'POST'
                 })
             })
            , width: 500
            , multiSelect: false
            , emptyText: eBook.Menu.Client_NoClosedFiles
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        eBook.Interface.loadFile(lv.getStore().getAt(idx),true);
                        eBook.Interface.center.clientMenu.toggleGroupBy('eBook-Client-files-closed');
                    }
                    , scope: this
                }
            }
            , columns: [{
                header: eBook.Menu.Client_FileName,
                width: .6,
                dataIndex: 'Name'
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'StartDate'
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'EndDate'
}]
            }
            , {
                id: 'eBook-Client-files-deleted-list-list'
                , listId: 'eBook-Client-files-deleted-list'
                , store:
                     new Ext.data.JsonStore({
                         autoDestroy: true,
                         root: 'GetFileInfosResult',
                         fields: eBook.data.RecordTypes.FileInfo,
                         baseParams: {
                             MarkedForDeletion: true
                            , Deleted: false
                         },
                         //idField: 'Id',
                         // data: [{Id:'1','n':'Naam','sd': new Date(), 'ed':new Date()}]
                         proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                             url: eBook.Service.file + 'GetFileInfos'
                             , criteriaParameter: 'cfdc'
                             , method: 'POST'
                         })
                     })
                , width: 500
                , multiSelect: false
                , emptyText: eBook.Menu.Client_NoDeletedFiles
                , reserveScrollOffset: false
                , autoHeight: true
                , columns: [{
                    header: eBook.Menu.ClientFileName_,
                    width: .6,
                    dataIndex: 'Name'
                }, {
                    header: eBook.Menu.Client_StartDate,
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    width: .2,
                    dataIndex: 'StartDate'
                }, {
                    header: eBook.Menu.Client_EndDate,
                    xtype: 'datecolumn',
                    format: 'd/m/Y',
                    width: .2,
                    dataIndex: 'EndDate'
}]
                }
                
    ]

    , loadData: function(record) {
        this.listLoadCount = 0;
        eBook.CurrentClient = record.get('Id');
        var subtext = record.get('Address') + ', ' + record.get('ZipCode') + ' ' + record.get('City');
        subtext += '<br/>PMT ROLLEN: <b>' + eBook.User.activeClientRoles + '</b>';
        if (record.get('ProAccDatabase')) {
            subtext += '<br/>ProAcc database: <b>' + record.get('ProAccDatabase') + '</b>';
            if (record.get('ProAccLinkUpdated' != null)) subtext += ' (last updated: ' + record.get('ProAccLinkUpdated').format('d/m/Y') + ')';
        }
        if (record.get('Shared')) {
            subtext += '<br/><span class="eBook-client-shared-txt">Gedeeld dossier ACR & TAX</span>';
        }
        this.setTitle({
            id: this.id
            , name: record.get('Name')
            , subtext: subtext
        });
        for (var i = 0; i < this.lists.length; i++) {
            //params
            if (this.lists[i].rendered) {
                this.lists[i].store.load({ params: {
                    ClientId: eBook.CurrentClient
                }, callback: this.listLoaded, scope: this
                });
            } else {
                if (this.lists[i].store) {
                    this.lists[i].store.load({ params: { ClientId: eBook.CurrentClient }, callback: this.listLoaded, scope: this });
                } else {
                    this.lists[i].listConfig.store.load({ params: { ClientId: eBook.CurrentClient }, callback: this.listLoaded, scope: this });
                }
            }
        }
        //this.refresh();
    }
    , updateList: function(listId) {
        for (var i = 0; i < this.lists.length; i++) {
            //params
            if (this.lists[i].listId == listId) {
                if (this.lists[i].rendered) {
                    this.lists[i].store.load({ params: {
                        ClientId: eBook.CurrentClient
                    }, scope: this
                    });
                } else {
                    if (this.lists[i].store) {
                        this.lists[i].store.load({ params: { ClientId: eBook.CurrentClient }, scope: this });
                    } else {
                        this.lists[i].listConfig.store.load({ params: { ClientId: eBook.CurrentClient }, scope: this });
                    }
                }
            }
        }
    }
    , show: function(tp) {
        eBook.Menu.Client.superclass.show.call(this);
    }
    , renderLists: function() {

        eBook.Menu.Client.superclass.renderLists.call(this);
        this.initFileDragZone(this.renderedLists[0]);
        this.mon(this.renderedLists[2].store, 'datachanged', this.fileRecycleBinChanged);
        this.initRecycleBinDropZone(Ext.get('eBook-Client-files-deleted'), this.lists[2]); //
    }
    , reloadFiles: function() {
        this.renderedLists[0].store.reload();
    }
    , reloadFilesAndOpen: function(id, importPrevious) {
        this.renderedLists[0].store.load({ params: {
            ClientId: eBook.CurrentClient
        }, callback: function() { this.openFile(id, importPrevious) }, scope: this
        });
        this.renderedLists[1].store.load({ params: {
            ClientId: eBook.CurrentClient
        }, callback: function() { this.openFile(id, importPrevious) }, scope: this
        });
    }
    , openFile: function(id, importPrevious) {
        var rec = this.renderedLists[0].store.getById(id);
        var recCl = this.renderedLists[1].store.getById(id);
        if (rec) eBook.Interface.loadFile(rec, importPrevious);
        if (recCl) eBook.Interface.loadFile(recCl, importPrevious);
    }
    , fileRecycleBinChanged: function(store) {
        var icon = Ext.get('eBook-Client-files-deleted-icon');
        if (store.getCount() > 0) {
            icon.addClass('eBook-recyclebin-full');
        } else {
            icon.removeClass('eBook-recyclebin-full');
        }
    }
    , initFileDragZone: function(v) {

        v.dragZone = new Ext.dd.DragZone(v.innerBody, {
            ddGroup: 'eBook-files',
            getDragData: function(e) {
                var sourceEl = e.getTarget(v.itemSelector, 10);
                if (sourceEl) {
                    d = sourceEl.cloneNode(true);
                    d.id = Ext.id();
                    var rec = v.getRecord(sourceEl);
                    return v.dragData = {
                        sourceEl: sourceEl,
                        repairXY: Ext.fly(sourceEl).getXY(),
                        ddel: d,
                        storeIdx: v.store.indexOf(rec),
                        fileRecord: rec
                    }
                }
            },
            getRepairXY: function() {
                return this.dragData.repairXY;
            }
        });
    }
                /*,refresh:function() {
                this.iconDropZone.destroy();
                eBook.Menu.Client.superclass.refresh.apply(this);
                }*/
    , initRecycleBinDropZone: function(iconEl, listItem) {
        //var list =Ext.getCmp()

        // init icon drop
        this.iconDropZone = new Ext.dd.DropTarget(iconEl, {
            ddGroup: 'eBook-files',
            notifyDrop: function(ddSource, e, data) {
                eBook.Interface.center.clientMenu.markFileForDeletion(data.fileRecord.get('Id'));
                var lst = Ext.getCmp(Ext.get(data.sourceEl).parent(".x-list-wrap").id);
                lst.store.removeAt(data.storeIdx);
                listItem.store.add(data.fileRecord);
                lst.store.commitChanges();
                listItem.store.commitChanges();
                listItem.store.fireEvent('datachanged', listItem.store);
                // get marked deleted store and add record.
                // get source store and remove record.
                return true;
            }
        });
    }
    , markFileForDeletion: function(id) {
        Ext.Ajax.request({
            url: eBook.Service.url + 'MarkFileForDeletion'
            , method: 'POST'
            , params: Ext.encode({ id: id })
            , success: this.onMarkFileForDeletionSuccess
            , failure: this.onMarkFileForDeletionFailure
            , scope: this
        });
    }
    , onMarkFileForDeletionSuccess: function(resp, opts) {
        //alert("marked for deletion");
    }
    , onMarkFileForDeletionFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.title);
    }
    , onContextMenu: function(e) {
        var it = e.getTarget(".eBook-marked-deletion-files-menu-ico", this.el);
        if (it) {
            if (!this.iconRecycleContext) {
                this.iconRecycleContext = new eBook.Recycle.IconContextMenu({ id: 'eBook-recycle-file-context', listId: this.lists[2].id });
            }
            e.stopEvent();
            this.iconRecycleContext.showAt(e.getXY());
        }
    }
    , onItemActionClick: function(item, itemData) {
        switch (itemData.id) {
            case 'eBook-Client-TEST':
                var wn = new eBook.Client.PreWorksheetWindow({
                    ruleApp: 'AangifteVenBManualApp'
                    , clientId: eBook.CurrentClient
                    , typeName: 'Manuele aangifte venb'
                });
                wn.show();
                break;
            case 'eBook-Client-data':
                var w = new eBook.Client.Window({ title: itemData.Title });
                w.show();
                break;
            case 'eBook-Client-PMT':
                var wn = new eBook.PMT.TeamWindow({});
                wn.show();
                break;
            case 'eBook-Client-customers':
                var wn = new eBook.BusinessRelations.Window({
                    businessRelationType: 'C'
                    , title: eBook.Menu.Client_Customers
                });
                wn.show();
                break;
            case 'eBook-Client-suppliers':
                var wn = new eBook.BusinessRelations.Window({
                    businessRelationType: 'S'
                    , title: eBook.Menu.Client_Suppliers
                });
                wn.show();
                break;
            case 'eBook-Client-files-add':
               // var wn = new eBook.Create.Window({});
                //wn.show();
                var eb = new eBook.NewFile.Window({});
                eb.show();
                break;
        }
    }
    , onClose: function() {
        eBook.CurrentClient = null;
        this.setTitle({
            id: this.id
                , name: ''
                , subtext: ''
        });
        for (var i = 0; i < this.lists.length; i++) {
            //params
            if (this.lists[i].store) this.lists[i].store.removeAll();
            if (this.lists[i].listConfig) this.lists[i].listConfig.store.removeAll();
        }
        eBook.Menu.Client.superclass.onClose.call(this);
        eBook.Interface.home();
    }
    , load: function(record) {
        if (eBook.User.isCurrentlyAdmin()) {
            // load client
            eBook.User.activeClientRoles.push('ADMINISTRATOR');
            eBook.Menu.Client.superclass.load.apply(this, arguments);
        } else if (eBook.User.isCurrentlyChampion()) {
            eBook.User.activeClientRoles.push('eBook Champion');
            eBook.Menu.Client.superclass.load.apply(this, arguments);
        } else {
            Ext.Ajax.request({
                url: eBook.Service.url + 'GetClientRoles'
                , method: 'POST'
                , params: Ext.encode({ cipdc: {
                    Id: record.get('Id'),
                    Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: function(opts, success, resp) { this.onRolesCallback(opts, success, resp, record); }
                , scope: this
            });
        }
    }
    , onRolesCallback: function(opts, success, resp, record) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            var lst = robj.GetClientRolesResult;

            if (lst != null && Ext.isArray(lst) && lst.length > 0) {
                eBook.User.activeClientRoles = lst;
                eBook.Menu.Client.superclass.load.apply(this, [record]);
            } else {
                this.onClose();
                eBook.Interface.showError('You don\'t have access to client ' + record.get('Name'), 'Client');
            }
        } else {
            this.onClose();
            eBook.Interface.showResponseError(resp, 'Client');
        }
    }
});

