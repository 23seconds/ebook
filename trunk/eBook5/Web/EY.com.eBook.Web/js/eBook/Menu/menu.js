//<div id="{id}-edit" class="eBook-icon-16 eBook-menu-title-block-edit-ico"></div>
eBook.Menu.ACTION = 'action';
eBook.Menu.GROUP = 'group';
eBook.Menu.LIST = 'list';

eBook.AlertLevel = {
    ERROR:'error',
    WARNING:'warning',
    INFORMATION:'info',
    NONE:''
}

eBook.Menu.menu = Ext.extend(Ext.BoxComponent, {
    overCls: 'eBook-menu-over'
    , menuCloseSelector: 'eBook-menu-title-block-close-ico'
    , titleToolBarItemSelector: '.eBook-menu-title-block-toolbar-item'
    , titleSelector: '.eBook-menu-title-block'
    , itemSelector: '.eBook-menu-item'
    , listSelector: '.eBook-menu-group'
    , groupSelector: '.eBook-menu-group'
    , proxySelector: '.eBook-menu-item-proxy'
    , listItemSelector: '.eBook-menu-group-item' // ? or real list objects (!= lightweight)
    , titleTemplate: new Ext.XTemplate('<tpl for="."><div id="{id}-toolbar" class="eBook-menu-title-block-toolbar"> <div id="{id}-close" class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div></div><div id="{id}-title" class="eBook-menu-title">{name}</div><div id="{id}-subtext" class="eBook-menu-subtext">{subtext}</div></tpl>', { compiled: true })
    , template: new Ext.XTemplate('<tpl for="."><div>'
            , '<div id="{id}-titleblock" class="eBook-menu-title-block {IconCls}"></div>'
                , '<div id="{id}-children" class="eBook-menu-childcontainer">'
                    , '<tpl for="items">'
                            , '<div id="{id}" class="eBook-menu-item {[values[\"align\"]!=null ? \"eBook-float-\" + values[\"align\"] : \"\"]}">'
                                , '<div id="{id}-icon" class="eBook-menu-item-icon {IconCls}{[values[\'Title\']!=\'\'? \'\' : \' eBook-notitle\']}"><div id="{id}-alerticon" class="eBook-menu-item-alert-icon"></div></div>'
                                , '<tpl if="Title!=\'\'"><div id="{id}-title" class="eBook-menu-item-title">{Title}</div></tpl>'
                            , '</div>'
                    , '</tpl>'
                    , '<div class="eBook-menu-groups">'
                    , '<tpl for="items">'
                        , '<tpl if="type==eBook.Menu.GROUP">'
                            , '<div id="{id}-group" class="eBook-menu-group" style="display:none"><div id="{id}-group-parent" class="eBook-menu-group-parent"></div><div id="{id}-group-content" class="eBook-menu-group-content">'
                                , '<tpl for="items">'
                                    , '<tpl if="type==eBook.Menu.LIST">'
                                        , '<div id="{id}" class="eBook-menu-list">'
                                        , '</div>'
                                    , '</tpl>'
                                    , '<tpl if="type==eBook.Menu.ACTION || type==eBook.Menu.GROUP">'
                                        , '<div id="{id}" class="eBook-menu-item">'
                                            , '<div id="{id}-icon" class="eBook-menu-item-icon {IconCls}"><div id="{id}-alerticon" class="eBook-menu-item-alert-icon"></div></div>'
                                            , '<div id="{id}-title" class="eBook-menu-item-title">{Title}</div>'
                                        , '</div>'
                                    , '</tpl>'
                                , '</tpl>'
                                , '<tpl for="items">'
                                    , '<tpl if="type==eBook.Menu.GROUP">'
    //, '<div class="eBook-menu-groups">'
                                            , '<div id="{id}-group" class="eBook-menu-group" style="display:none"><div id="{id}-group-parent" class="eBook-menu-group-parent"></div><div id="{id}-group-content" class="eBook-menu-group-content">'

                                                , '<tpl for="items">'
                                                    , '<tpl if="type==eBook.Menu.LIST">'
                                                        , '<div id="{id}" class="eBook-menu-list">'
                                                        , '</div>'
                                                    , '</tpl>'
                                                    , '<tpl if="type==eBook.Menu.ACTION">'
                                                        , '<div id="{id}" class="eBook-menu-item">'
                                                            , '<div id="{id}-icon" class="eBook-menu-item-icon {IconCls}"><div id="{id}-alerticon" class="eBook-menu-item-alert-icon"></div></div>'
                                                            , '<div id="{id}-title" class="eBook-menu-item-title">{Title}</div>'
                                                        , '</div>'
                                                    , '</tpl>'
                                                , '</tpl>'
                                            , '</div></div>'
    //, '</div>'
                                    , '</tpl>'
                                , '</tpl>'
                            , '</div></div>'
                        , '</tpl>'
                    , '</tpl>'
                    , '</div>'
                , '</div>'
        , '</div>'
        , '</tpl>'

        , { compiled: true })
    , lists: []
    , getData: function() { return {}; }
    , constuctor: function(config) {
        this.addEvents({
            "loaded": true,
            "closed": true
        });
        eBook.Menu.menu.superclass.constructor.call(this, config);
    }
    , initComponent: function() {
        if (!this.id) { this.id = this.getId() };
        Ext.apply(this, {
            autoEl: {
                tag: 'div'
                , cls: 'eBook-menu'
            }
        });
        this.data = this.getData();
        this.data.id = this.getId();
        eBook.Menu.menu.superclass.initComponent.apply(this, arguments);
    }
    , data: {}
    , dynamicLists: []
    , refresh: function(reloadData) {
        this.removeEvents();
        if (reloadData) this.data = this.getData();
        this.data.id = this.getId();

        this.removeLists();
        this.el.update('');
        this.afterRender();
        //this.setTitle();
    }
     , onRender: function(ct, position) {
         if (!this.el) {
             this.el = document.createElement('div');
             if (Ext.isString(this.autoEl)) {
                 this.el = document.createElement(this.autoEl);
             } else {
                 var div = document.createElement('div');
                 Ext.DomHelper.overwrite(div, this.autoEl);
                 this.el = div.firstChild;
             }
             if (!this.el.id) {
                 this.el.id = this.getId();
             }
         }
         if (this.el) {
             this.el = Ext.get(this.el);
             if (this.allowDomMove !== false) {
                 ct.dom.insertBefore(this.el.dom, position);
                 if (div) {
                     Ext.removeNode(div);
                     div = null;
                 }
             }
         }

         var mxHeight = 72;

     }
    , afterRender: function(ct) {

        this.template.overwrite(this.el, this.data, true);
        this.setEvents();
        this.renderLists();
    }
    , removeLists: function() {
        for (var i = 0; i < this.renderedLists.length; i++) {
            this.renderedLists[i].purgeListeners();
            if (this.renderedLists[i].data) this.renderedLists[i].destroy();
        }
        this.renderedLists = [];
    }
    , renderLists: function() {
        this.renderedLists = [];
        for (var i = 0; i < this.lists.length; i++) {
            var cfg = this.lists[i];
            Ext.apply(cfg, {
                renderTo: this.lists[i].listId
            });
            this.renderedLists.push(
                Ext.create(cfg, 'listview')
                );
            //this.lists[i].render();
        }
    }
    , removeEvents: function() {
        this.mun(this.el, {
            "click": this.onClick,
            "dblclick": this.onDblClick,
            "contextmenu": this.onContextMenu,
            "mouseover": this.onMouseOver,
            "mouseout": this.onMouseOut
        });

    }
    , setEvents: function() {
        this.mon(this.el, {
            "click": this.onClick,
            "dblclick": this.onDblClick,
            "contextmenu": this.onContextMenu,
            "mouseover": this.onMouseOver,
            "mouseout": this.onMouseOut,
            scope: this
        });

    }
    , clearAllAlerts: function() {
        var nds = this.el.query('.eBook-menu-item-alert-icon');
        for (var i = 0; i < nds.length; i++) {
            var nel = Ext.get(nds[i]);
            nel.removeClassLike("eBook-icon-16-");
            //nel.removeClass("eBook-icon-16-warning");
            var par = nel.up('.eBook-menu-item', 5);
            if (par) {
                Ext.get(par).setQtip('');
            }
        }
    }
    , clearMenuItemAlertLevel: function(itemEl, iconEl) {
        iconEl.removeClassLike('eBook-icon-16-');
        itemEl.setQtip("");
    }
    , setMenuItemAlertLevel: function(idOrEl, level, cnt) {
        var itemEl = Ext.get(idOrEl);
        var iconEl = Ext.get(itemEl.child(".eBook-menu-item-alert-icon"));
        if (iconEl) {
            this.clearMenuItemAlertLevel(itemEl, iconEl);
            iconEl.addClass("eBook-icon-16-" + level);
            if (Ext.isDefined(cnt)) itemEl.setQtip(cnt);
        }
    }

    , onDblClick: function(e) { }
    , onContextMenu: function(e) { }
    , onMouseOut: function(e) {
        var title = e.getTarget(this.titleSelector, this.el);
        var item = e.getTarget(this.itemSelector, this.el);
        var proxy = e.getTarget(this.proxySelector, this.el);

        if (title) {
            Ext.get(title).removeClass(this.overCls);
        } else if (item) {
            Ext.get(item).removeClass(this.overCls);
        } else if (proxy) {
            Ext.get(proxy).removeClass(this.overCls);
        }
    }
    , onMouseOver: function(e) {
        var title = e.getTarget(this.titleSelector, this.el);
        var item = e.getTarget(this.itemSelector, this.el);
        var proxy = e.getTarget(this.proxySelector, this.el);

        if (title) {
            Ext.get(title).addClass(this.overCls);
        } else if (item) {
            Ext.get(item).addClass(this.overCls);
        } else if (proxy) {
            Ext.get(proxy).addClass(this.overCls);
        }
    }
    , onClick: function(e) {
        var titleTB = e.getTarget(this.titleToolBarItemSelector, this.el);
        var title = e.getTarget(this.titleSelector, this.el);
        var item = e.getTarget(this.itemSelector, this.el);
        var proxy = e.getTarget(this.proxySelector, this.el);
        //var listItem = e.getTarget(this.

        if (titleTB) {
            if (Ext.get(titleTB).hasClass(this.menuCloseSelector)) {
                this.onClose();
            } else {
                this.onTitleToolBarItemClick(e, titleTB);
            }
        } else if (title) {
            this.onTitleClick(e, title);
        } else if (proxy) {
            this.onItemProxyClick(e, proxy);
            //this.onContainerClick(e);
        } else if (item) {

            this.onItemClick(e, item);

            //this.onItemClick(item, this.getDataItem(item), e);

        } else {
            if (this.onContainerClick) this.onContainerClick(e);
        }
    }
    , onTitleToolBarItemClick: function(e, toolbar) { }
    , onTitleClick: function(e, title) {
        this.toggleChildren(this.id);
    }
    , onItemClick: function(e, item) {
        var itemData = this.getItemData(this.data, item.id);
        if (itemData && itemData.type == eBook.Menu.GROUP) {
            this.toggleGroup(item, itemData);
        } else {
            this.onItemActionClick(item, itemData);
            //this.toggleChildren(item.id);
        }
    }
    , onItemActionClick: function(item, itemData) {
        //alert("open " + itemData.Title);
    }
    , onItemProxyClick: function(e, proxy) {
        var itemId = Ext.get(proxy).getAttributeNS('eBook', 'itemId');
        this.onItemClick(e, Ext.get(itemId));
    }
    , onListItemClick: function(e, listname, listRecord) {

    }
    , getItemData: function(cont, id) {
        for (var i = 0; i < cont.items.length; i++) {
            var it = cont.items[i];
            if (it.id == id) return it;
            if (it.items.length > 0) {
                var itdeep = this.getItemData(it, id);
                if (itdeep) return itdeep;
            }
        }
        return null;
    }
    , animate: true
    , toggleGroupBy: function(itemid) {
        var item = Ext.get(itemid);
        var itemData = this.getItemData(this.data, item.id);
        if (itemData && itemData.type == eBook.Menu.GROUP) {
            this.toggleGroup(item, itemData);
        }
    }
    , toggleGroup: function(item, itemData) {
        var item = Ext.get(item);
        var lst = Ext.get(item.id + '-group');
        if (lst) {
            var lstIcon = Ext.get(item.id + '-group-parent');
            lst.setVisibilityMode(Ext.Element.DISPLAY);
            if (!lst.isVisible()) {


                var proxy = this.getProxy(item, itemData, lst, lstIcon);
                item.orgLocation = item.getXY();
                if (!this.animate) {

                    proxy.show();
                    item.setOpacity(0.3);
                    lst.show();
                } else {
                    proxy.show();
                    item.setOpacity(0.3);
                    lst.show();
                    proxy.moveTo(lstIcon.getX(), lstIcon.getY(), {
                        duration: .5
                , callback: function() { this.clearPositioning(); }
                    });
                }

                // lst.show();


            } else {

                //if (item.orgLocation) item.moveTo(item.orgLocation[0], item.orgLocation[1], true);
                var proxy = this.getProxy(item, itemData);
                // proxy.remove();
                if (!this.animate) {
                    item.clearOpacity();
                    proxy.hide();
                    lst.hide();
                } else {
                    proxy.position('absolute');
                    proxy.moveTo(item.getX(), item.getY(), {
                        duration: .5
                , callback: function() { item.clearOpacity(); lst.hide(); this.hide(); }
                    });
                }

                //lst.hide();

            }
        }
    }
    , getProxy: function(item, itemData, lst) {
        var proxy = Ext.get(item.id + '-proxy');
        if (proxy) return proxy;
        proxy = Ext.DomHelper.createDom({
            tag: 'div'
            , cls: 'eBook-menu-item eBook-menu-item-proxy'
            , id: item.id + '-proxy'
            , itemId: item.id
            , style: 'display:none'
            , children: [{
                tag: 'div'
                    , cls: 'eBook-menu-item-icon ' + itemData.IconCls
                    , html: ''
            }, {
                tag: 'div'
                    , cls: 'eBook-menu-item-title'
                    , html: itemData.Title
            }
            ]
        });
        //Ext.getBody().appendChild(proxy);
        Ext.get(item.id + '-group-parent').appendChild(proxy);
        proxy = Ext.get(proxy);
        proxy.set({ 'eBook:itemId': item.id });
        proxy.show();
        proxy.position('absolute');
        proxy.moveTo(item.getX(), item.getY());
        return proxy;
    }

    , toggleChildren: function(id) {
        var children = Ext.get(id + '-children');
        if (children) {
            children.setVisibilityMode(Ext.Element.DISPLAY);
            if (children.isVisible()) {
                children.hide();
            } else {
                children.show();
            }
        }
    }
    , show: function() {
        if (!this.el) return;
        this.el.show();
    }
    , hide: function() {
        if (this.el) {
            this.el.hide();
        }
    }
    , load: function(record) {
        this.record = record;
        this.loadData(record);
    }
    , reload: function() {
        if (this.record) {
            this.loadData(record);
        }
    }
    , loadData: Ext.emptyFn // override
    , listLoadCount: 0
    , listLoaded: function() {
        this.listLoadCount++;
        if (this.listLoadCount == this.lists.length) this.fireEvent('loaded', this);
    }
    , setTitle: function(data) {
        if (data) this.titleData = data;
        this.titleTemplate.overwrite(Ext.get(this.id + "-titleblock"), this.titleData, true);
    }
    , syncSizes: function() {
        var menuItems = this.el.query('.eBook-menu-item');
        Ext.each(menuItems, function(it) {
            Ext.get(it).autoHeight();
        }, this);
    }
    , onClose: function() {
        eBook.Pdf.WindowMgr.closeAll();
        this.hide();
    }
    , refreshList: function(listId) {
        for (var i = 0; i < this.renderedLists.length; i++) {
            if (this.renderedLists[i].listId == listId) {
                this.renderedLists[i].getStore().load({ params: this.renderedLists[i].getMyParams(), scope: this });
                break;
            }
        }
    }
});
