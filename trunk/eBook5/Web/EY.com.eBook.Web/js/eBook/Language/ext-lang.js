/*
  * List compiled by mystix on the extjs.com forums.
  * Thank you Mystix!
  *
  * Dutch Translations
  * by Ido Sebastiaan Bas van Oostveen (12 Oct 2007)
  */

Ext.UpdateManager.defaults.indicatorText = '<div class="loading-indicator">Bezig met laden...</div>';

Ext.ns('eBook.Language');
 
Ext.ns('eBook.Language.Common');

eBook.Language.Common.Client = 'Klant';
eBook.Language.Common.Supplier = 'Leverancier';
eBook.Language.Common.Ledger = 'Grootboek';
eBook.Language.Common.Account = 'Rekening';
eBook.Language.Common.Debet = 'Debet';
eBook.Language.Common.Credit = 'Credit';
eBook.Language.Common.Type = 'Type';
eBook.Language.Common.Start = 'Begin';
eBook.Language.Common.End = 'Einde';
eBook.Language.Common.Description = 'Omschrijving';
eBook.Language.Common.KeyName = 'KeyName';
eBook.Language.Common.Name = 'Naam';
eBook.Language.Common.LoadingDataFor = 'Bezig met het opladen van data voor ';
eBook.Language.Common.Worksheet = 'werkpapier ';
eBook.Language.Common.NotFound = ' niet gevonden';
eBook.Language.Common.AddNew = 'Toevoegen';
eBook.Language.Common.Status = 'Status';
eBook.Language.Common.TempContent = 'Tijdelijke inhoud';
eBook.Language.Common.GenerateState = 'Genereer';
eBook.Language.Common.ID = 'ID';
eBook.Language.Common.BookingID = 'BoekingID';
eBook.Language.Common.ImportData = 'Importeer gegevens';
eBook.Language.Common.WizardIncomplete = 'Onvolledig';
eBook.Language.Common.WizardIncompleteText = 'Dit onderdeel bevat nog onvolledige velden.';
eBook.Language.Common.WindowNotFound = 'venster not found! [';
eBook.Language.Common.Windows = '] venster:';
eBook.Language.Common.ImportFailed = 'Import mislukt: ';
eBook.Language.Common.ImportFailedText = 'Import mislukt: geen foutmeldingen gemeld.';
eBook.Language.Common.NotThere = "nog steeds niet aanwezig";
eBook.Language.Common.UndoChanges = 'Wijzigingen ongedaan maken';
eBook.Language.Common.Changed = 'De gegevens werden gewijzigd. Wenst u de wijzigingen op te slaan?';
eBook.Language.Common.DeleteFile = 'Bent u zeker dat u dit dossier wenst te verwijderen?';
eBook.Language.Common.DeleteFileTip = 'Dossier verwijderen';
eBook.Language.Common.MarkDeleteFileTip = 'Dossier markeren ter verwijdering';

eBook.Language.Common.Loading = 'Bezig met laden...';
eBook.Language.Common.Loading2 = 'Bezig met opladen...';
eBook.Language.Common.LoadingFailed = 'Laden mislukt.';
eBook.Language.Common.Saving = 'Bezig met opslaan';
eBook.Language.Common.SaveAndClose = 'Opslaan en sluiten';
eBook.Language.Common.Print = 'Print';
eBook.Language.Common.Save = 'Opslaan';
eBook.Language.Common.Fiche = 'Fiche';
eBook.Language.Common.Update = 'Bijwerken';
eBook.Language.Common.ExportExcel = 'Exporteer naar Excel';
eBook.Language.Common.Close = 'Sluiten';
eBook.Language.Common.Delete = 'Verwijder';
eBook.Language.Common.Refresh = 'Vernieuwen';
eBook.Language.Common.RefreshToolTip = 'Vernieuw de data';
eBook.Language.Common.CloseNoSave = 'Sluiten, niet bewaarde gegevens gaan verloren.';
eBook.Language.Common.CloseTooltip = 'Sluit dit scherm';
eBook.Language.Common.WizardPrevious = 'Vorige stap';
eBook.Language.Common.WizardPreviousTooltip = 'Vorige stap';
eBook.Language.Common.WizardNext = 'Volgende stap';
eBook.Language.Common.WizardNextTooltip = 'Volgende stap';
eBook.Language.Common.WizardSubmit = 'Verzend';
eBook.Language.Common.WizardSubmitTooltip = 'Verzend';
eBook.Language.Common.WizardCancel = 'Annuleren';
eBook.Language.Common.WizardCancelTooltip = 'Annuleren';
eBook.Language.Common.Find = 'Zoek';
eBook.Language.Common.FindTooltip = 'Type een text om te zoeken, druk op de &amp;Esc&amp toets om de filter te verwijderen';
eBook.Language.Common.SelectAllFields = 'Selecteer alle velden';
eBook.Language.Common.BusyDelete = 'Bezig met verwijderen.';
eBook.Language.Common.SaveChanges = 'Wijzigingen bewaren';
eBook.Language.Common.SaveYourChanges = 'Bewaar uw wijzigingen';
eBook.Language.Common.YourChangesAreBeingSaved = 'Uw wijzigingen worden bewaard';
eBook.Language.Common.SaveChangesFailed = 'De wijzigingen konden niet bewaard worden.';
eBook.Language.Common.WrongData = 'Foutieve gegevens';
eBook.Language.Common.WrongDataMessage = 'Enkele velden zijn nog foutief, kijk deze eerst na.';
eBook.Language.Common.DeleteItem = 'Verwijder item';
eBook.Language.Common.EmptyCollection = 'Collectie ledigen';
eBook.Language.Common.DataBeingLoaded =  'Gegevens worden ingeladen.';
eBook.Language.Common.ValueNotFound = 'Geen waarde gevonden...';
eBook.Language.Common.SelectValue = 'Selecteer een waarde...';
eBook.Language.Common.NoValidCultureLanguage = 'Geen juiste taal gevonden...';
eBook.Language.Common.ChooseCulture = 'Selecteer een taal...';
eBook.Language.Common.AddDescription = 'Voeg een omschrijving toe';
eBook.Language.Common.RemoveDescription = 'Voeg een omschrijving toe';
eBook.Language.Common.NoCustomerSupplierFound = 'Geen klant of leverancier gevonden...';
eBook.Language.Common.SelectCustomerSupplierFound = 'Selecteer een klant of leverancier';
eBook.Language.Common.DescriptionTranslation = 'Omschrijvingen/Vertalingen'; 
eBook.Language.Common.ImportFrom = 'Importeer uit';
eBook.Language.Common.SelectImportType = 'Selecteer importtype...'; 
eBook.Language.Common.ImportFrom = 'Importeer uit';
eBook.Language.Common.FileCreationImportError = 'Dossier creatie - Import';
eBook.Language.Common.FileCreationImportErrorMessage = 'Het dossier kon niet worden gecreëerd wegens een serverfout: '; 
eBook.Language.Common.ReloadStatus = 'Data wordt geïmporteerd';
eBook.Language.Common.PreparingInterface = 'Bezig met het voorbereiden van de interface...';
eBook.Language.Common.DataLoading = 'wordt ingeladen';
eBook.Language.Common.NotFound = 'niet gevonden';
eBook.Language.Common.Add = 'Toevoegen';
eBook.Language.Common.Load = 'Opladen';
eBook.Language.Common.LoadingFile = 'bestand wordt opgeladen';
eBook.Language.Common.Importing = 'Data wordt geïmporteerd, even geduld a.u.b.';
eBook.Language.Common.ImportingStart = 'Import initialize';

eBook.Language.Common.FileInQueue = 'Bestand <b>{0}</b> staat in de wachtlijst om op te laden';
eBook.Language.Common.FileSuccess = 'Bestand <b>{0}</b> is succesvol opgeladen' ;
eBook.Language.Common.FileFailed = 'Bestand <b>{0}</b> kon niet worden opgeladen';
eBook.Language.Common.FileStopped = 'Opladen van bestand <b>{0}</b> is gestopt door gebruiker';
eBook.Language.Common.FileLoading = 'Bezig met het opladen van dit bestand';
eBook.Language.Common.EraseFileList = 'Op te laden lijst wissen';
eBook.Language.Common.FileClickToDeleteFromList = 'Klik om bestand uit de lijst te verwijderen';
eBook.Language.Common.FileClickToStop = 'Klik om opladen te stoppen';
eBook.Language.Common.LoadPdf = 'Pdf bestand(-en) opladen';

eBook.Language.Common.Generating = 'De bundel wordt gegenereerd';
eBook.Language.Common.Generate = 'Genereer bundel';
eBook.Language.Common.Messages = 'Meldingen';
eBook.Language.Common.ImportHistory = 'Import history';
eBook.Language.Common.ImportingHistory = 'Importing history';

// Algemene gegevens
eBook.Language.Common.LastName = 'Naam';  
eBook.Language.Common.Address = 'Adres';  
eBook.Language.Common.City = 'Woonplaats';    
eBook.Language.Common.BirthDate = 'Geboortedatum'; 
eBook.Language.Common.ZipCode = 'Postcode';  
eBook.Language.Common.Country = 'Land';  
eBook.Language.Common.VatNr = 'BTW Nummer';  
eBook.Language.Common.Date = 'Datum';  
eBook.Language.Common.Description = 'Omschrijving'; 
eBook.Language.Common.AccountNr = 'Rekeningnummer';  
eBook.Language.Common.Number = 'Nr';   
eBook.Language.Common.Debet = 'Debet'; 
eBook.Language.Common.Credit = 'Credit';   
eBook.Language.Common.Save = 'Bewaren';  
eBook.Language.Common.Cancel = 'Annuleren';   
eBook.Language.Common.Actions = 'Acties';   
eBook.Language.Common.Language = 'Taal';

// Menu items
Ext.ns('eBook.Language.Menu');
eBook.Language.Menu.FileClosing = 'Finale afsluiting dossier';
eBook.Language.Menu.PermanentMeta = 'Permanente gegevens'; //permanentegegevensaanvullen
eBook.Language.Menu.Suppliers = 'Leveranciers'; //leveranciers
eBook.Language.Menu.Clients = 'Klanten'; //klanten
eBook.Language.Menu.Personnel = 'Personneel'; //personneel
eBook.Language.Menu.Files = 'Dossiers'; //dossier
eBook.Language.Menu.NewFile = 'Creëer dossier'; //creeerdossier
eBook.Language.Menu.ExistingFiles = 'Bestaande dossiers';
eBook.Language.Menu.Ledger = 'Adjustments & beheer rekeningen'; //grootboek
eBook.Language.Menu.AccountMeta = 'Mapping algemene rekeningen'; //rekeningenmetabeheren;
eBook.Language.Menu.AccountManagement = 'Beheer algemene rekeningen'; //rekeningenbeheren;
eBook.Language.Menu.PreviousJournal = 'Eindbalans vorig boekjaar'; //lastyearjournaal;
eBook.Language.Menu.OpeningsJournal = 'Openingsjournaal'; //openingsjournaal;
eBook.Language.Menu.ManualJournal = 'Adjustments - handmatig'; //diversjournaalmanual;
eBook.Language.Menu.AutomaticJournal = 'Adjustments - werkpapieren'; //diversjournaalauto;
eBook.Language.Menu.FinalTrialBalance = 'Finale proef- en saldibalans'; //FinalTrialBalance;
eBook.Language.Menu.MessageOverview = 'Controle werkpapieren'; //messagestitle;
eBook.Language.Menu.WorksheetsAL = 'Algemene werkpapieren'; //worksheetsAL;
eBook.Language.Menu.WorksheetsFI = 'Fiscale werkpapieren'; //worksheetsFI;
eBook.Language.Menu.WorksheetsFPDF = 'Formulier werkpapieren'; //worksheetsFPDF;
eBook.Language.Menu.ExtraFiles = 'Overige werkpapieren'; //extraFiles;
eBook.Language.Menu.Fiches = 'Formulieren'; //fiches;
eBook.Language.Menu.Documents = 'Documenten'; //documents;
eBook.Language.Menu.ReportGeneration = 'Genereren bundel jaarrekening'; //genererenbundeljaarrekening;
eBook.Language.Menu.GeneratedDocuments = 'Finale bundels (PDF)';

Ext.ns('eBook.Language.FileClosing');
eBook.Language.FileClosing.Title = eBook.Language.Menu.FileClosing;
eBook.Language.FileClosing.CloseBookingsTitle = 'Stap 1: definitieve afsluiting v/d cijfers';
eBook.Language.FileClosing.CloseBookings = 'Druk op onderstaande knop om de cijfers (journaals) definitief af te sluiten';
eBook.Language.FileClosing.CloseBookingsPerform = 'Definitieve afsluiting v/d cijfers';
eBook.Language.FileClosing.CloseReportTitle = 'Stap 2: definitieve afsluiting van bundel(s)';
eBook.Language.FileClosing.UpdateClosedFiles = 'Update lijst finale bundels';
eBook.Language.FileClosing.UpdateClientFiles = 'Update dossier';
eBook.Language.FileClosing.ForceCloseWarning = 'Warning forced closing';
eBook.Language.FileClosing.ForceCloseWarningTitle = 'Warning forced closing title';
eBook.Language.FileClosing.ForceClosePerform = 'Warning forced perform';

// Employees
Ext.ns('eBook.Language.Employees');
eBook.Language.Employees.Employees = 'Personeel';
eBook.Language.Employees.NewEmployee  = 'Nieuw personeelslid';
eBook.Language.Employees.NewEmployeeTooltip  = 'Een nieuw personeelslid toevoegen';
eBook.Language.Employees.EditEmployee  = 'Wijzig personeelslid';
eBook.Language.Employees.EditEmployeeTooltip = 'Het geselecteerde personeelslid wijzigen';  
eBook.Language.Employees.DeleteEmployee  = 'Verwijder personeelslid';
eBook.Language.Employees.DeleteEmployeeTooltip = 'Het geselecteerde personeelslid verwijderen';  
eBook.Language.Employees.ColumnName = 'Naam';
eBook.Language.Employees.ColumnAddress = 'Adres';
// Business Relations
Ext.ns('eBook.Language.BusinessRelations');
eBook.Language.BusinessRelations.ReloadData = 'Update klanten & leveranciers';
eBook.Language.BusinessRelations.NewBusinessRelation  = 'Nieuwe zakenrelatie';
eBook.Language.BusinessRelations.NewBusinessRelationTooltip  = 'Een nieuwe zakenrelatie toevoegen';
eBook.Language.BusinessRelations.EditNewBusinessRelation  = 'Wijzig zakenrelatie';
eBook.Language.BusinessRelations.EditNewBusinessRelationTooltip = 'De geselecteerde zakenrelatie wijzigen';  
eBook.Language.BusinessRelations.DeleteNewBusinessRelation  = 'Verwijder zakenrelatie';
eBook.Language.BusinessRelations.DeleteNewBusinessRelationTooltip = 'De geselecteerde zakenrelatie verwijderen';  
  
// Leverancier
Ext.ns('eBook.Language.Suppliers');
eBook.Language.Suppliers.Suppliers  = 'Leveranciers';
eBook.Language.Suppliers.NewSupplier  = 'Nieuwe leverancier';
eBook.Language.Suppliers.NewSupplierTooltip  = 'Een nieuwe leverancier toevoegen';
eBook.Language.Suppliers.EditNewSupplier  = 'Wijzig leverancier';
eBook.Language.Suppliers.EditNewSupplierTooltip = 'De geselecteerde leverancier wijzigen';  
eBook.Language.Suppliers.DeleteNewSupplier  = 'Verwijder leverancier';
eBook.Language.Suppliers.DeleteNewSupplierTooltip = 'De geselecteerde leverancier verwijderen';  
eBook.Language.Suppliers.ColumnName = 'Naam';
eBook.Language.Suppliers.ColumnCompanyNumber = 'Ondernemingsnummer';
eBook.Language.Suppliers.ColumnAddress = 'Adres';
eBook.Language.Suppliers.ColumnCity = 'Woonplaats';
eBook.Language.Suppliers.ColumnCountry = 'Land';

// Customers    
Ext.ns('eBook.Language.Customers');
eBook.Language.Customers.Customers  = 'Klanten';
eBook.Language.Customers.NewCustomer  = 'Nieuwe klant';
eBook.Language.Customers.NewCustomerTooltip  = 'Een nieuwe klant toevoegen';
eBook.Language.Customers.EditCustomer  = 'Wijzig klant';
eBook.Language.Customers.EditCustomerTooltip = 'De geselecteerde klant wijzigen';  
eBook.Language.Customers.DeleteCustomer  = 'Verwijder klant';
eBook.Language.Customers.DeleteCustomerTooltip = 'De geselecteerde klant verwijderen';  
eBook.Language.Customers.ColumnName = 'Naam';
eBook.Language.Customers.ColumnCompanyNumber = 'Ondernemingsnummer';
eBook.Language.Customers.ColumnAddress = 'Adres';
eBook.Language.Customers.ColumnCity = 'Woonplaats';
eBook.Language.Customers.ColumnCountry = 'Land';
eBook.Language.Customers.CheckImportType = 'Importtype wordt onderzocht of deze mogelijk is voor deze klant.';
eBook.Language.Customers.CustomerFoundImportType = 'De klant werd gevonden voor het importtype \'';
eBook.Language.Customers.CustomerNotFoundImportType = 'De klant kon niet worden gevonden voor het importtype \'';
eBook.Language.Customers.CustomerNotFound = 'Klant werd niet gevonden.';
// Accounts
Ext.ns('eBook.Language.Account');
eBook.Language.Account.AccountNumber = 'Rekeningnummer';
eBook.Language.Account.CreateAccountNumber = 'Rekening aanmaken';
eBook.Language.Account.EditAccountNumber = 'Rekening editeren';
eBook.Language.Account.AddAccount = 'Toevoegen van rekening';
eBook.Language.Account.AddAccountMessage = 'Voor het toevoegen van een nieuwe rekening dient enkel het rekeningnummer te worden ingevuld, geen tekst.';
eBook.Language.Account.AddAccountFailed = 'Aanmaak van rekening mislukt.';
eBook.Language.Account.AddAccountFailedMessage = 'De rekening kon niet worden aangemaakt.';
eBook.Language.Account.AccountAlreadyExists = 'Rekening bestaat reeds.';
eBook.Language.Account.AccountAlreadyExistsMessage = 'Deze rekening is reeds gekend.';
eBook.Language.Account.AccountDoesntExists = 'Rekening bestaat niet.';
eBook.Language.Account.AccountDoesntExistsMessage = 'Deze rekening bestaat nog niet, kies \'Rekening aanmaken\'.';
eBook.Language.Account.AccountSaved = 'Rekening bewaard';
eBook.Language.Account.AccountSavedMessage = 'De gegevens werden succesvol bewaard';
eBook.Language.Account.AccountNotSaved = 'Rekening niet bewaard';
eBook.Language.Account.AccountNotSavedMessage = 'De gegevens konden niet worden bewaard, kijk uw gegevens na.';
eBook.Language.Account.SelectAccount = 'Selecteer een rekening...';
eBook.Language.Account.ImportFailed = 'Het is niet mogelijk om de data te importeren';
eBook.Language.Account.MetaData = 'Mappingen';
eBook.Language.Account.MetaDataKey = 'Mappingstype';
eBook.Language.Account.MetaDataValue = 'Waarde';
eBook.Language.Account.History = 'Historiek';
eBook.Language.Account.Translations = 'Vertalingen';
Ext.ns('eBook.Language.Account.Translation');
eBook.Language.Account.Translation.AlreadyForEachLanguage = 'Elke beschikbare taal heeft reeds een omschrijving.';
eBook.Language.Account.Translation.MinimumOne = 'Gelieve een omschrijving in te geven voor uw huidige taal.';
eBook.Language.Account.Translation.Duplicate = 'Meerdere omschrijvingen voor één taal zijn niet toegelaten.';
eBook.Language.Account.SearchAccount = 'Rekening zoeken';

//Dossiergegevens
Ext.ns('eBook.Language.FileData');
eBook.Language.FileData.ExtraFileData = 'Extra dossiergegevens';
eBook.Language.FileData.PermanentData ='Dossier Permanente gegevens';
eBook.Language.FileData.FileData = 'Dossiergegevens';
eBook.Language.FileData.NrOfEmployeesOnFirstDay = 'Aantal werknemers op de eerste dag belastbaar tijdperk';
eBook.Language.FileData.PaidVacationWage = 'Vakantiegeld';
eBook.Language.FileData.PaidVacationWageBookedCustomer = 'Vakantiegeld geboekt door klant';
eBook.Language.FileData.PaidVacationWagePC = 'Paritair comité 124/126 (bouw)';
eBook.Language.FileData.LegalSocietyAndReport = 'Wettelijke vennootschappen en verslagen';
eBook.Language.FileData.SmallEnterprise = 'Kleine onderneming';
eBook.Language.FileData.ControlledCommisioner = 'Gecontroleerd door commissaris';
eBook.Language.FileData.FreeSubConsolidation  = 'Vrijstelling sub-consilidatie';
eBook.Language.FileData.CounsilManagers = 'College van zaakvoerders';
eBook.Language.FileData.AllowedCapital = 'Toegestaan kapitaal (gifpil)';
eBook.Language.FileData.ObligationsWarrants = 'Converteerbare obligaties';
eBook.Language.FileData.WizardNewFile = 'Wizard -- Creëer nieuw dossier';
eBook.Language.FileData.MakeNewFile = 'Aanmaken van een nieuw dossier'; 
eBook.Language.FileData.FileCreation = 'Dossiercreatie'; 
eBook.Language.FileData.FileType = 'Dossiertype'; 
eBook.Language.FileData.SelectFileType = 'Selecteer type...'; 
eBook.Language.FileData.ShortDescription = 'Korte omschrijving';
eBook.Language.FileData.CurrentBookyear = ' Huidig boekjaar ';
eBook.Language.FileData.LastBookyear = ' Vorig boekjaar ';
eBook.Language.FileData.StartDateBookyear = 'Startdatum boekjaar';
eBook.Language.FileData.EndDateBookyear = 'Einddatum boekjaar';
eBook.Language.FileData.FileCreationImport = 'Dossier creatie - Import';
eBook.Language.FileData.FileCreationError = 'Het dossier kon niet worden gecreëerd wegens een serverfout: ';
eBook.Language.FileData.NoLastBookyear = 'Geen vorig boekjaar (huidig=eerste)';
eBook.Language.FileData.ImportFile = 'Te importeren bestand';
eBook.Language.FileData.MetaData = 'Paritair comité voor het houtbedrijf / de stoffering en houtbewerking';
eBook.Language.FileData.Art15 = 'Art. 15 W.Venn';
eBook.Language.FileData.Art14 = 'Art. 141 W.Venn';
eBook.Language.FileData.Art13 = 'Art. 113 W.Venn';
eBook.Language.FileData.MoreThanOneManager = 'Bestuur door meerdere zaakvoerders gezamelijk';
eBook.Language.FileData.Art603 = 'Art. 603 W.Venn';
eBook.Language.FileData.CurrentBookPeriod = 'Huidige boekhoudperiode';
eBook.Language.FileData.BookYear = 'Boekjaar';
eBook.Language.Common.From = 'Van';
eBook.Language.Common.To = 'Tot';


Ext.ns('eBook.Language.File.Messages');
eBook.Language.File.Messages.Title = 'Overzicht werkpapieren';
eBook.Language.File.Messages.NoMessages = 'Geen meldingen gevonden';
eBook.Language.File.Messages.WorksheetFilter = 'Filter werkpapieren';
eBook.Language.File.Messages.AllWorksheets = 'Alle werkpapieren';
eBook.Language.File.Messages.WorksheetsAL = 'Algemene werkpapieren';
eBook.Language.File.Messages.WorksheetsFI = 'Fiscale werkpapieren';
eBook.Language.File.Messages.WorksheetsFPDF = 'Formulier werkpapieren';
eBook.Language.File.Messages.Reanalyze = 'Heranalyseer alle werkpapieren';
eBook.Language.File.Browse = 'Kies een bestand op uw PC';
eBook.Language.File.XtraWorksheets = 'Overige werkpapieren';
eBook.Language.File.XtraWorksheetsInvalid = 'Enkel Excel & Word documenten zijn toegelaten.';
eBook.Language.File.XtraWorksheetsUploading = 'Document wordt opgeladen.';

Ext.ns('eBook.Language.File');
eBook.Language.File.Correct = 'Geldig bestand, extentie';
eBook.Language.File.Invalid = 'Ongeldig bestand, extentie';
eBook.Language.File.Error = 'er is een fout opgetreden';
eBook.Language.File.CreatingFile = 'Dossier wordt aangemaakt';
eBook.Language.File.NoWorksheetsFound = 'Geen werkpapieren gevonden';
eBook.Language.File.Ready = 'klaar';
eBook.Language.File.LastYearNoFile = 'Vorig boekjaar, zonder eBook dossier';

//Bookings
Ext.ns('eBook.Language.Bookings');
eBook.Language.Bookings.ReloadOpening = 'Update saldi uit ProAcc';
eBook.Language.Bookings.ReloadOpeningStatus = 'Data wordt geïmporteerd';
eBook.Language.Bookings.Auto = 'Automatische boekingen';
eBook.Language.Bookings.Manual = 'Manuele boekingen';
eBook.Language.Bookings.NoTypeFound = 'Geen type gevonden...';
eBook.Language.Bookings.SelectBookingType = 'Selecteer een boekingstype';
eBook.Language.Bookings.NoSupplierFound = 'Geen leverancier gevonden...';
eBook.Language.Bookings.SelectSupplier = 'Selecteer een leverancier';
eBook.Language.Bookings.AccountColumn = 'Rekening';
eBook.Language.Bookings.Type = 'Type';
eBook.Language.Bookings.Print = 'Print';

eBook.Language.Bookings.DebetColumn = 'Debet';
eBook.Language.Bookings.CreditColumn = 'Credit';
eBook.Language.Bookings.AddBookingLine = 'Voeg een boekingslijn toe';
eBook.Language.Bookings.WrongBooking = 'Foutieve boeking';
eBook.Language.Bookings.WrongBookingMessage = 'U tracht een foutieve boeking op te slaan, kijk uw gegevens en debet-credit saldo na.';
eBook.Language.Bookings.BookingType = 'Boekingstype';
eBook.Language.Bookings.BookingDate = 'Boekingsdatum';
eBook.Language.Bookings.BookingDescription = 'Omschrijving';
eBook.Language.Bookings.BusinessRelation = 'Zakenrelatie';
eBook.Language.Bookings.DescriptionMandatory = 'Omschrijving is vereist.';
eBook.Language.Bookings.TypeMandatory = 'Boekingstype is vereist.';
eBook.Language.Bookings.DateMandatory = 'Datum is vereist.';

eBook.Language.Bookings.Header = 'Boekingen';
eBook.Language.Bookings.EditBooking = 'Wijzig deze boeking';
eBook.Language.Bookings.NewBooking = 'Nieuwe boeking';
eBook.Language.Bookings.EditImportedBooking = 'Wijzig geïmporteerde boeking';
eBook.Language.Bookings.EditImportedBookingMessage = 'Dit is een geïmporteerde boeking. Deze kan niet gewijzigd worden.';
eBook.Language.Bookings.DeleteBooking = 'Verwijder deze boeking';
eBook.Language.Bookings.NewBooking = 'Nieuwe boeking';
eBook.Language.Bookings.DeleteBookingFailedText = 'Verwijderen boeking';
eBook.Language.Bookings.DeleteBookingFailedMessage = 'De boeking kon niet verwijderd worden wegens een serverfout: ';
eBook.Language.Bookings.DeleteOpeningBookingFailedText = 'Verwijderen openingsboeking';
eBook.Language.Bookings.DeleteOpeningBookingFailedMessage = 'Dit is een openingsboeking. Deze kan niet verwijderd worden.';
eBook.Language.Bookings.DeleteAutomaticBookingFailedMessage = 'Dit is een automatische boeking. Deze kan enkel door het desbetreffende werkpapier verwijderd worden.';
eBook.Language.Bookings.NotSaved = 'Boeking niet opgeslagen';
eBook.Language.Bookings.SaveFailed = 'De boeking kon niet worden opgeslagen.';
eBook.Language.Bookings.ImportFailed = 'Het is niet mogelijk om de data te importeren';
eBook.Language.Bookings.DelBooking = 'Verwijderen van een boeking';

eBook.Language.Bookings.EditBookingFailedText = 'Wijzigen boeking'; 
eBook.Language.Bookings.EditOpeningBookingFailedMessage = 'Dit is een openingsboeking. Deze kan enkel vanuit het openingsjournaal gewijzigd worden.';
eBook.Language.Bookings.EditImportedBookingFailedText = 'Geïmporteerde boeking wijzigen';
eBook.Language.Bookings.EditImportedBookingFailedMessage = 'Dit is een geïmporteerde boeking. Deze kan niet gewijzigd worden.';
eBook.Language.Bookings.SaveChanges = 'De boeking is gewijzigd. Wijzigingen opslaan?';
eBook.Language.Bookings.SaveChangesTitle = 'Adjustment';

Ext.ns('eBook.Language.Journal');
eBook.Language.Journal.OpeningJournal = 'Openingsjournaal';
eBook.Language.Journal.DiversJournal = 'Adjustments'; //'Divers journaal'
eBook.Language.Journal.Administration = 'Beheer journaalrekeningen';

Ext.ns('eBook.Language.Client');
eBook.Language.Client.GfisClientNumber = 'GFIS Clientnummer';
eBook.Language.Client.ClientLoading = 'Client wordt geladen...';
eBook.Language.Client.PermanentData  = 'Client permanente gegevens';
eBook.Language.Client.SearchClient = 'Zoek een client: ';
eBook.Language.Client.SearchClientMessage = 'Type een text om te zoeken, druk op de &amp;Esc&amp toets om de filter te verwijderen';
eBook.Language.Client.FailedToFindClient = 'De client werd niet gevonden.';
eBook.Language.Client.ExistingFiles = 'Bestaande dossiers';


Ext.ns('eBook.Language.AccountMeta','eBook.Language.AccountMeta.Grid');
eBook.Language.AccountMeta.DetailedView = 'Detailoverzicht';
eBook.Language.AccountMeta.HideZeroSaldi = 'Saldi \'0\' verbergen';
eBook.Language.AccountMeta.Title = 'Beheer mapping van algemene rekeningen';
eBook.Language.AccountMeta.ExportToExcel = 'Export naar Excel';

eBook.Language.AccountMeta.Grid.Account = 'Rekening';
eBook.Language.AccountMeta.Grid.Balance = 'Saldo';
eBook.Language.AccountMeta.Grid.NotionalIntrestDeduction = 'Aftrek risicokapitaal';
eBook.Language.AccountMeta.Grid.Investments = 'Investering rekeningen';
eBook.Language.AccountMeta.Grid.DisallowedExpenses = 'Verworpen uitgaven';
eBook.Language.AccountMeta.Grid.SalaryReconciliation = 'Salaris aansluiting';
eBook.Language.AccountMeta.Grid.VatReconciliation = 'BTW-omzet aansluting';
eBook.Language.AccountMeta.Grid.AdvantagesInKind = 'Voordelen alle aard';
eBook.Language.AccountMeta.Grid.Fees = 'Erelonen';
eBook.Language.AccountMeta.Grid.IntrestbearingAdvances = 'Rentegevende voorschotten';
eBook.Language.AccountMeta.Grid.BelasteReserves='Belaste reserves';
eBook.Language.AccountMeta.Grid.DBI = 'DBI';

Ext.ns('eBook.Language.Worksheets');
eBook.Language.Worksheets.ExtraFiles = 'Voeg document toe';
eBook.Language.Worksheets.AddItem = 'Item toevoegen';
eBook.Language.Worksheets.AddItemToolTip = 'Voeg een item toe';
eBook.Language.Worksheets.EditItem = 'Item bewerken';
eBook.Language.Worksheets.EditItemToolTip = 'Bewerk het geselecteerde item';
eBook.Language.Worksheets.DeleteItem = 'Item verwijderen';
eBook.Language.Worksheets.DeleteItemToolTip = 'Verwijder het geselecteerde item';
eBook.Language.Worksheets.DeletingItem = 'Item wordt verwijderd.';
eBook.Language.Worksheets.DeleteItemFailed = 'Kon item niet niet verwijderen.';
eBook.Language.Worksheets.EditForm = 'Gegevens bewerken';
eBook.Language.Worksheets.EditFormToolTip = 'Bewerk deze gegevens';
eBook.Language.Worksheets.RetrievingData = 'Gegevens worden opgehaald.';
eBook.Language.Worksheets.ExecutingRules = 'Bewerkingen worden uitgevoerd.';
eBook.Language.Worksheets.UpdatingDataAndExecutingRules = 'De gegevens worden bijgewerkt en bewaard.';
eBook.Language.Worksheets.FaultyFieldsTitle = 'Foutieve data';
eBook.Language.Worksheets.FaultyFieldsMsg = 'Enkele velden zijn niet correct.';
eBook.Language.Worksheets.ErrorSavingTitle = 'Bewaren van formulier data';
eBook.Language.Worksheets.ErrorSavingMsg = 'Formulier data kon niet worden bewaard.';
eBook.Language.Worksheets.SaveWorksheet = 'Bewaren';
eBook.Language.Worksheets.SaveCloseWorksheet = 'Bewaren & sluiten';
eBook.Language.Worksheets.LockingWorksheet = 'Het werkpapier wordt ingeladen.';
eBook.Language.Worksheets.LockingWorksheetFailed = 'Het werkpapier kon niet worden vergrendeld. ';
eBook.Language.Worksheets.WorksheetLockedBy = 'Dit werkpapier is reeds vergrendeld door: ';
eBook.Language.Worksheets.Worksheet = 'Werkpapier';
eBook.Language.Worksheets.ClosingWorksheet = 'Het werkpapier wordt afgesloten.';
eBook.Language.Worksheets.SavingDataAndExecutingRules = 'Het werkpapier wordt bewaard.';
eBook.Language.Worksheets.SavingDataAndExecutingRulesError = 'Het werkpapier kon niet worden bewaard. ';
eBook.Language.Worksheets.Changed = 'Het werkpapier is gewijzigd. Wijzigingen opslaan?';

Ext.ns('eBook.Language.FinalTrialBalance');
eBook.Language.FinalTrialBalance.Export = 'Exporteren';
eBook.Language.FinalTrialBalance.ExportToExcel = 'Exporteren naar Excel';
eBook.Language.FinalTrialBalance.AccountName = 'Rekening';
eBook.Language.FinalTrialBalance.StartAmount = 'Balans voor adjustments'; 
eBook.Language.FinalTrialBalance.Debet = 'Adjustments Debet';
eBook.Language.FinalTrialBalance.Credit = 'Adjustments Credit';
eBook.Language.FinalTrialBalance.EndAmount = 'Saldo na adjustments';
eBook.Language.FinalTrialBalance.FinalTrialBalance = 'Finale proef- en saldibalans';

Ext.ns('eBook.Language.Documents');
eBook.Language.Documents.PrintReport = 'Print verslag';
eBook.Language.Documents.SaveData = 'Bewaar data';
eBook.Language.Documents.AcrobatNotInstalled = 'Acrobat Viewer is niet geïnstalleerd';
eBook.Language.Documents.Loading = 'Het document wordt ingeladen ...';
eBook.Language.Documents.Dropped = 'dropped';                       // translation ?
eBook.Language.Documents.EditParagraph = 'Wijzig alinea';
eBook.Language.Documents.DeleteParagraph = 'Verwijder alinea';
eBook.Language.Documents.MoveUp = 'Omhoog';
eBook.Language.Documents.MoveDown = 'Omlaag';
eBook.Language.Documents.Paragraph = 'Alinea';

Ext.ns('eBook.Language.Forms');
eBook.Language.Forms.NoData = 'Dit formulier bevat geen data. Wilt u data toevoegen?';
eBook.Language.Forms.OpenFailed = 'Het is niet mogelijk om het formulier te openen. Contacteer de helpdesk.';
eBook.Language.Forms.Title = 'Formulieren';
eBook.Language.Forms.SelectRows = 'Selecteer rijen';
eBook.Language.Forms.SelectAll = 'Selecteer alle rijen';
eBook.Language.Forms.SelectThese = 'Selecteer enkel deze rijen';
eBook.Language.Forms.Test = 'test';

Ext.ns('eBook.Language.MetaData');
eBook.Language.MetaData.ItemContainsError = 'Dit onderdeel bevat fouten.';
eBook.Language.MetaData.Update = 'Bijwerken metadata.';
eBook.Language.MetaData.UpdateFailed = 'Bijwerken van de metadata gefaald';
eBook.Language.MetaData.UpdateFailedMsg = 'Het bijwerken van de metadata is gefaald!';
eBook.Language.MetaData.Title = 'Metadata entiteit';

Ext.ns('eBook.Language.Reports');
eBook.Language.Reports.Titel = 'Titel:';
eBook.Language.Reports.FromPage = 'vanaf p.';
eBook.Language.Reports.ToPage = 'tot en met p.';
eBook.Language.Reports.ShowHeading = 'Toon hoofding:';
eBook.Language.Reports.ClientLayout = 'Klant Layout';
eBook.Language.Reports.FiscusLayout = 'Fiscus Layout';
eBook.Language.Reports.EmptyLayout = 'Lege Layout';
eBook.Language.Reports.Draft = 'Draft';
eBook.Language.Reports.Footer = 'Footer?';
eBook.Language.Reports.FromTo = 'van x tot x';
eBook.Language.Reports.P1 = ' [p.';
eBook.Language.Reports.P2 = ' - p.';

Ext.ns('eBook.Language.Data');
eBook.Language.Data.Error = 'Data bulk load';
eBook.Language.Data.ErrorNoXml = 'failure no xml';
eBook.Language.Data.ErrorRequest = 'failure request';

Ext.ns('eBook.Language.Loadsequence');
eBook.Language.Loadsequence.Initializing = 'Bezig met initialiseren';
 

if(Ext.View){
  Ext.View.prototype.emptyText = '';
}

if(Ext.grid.GridPanel){
  Ext.grid.GridPanel.prototype.ddText = '{0} geselecteerde rij(en)';
}

if(Ext.TabPanelItem){
  Ext.TabPanelItem.prototype.closeText = 'Sluit dit tabblad';
}

if(Ext.LoadMask){
  Ext.LoadMask.prototype.msg = 'Bezig met laden...';
}

Date.monthNames = [
  'januari',
  'februari',
  'maart',
  'april',
  'mei',
  'juni',
  'juli',
  'augustus',
  'september',
  'oktober',
  'november',
  'december'
];

Date.getShortMonthName = function(month) {
  if (month == 2) {
  return 'mrt';
  }
  return Date.monthNames[month].substring(0, 3);
};

Date.monthNumbers = {
  jan: 0,
  feb: 1,
  maa: 2,
  apr: 3,
  mei: 4,
  jun: 5,
  jul: 6,
  aug: 7,
  sep: 8,
  okt: 9,
  nov: 10,
  dec: 11
};

Date.getMonthNumber = function(name) {
  var sname = name.substring(0, 3).toLowerCase();
  if (sname == 'mrt') {
    return 2;
  }
  return Date.monthNumbers[sname];
};

Date.dayNames = [
  'zondag',
  'maandag',
  'dinsdag',
  'woensdag',
  'donderdag',
  'vrijdag',
  'zaterdag'
];

Date.getShortDayName = function(day) {
  return Date.dayNames[day].substring(0, 3);
};

if(Ext.MessageBox){
  Ext.MessageBox.buttonText = {
    ok: 'OK',
    cancel: 'Annuleren',
    yes: 'Ja',
    no: 'Nee'
  };
}

if(Ext.util.Format){
  Ext.util.Format.date = function(v, format){
    if (!v) return '';
    if (!(v instanceof Date)) v = new Date(Date.parse(v));
    return v.dateFormat(format || 'j-m-y');
  };
}

if(Ext.DatePicker){
  Ext.apply(Ext.DatePicker.prototype, {
    todayText: 'Vandaag',
    minText: 'Deze datum is eerder dan de minimale datum',
    maxText: 'Deze datum is later dan de maximale datum',
    disabledDaysText: '',
    disabledDatesText: '',
    monthNames: Date.monthNames,
    dayNames: Date.dayNames,
    nextText: 'Volgende maand (Ctrl+rechts)',
    prevText: 'Vorige maand (Ctrl+links)',
    monthYearText: 'Kies een maand (Ctrl+omhoog/omlaag volgend/vorig jaar)',
    todayTip: '{0} (spatie)',
    format: 'j-m-y',
    okText: '&#160;OK&#160;',
    cancelText: 'Annuleren',
    startDay: 1
  });
}

if(Ext.PagingToolbar){
  Ext.apply(Ext.PagingToolbar.prototype, {
    beforePageText: 'Pagina',
    afterPageText: 'van {0}',
    firstText: 'Eerste pagina',
    prevText: 'Vorige pagina',
    nextText: 'Volgende pagina',
    lastText: 'Laatste pagina',
    refreshText: 'Ververs',
    displayMsg: 'Getoond {0} - {1} van {2}',
    emptyMsg: 'Geen gegevens om weer te geven'
  });
}

if(Ext.form.Field){
  Ext.form.Field.prototype.invalidText = 'De waarde van dit veld is ongeldig';
}

if(Ext.form.TextField){
  Ext.apply(Ext.form.TextField.prototype, {
    minLengthText: 'De minimale lengte van dit veld is {0}',
    maxLengthText: 'De maximale lengte van dit veld is {0}',
    blankText: 'Dit veld is verplicht',
    regexText: '',
    emptyText: null
  });
}

if(Ext.form.NumberField){
  Ext.apply(Ext.form.NumberField.prototype, {
    minText: 'De minimale waarde van dit veld is {0}',
    maxText: 'De maximale waarde van dit veld is {0}',
    nanText: '{0} is geen geldig getal'
  });
}

if(Ext.form.DateField){
  Ext.apply(Ext.form.DateField.prototype, {
    disabledDaysText: 'Uitgeschakeld',
    disabledDatesText: 'Uitgeschakeld',
    minText: 'De datum in dit veld moet na {0} liggen',
    maxText: 'De datum in dit veld moet voor {0} liggen',
    invalidText: '{0} is geen geldige datum - formaat voor datum is {1}',
    format: 'j-m-y',
    altFormats: 'd/m/Y|d/m/Y|d/m/Y|d/m|d-m|dm|dmy|dmY|d|Y-m-d'
  });
}

if(Ext.form.ComboBox){
  Ext.apply(Ext.form.ComboBox.prototype, {
    loadingText: 'Bezig met laden...',
    valueNotFoundText: undefined
  });
}

if(Ext.form.VTypes){
  Ext.apply(Ext.form.VTypes, {
    emailText: 'Dit veld moet een e-mail adres bevatten in het formaat "gebruiker@domein.nl"',
    urlText: 'Dit veld moet een URL bevatten in het formaat "http:/'+'/www.domein.nl"',
    alphaText: 'Dit veld mag alleen letters en _ bevatten',
    alphanumText: 'Dit veld mag alleen letters, cijfers en _ bevatten'
  });
}

if(Ext.form.HtmlEditor){
  Ext.apply(Ext.form.HtmlEditor.prototype, {
  createLinkText: 'Vul hier de URL voor de hyperlink in:',
  buttonTips: {
      bold: {
        title: 'Vet (Ctrl+B)',
        text: 'Maak de geselecteerde tekst vet.',
        cls: 'x-html-editor-tip'
      },
      italic: {
        title: 'Cursief (Ctrl+I)',
        text: 'Maak de geselecteerde tekst cursief.',
        cls: 'x-html-editor-tip'
      },
      underline: {
        title: 'Onderstrepen (Ctrl+U)',
        text: 'Onderstreep de geselecteerde tekst.',
        cls: 'x-html-editor-tip'
      },
      increasefontsize: {
        title: 'Tekst vergroten',
        text: 'Vergroot het lettertype.',
        cls: 'x-html-editor-tip'
      },
      decreasefontsize: {
        title: 'Tekst verkleinen',
        text: 'Verklein het lettertype.',
        cls: 'x-html-editor-tip'
      },
      backcolor: {
        title: 'Tekst achtergrondkleur',
        text: 'Verander de achtergrondkleur van de geselecteerde tekst.',
        cls: 'x-html-editor-tip'
      },
      forecolor: {
        title: 'Tekst kleur',
        text: 'Verander de kleur van de geselecteerde tekst.',
        cls: 'x-html-editor-tip'
      },
      justifyleft: {
        title: 'Tekst links uitlijnen',
        text: 'Lijn de tekst links uit.',
        cls: 'x-html-editor-tip'
      },
      justifycenter: {
        title: 'Tekst centreren',
        text: 'Centreer de tekst.',
        cls: 'x-html-editor-tip'
      },
      justifyright: {
        title: 'Tekst rechts uitlijnen',
        text: 'Lijn de tekst rechts uit.',
        cls: 'x-html-editor-tip'
      },
      insertunorderedlist: {
        title: 'Opsommingstekens',
        text: 'Begin een ongenummerde lijst.',
        cls: 'x-html-editor-tip'
      },
      insertorderedlist: {
        title: 'Genummerde lijst',
        text: 'Begin een genummerde lijst.',
        cls: 'x-html-editor-tip'
      },
      createlink: {
        title: 'Hyperlink',
        text: 'Maak van de geselecteerde tekst een hyperlink.',
        cls: 'x-html-editor-tip'
      },
      sourceedit: {
        title: 'Bron aanpassen',
        text: 'Schakel modus over naar bron aanpassen.',
        cls: 'x-html-editor-tip'
      }
    }
  });
}

if(Ext.form.BasicForm){
  Ext.form.BasicForm.prototype.waitTitle = 'Even wachten a.u.b...';
}

if(Ext.grid.GridView){
  Ext.apply(Ext.grid.GridView.prototype, {
    sortAscText: 'Sorteer oplopend',
    sortDescText: 'Sorteer aflopend',
    lockText: 'Kolom vastzetten',
    unlockText: 'Kolom vrijgeven',
    columnsText: 'Kolommen'
  });
}

if(Ext.grid.GroupingView){
  Ext.apply(Ext.grid.GroupingView.prototype, {
  emptyGroupText: '(Geen)',
  groupByText: 'Dit veld groeperen',
  showGroupsText: 'Toon in groepen'
  });
}

if(Ext.grid.PropertyColumnModel){
  Ext.apply(Ext.grid.PropertyColumnModel.prototype, {
    nameText: 'Naam',
    valueText: 'Waarde',
    dateFormat: 'j-m-Y'
  });
}

if(Ext.layout.BorderLayout && Ext.layout.BorderLayout.SplitRegion){
  Ext.apply(Ext.layout.BorderLayout.SplitRegion.prototype, {
    splitTip: 'Sleep om grootte aan te passen.',
    collapsibleSplitTip: 'Sleep om grootte aan te passen. Dubbel klikken om te verbergen.'
  });
}

if(Ext.form.TimeField){
  Ext.apply(Ext.form.TimeField.prototype, {
    minText: 'De tijd in dit veld moet op of na {0} liggen',
    maxText: 'De tijd in dit veld moet op of voor {0} liggen',
    invalidText: '{0} is geen geldig tijdstip',
    format: 'G:i',
    altFormats: 'g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H'
  });
}