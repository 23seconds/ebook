/*
  * List compiled by mystix on the extjs.com forums.
  * Thank you Mystix!
  *
  * Dutch Translations
  * by Ido Sebastiaan Bas van Oostveen (12 Oct 2007)
  */

Ext.UpdateManager.defaults.indicatorText = '<div class="loading-indicator">Chargement en cours...</div>';

Ext.ns('eBook.Language');
Ext.ns('eBook.Language.Roles');
eBook.Language.YEAREND = 'Clôture de l\'exercice comptable';
eBook.Language.BIZTAXPREP = 'BizTax préparation';
eBook.Language.BIZTAXDECLARATION = 'BizTax déclaration';
eBook.Language.BIZTAX_AUTO = 'BizTax module (feuilles de travailles)';
eBook.Language.BIZTAX_MANUAL = 'BizTax module (sans feuilles de travailles)';
eBook.Language.ANNUALACCOUNTS_FILING = 'Déposer comptes consolidés';
eBook.Language.Roles.ADMIN = 'Administrator';
eBook.Language.Roles.CHAMPION = 'eBook Champion';
eBook.Accounts.Manage.EditWindow_Title = 'Editer compte';
eBook.Accounts.Manage.EditWindow_TitleMappings = 'Mappings';
eBook.Accounts.Manage.EditWindow_TitleTranslations = 'Traductions';
eBook.Accounts.Manage.MappingGrid_DeletingMapping = 'Effacer du mapping {0} {1}';
eBook.Accounts.Manage.MappingGrid_Mapping = 'Mapping';
eBook.Accounts.Manage.MappingGrid_SavingMapping = 'Sauvegarde du mapping {0} {1}';
eBook.Accounts.Manage.MappingGrid_WorksheetHeader = 'Feuille de travail';
eBook.Accounts.Manage.TranslationsGrid_Add = 'Ajouter traduction';
eBook.Accounts.Manage.TranslationsGrid_DefaultText = 'Nouvelle description';
eBook.Accounts.Manage.TranslationsGrid_Delete = 'Effacer traduction';
eBook.Accounts.Manage.TranslationsGrid_Deleting = 'Effacer {0} {1}';
eBook.Accounts.Manage.TranslationsGrid_Description = 'Description';
eBook.Accounts.Manage.TranslationsGrid_Language = 'Langue';
eBook.Accounts.Manage.TranslationsGrid_Saving = 'Sauvegarder {0} {1}';
eBook.Accounts.Manage.Window_AccountDesc = 'Description';
eBook.Accounts.Manage.Window_AccountNr = 'Compte';
eBook.Accounts.Manage.Window_AddAccount = 'Nouveau compte';
eBook.Accounts.Manage.Window_EditAccount = 'Editer compte';
eBook.Accounts.Manage.Window_SearchText = 'Rechercher des comptes';
eBook.Accounts.Manage.Window_SearchTip = 'Rechercher des comptes';
eBook.Accounts.Manage.Window_SelectAll = 'Selectionner tous';
eBook.Accounts.Manage.Window_Title = 'Gestion comptes généraux';
eBook.Accounts.Mappings.GridPanel_ColumnAccount = 'Compte';
eBook.Accounts.Mappings.GridPanel_ColumnBelRes = 'Réserves taxables';
eBook.Accounts.Mappings.GridPanel_ColumnBTW = 'Concordance chiffre d\'affaires/TVA';
eBook.Accounts.Mappings.GridPanel_ColumnDBI = 'RDT';
eBook.Accounts.Mappings.GridPanel_ColumnMWAandelen = 'Plus-values sur actions';
eBook.Accounts.Mappings.GridPanel_ColumnErelonen = 'Honoraires';
eBook.Accounts.Mappings.GridPanel_ColumnRV = 'Avances productives d\'intérêts';
eBook.Accounts.Mappings.GridPanel_ColumnRVI = 'PM/Intérêts';
eBook.Accounts.Mappings.GridPanel_ColumnSalaris = 'Concordance Salaires';
eBook.Accounts.Mappings.GridPanel_ColumnSaldo = 'Solde';
eBook.Accounts.Mappings.GridPanel_ColumnSaldoPrevious = 'Solde exercice comptable précédent';
eBook.Accounts.Mappings.GridPanel_ColumnVAA = 'Avantages de toute nature';
eBook.Accounts.Mappings.GridPanel_ColumnVU = 'Dépenses non admises';
eBook.Accounts.Mappings.GridPanel_EditAccount = 'Editer compte';
eBook.Accounts.Mappings.GridPanel_ExportExcelAdjustments = 'Exporter adjustments';
eBook.Accounts.Mappings.GridPanel_ExportMappings = 'Mappings';
eBook.Accounts.Mappings.GridPanel_ExportProAccAdjustments = 'Exporter Adjustments';
eBook.Accounts.Mappings.GridPanel_ExportState = 'Exporter histoire ProAcc & eBook';
eBook.Accounts.Mappings.GridPanel_ImportExcel = 'Importation';
eBook.Accounts.Mappings.GridPanel_ImportExport = 'Import / Export';
eBook.Accounts.Mappings.GridPanel_ImportProAcc = 'Importation ProAcc';
eBook.Accounts.Mappings.GridPanel_ImportExact = 'Importation Exact';
eBook.Accounts.Mappings.GridPanel_ManageAccounts = 'Gestion comptes généraux';
eBook.Accounts.Mappings.GridPanel_NewAccount = 'Nouveau compte';
eBook.Accounts.Mappings.GridPanel_PreparingExcel = 'Génération d\'Excel en cours';
eBook.Accounts.Mappings.GridPanel_PreparingExcelAdjust = 'Exporter adjustments';
eBook.Accounts.Mappings.GridPanel_ViewDetail = 'Vue détaillée';
eBook.Accounts.Mappings.GridPanel_ViewFilter = 'Vue / Filtre';
eBook.Accounts.Mappings.GridPanel_ViewSaldi0 = 'Solde 0,00€';
eBook.Accounts.Mappings.Window_DeletingMapping = 'Efface du mapping {0} {1} est en cours';
eBook.Accounts.Mappings.Window_SavingMapping = 'Sauvegarde du mapping {0} {1} est en cours';
eBook.Accounts.Mappings.Window_SavingMappings = 'Sauvegarde des mappings en cours';
eBook.Accounts.Mappings.Window_Title = 'Mapping comptes généraux';
eBook.Accounts.New.Window_AccountDesc = 'Description ({0})';
eBook.Accounts.New.Window_AccountNr = 'Compte';
eBook.Accounts.New.Window_Saving = 'Création du compte {0} {1} est en cours';
eBook.Accounts.New.Window_SavingBusy = 'Création du compte est en cours';
eBook.Accounts.New.Window_Title = 'Nouveau compte';
eBook.Bundle_Statements = 'Etats';
eBook.Bundle.BundlePanel_Library = 'Bibliothèque';
eBook.Bundle.BundlePanel_NewChapter = 'Nouveau chapitre';
eBook.Bundle.NewWindow_Bundle_Create = 'Créer liasse';
eBook.Bundle.NewWindow_Deliverable_Create = 'Créer deliverable';
eBook.Bundle.NewWindow_Creating = 'Création du liasse est en cours';
eBook.Bundle.NewWindow_IncludePrevious = 'y Compris les liasse dans le dossier précédent';
eBook.Bundle.NewWindow_Language = 'Choisi une langue';
eBook.Bundle.NewWindow_Layout = 'Choisi mise en page';
eBook.Bundle.NewWindow_Name = 'Nom du liasse';
eBook.Bundle.NewWindow_Deliverable_Name = 'Nom du deliverable';
eBook.Bundle.NewWindow_Bundle_Name = 'Nom du liasse';
eBook.Bundle.NodeInfoPanel_Detailed = 'Vue détaillée';
eBook.Bundle.NodeInfoPanel_Footer = 'Pied de page';
eBook.Bundle.NodeInfoPanel_Header = 'En-tête';
eBook.Bundle.NodeInfoPanel_InfoTitle = 'Info d\'élément: {0}';
eBook.Bundle.NodeInfoPanel_BundleSettingsTitle = 'Info liasse';
eBook.Bundle.NodeInfoPanel_Language = 'Langue';
eBook.Bundle.NodeInfoPanel_PageFrom = 'à partir de la page';
eBook.Bundle.NodeInfoPanel_PageTo = 'jusqu\'à la page';
eBook.Bundle.NodeInfoPanel_ReconfigureChildren = 'Reconfigurer toutes éléments';
eBook.Bundle.NodeInfoPanel_ReportSettings = 'Configuration générale';
eBook.Bundle.NodeInfoPanel_ShowFootNote = 'Note en bas de page';
eBook.Bundle.NodeInfoPanel_ShowInIndex = 'Visible dans page d\'index';
eBook.Bundle.NodeInfoPanel_ShowPageNr = 'Numérotation de page';
eBook.Bundle.NodeInfoPanel_ShowTitle = 'Afficher le titre';
eBook.Bundle.NodeInfoPanel_Title = 'Titre dans liasse';
eBook.Bundle.NodeInfoPanel_UpdateChildrenMsg = 'Tous les élément dans liasse sont assimilées à la configuration générale. Êtes-vous sûr de vouloir mettre en œuvre?';
eBook.Bundle.NodeInfoPanel_UpdateChildrenTitle = 'Reconfigurer toutes éléments?';
eBook.Bundle.ObjectsTree_Title = 'Bibliothèque';
eBook.Bundle.Window_DeleteItem = 'Effacer élément';
eBook.Bundle.Window_DeletePdfMsg = 'Êtes-vous sûr de vouloir supprimer le PDF "{0}"?';
eBook.Bundle.Window_DeleteReportMsg = 'Êtes-vous sûr de vouloir supprimer "{0}"?';
eBook.Bundle.Window_DeleteReportTitle = 'Supprimer liasse';
eBook.Bundle.Window_DeletingPdfMsg = 'Supprimer PDF "{0}"';
eBook.Bundle.Window_DeletingReport = 'Supprimer "{0}"';
eBook.Bundle.Window_EmptyMenu = 'Aucun article trouvé';
eBook.Bundle.Window_GeneralActions = 'Actions générales';
eBook.Bundle.Window_GeneratingReport = 'Génération du liasse {0} en cours';
eBook.Bundle.Window_LibraryActions = 'Actions de la bibliothèque';
eBook.Bundle.Window_LibraryAddPdf = 'Ajouter PDF';
eBook.Bundle.Window_LibraryDeletePdf = 'Supprimer pdf';
eBook.Bundle.Window_LibraryReload = 'Recharger la bibliothèque';
eBook.Bundle.Window_LoadingLibrary = 'Chargement de la bibliothèque en cours';
eBook.Bundle.Window_LoadingMenu = 'Rechercher de liasses';
eBook.Bundle.Window_LoadingReport = 'Chargement du liasse {0} en cours';
eBook.Bundle.Window_NewChapter = 'Ajouter chapitre';
eBook.Bundle.Window_ReportActions = 'Actions du liasse';
eBook.Bundle.Window_ReportDelete = 'Supprimer liasse';
eBook.Bundle.Window_ReportGenerate = 'Générer liasse';
eBook.Bundle.Window_ReportNew = 'Nouveau liasse';
eBook.Bundle.Window_ReportOpen = 'Ouvrez liasse';
eBook.Bundle.Window_ReportSave = 'Sauvegarde liasse';
eBook.Bundle.Window_SavingReport = 'Sauvegarde du liasse {0} en cours';
eBook.Bundle.Window_Bundle_New = 'Nouveau liasse';
eBook.Bundle.Window_Bundle_Open = 'Ouvrez liasse';
eBook.Bundle.Window_Bundle_Save = 'Sauvegarde liasse';
eBook.Bundle.Window_Bundle_Actions = 'Actions du liasse';
eBook.Bundle.Window_Bundle_Delete = 'Supprimer liasse';
eBook.Bundle.Window_Bundle_Generate = 'Générer liasse';
eBook.Bundle.Window_Deliverable_New = 'Nouveau deliverable';
eBook.Bundle.Window_Deliverable_Open = 'Ouvrez deliverable';
eBook.Bundle.Window_Deliverable_Save = 'Sauvegarde deliverable';
eBook.Bundle.Window_Deliverable_Actions = 'Actions du deliverable';
eBook.Bundle.Window_Deliverable_Delete = 'Sauvegarde deliverable';
eBook.Bundle.Window_Deliverable_Generate = 'Générer deliverable';
eBook.Bundle.Window_Bundle_Title = 'Liasses';
eBook.Bundle.Window_Deliverable_Title = 'Deliverables';
eBook.Bundle.Window_AnnualAccounts_Title = 'NBB filing';
eBook.Bundle.Window_SpecificActions = 'Workflow';
eBook.Bundle.Window_Status = 'Statut';
eBook.Bundle.Window_Submit = 'Soumettre';
eBook.Bundle.Window_Approve = 'Approuver';
eBook.Bundle.Window_Reject = 'Rejeter';
eBook.Bundle.Window_ReviewNotes = "Review Notes";
eBook.Bundle.Window_AddComment = 'Ouvert';
eBook.Bundle.Window_AddComment_Title = "Review Notes";
eBook.Bundle.Window_Unlock = 'Unlock';
eBook.Bundle.Window_Lock = 'Lock';
eBook.BusinessRelations.FormPanel_Address = 'Adresse';
eBook.BusinessRelations.FormPanel_City = 'Lieu de résidence';
eBook.BusinessRelations.FormPanel_Country = 'Pays';
eBook.BusinessRelations.FormPanel_EnterpriseNumber = 'Numéro de l\'entreprise';
eBook.BusinessRelations.FormPanel_FirstName = 'Prénom';
eBook.BusinessRelations.FormPanel_LastName = 'Nom de l\'entreprise / de famille';
eBook.BusinessRelations.FormPanel_Saving = 'Sauvegarder {0}';
eBook.BusinessRelations.FormPanel_VatNumber = 'Numéro TVA';
eBook.BusinessRelations.FormPanel_ZipCode = 'Code postal';
eBook.BusinessRelations.Window_Customer = 'Client';
eBook.BusinessRelations.Window_Delete = 'Effacer {0}';
eBook.BusinessRelations.Window_DeleteMsg = 'Êtes-vous sur d\'effacer {0}?';
eBook.BusinessRelations.Window_DeleteTitle = 'Effacer {0}';
eBook.BusinessRelations.Window_Deleting = 'Efface de {0} est en cours';
eBook.BusinessRelations.Window_Edit = 'Editer {0}';
eBook.BusinessRelations.Window_Import = 'Import';
eBook.BusinessRelations.Window_ImportExcel = 'Importation Excel';
eBook.BusinessRelations.Window_Importing = 'Importation est en cours';
eBook.BusinessRelations.Window_ImportProAcc = 'Importation ProAcc';
eBook.BusinessRelations.Window_Manage = 'Gestion de {0}';
eBook.BusinessRelations.Window_New = 'Nouveau {0}';
eBook.BusinessRelations.Window_Supplier = 'Fournisseur';
eBook.Client.PreWorksheetWindow_Create = 'Créer feuille de travail';
eBook.Client.PreWorksheetWindow_Description = 'Déscription';
eBook.Client.PreWorksheetWindow_New = 'Créer nouveau déclaration';
eBook.Client.PreWorksheetWindow_Open = 'Ouvrer feuille de travail';
eBook.Client.PreWorksheetWindow_PreviousEnddate = 'Date à la fin de l\'exercice précédente';
eBook.Client.PreWorksheetWindow_Title = 'Déclaration à l\'impôt des sociétés manuel';
eBook.Client.Window_Active = 'Actif';
eBook.Client.Window_Address = 'Adresse';
eBook.Client.Window_City = 'Lieu de résidence';
eBook.Client.Window_Country = 'Pays';
eBook.Client.Window_Director = 'Directeur';
eBook.Client.Window_eBookOverrule = 'Données de remplacement';
eBook.Client.Window_Email = 'Email';
eBook.Client.Window_EnterpriseNr = 'Numéro de l\'entreprise';
eBook.Client.Window_Fax = 'Fax';
eBook.Client.Window_GFIS = 'Code GFIS';
eBook.Client.Window_LegalStruct = 'Forme juridique';
eBook.Client.Window_Loading = 'Recharge en cours';
eBook.Client.Window_Name = 'Nom';
eBook.Client.Window_Phone = 'Téléphone';
eBook.Client.Window_Rpr = 'Lieu d\'enregistrement';
eBook.Client.Window_SaveChanges = 'Sauvegarde des modifications';
eBook.Client.Window_SaveMsg = 'Sauvegarde des modifications?';
eBook.Client.Window_SaveTitle = 'Sauvegarde';
eBook.Client.Window_Saving = 'Sauvegarde en cours';
eBook.Client.Window_Vat = 'Numéro TVA';
eBook.Client.Window_VatElegible = 'Obligation TVA';
eBook.Client.Window_ZipCode = 'Code postal';
eBook.Closing.View_BizTaxDecline = 'Ce dossier ne contient pas de déclaration à rentrer par EY';
eBook.Closing.View_BizTaxExecute = 'Déposer déclaration';
eBook.Closing.View_BizTaxTitle = 'Déclaration ISOC';
eBook.Closing.View_ClosingAllText = '\'Clôturer tous les composants';
eBook.Closing.View_ClosingReportsMsg = 'Clôture les liasses';
eBook.Closing.View_ClosingReportsTitle = 'Clôture les liasses';
eBook.Closing.View_ClosingText = 'Clôturer';
eBook.Closing.View_ClosingWorksheets = 'Clôturer les feuilles de travailles';
eBook.Closing.View_ClosingWorksheetsMsg = 'Clôturer les feuilles de travailles';
eBook.Closing.View_ClosingWorksheetsTitle = 'Clôture des feuilles de travailles';
eBook.Closing.View_DeleteText = 'Supprimer';
eBook.Closing.View_FileCloseOk = 'Toutes les étapes précédentes sont clôturées, vous pouvez maintenant clôturer le dossier définitivement';
eBook.Closing.View_FileClosePrevious = 'Toutes les étapes précédentes doivent être clôturées afin de pouvoir clôturer définitivement le dossier';
eBook.Closing.View_FileIsClosed = 'Ce dossier est définitivement clôturé.';
eBook.Closing.View_FileTitle = 'Clôturer le dossier définitivement';
eBook.Closing.View_FinalClosing = 'Clôturer définitivement';
eBook.Closing.View_InvestigateFile = 'Dossier est examiné';
eBook.Closing.View_NumbersInfo = 'Dans la première étape de la procédure de clôture la balance sera vérrouillée';
eBook.Closing.View_NumbersProAccMsg = 'Avant de pouvoir clôturer le dossier en eBook, il faut clôturer l\'exercice comptable en ProAcc.';
eBook.Closing.View_NumbersProfitLossMsg = 'Il y a une différence entre le résultat de l\'exercice comptable en ProAcc [{0}] et eBook [{1}] - Ce dossier ne peut pas être clôturé.';
eBook.Closing.View_NumbersQ1 = 'Ce dossier concerne-t-il une clôture de l\'exercice comptable?';
eBook.Closing.View_NumbersQ2 = 'Pour ce dossier E&Y tient la comptabilité en ProAcc?';
eBook.Closing.View_NumbersQ3 = 'L\'exercice comptable a-t-il été clôturé en ProAcc?';
eBook.Closing.View_NumbersQ4 = 'Complétez le montant du profit/perte de l\'exercice comptable en question (en EUR, avec 2 décimales)';
eBook.Closing.View_NumbersTitle = 'Clôturer les chiffres';
eBook.Closing.View_OpenText = 'Ouvrir';
eBook.Closing.View_ReOpen = 'Réouverté';
eBook.Closing.View_ReOpenAllWorksheet = 'Réouverter toutes les feuilles de travailles';
eBook.Closing.View_ReOpenBundle = 'Réouverter liasse';
eBook.Closing.View_ReOpenFile = 'Dossier est réouverté';
eBook.Closing.View_ReOpenNumber = 'Réouverter les chiffres';
eBook.Closing.View_ReportsPreviousUnClosedText = 'Une fois les chiffres et les feuilles de travails clôturés vous pouvez clôturer les liasses créées';
eBook.Closing.View_ReportsTitle = 'Clôturer la/les liasse(s)';
eBook.Closing.View_SharedInfo = '!! Attention: Ce client est partagé entre ACR et TAX';
eBook.Closing.View_TaxValidation = '<b><u>Validation du calcul de l\'impôt des sociétés</u></b><br/>avant le après le recalcul:<b>{1}</b>.<br/>Souhaitez-vous poursuivre de clôturer les feuilles de travailles?';
eBook.Closing.View_ViewText = 'Regarder';
eBook.Closing.View_WorksheetsNumbersUnlockedText = 'Une fois les chiffres clôturés vous pouvez clôturer les feuilles de travail';
eBook.Closing.View_WorksheetsTitle = 'Clôturer les feuilles de travail';
eBook.Closing.View_WorksheetsWithErrors = 'Certains feuille de travailles contiennent des erreurs. La procedure de clôture est annulée.';
eBook.Closing.Window_BundleMsg = 'La mention \'draft\' sur la liasse disparait automatiquement lorsque le dossier (y compris le rapport) est définitivement clôturé.';
eBook.Closing.Window_NumbersMsg = 'Dès que vous avez clôturé les chiffres:<ul><li>Une mise à jour des chiffres n’est plus possible (Proacc/Excel)</li><li>Plus aucune opération diverse ne pourra être ajoutée (manuelle/automatique)</li><li>Le mapping pourra encore être changé</li></ul>';
eBook.Closing.Window_Title = 'Procédure de clôture';
eBook.Create.Empty.ExcelFileFieldSet_File = 'Fichier';
eBook.Create.Empty.ExcelFileFieldSet_Select = 'sélectionnez fichier Excel';
eBook.Create.Empty.ExcelFileFieldSet_Sheets = 'Feuilles de travaille';
eBook.Create.Empty.ExcelFileFieldSet_Title = 'Fichier Excel';
eBook.Create.Empty.FileDatesFieldSet_Assessmentyear = 'Année d\'imposition';
eBook.Create.Empty.FileDatesFieldSet_CurrentAssesment = 'Année d\'imposition courante';
eBook.Create.Empty.FileDatesFieldSet_Enddate = 'Fin de l\'exercice comptable';
eBook.Create.Empty.FileDatesFieldSet_EnddateInvalid = 'a date de fin ne peut pas être plus jeune que la date de début';
eBook.Create.Empty.FileDatesFieldSet_Startdate = 'Début de l\'exercice comptable';
eBook.Create.Empty.FileInfoFieldSet_FileType = 'Type de dossier';
eBook.Create.Empty.FileInfoFieldSet_Language = 'Langue maternelle';
eBook.Create.Empty.FileInfoFieldSet_Name = 'Nom du dossier';
eBook.Create.Empty.FileInfoFieldSet_Title = 'Données du dossier';
eBook.Create.Empty.MainPanel_Create = 'Créer dossier';
eBook.Create.Empty.MainPanel_ImportPrevious = 'Importation des comptes généraux et mappings  du dossier précédent';
eBook.Create.Empty.MainPanel_ImportPreviousWorksheet = 'Initialiser feuille de travaille {0} basée sur dossier précédent';
eBook.Create.Empty.MainPanel_ImportStandard = 'Ajouter les comptes généraux d\'Ernst & Young';
eBook.Create.Empty.MainPanel_Loading = 'Chargement du dossier {0} en cours';
eBook.Create.Empty.MainPanel_MissingData = 'Certains champs ne sont pas corrects.';
eBook.Create.Empty.MainPanel_Undo = 'Défaire du dossier';
eBook.Create.Empty.PreviousFileFieldSet_EbookFile = 'Dossier d\'eBook précedent';
eBook.Create.Empty.PreviousFileFieldSet_Enddate = 'Fin de l\'exercice comptable';
eBook.Create.Empty.PreviousFileFieldSet_Startdate = 'Début de l\'exercice comptable';
eBook.Create.Empty.PreviousFileFieldSet_Title = 'Année d\'imposition précédent';
eBook.Create.ProAcc.FileDatesFieldSet_Bookyear = 'Exercice comptable';
eBook.Create.ProAcc.FileDatesFieldSet_GetDates = 'Chargement des dates en cours';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccAccounts = 'Importation des comptes sur base de ProAcc';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccEndJournal = 'Importation de journal d\'exercice comptable précédent';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccStartJournal = 'Importation de journal d\'exercice comptable actuel';
eBook.Create.ProAcc.MainPanel_ImportProAccAccounts = 'Importation des comptes généraux de ProAcc';
eBook.Create.ProAcc.MainPanel_ImportProAccEndJournal = 'Importations du journal d\'exercice précédent';
eBook.Create.ProAcc.MainPanel_ImportProAccStartJournal = 'Importations du journal d\'exercice actuel';
eBook.Create.Exact.FinancialYears = 'Select periode';
eBook.Create.Window_ActionTitle = 'Création du dossier en cours :';
eBook.Create.Window_Copy = 'Copie du dossier';
eBook.Create.Window_EmptyFile = 'Dossier vide';
eBook.Create.Window_Excel = 'Importation Excel';
eBook.Create.Window_ImportTypes = 'Sélectionnez le type d\'importation/création';
eBook.Create.Window_Exact = 'Importation Exact (excel)';
eBook.Create.Window_ProAcc = 'Importation ProAcc';
eBook.Document.Fields.BlockEditor_Cancel = 'Annuler';
eBook.Document.Fields.BlockEditor_Save = 'Sauvegarder';
eBook.Document.NewDocumentWindow_CreateDoc = 'Créer document';
eBook.Document.NewDocumentWindow_DocName = 'Nom du document';
eBook.Document.NewDocumentWindow_Group = 'Type de document';
eBook.Document.NewDocumentWindow_Language = 'Langue';
eBook.Document.NewDocumentWindow_LoadingTemplates = 'Chargement des modèles en cours';
eBook.Document.NewDocumentWindow_NoTemplateSelected = 'Aucune modèle sélectionner';
eBook.Document.NewDocumentWindow_Template = 'Modèle';
eBook.Document.NewDocumentWindow_Title = 'Nouveau document';
eBook.Document.OpenDocumentWindow_NoDocuments = 'Aucune modèle trouver';
eBook.Document.Window_Loading = 'Chargement du document en cours';
eBook.Document.Window_LoadingMenu = 'Recherche des documents en cours';
eBook.Document.Window_MenuEmpty = 'Aucun document trouvé';
eBook.Document.Window_NewDocument = 'Nouveau document';
eBook.Document.Window_OpenDocument = 'Ouvrez document';
eBook.Document.Window_PrintPreview = 'Extrait';
eBook.Document.Window_SaveDocument = 'Sauvegarder document';
eBook.Document.Window_Saving = 'Sauvegarde du document {0} en cours';
eBook.Document.Window_Title = 'Document';
eBook.Excel.AccountSchemaPanel_AccountDesc = 'Description';
eBook.Excel.AccountSchemaPanel_AccountNr = 'Compte';
eBook.Excel.AccountSchemaPanel_Language = 'Langue';
eBook.Excel.BalansPanel_Saldo = 'Solde';
eBook.Excel.Grid_NoWorksheetMsg = 'Sélectionner une feuille de travail Excel';
eBook.Excel.Grid_NoWorksheetTitle = 'Aucune feuille de travaille';
eBook.Excel.Grid_Preview = 'Exemple des données';
eBook.Excel.MappingPanel_Sheets = 'Feuille de travail Excel';
eBook.Excel.UploadWindow_Invalid = 'Mauvais fichier';
eBook.Excel.UploadWindow_Processing = 'Le document est traité';
eBook.Excel.UploadWindow_SelectFile = 'Sélectionnez Excel';
eBook.Excel.UploadWindow_Title = 'Télécharger Excel';
eBook.Excel.UploadWindow_Upload = 'Télécharger fichier';
eBook.Excel.UploadWindow_Uploading = 'Téléchargement en cours';
eBook.Excel.Window_AccountSchema = 'Comptes généraux';
eBook.Excel.Window_CurrentBalans = 'Journal actuel';
eBook.Excel.Window_Import = 'Importer';
eBook.Excel.Window_ImportAS = 'Importation des comptes généraux';
eBook.Excel.Window_ImportData = 'Importer des données';
eBook.Excel.Window_ImportDoneMsg = 'Importation compléte';
eBook.Excel.Window_ImportDoneTitle = 'Importation compléte';
eBook.Excel.Window_ImportHB = 'Importation du journal actuel';
eBook.Excel.Window_Importing = 'Importation des données en cours :';
eBook.Excel.Window_ImportVB = 'Importation du journal précédent';
eBook.Excel.Window_NoDataMsg = 'Sélectionner les données d\'importation';
eBook.Excel.Window_NoDataTitle = 'Aucune données';
eBook.Excel.Window_PreviousBalans = 'Journal précédent';
eBook.Excel.Window_TabFailedMsg = 'Pas tous les onglets sélectionnées sont correctes';
eBook.Excel.Window_TabFailedTitle = 'Onglet mauvais';
eBook.Exact.UploadWindow_SelectFile = 'Sélectionnez file (.txt)';
eBook.ExtraFiles.Window_Processing = 'Le fichier {0} est traité';
eBook.ExtraFiles.Window_SelectExWord = 'Sélectionnez Excel/Word';
eBook.ExtraFiles.Window_Title = 'Autre documents';
eBook.ExtraFiles.Window_Upload = 'Télécharge fichier';
eBook.ExtraFiles.Window_Uploading = 'Téléchargement en cours';
eBook.Fields.FodAddress_Adrestype = 'Type d\'adresse';
eBook.Fields.FodAddress_Box = 'Boite';
eBook.Fields.FodAddress_City = 'Commune';
eBook.Fields.FodAddress_Country = 'Pays';
eBook.Fields.FodAddress_Nr = 'N°';
eBook.Fields.FodAddress_Street = 'Rue';
eBook.Fields.FodAddress_Zipcode = 'Code postal';
eBook.Fields.FodPhone_Country = 'Code pays';
eBook.Fields.FodPhone_Extension = 'Extension n°';
eBook.Fields.FodPhone_Local = 'Téléphone local';
eBook.Fields.FodPhone_Zone = 'Zone';
eBook.File.HomeHeader_BizTax = 'Déclaration BizTax';
eBook.File.HomeHeader_Role = 'Votre rôle actuel';
eBook.File.HomeHeader_YearEnd = 'Clôture annuelle';
eBook.Help.ChampionsGrid_Email = 'E-mail';
eBook.Help.ChampionsGrid_FirstName = 'Prénom';
eBook.Help.ChampionsGrid_LastName = 'Nom de famille';
eBook.Help.ChampionsGrid_Office = 'Office';
eBook.InterfaceTrans_Client = 'Client';
eBook.InterfaceTrans_ClientAddress = 'Adresse';
eBook.InterfaceTrans_ClientCity = 'Lieu de résidence';
eBook.InterfaceTrans_ClientCountry = 'Pays';
eBook.InterfaceTrans_ClientName = 'Nom';
eBook.InterfaceTrans_ClientSearch = 'Trouver et ouvrir client';
eBook.InterfaceTrans_ClientSearchText = 'Recherche client';
eBook.InterfaceTrans_ClientZipCode = 'Code postal';
eBook.InterfaceTrans_EndDate = 'Fin de l\'exercice comptable';
eBook.InterfaceTrans_File = 'Dossier';
eBook.InterfaceTrans_FileName = 'num';
eBook.InterfaceTrans_GFISCode = 'Code GFIS';
eBook.InterfaceTrans_LastAccessed = 'Dernière visite';
eBook.InterfaceTrans_LoadingClient = 'Chargement de client {0} en cours';
eBook.InterfaceTrans_LoadingClients = 'Chargement des clients en cours';
eBook.InterfaceTrans_LoadingFile = 'Chargement du dossier {0} en cours';
eBook.InterfaceTrans_LoggingIn = 'Connexion et synchroniser PMT en cours';
eBook.InterfaceTrans_MenuEbook = 'eBook';
eBook.InterfaceTrans_MenuStatistics = 'Statistiques';
eBook.InterfaceTrans_MyLatestFiles = 'Mes derniers dossier ouverts';
eBook.InterfaceTrans_NoProAccMsg = 'Cette client n\'avais pas une connection connus par ProAcc';
eBook.InterfaceTrans_NoProAccTitle = 'Pas connection ProAcc';
eBook.InterfaceTrans_SelectAllFields = 'Sélectionnez tous';
eBook.InterfaceTrans_StartDate = 'Début de l\'exercice comptable';
eBook.Journal.Booking.Grid_AccountNr = 'Compte';
eBook.Journal.Booking.Grid_Credit = 'Crédit';
eBook.Journal.Booking.Grid_Debet = 'Débit';
eBook.Journal.Booking.Grid_DeleteLineTip = 'Effacer ligne';
eBook.Journal.Booking.Panel_Desc = 'Description / Titre';
eBook.Journal.Booking.Panel_HideClients = 'Cacher pour client';
eBook.Journal.Booking.Window_AddLines = 'Ajouter des lignes';
eBook.Journal.Booking.Window_DebetCredit = 'Débit {0} n\'est pas égale à crédit {1}';
eBook.Journal.Booking.Window_ImportExcel = 'Importation Excel';
eBook.Journal.Booking.Window_Save = 'Sauvegarder';
eBook.Journal.Booking.Window_Saving = 'Sauvegarde du bilan {0} en cours';
eBook.Journal.Grid_Account = 'Compte';
eBook.Journal.Grid_AdjustmentsCredit = 'Ajustements crédit';
eBook.Journal.Grid_AdjustmentsDebet = 'Ajustements débit';
eBook.Journal.Grid_Credit = 'Crédit';
eBook.Journal.Grid_Debet = 'Débit';
eBook.Journal.Grid_End = 'Bilan après ajustements';
eBook.Journal.Grid_Start = 'Bilan avant ajustements';
eBook.Journal.Account_Adjustments_gs = "Solde importé";
eBook.Journal.Account_Adjustments_t = "Total";
eBook.Journal.Account_Adjustments_s = "Solde";
eBook.Journal.GroupedGrid_Account = 'Compte';
eBook.Journal.GroupedGrid_Booking = 'Écriture';
eBook.Journal.GroupedGrid_Credit = 'Crédit';
eBook.Journal.GroupedGrid_Debet = 'Débit';
eBook.Journal.GroupedGrid_DeleteBooking = 'Effacer écriture';
eBook.Journal.GroupedGrid_DeleteBookingMsg = 'Effacer écriture {0} ?';
eBook.Journal.GroupedGrid_DeleteBookingTitle = 'Effacer écriture?';
eBook.Journal.GroupedGrid_DeletingBooking = 'Effacer d\'écriture {0} en cours';
eBook.Journal.GroupedGrid_EditBooking = 'Editer écriture';
eBook.Journal.GroupedGrid_BusinessRelation = 'Client';
eBook.Journal.GroupedGrid_Comment = 'Comment';
eBook.Journal.Window_AddBooking = 'Ajouter écriture';
eBook.Journal.Window_ExcelExporting = 'Exporter vers Excel en cours';
eBook.Journal.Window_ExportExcel = 'Exporter vers Excel';
eBook.Journal.Window_ImportExcel = 'Importation Excel';
eBook.Journal.Window_ImportExport = 'Import / Export';
eBook.Journal.Window_ImportProAcc = 'Importation ProAcc';
eBook.Language_Dutch = 'Néerlandais';
eBook.Language_English = 'Anglais';
eBook.Language_French = 'Français';
eBook.Language_German = 'Allemand';
eBook.RepositoryCategory_Permanent = 'Permanent';
eBook.RepositoryCategory_Period = 'Périodiquement';
eBook.Menu.Client_ClientData = 'Données du client';
eBook.Menu.Client_ClosedFiles = 'Dossiers clôturés';
eBook.Menu.Client_Customers = 'Clients';
eBook.Menu.Client_Deleted = '';
eBook.Menu.Client_EndDate = 'Fin de l\'exercice comptable';
eBook.Menu.Client_FileName = 'Nom du dossier';
eBook.Menu.Client_NewFile = 'Nouveau dossier';
eBook.Menu.Client_NoClosedFiles = 'Aucun dossier clôtures trouvé';
eBook.Menu.Client_NoDeletedFiles = 'Aucun dossier effacer trouvé';
eBook.Menu.Client_NoOpenFiles = 'Aucun dossier ouvert trouvé';
eBook.Menu.Client_OpenFiles = 'Dossiers ouverts';
eBook.Menu.Client_StartDate = 'Début de l\'exercice comptable';
eBook.Menu.Client_Suppliers = 'Fournisseurs';
eBook.Menu.File_AccountManagement = 'Gestion comptes généraux';
eBook.Menu.File_AccountSchema = 'Comptes généraux';
eBook.Menu.File_AccountsMapping = 'Mapping comptes généraux';
eBook.Menu.File_AddExtraFile = 'Ajouter fichier Word/Excel';
eBook.Menu.File_Documents = 'Documents/Reports';
eBook.Menu.File_ExtraFiles = 'Autre documents';
eBook.Menu.File_FileTitle = 'Dossier {0}';
eBook.Menu.File_FinalTrialBalance = 'Balance définitive';
eBook.Menu.File_JournalCurrent = 'Journal précédent';
eBook.Menu.File_JournalManual = 'Corrections manuelles';
eBook.Menu.File_JournalPrevious = 'Journal actuel';
eBook.Menu.File_Journals = 'Journals';
eBook.Menu.File_JournalWorksheets = 'Corrections feuilles de travail';
eBook.Menu.File_LastImported = 'Dernière mise à jour: {0}';
eBook.Menu.File_NoExtraFiles = 'Aucun "autre document"';
eBook.Menu.File_PDFFiles = 'PDF Files';
eBook.Menu.File_Reporting = 'Liasses';
eBook.Menu.File_Worksheets = 'Feuilles de travail';
eBook.Menu.File_WorksheetsAL = 'Feuilles de travail générales';
eBook.Menu.File_WorksheetsFI = 'Feuilles de travail fiscales';
eBook.Menu.File_WorksheetsFPDF = 'Fichiers';
eBook.Meta.GridPanel_AddItem = 'Ajouter item';
eBook.Meta.GridPanel_DeleteItem = 'Supprimer item';
eBook.Meta.GridPanel_EditItem = 'Modification du item';
eBook.Meta.MeetingEditor_Chairman = 'Président';
eBook.Meta.MeetingEditor_Date = 'Date';
eBook.Meta.MeetingEditor_Delayed = 'Report';
eBook.Meta.MeetingEditor_Secretary = 'Secrétaire';
eBook.Meta.MeetingEditor_Teller = 'Scrutateur';
eBook.Meta.MeetingEditor_Time = 'Heure';
eBook.Meta.PersonEditor_Female = 'Femme';
eBook.Meta.PersonEditor_Gender = 'Le sexe';
eBook.Meta.PersonEditor_Male = 'Masculin';
eBook.Meta.PersonEditor_Name = 'Nom';
eBook.Meta.ShareHolderEditor_RepresentedBy = 'Repreésenté par';
eBook.Meta.ShareHolderEditor_Shares = 'Nombre de parts';
eBook.Meta.Window_Title = 'Metadata';
eBook.Pdf.UploadWindow_ChoosePDF = 'Sélectionnez le fichier';
eBook.Pdf.UploadWindow_Processing = 'Le document {0} est traité';
eBook.Pdf.UploadWindow_ReplacePdf = 'Remplacer le fichier';
eBook.Pdf.UploadWindow_Title = 'Ajouter PDF';
eBook.Pdf.UploadWindow_Uploading = 'Télécharge du document est en cours';
eBook.Pdf.UploadWindow_UploadPDF = 'Télécharger le fichier';
eBook.Pdf.UploadWindow_WrongType = 'Le document sélectionnez n\'est pas une PDF';
eBook.Pdf.Window_AddPdf = 'Ajouter le fichier';
eBook.Pdf.Window_RefreshList = 'Rafraîchir la liste';
eBook.Pdf.Window_RemovePdf = 'Supprimer Pdf';
eBook.Pdf.Window_ReplacePdf = 'Remplacer Pdf';
eBook.Pdf.Window_Title = 'Fichiers PDF';
eBook.Pdf.Window_ViewPdf = 'Voir Pdf';
eBook.PMT.TeamGrid_Administrator = 'Administrator';
eBook.PMT.TeamGrid_Department = 'Département';
eBook.PMT.TeamGrid_eBookRole = 'Rôle en eBook';
eBook.PMT.TeamGrid_Editor = 'Editor';
eBook.PMT.TeamGrid_Member = 'Membre d\'équipe';
eBook.PMT.TeamGrid_PMTRole = 'Rôle en PMT';
eBook.PMT.TeamGrid_Reviewer = 'Reviewer';
eBook.PMT.TeamWindow_Title = 'Equipe';
eBook.ProAcc.Window_ImportAccountSchema = 'Importation des comptes ';
eBook.ProAcc.Window_ImportCurrentBalans = 'Importations du journal d\'exercice actuel';
eBook.ProAcc.Window_ImportCustomers = 'Importation des clients';
eBook.ProAcc.Window_ImportDoneMsg = 'Imporation complète';
eBook.ProAcc.Window_ImportDoneTitle = 'Imporation complète';
eBook.ProAcc.Window_ImportPreviousBalans = 'Importations du journal d\'exercice précédent';
eBook.ProAcc.Window_ImportSuppliers = 'Importation des fournisseurs';
eBook.ProAcc.Window_StartImport = 'Commencer l\'importation';
eBook.ProAcc.Window_Title = 'Importation ProAcc';
eBook.Recycle.IconContextMenu_Empty = 'Effacer tous dossier dans poubelle';
eBook.Repository.FileDetailsPanel_LinksTitle = 'Links';
eBook.Repository.FileDetailsPanel_MetaTitle = 'Information de base';
eBook.Repository.FileDetailsPanel_PreviewTitle = 'Aperçu Pdf';
eBook.Repository.FileMetaPanel_BOOKYEAR = 'Exercice comptable';
eBook.Repository.FileMetaPanel_CALENDERYEAR = 'Année calendrier';
eBook.Repository.FileMetaPanel_FileName = 'Nom fichier';
eBook.Repository.FileMetaPanel_FISCALYEAR = 'Exercice d’imposition ';
eBook.Repository.FileMetaPanel_Location = 'Categorie';
eBook.Repository.FileMetaPanel_MONTHLY = 'Mensuel';
eBook.Repository.FileMetaPanel_PERIOD = 'Période';
eBook.Repository.FileMetaPanel_QUARTERLY = 'Trimestriel';
eBook.Repository.FileMetaPanel_QUARTERMONTH = 'Période';
eBook.Repository.FileMetaPanel_Status = 'Status';
eBook.Repository.FileMetaPanel_YEAR = 'Année (document)';
eBook.Repository.Window_ReplaceMsg = 'Êtes-vous sûr de vouloir supprimer "{0}"?';
eBook.Repository.ReplaceFile = 'Remplacer le fichier';
eBook.Repository.NewFile = 'Nouveau fichier';
eBook.Window_Busy = 'Certaines actions ne sont pas terminées. Une moment s.v.p.';
eBook.Worksheet_UpdateLinkedSheets = 'Voulez-vous actualisez les feuilles de travaille liée?';
eBook.Worksheet.FormPanel_ErrorsMsg = 'Certains champs ne sont pas corrects.';
eBook.Worksheet.FormPanel_ErrorsTitle = 'Données fautives';
eBook.Worksheet.FormPanel_UpdatingItem = 'Côntrole et calculation des données en cours';
eBook.Worksheet.GridPanel_AddItem = 'Ajouter une donnée';
eBook.Worksheet.GridPanel_DeleteItem = 'Effacer donnée';
eBook.Worksheet.GridPanel_DeletingItem = 'Effacer du donnée {0} en cours';
eBook.Worksheet.GridPanel_EditItem = 'Editer donnée';
eBook.Worksheet.Messages_NoMessages = 'Aucun message';
eBook.Worksheet.Rules.Window_Title = 'Mise à jour des feuilles de travaille';
eBook.Worksheet.TabPanel_TabCloseMsg = 'Les données dans cette onglet sont modifiers. Sauvegarder les modifications?';
eBook.Worksheet.TabPanel_TabCloseTitle = 'Sauvegarde des modifications?';
eBook.Worksheet.Window_GeneratingPreview = 'Extrait en cours';
eBook.Worksheet.Window_HelpInfo = 'Aide / Info';
eBook.Worksheet.Window_HelpMsg = '';
eBook.Worksheet.Window_HelpTitle = 'Aide / Info';
eBook.Worksheet.Window_ImportHistory = 'Importation d\'historique (ProAcc)';
eBook.Worksheet.Window_ImportingHistory = 'Importation de historique en cours';
eBook.Worksheet.Window_LoadingRules = 'Calculation en cours';
eBook.Worksheet.Window_PrintPreview = 'Extrait';
eBook.Worksheet.Window_QualityControl = 'Contrôle de la qualité';
eBook.Worksheet.Window_Recalc = 'Recalculer feuille de travaill';
eBook.Xbrl.AttachmentPanel_Add = 'Ajouter un/des fichier(s)';
eBook.Xbrl.AttachmentPanel_automated = 'Cette pièce jointe est,  en cas de besoin, automatiquement ajoutée';
eBook.Xbrl.AttachmentPanel_DefaultInValidMsg = 'Les annexes suivantes sont sélectionnées, mais ne contiennent pas de pièces jointes:';
eBook.Xbrl.AttachmentPanel_Delete = 'effacer';
eBook.Xbrl.AttachmentPanel_FichesTitle = 'Fiches';
eBook.Xbrl.AttachmentPanel_InfoTitle = 'Annexes informatives';
eBook.Xbrl.BizTaxWindow_Generating = 'Préparer déclaration ISOC';
eBook.Xbrl.ClientDetails_Address = 'Adresse';
eBook.Xbrl.ClientDetails_BankInfo = 'Information bancaire';
eBook.Xbrl.ClientDetails_EnterpriseNr = 'Numéro d\'entreprise';
eBook.Xbrl.ClientDetails_Legaltype = 'Forme juridique';
eBook.Xbrl.ClientDetails_Name = 'Nom';
eBook.Xbrl.ClientDetails_Title = 'Données client';
eBook.Xbrl.ContactDetails_Address = 'Adresse';
eBook.Xbrl.ContactDetails_Contacttype = 'Type de contact';
eBook.Xbrl.ContactDetails_Email = 'E-mail';
eBook.Xbrl.ContactDetails_FirstName = 'Prénom';
eBook.Xbrl.ContactDetails_Function = 'Fonction';
eBook.Xbrl.ContactDetails_Name = 'Nom';
eBook.Xbrl.ContactDetails_Phone = 'Téléphone';
eBook.Xbrl.ContactDetails_Title = 'Personne de contact';
eBook.Xbrl.Overview_AttachmentsTitle = 'Annexes diverses';
eBook.Xbrl.Overview_AutomatedFormsTitle = 'Fiches qui sont inclues automatiquement dans l\'envoi';
eBook.Xbrl.Overview_BizTaxCalculation = 'Simulation de BizTax';
eBook.Xbrl.Overview_Box = 'Boite';
eBook.Xbrl.Overview_Bundle = 'Liasse';
eBook.Xbrl.Overview_CalculationTitle = 'Calcul de l\'impôt';
eBook.Xbrl.Overview_ClientTitle = 'Client de la déclaration';
eBook.Xbrl.Overview_ContactTitle = 'Informations sur le document, responsable de la soumission (xbrl)';
eBook.Xbrl.Overview_eBookCalculation = 'Simulation d\'eBook';
eBook.Xbrl.Overview_EnterpriseNr = 'Numéro d\'entreprise';
eBook.Xbrl.Overview_Extension = 'extension';
eBook.Xbrl.Overview_FichesAttachmentsTitle = 'Fiches ci-dessous "Annexes diverses"';
eBook.Xbrl.Overview_GeneratingBundle = 'La liasse est réalisée';
eBook.Xbrl.Overview_History = 'Historique';
eBook.Xbrl.Overview_SendingToBizTax = 'La déclaration est réalisée et envoyée';
eBook.Xbrl.Overview_SendToBizTax = 'Soumettre la déclaration ISOC';
eBook.Xbrl.Overview_Starting = 'L\'assistant BizTax est prêt';
eBook.Xbrl.Overview_Status = 'Préparation de la soumission';
eBook.Xbrl.Overview_ValidatedPartner = 'Validé par le partner/director';
eBook.Xbrl.Overview_Version = 'Version';
eBook.Xbrl.Overview_Wizard = 'Wizard';
eBook.Xbrl.ValidationCheckPanel_Validating = 'Valider ce dossier';
eBook.Xbrl.Wizard_Next = 'Suivant';
eBook.Xbrl.Wizard_Previous = 'Précédent';
eBook.Cancel = 'Annuler';



if(Ext.View){
  Ext.View.prototype.emptyText = '';
}

if(Ext.grid.GridPanel){
  Ext.grid.GridPanel.prototype.ddText = '{0} rang(s) sélectionné(s)';
}

if(Ext.TabPanelItem){
  Ext.TabPanelItem.prototype.closeText = 'Fermer onglet';
}

if(Ext.LoadMask){
  Ext.LoadMask.prototype.msg = 'Chargement en cours...';
}

Date.monthNames = [
  'janvier',
  'février',
  'mars',
  'avril',
  'mai',
  'juin',
  'juillet',
  'août',
  'septembre',
  'octobre',
  'novembre',
  'décembre'
];

Date.getShortMonthName = function(month) {
  if (month == 2) {
  return 'mrt';
  }
  return Date.monthNames[month].substring(0, 3);
};

Date.monthNumbers = {
  jan: 0,
  feb: 1,
  maa: 2,
  apr: 3,
  mei: 4,
  jun: 5,
  jul: 6,
  aug: 7,
  sep: 8,
  okt: 9,
  nov: 10,
  dec: 11
};

Date.getMonthNumber = function(name) {
  var sname = name.substring(0, 3).toLowerCase();
  if (sname == 'mrt') {
    return 2;
  }
  return Date.monthNumbers[sname];
};

Date.dayNames = [
  'dimanche',
  'lundi',
  'mardi',
  'mercredi',
  'jeudi',
  'vendredi',
  'samedi'
];

Date.getShortDayName = function(day) {
  return Date.dayNames[day].substring(0, 3);
};

if(Ext.MessageBox){
  Ext.MessageBox.buttonText = {
    ok: 'OK',
    cancel: 'Annuler',
    yes: 'Oui',
    no: 'Non'
  };
}

if(Ext.util.Format){
  Ext.util.Format.date = function(v, format){
    if (!v) return '';
    if (!(v instanceof Date)) v = new Date(Date.parse(v));
    return v.dateFormat(format || 'j-m-y');
  };
}

if(Ext.DatePicker){
  Ext.apply(Ext.DatePicker.prototype, {
    todayText: 'Aujourd\'hui',
    minText: 'Cette date est avant la date minimale',
    maxText: 'Cette date est après la date minimale',
    disabledDaysText: '',
    disabledDatesText: '',
    monthNames: Date.monthNames,
    dayNames: Date.dayNames,
    nextText: 'Mois suivant (Ctrl+rechts)',
    prevText: 'Mois précédent (Ctrl+links)',
    monthYearText: 'Choisir un mois (Ctrl+haut/bas prochain/précédent année)',
    todayTip: '{0} (espace)',
    format: 'j-m-y',
    okText: '&#160;OK&#160;',
    cancelText: 'Annuler',
    startDay: 1
  });
}

if(Ext.PagingToolbar){
  Ext.apply(Ext.PagingToolbar.prototype, {
    beforePageText: 'Page',
    afterPageText: 'de {0}',
    firstText: 'Première page',
    prevText: 'Page prédédente',
    nextText: 'Page suivante',
    lastText: 'Dernière page',
    refreshText: 'Mettre à jour',
    displayMsg: 'Montré {0} - {1} van {2}',
    emptyMsg: 'Pas de données disponibles pour aperçu'
  });
}

if(Ext.form.Field){
  Ext.form.Field.prototype.invalidText = 'La valeur de ce champ n\'est pas valable';
}

if(Ext.form.TextField){
  Ext.apply(Ext.form.TextField.prototype, {
    minLengthText: 'Longueur minimale du champ est {0}',
    maxLengthText: 'Longueur maximale du champ est {0}',
    blankText: 'Champ obligatoire',
    regexText: '',
    emptyText: null
  });
}

if(Ext.form.NumberField){
  Ext.apply(Ext.form.NumberField.prototype, {
    minText: 'Longueur minimale du champ est {0}',
    maxText: 'Longueur maximale du champ est {0}',
    nanText: '{0} n\'est pas un chiffre valable '
  });
}

if(Ext.form.DateField){
  Ext.apply(Ext.form.DateField.prototype, {
    disabledDaysText: 'Eliminé',
    disabledDatesText: 'Eliminé',
    minText: 'La date dans ce champ doit être après {0}',
    maxText: 'La date dans ce champ doit être avant {0}',
    invalidText: '{0} n\'est pas une date valable - le format date est {1}',
    format: 'j-m-y',
    altFormats: 'd/m/Y|d/m/Y|d/m/Y|d/m|d-m|dm|dmy|dmY|d|Y-m-d'
  });
}

if(Ext.form.ComboBox){
  Ext.apply(Ext.form.ComboBox.prototype, {
    loadingText: 'Chargement en cours...',
    valueNotFoundText: undefined
  });
}

if(Ext.form.VTypes){
  Ext.apply(Ext.form.VTypes, {
    emailText: 'Ce champ doit contenir une adresse e-mail au format "utilisateur@domaine.be"',
    urlText: 'Ce champ doit contenir un URL au format "http:/'+'/www.domaine.be"',
    alphaText: 'Ce champ ne doit contenir que des lettres et_',
    alphanumText: 'Ce champ ne doit contenir que des lettres, des chiffres et_'
  });
}

if(Ext.form.HtmlEditor){
  Ext.apply(Ext.form.HtmlEditor.prototype, {
  createLinkText: 'Introduisez ici l\'URL pour l\'hyperlink en:',
  buttonTips: {
      bold: {
        title: 'Gras (Ctrl+B)',
        text: 'Mettre le texte sélectionné en gras.',
        cls: 'x-html-editor-tip'
      },
      italic: {
        title: 'Italique (Ctrl+I)',
        text: 'Mettre le texte sélectionné en italique.',
        cls: 'x-html-editor-tip'
      },
      underline: {
        title: 'Souligner (Ctrl+U)',
        text: 'Souligner le texte sélectionné.',
        cls: 'x-html-editor-tip'
      },
      increasefontsize: {
        title: 'Agrandir texte',
        text: 'Agrandir le texte.',
        cls: 'x-html-editor-tip'
      },
      decreasefontsize: {
        title: 'Réduire texte',
        text: 'Réduire le texte.',
        cls: 'x-html-editor-tip'
      },
      backcolor: {
        title: 'Couleur arrière-plan texte',
        text: 'Modifier couleur arrière-plan texte sélectionné.',
        cls: 'x-html-editor-tip'
      },
      forecolor: {
        title: 'Couleur texte',
        text: 'Modifier la couleur du texte sélectionné.',
        cls: 'x-html-editor-tip'
      },
      justifyleft: {
        title: 'Aligner texte à gauche',
        text: 'Aligner le texte à gauche.',
        cls: 'x-html-editor-tip'
      },
      justifycenter: {
        title: 'Centrer le texte',
        text: 'Centrer le texte.',
        cls: 'x-html-editor-tip'
      },
      justifyright: {
        title: 'Aligner texte à droite',
        text: 'Aligner le texte à droite.',
        cls: 'x-html-editor-tip'
      },
      insertunorderedlist: {
        title: 'Tiret d\'énumération',
        text: 'Commencer une liste d\'énumération.',
        cls: 'x-html-editor-tip'
      },
      insertorderedlist: {
        title: 'Liste d\'énumération',
        text: 'Commencer une liste d\'énumération.',
        cls: 'x-html-editor-tip'
      },
      createlink: {
        title: 'Hyperlink',
        text: 'Faire du texte un hyperlink.',
        cls: 'x-html-editor-tip'
      },
      sourceedit: {
        title: 'Modifier source',
        text: 'Modifier la source.',
        cls: 'x-html-editor-tip'
      }
    }
  });
}

if(Ext.form.BasicForm){
  Ext.form.BasicForm.prototype.waitTitle = 'Veuillez patienter s.v.p....';
}

if(Ext.grid.GridView){
  Ext.apply(Ext.grid.GridView.prototype, {
    sortAscText: 'Tri ascendant',
    sortDescText: 'Tri descendant',
    lockText: 'Bloquer colonne',
    unlockText: 'Débloquer colonne',
    columnsText: 'Colonnes'
  });
}

if(Ext.grid.GroupingView){
  Ext.apply(Ext.grid.GroupingView.prototype, {
  emptyGroupText: '(Aucun)',
  groupByText: 'Grouper ce champ',
  showGroupsText: 'Montrer en groupes'
  });
}

if(Ext.grid.PropertyColumnModel){
  Ext.apply(Ext.grid.PropertyColumnModel.prototype, {
    nameText: 'Nom',
    valueText: 'Valeur',
    dateFormat: 'j-m-Y'
  });
}

if(Ext.layout.BorderLayout && Ext.layout.BorderLayout.SplitRegion){
  Ext.apply(Ext.layout.BorderLayout.SplitRegion.prototype, {
    splitTip: 'Glisser afin de modifier la grandeur.',
    collapsibleSplitTip: 'Glisser afin de modifier la grandeur. Cliquez deux fois pour cacher.'
  });
}

if(Ext.form.TimeField){
  Ext.apply(Ext.form.TimeField.prototype, {
    minText: 'Le temps dans ce champ doit être égal ou au-delà {0}',
    maxText: 'Le temps dans ce champ doit être égal ou en deçà {0}',
    invalidText: '{0} n\'est pas un moment valable',
    format: 'G:i',
    altFormats: 'g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H'
  });
}