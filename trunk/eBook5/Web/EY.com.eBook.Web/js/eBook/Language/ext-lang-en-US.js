/*
  * List compiled by mystix on the extjs.com forums.
  * Thank you Mystix!
  *
  * Dutch Translations
  * by Ido Sebastiaan Bas van Oostveen (12 Oct 2007)
  */

Ext.UpdateManager.defaults.indicatorText = '<div class="loading-indicator">Loading...</div>';

Ext.ns('eBook.Language');
Ext.ns('eBook.Language.Roles');

eBook.Language.YEAREND = 'Yearend closing';
eBook.Language.BIZTAXPREP = 'BizTax preparation';
eBook.Language.BIZTAXDECLARATION = 'BizTax declaration';
eBook.Language.BIZTAX_AUTO = 'BizTax module (worksheets)';
eBook.Language.BIZTAX_MANUAL = 'BizTax module (without worksheets)';
eBook.Language.ANNUALACCOUNTS_FILING = 'Filing annual accounts';

eBook.Language.Roles.ADMIN = 'Administrator';
eBook.Language.Roles.CHAMPION = 'eBook Champion';

eBook.Accounts.Manage.EditWindow_Title = 'Rekening editeren';
eBook.Accounts.Manage.EditWindow_TitleMappings = 'Mappings';
eBook.Accounts.Manage.EditWindow_TitleTranslations = 'Vertalingen';
eBook.Accounts.Manage.MappingGrid_DeletingMapping = 'Mapping wordt verwijderd voor {0} {1}';
eBook.Accounts.Manage.MappingGrid_Mapping = 'Mapping';
eBook.Accounts.Manage.MappingGrid_SavingMapping = 'Mapping wordt bewaard voor {0} {1}';
eBook.Accounts.Manage.MappingGrid_WorksheetHeader = 'Werkpapier';
eBook.Accounts.Manage.TranslationsGrid_Add = 'Voeg een lijn toe';
eBook.Accounts.Manage.TranslationsGrid_DefaultText = 'Nieuwe omschrijving';
eBook.Accounts.Manage.TranslationsGrid_Delete = 'Verwijder lijn';
eBook.Accounts.Manage.TranslationsGrid_Deleting = 'Omschrijving voor {0} {1} wordt verwijderd';
eBook.Accounts.Manage.TranslationsGrid_Description = 'Omschrijving';
eBook.Accounts.Manage.TranslationsGrid_Language = 'Taal';
eBook.Accounts.Manage.TranslationsGrid_Saving = 'Omschrijving voor {0} {1} wordt bewaard';
eBook.Accounts.Manage.Window_AccountDesc = 'Omschrijving';
eBook.Accounts.Manage.Window_AccountNr = 'Rekeningnr';
eBook.Accounts.Manage.Window_AddAccount = 'Nieuwe rekening';
eBook.Accounts.Manage.Window_EditAccount = 'Wijzig rekening';
eBook.Accounts.Manage.Window_SearchText = 'Rekening opzoeken';
eBook.Accounts.Manage.Window_SearchTip = 'Rekening opzoeken';
eBook.Accounts.Manage.Window_SelectAll = 'Selecteer alle velden';
eBook.Accounts.Manage.Window_Title = 'Beheer rekeningen';
eBook.Accounts.Mappings.GridPanel_ColumnAccount = 'Rekening';
eBook.Accounts.Mappings.GridPanel_ColumnBelRes = 'Reserves';
eBook.Accounts.Mappings.GridPanel_ColumnBTW = 'BTW-omzet aansluiting';
eBook.Accounts.Mappings.GridPanel_ColumnDBI = 'DBI';
eBook.Accounts.Mappings.GridPanel_ColumnErelonen = 'Erelonen';
eBook.Accounts.Mappings.GridPanel_ColumnRV = 'Rentegevende voorschotten';
eBook.Accounts.Mappings.GridPanel_ColumnRVI = 'RV/Intrest';
eBook.Accounts.Mappings.GridPanel_ColumnSalaris = 'Salaris aansluiting';
eBook.Accounts.Mappings.GridPanel_ColumnSaldo = 'Saldo';
eBook.Accounts.Mappings.GridPanel_ColumnSaldoPrevious = 'Vorig boekjaar';
eBook.Accounts.Mappings.GridPanel_ColumnVAA = 'Voordelen alle aard';
eBook.Accounts.Mappings.GridPanel_ColumnVU = 'Verworpen uitgaven';
eBook.Accounts.Mappings.GridPanel_ColumnMWAandelen = 'Capital Gains Shares';
eBook.Accounts.Mappings.GridPanel_EditAccount = 'Rekening wijzigen';
eBook.Accounts.Mappings.GridPanel_ExportExcelAdjustments = 'Exporteer adjustments';
eBook.Accounts.Mappings.GridPanel_ExportMappings = 'Mappingen';
eBook.Accounts.Mappings.GridPanel_ExportProAccAdjustments = 'Exporteer Adjustments';
eBook.Accounts.Mappings.GridPanel_ExportState = 'Exporteer historiek ProAcc & eBook';
eBook.Accounts.Mappings.GridPanel_ImportExcel = 'Importeer';
eBook.Accounts.Mappings.GridPanel_ImportExport = 'Import / Export';
eBook.Accounts.Mappings.GridPanel_ImportProAcc = 'Importeer uit ProAcc';
eBook.Accounts.Mappings.GridPanel_ManageAccounts = 'Beheer rekeningen';
eBook.Accounts.Mappings.GridPanel_NewAccount = 'Nieuwe rekening';
eBook.Accounts.Mappings.GridPanel_PreparingExcel = 'Excel wordt voorbereid';
eBook.Accounts.Mappings.GridPanel_PreparingExcelAdjust = 'Adjustments worden geëxporteerd';
eBook.Accounts.Mappings.GridPanel_ViewDetail = 'Detailweergave';
eBook.Accounts.Mappings.GridPanel_ViewFilter = 'Weergave / Filter';
eBook.Accounts.Mappings.GridPanel_ViewSaldi0 = 'Toon saldi 0,00€';
eBook.Accounts.Mappings.Window_DeletingMapping = 'Mapping wordt verwijderd voor {0} {1}';
eBook.Accounts.Mappings.Window_SavingMapping = 'Mapping wordt bewaard voor {0} {1}';
eBook.Accounts.Mappings.Window_SavingMappings = 'Bezig met het bewaren van mappingen.';
eBook.Accounts.Mappings.Window_Title = 'Mapping algemene rekeningen';
eBook.Accounts.New.Window_AccountDesc = 'Omschrijving ({0})';
eBook.Accounts.New.Window_AccountNr = 'Rekeningnr';
eBook.Accounts.New.Window_Saving = 'Rekening {0} {1} wordt aangemaakt';
eBook.Accounts.New.Window_SavingBusy = 'Rekening wordt aangemaakt';
eBook.Accounts.New.Window_Title = 'Nieuwe rekening';
eBook.Bundle_Statements = 'Jaarrekeningen';
eBook.Bundle.BundlePanel_Library = 'Bibliotheek';
eBook.Bundle.BundlePanel_NewChapter = 'Nieuw hoofdstuk';
eBook.Bundle.NewWindow_Bundle_Create = 'Create bundle';
eBook.Bundle.NewWindow_Deliverable_Create = 'Create deliverable';
eBook.Bundle.NewWindow_Creating = 'Bundel wordt aangemaakt';
eBook.Bundle.NewWindow_IncludePrevious = 'Inclusief bundels van het vorige dossier';
eBook.Bundle.NewWindow_Language = 'Kies een taal';
eBook.Bundle.NewWindow_Layout = 'Kies een start layout';
eBook.Bundle.NewWindow_Deliverable_Name = 'Naam van de deliverable';
eBook.Bundle.NewWindow_Bundle_Name = 'Naam van de bundle';
eBook.Bundle.NodeInfoPanel_Detailed = 'Gedetailleerde weergave';
eBook.Bundle.NodeInfoPanel_Footer = 'Voettekst';
eBook.Bundle.NodeInfoPanel_Header = 'Koptekst';
eBook.Bundle.NodeInfoPanel_InfoTitle = 'Item info: {0}';
eBook.Bundle.NodeInfoPanel_Language = 'Taal';
eBook.Bundle.NodeInfoPanel_PageFrom = 'Vanaf pagina';
eBook.Bundle.NodeInfoPanel_PageTo = 'Tot pagina';
eBook.Bundle.NodeInfoPanel_ReconfigureChildren = 'Herconfigureer alle items';
eBook.Bundle.NodeInfoPanel_ReportSettings = 'Algemene configuratie';
eBook.Bundle.NodeInfoPanel_ShowFootNote = 'Toon voetnoot';
eBook.Bundle.NodeInfoPanel_ShowInIndex = 'Toon in indexpagina';
eBook.Bundle.NodeInfoPanel_ShowPageNr = 'Toon pagina nummering';
eBook.Bundle.NodeInfoPanel_ShowTitle = 'Toon item titel';
eBook.Bundle.NodeInfoPanel_Title = 'Titel in de bundel';
eBook.Bundle.NodeInfoPanel_UpdateChildrenMsg = 'Voor alle elementen (hoofdstukken, documenten, pdf\'s, werkpapieren,jaarrekeningen,...) in deze bundel wordt de header-footer configuratie overgenomen van deze instelling. Bent u zeker dat u dit wenst uit te voeren?';
eBook.Bundle.NodeInfoPanel_UpdateChildrenTitle = 'Herconfigureer alle items?';
eBook.Bundle.ObjectsTree_Title = 'Bibliotheek';
eBook.Bundle.Window_DeleteItem = 'Item verwijderen';
eBook.Bundle.Window_DeletePdfMsg = 'Bent u zeker dat het bestand  "{0}" wenst te verwijderen?';
eBook.Bundle.Window_DeleteReportTitle = 'Bundel verwijderen';
eBook.Bundle.Window_DeletingPdfMsg = 'Deleting PDF "{0}"';
eBook.Bundle.Window_DeletingReport = 'Deleting "{0}"...';
eBook.Bundle.Window_EmptyMenu = 'No items found';
eBook.Bundle.Window_GeneralActions = 'Algemene acties';
eBook.Bundle.Window_GeneratingReport = 'De bundel {0} wordt gegenereerd';
eBook.Bundle.Window_LibraryActions = 'Bibliotheek acties';
eBook.Bundle.Window_LibraryAddPdf = 'PDF toevoegen';
eBook.Bundle.Window_LibraryDeletePdf = 'Pdf verwijderen';
eBook.Bundle.Window_LibraryReload = 'Bibliotheek herladen';
eBook.Bundle.Window_LoadingLibrary = 'De bibliotheek wordt geladen';
eBook.Bundle.Window_LoadingMenu = 'Bezig met zoeken naar bestaande bundels';
eBook.Bundle.Window_LoadingReport = 'De bundel {0} wordt geladen';
eBook.Bundle.Window_NewChapter = 'Hoofstuk toevoegen';
eBook.Bundle.Window_ReportActions = 'Bundel acties';
eBook.Bundle.Window_ReportDelete = 'Bundel verwijderen';
eBook.Bundle.Window_ReportGenerate = 'Bundel genereren';
eBook.Bundle.Window_ReportNew = 'Nieuwe bundel';
eBook.Bundle.Window_ReportOpen = 'Bundel openen';
eBook.Bundle.Window_ReportSave = 'Bundel bewaren';
eBook.Bundle.Window_SavingReport = 'De bundel {0} wordt bewaard';
eBook.Bundle.Window_SpecificActions = 'Workflow';
eBook.Bundle.Window_Status = 'Status';
eBook.Bundle.Window_Submit = 'Submit';
eBook.Bundle.Window_Approve = 'Approve';
eBook.Bundle.Window_Reject = 'Reject';
eBook.Bundle.Window_ReviewNotes = 'Review Notes';
eBook.Bundle.Window_AddComment = 'Open';
eBook.Bundle.Window_AddComment_Title = 'Review Notes';
eBook.Bundle.Window_Unlock = 'Unlock';
eBook.Bundle.Window_Lock = 'Lock';
eBook.BusinessRelations.FormPanel_Address = 'Adres';
eBook.BusinessRelations.FormPanel_City = 'Woonplaats';
eBook.BusinessRelations.FormPanel_Country = 'Land';
eBook.BusinessRelations.FormPanel_EnterpriseNumber = 'Ondernemingsnummer';
eBook.BusinessRelations.FormPanel_FirstName = 'Voornaam';
eBook.BusinessRelations.FormPanel_LastName = 'Firmanaam / achternaam';
eBook.BusinessRelations.FormPanel_Saving = '{0} wordt bewaard';
eBook.BusinessRelations.FormPanel_VatNumber = 'B.T.W. nummer';
eBook.BusinessRelations.FormPanel_ZipCode = 'Postcode';
eBook.BusinessRelations.Window_Customer = 'klant';
eBook.BusinessRelations.Window_Delete = 'Verwijder {0}';
eBook.BusinessRelations.Window_DeleteMsg = 'Bent u zeker dat u {0} wenst te verwijderen?';
eBook.BusinessRelations.Window_DeleteTitle = '{0} verwijderen';
eBook.BusinessRelations.Window_Deleting = '{0} wordt verwijderd';
eBook.BusinessRelations.Window_Edit = 'Wijzig {0}';
eBook.BusinessRelations.Window_Import = 'Importeren';
eBook.BusinessRelations.Window_ImportExcel = 'Import van Excel';
eBook.BusinessRelations.Window_Importing = 'Bezig met importeren';
eBook.BusinessRelations.Window_ImportProAcc = 'Import van ProAcc';
eBook.BusinessRelations.Window_Manage = 'Beheer {0}';
eBook.BusinessRelations.Window_New = 'Nieuwe {0}';
eBook.BusinessRelations.Window_Supplier = 'leverancier';
eBook.Client.PreWorksheetWindow_Create = 'Creëer werkpapier';
eBook.Client.PreWorksheetWindow_Description = 'Omschrijving';
eBook.Client.PreWorksheetWindow_New = 'Nieuwe aangifte aanmaken';
eBook.Client.PreWorksheetWindow_Open = 'Open werkpapier';
eBook.Client.PreWorksheetWindow_PreviousEnddate = 'Eind vorig boekjaar';
eBook.Client.PreWorksheetWindow_Title = 'Manuele aangifte vennootschapsbelasting';
eBook.Client.Window_Active = 'Actief';
eBook.Client.Window_Address = 'Adres';
eBook.Client.Window_City = 'Woonplaats';
eBook.Client.Window_Country = 'Land';
eBook.Client.Window_Director = 'Directeur';
eBook.Client.Window_eBookOverrule = 'Alternatieve data';
eBook.Client.Window_Email = 'Email';
eBook.Client.Window_EnterpriseNr = 'Ondernemingsnummer';
eBook.Client.Window_Fax = 'Fax';
eBook.Client.Window_GFIS = 'GFIS Code';
eBook.Client.Window_LegalStruct = 'Entiteit';
eBook.Client.Window_Loading = 'Bezig met laden';
eBook.Client.Window_Name = 'Naam';
eBook.Client.Window_Phone = 'Telefoon';
eBook.Client.Window_Rpr = 'RPR';
eBook.Client.Window_SaveChanges = 'Wijzigingen bewaren';
eBook.Client.Window_SaveMsg = 'De gevens werden gewijzigd. Wijzigingen bewaren?';
eBook.Client.Window_SaveTitle = 'Wijziging bewaren?';
eBook.Client.Window_Saving = 'Gegevens worden bewaard';
eBook.Client.Window_Vat = 'B.T.W. nummer';
eBook.Client.Window_VatElegible = 'B.T.W. verplicht';
eBook.Client.Window_ZipCode = 'Postcode';
eBook.Closing.View_BizTaxDecline = 'Dit dossier omvat geen aangifte door EY';
eBook.Closing.View_BizTaxExecute = 'Aangifte uitvoeren';
eBook.Closing.View_BizTaxTitle = 'Aangifte vennootschapsbelasting';
eBook.Closing.View_ClosingAllText = 'Sluit alle onderdelen af';
eBook.Closing.View_ClosingReportsMsg = 'Bezig met het afsluiten van bundels';
eBook.Closing.View_ClosingReportsTitle = 'Afsluiten bundels';
eBook.Closing.View_ClosingText = 'Afsluiten';
eBook.Closing.View_ClosingWorksheets = 'Werkpapieren worden afgesloten';
eBook.Closing.View_ClosingWorksheetsMsg = 'Bezig met het afsluiten van werkpapieren';
eBook.Closing.View_ClosingWorksheetsTitle = 'Afsluiten werkpapieren';
eBook.Closing.View_DeleteText = 'Verwijderen';
eBook.Closing.View_FileCloseOk = 'Alle voorgaande stappen zijn afgesloten, u kan nu het dossier finaal afsluiten.';
eBook.Closing.View_FileClosePrevious = 'Alle voorgaande stappen dienen te zijn afgesloten alvorens u het dossier definitief kan afsluiten.';
eBook.Closing.View_FileIsClosed = 'Dit dossier is finaal afgesloten.';
eBook.Closing.View_FileTitle = 'Dossier finaal afsluiten';
eBook.Closing.View_FinalClosing = 'Finaal afsluiten';
eBook.Closing.View_InvestigateFile = 'Dossier wordt onderzocht';
eBook.Closing.View_NumbersInfo = 'In de eerste stap van de afsluitprocedure wordt de balans vergrendeld.';
eBook.Closing.View_NumbersProAccMsg = 'Sluit eerst het boekjaar af in ProAcc. Pas dan kan het eBook dossier worden afgesloten';
eBook.Closing.View_NumbersProfitLossMsg = 'Er is een verschil tussen de winst/verlies van het boekjaar in ProAcc [{0}] en eBook [{1}]. Dit dossier kan niet worden afgesloten.';
eBook.Closing.View_NumbersQ1 = 'Dit dossier betreft een jaarafsluiting?';
eBook.Closing.View_NumbersQ2 = 'Voor dit dossier voert E&Y de boekhouding in ProAcc?';
eBook.Closing.View_NumbersQ3 = 'Werd het boekjaar in ProAcc afgesloten?';
eBook.Closing.View_NumbersQ4 = 'Vervolledig het bedrag van de winst/verlies van het respectievelijke boekjaar (in EUR, met 2 decimalen)';
eBook.Closing.View_NumbersTitle = 'Cijfers afsluiten';
eBook.Closing.View_OpenText = 'Openen';
eBook.Closing.View_ReOpen = 'Heropenen';
eBook.Closing.View_ReOpenAllWorksheet = 'Alle werkpapieren worden heropend';
eBook.Closing.View_ReOpenBundle = 'Bundel wordt heropend';
eBook.Closing.View_ReOpenFile = 'Dossier wordt heropend';
eBook.Closing.View_ReOpenNumber = 'Cijfers worden heropend';
eBook.Closing.View_ReportsPreviousUnClosedText = 'Pas als de cijfers en alle werkpapieren zijn afgesloten, kan u de aangemaakte bundels afsluiten.';
eBook.Closing.View_ReportsTitle = 'Bundel(s) afsluiten';
eBook.Closing.View_SharedInfo = '!! Opgelet: Deze cliënt is een gedeelde klant tussen ACR &amp; BTC';
eBook.Closing.View_TaxValidation = '<b><u>Belasting berekening validatie</u></b><br/>Voor hercalculatie:<b>{0}</b><br/>Na hercalculatie:<b>{1}</b>.<br/>Wenst u door te gaan met het afsluiten van de werkpapieren?';
eBook.Closing.View_ViewText = 'Bekijken';
eBook.Closing.View_WorksheetsNumbersUnlockedText = 'Pas als de cijfers zijn afgesloten, kan u de werkpapieren afsluiten.';
eBook.Closing.View_WorksheetsTitle = 'Werkpapieren afsluiten';
eBook.Closing.View_WorksheetsWithErrors = 'Enkele werkpapieren bevatten nog foutmeldingen. De afsluitprocedure werd geannuleerd';
eBook.Closing.Window_BundleMsg = 'De melding \'draft\' op de bundel wordt automatisch verwijderd van zodra het dossier  (inclusief bundel) finaal  is afgesloten.';
eBook.Closing.Window_NumbersMsg = 'Eens u de cijfers heeft afgesloten:<ul><li>kan er geen update van de cijfers meer plaatsvinden (ProAcc/Excel)</li><li>kunnen er geen boekingen meer worden aangemaakt (manueel/automatisch)</li><li>blijft een wijziging van de mapping wel mogelijk</li></ul>';
eBook.Closing.Window_Title = 'Afsluitprocedure';
eBook.Create.Empty.ExcelFileFieldSet_File = 'Bestand';
eBook.Create.Empty.ExcelFileFieldSet_Select = 'Selecteer excelbestand';
eBook.Create.Empty.ExcelFileFieldSet_Sheets = 'Werkbladen';
eBook.Create.Empty.ExcelFileFieldSet_Title = 'Excelbestand';
eBook.Create.Empty.FileDatesFieldSet_Assessmentyear = 'Aanslagjaar';
eBook.Create.Empty.FileDatesFieldSet_CurrentAssesment = 'Huidig aanslagjaar';
eBook.Create.Empty.FileDatesFieldSet_Enddate = 'Einddatum';
eBook.Create.Empty.FileDatesFieldSet_EnddateInvalid = 'De einddatum kan niet voor de startdatum komen.';
eBook.Create.Empty.FileDatesFieldSet_Startdate = 'Startdatum';
eBook.Create.Empty.FileInfoFieldSet_FileType = 'Dossiertype';
eBook.Create.Empty.FileInfoFieldSet_Language = 'Taal dossier';
eBook.Create.Empty.FileInfoFieldSet_Name = 'Dossier benaming';
eBook.Create.Empty.FileInfoFieldSet_Title = 'Dossiergegevens';
eBook.Create.Empty.MainPanel_Create = 'Dossier aanmaken';
eBook.Create.Empty.MainPanel_ImportPrevious = 'Rekeningschema en mappings van het vorige dossier importeren';
eBook.Create.Empty.MainPanel_ImportPreviousWorksheet = 'Werkpapier {0} initialiseren op basis van vorig dossier';
eBook.Create.Empty.MainPanel_ImportStandard = 'Aanvullen met standaard Ernst & Young schema';
eBook.Create.Empty.MainPanel_Loading = 'Dossier {0} wordt geladen';
eBook.Create.Empty.MainPanel_MissingData = 'Niet alle gegevens zijn ingevuld/correct';
eBook.Create.Empty.MainPanel_Undo = 'Creatie dossier ongedaan maken';
eBook.Create.Empty.PreviousFileFieldSet_EbookFile = 'Vorig eBook dossier';
eBook.Create.Empty.PreviousFileFieldSet_Enddate = 'Einddatum';
eBook.Create.Empty.PreviousFileFieldSet_Startdate = 'Startdatum';
eBook.Create.Empty.PreviousFileFieldSet_Title = 'Vorig aanslagjaar';
eBook.Create.ProAcc.FileDatesFieldSet_Bookyear = 'Boekjaar';
eBook.Create.ProAcc.FileDatesFieldSet_GetDates = 'Datums worden opgehaald';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccAccounts = 'Rekeningschema importeren uit ProAcc';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccEndJournal = 'Eindjournaal importeren <u>vorig</u> boekjaar uit ProAcc';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccStartJournal = 'Startjournaal importeren <u>huidig</u> boekjaar uit ProAcc';
eBook.Create.ProAcc.MainPanel_ImportProAccAccounts = 'Importeer rekeningstelsel uit ProAcc';
eBook.Create.ProAcc.MainPanel_ImportProAccEndJournal = 'Importeer eindbalans vorig boekjaar uit ProAcc';
eBook.Create.ProAcc.MainPanel_ImportProAccStartJournal = 'Importeer eindbalans huidig boekjaar uit ProAcc';
eBook.Create.Exact.FinancialYears = 'Select period';
eBook.Create.Window_ActionTitle = 'Dossier wordt aangemaakt :';
eBook.Create.Window_Copy = 'Kopiëer dossier';
eBook.Create.Window_EmptyFile = 'Leeg dossier';
eBook.Create.Window_Excel = 'Importeer uit Excel';
eBook.Create.Window_Exact = 'Import Exact (excel)';
eBook.Create.Window_ImportTypes = 'Selecteer type creatie / import';
eBook.Create.Window_ProAcc = 'Importeer uit ProAcc';
eBook.Document.Fields.BlockEditor_Cancel = 'Ongedaan maken';
eBook.Document.Fields.BlockEditor_Save = 'Bewaren';
eBook.Document.NewDocumentWindow_CreateDoc = 'Document aanmaken';
eBook.Document.NewDocumentWindow_DocName = 'Documentnaam';
eBook.Document.NewDocumentWindow_Group = 'Documenttype';
eBook.Document.NewDocumentWindow_Language = 'Taal';
eBook.Document.NewDocumentWindow_LoadingTemplates = 'Sjablonen worden geladen';
eBook.Document.NewDocumentWindow_NoTemplateSelected = 'Geen sjabloon geselecteerd';
eBook.Document.NewDocumentWindow_Template = 'Sjabloon';
eBook.Document.NewDocumentWindow_Title = 'Nieuw document aanmaken';
eBook.Document.OpenDocumentWindow_NoDocuments = 'Geen sjablonen gevonden';
eBook.Document.Window_Loading = 'Document wordt geladen';
eBook.Document.Window_LoadingMenu = 'Bezig met zoeken naar bestaande documenten';
eBook.Document.Window_MenuEmpty = 'Geen bestaande documenten gevonden';
eBook.Document.Window_NewDocument = 'Nieuw document';
eBook.Document.Window_OpenDocument = 'Open document';
eBook.Document.Window_PrintPreview = 'Afdrukvoorbeeld';
eBook.Document.Window_SaveDocument = 'Bewaar document';
eBook.Document.Window_Saving = 'Document {0} wordt bewaard';
eBook.Document.Window_Title = 'Document console';
eBook.Excel.AccountSchemaPanel_AccountDesc = 'Rekeningomschrijving';
eBook.Excel.AccountSchemaPanel_AccountNr = 'Rekeningnummer';
eBook.Excel.AccountSchemaPanel_Language = 'Taal';
eBook.Excel.BalansPanel_Saldo = 'Saldo';
eBook.Excel.Grid_NoWorksheetMsg = 'Selecteer eerst een werkblad';
eBook.Excel.Grid_NoWorksheetTitle = 'Geen werkblad';
eBook.Excel.Grid_Preview = 'Voorbeeldweergave';
eBook.Excel.MappingPanel_Sheets = 'Excel werkblad';
eBook.Excel.UploadWindow_Invalid = 'Foutief bestand';
eBook.Excel.UploadWindow_Processing = 'Bestand wordt verwerkt';
eBook.Excel.UploadWindow_SelectFile = 'Selecteer excelbestand';
eBook.Excel.UploadWindow_Title = 'Excel bestand opladen';
eBook.Excel.UploadWindow_Upload = 'Bestand opladen';
eBook.Excel.UploadWindow_Uploading = 'Bestand wordt opgeladen';
eBook.Excel.Window_AccountSchema = 'Rekeningschema';
eBook.Excel.Window_CurrentBalans = 'Huidige balans';
eBook.Excel.Window_Import = 'Importeer';
eBook.Excel.Window_ImportAS = 'Importeren van het rekeningschema';
eBook.Excel.Window_ImportData = 'Importeer gegevens';
eBook.Excel.Window_ImportDoneMsg = 'Het importeren is afgerond.';
eBook.Excel.Window_ImportDoneTitle = 'Import voltooid';
eBook.Excel.Window_ImportHB = 'Importeren van de balans huidig boekjaar';
eBook.Excel.Window_Importing = 'Gegevens worden geïmporteerd :';
eBook.Excel.Window_ImportVB = 'Importeren van de balans vorig boekjaar';
eBook.Excel.Window_NoDataMsg = 'Duidt eerst de gegevens aan die u wenst te importeren.';
eBook.Excel.Window_NoDataTitle = 'Geen gegevens';
eBook.Excel.Window_PreviousBalans = 'Balans vorig boekjaar';
eBook.Excel.Window_TabFailedMsg = 'Niet alle geselecteerde tabbladen zijn correct.';
eBook.Excel.Window_TabFailedTitle = 'Incorrecte tabblad(en)';
eBook.ExtraFiles.Window_Processing = 'Bestand {0} wordt verwerkt';
eBook.ExtraFiles.Window_SelectExWord = 'Selecteer Excel-/Wordbestand';
eBook.ExtraFiles.Window_Title = 'Overige werkpapieren';
eBook.ExtraFiles.Window_Upload = 'Bestand opladen';
eBook.ExtraFiles.Window_Uploading = 'Bestand wordt opgeladen';
eBook.Fields.FodAddress_Adrestype = 'Adrestype';
eBook.Fields.FodAddress_Box = 'Bus';
eBook.Fields.FodAddress_City = 'Woonplaats';
eBook.Fields.FodAddress_Country = 'Land';
eBook.Fields.FodAddress_Nr = 'Nr';
eBook.Fields.FodAddress_Street = 'Straat';
eBook.Fields.FodAddress_Zipcode = 'Postcode';
eBook.Fields.FodPhone_Country = 'Landcode';
eBook.Fields.FodPhone_Extension = 'Extensie';
eBook.Fields.FodPhone_Local = 'Lokaal telefoonnr';
eBook.Fields.FodPhone_Zone = 'Zone';
eBook.File.HomeHeader_BizTax = 'BizTax aangifte';
eBook.File.HomeHeader_Role = 'Current role(s)';
eBook.File.HomeHeader_YearEnd = 'Jaarafsluiting';
eBook.Help.ChampionsGrid_Email = 'E-mail';
eBook.Help.ChampionsGrid_FirstName = 'Voornaam';
eBook.Help.ChampionsGrid_LastName = 'Achternaam';
eBook.Help.ChampionsGrid_Office = 'Kantoor';
eBook.InterfaceTrans_Client = 'Cliënt';
eBook.InterfaceTrans_ClientAddress = 'Adres';
eBook.InterfaceTrans_ClientCity = 'Woonplaats';
eBook.InterfaceTrans_ClientCountry = 'Land';
eBook.InterfaceTrans_ClientName = 'Naam';
eBook.InterfaceTrans_ClientSearch = 'Klant opzoeken en openen';
eBook.InterfaceTrans_ClientSearchText = 'Klant opzoeken';
eBook.InterfaceTrans_ClientZipCode = 'Postcode';
eBook.InterfaceTrans_EndDate = 'Einde boekjaar';
eBook.InterfaceTrans_File = 'Dossier';
eBook.InterfaceTrans_FileName = 'naam';
eBook.InterfaceTrans_GFISCode = 'GFIS Code';
eBook.InterfaceTrans_LastAccessed = 'Laast geopend';
eBook.InterfaceTrans_LoadingClient = 'Klant {0} wordt ingeladen';
eBook.InterfaceTrans_LoadingClients = 'Klanten worden ingeladen';
eBook.InterfaceTrans_LoadingFile = 'Dossier {0} wordt geladen';
eBook.InterfaceTrans_LoggingIn = 'Bezig met aanmelden & synchronisatie met PMT';
eBook.InterfaceTrans_MenuEbook = 'eBook';
eBook.InterfaceTrans_MenuStatistics = 'Statistieken';
eBook.InterfaceTrans_MyLatestFiles = 'Mijn laatst geopende dossiers';
eBook.InterfaceTrans_NoProAccMsg = 'De huidige klant heeft geen gekende connectie met een ProAcc database';
eBook.InterfaceTrans_NoProAccTitle = 'Geen ProAcc connectie';
eBook.InterfaceTrans_SelectAllFields = 'Alle velden selecteren';
eBook.InterfaceTrans_StartDate = 'Start boekjaar';
eBook.Journal.Booking.Grid_AccountNr = 'Rekening';
eBook.Journal.Booking.Grid_Credit = 'Credit';
eBook.Journal.Booking.Grid_Debet = 'Debet';
eBook.Journal.Booking.Grid_DeleteLineTip = 'Verwijder lijn';
eBook.Journal.Booking.Panel_Desc = 'Omschrijving/Titel';
eBook.Journal.Booking.Panel_HideClients = 'Verberg voor klanten';
eBook.Journal.Booking.Window_AddLines = 'Voeg lijnen toe';
eBook.Journal.Booking.Window_DebetCredit = 'Debet {0} is niet gelijk aan credit {1}';
eBook.Journal.Booking.Window_ImportExcel = 'Importeer uit Excel';
eBook.Journal.Booking.Window_Save = 'Bewaar boeking';
eBook.Journal.Booking.Window_Saving = 'De boeking "{0}" wordt bewaard';
eBook.Journal.Grid_Account = 'Rekening';
eBook.Journal.Grid_AdjustmentsCredit = 'Adjustments credit';
eBook.Journal.Grid_AdjustmentsDebet = 'Adjustments debet';
eBook.Journal.Grid_Credit = 'Credit';
eBook.Journal.Grid_Debet = 'Debet';
eBook.Journal.Grid_End = 'Einde balans';
eBook.Journal.Grid_Start = 'Start balans';
eBook.Journal.GroupedGrid_Account = 'Rekening';
eBook.Journal.GroupedGrid_Booking = 'Boeking';
eBook.Journal.GroupedGrid_Credit = 'Credit';
eBook.Journal.GroupedGrid_Debet = 'Debet';
eBook.Journal.GroupedGrid_DeleteBooking = 'Verwijder boeking';
eBook.Journal.GroupedGrid_DeleteBookingMsg = 'Betn u zeker dat u de boeking {0} wenst te verwijderen?';
eBook.Journal.GroupedGrid_DeleteBookingTitle = 'Boeking verwijderen?';
eBook.Journal.GroupedGrid_DeletingBooking = 'Boeking {0} wordt verwijderd';
eBook.Journal.GroupedGrid_EditBooking = 'Wijzig boeking';
eBook.Journal.GroupedGrid_BusinessRelation = 'Client/Supplier';
eBook.Journal.GroupedGrid_Comment = 'Comment';
eBook.Journal.Window_AddBooking = 'Voeg boeking toe';
eBook.Journal.Window_ExcelExporting = 'De gevens worden verzameld & het excel bestand wordt aangemaakt';
eBook.Journal.Window_ExportExcel = 'Exporteer naar Excel';
eBook.Journal.Window_ImportExcel = 'Importeer uit Excel';
eBook.Journal.Window_ImportExport = 'Import / Export';
eBook.Journal.Window_ImportProAcc = 'Importeer uit ProAcc';
eBook.Language_Dutch = 'Dutch';
eBook.Language_English = 'Engels';
eBook.Language_French = 'Frans';
eBook.Language_German = 'Duits';
eBook.Menu.Client_ClientData = 'Klantgegevens';
eBook.Menu.Client_ClosedFiles = 'Gesloten dossiers';
eBook.Menu.Client_Customers = 'Klanten';
eBook.Menu.Client_Deleted = '';
eBook.Menu.Client_EndDate = 'Einde boekjaar';
eBook.Menu.Client_FileName = 'Dossiernaam';
eBook.Menu.Client_NewFile = 'Nieuw dossier';
eBook.Menu.Client_NoClosedFiles = 'Geen afgesloten dossiers gevonden';
eBook.Menu.Client_NoDeletedFiles = 'Geen verwijderde dossier gevonden';
eBook.Menu.Client_NoOpenFiles = 'Geen open dossiers gevonden';
eBook.Menu.Client_OpenFiles = 'Open dossiers';
eBook.Menu.Client_StartDate = 'Start boekjaar';
eBook.Menu.Client_Suppliers = 'Leveranciers';
eBook.Menu.File_AccountManagement = 'Rekeningen beheren';
eBook.Menu.File_AccountSchema = 'Beheer & mapping rekeningen';
eBook.Menu.File_AccountsMapping = 'Rekeningen mappen';
eBook.Menu.File_AddExtraFile = 'Voeg Word/Excel bestand toe';
eBook.Menu.File_Documents = 'Documenten';
eBook.Menu.File_ExtraFiles = 'Overige werkpapieren';
eBook.Menu.File_FileTitle = 'Dossier {0}';
eBook.Menu.File_FinalTrialBalance = 'Finale proef- en saldibalans';
eBook.Menu.File_JournalCurrent = 'Journaal huidig boekjaar';
eBook.Menu.File_JournalManual = 'Adjustments - handmatig';
eBook.Menu.File_JournalPrevious = 'Journaal vorig boekjaar';
eBook.Menu.File_Journals = 'Journaals';
eBook.Menu.File_JournalWorksheets = 'Adjustments - automatisch';
eBook.Menu.File_LastImported = 'Laatst geïmporteerd: {0}';
eBook.Menu.File_NoExtraFiles = 'Geen overige werkpapieren';
eBook.Menu.File_PDFFiles = 'PDF Repository';
eBook.Menu.File_Reporting = 'Bundels';
eBook.Menu.File_Worksheets = 'Werkpapieren';
eBook.Menu.File_WorksheetsAL = 'Algemene werkpapieren';
eBook.Menu.File_WorksheetsFI = 'Fiscale werkpapieren';
eBook.Menu.File_WorksheetsFPDF = 'Formulieren';
eBook.Meta.GridPanel_AddItem = 'Item toevoegen';
eBook.Meta.GridPanel_DeleteItem = 'Verwijder item';
eBook.Meta.GridPanel_EditItem = 'Wijzig item';
eBook.Meta.MeetingEditor_Chairman = 'Voorzitter';
eBook.Meta.MeetingEditor_Date = 'Datum';
eBook.Meta.MeetingEditor_Delayed = 'Uitgesteld';
eBook.Meta.MeetingEditor_Secretary = 'Secretaris';
eBook.Meta.MeetingEditor_Teller = 'Stemopnemer';
eBook.Meta.MeetingEditor_Time = 'Tijd';
eBook.Meta.PersonEditor_Female = 'Vrouwelijk';
eBook.Meta.PersonEditor_Gender = 'Geslacht';
eBook.Meta.PersonEditor_Male = 'Mannelijk';
eBook.Meta.PersonEditor_Name = 'Naam';
eBook.Meta.ShareHolderEditor_RepresentedBy = 'Vertegenwoordigd door';
eBook.Meta.ShareHolderEditor_Shares = 'Aantal aandelen';
eBook.Meta.Window_Title = 'Metadata';
eBook.Pdf.UploadWindow_ChoosePDF = 'Selecteer een bestand';
eBook.Pdf.UploadWindow_Processing = 'Het bestand {0} wordt verwerkt';
eBook.Pdf.UploadWindow_ReplacePdf = 'Vervang bestaand bestand';
eBook.Pdf.UploadWindow_Title = 'Bestand toevoegen';
eBook.Pdf.UploadWindow_Uploading = 'Het bestand wordt opgeladen';
eBook.Pdf.UploadWindow_UploadPDF = 'Voeg het bestand toe';
eBook.Pdf.UploadWindow_WrongType = 'Het geselecteerde bestand is geen PDF';
eBook.Pdf.Window_AddPdf = 'Pdf toevoegen';
eBook.Pdf.Window_RefreshList = 'Lijst vernieuwen';
eBook.Pdf.Window_RemovePdf = 'Pdf verwijderen';
eBook.Pdf.Window_ReplacePdf = 'Pdf vervangen';
eBook.Pdf.Window_Title = 'PDF Repository';
eBook.Pdf.Window_ViewPdf = 'Pdf bekijken';
eBook.PMT.TeamGrid_Administrator = 'Administrator';
eBook.PMT.TeamGrid_Department = 'Departement';
eBook.PMT.TeamGrid_eBookRole = 'Rol in eBook';
eBook.PMT.TeamGrid_Editor = 'Editor';
eBook.PMT.TeamGrid_Member = 'Teamlid';
eBook.PMT.TeamGrid_PMTRole = 'Rol uit PMT';
eBook.PMT.TeamGrid_Reviewer = 'Reviewer';
eBook.PMT.TeamWindow_Title = 'Teamleden';
eBook.ProAcc.Window_ImportAccountSchema = 'Importeer het rekeningschema';
eBook.ProAcc.Window_ImportCurrentBalans = 'Importeer de balans van het huidige boekjaar';
eBook.ProAcc.Window_ImportCustomers = 'Importeer klanten';
eBook.ProAcc.Window_ImportDoneMsg = 'Het importeren van de gegevens is afgerond.';
eBook.ProAcc.Window_ImportDoneTitle = 'Importeren voltooid';
eBook.ProAcc.Window_ImportPreviousBalans = 'Importeer de balans van het vorige boekjaar';
eBook.ProAcc.Window_ImportSuppliers = 'Importeer leveranciers';
eBook.ProAcc.Window_StartImport = 'Start import';
eBook.ProAcc.Window_Title = 'Importeer uit ProAcc';
eBook.Recycle.IconContextMenu_Empty = 'Verwijder alle dossiers (prullenbak ledigen)';
eBook.Repository.FileDetailsPanel_LinksTitle = 'Links';
eBook.Repository.FileDetailsPanel_MetaTitle = 'Basisinformatie';
eBook.Repository.FileDetailsPanel_PreviewTitle = 'Toon Pdf ';
eBook.Repository.FileMetaPanel_BOOKYEAR = 'Boekjaar';
eBook.Repository.FileMetaPanel_CALENDERYEAR = 'Kalenderjaar';
eBook.Repository.FileMetaPanel_FileName = 'Naam bestand';
eBook.Repository.FileMetaPanel_FISCALYEAR = 'Aanslagjaar';
eBook.Repository.FileMetaPanel_Location = 'Categorie';
eBook.Repository.FileMetaPanel_MONTHLY = 'Maandelijks';
eBook.Repository.FileMetaPanel_PERIOD = 'Periode';
eBook.Repository.FileMetaPanel_QUARTERLY = 'Per kwartaal';
eBook.Repository.FileMetaPanel_QUARTERMONTH = 'Periode';
eBook.Repository.FileMetaPanel_Status = 'Status';
eBook.Repository.FileMetaPanel_YEAR = 'Jaar (document)';
eBook.Repository.Window_ReplaceMsg = 'Are you sure you want to replace the file "{0}"?';
eBook.Repository.ReplaceFile = 'Replace file';
eBook.Repository.NewFile = 'New file';
eBook.Window_Busy = 'Enkele acties zijn nog niet voltooid. Even geduld a.u.b.';
eBook.Worksheet_UpdateLinkedSheets = 'Wenst u de gelinkte werkpapier bij te werken?';
eBook.Worksheet.FormPanel_ErrorsMsg = 'Dit formulier bevat fouten, raadpleeg de kwaliteitscontrole in het paneel rechts';
eBook.Worksheet.FormPanel_ErrorsTitle = 'Formulier bevat fouten';
eBook.Worksheet.FormPanel_UpdatingItem = 'Gegevens worden gecontroleerd en berekend';
eBook.Worksheet.GridPanel_AddItem = 'Item toevoegen';
eBook.Worksheet.GridPanel_DeleteItem = 'Item verwijderen';
eBook.Worksheet.GridPanel_DeletingItem = 'Item in {0} wordt verwijderd';
eBook.Worksheet.GridPanel_EditItem = 'Item wijzigen';
eBook.Worksheet.Messages_NoMessages = 'Geen meldingen';
eBook.Worksheet.Rules.Window_Title = 'Werkpapieren bijwerken';
eBook.Worksheet.TabPanel_TabCloseMsg = 'De gegevens in dit tabblad werden mogelijks gewijzigd. Wenst u deze wijzigingen te bewaren?';
eBook.Worksheet.TabPanel_TabCloseTitle = 'Wijzigingen bewaren?';
eBook.Worksheet.Window_GeneratingPreview = 'Afdrukvoorbeeld wordt voorbereid';
eBook.Worksheet.Window_HelpInfo = 'Help / Info';
eBook.Worksheet.Window_HelpMsg = 'Help mee deze functionaliteit uit te bouwen. Stuur helpende / informatieve text en/of links naar je lokale eBook Champion';
eBook.Worksheet.Window_HelpTitle = 'Help / Info - in opmaak';
eBook.Worksheet.Window_ImportHistory = 'Importeer historiek (ProAcc)';
eBook.Worksheet.Window_ImportingHistory = 'Bezig met het importeren van de historiek';
eBook.Worksheet.Window_LoadingRules = 'Bezig met het uitvoeren van de berekeningen';
eBook.Worksheet.Window_PrintPreview = 'Afdrukvoorbeeld';
eBook.Worksheet.Window_QualityControl = 'Kwaliteitscontrole';
eBook.Worksheet.Window_Recalc = 'Herbereken werkpapier';
eBook.Xbrl.AttachmentPanel_Add = 'Item(s) toevoegen';
eBook.Xbrl.AttachmentPanel_automated = 'Dit onderdeel wordt automatisch bepaald en, indien benodigd, toegevoegd';
eBook.Xbrl.AttachmentPanel_DefaultInValidMsg = 'Volgende bijlagen zijn aangeduid, maar bevatten geen onderdelen:';
eBook.Xbrl.AttachmentPanel_Delete = 'verwijder';
eBook.Xbrl.AttachmentPanel_FichesTitle = 'Formulieren';
eBook.Xbrl.AttachmentPanel_InfoTitle = 'Informatieve bijlagen';
eBook.Xbrl.BizTaxWindow_Generating = 'Aangifte wordt voorbereid';
eBook.Xbrl.ClientDetails_Address = 'Adres';
eBook.Xbrl.ClientDetails_BankInfo = 'Bank informatie';
eBook.Xbrl.ClientDetails_EnterpriseNr = 'Ondernemingsnummer';
eBook.Xbrl.ClientDetails_Legaltype = 'Rechtsvorm';
eBook.Xbrl.ClientDetails_Name = 'Naam';
eBook.Xbrl.ClientDetails_Title = 'Cliëntgegevens';
eBook.Xbrl.ContactDetails_Address = 'Adres';
eBook.Xbrl.ContactDetails_Contacttype = 'Hoedanigheid';
eBook.Xbrl.ContactDetails_Email = 'E-mail';
eBook.Xbrl.ContactDetails_FirstName = 'Voornaam';
eBook.Xbrl.ContactDetails_Function = 'Functie';
eBook.Xbrl.ContactDetails_Name = 'Naam';
eBook.Xbrl.ContactDetails_Phone = 'Telefoon';
eBook.Xbrl.ContactDetails_Title = 'Contactgegevens';
eBook.Xbrl.Overview_AttachmentsTitle = 'Andere (diverse) bijlagen';
eBook.Xbrl.Overview_AutomatedFormsTitle = 'Fiches dewelke automatisch zijn opgenomen in de indiening (xbrl)';
eBook.Xbrl.Overview_BizTaxCalculation = 'BizTax simulatie';
eBook.Xbrl.Overview_Box = 'Bus';
eBook.Xbrl.Overview_Bundle = 'Deliverable';
eBook.Xbrl.Overview_CalculationTitle = 'Belastingsberekening';
eBook.Xbrl.Overview_ClientTitle = 'Cliëntgegevens van de aangifte';
eBook.Xbrl.Overview_ContactTitle = 'Document informatie, verantwoordelijke voor de indiening:';
eBook.Xbrl.Overview_eBookCalculation = 'eBook simulatie';
eBook.Xbrl.Overview_EnterpriseNr = 'Ondernemingsnummer';
eBook.Xbrl.Overview_Extension = 'extensie';
eBook.Xbrl.Overview_FichesAttachmentsTitle = 'Fiches onder "Andere bijlagen"';
eBook.Xbrl.Overview_GeneratingBundle = 'Deliverable wordt opgemaakt';
eBook.Xbrl.Overview_History = 'Historiek';
eBook.Xbrl.Overview_SendingToBizTax = 'Aangifte wordt opgemaakt en verzonden';
eBook.Xbrl.Overview_SendToBizTax = 'Aangifte indienen';
eBook.Xbrl.Overview_Starting = 'Biztax wizard wordt voorbereid.';
eBook.Xbrl.Overview_Status = 'Status indiening';
eBook.Xbrl.Overview_ValidatedPartner = 'Gevalideerd door partner/director';
eBook.Xbrl.Overview_Version = 'Versie';
eBook.Xbrl.Overview_Wizard = 'Wizard';
eBook.Xbrl.ValidationCheckPanel_Validating = 'Het dossier wordt gevalideerd';
eBook.Xbrl.Wizard_Next = 'Volgende stap';
eBook.Xbrl.Wizard_Previous = 'Vorige stap';
eBook.Accounts.Manage.EditWindow_Title = 'Edit account';
eBook.Accounts.Manage.EditWindow_TitleMappings = 'Mappings';
eBook.Accounts.Manage.EditWindow_TitleTranslations = 'Translations';
eBook.Accounts.Manage.MappingGrid_DeletingMapping = 'Mapping is being deleted for {0} {1}';
eBook.Accounts.Manage.MappingGrid_Mapping = 'Mapping';
eBook.Accounts.Manage.MappingGrid_SavingMapping = 'Mapping is being saved for {0} {1}';
eBook.Accounts.Manage.MappingGrid_WorksheetHeader = 'Workpaper';
eBook.Accounts.Manage.TranslationsGrid_Add = 'Ad line';
eBook.Accounts.Manage.TranslationsGrid_DefaultText = 'New description';
eBook.Accounts.Manage.TranslationsGrid_Delete = 'Delete line';
eBook.Accounts.Manage.TranslationsGrid_Deleting = 'Description for {0} {1} is being deleted';
eBook.Accounts.Manage.TranslationsGrid_Description = 'Description';
eBook.Accounts.Manage.TranslationsGrid_Language = 'Language';
eBook.Accounts.Manage.TranslationsGrid_Saving = 'Description for {0} {1} is being saved';
eBook.Accounts.Manage.Window_AccountDesc = 'Description';
eBook.Accounts.Manage.Window_AccountNr = 'Account number';
eBook.Accounts.Manage.Window_AddAccount = 'New account';
eBook.Accounts.Manage.Window_EditAccount = 'Edit account';
eBook.Accounts.Manage.Window_SearchText = 'Lookup account';
eBook.Accounts.Manage.Window_SearchTip = 'Lookup account';
eBook.Accounts.Manage.Window_SelectAll = 'Select all fields';
eBook.Accounts.Manage.Window_Title = 'Edit accounts';
eBook.Accounts.Mappings.GridPanel_ColumnAccount = 'Account';
eBook.Accounts.Mappings.GridPanel_ColumnBelRes = 'Reserves';
eBook.Accounts.Mappings.GridPanel_ColumnBTW = 'VAT reconciliation';
eBook.Accounts.Mappings.GridPanel_ColumnDBI = 'DRD';
eBook.Accounts.Mappings.GridPanel_ColumnErelonen = 'Professional fees';
eBook.Accounts.Mappings.GridPanel_ColumnRV = 'Interest-bearing advances';
eBook.Accounts.Mappings.GridPanel_ColumnRVI = 'Movable deduction at source/interests';
eBook.Accounts.Mappings.GridPanel_ColumnSalaris = 'Salary reconciliation';
eBook.Accounts.Mappings.GridPanel_ColumnSaldo = 'Balance';
eBook.Accounts.Mappings.GridPanel_ColumnSaldoPrevious = 'Previous period';
eBook.Accounts.Mappings.GridPanel_ColumnVAA = 'Benefits In Kind';
eBook.Accounts.Mappings.GridPanel_ColumnVU = 'Disallowed Expenses';
eBook.Accounts.Mappings.GridPanel_EditAccount = 'Edit account';
eBook.Accounts.Mappings.GridPanel_ExportExcelAdjustments = 'Export adjustments';
eBook.Accounts.Mappings.GridPanel_ExportMappings = 'Mappings';
eBook.Accounts.Mappings.GridPanel_ExportProAccAdjustments = 'Export Adjustments';
eBook.Accounts.Mappings.GridPanel_ExportState = 'Export history ProAcc & eBook';
eBook.Accounts.Mappings.GridPanel_ImportExcel = 'Import';
eBook.Accounts.Mappings.GridPanel_ImportExport = 'Import / Export';
eBook.Accounts.Mappings.GridPanel_ImportProAcc = 'Import from ProAcc';
eBook.Accounts.Mappings.GridPanel_ImportExact = 'Import from Exact';
eBook.Accounts.Mappings.GridPanel_ManageAccounts = 'Manage Accounts';
eBook.Accounts.Mappings.GridPanel_NewAccount = 'New account';
eBook.Accounts.Mappings.GridPanel_PreparingExcel = 'Excel is being prepared';
eBook.Accounts.Mappings.GridPanel_PreparingExcelAdjust = 'Adjustments are being exported';
eBook.Accounts.Mappings.GridPanel_ViewDetail = 'View detail';
eBook.Accounts.Mappings.GridPanel_ViewFilter = 'View / Filter';
eBook.Accounts.Mappings.GridPanel_ViewSaldi0 = 'Show 0,00€ balances ';
eBook.Accounts.Mappings.Window_DeletingMapping = 'Mapping is being deleted for {0} {1}';
eBook.Accounts.Mappings.Window_SavingMapping = 'Mapping is being saved for {0} {1}';
eBook.Accounts.Mappings.Window_SavingMappings = 'Saving mapping';
eBook.Accounts.Mappings.Window_Title = 'Mapping general accounts';
eBook.Accounts.New.Window_AccountDesc = 'Description ({0})';
eBook.Accounts.New.Window_AccountNr = 'Account number';
eBook.Accounts.New.Window_Saving = 'Account {0} {1} is being processed';
eBook.Accounts.New.Window_SavingBusy = 'Account is being processed';
eBook.Accounts.New.Window_Title = 'New account';
eBook.Bundle_Statements = 'Statements';
eBook.Bundle.BundlePanel_Library = 'Library';
eBook.Bundle.BundlePanel_NewChapter = 'New chapter';
eBook.Bundle.NewWindow_Create = 'Create report';
eBook.Bundle.NewWindow_Creating = 'Report is being processed';
eBook.Bundle.NewWindow_IncludePrevious = 'Inclusive reports of previous years';
eBook.Bundle.NewWindow_Language = 'Choice a language';
eBook.Bundle.NewWindow_Layout = 'Choice a layout';
eBook.Bundle.NewWindow_Name = 'Name of the report';
eBook.Bundle.NodeInfoPanel_Detailed = 'Detailed view';
eBook.Bundle.NodeInfoPanel_Footer = 'Footer';
eBook.Bundle.NodeInfoPanel_Header = 'Header';
eBook.Bundle.NodeInfoPanel_InfoTitle = 'Item info: {0}';
eBook.Bundle.NodeInfoPanel_BundleSettingsTitle = 'Deliverable info';
eBook.Bundle.NodeInfoPanel_Language = 'Language';
eBook.Bundle.NodeInfoPanel_PageFrom = 'Page from';
eBook.Bundle.NodeInfoPanel_PageTo = 'Page to';
eBook.Bundle.NodeInfoPanel_ReconfigureChildren = 'Reconfigure all items';
eBook.Bundle.NodeInfoPanel_ReportSettings = 'General report configuration';
eBook.Bundle.NodeInfoPanel_ShowFootNote = 'Show footer';
eBook.Bundle.NodeInfoPanel_ShowInIndex = 'Show index';
eBook.Bundle.NodeInfoPanel_ShowPageNr = 'Show page numbers';
eBook.Bundle.NodeInfoPanel_ShowTitle = 'Show items in title';
eBook.Bundle.NodeInfoPanel_Title = 'Title of report';
eBook.Bundle.NodeInfoPanel_UpdateChildrenMsg = 'For all items (chapters, documents, pdf\'s, work papers, annual accounts,…) in this report are being adopted from this setting. Are you sure that you want to proceed?';
eBook.Bundle.NodeInfoPanel_UpdateChildrenTitle = 'Reconfigure all items?';
eBook.Bundle.ObjectsTree_Title = 'Library';
eBook.Bundle.Window_DeleteItem = 'Delete item';
eBook.Bundle.Window_DeletePdfMsg = 'Are you sure you want to delete the PDF-file "{0}"?';
eBook.Bundle.Window_DeleteReportMsg = 'Are you sure you want to delete the report "{0}"?';
eBook.Bundle.Window_DeleteReportTitle = 'Delete report';
eBook.Bundle.Window_DeletingPdfMsg = 'PDF "{0}" is being deleted';
eBook.Bundle.Window_DeletingReport = 'Report "{0}" is being deleted';
eBook.Bundle.Window_EmptyMenu = 'No reports found';
eBook.Bundle.Window_GeneralActions = 'General actions';
eBook.Bundle.Window_GeneratingReport = 'Report is being generated';
eBook.Bundle.Window_LibraryActions = 'Library actions';
eBook.Bundle.Window_LibraryAddPdf = 'Insert PDF';
eBook.Bundle.Window_LibraryDeletePdf = 'Delete PDF';
eBook.Bundle.Window_LibraryReload = 'Reload library';
eBook.Bundle.Window_LoadingLibrary = 'The library is being reloaded';
eBook.Bundle.Window_LoadingMenu = 'Searching reports';
eBook.Bundle.Window_LoadingReport = 'Report {0} is being loaded';
eBook.Bundle.Window_NewChapter = 'Add chapter';
eBook.Bundle.Window_ReportActions = 'Report actions';
eBook.Bundle.Window_ReportDelete = 'Delete report';
eBook.Bundle.Window_ReportGenerate = 'Generate report';
eBook.Bundle.Window_Bundle_New = 'New report';
eBook.Bundle.Window_Bundle_Open = 'Open report';
eBook.Bundle.Window_Bundle_Save = 'Save report';
eBook.Bundle.Window_Bundle_Actions = 'Report actions';
eBook.Bundle.Window_Bundle_Delete = 'Delete report';
eBook.Bundle.Window_Bundle_Generate = 'Generate report';
eBook.Bundle.Window_Bundle_Title = 'Bundles';
eBook.Bundle.Window_Deliverable_New = 'New deliverable';
eBook.Bundle.Window_Deliverable_Open = 'Open deliverable';
eBook.Bundle.Window_Deliverable_Save = 'Save deliverable';
eBook.Bundle.Window_Deliverable_Actions = 'Deliverable actions';
eBook.Bundle.Window_Deliverable_Delete = 'Delete deliverable';
eBook.Bundle.Window_Deliverable_Generate = 'Generate deliverable';
eBook.Bundle.Window_AnnualAccounts_Title = 'NBB filing';
eBook.Bundle.Window_ReportNew = 'New report';
eBook.Bundle.Window_ReportOpen = 'Open report';
eBook.Bundle.Window_ReportSave = 'Save report';
eBook.Bundle.Window_SavingReport = 'Report "{0}" is being saved';
eBook.Bundle.Window_Title = 'Reports';
eBook.Bundle.Window_Deliverable_Title = 'Deliverables';
eBook.BusinessRelations.FormPanel_Address = 'Address';
eBook.BusinessRelations.FormPanel_City = 'City';
eBook.BusinessRelations.FormPanel_Country = 'Country';
eBook.BusinessRelations.FormPanel_EnterpriseNumber = 'Enterprise number';
eBook.BusinessRelations.FormPanel_FirstName = 'First name';
eBook.BusinessRelations.FormPanel_LastName = 'Last name';
eBook.BusinessRelations.FormPanel_Saving = '"{0}" is being saved';
eBook.BusinessRelations.FormPanel_VatNumber = 'VAT number';
eBook.BusinessRelations.FormPanel_ZipCode = 'Zip code';
eBook.BusinessRelations.Window_Customer = 'Customer';
eBook.BusinessRelations.Window_Delete = 'Delete {0}';
eBook.BusinessRelations.Window_DeleteMsg = 'Are you sure you want to delete {0}?';
eBook.BusinessRelations.Window_DeleteTitle = 'Delete {0}';
eBook.BusinessRelations.Window_Deleting = '{0} is being deleted';
eBook.BusinessRelations.Window_Edit = 'Change {0}';
eBook.BusinessRelations.Window_Import = 'Import';
eBook.BusinessRelations.Window_ImportExcel = 'Import Excel';
eBook.BusinessRelations.Window_Importing = 'Importing';
eBook.BusinessRelations.Window_ImportProAcc = 'Import from ProAcc';
eBook.BusinessRelations.Window_Manage = 'Manage {0}';
eBook.BusinessRelations.Window_New = 'New {0}';
eBook.BusinessRelations.Window_Supplier = 'Supplier';
eBook.Client.PreWorksheetWindow_PreviousEnddate = 'End previous bookyear';
eBook.Client.Window_Active = 'Active';
eBook.Client.Window_Address = 'Address';
eBook.Client.Window_City = 'City';
eBook.Client.Window_Country = 'Country';
eBook.Client.Window_Director = 'Director';
eBook.Client.Window_eBookOverrule = 'Alternative data';
eBook.Client.Window_Email = 'Email';
eBook.Client.Window_EnterpriseNr = 'Enterprise number';
eBook.Client.Window_Fax = 'Fax';
eBook.Client.Window_GFIS = 'GFIS code';
eBook.Client.Window_LegalStruct = 'Entity';
eBook.Client.Window_Loading = 'Loading';
eBook.Client.Window_Name = 'Name';
eBook.Client.Window_Phone = 'Phone';
eBook.Client.Window_Rpr = 'RPR';
eBook.Client.Window_SaveChanges = 'Save changes';
eBook.Client.Window_SaveMsg = 'The data has been changed. Save changes?';
eBook.Client.Window_SaveTitle = 'Save changes?';
eBook.Client.Window_Saving = 'Data is being changed';
eBook.Client.Window_Vat = 'VAT number';
eBook.Client.Window_VatElegible = 'VAT required';
eBook.Client.Window_ZipCode = 'Zip code';
eBook.Closing.View_ClosingAllText = 'Closing all parts';
eBook.Closing.View_ClosingReportsMsg = 'Closing reports';
eBook.Closing.View_ClosingReportsTitle = 'Closing reports';
eBook.Closing.View_ClosingText = 'Close';
eBook.Closing.View_ClosingWorksheets = 'Closing all worksheets';
eBook.Closing.View_ClosingWorksheetsMsg = 'Closing worksheets';
eBook.Closing.View_ClosingWorksheetsTitle = 'Closing worksheets';
eBook.Closing.View_DeleteText = 'Delete';
eBook.Closing.View_FileCloseOk = 'All previous steps are finished, the report can be finalized';
eBook.Closing.View_FileClosePrevious = 'All previous steps have to be finished before you can finalize the report';
eBook.Closing.View_FileIsClosed = 'Report is closed as final';
eBook.Closing.View_FileTitle = 'Close final report';
eBook.Closing.View_FinalClosing = 'Close final';
eBook.Closing.View_InvestigateFile = 'Reviewing file status';
eBook.Closing.View_NumbersInfo = 'In the first step of the closing procedure, the balance will be locked';
eBook.Closing.View_NumbersProAccMsg = 'First close period in ProAcc. Only then can this report be finalized.';
eBook.Closing.View_NumbersProfitLossMsg = 'There is al difference between the profit/loss of the period in ProAcc [{0}] and eBook [{1}]. This report cannot be finalized.';
eBook.Closing.View_NumbersQ1 = 'This report concerns a year-end-closing?';
eBook.Closing.View_NumbersQ2 = 'ProAcc bookkeeping done by E&Y?';
eBook.Closing.View_NumbersQ3 = 'Has the period been closed in ProAcc?';
eBook.Closing.View_NumbersQ4 = 'Add 2 decimals to the profit/loss amount from this period';
eBook.Closing.View_NumbersTitle = 'Close balances';
eBook.Closing.View_OpenText = 'Open';
eBook.Closing.View_ReOpen = 'Unclose';
eBook.Closing.View_ReOpenAllWorksheet = 'Reopening all worksheets';
eBook.Closing.View_ReOpenBundle = 'Reopening report';
eBook.Closing.View_ReOpenFile = 'File is being reopened';
eBook.Closing.View_ReOpenNumber = 'Numbers are being reopend';
eBook.Closing.View_ReportsPreviousUnClosedText = 'Once the numbers are final and all work papers are closed, u can finalize the report.';
eBook.Closing.View_ReportsTitle = 'Finalize report(s)';
eBook.Closing.View_SharedInfo = '!! Attention: This client is a shared client of ACR &amp; BTC';
eBook.Closing.View_TaxValidation = '<b><u>Validation of tax calculation</u></b><br/>Before recalculation:<b>{0}</b><br/>After recalculation:<b>{1}</b>.<br/>Do you whish to proceed with the closing of all worksheets?';
eBook.Closing.View_ViewText = 'View';
eBook.Closing.View_WorksheetsNumbersUnlockedText = 'Once the numbers are final, you can close the work papers.';
eBook.Closing.View_WorksheetsTitle = 'Finalize work papers';
eBook.Closing.View_WorksheetsWithErrors = 'Some worksheets have errors. Closing cancelled.';
eBook.Closing.Window_BundleMsg = 'The message "draft" on the report will be automatically removed once the file (inclusive report) is closed as final.';
eBook.Closing.Window_NumbersMsg = 'Once you finalized de numbers: <ul><li> no update of the numbers can take place (ProAcc/Excel)</li><li> no entries can be made (manual/automatic)</li><li>changes in mapping are still possible</li></ul>';
eBook.Closing.Window_Title = 'Closing procedure';
eBook.Create.Empty.ExcelFileFieldSet_File = 'File';
eBook.Create.Empty.ExcelFileFieldSet_Select = 'Select excel file';
eBook.Create.Empty.ExcelFileFieldSet_Sheets = 'Work papers';
eBook.Create.Empty.ExcelFileFieldSet_Title = 'Excel file';
eBook.Create.Empty.FileDatesFieldSet_Assessmentyear = 'Assessment year';
eBook.Create.Empty.FileDatesFieldSet_CurrentAssesment = 'Current fiscal year';
eBook.Create.Empty.FileDatesFieldSet_Enddate = 'End date';
eBook.Create.Empty.FileDatesFieldSet_EnddateInvalid = 'The end date can\'t be before the start date';
eBook.Create.Empty.FileDatesFieldSet_Startdate = 'Start date';
eBook.Create.Empty.FileInfoFieldSet_FileType = 'File type';
eBook.Create.Empty.FileInfoFieldSet_Language = 'Language file';
eBook.Create.Empty.FileInfoFieldSet_Name = 'File name';
eBook.Create.Empty.FileInfoFieldSet_Title = 'File data';
eBook.Create.Empty.MainPanel_Create = 'Create file';
eBook.Create.Empty.MainPanel_ImportPrevious = 'Import accounts and mappings from previous file';
eBook.Create.Empty.MainPanel_ImportPreviousWorksheet = 'Initialize work paper {0} based on previous file';
eBook.Create.Empty.MainPanel_ImportStandard = 'Completing with standard Enst&Young accounts';
eBook.Create.Empty.MainPanel_Loading = 'File {0} is being loaded';
eBook.Create.Empty.MainPanel_MissingData = 'Not all data is filled in/correct';
eBook.Create.Empty.MainPanel_Undo = 'Undo creation new file';
eBook.Create.Empty.PreviousFileFieldSet_EbookFile = 'Previous eBook file';
eBook.Create.Empty.PreviousFileFieldSet_Enddate = 'End date';
eBook.Create.Empty.PreviousFileFieldSet_Startdate = 'Start date';
eBook.Create.Empty.PreviousFileFieldSet_Title = 'Previous fiscal year';
eBook.Create.ProAcc.FileDatesFieldSet_Bookyear = 'Period';
eBook.Create.ProAcc.FileDatesFieldSet_GetDates = 'Dates are being retrieved';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccAccounts = 'Import accounts from ProAcc';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccEndJournal = 'Import end journal <u>previous</u> period from ProAcc';
eBook.Create.ProAcc.FileDatesFieldSet_ImportProAccStartJournal = 'Import journal <u>current</u> period from ProAcc';
eBook.Create.ProAcc.MainPanel_ImportProAccAccounts = 'Import accounts from ProAcc';
eBook.Create.ProAcc.MainPanel_ImportProAccEndJournal = 'Import end journal previous period from ProAcc';
eBook.Create.ProAcc.MainPanel_ImportProAccStartJournal = 'Import journal current period from ProAcc';
eBook.Create.Window_ActionTitle = 'File is being made';
eBook.Create.Window_Copy = 'Copy file';
eBook.Create.Window_EmptyFile = 'Empty file';
eBook.Create.Window_Excel = 'Import from Excel';
eBook.Create.Window_ImportTypes = 'Select type creation/import';
eBook.Create.Window_ProAcc = 'Import from ProAcc';
eBook.Document.Fields.BlockEditor_Cancel = 'Undo';
eBook.Document.Fields.BlockEditor_Save = 'Save';
eBook.Document.NewDocumentWindow_CreateDoc = 'Create document';
eBook.Document.NewDocumentWindow_DocName = 'Document name';
eBook.Document.NewDocumentWindow_Group = 'Document type';
eBook.Document.NewDocumentWindow_Language = 'Language';
eBook.Document.NewDocumentWindow_LoadingTemplates = 'Template are being loaded';
eBook.Document.NewDocumentWindow_NoTemplateSelected = 'No template selected';
eBook.Document.NewDocumentWindow_Template = 'Template';
eBook.Document.NewDocumentWindow_Title = 'Create new document';
eBook.Document.OpenDocumentWindow_NoDocuments = 'No templates found';
eBook.Document.Window_Loading = 'Document is being loaded';
eBook.Document.Window_LoadingMenu = 'Searching existing documents';
eBook.Document.Window_MenuEmpty = 'No existing documents found';
eBook.Document.Window_NewDocument = 'New document';
eBook.Document.Window_OpenDocument = 'Open document';
eBook.Document.Window_PrintPreview = 'Preview';
eBook.Document.Window_SaveDocument = 'Save document';
eBook.Document.Window_Saving = 'Document {0} is being saved';
eBook.Document.Window_Title = 'Document console';
eBook.Excel.AccountSchemaPanel_AccountDesc = 'Account description';
eBook.Excel.AccountSchemaPanel_AccountNr = 'Account number';
eBook.Excel.AccountSchemaPanel_Language = 'Language';
eBook.Excel.BalansPanel_Saldo = 'Balance';
eBook.Excel.Grid_NoWorksheetMsg = 'Select a worksheet';
eBook.Excel.Grid_NoWorksheetTitle = 'No worksheet';
eBook.Excel.Grid_Preview = 'Preview';
eBook.Excel.MappingPanel_Sheets = 'Excel worksheet';
eBook.Excel.UploadWindow_Invalid = 'Wrong file';
eBook.Excel.UploadWindow_Processing = 'File is being processed';
eBook.Excel.UploadWindow_SelectFile = 'Select excel file';
eBook.Excel.UploadWindow_Title = 'Upload excel file';
eBook.Excel.UploadWindow_Upload = 'Upload file';
eBook.Excel.UploadWindow_Uploading = 'File is being uploaded';
eBook.Excel.Window_AccountSchema = 'Chart of accounts';
eBook.Excel.Window_CurrentBalans = 'Current balance sheet';
eBook.Excel.Window_Import = 'Import';
eBook.Excel.Window_ImportAS = 'Import chart of accounts';
eBook.Excel.Window_ImportData = 'Import data';
eBook.Excel.Window_ImportDoneMsg = 'Importing is completed';
eBook.Excel.Window_ImportDoneTitle = 'Import completed';
eBook.Excel.Window_ImportHB = 'Import balance previous period';
eBook.Excel.Window_Importing = 'Data is being imported';
eBook.Excel.Window_ImportVB = 'Import previous period balance';
eBook.Excel.Window_NoDataMsg = 'Choice the data you want to import';
eBook.Excel.Window_NoDataTitle = 'No data';
eBook.Excel.Window_PreviousBalans = 'Balance previous period';
eBook.Excel.Window_TabFailedMsg = 'Not all selected tabs are correct';
eBook.Excel.Window_TabFailedTitle = 'Incorrect tab(s)';
eBook.Exact.UploadWindow_SelectFile = 'Select file (.txt)';
eBook.ExtraFiles.Window_Processing = 'File {0} is being processed';
eBook.ExtraFiles.Window_SelectExWord = 'Select Excel-/Word file';
eBook.ExtraFiles.Window_Title = 'Other work papers';
eBook.ExtraFiles.Window_Upload = 'Upload file';
eBook.ExtraFiles.Window_Uploading = 'File is being uploaded';
eBook.File.HomeHeader_BizTax = 'BizTax declaration';
eBook.File.HomeHeader_Role = 'Your current role';
eBook.File.HomeHeader_YearEnd = 'Yearend closing';
eBook.Help.ChampionsGrid_Email = 'Email';
eBook.Help.ChampionsGrid_FirstName = 'First name';
eBook.Help.ChampionsGrid_LastName = 'Last name';
eBook.Help.ChampionsGrid_Office = 'Office';
eBook.InterfaceTrans_Client = 'Client';
eBook.InterfaceTrans_ClientAddress = 'Address';
eBook.InterfaceTrans_ClientCity = 'City';
eBook.InterfaceTrans_ClientCountry = 'Country';
eBook.InterfaceTrans_ClientName = 'Name';
eBook.InterfaceTrans_ClientSearch = 'Search and open client';
eBook.InterfaceTrans_ClientSearchText = 'Search client';
eBook.InterfaceTrans_ClientZipCode = 'Zip code';
eBook.InterfaceTrans_EndDate = 'End period';
eBook.InterfaceTrans_File = 'File';
eBook.InterfaceTrans_FileName = 'Name';
eBook.InterfaceTrans_GFISCode = 'GFIS code';
eBook.InterfaceTrans_LastAccessed = 'Last opened';
eBook.InterfaceTrans_LoadingClient = 'Client {0} is being loaded';
eBook.InterfaceTrans_LoadingClients = 'Clients are being loaded';
eBook.InterfaceTrans_LoadingFile = 'File {0} is being loaded';
eBook.InterfaceTrans_LoggingIn = 'Logging on and synchronizing with PMT';
eBook.InterfaceTrans_MenuEbook = 'eBook';
eBook.InterfaceTrans_MenuStatistics = 'Statistics';
eBook.InterfaceTrans_MyLatestFiles = 'My last opened files';
eBook.InterfaceTrans_NoProAccMsg = 'The client you chose does not have a connection with ProAcc';
eBook.InterfaceTrans_NoProAccTitle = 'No ProAcc connection';
eBook.InterfaceTrans_SelectAllFields = 'Select all fields';
eBook.InterfaceTrans_StartDate = 'Start period';
eBook.Journal.Booking.Grid_AccountNr = 'Account';
eBook.Journal.Booking.Grid_Credit = 'Credit';
eBook.Journal.Booking.Grid_Debet = 'Debit';
eBook.Journal.Booking.Grid_DeleteLineTip = 'Delete line';
eBook.Journal.Booking.Panel_Desc = 'Description/Title';
eBook.Journal.Booking.Panel_HideClients = 'Hide for clients';
eBook.Journal.Booking.Window_AddLines = 'Add line';
eBook.Journal.Booking.Window_DebetCredit = 'Debet {0} does not equal credit {1}';
eBook.Journal.Booking.Window_ImportExcel = 'Import from Excel';
eBook.Journal.Booking.Window_Save = 'Save entry';
eBook.Journal.Booking.Window_Saving = 'Entry "{0}" is being saved';
eBook.Journal.Grid_Account = 'Account';
eBook.Journal.Grid_AdjustmentsCredit = 'Adjustments credit';
eBook.Journal.Grid_AdjustmentsDebet = 'Adjustments debet';
eBook.Journal.Grid_Credit = 'Credit';
eBook.Journal.Grid_Debet = 'Debet';
eBook.Journal.Grid_End = 'End balance';
eBook.Journal.Grid_Start = 'Start balance';
eBook.Journal.Account_Adjustments_gs = "Geïmporteerd saldo";
eBook.Journal.Account_Adjustments_t = "Total";
eBook.Journal.Account_Adjustments_s = "Saldo";
eBook.Journal.GroupedGrid_Account = 'Account';
eBook.Journal.GroupedGrid_Booking = 'Entry';
eBook.Journal.GroupedGrid_Credit = 'Credit';
eBook.Journal.GroupedGrid_Debet = 'Debet';
eBook.Journal.GroupedGrid_DeleteBooking = 'Delete entry';
eBook.Journal.GroupedGrid_DeleteBookingMsg = 'Are you sure you want to delete entry {0}?';
eBook.Journal.GroupedGrid_DeleteBookingTitle = 'Delete entry?';
eBook.Journal.GroupedGrid_DeletingBooking = 'Entry "{0}" is being deleted';
eBook.Journal.GroupedGrid_EditBooking = 'Change entry';
eBook.Journal.Window_AddBooking = 'Add an entry';
eBook.Journal.Window_ExcelExporting = 'Data is being collected and the excelfile is being made';
eBook.Journal.Window_ExportExcel = 'Export to Excel';
eBook.Journal.Window_ImportExcel = 'Import from Excel';
eBook.Journal.Window_ImportExport = 'Import / Export';
eBook.Journal.Window_ImportProAcc = 'Import from ProAcc';
eBook.Language_Dutch = 'Dutch';
eBook.Language_English = 'English';
eBook.Language_French = 'French';
eBook.Language_German = 'German';
eBook.RepositoryCategory_Permanent = 'Permanent';
eBook.RepositoryCategory_Period = 'Periodically';
eBook.Menu.Client_ClientData = 'Clientdata';
eBook.Menu.Client_ClosedFiles = 'Closed files';
eBook.Menu.Client_Customers = 'Clients';
eBook.Menu.Client_Deleted = 'Deleted';
eBook.Menu.Client_EndDate = 'End of period';
eBook.Menu.Client_FileName = 'File name';
eBook.Menu.Client_NewFile = 'New file';
eBook.Menu.Client_NoClosedFiles = 'No closed files found';
eBook.Menu.Client_NoDeletedFiles = 'No deleted files found';
eBook.Menu.Client_NoOpenFiles = 'No open files found';
eBook.Menu.Client_OpenFiles = 'Open files';
eBook.Menu.Client_StartDate = 'Start period';
eBook.Menu.Client_Suppliers = 'Suppliers';
eBook.Menu.File_AccountManagement = 'Manage accounts';
eBook.Menu.File_AccountSchema = 'Manage & mapping of accounts';
eBook.Menu.File_AccountsMapping = 'Mapping accounts';
eBook.Menu.File_AddExtraFile = 'Add Word/Excel files';
eBook.Menu.File_Documents = 'Documents';
eBook.Menu.File_ExtraFiles = 'Other workpapers';
eBook.Menu.File_FileTitle = 'File {0}';
eBook.Menu.File_FinalTrialBalance = 'Final trial balance';
eBook.Menu.File_JournalCurrent = 'Journals current period';
eBook.Menu.File_JournalManual = 'Adjustments - manually';
eBook.Menu.File_JournalPrevious = 'Journals previous period';
eBook.Menu.File_Journals = 'Journals';
eBook.Menu.File_JournalWorksheets = 'Adjustments - automatically';
eBook.Menu.File_LastImported = 'Last import: {0}';
eBook.Menu.File_NoExtraFiles = 'No other workpapers';
eBook.Menu.File_PDFFiles = 'PDF repository';
eBook.Menu.File_Reporting = 'Reports';
eBook.Menu.File_Worksheets = 'Workpapers';
eBook.Menu.File_WorksheetsAL = 'General Workpapers';
eBook.Menu.File_WorksheetsFI = 'Fiscal workpapers';
eBook.Menu.File_WorksheetsFPDF = 'Forms';
eBook.Meta.GridPanel_AddItem = 'Add item';
eBook.Meta.GridPanel_DeleteItem = 'Delete item';
eBook.Meta.GridPanel_EditItem = 'Edit item';
eBook.Meta.MeetingEditor_Chairman = 'Chairman';
eBook.Meta.MeetingEditor_Date = 'Date';
eBook.Meta.MeetingEditor_Delayed = 'Delayed';
eBook.Meta.MeetingEditor_Secretary = 'Secretary';
eBook.Meta.MeetingEditor_Teller = 'Teller';
eBook.Meta.MeetingEditor_Time = 'Time';
eBook.Meta.PersonEditor_Female = 'Female';
eBook.Meta.PersonEditor_Gender = 'Gender';
eBook.Meta.PersonEditor_Male = 'Male';
eBook.Meta.PersonEditor_Name = 'Name';
eBook.Meta.ShareHolderEditor_RepresentedBy = 'Represented by';
eBook.Meta.ShareHolderEditor_Shares = 'Number of shares';
eBook.Meta.Window_Title = 'Metadata';
eBook.Pdf.UploadWindow_ChoosePDF = 'Select your file';
eBook.Pdf.UploadWindow_Processing = 'File is being processed';
eBook.Pdf.UploadWindow_ReplacePdf = 'Replace existing PDF';
eBook.Pdf.UploadWindow_Title = 'Add PDF-file';
eBook.Pdf.UploadWindow_Uploading = 'File is being uploaded';
eBook.Pdf.UploadWindow_UploadPDF = 'Add file';
eBook.Pdf.UploadWindow_WrongType = 'Selected file is not a PDF';
eBook.Pdf.Window_AddPdf = 'Add PDF';
eBook.Pdf.Window_RefreshList = 'Renew list';
eBook.Pdf.Window_RemovePdf = 'Delete PDF';
eBook.Pdf.Window_ReplacePdf = 'Replace PDF';
eBook.Pdf.Window_Title = 'PDF repository';
eBook.Pdf.Window_ViewPdf = 'View PDF';
eBook.PMT.TeamGrid_Administrator = 'Administrator';
eBook.PMT.TeamGrid_Department = 'Department';
eBook.PMT.TeamGrid_eBookRole = 'Role in eBook';
eBook.PMT.TeamGrid_Editor = 'Editor';
eBook.PMT.TeamGrid_Member = 'Member';
eBook.PMT.TeamGrid_PMTRole = 'PMT Role';
eBook.PMT.TeamGrid_Reviewer = 'Reviewer';
eBook.PMT.TeamWindow_Title = 'Teammembers';
eBook.ProAcc.Window_ImportAccountSchema = 'Import chart of accounts';
eBook.ProAcc.Window_ImportCurrentBalans = 'Import current period balance';
eBook.ProAcc.Window_ImportCustomers = 'Import clients';
eBook.ProAcc.Window_ImportDoneMsg = 'Importing data is completed';
eBook.ProAcc.Window_ImportDoneTitle = 'Import completed';
eBook.ProAcc.Window_ImportPreviousBalans = 'Import previous period balance';
eBook.ProAcc.Window_ImportSuppliers = 'Import suppliers';
eBook.ProAcc.Window_StartImport = 'Start import';
eBook.ProAcc.Window_Title = 'Import from ProAcc';
eBook.Recycle.IconContextMenu_Empty = 'Delete all files (empty trash can)';
eBook.Repository.FileDetailsPanel_LinksTitle = 'Links';
eBook.Repository.FileDetailsPanel_MetaTitle = 'Basic information';
eBook.Repository.FileDetailsPanel_PreviewTitle = 'Preview pdf';
eBook.Repository.FileMetaPanel_BOOKYEAR = 'Financial Year ';
eBook.Repository.FileMetaPanel_CALENDERYEAR = 'Kalenderjaar';
eBook.Repository.FileMetaPanel_FileName = 'File name';
eBook.Repository.FileMetaPanel_FISCALYEAR = 'Fiscal Year ';
eBook.Repository.FileMetaPanel_Location = 'Category';
eBook.Repository.FileMetaPanel_MONTHLY = 'Monthly';
eBook.Repository.FileMetaPanel_PERIOD = 'Period';
eBook.Repository.FileMetaPanel_QUARTERLY = 'Quarterly';
eBook.Repository.FileMetaPanel_QUARTERMONTH = 'Period';
eBook.Repository.FileMetaPanel_Status = 'Status';
eBook.Repository.FileMetaPanel_YEAR = 'Year (document)';
eBook.Window_Busy = 'Some actions are still running. Please wait.';
eBook.Worksheet_UpdateLinkedSheets = 'Do you want to update all linked worksheets?';
eBook.Worksheet.FormPanel_ErrorsMsg = 'This form contains errors, please consult the qualitycontrole in the panel in the right';
eBook.Worksheet.FormPanel_ErrorsTitle = 'Form contains errors';
eBook.Worksheet.FormPanel_UpdatingItem = 'Data is being controlled and calculated';
eBook.Worksheet.GridPanel_AddItem = 'Add item';
eBook.Worksheet.GridPanel_DeleteItem = 'Delete item';
eBook.Worksheet.GridPanel_DeletingItem = 'Item {0} is being deleted';
eBook.Worksheet.GridPanel_EditItem = 'Change item';
eBook.Worksheet.Messages_NoMessages = 'No messages';
eBook.Worksheet.Rules.Window_Title = 'Update workpapers';
eBook.Worksheet.TabPanel_TabCloseMsg = 'The data is this tab is possibly changed. Do you wish to change your changes?';
eBook.Worksheet.TabPanel_TabCloseTitle = 'Save changes?';
eBook.Worksheet.Window_GeneratingPreview = 'Preview is being prepared';
eBook.Worksheet.Window_HelpInfo = 'Help / Info';
eBook.Worksheet.Window_HelpMsg = 'Assist us in improving the functionality. Send helping/informative text and/or links to you local eBook Champion';
eBook.Worksheet.Window_HelpTitle = 'Help / Info - Layout';
eBook.Worksheet.Window_ImportHistory = 'Import history (ProAcc)';
eBook.Worksheet.Window_ImportingHistory = 'Importing history';
eBook.Worksheet.Window_LoadingRules = 'Calculating';
eBook.Worksheet.Window_PrintPreview = 'Preview';
eBook.Worksheet.Window_QualityControl = 'Qualitycontrole';
eBook.Worksheet.Window_Recalc = 'Recalcule workpaper';
eBook.Cancel = 'Cancel';



if(Ext.View){
  Ext.View.prototype.emptyText = '';
}

if(Ext.grid.GridPanel){
  Ext.grid.GridPanel.prototype.ddText = '{0} selected row(s)';
}

if(Ext.TabPanelItem){
  Ext.TabPanelItem.prototype.closeText = 'close worksheet';
}

if(Ext.LoadMask){
  Ext.LoadMask.prototype.msg = 'Loading...';
}

Date.monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

Date.getShortMonthName = function(month) {
  if (month == 2) {
  return 'mrt';
  }
  return Date.monthNames[month].substring(0, 3);
};

Date.monthNumbers = {
  jan: 0,
  feb: 1,
  mar: 2,
  apr: 3,
  may: 4,
  jun: 5,
  jul: 6,
  aug: 7,
  sep: 8,
  oct: 9,
  nov: 10,
  dec: 11
};

Date.getMonthNumber = function(name) {
  var sname = name.substring(0, 3).toLowerCase();
  if (sname == 'mrt') {
    return 2;
  }
  return Date.monthNumbers[sname];
};

Date.dayNames = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];

Date.getShortDayName = function(day) {
  return Date.dayNames[day].substring(0, 3);
};

if(Ext.MessageBox){
  Ext.MessageBox.buttonText = {
    ok: 'OK',
    cancel: 'Cancel',
    yes: 'Yes',
    no: 'No'
  };
}

if(Ext.util.Format){
  Ext.util.Format.date = function(v, format){
    if (!v) return '';
    if (!(v instanceof Date)) v = new Date(Date.parse(v));
    return v.dateFormat(format || 'j-m-y');
  };
}

if(Ext.DatePicker){
  Ext.apply(Ext.DatePicker.prototype, {
    todayText: 'Today',
    minText: 'Date is before min date',
    maxText: 'Date is later then max date',
    disabledDaysText: '',
    disabledDatesText: '',
    monthNames: Date.monthNames,
    dayNames: Date.dayNames,
    nextText: 'Next month (Ctrl+right)',
    prevText: 'Previous month (Ctrl+left)',
    monthYearText: 'Pick a month(Ctrl+up/down next/previous year)',
    todayTip: '{0} (space bar)',
    format: 'j-m-y',
    okText: '&#160;OK&#160;',
    cancelText: 'Cancel',
    startDay: 1
  });
}

if(Ext.PagingToolbar){
  Ext.apply(Ext.PagingToolbar.prototype, {
    beforePageText: 'Page',
    afterPageText: 'van {0}',
    firstText: 'First page',
    prevText: 'Previous page',
    nextText: 'Next page',
    lastText: 'Last page',
    refreshText: 'Refresh',
    displayMsg: 'Shown {0} - {1} of {2}',
    emptyMsg: 'No data to fetch'
  });
}

if(Ext.form.Field){
  Ext.form.Field.prototype.invalidText = 'Unvalid value';
}

if(Ext.form.TextField){
  Ext.apply(Ext.form.TextField.prototype, {
    minLengthText: 'Minimal length is {0}',
    maxLengthText: 'Maximal length is {0}',
    blankText: 'Mandatory field',
    regexText: '',
    emptyText: null
  });
}

if(Ext.form.NumberField){
  Ext.apply(Ext.form.NumberField.prototype, {
    minText: 'Minimal length is {0}',
    maxText: 'Maximal length is {0}',
    nanText: '{0} is not a valid number'
  });
}

if(Ext.form.DateField){
  Ext.apply(Ext.form.DateField.prototype, {
    disabledDaysText: 'Disabled',
    disabledDatesText: 'Disabled',
    minText: 'Date should be after {0}',
    maxText: 'Date should be before {0}',
    invalidText: '{0} is no valid date - date format is {1}',
    format: 'j-m-y',
    altFormats: 'd/m/Y|d/m/Y|d/m/Y|d/m|d-m|dm|dmy|dmY|d|Y-m-d'
  });
}

if(Ext.form.ComboBox){
  Ext.apply(Ext.form.ComboBox.prototype, {
    loadingText: 'Loading...',
    valueNotFoundText: undefined
  });
}

if(Ext.form.VTypes){
  Ext.apply(Ext.form.VTypes, {
    emailText: 'Field should contain a valid email adress like "gebruiker@domein.nl"',
    urlText: 'Field should contain a valid URL like "http:/'+'/www.domein.nl"',
    alphaText: 'Field can only contain characters and _ ',
    alphanumText: 'Field can only contain characters, numbers and _'
  });
}

if(Ext.form.HtmlEditor){
  Ext.apply(Ext.form.HtmlEditor.prototype, {
  createLinkText: 'Type URL for hyperlink:',
  buttonTips: {
      bold: {
        title: 'Vet (Ctrl+B)',
        text: 'Maak de geselecteerde tekst vet.',
        cls: 'x-html-editor-tip'
      },
      italic: {
        title: 'Cursief (Ctrl+I)',
        text: 'Maak de geselecteerde tekst cursief.',
        cls: 'x-html-editor-tip'
      },
      underline: {
        title: 'Onderstrepen (Ctrl+U)',
        text: 'Onderstreep de geselecteerde tekst.',
        cls: 'x-html-editor-tip'
      },
      increasefontsize: {
        title: 'Tekst vergroten',
        text: 'Vergroot het lettertype.',
        cls: 'x-html-editor-tip'
      },
      decreasefontsize: {
        title: 'Tekst verkleinen',
        text: 'Verklein het lettertype.',
        cls: 'x-html-editor-tip'
      },
      backcolor: {
        title: 'Tekst achtergrondkleur',
        text: 'Verander de achtergrondkleur van de geselecteerde tekst.',
        cls: 'x-html-editor-tip'
      },
      forecolor: {
        title: 'Tekst kleur',
        text: 'Verander de kleur van de geselecteerde tekst.',
        cls: 'x-html-editor-tip'
      },
      justifyleft: {
        title: 'Tekst links uitlijnen',
        text: 'Lijn de tekst links uit.',
        cls: 'x-html-editor-tip'
      },
      justifycenter: {
        title: 'Tekst centreren',
        text: 'Centreer de tekst.',
        cls: 'x-html-editor-tip'
      },
      justifyright: {
        title: 'Tekst rechts uitlijnen',
        text: 'Lijn de tekst rechts uit.',
        cls: 'x-html-editor-tip'
      },
      insertunorderedlist: {
        title: 'Opsommingstekens',
        text: 'Begin een ongenummerde lijst.',
        cls: 'x-html-editor-tip'
      },
      insertorderedlist: {
        title: 'Genummerde lijst',
        text: 'Begin een genummerde lijst.',
        cls: 'x-html-editor-tip'
      },
      createlink: {
        title: 'Hyperlink',
        text: 'Maak van de geselecteerde tekst een hyperlink.',
        cls: 'x-html-editor-tip'
      },
      sourceedit: {
        title: 'Bron aanpassen',
        text: 'Schakel modus over naar bron aanpassen.',
        cls: 'x-html-editor-tip'
      }
    }
  });
}

if(Ext.form.BasicForm){
  Ext.form.BasicForm.prototype.waitTitle = 'Even wachten a.u.b...';
}

if(Ext.grid.GridView){
  Ext.apply(Ext.grid.GridView.prototype, {
    sortAscText: 'Sorteer oplopend',
    sortDescText: 'Sorteer aflopend',
    lockText: 'Kolom vastzetten',
    unlockText: 'Kolom vrijgeven',
    columnsText: 'Kolommen'
  });
}

if(Ext.grid.GroupingView){
  Ext.apply(Ext.grid.GroupingView.prototype, {
  emptyGroupText: '(Geen)',
  groupByText: 'Dit veld groeperen',
  showGroupsText: 'Toon in groepen'
  });
}

if(Ext.grid.PropertyColumnModel){
  Ext.apply(Ext.grid.PropertyColumnModel.prototype, {
    nameText: 'Naam',
    valueText: 'Waarde',
    dateFormat: 'j-m-Y'
  });
}

if(Ext.layout.BorderLayout && Ext.layout.BorderLayout.SplitRegion){
  Ext.apply(Ext.layout.BorderLayout.SplitRegion.prototype, {
    splitTip: 'Sleep om grootte aan te passen.',
    collapsibleSplitTip: 'Sleep om grootte aan te passen. Dubbel klikken om te verbergen.'
  });
}

if(Ext.form.TimeField){
  Ext.apply(Ext.form.TimeField.prototype, {
    minText: 'De tijd in dit veld moet op of na {0} liggen',
    maxText: 'De tijd in dit veld moet op of voor {0} liggen',
    invalidText: '{0} is geen geldig tijdstip',
    format: 'G:i',
    altFormats: 'g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H'
  });
}