// important strongly typed json serialization:
// "__type":"{datacontractName}:#{namespace}"

// Construct eBook Namespace
Ext.ns('eBook'
        , 'eBook.Repository'
        , 'eBook.Home'
        , 'eBook.File'
        , 'eBook.Health'
        , 'eBook.NewFile'
        , 'eBook.Fields'
        , 'eBook.data'
        , 'eBook.Language'
        , 'eBook.grid'
        , 'eBook.Interface'
        , 'eBook.InterfaceTrans'
        , 'eBook.Menu'
        , 'eBook.Service'
        , 'eBook.Recycle'
        , 'eBook.Message'
        , 'eBook.BusinessRelations'
        , 'eBook.Meta'
        , 'eBook.Client'
        , 'eBook.Create'
        , 'eBook.Create.Empty'
        , 'eBook.Create.Excel'
        , 'eBook.Create.ProAcc'
        , 'eBook.Create.Exact'
        , 'eBook.Accounts'
        , 'eBook.Accounts.Mappings'
        , 'eBook.Accounts.New'
        , 'eBook.Accounts.Adjustments'
        , 'eBook.Accounts.Manage'
        , 'eBook.Journal'
        , 'eBook.Journal.Booking'
        , 'eBook.Worksheet'
        , 'eBook.Worksheet.Rules'
        , 'eBook.Worksheets'
        , 'eBook.Worksheets.Fields'
        , 'eBook.Worksheets.Lists'
        , 'eBook.Document'
        , 'eBook.Document.Fields'
        , 'eBook.Bundle'
        , 'eBook.Bundle.Pdf'
        , 'eBook.Excel'
        , 'eBook.Exact'
        , 'eBook.ProAcc'
        , 'eBook.TabPanel'
        , 'eBook.List'
        , 'eBook.Statistics'
        , 'eBook.ExtraFiles'
        , 'eBook.Closing'
        , 'eBook.Pdf'
        , 'eBook.Help'
        , 'eBook.PMT'
        , 'eBook.Xbrl'
        , 'eBook.GTH'
        , 'eBook.BizTax'
        , 'eBook.BizTax.Declaration'
        , 'eBook.Yearend'
        , 'eBook.Reports.EngagementAgreement'
        , 'eBook.Reports.PowerOfAttorney'
        , 'eBook.Reports.FileService'
        , 'eBook.Docstore'
        , 'eBook.Forms.AnnualAccounts'
        );


Date.patterns = {
    ISO8601Long: "Y-m-d H:i:s",
    ISO8601Short: "Y-m-d",
    ShortDate: "n/j/Y",
    LongDate: "l, F d, Y",
    FullDateTime: "l d F Y, H:i:s",
    MonthDay: "F d",
    ShortTime: "g:i A",
    LongTime: "g:i:s A",
    SortableDateTime: "Y-m-d\\TH:i:s",
    UniversalSortableDateTime: "Y-m-d H:i:sO",
    YearMonth: "F, Y"
};




// Set ajax settings
Ext.lib.Ajax.defaultPostHeader = 'application/json';
Ext.Ajax.timeout = 1200000;

Ext.chart.Chart.CHART_URL = 'resources/charts.swf';

// set state provider.
eBook.state = new Ext.state.CookieProvider();
Ext.state.Manager.setProvider(eBook.state);

Ext.layout.FormLayout.prototype.trackLabels = true;

eBook.CurrentClient = null;
eBook.CurrentFile = null;

// add escape function to regex class
if ('function' !== typeof RegExp.escape) {
    RegExp.escape = function(s) {
        if ('string' !== typeof s) {
            return s;
        }
        return s.replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
    }; // eo function escape
}

// Guid generation, to move to serverside (more trustworthy)
eBook.GuidItem = function() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
};

eBook.NewGuid = function() {
    return (eBook.GuidItem() + eBook.GuidItem() + "-" + eBook.GuidItem() + "-" + eBook.GuidItem() + "-" + eBook.GuidItem() + "-" + eBook.GuidItem() + eBook.GuidItem() + eBook.GuidItem());
};

eBook.isGuid = function(val) {
    var reGuid = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/;
    return reGuid.test(val);
};

// disable / enable log

eBook.disableLog = function () {
    console.log('Disabled console logging, eBook.enableLog() to re-enable.');
    eBook.consoleLog = console.log;
    window['console']['log'] = function () { };
};

eBook.enableLog = function () {

    window['console']['log'] = eBook.consoleLog;
    console.log('Enabled console logging, eBook.disableLog() to disable.');
};

eBook.disableLog();

//console = { log: function() { } };

// Empty guid string representation
eBook.EmptyGuid = "00000000-0000-0000-0000-000000000000";

eBook.getMapping = function(map, tpe) {
    if (tpe == 'VALUE' || !Ext.isDefined(tpe)) {
        return new Function('data', 'var am = data.am; for (var i = 0; i < am.length; i++) { if (am[i].wt == "' + map + '" && am[i].mi) return am[i].mi.id; } return "";');
    } else if(tpe=='NAME'){
        return new Function('data', 'var am = data.am; for (var i = 0; i < am.length; i++) { if (am[i].wt == "' + map + '" && am[i].mi) return am[i].mi.n; } return "";');
    } else if(tpe=='LASTYEAR'){
        return new Function('data', 'var am = data.am; for (var i = 0; i < am.length; i++) { if (am[i].wt == "' + map + '") return am[i].mly; } return false;');
    }       
    else return new Function();
};

eBook.GetShared = function(fileid,file) {
    return 'file://'+eBook.Share+'/'+fileid+'/'+file;
};

// Extension of format providers
Ext.apply(Ext.util.Format, {
    intFormat: function(v) {
        var strV = "" + v;
        strV = strV.replace(",", ".");
        v = parseInt(strV);

        if (isNaN(v)) {
            return "";
        }
        return v;
    },
    decimalFormat: function(v, p) {
        if (!Ext.isNumber(p)) {
            p = 2;
        }
        var strV = "" + v;
        strV = strV.replace(",", ".");
        v = parseFloat(strV);

        if (isNaN(v)) {
            return "";
        }
        mp = Math.pow(10, p);
        v = (Math.round((v - 0) * mp)) / mp;
        v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
        v = String(v);
        var ps = v.split('.');
        var whole = ps[0];
        var sub = ps[1] ? ',' + ps[1] : '.00';
        var r = /(\d+)(\d{3})/;
        while (r.test(whole)) {
            whole = whole.replace(r, '$1' + '.' + '$2');
        }
        v = whole + sub;
        //if(v.charAt(0) == '-'){
        //    return '-$' + v.substr(1);
        //}
        //if(isNaN(v)) { return ""; }
        return v;
    },
    euroMoney: function(v) {
       
        var r = Ext.util.Format.decimalFormat(v);
        if (r != '') {
            return Ext.util.Format.decimalFormat(v) + "&euro;";
        } else {
            return '';
        }
    }
        ,
    euroMoneyJournal: function(v) {
        if (v == 0 || v == '0') return '';
        var r = Ext.util.Format.decimalFormat(v);
        if (r != '') {
            return Ext.util.Format.decimalFormat(v) + "&euro;";
        } else {
            return '';
        }
    }
});

Ext.grid.Column.types.date = Ext.grid.DateColumn;

eBook.removeDocStoreUploadWindow = function () {
    var window = Ext.getCmp('docStoreInterfaceWindow');
    window.close();
    eBook.Interface.center.clientMenu.repository.reload();
}

eBook.Splash = {
    textItem: Ext.get("eBook-start-loader-text")
    , maskItem: Ext.get("eBook-mask")
    , splashItem: Ext.get("eBook-startload")
    , curText : ''
    , setText: function(txt) {
        this.textItem.update(txt);
        this.curText=txt;
    }
    , hide: function() {
        this.maskItem.hide();
        this.splashItem.hide();
    }
    , show: function() {
        this.maskItem.show();
        this.splashItem.show();
    }
    , isVisible:function() {
        return this.splashItem.isVisible();
    }
};

eBook.Splash.maskItem.setVisibilityMode(Ext.Element.DISPLAY);
eBook.Splash.splashItem.setVisibilityMode(Ext.Element.DISPLAY);

String.prototype.rightPad = function(padString, length) {
    var str = this.toString();
    while (str.length < length)
        str = str + padString;
    return str;
}

eBook.CachedAjax = {
    FileCacheAjax: new Ext.data.Connection({ autoAbort: false, serializeForm: function(form) { return Ext.lib.Ajax.serializeForm(form); } })
    , Ajax: new Ext.data.Connection({ autoAbort: false, serializeForm: function(form) { return Ext.lib.Ajax.serializeForm(form); } })
    , request: function(o) {
        var orig = {};
        Ext.apply(orig, o);
        o.originalSettings = orig;
        o.success = null;
        o.failure = null;
        o.callback = this.callback;
        o.scope = this;
        this.Ajax.request(o);
    }
    , callback: function(opts, success, resp) {
        if (success) {
            this.relaySuccessResponse(opts, success, resp);
        } else {
            try {
                var o = Ext.decode(resp.responseText);
                if (o.Message) {
                    if (o.Message == "CACHE-MISS") {
                        var splash = null;
                        if (eBook.Splash.isVisible()) {
                            splash = eBook.Splash.curText;
                        }
                        eBook.Splash.setText("Preparing file...");
                        eBook.Splash.show();
                        this.FileCacheAjax.request({
                            url: eBook.Service.file + 'SetFileInCache'
                            , method: 'POST'
                            , params: Ext.encode({ cfdc: { FileId: o.FileId} })
                            , callback: this.CacheCallback
                            , scope: this
                            , originalSettings: opts.originalSettings
                            , splash: splash
                        });
                        return;
                    } else if (o.Message.indexOf('endpoint') > 0) {
                        if (!opts.retryCounter) opts.retryCounter = 0;
                        opts.retryCounter++;
                        if (opts.retryCounter < 6) {
                            eBook.Splash.setText("Service is restarting, please wait...");
                            eBook.Splash.show();
                            opts.originalSettings.retryCounter = opts.retryCounter;
                            this.request.defer(Math.max(2000 * opts.retryCounter, 8000), this, [opts.originalSettings]);
                            return;
                        } else {
                            this.relayFaultResponse(resp, opts);
                        }
                    }
                }
            } catch (e) { }
            this.relayFaultResponse(resp, opts);
        }
    }
    , CacheCallback: function(opts, success, reps) {
        eBook.Splash.hide();
        if (opts.splash != null) {
            eBook.Splash.setText(opts.splash);
            eBook.Splash.show();
        }
        if (success) {
            this.request(opts.originalSettings);
        } else {
            eBook.Interface.showResponseError(reps, 'FILE CACHE FAILED!!');
        }
    }
    , relaySuccessResponse: function(opts, success, resp) {
        eBook.Splash.hide();
        var origOpts = opts.originalSettings;
        if (origOpts.success) {
            origOpts.success.call(origOpts.scope, resp, origOpts);
        }
        if (origOpts.callback) {
            origOpts.callback.call(origOpts.scope, origOpts, true, resp);
        }
    }
    , relayFaultResponse: function(resp, opts) {
        var origOpts = opts.originalSettings;
        if (origOpts.failure) {
            origOpts.failure.call(origOpts.scope, resp, origOpts);
        }
        if (origOpts.callback) {
            origOpts.callback.call(origOpts.scope, origOpts, false, resp);
        }
    }
    , serializeForm: function(form) {
        return Ext.lib.Ajax.serializeForm(form);
    }
};

var mapRules = [];
function mappingCssCache() {
    var sc = document.styleSheets[6];
    try {
        var ssRules = sc.cssRules || sc.rules;
        for (var j = ssRules.length - 1; j >= 0; --j) {
            mapRules[ssRules[j].selectorText.toLowerCase()] = ssRules[j];
        }
    } catch (e) { }
}

eBook.getGuidPath = function(id) {
    var sid = id.replace(/-/g, "");
    path = "";
    for (var i = 0; i < sid.length; i = i + 2) {
        path += sid.substr(i, 2) + "/";
    }
    return path;
};

Ext.enumerables = ['hasOwnProperty', 'valueOf', 'isPrototypeOf', 'propertyIsEnumerable',
                       'toLocaleString', 'toString', 'constructor'];

Ext.clone = function(item,noEnum) {
    var type,
                i,
                j,
                k,
                clone,
                key;

    if (item === null || item === undefined) {
        return item;
    }

    // DOM nodes
    // TODO proxy this to Ext.Element.clone to handle automatic id attribute changing
    // recursively
    if (item.nodeType && item.cloneNode) {
        return item.cloneNode(true);
    }

    type = toString.call(item);

    // Date
    if (type === '[object Date]') {
        return new Date(item.getTime());
    }


    // Array
    if (type === '[object Array]') {
        i = item.length;

        clone = [];

        while (i--) {
            clone[i] = Ext.clone(item[i], noEnum);
        }
    }
    // Object
    else if (type === '[object Object]' && item.constructor === Object) {
        clone = {};

        for (key in item) {
            clone[key] = Ext.clone(item[key], noEnum);
        }

        if (Ext.enumerables && !noEnum) {
            for (j = Ext.enumerables.length; j--; ) {
                k = Ext.enumerables[j];
                clone[k] = item[k];
            }
        }
    }

    return clone || item;
};


// EXT.DESTROY OVERRIDE TO DESTROY ALL OBJECT MEMBERS AS WELL
/*
Ext.destroyComplete = function(arg, forceAll) {
   
        if (arg) {
            if (Ext.isArray(arg)) {
                Ext.destroy.apply(this, arg);
            } else if (typeof arg.destroy == 'function') {
                arg.destroy();
            } else if (arg.dom) {
                arg.remove();
            } else if (Ext.isObject(arg)) {
                var arr = [];
                for (var att in arg) {
                    arr.push(att);
                }
                Ext.destroyMembersArray(arg, arr, forceAll);
            }
        }
};

Ext.destroyMembersArray = function(o, a, forceAll) {
    for (var i = 0, len = a.length; i < len; i++) {
        if (forceAll) {
            Ext.destroyComplete(o[a[i]],forceAll);
        } else {
            Ext.destroy(o[a[i]]);
        }
        delete o[a[i]];
    }
};
*/
