

eBook.BusinessRelations.FormPanel = Ext.extend(Ext.form.FormPanel, {
    performAction: ''
    , initComponent: function() {
        Ext.apply(this, {
            labelAlign: 'top',
            bodyStyle: 'padding:5px 5px 0',
            frame: true,
            border: false,
            autoScroll: true,
            buttons: [{
                //text:'Bewaar gegevens',
                ref: '../saveButton',
                iconCls: 'eBook-window-save-ico',
                scale: 'large',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
            }, {
                //text: 'Annuleer',
                ref: '../cancelButton',
                iconCls: 'eBook-window-cancel-ico',
                scale: 'large',
                iconAlign: 'top',
                handler: this.onCancelClick,
                scope: this
}],
                items: [
                {
                    border: false,
                    layout: 'column'
                    , items: [
                        {
                            columnWidth: .5
                            , border: false
                            , layout: 'form'
                            , items: [
                                {
                                    xtype: 'hidden'
                                    , name: 'Id'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_LastName
                                    , name: 'LastName'
                                    , anchor: '95%'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_FirstName
                                    , name: 'FirstName'
                                    , anchor: '95%'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_VatNumber
                                    , name: 'VatNumber'
                                    , anchor: '95%'
                                }
                                , {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_EnterpriseNumber
                                    , name: 'EnterpriseNumber'
                                    , anchor: '95%'
                                }
                            ]
                        }, {
                            columnWidth: .5
                            , border: false
                            , layout: 'form'
                            , items: [
                                {
                                    xtype: 'hidden'
                                    , name: 'ImportedId'
                                }
                                , {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_Address
                                    , name: 'Address'
                                    , anchor: '95%'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_ZipCode
                                    , name: 'Zip'
                                    , anchor: '95%'
                                }, {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_City
                                    , name: 'City'
                                    , anchor: '95%'
                                }
                                , {
                                    xtype: 'textfield'
                                    , fieldLabel: eBook.BusinessRelations.FormPanel_Country
                                    , name: 'Country'
                                    , anchor: '95%'
                                }
                            ]
                        }
                    ]
                }
            ]
            });
            eBook.BusinessRelations.FormPanel.superclass.initComponent.apply(this, arguments);
        }
    , loadRecord: function(rec) {
        this.getForm().reset();
        this.activeRecord = rec;
        this.getForm().loadRecord(rec);
    }
    , onCancelClick: function() {
        this.activeRecord = null;
        this.refOwner.layout.setActiveItem(0);
    }
    , onSaveClick: function() {
        this.getForm().updateRecord(this.activeRecord);
        if (!this.activeRecord.dirty) return this.onCancelClick();

        this.refOwner.setStatusBusy(String.format(eBook.BusinessRelations.FormPanel_Saving, this.activeRecord.get('LastName')));
        var o = {};
        o[this.refOwner.criteriaParameter] = eBook.Util.ConstructObjectFromRecord(this.activeRecord);
        Ext.Ajax.request({
            url: eBook.Service.businessrelation + this.refOwner.updateAction
            , method: 'POST'
            , params: Ext.encode(o)
            , success: this.onSaveSuccess
            , failure: this.onSaveFailure
            , scope: this
        });

    }
    , onSaveSuccess: function(resp, opts) {
        if (this.performAction == "ADD") {
            var resp = Ext.decode(resp.responseText);
            this.activeRecord.id = resp[this.refOwner.updateAction + 'Result'].Id;
            this.activeRecord.set('Id', this.activeRecord.id);
            this.refOwner.grid.store.add(this.activeRecord);
        }
        this.refOwner.setStatus("Done");
        this.onCancelClick();
    }
    , onSaveFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.title);
        this.refOwner.setStatus("!! Failed saving changes !!");
    }
});