


eBook.BusinessRelations.Window = Ext.extend(eBook.Window, {
    businessRelationType: 'C'
    , selectAction: ''
    , deleteAction: ''
    , updateAction: ''
    , criteriaParameter: ''
    , deleting: []
    , initComponent: function() {
        var action = '';
        var trans = '';
        switch (this.businessRelationType) {
            case 'C':
                action = 'Customer';
                trans = eBook.BusinessRelations.Window_Customer;
                this.criteriaParameter = 'brdc';
                break;
            case 'S':
                action = 'Supplier';
                this.criteriaParameter = 'brdc';
                trans = eBook.BusinessRelations.Window_Supplier;
                break;
            case 'P':
                action = 'Personnel';
                trans = 'Personnel';
                break;
        }
        this.translation = trans;
        this.selectAction = 'Get' + action + 'sRange';
        this.selectRoot = 'Get' + action + 'sRangeResult';
        this.deleteAction = 'DeleteBusinessRelation';
        this.updateAction = 'SaveBusinessRelationDataContract';
        this.languageSelector = action;

        Ext.apply(this, {
            layout: 'card'
            , layoutConfig: { deferredRender: false }
            , iconCls: 'eBook-Window-contacts-ico'
            , activeItem: 0
            , items: [
                new eBook.grid.PagedGridPanel({
                    id: this.id + '-grid'
                    , ref: 'grid'
                    , storeConfig: {
                        serviceUrl: eBook.Service.businessrelation,
                        selectAction: this.selectAction,
                        fields: eBook.data.RecordTypes.BusinessRelation,
                        idField: 'id',
                        criteriaParameter: 'cbidc',
                        baseParams: {
                            Id: eBook.CurrentClient
                        }
                    }
                    , tbar: new Ext.Toolbar({
                    disabled: eBook.Interface.isFileClosed(),
                        items: [{
                            xtype: 'buttongroup',
                            ref: 'editgroup',
                            id: this.id + '-toolbar-editgroup',
                            columns: 3,
                            // height:90,
                            title: String.format(eBook.BusinessRelations.Window_Manage,trans),
                            
                            items: [{
                                ref: '../addButton',
                                text: String.format(eBook.BusinessRelations.Window_New,trans),
                                iconCls: 'eBook-businessrelation-add-ico',
                                scale: 'medium',
                                iconAlign: 'top',
                                handler: this.onAddClick,
                                scope: this
                            }, {
                                ref: '../editButton',
                                text: String.format(eBook.BusinessRelations.Window_Edit,trans),
                                iconCls: 'eBook-businessrelation-edit-ico',
                                scale: 'medium',
                                iconAlign: 'top',
                                disabled: true,
                                handler: this.onEditClick,
                                scope: this
                            }, {
                                ref: '../deleteButton',
                                text: String.format(eBook.BusinessRelations.Window_Delete,trans),
                                iconCls: 'eBook-businessrelation-delete-ico',
                                scale: 'medium',
                                iconAlign: 'top',
                                disabled: true,
                                handler: this.onDeleteClick,
                                scope: this
}]
                            }, {
                                xtype: 'buttongroup',
                                ref: 'createtypes',
                                id: this.id + '-toolbar-createtypes',
                                columns: 4,
                                // height:90,
                                title: eBook.BusinessRelations.Window_Import,
                                items: [{
                                    ref: '../proaccImport',
                                    text: eBook.BusinessRelations.Window_ImportProAcc,
                                    iconCls: 'eBook-icon-importproacc-24 ',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    handler: this.onProAccImportClick,
                                    disabled: true,
                                    scope: this
                                }, {
                                    ref: '../excelImport',
                                    text: eBook.BusinessRelations.Window_ImportExcel,
                                    iconCls: 'eBook-icon-importexcel-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    handler: this.onExcelImportClick,
                                    scope: this, hidden: true
}]
}]
                                })
                    , loadMask: true
                    , pagingConfig: {
                        displayInfo: true,
                        pageSize: 25,
                        prependButtons: true
                    }
                    , sm: new Ext.grid.RowSelectionModel({
                        singleSelect: true
                        , listeners: {
                            'selectionchange': {
                                fn: this.onRowSelect
                                , scope: this
                            }
                        }
                    })
                    , columns: [{
                        header: eBook.BusinessRelations.FormPanel_LastName,
                        width: 300,
                        dataIndex: 'Name',
                        sortable: true,
                        renderer: function(v, m, r, ri, ci, s) {
                            if (('' + r.get('LastName')).length > 0) {
                                if (('' + r.get('FirstName')).length > 0) {
                                    return r.get('LastName') + ', ' + r.get('FirstName');
                                }
                                return r.get('LastName');
                            } else {
                                return r.get('FirstName');
                            }
                        }
                    },
			            {
			                header: eBook.BusinessRelations.FormPanel_Address,
			                width: 160,
			                dataIndex: 'Address',
			                sortable: true
			            },
			            {
			                header: eBook.BusinessRelations.FormPanel_ZipCode,
			                width: 120,
			                dataIndex: 'Zip',
			                sortable: true
			            },
			            {
			                header: eBook.BusinessRelations.FormPanel_City,
			                width: 120,
			                dataIndex: 'City',
			                sortable: true
			            },
			            {
			                header: eBook.BusinessRelations.FormPanel_Country,
			                width: 120,
			                dataIndex: 'Country',
			                sortable: true
			            }
			            ,
			            {
			                header: eBook.BusinessRelations.FormPanel_VatNumber,
			                width: 120,
			                dataIndex: 'VatNumber',
			                sortable: true
			            }
                    ]
                     , plugins: [
                        new Ext.ux.grid.Search({
                            iconCls: 'eBook-search-ico',
                            minChars: 2,
                            minLength: 2,
                            showMenu: false,
                            // buttonGroup: this.id + '-grid-toolbar-view',
                            autoFocus: true,
                            position: 'bottom',
                            mode: 'remote',
                            width: 210,
                            tbPosition: 11,
                            searchText: '',
                            searchTipText: '',
                            selectAllText: ''
                            //  fieldConfig: {colspan:3}
                        })]
                            })
                , new eBook.BusinessRelations.FormPanel({ ref: 'form' })
            ]
        });
        eBook.BusinessRelations.Window.superclass.initComponent.apply(this, arguments);
    }
    , show: function() {
        eBook.BusinessRelations.Window.superclass.show.apply(this, arguments);
        this.grid.store.load({ params: { Start: 0, Limit: this.grid.pagingConfig.pageSize} });
        if (eBook.Interface.currentClient.get('ProAccDatabase')) {
            this.grid.getTopToolbar().proaccImport.enable();
        }
    }
    , onRowSelect: function(sm) {
        if (this.activeRecord == sm.getSelected() || sm.getSelected() == null) {
            this.activeRecord = null;
            this.grid.getTopToolbar().editButton.disable();
            this.grid.getTopToolbar().deleteButton.disable();

        } else {
            this.activeRecord = sm.getSelected();
            this.grid.getTopToolbar().editButton.enable();
            this.grid.getTopToolbar().deleteButton.enable();
        }
        // if (sm.isSelected(idx)) return false; //this.onRowDeselect(sm, idx, r);
        // this.form.getForm().loadRecord(r);
        // this.layout.setActiveItem(1);
        //return true;
    }
    , onAddClick: function() {
        this.form.performAction = "ADD";
        this.form.loadRecord(new this.grid.store.recordType({ Id: eBook.EmptyGuid, ClientId: eBook.CurrentClient, RelationType: this.businessRelationType }));
        this.layout.setActiveItem(1);
    }
    , onEditClick: function() {
        this.form.performAction = "UPDATE";
        this.form.loadRecord(this.grid.getSelectionModel().getSelected());
        this.layout.setActiveItem(1);
    }
    , onDeleteClick: function() {
        var rec = this.grid.getSelectionModel().getSelected();
        var name = '' + rec.get('LastName');
        if (rec.get('FirstName')) {
            if (name && name.length > 0) name += ', ';
            name += rec.get('FirstName');
        }
        Ext.Msg.show({
            animEl: this.grid.getTopToolbar().deleteButton.el // if animation is on
            , buttons: Ext.MessageBox.YESNO
            , closable: false
            , fn: this.onConfirmDelete
            , scope: this
            , icon: Ext.MessageBox.WARNING
            , modal: true
            , msg: String.format(eBook.BusinessRelations.Window_DeleteMsg, name)
            , title: String.format(eBook.BusinessRelations.Window_DeleteTitle, this.translation)
        });


    }
    , onConfirmDelete: function(btn) {
        if (btn == 'yes') this.onPerformDelete();
    }
    , onPerformDelete: function() {
        var rec = this.grid.getSelectionModel().getSelected();
        var id = rec.get('Id');
        var name = '' + rec.get('LastName');
        if (rec.get('FirstName')) {
            if (name && name.length > 0) name += ', ';
            name += rec.get('FirstName');
        }
        this.grid.store.remove(rec);
        var action = {
            url: eBook.Service.businessrelation
            , text: String.format(eBook.BusinessRelations.Window_Deleting,name)
            , params: { cidc: { Id: id} }
            , action: this.deleteAction
            , record: rec
        };
        this.addAction(action);
    }
    , onProAccImportClick: function() {
        this.getEl().mask(eBook.BusinessRelations.Window_Importing, 'x-mask-loading');
        var importAction = this.businessRelationType == 'C' ? 'ImportCustomersProAcc' : 'ImportSuppliersProAcc';
        var pars = {
            cidc: {
                Id: eBook.Interface.currentClient.get('Id')
                , Culture: ''
                , Person: eBook.User.getActivePersonDataContract()
            }
        };
        Ext.Ajax.request({
                url: eBook.Service.businessrelation + importAction
                , method: 'POST'
                , params: Ext.encode(pars)
                , callback: this.onProAccImportSuccess
                , scope: this
                 
        });
    }
    , onProAccImportSuccess: function(opts, s, r) {
        if (s) {
            this.grid.store.load({ params: { Start: 0, Limit: this.grid.pagingConfig.pageSize} });
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }

});