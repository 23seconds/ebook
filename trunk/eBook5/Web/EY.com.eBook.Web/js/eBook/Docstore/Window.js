
eBook.Docstore.Window = Ext.extend(eBook.Window, {
    constructor: function (config) {
        this.config = config;
        Ext.apply(this, config || {}, {
            id: 'docStoreInterfaceWindow',
            modal: true,
            width: 400,
            layout: 'fit',
            title: 'Docstore',
            //html: '<iframe width="100%" height="100%" src="http://defranmceebo01/docstoreinterface/index.html?documentId=' + this.config.documentId + '&culture=' + this.config.culture + '&partnerGpn=' + this.config.partnerGpn + '"></iframe>'
            html: '<iframe width="100%" height="100%" src="' + eBook.DocStorePath + '?documentId=' + this.config.documentId + '&culture=' + this.config.culture + '&partnerGpn=' + this.config.partnerGpn + '&uploaderGpn=' + this.config.uploaderGpn + '"></iframe>'

        });
        eBook.Docstore.Window.superclass.constructor.call(this, config);
    }
});

