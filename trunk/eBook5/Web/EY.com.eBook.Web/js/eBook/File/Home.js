
eBook.File.HomeHeader = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
                , '<div class="boxSides">'
                    , '<div id="eyMenuNav" style="">'
                        , '<div class="eBook-Client">'
                            , '<div class="eBook-menu-text"><tpl for="client">{Name}</tpl></div>'
                            , '<div class="eBook-menu-close"> '
                                , '<div class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div>'
                            , '</div>		'
       	                , '</div>'
       	                , '<div class="eBook-File <tpl if="values.Closed">eBook-File-Closed</tpl>">'
                            , '<div class="eBook-menu-text">{StartDate:date("d/m/Y")} &gt; {EndDate:date("d/m/Y")}</div>'
                            , '<div class="eBook-menu-close"> '
                                , '<div class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div>'
                            , '</div>		'
       	                , '</div>'
       	                , '<div class="eBook-activity"></div>'
                    , '</div>'
                , '</div>	'
                , '<div class="eBook-menu-info-block">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-address">'
                         , '<div class="eBook-menu-info-block-item-content"><tpl for="client">{Address}'
                        , '<br>{ZipCode} {City}</tpl>'
                        , '<tpl if="client.Shared"><div class="eBook-menu-info-shared">GEDEELD DOSSIER ACR/TAX</div></tpl>'
                        , '<tpl if="bni"><div class="eBook-menu-info-shared">BNI</div></tpl>'
                     , '</div></div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-PMT">'
                        , '<div class="eBook-menu-info-block-item-content">' + eBook.File.HomeHeader_Role + ': <br/><b>{roles}</b>'
                        , '<div class="eBook-menu-info-PMT-Team">{teamleden}</div>'
                    , '</div></div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-ProAcc">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>ProAcc</u></b><br>'
                        , '<tpl if="client.ProAccServer==null || client.ProAccServer==\'\'">{NoProAcc}</tpl>'
                        , '<tpl if="client.ProAccServer!=null && client.ProAccServer!=\'\'">'
                            , '<tpl for="client">Database: <b>{ProAccDatabase}</b>'
                            , '</tpl>'//, '<br/>Server: <b>{ProAccServer}</b></tpl>'
                            , '<br/>Last import: <span class="proacc-importdate">{ImportDate:date("d/m/Y H:i:s")}</span>'
                        , '</tpl>'
                    , '</div></div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Services">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>Services</u></b>'
                        , '<br/>' + eBook.File.HomeHeader_YearEnd + ': In progress'
                        , '<br/>' + eBook.File.HomeHeader_BizTax + ': To do'
                    , '</div></div>'
    //                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Health">'
    //                        , '<div class="eBook-menu-info-block-item-content"><b><u>Health</u></b>'
    //                        , '<br/>Errors: 0'
    //                        , '<br/>Warnings: 0'
    //    // , 'Errors: 0'
    //    //, 'Errors: 0'
    //                    , '</div></div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Money">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>Tax calculation</u></b>'
                        , '<div class="eBook-menu-info-tax">€</div>'
                    , '</div></div>'
                 , '</div>'
                , '<div style="visibility: visible;" class="eBook-menu">'
                    , '<div class="eBook-menu-childcontainer">'
                        , '<!--div class="eBook-menu-item" id="eBook-File-Wizard">'
                            , '<div class="eBook-menu-item-icon eBook-wizard-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{jaarrekening}</div>'
                        , '</div-->'
                         , '<div class="eBook-menu-item" id="eBook-File-repository">'
                            , '<div class="eBook-menu-item-icon eBook-repository-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">Repository</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-suppliers">'
                            , '<div class="eBook-menu-item-icon eBook-suppliers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{leveranciers}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-customers">'
                            , '<div class="eBook-menu-item-icon eBook-customers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{klanten}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Settings" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-file-settings-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{settings}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Meta" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-file-meta-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{metadata}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Schema" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-accountmappings-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{schema}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-journal" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-journals-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{adjustments}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-finalTrialBalance" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-final-trialbalance-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{finaltrial}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-leadsheets" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-leadsheets-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{leadsheets}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-documents" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-documents-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{documents}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-ReportGeneration" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-reports-generated">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{reports}</div>'
                        , '</div>'
    //                        , '<div class="eBook-menu-item " id="eBook-File-BizTax" qtip="">'
    //                            , '<div class="eBook-menu-item-icon eBook-biztax-menu-ico">'
    //                                , '<div class="eBook-menu-item-alert-icon"></div>'
    //                            , '</div>'
    //                            , '<div class="eBook-menu-item-title">{biztax}</div>'
    //                        , '</div>'
                    , '</div>'
                , '</div>'
            , '</tpl>', { compiled: true })
    , initComponent: function() {
        Ext.apply(this, {
            html: '<div>open client</div>'
            , border: false
        });
        eBook.File.HomeHeader.superclass.initComponent.apply(this, arguments);
    }
    , setLastImportDate: function(dte) {
        var lid = this.body.child('.proacc-importdate');
        if (lid) {
            lid.update(dte.format('d/m/Y H:i:s'));
        }
    }
    , updatingTax: function() {
        var txe = this.body.child('.eBook-menu-info-tax');
        if (txe) {
            txe.addClass('eBook-loading-ico');
        }
    }
    , setTax: function(tx) {
        var txe = this.body.child('.eBook-menu-info-tax');
        if (txe) {
            txe.removeClass('eBook-loading-ico');
            txe.update(tx == null ? '' : Ext.util.Format.number(tx, '0.000,00/i') + '€');
        }
        // this.mymenu.setTax(tx);
    }
    , updatingWorksheets: function() {
        var ael = this.body.child('.eBook-activity');
        ael.removeClass('ebook-opacity-0');
        ael.addClass('ebook-opacity-1');
        ael.dom.setAttribute('busy', 'true');
        ael.update('Updating all worksheets');
    }
    , updatingWorksheetsDone: function(time) {
        var ael = this.body.child('.eBook-activity');
        ael.dom.setAttribute('busy', 'false');
        var sec = Math.floor(time / 1000);
        var ms = time - (sec * 1000);
        var timing = sec + 's, ' + ms + 'ms)';
        ael.update('All worksheets updated (' + timing);
        ael.addClass('ebook-opacity-0');
    }
    , afterRender: function() {
        eBook.File.HomeHeader.superclass.afterRender.call(this);

        // set events
        this.el.on('mouseover', this.onBodyMouseOver, this);
        this.el.on('mouseout', this.onBodyMouseOut, this);
        this.el.on('click', this.onBodyClick, this);
        //this.el.on('contextmenu', this.onBodyContext, this);
    }
    , onBodyMouseOver: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            Ext.get(mnuItem).addClass('eBook-menu-item-over');
            //   Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '90%');
        } else if (mnuBlock && !mnuBlockContent) {
            Ext.get(mnuBlock).addClass('eBook-menu-item-over');
        }
    }
    , onBodyMouseOut: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            Ext.get(mnuItem).removeClass('eBook-menu-item-over');
            // Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '100%');
        } else if (mnuBlock && !mnuBlockContent) {
            Ext.get(mnuBlock).removeClass('eBook-menu-item-over');
        }
    }
    , onBodyClick: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var close = e.getTarget('.eBook-menu-close');
        var team = e.getTarget('.eBook-menu-info-PMT-Team');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            eBook.Interface.openWindow(mnuItem.id);
        } else if (close) {
            var xclose = Ext.get(close);
            if (xclose.parent().hasClass('eBook-Client')) {
                eBook.Interface.closeClient();
            } else if (xclose.parent().hasClass('eBook-File')) {
                eBook.Interface.closeFile();
            }
        } else if (team) {
            eBook.Interface.openWindow('clientteam');
        } else if (mnuBlock && !mnuBlockContent) {
            mnuBlockContent = Ext.get(mnuBlock).child('.eBook-menu-info-block-item-content');
            mnuBlockContent.toggleClass('x-hide-display');
        }
    }
    , updateMe: function(rec) {
        var dta = {};
        Ext.apply(dta, {
            roles: eBook.User.activeClientRoles
            , jaarrekening: 'Jaarafsluiting'
            , teamleden: eBook.PMT.TeamWindow_Title
            , NoProAcc: 'GEEN PROACC'
            , leveranciers: eBook.Menu.Client_Suppliers
            , klanten: eBook.Menu.Client_Customers
            , nieuwdossier: eBook.Menu.Client_NewFile
            , settings: 'Settings'
            , metadata: 'Metadata'
            , schema: 'Schema'
            , adjustments: 'Adjustments'
            , finaltrial: eBook.Menu.File_FinalTrialBalance
            , leadsheets: 'Leadsheets'
            , documents: eBook.Menu.File_Documents
            , reports: eBook.Menu.File_Reporting
            , biztax: 'BizTax'
            , client: eBook.Interface.currentClient.data
        });
        //  Ext.apply(dta, eBook.Interface.currentClient.data);
        Ext.apply(dta, rec.data);
        this.body.update(this.mainTpl.apply(dta));

    }
});


Ext.reg('filehomeheader', eBook.File.HomeHeader);



 //, style: 'background-color:#FFF;margin-left:60px;margin-right:60px;'
                        //, bodyStyle:'background-color:transparent;margin-left:60px;margin-right:60px;'
eBook.File.Home = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , items: [
                        { xtype: 'panel', region: 'center', layout: 'border', border: false
                            , items: [{ xtype: 'filehomeheader', region: 'north', height: 300, ref: '../mymenu' }
                            //                                    , { xtype: 'panel', region: 'center', html: '<div>todo</div>'
                            //                                        , bodyStyle: 'padding-left:60px;padding-right:60px'
                            //                                        , layout: 'fit'
                            //                                        , border: false
                            //                                    }
                                        , { xtype: 'panel', region: 'center'
                                        , padding: '0 60 0 60'
                                            // , bodyStyle: 'padding-left:60px;padding-right:60px'
                                        , layout: 'border'
                                            //, layoutConfig: {}
                                        , border: false
                                        , items: [{ split: true, ref: '../../worksheets', xtype: 'worksheetstab', region: 'west', width: '50%' }, { xtype: 'health', ref: '../../health', region: 'center'}]
                                        }
                                    ]
                        }
            /* , { xtype: 'repository', title: 'Repository'
            , width: 500, region: 'east', collapsible: true,
            collapsed: true, collapsedCls: 'repositoryCollapsed', id: 'file-repository'
            }*/
                    ]
        });
        eBook.File.Home.superclass.initComponent.apply(this, arguments);
    }
    , redrawWorksheetsView: function() {
        this.worksheets.redrawWorksheetsView();
    }
    , getItemMessageType: function(id) {
        return this.health.getItemMessageType(id);
    }
    , loadRec: function(rec) {
        this.mymenu.updateMe(rec);
        //this.files.loadTabs(rec.get('Id'));
    }
    , setTax: function(tx) {
        this.mymenu.setTax(tx);
    }
    , openWorksheetSh: function(typeId, tab, row, field) {
        this.openWorksheet(this.getWorksheetRecord(typeId), tab, row, field);
    }
    , openWorksheet: function(rec, tab, row, field) {
        var cfg = this.getWorksheetWindowCfg(rec);
        if (cfg == null) {
            var ocfg = {
                rec: rec
                , tab: tab
                , row: row
                , field: field
            }; var ass = eBook.Interface.currentFile.get('AssessmentYear');
            eBook.Splash.setText("Loading worksheets " + ass);
            eBook.Splash.show();
            eBook.LazyLoad.loadOnce.defer(50, this, ["js/Worksheets/" + ass + "-" + eBook.Interface.Culture + "-build.js?" + eBook.SpecificVersion, this.onWorksheetsLoaded, ocfg, this, true]);
        } else {
            var wn = new cfg({ title: rec.get('name') });
            wn.show(tab, row, field);
        }
    }
    , getWorksheetWindowCfg: function(rec) {
        var cfg = eBook.Worksheets['AY' + eBook.Interface.currentFile.get('AssessmentYear')];
        if (cfg == null) return null;
        return cfg[rec.get('ruleApp')].Window;
    }
    , onWorksheetsLoaded: function(ocfg) {
        this.openWorksheet(ocfg.rec, ocfg.tab, ocfg.row, ocfg.field);
        eBook.Splash.hide();
    }
    , getWorksheetRecord: function(typeid) {
        var str = this.worksheets.getStore();
        var idx = str.find('id', typeid);
        if (idx < 0) return null;
        return str.getAt(idx);
    }
    , updateWorksheets: function(force) {
        this.mymenu.updatingWorksheets();
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        if (force) {
            eBook.Splash.setText("Force recalculating all worksheets " + ass);
            eBook.Splash.show();
        }
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + (force ? 'ForceReCalculate' : 'ReCalculate')
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
                , callback: this.worksheetsUpdated
                , started: new Date().getTime()
                , scope: this
                , forced: force
        });
    }
    , worksheetsUpdated: function(opts, success, resp) {
        var end = new Date().getTime();
        this.mymenu.updatingWorksheetsDone((end - opts.started));
        if (opts.forced) {
            eBook.Splash.hide();
        }
        this.updateTaxCalculation();
    }
    , updateTaxCalculation: function() {
        this.mymenu.updatingTax();
        eBook.Interface.reloadHealth();
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + 'GetTaxCalculation'
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
                , callback: this.taxCalculationCallback
                , scope: this
        });
    }
    , taxCalculationCallback: function(opts, success, resp) {
        if (success) {
            try {
                var o = Ext.decode(resp.responseText);
                o = o.GetTaxCalculationResult;
                this.setTax(o);
            } catch (e) {
                this.setTax(null);
            }
        } else { this.setTax(null); }

    }
});

Ext.reg('filehome', eBook.File.Home);