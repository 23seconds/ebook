
// split up

eBook.File.HomeHeaderPanel = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for="."></tpl>')
      , initComponent: function () {
          Ext.apply(this, {
              html: '<div>open client</div>'
            , border: false
          });
          eBook.File.HomeHeaderPanel.superclass.initComponent.apply(this, arguments);
      }
    , setLastImportDate: function (dte) {
        var lid = this.body.child('.proacc-importdate');
        if (lid) {
            lid.update(dte.format('d/m/Y H:i:s'));
        }
    }
    , updatingTax: function () {
        var txe = this.body.child('.eBook-menu-info-tax');
        if (txe) {
            txe.addClass('eBook-loading-ico');
        }
    }
    , setTax: function (tx) {
        var txe = this.body.child('.eBook-menu-info-tax');
        if (txe) {
            txe.removeClass('eBook-loading-ico');
            txe.update(tx == null ? '' : Ext.util.Format.number(tx, '0.000,00/i') + '€ (eBook)');
        }
        // this.mymenu.setTax(tx);
    }

    , setBizTax: function (tx) {
        var txe = this.body.child('.eBook-menu-info-tax-biztax');
        if (txe) {
            txe.removeClass('eBook-loading-ico');
            if (Ext.isString(tx)) {
                txe.update(tx);
            } else {
                txe.update(tx == null ? '' : Ext.util.Format.number(tx, '0.000,00/i') + '€ (BizTax)');
            }
        }
        // this.mymenu.setTax(tx);
    }

    , enableBizTax: function (yesOrNo) {
        var txe = this.body.child('.eBook-menu-info-tax-biztax');
        if (txe) {
            if (yesOrNo) {
                txe.show();
            } else {
                txe.hide();
            }
        }
    }
    , enableTax: function (yesOrNo) {
        var txe = this.body.child('.eBook-menu-info-tax');
        if (txe) {
            if (yesOrNo) {
                txe.show();
            } else {
                txe.hide();
            }
        }
    }
    , updatingWorksheets: function () {
        var ael = this.body.child('.eBook-activity');
        ael.removeClass('ebook-opacity-0');
        ael.addClass('ebook-opacity-1');
        ael.dom.setAttribute('busy', 'true');
        ael.update('Updating all worksheets');
    }
    , updatingWorksheetsDone: function (time) {
        var ael = this.body.child('.eBook-activity');
        ael.dom.setAttribute('busy', 'false');
        var sec = Math.floor(time / 1000);
        var ms = time - (sec * 1000);
        var timing = sec + 's, ' + ms + 'ms)';
        ael.update('All worksheets updated (' + timing);
        ael.addClass('ebook-opacity-0');
    }
    , afterRender: function () {
        eBook.File.HomeHeaderPanel.superclass.afterRender.call(this);

        // set events
        this.el.on('mouseover', this.onBodyMouseOver, this);
        this.el.on('mouseout', this.onBodyMouseOut, this);
        this.el.on('click', this.onBodyClick, this);
        //this.el.on('contextmenu', this.onBodyContext, this);
    }
    , onBodyMouseOver: function (e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            Ext.get(mnuItem).addClass('eBook-menu-item-over');
            //   Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '90%');
        } else if (mnuBlock && !mnuBlockContent) {
            Ext.get(mnuBlock).addClass('eBook-menu-item-over');
        }
    }
    , onBodyMouseOut: function (e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            Ext.get(mnuItem).removeClass('eBook-menu-item-over');
            // Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '100%');
        } else if (mnuBlock && !mnuBlockContent) {
            Ext.get(mnuBlock).removeClass('eBook-menu-item-over');
        }
    }
    , onBodyClick: function (e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var close = e.getTarget('.eBook-menu-close');
        var team = e.getTarget('.eBook-menu-info-PMT-Team');
        var mnuBlock = e.getTarget('.eBook-menu-info-block-item');
        var mnuBlockContent = e.getTarget('.eBook-menu-info-block-item-content');

        if (mnuItem) {
            eBook.Interface.openWindow(mnuItem.id);
        } else if (close) {
            var xclose = Ext.get(close);
            if (xclose.parent().hasClass('eBook-Client')) {
                eBook.Interface.closeClient();
            } else if (xclose.parent().hasClass('eBook-File')) {
                eBook.Interface.closeFile();
            }
        } else if (team) {
            eBook.Interface.openWindow('clientteam');
        } else if (mnuBlock && !mnuBlockContent) {
            mnuBlockContent = Ext.get(mnuBlock).child('.eBook-menu-info-block-item-content');
            mnuBlockContent.toggleClass('x-hide-display');
        }
    }
    , updateMe: function (rec, svcs) {
        var dta = {};
        Ext.apply(dta, svcs);
        Ext.apply(dta, {
            roles: eBook.User.activeClientRoles
            , teamleden: eBook.PMT.TeamWindow_Title
            , NoProAcc: 'GEEN PROACC'
            , leveranciers: eBook.Menu.Client_Suppliers
            , klanten: eBook.Menu.Client_Customers
            , nieuwdossier: eBook.Menu.Client_NewFile
            , settings: 'Settings'
            , metadata: 'Metadata'
            , schema: 'Schema'
            , adjustments: 'Adjustments'
            , finaltrial: eBook.Menu.File_FinalTrialBalance
            , leadsheets: 'Leadsheets'
            , quickprint: 'Quickprint'
            , documents: eBook.Menu.File_Documents
            , deliverables: eBook.Bundle.Window_Deliverable_Title
            , bundles: eBook.Bundle.Window_Bundle_Title
            , annualAccountsTitle: eBook.Bundle.Window_AnnualAccounts_Title
            , closeyearend: 'Pre-close year-end service'
            , closeyearendSoft: 'Definitive close year-end service'
            , closeyearendHard: 'Year-end service closed'
            , biztax: 'BizTax'
            , client: eBook.Interface.currentClient.data
            , yearend: eBook.Interface.hasService('C3B8C8DB-3822-4263-9098-541FAE897D02')
            , yearendStatus:eBook.Interface.hasService("C3B8C8DB-3822-4263-9098-541FAE897D02")?eBook.Interface.serviceYearend.s:null
            , biztaxAuto: eBook.Interface.hasService('BD7C2EAE-8500-4103-8984-0131E73D07FA')
            , biztaxManual: eBook.Interface.hasService('60CA9DF9-C549-4A61-A2AD-3FDB1705CF55')
            , annualAccounts: eBook.Interface.hasService('1ABF0836-7D7B-48E7-9006-F03DB97AC28B')
        });
        //  Ext.apply(dta, eBook.Interface.currentClient.data);
        Ext.apply(dta, rec.data);
        this.body.update(this.mainTpl.apply(dta));

    }
});

eBook.File.HomeHeader = Ext.extend(eBook.File.HomeHeaderPanel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
                , '<div class="boxSides">'
                    , '<div id="eyMenuNav" style="">'
                        , '<div class="eBook-Client">'
                            , '<div class="eBook-menu-text"><tpl for="client">{Name}</tpl></div>'
                            , '<div class="eBook-menu-close"> '
                                , '<div class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div>'
                            , '</div>		'
       	                , '</div>'
       	                , '<div class="eBook-File <tpl if="values.Closed">eBook-File-Closed</tpl>">'
                            , '<div class="eBook-menu-text">{StartDate:date("d/m/Y")} &gt; {EndDate:date("d/m/Y")}</div>'
                            , '<div class="eBook-menu-close"> '
                                , '<div class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div>'
                            , '</div>		'
       	                , '</div>'
       	                , '<div class="eBook-activity"></div>'
                    , '</div>'
                , '</div>	'
                , '<div class="eBook-menu-info-block">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-address">'
                         , '<div class="eBook-menu-info-block-item-content"><tpl for="client">{Address}'
                        , '<br>{ZipCode} {City}</tpl>'
                        , '<tpl if="client.Shared"><div class="eBook-menu-info-shared">GEDEELD DOSSIER ACR/TAX</div></tpl>'
                        , '<tpl if="client.bni"><div class="eBook-menu-info-shared">BNI</div></tpl>'
                     , '</div></div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-PMT">'
                        , '<div class="eBook-menu-info-block-item-content">' + eBook.File.HomeHeader_Role + ': <br/><b>{roles}</b>'
                        , '<div class="eBook-menu-info-PMT-Team">{teamleden}</div>'
                    , '</div></div>'
                    , '<tpl if="ImportType==\'exact\'">'
                        , '<div class="eBook-menu-info-block-item eBook-menu-info-ProAcc">'
                            , '<div class="eBook-menu-info-block-item-content"><b><u>Exact</u></b><br></div>'
                        , '</div>'
                    , '</tpl>'
                    , '<tpl if="ImportType==null || ImportType==\'\'">'
                        , '<div class="eBook-menu-info-block-item eBook-menu-info-ProAcc">'
                            , '<div class="eBook-menu-info-block-item-content"><b><u>ProAcc</u></b><br>'
                            , '<tpl if="client.ProAccServer==null || client.ProAccServer==\'\'">{NoProAcc}</tpl>'
                            , '<tpl if="client.ProAccServer!=null && client.ProAccServer!=\'\'">'
                                , '<tpl for="client">Database: <b>{ProAccDatabase}</b>'
                                , '</tpl>'//, '<br/>Server: <b>{ProAccServer}</b></tpl>'
                                , '<br/>Last import: <span class="proacc-importdate">{ImportDate:date("d/m/Y H:i:s")}</span>'
                            , '</tpl>'
                        , '</div></div>'
                    , '</tpl>'
                    , '<tpl if="Ext.isDefined(eBook.Interface.serviceYearend) && eBook.Interface.serviceYearend!=null">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Services">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>Year-end service</u></b>'
                        , '<tpl for="eBook.Interface.serviceYearend"><br/><tpl if="c==null">In progress</tpl><tpl if="c!=null">Closed: {c:date("d/m/Y")}<br/> {cb} </tpl></tpl>'
                    , '</div></div>'
                    , '</tpl>'
                     , '<tpl if="eBook.Interface.serviceBizTax!=null">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-BizTax">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>BizTax service</u></b>'
                        , '<tpl if="Ext.isDefined(eBook.Interface.serviceYearend) && eBook.Interface.serviceYearend!=null"><tpl if="eBook.Interface.serviceYearend.c==null"><br/> Awaiting closing of year-end service.</tpl><tpl if="eBook.Interface.serviceYearend.c!=null"><br/><tpl for="eBook.Interface.serviceBizTax"><br/><tpl if="c==null">In progress</tpl><tpl if="c!=null">Closed: {c:date("d/m/Y")}<br/> {cb} </tpl></tpl></tpl></tpl>'
                        , '<tpl if="!(Ext.isDefined(eBook.Interface.serviceYearend) && eBook.Interface.serviceYearend!=null)"><tpl for="eBook.Interface.serviceBizTax"><br/><tpl if="c==null">In progress</tpl><tpl if="c!=null">Closed: {c:date("d/m/Y")}<br/> {cb} </tpl></tpl></tpl>'
                        , '<tpl for="eBook.Interface.serviceBizTax"><br/><tpl if="c==null">In progress</tpl><tpl if="c!=null">Closed: {c:date("d/m/Y")}<br/> {cb} </tpl></tpl>'
                    , '</div></div>'
                    ,'</tpl>'
    //                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Health">'
    //                        , '<div class="eBook-menu-info-block-item-content"><b><u>Health</u></b>'
    //                        , '<br/>Errors: 0'
    //                        , '<br/>Warnings: 0'
    //    // , 'Errors: 0'
    //    //, 'Errors: 0'
    //                    , '</div></div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-Money">'
                        , '<div class="eBook-menu-info-block-item-content"><b><u>Tax calculation</u></b>'
                        , '<div class="eBook-menu-info-tax">€</div>'
                        , '<div class="eBook-menu-info-tax-biztax">€</div>'
                    , '</div></div>'
                 , '</div>'

            , '</tpl>',
        {
            compiled: true
        })
   
});


Ext.reg('filehomeheader', eBook.File.HomeHeader);


eBook.File.HomeMenu = Ext.extend(eBook.File.HomeHeaderPanel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
                , '<div style="visibility: visible;" class="eBook-menu">'
                    , '<div class="eBook-menu-childcontainer">'
                        , '<!--div class="eBook-menu-item" id="eBook-File-Wizard">'
                            , '<div class="eBook-menu-item-icon eBook-wizard-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{jaarrekening}</div>'
                        , '</div-->'
                         , '<div class="eBook-menu-item" id="eBook-File-repository">'
                            , '<div class="eBook-menu-item-icon eBook-repository-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">Repository</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-suppliers">'
                            , '<div class="eBook-menu-item-icon eBook-suppliers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{leveranciers}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-customers">'
                            , '<div class="eBook-menu-item-icon eBook-customers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{klanten}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Settings" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-file-settings-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{settings}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Meta" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-file-meta-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{metadata}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Schema" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-accountmappings-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{schema}</div>'
                        , '</div>'
                        //, '<tpl if="ImportType==\'exact\'"><div class="eBook-menu-item eBook-menu-item-disabled" id="eBook-File-journal" qtip=""></tpl>'
                        //, '<tpl if="ImportType==null || ImportType!=\'exact\'"><div class="eBook-menu-item" id="eBook-File-journal" qtip=""></tpl>'
                        , '<div class="eBook-menu-item" id="eBook-File-journal" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-journals-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{adjustments}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-finalTrialBalance" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-final-trialbalance-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{finaltrial}</div>'
                        , '</div>'
//                        , '<div class="eBook-menu-item" id="eBook-File-leadsheets" qtip="">'
//                            , '<div class="eBook-menu-item-icon eBook-leadsheets-menu-ico">'
//                                , '<div class="eBook-menu-item-alert-icon"></div>'
//                            , '</div>'
//                            , '<div class="eBook-menu-item-title">{leadsheets}</div>'
//                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-quickprint" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-quickprint-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{quickprint}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-documents" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-documents-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{documents}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-File-Bundles" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-reports-generated">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{bundles}</div>'
                        , '</div>'
                        , '<tpl if="yearend"><div class="eBook-menu-item" id="eBook-File-ReportGeneration-yearend" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-services-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">Year-end {deliverables}</div>'
                        , '</div></tpl>'
                        , '<tpl if="(yearend || annualAccounts) && [this.userAllowed()] == 1"><div class="eBook-menu-item" id="eBook-File-ReportGeneration-AnnualAccounts" qtip="">'
                            , '<div class="eBook-menu-item-icon eBook-services-ico">'
                            , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{annualAccountsTitle}</div>'
                        , '</div></tpl>'
                        //    , '<div class="eBook-menu-item " id="eBook-File-BizTax" qtip="">'
                        //        , '<div class="eBook-menu-item-icon eBook-biztax-menu-ico">'
                        //            , '<div class="eBook-menu-item-alert-icon"></div>'
                        //        , '</div>'
                        //    , '<div class="eBook-menu-item-title">{biztax}</div>'
                        //, '</div>'
                    , '</div>'
                , '</div>'
                ,
        {
            compiled: true,
            userAllowed: function() {
                var currentUser = eBook.User.personId;
                var allowedUsers = ['29EA3413-4930-4B20-A22F-35B11B4864F7','6BF61892-D9A0-495E-A07B-185DC7B03F54','A2AA22FF-EAA3-42F7-8B10-26423F8F581C','18BC4A5D-A798-4D20-B097-E2F6E1A66E3E','BF723AA4-4974-4DCE-B01D-643764805FB7','AC7B9975-423F-4EB4-86E2-7EFBFA93AF47']; //Kim,Timothy,Theo,Lieve,Mieke,Frédéric
                for(var i = 0;i < allowedUsers.length; i++)
                {
                    if(currentUser.toUpperCase() == allowedUsers[i].toUpperCase())
                    {
                        return 1
                    }
                }
                return 0;
            }
        })
});
Ext.reg('filehomemenu', eBook.File.HomeMenu);

//, style: 'background-color:#FFF;margin-left:60px;margin-right:60px;'
//, bodyStyle:'background-color:transparent;margin-left:60px;margin-right:60px;'
eBook.File.Home = Ext.extend(Ext.Panel, {
    initComponent: function () {
        var fileClosed = false;
        Ext.apply(this, {
            layout: 'vbox', layoutConfig: { align: 'stretch', defaultMargins: { top: 0, bottom: 10, left: 0, right: 0} }

            , items: [
                { xtype: 'filehomeheader', height: 124, ref: 'mymenu' }
                , { xtype: 'icontabpanel', flex: 1,
                    border: false,
                    ref: 'tabs',
                    cls: 'ebook-file-main-tab',
                    //padding: { top: 100, bottom: 10, right: 10, left: 10 },
                    activeTab: 0,
                    //deferredRender: true,
                    //tabStripTitle: 'SERVICES',
                    items: [
                        { xtype: 'panel', ref: '../../yearend', title: 'Jaarrekening &amp; voorbereiding BizTax', tabIconCls: 'eBook-menu-info-Services ',
                            layout: 'vbox', layoutConfig: { align: 'stretch' }
                            , items: [{ xtype: 'filehomemenu', height: 200, ref: '../../myiconmenu' }
                                    , { xtype: 'panel', flex: 1, padding: '0 60 0 60', layout: 'border', border: false
                                            , items: [{ split: true, ref: '../../../worksheets', xtype: 'worksheetstab', region: 'west', width: '50%' }, { xtype: 'health', ref: '../../../health', region: 'center'}]
                                    }]
                        }
                        ,
                    /*{ xtype: 'panel', title: 'BizTax indiening', tabIconCls: 'eBook-biztax-menu-ico '
                    , tbar: [
                    {
                    ref: 'reImportWorksheets',
                    text: 'Re-import/refresh worksheets',
                    iconCls: 'eBook-icon-import-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    },
                    {
                    ref: 'startValidation',
                    text: 'Validate',
                    iconCls: 'eBook-icon-checklist-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    }
                    , {
                    ref: 'clientBundle',
                    text: 'Client Bundle',
                    iconCls: 'eBook-bundle-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    }
                    , {
                    ref: 'reviewBundle',
                    text: 'Internal Review Bundle',
                    iconCls: 'eBook-biztax-bundle-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    }
                    , {
                    ref: 'declaration',
                    text: 'Declare',
                    iconCls: 'eBook-biztax-send-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function() { alert('To implement'); },
                    scope: this,
                    disabled: fileClosed
                    }
                    ]
                    }
                 
                    */
                {xtype: 'biztax-module', items: [], ref: '../../module', enableTabScroll: true, title: eBook.Language.BIZTAXDECLARATION, tabIconCls: 'eBook-biztax-menu-ico ' }

                    ]
                }

                ]
        });
        eBook.File.Home.superclass.initComponent.apply(this, arguments);
    }
    , redrawWorksheetsView: function () {
        this.worksheets.redrawWorksheetsView();
    }
    , getItemMessageType: function (id) {
        return this.health.getItemMessageType(id);
    }
    , checkHealthTab: function (tpeId, name) {
        return this.health.checkHealthTab(tpeId, name);
    }
    , getFieldsByTab: function (tpeId, tab) {
        return this.health.getFieldsByTab(tpeId, tab);
    }
    , loadRec: function (rec) {
        eBook.Interface.serviceYearend = null;
        eBook.Interface.serviceBizTax = null;
        eBook.Interface.serviceAnnualAccounts = null;
        var yearend = false,
            autobiztax = false,
            manbiztax = false,
            annualAccounts = false,
            svcTxts = {};
        eBook.Interface.hasService('BD7C2EAE-8500-4103-8984-0131E73D07FA');
        var svcs = rec.get('Services');
        for (var i = 0; i < svcs.length; i++) {
            switch (svcs[i].sid.toUpperCase()) {
                case 'C3B8C8DB-3822-4263-9098-541FAE897D02':
                    eBook.Interface.serviceYearend = svcs[i];
                    if (eBook.Interface.serviceYearend.c) eBook.Interface.serviceYearend.c = Ext.data.Types.WCFDATE.convert(eBook.Interface.serviceYearend.c);

                    break;
                case 'BD7C2EAE-8500-4103-8984-0131E73D07FA':
                    eBook.Interface.serviceBizTax = svcs[i];
                    if (eBook.Interface.serviceBizTax.c) eBook.Interface.serviceBizTax.c = Ext.data.Types.WCFDATE.convert(eBook.Interface.serviceBizTax.c);
                    eBook.Interface.serviceBizTax.auto = true;
                    autobiztax = true;
                    break;
                case '60CA9DF9-C549-4A61-A2AD-3FDB1705CF55':
                    eBook.Interface.serviceBizTax = svcs[i];
                    if (eBook.Interface.serviceBizTax.c) eBook.Interface.serviceBizTax.c = Ext.data.Types.WCFDATE.convert(eBook.Interface.serviceBizTax.c);
                    eBook.Interface.serviceBizTax.auto = false;
                    break;
                case '1ABF0836-7D7B-48E7-9006-F03DB97AC28B':
                    eBook.Interface.serviceAnnualAccounts = svcs[i];
                    if (eBook.Interface.serviceAnnualAccounts.c) eBook.Interface.serviceAnnualAccounts.c = Ext.data.Types.WCFDATE.convert(eBook.Interface.serviceAnnualAccounts.c);
                    eBook.Interface.serviceAnnualAccounts.auto = false;
                    break;
            }
        }

        var title0 = '';
        if (eBook.Interface.serviceYearend) {
            title0 += eBook.Language.YEAREND;
        }


        //autoBiztax
        if (eBook.Interface.serviceBizTax && eBook.Interface.serviceBizTax.auto) {
            if (title0.length > 0) title0 += ' &amp; ';
            title0 += eBook.Language.BIZTAXPREP;
        }
        this.tabs.get(0).setTitle(title0);
        //manualBiztax
        if (eBook.Interface.serviceBizTax && !eBook.Interface.serviceBizTax.auto) {
            this.tabs.setActiveTab(1);
            this.tabs.hideTabStripItem(0);

        } else {
            this.tabs.setActiveTab(0);
            this.tabs.unhideTabStripItem(0);
        }

        //No Biztax
        if (!eBook.Interface.serviceBizTax) {
            this.tabs.hideTabStripItem(1);
        } else {
            this.tabs.unhideTabStripItem(1);
        }


        this.mymenu.updateMe(rec);
        this.myiconmenu.updateMe(rec);
        this.mymenu.enableBizTax(eBook.Interface.serviceBizTax != null);
        if (eBook.Interface.serviceBizTax != null && !eBook.Interface.serviceBizTax.auto) {
            this.mymenu.setBizTax("open biztax tab");
        }
        this.mymenu.enableTax(eBook.Interface.serviceYearend != null);
        this.redrawWorksheetsView();
        //this.files.loadTabs(rec.get('Id'));
    }
    , setTax: function (tx) {
        this.mymenu.setTax(tx);
    }
    , setBizTax: function (tx) {
        this.mymenu.setBizTax(tx);
    }
    , openWorksheetSh: function (typeId, tab, row, field) {
        this.openWorksheet(this.getWorksheetRecord(typeId), tab, row, field);
    }
    , openWorksheet: function (rec, tab, row, field) {
        var cfg = this.getWorksheetWindowCfg(rec);
        if (cfg == null) {
            var ocfg = {
                rec: rec
                , tab: tab
                , row: row
                , field: field
            }; var ass = eBook.Interface.currentFile.get('AssessmentYear');
            eBook.Splash.setText("Loading worksheets " + ass);
            eBook.Splash.show();
            eBook.LazyLoad.loadOnce.defer(50, this, ["js/Worksheets/" + ass + "-" + eBook.Interface.Culture + ".js?" + eBook.SpecificVersion, this.onWorksheetsLoaded, ocfg, this, true]);
        } else {
            var wn = new cfg({ title: rec.get('name'), wsClosed: rec.closed, id: rec.get('id') });
            wn.show(tab, row, field);
        }
    }
    , getWorksheetWindowCfg: function (rec) {
        var cfg = eBook.Worksheets['AY' + eBook.Interface.currentFile.get('AssessmentYear')];
        if (cfg == null) return null;
            return cfg[rec.get('ruleApp')].Window;
    }
    , onWorksheetsLoaded: function (ocfg) {
        this.openWorksheet(ocfg.rec, ocfg.tab, ocfg.row, ocfg.field);
        eBook.Splash.hide();
    }
    , getWorksheetRecord: function (typeid) {
        var str = this.worksheets.getStore();
        var idx = str.find('id', typeid);
        if (idx < 0) return null;
        return str.getAt(idx);
    }
    , updateWorksheets: function (force) {
        this.mymenu.updatingWorksheets();
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        if (force) {
            eBook.Splash.setText("Force recalculating all worksheets " + ass);
            eBook.Splash.show();
        }
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + (force ? 'ForceReCalculate' : 'ReCalculate')
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
                , callback: this.worksheetsUpdated
                , started: new Date().getTime()
                , scope: this
                , forced: force
        });
    }
    , worksheetsUpdated: function (opts, success, resp) {
        var end = new Date().getTime();
        this.mymenu.updatingWorksheetsDone((end - opts.started));
        if (opts.forced) {
            eBook.Splash.hide();
        }
        this.updateTaxCalculation();
    }
    , updateTaxCalculation: function () {
        this.mymenu.updatingTax();
        eBook.Interface.reloadHealth();
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + 'GetTaxCalculation'
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
                , callback: this.taxCalculationCallback
                , scope: this
        });
    }
    , taxCalculationCallback: function (opts, success, resp) {
        if (success) {
            try {
                var o = Ext.decode(resp.responseText);
                o = o.GetTaxCalculationResult;
                this.setTax(o);
            } catch (e) {
                this.setTax(null);
            }
        } else { this.setTax(null); }

    }
});

Ext.reg('filehome', eBook.File.Home);