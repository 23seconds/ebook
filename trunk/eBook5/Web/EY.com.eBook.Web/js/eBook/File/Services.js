
eBook.File.ServicesPanel = Ext.extend(Ext.form.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'hbox', layoutConfig: { align: 'stretch' },
            defaults: { margins: { top: 5, left: 5, right: 5, bottom: 5} },
            disabled: eBook.Interface.isFileClosed(),
            items: [
                 { xtype: 'fieldset', flex: 1, title: 'Periods', ref: 'periods'
                    , items: [{ xtype: 'fieldset', title: 'Current period', ref: 'curperiod'
                                                , items: [{
                                                    xtype: 'datefield'
                                                                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                                                                , allowBlank: false
                                                                , format: 'd/m/Y'
                                                                , name: 'StartDate'
                                                                , ref: 'startdate'
                                                                , width: 100
                                                                , ctx: 'current'
                                                }, {
                                                    xtype: 'datefield'
                                                                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                                                                , allowBlank: false
                                                                , format: 'd/m/Y'
                                                                , name: 'EndDate'
                                                                , ref: 'enddate'
                                                                , ctx: 'current'
                                                                , width: 100
                                                                , listeners: {
                                                                    'change': {
                                                                        fn: this.onEndDateChanged
                                                                        , scope: this
                                                                    }
                                                                }
                                                }, {
                                                    xtype: 'displayfield'
                                                                , name: 'Assessmentyear'
                                                                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Assessmentyear
                                                                , ref: 'assessmentyear'
}]
                    }, { xtype: 'fieldset', title: 'Previous period', ref: 'prevperiod'
                            , items: [{
                                xtype: 'datefield'
                                    , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                                    , allowBlank: false
                                    , format: 'd/m/Y'
                                    , name: 'StartDate'
                                    , ref: 'startdate'
                                    , ctx: 'previous'
                                    , width: 100
                            }, {
                                xtype: 'datefield'
                                    , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                                    , allowBlank: false
                                    , format: 'd/m/Y'
                                    , name: 'EndDate'
                                    , ref: 'enddate'
                                    , ctx: 'previous'
                                    , width: 100
                                    , listeners: {
                                        'change': {
                                            fn: this.onEndDateChanged
                                            , scope: this
                                        }
                                    }
                            }, {
                                xtype: 'displayfield'
                                    , name: 'Assessmentyear'
                                    , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Assessmentyear
                                    , ref: 'assessmentyear'
}]
}]
                 },
                    { xtype: 'fieldset', flex: 1, title: 'Settings', ref: 'sets'
                            , items: [
                    {
                        xtype: 'languagebox'
                         , ref: 'language'
                         , name: 'language'
                         , allowBlank: false
                         , fieldLabel: 'Language'
                         , width: 200
                         , listWidth: 200
                    }
                        , { xtype: 'checkbox', ref: 'first', name: 'first', boxLabel: 'First bookyear', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallPrevBef', boxLabel: 'Before previous period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallPrev', boxLabel: 'Previous period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallCur', boxLabel: 'Current period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'oneof', boxLabel: 'Current period is one of the first 3 periods', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'taxshelter', boxLabel: 'Tax shelter?', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
}]
                        }
                    ]
        });
        eBook.File.ServicesPanel.superclass.initComponent.apply(this, arguments);
    }
    , onEndDateChanged: function(lfd, n, o) {
        var par = lfd.ownerCt;
        if (Ext.isDate(n)) {
            yr = n.getFullYear();
            if (n.getMonth() == 11 && n.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
            var ay = par.ownerCt.ownerCt['ay' + lfd.ctx];
            this.valid = true;
            if (ay != yr) {
                Ext.Msg.show({
                    title: 'INVALID',
                    msg: 'Changing assessmentyear is not allowed',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
                lfd.markInvalid('Changing assessmentyear is not allowed');
                this.valid = false;
            }
            par.assessmentyear.setValue(yr);
            par.assessmentyear.originalValue = yr;
        } else {
            par.assessmentyear.setValue('');
            par.assessmentyear.originalValue = '';
        }
    }
    , checkChanged: function(chk, checked) {
        if (chk.name == 'first') {
            if (checked) {
                this.sets.smallPrevBef.setValue(false);
                this.sets.smallPrev.setValue(false);
                this.sets.oneof.setValue(true);
                this.sets.smallPrevBef.disable();
                this.sets.smallPrev.disable();
                this.sets.oneof.disable();
                this.periods.prevperiod.startdate.allowBlank = true;
                this.periods.prevperiod.enddate.allowBlank = true;
                this.periods.prevperiod.startdate.setValue(null);
                this.periods.prevperiod.enddate.setValue(null);
                this.periods.prevperiod.assessmentyear.setValue('');
                this.periods.prevperiod.disable();

            } else {

                this.periods.prevperiod.startdate.allowBlank = false;
                this.periods.prevperiod.enddate.allowBlank = false;
                this.periods.prevperiod.enable();
                this.sets.smallPrevBef.enable();
                this.sets.smallPrev.enable();
                this.sets.oneof.enable();
            }
        }
    }
    , loadData: function(robj) {
        this.periods.curperiod.startdate.setValue(Ext.data.Types.WCFDATE.convert(robj.StartDate));
        this.periods.curperiod.enddate.setValue(Ext.data.Types.WCFDATE.convert(robj.EndDate));

        var dte = Ext.data.Types.WCFDATE.convert(robj.EndDate);
        if (dte) {
            this.aycurrent = dte.getFullYear();
            if (dte.getMonth() == 11 && dte.getDate() == 31) this.aycurrent++;
            this.onEndDateChanged(this.periods.curperiod.enddate, this.periods.curperiod.enddate.getValue());
        }
        this.periods.prevperiod.startdate.setValue(Ext.data.Types.WCFDATE.convert(robj.PreviousStartDate));
        this.periods.prevperiod.enddate.setValue(Ext.data.Types.WCFDATE.convert(robj.PreviousEndDate));


        dte = Ext.data.Types.WCFDATE.convert(robj.PreviousEndDate);
        if (dte) {
            this.ayprevious = dte.getFullYear();
            if (dte.getMonth() == 11 && dte.getDate() == 31) this.ayprevious++;

            this.onEndDateChanged(this.periods.prevperiod.enddate, this.periods.prevperiod.enddate.getValue());
        }
        this.sets.language.setValue(robj.Culture);
        this.sets.smallPrevBef.setValue(robj.Settings.bps);
        this.sets.smallPrev.setValue(robj.Settings.ps);
        this.sets.smallCur.setValue(robj.Settings.cs);

        this.sets.oneof.setValue(robj.Settings.oft);
        this.sets.first.setValue(robj.Settings.fby);
        this.checkChanged(this.sets.first, robj.Settings.fby);
        this.sets.taxshelter.setValue(robj.Settings.ts);

        this.resetOriginals();
    }
    , detectChanges: function() {
        return this.getForm().isDirty();

    }
    , resetOriginals: function() {
        this.periods.curperiod.startdate.originalValue = this.periods.curperiod.startdate.getValue();
        this.periods.curperiod.enddate.originalValue = this.periods.curperiod.enddate.getValue();
        this.periods.prevperiod.startdate.originalValue = this.periods.prevperiod.startdate.getValue();
        this.periods.prevperiod.enddate.originalValue = this.periods.prevperiod.enddate.getValue();
        this.sets.language.originalValue = this.sets.language.getValue();
        this.sets.smallPrevBef.originalValue = this.sets.smallPrevBef.getValue();
        this.sets.smallPrev.originalValue = this.sets.smallPrev.getValue();
        this.sets.smallCur.originalValue = this.sets.smallCur.getValue();
        this.sets.oneof.originalValue = this.sets.oneof.getValue();
        this.sets.first.originalValue = this.sets.first.getValue();
        this.sets.taxshelter.originalValue = this.sets.taxshelter.getValue();
    }
    , getData: function(robj) {
        var obj = {
            FileId: eBook.Interface.currentFile.get('Id')
            , Culture: this.sets.language.getValue()
            , StartDate: this.periods.curperiod.startdate.getValue()
            , EndDate: this.periods.curperiod.enddate.getValue()
            , PreviousStartDate: this.periods.prevperiod.startdate.getValue()
            , PreviousEndDate: this.periods.prevperiod.enddate.getValue()
            , Settings: {
                fid: eBook.Interface.currentFile.get('Id')
                , fby: this.sets.first.getValue()
                , oft: this.sets.oneof.getValue()
                , bps: this.sets.smallPrevBef.getValue()
                , ps: this.sets.smallPrev.getValue()
                , cs: this.sets.smallCur.getValue()
                , ts: this.sets.taxshelter.getValue()
            }
        };
        if (!Ext.isDefined(obj.PreviousStartDate) || Ext.isEmpty(obj.PreviousStartDate)) obj.PreviousStartDate = null;
        if (!Ext.isDefined(obj.PreviousEndDate) || Ext.isEmpty(obj.PreviousEndDate)) obj.PreviousEndDate = null;
        return obj;
    }
});
Ext.reg('file-services-panel',eBook.File.ServicesPanel);

eBook.File.ServicesWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            items: [{ xtype: 'panel', html: '!IMPORTANT! Changing these settings will result in a complete recalculation of this file and all content. <span style="color:#FF0;">Review your changes <u>before saving</u>!</span>', height: 24
                        , border: false, region: 'north', bodyStyle: 'background-color:#FE0000;padding:5px;font-weight:bold;color:#FFF;'
            },
                    { xtype: 'file-services-panel', region: 'center', ref: 'servicePanel'}]
            , layout: 'border'
            , bbar: ['->', {
                ref: 'savechanges',
                text: "Save changes",
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                disabled: eBook.Interface.isFileClosed(),
                scope: this
}]
            , title: 'File settings'
            , width: 880
            , height: 410
            , modal: true
            });
            eBook.File.ServicesWindow.superclass.initComponent.apply(this, arguments);
        }
    , show: function() {
        eBook.File.ServicesWindow.superclass.show.call(this);
        this.loadData();
    }
    , loadData: function() {
        this.getEl().mask('Loading data', 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'GetFileSettings'
                , method: 'POST'
                , params: Ext.encode({
                    cidc: {
                        Id: eBook.Interface.currentFile.get('Id')
                    }
                })
                , callback: this.onDataObtained
                , scope: this
        });
    }
    , onDataObtained: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText).GetFileSettingsResult;
            this.servicePanel.loadData(robj);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , detectChanges: function() {
        return this.servicePanel.detectChanges();
    }
    , onBeforeClose: function() {
        if (this.detectChanges()) {
            Ext.Msg.show({
                title: '!! Changes detected !!',
                msg: 'You made changes to the settings. Are you sure you wish to <b>CANCEL ALL CHANGES</b>?',
                buttons: Ext.Msg.YESNO,
                fn: this.onChangesDetected,
                scope: this,
                icon: Ext.MessageBox.QUESTION
            });
            return false;
        }
        return eBook.File.ServicesWindow.superclass.onBeforeClose.call(this);
    }
    , onChangesDetected: function(btnid, txt, opts) {
        if (btnid == 'yes') {
            this.servicePanel.resetOriginals();
            this.close();
        }
    }
    , onSaveClick: function() {
        if (this.servicePanel.valid) {
            this.saveChanges();
        } else {
            Ext.Msg.show({
                title: 'INVALID',
                msg: 'The changes you\'ve made are invalid. Review!',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }
    , saveChanges: function(force) {
        if (!this.detectChanges() && !force) {
            Ext.Msg.show({
                title: 'No changes detected',
                msg: 'No changes to the settings were detected, do you want to force-save changes anyway?',
                buttons: Ext.Msg.YESNO,
                fn: this.onNoChanges,
                scope: this,
                icon: Ext.MessageBox.QUESTION
            });
        } else {
            this.getEl().mask('Saving changes', 'x-mask-loading');
            this.servicePanel.resetOriginals();
            eBook.CachedAjax.request({
                url: eBook.Service.file + 'SaveFileSettings'
                    , method: 'POST'
                    , params: Ext.encode({
                        csfsdc: {
                            Settings: this.servicePanel.getData()
                            , Person: eBook.User.getActivePersonDataContract()
                        }
                    })
                    , callback: this.onSavedChanges
                    , scope: this
            });

        }
    }
    , onNoChanges: function(btnid, txt, opts) {
        if (btnid == 'yes') {
            this.saveChanges(true);
        } else {
            this.close();
        }
    }
    , onSavedChanges: function(opts, success, resp) {
        if (success) {
            eBook.Interface.center.fileMenu.updateWorksheets(true);
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }

    });        