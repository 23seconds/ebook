eBook.File.QuickprintWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        if (eBook.Interface.Culture == 'fr-FR') {
            this.leadsheets = 'Leadsheets';
            this.balans = 'Bilan';
            this.balansV = 'Bilan - comparant';
            this.activa = 'Activa';
            this.passiva = 'Passiva';
            this.resultatenrekening = 'Compte de résultat';
            this.resultaatverwerking = 'Resultaatverwerking';
            this.correctieboeking = 'Operations diverses';
        } else {
            this.leadsheets = 'Leadsheets';
            this.balans = 'Balans';
            this.balansV = 'Balans - vergelijkend';
            this.activa = 'Activa';
            this.passiva = 'Passiva';
            this.resultatenrekening = 'Resultatenrekening';
            this.resultaatverwerking = 'Resultaatverwerking';
            this.correctieboeking = 'Correctieboeking';
        }
        /* TO IMPLEMENT
        var leadsheets,
        balans,
        balansV,
        activa,
        passiva,
        resultatenrekening,
        resultaatverwerking,
        correctieboeking;
        this.leadsheets = null;
        Ext.Ajax.request({
        url: eBook.Service.lists + '/GetList'//eBook.Service.url + action
        , method: 'POST'
        , jsonData: { cldc: { lik: 'Quickprint'} }
        , success: function(resp) {
        console.log(this);
        alert(resp);
        var array = Ext.decode(resp.responseText);
        //                    for (var i = 0; i < array.length; i++) {
        //                        switch (array[i].id) {
        //                            case '16AE9C19-A0F8-45AB-A470-0068843F6218':
        //                                this.leadsheets = array[i].nl;
        //                                break;
        //                            case '94741A7E-8650-4127-9ACA-70DA2A7D83B8':
        //                                balans = array[i].nl;
        //                                break;
        //                            case '710C3D60-8F3D-4FDC-AB0C-85A3CCEC13FE':
        //                                activa = array[i].nl;
        //                                break;
        //                            case 'F015D7E6-51F9-4DED-B6AE-E1646636FE14':
        //                                passiva = array[i].nl;
        //                                break;
        //                            case '31F9CA3C-1BBB-4C78-84C2-5A41DA099026':
        //                                resultatenrekening = array[i].nl;
        //                                break;
        //                            case '7597CEBD-33C1-45C6-A9B3-3A57D6DFC997':
        //                                resultaatverwerking = array[i].nl;
        //                                break;
        //                            case '7725E891-13DE-4E9C-98C1-47FF449271F0':
        //                                correctieboeking = array[i].nl;
        //                                break;
        //                            case 'E28CF7AC-0AB3-4169-83D6-1DE815CF0FE8':
        //                                balansV = array[i].nl;
        //                                break;
        //                        }
        //                    }
        //                    leadsheets,
        //                    balans,
        //                    activa,
        //                    passiva,
        //                    resultatenrekening,
        //                    resultaatverwerking;
        }

                , scope: this
        });
        */
        Ext.apply(this, {
            items: [
            {
                xtype: 'fieldset',
                region: 'center',
                title: 'Options',
                margins: '0 8 8 8',
                items: [{ xtype: 'checkbox', boxLabel: this.leadsheets, tag: 'leadsheets', itemCls: 'eBook-File-Quickprints-checkbox-box' },
                        {
                            xtype: 'fieldset',
                            checkboxToggle: true,
                            collapsed: true,
                            title: this.balans,
                            defaults: {
                                xtype: 'checkbox',
                                itemCls: 'eBook-File-Quickprints-checkbox-fieldset-box'
                            },
                            items: [
                                { boxLabel: this.activa, tag: 'jaarrekening_activa' }
                                , { boxLabel: this.passiva, tag: 'jaarrekening_passiva' }
                                , { boxLabel: this.resultatenrekening, tag: 'jaarrekening_resultatenrekening' }
                                , { boxLabel: this.resultaatverwerking, tag: 'jaarrekening_resultaatverwerking' }
                            ],
                            listeners: {
                                'expand': {
                                    fn: this.autoCheckCheckboxes,
                                    scope: this
                                },
                                'collapse': {
                                    fn: this.autoUncheckCheckboxes,
                                    scope: this
                                }
                            }
                        },
                            {
                                xtype: 'fieldset',
                                checkboxToggle: true,
                                collapsed: true,
                                title: this.balansV,
                                defaults: {
                                    xtype: 'checkbox',
                                    itemCls: 'eBook-File-Quickprints-checkbox-fieldset-box'
                                },
                                items: [
                                { boxLabel: this.activa, tag: 'jaarrekening_activa_v' }
                                , { boxLabel: this.passiva, tag: 'jaarrekening_passiva_v' }
                                , { boxLabel: this.resultatenrekening, tag: 'jaarrekening_resultatenrekening_v' }
                                , { boxLabel: this.resultaatverwerking, tag: 'jaarrekening_resultaatverwerking_v' }
                            ],
                                listeners: {
                                    'expand': {
                                        fn: this.autoCheckCheckboxes,
                                        scope: this
                                    },
                                    'collapse': {
                                        fn: this.autoUncheckCheckboxes,
                                        scope: this
                                    }
                                }

                            },
                        { xtype: 'checkbox', boxLabel: this.correctieboeking, tag: 'correctieboekingen', itemCls: 'eBook-File-Quickprints-checkbox-box' },
                        { xtype: 'combo',
                            mode: 'local',
                            store: new Ext.data.ArrayStore({
                                id: 0,
                                fields: [
                                    'myId',
                                    'displayText'
                                ],
                                data: [['nl-BE', 'Nederlands'], ['fr-FR', 'Frans'], ['en-US', 'Engels']],
                                autoLoad: true
                            }),
                            valueField: 'myId',
                            displayField: 'displayText',
                            fieldLabel: 'Language',
                            itemCls: 'eBook-File-Quickprints-checkbox-combo',
                            allowBlank: false,
                            id: 'quickprintLanguagueCombo',
                            value: 'all',
                            listeners: {
                                'afterrender': {
                                    fn: this.loadUserLanguage,
                                    scope: this
                                },
                                'expand': {
                                    fn: this.loadComboOptions,
                                    scope: this
                                }
                            }
                        }
                    ]
            }
            ]
            , layout: 'border'
            , bbar: ['->', {
                ref: 'savechanges',
                text: "Save changes",
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                //disabled: eBook.Interface.isFileClosed(),
                scope: this
}]
            , title: 'Quickprint'
            , width: 350
            , height: 550
            , modal: true
            });

            eBook.File.QuickprintWindow.superclass.initComponent.apply(this, arguments);
        }
        , loadComboOptions: function(combo) {
            combo.store.loadData([['nl-BE', 'Nederlands'], ['fr-FR', 'Frans'], ['en-US', 'Engels']]);
        }

    , loadUserLanguage: function(combo) {
        combo.setValue(combo.store.getAt('0').get('myId'))
        //Ext.getCmp('quickprintLanguagueCombo').setValue(store.getAt('0').get('displayText'));
        //combo.selectByValue(1, false);
    }

    , autoCheckCheckboxes: function(fieldset) {
        this.handleCheckboxes(fieldset, true);
    }

    , autoUncheckCheckboxes: function(fieldset) {
        this.handleCheckboxes(fieldset, false);
    }

    , handleCheckboxes: function(fieldset, bool) {
        var checkboxes = fieldset.items.items;
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].setValue(bool);
        }
    }
    , show: function() {
        eBook.File.QuickprintWindow.superclass.show.call(this);
        this.loadData();
    }
    , loadData: function() {
        this.getEl().mask('Loading data', 'x-mask-loading');
        this.getEl().unmask();
    }
    , onSaveClick: function(btn) {
        var window = btn.ownerCt.ownerCt;
        var checkedCheckboxes = new Array();
        var components = window.findBy(function(component, window) {
            if (component.isXType('checkbox') && component.checked && component.tag) {
                checkedCheckboxes.push(component.tag);
            }
        });
        var combo = Ext.getCmp('quickprintLanguagueCombo');
        eBook.Splash.setText("Generating quickprint");
        eBook.Splash.show();
        eBook.CachedAjax.request({
            url: eBook.Service.output + 'GenerateQuickprint'
            , method: 'POST'
            , params: Ext.encode({
                cqdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    Culture: combo.getValue(),
                    Options: checkedCheckboxes
                }
            })
            , callback: this.onQuickprintReady
            , scope: this
        });

        //this.saveChanges();

    }
    , onQuickprintReady: function(opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GenerateQuickprintResult);
            //window.open('pickup/' + robj.GenerateLeadSheetsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , saveChanges: function(force) {
        alert('saving changes');

        //            this.getEl().mask('Saving changes', 'x-mask-loading');
        //            this.servicePanel.resetOriginals();
        //            eBook.CachedAjax.request({
        //                url: eBook.Service.file + 'SaveFileSettings'
        //                    , method: 'POST'
        //                    , params: Ext.encode({
        //                        csfsdc: {
        //                            Settings: this.servicePanel.getData()
        //                            , Person: eBook.User.getActivePersonDataContract()
        //                        }
        //                    })
        //                    , callback: this.onSavedChanges
        //                    , scope: this
        //            });


    }
    , onSavedChanges: function(opts, success, resp) {
        if (success) {
            eBook.Interface.center.fileMenu.updateWorksheets(true);
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }

    });