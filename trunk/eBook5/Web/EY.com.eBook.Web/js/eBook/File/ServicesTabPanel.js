
Ext.ns('Ext.ux');

Ext.ux.IconTabPanel = Ext.extend(Ext.Panel, {
    deferredRender: true,
    tabWidth: 120,
    minTabWidth: 30,
    resizeTabs: false,
    enableTabScroll: false,
    scrollIncrement: 0,
    scrollRepeatInterval: 400,
    scrollDuration: 0.35,
    animScroll: true,
    tabPosition: 'left', // left or right
    baseCls: 'ux-icontab-panel',
    autoTabs: false,
    autoTabSelector: 'div.ux-icontab',
    activeTab: undefined,
    tabMargin: 2,
    plain: false,
    wheelIncrement: 20,
    idDelimiter: '__',
    itemCls: 'ux-icontab-item',
    elements: 'body',
    headerAsText: false,
    frame: false,
    hideBorders: true,
    initComponent: function() {
        this.frame = false;
        Ext.apply(this, { defaults: { headerAsText: false, header: false} });
        Ext.ux.IconTabPanel.superclass.initComponent.apply(this, arguments);
        this.addEvents(
            'beforetabchange',
            'tabchange',
            'contextmenu'
        );
        this.setLayout(new Ext.layout.CardLayout(Ext.apply({
            layoutOnCardChange: this.layoutOnTabChange,
            deferredRender: this.deferredRender
        }, this.layoutConfig)));

        if (!this.stack) {
            this.stack = Ext.ux.IconTabPanel.AccessStack();
        }
        this.initItems();
    },
    getLayoutTarget: function() {
        return this.innerBody;
    },

    // private
    getContentTarget: function() {
        return this.innerBody;
    },
    // private
    onRender: function(ct, position) {
        Ext.ux.IconTabPanel.superclass.onRender.call(this, ct, position);


        if (this.enableTabScroll) {
            this.tabStripWrap = this.body.createChild({ cls: 'ux-icontab-tabstrip-wrap', style: 'width:' + this.tabWidth + 'px;' });
            this.tabStripWrapScrollUp = this.tabStripWrap.createChild({ cls: 'ux-icontab-tabstrip-scroll-up' });
            this.tabStripWrapBody = this.tabStripWrap.createChild({ cls: 'ux-icontab-tabstrip-body' });
            this.tabStrip = this.tabStripWrapBody.createChild({ cls: 'ux-icontab-tabstrip', style: 'width:100%;' }); // float left and/or fix width;
            this.tabStripWrapScrollDown = this.tabStripWrap.createChild({ cls: 'ux-icontab-tabstrip-scroll-down' });

            this.upRepeater = new Ext.util.ClickRepeater(this.tabStripWrapScrollUp, {
                interval: this.scrollRepeatInterval,
                handler: this.onScrollUp,
                scope: this
            });
            this.downRepeater = new Ext.util.ClickRepeater(this.tabStripWrapScrollDown, {
                interval: this.scrollRepeatInterval,
                handler: this.onScrollDown,
                scope: this

            });
        } else {
            this.tabStrip = this.body.createChild({ cls: 'ux-icontab-tabstrip', style: 'width:' + this.tabWidth + 'px;' }); // float left and/or fix width;
        }
        this.innerBody = this.body.createChild({ cls: 'ux-icontab-innerbody' }); // float right and/or fix width;

        if (this.tabStripTitle) {
            this.tabStrip.createChild({ cls: 'ux-icontab-tabstrip-title', html: this.tabStripTitle });
        }

        this.body.addClass('ux-icontab-panel-body-' + this.tabPosition);

        if (!this.itemTpl) {
            var tt = new Ext.XTemplate(
                 '<div class="ux-icontab-tabItem {cls}" id="{id}" idx="{idx}">',
                 '<tpl if="values.tabIconCls"><div class="ux-icontab-tabItem-icon {tabIconCls}"></div></tpl>',
                 '<div class="ux-icontab-tabItem-txt"><span class="ux-icontab-strip-text">{text}</span></div>',
                 '</div>'
            );
            tt.disableFormats = true;
            tt.compile();
            Ext.ux.IconTabPanel.prototype.itemTpl = tt;
        }

        this.items.each(this.initTab, this);
    },

    // private
    afterRender: function() {
        Ext.ux.IconTabPanel.superclass.afterRender.call(this);

        if (this.activeTab !== undefined) {
            var item = Ext.isObject(this.activeTab) ? this.activeTab : this.items.get(this.activeTab);
            delete this.activeTab;
            this.setActiveTab(item);
        }
    },

    // private
    initEvents: function() {
        Ext.ux.IconTabPanel.superclass.initEvents.call(this);

        this.mon(this.tabStrip, {
            scope: this,
            mousedown: this.onStripMouseDown,
            contextmenu: this.onStripContextMenu
        });

        if (this.enableTabScroll) {
            this.mon(this.tabStripWrapBody, 'mousewheel', this.onWheel, this);
        }
    },

    // private
    findTargets: function(e) {
        var item = null,
            itemEl = e.getTarget('div.ux-icontab-tabItem ', this.tabStrip);

        if (itemEl) {
            item = this.getComponent(itemEl.id.split(this.idDelimiter)[1]);
            if (item.disabled) {
                return {
                    close: null,
                    item: null,
                    el: null
                };
            }
        }
        return {
            close: e.getTarget('.ux-icontab-strip-close', this.tabStrip),
            item: item,
            el: itemEl
        };
    },

    // private
    onStripMouseDown: function(e) {
        if (e.button !== 0) {
            return;
        }
        e.preventDefault();
        var t = this.findTargets(e);
        if (t.close) {
            if (t.item.fireEvent('beforeclose', t.item) !== false) {
                t.item.fireEvent('close', t.item);
                this.remove(t.item);
            }
            return;
        }
        if (t.item && t.item != this.activeTab) {
            this.setActiveTab(t.item);
        }
    },

    // private
    onStripContextMenu: function(e) {
        e.preventDefault();
        var t = this.findTargets(e);
        if (t.item) {
            this.fireEvent('contextmenu', this, t.item, e);
        }
    },

    /**
    * True to scan the markup in this tab panel for <tt>{@link #autoTabs}</tt> using the
    * <tt>{@link #autoTabSelector}</tt>
    * @param {Boolean} removeExisting True to remove existing tabs
    */
    readTabs: function(removeExisting) {
        if (removeExisting === true) {
            this.items.each(function(item) {
                this.remove(item);
            }, this);
        }
        var tabs = this.el.query(this.autoTabSelector);
        for (var i = 0, len = tabs.length; i < len; i++) {
            var tab = tabs[i],
                title = tab.getAttribute('title');
            if(!this.enableTabTitles) tab.removeAttribute('title');
            this.add({
                title: title,
                contentEl: tab
            });
        }
    },

    // private
    initTab: function(item, index) {
        var childIdx = this.tabStripTitle ? index + 1 : index;
        var before = this.tabStrip.dom.childNodes[childIdx],
            p = this.getTemplateArgs(item);
        p.idx = index;
        var el = before ?
                 this.itemTpl.insertBefore(before, p) :
                 this.itemTpl.append(this.tabStrip, p),
            cls = 'ux-icontab-strip-over',
            tabEl = Ext.get(el);

        tabEl.hover(function() {
            if (!item.disabled) {
                tabEl.addClass(cls);
            }
        }, function() {
            tabEl.removeClass(cls);
        });

        if (item.tabTip) {
            tabEl.qtip = item.tabTip;
        }
        item.tabEl = el;

        // Route *keyboard triggered* click events to the tab strip mouse handler.
        /*
        tabEl.select('a').on('click', function(e) {
        if (!e.getPageX()) {
        this.onStripMouseDown(e);
        }
        }, this, { preventDefault: true });
        */
        item.on({
            scope: this,
            disable: this.onItemDisabled,
            enable: this.onItemEnabled,
            titlechange: this.onItemTitleChanged,
            iconchange: this.onItemIconChanged,
            beforeshow: this.onBeforeShowItem
        });
    },

    getTemplateArgs: function(item) {

        var cls = item.closable ? 'ux-icontab-strip-closable' : '';
        if (item.disabled) {
            cls += ' x-item-disabled';
        }
        if (item.tabIconCls) {
            cls += ' ux-icontab-with-icon';
        }
        if (item.tabCls) {
            cls += ' ' + item.tabCls;
        }
        if (this.itemsCls) {
            cls += ' ' + this.itemsCls;
        }

        return {
            id: this.id + this.idDelimiter + item.getItemId(),
            text: item.title,
            cls: cls,
            iconCls: item.iconCls || '',
            tabIconCls: item.tabIconCls
        };
        return item;
    },

    // private
    onAdd: function(c) {
        Ext.ux.IconTabPanel.superclass.onAdd.call(this, c);
        if (this.rendered) {
            var items = this.items;
            this.initTab(c, items.indexOf(c));
            this.delegateUpdates();
        }
    },

    // private
    onBeforeAdd: function(item) {
        var existing = item.events ? (this.items.containsKey(item.getItemId()) ? item : null) : this.items.get(item);
        if (existing) {
            this.setActiveTab(item);
            return false;
        }
        Ext.ux.IconTabPanel.superclass.onBeforeAdd.apply(this, arguments);
        var es = item.elements;
        item.elements = es ? es.replace(',left', '') : es;
        item.border = (item.border === true);
    },

    // private
    onRemove: function(c) {
        var te = Ext.get(c.tabEl);
        // check if the tabEl exists, it won't if the tab isn't rendered
        if (te) {
            te.select('a').removeAllListeners();
            Ext.destroy(te);
        }
        Ext.ux.IconTabPanel.superclass.onRemove.call(this, c);
        this.stack.remove(c);
        delete c.tabEl;
        c.un('disable', this.onItemDisabled, this);
        c.un('enable', this.onItemEnabled, this);
        c.un('titlechange', this.onItemTitleChanged, this);
        c.un('iconchange', this.onItemIconChanged, this);
        c.un('beforeshow', this.onBeforeShowItem, this);
        if (c == this.activeTab) {
            var next = this.stack.next();
            if (next) {
                this.setActiveTab(next);
            } else if (this.items.getCount() > 0) {
                this.setActiveTab(0);
            } else {
                this.setActiveTab(null);
            }
        }
        if (!this.destroying) {
            this.delegateUpdates();
        }
    },

    // private
    onBeforeShowItem: function(item) {
        if (item != this.activeTab) {
            this.setActiveTab(item);
            return false;
        }
    },

    // private
    onItemDisabled: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).addClass('x-item-disabled');
        }
        this.stack.remove(item);
    },

    // private
    onItemEnabled: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).removeClass('x-item-disabled');
        }
    },

    // private
    onItemTitleChanged: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).child('span.ux-icontab-strip-text', true).innerHTML = item.title;
        }
    },

    //private
    onItemIconChanged: function(item, iconCls, oldCls) {
        var el = this.getTabEl(item);
        if (el) {
            el = Ext.get(el);
            el.child('span.ux-icontab-strip-text').replaceClass(oldCls, iconCls);
            el[Ext.isEmpty(iconCls) ? 'removeClass' : 'addClass']('ux-icontab-with-icon');
        }
    },

    getTabEl: function(item) {
        var c = this.getComponent(item);
        return c ? c.tabEl : null;
    },

    // private
    onResize: function(adjWidth, adjHeight, rawWidth, rawHeight) {
        var w = adjWidth,
            h = adjHeight;

        if (Ext.isDefined(w) || Ext.isDefined(h)) {
            if (!this.collapsed) {
                // First, set the the Panel's body width.
                // If we have auto-widthed it, get the resulting full offset width so we can size the Toolbars to match
                // The Toolbars must not buffer this resize operation because we need to know their heights.

                if (Ext.isNumber(w)) {
                    this.body.setWidth(w = this.adjustBodyWidth(w - this.getFrameWidth()));
                } else if (w == 'auto') {
                    w = this.body.setWidth('auto').dom.offsetWidth;
                } else {
                    w = this.body.dom.offsetWidth;
                }
                var iw = this.body.getBox(true).width - this.tabStrip.getWidth();
                this.innerBody.setWidth(iw);

                if (this.tbar) {
                    this.tbar.setWidth(w);
                    if (this.topToolbar) {
                        this.topToolbar.setSize(w);
                    }
                }
                if (this.bbar) {
                    this.bbar.setWidth(w);
                    if (this.bottomToolbar) {
                        this.bottomToolbar.setSize(w);
                        // The bbar does not move on resize without this.
                        if (Ext.isIE) {
                            this.bbar.setStyle('position', 'static');
                            this.bbar.setStyle('position', '');
                        }
                    }
                }
                if (this.footer) {
                    this.footer.setWidth(w);
                    if (this.fbar) {
                        this.fbar.setSize(Ext.isIE ? (w - this.footer.getFrameWidth('lr')) : 'auto');
                    }
                }

                // At this point, the Toolbars must be layed out for getFrameHeight to find a result.
                if (Ext.isNumber(h)) {
                    h = Math.max(0, h - this.getFrameHeight());
                    //h = Math.max(0, h - (this.getHeight() - this.body.getHeight()));
                    this.body.setHeight(h);
                } else if (h == 'auto') {
                    this.body.setHeight(h);
                }

                if (this.disabled && this.el._mask) {
                    this.el._mask.setSize(this.el.dom.clientWidth, this.el.getHeight());
                }
            } else {
                // Adds an event to set the correct height afterExpand.  This accounts for the deferHeight flag in panel
                this.queuedBodySize = { width: w, height: h };
                if (!this.queuedExpand && this.allowQueuedExpand !== false) {
                    this.queuedExpand = true;
                    this.on('expand', function() {
                        delete this.queuedExpand;
                        this.onResize(this.queuedBodySize.width, this.queuedBodySize.height);
                    }, this, { single: true });
                }
            }
            this.onBodyResize(w, h);
        }
        this.syncShadow();
        if (this.tabStripWrapBody) {
            var h = this.tabStripWrap.getHeight() - this.tabStripWrapScrollUp.getHeight() - this.tabStripWrapScrollDown.getHeight();
            this.tabStripWrapBody.setHeight(h);
        }
        //Ext.ux.IconTabPanel.superclass.superclass.onResize.call(this, adjWidth, adjHeight, rawWidth, rawHeight);
        // panel since we've completly override panels onResize
        Ext.Panel.superclass.onResize.call(this, adjWidth, adjHeight, rawWidth, rawHeight);
        this.delegateUpdates();

    },

    /**
    * Suspends any internal calculations or scrolling while doing a bulk operation. See {@link #endUpdate}
    */
    beginUpdate: function() {
        this.suspendUpdates = true;
    },

    /**
    * Resumes calculations and scrolling at the end of a bulk operation. See {@link #beginUpdate}
    */
    endUpdate: function() {
        this.suspendUpdates = false;
        this.delegateUpdates();
    },

    /**
    * Hides the tab strip item for the passed tab
    * @param {Number/String/Panel} item The tab index, id or item
    */
    hideTabStripItem: function(item) {
        item = this.getComponent(item);
        var el = this.getTabEl(item);
        this.stack.remove(item);
        if (el) {
            Ext.get(el).addClass('x-hide-display');
            this.delegateUpdates();
        }
        
    },

    /**
    * Unhides the tab strip item for the passed tab
    * @param {Number/String/Panel} item The tab index, id or item
    */
    unhideTabStripItem: function(item) {
        item = this.getComponent(item);
        var el = this.getTabEl(item);
        if (el) {
            Ext.get(el).removeClass('x-hide-display');
            this.delegateUpdates();
        }
    },

    // private
    delegateUpdates: function() {
        if (this.suspendUpdates) {
            return;
        }
        if (this.resizeTabs && this.rendered) {
            this.autoSizeTabs();
        }
        if (this.enableTabScroll && this.rendered) {
            this.autoScrollTabs();
        }
    },

    // private
    autoSizeTabs: function() {
        var count = this.items.length,
            ce = this.tabPosition != 'bottom' ? 'header' : 'footer',
            ow = this[ce].dom.offsetWidth,
            aw = this[ce].dom.clientWidth;

        if (!this.resizeTabs || count < 1 || !aw) { // !aw for display:none
            return;
        }

        var each = Math.max(Math.min(Math.floor((aw - 4) / count) - this.tabMargin, this.tabWidth), this.minTabWidth); // -4 for float errors in IE
        this.lastTabWidth = each;
        var lis = this.strip.query('li:not(.x-tab-edge)');
        for (var i = 0, len = lis.length; i < len; i++) {
            var li = lis[i],
                inner = Ext.fly(li).child('.x-tab-strip-inner', true),
                tw = li.offsetWidth,
                iw = inner.offsetWidth;
            inner.style.width = (each - (tw - iw)) + 'px';
        }
    },

    // private
    adjustBodyWidth: function(w) {
        if (this.header) {
            this.header.setWidth(w);
        }
        if (this.footer) {
            this.footer.setWidth(w);
        }
        return w;
    },

    /**
    * Sets the specified tab as the active tab. This method fires the {@link #beforetabchange} event which
    * can <tt>return false</tt> to cancel the tab change.
    * @param {String/Number} item
    * The id or tab Panel to activate. This parameter may be any of the following:
    * <div><ul class="mdetail-params">
    * <li>a <b><tt>String</tt></b> : representing the <code>{@link Ext.Component#itemId itemId}</code>
    * or <code>{@link Ext.Component#id id}</code> of the child component </li>
    * <li>a <b><tt>Number</tt></b> : representing the position of the child component
    * within the <code>{@link Ext.Container#items items}</code> <b>property</b></li>
    * </ul></div>
    * <p>For additional information see {@link Ext.util.MixedCollection#get}.
    */
    setActiveTab: function(item) {
        item = this.getComponent(item);
        if (this.fireEvent('beforetabchange', this, item, this.activeTab) === false) {
            return;
        }
        if (!this.rendered) {
            this.activeTab = item;
            return;
        }
        if (this.activeTab != item) {
            if (this.activeTab) {
                var oldEl = this.getTabEl(this.activeTab);
                if (oldEl) {
                    Ext.fly(oldEl).removeClass('ux-icontab-strip-active');
                }
            }
            this.activeTab = item;
            if (item) {
                var el = this.getTabEl(item);
                Ext.fly(el).addClass('ux-icontab-strip-active');
                this.stack.add(item);

                this.layout.setActiveItem(item);
                if (this.scrolling) {
                    this.scrollToTab(item, this.animScroll);
                }
            }
            this.fireEvent('tabchange', this, item);
        }
    },

    /**
    * Returns the Component which is the currently active tab. <b>Note that before the TabPanel
    * first activates a child Component, this method will return whatever was configured in the
    * {@link #activeTab} config option.</b>
    * @return {BoxComponent} The currently active child Component if one <i>is</i> active, or the {@link #activeTab} config value.
    */
    getActiveTab: function() {
        return this.activeTab || null;
    },

    /**
    * Gets the specified tab by id.
    * @param {String} id The tab id
    * @return {Panel} The tab
    */
    getItem: function(item) {
        return this.getComponent(item);
    },

    // private
    autoScrollTabs: function() {
        /*
        this.tabStripWrap 
        this.tabStripWrapScrollUp 
        this.tabStripWrapBody
        this.tabStrip
        this.tabStripWrapScrollDown 
        */

        this.pos = this.tabStrip; //this.tabPosition == 'bottom' ? this.footer : this.header;
        var count = this.items.length,
            ow = this.pos.dom.offsetHeight,
            tw = this.pos.dom.clientHeight,
            wrap = this.tabStripWrapBody,
            wd = wrap.dom,
            cw = wd.offsetHeight,
            pos = this.getScrollPos(),
            l = this.tabStripWrapBody.getOffsetsTo(this.tabStrip)[1] + pos;


        if (!this.enableTabScroll || count < 1 || cw < 20) { // 20 to prevent display:none issues
            return;
        }
        if (l <= tw) {
            wd.scrollTop = 0;
            wrap.setHeight(tw);
            if (this.scrolling) {
                this.scrolling = false;
                this.pos.removeClass('ux-icontab-scrolling');
                this.tabStripWrapScrollUp.hide();
                this.tabStripWrapScrollDown.hide();
                // See here: http://extjs.com/forum/showthread.php?t=49308&highlight=isSafari
                if (Ext.isAir || Ext.isWebKit) {
                    wd.style.marginLeft = '';
                    wd.style.marginRight = '';
                }
            }
        } else {
            if (!this.scrolling) {
                this.pos.addClass('ux-icontab-scrolling');
                // See here: http://extjs.com/forum/showthread.php?t=49308&highlight=isSafari
                /* if (Ext.isAir || Ext.isWebKit) {
                wd.style.marginLeft = '18px';
                wd.style.marginRight = '18px';
                }*/
            }
            tw -= wrap.getMargins('tb');
            wrap.setWidth(tw > 20 ? tw : 20);
            if (!this.scrolling) {

                this.tabStripWrapScrollUp.show();
                this.tabStripWrapScrollDown.show();

            }
            this.scrolling = true;
            if (pos > (l - tw)) { // ensure it stays within bounds
                wd.scrollTop = l - tw;
            } else { // otherwise, make sure the active tab is still visible
                this.scrollToTab(this.activeTab, false);
            }
            this.updateScrollButtons();
        }
    },

    // private
    createScrollers: function() {
        this.pos.addClass('ux-icontab-scrolling-' + this.tabPosition);
        var h = this.stripWrap.dom.offsetHeight;

        // left
        var sl = this.pos.insertFirst({
            cls: 'ux-icontab-scroller-left'
        });
        sl.setHeight(h);
        sl.addClassOnOver('ux-icontab-scroller-left-over');
        this.leftRepeater = new Ext.util.ClickRepeater(sl, {
            interval: this.scrollRepeatInterval,
            handler: this.onScrollLeft,
            scope: this
        });
        this.scrollLeft = sl;

        // right
        var sr = this.pos.insertFirst({
            cls: 'ux-icontab-scroller-right'
        });
        sr.setHeight(h);
        sr.addClassOnOver('ux-icontab-scroller-right-over');
        this.rightRepeater = new Ext.util.ClickRepeater(sr, {
            interval: this.scrollRepeatInterval,
            handler: this.onScrollRight,
            scope: this
        });
        this.scrollRight = sr;
    },

    // private
    getScrollWidth: function() {
        return this.edge.getOffsetsTo(this.tabStrip)[0] + this.getScrollPos();
    },
    getScrollHeight: function() {
        return this.tabStripWrapBody.getOffsetsTo(this.tabStrip)[1] + this.getScrollPos();
    },
    // private
    getScrollPos: function() {
        return parseInt(this.tabStrip.dom.scrollTop, 10) || 0;
    },

    // private
    getScrollArea: function() {
        return parseInt(this.tabStrip.dom.clientHeight, 10) || 0;
    },

    // private
    getScrollAnim: function() {
        return { duration: this.scrollDuration, callback: this.updateScrollButtons, scope: this };
    },

    // private
    getScrollIncrement: function() {
        return this.scrollIncrement || (this.resizeTabs ? this.lastTabHeight + 2 : 100);
    },

    /**
    * Scrolls to a particular tab if tab scrolling is enabled
    * @param {Panel} item The item to scroll to
    * @param {Boolean} animate True to enable animations
    */

    scrollToTab: function(item, animate) {
        if (!item) {
            return;
        }
        var el = this.getTabEl(item),
            pos = this.getScrollPos(),
            area = this.getScrollArea(),
            left = Ext.fly(el).getOffsetsTo(this.tabStrip)[1] + pos,
            right = left + el.offsetHeight;
        if (left < pos) {
            this.scrollTo(left, animate);
        } else if (right > (pos + area)) {
            this.scrollTo(right - area, animate);
        }
    },

    // private
    scrollTo: function(pos, animate) {
        this.tabStrip.scrollTo('top', pos, animate ? this.getScrollAnim() : false);
        if (!animate) {
            this.updateScrollButtons();
        }
    },

    onWheel: function(e) {
        var d = e.getWheelDelta() * this.wheelIncrement * -1;
        e.stopEvent();

        var pos = this.getScrollPos(),
            newpos = pos + d;
        //,
        //sh = this.getScrollHeight(); // -this.getScrollArea();

        var s = newpos; // Math.max(0, Math.min(sh, newpos));
        if (s != pos) {
            this.scrollTo(s, false);
        }
    },

    // private
    onScrollDown: function() {
        var sw = this.getScrollHeight(), //- this.getScrollArea()
            pos = this.getScrollPos(),
            s = pos + this.getScrollIncrement(); //Math.min(sw, pos + this.getScrollIncrement());
        if (s != pos) {
            this.scrollTo(s, this.animScroll);
        }
    },

    // private
    onScrollUp: function() {
        var pos = this.getScrollPos(),
            s = Math.max(0, pos - this.getScrollIncrement());
        if (s != pos) {
            this.scrollTo(s, this.animScroll);
        }
    },

    // private
    updateScrollButtons: function() {
        /* TODO
        var pos = this.getScrollPos();
        this.scrollLeft[pos === 0 ? 'addClass' : 'removeClass']('x-tab-scroller-left-disabled');
        this.scrollRight[pos >= (this.getScrollWidth() - this.getScrollArea()) ? 'addClass' : 'removeClass']('x-tab-scroller-right-disabled');
        */
    },

    // private
    beforeDestroy: function() {
        Ext.destroy(this.upRepeater, this.downRepeater); // TODO
        this.deleteMembers('strip', 'edge', 'scrollLeft', 'scrollRight', 'stripWrap');
        this.activeTab = null;
        Ext.ux.IconTabPanel.superclass.beforeDestroy.apply(this);
    }


});
Ext.reg('icontabpanel', Ext.ux.IconTabPanel);

/**
* See {@link #setActiveTab}. Sets the specified tab as the active tab. This method fires
* the {@link #beforetabchange} event which can <tt>return false</tt> to cancel the tab change.
* @param {String/Panel} tab The id or tab Panel to activate
* @method activate
*/
Ext.ux.IconTabPanel.prototype.activate = Ext.ux.IconTabPanel.prototype.setActiveTab;

// private utility class used by IconTabPanel (copy from tabpanel)
Ext.ux.IconTabPanel.AccessStack = function() {
    var items = [];
    return {
        add: function(item) {
            items.push(item);
            if (items.length > 10) {
                items.shift();
            }
        },

        remove: function(item) {
            var s = [];
            for (var i = 0, len = items.length; i < len; i++) {
                if (items[i] != item) {
                    s.push(items[i]);
                }
            }
            items = s;
        },

        next: function() {
            return items.pop();
        }
    };
};
