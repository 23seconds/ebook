eBook.Home.Cards = Ext.extend(Ext.Panel, {
    initComponent: function () {
        this.GTH = eBook.User.hasRoleLike('GTH');
        this.eBookUser = eBook.User.hasRoleLike('GTH', true); // roles NOT like GTH
        var its = [],
            reportItems = [
                {
                    xtype: 'button'
                    , text: "My client's engagement agreements"
                    , listeners: {
                    click: this.openEngagementAgreementView
                },
                    flex: 1,
                    iconCls: 'eBook-icon-excel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    width: 100
                },
                {
                    xtype: 'button'
                    , text: "My client's CODA proxies"
                    , listeners: {
                    click: this.openPowerOfAttorneyView
                },
                    flex: 1,
                    iconCls: 'eBook-icon-excel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    width: 100
                },
                {
                    xtype: 'button',
                    text: "My client's year end deliverable",
                    handler: this.openYearEndReport,
                    scope: this,
                    flex: 1,
                    iconCls: 'eBook-icon-excel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    width: 100
                }
                /*{
                 xtype: 'home-charts', flex: 3
                 }*/
            ];

        var currentUser = eBook.User.personId;
        var allowedUsers = ['29EA3413-4930-4B20-A22F-35B11B4864F7','6BF61892-D9A0-495E-A07B-185DC7B03F54','A2AA22FF-EAA3-42F7-8B10-26423F8F581C','18BC4A5D-A798-4D20-B097-E2F6E1A66E3E','BF723AA4-4974-4DCE-B01D-643764805FB7','AC7B9975-423F-4EB4-86E2-7EFBFA93AF47']; //Kim,Timothy,Theo,Lieve,Mieke,Frédéric
        for(var i = 0;i < allowedUsers.length; i++)
        {
            if(currentUser.toUpperCase() == allowedUsers[i].toUpperCase())
            {
                reportItems.push({
                    xtype: 'button',
                    text: "My client's NBB filing",
                    handler: this.openNBBfilingReport,
                    scope: this,
                    flex: 1,
                    iconCls: 'eBook-icon-excel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    width: 100
                });
            }
        }

        reportItems.push({
            xtype: 'spacer',
            flex: 3
        });

        if (this.eBookUser) {
            // START PAGE eBOOK USER
            its.push({
                xtype: 'panel'
                , id: 'eBook-mainPage'
                , layout: 'anchor'
                , anchorSize: { width: 800, height: 600 }
                , items: [{
                    xtype: 'panel', anchor: '100% 50%', layout: 'hbox'
                             , layoutConfig: { align: 'stretch' },
                    items: [{ xtype: 'ebook-home-recentfiles', ref: '../../lastFiles', flex: 1, anchor: '50% 100%' },
                                    { ref: '../statistics'
                                    , xtype: 'panel'
                                    , flex: 1
                                    , layout: {
                                        type: 'vbox',
                                        padding: '5',
                                        align: 'stretch'
                                    }
                                    , items: reportItems
                                    , anchor: '50% 100%'
                                    }
                             ]
                }
                          , { xtype: 'ebook-home-myclients', ref: '../clientSearch', anchor: '100% 50%', id: 'eBook-clients-grid' }
                        ]
                , border: false
                , ref: 'eBookStart'
            });

            // CLIENT OVERVIEW
            its.push({ xtype: 'clienthome', ref: 'clientMenu' });

            // FILE OVERVIEW
            its.push({ xtype: 'filehome', ref: 'fileMenu' });
        }
        if (this.GTH) {
            this.GTHIndex = its.length;
            its.push({ xtype: 'gthhome', ref: 'gthHome', listeners: { 'activate': { fn: this.onGTHActive, scope: this }, 'deactivate': { fn: this.onGTHInactive, scope: this}} });
        }

        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , layoutConfig: {
                deferredRender: true
            }
            , items: its
            , border: false
        });
        eBook.Home.Cards.superclass.initComponent.apply(this, arguments);
    }
    , onGTHActive: function () {
        this.gthHome.startUpdates();
    }
    , onGTHInactive: function () {
        this.gthHome.stopUpdates();
    }
    , showGth: function () {
        if (this.GTH) this.layout.setActiveItem(this.GTHIndex);
    }
    , showeBook: function () {
        if (this.eBookUser) {
            this.layout.setActiveItem(0);
            this.lastFiles.store.load({
                params: {
                    Person: eBook.User.getActivePersonDataContract()
                }
            });
        }
    }
    , openClient: function (record) {
        if (this.eBookUser) {
            this.layout.setActiveItem(1);
            this.clientMenu.repository.reload();
            this.clientMenu.loadRec(record);
        }
    }
    , openFile: function (record) {
        if (this.eBookUser) {
            this.layout.setActiveItem(2);
            this.fileMenu.loadRec(record);
        }
    }
    , setTax: function (o) {
        this.fileMenu.setTax(o);
    }
    , openEngagementAgreementView: function (btn) {
        var window = new eBook.Reports.EngagementAgreement.Window({});
        window.show();
    }
    , openPowerOfAttorneyView: function (btn) {
        var window = new eBook.Reports.PowerOfAttorney.Window({});
        window.show();
    }
    , openYearEndReport: function(btn) {
        var me = this,
            serviceId = 'C3B8C8DB-3822-4263-9098-541FAE897D02',
            department = 'ACR';

        me.openFileServiceView(serviceId,department);
    }
    ,
    openNBBfilingReport: function(btn) {
        var me = this,
            serviceId = '1ABF0836-7D7B-48E7-9006-F03DB97AC28B',
            department = 'ACR';

        me.openFileServiceView(serviceId,department);
    }
    ,
    openFileServiceView: function (serviceId,department) {
        var window = new eBook.Reports.FileService.Window({serviceId: serviceId, department: 'ACR'});
        window.show();
    }
});

Ext.reg('ebook-home-cards', eBook.Home.Cards);