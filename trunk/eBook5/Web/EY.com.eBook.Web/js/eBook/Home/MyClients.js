eBook.Home.MyClients = Ext.extend(eBook.grid.PagedGridPanel, {

    initComponent:function() {
        Ext.apply(this, {
             title: eBook.InterfaceTrans_ClientSearch
            , storeConfig: {
                selectAction: 'GetClients',
                fields: eBook.data.RecordTypes.ClientBase,
                idField: 'id',
                criteriaParameter: 'ccdc',
                serviceUrl: eBook.Service.home
            }
            , sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
            })
            ,border:true
            //,bodyStyle:'margin-top:10px;'
            , viewConfig: { forceResize: true }
            , listeners: {
                'rowmousedown': {
                    fn: function(grid, rowIndex, e) {
                        var rec = grid.store.getAt(rowIndex);

                        eBook.Interface.openClient(rec);
                    },
                    scope: this
                }
            }
            , tbar: []
            , pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            }
            , plugins: [new Ext.ux.grid.Search({
                ref: 'searchfield',
                iconCls: 'eBook-search-ico',
                minChars: 3,
                minLength: 3,
                autoFocus: true,
                position: 'top',
                mode: 'remote',
                width: 250,
                searchText: eBook.InterfaceTrans_ClientSearchText,
                searchTipText: '',
                selectAllText: eBook.InterfaceTrans_SelectAllFields
            })]
            , loadMask: true
			, columns: [
			    {
			        header: eBook.InterfaceTrans_GFISCode,
			        width: 120,
			        dataIndex: 'GFISCode',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientName,
			        width: 300,
			        dataIndex: 'Name',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientAddress,
			        width: 160,
			        dataIndex: 'Address',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientZipCode,
			        width: 120,
			        dataIndex: 'ZipCode',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientCity,
			        width: 120,
			        dataIndex: 'City',
			        sortable: true
			    },
			    {
			        header: eBook.InterfaceTrans_ClientCountry,
			        width: 120,
			        dataIndex: 'Country',
			        sortable: true
            }]
            , anchor: '100% 50%'
            , border: false
            , style: 'margin-top:10px'
        });
        eBook.Home.MyClients.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('ebook-home-myclients', eBook.Home.MyClients);