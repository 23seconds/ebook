
eBook.Home.RecentFiles = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        Ext.apply(this, {
            title: eBook.InterfaceTrans_MyLatestFiles
            ,loadMask:true
            , store: new eBook.data.JsonStore({
                selectAction: 'GetRecentFiles'
                    , criteriaParameter: 'ccdc'
                    , autoDestroy: true
                    , serviceUrl: eBook.Service.home
                    , fields: eBook.data.RecordTypes.RecentFiles
                    , autoLoad: false
            })
            , listeners: {
                'rowmousedown': {
                    fn: function(grid, rowIndex, e) {
                        var rec = grid.store.getAt(rowIndex);
                        eBook.Interface.openShortCut(rec);
                        //eBook.Interface.loadShortcut({ 'shortcut': rec, 'client': lsti[8], 'file': lsti[9] });
                    },
                    scope: this
                }
            }
            , columns: [{
                header: String.format("{0} {1}", eBook.InterfaceTrans_Client, eBook.InterfaceTrans_ClientName),
                width: 300,
                dataIndex: 'client',
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return value.n;
                }
            }, {
                header: '',
                width: 150,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    var sd = Ext.data.Types.WCFDATE.convert(value.sd);
                    var ed = Ext.data.Types.WCFDATE.convert(value.ed);
                    var ay = value.ay;
                    var str = "" + ay;
                    str += " " + sd.format('d/m/Y');
                    str += " &gt; " + ed.format('d/m/Y');

                    return str;
                }
                   , dataIndex: 'file'
            }, {
                header: eBook.InterfaceTrans_LastAccessed,
                xtype: 'datecolumn',
                format: Date.patterns.FullDateTime,
                //width: 220,
                dataIndex: 'lastaccessed'
}]
            });
            eBook.Home.RecentFiles.superclass.initComponent.apply(this, arguments);
        }
    });
    Ext.reg('ebook-home-recentfiles', eBook.Home.RecentFiles);
    