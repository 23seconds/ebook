/*
eBook.Home.Charts = Ext.extend(Ext.ux.Raphael.Chart.Pie, {
    initComponent:function() {
        Ext.apply(this, {
            layout:'fit',
            store: new Ext.data.JsonStore({
                fields: ['season', 'total'],
                data: [{
                    season: 'Summer',
                    total: 150
                },
                {
                    season: 'Fall',
                    total: 245
                },
                {
                    season: 'Winter',
                    total: 117
                },
                {
                    season: 'Spring',
                    total: 184
                }]
            })
            , dataField: 'total'
            , categoryField: 'season'
            , radius: 80
            , listeners: {resize:{fn:this.onResize,scope:this}}
        });
        eBook.Home.Charts.superclass.initComponent.apply(this,arguments);
    }
    , setSize: function() { 
        
    }
    , onResize: function(adjWidth, adjHeight, rawWidth, rawHeight) {
        eBook.Home.Charts.superclass.onResize.apply(this, arguments);
        this.drawIt();
    }
    , draw: function(p) {
        this.x = this.el.getWidth() / 3;
        this.y = this.el.getHeight() / 3;
        this.radius = this.x < this.y ? this.x * 0.7 : this.y * 0.7;
        eBook.Home.Charts.superclass.draw.apply(this, arguments);
    }
});

Ext.reg('home-charts', eBook.Home.Charts);
*/


eBook.Home.Charts = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit',
            html: 'coming soon'
            , listeners: { afterlayout: { fn: this.onResize, scope: this} }
        });
        eBook.Home.Charts.superclass.initComponent.apply(this, arguments);
    }
     , onResize: function(adjWidth, adjHeight, rawWidth, rawHeight) {
         //  eBook.Home.Charts.superclass.onResize.apply(this, arguments);
         var sze = this.ownerCt.getSize();
         this.tmlEl.setSize(sze);
         if (this.paper) this.paper.clear();
         this.paper = Raphael(this.tmlEl.dom, sze.width, sze.height);
                  this.paper.image('images/chart.png', (sze.width / 2) - 64, (sze.height / 3) - 64, 128, 128);
                  this.paper.text((sze.width / 2), (sze.height / 3) + 70, 'COMING SOON');
         //this.paper.print(0,0, 'COMING SOON', this.paper.getFont('Arial')).attr({'color':'#000'});

/*
         var lwd = 24;

         var sx = lwd;
         var sy = lwd;

         var ex = lwd;
         var ey = sze.height - lwd - lwd;

         var cp1x = sze.width * 1.2; // 70%;
         var cp1y = sy - (ey * 0.2);

         var cp2x = sze.width * 1.2; // 70%;
         var cp2y = ey + (ey * 0.2);



         var wd = sze.width;
         var hg = sze.height - 24;
         var hgdf = hg / 4;
         //rdp = this.paper.path('M48,24C' + sze.width + ',48,' + sze.width + ',' + (sze.height - 48) + ',48,' + (sze.height - 48));
         //rdp = this.paper.path('M48,24,' + ((wd / 2) + ',24S') + (wd + ',' + (hgdf) + ',') + (wd + ',' + (hg - hgdf)));
         rdp = this.paper.path('M' + sx + ',' + sy
            + ' C ' + cp1x + ',' + cp1y  // control point 1
            + ',' + cp2x + ',' + cp2y // control point 2
            + ',' + ex + ',' + ey);

         rdp.attr({ "arrow-end": "block-midium-short", stroke: "#000", "stroke-width": 24, "stroke-linecap": "round" });

         var len = Raphael.getTotalLength(rdp.attr('path'));
         var cnt = 10;
         var step = len / (cnt + 2);
         var mlen = step;
         for (var i = 0; i < cnt; i++) {
             
             var p = Raphael.getPointAtLength(rdp.attr('path'), mlen);
             var c = this.paper.circle(p.x, p.y, lwd / 3);
             c.attr("fill", "#FFE600");
             c.attr("opacity", "0.6");
             c.mouseover(function() { this.animate({ "r": this.attr('r') * 2, "opacity": "1" }, 500, "<>"); });
             c.mouseout(function() { this.animate({ "r": this.attr('r') / 2, "opacity": "0.6" }, 500, "<>"); });
             mlen += (step);
         }
         */

     }
    , afterRender: function() {
        eBook.Home.Charts.superclass.afterRender.apply(this, arguments);
        this.body.update('<div class="rph-timeline"></div>');
        this.tmlEl = this.body.child('.rph-timeline');
        // this.tmlEl.update('TIMELINE');
        var sze = this.ownerCt.getSize();
        this.tmlEl.setSize(sze);
        this.tmlEl.setStyle('opacity', '0.5');
        /*
        this.paper = Raphael(this.tmlEl.dom, 200, 50);
        var rc = this.paper.rect(0, 0, 400, 50);
        rc.attr('fill', '#CCC');
        rc.attr('stroke-width', 0);

        this.selectionBox = this.paper.rect(10, 0, 180, 50);
        this.selectionBox.attr('fill', '#FFE600');
        this.selectionBox.attr('stroke-width', 0);
        this.selectionBox.attr('opacity', 0.4);
        this.selectionBox.attr('cursor', 'move');

        this.penStart = this.paper.rect(10, 0, 2, 50);
        this.penStart.attr('fill', '#FFE600');
        this.penStart.attr('stroke-width', 0);
        this.penStart.attr('cursor', 'col-resize');
        this.penStart.ox = 0;
        this.penStart.drag(this.onMove, null, this.onPenDragEnd, { pen: this.penStart, ref: 'start', owner: this }, null, { pen: this.penStart, owner: this });

        this.penEnd = this.paper.rect(190, 0, 2, 50);
        this.penEnd.attr('fill', '#FFE600');
        this.penEnd.attr('stroke-width', 0);
        this.penEnd.attr('cursor', 'col-resize');
        this.penEnd.ox = 0;
        this.penEnd.drag(this.onMove, null, this.onPenDragEnd, { pen: this.penEnd, ref: 'end', owner: this }, null, { pen: this.penEnd, owner: this });
        */
    }
    , onMove: function(dx, dy, x, y, evt) {
        this.pen.transform('...T' + (dx - this.pen.ox) + ',0');
        var wd = this.owner.selectionBox.attr('width');

        if (this.ref == 'start') {
            wd = wd - (dx - this.pen.ox);
            this.owner.selectionBox.attr('width', wd);
            this.owner.selectionBox.transform('...T' + (dx - this.pen.ox) + ',0');
        } else {
            wd = wd + (dx - this.pen.ox);
            this.owner.selectionBox.attr('width', wd)
        }

        this.pen.ox = dx;
    }
    , onPenDragEnd: function() {
        this.pen.ox = 0;
    }
});

    Ext.reg('home-charts', eBook.Home.Charts);