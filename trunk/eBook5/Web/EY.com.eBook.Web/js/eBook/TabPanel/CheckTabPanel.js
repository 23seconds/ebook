
eBook.TabPanel.CheckTabPanel = Ext.extend(Ext.TabPanel, {
    initComponent: function() {
        this.itemTpl = new Ext.Template(
                 '<li class="{cls}" id="{id}"><a class="x-tab-strip-close"></a>',

                 '<a class="x-tab-right" href="#"><em class="x-tab-left">',
                 '',
                 '<span class="x-tab-strip-inner"><span class="x-tab-strip-text {iconCls}"><input type="checkbox" class="x-tab-check"/> <b>{text}</b></span></span>',
                 '</em></a>',
                 '</li>'
            );
        this.itemTpl.disableFormats = true;
        this.itemTpl.compile();
        eBook.TabPanel.CheckTabPanel.superclass.initComponent.apply(this, arguments);
    }
    , hasEnabledTabs: function() {
        var hase = false;
        this.items.each(function(pnl) {
            hase = hase || !pnl.disabled;
        }, this);
        return hase;
    }
    , findTargets: function(e) {
        var item = null,
            itemEl = e.getTarget('li:not(.x-tab-edge)', this.strip),
            itemCheckEl = e.getTarget('.x-tab-check', this.strip);

        if (itemEl) {
            item = this.getComponent(itemEl.id.split(this.idDelimiter)[1]);
            if (itemCheckEl) {
                itemCheckEl.checked = !itemCheckEl.checked;
                if (itemCheckEl.checked) {
                    item.enable();
                } else {
                    item.disable();
                }
            } else {
                if (item.disabled) {
                    return {
                        close: null,
                        item: null,
                        el: null
                    };
                }
            }
        }
        return {
            close: e.getTarget('.x-tab-strip-close', this.strip),
            check: itemCheckEl,
            item: item,
            el: itemEl
        };
    },
    onItemTitleChanged: function(item) {
        var el = this.getTabEl(item);
        if (el) {
            Ext.fly(el).child('span.x-tab-strip-text>b', true).innerHTML = item.title;
        }
    },
    // private
    onStripMouseDown: function(e) {
        if (e.button !== 0) {
            return;
        }
        e.preventDefault();
        var t = this.findTargets(e);
        if (t.close) {
            if (t.item.fireEvent('beforeclose', t.item) !== false) {
                t.item.fireEvent('close', t.item);
                this.remove(t.item);
            }
            return;
        }
        if (t.item && t.item != this.activeTab) {
            this.setActiveTab(t.item);
        }
    }
});

Ext.reg('eBook.TabPanel.CheckTabPanel', eBook.TabPanel.CheckTabPanel);