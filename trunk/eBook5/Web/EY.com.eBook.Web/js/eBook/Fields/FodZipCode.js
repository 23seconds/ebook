
eBook.Fields.FodZipCode = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: true
            , forceSelection: true
            , allowBlank: false
            , hideTrigger: false
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'name'
            , width: 70
            , queryParam: 'Query'
            , queryDelay: 0
            , minChars: 0
            , store: new eBook.data.JsonStore({
                selectAction: 'GetFodList'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'cfldc'
                        , baseParams: { ListId: 'ZIPCODE', Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.FodList

            })
        });
        eBook.Fields.FodZipCode.superclass.initComponent.apply(this, arguments);
    }
    , getContract: function() {
        var rec = this.findRecord(this.valueField, this.getValue());
        if (rec) {
            return rec.json;
        } else {
            return null;
        }
    }
    , setValue: function(v) {
        if (typeof v == "object") {
            this.setContract(v);
            return;
        }
        eBook.Fields.FodZipCode.superclass.setValue.call(this, v);
    }
    , setContract: function(obj) {
        eBook.Fields.FodZipCode.superclass.setValue.call(this, obj[this.valueField]);
    }
});

Ext.reg('fod.zipcode', eBook.Fields.FodZipCode);