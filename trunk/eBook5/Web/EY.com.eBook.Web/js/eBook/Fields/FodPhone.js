
eBook.Fields.FodPhone = Ext.extend(Ext.form.FieldSet, {
    initComponent: function() {
        Ext.apply(this, {
            items: [
            //{ xtype: 'textfield', ref: 'phonefax', fieldLabel: 'Phone/Fax' }
                {xtype: 'compositefield', msgTarget: 'side', ref: 'countryzone', allowBlank: false
                    , items: [{ xtype: 'textfield', ref: 'country', fieldLabel: eBook.Fields.FodPhone_Country, value: '+32', width: 40, allowBlank: false }
                            , { xtype: 'textfield', ref: 'zone', fieldLabel: eBook.Fields.FodPhone_Zone, width: 40, allowBlank: false }
                          ]
            }
                , { xtype: 'textfield', ref: 'local', fieldLabel: eBook.Fields.FodPhone_Local, allowBlank: false }
                 , { xtype: 'textfield', ref: 'extension', fieldLabel: eBook.Fields.FodPhone_Extension, allowBlank: true }
             ]
        });
        eBook.Fields.FodPhone.superclass.initComponent.apply(this, arguments);
    }
    , clearInvalid: function() {
        this.countryzone.innerCt.country.clearInvalid();
        this.countryzone.innerCt.zone.clearInvalid();
        this.local.clearInvalid();
    }
    , isValid: function(prevMark) {
        this.clearInvalid();
        if (this.isChecked()) {
            var v = this.getValue();
            var valid = true;
            if (Ext.isEmpty(v.cc)) {
                this.countryzone.innerCt.country.markInvalid();
                valid = false;
            }
            if (Ext.isEmpty(v.z)) {
                this.countryzone.innerCt.zone.markInvalid();
                valid = false;
            }
            if (Ext.isEmpty(v.l)) {
                this.local.markInvalid();
                valid = false;
            }
            return valid;
        }
        return true;
    }
    , getValue: function() {
        return {
            act: this.isChecked()
            , cc: this.countryzone.innerCt.country.getValue()
            , z: this.countryzone.innerCt.zone.getValue()
            , l: this.local.getValue()
            , e: this.extension.getValue()
        };
    }
    , setValue: function(v) {
        if (v) {
            this.countryzone.innerCt.country.setValue(v.cc ? v.cc : '');
            this.countryzone.innerCt.zone.setValue(v.z ? v.z : '');
            this.local.setValue(v.l ? v.l : '');
            this.extension.setValue(v.e ? v.e : '');
            if (v.act != null && typeof v.act != "undefined" && this.checkbox) { this.checkbox.dom.checked = v.act; }
        }
    }
    , isChecked: function() {
        if (this.checkbox) { return this.checkbox.dom.checked; }
        return true;
    }
});

Ext.reg('fod.phone', eBook.Fields.FodPhone);