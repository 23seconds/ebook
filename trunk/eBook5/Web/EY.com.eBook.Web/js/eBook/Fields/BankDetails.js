
eBook.Fields.BankDetails = Ext.extend(Ext.form.FieldSet, {
    initComponent: function() {
        Ext.apply(this, {
        //hideLabel:true,
            autoScroll: false,
            checkboxToggle: true,
            items: [{ xtype: 'textfield', ref: 'bic', fieldLabel: 'BIC', allowBlank: false }
                , { xtype: 'textfield', ref: 'iban', fieldLabel: 'IBAN', allowBlank: true}]
        });
        eBook.Fields.BankDetails.superclass.initComponent.apply(this, arguments);
    }
    , clearInvalid: function() {
        this.bic.clearInvalid();
        this.iban.clearInvalid();
    }
    , isValid: function(prevMark) {
        this.clearInvalid();
        if (this.isChecked()) {
            var v = this.getValue();
            var valid = true;
            if (Ext.isEmpty(v.bic)) {
                this.bic.markInvalid();
                valid = false;
            }
            if (Ext.isEmpty(v.iban)) {
                this.iban.markInvalid();
                valid = false;
            }
            return valid;
        }
        return true;
    }
     , getValue: function() {
         return {
             act: this.isChecked()
            , bic: this.bic.getValue()
            , iban: this.iban.getValue()
         };
     }
    , setValue: function(v) {
        if (v) {
            this.bic.setValue(v.bic ? v.bic : '');
            this.iban.setValue(v.iban ? v.iban : '');
            if (v.act != null && typeof v.act != "undefined" && this.checkbox) { this.checkbox.dom.checked = v.act; }
        }
    }
    , isChecked: function() {
        if (this.checkbox) { return this.checkbox.dom.checked; }
        return true;
    }
});

Ext.reg('bankdetails', eBook.Fields.BankDetails);