
eBook.Fields.FodAddressType = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: false
            , width: 300
            ,listWidth:300
            , forceSelection: true
            , hideTrigger: false
            , typeAhead: false
            , disableKeyFilter: true
            , editable: false
            , enableKeyEvents: false
            ,triggerAction :'all'
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'name'
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetFodList'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'cfldc'
                        , baseParams: { ListId: 'ADDRESSTYPE', Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.FodList
            })
        });
        eBook.Fields.FodAddressType.superclass.initComponent.apply(this, arguments);
    }
    , getContract: function() {
        var rec = this.findRecord(this.valueField, this.getValue());
        if (rec) {
            return rec.json;
        } else {
            return null;
        }
    }
    , setValue: function(v) {
        if (typeof v == "object") {
            this.setContract(v);
            return;
        }
        eBook.Fields.FodAddressType.superclass.setValue.call(this, v);
    }
    , setContract: function(obj) {
        eBook.Fields.FodAddressType.superclass.setValue.call(this, obj[this.valueField]);
    }
});

Ext.reg('fod.addresstype', eBook.Fields.FodAddressType);