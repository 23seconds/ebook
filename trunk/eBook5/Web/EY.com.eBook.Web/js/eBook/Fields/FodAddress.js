
eBook.Fields.FodAddress = Ext.extend(Ext.form.FieldSet, {
    initComponent: function() {
        Ext.apply(this, {
        //hideLabel:true,
            autoScroll:false,
            items: [{ xtype: 'fod.addresstype', ref: 'addresstype', fieldLabel: eBook.Fields.FodAddress_Adrestype, allowBlank: false }
                , {
                    xtype: 'compositefield',
                    //fieldLabel: 'Address',
                    msgTarget: 'side',
                    ref: 'address',
                    // anchor: '-20',
                    items: [{ xtype: 'textfield', ref: 'street', fieldLabel: eBook.Fields.FodAddress_Street, width: 250, allowBlank: false }
                            , { xtype: 'textfield', ref: 'nr', fieldLabel: eBook.Fields.FodAddress_Nr, width: 50, allowBlank: false }
                            , { xtype: 'textfield', ref: 'bus', fieldLabel: eBook.Fields.FodAddress_Box, width: 50 }
                            ]
                }
                , { xtype: 'fod.country', ref: 'country', fieldLabel: eBook.Fields.FodAddress_Country, allowBlank: false }
                , { xtype: 'fod.zipcode', ref: 'zipcode', fieldLabel: eBook.Fields.FodAddress_Zipcode, allowBlank: false }
                , { xtype: 'textfield', ref: 'city', fieldLabel: eBook.Fields.FodAddress_City, allowBlank: true}]
        });
        eBook.Fields.FodAddress.superclass.initComponent.apply(this, arguments);
    }
    , isValid: function(prevmark) {

        var t =this.addresstype.isValid(prevmark)
            || this.address.innerCt.street.isValid(prevmark)
            || this.address.innerCt.nr.isValid(prevmark)
            || this.address.innerCt.bus.isValid(prevmark)
            || this.country.isValid(prevmark)
            || this.zipcode.isValid(prevmark)
            || this.city.isValid(prevmark);
        
        return this.addresstype.isValid(prevmark)
            && this.address.innerCt.street.isValid(prevmark)
            && this.address.innerCt.nr.isValid(prevmark)
            && this.address.innerCt.bus.isValid(prevmark)
            && this.country.isValid(prevmark)
            && this.zipcode.isValid(prevmark)
            && this.city.isValid(prevmark);
    }
    , getValue: function() {
        return {
            at: this.addresstype.getContract()
            , s: this.address.innerCt.street.getValue()
            , n: this.address.innerCt.nr.getValue()
            , b: this.address.innerCt.bus.getValue()
            , co: this.country.getContract()
            , z: this.zipcode.getContract()
            , ci: this.city.getValue()
        };
    }
    , setValue: function(v) {
        if (v) {
            this.addresstype.setValue(v.at ? v.at : '');
            this.address.innerCt.street.setValue(v.s ? v.s : '');
            this.address.innerCt.nr.setValue(v.n ? v.n : '');
            this.address.innerCt.bus.setValue(v.b ? v.b : '');
            this.country.setValue(v.co ? v.co : '');
            this.zipcode.setValue(v.z ? v.z : '');
            this.city.setValue(v.ci ? v.ci : '');
        }
    }
});

Ext.reg('fod.address', eBook.Fields.FodAddress);