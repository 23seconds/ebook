

eBook.Fields.LanguageBox = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            store: eBook.Language.LanguagesList
            , typeAhead: false
            , mode: 'local'
            , selectOnFocus: true
            , autoSelect: true
            , editable: false
            , forceSelection: true
            , triggerAction: 'all'
        });
        eBook.Fields.LanguageBox.superclass.initComponent.apply(this, arguments);
    }
    , getSelectedDisplay: function() {
        var idx = this.store.findExact(this.valueField, this.getValue());
        var rec = this.store.getAt(idx);
        return rec.get(this.displayField);
    }
});

Ext.reg('languagebox',eBook.Fields.LanguageBox);