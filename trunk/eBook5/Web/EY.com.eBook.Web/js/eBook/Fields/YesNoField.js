

eBook.Fields.YesNoField = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            store: [Ext.MessageBox.buttonText.yes, Ext.MessageBox.buttonText.no]
            , typeAhead: false
            , triggerAction: 'all'
        });
        eBook.Fields.YesNoField.superclass.initComponent.apply(this, arguments);
    }
    , getValue: function() {
        var val = eBook.Fields.YesNoField.superclass.getValue.call(this);
        switch (val) {
            case Ext.MessageBox.buttonText.yes:
                return true;
            case Ext.MessageBox.buttonText.no:
                return false;
        }
        return null;
    }
    , setValue: function(val) {
        if (Ext.isBoolean(val)) {
            if (val) {
                eBook.Fields.YesNoField.superclass.setValue.call(this, Ext.MessageBox.buttonText.yes);
            } else {
                eBook.Fields.YesNoField.superclass.setValue.call(this, Ext.MessageBox.buttonText.no);
            }
        } else if (val == Ext.MessageBox.buttonText.yes || val == Ext.MessageBox.buttonText.no) {
            eBook.Fields.YesNoField.superclass.setValue.call(this, val);
        } else {
            this.clearValue();
        }
    }
});

Ext.reg('yesno', eBook.Fields.YesNoField);