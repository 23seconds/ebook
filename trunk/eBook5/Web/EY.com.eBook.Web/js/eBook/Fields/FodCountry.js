
eBook.Fields.FodCountry = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: true
            , forceSelection: true
            , hideTrigger: true
            , mode: 'local'
            , width: 200
            , valueField: 'id'
            , hideTrigger: true
            , displayField: 'name'
            , queryParam: 'Query'
            , queryDelay: 10
            , minChars:2
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetFodList'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'cfldc'
                        , baseParams: { ListId: 'COUNTRY', Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.FodList
            })
        });
        eBook.Fields.FodCountry.superclass.initComponent.apply(this, arguments);
    }
    , getContract: function() {
        var rec = this.findRecord(this.valueField, this.getValue());
        if (rec) {
            return rec.json;
        } else {
            return null;
        }
    }
    , setValue: function(v) {
        if (typeof v == "object") {
            this.setContract(v);
            return;
        }
        eBook.Fields.FodCountry.superclass.setValue.call(this, v);
    }
    , setContract: function(obj) {
        eBook.Fields.FodCountry.superclass.setValue.call(this, obj[this.valueField]);
    }
});

Ext.reg('fod.country', eBook.Fields.FodCountry);