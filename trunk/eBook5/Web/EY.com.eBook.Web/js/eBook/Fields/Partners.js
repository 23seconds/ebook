
eBook.Fields.TaxPartners = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: false
            , width: 300
            , autoSelect: true
            , listWidth: 300
            , forceSelection: true
            , hideTrigger: false
            , typeAhead: false
            , disableKeyFilter: true
            , editable: false
            , enableKeyEvents: false
            , triggerAction: 'all'
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'name'
            , store: new eBook.data.JsonStore({
                selectAction: 'GetPartners'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'ciddc'
                        , baseParams: { Id: eBook.Interface.currentClient.get('Id'), Department:'TAX' }
                        , fields: eBook.data.RecordTypes.Partners
                //, queryParam: 'Query'
            })
        });
        eBook.Fields.TaxPartners.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('TaxPartners', eBook.Fields.TaxPartners);