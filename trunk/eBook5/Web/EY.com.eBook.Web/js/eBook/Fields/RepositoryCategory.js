


eBook.Fields.RepositoryCategory = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            store: eBook.Repository.CategoriesList
            , typeAhead: false
            , mode: 'local'
            , selectOnFocus: true
            , autoSelect: true
            , editable: false
            , forceSelection: true
            , triggerAction: 'all'
        });
        eBook.Fields.RepositoryCategory.superclass.initComponent.apply(this, arguments);
    }
    , getSelectedDisplay: function() {
        var idx = this.store.findExact(this.valueField, this.getValue());
        var rec = this.store.getAt(idx);
        return rec.get(this.displayField);
    }
});

Ext.reg('repository-category', eBook.Fields.RepositoryCategory);