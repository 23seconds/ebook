
eBook.Fields.LegalType = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            typeAhead: false
            , width: 300
            ,autoSelect :true
            ,listWidth:300
            , forceSelection: true
            , hideTrigger: false
            , typeAhead: false
            , disableKeyFilter: true
            , editable: false
            , enableKeyEvents: false
            ,triggerAction :'all'
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'listdesc'
            , store: new eBook.data.JsonStore({
                        selectAction: 'GetLegalTypes'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'ccdc'
                        , baseParams: { Culture: eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.LegalTypes
                        //, queryParam: 'Query'
            })
        });
        eBook.Fields.LegalType.superclass.initComponent.apply(this, arguments);
    }
//    , getContract: function() {
//        var rec = this.findRecord(this.valueField, this.getValue());
//        if (rec) {
//            return rec.json;
//        } else {
//            return null;
//        }
//    }
//    , setValue: function(v) {
//        if (typeof v == "object") {
//            this.setContract(v);
//            return;
//        }
//        eBook.Fields.LegalType.superclass.setValue.call(this, v);
//    }
//    , setContract: function(obj) {
//    eBook.Fields.LegalType.superclass.setValue.call(this, obj[this.valueField]);
//    }
});

Ext.reg('legaltype', eBook.Fields.LegalType);