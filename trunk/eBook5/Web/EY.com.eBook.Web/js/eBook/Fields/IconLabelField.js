

eBook.Fields.IconLabelField = Ext.extend(Ext.BoxComponent, {
    iconCls: ''
    , text: ''
    , autoEl: 'div'
    , cls: ''
    , itemStyle: ''
    , onRender: function(ct, position) {
        eBook.Fields.IconLabelField.superclass.onRender.call(this, ct, position);
        this.el.addClass('eBook-fields-iconlabelfield');
        if (this.cls != '') this.el.addClass(this.cls);
        if(this.itemStyle!='') this.el.setStyle(this.itemStyle);
        if (this.iconCls != '') this.el.addClass(this.iconCls);
        this.el.update(this.text);
    }
    , setIconCls: function(cls) {
        this.el.replaceClass(this.iconCls, cls);
        this.iconCls = cls;
    }
    , setText: function(txt) {
        this.el.update(txt);
        this.text = txt;
    }
    , update: function(txt, iconCls) {
        this.setIconCls(iconCls);
        this.setText(txt);
    }
});

Ext.reg('iconlabelfield', eBook.Fields.IconLabelField);
