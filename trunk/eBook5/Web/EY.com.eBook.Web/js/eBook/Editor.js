
eBook.Editor = Ext.extend(Ext.Editor, {
    onRender: function(ct, position) {
        this.el = new Ext.Layer({
            shadow: this.shadow,
            cls: "x-tree-editor",
            parentEl: ct,
            shim: this.shim,
            shadowOffset: this.shadowOffset || 4,
            id: this.id,
            constrain: this.constrain
        });
        if (this.zIndex) {
            this.el.setZIndex(this.zIndex);
        }
        this.el.setStyle("overflow", Ext.isGecko ? "auto" : "hidden");
        if (this.field.msgTarget != 'title') {
            this.field.msgTarget = 'qtip';
        }
        this.field.inEditor = true;
        this.mon(this.field, {
            scope: this,
            blur: this.onBlur,
            specialkey: this.onSpecialKey
        });
        if (this.field.grow) {
            this.mon(this.field, "autosize", this.el.sync, this.el, { delay: 1 });
        }
        this.field.render(this.el).show();
        this.field.getEl().dom.name = '';
        if (this.swallowKeys) {
            this.field.el.swallowEvent([
                    'keypress', // *** Opera
                    'keydown'   // *** all other browsers
                ]);
        }
    }
    , startEdit: function(el, value) {
        if (this.editing) {
            this.completeEdit();
        }
        this.boundEl = Ext.get(el);
        var v = value !== undefined ? value : this.boundEl.dom.innerHTML;
        if (!this.rendered) {
            this.render(this.parentEl || document.body);
        }
        if (this.fireEvent("beforestartedit", this, this.boundEl, v) !== false) {
            this.startValue = v;
            this.field.reset();
            this.field.setValue(v);
            this.show();
            this.realign(true);
            this.editing = true;

        }
    },
    realign: function(autoSize) {
        if (autoSize === true) {
            this.doAutoSize();
        }
        //this.el.alignTo(this.boundEl, this.alignment, this.offsets);
        this.el.setXY(this.boundEl.getXY());
    }
    , onClick: function() { }
    //, onBlur: function() { alert('blur'); }
});