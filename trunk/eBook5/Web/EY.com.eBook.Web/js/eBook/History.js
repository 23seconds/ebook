eBook.History = {
    maxLength: 5
    , cookieName: 'eBook.FileHistory'
    , list: []
    , store: new Ext.data.ArrayStore({
        idIndex: 4
        , fields: eBook.data.RecordTypes.FileHistory
    })
    , addFile: function(fileRec, clientRec) {
        var idx = this.searchFile(fileRec.get('Id'));
        if (idx > -1) this.list.splice(idx);
        if (this.list.length >= 5) this.list.splice(4);
        var frec = [
            clientRec.get('Id')
            , clientRec.get('GFISCode')
            , clientRec.get('Name')
            , fileRec.get('Id')
            , fileRec.get('Name')
            , fileRec.get('StartDate')
            , fileRec.get('EndDate')
            , new Date()
        ];
        this.list.unshift({});
        this.list[0] = frec;
        this.save();
    }
    , searchFile: function(fid) {
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i][3] == fid) return i;
        }
        return -1;
    }
    , load: function() {
        this.list = Ext.decode(Ext.util.Cookies.get(this.cookieName));
        if (this.list == null) this.list = [];
        this.store.loadData(this.list);
    }
    , save: function() {
        Ext.util.Cookies.set(this.cookieName, Ext.encode(this.list));
        //Ext.util.Cookies.set(this.cookieName, Ext.encode([]));
        this.store.loadData(this.list);
    }
};
