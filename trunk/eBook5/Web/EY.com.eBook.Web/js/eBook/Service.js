// PRODUCTION VERSION
eBook.Service = {
    url:'/EY.com.eBook.API.5/GlobalService.svc/',
    bizTaxOld: '/EY.com.eBook.API.5/BizTaxService.svc/',
    bundle: '/EY.com.eBook.API.5/BundleService.svc/',
    lists: '/EY.com.eBook.API.5/ListService.svc/',
    home: '/EY.com.eBook.API.5/HomeService.svc/',
    file: '/EY.com.eBook.API.5/FileService.svc/',
    schema: '/EY.com.eBook.API.5/AccountSchemaService.svc/',
    client: '/EY.com.eBook.API.5/ClientService.svc/',
    businessrelation: '/EY.com.eBook.API.5/BusinessRelationService.svc/',
    proacc: '/EY.com.eBook.API.5/ProAccWebService.svc/',
    excel: '/EY.com.eBook.API.5/ExcelService.svc/',
    document: '/EY.com.eBook.API.5/DocumentService.svc/',
    output: '/EY.com.eBook.API.5/OutputService.svc/',
    rule2011: '/EY.com.eBook.RuleEngine.5/RuleEngine2011.svc/',
    rule2012: '/EY.com.eBook.RuleEngine.5/RuleEngine2012.svc/',
    rule2013: '/EY.com.eBook.RuleEngine.5/RuleEngine2013.svc/',
    rule2014: '/EY.com.eBook.RuleEngine.5/RuleEngine2014.svc/',
    rule2015: '/EY.com.eBook.RuleEngine.5/RuleEngine2015.svc/',
    rule2016: '/EY.com.eBook.RuleEngine.5/RuleEngine2016.svc/',
    rule: function(year){return '/EY.com.eBook.RuleEngine.5/RuleEngine'+year+'.svc/'},
    repository: '/EY.com.eBook.API.5/RepositoryService.svc/',
    GTH: '/EY.com.eBook.API.5/GTHTeamService.svc/',
    biztax: '/EY.com.eBook.BizTax.Wcf.5/',
    docstore: '/EY.com.eBook.API.5/DocstoreService.svc/',
    annualAccount: '/EY.com.eBook.API.5/AnnualAccountService.svc/'
};

// DEVELOPMENT VERSION
/*
 eBook.Service = {
 url: '/EY.com.eBook.API.5.DEV/EbookService.svc/'
 };
 */
