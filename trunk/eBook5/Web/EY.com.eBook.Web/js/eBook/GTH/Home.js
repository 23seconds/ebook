

//eBook.GTH.ToDoList = Ext.extend(Ext.list.ListView, {

/*

eBook.GTH.ClientSelector = Ext.extend(Ext.Panel, {
initComponent: function() {
Ext.apply(this, {
layout: {
type: 'vbox',
// padding:'5',
align: 'stretch'
}
, items: [{ xtype: 'panel', border: false,bodyStyle:'text-align:center;padding:10px;', items: [{ xtype: 'button',width:400,style:'margin:auto;font-weight:bold', text: 'Select first available client', handler: this.selectFirstAvailable}], region: 'north', height: 60 }
, { xtype: 'panel', border: false, flex: 1, html: 'fjezifjezofjezo jezo fjezofjez f', region: 'center' }
]
});
eBook.GTH.ClientSelector.superclass.initComponent.apply(this, arguments);
}
});

Ext.reg('GTHClientSelector', eBook.GTH.ClientSelector);
*/



eBook.GTH.Home = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'hbox'
            , layoutConfig: {
                padding: '5',
                defaultMargins: { top: 0, left: 0, right: 5, bottom: 0 },
                align: 'stretch'
            }
            , cls: 'gth-home'
            , items: [
                { xtype: 'TeamExecutor', title: 'Team 1 <br/> Statutory Accounts - NBB Filing', flex: 1
                    , teamId: 1
                    , steps: [
                            {
                                stepId: 0,
                                xtype: 'GTHClientSelector',
                                stepTitle: 'STEP 1: Select a client',
                                selectStatus: 'INPROGRESS'
                            }, {
                                stepId: 1,
                                xtype: 'GTHTeam1UploadFiles',
                                stepTitle: 'STEP 2: Download files',
                                selectStatus: 'INPROGRESS'
}]
                }
                , { xtype: 'TeamExecutor', teamId: 2, title: 'Team 2 <br/> Statutory Accounts - Supporting Documents', flex: 1
                    , steps: [
                            {
                                stepId: 0,
                                xtype: 'GTHClientSelector',
                                stepTitle: 'STEP 1: Select a client',
                                selectStatus: 'INPROGRESS',
                                statusList: []
                            }, {
                                stepId: 1,
                                xtype: 'GTHTeam2Process',
                                stepTitle: 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel',
                                selectStatus: 'INPROGRESS'
}]
                }
               , { xtype: 'TeamExecutor', teamId: 3, title: 'Team 3 <br/> Statutory Accounts - Reconciliation', flex: 1 }
               , { xtype: 'TeamExecutor', teamId: 4, title: 'Team 4 <br/> Financial Reporting - Financial Health Analysis', flex: 1
                    , steps: [
                    {
                        stepId: 0,
                        xtype: 'GTHClientSelector',
                        stepTitle: 'STEP 1: Select a client',
                        selectStatus: 'INPROGRESS',
                        statusList: []
                    }, {
                        stepId: 1,
                        xtype: 'GTHTeam4Process',
                        stepTitle: 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel',
                        selectStatus: 'INPROGRESS'
                    }]
            }
                , { xtype: 'TeamExecutor', teamId: 5, title: 'Team 5 <br/> Filing annual accounts', flex: 1 , steps: [
                            {
                                stepId: 0,
                                xtype: 'GTHTeam5',
                                stepTitle: 'STEP 1: Select a client',
                                selectStatus: 'INPROGRESS'
                            }]}
                , { xtype: 'TeamExecutor', teamId: 6, title: 'Team 6 <br/> Engagement Agreement', flex: 1 
                    , steps: [
                            {
                                stepId: 0,
                                xtype: 'GTHClientSelector',
                                stepTitle: 'STEP 1: Select a client',
                                selectStatus: 'INPROGRESS',
                                statusList: []
                                //searchButton:false,
                                //getAllClients: true
                            }, {
                                stepId: 1,
                                xtype: 'GTHTeam6Process',
                                stepTitle: 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel',
                                selectStatus: 'INPROGRESS'
}]
                }
            ]
            });
            
        eBook.GTH.Home.superclass.initComponent.apply(this, arguments);
    }
   , startUpdates: function() {
       if (this.rendered) this.items.each(function(it) { it.startUpdateTask(); }, this);
   }
   , stopUpdates: function() {
       if (this.rendered) this.items.each(function(it) { it.stopUpdateTask(); }, this);
   }
   , startItem: function(index) {
       var items = this.items.items;
       for (var i = 0; i < items.length; i++) {
           if (i != index) {
               items[i].el.setStyle('opacity', '0');
               items[i].el.setStyle.defer(500, items[i].el, ['display', 'none']);
           }
       }
       items[index].startWork.defer(500, items[index]);
   }
   , stopItem: function(index) {
       var items = this.items.items;
       for (var i = 0; i < items.length; i++) {
           if (i != index) {
               items[i].el.setStyle('display', 'block');
               items[i].el.setStyle('opacity', '1');

           }
       }
       //items[index].startWork.defer(500, items[index]);
   }
});

Ext.reg('gthhome', eBook.GTH.Home);

