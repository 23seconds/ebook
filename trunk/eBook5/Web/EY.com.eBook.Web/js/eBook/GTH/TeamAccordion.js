/* 'beforeload': {
                                fn: this.maskLists
                                , scope: this
                            }
                            ,
*/

eBook.GTH.ClientList = Ext.extend(Ext.Panel, {
    initComponent: function() {
        var pers = '';
        if (this.statusType != 'TODO') {
            pers = '<div class="eb-gth-client-actor">{[this.getLastUpdate(values.LastUpdate)]}: {PersonName}</div>';
        }
        this.template = new Ext.XTemplate(
                '<tpl for=".">',
                    '<div class="eb-gth-client">',
                    '<div>{ClientName}',
                    '<tpl if="values.StartDate!=null && values.EndDate!=null "><br/><i>{[this.getPeriod(values)]}</i></tpl>',
                    '</div>',
                    pers,
                    '</div>',
                '</tpl>',
                '<div class="x-clear"></div>', {
                        getLastUpdate: function(value) { var dte = Ext.data.Types.WCFDATE.convert(value); return dte.format("d/m/Y H:i:s"); }
                        ,getPeriod:function(values) { var st = Ext.data.Types.WCFDATE.convert(values.StartDate), ed=Ext.data.Types.WCFDATE.convert(values.EndDate);return st.format("d/m/Y") + ' - ' +  ed.format("d/m/Y");}
                }
            );
        this.template.compile();
        Ext.apply(this, {
            html: '<div class="eb-gth-client-list"></div>'
            , title: this.baseTitle
            , autoScroll: true
            , bodyStyle:'background-color:#FFF;'
        });
        eBook.GTH.ClientList.superclass.initComponent.apply(this, arguments);
    }
    , onRender: function(ct, position) {
        eBook.GTH.ClientList.superclass.onRender.apply(this, arguments);
        //        if (this.ownerCt.store && this.ownerCt.store.getCount() > 0) {
        //            
        //        }
        this.dataEl = this.el.child('eb-gth-client-list');
        if (this.startDta) {
            this.updateList(this.startDta);
        }
    }
    , updateList: function(dta) {
        if (this.rendered) {
            this.startDta = null;
            this.template.overwrite(this.body, dta, true);
            this.setTitle(this.baseTitle + ' (' + dta.length + ')');
        } else {
            this.startDta = dta;
        }
    }
});

Ext.reg('GTHClientList', eBook.GTH.ClientList);

eBook.GTH.TeamAccordion = Ext.extend(Ext.Panel, {
    initComponent: function() {
        var accordionItems = [{ xtype: 'GTHClientList', baseTitle: 'Todo', teamId: this.teamId, statusType: 'TODO', ref: 'todos' }
                             , { xtype: 'GTHClientList', baseTitle: 'In Progress', teamId: this.teamId, statusType: 'OTHER', ref: 'progress' }
                             , { xtype: 'GTHClientList', baseTitle: 'Finalized', teamId: this.teamId, statusType: 'FINAL', ref: 'finals'}];
        /*if (this.teamId == 6) {
            accordionItems = [{ xtype: 'GTHClientList', baseTitle: 'Clients', teamId: this.teamId, statusType: 'TODO', ref: 'todos'}];
        }*/

        Ext.apply(this, {
            layout: 'accordion',
            border: false,
            bodyStyle: 'background-color:#FFF;',
            items: accordionItems
        });
        //        this.store = new eBook.data.JsonStore({
        //            selectAction: 'GetClientList'
        //                        , serviceUrl: eBook.Service.GTH
        //                        , autoLoad: true
        //                        , autoDestroy: true
        //                        , criteriaParameter: 'ctdc'
        //                        , baseParams: { TeamId: this.teamId, StatusList: [] }
        //                        , listeners: {
        //                            'load': {
        //                                fn: this.updateLists
        //                                , scope: this
        //                            }
        //                        }
        //                        , fields: eBook.data.RecordTypes.GTHClientsList
        //        });
        eBook.GTH.TeamAccordion.superclass.initComponent.apply(this, arguments);

    }
    , updateLists: function() {

        this.loadData();
    }
    , afterRender: function(ct) {
        eBook.GTH.TeamAccordion.superclass.afterRender.apply(this, arguments);
        this.loadData();
    }
    , loadData: function() {
        if (this.pollTask) {
            //Ext.TaskMgr.stop(this.pollTask);
        }
        if (this.el.parent('.eb-gth-team-container').isVisible()) {
            this.getEl().mask('loading');
            Ext.Ajax.request({
                url: eBook.Service.GTH + 'GetClientStartList'
                , method: 'POST'
                , params: Ext.encode({ ctdc: { TeamId: this.teamId} })
                , callback: this.onLoadDataCallback
                , scope: this

            });
        }
    }
    , onLoadDataCallback: function(opts, success, resp) {
        if (success) {
            var dta = Ext.decode(resp.responseText);
            dta = dta.GetClientStartListResult;
            this.todos.updateList(dta.Todo);
            /*if (this.teamId != 6) {*/
                this.progress.updateList(dta.Progress);
                this.finals.updateList(dta.Final);
            /*}*/
            
        } else {
            this.todos.updateList([{ ClientName: 'test'}]);
            this.progress.updateList([]);
            this.finals.updateList([]);
        }
        this.getEl().unmask();
        //var def = parseInt(Math.random() * 10000);
        //alert(def);
        this.startUpdateTask();
    }
    , stopUpdateTask: function() {
        if (this.pollTask) {
            Ext.TaskMgr.stop(this.pollTask);
            this.pollTask = null;
        }
    }
    , startUpdateTask: function() {
        if (!this.pollTask) this.pollTask = Ext.TaskMgr.start({ run: this.loadData, scope: this, interval: 30000 });  //.defer(def, [this.pollTask]);

    }
});

Ext.reg('GTHTeamAccordion', eBook.GTH.TeamAccordion);