eBook.CleanEnterprise = function(v) {
    if (Ext.isEmpty(v) || Ext.isEmpty(v.trim())) return 'NULL';
    v = v.replace('BE', '');
    v = v.replace(/^0+/, '');
    //0xxx.xxx.xxx
    while (v.length < 10) {
        v = '0' + v;
    }
    return v;
};

eBook.GTH.ClientSearch = Ext.extend(eBook.grid.PagedGridPanel, {
    statusList: ['TODO']
    //, getAllClients : false
    , initComponent: function() {
        var storeCfg = {
                        selectAction: 'GetClients', /*'GetAllClients',*/
                        fields: eBook.data.RecordTypes.GTHClientsList,
                        idField: 'Id',
                        criteriaParameter: 'cstdc',
                        baseParams: { TeamId: this.teamId, statusList: this.statusList, PersonId: eBook.User.personId },
                        serviceUrl: eBook.Service.GTH
                       };
        if (this.getAllClients){
            storeCfg = {
                        selectAction: 'GetAllClients', /*'GetAllClients',*/
                        fields: eBook.data.RecordTypes.GTHClientsList,
                        idField: 'Id',
                        criteriaParameter: 'cstdc',
                        baseParams: { TeamId: this.teamId, statusList: this.statusList, PersonId: eBook.User.personId },
                        serviceUrl: eBook.Service.GTH
                       };        
        } 
        
        Ext.apply(this, {
            title: "Search Client"
            , storeConfig: storeCfg
            , sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
            })
            , border: true
            //,bodyStyle:'margin-top:10px;'
            , viewConfig: { forceResize: true }
            , listeners: {
                'rowmousedown': {
                    fn: function(grid, rowIndex, e) {
                        var rec = grid.store.getAt(rowIndex);
                        this.selectClient(rec);
                    },
                    scope: this
                }
            }
            , tbar: []
            , pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            }
            , plugins: [new Ext.ux.grid.Search({
                ref: 'searchfield',
                iconCls: 'eBook-search-ico',
                minChars: 3,
                minLength: 3,
                autoFocus: true,
                position: 'top',
                mode: 'remote',
                width: 250,
                searchText: "Search Client",
                searchTipText: '',
                selectAllText: "Select all fields"
            })]
            , loadMask: true
			, columns: [
			    {
			        header: 'Client Name',
			        width: 300,
			        dataIndex: 'ClientName',
			        sortable: true
			    },
			    {
			        header: 'Company Nr',
			        width: 120,
			        dataIndex: 'ClientEnterprise',
			        renderer: function(v, m, rec, ridx, cidx, str) {
			            return eBook.CleanEnterprise(v);
			        },
			        sortable: true
			    }, {
			        header: 'Period',
			        width: 300,
			        dataIndex: 'StartDate',
			        renderer: function(v, m, rec, ridx, cidx, str) {
			            if(v && rec.get('EndDate')) {
			                return rec.get('StartDate').format('d/m/Y') + ' - ' + rec.get('EndDate').format('d/m/Y');
			            } 
			            return '';
			        }
			    }, {
			        header: 'Client GFIS',
			        width: 300,
			        dataIndex: 'ClientGfis',
			        sortable: true
}]
            , border: false
            , style: 'margin-top:10px'
        });
        eBook.GTH.ClientSearch.superclass.initComponent.apply(this, arguments);
    }
    , selectClient: function(rec) {
        this.ownerCt.selectClient(rec.get('Id'));
    }
});
Ext.reg('GTHClientSearch', eBook.GTH.ClientSearch);


eBook.GTH.ClientSelector = Ext.extend(Ext.Panel, {
searchButton: true
    ,getAllClients:false
    , initComponent: function() {
        var srchBtn = {};
        if (this.searchButton) {
            srchBtn = { xtype: 'panel', border: false, bodyStyle: 'text-align:center;padding:10px;', items: [{ xtype: 'button', ref: 'btnSrch', width: 400, style: 'margin:auto;font-weight:bold', text: 'Select first available client', handler: function() { this.selectClient(null); }, scope: this}], region: 'north', height: 60 }
        }
        Ext.apply(this, {
            layout: 'border'
            , items: [ srchBtn
                      , { xtype: 'GTHClientSearch', getAllClients:this.getAllClients,teamId: this.teamId, ref: 'srch', region: 'center' }
                      , { xtype: 'spacer', region: 'south', height: 20 }
                      ]
        });
        eBook.GTH.ClientSelector.superclass.initComponent.apply(this, arguments);
    }
    , selectClient: function(id) {
        this.getEl().mask('loading');
        Ext.Ajax.request({
            url: eBook.Service.GTH + 'SelectClient'
                    , method: 'POST'
                    , params: Ext.encode({ cctdc: { TeamId: this.teamId, GTHId: Ext.isEmpty(id) ? null : id, Status: this.selectStatus, PersonId: eBook.User.personId} })
                    , callback: this.onSelectClientCallback
                    , scope: this

        });
    }
    , onSelectClientCallback: function(opts, success, resp) {
        if (success) {
            var obj = Ext.decode(resp.responseText).SelectClientResult;
            this.ownerCt.selectClient(obj, this.stepId);
        } else {
            eBook.Interface.showResponseError(resp, "Select client");
        }
        this.getEl().unmask();
    }
    , initStep: function(owner) {
        owner.client = null;

        owner.showStop();
        this.srch.store.load();
    }
});


Ext.reg('GTHClientSelector', eBook.GTH.ClientSelector);