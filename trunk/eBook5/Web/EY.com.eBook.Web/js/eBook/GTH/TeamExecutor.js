eBook.GTH.TeamExecutor = Ext.extend(Ext.Container, {

    baseCls: 'x-panel',
    collapsedCls: 'x-panel-collapsed',
    maskDisabled: true,
    animCollapse: Ext.enableFx,
    headerAsText: true,
    buttonAlign: 'right',
    collapsed: false,
    collapseFirst: true,
    minButtonWidth: 75,
    preventBodyReset: false,
    padding: undefined,
    resizeEvent: 'bodyresize',
    toolTarget: 'header',
    collapseEl: 'bwrap',
    slideAnchor: 't',
    disabledClass: '',
    deferHeight: true,
    expandDefaults: {
        duration: 0.25
    },
    // private
    collapseDefaults: {
        duration: 0.25
    },

    // private
    initComponent: function() {
        eBook.GTH.TeamExecutor.superclass.initComponent.call(this);
        //        Ext.Panel.superclass.initComponent.call(this);

        //        this.addEvents(
        //            'bodyresize',
        //            'titlechange',
        //            'iconchange'
        //            'collapse',
        //            'expand',
        //            'beforecollapse',
        //            'beforeexpand',
        //            'beforeclose',
        //            'close',
        //            'activate',
        //            'deactivate'
        //        );

        //        if (this.unstyled) {
        //            this.baseCls = 'x-plain';
        //        }


        //        this.toolbars = [];
        //        // shortcuts
        //        if (this.tbar) {
        //            this.elements += ',tbar';
        //            this.topToolbar = this.createToolbar(this.tbar);
        //            this.tbar = null;

        //        }
        //        if (this.bbar) {
        //            this.elements += ',bbar';
        //            this.bottomToolbar = this.createToolbar(this.bbar);
        //            this.bbar = null;
        //        }

        //        if (this.header === true) {
        //            this.elements += ',header';
        //            this.header = null;
        //        } else if (this.headerCfg || (this.title && this.header !== false)) {
        //            this.elements += ',header';
        //        }

        //        if (this.footerCfg || this.footer === true) {
        //            this.elements += ',footer';
        //            this.footer = null;
        //        }

        //        if (this.buttons) {
        //            this.fbar = this.buttons;
        //            this.buttons = null;
        //        }
        //        if (this.fbar) {
        //            this.createFbar(this.fbar);
        //        }
        //        if (this.autoLoad) {
        //            this.on('render', this.doAutoLoad, this, { delay: 10 });
        //        }
    },

    // private
    createFbar: function(fbar) {
        var min = this.minButtonWidth;
        this.elements += ',footer';
        this.fbar = this.createToolbar(fbar, {
            buttonAlign: this.buttonAlign,
            toolbarCls: 'x-panel-fbar',
            enableOverflow: false,
            defaults: function(c) {
                return {
                    minWidth: c.minWidth || min
                };
            }
        });
        // @compat addButton and buttons could possibly be removed
        // @target 4.0
        /**
        * This Panel's Array of buttons as created from the <code>{@link #buttons}</code>
        * config property. Read only.
        * @type Array
        * @property buttons
        */
        this.fbar.items.each(function(c) {
            c.minWidth = c.minWidth || this.minButtonWidth;
        }, this);
        this.buttons = this.fbar.items.items;
    },

    // private
    createToolbar: function(tb, options) {
        var result;
        // Convert array to proper toolbar config
        if (Ext.isArray(tb)) {
            tb = {
                items: tb
            };
        }
        result = tb.events ? Ext.apply(tb, options) : this.createComponent(Ext.apply({}, tb, options), 'toolbar');
        this.toolbars.push(result);
        return result;
    },

    // private
    createElement: function(name, pnode) {
        if (this[name]) {
            pnode.appendChild(this[name].dom);
            return;
        }

        if (name === 'bwrap' || this.elements.indexOf(name) != -1) {
            if (this[name + 'Cfg']) {
                this[name] = Ext.fly(pnode).createChild(this[name + 'Cfg']);
            } else {
                var el = document.createElement('div');
                el.className = this[name + 'Cls'];
                this[name] = Ext.get(pnode.appendChild(el));
            }
            if (this[name + 'CssClass']) {
                this[name].addClass(this[name + 'CssClass']);
            }
            if (this[name + 'Style']) {
                this[name].applyStyles(this[name + 'Style']);
            }
        }
    },
    workpanelRendered: false,
    workpanelRender: function(w, h) {
        if (!this.workpanelRendered && this.workPanelEl) {
            // render step 1
            if (this.steps) {
                this.steps[0] = this.stepRender(this.steps[0], w, h);
                this.activeStep = 0;
                this.setTitle(this.steps[0].stepTitle);
            }
            this.workpanelRendered = true;
        }
    },
    moveNextStep: function() {


        var st = this.activeStep;

        var p = this.steps[st].el.parent();
        p.setVisibilityMode(Ext.Element.DISPLAY);
        p.hide();
        if (st + 1 < this.steps.length) {
            st++;
        } else {
            st = 0;
        }
        var sz = this.workPanelEl.getSize();
        var sh = this.steps[st].rendered;
        this.steps[st] = this.stepRender(this.steps[st], sz.width - 41, sz.height);
        if (sh) {
            p = this.steps[st].el.parent();
            p.setVisibilityMode(Ext.Element.DISPLAY);
            p.show();
        }
        this.steps[st].initStep(this);
        this.setTitle(this.steps[st].stepTitle);
        this.activeStep = st;

    },
    stepRender: function(stepCfg, w, h) {
        if (!stepCfg.rendered) {
            var dv = document.createElement('div');
            dv.className = 'eb-gth-team-workpane-step';

            var wpBody = this.workPanelEl.child('.eb-gth-workpane-body');
            wpBody.appendChild(dv);
            stepCfg.renderTo = dv;
            stepCfg.teamId = this.teamId;
            stepCfg = this.createComponent(stepCfg);
            stepCfg.ownerCt = this;
            //stepCfg.render(dv);
            // stepCfg.rendered = true;
            //stepCfg.el = Ext.get(dv);
            //stepCfg.el.setHeight(wpBody.getHeight());
            stepCfg.setSize(wpBody.getWidth() - 10, wpBody.getHeight());
        }
        return stepCfg;
    },

    // private
    onRender: function(ct, position) {
        if (!this.el) {
            var containingDiv = document.createElement('div');
            containingDiv.className = 'eb-gth-team-container';
            var body = document.createElement('div');
            body.className = 'eb-gth-team-body';
            var listDiv = document.createElement('div');
            listDiv.className = 'eb-gth-team-todo-list';
            var workpane = document.createElement('div');
            workpane.className = 'eb-gth-team-workpane';
            body.appendChild(listDiv);
            body.appendChild(workpane);
            body.id = Ext.id();
            containingDiv.appendChild(body);
            containingDiv.id = Ext.id();
            this.el = Ext.get(containingDiv);
            this.body = Ext.get(body);
            this.listEl = Ext.get(listDiv);
            this.workPanelEl = Ext.get(workpane);

            if (this.title) {
                var listTitle = document.createElement('div');
                listTitle.className = 'eb-gth-todo-title';
                listTitle.innerHTML = this.title;
                this.listEl.appendChild(listTitle);
            }
            var listBody = document.createElement('div');
            listBody.className = 'eb-gth-todo-body';

            this.activeTeam = false;
            if (eBook.User.isExistingRole('GTH Team ' + this.teamId)) {
                var startBtn = document.createElement('div');
                startBtn.className = 'eb-gth-team-startbtn';
                startBtn.innerHTML = 'START WORK';
                listBody.appendChild(startBtn);
                this.activeTeam = true;
            }

            var accord = document.createElement('div');
            accord.className = 'eb-gth-team-accordion';
            listBody.appendChild(accord);

            this.listEl.appendChild(listBody);
            this.accordionEl = Ext.get(accord);
            if (this.activeTeam) this.startEl = Ext.get(startBtn);

            var wpTitle = document.createElement('div');
            wpTitle.className = 'eb-gth-workpane-title';
            wpTitle.innerHTML = 'Klantnaam - ondernemingsnummer - huidige stap';
            var wpBody = document.createElement('div');
            wpBody.className = 'eb-gth-workpane-body';
            this.workPanelEl.appendChild(wpTitle);
            this.workPanelEl.appendChild(wpBody);

            this.workpanelRendered = false;


            this.accordion = new eBook.GTH.TeamAccordion({ teamId: this.teamId, renderTo: this.accordionEl });
            /*{ border: false, renderTo: this.accordionEl,
            /*layout: {
            type:'vbox',
            // padding:'5',
            align:'stretch'
            }**
            layout:'accordion'
            , items: [{ xtype: 'panel', border: false, title: 'test 1', flex:1,html: 'test' }, { xtype: 'panel',flex:1, border: false, title: 'test 2', html: 'test'}] });
            */


        }

        this.workPanelEl.setStyle('display', 'none');

        //        if(!this.el && this.autoEl){
        //            if(Ext.isString(this.autoEl)){
        //                this.el = document.createElement(this.autoEl);
        //            }else{
        //                var div = document.createElement('div');
        //                Ext.DomHelper.overwrite(div, this.autoEl);
        //                this.el = div.firstChild;
        //            }
        //            if (!this.el.id) {
        //                this.el.id = this.getId();
        //            }
        //        }
        //        if(this.el){
        //            this.el = Ext.get(this.el);
        //            if(this.allowDomMove !== false){
        //                ct.dom.insertBefore(this.el.dom, position);
        //                if (div) {
        //                    Ext.removeNode(div);
        //                    div = null;
        //                }
        //            }
        //        }

        //        Ext.Panel.superclass.onRender.call(this, ct, position);
        //        this.createClasses();

        //        var el = this.el,
        //            d = el.dom,
        //            bw,
        //            ts;


        //        if (this.collapsible && !this.hideCollapseTool) {
        //            this.tools = this.tools ? this.tools.slice(0) : [];
        //            this.tools[this.collapseFirst ? 'unshift' : 'push']({
        //                id: 'toggle',
        //                handler: this.toggleCollapse,
        //                scope: this
        //            });
        //        }

        //        if (this.tools) {
        //            ts = this.tools;
        //            this.elements += (this.header !== false) ? ',header' : '';
        //        }
        //        this.tools = {};

        //        el.addClass(this.baseCls);
        //        if (d.firstChild) { // existing markup
        //            this.header = el.down('.' + this.headerCls);
        //            this.bwrap = el.down('.' + this.bwrapCls);
        //            var cp = this.bwrap ? this.bwrap : el;
        //            this.tbar = cp.down('.' + this.tbarCls);
        //            this.body = cp.down('.' + this.bodyCls);
        //            this.bbar = cp.down('.' + this.bbarCls);
        //            this.footer = cp.down('.' + this.footerCls);
        //            this.fromMarkup = true;
        //        }
        //        if (this.preventBodyReset === true) {
        //            el.addClass('x-panel-reset');
        //        }
        //        if (this.cls) {
        //            el.addClass(this.cls);
        //        }

        //        if (this.buttons) {
        //            this.elements += ',footer';
        //        }

        //        // This block allows for maximum flexibility and performance when using existing markup

        //        // framing requires special markup
        //        if (this.frame) {
        //            el.insertHtml('afterBegin', String.format(Ext.Element.boxMarkup, this.baseCls));

        //            this.createElement('header', d.firstChild.firstChild.firstChild);
        //            this.createElement('bwrap', d);

        //            // append the mid and bottom frame to the bwrap
        //            bw = this.bwrap.dom;
        //            var ml = d.childNodes[1], bl = d.childNodes[2];
        //            bw.appendChild(ml);
        //            bw.appendChild(bl);

        //            var mc = bw.firstChild.firstChild.firstChild;
        //            this.createElement('tbar', mc);
        //            this.createElement('body', mc);
        //            this.createElement('bbar', mc);
        //            this.createElement('footer', bw.lastChild.firstChild.firstChild);

        //            if (!this.footer) {
        //                this.bwrap.dom.lastChild.className += ' x-panel-nofooter';
        //            }
        //            /*
        //            * Store a reference to this element so:
        //            * a) We aren't looking it up all the time
        //            * b) The last element is reported incorrectly when using a loadmask
        //            */
        //            this.ft = Ext.get(this.bwrap.dom.lastChild);
        //            this.mc = Ext.get(mc);
        //        } else {
        //            this.createElement('header', d);
        //            this.createElement('bwrap', d);

        //            // append the mid and bottom frame to the bwrap
        //            bw = this.bwrap.dom;
        //            this.createElement('tbar', bw);
        //            this.createElement('body', bw);
        //            this.createElement('bbar', bw);
        //            this.createElement('footer', bw);

        //            if (!this.header) {
        //                this.body.addClass(this.bodyCls + '-noheader');
        //                if (this.tbar) {
        //                    this.tbar.addClass(this.tbarCls + '-noheader');
        //                }
        //            }
        //        }

        //        if (Ext.isDefined(this.padding)) {
        //            this.body.setStyle('padding', this.body.addUnits(this.padding));
        //        }

        //        if (this.border === false) {
        //            this.el.addClass(this.baseCls + '-noborder');
        //            this.body.addClass(this.bodyCls + '-noborder');
        //            if (this.header) {
        //                this.header.addClass(this.headerCls + '-noborder');
        //            }
        //            if (this.footer) {
        //                this.footer.addClass(this.footerCls + '-noborder');
        //            }
        //            if (this.tbar) {
        //                this.tbar.addClass(this.tbarCls + '-noborder');
        //            }
        //            if (this.bbar) {
        //                this.bbar.addClass(this.bbarCls + '-noborder');
        //            }
        //        }

        //        if (this.bodyBorder === false) {
        //            this.body.addClass(this.bodyCls + '-noborder');
        //        }

        //        this.bwrap.enableDisplayMode('block');

        //        if (this.header) {
        //            this.header.unselectable();

        //            // for tools, we need to wrap any existing header markup
        //            if (this.headerAsText) {
        //                this.header.dom.innerHTML =
        //                    '<span class="' + this.headerTextCls + '">' + this.header.dom.innerHTML + '</span>';

        //                if (this.iconCls) {
        //                    this.setIconClass(this.iconCls);
        //                }
        //            }
        //        }

        //        if (this.floating) {
        //            this.makeFloating(this.floating);
        //        }

        //        if (this.collapsible && this.titleCollapse && this.header) {
        //            this.mon(this.header, 'click', this.toggleCollapse, this);
        //            this.header.setStyle('cursor', 'pointer');
        //        }
        //        if (ts) {
        //            this.addTool.apply(this, ts);
        //        }

        //        // Render Toolbars.
        //        if (this.fbar) {
        //            this.footer.addClass('x-panel-btns');
        //            this.fbar.ownerCt = this;
        //            this.fbar.render(this.footer);
        //            this.footer.createChild({ cls: 'x-clear' });
        //        }
        //        if (this.tbar && this.topToolbar) {
        //            this.topToolbar.ownerCt = this;
        //            this.topToolbar.render(this.tbar);
        //        }
        //        if (this.bbar && this.bottomToolbar) {
        //            this.bottomToolbar.ownerCt = this;
        //            this.bottomToolbar.render(this.bbar);
        //        }
    },

    /**
    * Sets the CSS class that provides the icon image for this panel.  This method will replace any existing
    * icon class if one has already been set and fire the {@link #iconchange} event after completion.
    * @param {String} cls The new CSS class name
    */
    setIconClass: function(cls) {
        var old = this.iconCls;
        this.iconCls = cls;
        if (this.rendered && this.header) {
            if (this.frame) {
                this.header.addClass('x-panel-icon');
                this.header.replaceClass(old, this.iconCls);
            } else {
                var hd = this.header,
                    img = hd.child('img.x-panel-inline-icon');
                if (img) {
                    Ext.fly(img).replaceClass(old, this.iconCls);
                } else {
                    var hdspan = hd.child('span.' + this.headerTextCls);
                    if (hdspan) {
                        Ext.DomHelper.insertBefore(hdspan.dom, {
                            tag: 'img', src: Ext.BLANK_IMAGE_URL, cls: 'x-panel-inline-icon ' + this.iconCls
                        });
                    }
                }
            }
        }
        this.fireEvent('iconchange', this, cls, old);
    },

    // private
    makeFloating: function(cfg) {
        this.floating = true;
        this.el = new Ext.Layer(Ext.apply({}, cfg, {
            shadow: Ext.isDefined(this.shadow) ? this.shadow : 'sides',
            shadowOffset: this.shadowOffset,
            constrain: false,
            shim: this.shim === false ? false : undefined
        }), this.el);
    },

    /**
    * Returns the {@link Ext.Toolbar toolbar} from the top (<code>{@link #tbar}</code>) section of the panel.
    * @return {Ext.Toolbar} The toolbar
    */
    getTopToolbar: function() {
        return this.topToolbar;
    },

    /**
    * Returns the {@link Ext.Toolbar toolbar} from the bottom (<code>{@link #bbar}</code>) section of the panel.
    * @return {Ext.Toolbar} The toolbar
    */
    getBottomToolbar: function() {
        return this.bottomToolbar;
    },

    /**
    * Returns the {@link Ext.Toolbar toolbar} from the footer (<code>{@link #fbar}</code>) section of the panel.
    * @return {Ext.Toolbar} The toolbar
    */
    getFooterToolbar: function() {
        return this.fbar;
    },

    /**
    * Adds a button to this panel.  Note that this method must be called prior to rendering.  The preferred
    * approach is to add buttons via the {@link #buttons} config.
    * @param {String/Object} config A valid {@link Ext.Button} config.  A string will become the text for a default
    * button config, an object will be treated as a button config object.
    * @param {Function} handler The function to be called on button {@link Ext.Button#click}
    * @param {Object} scope The scope (<code>this</code> reference) in which the button handler function is executed. Defaults to the Button.
    * @return {Ext.Button} The button that was added
    */
    addButton: function(config, handler, scope) {
        if (!this.fbar) {
            this.createFbar([]);
        }
        if (handler) {
            if (Ext.isString(config)) {
                config = { text: config };
            }
            config = Ext.apply({
                handler: handler,
                scope: scope
            }, config);
        }
        return this.fbar.add(config);
    },

    // private
    addTool: function() {
        if (!this.rendered) {
            if (!this.tools) {
                this.tools = [];
            }
            Ext.each(arguments, function(arg) {
                this.tools.push(arg);
            }, this);
            return;
        }
        // nowhere to render tools!
        if (!this[this.toolTarget]) {
            return;
        }
        if (!this.toolTemplate) {
            // initialize the global tool template on first use
            var tt = new Ext.Template(
                 '<div class="x-tool x-tool-{id}">&#160;</div>'
            );
            tt.disableFormats = true;
            tt.compile();
            eBook.GTH.TeamExecutor.prototype.toolTemplate = tt;
        }
        for (var i = 0, a = arguments, len = a.length; i < len; i++) {
            var tc = a[i];
            if (!this.tools[tc.id]) {
                var overCls = 'x-tool-' + tc.id + '-over';
                var t = this.toolTemplate.insertFirst(this[this.toolTarget], tc, true);
                this.tools[tc.id] = t;
                t.enableDisplayMode('block');
                this.mon(t, 'click', this.createToolHandler(t, tc, overCls, this));
                if (tc.on) {
                    this.mon(t, tc.on);
                }
                if (tc.hidden) {
                    t.hide();
                }
                if (tc.qtip) {
                    if (Ext.isObject(tc.qtip)) {
                        Ext.QuickTips.register(Ext.apply({
                            target: t.id
                        }, tc.qtip));
                    } else {
                        t.dom.qtip = tc.qtip;
                    }
                }
                t.addClassOnOver(overCls);
            }
        }
    },

    onLayout: function(shallow, force) {
        eBook.GTH.TeamExecutor.superclass.onLayout.apply(this, arguments);
        //        if (this.hasLayout && this.toolbars.length > 0) {
        //            Ext.each(this.toolbars, function(tb) {
        //                tb.doLayout(undefined, force);
        //            });
        //            this.syncHeight();
        //        }
    },

    syncHeight: function() {
        var h = this.toolbarHeight,
                bd = this.body,
                lsh = this.lastSize.height,
                sz;

        if (this.autoHeight || !Ext.isDefined(lsh) || lsh == 'auto') {
            return;
        }


        if (h != this.getToolbarHeight()) {
            h = Math.max(0, lsh - this.getFrameHeight());
            bd.setHeight(h);
            sz = bd.getSize();
            this.toolbarHeight = this.getToolbarHeight();
            this.onBodyResize(sz.width, sz.height);
        }
    },

    // private
    onShow: function() {
        if (this.floating) {
            return this.el.show();
        }
        eBook.GTH.TeamExecutor.superclass.onShow.call(this);
    },

    // private
    onHide: function() {
        if (this.floating) {
            return this.el.hide();
        }
        eBook.GTH.TeamExecutor.superclass.onHide.call(this);
    },

    // private
    createToolHandler: function(t, tc, overCls, panel) {
        return function(e) {
            t.removeClass(overCls);
            if (tc.stopEvent !== false) {
                e.stopEvent();
            }
            if (tc.handler) {
                tc.handler.call(tc.scope || t, e, t, panel, tc);
            }
        };
    },
    hideStop: function() {
        if (this.startEl) {
            this.startEl.setVisibilityMode(Ext.Element.DISPLAY);
            this.startEl.hide();
        }
    },
    showStop: function() {
        if (this.startEl) {
            this.startEl.setVisibilityMode(Ext.Element.DISPLAY);
            this.startEl.show();
        }
    },
    stopWork: function() {
        //this.el.setX(this.prevX);
        this.workPanelEl.setWidth(0);
        this.workPanelEl.setStyle.defer(1900, this.workPanelEl, ['display', 'none']);
        this.el.setX.defer(2000, this.el, [this.prevX]);
        this.ownerCt.stopItem.defer(2500, this.ownerCt, [this.ownerCt.items.indexOf(this)]);
        this.working = false;
        if (this.startEl) {
            this.startEl.update('START WORK');
            this.startEl.show();
        }
    },
    startWork: function() {
        if (!this.working) {
            this.working = true;
            if (this.startEl) this.startEl.update('STOP WORK');
            var listSz = this.listEl.getSize();
            var ownerCT = this.ownerCt.getSize(true);
            var w = ownerCT.width - listSz.width - 30;
            var h = listSz.height - 40;
            var lx = this.ownerCt.el.getLeft(false) + 8; //+this.ownerCt.el.getPadding('l') + this.body.getPadding('l');

            this.workSizes = {
                l: { x: lx + 5 }
                , wp: { h: listSz.height - 40, w: ownerCT.width - listSz.width - 20
                         , x: listSz.width//this.listEl.getComputedWidth()
                         , y: this.listEl.getY()
                }
                , gw: ownerCT.width - 20
            };
            this.workSizes.wpb = { h: this.workSizes.wp.h - 41, w: this.workSizes.wp.w};
            //this.workPanelEl.setWidth(w);
            this.workPanelEl.setHeight(this.workSizes.wp.h);
            wpBody = this.workPanelEl.child('.eb-gth-workpane-body');
            wpBody.setHeight(this.workSizes.wpb.h);
            wpBody.setWidth(this.workSizes.wpb.w);
            this.workpanelRender(this.workSizes.wp.w, this.workSizes.wp.h);
            this.prevX = this.el.getX();

            this.el.setX(lx);

            this.startWorkStep2.defer(500, this);
        } else {
            this.stopWork();
        }
        //this.workPanelEl.move('r', listSz.width + w, true);
    },
    startWorkStep2: function() {

        this.el.setWidth(this.workSizes.gw);
        this.body.setWidth(this.workSizes.gw);
        this.listEl.setStyle('position', 'absolute');


        this.workPanelEl.setStyle('-webkit-transition', 'none');
        this.workPanelEl.setStyle('-moz-transition', 'none');
        this.workPanelEl.setStyle('position', 'absolute');
        this.workPanelEl.setStyle('left', (this.workSizes.wp.x) + 'px');
        this.workPanelEl.setStyle('top', '20' + 'px');
        this.workPanelEl.setStyle('width', '0px');
        this.workPanelEl.setStyle('display', 'block');
        this.workPanelEl.setStyle('-moz-transition', 'all 2s ease-in-out');
        this.workPanelEl.setStyle('-webkit-transition', 'all 2s ease-in-out');
        this.workPanelEl.setStyle.defer(500, this.workPanelEl, ['width', this.workSizes.wp.w + 'px']);
        if (this.steps) {
            var st = this.activeStep;
            if (st != 0) {
                var p = this.steps[st].el.parent();
                p.setVisibilityMode(Ext.Element.DISPLAY);
                p.hide();

            }
            st = 0;
            var sh = this.steps[st].rendered;
            //this.steps[st] = this.stepRender(this.steps[st], sz.width - 41, sz.height);
            if (sh) {
                p = this.steps[st].el.parent();
                p.setVisibilityMode(Ext.Element.DISPLAY);
                p.show();
            }
            
            this.setTitle(this.steps[st].stepTitle);
            this.activeStep = st;

            this.steps[this.activeStep].setSize(this.workSizes.wpb.w, this.workSizes.wpb.h);
            if(this.steps[this.activeStep].initStep) {
                this.steps[this.activeStep].initStep(this);
            }
        }
    },
    // private
    afterRender: function() {
        //        if (this.floating && !this.hidden) {
        //            this.el.show();
        //        }
        //        if (this.title) {
        //            this.setTitle(this.title);
        //        }
        eBook.GTH.TeamExecutor.superclass.afterRender.call(this); // do sizing calcs last
        //        if (this.collapsed) {
        //            this.collapsed = false;
        //            this.collapse(false);
        //        }
        this.initEvents();
    },

    // private
    getKeyMap: function() {
        if (!this.keyMap) {
            this.keyMap = new Ext.KeyMap(this.el, this.keys);
        }
        return this.keyMap;
    },

    // private
    initEvents: function() {
        //        if (this.keys) {
        //            this.getKeyMap();
        //        }
        //        if (this.draggable) {
        //            this.initDraggable();
        //        }
        //        if (this.toolbars.length > 0) {
        //            Ext.each(this.toolbars, function(tb) {
        //                tb.doLayout();
        //                tb.on({
        //                    scope: this,
        //                    afterlayout: this.syncHeight,
        //                    remove: this.syncHeight
        //                });
        //            }, this);
        //            this.syncHeight();
        //        }
        if (this.startEl) {
            this.startEl.on('click', function() {
                this.ownerCt.startItem(this.ownerCt.items.indexOf(this));
            }, this);
        }

    },

    //    // private
    //    initDraggable: function() {
    //        /**
    //        * <p>If this Panel is configured {@link #draggable}, this property will contain
    //        * an instance of {@link Ext.dd.DragSource} which handles dragging the Panel.</p>
    //        * The developer must provide implementations of the abstract methods of {@link Ext.dd.DragSource}
    //        * in order to supply behaviour for each stage of the drag/drop process. See {@link #draggable}.
    //        * @type Ext.dd.DragSource.
    //        * @property dd
    //        */
    //        this.dd = new Ext.Panel.DD(this, Ext.isBoolean(this.draggable) ? null : this.draggable);
    //    },

    // private
    beforeEffect: function(anim) {
        if (this.floating) {
            this.el.beforeAction();
        }
        if (anim !== false) {
            this.el.addClass('x-panel-animated');
        }
    },

    // private
    afterEffect: function(anim) {
        this.syncShadow();
        this.el.removeClass('x-panel-animated');
    },

    // private - wraps up an animation param with internal callbacks
    createEffect: function(a, cb, scope) {
        var o = {
            scope: scope,
            block: true
        };
        if (a === true) {
            o.callback = cb;
            return o;
        } else if (!a.callback) {
            o.callback = cb;
        } else { // wrap it up
            o.callback = function() {
                cb.call(scope);
                Ext.callback(a.callback, a.scope);
            };
        }
        return Ext.applyIf(o, a);
    },

    /**
    * Collapses the panel body so that it becomes hidden.  Fires the {@link #beforecollapse} event which will
    * cancel the collapse action if it returns false.
    * @param {Boolean} animate True to animate the transition, else false (defaults to the value of the
    * {@link #animCollapse} panel config)
    * @return {Ext.Panel} this
    */
    collapse: function(animate) {
        if (this.collapsed || this.el.hasFxBlock() || this.fireEvent('beforecollapse', this, animate) === false) {
            return;
        }
        var doAnim = animate === true || (animate !== false && this.animCollapse);
        this.beforeEffect(doAnim);
        this.onCollapse(doAnim, animate);
        return this;
    },

    // private
    onCollapse: function(doAnim, animArg) {
        if (doAnim) {
            this[this.collapseEl].slideOut(this.slideAnchor,
                    Ext.apply(this.createEffect(animArg || true, this.afterCollapse, this),
                        this.collapseDefaults));
        } else {
            this[this.collapseEl].hide(this.hideMode);
            this.afterCollapse(false);
        }
    },

    // private
    afterCollapse: function(anim) {
        this.collapsed = true;
        this.el.addClass(this.collapsedCls);
        if (anim !== false) {
            this[this.collapseEl].hide(this.hideMode);
        }
        this.afterEffect(anim);

        // Reset lastSize of all sub-components so they KNOW they are in a collapsed container
        this.cascade(function(c) {
            if (c.lastSize) {
                c.lastSize = { width: undefined, height: undefined };
            }
        });
        this.fireEvent('collapse', this);
    },

    /**
    * Expands the panel body so that it becomes visible.  Fires the {@link #beforeexpand} event which will
    * cancel the expand action if it returns false.
    * @param {Boolean} animate True to animate the transition, else false (defaults to the value of the
    * {@link #animCollapse} panel config)
    * @return {Ext.Panel} this
    */
    expand: function(animate) {
        if (!this.collapsed || this.el.hasFxBlock() || this.fireEvent('beforeexpand', this, animate) === false) {
            return;
        }
        var doAnim = animate === true || (animate !== false && this.animCollapse);
        this.el.removeClass(this.collapsedCls);
        this.beforeEffect(doAnim);
        this.onExpand(doAnim, animate);
        return this;
    },

    // private
    onExpand: function(doAnim, animArg) {
        if (doAnim) {
            this[this.collapseEl].slideIn(this.slideAnchor,
                    Ext.apply(this.createEffect(animArg || true, this.afterExpand, this),
                        this.expandDefaults));
        } else {
            this[this.collapseEl].show(this.hideMode);
            this.afterExpand(false);
        }
    },

    // private
    afterExpand: function(anim) {
        this.collapsed = false;
        if (anim !== false) {
            this[this.collapseEl].show(this.hideMode);
        }
        this.afterEffect(anim);
        if (this.deferLayout) {
            delete this.deferLayout;
            this.doLayout(true);
        }
        this.fireEvent('expand', this);
    },

    /**
    * Shortcut for performing an {@link #expand} or {@link #collapse} based on the current state of the panel.
    * @param {Boolean} animate True to animate the transition, else false (defaults to the value of the
    * {@link #animCollapse} panel config)
    * @return {Ext.Panel} this
    */
    toggleCollapse: function(animate) {
        this[this.collapsed ? 'expand' : 'collapse'](animate);
        return this;
    },

    // private
    onDisable: function() {
        if (this.rendered && this.maskDisabled) {
            this.el.mask();
        }
        eBook.GTH.TeamExecutor.superclass.onDisable.call(this);
    },

    // private
    onEnable: function() {
        if (this.rendered && this.maskDisabled) {
            this.el.unmask();
        }
        eBook.GTH.TeamExecutor.superclass.onEnable.call(this);
    },
    working: false,
    // private
    onResize: function(adjWidth, adjHeight, rawWidth, rawHeight) {
        var w = adjWidth,
            h = adjHeight;

        if (Ext.isDefined(w) || Ext.isDefined(h)) {
            if (!this.collapsed) {
                // First, set the the Panel's body width.
                // If we have auto-widthed it, get the resulting full offset width so we can size the Toolbars to match
                // The Toolbars must not buffer this resize operation because we need to know their heights.

                if (Ext.isNumber(w)) {
                    this.body.setWidth(w = this.adjustBodyWidth(w)); //- this.getFrameWidth()
                } else if (w == 'auto') {
                    w = this.body.setWidth('auto').dom.offsetWidth;
                } else {
                    w = this.body.dom.offsetWidth;
                }
                if (!this.working) {
                    this.listEl.setWidth(w);
                }

                //                if (this.tbar) {
                //                    this.tbar.setWidth(w);
                //                    if (this.topToolbar) {
                //                        this.topToolbar.setSize(w);
                //                    }
                //                }
                //                if (this.bbar) {
                //                    this.bbar.setWidth(w);
                //                    if (this.bottomToolbar) {
                //                        this.bottomToolbar.setSize(w);
                //                        // The bbar does not move on resize without this.
                //                        if (Ext.isIE) {
                //                            this.bbar.setStyle('position', 'static');
                //                            this.bbar.setStyle('position', '');
                //                        }
                //                    }
                //                }
                //                if (this.footer) {
                //                    this.footer.setWidth(w);
                //                    if (this.fbar) {
                //                        this.fbar.setSize(Ext.isIE ? (w - this.footer.getFrameWidth('lr')) : 'auto');
                //                    }
                //                }

                // At this point, the Toolbars must be layed out for getFrameHeight to find a result.
                if (Ext.isNumber(h)) {
                    h = Math.max(0, h); //- this.getFrameHeight()
                    //h = Math.max(0, h - (this.getHeight() - this.body.getHeight()));
                    this.body.setHeight(h);
                } else if (h == 'auto') {
                    this.body.setHeight(h);
                }
                // if (!this.working) {
                this.listEl.setHeight(h);
                var hh = this.listEl.child('.eb-gth-todo-title').getHeight();
                var sbh = this.startEl ? this.listEl.child('.eb-gth-team-startbtn').getHeight() : 0;
                this.accordionEl.setHeight(h - sbh - hh);
                var w = this.accordion.el.getWidth();
                this.accordion.setSize(w, h - hh - sbh);
                // }

                if (this.disabled && this.el._mask) {
                    this.el._mask.setSize(this.el.dom.clientWidth, this.el.getHeight());
                }
            } else {
                // Adds an event to set the correct height afterExpand.  This accounts for the deferHeight flag in panel
                this.queuedBodySize = { width: w, height: h };
                if (!this.queuedExpand && this.allowQueuedExpand !== false) {
                    this.queuedExpand = true;
                    this.on('expand', function() {
                        delete this.queuedExpand;
                        this.onResize(this.queuedBodySize.width, this.queuedBodySize.height);
                    }, this, { single: true });
                }
            }
            this.onBodyResize(w, h);
        }
        // this.syncShadow();
        eBook.GTH.TeamExecutor.superclass.onResize.call(this, adjWidth, adjHeight, rawWidth, rawHeight);

    },

    // private
    onBodyResize: function(w, h) {
        this.fireEvent('bodyresize', this, w, h);
    },

    // private
    getToolbarHeight: function() {
        var h = 0;
        if (this.rendered) {
            Ext.each(this.toolbars, function(tb) {
                h += tb.getHeight();
            }, this);
        }
        return h;
    },

    // deprecate
    adjustBodyHeight: function(h) {
        return h;
    },

    // private
    adjustBodyWidth: function(w) {
        return w;
    },

    // private
    onPosition: function() {
        this.syncShadow();
    },

    /**
    * Returns the width in pixels of the framing elements of this panel (not including the body width).  To
    * retrieve the body width see {@link #getInnerWidth}.
    * @return {Number} The frame width
    */
    getFrameWidth: function() {
        var w = this.el.getFrameWidth('lr');

        if (this.frame) {
            var l = this.body.dom;
            w += (Ext.fly(l).getFrameWidth('l') + Ext.fly(l.firstChild).getFrameWidth('r'));
            w += this.mc.getFrameWidth('lr');
        }
        return w;
    },

    /**
    * Returns the height in pixels of the framing elements of this panel (including any top and bottom bars and
    * header and footer elements, but not including the body height).  To retrieve the body height see {@link #getInnerHeight}.
    * @return {Number} The frame height
    */
    getFrameHeight: function() {
        var h = Math.max(0, this.getHeight() - this.body.getHeight());

        if (isNaN(h)) {
            h = 0;
        }
        return h;

        /* Deprecate
        var h  = this.el.getFrameWidth('tb') + this.bwrap.getFrameWidth('tb');
        h += (this.tbar ? this.tbar.getHeight() : 0) +
        (this.bbar ? this.bbar.getHeight() : 0);

            if(this.frame){
        h += this.el.dom.firstChild.offsetHeight + this.ft.dom.offsetHeight + this.mc.getFrameWidth('tb');
        }else{
        h += (this.header ? this.header.getHeight() : 0) +
        (this.footer ? this.footer.getHeight() : 0);
        }
        return h;
        */
    },

    /**
    * Returns the width in pixels of the body element (not including the width of any framing elements).
    * For the frame width see {@link #getFrameWidth}.
    * @return {Number} The body width
    */
    getInnerWidth: function() {
        return this.getSize().width - this.getFrameWidth();
    },

    /**
    * Returns the height in pixels of the body element (not including the height of any framing elements).
    * For the frame height see {@link #getFrameHeight}.
    * @return {Number} The body height
    */
    getInnerHeight: function() {
        return this.body.getHeight();
        /* Deprecate
        return this.getSize().height - this.getFrameHeight();
        */
    },

    // private
    syncShadow: function() {
        if (this.floating) {
            this.el.sync(true);
        }
    },

    // private
    getLayoutTarget: function() {
        return this.body;
    },

    // private
    getContentTarget: function() {
        return this.body;
    },

    /**
    * <p>Sets the title text for the panel and optionally the {@link #iconCls icon class}.</p>
    * <p>In order to be able to set the title, a header element must have been created
    * for the Panel. This is triggered either by configuring the Panel with a non-blank <code>{@link #title}</code>,
    * or configuring it with <code><b>{@link #header}: true</b></code>.</p>
    * @param {String} title The title text to set
    * @param {String} iconCls (optional) {@link #iconCls iconCls} A user-defined CSS class that provides the icon image for this panel
    */
    setTitle: function(title, iconCls) {
        this.title = title;
        var wpTitle = this.el.child('.eb-gth-workpane-title');
        wpTitle.update(title);
        return this;
    },

    /**
    * Get the {@link Ext.Updater} for this panel. Enables you to perform Ajax updates of this panel's body.
    * @return {Ext.Updater} The Updater
    */
    getUpdater: function() {
        return this.body.getUpdater();
    },

    /**
    * Loads this content panel immediately with content returned from an XHR call.
    * @param {Object/String/Function} config A config object containing any of the following options:
    <pre><code>
    panel.load({
    url: 'your-url.php',
    params: {param1: 'foo', param2: 'bar'}, // or a URL encoded string
    callback: yourFunction,
    scope: yourObject, // optional scope for the callback
    discardUrl: false,
    nocache: false,
    text: 'Loading...',
    timeout: 30,
    scripts: false
    });
    </code></pre>
    * The only required property is url. The optional properties nocache, text and scripts
    * are shorthand for disableCaching, indicatorText and loadScripts and are used to set their
    * associated property on this panel Updater instance.
    * @return {Ext.Panel} this
    */
    load: function() {
        var um = this.body.getUpdater();
        um.update.apply(um, arguments);
        return this;
    },

    // private
    beforeDestroy: function() {
        Ext.Panel.superclass.beforeDestroy.call(this);
        if (this.header) {
            this.header.removeAllListeners();
        }
        if (this.tools) {
            for (var k in this.tools) {
                Ext.destroy(this.tools[k]);
            }
        }
        if (this.toolbars.length > 0) {
            Ext.each(this.toolbars, function(tb) {
                tb.un('afterlayout', this.syncHeight, this);
                tb.un('remove', this.syncHeight, this);
            }, this);
        }
        if (Ext.isArray(this.buttons)) {
            while (this.buttons.length) {
                Ext.destroy(this.buttons[0]);
            }
        }
        if (this.rendered) {
            Ext.destroy(
                this.ft,
                this.header,
                this.footer,
                this.tbar,
                this.bbar,
                this.body,
                this.mc,
                this.bwrap,
                this.dd
            );
            if (this.fbar) {
                Ext.destroy(
                    this.fbar,
                    this.fbar.el
                );
            }
        }
        Ext.destroy(this.toolbars);
    },

    // private
    createClasses: function() {
        this.headerCls = this.baseCls + '-header';
        this.headerTextCls = this.baseCls + '-header-text';
        this.bwrapCls = this.baseCls + '-bwrap';
        this.tbarCls = this.baseCls + '-tbar';
        this.bodyCls = this.baseCls + '-body';
        this.bbarCls = this.baseCls + '-bbar';
        this.footerCls = this.baseCls + '-footer';
    },

    // private
    createGhost: function(cls, useShim, appendTo) {
        var el = document.createElement('div');
        el.className = 'x-panel-ghost ' + (cls ? cls : '');
        if (this.header) {
            el.appendChild(this.el.dom.firstChild.cloneNode(true));
        }
        Ext.fly(el.appendChild(document.createElement('ul'))).setHeight(this.bwrap.getHeight());
        el.style.width = this.el.dom.offsetWidth + 'px'; ;
        if (!appendTo) {
            this.container.dom.appendChild(el);
        } else {
            Ext.getDom(appendTo).appendChild(el);
        }
        if (useShim !== false && this.el.useShim !== false) {
            var layer = new Ext.Layer({ shadow: false, useDisplay: true, constrain: false }, el);
            layer.show();
            return layer;
        } else {
            return new Ext.Element(el);
        }
    },

    // private
    doAutoLoad: function() {
        var u = this.body.getUpdater();
        if (this.renderer) {
            u.setRenderer(this.renderer);
        }
        u.update(Ext.isObject(this.autoLoad) ? this.autoLoad : { url: this.autoLoad });
    },

    /**
    * Retrieve a tool by id.
    * @param {String} id
    * @return {Object} tool
    */
    getTool: function(id) {
        return this.tools[id];
    }

    , selectClient: function(client, stepid) {
        this.client = client;
        this.moveNextStep();
    }
    , stopUpdateTask: function() {
        if (this.accordion) this.accordion.stopUpdateTask();
    }
    , startUpdateTask: function() {
        if (this.accordion) this.accordion.startUpdateTask();

    }

});
Ext.reg('TeamExecutor', eBook.GTH.TeamExecutor);