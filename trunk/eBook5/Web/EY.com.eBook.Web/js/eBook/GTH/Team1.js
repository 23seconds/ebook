/*
eBook.GTH.FileUpload = Ext.extend(Ext.ux.form.FileUploadField, {
    validateValue:function(v) {
        var valid = eBook.GTH.FileUpload.superclass.validateValue.call(this,v);
        this.periodEnd.format('ddMMY');
        var rexp = new RegExp(this.enterprisNr + '_' +  + '/gi')
    }
*/

eBook.GTH.Team1_Upload = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: { type: 'vbox', align: 'stretch' }
            , border: false
            , items: [{ xtype: 'panel', layout: { type: 'hbox', align: 'stretch' }, flex: 1, items: [{ xtype: 'GTHClientDetails', teamId: this.teamId, ref: '../clientDetails', height: 80, flex: 1 }, { xtype: 'GTHTeam4DownloadFiles', teamId: this.teamId, ref: '../downloadfiles', flex: 1}] }
                      , { xtype: 'GTHUploadNBB', teamId: this.teamId, ref: 'uploadNBB', flex: 1 }
                      , { xtype: 'button', text: 'Upload files', handler: this.onUploadClick, scope: this, height: 60, cls: 'eb-massive-btn' }
                      ]
        });
        eBook.GTH.Team1_Upload.superclass.initComponent.apply(this, arguments);
    }
    , initStep: function(owner) {
        owner.hideStop();
        var cl = owner.client;
        this.clientId = cl.Id;
        this.stepTitle = 'STEP 2: Download &amp; upload files for ' + cl.ClientName;
        this.clientDetails.initStep(cl);
        this.uploadNBB.reset(); //getForm().applyToFields({ originalValue: null });
        this.uploadNBB.setClient(owner.client);
        this.downloadfiles.getFiles(cl);
       // this.uploadNBB.getForm().reset();

    }
    , onUploadClick: function(e) {
        var f = this.uploadNBB.getForm();

        if (!this.uploadNBB.isValid()) {
            alert("Some required fields are invalid/missing");
            return;
        }
        f.isValid = function() { return true; };
        f.submit({
            url: 'UploadMultiple.aspx',
            waitMsg: 'Uploading files',
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function(fp, o) {
        this.getEl().mask("Validating &amp; processing files", 'x-mask-loading');
        var dta = this.uploadNBB.getData(o.result.files);
        Ext.Ajax.request({
            url: eBook.Service.GTH + 'ProcessFilesTeam1'
            , method: 'POST'
            , params: Ext.encode({ cpfdc: {  StructureId: '3787f2a4-112d-4589-a231-e9b95bdcfcd9', Files: dta, TeamId: this.teamId, Status: 'FINAL', PersonId: eBook.User.personId, GTHClientId: this.clientId} })
             , callback: this.handleFileProcessing
            , scope: this
        });
        //process o.result.file
        /*this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
       
        ¨*/

    }
    , failedUpload: function(fp, o) {
        eBook.Interface.showError(o.message, "UPLOAD FAILED!!")
        this.getEl().unmask();
    }
    , handleFileProcessing: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.ownerCt.moveNextStep();

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam1UploadFiles', eBook.GTH.Team1_Upload);


eBook.GTH.GTHClientDetails = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.templ = new Ext.XTemplate('<div class="gth-team1-downloadfiles">'
                                            , '<div class="gth-team1-downloadfiles-body">'
                                                , '<div>CLIENT: <b>{ClientName}</b></div>'
                                                , '<div>COMPANY NR: <b>{[eBook.CleanEnterprise(values.ClientEnterprise)]}</b>'
                                                , '<tpl if="values.StartDate!=null"><span style="padding-left:10px;">Period : <b>{StartDate:date("d/m/Y")} &gt; {EndDate:date("d/m/Y")}</b></span></tpl>'
                                                ,'</div>'
                                                
        // , '<i>Download the files from the NBB website (internet explore) and upload them here.</i>'
                                            , '</div>'
        // , '<div class="gth-bottomtoolbar">'
        //     , '<div class="gth-moveNext"></div>'
        // , '</div>'
                                           , ' </div>');
        this.templ.compile();
        Ext.apply(this, {
            html: 'test'
        });
        eBook.GTH.GTHClientDetails.superclass.initComponent.apply(this, arguments);
    }
     , initStep: function(cl) {
        if (cl.StartDate != null) cl.StartDate = Ext.data.Types.WCFDATE.convert(cl.StartDate);
        if (cl.EndDate != null) cl.EndDate = Ext.data.Types.WCFDATE.convert(cl.EndDate);
         this.body.update(this.templ.apply(cl));
     }
});        


Ext.reg('GTHClientDetails', eBook.GTH.GTHClientDetails);


eBook.GTH.GTHUploadNBB = Ext.extend(Ext.form.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            fileUpload: true,
            border: false,
            bodyStyle: 'padding:10px;',
            autoScroll: true,
            layout: 'column',
            items: [{ xtype: 'GTHNbbFrameSet', ref: 'period1', title: 'Period 1', checkboxName: 'period1' }
                    , { xtype: 'GTHNbbFrameSet', ref: 'period2', title: 'Period 2', checkboxName: 'period2' }
            //, { xtype: 'GTHNbbFrameSet', ref: 'period2' }
            ]
        });
        eBook.GTH.GTHUploadNBB.superclass.initComponent.apply(this, arguments);
    }
    , isValid: function() {
        var valid = true;
        if (!this.period1.collapsed) {
            valid = valid && this.period1.isValid();
        }
        if (!this.period2.collapsed) {
            valid = valid && this.period2.isValid();
        }
        return valid;
    }
    , setClient: function(client) {
        client.ClientEnterprise = eBook.CleanEnterprise(client.ClientEnterprise);
        if (client.ClientEnterprise == 'NULL') client.ClientEnterprise = '';
        this.period1.setClient(client);
        this.period2.setClient(client);
    }
    , reset: function() {
        this.period1.reset();
        this.period2.reset();
    }
    , getData: function(uplFiles) {
        var res = [];
        if (!this.period1.collapsed) res = res.concat(this.period1.getData(uplFiles));
        if (!this.period2.collapsed) res = res.concat(this.period2.getData(uplFiles));
        return res;
    }
});
Ext.reg('GTHUploadNBB', eBook.GTH.GTHUploadNBB);

eBook.GTH.GTHNbbFrameSet = Ext.extend(Ext.form.FieldSet, {
    initComponent: function() {
        Ext.apply(this, {
            // collapsible: true,
            autoHeight: true,
            checkboxToggle: true,
            columnWidth: 0.5,
            bodyStyle: 'padding:5px;',
            border: false,
            items: [{ xtype: 'datefield', format: 'd M Y', ref: 'periodStart', name: 'StartPeriod', allowBlank: false, fieldLabel: 'Financial year start' }
                    , { xtype: 'datefield', format: 'd M Y', ref: 'periodEnd', name: 'EndPeriod', allowBlank: false, fieldLabel: 'Financial year end' }
                    , new Ext.ux.form.FileUploadField({ width: 350, ref: 'xbrl', allowBlank: true, fieldLabel: 'XBRL File', fileTypeRegEx: /(\.xbrl)$/i })
                    , new Ext.ux.form.FileUploadField({ width: 350, ref: 'pdf', allowBlank: false, fieldLabel: 'PDF File', fileTypeRegEx: /(\.pdf)$/i })
            ]

        });
        eBook.GTH.GTHNbbFrameSet.superclass.initComponent.apply(this, arguments);
    }
    , reset: function() {
        this.periodStart.reset();
        this.periodEnd.reset();
        this.xbrl.reset();
        this.pdf.reset();
    }
    , getData: function(uplFiles) {
        var start = this.periodStart.getValue();
        var end = this.periodEnd.getValue();
        var vxbrl = this.xbrl.getValue();
        var vpdf = this.pdf.getValue();
        var fles = [];
        for (var i = 0; i < uplFiles.length; i++) {
            var fle = uplFiles[i];
            var fo = { Name: fle.originalFile, Id: fle.id, Extension: fle.extension, ContentType: fle.contentType, fileName: fle.file };
            if (!Ext.isEmpty(vxbrl) && vxbrl.indexOf(fle.originalFile) > -1) {
                fo.Start = start;
                fo.End = end;
                fles.push(fo);
            } else if (!Ext.isEmpty(vpdf) && vpdf.indexOf(fle.originalFile) > -1) {
                fo.Start = start;
                fo.End = end;
                fles.push(fo);
            }
        }
        return fles;

    }
    , setClient: function(client) {
        this.client = client;

    }
    , isValid: function() {
        var valid = true;
        this.items.each(function(it) {
            var vit = it.isValid();
            valid = valid && vit;
        }, this);

        if (valid) {
            if (this.periodStart.getValue() > this.periodEnd.getValue()) {
                this.periodStart.markInvalid("Start of period cannot be after end of period.");
                this.periodEnd.markInvalid("End of period cannot be before start of period.");
                valid = false;
            } else {
                var fmsg = 'Format of the filename needs to be {CompanyNr}_{day}{month}{year}. Day & month must include a leading zero if lower then 10';
                var fileClientRegex = new RegExp(this.client.ClientEnterprise + '_' + this.periodEnd.getValue().format('dmY'));
                var v = this.xbrl.getValue();
                if (!Ext.isEmpty(v)) {
                    if (!fileClientRegex.test(v)) {
                        valid = false;
                        this.xbrl.markInvalid(fmsg);
                    }
                }
                v = this.pdf.getValue();
                if (!Ext.isEmpty(v)) {
                    if (!fileClientRegex.test(v)) {
                        valid = false;
                        this.pdf.markInvalid(fmsg);
                    }
                }
            }


        }
        return valid;
    }
});
Ext.reg('GTHNbbFrameSet', eBook.GTH.GTHNbbFrameSet);            
            

