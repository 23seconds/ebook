eBook.GTH.Team5 = Ext.extend(Ext.Panel, {
    id: 'GthTeam5MainPanel',
    period: null,
    serviceId: '1ABF0836-7D7B-48E7-9006-F03DB97AC28B',

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            bodyCssClass: 'eBook-GTH-Team5',
            ref: 'eBook-GTH-Team5',
            layout: {
                type: 'vbox',
                align: 'stretch',
                pack: 'start'
            },
            defaults: {
                bodyStyle: 'padding:15px'
            },
            items: [
                {
                    xtype: 'panel',
                    title: 'Annual Accounts awaiting filing',
                    ref: 'AwaitingFilingPanel',
                    //height: 250,
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    padding: 0,
                    items: [
                        {
                            xtype: 'button',
                            ref: 'btnPickupAwaitingFiling',
                            text: 'Pick up item for filing<br>(from list or allow system to choose latest)',
                            width: 220,
                            //height: 250,
                            record: {action: 'Filing'}, //record obj
                            cls: 'eBook-GTH-Team5-btnPickupFirstOrSelectedRecord', //padding-top increase for button icon and text
                            iconCls: 'eBook-repository-select-ico',
                            iconAlign: 'top',
                            listeners: {
                                click: {fn: me.onBtnPickUpRecordClicked, scope: me}
                            }
                        },
                        {
                            xtype: 'reportFSgrid',
                            flex: 1,
                            //height: 250,
                            cls: 'eBookAT-GTH-Persongrid',
                            ref: 'pickupGrid',
                            serviceId: me.serviceId,
                            title: 'List of annual accounts to be filed with the NBB filing application',
                            filterValues: {status: [5,10,13], field: 'daysOnStatus', direction: 'DSC'} //Only Approved by Partner, Returned to pool by GTH or Refiling requested by NBB Admin (hence the ones that need NBB filing)
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    title: 'Annual Accounts awaiting invoice',
                    ref: 'AwaitingInvoicePanel',
                    //height: 250,
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    padding: 0,
                    items: [
                        {
                            xtype: 'button',
                            ref: 'btnPickupAwaitingInvoice',
                            text: 'Pick up item for invoice info input<br>(from list)',
                            width: 220,
                            //height: 250,
                            record: {action: 'Input invoice info'}, //record obj
                            cls: 'eBook-GTH-Team5-btnPickupFirstOrSelectedRecord', //padding-top increase for button icon and text
                            iconCls: 'eBook-invoice-select-ico',
                            iconAlign: 'top',
                            listeners: {
                                click: {fn: me.onBtnPickUpRecordClicked, scope: me}
                            }
                        },
                        {
                            xtype: 'reportFSgrid',
                            flex: 1,
                            height: 250,
                            cls: 'eBookAT-GTH-Persongrid',
                            ref: 'pickupGrid',
                            serviceId: me.serviceId,
                            title: 'List of annual accounts awaiting invoice information from NBB',
                            filterValues: {status: 9, field: 'daysOnStatus', direction: 'DSC'} //Only Filed by GTH  (hence the ones that awaiting invoice feedback from NBB)
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    ref: 'recordPnl',
                    record: null, //custom record prop
                    title: 'Use the above button to pick a client',
                    padding: 0,
                    flex: 1,
                    boxMinHeight: 200,
                    disabled: true,
                    layout: {
                        type: 'hbox',
                        align: 'stretch',
                        pack: 'start'
                    },
                    items: [
                        {
                            xtype: 'formOverview',
                            ref: 'formOverview',
                            flex: 1,
                            serviceUrl: eBook.Service.annualAccount
                        },
                        {
                            xtype: 'panel',
                            ref: 'actionPnl',
                            title: 'Actions',
                            flex: 2,
                            padding: 0,
                            layout: {
                                type: 'vbox',
                                align: 'stretch',
                                pack: 'start',
                                defaultMargins: {
                                    top: 10,
                                    bottom: 10,
                                    right: 5,
                                    left: 10
                                }
                            },
                            items: [{
                                xtype: 'button',
                                text: 'Copy folder URL<br>(folder contains file required for upload on NBB website)',
                                ref: 'btnCopyUrl',
                                iconCls: 'eBook-icon-openreport-24',
                                flex: 1,
                                scale: 'medium',
                                iconAlign: 'top',
                                margins: {top: 10, right: 10, bottom: 10, left: 10},
                                listeners: {
                                    click: {fn: me.onBtnCopyUrl, scope: me}
                                }
                            },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch',
                                        pack: 'start',
                                        defaultMargins: {
                                            top: 0,
                                            bottom: 0,
                                            right: 5,
                                            left: 0
                                        }
                                    },
                                    ref: 'submitBtnsCon',
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: 'success', //dynamically updated through status
                                            ref: '../btnSuccess',
                                            iconCls: 'eBook-yes-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            flex: 1,
                                            listeners: {
                                                click: {fn: me.onBtnSuccessClicked, scope: me}
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'failed', //dynamically updated through status
                                            ref: '../btnFailure',
                                            iconCls: 'eBook-grid-row-delete-ico',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            flex: 1,
                                            listeners: {
                                                click: {fn: me.onBtnFailedClicked, scope: me}
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                afterrender: function () {
                    me.getEl().mask("Checking if a filing item was already picked up and if so retrieving it");
                    //load any picked up item
                    if (me.recordPnl) {me.recordPnl.record = {statusId: 7, action: 'Filing'};}
                    me.getLongestOnStatusFileServiceReportRecord(7, eBook.User.personId);
                }
            }
        });

        eBook.GTH.Team5.superclass.initComponent.apply(this, arguments);
    },

    ///show override
    /*
     show: function ()
     {
     var me = this;

     me.AwaitingFilingPanel.pickupGrid.storeLoad({status: 5, field: 'daysOnStatus', direction: 'DSC'});
     eBook.GTH.Team5.Window.superclass.show.call(me);
     },
     */
    refreshPickupGrid: function () {
        var me = this;

        me.AwaitingFilingPanel.pickupGrid.storeLoad({status: [5,10,13], field: 'daysOnStatus', direction: 'DSC'});
        me.AwaitingInvoicePanel.pickupGrid.storeLoad({status: 9, field: 'daysOnStatus', direction: 'DSC'});
    },

    ///User has clicked the pick up button (FIFO or selected item), which calls the GetFileServiceReport function allowing the retrieval of one record, sorted by daysOnStatus
    onBtnPickUpRecordClicked: function (btn) {
        var me = this,
            grid = btn.refOwner.pickupGrid,
            selectedRecord = grid.getActiveRecord();

        if (me.recordPnl) {
            if (me.recordPnl.record) { //record already loaded?
                Ext.MessageBox.show({
                    title: "Item already picked up. Move item back to pool?",
                    msg: "An item was already selected. Do you want to return this item to the pool and select a new one?",
                    buttons: Ext.MessageBox.YESNO,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            me.getEl().mask("Moving item to awaiting filing list");
                            if (me.recordPnl.record.statusId == 7) {
                                me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 10, null); //Updates service status to 'Returned to pool by GTH'
                            }
                            else {
                                me.unloadFileServiceRecord();
                            }
                        }
                    }
                });
            } else {
                me.recordPnl.record = {};
                /*for (var prop in btn.record) {
                    me.recordPnl.record[prop] = btn.record[prop]; //Add pre-defined tn record props to recordPnl record obj
                }*/

                if (selectedRecord) //user selected record?
                {
                    me.getEl().mask("Retrieving selected Annual Accounts item");
                    var recordLoaded = me.loadFileServiceRecord(selectedRecord.data);

                    if (recordLoaded) {
                        grid.getSelectionModel().clearSelections();
                        if (selectedRecord.get("statusId") == 5 || selectedRecord.get("statusId") == 10 || selectedRecord.get("statusId") == 13) {
                            me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 7, null); //Updates service status to 'Picked up by GTH'
                        }
                        else {
                            me.updateRecordPnl();
                        }
                    }
                }
                else if (btn.record.action == "Filing") { //auto pick up record if action is filing
                    me.getEl().mask("Automatically retrieving oldest Annual Accounts item that requires filing");
                        me.getLongestOnStatusFileServiceReportRecord([5,10,13], null); //get longest on status for any filing pick up status
                }
                else {
                    me.recordPnl.record = null;
                    Ext.Msg.alert("Select an item", "Please select an item.");
                }
            }
        }
    },

    ///Get the record that has been the most days on a certain status and optionally for a certain person
    getLongestOnStatusFileServiceReportRecord: function (statusIds, lastModifiedBy) {
        var me = this,
            storeConfig = {
                selectAction: 'GetFileServiceReport',
                fields: eBook.data.RecordTypes.ClientPersonFileServiceReport,
                idField: 'itemId',
                criteriaParameter: 'ccpfsdc',
                serviceUrl: eBook.Service.file
            },
            jsonStore = new eBook.data.PagedJsonStore(storeConfig),
            params = {
                Start: 0,
                Limit: 1,
                sv: me.serviceId ? me.serviceId : null,
                dp: me.department ? me.department : null,
                pid: eBook.User.personId,
                lmb: lastModifiedBy,
                sid: statusIds.toString(),
                sf: 'daysOnStatus',
                so: 'DSC'
            }; //Get the latest item (hence get top 1 on status 5 ordered by daysOnStatus desc

        jsonStore.load(
            {
                params: params,
                callback: me.onFileServiceRecordRetrieved,
                scope: me
            }
        );
    },

    ///Process response from GetFileServiceReport and UpdateFileService and load the record
    onFileServiceRecordRetrieved: function (response, opts, success) {
        var me = this,
            fsRecord = null;

        if (success) {
            if (response.length == 1) {
                fsRecord = response[0].data;

                if (me.recordPnl) {
                    var recordLoaded = me.loadFileServiceRecord(fsRecord);

                    if (recordLoaded) {
                        if (fsRecord.statusId === 5) { //approved by partner
                            me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 7, null); //Updates service status to 'Picked up by GTH'
                        }
                        else {
                            me.updateRecordPnl();
                        }
                    }
                }
            }
            else {
                if (me.recordPnl.record) {me.recordPnl.record = null;}
                me.getEl().unmask();
            }
        }
        else {
            eBook.Interface.showResponseError(response, me.title);
        }
    },

    loadFileServiceRecord: function (fsRecord) {
        var me = this;

        if (me.recordPnl) {
            for (var prop in fsRecord) {
                me.recordPnl.record[prop] = fsRecord[prop]; //Add retrieved prop to recordPnl record obj
            }
        }

        eBook.Interface.currentFile = { //set currentFile
            gthTeam5FileId: fsRecord.fileId,
            gthTeam5FileName: fsRecord.fileName,
            gthTeam5StartDate: fsRecord.startDate,
            gthTeam5EndDate: fsRecord.endDate
        };

        eBook.Interface.currentClient = { //set currentClient
            gthTeam5ClientId: fsRecord.clientId,
            gthTeam5ClientName: fsRecord.clientName
        };

        me.getEl().unmask();

        return true;
    },

    ///Clear current file and client objects and hide the record panel
    unloadFileServiceRecord: function () {
        var me = this,
            recordPnl = me.recordPnl;

        eBook.Interface.currentFile = null;
        eBook.Interface.currentClient = null;

        if (recordPnl) {
            recordPnl.record = null;
            recordPnl.disable();
            recordPnl.setTitle('Use the above button to pick a client');
            if (recordPnl.formOverview) {
                recordPnl.formOverview.clearStore();
            }
        }
        me.getEl().unmask();
        me.refreshPickupGrid();
    },

    ///Set current file and client objects using the records data and load the record panel
    updateRecordPnl: function () {
        var me = this,
            recordPnl = me.recordPnl,
            actionPnl = recordPnl.actionPnl,
            btnSuccess = actionPnl.btnSuccess,
            btnFailure = actionPnl.btnFailure,
            btnCopyUrl = actionPnl.btnCopyUrl,
            fsRecord = recordPnl.record;

        if (fsRecord) {

            //hide/show/update buttons
            if (btnSuccess && fsRecord.action) {
                btnSuccess.btnEl.update(fsRecord.action + " success" || '&#160;'); //not using setText as it uses autowidth
            }
            if (btnFailure && fsRecord.action) {
                btnFailure.btnEl.update(fsRecord.action + " failed" || '&#160;');
            }
            if (btnCopyUrl && fsRecord.statusId == 7) {
                btnCopyUrl.show();
            }
            if (recordPnl.formOverview) {
                recordPnl.formOverview.loadStore(fsRecord.fileId); //load form overview of record
            }

            recordPnl.setTitle(fsRecord.clientName + ': ' + fsRecord.fileName);
            recordPnl.enable();
            me.getEl().unmask();
        }

        me.refreshPickupGrid();
    },

    ///Creates the URL to the NBB file using file and client data
    onBtnCopyUrl: function () {
        var destinationFolderPath = "C:\\ACR Central Processing\\GTH\\NBB\\TODO", //To be supplied by server
            currentClient = eBook.Interface.currentClient,
            currentFile = eBook.Interface.currentFile;

        if (currentClient && currentFile) {
            //Client folder
            destinationFolderPath += '\\' + currentClient.gthTeam5ClientName.replace(/[^a-z\d\-]+/gi, "") + '-' + currentClient.gthTeam5ClientId; //Keep only letters, numbers and -
            //File folder
            destinationFolderPath += '\\' + currentFile.gthTeam5FileName.substring(4).replace(/[^a-z\d\-]+/gi, "") + '-' + currentFile.gthTeam5FileId; //Removed the year prefix and keep only letters, numbers and -

            window.prompt("Copy the URL to your clipboard by pressing Ctrl+C or right click and choose copy. Close this window with enter.", destinationFolderPath);
        }
        else {
            Ext.Msg.alert("No client selected", "Select a client.");
        }
    },

    onBtnSuccessClicked: function (successButton,e,invoiceAmount,invoiceAmountInvalidText,invoiceStructuredMessage,invoiceStructuredMessageInvalidText) { //optional params on selfcall
        var me = this,
            record = me.recordPnl.record,
            msg = '',
            buttons = null;

        if (record.statusId == 7) //Picked up for filing
        {
            msg += 'Was the annual accounts file successfully uploaded on the NBB website?';
            buttons = Ext.MessageBox.YESNO;
        }
        else if (record.statusId == 9) //Invoice
        {
            msg += 'Amount to be paid:<br><input type="text" id="invoiceAmount" ' + (invoiceAmount ? (' value="' + invoiceAmount + '"') : '') + '>'; //add if given
            msg += '<div style="color:red;" id="invoiceAmountInvalidText">' + (invoiceAmountInvalidText ? invoiceAmountInvalidText : '') + '</div>';
            msg += 'Invoice structured message:<br><input type="text" id="invoiceStructuredMessage" maxlength="20" ' + (invoiceStructuredMessage ? (' value="' + invoiceStructuredMessage + '" ') : '') + 'oninput="Ext.getCmp(&quot;GthTeam5MainPanel&quot;).autoMarkupStructuredMessage()">'; //add if given, auto-fill-function on input
            msg += '<div style="color:red;" id="invoiceStructuredMessageInvalidText">' + (invoiceStructuredMessageInvalidText ? invoiceStructuredMessageInvalidText : '') + '</div>';
            buttons = {ok: "Submit", cancel: "Cancel"};
        }

        Ext.MessageBox.show({
            title: record.action,
            msg: msg,
            width: 200,
            buttons: buttons,
            fn: function(btn) {
                if (btn == 'ok') {
                    //get form input
                    invoiceAmount =  Ext.get('invoiceAmount').getValue();
                    invoiceStructuredMessage = Ext.get('invoiceStructuredMessage').getValue();
                    invoiceAmountInvalidText = null;
                    invoiceStructuredMessageInvalidText = null;

                    var pInvoiceAmount = parseFloat(invoiceAmount.replace(/[^0-9-,]/g, '').replace(/\,/g, '.')), //capture and replace any currency formatting
                        numbers = me.getNumbersFromString(invoiceStructuredMessage),
                        sequence = numbers.substring(0, numbers.length - 2),
                        check = numbers.slice(-2); //last 2 figures

                    //validation
                    if (pInvoiceAmount.toFixed(2) != pInvoiceAmount) { //invoiceAmount validation
                        invoiceAmountInvalidText = "Amount invalid. Check numbers after comma.";
                    }
                    if (!(sequence % 97 == check || (sequence % 97 === 0 && check == 97))) { //structured message validation
                        invoiceStructuredMessageInvalidText = "Structured message invalid. Please verify.";
                    }

                    //followup
                    if (pInvoiceAmount && invoiceStructuredMessage && eBook.Interface.currentFile.gthTeam5FileId && !invoiceAmountInvalidText && !invoiceStructuredMessageInvalidText) {
                        record.invoiceAmount = pInvoiceAmount;
                        record.invoiceStructuredMessage = invoiceStructuredMessage;
                        me.openUploadWindow('b8266c9e-8dcd-414d-bcf7-6014e5e76e1d', 'Invoice NBB ' + eBook.Interface.currentClient.gthTeam5ClientName, 'c6165544-80e3-4390-bc8b-bc159f2cc880');
                    }
                    else {
                        return me.onBtnSuccessClicked(successButton,e,invoiceAmount,invoiceAmountInvalidText,invoiceStructuredMessage,invoiceStructuredMessageInvalidText); //ropen alert with validation explanation (yes I should have used a window)
                    }
                }
                else if (btn == 'yes') {
                    me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 9, null); //Updates service status to 'NBB Filed by GTH'
                    me.getEl().mask("Moving item to awaiting invoice list");
                }
            }
        });
    },

    getNumbersFromString: function (text) {
        return text.replace(/[^0-9]/g, '');
    },

    autoMarkupStructuredMessage: function () {
        var me = this,
            field = Ext.get("invoiceStructuredMessage");

        if (field) {
            var fieldValue = field.getValue(),
                insertString = function (target, source, index) {
                    return (target.slice(0, index) + source + target.slice(index));
                };

            fieldValue = me.getNumbersFromString(fieldValue); //remove anything but numbers

            if (fieldValue.length < 18) { //waterfall through and auto-add chars (if's are faster than switch)
                switch (true) {
                    case fieldValue.length > 11:
                        fieldValue += "+++";
                    case fieldValue.length > 6:
                        fieldValue = insertString(fieldValue, "/", 7);
                    case fieldValue.length > 2:
                        fieldValue = insertString(fieldValue, "/", 3);
                    case fieldValue.length > 0:
                        fieldValue = "+++" + fieldValue;
                        break;
                }
            }
            else {
                Ext.Msg.alert("Too many characters", "Structured message contains too many characters.");
            }

            field.dom.value = fieldValue;
        }
    },

    onBtnFailedClicked: function () {
        var me = this,
            buttons = {yes: "Submit", cancel: "Cancel"};

        Ext.Msg.show({
            title: 'Explain why the filing failed',
            width: 300,
            multiline: 75,
            buttons: buttons,
            fn: function (buttonId, text, opt) {
                var comment = null;
                switch (buttonId) {
                    case 'cancel':
                        //user cancels update
                        break;
                    case 'yes':
                        //user adds comment
                        if (text.length === 0) {return me.onBtnFilingFailedClicked();}
                        comment = text;
                    //notice no break!
                    default:
                        me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 8, comment); //Updates service status to 'Rejected by GTH' and adds review note
                        me.getEl().mask("Informing team of failure");
                }
            }
        });
    },

    openUploadWindow: function (location, filename, repositoryStatusId, periodStart, periodEnd) {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
            standards = {
                location: location,
                periodStart: periodStart,
                periodEnd: periodEnd
            };

        //optionals
        if (filename) {standards.fileName = filename;}
        if (repositoryStatusId) {standards.statusid = repositoryStatusId;}

        var wn = new eBook.Repository.SingleUploadWindow({
            clientId: eBook.Interface.currentClient.gthTeam5ClientId,
            changeFileName: true,
            standards: standards,
            parentCaller: me.ref,
            readycallback: {
                fn: me.onUploadSuccess,
                scope: me
            }
        });

        wn.show(me);
    },
    ///after upload, update annual accounts invoice info & update the status to "Invoice uploaded by GTH"
    onUploadSuccess: function (repositoryItemId) {
        var me = this,
            record = me.recordPnl ? me.recordPnl.record : null;

        record.invoiceRepositoryItemId = repositoryItemId;

        if(record.invoiceAmount && record.invoiceStructuredMessage && record.invoiceRepositoryItemId  && eBook.Interface.currentFile.gthTeam5FileId) {
            Ext.Ajax.request({
                url: eBook.Service.annualAccount + 'UpdateAnnualAccounts',
                method: 'POST',
                params: Ext.encode({
                    cfaadc: {
                        FileId: eBook.Interface.currentFile.gthTeam5FileId,
                        InvoiceAmount: record.invoiceAmount,
                        InvoiceStructuredMessage: record.invoiceStructuredMessage,
                        InvoiceRepositoryItemId: record.invoiceRepositoryItemId
                    }
                }),
                callback: me.onUpdateAnnualAccountsSuccess, //Invoice uploaded by GTH
                scope: me
            });
        }
    },

    ///once the annual accounts record is up to date, update status of service
    onUpdateAnnualAccountsSuccess : function(options, success, response)
    {
        var me = this;

        if (success && response.responseText) {
            me.updateFileService(eBook.Interface.currentFile.gthTeam5FileId, me.serviceId, 11, null);
        }
    },

    updateFileService: function (fileId, serviceId, statusId, comment) {
        //All must be filled in, except for comment
        if (!fileId || !serviceId) {return;}
        //At least one of these must be filled in
        if (!statusId && !comment) {return;}

        var me = this,
            ufsdc = { //file service update object
                FileId: fileId,
                ServiceId: serviceId,
                Person: eBook.User.getActivePersonDataContract()
            };

        me.getEl().mask("Updating...");

        //add optionals
        if (statusId) {ufsdc.Status = statusId;}
        if (comment) {ufsdc.Comment = comment;}

        //call
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'UpdateFileService',
            method: 'POST',
            jsonData: {
                ufsdc: ufsdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText && Ext.decode(response.responseText).UpdateFileServiceResult) {
                    var fsRecord = Ext.decode(response.responseText).UpdateFileServiceResult;
                    if (fsRecord.si == 7) //Picked up by GTH (in-between status)
                    {
                        if (me.recordPnl) {
                            me.recordPnl.record.statusId = fsRecord.si;
                        }
                        me.updateRecordPnl();
                    }
                    else {
                        me.unloadFileServiceRecord();
                    }
                } else {
                    eBook.Interface.showResponseError(response, me.title);
                    me.getEl().unmask();
                }
            },
            scope: me
        });
    },

    ///Notify the team that the files are missing
    onBtnNotifyTeamClicked: function (btn, e) {
        var me = this;
        me.getEl().mask("Sending mail");
        var clientId = eBook.Interface.currentClient.gthTeam5ClientId;
        Ext.Ajax.request({
            url: eBook.Service.GTH + 'Team5NotifyTeam',
            method: 'POST',
            jsonData: {ciacdc: {Id: clientId, Culture: this.ownerCt.ownerCt.period}},
            callback: function (options, success, response) {
                if (success) {
                    Ext.getCmp('GthTeam5MainPanel').getEl().unmask();
                    Ext.Msg.show({
                        title: 'Info',
                        msg: 'Mail sent',
                        buttons: Ext.Msg.OK,
                        fn: function (btn) {
                            if (btn == 'ok') {
                                Ext.getCmp('GthTeam5RepoPanel').standardFilter.pe = null;
                                Ext.getCmp('GthTeam5RepoPanel').standardFilter.ps = null;
                                Ext.getCmp('GthTeam5RepoPanel').hide();
                                Ext.getCmp('GthTeam5NotifyTeamBtn').hide();
                                Ext.getCmp('GthTeam5FilesMissingBtn').hide();
                                Ext.getCmp('GthTeam5FilesPanel').filelist.store.removeAll();
                                Ext.getCmp('GthTeam5TextFieldGfis').setValue("");
                                Ext.getCmp('GthTeam5FilesPanel').setTitle("Files");
                            }
                        },
                        icon: Ext.MessageBox.INFO,
                        scope: this
                    });
                } else {
                    alert('Mail not sent. Please contact an administrator');
                }
            },
            scope: this
        });
    }
});
Ext.reg('GTHTeam5', eBook.GTH.Team5);

eBook.GTH.Team5.FilesList = Ext.extend(Ext.list.ListView, {
    initComponent: function () {
        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                autoDestroy: true,
                root: 'GetDistinctFilesWithCounterResult',
                fields: eBook.data.RecordTypes.DistinctCounterFiles,
                baseParams: {
                    String: null
                },
                proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                    url: eBook.Service.file + 'GetDistinctFilesWithCounter',
                    criteriaParameter: 'csdc',
                    method: 'POST'
                }),
                listeners: {
                    load: function (store, records, options) {
                        Ext.getCmp('GthTeam5FilesPanel').setTitle('Files (' + records[0].get('ClientName') + ')');
                    }
                }
            }),
            multiSelect: false,
            loadingText: 'Loading files...',
            emptyText: eBook.Menu.Client_NoOpenFiles,
            reserveScrollOffset: false,
            cls: 'client-home-openFiles',
            autoHeight: true,
            autoScroll: true,
            singleSelect: true,
            listeners: {
                click: function (listv, index, node, e) {
                    this.ownerCt.ownerCt.period = listv.selected.elements[0].innerText;
                    if (listv.store.getAt(index).get('Counter') > 1) {
                        listv.ownerCt.ownerCt.repo.hide();
                        listv.ownerCt.ownerCt.buttonPnl.btnFilesMissing.hide();
                        listv.ownerCt.ownerCt.btnNotifyTeam.show();
                    } else {

                        eBook.Interface.currentFile = {
                            gthTeam5FileId: listv.store.getAt(index).get("FileId"),
                            gthTeam5StartDate: listv.store.getAt(index).get('StartDate'),
                            gthTeam5EndDate: listv.store.getAt(index).get('EndDate')
                        };
                        eBook.Interface.currentClient = {
                            gthTeam5ClientId: listv.store.getAt(index).get('ClientId')
                        };
                        listv.ownerCt.ownerCt.btnNotifyTeam.hide();
                        listv.ownerCt.ownerCt.repo.standardFilter.pe = listv.store.getAt(index).get('EndDate');
                        listv.ownerCt.ownerCt.repo.standardFilter.ps = listv.store.getAt(index).get('StartDate');
                        listv.ownerCt.ownerCt.repo.show();
                        listv.ownerCt.ownerCt.repo.reload();
                        listv.ownerCt.ownerCt.buttonPnl.btnFilesMissing.show();
                    }
                }
            },
            columns: [{
                header: '#Files',
                dataIndex: 'Counter',
                width: 0.10
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                dataIndex: 'StartDate',
                width: 0.45
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                dataIndex: 'EndDate',
                width: 0.45
            }]


        });
        eBook.GTH.Team5.FilesList.superclass.initComponent.apply(this, arguments);
    }
    /*
     , onMyBodyContext: function(dv, idx, nde, e) {
     //var el = e.getTarget();
     if (dv.ref == 'openFiles') {
     //eBook.Interface.center.clientMenu.mymenu.unHighlightTrashCan();
     if (!this.contextMenu) {
     this.contextMenu = new eBook.Client.ContextMenu({ scope: this });
     this.contextMenu.addItem({
     text: 'Delete',
     iconCls: 'eBook-icon-delete-16',
     handler: function() {
     this.scope.onDeleteClick(this.activeRecord);
     },
     ref: 'deleteFile',
     scope: this.contextMenu
     });
     }
     this.contextMenu.activeRecord = dv.store.getAt(idx);



     this.contextMenu.showMeAt(e);
     }

     }
     , onRestoreClickCallback: function(opts, success, resp) {
     if (success) {
     this.store.load({ params: { ClientId: this.activeClient} });
     }
     }
     */
});

Ext.reg('team5filelist', eBook.GTH.Team5.FilesList);