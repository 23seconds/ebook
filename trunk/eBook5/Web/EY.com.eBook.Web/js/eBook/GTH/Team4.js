/*
 eBook.GTH.FileUpload = Ext.extend(Ext.ux.form.FileUploadField, {
 validateValue:function(v) {
 var valid = eBook.GTH.FileUpload.superclass.validateValue.call(this,v);
 this.periodEnd.format('ddMMY');
 var rexp = new RegExp(this.enterprisNr + '_' +  + '/gi')
 }
 */


eBook.GTH.Team4_Process = Ext.extend(Ext.Panel, {
    initComponent: function () {
        Ext.apply(this, {
            layout: {type: 'vbox', align: 'stretch'}
            , border: false
            , items: [{xtype: 'GTHClientDetails', teamId: this.teamId, ref: 'clientDetails', height: 60}
                , {
                    xtype: 'panel',
                    layout: 'accordion',
                    cls: 'gth-team2-accord',
                    animate: true,
                    flex: 1,
                    margins: {top: 0, bottom: 10, left: 0, right: 0},
                    items:
                    [
                        {
                            xtype: 'panel',
                            step: 1,
                            title: 'Step 1: Excel template, Teammembers, eBook files',
                            layout: {type: 'hbox', align: 'stretch'}
                            ,
                            listeners: {expand: {fn: this.onAccordionPaneExpand, scope: this}}
                            ,
                            items: [
                                {
                                    xtype: 'panel',
                                    title: 'Excel, NBB & Teammembers',
                                    flex: 1,
                                    layout: {type: 'vbox', align: 'stretch'}

                                    ,
                                    items: [{
                                        xtype: 'GTHTeam4DownloadFiles',
                                        height: 80,
                                        autoScroll: true,
                                        teamId: this.teamId,
                                        ref: '../../../downloadfiles',
                                        flex: 1
                                    }
                                        , {
                                            xtype: 'eBook.PMT.TeamGrid',
                                            ref: '../../../teamgrid',
                                            includeMail: true,
                                            includeEbook: false,
                                            storeCfg: {
                                                selectAction: 'GetClientTeamDepartment',
                                                fields: eBook.data.RecordTypes.TeamMember,
                                                serviceUrl: eBook.Service.client,
                                                idField: 'Id',
                                                autoDestroy: true,
                                                autoLoad: false,
                                                criteriaParameter: 'ciddc',
                                                baseParams: {
                                                    Department: 'ACR'
                                                }

                                            },
                                            flex: 1
                                        }]
                                }
                                // REMOVE REPOSITORY
                                , {
                                    xtype: 'panel',
                                    layout: {type: 'vbox', align: 'stretch'},
                                    flex: 1,
                                    items: [
                                        /*{ //Removed on 2015/10/16 per request
                                         xtype: 'GTHTeam4Files',
                                         title: 'eBook files',
                                         ref: '../../../filesInfo',
                                         flex: 2, autoScroll: true
                                         },*/
                                        {
                                            xtype: 'GTHFilesMeta',
                                            title: 'eBook files meta (Peergroup)',
                                            ref: '../../../GTHFilesMeta',
                                            flex: 1,
                                            autoScroll: true
                                        }
                                    ]
                                }
                            ]
                        }
                        ,{
                            xtype: 'GTHTeam4UploadFinHealthDocs',
                            ref: '../uploadexcel',
                            step: 2,
                            title: 'Step 2: Final: upload definitive excel',
                            listeners: {expand: {fn: this.onAccordionPaneExpand, scope: this}}
                        }
                    ]
                }
                , {
                    xtype: 'button',
                    text: 'Start new client',
                    ref: 'newclientstart',
                    handler: this.onNewClientClick,
                    scope: this,
                    height: 60,
                    cls: 'eb-massive-btn'
                }]
        });
        eBook.GTH.Team4_Process.superclass.initComponent.apply(this, arguments);
    }
    , initStep: function (owner) {
        //owner.hideStop(); can stop in the middle or start new
        this.client = owner.client;
        this.ownerSteps = owner;
        var cl = this.client;
        this.clientId = cl.Id; // not actual client id => is GTH_team_status ID
        this.stepTitle = 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel for ' + cl.ClientName;
        this.clientDetails.initStep(cl);
        if (this.uploadexcel && this.uploadexcel.rendered) this.uploadexcel.reset();
        this.downloadfiles.getFiles(cl);
        this.teamgrid.store.load({params: {Id: cl.ClientId}});

        //this.filesInfo.getFiles(cl.ClientId);

        // deleted repository for this team so removed mask
        //this.repos.getEl().mask("Validating &amp; processing files", 'x-mask-loading');
        //var rt = this.repos.getRootNode();
        //var loader = rt.loader || rt.attributes.loader || this.repos.getLoader();
        //loader.ClientId = cl.ClientId;
        //loader.filter = { ps: cl.StartDate, pe: cl.EndDate };
        //rt.loader = loader;
        //rt.reload(this.onReloadedRepos, this);
    }
    , onAccordionPaneExpand: function (panel) {
        this.newclientstart.enable();
        if (panel.step == 2) {
            this.newclientstart.disable();
        }
    }
    , onReloadedRepos: function () {
        this.repos.getEl().unmask();
    }
    , onNewClientClick: function () {
        this.ownerSteps.moveNextStep();
    }
});
Ext.reg('GTHTeam4Process', eBook.GTH.Team4_Process);


// download empty excel template
// list files team 1 of this period.
// Either globalize (excel via config) or team 4
eBook.GTH.Team4_DownloadFiles = Ext.extend(Ext.Panel, {
    initComponent: function () {
        this.templTeam4 = new Ext.XTemplate('<div class="gth-team2-fileslist">'
            // WAITING FOR EXCEL TEMPLATE GTHTeam4_Template !!!!!!!!!!!!!!!!!!!!!
            , '<div class="gth-team2-exceltempl"><a href="Repository/FinancialHealth_20151119_TemplateInclCompetitorAnalysis.xlsm" target="_blank">Download empty Excel template</a></div>',
            /*, '<div class="gth-team2-repo1">TEAM 1 Files:<ul>'
             , '<tpl for="files"><li><a href="{[this.getLink(values)]}" target="_blank">{text} <i><tpl for="Item">({Extension})</tpl></i></a></li></tpl>'
             , '</ul></div>'*/
            {
                getLink: function (values) {
                    return "Repository/" + eBook.getGuidPath(values.Item.ClientId) + values.Item.PhysicalFileId + values.Item.Extension
                }
            });

        this.templTeam1 = new Ext.XTemplate('<div class="gth-team2-fileslist">'
            // WAITING FOR EXCEL TEMPLATE GTHTeam4_Template !!!!!!!!!!!!!!!!!!!!!
            , '<div class="gth-team2-repo1">Present Files:<ul>'
            , '<tpl for="files"><li><a href="{[this.getLink(values)]}" target="_blank">{text} <i><tpl for="Item">({Extension})</tpl></i></a></li></tpl>'
            , '</ul></div>', {
                getLink: function (values) {
                    return "Repository/" + eBook.getGuidPath(values.Item.ClientId) + values.Item.PhysicalFileId + values.Item.Extension
                }
            });
        if (this.teamId == "1") {
            this.templTeam1.compile();
        } else if (this.teamId == "4") {
            this.templTeam4.compile();
        }

        Ext.apply(this, {html: 'loading data'});
        eBook.GTH.Team4_DownloadFiles.superclass.initComponent.apply(this, arguments);
    }
    , getFiles: function (cl) {
        //if (cl.StartDate != null && !Ext.isDate(cl.StartDate)) cl.StartDate = Ext.data.Types.WCFDATE.convert(cl.StartDate);
        //if (cl.EndDate != null && !Ext.isDate(cl.EndDate)) cl.EndDate = Ext.data.Types.WCFDATE.convert(cl.EndDate);
        //cl.StartDate = null;
        //cl.EndDate = null;
        this.getEl().mask("Loading data team 1", 'x-mask-loading');

        Ext.Ajax.request({
            url: eBook.Service.repository + 'QueryFiles'
            ,
            method: 'POST'
            ,
            params: Ext.encode({
                cqfdc: {
                    cid: cl.ClientId,
                    sid: '3787F2A4-112D-4589-A231-E9B95BDCFCD9',
                    ps: cl.StartDate,
                    pe: cl.EndDate
                }
            })
            ,
            callback: this.onFilesObtained
            ,
            scope: this
        });
    }
    , onFilesObtained: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var o = Ext.decode(resp.responseText).QueryFilesResult;
            if (this.teamId == "1") {
                this.body.update(this.templTeam1.apply({files: o}));
            } else if (this.teamId == "4") {
                this.body.update(this.templTeam4.apply({files: o}));
            }

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam4DownloadFiles', eBook.GTH.Team4_DownloadFiles);


//eBook.GTH.GTHUploadSupDocs = Ext.extend(Ext.form.FormPanel, {
eBook.GTH.GTHUploadFinHealthDocs = Ext.extend(Ext.form.FormPanel, {
    initComponent: function () {
        Ext.apply(this, {
            fileUpload: true,
            border: false,
            bodyStyle: 'padding:10px;',
            //layout: { type: 'vbox', align: 'stretch' },
            autoScroll: true,
            items: [
                new Ext.ux.form.FileUploadField({
                    width: 350,
                    labelWidth: 50,
                    ref: 'excel',
                    allowBlank: false,
                    fieldLabel: 'Final Excel Template',
                    fileTypeRegEx: /((\.xls)(x|m){0,1})$/i
                })
                , {
                    xtype: 'combo',
                    ref: 'competitionAnalysis',
                    fieldLabel: 'Competition analysis',
                    name: 'competitionAnalysis',
                    triggerAction: 'all',
                    typeAhead: false,
                    selectOnFocus: true,
                    autoSelect: true,
                    allowBlank: true,
                    forceSelection: true,
                    valueField: 'field1',
                    displayField: 'field1',
                    editable: false,
                    listWidth: 300,
                    labelWidth: 50,
                    mode: 'local',
                    store: ["Not included","Included"],
                    width: 300
                }
                , {
                    xtype: 'label',
                    text: '/!\\ IMPORTANT: This is the final step for each Client/period. When uploading the excel and pdf file, you will mark this job as "finalized".',
                    style: 'font-weight:bold;'
                }
                , {
                    xtype: 'button',
                    width: 350,
                    text: 'Upload file',
                    handler: this.onUploadClick,
                    scope: this,
                    height: 60,
                    cls: 'eb-massive-btn',
                    style: 'margin-top:20px;'
                }
            ]
        });
        eBook.GTH.GTHUploadFinHealthDocs.superclass.initComponent.apply(this, arguments);
    }
    , reset: function () {
        var me = this;
        if (me.excel && me.excel.rendered) me.excel.reset();
        if (me.pdf && me.pdf.rendered) me.pdf.reset();
        if (me.competitionAnalysis)
        {
            var store = me.competitionAnalysis.getStore(),
                record = store ? store.getAt(0) : null;

            me.competitionAnalysis.setValue(record.get("field1"));
        }
    }
    , onUploadClick: function () {
        var f = this.getForm();
        if (this.excel.isValid()) {
            f.isValid = function () {
                return true;
            };
            this.ownerCt.ownerCt.ownerSteps.hideStop();
            f.submit({
                url: 'UploadMultiple.aspx',
                waitMsg: 'Uploading files',
                success: this.successUpload,
                failure: this.failedUpload,
                scope: this
            });
        } else {
            alert("Selected file/upload field contains invalid or no data.");
        }
    }
    , successUpload: function (fp, o) {
        var exc = o.result.files[0];
        var cl = this.ownerCt.ownerCt.client;
        var fle;
        this.ownerCt.ownerCt.getEl().mask("Validating &amp; processing files", 'x-mask-loading');

        var fls = [];
        for (var i = 0; i < o.result.files.length; i++) {
            fle = o.result.files[i];
            fls.push({
                Name: fle.originalFile,
                Start: cl.StartDate,
                End: cl.EndDate,
                Id: fle.id,
                Extension: fle.extension,
                ContentType: fle.contentType,
                fileName: fle.file,
                fileNameParentheticalRemark: fp.getFieldValues().competitionAnalysis == "Not included" ? null : "Competition analysis included"
            });
        }


        Ext.Ajax.request({
            url: eBook.Service.GTH + 'ProcessFilesTeam2' // saves to repository GTH folder team 4 - financial health
            ,
            method: 'POST'
            ,
            params: Ext.encode({
                cpfdc: {
                    StructureId: '1b444980-2d74-45a0-95e6-d9bc196f07f5',
                    Files: fls,
                    TeamId: this.teamId,
                    Status: 'FINAL',
                    PersonId: eBook.User.personId,
                    GTHClientId: this.ownerCt.ownerCt.clientId
                }
            })
            ,
            callback: this.handleFileProcessing
            ,
            scope: this
        });

        //process o.result.file
        /*this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');

         ¨*/

    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, "UPLOAD FAILED!!")
        this.ownerCt.ownerCt.ownerSteps.showStop();
        this.ownerCt.ownerCt.getEl().unmask();
    }
    , handleFileProcessing: function (opts, success, resp) {
        this.ownerCt.ownerCt.getEl().unmask();
        this.ownerCt.ownerCt.ownerSteps.showStop();
        if (success) {
            this.ownerCt.ownerCt.ownerCt.moveNextStep();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});

Ext.reg('GTHTeam4UploadFinHealthDocs', eBook.GTH.GTHUploadFinHealthDocs);


eBook.GTHFiles = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
        , '<tpl for="GetFileInfosResult">'
        , '<div id={id} class="eBook-GTH-team4-file">'
        , '<div> {n} </div>'
        , '<tpl><i>{[this.getPeriod(values)]}</i></tpl>'
        , '</div>'
        , '</tpl>'
        , '</tpl>'
        , '<div class="x-clear"></div>', {
            getPeriod: function (values) {
                var sDate = Ext.data.Types.WCFDATE.convert(values.sd),
                    eDate = Ext.data.Types.WCFDATE.convert(values.ed);
                return sDate.format("d/m/Y") + ' - ' + eDate.format("d/m/Y");
            }
        }
    )

    , initComponent: function () {
        Ext.apply(this, {
            border: false
        });
        eBook.GTHFiles.superclass.initComponent.apply(this, arguments);
    }
    , onRender: function (ct, pos) {
        eBook.GTHFiles.superclass.onRender.call(this, ct, pos);
        //        this.body.update(this.mainTpl.apply(this.data));
    }
    , afterRender: function () {
        eBook.GTHFiles.superclass.afterRender.call(this);

        // set events
        this.el.on('click', this.onBodyClick, this);
    }

    , onBodyClick: function (e, t, o) {
        var file = e.getTarget('.eBook-GTH-team4-file');

        var cicdc = {
            cicdc: {
                Id: file.id
                , Culture: 'en-US'
            }
        }

        this.getEl().mask("Loading files", 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.output + 'GetAnnualAccountsBundle'
            , method: 'POST'
            , FileId: cicdc.Id
            , params: Ext.encode(cicdc)
            , scope: this
            , callback: function (opts, success, resp) {
                this.getEl().unmask();

                if (success) {
                    // alert(Ext.decode(resp.responseText).GetAnnualAccountsBundleResult);
                    window.open(Ext.decode(resp.responseText).GetAnnualAccountsBundleResult);
                } else {
                    eBook.Interface.showResponseError(resp, this.title);
                }

            }
        });
    }

    , getFiles: function (cliendID) {
        this.getEl().mask("Loading files", 'x-mask-loading');

        var cfdc = {
            cfdc: {
                ClientId: cliendID
                , MarkedForDeletion: 'false'
                , Deleted: 'false'
                , Closed: null
                , query: null
                , StartDate: null
            }
        };

        Ext.Ajax.request({
            url: eBook.Service.file + 'GetFileInfos'
            , method: 'POST'
            , params: Ext.encode(cfdc)
            , scope: this
            , callback: this.handleFileProcessing
        });
    }

    , handleFileProcessing: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.data = resp.responseText;
            this.body.update(this.mainTpl.apply(Ext.decode(this.data)));

            if (Ext.decode(this.data).GetFileInfosResult.length > 0) {
                this.refOwner.GTHFilesMeta.getFilesMeta(Ext.decode(this.data).GetFileInfosResult[0].cid);
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam4Files', eBook.GTHFiles);

eBook.GTHFilesMeta = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
        , '<div  class="eBook-GTH-team4-file">'
        , '<div> {n} </div>'
        , '<tpl><i>{ond} {nme}</i></tpl>'
        , '</div>'
        , '</tpl>'
        , '<div class="x-clear"></div>'
    )

    , initComponent: function () {
        Ext.apply(this, {
            border: false
        });
        eBook.GTHFilesMeta.superclass.initComponent.apply(this, arguments);
    }
    , onRender: function (ct, pos) {
        eBook.GTHFilesMeta.superclass.onRender.call(this, ct, pos);
        //        this.body.update(this.mainTpl.apply(this.data));
    }


    , getFilesMeta: function (clientId) {
        this.getEl().mask("Loading files", 'x-mask-loading');

        var cidc = {
            cidc: {Id: clientId}
        };

        Ext.Ajax.request({
            url: eBook.Service.file + 'GetMetaByClientId'
            , method: 'POST'
            , params: Ext.encode(cidc)
            , scope: this
            , callback: this.handleFileProcessing
        });
    }

    , handleFileProcessing: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.data = resp.responseText;
            var arr = [];
            var filesMeta = Ext.decode(this.data).GetMetaByClientIdResult;
            if (filesMeta.length > 0) {
                for (var i = 0; i < filesMeta.length; i++) {
                    if (filesMeta[i].dta.pgroup.length > 0) {
                        for (var j = 0; j < filesMeta[i].dta.pgroup.length; j++) {
                            var x = {}
                            x.ond = filesMeta[i].dta.pgroup[j].ond;
                            x.nme = filesMeta[i].dta.pgroup[j].nme;
                            arr.push(x);
                        }
                    }
                }
            }
            this.body.update(this.mainTpl.apply(arr));
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHFilesMeta', eBook.GTHFilesMeta);

