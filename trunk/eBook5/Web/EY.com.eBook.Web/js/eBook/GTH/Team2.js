/*
eBook.GTH.FileUpload = Ext.extend(Ext.ux.form.FileUploadField, {
    validateValue:function(v) {
        var valid = eBook.GTH.FileUpload.superclass.validateValue.call(this,v);
        this.periodEnd.format('ddMMY');
        var rexp = new RegExp(this.enterprisNr + '_' +  + '/gi')
    }
*/

eBook.GTH.Team2_Process = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: { type: 'vbox', align: 'stretch' }
            , border: false
              , items: [{ xtype: 'GTHClientDetails', teamId: this.teamId, ref: 'clientDetails', height: 60 }
                        , { xtype: 'panel', layout: 'accordion', cls: 'gth-team2-accord', animate: true, flex: 1, margins: { top: 0, bottom: 10, left: 0, right: 0 },
                            items: [{ xtype: 'panel', step: 1, title: 'Step 1: Excel template, NBB files, Teammembers, File repository', layout: { type: 'hbox', align: 'stretch' }
                                    , listeners: { expand: { fn: this.onAccordionPaneExpand, scope: this} }
                                    , items: [
                                     { xtype: 'panel', title: 'Excel, NBB & Teammembers', flex: 1, layout: { type: 'vbox', align: 'stretch' }

                                        , items: [{ xtype: 'GTHTeam2DownloadFiles', height: 60, teamId: this.teamId, ref: '../../../downloadfiles', flex: 1 }
                                               , { xtype: 'eBook.PMT.TeamGrid', ref: '../../../teamgrid', includeMail: true, includeEbook: false, storeCfg: {
                                                   selectAction: 'GetClientTeamDepartment',
                                                   fields: eBook.data.RecordTypes.TeamMember,
                                                   serviceUrl: eBook.Service.client,
                                                   idField: 'Id',
                                                   autoDestroy: true,
                                                   autoLoad: false,
                                                   criteriaParameter: 'ciddc',
                                                   baseParams: {
                                                       Department: 'ACR'
                                                   }

                                               }, flex: 1
}]
                                     }, { xtype: 'repository', title: 'Repository', ref: '../../repos', readOnly: true, startFrom: { type: 'period', id: '77C537A1-38C6-4868-AAE0-7CD6AA57E66F' }, flex: 1 }
                                    ]
                            }
                                    , { xtype: 'GTHTeam2UploadSupDocs', ref: '../uploadexcel', step: 2, title: 'Step 2: Final: upload definitive excel', listeners: { expand: { fn: this.onAccordionPaneExpand, scope: this}} }
                                ]
                        }
                    , { xtype: 'button', text: 'Start new client', ref: 'newclientstart', handler: this.onNewClientClick, scope: this, height: 60, cls: 'eb-massive-btn'}]
        });
        eBook.GTH.Team2_Process.superclass.initComponent.apply(this, arguments);
    }
    , initStep: function(owner) {
        //owner.hideStop(); can stop in the middle or start new
        this.client = owner.client;
        this.ownerSteps = owner;
        var cl = this.client;
        this.clientId = cl.Id;
        this.stepTitle = 'STEP 2: Prepare excel template, Check files with EY Belgium, upload final excel for ' + cl.ClientName;
        this.clientDetails.initStep(cl);
        if (this.uploadexcel && this.uploadexcel.rendered) this.uploadexcel.reset();
        this.downloadfiles.getFiles(cl);
        this.teamgrid.store.load({ params: { Id: cl.ClientId} });
        this.repos.getEl().mask("Validating &amp; processing files", 'x-mask-loading');
//        var rt = this.repos.getRootNode();
//        var loader = rt.loader || rt.attributes.loader || this.repos.getLoader();
//        loader.ClientId = cl.ClientId;
//        loader.filter = { ps: cl.StartDate, pe: cl.EndDate };
//        rt.loader = loader;
        //        rt.reload(this.onReloadedRepos, this);
        var rt = this.repos.getRootNode();
        rt.removeAll();
        this.repos.ClientId = cl.ClientId;
        this.repos.filter = { ps: cl.StartDate, pe: cl.EndDate };
        this.repos.getLoader().load(rt, this.onReloadedRepos, this);
    }
    , onAccordionPaneExpand: function(panel) {
        this.newclientstart.enable();
        if (panel.step == 2) {
            this.newclientstart.disable();
        }
    }
    , onReloadedRepos: function() {
        this.repos.getEl().unmask();
    }
    , onNewClientClick: function() {
        this.ownerSteps.moveNextStep();
    }
});
Ext.reg('GTHTeam2Process', eBook.GTH.Team2_Process);


// download empty excel template
// list files team 1 of this period.
eBook.GTH.Team2_DownloadFiles = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.templ = new Ext.XTemplate('<div class="gth-team2-fileslist">'
                                        , '<div class="gth-team2-exceltempl"><a href="Repository/GTHTeam2_Template_v8.xlsx" target="_blank">Download empty Excel template</a></div>'
                                        , '<div class="gth-team2-repo1">TEAM 1 Files:<ul>'
                                        , '<tpl for="files"><li><a href="{[this.getLink(values)]}" target="_blank">{text} <i><tpl for="Item">({Extension})</tpl></i></a></li></tpl>'
                                        , '</ul></div>', {
                                            getLink: function(values) {
                                                return "Repository/" + eBook.getGuidPath(values.Item.ClientId) + values.Item.PhysicalFileId + values.Item.Extension
                                            }
                                        });
        this.templ.compile();
        Ext.apply(this, { html: 'loading data' });
        eBook.GTH.Team2_DownloadFiles.superclass.initComponent.apply(this, arguments);
    }
    , getFiles: function(cl) {
        if (cl.StartDate != null && !Ext.isDate(cl.StartDate)) cl.StartDate = Ext.data.Types.WCFDATE.convert(cl.StartDate);
        if (cl.EndDate != null && !Ext.isDate(cl.EndDate)) cl.EndDate = Ext.data.Types.WCFDATE.convert(cl.EndDate);
        this.getEl().mask("Loading data team 1", 'x-mask-loading');

        Ext.Ajax.request({
            url: eBook.Service.repository + 'QueryFiles'
            , method: 'POST'
            , params: Ext.encode({ cqfdc: { cid: cl.ClientId, sid: '3787F2A4-112D-4589-A231-E9B95BDCFCD9', ps: cl.StartDate, pe: cl.EndDate} })
             , callback: this.onFilesObtained
            , scope: this
        });
    }
    , onFilesObtained: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var o = Ext.decode(resp.responseText).QueryFilesResult;
            this.body.update(this.templ.apply({ files: o }));
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});
Ext.reg('GTHTeam2DownloadFiles', eBook.GTH.Team2_DownloadFiles);

eBook.GTH.GTHUploadSupDocs = Ext.extend(Ext.form.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            fileUpload: true,
            border: false,
            bodyStyle: 'padding:10px;',
            //layout: { type: 'vbox', align: 'stretch' },
            autoScroll: true,
            items: [
                new Ext.ux.form.FileUploadField({ width: 350, ref: 'excel', allowBlank: false, fieldLabel: 'Final Excel Template', fileTypeRegEx: /((\.xls)(x){0,1})$/i })
            //new Ext.ux.form.FileUploadField({ width: 350, ref: 'pdf', allowBlank: false, fieldLabel: 'Executive summary', fileTypeRegEx: /((\.pdf))$/i })
                , { xtype: 'label', text: '/!\\ IMPORTANT: This is the final step for each Client/period. When uploading the excel and pdf file, you will mark this job as "finalized".', style: 'font-weight:bold;' }
                , { xtype: 'button', width: 350, text: 'Upload file', handler: this.onUploadClick, scope: this, height: 60, cls: 'eb-massive-btn', style: 'margin-top:20px;' }
            ]
        });
        eBook.GTH.GTHUploadSupDocs.superclass.initComponent.apply(this, arguments);
    }
    , reset: function() {
        if (this.excel && this.excel.rendered) this.excel.reset();
        //if (this.pdf && this.pdf.rendered) this.pdf.reset();
    }
//    , onUploadClick: function() {
//        var f = this.getForm();
//        if (this.excel.isValid() && this.pdf.isValid()) {
//            f.isValid = function() { return true; };
//            this.ownerCt.ownerCt.ownerSteps.hideStop();
//            f.submit({
//                url: 'UploadMultiple.aspx',
//                waitMsg: 'Uploading files',
//                success: this.successUpload,
//                failure: this.failedUpload,
//                scope: this
//            });
//        } else {
//            alert("Selected file/upload field contains invalid or no data.");
//        }
    //    }
    , onUploadClick: function() {
        var f = this.getForm();
        if (this.excel.isValid()) {
            f.isValid = function() { return true; };
            this.ownerCt.ownerCt.ownerSteps.hideStop();
            f.submit({
                url: 'UploadMultiple.aspx',
                waitMsg: 'Uploading files',
                success: this.successUpload,
                failure: this.failedUpload,
                scope: this
            });
        } else {
            alert("Selected file/upload field contains invalid or no data.");
        }
    }
     , successUpload: function(fp, o) {
         var exc = o.result.files[0];
         var cl = this.ownerCt.ownerCt.client;
         var fle;
         this.ownerCt.ownerCt.getEl().mask("Validating &amp; processing files", 'x-mask-loading');

         var fls = [];
         for (var i = 0; i < o.result.files.length; i++) {
             fle = o.result.files[i];
             fls.push({ Name: fle.originalFile, Start: cl.StartDate, End: cl.EndDate, Id: fle.id, Extension: fle.extension, ContentType: fle.contentType, fileName: fle.file });
         }


         Ext.Ajax.request({
             url: eBook.Service.GTH + 'ProcessFilesTeam2'
            , method: 'POST'
            , params: Ext.encode({ cpfdc: { StructureId: 'e2e2440a-6e08-444a-9494-052d8e8c3dc2', Files: fls, TeamId: this.teamId, Status: 'FINAL', PersonId: eBook.User.personId, GTHClientId: this.ownerCt.ownerCt.clientId} })
             , callback: this.handleFileProcessing
            , scope: this
         });

         //process o.result.file
         /*this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
       
        ¨*/

     }
    , failedUpload: function(fp, o) {
        eBook.Interface.showError(o.message, "UPLOAD FAILED!!")
        this.ownerCt.ownerCt.ownerSteps.showStop();
        this.ownerCt.ownerCt.getEl().unmask();
    }
    , handleFileProcessing: function(opts, success, resp) {
        this.ownerCt.ownerCt.getEl().unmask();
        this.ownerCt.ownerCt.ownerSteps.showStop();
        if (success) {
            this.ownerCt.ownerCt.ownerCt.moveNextStep();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
});

Ext.reg('GTHTeam2UploadSupDocs', eBook.GTH.GTHUploadSupDocs);