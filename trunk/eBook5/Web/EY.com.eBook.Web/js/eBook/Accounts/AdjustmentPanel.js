
eBook.Accounts.Adjustments.MainTpl = new Ext.XTemplate('<tpl for=".">'
                    , '<div class="eb-adjs-container">'
                        , '<div class="eb-adjs-item-header">'
                            , '<div class="eb-adjs-item-header-nr">ADJ</div>'
                            , '<div class="eb-adjs-item-header-trans">DESCRIPTIONS</div>'
                            , '<div class="eb-adjs-item-header-SaldoN-1">SALDO N-1<br/>{[fm.decimalFormat(values.ps,2)]}</div>'
                            , '<div class="eb-adjs-item-header-Saldo">SALDO N<br/>{[fm.decimalFormat(values.s,2)]}</div>'
                        , '</div>'
                         , '<div class="eb-adjs-items-body">'
                        ,'<tpl for="adj">'
                            , '{[this.renderAdjustment(values)]}'
                        , '</tpl>'
                        , '<tpl if="values.adj.length==0">'
                        , '<div class="eb-adjs-items-nodata">ACCOUNT HAS NO ADJUSTMENTS</div>'
                        , '</tpl>'
                        , '</div>'
                   ,'</div>'
                 ,'</tpl>'
                 , {
                    renderAdjustment:function(values) {
                        return eBook.Accounts.Adjustments.AdjustmentTpl.apply(values);
                    }
                });

eBook.Accounts.Adjustments.AdjustmentTpl = new Ext.XTemplate(
                        '<tpl for=".">'
                            , '<div class="eb-adjs-item" id="eb-adjs-item-{adjNr}" adjustment="{adjNr}">'
                                , '<div class="eb-adjs-item-body">'
                                    , '<div class="eb-adjs-item-nr">{adjNr}</div>'
                                    , '<div class="eb-adjs-item-trans">'
                                        , '<tpl for="this.getDefaultDesc(values.ads)">'
                                            , '<div class="eb-adjs-item-trans-header">'
                                                , '<div id="eb-adjs-item-{parent.adjNr}-trans-header-txt" class="eb-adjs-item-trans-header-txt">{d}</div>'
                                                , '<div class="eb-adjs-item-trans-header-edit"></div>'
                                            , '</div>'
                                        , '</tpl>'
                                        , '<div class="eb-adjs-item-trans-body">'
                                            , '<div class="eb-adjs-item-trans-edit">'
                                                , '<tpl for="ads">'
                                                    , '<div class="eb-adjs-item-trans-row">'
                                                            , '<div class="eb-adjs-item-trans-row-language">{[this.getLanguage(values.c)]}</div>'
                                                            , '<div class="eb-adjs-item-trans-row-description">'
                                                                , '<input type="text" class="eb-adjs-item-trans-row-desc" id="eb-adj-desc-{parent.adjNr}-{c}" value="{d}"  adjustment="{parent.adjNr}" culture="{c}" />'
                                                            , '</div>'
                                                    , '</div>'
                                                , '</tpl>'
                                            , '</div>'
                                            , '<div class="eb-adjs-item-trans-edit-close"></div>'
                                        , '</div>'
                                    , '</div>'
                                    , '<div class="eb-adjs-item-SaldoN-1"><input class="eb-adjs-item-Saldo-input"  id="eb-adj-desc-{adjNr}-previous" type="text" value="{ps}"  adjustment="{adjNr}"/></div>'
                                    , '<div class="eb-adjs-item-Saldo"><input class="eb-adjs-item-Saldo-input"  id="eb-adj-desc-{adjNr}-current" type="text" value="{s}" adjustment="{adjNr}"/></div>'
                                , '</div>'
                                , '<tpl if="values.adjNr!=\'999\'"><div class="eb-adjs-item-delete"></div></tpl>'
                            , '</div>'
                        , '</tpl>'
                        , {
                            getDefaultDesc: function(descs) {
                                var o = descs[0];
                                for (var i = 0; i < descs.length; i++) {
                                    if (descs[i].c == eBook.Interface.Culture) {
                                        return descs[i];
                                    }
                                }
                                return o;
                            }
                            , getLanguage: function(culture) {
                                return eBook.Language.Languages[culture.replace('-', '_')];
                            }
                        });

                        eBook.Accounts.Adjustments.Panel = Ext.extend(Ext.Panel, {
                            initComponent: function() {
                                // Ext.apply(this,{html:'<div class="eb-adjs-container">&nbsp;</div>' });
                                Ext.apply(this, {
                                    layout: 'fit', tbar: [{
                                        ref: 'addadj',
                                        text: 'Add Adjustment',
                                        iconCls: 'eBook-icon-add-16',
                                        scale: 'small',
                                        iconAlign: 'left',
                                        disabled: false,
                                        handler: this.onAddAdjustmentClick,
                                        scope: this
                                    }, {
                                        ref: 'delAll',
                                        text: 'Clear all',
                                        iconCls: 'eBook-icon-delete-16',
                                        scale: 'small',
                                        iconAlign: 'left',
                                        disabled: false,
                                        handler: this.onClearAllClick,
                                        scope: this
}]

                                    });

                                    eBook.Accounts.Adjustments.Panel.superclass.initComponent.apply(this, arguments);
                                }
    , onRender: function(ct, pos) {
        eBook.Accounts.Adjustments.Panel.superclass.onRender.apply(this, arguments);
        if (this.dta == null) {
            this.dta = { adj: [] };
        }
        //        this.el.dom.innerHTML = '<div class="eb-adjs-container">&nbsp;</div>';
        //        this.contEl = this.el.child('.eb-adjs-container');
        this.loadAccount(this.dta);

        this.el.on('click', this.onBodyClick, this);
        this.el.on('mouseover', this.onBodyOver, this);
        this.el.on('mouseout', this.onBodyOut, this);

    }
    , onBodyOver: function(e, t, o) {
        var isDelete = e.getTarget('.eb-adjs-item-delete');

        if (isDelete) {
            var par = Ext.get(e.getTarget('.eb-adjs-item'));
            par.setStyle('background-color', 'rgba(255,0,0,0.1)');
        }
    }
    , onBodyOut: function(e, t, o) {
        var isDelete = e.getTarget('.eb-adjs-item-delete');

        if (isDelete) {
            var par = Ext.get(e.getTarget('.eb-adjs-item'));
            par.setStyle('background-color', '');

        }
    }
    , onBodyClick: function(e, t, o) {
        var isEdit = e.getTarget('.eb-adjs-item-trans-header-edit');
        var isSave = e.getTarget('.eb-adjs-item-trans-edit-close');
        var isDelete = e.getTarget('.eb-adjs-item-delete');

        var par = Ext.get(e.getTarget('.eb-adjs-item'));

        if (isEdit) {
            par.addClass('eb-adjs-item-editing');
        } else if (isSave) {
            par.removeClass('eb-adjs-item-editing');
        } else if (isDelete) {
            var adj = par.getAttribute("adjustment");
            var idx = parseInt(adj) - 1;
            this.dta.adj.remove(this.dta.adj[idx]);
            this.reIndexData();
        }

    }
    , reIndexData: function() {
        if (this.dta.adj[0].adjNr == "999") {
            this.dta.adj = [];
        } else {
            this.calculateFinal();
            var nl, fr, en;
            for (var j = 0; j < this.dta.ads.length; j++) {
                switch (this.dta.ads[j].c) {
                    case 'nl-BE':
                        nl = this.dta.ads[j].d;
                        break;
                    case 'fr-FR':
                        fr = this.dta.ads[j].d;
                        break;
                    case 'en-US':
                        en = this.dta.ads[j].d;
                        break;
                }
            }
            for (var i = 0; i < this.dta.adj.length; i++) {
                if (this.dta.adj[i].adjNr != '999') {

                    var adjNr = String.leftPad('' + (i + 1), 3, '0');
                    this.dta.adj[i].adjNr = adjNr;
                    this.dta.adj[i].inr = this.dta.inr + '.' + adjNr;
                    for (var j = 0; j < this.dta.adj[i].ads.length; j++) {
                        this.dta.adj[i].ads[j].inr = this.dta.inr + '.' + adjNr;
                        if (this.dta.adj[i].ads[j].d.indexOf('ADJ-') > -1) {
                            switch (this.dta.ads[j].c) {
                                case 'nl-BE':
                                    this.dta.adj[i].ads[j].d = nl;
                                    break;
                                case 'fr-FR':
                                    this.dta.adj[i].ads[j].d = fr;
                                    break;
                                case 'en-US':
                                    this.dta.adj[i].ads[j].d = en;
                                    break;
                            }
                            this.dta.adj[i].ads[j].d += ' ADJ-' + adjNr;
                        }
                    }
                }
            }

        }
        this.loadAccount(this.dta);

    }
    , onClearAllClick: function() {
        this.dta.adj = [];
        this.loadAccount(this.dta);
    }
    , onAddAdjustmentClick: function() {
        var pAdj = String.leftPad('' + this.maxAdj, 3, '0');
        this.maxAdj++;
        var sAdj = String.leftPad('' + this.maxAdj, 3, '0');
        var fnl = {
            fid: this.dta.fid
                        , inr: this.dta.inr + '.' + sAdj
                        , adjNr: sAdj
                        , ads: []
                        , s: 0
                        , ps: 0
        };
        for (var i = 0; i < this.dta.ads.length; i++) {
            fnl.ads.push({
                fid: this.dta.fid,
                inr: this.dta.inr + '.' + sAdj,
                c: this.dta.ads[i].c,
                d: this.dta.ads[i].d + ' ADJ-' + sAdj
            });
        }
        this.dta.adj.push(fnl);
        this.dta.adj.sort(function(a, b) {
            if (a.adjNr < b.adjNr) return -1;
            if (a.adjNr > b.adjNr) return 1;
            return 0;
        });
        var el;
        if (this.maxAdj == 1) {
            this.renderFinal(0, 0, -1);
            this.contEl = eBook.Accounts.Adjustments.MainTpl.overwrite(this.body, this.dta, true);
        } else {
            el = eBook.Accounts.Adjustments.AdjustmentTpl.insertAfter('eb-adjs-item-' + pAdj, fnl, true);
        }
        this.renderFormFields(el);
    }
    , onDescriptionChange: function(el, nw, old) {
        var idx = parseInt(el.adjustment) - 1;
        if (el.adjustment == "999") idx = this.dta.adj.length - 1;
        for (var i = 0; i < this.dta.adj[idx].ads.length; i++) {
            if (this.dta.adj[idx].ads[i].c == el.culture) {
                this.dta.adj[idx].ads[i].d = nw;
            }
        }

        if (el.culture == eBook.Interface.Culture) {
            var hd = Ext.get('eb-adjs-item-' + el.adjustment + '-trans-header-txt');
            hd.update(nw);
        }
    }
    , onSaldiChanged: function(el, nw, old) {
        var idx = parseInt(el.adjustment) - 1;
        if (el.id.indexOf('previous') > -1) {
            this.dta.adj[idx].ps = nw;
        } else {
            this.dta.adj[idx].s = nw;
        }
        this.calculateFinal();
    }
    , calculateFinal: function() {
        var it = Ext.getDom('eb-adjs-item-999');
        var idx = this.dta.adj.length - 1;
        var ttlS = 0;
        var ttlPs = 0;
        for (var i = 0; i < idx; i++) {
            ttlS += this.dta.adj[i].s;
            ttlPs += this.dta.adj[i].ps;
        }
        this.dta.adj[idx].s = Math.round((this.dta.s - ttlS) * 100) / 100;
        this.dta.adj[idx].ps = Math.round((this.dta.ps - ttlPs) * 100) / 100;
        var cp = Ext.getCmp('eb-adj-desc-999-previous');
        var cs = Ext.getCmp('eb-adj-desc-999-current');
        cp.setValue(this.dta.adj[idx].ps);
        cs.setValue(this.dta.adj[idx].s);

    }
    , renderFinal: function(s, ps, idx) {
        if (this.dta.adj.length > 0) {
            if (idx == -1) {
                var fnl = {
                    fid: this.dta.fid
                    , inr: this.dta.inr + '.999'
                    , adjNr: '999'
                    , ads: []
                };
                for (var i = 0; i < this.dta.ads.length; i++) {
                    fnl.ads.push({
                        fid: this.dta.fid,
                        inr: this.dta.inr + '.999',
                        c: this.dta.ads[i].c,
                        d: this.dta.ads[i].d + ' ADJ-999'
                    });
                }
                this.dta.adj.push(fnl);
                idx = this.dta.adj.length - 1;
            }
            this.dta.adj[idx].s = Math.round((this.dta.s - s) * 100) / 100;
            this.dta.adj[idx].ps = Math.round((this.dta.ps - ps) * 100) / 100;
        }

    }
    , prepareData: function() {
        if (!this.dta.adj) this.dta.adj = [];
        var hasFinal = -1;
        this.maxAdj = 0;
        var s = 0;
        var ps = 0;
        for (var i = 0; i < this.dta.adj.length; i++) {
            this.dta.adj[i].adjNr = this.dta.adj[i].inr.split('.')[1];
            if (this.dta.adj[i].adjNr == '999') {
                hasFinal = i;
            }
            else {
                var inr = parseInt(this.dta.adj[i].adjNr);
                if (inr > this.maxAdj) this.maxAdj = inr;
                s += this.dta.adj[i].s;
                ps += this.dta.adj[i].ps;
            }
        }
        this.renderFinal(s, ps, hasFinal);
        this.dta.adj.sort(function(a, b) {
            if (a.adjNr < b.adjNr) return -1;
            if (a.adjNr > b.adjNr) return 1;
            return 0;
        });
    }
    , getVisibilityEl: function() {
        return this.el;
    }
    , loadAccount: function(dta) {

        this.dta = dta; // to clean, get adj nr
        this.prepareData();
        // this.clearBodyEvents();
        // destroy form fields?
        if (this.rendered) {
            this.contEl = eBook.Accounts.Adjustments.MainTpl.overwrite(this.body, this.dta, true);
            var hght = this.ownerCt.getHeight(true);
            this.contEl.setStyle('height', (hght) + 'px');
            this.renderFormFields();
        }
    }
    , renderFormFields: function(el) {
        var hght = this.body.getHeight(true);
        this.contEl.setStyle('height', (hght) + 'px');
        this.contEl.child('.eb-adjs-items-body').setStyle('height', (hght - 40) + 'px');

        if (!el) el = this.contEl
        var descs = el.query('.eb-adjs-item-trans-row-desc');
        var saldi = el.query('.eb-adjs-item-Saldo-input');
        for (var i = 0; i < descs.length; i++) {
            //var id = descs.findParent('.eb-adjs-item').getAttribute("adjustment");
            var adj = descs[i].getAttribute("adjustment");
            new Ext.form.TextField({ applyTo: descs[i], width: 300, adjustment: adj
                , culture: descs[i].getAttribute("culture")
                , listeners: {
                    change: {
                        fn: this.onDescriptionChange
                        , scope: this
                    }
                }
            });
        }
        var lsts = {
            change: {
                fn: this.onSaldiChanged
                    , scope: this
            }
        };
        for (var i = 0; i < saldi.length; i++) {
            var adj = saldi[i].getAttribute("adjustment");

            new Ext.form.NumberField({ applyTo: saldi[i]
                    , width: 100
                    , align: 'right'
                    , adjustment: adj
                    , disabled: adj == "999"
                    , listeners: adj == "999" ? {} : lsts
                    , id: saldi[i].id
            });
        }
    }
    , getResult: function() {
        var dta = this.dta;
        for (var i = 0; i < this.dta.adj.length; i++) {
            var nr = dta.adj[i].adjNr;
            delete dta.adj[i].adjNr;
            dta.adj[i].vnr = this.dta.vnr + '.' + nr;
        }
        return dta.adj;
    }
                            });
      
Ext.reg('adjustmentsPanel', eBook.Accounts.Adjustments.Panel);
        
/*
 dta: {
        fid: ''
        , inr: '100000000000'
        , s: 12000
        , ps: 2343.56
        , ss: 1000
        , ads: [
            { fid: '', inr: '100000000000', c: 'nl-BE', d: 'parent omschrijving NL' }
            , { fid: '', inr: '100000000000', c: 'fr-FR', d: 'parent omschrijving FR' }
            , { fid: '', inr: '100000000000', c: 'en-US', d: 'parent omschrijving EN' }
        ]
        , adjs: [
                                //            {
                                //                fid: ''
                                //                        , inr: '100000000000.002'
                                //                        , s: 4354
                                //                        , ps: 343.12
                                //                        , ads: [
                                //                            { fid: '', inr: '100000000000.002', c: 'nl-BE', d: 'omschrijving 2 NL' }
                                //                            , { fid: '', inr: '100000000000.002', c: 'fr-FR', d: 'omschrijving 2 FR' }
                                //                            , { fid: '', inr: '100000000000.002', c: 'en-US', d: 'omschrijving 2 EN' }
                                //                        ]
                                //            }, {
                                //                fid: ''
                                //                        , inr: '100000000000.001'
                                //                        , s: 12.30
                                //                        , ps: 34.4
                                //                        , ads: [
                                //                            { fid: '', inr: '100000000000.001', c: 'nl-BE', d: 'omschrijving NL' }
                                //                            , { fid: '', inr: '100000000000.001', c: 'fr-FR', d: 'omschrijving FR' }
                                //                            , { fid: '', inr: '100000000000.001', c: 'en-US', d: 'omschrijving EN' }
                                //                            ]
                                //            }
        ]
                            }
    , 
*/