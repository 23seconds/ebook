String.prototype.rpad = function(padString, length) {
	var str = this;
    while (str.length < length)
        str = str + padString;
    return str;
}
String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

eBook.Accounts.TranslationPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        var fcult = eBook.Interface.currentFile.get('Culture');
        this.defaultCult = fcult;
        var cults = [fcult];
        if (fcult != 'nl-BE') cults.push('nl-BE');
        if (fcult != 'fr-FR') cults.push('fr-FR');
        if (fcult != 'en-US') cults.push('en-US');
        if (fcult != 'de-DE') cults.push('de-DE');
        var flds = [];
        var cultListener = { change: { fn: this.onCurrentTranslationChange, scope: this} };
        flds.push(new Ext.form.TextField({
            fieldLabel: eBook.Accounts.New.Window_AccountNr
                            , name: 'nr'
                            , ref: 'nr'
                            , allowBlank: false
                            , disabled: this.actionMode == 'EDIT'
                            , maskRe: new RegExp('[0123456789]') 
                            , listeners: this.actionMode == 'ADD' ? cultListener : {}
        }));
        for (var i = 0; i < cults.length; i++) {
            flds.push(new Ext.form.TextField({
                fieldLabel: String.format(eBook.Accounts.New.Window_AccountDesc, eBook.Language.Languages[cults[i].replace('-', '_')])
                , name: cults[i]
                , width: 300
                , allowBlank: cults[i] != fcult
                , ref: cults[i].replace('-', '_')
                , listeners: cults[i] == eBook.Interface.Culture ? cultListener : {}
            }));
        }
        Ext.apply(this, {
            labelWidth: 160
            , layout: 'form'
            , ref: 'form'
            , monitorValid: true
            , frame: true
            , bodyStyle: 'padding:5px 5px 0'
            , items: flds

        });
        eBook.Accounts.TranslationPanel.superclass.initComponent.apply(this, arguments);
    }
    , getVisibilityEl: function() {
        return this.el;
    }
    , onCurrentTranslationChange: function(el, nw, old) {
        var orignr = ""+this.nr.getValue();
        var splittedOrignr = orignr.split('.');
        var nr = ("" + splittedOrignr[0].rpad('0', 6)).substring(0, 6);
        if (splittedOrignr.length > 1) {
            nr += ("." + splittedOrignr[1].lpad('0',3).substring(0, 3));
        }
      
        this.nr.setValue(nr);
        var ttl = this[eBook.Interface.Culture.replace('-', '_')].getValue();
        this.refOwner.refOwner.setTitle(nr + ' ' + ttl);
    }
    , loadAccount: function(account) {
        this.account = account;
        for (var i = 0; i < this.account.ads.length; i++) {
            var obj = this.account.ads[i];
            if (this[obj.c.replace('-', '_')]) {
                this[obj.c.replace('-', '_')].setValue(obj.d);
            }
        }
        
        this.nr.setValue(this.account.vnr);
        this.onCurrentTranslationChange();
    }
    , getTranslationObject: function(culture, internalNr) {
        return {
            fid: eBook.Interface.currentFile.get('Id')
            , inr: internalNr
            , c: culture
            , d: this[culture.replace('-', '_')].getValue()
            , ch: true
        };
    }
    , getResult: function() {
        var ac;
        var internalNr;
        if (this.account) {
            internalNr = this.account.inr;
            ac = {
                fid: eBook.Interface.currentFile.get('Id')
                , inr: this.account.inr
                , vnr: "" + this.nr.getValue()
                , iid: this.account.iid
                , hta: false
                , ely: this.account.ely
                , s: this.account.s
                , ss: this.account.ss
                , ps: this.account.ps
                , ita: false
                , dd: ''
                , c: eBook.Interface.Culture
                , lc: new Date()
                , ads: []
                , ams: []
                , adj: []
            };
        } else {
            internalNr = ("" + this.nr.getValue()).rpad('0', 12);
            ac = {
                fid: eBook.Interface.currentFile.get('Id')
                , inr: internalNr
                , vnr: "" + this.nr.getValue()
                , iid: ""
                , hta: false
                , ely: false
                , s: 0
                , ss: 0
                , ps: 0
                , ita: false
                , dd: ''
                , c: eBook.Interface.Culture
                , lc: new Date()
                , ads: []
                , ams: []
                , adj: []
            };
        }
        ac.ads.push(this.getTranslationObject('nl-BE', internalNr));
        ac.ads.push(this.getTranslationObject('fr-FR', internalNr));
        ac.ads.push(this.getTranslationObject('en-US', internalNr));
        ac.ads.push(this.getTranslationObject('de-DE', internalNr));
        return ac;
    }
});

Ext.reg('AccountTranslationsPanel', eBook.Accounts.TranslationPanel);