
eBook.Accounts.ContextMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'eBook-account-context-menu'
            , shadow: false
            ,items:[{
                    text: 'Edit account',
                    iconCls: 'eBook-icon-edit-16',
                    handler: this.scope.onEditAccountClick,
                    ref:'editAccount',
                    scope: this.scope
                }, '-', {
                    text: 'Add Tax Adjustment',
                    iconCls: 'eBook-icon-add-16',
                    handler: this.scope.onAddAjdustmentClick,
                    ref: 'addAdjustment',
                    scope: this.scope
                }, {
                    text: 'Edit Tax Adjustments',
                    iconCls: 'eBook-icon-edit-16',
                    handler: this.scope.onEditAjdustmentsClick,
                    ref: 'editAdjustments',
                    scope: this.scope
                }]
                
        });
        eBook.Accounts.ContextMenu.superclass.initComponent.apply(this, arguments);
    }
    , showMe: function(el, accountNr, hta) {
        this.activeEl = el;
        this.accountNr = accountNr;
        eBook.Accounts.ContextMenu.superclass.show.apply(this, [el,'c']);
        if (!hta) {
            this.editAdjustments.disable();
        } else {
            this.editAdjustments.enable();
        }
    }
    , showMeAt: function(el, accountNr, hta, xy) {
        this.activeEl = el;
        this.accountNr = accountNr;
        eBook.Accounts.ContextMenu.superclass.showAt.apply(this, [xy]);
        if (!hta) {
            this.editAdjustments.disable();
        } else {
            this.editAdjustments.enable();
        }
    }
});

Ext.reg('accountcontextmenu', eBook.Accounts.ContextMenu);