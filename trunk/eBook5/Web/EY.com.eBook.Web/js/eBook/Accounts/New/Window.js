
eBook.Accounts.Manage.Window = Ext.extend(eBook.Window, {
    
    initComponent: function() {
        var its = [{ xtype: 'AccountTranslationsPanel', title: 'Descriptions', ref: '../descs'
                    , actionMode: this.internalNr ? "EDIT" : "ADD"}];
            this.adjustmentsPanelAdded = false;
            if (!Ext.isEmpty(this.internalNr) && (this.internalNr.indexOf('.') == -1)) {
                its.push({ xtype: 'adjustmentsPanel', title: 'Adjustments', ref: '../adjs' });
                this.adjustmentsPanelAdded = true;
            }

            Ext.apply(this, {
                width: 758
            , height: 454
            , layout: 'fit'
            , maximizable: false
            , items: [
            { xtype: 'tabpanel', activeItem: 0, ref: 'tab'
                , items: its
            }
            ]
            , bbar: ['->', {
                ref: 'savechanges',
                text: "Save changes",
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
}]
            });
            eBook.Accounts.Manage.Window.superclass.initComponent.apply(this, arguments);
        }
    , onSaveClick: function() {
        var serviceMethod = 'SaveNewAccount'
        var account = this.tab.items.items[0].getResult();
        if (this.internalNr) {
            account.adj = this.tab.items.items[1].getResult();
            serviceMethod = 'SaveAccount'
        }
        this.getEl().mask('Saving');
        eBook.CachedAjax.request({
            url: eBook.Service.schema + serviceMethod
            , serviceMethod: serviceMethod
            , method: 'POST'
            , params: Ext.encode({ adc: account })
            , callback: this.onSaveAccountCallback
            , scope: this
        });
    }
    , onSaveAccountCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            //var robj = resp.respons
            this.parent.hasChanges = true;
            this.parent.doRefresh();
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , show: function(parent, ctx) {
        this.parent = parent;
        this.callingCtx = ctx;
        eBook.Accounts.Manage.Window.superclass.show.call(this);
        if (this.internalNr) {
            this.loadAccount(this.internalNr);
        }

    }
    , loadAccount: function(nr) {
        this.getEl().mask('loading');
        eBook.CachedAjax.request({
            url: eBook.Service.schema + 'GetAccount'
                , method: 'POST'
                , params: Ext.encode({ cadc: { FileId: eBook.Interface.currentFile.get('Id')
                                                , InternalNr: '' + nr
                                                , Culture: eBook.Interface.Culture
                                                , IncludeMappingDetails: false
                                                , IncludeAllDescriptions: true
                }
                })
                , callback: this.onLoadAccountCallback
                , scope: this

        });
    }
    , onLoadAccountCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            this.account = robj.GetAccountResult;
            this.tab.items.items[0].loadAccount(this.account);
            if (this.adjustmentsPanelAdded) {
                this.tab.items.items[1].loadAccount(this.account);
            }
            
            switch (this.callingCtx) {
                case "DESC":
                    //nothing, default tab
                    break;
                case "ADJ":
                    this.tab.setActiveTab(1);
                    break;
                case "ADDADJ":
                    this.tab.setActiveTab(1);
                    this.tab.items.items[1].onAddAdjustmentClick();
                    break;
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    }); 

/*
eBook.Accounts.New.Window = Ext.extend(eBook.Window, {
    actionMode: 'ADD'
    , initComponent: function() {
        var fcult = eBook.Interface.currentFile.get('Culture');
        this.defaultCult = fcult;
        var cults = [fcult];
        if (fcult != 'nl-BE') cults.push('nl-BE');
        if (fcult != 'fr-FR') cults.push('fr-FR');
        if (fcult != 'en-US') cults.push('en-US');
        if (this.internalNr) this.actionMode = 'EDIT';
        var flds = [];
        flds.push(new Ext.form.NumberField({
            fieldLabel: eBook.Accounts.New.Window_AccountNr
                            , name: 'nr'
                            , ref: 'nr'
                            , allowBlank: false
                            , disabled: this.actionMode == 'EDIT'
        }));
        for (var i = 0; i < cults.length; i++) {
            flds.push(new Ext.form.TextField({
                fieldLabel: String.format(eBook.Accounts.New.Window_AccountDesc, cults[i])
                            , name: cults[i]
                            , width: 300
                            , allowBlank: cults[i] != fcult
                            , ref: cults[i].replace('-', '_')
            }));
        }

        Ext.apply(this, {
            title: ''
            , busyMsg: ''
            , layout: 'fit'
            , width: 620
            , height: 300
            , modal: true
            , iconCls: 'eBook-Window-account-ico'
            , items: [new Ext.form.FormPanel({
                labelWidth: 160
                , ref: 'form'
                , monitorValid: true
                , frame: true
                , bodyStyle: 'padding:5px 5px 0'
                , items: flds
                , buttons: [{
                    //text:'Bewaar gegevens',
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onSaveClick,
                    formBind: true,
                    scope: this
                }, {
                    //text: 'Annuleer',
                    ref: '../cancelButton',
                    iconCls: 'eBook-window-cancel-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onCancelClick,
                    scope: this
}]
                })]
            });
            eBook.Accounts.New.Window.superclass.initComponent.apply(this, arguments);
        }
    , show: function(parent) {
        this.calledFrom = parent ? parent : null;
        eBook.Accounts.New.Window.superclass.show.call(this);
        if (this.internalNr) this.loadAccount(this.internalNr);
    }
    , loadAccount: function(nr) {
        this.getEl().mask('loading');
        Ext.Ajax.request({
            url: eBook.Service.schema + 'GetAccount'
                , method: 'POST'
                , params: Ext.encode({ adc: { fileId: eBook.Interface.currentFile.get('Id'), inr: nr} })
                , callback: this.onLoadAccountCallback
                , scope: this

        });
    }
    , onLoadAccountCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {

        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , correctVisualNr: function(nr) {
        nr = '' + nr;
        nr = nr.trim();
        return nr.rightPad('0', 6); // get file visual account length
    }
    , getInternalNr: function(nr) {
        nr = '' + nr;
        nr = nr.trim();
        return nr.rightPad('0', 12);
    }
    , onCancelClick: function() {
        this.close();
    }
    , onSaveClick: function() {
        //mask
        var ct = this.defaultCult.replace('-', '_');
        var t = {
            fid: eBook.Interface.currentFile.get('Id')
            , inr: this.getInternalNr(this.form.nr.getValue())
            , vnr: this.correctVisualNr(this.form.nr.getValue())
            , nl: '' + this.form.nl_BE.getValue()
            , fr: '' + this.form.fr_FR.getValue()
            , en: '' + this.form.en_US.getValue()
            , Person: eBook.User.getActivePersonDataContract()
        };
        var def = this.form[ct].getValue();
        if (Ext.isEmpty(t.nl)) t.nl = def;
        if (Ext.isEmpty(t.fr)) t.fr = def;
        if (Ext.isEmpty(t.en)) t.en = def;

        this.getEl().mask('Saving');
        Ext.Ajax.request({
            url: eBook.Service.schema + (this.actionMode == 'ADD' ? 'AddAccount' : 'EditAccount')
            , method: 'POST'
            , params: Ext.encode({ adc: t })
            , callback: this.onAddCallback
            , scope: this

        });

    }
    , onAddCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            if (this.calledFrom) {
                if (this.calledFrom.doRefresh) {
                    this.calledFrom.doRefresh();
                } else if (this.calledFrom.store) { this.calledFrom.store.reload(); }
            }
            // unmask
            this.close.defer(300, this);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    });
                            
                                
*/