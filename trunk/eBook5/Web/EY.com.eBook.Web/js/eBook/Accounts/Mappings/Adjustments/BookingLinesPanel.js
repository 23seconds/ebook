eBook.Accounts.Adjustments.BookingLinesPanel = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function () {
        Ext.apply(this, {
            store:
                 new Ext.data.JsonStore({
                     StartSaldo: this.StartSaldo,
                     autoLoad: true,
                     autoDestroy: false,
                     root: 'GetBookingLinesByAccountNrResult',
                     fields: eBook.data.RecordTypes.BookingLines,
                     baseParams: {
                         FileId: eBook.Interface.currentFile.get('Id')
                        , AccountNr: this.AccountNr
                     },
                     proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                         url: eBook.Service.schema + 'GetBookingLinesByAccountNr'
                         , criteriaParameter: 'cbldc'
                         , method: 'POST'
                     }),
                     listeners: {

                         'load': {
                             fn: function (t, r, o) {
                                 var totalC = 0;
                                 var totalD = 0;


                                 if (this.StartSaldo > 0) {
                                     var r1 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_gs, 'Debet': this.StartSaldo, 'Bold': 'true' }, 1); // create new record
                                     totalD = parseInt(this.StartSaldo);
                                 } else {
                                     var r1 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_gs, 'Credit': this.StartSaldo * (-1), 'Bold': 'true' }, 1); // create new record
                                     totalC = parseInt(this.StartSaldo * (-1));
                                 }
                                 t.insert(0, r1);

                                 for (var i = 0; i < r.length; i++) {
                                     totalC += parseInt(r[i].get('Credit'));
                                     totalD += parseInt(r[i].get('Debet'));
                                 }

                                 var r2 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_t, 'Debet': totalD, 'Credit': totalC, 'Bold': 'true' }, t.data.length); // create new record
                                 t.insert(t.data.length, r2);

                                 if (totalD > totalC) {
                                     var r3 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_s, 'Debet': totalD - totalC, 'Bold': 'true' }, t.data.length); // create new record
                                 } else {
                                     var r3 = new t.recordType({ 'InternalAccountNr': eBook.Journal.Account_Adjustments_s, 'Credit': totalC - totalD, 'Bold': 'true' }, t.data.length); // create new record
                                 }
                                 t.insert(t.data.length, r3);

                                 return true;
                             }
                         }
                     }

                 })
             , loadMask: true
             , autoScroll: true
             , autoExpandColumn: 'CommentColumnSchemaAdjustments'
             , disableSelection: true
             , viewConfig: {
                 stripeRows: false,
                 getRowClass: function (record) {
                     if (record.get('Bold') == 'true') {
                         return 'schemaAdjustmentBold';
                     }
                     return '';
                     //return calcAge(record.get('dob')) < 18 ? 'minor' : 'adult';
                 }
             }
            , listeners: {
                'rowclick': {
                    fn: function (t, idx, e) {
                        if (this.store.getAt(idx).get('Bold') == "") {
                            var wn = new eBook.Journal.Window({ LaunchedFromSchema: true, bookingId: this.store.getAt(idx).get('BookingId') });
                            wn.show();
                        }
                    }
                , scope: this
                }

            }
            , columns: [{
                header: eBook.Journal.GroupedGrid_Account,
                width: 150,
                dataIndex: 'InternalAccountNr',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == 'true') {
                        metaData.css += 'schemaAdjustmentBold';
                    } else {
                        metaData.css += 'schemaAdjustmentPointer';
                    }
                    return value;
                }

            }, {
                header: 'Nr',
                dataIndex: 'GroupNr',
                align: 'center',
                width: 30

            }, {
                header: eBook.Journal.GroupedGrid_Debet,
                dataIndex: 'Debet',
                align: 'right',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == 'true') {
                        metaData.css += 'schemaAdjustmentBold';
                    } else {
                        metaData.css += 'schemaAdjustmentPointer';
                    }
                    return Ext.util.Format.euroMoney(value);
                }
            }, {
                header: eBook.Journal.GroupedGrid_Credit,
                dataIndex: 'Credit',
                align: 'right',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == 'true') {
                        metaData.css += 'schemaAdjustmentBold';
                    } else {
                        metaData.css += 'schemaAdjustmentPointer';
                    }
                    return Ext.util.Format.euroMoney(value);
                }
            }, {
                header: eBook.Journal.GroupedGrid_BusinessRelation,
                width: 150,
                dataIndex: 'ClientSupplierName',
                align: 'center',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == '') {
                        metaData.css += 'schemaAdjustmentPointer';
                    }
                    return value;
                }
            }, {
                header: eBook.Journal.GroupedGrid_Comment,
                dataIndex: 'Comment',
                id: 'CommentColumnSchemaAdjustments',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (record.get('Bold') == '') {
                        metaData.css += 'schemaAdjustmentPointer';
                        metaData.attr = 'ext:qtip="' + value + '"';
                    }
                    return value;
                }
            }]


        });
        eBook.Accounts.Adjustments.BookingLinesPanel.superclass.initComponent.apply(this, arguments);
    }

});

Ext.reg('bookingLinesPanel', eBook.Accounts.Adjustments.BookingLinesPanel);