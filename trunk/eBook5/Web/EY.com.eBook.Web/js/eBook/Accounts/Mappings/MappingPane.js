



eBook.Accounts.Mappings.accountCols = [{ colIdx: 0, name: ' ', dataIndex: 'id', render: function(vals, colCfg) { return vals[colCfg.dataIndex]; } }
                    , { colIdx: 1, name: 'Rekening', render: function(vals, colCfg) {
                        if (vals.ita) {
                            var str = vals.vnr.split('.')[1];
                            return '.' + str + ' ' + vals['dd'];
                        }
                        return vals['vnr'] + ' ' + vals['dd'];
                    }
                    }
                    , { colIdx: 2, name: 'Saldo N-1', dataIndex: 'ps', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 3, name: 'Saldo N', dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    ]; 

                    eBook.Accounts.Mappings.mappingCols = [{ colIdx: 4, key: 'VerworpenUitgaven', name: eBook.Accounts.Mappings.GridPanel_ColumnVU, dataIndex: 'id', render: function(vals, colCfg) { return vals[colCfg.dataIndex]; } }
                    , { colIdx: 5, key: 'Personeel', name: eBook.Accounts.Mappings.GridPanel_ColumnSalaris, render: function(vals, colCfg) { return vals['vnr'] + ' ' + vals['dd']; } }
                    , { colIdx: 6, key: 'BTW', name: eBook.Accounts.Mappings.GridPanel_ColumnBTW, dataIndex: 'ps', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 7, key: 'VoordelenAlleAard', name: eBook.Accounts.Mappings.GridPanel_ColumnVAA, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 8, key: 'Erelonen', name: eBook.Accounts.Mappings.GridPanel_ColumnErelonen, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    //, { colIdx: 9, key: 'VoordelenAlleAard', name: eBook.Accounts.Mappings.GridPanel_ColumnRV, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 9, key: 'RentegevendeVoorschotten', name: eBook.Accounts.Mappings.GridPanel_ColumnRV, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 10, key: 'RVIntresten', name: eBook.Accounts.Mappings.GridPanel_ColumnRVI, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 11, key: 'Reserves', name: eBook.Accounts.Mappings.GridPanel_ColumnBelRes, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 12, key: 'DBI', name: eBook.Accounts.Mappings.GridPanel_ColumnDBI, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    , { colIdx: 13, key: 'meerwaardeaandelen', name: eBook.Accounts.Mappings.GridPanel_ColumnMWAandelen, dataIndex: 's', render: function(vals, colCfg) { return Ext.util.Format.euroMoneyJournal(vals[colCfg.dataIndex]); } }
                    ];


                    eBook.Accounts.Mappings.MappingPane = Ext.extend(Ext.Panel, {
                        mainTpl: new Ext.XTemplate('<tpl for="."><div class="eb-map-container">'
                , '<div class="eb-map-accounts">'
                    , '<div class="eb-map-header">'
                        , '<div class="eb-map-header-inner">'
                            , '<div class="eb-map-row eb-map-row-hd" row="hd">'
                                , '<tpl for="accountHeaders">'
                                    , '<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-hd" id="eb-map-col-row-hd-ig-{colIdx}" row="hd" col="{colIdx}">{name}</div>'
                                , '</tpl>'
                            , '<div class="x-clear"></div></div>'
                        , '</div>'
                    , '</div>'
                    , '<div class="eb-map-body">'
                        , '<div class="eb-map-scroller">'
                            , '<tpl for="accounts">'
                                , '<tpl exec="values.rowIdx = xindex;"></tpl>'
                                , '<div class="eb-map-row eb-map-row-{rowIdx}<tpl if=\"hta\"> eb-map-adjusted</tpl>" row="{rowIdx}" acc="{inr}">'
                                    , '<tpl for="this.getAccountColumned(parent,values)">'
                                        , '<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx}<tpl if=\"this.hasRepoLinks(values.internalNr);\"> eb-map-hasAttachments</tpl>" id="eb-map-col-row-{rowIdx}-{colIdx}" row="{rowIdx}" col="{colIdx}" acc="{internalNr}">'
                                            , '<tpl if="colIdx==1">'
                                                , '<div class="eb-map-row-tools"<tpl if="!hta"></tpl> >'
                                                    , '<div class="eb-map-add-adjust"></div>'
                                                    , '<div class="eb-map-show-adjust"></div>'
                                                , '</div>'
                                            , '</tpl>'
                                            , '<tpl if="colIdx==3">'
                                                    , '<tpl if="startsaldo!=saldo">'
                                                        , '<div class="eb-map-content eb-map-saldo eb-map-saldo-changed" saldo="{saldo}" startsaldo="{startsaldo}" acc="{internalNr}">{content}</div>'
                                                    , '</tpl>'
                                                    , '<tpl if="startsaldo==saldo">'
                                                        , '<div class="eb-map-content eb-map-saldo">{content}</div>'
                                                    , '</tpl>'
                                            , '</tpl>'
                                             , '<tpl if="colIdx==0">'
                                                    , '<div class="eb-map-icons eb-map-attach"></div>'
                                                    , '<div class="eb-map-icons eb-map-health"></div>'
                                            , '</tpl>'
                                            , '<tpl if="colIdx!=3 && colIdx!=0">'
                                                , '<div class="eb-map-content">{content}</div>'
                                            , '</tpl>'

                                        , '</div>'
                                    , '</tpl>'
                                    , '<div class="x-clear"></div>'
                                    , '<tpl if="hta && adj!=null">'
                                        , '<tpl for="adj">'
                                            , '<tpl exec="values.rowIdx = parent.rowIdx;"></tpl>'
                                            , '<tpl exec="values.idx = xindex;"></tpl>'
                                            , '<div class="eb-map-row eb-map-row-{rowIdx}-{idx} eb-map-adjustment" row="{rowIdx}" adj="{idx}">'
                                                , '<tpl for="this.getAccountAdjColumned(parent,values)">'
                                                    , '<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx}-{idx}" id="eb-map-col-row-{rowIdx}-{colIdx}-{idx}" row="{rowIdx}" col="{colIdx}" adj="{idx}" acc="{internalNr}">'
                                                    , '<div class="eb-map-content">{content}</div>'
                                                    , '</div>'
                                                , '</tpl>'
                                                 , '<div class="x-clear"></div>'
                                                , '</div>'
                                            , '</tpl>'
                                    , '</tpl>'
                                , '</div>'
                            , '</tpl>'
                        , '</div>'
                    , '</div>'
                , '</div>'
                , '<div class="eb-map-mappings">'
                    , '<div class="eb-map-header">'
                        , '<div class="eb-map-header-inner">'
                        , '<div class="eb-map-row eb-map-row-hd" row="hd">'
                                , '<tpl for="mappingHeaders">'
                                    , '<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-hd" id="eb-map-col-row-hd-{colIdx}" row="hd" col="{colIdx}">{name}</div>'
                                , '</tpl>'
                            , '<div class="x-clear"></div></div>'
                        , '</div>'
                    , '</div>'
                    , '<div class="eb-map-body">'
                        , '<div class="eb-map-scroller">'
                           , '<tpl for="accounts">'
                                , '<tpl exec="values.rowIdx = xindex;"></tpl>'
                                , '<div class="eb-map-row eb-map-row-{rowIdx}<tpl if=\"hta\"> eb-map-adjusted</tpl>" row="{rowIdx}" acc="{inr}">'
                                    , '<tpl for="this.getMappingColumned(parent,values)">'
                                        , '<div qtip="{altText}" class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx} eb-map-mapping<tpl if=\"hasCurrent\"> eb-map-mapping-current</tpl><tpl if=\"hasPrevious\"> eb-map-mapping-previous</tpl>" id="eb-map-col-row-{rowIdx}-{colIdx}" row="{rowIdx}" col="{colIdx}"  acc="{internalNr}" key="{key}" currentMap="{current}" previousMap="{previous}" >'
                                            , '<div class="eb-map-delete">&nbsp;</div>'
                                        , '</div>'
                                    , '</tpl>'
                                    , '<div class="x-clear"></div>'
                                , '</div>'
                                 , '<tpl if="hta && adj!=null">'
                                        , '<tpl for="adj">'
                                            , '<tpl exec="values.idx = xindex;"></tpl>'
                                             , '<tpl exec="values.rowIdx = parent.rowIdx;"></tpl>'
                                            , '<div class="eb-map-adjustment eb-map-row eb-map-row-{rowIdx}-{idx}<tpl if=\"hta\"> eb-map-adjusted</tpl>" row="{rowIdx}" adj="{idx}">'
                                                , '<tpl for="this.getMappingColumned(parent,values)">'
                                                    , '<div qtip="{altText}" class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx} eb-map-adjustment eb-map-mapping<tpl if=\"hasCurrent\"> eb-map-mapping-current</tpl><tpl if=\"hasPrevious\"> eb-map-mapping-previous</tpl>" id="eb-map-col-row-{rowIdx}-{colIdx}-{idx}" row="{rowIdx}" col="{colIdx}"  adj="{idx}"  acc="{internalNr}" key="{key}" currentMap="{current}" previousMap="{previous}" >'
                                                        , '<div class="eb-map-delete">&nbsp;</div>'
                                                    , '</div>'
                                                , '</tpl>'
                                                , '<div class="x-clear"></div>'
                                            , '</div>'
                                        , '</tpl>'
                                 , '</tpl>'
                            , '</tpl>'
                        , '</div>'
                    , '</div>'
                , '</div>'
            , '</div></tpl>'
            , {
                hasRepoLinks: function (inr) {
                    return Ext.isDefined(eBook.Interface.fileRepository['L' + inr]);
                },
                getAccountAdjColumned: function (parent, values) {
                    var cols = eBook.Accounts.Mappings.accountCols;
                    var rval = [];
                    for (var i = 0; i < cols.length; i++) {
                        rval.push({
                            rowIdx: values.rowIdx
                                        , colIdx: cols[i].colIdx
                                        , internalNr: values.inr
                                        , hta: values.hta
                                        , startsaldo: values.ss
                                        , saldo: values.s
                                        , content: cols[i].render(values, cols[i])
                                        , parent: parent
                                        , idx: values.idx
                        });
                    }
                    return rval;
                }
                , getAccountColumned: function (parent, values) {
                    var cols = eBook.Accounts.Mappings.accountCols;
                    var rval = [];
                    for (var i = 0; i < cols.length; i++) {
                        rval.push({
                            rowIdx: values.rowIdx
                            , colIdx: cols[i].colIdx
                            , internalNr: values.inr
                            , startsaldo: values.ss
                            , saldo: values.s
                            , hta: values.hta
                            , idx: values.idx
                            , content: cols[i].render(values, cols[i])
                        });
                    }
                    return rval;
                },
                getMappingColumned: function (parent, values, idx) {
                    var cols = eBook.Accounts.Mappings.mappingCols;
                    var rval = [];
                    for (var i = 0; i < cols.length; i++) {
                        var mp = this.getMapped(values, cols[i].key);
                        var alt = values.hta ? "<b>not used: overruled by adjustments</b><br/>" : "";
                        alt += mp ? !Ext.isEmpty(mp.miid) ? eBook.Accounts.Mappings.Descs['M' + mp.miid] : '' : '';
                        var altprev = mp ? !Ext.isEmpty(mp.pmiid) ? 'previous: ' + eBook.Accounts.Mappings.Descs['M' + mp.pmiid] : '' : '';
                        if (alt != '' && altprev != '') alt += '<br/>';
                        alt += altprev

                        rval.push({
                            rowIdx: values.rowIdx
                            , colIdx: cols[i].colIdx
                            , idx: values.idx
                            , internalNr: values.inr
                            , content: cols[i].render(values, cols[i])
                            , key: cols[i].key
                            , hasCurrent: mp ? !Ext.isEmpty(mp.miid) : false
                            , hasPrevious: mp ? !Ext.isEmpty(mp.pmiid) : false
                            , current: mp ? !Ext.isEmpty(mp.miid) ? mp.miid : '' : ''
                            , previous: mp ? !Ext.isEmpty(mp.pmiid) ? mp.pmiid : '' : ''
                            , mapped: mp
                            , altText: alt
                        });
                    }
                    return rval;
                }
                , getMapped: function (values, key) {
                    if (!values.ams) return;
                    for (var i = 0; i < values.ams.length; i++) {
                        if (values.ams[i].mk == key) {
                            return values.ams[i];
                        }
                    }
                }
            })
                        // IF NEEDED APART (is contained in mainTpl)
    , headerTpl: new Ext.XTemplate('<div class="eb-map-row eb-map-row-hd" row="hd">columns<div class="x-clear"></div></div>')
                        // IF NEEDED APART (is contained in mainTpl)
    , headerColumnTpl: new Ext.XTemplate('<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-hd" id="eb-map-col-row-hd-{colIdx}" row="hd" col="{colIdx}">{name}</div>')

    , createPagingToolBar: function () {
        return [{
            xtype: 'buttongroup',
            ref: 'nav',
            border: false,
            id: this.id + '-toolbar-nav',
            columns: 11,
            items: [{
                ref: 'firstPage',
                tooltip: Ext.PagingToolbar.prototype.firstText,
                overflowText: Ext.PagingToolbar.prototype.firstText,
                iconCls: 'x-tbar-page-first',
                disabled: true,
                handler: this.moveFirst,
                scope: this
            }, {
                ref: 'previousPage',
                tooltip: Ext.PagingToolbar.prototype.prevText,
                overflowText: Ext.PagingToolbar.prototype.prevText,
                iconCls: 'x-tbar-page-prev',
                disabled: true,
                handler: this.movePrevious,
                scope: this
            },
             { xtype: 'tbtext',
                 text: Ext.PagingToolbar.prototype.beforePageText
             },
            { xtype: 'numberfield',
                cls: 'x-tbar-page-number',
                ref: 'currentpage',
                allowDecimals: false,
                allowNegative: false,
                enableKeyEvents: true,
                selectOnFocus: true,
                value: 1,
                submitValue: false,
                listeners: {
                    scope: this,
                    keydown: this.onPagingKeyDown,
                    blur: this.onPagingBlur
                }
            },
            { xtype: 'tbtext',
                ref: 'pages',
                text: String.format(Ext.PagingToolbar.prototype.afterPageText, 1)
            }
            , {
                ref: 'nextPage',
                tooltip: Ext.PagingToolbar.prototype.nextText,
                overflowText: Ext.PagingToolbar.prototype.nextText,
                iconCls: 'x-tbar-page-next',
                disabled: true,
                handler: this.moveNext,
                scope: this
            }, {
                ref: 'lastPage',
                tooltip: Ext.PagingToolbar.prototype.lastText,
                overflowText: Ext.PagingToolbar.prototype.lastText,
                iconCls: 'x-tbar-page-last',
                disabled: true,
                handler: this.moveLast,
                scope: this
            }

            , {
                tooltip: Ext.PagingToolbar.prototype.refreshText,
                overflowText: Ext.PagingToolbar.prototype.refreshText,
                iconCls: 'x-tbar-loading',
                handler: this.doRefresh,
                scope: this
            }]
        }
        , { xtype: 'mappingsearch', name: 'query', ref: 'query', emptyText: 'enter search term', width: '100%'
            , scope: this, qryFunction: 'performQuery'

        }
        ];

    }
    , performQuery: function (val) {
        this.loadAccounts();
    }
    , onPagingKeyDown: function (field, e) {
        var k = e.getKey(), pageNum;
        var tb = this.getTopToolbar();
        var nav = tb.navigate.nav;
        var pageNum = nav.currentpage.getValue()
        if (k == e.RETURN) {
            e.stopEvent();
            this.updateContents(pageNum);
        } else if (k == e.HOME || k == e.END) {
            e.stopEvent();
            pageNum = 1;
            this.updateContents(pageNum);
        } else if (k == e.UP || k == e.PAGEUP) {
            this.moveNext();
        } else if (k == e.DOWN || k == e.PAGEDOWN) {
            this.movePrevious();
        }
    }
    , onPagingBlur: function () {
        //
    }

    , doRefresh: function () {
        this.loadAccounts(true);
    }
    , moveFirst: function () {
        this.updateContents(1);
    }
    , movePrevious: function () {
        var tb = this.getTopToolbar();
        var nav = tb.navigate.nav;
        var curpage = nav.currentpage.getValue() - 1;
        this.updateContents(curpage);
    }
    , moveNext: function () {
        var tb = this.getTopToolbar();
        var nav = tb.navigate.nav;
        var curpage = nav.currentpage.getValue() + 1;
        this.updateContents(curpage);
    }
    , moveLast: function () {
        this.updateContents(this.pageCount);
    }
    , initComponent: function () {
        this.mainTpl.compile();
        var fileClosed = eBook.Interface.currentFile.get('Closed');

        Ext.apply(this, {
            //autoLoad:'mapping.html?v1.2'
            html: '<div>loading</div>'
            // , id: 'ebook-account-mapping'
            , listeners: { 'resize': { fn: this.updateSizes, scope: this} }
            //, bbar: this.createPagingToolBar()
            , tbar: new Ext.Toolbar({
                enableOverflow: true,
                items: [{
                    xtype: 'buttongroup',
                    ref: 'edit',
                    id: this.id + '-toolbar-edit',
                    columns: 1,
                    // height:90,
                    hidden: fileClosed,
                    title: eBook.Accounts.Mappings.GridPanel_ManageAccounts,
                    items: [
                                {
                                    ref: 'addaccount',
                                    text: eBook.Accounts.Mappings.GridPanel_NewAccount,
                                    iconCls: 'eBook-icon-addaccount-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    handler: this.onAddAccountClick,
                                    disabled: this.accountsLocked,
                                    width: 70,
                                    scope: this
                                }]
                }, {
                    xtype: 'buttongroup',
                    ref: 'view',
                    id: this.id + '-toolbar-view',
                    columns: 2,
                    title: eBook.Accounts.Mappings.GridPanel_ViewFilter,
                    items: [
                                {
                                    ref: 'zeroes',
                                    text: eBook.Accounts.Mappings.GridPanel_ViewSaldi0,
                                    enableToggle: true,
                                    iconCls: 'eBook-icon-saldi0account-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    toggleHandler: this.onZeroesToggle,
                                    //width: 70,
                                    scope: this
                                }, {
                                    ref: 'mappings',
                                    text: 'Only mapped accounts',
                                    enableToggle: true,
                                    iconCls: 'eBook-icon-detailedaccount-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    toggleHandler: this.onMappedToggle,
                                    //width: 70,
                                    scope: this
                                }]
                }, {
                    xtype: 'buttongroup',
                    ref: 'importexport',
                    id: this.id + '-toolbar-importexport',
                    columns: 4,
                    hidden: fileClosed,
                    title: eBook.Accounts.Mappings.GridPanel_ImportExport,
                    items: [
                                {
                                    text: 'ProAcc',
                                    iconCls: 'eBook-icon-proacc-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    menu: {

                                        width: 215,
                                        items: [{
                                            ref: 'importProAcc',
                                            text: eBook.Accounts.Mappings.GridPanel_ImportProAcc,
                                            //iconCls: 'eBook-icon-importproacc-24',
                                            //scale: 'medium',
                                            //iconAlign: 'top',
                                            handler: this.onImportProAccClick,
                                            //width: 175,
                                            scope: this,
                                            disabled: this.accountsLocked ? true : !eBook.Interface.isProAccClient()
                                        }]
                                    }
                                }, {
                                    text: 'Excel',
                                    iconCls: 'eBook-icon-excel-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    menu: {
                                        width: 215,
                                        items: [{
                                            ref: 'importExcel',
                                            text: eBook.Accounts.Mappings.GridPanel_ImportExcel,
                                            // iconCls: 'eBook-icon-importexcel-24',
                                            //scale: 'medium',
                                            //iconAlign: 'top',
                                            handler: this.onImportExcelClick,
                                            disabled: this.accountsLocked,
                                            //width: 175,
                                            scope: this
                                        }, {
                                            ref: 'excelMappings',
                                            text: eBook.Accounts.Mappings.GridPanel_ExportMappings,
                                            // iconCls: 'eBook-icon-exportexcel-24',
                                            // scale: 'medium',
                                            //  iconAlign: 'top',
                                            handler: this.onExportExcelMappingsClick,
                                            // width: 175,
                                            scope: this
                                        }, {
                                            ref: 'excelState',
                                            text: eBook.Accounts.Mappings.GridPanel_ExportState,
                                            // iconCls: 'eBook-icon-exportexcel-24',
                                            // scale: 'medium',
                                            // iconAlign: 'top',
                                            handler: this.onExportExcelStateClick,
                                            // width: 175,
                                            scope: this
                                        }]
                                    }
                                },
                                {
                                    text: 'Exact',
                                    iconCls: 'eBook-exact-ico-24-9',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    menu: {

                                        width: 215,
                                        items: [{
                                            ref: 'importExact',
                                            text: eBook.Accounts.Mappings.GridPanel_ImportExact,
                                            handler: this.onImportExactClick,
                                            scope: this,
                                            disabled: this.accountsLocked ? true : !eBook.Interface.isExactFile()
                                        }]
                                    }
                                }]
                }
                        , {
                            xtype: 'buttongroup',
                            ref: 'navigate',
                            id: this.id + '-toolbar-navigate',
                            columns: 1,
                            title: "Navigatie",
                            items: this.createPagingToolBar()
                        }, '->', {
                            ref: 'repositorySelect',
                            //text: 'Repository',
                            iconCls: 'eBook-repository-select-ico',
                            scale: 'xlarge',
                            enableToggle: true,
                            toggleHandler: this.onShowRepository,
                            disabled: eBook.Interface.isFileClosed(),
                            // disabled: this.accountsLocked,
                            //width: 175,
                            width: 64, height: 64,
                            scope: this
                        }]
            })
        });

        eBook.Accounts.Mappings.MappingPane.superclass.initComponent.apply(this, arguments);
    }
    , onShowRepository: function (btn, state) {
        eBook.Interface.showRepositorySelection(btn, state);
    }
    , syncScrollMappings: function (e) {
        //this.lockedScroller.dom.scrollTop = mb.scrollTop;
        this.mapHeaderScroller.dom.scrollLeft = this.mapScroller.dom.scrollLeft;
        this.accountsScroller.dom.scrollTop = this.mapScroller.dom.scrollTop;
    }

    , afterRender: function () {
        eBook.Accounts.Mappings.MappingPane.superclass.afterRender.call(this);

        // set events
        this.body.on('mouseover', this.onBodyMouseOver, this);
        this.body.on('mouseout', this.onBodyMouseOut, this);
        // check if file is closed
        if (eBook.Interface.currentFile != null && !eBook.Interface.currentFile.get('Closed')) {
            this.body.on('click', this.onBodyClick, this);
            this.body.on('contextmenu', this.onBodyContext, this);
        }

        this.updateContents();
        this.setDropZone();
    }
    , setDropZone: function () {
        this.dropZone = new Ext.dd.DropZone(this.body, {
            ddGroup: 'repository',
            pane: this,
            getTargetFromEvent: function (e) {
                return e.getTarget('.eb-map-row');
            },

            onNodeEnter: function (target, dd, e, data) {
                // Ext.fly(target).addClass('my-row-highlight-class');
            },

            //      On exit from a target node, unhighlight that node.
            onNodeOut: function (target, dd, e, data) {
                //  Ext.fly(target).removeClass('my-row-highlight-class');
            },

            onNodeOver: function (target, dd, e, data) {
                if (target) {
                    rw = target.getAttribute('row');
                    if (!Ext.isEmpty(rw) && rw != "hd" && dd.groups.repository) return Ext.dd.DropZone.prototype.dropAllowed;
                }

                return Ext.dd.DropZone.prototype.dropNotAllowed;
            },
            onNodeDrop: function (target, dd, e, data) {
                this.pane.addAttachment(dd.dragData.node.attributes.Item.Id, target.getAttribute('acc'), target.getAttribute('row'));
                data.node.ownerTree.startHide();
                return true;
            }
        });
    }

    , onBodyContext: function (e, t, o) {

        var acc = e.getTarget('.eb-map-col-1');
        if (acc) {
            var hta = Ext.get(acc.parentNode).hasClass('eb-map-adjusted') || Ext.get(acc.parentNode).hasClass('eb-map-adjustment');
            var nr = acc.getAttribute('acc');
            var rw = acc.getAttribute('row');
            if (!this.contextMenu) {
                this.contextMenu = new eBook.Accounts.ContextMenu({ scope: this });
            }
            this.contextMenu.showMeAt(acc, nr, hta, e.xy);
        }
        //
        return true;
    }
    , onBodyClick: function (e, t, o) {
        //if (eBook.Interface.currentFile != null && !eBook.Interface.currentFile.get('Closed')){

        var map = e.getTarget('.eb-map-mapping');
        var delmap = e.getTarget('.eb-map-delete');
        var changedSaldi = e.getTarget('.eb-map-saldo-changed');

        if (changedSaldi) {
            var wn = new eBook.Accounts.Adjustments.Window({ AccountNr: changedSaldi.getAttribute('acc'), StartSaldo: changedSaldi.getAttribute('startSaldo') });
            wn.show();
            //changedSaldi.getAttribute('acc')
        }

        if (delmap) {
            var xdelmap = Ext.get(delmap);

            var xmap = xdelmap.parent();
            var row = xmap.parent('.eb-map-row');
            if (row.hasClass('eb-map-adjusted')) return;

            var key = map.getAttribute('key');

            this.deleteMapping(key, map);
            xdelmap.setStyle('display', 'none');
        } else if (map) {
            //            if (this.activeMapEl!= map && this.activeMapEl) {
            //                this.activeMapEl.setAttribute('active', 'false');
            //                this.activeMapEl = null;
            //            }
            var xmap = Ext.get(map);

            var row = xmap.parent('.eb-map-row');
            if (row.hasClass('eb-map-adjusted')) return;

            var key = map.getAttribute('key');
            var mnu = eBook.Accounts.Mappings.Menus[key];
            //mnu.syncSize();
            mnu.show(map);
            map.setAttribute('active', 'true');
            this.activeMapEl = map;
            this.setRowClass.defer(100, this, [row.getAttribute('row'), map.getAttribute('col'), map.getAttribute('adj'), true]);
        } else {
            //            if(this.activeMapEl) {
            //                this.activeMapEl.setAttribute('active', 'false');
            //              //  this.setRowClass.defer(100, this, [this.activeMapEl.getAttribute('row'), this.activeMapEl.getAttribute('col'), true]);
            //                this.activeMapEl = null;
            //            }
        }
        //}

    }
    , setRowClass: function (ridx, ajidx, cidx, st) {
        var cls = '.eb-map-row-' + ridx;
        if (ajidx) cls += '-' + ajidx;
        var els = this.body.query(cls);

        for (var i = 0; i < els.length; i++) {
            if (st) Ext.get(els[i]).addClass('eb-map-row-active');
            if (!st) Ext.get(els[i]).removeClass('eb-map-row-active');
        }
        if (cidx) {
            var hd = Ext.get('eb-map-col-row-hd-' + cidx);
            if (hd) {
                if (st) {
                    hd.addClass('eb-map-col-active');
                } else {
                    hd.removeClass('eb-map-col-active');
                }
            }
        }
    }
                        /*if (cidx) {
                        var camelRe = /(-[a-z])/gi;
                        var camelFn = function(m, a) { return a.charAt(1).toUpperCase(); };
                        var clr = st ? 'rgba(255, 230, 0, 0.5)' : 'transparent';
                        var prop = 'background-color';
                        mapRules['.eb-map-col-' + cidx].style[prop.replace(camelRe, camelFn)] = clr;


                        }*/
    , showSaldiTip: function (el, startsaldo, saldo) {
        var nw = false;
        if (this.saldiTip == null) {
            this.saldiTip = new Ext.ToolTip({
                id: 'content-anchor-tip',
                anchor: 'left',
                html: null,
                width: 160,
                autoHide: true,
                closable: false
            });
            nw = true;
        }
        if (this.saldiTip.target != el) {
            this.saldiTip.target = el;
            this.saldiTip.initTarget(el);

        }
        this.saldiTip.show();
        startsaldo = Ext.util.Format.euroMoney(startsaldo);
        saldo = Ext.util.Format.euroMoney(saldo);
        this.saldiTip.update('<div class="eb-saldi-qt-txt"><div class="eb-saldi-qt-title">imported:</div><div class="eb-saldi-qt-amount">' + startsaldo + '</div></div><div class="eb-down-arrow">&nbsp;</div><div class="eb-saldi-qt-txt"><div class="eb-saldi-qt-title">adjusted to: </div><div class="eb-saldi-qt-amount">' + saldo + '</div></div>');
    }
    , hideSaldiTip: function () {
        if (this.saldiTip) this.saldiTip.hide();
    }
    , onBodyMouseOver: function (e, t, o) {
        var attachment = e.getTarget('.eb-map-attach');
        var changedSaldi = e.getTarget('.eb-map-saldo-changed');
        var mapped = e.getTarget('.eb-map-mapping-current');
        var rowOver = e.getTarget('.eb-map-row');
        var colOver = e.getTarget('.eb-map-col');


        if (attachment) {
            var par = Ext.get(attachment.parentNode);
            if (par.hasClass('eb-map-hasAttachments')) {
                eBook.Interface.showRepoLinksToolTip(attachment, 'ACCOUNT', par.dom.getAttribute('acc'), this);
            }

        }

        if (changedSaldi) {
            changedSaldi = Ext.get(changedSaldi);
            var ss = changedSaldi.getAttribute('startsaldo');
            var s = changedSaldi.getAttribute('saldo');
            this.showSaldiTip(changedSaldi, ss, s);
        } else if (rowOver && colOver) {
            rowOver = Ext.get(rowOver);
            colOver = Ext.get(colOver);
            if (rowOver.hasClass('eb-map-row-hd')) return;
            this.setRowClass(rowOver.getAttribute('row'), rowOver.getAttribute('adj'), colOver.getAttribute('col'), true);

        } else if (rowOver) {
            rowOver = Ext.get(rowOver);
            if (rowOver.hasClass('eb-map-row-hd')) return;
            this.setRowClass(rowOver.getAttribute('row'), rowOver.getAttribute('adj'), null, true);

        }
        if (mapped) {
            var xmap = Ext.get(mapped);
            var xdel = xmap.child('.eb-map-delete');
            if (xdel) {
                xdel.setStyle('display', 'block');
            }
        }

    }
    , onBodyMouseOut: function (e, t, o) {
        var changedSaldi = e.getTarget('.eb-map-saldo-changed');
        var mapped = e.getTarget('.eb-map-mapping-current');
        var map = e.getTarget('.eb-map-mapping');
        var rowOver = e.getTarget('.eb-map-row');
        var colOver = e.getTarget('.eb-map-col');


        if (map) {
            // if(map.getAttribute('active')=='true') return;
        }

        if (changedSaldi) {
            changedSaldi = Ext.get(changedSaldi);
            this.hideSaldiTip();
        } else if (rowOver && colOver) {
            rowOver = Ext.get(rowOver);
            colOver = Ext.get(colOver);
            if (rowOver.hasClass('eb-map-row-hd')) return;
            this.setRowClass(rowOver.getAttribute('row'), rowOver.getAttribute('adj'), colOver.getAttribute('col'), false);
        } else if (rowOver) {
            rowOver = Ext.get(rowOver);
            if (rowOver.hasClass('eb-map-row-hd')) return;
            this.setRowClass(rowOver.getAttribute('row'), rowOver.getAttribute('adj'), null, false);

        }
        if (mapped) {
            var xmap = Ext.get(mapped);
            var xdel = xmap.child('.eb-map-delete');
            if (xdel) {
                xdel.setStyle('display', 'none');
            }
        }
    }
    , updateContents: function (page) {

        this.body.update(this.mainTpl.apply(this.getDta(page)));
        this.mapScroller = this.body.child('.eb-map-mappings .eb-map-body');
        this.mapHeaderScroller = this.body.child('.eb-map-mappings .eb-map-header');
        this.accountsScroller = this.body.child('.eb-map-accounts .eb-map-body');
        this.mon(this.mapScroller, 'scroll', this.syncScrollMappings, this);
        this.syncSize();
    }
    , updateSizes: function (me, adjWidth, adjHeight, rawWidth, rawHeight) {
        var ct = this.body.child('.eb-map-container');
        var cts = ct.getSize();
        var ac = this.body.child('.eb-map-accounts');
        var acs = ac.getSize();
        var minWd = 100 * 11;
        var mapWd = (cts.width - acs.width);
        var mp = this.body.child('.eb-map-mappings');
        var scrollWd = mapWd;
        if (scrollWd < minWd) scrollWd = minWd;
        mp.setStyle('width', mapWd + 'px');

        var acb = ac.child('.eb-map-body');
        var mpb = mp.child('.eb-map-body');
        var mpbs = mpb.child('.eb-map-scroller');
        var mph = mp.child('.eb-map-header');
        var mphs = mph.child('.eb-map-header-inner');
        if (mp && mpb && mph) {

            mpb.setStyle('width', mapWd + 'px');
            mph.setStyle('width', mapWd + 'px');
            mphs.setStyle('width', scrollWd + 'px');
            mpbs.setStyle('width', scrollWd + 'px');

            var szh = mph.getSize();
            var sz = mp.getSize();
            acb.setStyle('height', (sz.height - szh.height) + 'px');
            mpb.setStyle('height', (sz.height - szh.height) + 'px');

        }
    }
    , getQuery: function () {
        var tb = this.getTopToolbar();
        return tb.navigate.query.getValue();
    }
    , importDone: function () {
        this.loadAccounts();
    }
    , loadAccounts: function (retainPage) {
        this.getEl().mask("Loading", 'x-mask-loading');
        var tb = this.getTopToolbar();
        var includeZeroes = tb.view.zeroes.pressed;
        var onlyMapped = tb.view.mappings.pressed;
        retainPage = retainPage == true;

        var nav = tb.navigate.nav;
        var curpage = nav.currentpage.getValue();

        eBook.CachedAjax.request({
            url: eBook.Service.schema + 'GetAccountsMappingView'
            , method: 'POST'
            , params: Ext.encode({ cicdc: { FileId: eBook.Interface.currentFile.get('Id')
                                    , Culture: eBook.Interface.Culture
                                    , IncludeZeroes: includeZeroes
                                    , MappedOnly: onlyMapped
                                    , Query: this.getQuery()
            }
            })
            , callback: this.loadCallback
            , scope: this
            , pageNum: retainPage ? curpage : null

        });
    }
    , onAddAccountClick: function () {
        var wn = new eBook.Accounts.Manage.Window({});
        wn.show(this);
    }
    , onZeroesToggle: function (btn, state) {
        this.loadAccounts();
    }
    , onMappedToggle: function (btn, state) {
        this.loadAccounts();
    }
    , onImportProAccClick: function () {
        eBook.Interface.showProAccImport(this);
    }
    , onExportProAccAdjustClick: function () {
    }
    , onImportExcelClick: function () {
        var wn = new eBook.Excel.UploadWindow({ mappingPane: this });
        wn.show();
    }
    , onImportExactClick: function () {
        var wn = new eBook.Exact.UploadWindow({mappingPane: this});
        wn.show();
    }
    , onExportExcelMappingsClick: function () {

        this.getEl().mask("Preparing export", 'x-mask-loading');
        eBook.CachedAjax.request({
            url: eBook.Service.output + 'ExportAccountMappings'
            , method: 'POST'
            , params: Ext.encode({ cicdc: { Id: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.Culture} })
            , callback: this.onExportExcelMappingsCallback
            , scope: this
        });
    }
    , onExportExcelMappingsCallback: function (opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.ExportAccountMappingsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onExportExcelStateClick: function () {
    }
    , accounts: []
    , loadCallback: function (opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                var rc = robj['GetAccountsMappingViewResult'];
                this.accounts = rc;
                this.updateContents(opts.pageNum);
                this.getEl().unmask();
            } else {
                eBook.Interface.showResponseError(resp, this.title);
                this.getEl().unmask();
            }
        } catch (e) {
            //console.log(e);
            this.getEl().unmask();
        }

    }
    , currentpage: 1
    , setPaging: function (page) {
        var tb = this.getTopToolbar();
        var nav = tb.navigate.nav;

        this.pageCount = Math.floor(this.accounts.length / this.maxLoad);
        if (this.accounts.length % this.maxLoad > 0) this.pageCount++;
        if (page > this.pageCount) return;
        nav.pages.setText(String.format(Ext.PagingToolbar.prototype.afterPageText, this.pageCount));

        nav.currentpage.setValue(page);
        this.currentpage = page;
        if (page > 1) {
            nav.firstPage.enable();
            nav.previousPage.enable();
        } else {
            nav.firstPage.disable();
            nav.previousPage.disable();
        }

        if (page < this.pageCount) {
            nav.nextPage.enable();
            nav.lastPage.enable();
        } else {
            nav.nextPage.disable();
            nav.lastPage.disable();
        }

    }
    , maxLoad: 30
    , getDta: function (page) {
        var from = 0;
        var to = this.maxLoad;
        page = Ext.isEmpty(page) ? 1 : page;
        var ranged = Ext.isDefined(page);
        if (!ranged && this.accounts.length > this.maxLoad) {
            ranged = true;
            from = 0;
            to = this.maxLoad;
            page = 1;
        } else if (ranged) {
            from = page == 1 ? 0 : ((page - 1) * this.maxLoad);
            to = this.maxLoad * page;
        } else {
            page = 1;
        }
        this.setPaging(page, from, to, this.accounts.length);
        return {
            accountHeaders: eBook.Accounts.Mappings.accountCols
            , mappingHeaders: eBook.Accounts.Mappings.mappingCols
            , accounts: ranged ? this.accounts.slice(from, to) : this.accounts
        }
    }
    , hasChanges: false
    , deleteMapping: function (key, mapEl) {
        var row = mapEl.getAttribute('row');
        var adj = mapEl.getAttribute('adj');
        if (Ext.isDefined(adj) && !Ext.isEmpty(adj)) {
            adj = parseInt(adj) - 1;
        } else {
            adj = -1;
        }
        var pg = this.currentpage == 1 ? 0 : ((this.currentpage - 1) * this.maxLoad);
        var idx = pg + parseInt(row) - 1
        mapEl = Ext.get(mapEl);
        var nr = mapEl.dom.getAttribute('acc');
        mapEl.dom.setAttribute('currentMap', '');
        mapEl.removeClass('eb-map-mapping-current');
        this.setQTip(mapEl);
        this.hasChanges = true;

        var acc = this.accounts[idx];
        if (adj > -1) acc = acc.adj[adj];

        if (!acc.ams) {
        } else {
            var del = null;
            for (var i = 0; i < acc.ams.length; i++) {
                if (acc.ams[i].mk == key) {
                    if (acc.ams[i].pmiid) {
                        acc.ams[i].mm = null;
                        acc.ams[i].miid = null;
                    } else {
                        del = acc.ams[i];
                    }
                    break;
                }
            }
            if (del) acc.ams.remove(del);
        }
        if (adj > -1) {
            this.accounts[idx].adj[adj] = acc;
        } else {
            this.accounts[idx] = acc;
        }

        var action = {
            text: String.format(eBook.Accounts.Mappings.Window_SavingMapping, nr, '')
            , url: eBook.Service.schema
            , params: { camdc: {
                FileId: eBook.Interface.currentFile.get('Id')
                        , InternalNr: nr
                        , MappingKey: key
                        , Person: eBook.User.getActivePersonDataContract()
                //,pmiid:null
            }
            }
            , action: 'DeleteAccountMapping'
        };
        this.refOwner.addAction(action);

    }
    , setQTip: function (el) {
        if (el.dom) el = el.dom;
        var key = el.getAttribute('key');
        var cur = el.getAttribute('currentMap');
        var prev = el.getAttribute('previousMap');
        if (Ext.isEmpty(key)) return;
        //var mp = this.getMapped(values, cols[i].key);


        if (Ext.isEmpty(cur) && Ext.isEmpty(prev)) {
            el.setAttribute('qtip', '');
        }
        else {
            var alt = !Ext.isEmpty(cur) ? eBook.Accounts.Mappings.Descs['M' + cur] : '';
            var altprev = !Ext.isEmpty(prev) ? eBook.Accounts.Mappings.Descs['M' + prev] : '';
            if (alt != '' && altprev != '') alt += '<br/>Previous:';
            alt += altprev;
            el.setAttribute('qtip', alt);
        }
    }
    , changeMapping: function (mnuEl, mapEl) {
        var row = mapEl.getAttribute('row');
        var adj = mapEl.getAttribute('adj');
        if (Ext.isDefined(adj) && !Ext.isEmpty(adj)) {
            adj = parseInt(adj) - 1;
        } else {
            adj = -1;
        }
        var pg = this.currentpage == 1 ? 0 : ((this.currentpage - 1) * this.maxLoad);
        var idx = pg + parseInt(row) - 1;

        mapEl = Ext.get(mapEl);
        mapEl.addClass('eb-map-mapping-current');
        var cur = mapEl.dom.getAttribute('currentMap');
        if (cur == mnuEl.mapId) return;

        mapEl.dom.setAttribute('currentMap', mnuEl.mapId);
        this.setQTip(mapEl);
        var nr = mapEl.dom.getAttribute('acc');

        var amdc = {
            fid: eBook.Interface.currentFile.get('Id')
                        , inr: nr
                        , mk: mnuEl.key
                        , mm: mnuEl.meta
                        , miid: mnuEl.mapId
        };

        var acc = this.accounts[idx];
        if (adj > -1) acc = acc.adj[adj];

        if (!acc.ams) {
            acc.ams = [];
            acc.ams.push(amdc);
        } else {
            var ok = false;
            for (var i = 0; i < acc.ams.length; i++) {
                if (acc.ams[i].mk == amdc.mk) {
                    acc.ams[i].mm = amdc.mm;
                    acc.ams[i].miid = amdc.miid;
                    ok = true;
                    break;
                }
            }
            if (!ok) acc.ams.push(amdc);
        }
        if (adj > -1) {
            this.accounts[idx].adj[adj] = acc;
        } else {
            this.accounts[idx] = acc;
        }
        // mapEl.setAttribute('active', 'false');
        this.hasChanges = true;
        var action = {
            text: String.format(eBook.Accounts.Mappings.Window_SavingMapping, nr, '')
            , url: eBook.Service.schema
            , params: { amdc: {
                fid: eBook.Interface.currentFile.get('Id')
                        , inr: nr
                        , mk: mnuEl.key
                        , mm: mnuEl.meta
                        , miid: mnuEl.mapId
                        , Person: eBook.User.getActivePersonDataContract()
                //,pmiid:null
            }
            }
            , action: 'SaveAccountMapping'
        };
        this.refOwner.addAction(action);
        // this.pane.changeMapping(mnuEl, mapEl);
    }
    , addAttachment: function (reposId, accountNr, rowNr) {
        var el = Ext.get('eb-map-col-row-' + rowNr + '-0');
        el.addClass('eb-map-hasAttachments');
        var action = {
            text: 'Adding repository link to ' + accountNr//String.format(eBook.Accounts.Mappings.Window_SavingMapping, nr, '')
                , url: eBook.Service.repository
                , params: { rldc: {
                    ClientId: eBook.Interface.currentClient.get('Id')
                        , FileId: eBook.Interface.currentFile.get('Id')
                            , ItemId: reposId
                            , ConnectionType: 'ACCOUNT'
                            , ConnectionGuid: eBook.EmptyGuid
                            , ConnectionAccount: accountNr
                            , ConnectionDetailedPath: ''
                    // , Person: eBook.User.getActivePersonDataContract()
                    //,pmiid:null
                }
                }
                , action: 'AddLink'
        };
        this.refOwner.addAction(action);


        this.refOwner.addAction({
            text: 'updating links'
                , action: function () { eBook.Interface.loadRepoLinks(); return true; }
        });

    }
    , onEditAccountClick: function (mnuItem, e) {
        var accountNr = mnuItem.parentMenu.accountNr;
        var wn = new eBook.Accounts.Manage.Window({ internalNr: accountNr });
        wn.show(this, "DESC");
    }
    , onAddAjdustmentClick: function (mnuItem, e) {
        var accountNr = mnuItem.parentMenu.accountNr;
        var wn = new eBook.Accounts.Manage.Window({ internalNr: accountNr });
        wn.show(this, "ADDADJ");
    }
    , onEditAjdustmentsClick: function (mnuItem, e) {
        var accountNr = mnuItem.parentMenu.accountNr;
        var wn = new eBook.Accounts.Manage.Window({ internalNr: accountNr });
        wn.show(this, "ADJ");
    }
                    });

Ext.reg('mappingpane', eBook.Accounts.Mappings.MappingPane);

/*
bodyRowAccTpl


[
                { rowIdx: 0, id: '1ac', vnr: '100000', dd: 'Test rekening', ps: 12000, s: 324324.34, hta: false }
                , { rowIdx: 1, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false, ams: [{ mk: 'VerworpenUitgaven', miid: '2168e25f-286e-4955-ace0-cf62124105a2', pmiid: '2168e25f-286e-4955-ace0-cf62124105a2'}] }
                , { rowIdx: 2, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 3, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 4, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 5, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 6, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: true }
                , { rowIdx: 7, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 8, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 9, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 10, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: true }
                , { rowIdx: 11, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 12, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 13, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 14, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 15, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 16, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 17, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 18, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                , { rowIdx: 19, id: '11ac', vnr: '110000', dd: 'Test adjusted rekening', ps: 545665.32, s: 7657457.43, hta: false }
                ]
                

// adjustment
<tpl for="adjustments">
<div class="eb-map-row eb-map-row-{rowIdx}" row="{rowIdx}">
<tpl for=""
<div class="eb-map-col eb-map-col-{colIdx} eb-map-item-row-{rowIdx}" id="eb-map-col-row-{rowIdx}-{colIdx}-{adjIdx}" row="{rowIdx}" col="{colIdx}" adj="{adjIdx}" acc="{internalnr}"><div class="eb-map-content">im</div></div>
 <div class="x-clear"></div>

bodyRowMapTpl

//ROW:
//ROW: eb-map-hide-adjusts

*/


eBook.Accounts.Mappings.Search = function(config) {
    Ext.apply(this, config);
    eBook.Accounts.Mappings.Search.superclass.constructor.call(this);
}; // eo constructor

Ext.extend(eBook.Accounts.Mappings.Search, Ext.form.TwinTriggerField, {

    searchText: 'Search'
    , searchTipText: 'Type a text to search and press Enter'
    , selectAllText: 'Select All'
    , iconCls: 'icon-magnifier'
    , checkIndexes: 'all', disableIndexes: []
    , dateFormat: undefined
    , showSelectAll: true
    
    , initComponent: function() {
        Ext.apply(this, {
            selectOnFocus: undefined === this.selectOnFocus ? true : this.selectOnFocus
            , trigger1Class: 'x-form-clear-trigger'
            , trigger2Class: 'x-form-search-trigger'
            , onTrigger1Click: this.onTriggerClear.createDelegate(this)
            , onTrigger2Click: this.onTriggerSearch.createDelegate(this)
            , minLength: this.minLength
            , width:210
        });
        eBook.Accounts.Mappings.Search.superclass.initComponent.apply(this, arguments);
    }
    , onRender: function(container, position) {
        eBook.Accounts.Mappings.Search.superclass.onRender.call(this, container, position);
        this.el.dom.qtip = this.searchTipText;

        if (this.minChars) {
            this.el.on({ scope: this, buffer: 300, keyup: this.onKeyUp });
        }

        // install key map
        var map = new Ext.KeyMap(this.el, [{
            key: Ext.EventObject.ENTER
                , scope: this
                , fn: this.onTriggerSearch
        }, {
            key: Ext.EventObject.ESC
                , scope: this
                , fn: this.onTriggerClear
        }]);
        map.stopEvent = true;


        this.reconfigure();
    }
    , onTriggerClear: function() {
        this.setValue('');
        this.focus();
        this.onTriggerSearch();
    } // eo function onTriggerClear
        // }}}
        // {{{
        /**
        * private Search Trigger click handler (executes the search, local or remote)
        */
    , onTriggerSearch: function() {
        if (!this.isValid()) {
            return;
        }
        var val = this.getValue();
        var sc = this.scope;
        sc[this.qryFunction].call(sc, val);

    } // eo function onTriggerSearch
        // }}}
        // {{{

        /**
        * field el keypup event handler. Triggers the search
        * @private
        */
    , onKeyUp: function(e, t, o) {

        //if (e.isNavKeyPress()) {
        if (e.isSpecialKey() && e.keyCode != e.BACKSPACE) { // do not trigger on special keys (except backspace)
            return;
        }

        var length = this.getValue().toString().length;
        if (0 === length || this.minChars <= length) {
            this.onTriggerSearch();
        }
    } // eo function onKeyUp
        // }}}
        // {{{
        /**
        * @param {Boolean} true to disable search (TwinTriggerField), false to enable
        */
//    , setDisabled: function() {
//        this.setDisabled.apply(this.field, arguments);
//    }
//    , enable: function() {
//        this.setDisabled(false);
//    }
//    , disable: function() {
//        this.setDisabled(true);
//    }
    , reconfigure: function() {




    }
    
});
Ext.reg('mappingsearch', eBook.Accounts.Mappings.Search);