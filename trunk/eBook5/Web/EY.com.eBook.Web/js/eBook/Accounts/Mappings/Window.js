
eBook.Accounts.Mappings.Window = Ext.extend(eBook.Window, {
    accountsLocked: false,
    initComponent: function() {
        this.accountsLocked = eBook.Interface.currentFile.get('NumbersLocked');
        //alert(this.accountsLocked);
        Ext.apply(this, {
            title: eBook.Accounts.Mappings.Window_Title
            , busyMsg: eBook.Accounts.Mappings.Window_SavingMappings // TO TRANSLATE
            , layout: 'fit'
            , id: 'eBook-accountmapping-window'
            , iconCls: 'eBook-Window-accountmappings-ico'
            , actionsShowWait: false
            , items: [
                { xtype: 'mappingpane', ref: 'pane', accountsLocked: this.accountsLocked }
            ]
            , enableFileCache: true
        });
        eBook.Accounts.Mappings.Window.superclass.initComponent.apply(this, arguments);

    }
    , show: function() {
        eBook.Accounts.Mappings.Window.superclass.show.call(this);
        this.pane.loadAccounts();
    }
    , changeMapping: function(mnuEl, mapEl) {
        this.pane.changeMapping(mnuEl, mapEl);
    }
    , amReady: false
    , onBeforeClose: function() {

        if (this.actions.length == 0 && !this.isBusy) {
            if (this.pane.saldiTip) this.pane.saldiTip.destroy();
            if (this.pane.hasChanges) eBook.Interface.center.fileMenu.updateWorksheets();
            return true;
        }
        return eBook.Accounts.Mappings.Window.superclass.onBeforeClose.call(this);
    }

});
