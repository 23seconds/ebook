eBook.Accounts.Mappings.handleClick = function(menuItem, b, c, d, e, f, g) {
    var mnu = menuItem;
    while (mnu.parentMenu != null) {
        mnu = mnu.parentMenu;
    }
    if (mnu.showType == "mapping") {
        var mpane = Ext.WindowMgr.get('eBook-accountmapping-window');
        mpane.changeMapping(menuItem, mnu.activeEl);
    } else {
        var cmp = Ext.getCmp(mnu.activeEl.id);
        cmp.setValue(menuItem.listObj);
    }
    //Ext.get(mnu.activeEl);  //.setStyle('background-color', '#F00');
};

eBook.Accounts.MappingMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            cls:'eBook-mapping-menu'
            , shadow:false
        });
        eBook.Accounts.MappingMenu.superclass.initComponent.apply(this, arguments);
    }
    , show: function(el) {
        this.showType = "mapping";
        this.activeEl = el;
        eBook.Accounts.MappingMenu.superclass.show.apply(this, arguments);
    }
    , showField: function(el) {
        this.showType = "field";
        this.activeEl = el;
        eBook.Accounts.MappingMenu.superclass.show.apply(this, arguments);
    }
});

Ext.reg('mappingmenu', eBook.Accounts.MappingMenu);