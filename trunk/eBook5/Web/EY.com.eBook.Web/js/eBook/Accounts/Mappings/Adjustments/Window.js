
eBook.Accounts.Adjustments.Window = Ext.extend(eBook.Window, {
    accountsLocked: false,
    initComponent: function () {

        Ext.apply(this, {
            title: "Adjustments"//eBook.Accounts.Adjustments.Window_Title
            , layout: 'fit'
            , width: 800
            , height: 500
            , id: 'eBook-accountadjustments-window'
            , iconCls: 'eBook-Window-accountmappings-ico'
            , modal: true
            , actionsShowWait: false
            , items: [
                { xtype: 'bookingLinesPanel', ref: 'blpanel', AccountNr: this.AccountNr, StartSaldo: this.StartSaldo }
            ]
            , enableFileCache: true
            , tbar: {
                xtype: 'toolbar',
                items: [{
                    ref: '../PrintView',
                    text: eBook.Document.Window_PrintPreview,
                    iconCls: 'eBook-icon-previewdocument-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onPrintPreviewClick,
                    scope: this
                }]
            }
        });
        eBook.Accounts.Adjustments.Window.superclass.initComponent.apply(this, arguments);

    }
    , show: function () {
        eBook.Accounts.Adjustments.Window.superclass.show.call(this);
        //this.loadData();
        //this.pane.loadAccounts();
    }
    , onPrintPreviewClick: function () {
        var checkedCheckboxes = new Array();
        checkedCheckboxes.push('correctieboekingenSchema');
        eBook.Splash.setText("Generating quickprint");
        eBook.Splash.show();
        Ext.Ajax.request({
            url: eBook.Service.output + 'GenerateQuickprint'
            , method: 'POST'
            , params: Ext.encode({
                cqdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.Culture,
                    Options: checkedCheckboxes,
                    AccountNr: this.AccountNr,
                    StartSaldo: this.StartSaldo
                }
            })
            , callback: this.onPrintPreviewReady
            , scope: this
        });
    }
    , onPrintPreviewReady: function (opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GenerateQuickprintResult);
            //window.open('pickup/' + robj.GenerateLeadSheetsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }

});
