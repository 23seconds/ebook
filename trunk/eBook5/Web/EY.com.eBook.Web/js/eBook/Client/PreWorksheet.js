

eBook.Client.PreWorksheetWindow = Ext.extend(eBook.Window, {
    ruleApp: ''
    , typeName: ''
    , clientId: ''
    , initComponent: function() {
        Ext.apply(this, {
            title: eBook.Client.PreWorksheetWindow_Title
            , tbar: [{
                ref: 'opensheet',
                text: eBook.Client.PreWorksheetWindow_Create,
                iconCls: 'eBook-icon-tablesave-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onOpenSheet,
                scope: this
}]
            , width: 450
            , height: 300
            , items: [
               { xtype: 'form'
                   , ref: 'formitems'
                   , labelWidth: 200
                   , items: [
                       {
                           xtype: 'textfield'
                            , ref: 'filename'
                            , name: 'filename'
                            , allowBlank: false
                            , fieldLabel: eBook.Client.PreWorksheetWindow_Description
                            , anchor: '90%'
                       }, {
                           xtype: 'languagebox'
                             , ref: 'language'
                             , name: 'language'
                             , allowBlank: false
                             , fieldLabel: eBook.Create.Empty.FileInfoFieldSet_Language
                             , anchor: '90%'
                       }, {
                           xtype: 'datefield'
                            , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                            , allowBlank: false
                            , format: 'd/m/Y'
                            , name: 'StartDate'
                            , ref: 'startdate'
                       }, {
                           xtype: 'datefield'
                            , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                            , allowBlank: false
                            , format: 'd/m/Y'
                            , name: 'EndDate'
                            , ref: 'enddate'
                       }, {
                           xtype: 'datefield'
                            , fieldLabel: eBook.Client.PreWorksheetWindow_PreviousEnddate
                            , allowBlank: true
                            , format: 'd/m/Y'
                            , name: 'PreviousEndDate'
                            , ref: 'previousenddate'
                       }

                    ]
               }
            ]
            });
            eBook.Client.PreWorksheetWindow.superclass.initComponent.apply(this, arguments);
        }
    , onOpenSheet: function() {
        this.showWorksheet(this.worksheetId);
    }
    , showNew: function() {
        this.show();
    }
    , showWorksheet: function(id) {
        if (this.checkValid()) {
            var manual = this.createManual(id);
            var ed = this.formitems.enddate.getValue();
            var ass = ed.getFullYear();
            if (ed.getDate() == 31 && ed.getMonth() == 11) ass++;
            eBook.Worksheet.showManual(this.ruleApp, this.typeName, ass, manual)
            this.close();
        }
    }
    , checkValid: function() {
        return this.formitems.filename.isValid() &&
                this.formitems.language.isValid() &&
                this.formitems.startdate.isValid() &&
                this.formitems.enddate.isValid();
    }
    , show: function(rec) {
        eBook.Client.PreWorksheetWindow.superclass.show.call(this);
        if (rec) {
            this.getTopToolbar().opensheet.setText(eBook.Client.PreWorksheetWindow_Open);
            this.worksheetId = rec.json.id;
            this.formitems.filename.setValue(rec.json.n);
            this.formitems.language.setValue(rec.json.m.File.c);
            this.formitems.startdate.setValue(Ext.data.Types.WCFDATE.convert(rec.json.m.File.sd));
            this.formitems.enddate.setValue(Ext.data.Types.WCFDATE.convert(rec.json.m.File.ed));
            this.formitems.previousenddate.setValue(Ext.data.Types.WCFDATE.convert(rec.json.m.File.ped));
        }
    }
    , createManual: function(id) {
        if (!id) id = eBook.EmptyGuid;
        var ped = this.formitems.previousenddate.getValue();
        if (ped == '') {
            ped = null;
        }
        return {
            Client: null
            , File: {
                id: eBook.EmptyGuid
                , ft: null
                , cid: eBook.CurrentClient
                , n: this.formitems.filename.getValue()
                , c: this.formitems.language.getValue()
                , sd: this.formitems.startdate.getValue()
                , ed: this.formitems.enddate.getValue()
                , ped: ped
                , nl: false
                , al: 8
                , md: false
                , d: false
                , ers: 0
                , wrs: 0
                , dsp: 'display'
            }
            , Id: id
            , Description: this.formitems.filename.getValue()
        };
    }
    });