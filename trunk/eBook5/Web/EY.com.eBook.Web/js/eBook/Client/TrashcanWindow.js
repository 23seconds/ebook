
eBook.Client.TrashcanWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        var tb = [];
        var mgrRole = eBook.User.isActiveClientRolesFullAccess();
        var admRole = eBook.User.isCurrentlyChampion() || eBook.User.isCurrentlyAdmin();
        /*
        if (mgrRole) {
            tb = [{
                text: 'Empty trashcan',
                iconCls: 'eBook-pmt-refresh-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'updatepmt',
                handler: this.onEmptyTrashcan,
                scope: this
            }];
        }
        */
        Ext.apply(this, {
            layout: 'fit',
            modal: false,
            iconCls: 'eBook-recycle-menu-context-icon',
            title: 'Trashcan',
            width: 550,
            items: [{
                xtype: 'trashcanlist',
                ref: 'list',
                openFiles: true, closedFiles: false, deletedFiles: true
            , mgrRole: mgrRole, admRole: admRole
        }]
        , tbar: tb
            });
            eBook.Client.TrashcanWindow.superclass.initComponent.apply(this, arguments);
        }
    , onEmptyTrashcan: function() {
        alert('Empty list');

    }
});