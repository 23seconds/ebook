


eBook.Client.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            bodyStyle: 'padding:10px;background-color:#FFFFFF;'
            , autoScroll: true
            , tbar: [{
                ref: 'save',
                text: eBook.Client.Window_SaveChanges,
                iconCls: 'eBook-icon-tablesave-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveChangesClick,
                scope: this
}]
            , items: [
                    { xtype: 'eBook.Client.Panel'
                    , ref: 'panel'
                    , lines: [
                        {
                            xtype: 'eBook.Client.HeadingLine'
                            , heading1: ''
                            , heading2: eBook.Client.Window_GFIS
                            , heading3: eBook.Client.Window_eBookOverrule
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Name
                            , fieldType: 'textfield'
                            , ref: 'name'
                            , mapping: 'n'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Address
                            , fieldType: 'textfield'
                            , ref: 'address'
                            , mapping: 'a'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_ZipCode
                            , fieldType: 'textfield'
                            , ref: 'zipcode'
                            , mapping: 'zip'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_City
                            , fieldType: 'textfield'
                            , ref: 'city'
                            , mapping: 'ci'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Country
                            , fieldType: 'textfield'
                            , ref: 'country'
                            , mapping: 'co'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Vat
                            , fieldType: 'textfield'
                            , ref: 'vat'
                            , mapping: 'vnr'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_EnterpriseNr
                            , fieldType: 'textfield'
                            , ref: 'enterprise'
                            , mapping: 'enr'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Phone
                            , fieldType: 'textfield'
                            , ref: 'phone'
                            , mapping: 'ph'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Fax
                            , fieldType: 'textfield'
                            , ref: 'fax'
                            , mapping: 'fx'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Email
                            , fieldType: 'textfield'
                            , ref: 'email'
                            , mapping: 'em'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_LegalStruct
                            , fieldType: 'textfield'
                            , ref: 'legalstructure'
                            , mapping: 'ls'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Rpr
                            , fieldType: 'textfield'
                            , ref: 'rpr'
                            , mapping: 'rpr'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Director
                            , fieldType: 'textfield'
                            , ref: 'director'
                            , mapping: 'dn'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_VatElegible
                            , fieldType: 'yesno'
                            , ref: 'isvat'
                            , mapping: 'iv'
                        }, { xtype: 'eBook.Client.Line'
                            , fieldName: eBook.Client.Window_Active
                            , fieldType: 'yesno'
                            , ref: 'isactive'
                            , mapping: 'ia'
}]
}]
            });
            eBook.Client.Window.superclass.initComponent.apply(this, arguments);
        }
    , show: function() {
        eBook.Client.Window.superclass.show.call(this);
        this.getEl().mask(eBook.Client.Window_Loading, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.client + 'GetClientInfo'
                , method: 'POST'
                , params: Ext.encode({ cidc: {
                    Id: eBook.Interface.currentClient.get('Id')
                    }
                })
                , callback: this.onClientLoaded
                , scope: this
        });
    }
    , onClientLoaded: function(opts, s, resp) {
        if (s) {
            var o = Ext.decode(resp.responseText).GetClientInfoResult;
            this.panel.loadData(o.gfis, o.overrule);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onSaveChangesClick: function(b, e, l) {
        if (!Ext.isDefined(l)) l = false;
        var action = {
            text: eBook.Client.Window_Saving
            , params: {
                cdc: this.panel.getData()
            }
            , action: 'SaveClientOverrule'
            , url: eBook.Service.client
            , lockWindow: l
        };
        this.addAction(action);
    }
    , onBeforeClose: function() {
        if (this.panel.hasChanges()) {

            Ext.MessageBox.show({
                title: eBook.Client.Window_SaveTitle,
                msg: eBook.Client.Window_SaveMsg,
                buttons: Ext.MessageBox.YESNO,
                animEl: this.getEl(),
                fn: this.closingWithChanges,
                scope: this,
                icon: Ext.MessageBox.WARNING
            });
            return false;
        }
        return eBook.Client.Window.superclass.onBeforeClose.call(this);
    }
    , closingWithChanges: function(b) {
        if (b == 'yes') {
            this.onSaveChangesClick(null, null, true);
            this.addAction({ action: 'CLOSE_WINDOW' });
            return;
        }
        this.doClose();
    }

});
