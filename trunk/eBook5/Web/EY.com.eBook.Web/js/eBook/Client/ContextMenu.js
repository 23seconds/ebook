
eBook.Client.ContextMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'eBook-client-context-menu'
            , shadow: false
            // Add items on creation            
            });
            eBook.Client.ContextMenu.superclass.initComponent.apply(this, arguments);
        }
    
    , showMeAt: function(el) {
        eBook.Client.ContextMenu.superclass.showAt.apply(this, [el.xy]);
    }
    });

    Ext.reg('clientcontextmenu', eBook.Client.ContextMenu);