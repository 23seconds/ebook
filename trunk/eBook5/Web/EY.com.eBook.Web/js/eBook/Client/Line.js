

eBook.Client.Line = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'hbox'
            , border: false
            , bodyStyle: 'padding:5px;border-bottom:1px dashed #999999;vertical-align:middle'
            , layoutConfig: {
                defaultMargins: { top: 0, right: 0, bottom: 0, left: 10 }
            }
            , overCls: 'eBook-over'
            , items: [{
                    xtype: 'label',
                    ref:'label',
                    text: this.fieldName,
                    flex: 1,
                    style: 'padding-top:5px;'
                }, {
                    xtype: this.fieldType,
                    ref: 'gfis',
                    disabled:true,
                    flex: 1
                }, {
                    xtype: this.fieldType,
                    ref: 'override',
                    flex: 1
            }]
        });
        eBook.Client.Line.superclass.initComponent.apply(this, arguments);
        
        
        if (Ext.isDefined(this.gfisValue) || Ext.isDefined(this.eBookValue)) {
            this.setValue(this.gfisValue,this.eBookValue);
        }
        
        this.on('afterrender', function(pnl) {
             this.getEl().on('click',function(el) {
                pnl.override.focus();
             },pnl);
        },this);
    }
    , setValue: function(gfis, override) {
        this.gfis.reset();
        this.gfis.setValue(gfis);
        this.override.reset();
        this.override.setValue(override);
    }
    ,getValue:function() {
        var val = this.override.getValue();
        if (Ext.isEmpty(val)) return null;
        return val;
    }
});

Ext.reg('eBook.Client.Line', eBook.Client.Line);




eBook.Client.HeadingLine = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'hbox'
            , border: false
            , bodyStyle: 'padding:5px;border-bottom:1px solid #000000;vertical-align:middle'
            , layoutConfig: {
                defaultMargins: { top: 0, right: 0, bottom: 0, left: 10 }
            }
            , items: [{
                        xtype: 'label',
                        ref: 'label',
                        text: this.heading1,
                        flex: 1,
                        style: 'padding-top:5px;font-weight:bold;font-size:12px;'
                    }, {
                        xtype: 'label',
                        ref: 'label',
                        text: this.heading2,
                        flex: 1,
                        style: 'padding-top:5px;font-weight:bold;font-size:12px;'
                    }, {
                        xtype: 'label',
                        ref: 'label',
                        text: this.heading3,
                        flex: 1,
                        style: 'padding-top:5px;font-weight:bold;font-size:12px;'
                    }]
        });
        eBook.Client.HeadingLine.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('eBook.Client.HeadingLine', eBook.Client.HeadingLine);