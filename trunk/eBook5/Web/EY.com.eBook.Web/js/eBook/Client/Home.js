

eBook.Client.HomeHeader = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<tpl for=".">'
                , '<div class="boxSides">'
                    , '<div id="eyMenuNav" style="">'
                        , '<div class="eBook-Client">'
                            , '<div class="eBook-menu-text">{Name}</div>'
                            , '<div class="eBook-menu-close"> '
                                , '<div class="eBook-menu-title-block-toolbar-item eBook-icon-16 eBook-menu-title-block-close-ico"></div>'
                            , '</div>		'
       	                , '</div>'
                    , '</div>'
                , '</div>	'
                , '<div class="eBook-menu-info-block">'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-address">'
                         , '{Address}'
                        , '<br>{ZipCode} {City}'
                        , '<tpl if="Shared"><div class="eBook-menu-info-shared">GEDEELD DOSSIER ACR/TAX</div></tpl>'
                        , '<tpl if="bni"><div class="eBook-menu-info-shared">BNI</div></tpl>'
                     , '</div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-PMT">'
                        , 'Current role(s): <br/><b>{rollen}</b>'
                        , '<div class="eBook-menu-info-PMT-Team">{teamleden}</div>'
                    , '</div>'
                    , '<div class="eBook-menu-info-block-item eBook-menu-info-ProAcc">'
                        , '<b><u>ProAcc</u></b><br>'
                        , '<tpl if="ProAccServer==null || ProAccServer==\'\'">{NoProAcc}</tpl>'
                        , '<tpl if="ProAccServer!=null && ProAccServer!=\'\'">'
                            , 'Database: <b>{ProAccDatabase}</b>'
                            , '<br/>Server: <b>{ProAccServer}</b>'
    // , '<br/>Link updated: <b>{ProAccLinkUpdated:date("d/m/Y H:i:s")}</b>'
                        , '</tpl>'
                    , '</div>'
                 , '</div>'
                , '<div style="visibility: visible;" class="eBook-menu">'
                    , '<div class="eBook-menu-childcontainer">'
                        , '<div class="eBook-menu-item" id="eBook-Client-suppliers">'
                            , '<div class="eBook-menu-item-icon eBook-suppliers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{leveranciers}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-Client-customers">'
                            , '<div class="eBook-menu-item-icon eBook-customers-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{klanten}</div>'
                        , '</div>'
                        , '<div id="eBook-Client-files-add" class="eBook-menu-item ">'
                            , '<div class="eBook-menu-item-icon eBook-createfile-menu-ico">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                            , '<div class="eBook-menu-item-title">{nieuwdossier}</div>'
                        , '</div>'
                        , '<div class="eBook-menu-item" id="eBook-Client-files-deleted">'
                            , '<div class="eBook-menu-item-icon eBook-marked-deletion-files-menu-ico  eBook-notitle eBook-recyclebin-full">'
                                , '<div class="eBook-menu-item-alert-icon"></div>'
                            , '</div>'
                        , '</div>'
                    , '</div>'
                , '</div>'
            , '</tpl>', { compiled: true })
    , initComponent: function() {

        Ext.apply(this, {
            html: '<div>open client</div>'
            , border: false
        });
        eBook.Client.HomeHeader.superclass.initComponent.apply(this, arguments);
    }
    , afterRender: function() {
        eBook.Client.HomeHeader.superclass.afterRender.call(this);

        // set events
        this.el.on('mouseover', this.onBodyMouseOver, this);
        this.el.on('mouseout', this.onBodyMouseOut, this);
        this.el.on('click', this.onBodyClick, this);
        //this.el.on('contextmenu', this.onBodyContext, this);
    }
    , onBodyMouseOver: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        if (mnuItem) {
            Ext.get(mnuItem).addClass('eBook-menu-item-over');
            //   Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '90%');
        }
    }
    , onBodyMouseOut: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        if (mnuItem) {
            Ext.get(mnuItem).removeClass('eBook-menu-item-over');
            // Ext.get(mnuItem).child('.eBook-menu-item-icon').setStyle('background-size', '100%');
        }
    }
    , onBodyClick: function(e, t, o) {
        var mnuItem = e.getTarget('.eBook-menu-item');
        var closeClient = e.getTarget('.eBook-menu-close');
        var team = e.getTarget('.eBook-menu-info-PMT-Team');

        if (mnuItem) {
            eBook.Interface.openWindow(mnuItem.id);
        } else if (closeClient) {
            eBook.Interface.closeClient();
        } else if (team) {
            eBook.Interface.openWindow('clientteam');
        }
    }
    , updateMe: function(rec) {
        if (this.trashcanDDTarget) this.trashcanDDTarget.destroy();
        var dta = {};
        Ext.apply(dta, {
            rollen: eBook.User.activeClientRoles
            , teamleden: eBook.PMT.TeamWindow_Title
            , NoProAcc: 'GEEN PROACC CONNECTIE'
            , leveranciers: eBook.Menu.Client_Suppliers
            , klanten: eBook.Menu.Client_Customers
            , nieuwdossier: eBook.Menu.Client_NewFile
        });
        Ext.apply(dta, rec.data);
        this.body.update(this.mainTpl.apply(dta));
        this.trashcanEl = this.body.child('#eBook-Client-files-deleted');
        this.trashcanDDTarget = new Ext.dd.DropTarget(this.trashcanEl, {
            ddGroup: 'filesDDGroup'
            , notifyDrop: function(ddSource, e, data) {
                eBook.Interface.center.clientMenu.files.openFiles.onDeleteClick(data.draggedRecord);                
                return true;
            }
        });
    }
    /*
    , highlightTrashCan: function() {
    if (this.trashcanEl) {
    this.trashcanEl.addClass('eBook-menu-item-over');
    //this.trashcanEl.setStyle('background-color','red');
    }
    }
    , unHighlightTrashCan: function() {
    if (this.trashcanEl) {
    this.trashcanEl.removeClass('eBook-menu-item-over');
    //this.trashcanEl.setStyle('background-color', 'white');
    }
    }
    */
});


Ext.reg('clienthomeheader', eBook.Client.HomeHeader);

eBook.Client.HomeFiles = Ext.extend(Ext.TabPanel, {
    initComponent: function() {
        Ext.apply(this, {
            items: [
                { xtype: 'filelist', openFiles: true, closedFiles: false, deletedFiles: false
                    , title: 'Open files', ref: 'openFiles'
                }
                , { xtype: 'filelist', openFiles: false, closedFiles: true, deletedFiles: false
                    , title: 'Closed files', ref: 'closedFiles'
                }
                ]
            , activeTab: 0
        });
        eBook.Client.HomeFiles.superclass.initComponent.apply(this, arguments);
    }
    , loadTabs: function(clientId) {
        this.openFiles.activeClient = clientId;
        this.closedFiles.activeClient = clientId;
        this.openFiles.store.load({ params: { ClientId: clientId} });
        this.closedFiles.store.load({ params: { ClientId: clientId} });
        this.trashcanFiles = this.getCountTrashcanFiles(clientId);
        
    }
    , reloadTabs: function() {
        if (this.openFiles.activeClient) {
            this.loadTabs(this.openFiles.activeClient);
            this.dragAndDrop();
        }
    }
    , getCountTrashcanFiles: function(clientId) {
        Ext.Ajax.request({
            url: eBook.Service.file + 'GetFileInfos'
            , method: 'POST'
            , params: Ext.encode({ cfdc: { ClientId: clientId, MarkedForDeletion: true, Deleted: false, Closed: false} })
            , callback: this.handleCallbackTrashcanFiles
            , scope: this
        });
    }
    , handleCallbackTrashcanFiles: function(opts, success, resp) {
        var o = Ext.decode(resp.responseText);
        var parent = Ext.get('eBook-Client-files-deleted');
        var icon = parent.select('div.eBook-marked-deletion-files-menu-ico');
        if (o.GetFileInfosResult.length > 0) {
            icon.addClass('eBook-recyclebin-full');
        } else {
            icon.removeClass('eBook-recyclebin-full');
        }
    }
    , getFileName: function(id) {
        var idx = this.openFiles.store.find('Id', id);
        if (idx > -1) {
            return this.openFiles.store.getAt(idx).get('Name');
        } else {
            var idx = this.closedFiles.store.find('Id', id);
            if (idx > -1) {
                return this.closedFiles.store.getAt(idx).get('Name');
            }
        }
        return 'deleted file';
    }
    , getLatestId: function() {

        var ro, rc;
        if (this.openFiles.store.getCount() > 0) {
            ro = this.openFiles.store.getAt(0);
        }
        if (this.closedFiles.store.getCount() > 0) {
            rc = this.closedFiles.store.getAt(0);
            if (ro) {
                if (ro.get('EndDate') < rc.get('EndDate')) ro = rc;
            } else {
                ro = rc;
            }
        }
        if (ro) {
            return ro.get('Id');
        }
        return null;
    }
    , dragAndDrop: function() {
        this.get


        /*
        // Configure the cars to be draggable
        var carElements = Ext.get('cars').select('div');
        Ext.each(carElements.elements, function(el) {
        var dd = new Ext.dd.DD(el, 'carsDDGroup', {
        isTarget  : false
        });
        Ext.apply(dd, overrides);
        });
        */
    }
});
Ext.reg('clienthomefiles', eBook.Client.HomeFiles);


eBook.Client.FilesList = Ext.extend(Ext.list.ListView, {
    initComponent: function() {
        Ext.apply(this, {
            store:
                 new Ext.data.JsonStore({
                     autoDestroy: true,
                     root: 'GetFileInfosResult',
                     fields: eBook.data.RecordTypes.FileInfo,
                     baseParams: {
                         MarkedForDeletion: this.deletedFiles
                        , Deleted: false
                        , Closed: this.closedFiles
                     },
                     proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                         url: eBook.Service.file + 'GetFileInfos'
                         , criteriaParameter: 'cfdc'
                         , method: 'POST'
                     }),
                     listeners: {
                     /*

                         'load': function(store, records, options) {

                             // Create an object that we'll use to implement and override drag behaviors a little later
                     var overrides = {
                     // Called the instance the element is dragged.
                     b4StartDrag: function() {
                     // Cache the drag element
                     if (!this.el) {
                     this.el = Ext.get(this.getEl());
                     }
                     this.el.setStyle('z-index', 8000);
                     this.el.setStyle('border', '1px solid');



                                     //Cache the original XY Coordinates of the element, we'll use this later.
                     this.originalXY = this.el.getXY();
                     },
                     // Called when element is dropped not anything other than a dropzone with the same ddgroup
                     onInvalidDrop: function() {
                     // Set a flag to invoke the animated repair
                     this.invalidDrop = true;
                     },
                     // Called when the drag operation completes
                     endDrag: function() {
                     // Invoke the animation if the invalidDrop flag is set to true
                     if (this.invalidDrop === true) {
                     // Remove the drop invitation
                     this.el.removeClass('dropOK');

                                         // Create the animation configuration object
                     var animCfgObj = {
                     easing: 'elasticOut',
                     duration: 1,
                     scope: this,
                     callback: function() {
                     // Remove the position attribute
                     this.el.dom.style.position = '';
                     }
                     };

                                         // Apply the repair animation
                     this.el.moveTo(this.originalXY[0], this.originalXY[1], animCfgObj);
                     delete this.invalidDrop;
                     this.el.addClass('cancelOnClick');
                     }

                                 },
                     // Called upon successful drop of an element on a DDTarget with the same
                     onDragDrop: function(evtObj, targetElId) {
                     // Wrap the drop target element with Ext.Element
                     var dropEl = Ext.get(targetElId);

                                     // Perform the node move only if the drag element's 
                     // parent is not the same as the drop target
                     if (this.el.dom.parentNode.id != targetElId) {

                                         // Move the element
                     dropEl.appendChild(this.el);

                                         // Remove the drag invitation
                     this.onDragOut(evtObj, targetElId);

                                         // Clear the styles
                     this.el.dom.style.position = '';
                     this.el.dom.style.top = '';
                     this.el.dom.style.left = '';
                     }
                     else {
                     // This was an invalid drop, initiate a repair
                     this.onInvalidDrop();
                     }
                     }
                     };

                             // Configure the cars to be draggable
                     var fileElements = Ext.select('.client-home-openFiles dl');
                     Ext.each(fileElements.elements, function(el) {
                     var dd = new Ext.dd.DD(el, 'filesDDGroup', {
                     isTarget: false
                     });
                     Ext.get(el).setStyle('z-index', '80000');

                                 //Apply the overrides object to the newly created instance of DD
                     Ext.apply(dd, overrides);
                     });

                         }*/
                 }
             })
            , multiSelect: false
            , loadingText: 'Loading files...'
            , emptyText: eBook.Menu.Client_NoOpenFiles
            , reserveScrollOffset: false
            , cls: 'client-home-openFiles'
            , autoHeight: true
            , listeners: {
                'click': {
                    fn: function(lv, idx, nd, e) {
                        //eBook.Interface.center.clientMenu.mymenu.unHighlightTrashCan();
                        if ((nd.className).indexOf('cancelOnClick') == -1) {
                            eBook.Interface.openFile(lv.getStore().getAt(idx), true);
                        } else {
                            Ext.get(nd).removeClass('cancelOnClick');
                        }
                        //eBook.Interface.clientMenu.toggleGroupBy('eBook-Client-files-openlist');
                    }
                , scope: this
                }
                ,
                'contextmenu': {
                    fn: this.onMyBodyContext
                    /*function() {
                    eBook.Client.FilesList.superclass.afterRender.call(this);
                    this.innerBody.on('contextmenu', this.onBodyContext, this);
                    }*/
                    , scope: this

                }

            }
            , columns: [new eBook.List.QualityColumn({
                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: eBook.Menu.Client_FileName,
                width: .55,
                dataIndex: 'Name'
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'StartDate'
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'EndDate'
}]


            });
            eBook.Client.FilesList.superclass.initComponent.apply(this, arguments);
        }
        , afterRender: function() {

            var v = this;
            if (v.ref == 'openFiles') {
                v.dragZone = new Ext.dd.DragZone(v.innerBody, {
                    ddGroup: 'filesDDGroup',
                    afterInvalidDrop: function() {
                        //eBook.Interface.center.clientMenu.mymenu.unHighlightTrashCan();
                    },
                    getDragData: function(e) {


                        var sourceEl = e.getTarget('dl', 10);


                        if (sourceEl) {
                            
                            d = sourceEl.cloneNode(true);
                            //  d.setStyle('z-index', 8000);
                            //d = Ext.DomHelper.createDom("<div> What the... you're dragging me!</div>");
                            //v.getEl().appendChild(d);
                            d.id = Ext.id();                            

                            return v.dragData = {
                                ddel: d,
                                sourceEl: sourceEl,
                                repairXY: Ext.fly(sourceEl).getXY(),
                                sourceStore: v.store,
                                draggedRecord: v.getRecord(sourceEl)
                            };
                            
                        }
                    },
                    getRepairXY: function() {
                        return this.dragData.repairXY;
                    }
                });
            }
            eBook.Client.FilesList.superclass.afterRender.apply(this, arguments);
        }
        , onMyBodyContext: function(dv, idx, nde, e) {
            //var el = e.getTarget();
            if (dv.ref == 'openFiles') {
                //eBook.Interface.center.clientMenu.mymenu.unHighlightTrashCan();
                if (!this.contextMenu) {
                    this.contextMenu = new eBook.Client.ContextMenu({ scope: this });
                    this.contextMenu.addItem({
                        text: 'Delete',
                        iconCls: 'eBook-icon-delete-16',
                        handler: function() {
                            this.scope.onDeleteClick(this.activeRecord);
                        },
                        ref: 'deleteFile',
                        scope: this.contextMenu
                    });
                }
                this.contextMenu.activeRecord = dv.store.getAt(idx);



                this.contextMenu.showMeAt(e);
            }

        }
        , onDeleteClick: function(activeRecord) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'SetFileMarkDeleted'
                , method: 'POST'
                , params: Ext.encode({ cidc: { Id: activeRecord.get('Id')} })
                 , callback: this.onRestoreClickCallback
                , scope: this
            });
        }
        , onRestoreClickCallback: function(opts, success, resp) {
            if (success) {
                this.store.load({ params: { ClientId: this.activeClient} });
                eBook.Interface.center.clientMenu.files.reloadTabs();
            }
        }
    });

Ext.reg('filelist', eBook.Client.FilesList);

 //, style: 'background-color:#FFF;margin-left:60px;margin-right:60px;'
                        //, bodyStyle:'background-color:transparent;margin-left:60px;margin-right:60px;'
eBook.Client.Home = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , items: [{ xtype: 'clienthomeheader', region: 'north', height: 250,ref:'mymenu' }
                      , {xtype:'panel', region:'center'
                            , bodyStyle: 'padding-left:60px;padding-right:60px'
                            , layout: 'fit'
                            , border:false
                            ,items: [
                                { xtype: 'panel', layout: 'border', border: false
                                     ,items : [
                                        {ref:'../../repository', xtype: 'repository', region: 'center', title:'Repository',border:true }
                                        , { ref: '../../files', xtype: 'clienthomefiles', region: 'east', width: 400, title: 'Files' }
                                        ]
                                }]
                      }]
        });
        eBook.Client.Home.superclass.initComponent.apply(this, arguments);
    }
    , loadRec: function(rec) {
        this.mymenu.updateMe(rec);
        this.files.loadTabs(rec.get('Id'));
    }
});

Ext.reg('clienthome', eBook.Client.Home);