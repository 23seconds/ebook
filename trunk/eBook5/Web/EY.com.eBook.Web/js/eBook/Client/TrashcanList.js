
eBook.Client.TrashcanList = Ext.extend(Ext.list.ListView, {
    initComponent: function() {
        var contextMenuListener;
        if (this.mgrRole) {
            contextMenuListener = {
                'contextmenu': {
                    fn: this.onMyBodyContext
                    , scope: this
                }
            };
        }
        Ext.apply(this, {
            store:
                 new Ext.data.JsonStore({
                     autoDestroy: true,
                     autoLoad: true,
                     root: 'GetFileInfosResult',
                     fields: eBook.data.RecordTypes.FileInfo,
                     baseParams: {
                         MarkedForDeletion: this.deletedFiles
                        , Deleted: false
                        , Closed: false
                        , ClientId: eBook.CurrentClient
                     },
                     proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                         url: eBook.Service.file + 'GetFileInfos'
                         , criteriaParameter: 'cfdc'
                         , method: 'POST'
                     })
                 })
            , multiSelect: false
            , loadingText: 'Loading files...'
            , emptyText: eBook.Menu.Client_NoOpenFiles
            , reserveScrollOffset: false
            , autoHeight: true
            , listeners: contextMenuListener
            , columns: [new eBook.List.QualityColumn({
                header: '',
                width: .05,
                dataIndex: 'Name'
            }), {
                header: eBook.Menu.Client_FileName,
                width: .55,
                dataIndex: 'Name'
            }, {
                header: eBook.Menu.Client_StartDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'StartDate'
            }, {
                header: eBook.Menu.Client_EndDate,
                xtype: 'datecolumn',
                format: 'd/m/Y',
                width: .2,
                dataIndex: 'EndDate'
}]


            });
            eBook.Client.TrashcanList.superclass.initComponent.apply(this, arguments);
        }
        , onMyBodyContext: function(dv, idx, nde, e) {
            //var el = e.getTarget();

            if (!this.contextMenu) {
                this.contextMenu = new eBook.Client.ContextMenu({ scope: this });
                this.contextMenu.addItem({
                    text: 'Delete permanently',
                    iconCls: 'eBook-icon-delete-16',
                    handler: function() {
                        this.scope.onPermanentDeleteClick(this.activeRecord);
                    },
                    ref: 'deleteFile'
                    , scope: this.contextMenu
                });
                //if (this.admRole) {
                if (this.mgrRole) {
                    this.contextMenu.addItem({
                        text: 'Restore',
                        iconCls: 'eBook-icon-arrowrefresh-16',
                        handler: function() {
                            this.scope.onRestoreClick(this.activeRecord);
                        },
                        ref: 'restoreFile',
                        scope: this.contextMenu
                    });
                }
            }
            this.contextMenu.activeRecord = dv.store.getAt(idx);


            this.contextMenu.showMeAt(e);

        }
        , onPermanentDeleteClick: function(activeRecord) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'DeleteFile'
                    , method: 'POST'
                    , params: Ext.encode({ cidc: { Id: activeRecord.get('Id')} })
                     , callback: function(opts, success, resp) { success ? this.store.load({ params: { ClientId: this.activeClient} }) : alert('failure') }
                    , scope: this
            });
        }
        , onRestoreClick: function(activeRecord) {
            Ext.Ajax.request({
                url: eBook.Service.file + 'SetFileUnMarkDeleted'
                        , method: 'POST'
                        , params: Ext.encode({ cidc: { Id: activeRecord.get('Id')} })
                         , callback: this.onRestoreClickCallback
                        , scope: this
            });
        }
        , onRestoreClickCallback: function(opts, success, resp) {
            if (success) {
                this.store.load({ params: { ClientId: this.activeClient} });
                eBook.Interface.center.clientMenu.files.reloadTabs();
            }
        }
    });

    Ext.reg('trashcanlist', eBook.Client.TrashcanList);