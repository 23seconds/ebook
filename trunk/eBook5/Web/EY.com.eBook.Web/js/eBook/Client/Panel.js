

eBook.Client.Panel = Ext.extend(Ext.FormPanel, {
    lines: []
    , initComponent: function() {

        Ext.apply(this, {
            border: false
            , items: this.lines
        });
        eBook.Client.Panel.superclass.initComponent.apply(this, arguments);
    }
    , loadData: function(gfisObject, overruleObject) {
        for (var i = 0; i < this.lines.length; i++) {
            var line = this.lines[i];
            if (line.xtype == "eBook.Client.Line") this[line.ref].setValue(gfisObject[line.mapping], overruleObject[line.mapping]);
        }
        this.clientOverrule = overruleObject;
    }
    , getData: function() {
        for (var i = 0; i < this.lines.length; i++) {
            if (this.lines[i].xtype == "eBook.Client.Line") {
                this.clientOverrule[this.lines[i].mapping] = this[this.lines[i].ref].getValue();
            }
        }
        return this.clientOverrule;
    }
    , hasChanges: function() {
        for (var i = 0; i < this.lines.length; i++) {
            if (this.lines[i].xtype == "eBook.Client.Line") {
                var b = this.clientOverrule[this.lines[i].mapping] == this[this.lines[i].ref].getValue();
                if (!b) return true;
            }
        }
        return false;
    }
});

Ext.reg('eBook.Client.Panel', eBook.Client.Panel);
        