
eBook.Client.ManualResourcesWindow = Ext.extend(eBook.Window, {
    currentData:[]
    ,initComponent: function() {
        Ext.apply(this, {
            title: 'eBook resources'
            , layout: 'border'
            , modal:true
            , items: [{ xtype: 'ManualResourceSelectionTab', ref: 'tabs', region: 'center', culture: this.culture, manualId: this.manualId }
                        , { xtype: 'ResourcesSelected', ref: 'overview', region: 'east', width: 250 }
            ]
            , bbar: ['->',{
                text: 'Cancel',
                iconCls: 'eBook-no-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'cancelbtn',
                handler: this.close,
                scope: this
            }, {
                text: 'OK (x selecties)',
                iconCls: 'eBook-yes-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'okbtn',
                handler: this.onOK,
                scope: this
            }]
            , tbar: [{
                text: eBook.Pdf.Window_AddPdf,
                iconCls: 'eBook-pdf-add-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'addpdf',
                handler: this.onAddPdf,
                scope: this
                }]
        });
        eBook.Client.ManualResourcesWindow.superclass.initComponent.apply(this, arguments);
    }
    , onAddPdf: function() {
        var wn = new eBook.Pdf.ManualUploadWindow({ parentCaller: this,manualId:this.manualId });
        wn.show(this);
    }
    , refreshPdfs: function() {
        this.tabs.store.reload();
    }
    , updateSelected: function(dta) {
        this.currentData=dta;
        this.overview.loadData(dta);
        this.getBottomToolbar().okbtn.setText("OK (" + dta.length + " selecties)");
    }
    ,show:function(src) {
        this.src = src;
        eBook.Client.ManualResourcesWindow.superclass.show.call(this);
    }
    , onOK: function(e) {
        if(this.src && this.src.resourcesPicked) {
            this.src.resourcesPicked(this.currentData);
        }
        this.close();
    }
});
Ext.reg('ManualResourcesWindow', eBook.Client.ManualResourcesWindow);

eBook.Client.ManualResourceSelectionTab = Ext.extend(Ext.DataView, {

    initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.eBook-resource-item'
            , style: 'padding:5px'
            , title: 'Pdf repository'
            , autoScroll: true
            , store: new eBook.data.JsonStore({
                selectAction: 'GetResourcesOfType'
                            , autoDestroy: true
                            , autoLoad: true
                            , criteriaParameter: 'crtdc'
                            , fields: eBook.data.RecordTypes.Resource
                            , baseParams: {
                                'Culture': this.culture
                                , 'Id': this.manualId
                                , 'ResourceType': 'MPDF'
                            }
            })
            , tpl: new Ext.XTemplate('<tpl for=".">'
                            , '<div class="eBook-resource-item {iconCls}">{Title}</div><div class="x-clear"></div>'
                            , '</tpl>', { compiled: true })
            , selectedClass: 'x-list-selected'
            , simpleSelect: true
            , multiSelect: true
            , overClass: 'x-list-over'
            // , data: [{ id: 1, title: 'test', __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'}]
            , prepareData: function(dta) {
                dta.iconCls = 'test';
                switch (dta.DC) {
                    //case "PDFManualDataContract:#EY.com.eBook.API.Contracts.Data":
                    case "PDFDataContract:#EY.com.eBook.API.Contracts.Data":
                        dta.iconCls = 'eBook-Client-tree-pdf';
                        break;
                }
                return dta;
            }
        });
        eBook.Client.ManualResourceSelectionTab.superclass.initComponent.apply(this, arguments);
        this.on('selectionchange', this.onChecks, this);
    }
    , onChecks: function(view, nodes) {
        // var dta = Ext.pluck(this.getSelectedRecords(), "json");
        //var dta = Ext.pluck([], "json");
        this.ownerCt.updateSelected(this.getMySelections());
    }
    , getMySelections: function() {
        return Ext.pluck(this.getSelectedRecords(), "json");
    }
});



Ext.reg('ManualResourceSelectionTab', eBook.Client.ManualResourceSelectionTab);

