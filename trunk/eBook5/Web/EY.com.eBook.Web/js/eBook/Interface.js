eBook.Interface = {
    viewPort: null,
    header: null,
    footer: null,
    center: null,
    mainPage: null,
    subPage: null,
    Culture: 'nl-BE',
    updatedPanels: [],
    counter: null,
    setCultureHighlight: function () {
        var el = Ext.get('eBook-Culture-' + this.Culture);
        el.setStyle('background-color', 'rgb(204, 204, 204)');
        el.setStyle('font-weight', 'bold');
    },
    setUpHeader: function (el, r) {
        this.setCultureHighlight(el, r);
        Ext.get('eBook-Title').update('eBook ' + eBook.Version);


    },
    showEBook: function (b, e) {
        /*if (EY && EY.Notification) {
         EY.Notification.alert("Er is (was) er eentje...", "JARIG!! Congrats Lena Lake!");
         }*/
        this.headerMenu.ebook.addClass('eBook-header-menu-btn-active');
        this.headerMenu.statistics.removeClass('eBook-header-menu-btn-active');
        this.viewPort.contentPanel.getLayout().setActiveItem(0);
    },
    showStatistics: function (b, e) {
        this.headerMenu.ebook.removeClass('eBook-header-menu-btn-active');
        this.headerMenu.statistics.addClass('eBook-header-menu-btn-active');
        this.viewPort.contentPanel.getLayout().setActiveItem(1);
    },
    onRepoStructureCallback: function (opts, success, resp) {
        if (success) {
            var o = Ext.decode(resp.responseText);
            eBook.Interface.RepoStructure = o.GetStructureTreeResult;
        } else {
            eBook.Interface.RepoStructure = null;
            eBook.Interface.showResponseError(resp, "REPOSITORY STRUCTURE");
        }
    },
    GetRepoStructureById: function (tpe, id) {
        var r = Ext.encode(eBook.Interface.RepoStructure[tpe]);
        var rs = Ext.decode(r);
        return this.findRepoId(rs, id);
    },
    GetRepoStructureByName: function (folderText) {
        //combine perm and period into a new object
        if(eBook.Interface.RepoStructure.period && eBook.Interface.RepoStructure.perm) {
            var folders = JSON.parse(JSON.stringify(eBook.Interface.RepoStructure.period.concat(eBook.Interface.RepoStructure.perm)));//Ext.util.clone();
            return this.findRepoText(folders, folderText);
        }
        return null;
    },
    repoId: function (nds, id) {
        for (var i = 0; i < nds.length; i++) {
            if (nds[i].id.toLowerCase() == id.toLowerCase()) {return nds[i];}
            if (nds[i].children) {
                var res = this.findRepoId(nds[i].children, id);
                if (res) {return res;}
            }
        }
        return null;
    },
    findRepoText: function (nds, text) {
        var me = this,
            matchedNodes = [];
        console.log(nds);
        for (var i = 0; i < nds.length; i++) {
            //console.log(nds[i].text.toLowerCase() + "==" + text);
            if (nds[i].text.toLowerCase().indexOf(text.toLowerCase()) != -1) //matches text?
            {
                var foundNode = nds[i];
                if(foundNode.text.indexOf("\t") != -1)
                {
                    foundNode.text = foundNode.text.slice(0,foundNode.text.indexOf("\t")); //remove an already added path added after a tab
                }

                if(foundNode.Path.lastIndexOf("\\") > 0) { //foundNode is child node
                    foundNode.text = foundNode.text + "\t(" + foundNode.Path.substring(0, foundNode.Path.lastIndexOf("\\")) + ")"; //add tab and path to text
                }
                matchedNodes.push(foundNode);
            }
            if (nds[i].children) { //has children?
                var foundChildNodes = me.findRepoText(nds[i].children, text);
                if (foundChildNodes.length > 0) {
                    matchedNodes = matchedNodes.concat(foundChildNodes);
                }
            }
        }

        return matchedNodes;
    },
    GetRepoStructure: function (tpe) {
        var me = this;
        if(eBook.Interface.RepoStructure == null) {
            if(me.viewPort) {
                me.viewPort.getEl().mask('Retrieving repository tree');
            }
            me.counter++;
            if (me.counter < 20) {
                me.GetRepoStructure.defer(500, me, [tpe]);
                return;
            }
        }

        me.viewPort.getEl().unmask();

        var r = Ext.encode(eBook.Interface.RepoStructure[tpe]);
        return Ext.decode(r);
    },
    GetSelectionRepoStructure: function () {
        var ret = [],
            o = eBook.Interface.GetRepoStructure('perm'),
            res = [],
            containers = [],
            periods = [];

        for (var i = 0; i < o.length; i++) {
            if (o[i].Key.indexOf('CONTAINER') == -1) {
                res.push(Ext.apply({}, o[i]));
            } else {
                containers.push(Ext.apply({}, o[i]));
            }
        }

        o = eBook.Interface.GetRepoStructure('period');
        for (i = 0; i < o.length; i++) {
            periods.push(Ext.apply({}, o[i]));
        }

        ret.push({
            nodeType: 'async',
            text: 'Client Permanent',
            draggable: false,
            id: 'Permanent',
            children: res,
            expanded: true
        });
        //        ret.push({
        //            nodeType: 'async',
        //            text: 'Existing period/ebook File',
        //            draggable: false,
        //            id: 'Existingperiods',
        //            children: containers,
        //            expanded: true
        //        });
        ret.push({
            nodeType: 'async',
            text: 'Period/file based',
            draggable: false,
            id: 'Period',
            children: periods,
            expanded: true
        });

        return ret;
    },
    render: function () {

        // LOAD REPO STRUCTURES IN BACKGROUND
        Ext.Ajax.request({
            method: 'POST',
            url: eBook.Service.repository + 'GetStructureTree',
            callback: this.onRepoStructureCallback,
            scope: this,
            params: Ext.encode({ccdc: {Culture: eBook.Interface.Culture}})
        });

        eBook.Splash.setText("Building interface...");
        this.header = new Ext.Panel({
            region: 'north',
            autoLoad: 'header.html?dc=' + eBook.SpecificVersion,
            height: 90,
            style: '',
            border: false
        });
        this.header.on('afterrender', function (pnl) {
            pnl.getUpdater().on('update', this.setUpHeader, this);
        }, this);

        this.footer = new Ext.Panel({
            region: 'south',
            autoLoad: 'footer.html?dc=' + eBook.SpecificVersion,
            border: false,
            height: 28
        });

        // construc center pane
        this.constructCenter();

        // this.statistics = new eBook.Statistics.DashBoard({});

        this.viewPort = new Ext.Viewport({
            layout: 'border',
            renderTo: Ext.getBody(),
            border: false,
            items: [
                this.header,
                new Ext.Panel({
                    id: 'contentPanel',
                    ref: 'contentPanel',
                    layout: 'card',
                    layoutConfig: {
                        deferredRender: true
                    },
                    activeItem: 0,
                    autoScroll: true,
                    region: 'center',
                    items: [this.center],
                    border: false,
                    height: '100%'
                }), this.footer
            ]
        });


        this.checkPanels.defer(100, this);
    },
    constructCenter: function () {
        this.center = new eBook.Home.Cards({});
    },
    checkPanels: function () {
        if (!this.header.getUpdater().isUpdating() && !this.footer.getUpdater().isUpdating()) {
            this.rendered = true;
            this.afterRendered.defer(500, this);
        } else {
            this.checkPanels.defer(200, this);
        }
    },
    rendered: false,
    setLogin: function () {
        Ext.get('et-local-champions').update(eBook.User.chpstr);

        Ext.get('eBook.User').update(eBook.User.fullName);
        var tmp = 'No office';
        if (eBook.User.defaultOffice !== null) {tmp = eBook.User.defaultOffice.n;}
        Ext.get('eBook.Office').update(tmp);
        // var d = eBook.Language.Roles[this.activeRole] ? eBook.Language.Roles[this.activeRole] : this.activeRole;
        //Ext.get('eBook.Role').update(d);

        eBook.Interface.updateRoles(eBook.User.getComboRoles());
        eBook.Interface.applyActiveRole(eBook.User.activeRole, '');
    },
    applyActiveRole: function (role, previousRole) {
        var p = eBook.User.getActivePersonDataContract();
        if (eBook.Interface.center.clientSearch) {
            eBook.Interface.center.clientSearch.store.baseParams.Person = p;
            if (eBook.Interface.center.clientSearch.isVisible()) {
                eBook.Interface.center.clientSearch.store.load({
                    params: {
                        Start: 0,
                        Limit: 25
                    }
                });
            }
        }

        if (role.indexOf('GTH') > -1) {
            // show GTH
            if (eBook.Interface.currentFile) {eBook.Interface.closeFile();}
            if (eBook.Interface.currentClient) {eBook.Interface.closeClient();}
            eBook.Interface.center.showGth();
        } else if (previousRole && previousRole.indexOf('GTH') > -1) {
            eBook.Interface.center.showeBook();
        } else {
            eBook.Interface.center.showeBook();
        }
        var d = eBook.Language.Roles[role] ? eBook.Language.Roles[role] : role;
        Ext.get('eBook.Role').update(d);

        //Mark role as last used
        if(previousRole) {
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.home + 'SetLastUsedRole',
                //callback: Ext.Msg.alert('file removed','File was removed from repository.'),
                scope: this,
                params: Ext.encode({cprdc: {pid: eBook.User.personId, r: role}})
            });
        }
    },
    //returns a bool verifying that the active role is part of a value in the given array
    isActiveRoleAllowed: function(arrAllowedClientRoles) {
        for(var role = 0; role < arrAllowedClientRoles.length; role++)
        {
            if(eBook.User.activeRole.indexOf(arrAllowedClientRoles[role]) != -1)
            {
                return true;
            }
        }
        return false;
    },
    afterRendered: function () {
        eBook.Interface.setLogin();
        eBook.Interface.setEYLogo();
        eBook.Splash.hide.defer(1000, eBook.Splash);
        // eBook.User.on('loggedin', this.loadClients, this);
        // eBook.Splash.setText(eBook.InterfaceTrans_LoggingIn);
        // eBook.User.login();
        //        eBook.Splash.hide();
        //        eBook.Splash.hide.defer(1000, eBook.Splash);
        //        var t = new eBook.Xbrl.BizTaxWindow({});
        //        //var t = new eBook.Bundle.ResourcesWindow({});
        //        //{ layout: 'fit', title: 'BizTax test', items: [{ xtype: 'Xbrl.Wizard'}], width: 900 });
        //        t.show();

    },
    updateRoles: function (roles) {
        if (this.roleSelector) this.roleSelector.destroy();
        if (Ext.isArray(roles) && roles.length > 1) {
            this.roleSelector = new Ext.Editor({
                cls: 'x-small-editor',
                alignment: 'bl-bl?',
                offsets: [0, 3],
                listeners: {
                    complete: function (ed, value, oldValue) {
                        if (value != '' && value != null && value != oldValue) eBook.User.setActiveRole(value);
                    }
                },
                field: {
                    width: 110,
                    triggerAction: 'all',
                    xtype: 'combo',
                    editable: false,
                    forceSelection: true,
                    store: roles
                }
            });
            this.roleSelector.mon(Ext.get('eBook.Role'), 'click', function (e, t) {
                this.roleSelector.startEdit(t);
            }, this);

        }
    },
    getDefaultWidth: function () {
        return Ext.getBody().getWidth(true) * 0.9;
    },
    getDefaultHeight: function () {
        return Ext.getBody().getHeight(true) * 0.9;
    },
    openShortCut: function (record) {
        var me = this;

        if(eBook.Interface.RepoStructure == null) {
            if(me.viewPort) {
                me.viewPort.getEl().mask('Retrieving repository tree');
            }
            me.counter++;
            if (me.counter < 20) {
                me.openShortCut.defer(500, this, [record]);
                return;
            }
        }

        me.viewPort.getEl().unmask();

        eBook.User.activeClientRoles = record.json.c.roles;
        if (eBook.User.isCurrentlyChampion() && !Ext.Array.contains(eBook.User.activeClientRoles,'Champion')) eBook.User.activeClientRoles.push('Champion');
        if (eBook.User.isCurrentlyAdmin() && !Ext.Array.contains(eBook.User.activeClientRoles,'Administrator')) eBook.User.activeClientRoles.push('Administrator');

        var creader = new Ext.data.JsonReader({
            root: 'root'
            , fields: eBook.data.RecordTypes.ClientBase
        });
        var freader = new Ext.data.JsonReader({
            root: 'root'
            , fields: eBook.data.RecordTypes.FileInfo
        });

        var crec = creader.readRecords({root: [record.get('client')]});
        var frec = freader.readRecords({root: [record.get('file')]});
        crec = crec.records[0];
        frec = frec.records[0];
        eBook.Interface.openClient(crec, frec);
    },
    openClient: function (record, fileRecord) {
        // GTH team 5 reset filter period
        Ext.getCmp('GthTeam5RepoPanel') ? Ext.getCmp('GthTeam5RepoPanel').standardFilter.pe = null : null;
        Ext.getCmp('GthTeam5RepoPanel') ? Ext.getCmp('GthTeam5RepoPanel').standardFilter.ps = null : null;

        eBook.Splash.setText(String.format(eBook.InterfaceTrans_LoadingClient, record.get('Name')));
        this.currentClient = record;
        eBook.CurrentClient = record.get('Id');
        eBook.User.activeClientRoles = record.json.roles;
        if (eBook.User.isCurrentlyChampion() && !Ext.Array.contains(eBook.User.activeClientRoles,'Champion')) eBook.User.activeClientRoles.push('Champion');
        if (eBook.User.isCurrentlyAdmin() && !Ext.Array.contains(eBook.User.activeClientRoles,'Administrator')) eBook.User.activeClientRoles.push('Administrator');
        this.center.openClient(record);


        //        eBook.Splash.show();

        //        this.currentFile = null;
        //        if (this.fileMenu.isVisible()) this.fileMenu.onClose();

        //        this.clientMenu.load(record);
        if (fileRecord) {
            eBook.Interface.openFile(fileRecord, true);
        }
    },
    isClientRole: function(clientId,role) {
        var me = this;

        Ext.Ajax.request({
            url: eBook.Service.client + 'GetClientBase',
            method: 'POST',
            jsonData: {
                cidc: {
                    'Id' : clientId
                }
            },
            callback: function (options, success, resp) {
                if (success && resp.responseText) {
                    var me = this,
                        robj = Ext.decode(resp.responseText);

                    if (robj.GetClientBaseResult.role.indexOf(role) != -1){
                        return true;
                    }
                    return false;
                }
                else {
                    Ext.Msg.alert('Could not retrieve client role.', 'Something went wrong');
                }
            },
            scope: me
        });
    },
    openFile: function (idOrRecord, tocache) {
        var id = idOrRecord;
        if (!Ext.isString(idOrRecord)) {
            id = idOrRecord.get('Id');
        }

        if (tocache) {
            eBook.Splash.setText("Preparing file...");
            eBook.Splash.show();
            Ext.Ajax.request({
                url: eBook.Service.file + 'SetFileInCache'
                , method: 'POST'
                , params: Ext.encode({cfdc: {FileId: id}})
                , callback: function () {
                    eBook.Interface.openFile(idOrRecord, false);
                }
                , scope: this
            });
        } else {
            eBook.Splash.setText("Opening file");
            eBook.Splash.show();

            if (!Ext.isString(idOrRecord)) {
                eBook.Splash.setText(String.format(eBook.InterfaceTrans_LoadingFile, idOrRecord.get('Name')));
                this.currentFile = idOrRecord;
                //eBook.CurrentFile = idOrRecord;
                this.getTax();
                var msgs = eBook.Interface.reloadHealth();
                eBook.Interface.redrawWorksheetsView();
                eBook.Interface.loadRepoLinks();
            } else {
                this.currentFile = id;

                eBook.Interface.center.clientMenu.files.openFiles.store.load({
                    params: {ClientId: this.currentClient.get('Id')}, callback: function (r, opts, succ) {
                        var str = eBook.Interface.center.clientMenu.files.openFiles.store;
                        var idx = str.find('Id', eBook.Interface.currentFile);
                        if (idx > -1) {
                            eBook.Interface.openFile(str.getAt(idx), false);
                        }
                    }
                });
            }

        }

    },
    updateFile: function (idOrRecord, tocache) {
        var id = idOrRecord;
        if (!Ext.isString(idOrRecord)) {
            id = idOrRecord.get('Id');
        }

        if (!Ext.isString(idOrRecord)) {
            this.currentFile = idOrRecord;
            this.getTax();
            var msgs = eBook.Interface.reloadHealth();
            eBook.Interface.redrawWorksheetsView();
            eBook.Interface.loadRepoLinks();
        } else {
            this.currentFile = id;

            eBook.Interface.center.clientMenu.files.openFiles.store.load({
                params: {ClientId: this.currentClient.get('Id')}, callback: function (r, opts, succ) {
                    var str = eBook.Interface.center.clientMenu.files.openFiles.store;
                    var idx = str.find('Id', eBook.Interface.currentFile);
                    if (idx > -1) {
                        eBook.Interface.updateFile(str.getAt(idx), false);
                    }
                }
            });
        }
    },
    redrawWorksheetsView: function () {
        eBook.Interface.center.fileMenu.redrawWorksheetsView();
    },
    reloadHealth: function () {
        return Ext.getCmp('eBook-file-healthmonitor').store.load({
            params: {
                FileId: this.currentFile.get('Id'),
                Culture: eBook.Interface.Culture
            }
        });
    },
    reloadHealthUpdateFields: function (form) {
        this.form = form;
        Ext.getCmp('eBook-file-healthmonitor').store.load({
            params: {
                FileId: this.currentFile.get('Id'),
                Culture: eBook.Interface.Culture
            }, callback: this.reloadHealthUpdateFieldsCallback, scope: this
        });
    },
    reloadHealthUpdateFieldsCallback: function (r, options, success) {
        if (success) {
            var fields = eBook.Interface.center.fileMenu.getFieldsByTab(this.form.xtype.split('-')[3]);
            if (fields) {
                for (var i = 0; i < fields.items.length; i++) {
                    var field = fields.items[i].data;
                    var fieldName = field.Field;
                    var item = this.form.findField(fieldName);
                    if (item) {
                        item.markInvalid(field.Text);
                    }
                }
            } else {
                this.form.clearInvalid();
            }
        }
    },
    reloadHealthUpdateTabs: function (tabs) {
        this.tabs = tabs;
        Ext.getCmp('eBook-file-healthmonitor').store.load({
            params: {
                FileId: this.currentFile.get('Id'),
                Culture: eBook.Interface.Culture
            }, callback: this.reloadHealthUpdateTabsCallback, scope: this
        });
    },
    reloadHealthUpdateTabsCallback: function (r, options, success) {
        if (success) {
            for (var i = 0; i < this.tabs.length; i++) {
                this.tabs[i].setIconClass(eBook.Interface.center.fileMenu.checkHealthTab(this.tabs[i].xtype.split('-')[3]));
                //this.tabs[i].setTitle = 'test';
            }
        }
    },
    getTax: function () {
        var fid = this.currentFile.get('Id');
        var ass = this.currentFile.get('AssessmentYear');
        Ext.Ajax.request({
            url: eBook.Service.rule(ass) + 'GetTaxCalculation',
            method: 'POST',
            params: Ext.encode({cfdc: {FileId: fid, Culture: eBook.Interface.Culture}}),
            callback: this.getTaxCallback,
            scope: this
        });
    },
    showRepoLinksToolTip: function (el, tpe, linkId, caller) {
        if (eBook.Interface.RepoLinks) {
            eBook.Interface.RepoLinks.showAt(el, tpe, linkId, caller);
        }

    },
    showRepoItem: function (id) {
        var wn = new eBook.Repository.FileDetailsWindow({repositoryItemId: id});
        wn.show();
        wn.loadById();
        eBook.Interface.RepoLinks.hideTask.cancel();
        eBook.Interface.RepoLinks.hide();
    },
    loadRepoLinks: function () {
        eBook.Interface.fileRepository = {};
        if (!eBook.Interface.RepoLinks) {
            eBook.Interface.RepoLinks = new eBook.Repository.ContextLinkList({});
        }
        eBook.Interface.RepoLinks.dv.store.load();

    },
    getTaxCallback: function (opts, success, resp) {
        this.center.openFile(this.currentFile);
        if (success) {
            try {
                var o = Ext.decode(resp.responseText);
                o = o.GetTaxCalculationResult;
                this.center.setTax(o);
            } catch (e) {
                this.center.setTax(null);
            }
        } else {
            this.center.setTax(null);
        }
        eBook.Splash.hide.defer(500, eBook.Splash);
    },
    setFileCulture: function (rec) {

        var wn = new eBook.FileCultureWindow({record: rec});
        wn.record = rec;
        wn.show();
    },
    shortcutClientLoaded: function () {
        this.loadClient(this.clientSearch.store.getAt(0));
    },
    onShortcutLoaded: function () {
        this.shortcut = null;
        eBook.Splash.hide();
    },
    home: function () {
        this.currentClient = null;
        this.currentFile = null;
        if (this.fileMenu.isVisible()) this.fileMenu.onClose();
        if (this.clientMenu.isVisible()) this.clientMenu.onClose();
        this.center.layout.setActiveItem(0);
    },
    setEYLogo: function()
    {
        var imgFolder = "images/ey/",
            imgFileName = "eyLogo-2.png";

        switch(window.location.hostname)
        {
            case "BEBE-IT05238":
            case "localhost":
                imgFileName = "eyLogo-2-dev.png";
                break;
            case "DEFRANMCETBO01":
                imgFileName = "eyLogo-2-test.png";
                break;
        }
        if(Ext.get('eyLogo')) {Ext.get('eyLogo').set({src: imgFolder + imgFileName});}
    },
    showExcelImport: function (caller) {
        var wn = new eBook.Excel.UploadWindow({});
        wn.show(caller);
    },
    showExcelMapping: function (dc, caller) {
        var wn = new eBook.Excel.Window({
            title: dc.originalFileName,
            fileName: dc.fn,
            sheets: dc.sns
        });
        wn.show(caller);
    },
    showProAccImport: function (caller) {
        if (this.currentClient != null && this.currentClient.get('ProAccServer') != null && this.currentClient.get('ProAccServer') != '') {
            var wn = new eBook.ProAcc.Window({
                clientOnly: this.currentFile == null,
                includePreviousYear: this.currentFile != null ? this.currentFile.get('PreviousStartDate') != null : false,
                width: 450,
                height: 320
            });
            wn.show(caller);
        } else {

            Ext.Msg.alert(eBook.InterfaceTrans_NoProAccTitle, eBook.InterfaceTrans_NoProAccMsg);
        }
    },
    isProAccClient: function () {
        if (this.currentClient == null) return false;
        return (this.currentClient.get('ProAccServer') != null && this.currentClient.get('ProAccServer') != '');
    },
    isExactFile: function () {
        if (this.currentFile.get('ImportType') == 'exact') {
            return true;
        } else {
            return false;
        }
    },
    EmptyField: function (dta) {
        var dtas = dta.split(";");
        for (var i = 0; i < dtas.length; i++) {

        }
    },
    showResponseError: function (response, title) {
        eBook.Splash.hide();
        var msg = "";

        try {
            var fo = Ext.decode(response.responseText);
            msg = fo.Message;
        } catch (e) {
            msg = response.responseText;
            if (Ext.isEmpty(msg)) msg = response.status + ' - ' + response.statusText;
        }
        this.showError(msg, title);

    },
    showError: function (message, title) {
        eBook.Splash.hide();
        if (title === "" || title === null) {title = "Error";}
        Ext.MessageBox.show({
            title: title,
            msg: message,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR
        });
    },
    openWindow: function (id) {
        var me = this,
            serviceId = null,
            wn = null,
            fileId = null;

        switch (id) {
            case 'clientteam':
                wn = new eBook.PMT.TeamWindow({});
                wn.show();
                break;
            case 'eBook-Client-customers':
            case 'eBook-File-customers':
                wn = new eBook.BusinessRelations.Window({
                    businessRelationType: 'C'
                    , title: eBook.Menu.Client_Customers
                });
                wn.show();
                break;
            case 'eBook-Client-suppliers':
            case 'eBook-File-suppliers':
                wn = new eBook.BusinessRelations.Window({
                    businessRelationType: 'S',
                    title: eBook.Menu.Client_Suppliers
                });
                wn.show();
                break;
            case 'eBook-Client-files-add':
                var eb = new eBook.NewFile.Window({});
                eb.show();
                break;
            case 'eBook-Client-files-deleted':
                wn = new eBook.Client.TrashcanWindow({});
                wn.show();
                break;
            case 'eBook-File-repository':
                wn = new eBook.Repository.FileWindow({});
                wn.show();
                break;
            case 'eBook-File-Meta':
                wn = new eBook.Meta.Window({});
                wn.show();
                break;
            case 'eBook-File-Schema':
                wn = new eBook.Accounts.Mappings.Window({});
                wn.show();
                break;
            case 'eBook-File-journal':
                wn = new eBook.Journal.Window({});
                wn.show();
                break;
            case 'eBook-File-finalTrialBalance':
                wn = new eBook.Journal.FTBWindow({});
                wn.show();
                break;
            case 'eBook-File-documents':
                wn = new eBook.Document.Window({});
                wn.show();
                break;
            case 'eBook-File-ReportGeneration-yearend':
                serviceId = 'C3B8C8DB-3822-4263-9098-541FAE897D02'; //yearend
                fileId = eBook.Interface.currentFile.get('Id');

                if (this.hasService(serviceId) && fileId) {
                        me.handleDeliverable(serviceId, fileId);
                } else {
                    alert("This file has no yearend service.");
                }
                break;
            case 'eBook-File-ReportGeneration-AnnualAccounts':
                serviceId = '1ABF0836-7D7B-48E7-9006-F03DB97AC28B'; //AnnualAccounts NBB Filing
                fileId = eBook.Interface.currentFile.get('Id');

                if (this.hasService(serviceId)) {
                    if(serviceId && fileId) {
                        me.handleDeliverable(serviceId, fileId);
                    }
                } else {
                    Ext.Msg.show({
                        title: 'Activate Annual Accounts Filing',
                        msg: 'This file does not yet have the annual accounts service activated.<br> Do you want to activate this service now?<br><br>This wil create an Annual Accounts deliverable and allow you<br>to start the proces towards filing.',
                        buttons: Ext.Msg.YESNO,
                        fn: me.addService,
                        scope: me,
                        icon: Ext.MessageBox.QUESTION,
                        serviceId: serviceId,
                        fileId: fileId
                    });
                    //alert("gThis file has no annual accounts service.");
                }

                break;
            case 'eBook-File-Bundles':
                wn = new eBook.Bundle.Window({});
                wn.show();
                break;
            case 'eBook-File-ReportGeneration-biztax':
                var service = null;
                if (this.hasService('BD7C2EAE-8500-4103-8984-0131E73D07FA')) {
                    service = 'BD7C2EAE-8500-4103-8984-0131E73D07FA';
                } else if (this.hasService('60CA9DF9-C549-4A61-A2AD-3FDB1705CF55')) {
                    service = '60CA9DF9-C549-4A61-A2AD-3FDB1705CF55';
                } else {
                    alert("This file has no biztax service.");
                }
                wn = new eBook.Bundle.Window({serviceId: service});
                wn.show();
                break;
            case 'eBook-File-BizTax':
                wn = new eBook.Xbrl.BizTaxWindow({});
                wn.show();
                break;
            case 'eBook-File-Close-yearend':
                eBook.Interface.verifyCloseService('C3B8C8DB-3822-4263-9098-541FAE897D02');
                break;
            case 'eBook-File-Settings':
                wn = new eBook.File.ServicesWindow({});
                wn.show();
                break;
            case 'eBook-File-quickprint':
                wn = new eBook.File.QuickprintWindow({});
                wn.show();
                break;
            case 'eBook-File-leadsheets':
                eBook.Splash.setText("Generating leadsheets");
                eBook.Splash.show();
                Ext.Ajax.request({
                    url: eBook.Service.output + 'GenerateLeadSheets'
                    , method: 'POST'
                    , params: Ext.encode({
                        cidc: {
                            Culture: eBook.Interface.currentFile.get('Culture')
                            , Id: eBook.Interface.currentFile.get('Id')
                        }
                    })
                    , callback: this.onLeadSheetsReady
                    , scope: this
                });
        }
    },
    ///Retrieve any existing deliverables (service linked bundles) or supply user with option to create one when none are found
    handleDeliverable: function(serviceId,fileId)
    {
        var me = this,
        bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');

        if(me.viewPort) {
            me.viewPort.getEl().mask('Retrieving deliverable');
        }

        if(bundleEditWindow) //making sure only one instance exists, as users tend to open this window multiple times.
        {
            bundleEditWindow.destroy();
        }

        //check existence of the service and its deliverables (=serviceBundles)
        Ext.Ajax.request({
            url: eBook.Service.bundle + 'GetServiceBundles'
            , method: 'POST'
            , jsonData: {
                cfsdc: {
                    'ServiceId': serviceId
                    , 'FileId': fileId
                    , 'Culture': eBook.Interface.Culture
                }
            }
            , callback: function (options, success, resp) {
                var me = this;
                if (success && resp.responseText) {
                    var robj = Ext.decode(resp.responseText),
                        serviceBundlesCount = robj.GetServiceBundlesResult.length;

                    if (eBook.User.activeClientRoles.indexOf('Administrator') > 0) {
                        serviceBundlesCount = 2; //Admin always sees the bundle list window with all options
                    }

                    switch (serviceBundlesCount) {
                        case 0: //open create bundle window
                            var wn = new eBook.Bundle.Window({serviceId: serviceId});
                            var nwn = new eBook.Bundle.NewWindow(
                                {
                                    serviceId: serviceId,
                                    readyCallback: serviceId ? {
                                        fn: wn.onGetFileServiceStatus,
                                        scope: wn
                                    } : null, //only retrieve fileservice if service set
                                    maskedCmp: me.viewPort
                                });
                            nwn.show();
                            break;
                        case 1: //open existing bundle in edit window
                            wn = new eBook.Bundle.Window({
                                serviceId: serviceId,
                                id: 'bundleWindow',
                                maskedCmp:  me.viewPort
                            });
                            wn.getBundle(robj.GetServiceBundlesResult[0]);
                            break;
                        default: //open bundle list window
                            wn = new eBook.Bundle.Window({serviceId: serviceId, maskedCmp:  me.viewPort});
                            wn.show();
                            break;
                    }
                }
                else {
                    Ext.Msg.alert('Could not retrieve existing deliverables', 'Something went wrong');
                }
            }
            , scope: me
        });
    },
    showRepositorySelection: function (callingBtn, show) {
        if (!eBook.Interface.RepositoryMenu) {
            eBook.Interface.RepositoryMenu = new eBook.Repository.SelectTree({renderTo: Ext.getBody()});
        }
        if (show) {
            eBook.Interface.RepositoryMenu.showMenu(callingBtn);
        } else {
            eBook.Interface.RepositoryMenu.hide();
        }
    },
    onLeadSheetsReady: function (opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open('pickup/' + robj.GenerateLeadSheetsResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    },
    closeClient: function () {
        var me = this;
        if (me.currentFile) me.closeFile();
        me.currentClient = null;
        me.center.layout.setActiveItem(0);
        me.closeWindows();
    },
    closeFile: function () {
        var me = this;
        me.currentFile = null;
        me.center.fileMenu.tabs.setActiveTab(0);
        me.center.fileMenu.tabs.module.clearAll();
        me.center.layout.setActiveItem(1);
        me.closeWindows();
    },
    closeWindows: function()
    {
        //Ext.WindowMgr.each(function(cmp) {cmp.destroy();});
    },
    isFileClosed: function () {
        if (this.currentFile != null && this.currentFile.get('Closed')) {
            return true;
        } else {
            return false;
        }
    },
    hasService: function (service) {
        var svcs = eBook.Interface.currentFile.get('Services');
        if (!Ext.isArray(svcs)) return;
        for (var i = 0; i < svcs.length; i++) {
            if (svcs[i].sid.toLowerCase() == service.toLowerCase()) return true;
        }
        return false;
    },
    addService: function (btnid, txt, opts)
    {
        var me = this,
            serviceId = opts.serviceId,
            fileId = opts.fileId;

        if (serviceId && btnid == 'yes') {
            if(me.viewPort) {
                me.viewPort.getEl().mask('Retrieving deliverable');
            }
            Ext.Ajax.request({
                url: eBook.Service.file + 'AddService'
                , method: 'POST'
                , params: Ext.encode({
                    cfspdc: {
                        FileId: fileId
                        , ServiceId: serviceId
                        , Person: eBook.User.getActivePersonDataContract()
                    }
                })
                , callback: this.onServiceAdded.createDelegate(me, [serviceId], true)
                , scope: this
            });
        }
    },
    onServiceAdded: function (opts, success, resp, serviceId) {
        var me = this,
            serviceId = serviceId, //AnnualAccounts
            fileId = eBook.Interface.currentFile.get('Id');

        if(me.viewPort) {
            me.viewPort.getEl().unmask();
        }

        if (success) {
            var addedService = Ext.decode(resp.responseText).AddServiceResult;

            if(addedService)
            {
                //add service client-side
                var currentFileServices = eBook.Interface.currentFile.get('Services');
                if (!Ext.isArray(currentFileServices)) {return;}
                currentFileServices.push(addedService);

                //Open deliverable after service creation
                me.handleDeliverable(serviceId, fileId);
            }
        }
        else{
            eBook.Interface.showResponseError(resp, "Could not create service");
        }
    },
    verifyCloseService: function (serviceId) {
        if (eBook.User.isActiveClientRolesFullAccess()) {
            var wn = new eBook.Yearend.Window({});
            wn.show(eBook.Interface.serviceYearend.s);
        }

    },
    closeService: function (btnid, txt, opts) {
        if (btnid == 'yes') {
            eBook.Splash.setText("Closing service...");
            eBook.Splash.show();
            Ext.Ajax.request({
                url: eBook.Service.file + 'CloseService'
                , method: 'POST'
                , params: Ext.encode({
                    cfspdc: {
                        FileId: eBook.Interface.currentFile.get('Id')
                        , ServiceId: opts.serviceId
                        , Person: eBook.User.getActivePersonDataContract()
                    }
                })
                , callback: this.onServiceClosed
                , scope: this
            });
        }
    },
    onServiceClosed: function () {
        var clientRec = eBook.Interface.currentClient;
        var fid = eBook.Interface.currentFile.get('Id');
        this.closeClient();
        this.openClient(clientRec, fid);
    }
};