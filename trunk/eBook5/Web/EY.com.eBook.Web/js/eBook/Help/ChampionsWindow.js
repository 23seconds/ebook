
eBook.Help.ChampionsGridView = Ext.extend(Ext.grid.GroupingView, {
    initTemplates: function() {
        Ext.grid.GroupingView.superclass.initTemplates.call(this);
        this.state = {};

        var sm = this.grid.getSelectionModel();
        sm.on(sm.selectRow ? 'beforerowselect' : 'beforecellselect',
        this.onBeforeRowSelect, this);

        if (!this.startGroup) {
            this.startGroup = new Ext.XTemplate(
                '<div id="{groupId}" class=\'x-grid-group {cls} <tpl if="values.gvalue==eBook.User.defaultOffice.n">et-champion-grid-highlight</tpl>\'>',
                '<div id="{groupId}-hd" class="x-grid-group-hd" style="{style}"><div class="x-grid-group-title">', this.groupTextTpl, '</div></div>',
                '<div id="{groupId}-bd" class="x-grid-group-body">'
            );
        }
        this.startGroup.compile();

        if (!this.endGroup) {
            this.endGroup = '</div></div>';
        }
    }
});

eBook.Help.ChampionsGrid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        var storeCfg = {
            selectAction: 'GetAllChampions',
            serviceUrl:eBook.Service.home,
            fields: eBook.data.RecordTypes.Champion,
            autoDestroy: true,
            autoLoad: true,
            groupField: 'officeName'
        };

        Ext.apply(this, {
            store: new eBook.data.GroupedJsonStore(storeCfg)
            , loadMask: true
            , columnLines: true
            , columns: [{
                header: eBook.Help.ChampionsGrid_Office,
                dataIndex: 'officeName',
                align: 'left',
                // hidden: true,
                groupRenderer: function(v, unused, r, ridx, cidx, ds) {
                    return v;
                }
                    , hideable: false
            }, {
                header: eBook.Help.ChampionsGrid_FirstName,
                dataIndex: 'firstName',
                align: 'left',
                hideable: false,
                sortable: true,
                renderer: function(v, m, r, ri, ci, s) {
                    return v + ' ' + r.get('lastName');
                }
            }, {
                header: eBook.Help.ChampionsGrid_Email,
                dataIndex: 'email',
                align: 'left',
                hideable: false,
                renderer: function(v, m, r, ri, ci, s) {
                    return '<a href="mailto:' + v + '">' + v + '</a>';
                },
                sortable: true
}]
            , view: new eBook.Help.ChampionsGridView({
                forceFit: true
                , groupTextTpl: '{group}'
                , showGroupName: false
                , enableGrouping: true
                , enableGroupingMenu: false
                , enableNoGroups: false
                , hideGroupedColumn: true
            })
            });
            eBook.Help.ChampionsGrid.superclass.initComponent.apply(this, arguments);
        }

    });
Ext.reg('eBook.Help.ChampionsGrid', eBook.Help.ChampionsGrid);

eBook.Help.ChampionsWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            id: 'et-help-champions-window',
            manager: eBook.Help.WindowMgr,
            title:'eBook Champions',
            items: [{xtype:'eBook.Help.ChampionsGrid'}],
            modal: true,
            iconCls: 'eBook-Team-icon',
            layout: 'fit',
            width: 500
        });
        eBook.Help.ChampionsWindow.superclass.initComponent.apply(this, arguments);
    }
});