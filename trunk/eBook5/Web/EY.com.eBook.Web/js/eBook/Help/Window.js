
eBook.Help.Window = Ext.extend(eBook.Window, {
    constructor: function(config) {
        this.config = config;
        Ext.apply(this, config || {}, {
            id: 'et-help-window',
            manager: eBook.Help.WindowMgr,
            modal: true,
            width: 1000,
            layout: 'border',
            title: 'Help',
            items: [{ xtype: 'eBook.Help.View', region: 'center', cls:'ebook-help-view', worksheetID: config.worksheetID },
                    { xtype: 'eBook.Help.IndexView', region: 'east', cls:'ebook-help-indexview', width: 300 }]

        });
        //eBook.Help.Window.superclass.initComponent.apply(this, arguments);
        eBook.Help.Window.superclass.constructor.call(this, config);
    },
    getStore: function(){
        var storeCfg = {
            selectAction: 'GetWorksheetHelpByID',
            serviceUrl: eBook.Service.url,
            fields: eBook.data.RecordTypes.WorksheetHelp,
            autoDestroy: true,
            autoLoad: true,
            baseParams: { cicdc:{Id: this.worksheetID, Culture: eBook.Interface.Culture}}
            //,            groupField: 'id'
        };
        return storeCfg;
    }
});

eBook.Help.View = Ext.extend(Ext.DataView, {
    initComponent: function() {
        var storeCfg = this.ownerCt.getStore();

        Ext.apply(this, {
            store: new eBook.data.GroupedJsonStore(storeCfg)
            , loadMask: true
            , autoScroll: true
            ,emptyText:'No help available for this worksheet. Please sent your suggestions to ebookquestions@be.ey.com.'
            , tpl: new Ext.XTemplate(
                
                '<tpl if="(values)">',
                    '<tpl for=".">',
		            '<div class="eBook-help-block">',
		                '<div id="{subject}" class="eBook-help-block-subject">{subject}</div>',
		                '<div class="eBook-help-block-body">{body}</div>',
		                '</br>',
		                '</br>',
		            '</div>',
                '</tpl>',   
                '</tpl>',
		                     
                '<div class="x-clear"></div>'
	        )
            
            });
            eBook.Help.View.superclass.initComponent.apply(this, arguments);
        }

    });
    Ext.reg('eBook.Help.View', eBook.Help.View);
    
    eBook.Help.IndexView = Ext.extend(Ext.DataView, {
    initComponent: function() {
        var storeCfg = this.ownerCt.getStore();

        Ext.apply(this, {
            store: new eBook.data.GroupedJsonStore(storeCfg)
            , tpl: new Ext.XTemplate(
                '<h1 class="ebook-help-index-h1">Index</h1>',
		        '<tpl for=".">',
		            '<div class="ebook-help-index">',
		                '<div class="ebook-help-index-subject"><a href="#{subject}">{subject}</a></div>',
		            '</div>',
                '</tpl>',
                '<div class="x-clear"></div>'
	        )
            });
            eBook.Help.IndexView.superclass.initComponent.apply(this, arguments);
        }

    });
    Ext.reg('eBook.Help.IndexView', eBook.Help.IndexView);