
eBook.Window = Ext.extend(Ext.Window, {
    actions: []
    , locked: false
    , constrain: true
    , failedActions: []
    , busyMsg: eBook.Window_Busy
    , isBusy: false
    , haltOnActionError: false
    , actionsShowWait: false
    , actionsAutoStart: true
    , waitWindowCfg: null
    , defaultWaitWindowCfg: {}
    , constructor: function (config) {
        config.closeAction = 'close';
        if (!config.width) config.width = eBook.Interface.getDefaultWidth();
        if (!config.height) config.height = eBook.Interface.getDefaultHeight();
        if (!config.iconCls) config.iconCls = 'eBook-Window-icon';
        Ext.apply(this, config || {}, {
            bbar: new Ext.ux.StatusBar({
                defaultText: 'Ready'
            })
            , cls: 'eBook-def-window'
            , shadow: false
            , maximizable: true
        });
        this.defaultWaitWindowCfg = {
            title: eBook.Window_Busy,
            msg: this.busyMsg
        };
        eBook.Window.superclass.constructor.call(this, config);
        this.on('beforeclose', this.onBeforeClose, this);
    }
    , mask: function (txt, cls) {
        this.getEl().mask(txt, cls);
    }
    , unmask: function () {
        this.getEl().unmask();
    }
    , clearStatus: function (o) {
        if (this.getBottomToolbar() && this.getBottomToolbar().clearStatus) {
            this.getBottomToolbar().clearStatus(o);
        }
    }
    , setStatus: function (msg) {
        this.clearStatus();
        this.getBottomToolbar().setStatus(msg);
    }
    , setStatusBusy: function (msg) {
        this.getBottomToolbar().showBusy(msg);
    }
    , startActions: function (waitWinCfg) {
        if (Ext.isDefined(waitWinCfg)) this.waitWindowCfg = waitWinCfg;
        if (!this.actionsAutoStart && !this.isBusy) {
            this.performAction.defer(200, this);
        }
    }
    , addAction: function (action) {
        this.actions.push(action);
        if (!this.isBusy && this.actionsAutoStart) this.performAction.defer(200, this);
    }
    , addActions: function (actions) {
        if (!Ext.isArray(actions)) return;
        for (var i = 0; i < actions.length; i++) {
            this.actions.push(actions[i]);
        }
        if (!this.isBusy && this.actionsAutoStart) this.performAction.defer(200, this);
    }
    , performAction: function () {
        if (this.actions.length > 0 && !this.isBusy) {
            this.isBusy = true;
            var act = this.actions.shift();
            if (act.action == "CLOSE_WINDOW") {
                this.doClose();
            } else if (act.action == "DUMMY") {
                if (this.actionsShowWait) this.updateWait(act);
                this.onActionSuccess.defer(2000, this, [null, null, act]);
            } else if (Ext.isFunction(act.action)) {
                var res = false;
                if (act.scope) {
                    res = act.action.call(act.scope, act);
                } else {
                    res = act.action(act);
                }
                if (res) {
                    this.onActionSuccess(null, null, act);
                } else {
                    this.onActionFailure(null, null, act);
                }
            } else {
                if (this.actionsShowWait) this.updateWait(act);
                var txt = act.text;
                if (this.actions.length > 0) txt += ' [' + this.actions.length + ' actions left]';
                this.setStatusBusy(txt);
                if (Ext.isDefined(act.lockWindow) && act.lockWindow) this.getEl().mask(act.txt, 'x-mask-loading');
                if (this.enableFileCache) {
                    eBook.CachedAjax.request({
                        url: (act.url ? act.url : eBook.Service.url) + act.action
                        , method: 'POST'
                        , params: Ext.encode(act.params)
                        , success: function (resp, opts) { this.onActionSuccess(resp, opts, act); }
                        , failure: function (resp, opts) { this.onActionFailure(resp, opts, act); }
                        , scope: this
                    });
                } else {
                    Ext.Ajax.request({
                        url: (act.url ? act.url : eBook.Service.url) + act.action
                        , method: 'POST'
                        , params: Ext.encode(act.params)
                        , success: function (resp, opts) { this.onActionSuccess(resp, opts, act); }
                        , failure: function (resp, opts) { this.onActionFailure(resp, opts, act); }
                        , scope: this
                    });
                }
            }
        } else {
            this.isBusy = false;
            this.setStatus('Ready');
            if (this.waitWindow) {
                this.waitWindow.getDialog().close();
                this.waitWindow = null;
                this.waitWindowCfg = null;
            }
        }
    }
    , onActionSuccess: function (resp, opts, action) {
        
        this.isBusy = false;
        this.setStatus(action.text + ' - SUCCESS');
        if (Ext.isDefined(action.lockWindow) && action.lockWindow && this.actions.length == 0) this.getEl().unmask();
        this.performAction.defer(100, this);
    }
    , onActionFailure: function (resp, opts, action) {
        
        this.isBusy = false;
        this.failedActions.push(action);
        if (Ext.isDefined(action.lockWindow) && action.lockWindow) this.getEl().unmask();
        if (this.haltOnActionError) {
            // show messagebox
            if (this.waitWindow) {
                this.waitWindow.getDialog().close();
                this.waitWindow = null;
                this.waitWindowCfg = null;
            }
        } else {
            this.setStatus(action.text + ' - ERROR');
            this.performAction.defer(100, this);
        }
    }
    , show: function () {
        this.actions = [];
        this.failedActions = [];
        //this.actions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        eBook.Window.superclass.show.call(this);
        this.clearStatus();
    }
    , waitWindow: null
    , length: 0
    , updateWait: function (action) {
        if (!this.waitWindow) this.length = this.actions.length + 1;
        if (!this.waitWindow) {
            this.waitWindowCfg = this.waitWindowCfg ? this.waitWindowCfg : this.defaultWaitWindowCfg;
            Ext.apply(this.waitWindowCfg, {
                buttons: false,
                progress: true,
                closable: false,
                minWidth: this.minProgressWidth,
                progressText: null
            });
            this.waitWindow = Ext.Msg.show(this.waitWindowCfg);

            // this.waitWindow.getDialog().setWidth(500);
        }
        var cnt = (this.length - this.actions.length);
        Ext.Msg.updateProgress((1 / this.length) * cnt, cnt + '/' + this.length + ': ' + action.text, this.waitWindowCfg.msg);
    }
    , onBeforeClose: function () {
        if (this.myCloseAction) {
            this.myCloseAction();
        }
        if (!this.waitWindow) this.length = this.actions.length;
        if (this.actions.length > 0) {
            if (!this.waitWindow) this.waitWindow = Ext.Msg.progress(eBook.Window_Busy, this.busyMsg);
            this.actions.shift();
            var cnt = (this.length - this.actions.length);
            Ext.Msg.updateProgress((1 / this.length) * cnt, cnt + '/' + this.length, this.busyMsg);
            this.close.defer(500, this);
            return false;
        } else if (this.isBusy) {
            if (!this.waitWindow) this.waitWindow = Ext.Msg.wait(eBook.Window_Busy, this.busyMsg);
            this.close.defer(200, this);
            return false;
        } else if (this.locked) {
            return false;
        } else {
            if (this.waitWindow) {
                this.waitWindow.getDialog().close();
                this.waitWindow = null;
            }
            this.doClose();
        }

        return true;

    }
});

Ext.reg('eBook.Window', eBook.Window);

//eBook.FileCultureWindow = Ext.extend(Ext.Window, {
//    initComponent: function() {
//        Ext.apply(this, {
//            width: 300,
//            height: 200,
//            title:'SET DEFAULT LANGUAGE',
//            bodyStyle:'padding:10px;',
//            items: [{ xtype: 'label', text: 'The import of this file into the new eBook version could not automatically determine the default language of this file.', style: 'display:block;font-weight:bold;' }
//                , { xtype: 'label', text: 'Please select the default language', style: 'display:block;font-weight:bold;margin-top:5px;' }
//                , { xtype: 'languagebox', allowBlank: false, ref: 'langbox' }
//                    ]
//            , maximizable: false
//            ,modal:true
//            ,buttons:[{  text: 'Save & continue...',
//                    iconCls: 'eBook-window-save-ico',
//                    scale: 'medium',
//                    iconAlign: 'top',
//                    handler: this.onSetFileCulture,
//                    width: 70,
//                    scope: this
//                }]
//        });
//        eBook.FileCultureWindow.superclass.initComponent.apply(this, arguments);
//    }
//    , onSetFileCulture: function() {
//        if(this.langbox.isValid()) {
//            this.getEl().mask('Saving language setting...', 'x-mask-loading');
//            this.record.set('Culture', this.langbox.getValue());
//            this.record.commit();
//            Ext.Ajax.request({
//                url: eBook.Service.url + 'SetFileCulture'
//                , method: 'POST'
//                , params: Ext.encode({ cicdc: {
//                    Id:this.record.get('Id'),
//                    Culture: this.langbox.getValue()
//                }
//                })
//                , callback: this.onSetFileCultureResponse
//                , scope: this
//            });
//        }
//    }
//    , onSetFileCultureResponse: function(opts, success, resp) {
//        eBook.Interface.loadFile(this.record);
//    }
//});
//Ext.reg('eBook.FileCultureWindow', eBook.FileCultureWindow);

