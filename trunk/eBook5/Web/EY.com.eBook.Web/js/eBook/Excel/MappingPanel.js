
//eBook.Excel.ImportTypes = [['as', 'Accountschema'], ['bo', 'Booking'], ['ba', 'Balans + accountschema'], ['c', 'Customer'], ['s', 'Supplier']]



eBook.Excel.MappingPanel = Ext.extend(Ext.FormPanel, {
    constructor: function (config) {
        this.addEvents('sheetchange');
        config.items = [{
            xtype: 'combo'
                            , ref: 'sheet'
                            , name: 'sheet'
                            , fieldLabel: eBook.Excel.MappingPanel_Sheet //'Excel werkblad'
                            , triggerAction: 'all'
                            , typeAhead: false
                            , selectOnFocus: true
                            , autoSelect: true
                            , allowBlank: false
                            , forceSelection: true
                            //, editable: false
                            , nullable: false
                            , mode: 'local'
                            , itemCls: 'excel-column-field'
                            , displayField: 'field1'
                            , valueField: 'field1'
                            , store: {
                                xtype: 'simplestore',
                                fields: ['field1'],
                                data: config.sheets ? config.sheets : [''],
                                expandData: true,
                                autoDestroy: true,
                                autoCreated: true

                            }
                            , listeners: {
                                'select': {
                                    fn: this.onSheetSelect
                                        , scope: this
                                }
                            }
        }];

        config.items = config.items.concat(config.columnMappings);

        eBook.Excel.MappingPanel.superclass.constructor.call(this, config);
    },
    initComponent: function () {
        Ext.apply(this, {
            labelWidth: 150
            , labelAlign: 'top'
            , bodyStyle: 'padding:5px;'
        });
        eBook.Excel.MappingPanel.superclass.initComponent.apply(this, arguments);
    }
    , onSheetSelect: function () {
        this.fireEvent('sheetchange', this.sheet.getValue());
    }
    , getMapping: function () {
        var mappings = [];
        this.getForm().items.each(function (fld) {
            if (fld.isMappingField) {
                mappings.push({ name: fld.name, column: fld.getValue() });
            }
        }, this);
        return mappings;
    }
    , loadFile: function (fileName, sheets) {
        this.fileName = fileName;
        this.sheet.store.removeAll();
        this.sheet.store.loadData(sheets, false);

        //this.sheet.expand();
        //this.sheet.select(0);
    }
});

Ext.reg('eBook.Excel.Mapping', eBook.Excel.MappingPanel);

