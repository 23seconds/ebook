


eBook.Excel.Grid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        this.excelStore = new eBook.data.PagedJsonStore({
            selectAction: 'ExcelGetSheetData',
            serviceUrl: eBook.Service.excel,
            fields: this.getExcelMappings(),
            idField: 'Id',
            baseParams: {
                FileName: this.fileName
                , SheetName: this.sheetName
            },
            criteriaParameter: 'cesdc'
        });
        // TODO:
        this.previewStore = new eBook.data.PagedJsonStore({
            selectAction: this.previewCfg.action,
            serviceUrl: eBook.Service.excel,
            fields: eBook.data.RecordTypes.ExcelPreview,
            baseParams: {
                FileName: this.fileName
                , SheetName: this.sheetName
                , Mapping: null
                , ImportType: this.importType
            },
            criteriaParameter: 'cesmdc'
        });

        Ext.apply(this, {
            columnLines: true
            , deferRowRender: true
            //, view: new eBook.Excel.GridView({})
            , columns: this.getColumns()
            , store: this.excelStore
            , loadMask: true
            , bbar: new Ext.PagingToolbar({
                displayInfo: true,
                pageSize: 40,
                prependButtons: true,
                store: this.excelStore,
                ref: 'pgToolbar'
            })
            , tbar: [{
                ref: '../preview',
                text: eBook.Excel.Grid_Preview,
                enableToggle: true,
                iconCls: 'eBook-magnify-ico-16',
                scale: 'small',
                iconAlign: 'left',
                toggleHandler: this.onTogglePreview,
                scope: this
}]
            });
            eBook.Excel.Grid.superclass.initComponent.apply(this, arguments);
        }
    , getExcelMappings: function() {
        var maps = [];
        var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (var i = 0; i < str.length; i++) {
            var ch = str.charAt(i);
            maps.push({ name: ch, mapping: ch, type: Ext.data.Types.STRING });
        }
        return maps;
    }
    , getColumns: function() {
        var cols = [new Ext.grid.RowNumberer()];
        // loop through alfabet
        var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (var i = 0; i < str.length; i++) {
            var ch = str.charAt(i);
            cols.push({
                header: ch,
                width: 50,
                dataIndex: ch,
                align: 'left'
            });
        }
        return cols;
    }
    , getColumnModel: function() {
        return new Ext.grid.ColumnModel(this.getColumns());
    }
    , loadFile: function(fileName, sheets) {
        this.fileName = fileName;
        this.sheets = sheets;
    }
    , loadSheet: function(sheetName) {
        this.sheetName = sheetName;
        var bp = {
            FileName: this.fileName
               , SheetName: this.sheetName
        };
        if (this.preview.pressed) {
            Ext.apply(bp, this.refOwner.getDataContract());
        }
        bp.FileName = this.fileName;
        this.store.baseParams = bp;
        this.store.load({
            params: {
                Start: 0
                , Limit: 40
            }
        });
    }
    , onTogglePreview: function(btn, pressed) {
        if (this.sheetName == null || this.sheetName == '') {
            btn.toggle(false, true);
            Ext.Msg.alert(eBook.Excel.Grid_NoWorksheetTitle, eBook.Excel.Grid_NoWorksheetMsh);
            return;
        }
        if (pressed) {
            this.reconfigure(this.previewStore, this.getPreviewColumnModel());
            this.getBottomToolbar().bindStore(this.previewStore);
        } else {
            this.reconfigure(this.excelStore, this.getColumnModel());
            this.getBottomToolbar().bindStore(this.excelStore);
        }

        this.loadSheet(this.sheetName);
    }
    , getPreviewColumnModel: function() {

        return new Ext.grid.ColumnModel(this.previewCfg.columns);
    }
    });

Ext.reg('eBook.Excel.Grid', eBook.Excel.Grid);