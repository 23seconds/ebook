
// select import method. 
// (accountshema,accountschema+ebook start journal,end balans previous, suppliers, customers
// upload file
// select columns
// -->

eBook.Excel.Window = Ext.extend(eBook.Window, {
    excelDC: null
    , fileId: ''
    , initComponent: function () {
        mappingPane = this.mappingPane;
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , modal: true
            //, title: this.fileName
            , items: [
                    { ref: 'main', xtype: 'eBook.Excel.Configurator', sheets: this.sheets, fileName: this.fileName
                    },
                    { xtype: 'eBook-createFile-actions'
                        , ref: 'create'
                        , title: eBook.Excel.Window_Importing
                    }
                   ]
            , tbar: {
                xtype: 'toolbar',
                items: [{
                    xtype: 'buttongroup',
                    ref: 'generalActions',
                    id: this.id + '-toolbar-general',
                    columns: 4,
                    title: eBook.Excel.Window_Import,
                    items: [{
                        ref: '../import',
                        text: eBook.Excel.Window_ImportData,
                        iconCls: 'eBook-icon-importexcel-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onImportClick,
                        scope: this
                    }]
                }]
            }
        });
        eBook.Excel.Window.superclass.initComponent.apply(this, arguments);
    }
    , onImportClick: function () {
        if (eBook.Interface.currentFile.get('Closed')) return;
        if (this.main.hasEnabledTabs()) {
            if (!this.main.areEnabledValid()) {
                Ext.Msg.alert(eBook.Excel.Window_TabFailedTitle, eBook.Excel.Window_TabFailedMsg);
            } else {
                this.performImports(this.main.getDataContracts());
            }
            //var datacontracts
            //this.main.items.each
        } else {
            Ext.Msg.alert(eBook.Excel.Window_NoDataTitle, eBook.Excel.Window_NoDataMsg);
        }
        //this.getLayout().setActiveItem(1);
        // this.main.grid.preview();
    }
    , performImports: function (dcs) {
        //this.im
        if (eBook.Interface.currentFile.get('Closed')) return;
        var acts = {
            actions: []
            , revertActions: []
            , postActions: []
        };
        var fileID = eBook.Interface.currentFile.get('Id');

        for (var i = 0; i < this.main.items.getCount(); i++) {
            var it = this.main.items.get(i);
            var dc = it.getDataContract();
            if (dc.active) {
                var act = { url: eBook.Service.file, response: null, status: 'ready', revertByFailure: false, params: null, action: '', text: '' };
                dc.FileId = fileID;
                act.params = { cesmdc: dc };
                switch (dc.ImportType) {
                    case 'as':
                        act.action = 'ImportAccountsExcel';
                        act.text = eBook.Excel.Window_ImportAS;  //'Importeren van het rekeningschema';
                        break;
                    case 'hb':
                        act.action = 'ImportCurrentStateExcel';
                        act.text = eBook.Excel.Window_ImportHB;  //'Importeren van de balans van het huidig boekjaar';
                        break;
                    case 'vb':
                        act.action = 'ImportPreviousStateExcel';
                        act.text = eBook.Excel.Window_ImportVB;  //'Importeren van de balans van het vorig boekjaar';
                        break;
                }

                acts.actions.push(act);
            }
        }

        acts.actions.push({
            state: 'ready'
                , response: null
                , text: 'Update cache'
                , url: eBook.Service.file
                , action: 'ForceFileCacheReload'
                , params: { cfdc: { FileId: fileID, Culture: eBook.Interface.Culture} }
        });

        this.getTopToolbar().disable();
        this.layout.setActiveItem(this.create);
        this.create.on('actionsDone', this.onImportDone, this);
        this.create.setActions(acts);
        this.create.start.defer(500, this.create);
    }
    , onImportDone: function () {
        var fid = eBook.Interface.currentFile.get('Id');
        var ass = eBook.Interface.currentFile.get('AssessmentYear');
        eBook.CachedAjax.request({
            url: eBook.Service.rule(ass) + 'ForceReCalculate'
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: fid, Culture: eBook.Interface.Culture} })
                , callback: this.worksheetsUpdated
                , started: new Date().getTime()
                , scope: this
        });
    }
    , worksheetsUpdated: function () {
        if (this.callingWindow && Ext.isFunction(this.callingWindow.importDone)) this.callingWindow.importDone();
        Ext.Msg.alert(eBook.Excel.Window_ImportDoneTitle, eBook.Excel.Window_ImportDoneMsg);
        this.mappingPane.doRefresh();
        this.close.defer(500, this);
    }
    , show: function (caller) {
        this.callingWindow = caller;
        //if (Ext.isDefined(this.fileName)) {
        if (!eBook.Interface.currentFile.get('Closed')) eBook.Excel.Window.superclass.show.call(this);
        //this.getLayout().setActiveItem(1);
        //this.main.loadExcel(excelFile)
        //}
        if (eBook.Interface.currentFile.get('Closed')) this.destroy();
    }
});

Ext.reg('eBook.Excel.Window', eBook.Excel.Window);