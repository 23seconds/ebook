
eBook.Excel.MainPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , items: [{ xtype: 'eBook.Excel.Mapping', ref: 'mapping', region: 'north', height: 150,collapsible:true }, { xtype: 'eBook.Excel.Grid', ref: 'grid', region: 'center'}]
        });
        eBook.Excel.MainPanel.superclass.initComponent.apply(this, arguments);
    }
    , loadExcel: function(excelFile) {
        this.refOwner.getEl().mask('Loading data', 'x-mask-loading');
        this.fileName = excelFile.fn;
        this.refOwner.setTitle(this.fileName);
        this.mapping.loadExcel(excelFile);
        this.refOwner.getEl().unmask();
    }
    , loadSheet: function(sheet) {
        this.grid.loadSheet(this.fileName, sheet);
    }
});

Ext.reg('eBook.Excel.Main', eBook.Excel.MainPanel);
