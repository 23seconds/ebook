
eBook.Excel.Configurator = Ext.extend(eBook.Excel.CheckTabPanel, {
    initComponent: function() {
        Ext.apply(this, {
            items: [{ xtype: 'eBook.Excel.AccountSchemaPanel', title: eBook.Excel.Window_AccountSchema
                        , ref: 'accounts', sheets: this.sheets, fileName: this.fileName, cfgName: 'accountsCfg'
            }
                  , { xtype: 'eBook.Excel.BalansPanel', title: eBook.Excel.Window_CurrentBalans
                        , ref: 'current', importType: 'hb', sheets: this.sheets, fileName: this.fileName, cfgName: 'currentCfg'
                  }
                  , { xtype: 'eBook.Excel.BalansPanel', title: eBook.Excel.Window_PreviousBalans
                        , ref: 'previous', importType: 'vb', sheets: this.sheets, fileName: this.fileName, cfgName: 'previousCfg'
                  }
            ]
            , activeTab: 0
        });
        eBook.Excel.Configurator.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('eBook.Excel.Configurator', eBook.Excel.Configurator); 