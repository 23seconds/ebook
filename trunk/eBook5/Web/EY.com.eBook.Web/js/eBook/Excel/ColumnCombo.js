

eBook.Excel.ColumnCombo = Ext.extend(Ext.form.ComboBox, {
    initComponent: function () {
        Ext.apply(this, {
            store: "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")
                , triggerAction: 'all'
                , typeAhead: false
                , selectOnFocus: true
                , autoSelect: true
            //, allowBlank: false
                , forceSelection: true
                , editable: false
            //, nullable: false
                , mode: 'local'
                , width: 50
                , listWidth: 50
        });
        eBook.Excel.ColumnCombo.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('eBook.Excel.ColumnCombo', eBook.Excel.ColumnCombo);