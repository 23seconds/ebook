

eBook.Excel.CheckTabPanel = Ext.extend(eBook.TabPanel.CheckTabPanel, {
    areEnabledValid: function() {
        var aev = true;
        for (var i = 0; i < this.items.items.length; i++) {
            var pnl = this.items.items[i];
            if (!pnl.disabled) {
                var b = pnl.isValidt();
                aev = aev && b;
            }
        }
        return aev;
    }
    , loadFile: function(fileName, sheets) {
        for (var i = 0; i < this.items.items.length; i++) {
            var pnl = this.items.items[i];
            pnl.loadFile(fileName, sheets);
        }
    }
    , getDataContracts: function() {
        var dcs = {};
        var cnt = 0;
        for (var i = 0; i < this.items.items.length; i++) {
            var pnl = this.items.items[i];
            if (!pnl.disabled) {
                dcs[pnl.initialConfig.cfgName] = pnl.getDataContract();
                cnt++;
            }
        }
        dcs.length = cnt;
        return dcs;
    }
});
Ext.reg('eBook.Excel.CheckTabPanel', eBook.Excel.CheckTabPanel);

eBook.Excel.TabPanel = Ext.extend(Ext.Panel, {
    constructor: function(config) {

        var cfg = {
            layout: 'border'
            , fileName: config.fileName
            , title: config.title
            , disabled: true
            , importType: config.importType
            , cfgName: config.cfgName
            , items: [{ xtype: 'eBook.Excel.Mapping'
                     , columnMappings: config.columnMappings
                     , sheets: config.sheets
                     , fileName: config.fileName
                     , region: 'north'
                     , split: true
                     , ref: 'mappingPanel'
                     , height: config.mappingHeight ? config.mappingHeight : 150
                     , listeners: {
                         'sheetchange': {
                             fn: this.onSheetChange
                                , scope: this
                         }
                     }
            }, {
                xtype: 'eBook.Excel.Grid'
                        , ref: 'grid'
                        , fileName: config.fileName
                        , region: 'center'
                        , previewCfg: config.previewCfg
}]
            };
            eBook.Excel.TabPanel.superclass.constructor.call(this, cfg);
        }

    , loadFile: function(fileName, sheets) {
        this.fileName = fileName;
        this.mappingPanel.loadFile(fileName, sheets);
        this.grid.loadFile(fileName, sheets);
    }
    , importType: null
    , getMapping: function() {
        return this.mappingPanel.getMapping();
    }
    , onSheetChange: function(sheet) {
        this.activeSheet = sheet;
        this.grid.loadSheet(sheet);
    }
        /*,getPreviewColumns:function() {
        var mappings = this.getMapping();
        var cols = [];
        cols = cols.concat(this.previewColumns);
        for(var i=0;i<cols.length;i++) {
        cols[i].dataIndex = mappings[cols[i].mappingItem];
        }
        return cols;
        }*/
    , isDirty: function() {
        return this.mappingPanel.getForm().isDirty();
    }
    , isValidt: function() {
        var vld = this.mappingPanel.getForm().isValid();
        if (!vld) {
            this.markTabInvalid();
        } else {
            this.clearTabInvalid();
        }
        return vld;
    }
     , markTabInvalid: function() {
         Ext.get(this.tabEl).addClass('eBook-tab-invalid');
     }
    , clearTabInvalid: function() {
        Ext.get(this.tabEl).removeClass('eBook-tab-invalid');
    }
    , getDataContract: function() {
        return {
            ImportType: this.initialConfig.importType
            , FileName: this.fileName
            , SheetName: this.activeSheet
            , Culture: eBook.Interface.Culture
            , Mapping: this.getMapping()
            , txt: this.activeSheet
            , active:!this.disabled
        };
    }
    , checkValid: function() {
        if (this.disabled) return true;
        if (!this.disabled) {
            //var dc = this.getDataContract();
            //if (dc.Mapping.length == this.columnMappings.length) {
            //    return true;
            //}
            return this.isValidt();
        }
        return false;
    }
    });



    eBook.Excel.AccountSchemaPanel = Ext.extend(eBook.Excel.TabPanel, {
        constructor: function(config) {
            Ext.apply(config, {
                mappingHeight: 55
            , importType: 'as'
            , cfgName: config.cfgName
            , columnMappings: [{
                xtype: 'eBook.Excel.ColumnCombo'
                                , name: 'accountnr'
                                , ref: 'accountnr'
                                , fieldLabel: eBook.Excel.AccountSchemaPanel_AccountNr
                                , itemCls: 'excel-column-field'
                                , nullable: false
                                , allowBlank: false
                                , isMappingField: true
                                , listeners: {
                                    'change': { fn: this.onChange, scope: this }
                                }
            }, {
                xtype: 'eBook.Excel.ColumnCombo'
                                , name: 'accountdescription'
                                , ref: 'accountdescription'
                                , fieldLabel: eBook.Excel.AccountSchemaPanel_AccountDesc
                                , itemCls: 'excel-column-field'
                                , nullable: false
                                , allowBlank: false
                                , isMappingField: true
                                , listeners: {
                                    'change': { fn: this.onChange, scope: this }
                                }
            }, {
                xtype: 'languagebox'
                                , name: 'culture'
                                , ref: 'culture'
                                , fieldLabel: eBook.Excel.AccountSchemaPanel_Language
                                , itemCls: 'excel-column-field'
                                , nullable: false
                                , allowBlank: false
                                , value: eBook.Interface.Culture
                                , listeners: {
                                    'change': { fn: this.onChange, scope: this }
                                }
}]
            , previewCfg: {
                columns: [{ header: eBook.Excel.AccountSchemaPanel_AccountNr, dataIndex: 'accountnr', mappingItem: 'accountnr' }
                              , { header: eBook.Excel.AccountSchemaPanel_AccountDesc, dataIndex: 'accountdescription', mappingItem: 'accountdescription', width: 400}]
                , action: 'ExcelGetAccountSchema'
            }
            });
            eBook.Excel.AccountSchemaPanel.superclass.constructor.apply(this, arguments);
        }
    , getDataContract: function() {
        var dc = eBook.Excel.AccountSchemaPanel.superclass.getDataContract.call(this);
        dc.Culture = this.mappingPanel.getForm().findField('culture').getValue();
        dc.ImportType = 'as';
        return dc;
    }
    
    , onChange: function() { }
    });

Ext.reg('eBook.Excel.AccountSchemaPanel', eBook.Excel.AccountSchemaPanel);


eBook.Excel.BalansPanel = Ext.extend(eBook.Excel.TabPanel, {
    constructor: function(config) {
        Ext.apply(config, {
            mappingHeight: 55
            , importType: config.importType
            , cfgName: config.cfgName
            , columnMappings: [{
                xtype: 'eBook.Excel.ColumnCombo'
                , name: 'accountnr'
                , ref: 'accountnr'
                , fieldLabel: eBook.Excel.AccountSchemaPanel_AccountNr
                , itemCls: 'excel-column-field'
                , nullable: false
                , allowBlank: false
                , isMappingField: true
                , listeners: {
                    'change': { fn: this.onChange, scope: this }
                }
            }, {
                xtype: 'eBook.Excel.ColumnCombo'
                , name: 'accountdescription'
                , ref: 'accountdescription'
                , fieldLabel: eBook.Excel.AccountSchemaPanel_AccountDesc
                , itemCls: 'excel-column-field'
                , nullable: false
                , allowBlank: false
                , isMappingField: true
                , listeners: {
                    'change': { fn: this.onChange, scope: this }
                }
            }, {
                xtype: 'languagebox'
                , name: 'culture'
                , fieldLabel: eBook.Excel.AccountSchemaPanel_Language
                , itemCls: 'excel-column-field'
                , nullable: false
                , allowBlank: false
                , value: eBook.Interface.Culture
                , listeners: {
                    'change': { fn: this.onChange, scope: this }
                }
            },{
                xtype: 'eBook.Excel.ColumnCombo'
                , name: 'saldo'
                , ref: 'saldo'
                , fieldLabel: eBook.Excel.BalansPanel_Saldo
                , itemCls: 'excel-column-field'
                , nullable: false
                , allowBlank: false
                , isMappingField: true
                , listeners: {
                    'change': { fn: this.onChange, scope: this }
                }
            }]
            , previewCfg: {
            columns: [{ header: eBook.Excel.AccountSchemaPanel_AccountNr, dataIndex: 'accountnr', mappingItem: 'accountnr' }
                              , { header: eBook.Excel.AccountSchemaPanel_AccountDesc, dataIndex: 'accountdescription', mappingItem: 'accountdescription', width: 200 }
                              , { header: eBook.Excel.BalansPanel_Saldo, dataIndex: 'saldo', mappingItem: 'saldo',align:'right'}]
                , action: 'ExcelGetBalans'
            }
            });
            eBook.Excel.BalansPanel.superclass.constructor.apply(this, arguments);
        }
    , getDataContract: function() {
        var dc = eBook.Excel.BalansPanel.superclass.getDataContract.call(this);
        dc.Culture = this.mappingPanel.getForm().findField('culture').getValue();
        dc.ImportType = this.importType;
        return dc;
    }
    , onChange: function() { }
    });

    Ext.reg('eBook.Excel.BalansPanel', eBook.Excel.BalansPanel);