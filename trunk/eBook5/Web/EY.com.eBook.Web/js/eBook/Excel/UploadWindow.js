


eBook.Excel.UploadWindow = Ext.extend(eBook.Window, {

    initComponent: function () {
        mappingPane = this.mappingPane;
        Ext.apply(this, {
            title: eBook.Excel.UploadWindow_Title
            , layout: 'fit'
            , width: 620
            , height: 200
            , modal: true
            , iconCls: 'eBook-Window-excel-ico'
            , items: [new Ext.FormPanel({
                fileUpload: true
                        , items: [{
                            xtype: 'fileuploadfield'
                                    , width: 400
                                    , fieldLabel: eBook.Excel.UploadWindow_SelectFile
                                    , fileTypeRegEx: /(\.xls(x{0,1}))$/i
                                    , ref: 'file'
                        }, {
                            xtype: 'hidden'
                                        , name: 'fileType'
                                        , value: 'excel'
                        }]
                        , ref: 'excelForm'
            })]
            , tbar: [{
                text: eBook.Excel.UploadWindow_Upload,
                ref: '../saveButton',
                iconCls: 'eBook-icon-uploadexcel-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
            }]
        });

        eBook.Excel.UploadWindow.superclass.initComponent.apply(this, arguments);
    }
    , onSaveClick: function (e) {
        var f = this.excelForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Excel.UploadWindow_Invalid);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Excel.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function (fp, o) {
        //process o.result.file
        this.uploaded = true;
        this.newFileName = o.result.file;
        this.originalFileName = o.result.originalFile;
        this.getEl().mask(String.format(eBook.Excel.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.excel + 'ExcelGetBasicInfo'
                , method: 'POST'
                , params: Ext.encode({ cedc: { FileName: o.result.file} })
                , callback: this.handleFileProcessing
                , scope: this
        });
    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, this.title);
    }
    , handleFileProcessing: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var dc = Ext.decode(resp.responseText).ExcelGetBasicInfoResult;
            dc.originalFileName = this.originalFileName;
            if (this.parentCaller && Ext.isFunction(this.parentCaller.setExcelInfo)) {
                this.parentCaller.setExcelInfo(dc);
            } else {
                var wn = new eBook.Excel.Window({
                    title: dc.originalFileName,
                    fileName: dc.fn,
                    sheets: dc.sns,
                    mappingPane: this.mappingPane
                });
                wn.show(this.parentCaller);
            }
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , show: function (caller) {
        this.parentCaller = caller;
        eBook.Excel.UploadWindow.superclass.show.call(this);
    }
});