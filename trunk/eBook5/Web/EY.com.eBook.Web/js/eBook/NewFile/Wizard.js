
eBook.NewFile.Wizard = Ext.extend(Ext.Panel, {
    data: {}
    , initComponent: function () {

        Ext.apply(this, {
            layout: 'card'
            , layoutConfig: {
                // layout-specific configs go here
                titleCollapse: false,
                animate: true,
                activeOnTop: false,
                hideCollapseTool: true
            }
            , activeItem: 0
            , items: [
                      { xtype: 'eBook.NewFile.FileServices', ref: 'step1', title: 'Services' }
                    , { xtype: 'eBook.NewFile.ImportType', txtStep: 10, excelStep: 9, proaccStep: 8, exactAdminStep: 3, exactFinYearStep: 4, defineBookyearStep: 5, ref: 'step2', title: 'Select file type' }
                    , { xtype: 'eBook.NewFile.ExactSettings', ref: 'step3', title: 'Exact: Administrations', inactive: true }
                    , { xtype: 'eBook.NewFile.ExactFinYears', ref: 'step4', title: 'Exact: Financial years', inactive: true }
                    , { xtype: 'eBook.NewFile.StartEnd', ref: 'step5', title: 'Define bookyear', inactive: false }
                    , { xtype: 'eBook.NewFile.SelectPrevious', ref: 'step6', title: 'Select previous eBook File' }
                    , { xtype: 'eBook.NewFile.PreviousStartEnd', ref: 'step7', title: 'Define previous bookyear' }
                    , { xtype: 'eBook.NewFile.ProAccSettings', ref: 'step8', title: 'ProAcc import settings', inactive: true }
                    , { xtype: 'eBook.NewFile.UploadExcel', ref: 'step9', title: 'Upload Excel file', inactive: true }
                    , { xtype: 'eBook.NewFile.UploadTxt', ref: 'step10', title: 'Upload Textfile', inactive: true }
                    , { xtype: 'eBook.NewFile.FileSettings', ref: 'step11', title: 'Settings/Meta' }
                    , { xtype: 'eBook.NewFile.CreationOverview', ref: 'step12', title: 'Creation overview' }
                    , { xtype: 'eBook.NewFile.CreateFile', ref: 'step13', title: 'Create new file' }
            //, { xtype: 'eBook.NewFile.StartEnd', ref: 'step8', title: '&gt; Review mappings' }

                ]
        });
        eBook.NewFile.Wizard.superclass.initComponent.apply(this, arguments);
    }
    , previousStep: 0
    , currentStep: 1
    , maxStep: 13
    , movePrevious: function () {
        var curStep = this.getLayout().activeItem;
        var currentStep = curStep.stepId;
        if (!curStep.inactive) this.ownerCt.updateState(currentStep, curStep.validate());
        if (currentStep > 1) {
            currentStep--;

            this.getLayout().setActiveItem(this['step' + currentStep]);
            //this.previousStep--;
            curStep = this['step' + currentStep];
            if (curStep.inactive == true) {
                this.ownerCt.updateState(currentStep, 'inactive');
                this.movePrevious();
            } else {
                this.ownerCt.updateState(currentStep, null);
            }
            curStep.doLayout();
        }

    }
    , moveNext: function () {
        var curStep = this.getLayout().activeItem;
        var currentStep = curStep.stepId;
        if (!curStep.inactive) this.ownerCt.updateState(currentStep, curStep.validate());
        if (currentStep < this.maxStep) {
            currentStep++;
            //this.previousStep++;
            this.getLayout().setActiveItem(this['step' + currentStep]);
            curStep = this['step' + currentStep];
            if (curStep.inactive == true) {
                this.ownerCt.updateState(currentStep, 'inactive');
                this.moveNext();
            } else {
                this.ownerCt.updateState(currentStep, null);
            }
            curStep.doLayout();
        }

    }
    , stepStates: {}
});

Ext.reg('eBook.NewFile.Wizard', eBook.NewFile.Wizard);

eBook.NewFile.Step = {
    autoNext: true
    , applyInit: function() {
        this.stepId = parseInt(this.ref.replace('step', ''));
        if (!this.disableMove) {
            Ext.apply(this, {
                bbar: [
                    { xtype: 'button'
                        , ref: 'prev'
                        , hidden: this.stepId < 2
                        , text: eBook.Xbrl.Wizard_Previous
                        , handler: this.onPrevious
                        , scope: this
                        , iconCls: 'eBook-back-24'
                        , scale: 'medium'
                        , iconAlign: 'top'
                    }
                    , '->'
                    , { xtype: 'button'
                        , text: eBook.Xbrl.Wizard_Next
                        , ref: 'nxt'
                        , handler: this.onNext
                        , scope: this
                        , iconCls: 'eBook-next-24'
                        , scale: 'medium'
                        , iconAlign: 'top'
                    }]
                
            });
        }
        Ext.apply(this, {
            cls: 'eBook-wizard-step-inactive eBook-wizard-step',
            iconCls: 'dummy'
            , labelWidth: 200
            , bodyStyle: 'padding:20px;'
            , listeners: {
                'activate': {
                    fn: this.checkLayout
                        , scope: this
                }
                }
            });
    }
    , checkLayout: function() {
        if (this.beforeCheckLayout) this.beforeCheckLayout();
        var bb = this.getBottomToolbar();
        if (bb && !this.disableMove) {
            if (this.stepId < 2) {
                bb.prev.disable();
            } else {
                bb.prev.enable();
            }
            if (this.stepId >= this.refOwner.maxStep) {
                bb.nxt.disable();
            } else {
                bb.nxt.enable();
            }
        }
        if (this.afterCheckLayout) this.afterCheckLayout();
    }
    , setIcon: function() {

    }
    , onPrevious: function() {
        this.refOwner.movePrevious();
    }
    , onNext: function() {
        if (this.validate()) {
            this.refOwner.moveNext();
        }
    }
    , validate: Ext.emptyFn
};

eBook.NewFile.StepPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.applyInit();
        eBook.NewFile.StepPanel.superclass.initComponent.apply(this, arguments);
    }
});

Ext.override(eBook.NewFile.StepPanel, eBook.NewFile.Step);

eBook.NewFile.ImportType = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            items: [{
                xtype: 'button',
                ref: 'proacc',
                text: eBook.Create.Window_ProAcc, // 'Import from ProAcc',
                iconCls: 'eBook-proacc-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                toggleHandler: function (btn, pressed) { if (pressed) { this.onSelectType('proacc'); } },
                enableToggle: true,
                toggleGroup: 'importtype',
                style: 'margin-bottom:10px;',
                width: 200,
                scope: this
            }, {
                xtype: 'button',
                ref: 'excel',
                text: eBook.Create.Window_Excel, //'Import from Excel',
                iconCls: 'eBook-excel-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                toggleHandler: function (btn, pressed) { if (pressed) { this.onSelectType('excel'); } },
                enableToggle: true,
                toggleGroup: 'importtype',
                style: 'margin-bottom:10px;',
                width: 200,
                scope: this,
                disabled: false
            }, {
                xtype: 'button',
                ref: 'exact',
                text: eBook.Create.Window_Exact, //'Import from Excel',
                iconCls: 'eBook-exact-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                toggleHandler: function (btn, pressed) { if (pressed) { this.onSelectType('exact'); } },
                enableToggle: true,
                toggleGroup: 'importtype',
                width: 200,
                scope: this,
                disabled: false
            }]
        });
        eBook.NewFile.ImportType.superclass.initComponent.apply(this, arguments);
    }
    , selectionMade: false
    , onSelectType: function (importType) {
        if (!this.proacc.pressed && !this.excel.pressed && !this.exact.pressed) {
            this.selectionMade = false;
        } else {
            var esobj = this.refOwner['step' + this.excelStep];
            var txobj = this.refOwner['step' + this.txtStep]
            var probj = this.refOwner['step' + this.proaccStep];
            var exaobj = this.refOwner['step' + this.exactAdminStep];
            var exfyobj = this.refOwner['step' + this.exactFinYearStep];
            var dbobj = this.refOwner['step' + this.defineBookyearStep];
            var noAdminsFound = false;
            if (importType == 'proacc') {
                esobj.inactive = true;
                this.ownerCt.ownerCt.updateState(this.excelStep, 'inactive');
                probj.inactive = false
                this.ownerCt.ownerCt.updateState(this.proaccStep, 0);
                this.importType = 'ProAcc';
            } else if (importType == 'excel') {
                esobj.inactive = false;
                this.ownerCt.ownerCt.updateState(this.excelStep, 0);
                probj.inactive = true
                this.ownerCt.ownerCt.updateState(this.proaccStep, 'inactive');
                this.importType = 'Excel';
            } else if (importType == 'exact') {
                //esobj.inactive = true;
                probj.inactive = true;
                dbobj.inactive = true;
                esobj.inactive = true;
                this.ownerCt.ownerCt.updateState(this.proaccStep, 'inactive');
                this.ownerCt.ownerCt.updateState(this.excelStep, 'inactive');
                this.ownerCt.ownerCt.updateState(this.defineBookyearStep, 'inactive');

                switch (this.refOwner.step3.GetCountAdmins()) {
                    case 0:
                        alert("No Administrations found. Make sure the client has a VAT number.");
                        noAdminsFound = true;
                        break;
                    case 1:
                        exaobj.inactive = true;
                        this.ownerCt.ownerCt.updateState(this.exactAdminStep, 'inactive');
                        this.refOwner.step3.selectedExactAdmin = this.refOwner.step3.GetAdmin();
                        this.refOwner.step4.loadStore();
                        break;
                    default:
                        exaobj.inactive = false;
                        this.ownerCt.ownerCt.updateState(this.exactAdminStep, 0);
                        break;
                }

                exfyobj.inactive = false;
                this.ownerCt.ownerCt.updateState(this.exactFinYearStep, 0);
                /*
                esobj.inactive = false;
                this.ownerCt.ownerCt.updateState(this.excelStep, 0);
                */
                txobj.inactive = false;
                this.ownerCt.ownerCt.updateState(this.txtStep, 0);

                this.importType = "Exact";

                //excel
                var checkboxedTabs = Ext.query('.x-tab-check');
                for (var i = 0; i < checkboxedTabs.length; i++) {
                    checkboxedTabs[i].checked = true;
                }
            }
            if (!noAdminsFound) {
                this.importType = importType;
                this.selectionMade = true;
                if (importType == 'excel') {
                    Ext.Msg.show({
                        title: '',
                        msg: 'If this file has an existing file in exact. Please create a file by pressing on the Exact button. Do you want to continue?',
                        buttons: Ext.Msg.YESNO,
                        fn: function(btnid){ if (btnid == 'yes') { if (this.autoNext) this.onNext(); } },
                        scope: this,
                        icon: Ext.MessageBox.QUESTION
                    });
                }else{
                    if (this.autoNext) this.onNext();
                }
                
            }
        }
    }
    , validate: function () {
        this.ownerCt.ownerCt.updateState(this.stepId, this.selectionMade);
        if (!this.selectionMade && !this.inactive) {
            alert("Select one importtype");
        }
        return this.selectionMade;
    }
});

Ext.reg('eBook.NewFile.ImportType', eBook.NewFile.ImportType);


eBook.NewFile.ExactSettings = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , layoutCfg: { labelWidth: 150 }
            , items: [{
                xtype: 'combo'
                        , ref: 'exactAdmins'
                        , fieldLabel: eBook.Create.Empty.PreviousFileFieldSet_EbookFile
                        , name: 'ExactAdmins'
                        , triggerAction: 'all'
                        , typeAhead: false
                        , selectOnFocus: true
                        , autoSelect: true
                        , allowBlank: true
                        , forceSelection: true
                        , valueField: 'ExactAdminCode'
                        , displayField: 'ExactAdminDisplay'
                        , editable: false
                        , nullable: true
                        , mode: 'local'
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetExactAdmins'
                            , serviceUrl: eBook.Service.file
                            , autoLoad: true
                            , autoDestroy: true
                            , criteriaParameter: 'cidc'
                           , baseParams: {
                               Id: eBook.CurrentClient

                           }
                            , fields: eBook.data.RecordTypes.ExactAdmin
                        })
                        , width: 300
                        , listWidth: 400
            }]
        });
        eBook.NewFile.ExactSettings.superclass.initComponent.apply(this, arguments);
    },
    GetCountAdmins: function () {
        return this.exactAdmins.store.getCount();
    },

    GetAdmin: function () {
        return this.exactAdmins.store.getAt(0);
    }

});

Ext.reg('eBook.NewFile.ExactSettings', eBook.NewFile.ExactSettings);


eBook.NewFile.ExactFinYears = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , layoutCfg: { labelWidth: 150 }
            , items: [{
                xtype: 'combo'
                        , ref: 'exactFinYears'
                        , fieldLabel: eBook.Create.Exact.FinancialYears
                        , name: 'ExactFinYears'
                        , triggerAction: 'all'
                        , typeAhead: false
                        , selectOnFocus: true
                        , autoSelect: true
                        , allowBlank: true
                        , forceSelection: true
                        , valueField: 'ExactFinYearId'
                        , displayField: 'ExactFinYearDisplay'
                        , editable: false
                        , nullable: true
                , mode: 'local'
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetExactFinYears'
                            , serviceUrl: eBook.Service.file
                            //  , autoLoad: true
                            // , autoDestroy: true
                            , criteriaParameter: 'csdc'
                            , fields: eBook.data.RecordTypes.ExactFinYears
                        })
                        , width: 300
                        , listWidth: 400
            }]
        });
        eBook.NewFile.ExactFinYears.superclass.initComponent.apply(this, arguments);
    },

    loadStore: function () {
        var i = 0;
        this.exactFinYears.store.load({ params: { String: this.refOwner.step3.selectedExactAdmin.data.ExactAdminCode} });



        //this.items.items[0].store.load({ String: this.ownerCt.ownerCt.selectedExactAdmin.data.ExactAdminVat });
    }

    , validate: function () {
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        if (this.exactFinYears.getRawValue() != "") {
            return true;
        } else {
            alert("Please select a financial year in order to continue");
        }

    }
    , onNext: function () {
        if (this.validate()) {

            this.selectedExactFinYear = this.exactFinYears.getRawValue();

            var res = this.selectedExactFinYear.split(" - ");
            var resStart = res[0].split("/");
            var resEnd = res[1].split("/");
            this.selectedExactFinYearStartdate = new Date(resStart[1] + "/" + resStart[0] + "/" + resStart[2]);
            this.selectedExactFinYearEnddate = new Date(resEnd[1] + "/" + resEnd[0] + "/" + resEnd[2]);

            if (Ext.isDate(this.selectedExactFinYearEnddate)) {
                yr = this.selectedExactFinYearEnddate.getFullYear();
                if (this.selectedExactFinYearEnddate.getMonth() == 11 && this.selectedExactFinYearEnddate.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
                this.selectedExactFinYearassessmentyear = yr;
            } else {
                this.selectedExactFinYearassessmentyear = "";
            }

            eBook.NewFile.ExactFinYears.superclass.onNext.call(this);
        }

    }

});

       Ext.reg('eBook.NewFile.ExactFinYears', eBook.NewFile.ExactFinYears);


eBook.NewFile.StartEnd = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , layoutCfg: { labelWidth: 150 }
            , items: [{
                xtype: 'datefield'
                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                , allowBlank: false
                , format: 'd/m/Y'
                , name: 'StartDate'
                , ref: 'startdate'
                , width: 100
            }, {
                xtype: 'datefield'
                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                , allowBlank: false
                , format: 'd/m/Y'
                , name: 'EndDate'
                , ref: 'enddate'
                , width: 100
                , listeners: {
                    'change': {
                        fn: this.onEndDateChanged
                        , scope: this
                    }
                }
            }, {
                xtype: 'displayfield'
                , name: 'Assessmentyear'
                , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Assessmentyear
                , ref: 'assessmentyear'
}]
            });
            eBook.NewFile.StartEnd.superclass.initComponent.apply(this, arguments);
        }
    , onEndDateChanged: function(lfd, n, o) {
        if (Ext.isDate(n)) {
            yr = n.getFullYear();
            if (n.getMonth() == 11 && n.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
            this.assessmentyear.setValue(yr);
        } else {
            this.assessmentyear.setValue('');
        }
    }
    , validate: function() {
        var sd = this.startdate.isValid();
        var ed = this.enddate.isValid();
        if (sd && ed) {
            sd = this.startdate.getValue();
            ed = this.enddate.getValue();
            if (sd >= ed) {
                this.enddate.markInvalid(eBook.Create.Empty.FileDatesFieldSet_EnddateInvalid);
            } if(ed < new Date(2011,11,31)) {
                this.enddate.markInvalid('eBook 5 only handles assessmentyear 2012, assesmentyear 2011 and earlier are to be finalised in eBook 4.4 !');
            }else {
                this.ownerCt.ownerCt.updateState(this.stepId, true);
                return true;
            }
        }
        this.ownerCt.ownerCt.updateState(this.stepId, false);
        return false;
    }
    , onNext: function() {
        if (this.validate()) {
            Ext.apply(this.refOwner.data, {
                startDate: this.startdate.getValue()
                    , endDate: this.enddate.getValue()
                    ,assessmentyear: this.assessmentyear.getValue()
            });
            eBook.NewFile.StartEnd.superclass.onNext.call(this);
        }

    }
    });

Ext.reg('eBook.NewFile.StartEnd', eBook.NewFile.StartEnd);


eBook.NewFile.PreviousStartEnd = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , layoutCfg: { labelWidth: 150 }
            , items: [{
                xtype: 'checkbox'
                        , ref: 'first'
                        , id: 'PreviousStartEnd-first'
                        , boxLabel: 'First bookyear'
                        , checked: false
                        , labelWidth: 200
                        , listeners: {
                            'check': {
                                fn: this.firstChanged
                                , scope: this
                            }
                        }
            }
                    , {
                        xtype: 'datefield'
                        , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Startdate
                        , allowBlank: false
                        , format: 'd/m/Y'
                        , name: 'StartDate'
                        , ref: 'startdate'
                        , width: 100
                    }, {
                        xtype: 'datefield'
                        , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Enddate
                        , allowBlank: false
                        , format: 'd/m/Y'
                        , name: 'EndDate'
                        , ref: 'enddate'
                        , width: 100
                        , listeners: {
                            'change': {
                                fn: this.onEndDateChanged
                                , scope: this
                            }
                        }
                    }, {
                        xtype: 'displayfield'
                        , name: 'Assessmentyear'
                        , fieldLabel: eBook.Create.Empty.FileDatesFieldSet_Assessmentyear
                        , ref: 'assessmentyear'
}]
        });
        eBook.NewFile.PreviousStartEnd.superclass.initComponent.apply(this, arguments);
    }
    , firstChanged: function(chk, checked) {
        if (checked) {
            this.startdate.allowBlank = true;
            this.enddate.allowBlank = true;
            this.startdate.setValue(null);
            this.enddate.setValue(null);
            this.startdate.disable();
            this.enddate.disable();
            this.assessmentyear.setValue('-');
        } else {
            this.startdate.allowBlank = false;
            this.enddate.allowBlank = false;
            this.beforeCheckLayout();
        }
    }
    , onEndDateChanged: function(lfd, n, o) {
        if (Ext.isDate(n)) {
            yr = n.getFullYear();
            if (n.getMonth() == 11 && n.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
            this.assessmentyear.setValue(yr);
        } else {
            this.assessmentyear.setValue('');
        }
    }
    , validate: function() {
        if (this.first.checked) {
            this.ownerCt.ownerCt.updateState(this.stepId, true);
            return true;
        }
        var sd = this.startdate.isValid();
        var ed = this.enddate.isValid();
        if (sd && ed) {
            sd = this.startdate.getValue();
            ed = this.enddate.getValue();
            if (sd >= ed) {
                this.enddate.markInvalid(eBook.Create.Empty.FileDatesFieldSet_EnddateInvalid);
            }
            var ok = true;
            if (sd >= this.refOwner.data.startDate) {
                this.startdate.markInvalid("Must be before " + this.refOwner.data.startDate);
                ok = false;
            }
            if (ed >= this.refOwner.data.startDate) {
                this.enddate.markInvalid("Must be before " + this.refOwner.data.startDate);
                ok = false;
            }
            this.ownerCt.ownerCt.updateState(this.stepId, ok);
            return ok;
        }
        this.ownerCt.ownerCt.updateState(this.stepId, false);
        return false;
    }
    , onNext: function() {
        if (this.validate()) {
            Ext.apply(this.refOwner.data, {
                previousStartDate: this.startdate.getValue()
                    , previousEndDate: this.enddate.getValue()
            });
            this.refOwner.moveNext();
        }

    }
    , hasPreviousFile: function() {
        var step6 = this.refOwner.step6;
        if (this.refOwner.rendered && step6 && step6.rendered) {
            if (step6.checker.checked) {
                var val = step6.previousfile.getValue();
                var rec = step6.previousfile.store.getById(val);
                return rec;
            }
            return false;
        }
        return false;
    }
    , beforeCheckLayout: function() {
        var rec = this.hasPreviousFile();
        if (rec) {
            this.first.setValue(false);
            this.first.disable();
            this.startdate.setValue(rec.get('StartDate'));
            this.enddate.setValue(rec.get('EndDate'));
            this.startdate.disable();
            this.enddate.disable();
            this.onEndDateChanged(this.enddate, this.enddate.getValue(), null);
        } else {

            this.first.enable();
            this.startdate.enable();
            this.enddate.enable();
        }
    }
});

Ext.reg('eBook.NewFile.PreviousStartEnd', eBook.NewFile.PreviousStartEnd);

eBook.NewFile.SelectPrevious = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , items: [
                    { xtype: 'checkbox', ref: 'checker', boxLabel: 'Has previous eBook file', checked: true
                        , labelWidth: 200
                        , listeners: {
                            'check': {
                                fn: function (bx, chkd) {
                                    if (chkd) {
                                        this.previousfile.enable();
                                        this.accounts.show();
                                        this.mappings.show();
                                        this.worksheets.show();
                                        this.previousimport.show();
                                        this.accounts.setValue(true);
                                        this.mappings.setValue(true);
                                        this.worksheets.setValue(true);
                                    } else {
                                        this.previousfile.disable();
                                        this.accounts.setValue(false);
                                        this.mappings.setValue(false);
                                        this.worksheets.setValue(false);
                                        this.previousimport.setValue(false);
                                        this.accounts.hide();
                                        this.mappings.hide();
                                        this.worksheets.hide();
                                        this.previousimport.hide();
                                    }
                                }
                                , scope: this
                            }
                        }
                    }, {
                        xtype: 'combo'
                        , ref: 'previousfile'
                        , fieldLabel: eBook.Create.Empty.PreviousFileFieldSet_EbookFile
                        , name: 'PreviousFile'
                        , triggerAction: 'all'
                        , typeAhead: false
                        , selectOnFocus: true
                        , autoSelect: true
                        , allowBlank: true
                        , forceSelection: true
                        , valueField: 'Id'
                        , displayField: 'Display'
                        , editable: false
                        , nullable: true
                        , mode: 'local'
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetFileInfos'
                            , serviceUrl: eBook.Service.file
                            , autoLoad: true
                            , autoDestroy: true
                            , criteriaParameter: 'cfdc'
                           , baseParams: {
                               MarkedForDeletion: false
                                , Deleted: false
                               , ClientId: eBook.Interface.currentClient.get('Id')
                                , Closed: null
                           }
                            , fields: eBook.data.RecordTypes.FileBase
                        })
                        , width: 300
                        , listWidth: 400
                    }
                , { xtype: 'checkbox', ref: 'accounts'
                    , boxLabel: 'Import accountshema &amp; descriptions'
                    , checked: true, disabled: true
                }
                , { xtype: 'checkbox', ref: 'mappings'
                    , boxLabel: 'Import mappings'
                    , checked: true, disabled: true
                }
                , { xtype: 'checkbox', ref: 'worksheets'
                    , boxLabel: 'Import worksheets (history)'
                    , checked: true, disabled: true
                }
                , { xtype: 'checkbox', ref: 'previousimport'
                            , boxLabel: 'Import end-state previous period from eBook previous file'
                            , checked: false, labelWidth: 200
                }]
        });
        eBook.NewFile.SelectPrevious.superclass.initComponent.apply(this, arguments);
    }
    , checkLayout: function () {
        var step5 = this.refOwner.step5;
        if (this.refOwner.rendered && step5 && step5.rendered && this.previousfile && !step5.inactive) {
            var val = step5.startdate.getValue();
            if (val && this.previousfile.store.baseParams.StartDate != val) {
                this.previousfile.store.baseParams.StartDate = val;
                this.previousfile.store.load();
            }
        } else if (step5.inactive) { // EXACT so get date from fin year
            var val = this.refOwner.step4.selectedExactFinYearStartdate;
            if (val && this.previousfile.store.baseParams.StartDate != val) {
                
                this.previousfile.store.baseParams.StartDate = val;
                this.previousfile.store.load();
            }
        }
        eBook.NewFile.SelectPrevious.superclass.checkLayout.call(this);
    }
    , onEndDateChanged: function (lfd, n, o) {
        if (Ext.isDate(n)) {
            yr = n.getFullYear();
            if (n.getMonth() == 11 && n.getDate() == 31) yr++; // 31/12 ==> remember: javascript date, months are 0-11
            this.assessmentyear.setValue(yr);
        } else {
            this.assessmentyear.setValue('');
        }
    }
    , getSettings: function () {
        if (this.inactive) return {};
        if (!this.checker.checked) return {};
        return this.mySettings;
    }
    , validate: function () {
        var sel = this.previousfile.getValue();
        var chkd = this.checker.checked;
        if (chkd && (!sel || Ext.isEmpty(sel))) {
            this.previousfile.markInvalid();
            this.ownerCt.ownerCt.updateState(this.stepId, false);
            return false;
        }
        this.ownerCt.ownerCt.updateState(this.stepId, true);

        this.mySettings = {
            importAccounts: this.accounts.checked
            , importMappings: this.mappings.checked
            , importWorksheets: this.worksheets.checked
            , importPreviousState: this.previousimport.checked
        };
        return true;
    }
    , onNext: function () {
        if (this.validate()) {
            var chkd = this.checker.checked;
            if (chkd) {
                this.refOwner.data.previousFile = { id: this.previousfile.getValue() };
            } else {
                this.refOwner.data.previousFile = null;
            }
            eBook.NewFile.SelectPrevious.superclass.onNext.call(this);
        }

    }
});

Ext.reg('eBook.NewFile.SelectPrevious', eBook.NewFile.SelectPrevious);

eBook.NewFile.FileServices = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , items: [
                        { xtype: 'checkbox', ref: 'yearend', id: 'FileServices-yearend', boxLabel: eBook.Language.YEAREND, checked: true
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'biztaxAuto', id: 'FileServices-biztax', boxLabel: eBook.Language.BIZTAX_AUTO, checked: true
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'biztaxManual', id: 'FileServices-biztax-manual', boxLabel: eBook.Language.BIZTAX_MANUAL, checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                    ]
        });
        eBook.NewFile.FileServices.superclass.initComponent.apply(this, arguments);
    }
    , checkLayout: function() {

        eBook.NewFile.FileServices.superclass.checkLayout.call(this);
    }
    , checkChanged: function(chk, checked) {
       if(this.biztaxManual.checked) {
            this.yearend.setValue(false);
            this.biztaxAuto.setValue(false);
            this.yearend.disable();
            this.biztaxAuto.disable();
       } else {
            this.yearend.enable();
            this.biztaxAuto.enable();
       }
    
       if(!this.yearend.checked && !this.biztaxAuto.checked && this.biztaxManual.checked) {
            this.refOwner.step2.importType='none';
            this.refOwner.step2.inactive=true;
            this.ownerCt.ownerCt.updateState(2, 'inactive');
            this.refOwner.step2.inactive=true;
            this.ownerCt.ownerCt.updateState(6, 'inactive');
            this.refOwner.step2.inactive=true;
            this.ownerCt.ownerCt.updateState(7, 'inactive');
       } else {
            this.refOwner.step2.importType='none';
            this.refOwner.step2.inactive=false;
            this.ownerCt.ownerCt.updateState(2, 0);
       }
    }
    , validate: function() {
        if (!this.yearend.checked && !this.biztaxManual.checked && !this.biztaxAuto.checked) {
            alert("At least one service is required");
            this.ownerCt.ownerCt.updateState(stepId, false);
            return false;
        }
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        return true;
    }
    , onNext: function() {
        if (this.validate()) {
            
            eBook.NewFile.FileServices.superclass.onNext.call(this);
        }

    }
});

Ext.reg('eBook.NewFile.FileServices', eBook.NewFile.FileServices);


eBook.NewFile.FileSettings = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , items: [{
                             xtype:'languagebox'
                             ,ref:'language'
                             , name: 'language'
                             ,allowBlank:false
                             , fieldLabel: eBook.Create.Empty.FileInfoFieldSet_Language
                             ,width:200
                             ,listWidth:200
                        }
                        , { xtype: 'checkbox', ref: 'first', id: 'FileSettings-first', boxLabel: 'First bookyear', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallPrevBef', id: 'FileSettings-smallPrevBef', boxLabel: 'Before previous period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallPrev', id: 'FileSettings-smallPrev', boxLabel: 'Previous period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'smallCur', id: 'FileSettings-smallCur', boxLabel: 'Current period small company', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'oneof', id: 'FileSettings-oneof', boxLabel: 'Current period is one of the first 3 periods', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                        , { xtype: 'checkbox', ref: 'taxshelter', id: 'FileSettings-taxshelter', boxLabel: 'Tax shelter?', checked: false
                            , labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkChanged
                                    , scope: this
                                }
                            }
                        }
                    ]
        });
        eBook.NewFile.FileSettings.superclass.initComponent.apply(this, arguments);
    }
    , beforeCheckLayout: function() {
        var step7 = this.refOwner.step7;
        this.first.setValue(step7.first.checked);
        this.first.disable();
        this.checkChanged(this.first, step7.first.checked);
    }
    , checkChanged: function(chk, checked) {
        if (chk.id == 'FileSettings-first') {
            if (checked) {
                this.smallPrevBef.setValue(false);
                this.smallPrev.setValue(false);
                this.oneof.setValue(true);
                this.smallPrevBef.disable();
                this.smallPrev.disable();
                this.oneof.disable();

            } else {
                this.smallPrevBef.enable();
                this.smallPrev.enable();
                this.oneof.enable();
            }
        }
//        else if (chk.id.indexOf('smallPrev') > -1) {
//            var mcheck = this.smallPrev.getValue() || this.smallPrevBef.getValue();
//            if (mcheck) {
//                this.first.setValue(false);
//                this.first.disable();
//            } else {
//                this.first.enable();
//            }
//        }
    }
    , validate: function() {
        if(!this.language.isValid()) {
            this.ownerCt.ownerCt.updateState(this.stepId, false);
            return false;
        }
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        return true;
    }
    , onNext: function() {
        if (this.validate()) {

            eBook.NewFile.FileSettings.superclass.onNext.call(this);
        }

    }
});

Ext.reg('eBook.NewFile.FileSettings', eBook.NewFile.FileSettings);



eBook.NewFile.ProAccSettings = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function() {
        Ext.apply(this, {
            autoNext: false
            , layout: 'form'
            , items: [
                        { xtype: 'checkbox', ref: 'accounts', id: 'ProAccSettings-accounts'
                            , boxLabel: 'Import new accounts &amp; descriptions'
                            , checked: true, labelWidth: 200,disabled:true
                        }
                        ,{ xtype: 'checkbox', ref: 'current', id: 'ProAccSettings-current'
                            , boxLabel: 'Import state current period'
                            , checked: true, labelWidth: 200,disabled:true
                        }
                         ,{ xtype: 'checkbox', ref: 'previous', id: 'ProAccSettings-current'
                            , boxLabel: 'Import end-state previous period'
                            , checked: true, labelWidth: 200
                            , listeners: {
                                'check': {
                                    fn: this.checkPreviousPeriodChanged
                                    , scope: this
                                }
                            }
                        }
                        ,{ xtype: 'radiogroup', ref:'previousImportGroup',columns: 1,cls:'eBook-newfile-proaccsettings-previmporttype'
                            ,items: [
                                {boxLabel: 'From ProAcc',  id:'nf-pit-proacc', name: 'previousImportType', inputValue: 'proacc', checked: true},
                                {boxLabel: 'From previous eBook file',id:'nf-pit-ebook', name: 'previousImportType', inputValue: 'ebook'}
                            ]
                        }
                        ,{ xtype: 'checkbox', ref: 'clientsuppliers', id: 'ProAccSettings-clientsuppliers'
                            , boxLabel: 'Update clients &amp; suppliers'
                            , checked: true, labelWidth: 200
                        }
                    ]
        });
        eBook.NewFile.ProAccSettings.superclass.initComponent.apply(this, arguments);
    }
    , checkPreviousPeriodChanged: function(chk, checked) {
        var rec = this.hasPreviousFile();
        if(checked && rec) {
            this.previousImportGroup.enable();
        } else {
            this.previousImportGroup.disable();
        }
    }
     , hasPreviousFile: function() {
        var step6 = this.refOwner.step6;
        if (this.refOwner.rendered && step6 && step6.rendered) {
            if (step6.checker.checked) {
                var val = step6.previousfile.getValue();
                var rec = step6.previousfile.store.getById(val);
                return rec;
            }
            return false;
        }
        return false;
    }
    ,beforeCheckLayout:function() {
        var rec = this.hasPreviousFile();
        if(!rec) {
            this.previousImportGroup.onSetValue('nf-pit-proacc', true);
            this.previousImportGroup.disable();
            if(this.refOwner.step7.first.checked) {
                this.previous.setValue(false);
                this.previous.disable();
            } else {
                this.previous.enable();
            }
        } else {
            if(this.refOwner.step6.previousimport.checked) {
                this.previous.setValue(true);
                this.checkPreviousPeriodChanged();
                this.previousImportGroup.onSetValue('nf-pit-ebook', true);
                this.previous.disable();
            } else {
                this.previousImportGroup.onSetValue('nf-pit-proacc', true);
                this.previousImportGroup.enable();
                this.previous.enable();
            }
        }
    }
    , getSettings:function() {
        if(this.inactive) return {};
        return this.mySettings;
    }
    , validate: function() {
        if(!this.previous.disabled && this.previous.checked && this.previousImportGroup.getValue().inputValue=='ebook') {
            this.refOwner.step6.previousimport.setValue(true);
            this.refOwner.step6.validate();
        } else if(this.previous.checked && this.previousImportGroup.getValue().inputValue=='proacc') {
            this.refOwner.step6.previousimport.setValue(false);
            this.refOwner.step6.validate();
        }
        this.mySettings = { 
            database: eBook.Interface.currentClient.get('ProAccDatabase')
            ,server: eBook.Interface.currentClient.get('ProAccServer')
            ,importAccounts:this.accounts.checked
            ,importCurrentState:this.current.checked
            ,importPreviousState:this.previous.checked && this.previousImportGroup.getValue().inputValue=='proacc'
            , updateBusinessRelations:this.clientsuppliers.checked
        };
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        return true;
    }
    , onNext: function() {
        if (this.validate()) {
            
            eBook.NewFile.ProAccSettings.superclass.onNext.call(this);
        }

    }
});

Ext.reg('eBook.NewFile.ProAccSettings', eBook.NewFile.ProAccSettings);

eBook.NewFile.UploadExcel = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        Ext.apply(this, {
            layout: 'border'
             , items: [{ xtype: 'form'
                        , fileUpload: true
                        , bodyStyle: 'padding:10px;'
                        , items: [{
                            xtype: 'fileuploadfield'
                                    , width: 400
                                    , fieldLabel: eBook.Excel.UploadWindow_SelectFile
                                    , fileTypeRegEx: /(\.xls(x{0,1}))$/i
                                    , ref: 'file'
                        }, {
                            xtype: 'hidden'
                                        , name: 'fileType'
                                        , value: 'excel'
                        }, {
                            xtype: 'button',
                            text: eBook.Excel.UploadWindow_Upload,
                            ref: '../saveButton',
                            iconCls: 'eBook-icon-uploadexcel-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onUploadClick,
                            scope: this,
                            style: 'margin-left:300px;'
                        }]
                        , ref: 'uploadForm'
                        , region: 'north'
             }
                    , { xtype: 'eBook.Excel.Configurator', region: 'center', ref: 'configurator', disabled: true }
            ]
        });
        eBook.NewFile.UploadExcel.superclass.initComponent.apply(this, arguments);
    }
    , beforeCheckLayout: function () {
        var fin = this.uploadForm.el.child('input.x-form-file-text');
        fin.setStyle('width', '250px');
        this.configurator.items.items[0].mappingPanel.sheet.setWidth(100);
        this.configurator.items.items[0].mappingPanel.culture.setWidth(100);
        this.configurator.items.items[0].enable();
        this.configurator.items.items[1].enable();
        if (this.refOwner.step2.importType == "exact") {
            this.configurator.items.items[2].enable();
        }
        //this.configurator.items.items[0].mappingPanel.items.items[0].setValue('test');  
    }
    , onUploadClick: function (e) {
        this.configurator.disable();
        var f = this.uploadForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Excel.UploadWindow_Invalid);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Excel.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function (fp, o) {
        //process o.result.file
        this.uploaded = true;
        this.newFileName = o.result.file;
        this.originalFileName = o.result.originalFile;
        this.getEl().mask(String.format(eBook.Excel.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.excel + 'ExcelGetBasicInfo'
                , method: 'POST'
                , params: Ext.encode({ cedc: { FileName: o.result.file} })
                , callback: this.handleFileProcessing
                , scope: this
        });
    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, this.title);
    }
    , handleFileProcessing: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var dc = Ext.decode(resp.responseText).ExcelGetBasicInfoResult;
            dc.originalFileName = this.originalFileName;
            this.configurator.setTitle(this.originalFileName);
            this.configurator.loadFile(dc.fn, dc.sns);
            if (this.refOwner.step2.importType == "exact") {
                // Tab rekeningschema
                var mappingRekSchema = this.configurator.items.items[0].mappingPanel;
                mappingRekSchema.sheet.setValue(mappingRekSchema.sheet.store.getAt(0).data.field1);
                mappingRekSchema.onSheetSelect();
                mappingRekSchema.accountnr.setValue('A');
                mappingRekSchema.accountdescription.setValue('B');

                // Tab balans
                var mappingBalans = this.configurator.items.items[1].mappingPanel;
                mappingBalans.sheet.setValue(mappingBalans.sheet.store.getAt(1).data.field1);
                mappingBalans.onSheetSelect();
                mappingBalans.accountnr.setValue('A');
                mappingBalans.accountdescription.setValue('B');
                mappingBalans.saldo.setValue('C');

                // Tab balans vorig
                var mappingBalansVorig = this.configurator.items.items[2].mappingPanel;
                mappingBalansVorig.sheet.setValue(mappingBalansVorig.sheet.store.getAt(2).data.field1);
                mappingBalansVorig.onSheetSelect();
                mappingBalansVorig.accountnr.setValue('A');
                mappingBalansVorig.accountdescription.setValue('B');
                mappingBalansVorig.saldo.setValue('C');

            }
            this.configurator.enable();

            //this.configurator.items.items[0].mappingPanel.items.items[0].expand();
            //this.configurator.items.items[0].mappingPanel.items.items[0].select(0);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , setExcelInfo: function (dc) {
        this.dataContract = dc;
        this.filename.setText(dc.originalFileName);
        this.filesheets.setText(dc.sns.join(","));
    }
    , getSettings: function () {
        if (this.inactive) return {};
        var dcs = this.configurator.getDataContracts();
        return { importAccounts: dcs.accountsCfg && dcs.accountsCfg.active
                , importCurrentState: dcs.currentCfg && dcs.currentCfg.active
                , importPreviousState: dcs.previousCfg && dcs.previousCfg.active
                , originalFileName: this.originalFileName
                , newFileName: this.newFileName
            , datacontracts: dcs
        };

    }
    , validate: function () {
       // return true; // TO DELETE!
        if (!this.uploaded) {
            this.ownerCt.ownerCt.updateState(this.stepId, false);
            return false;
        }
        var ok = !this.configurator.items.items[0].disabled && !this.configurator.items.items[1].disabled;
        ok = ok && this.configurator.getDataContracts().length > 1;
        ok = ok && this.configurator.items.items[0].checkValid();
        ok = ok && this.configurator.items.items[1].checkValid();
        ok = ok && this.configurator.items.items[2].checkValid();
        this.ownerCt.ownerCt.updateState(this.stepId, ok);
        return ok;
    }
});

Ext.reg('eBook.NewFile.UploadExcel', eBook.NewFile.UploadExcel);


eBook.NewFile.UploadTxt = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        this.importFile = false;
        Ext.apply(this, {
            layout: 'border'
             , items: [

                        { xtype: 'form'
                        , fileUpload: true
                        , bodyStyle: 'padding:10px;'
                        , items: [
                            { xtype: 'checkbox',
                                boxLabel: 'Import data',
                                checked: false,
                                listeners: {
                                    'check': {
                                        fn: this.firstChanged
                                        , scope: this
                                    }
                                }
                            },
                            {
                                xtype: 'fileuploadfield'
                                        , width: 400
                                        , fieldLabel: eBook.Exact.UploadWindow_SelectFile
                                        , fileTypeRegEx: /(\.txt())$/i
                                        , ref: '../file'
                                        , disabled: true
                            }, {
                                xtype: 'hidden'
                                            , name: 'fileType'
                                            , value: 'txt'
                            }, {
                                xtype: 'button',
                                text: eBook.Excel.UploadWindow_Upload,
                                ref: '../saveButton',
                                iconCls: 'eBook-uploadbiz-ico-24 ',
                                scale: 'medium',
                                iconAlign: 'top',
                                handler: this.onUploadClick,
                                scope: this,
                                style: 'margin-left:300px;',
                                disabled: true
                            }]
                        , ref: 'uploadForm'
                        , region: 'center'
                        }
            //, { xtype: 'eBook.Excel.Configurator', region: 'center', ref: 'configurator', disabled: true }
            ]
        });
        eBook.NewFile.UploadTxt.superclass.initComponent.apply(this, arguments);
    }
    , beforeCheckLayout: function () {
        var fin = this.uploadForm.el.child('input.x-form-file-text');
        fin.setStyle('width', '250px');
    }
    , firstChanged: function (chk, checked) {
        if (checked) {
            this.file.enable();
            this.saveButton.enable();
            this.importFile = true;
        } else {
            this.file.disable();
            this.saveButton.disable();
            this.importFile = false;
        }
    }
    , onUploadClick: function (e) {
        var f = this.uploadForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Excel.UploadWindow_Invalid);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Excel.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function (fp, o) {
        this.uploaded = true;
        this.newFileName = o.result.file;
        this.originalFileName = o.result.originalFile;
        Ext.Msg.show({
            title: '',
            msg: 'Your file was successfully uploaded. Press OK to continue.',
            buttons: Ext.Msg.OK,
            fn: function (btnid) { if (btnid == 'ok') { this.ownerCt.moveNext() } },
            scope: this,
            icon: Ext.MessageBox.QUESTION
        });
    }
    , failedUpload: function (fp, o) {
        eBook.Interface.showError(o.message, this.title);
    }
    , setExcelInfo: function (dc) {
        this.dataContract = dc;
        this.filename.setText(dc.originalFileName);
        this.filesheets.setText(dc.sns.join(","));
    }
    , getSettings: function () {
        if (this.inactive) return {};
        return { originalFileName: this.originalFileName
                , newFileName: this.newFileName
                , importFile: this.importFile
        };

    }
    , validate: function () {
        // return true; // TO DELETE!
        if (this.importFile) {
            if (this.uploaded) {
                this.ownerCt.ownerCt.updateState(this.stepId, true);
                return true;
            } else {
                this.ownerCt.ownerCt.updateState(this.stepId, false);
                return false;
            }
        } else {
            this.ownerCt.ownerCt.updateState(this.stepId, true);
            return true;
        }

        


    }
});

Ext.reg('eBook.NewFile.UploadTxt', eBook.NewFile.UploadTxt);


eBook.NewFile.CreationOverview = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        this.applyInit();
        this.tpl = new Ext.XTemplate('<div class="eBook-newfile-overview">'
                        , '<h2>Create File {assessmentYear}: {startDate:date("d/m/Y")} &gt; {endDate:date("d/m/Y")}</h2>'
                        , '<div class="eBook-newfile-overview-info">'
                                , 'Client default language: {language}'
                                , '<tpl if="!settings.first"><br/>Previous  period: <i>\[{previousAssessmentYear}\] {previousStartDate:date("d/m/Y")} - {previousEndDate:date("d/m/Y")}</i></tpl>'
                                , '<tpl if="settings.first"><br/><b>First bookyear</b>, no previous period.</tpl>'
                        , '</div>'
                        , '<div class="eBook-newfile-overview-tab">'
                            , '<div class="eBook-newfile-overview-tab-title">Import details</div>'
                            , '<div class="eBook-newfile-overview-tab-body">'
                                , '<tpl if="previouseBook">'
                                    , '<u>Previous eBook File</u>'
                                    , '<ul>'
                                        , '<tpl if="importConfig.ebook.importAccounts"><li>Import accountschema</li></tpl>'
                                        , '<tpl if="importConfig.ebook.importMappings"><li>Import mappings</li></tpl>'
                                        , '<tpl if="importConfig.ebook.importWorksheets"><li>Import worksheets (history)</li></tpl>'
                                        , '<tpl if="importConfig.ebook.importPreviousState"><li>Import state previous period</li></tpl>'
                                    , '</ul>'
                                    , '<br/>'
                                , '</tpl>'
                                , '<tpl if="importConfig.type==\'none\'">'
                                    , '<u><b>NO IMPORT</b></u>'
                                    , 'No data needs to be imported when performing manual BizTax declaration only'
                                 , '</tpl>'
                                , '<tpl if="importConfig.type==\'proacc\'">'
                                    , '<tpl for="importConfig.proacc"><u>ProAcc database - {database}</u></tpl>'
                                    , '<ul>'
                                        , '<tpl if="importConfig.proacc.importAccounts"><li>Import new accounts & descriptions</li></tpl>'
                                        , '<tpl if="importConfig.proacc.importPreviousState"><li>Import state previous period</li></tpl>'
                                        , '<tpl if="importConfig.proacc.importCurrentState"><li>Import state current period</li></tpl>'
                                        , '<tpl if="importConfig.proacc.updateBusinessRelations"><li>Update clients &amp; suppliers</li></tpl>'
                                    , '</ul>'
                               , '</tpl>'
                               , '<tpl if="importConfig.type==\'excel\'">'
                                    , '<tpl for="importConfig.excel"><u><u>Excel file {originalFileName}</u>'
                                    , '<ul>'
                                        , '<tpl if="importAccounts"><li>Import accounts & descriptions <br/>(<i><tpl for="datacontracts.accountsCfg">{txt}</tpl></i>) </li></tpl>'
                                        , '<tpl if="importPreviousState"><li>Import state previous period <br/>(<i><tpl for="datacontracts.previousCfg">{txt}</tpl></i>)</li></tpl>'
                                        , '<tpl if="importCurrentState"><li>Import state current period <br/>(<i><tpl for="datacontracts.currentCfg">{txt}</tpl></i>)</li></tpl>'
                                    , '</ul>'
                                    , '</tpl>'
                               , '</tpl>'
                               , '<tpl if="importConfig.type==\'exact\'">'
                                    , '<tpl if="importConfig.exact.importFile">'
                                        , '<tpl for="importConfig.exact"><u>Text file: {originalFileName}</u>'
                                        , '</tpl>'
                                    , '</tpl>'
                                    , '<tpl if="importConfig.exact.importFile==\'false\'">'
                                        , '<tpl for="importConfig.exact"><u>None</u>'
                                        , '</tpl>'
                                    , '</tpl>'
                               , '</tpl>'
                            , '</div>'
                        , '</div>'
                        , '<div class="eBook-newfile-overview-tab">'
                            , '<div class="eBook-newfile-overview-tab-title">Setting/Meta details</div>'
                            , '<div class="eBook-newfile-overview-tab-body">'
                                , '<u>Services</u>'
                                , '<ul>'
                                    , '<tpl if="services.yearend"><li>Yearend closing</li></tpl>'
                                    , '<tpl if="services.biztaxAuto"><li>Automated BizTax declaraion (client ledger available)</li></tpl>'
                                    , '<tpl if="services.biztaxManual"><li>MANUAL BizTax declaraion (client ledger NOT available)</li></tpl>'
                                , '</ul>'
                                , '<br/>'
                                , '<u>Meta</u>'
                                , '<ul>'
                                    , '<tpl if="settings.first"><li><b>First bookyear</b></li></tpl>'
                                    , '<tpl if="settings.oneof"><li>Current period is one of the first 3 periods</li></tpl>'
                                    , '<tpl if="settings.smallPrevBef"><li>Before previous period small company</li></tpl>'
                                    , '<tpl if="settings.smallPrev"><li>Previous period small company</li></tpl>'
                                    , '<tpl if="settings.smallCur"><li>Current period small company</li></tpl>'
                                    , '<tpl if="settings.taxshelter"><li>Tax Shelter</li></tpl>'
                                , '</ul>'
                            , '</div>'
                        , '</div>'
                    , '</div>');
        Ext.apply(this, {
            items: [{ xtype: 'panel', html: 'updating...', ref: 'overview'}]
            , disableMove: true
            , bbar: [
                    { xtype: 'button'
                        , ref: 'prev'
                        , hidden: this.stepId < 2
                        , text: 'Change settings'
                        , handler: this.onPrevious
                        , scope: this
                        , iconCls: 'eBook-back-24'
                        , scale: 'medium'
                        , iconAlign: 'top'
                    }
                    , '->'
                    , { xtype: 'button'
                        , text: eBook.Create.Empty.MainPanel_Create
                        , ref: 'nxt'
                        , handler: this.onNext
                        , scope: this
                        , iconCls: 'eBook-createFile-createButton'
                        , scale: 'medium'
                        , iconAlign: 'top'
                    }]
        });
        eBook.NewFile.CreationOverview.superclass.initComponent.apply(this, arguments);
    }
    , beforeCheckLayout: function () {
//        
        this.dta = {
            startDate: this.refOwner.step2.importType == "exact" ? this.refOwner.step4.selectedExactFinYearStartdate : this.refOwner.step5.startdate.getValue()
            , endDate: this.refOwner.step2.importType == "exact" ? this.refOwner.step4.selectedExactFinYearEnddate : this.refOwner.step5.enddate.getValue()
            , assessmentYear: this.refOwner.step2.importType == "exact" ? this.refOwner.step4.selectedExactFinYearassessmentyear : this.refOwner.step5.assessmentyear.getValue()
            , previouseBook: this.refOwner.step6.checker.checked
            , previousFileId: this.refOwner.step6.previousfile.getValue()
            , previousStartDate: this.refOwner.step7.startdate.getValue()
            , previousEndDate: this.refOwner.step7.enddate.getValue()
            , previousAssessmentYear: this.refOwner.step7.assessmentyear.getValue()
            , culture: this.refOwner.step11.language.getValue()
            , language: this.refOwner.step11.language.getSelectedDisplay()
            , importConfig: {
                type: this.refOwner.step2.importType
                , proacc: this.refOwner.step8.getSettings()
                , excel: this.refOwner.step9.getSettings()
                , ebook: this.refOwner.step6.getSettings()
                , exact: this.refOwner.step10.getSettings()
            }
            , services: {
                yearend: this.refOwner.step1.yearend.checked
                , biztaxAuto: this.refOwner.step1.biztaxAuto.checked
                , biztaxManual: this.refOwner.step1.biztaxManual.checked
            }
            , settings: {
                first: this.refOwner.step7.first.checked
                , smallPrevBef: this.refOwner.step11.smallPrevBef.checked
                , smallPrev: this.refOwner.step11.smallPrev.checked
                , smallCur: this.refOwner.step11.smallCur.checked
                , oneof: this.refOwner.step11.oneof.checked
                , taxshelter: this.refOwner.step11.taxshelter.checked
            }
        };
        this.ownerCt.creationData = this.dta;
        this.overview.el.update(this.tpl.apply(this.dta));
    }
    , validate: function () {
        this.ownerCt.ownerCt.updateState(this.stepId, true);
        return true;
    }
});

//Ext.override(eBook.NewFile.CreationOverview, eBook.NewFile.Step);

Ext.reg('eBook.NewFile.CreationOverview', eBook.NewFile.CreationOverview);



eBook.NewFile.CreateFile = Ext.extend(eBook.NewFile.StepPanel, {
    initComponent: function () {
        this.disableMove = true;
        this.applyInit();
        this.tpl = new Ext.XTemplate('<div class="eBook-newfile-overview">'
                        , '<h2>Create File {assessmentYear}: {startDate:date("d/m/Y")} &gt; {endDate:date("d/m/Y")}</h2>'
                        , '<tpl if="!settings.first"><div class="eBook-newfile-overview-info">Previous  period: <i>\[{previousAssessmentYear}\] {previousStartDate:date("d/m/Y")} - {previousEndDate:date("d/m/Y")}</i></div></tpl>'
                        , '<tpl if="settings.first"><div class="eBook-newfile-overview-info"><b>First bookyear</b>, no previous period.</div></tpl>'
                        , '<div class="eBook-newfile-actionsCT"><div class="eBook-newfile-actions">'
                            , '<ul>'
                            , '<tpl for="actions">'
                                , '<li class="{stateCls}" id="eBook-newfile-action-{id}">{description}<div class="eBook-newfile-action-errMsg" style="display:none"></div></li>'
                            , '</tpl>'
                            , '</ul>'
                        , '</div></div>'
                    , '</div>');
        Ext.apply(this, {
            items: [{ xtype: 'panel', html: 'updating...', ref: 'overview'}]
            , disableMove: true
        });
        eBook.NewFile.CreateFile.superclass.initComponent.apply(this, arguments);
    }
     , beforeCheckLayout: function () {
         this.settingsLog = this.refOwner.creationData;

         this.dta = {
             assessmentYear: this.settingsLog.assessmentYear
            , startDate: this.settingsLog.startDate
            , endDate: this.settingsLog.endDate
            , settings: this.settingsLog.settings
            , previousAssessmentYear: this.settingsLog.previousAssessmentYear
            , previousStartDate: this.settingsLog.previousStartDate
            , previousEndDate: this.settingsLog.previousEndDate
            , actions: []
         };

         var services = [];
         if (this.settingsLog.services.yearend) services.push('c3b8c8db-3822-4263-9098-541fae897d02');
         if (this.settingsLog.services.biztaxAuto) services.push('bd7c2eae-8500-4103-8984-0131e73d07fa');
         if (this.settingsLog.services.biztaxManual) services.push('60ca9df9-c549-4a61-a2ad-3fdb1705cf55');

         actions = [];

         idx = 0;
         actions.push({
             id: idx
            , stateCls: 'eBook-newfile-ready'
            , state: 'ready'
            , description: 'Create new file'
            , url: eBook.Service.file
            , action: 'CreateFile'
            , params: { ccfdc: {
                cid: eBook.CurrentClient
                            , as: services
                            , c: this.settingsLog.culture
                            , s: this.settingsLog.startDate
                            , e: this.settingsLog.endDate
                            , ps: !this.settingsLog.settings.first ? this.settingsLog.previousStartDate : null
                            , pe: !this.settingsLog.settings.first ? this.settingsLog.previousEndDate : null
                            , pfid: this.settingsLog.previouseBook ? this.settingsLog.previousFileId : null
                            , fs: {
                                fid: eBook.EmptyGuid
                                , fby: this.settingsLog.settings.first
                                , oft: this.settingsLog.settings.oneof
                                , bps: this.settingsLog.settings.smallPrevBef
                                , ps: this.settingsLog.settings.smallPrev
                                , cs: this.settingsLog.settings.smallCur
                                , ts: this.settingsLog.settings.taxshelter
                            }
                            , Person: eBook.User.getActivePersonDataContract()
                            , it: this.refOwner.step2.importType
            }
            }
            , callback: this.onFileCreated
            , scope: this
         });
         idx++;

         if (this.settingsLog.previouseBook) {
             actions.push({
                 id: idx
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , description: 'Import accounts previous eBook ' + (this.settingsLog.importConfig.ebook.importPreviousState ? ' (including previous state)' : '')
                , url: eBook.Service.file
                , action: 'ImportAccountseBook'
                , params: { iaedc: { fid: '', ips: this.settingsLog.importConfig.ebook.importPreviousState} }
                , paramKey: 'iaedc'
                , callback: this.onActionReady
                , scope: this
             });
             idx++;
             actions.push({
                 id: idx
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , description: 'Import mappings previous eBook'
                , url: eBook.Service.file
                , action: 'ImportMappingseBook'
                , params: { cfdc: { FileId: '', Culture: eBook.Interface.Culture} }
                , paramKey: 'cfdc'
                , parmFileKey: 'FileId'
                , callback: this.onActionReady
                , scope: this
             });
             idx++;
         }

         if (this.settingsLog.importConfig.type == 'proacc') {
             actions.push({
                 id: idx
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , description: 'Import (new) accounts from proAcc inlcuding current state' + (this.settingsLog.importConfig.proacc.importPreviousState ? ' and previous state' : '')
                , url: eBook.Service.file
                , action: 'ImportAccountsProAcc'
                , params: { iaedc: { fid: '', ips: this.settingsLog.importConfig.proacc.importPreviousState} }
                , paramKey: 'iaedc'
                , callback: this.onActionReady
                , scope: this
             });
             idx++;


             if (this.settingsLog.importConfig.proacc.updateBusinessRelations) {
                 actions.push({
                     id: idx
                     , stateCls: 'eBook-newfile-ready'
                     , state: 'ready'
                     , description: 'Update clients &amp; suppliers from proAcc'
                     , url: eBook.Service.businessrelation
                     , action: 'ImportBusinessRelationsProAcc'
                     , params: { cidc: { Id: eBook.Interface.currentClient.get('Id'), Culture: '', Person: eBook.User.getActivePersonDataContract()} }
                     , paramKey: 'cidc'
                     , callback: this.onActionReady
                     , scope: this
                 });
                 idx++;
             }


         }

         if (this.settingsLog.importConfig.type == 'excel') {
             /* TO DELETE start */
             var dcs = this.settingsLog.importConfig.excel.datacontracts;
             var par = dcs.accountsCfg;
             par.FileId = '';
             actions.push({
                 id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import (new) accounts from Excel file'
                 , url: eBook.Service.file
                 , action: 'ImportAccountsExcel'
                 , params: { cesmdc: par }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
             });
             idx++;

             if (this.settingsLog.importConfig.excel.importCurrentState) {
                 par = dcs.currentCfg;
                 par.FileId = '';
                 actions.push({
                     id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import current state from Excel file'
                 , url: eBook.Service.file
                 , action: 'ImportCurrentStateExcel'
                     //, action: 'ImportCurrentStateTxt'
                 , params: { cesmdc: par }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
                 });
                 idx++;

             }

             if (this.settingsLog.importConfig.excel.importPreviousState && !this.settingsLog.settings.first) {
                 par = dcs.previousCfg;
                 par.FileId = '';
                 actions.push({
                     id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import previous state from Excel file'
                 , url: eBook.Service.file
                 , action: 'ImportPreviousStateExcel'
                 , params: { cesmdc: par }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
                 });
                 idx++;

             }
             /* end */
         }

         if (this.settingsLog.importConfig.type == 'exact' && this.refOwner.step10.importFile && this.refOwner.step10.uploaded) {
             var dcs = this.settingsLog.importConfig.exact;

             actions.push({
                 id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import (new) accounts from Excel file'
                 , url: eBook.Service.file
                 , action: 'ImportAccountsTxt'
                 , params: { cesmdc: { FileName: dcs.newFileName, Culture: eBook.Interface.Culture} }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
             });
             idx++;

             actions.push({
                 id: idx
                 , stateCls: 'eBook-newfile-ready'
                 , state: 'ready'
                 , description: 'Import current state from Text file'
                 , url: eBook.Service.file
                 //, action: 'ImportCurrentStateExcel'
                 , action: 'ImportCurrentStateTxt'
                 , params: { cesmdc: { FileName: dcs.newFileName} }
                 , paramKey: 'cesmdc'
                 , parmFileKey: 'FileId'
                 , callback: this.onActionReady
                 , scope: this
             });
             idx++;
         }

         actions.push({
             id: idx
            , stateCls: 'eBook-newfile-ready'
            , state: 'ready'
            , description: 'Preparing new file for fast usage...'
            , url: eBook.Service.file
            , action: 'SetFileInCache'
            , params: { cfdc: { FileId: '', Culture: eBook.Interface.Culture} }
            , paramKey: 'cfdc'
            , parmFileKey: 'FileId'
            , callback: this.onActionReady
            , scope: this
         });
         idx++;

         if (this.settingsLog.previouseBook) {

             actions.push({
                 id: idx
                , stateCls: 'eBook-newfile-ready'
                , state: 'ready'
                , description: 'Import previous worksheets'
                , url: eBook.Service.rule(this.settingsLog.assessmentYear)
                , action: 'ImportPrevious'
                , params: { cfdc: { FileId: '', Culture: eBook.Interface.Culture} }
                , paramKey: 'cfdc'
                , parmFileKey: 'FileId'
                , callback: this.onActionReady
                , scope: this
             });
             idx++;

             /*
             actions.push({
             id: idx
             , stateCls: 'eBook-newfile-ready'
             , state: 'ready'
             , description: 'Update worksheets containing ProAcc history'
             , url: eBook.Service.file
             , action: 'ImportWorksheetsProAccHistory'
             , params: { ccfdc: {} }
             , paramKey: 'ccfdc'
             , callback: this.onActionReady
             , scope: this
             });
             idx++;
             */
         }



         actions.push({
             id: idx
            , stateCls: 'eBook-newfile-ready'
            , state: 'ready'
            , description: 'Open newly created file'
            , fn: this.openFile
            , callback: this.onFileOpened
            , scope: this
         });

         actions.push({
             state: 'ready'
                , response: null
                , text: 'Update cache'
                , url: eBook.Service.file
                , action: 'ForceFileCacheReload'
                , params: { cfdc: { FileId: this.fileId, Culture: eBook.Interface.Culture} }
         });
         this.dta.actions = actions;

         this.overview.el.update(this.tpl.apply(this.dta));
         this.startActions.defer(200, this);
     }
     , getStateClass: function (state) {
         // eBook-newfile-busy
         // eBook-newfile-ready
         // eBook-newfile-done
         // eBook-newfile-error
         return 'eBook-newfile-' + state;
     }
     , updateActionStatus: function (idx, prevstate, state) {
         var cls = this.getStateClass(state);
         var prevCls = this.getStateClass(prevstate);
         var elId = 'eBook-newfile-action-' + idx;
         var el = Ext.get(elId);
         if (el) {
             el.replaceClass(prevCls, cls);
         }

     }
     , startActions: function () {
         this.ownerCt.ownerCt.locked = true;
         this.actionIdx = 0;
         this.executeAction(this.actionIdx);
     }
     , executeAction: function (actIdx) {
         if (actIdx >= this.dta.actions.length) {
             this.ownerCt.ownerCt.locked = false;
             this.actionIdx = 0;
             return;
         }

         this.updateActionStatus(actIdx, 'ready', 'busy');

         var act = this.dta.actions[actIdx];
         if (!act.fn) {
             if (act.paramKey && this.fileId) {
                 if (act.parmFileKey) {
                     act.params[act.paramKey][act.parmFileKey] = this.fileId;
                 } else {
                     act.params[act.paramKey].fid = this.fileId;
                 }
             }
             Ext.Ajax.request({
                 url: act.url + act.action
                , method: 'POST'
                , params: Ext.encode(act.params)
                , callback: act.callback
                , scope: act.scope
                , actionIdx: actIdx
                , action: act.action
             });
         } else {
             act.fn.call(act.scope);
             //this.onActionReady({ actionIdx: actIdx }, true, null);
         }
     }
     , handleError: function (opts, resp) {
         this.updateActionStatus(opts.actionIdx, 'busy', 'error');
         var dtl = '';
         try {
             var respObj = Ext.decode(response.responseText);
             dtl = respObj.Message;
         } catch (e) {
             dtl = 'Status: ' + resp.status + ' - ' + resp.statusText;
         }
         var elId = 'eBook-newfile-action-' + opts.actionIdx;
         var el = Ext.get(elId);
         var errEl = el.child('.eBook-newfile-action-errMsg');
         errEl.setStyle('display', 'block');
         errEl.update(dtl);
         this.ownerCt.ownerCt.locked = false;
     }
     , onFileCreated: function (opts, success, resp) {
         if (!success) {
             this.handleError(opts, resp);
         } else {
             try {
                 var respObj = Ext.decode(resp.responseText);
                 this.fileId = respObj.CreateFileResult;
                 this.updateActionStatus(opts.actionIdx, 'busy', 'done');
                 this.executeAction(opts.actionIdx + 1);
             } catch (e) {
                 alert(e);
             }
         }
     }
     , onActionReady: function (opts, success, resp) {
         if (success) {
             this.updateActionStatus(opts.actionIdx, 'busy', 'done');
             this.executeAction(opts.actionIdx + 1);
         } else {
             this.handleError(opts, resp);
         }
     }
     , openFile: function () {
         eBook.Interface.openFile(this.fileId, true);
         this.ownerCt.ownerCt.locked = false;
         // open mappings?
         this.ownerCt.ownerCt.close();
     }
     , onFileOpened: function () {
         this.ownerCt.ownerCt.locked = false;
         // open mappings?
         this.ownerCt.ownerCt.close();
     }
});

Ext.reg('eBook.NewFile.CreateFile', eBook.NewFile.CreateFile);
