

eBook.NewFile.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            items: [
                { xtype: 'eBook.NewFile.State', ref: 'state', region: 'west', width: 250 }
                , { xtype: 'eBook.NewFile.Wizard', ref: 'wizard', region: 'center'}]
            , modal: true
            , layout: 'border'
            , listeners: {
                'render': {
                    fn: function() { this.state.loadSteps(this.wizard); }
                        , scope: this
                }
            }

        });
        eBook.NewFile.Window.superclass.initComponent.apply(this, arguments);
    }
    , updateState: function(stepId, state) {
        var istate = -1;
        if (state === true) { istate = 2; }
        if (state === null) { istate = 1; }
        if (state === false) { istate = -99; }
        if (state === 0) { istate = 0; }
        this.state.updateState(stepId - 1, istate);
    }
});


eBook.NewFile.State = Ext.extend(Ext.Panel, {
    dta: []
    , initComponent: function() {
        this.tpl = new Ext.XTemplate(
            '<div class="eBook-wizard-steps-body">'
            , '<tpl for=".">'

                , '<div class="eBook-wizard-step eBook-wizard-step-state-{state}"><div class="eBook-wizard-step-icon">{title}</div></div>'

            , '</tpl>'
            , '</div>'
        );
        this.tpl.compile();
        Ext.apply(this, {
            html: this.tpl.apply(this.dta)
            ,bodyStyle:'background-color:transparent;'
        });
        eBook.NewFile.State.superclass.initComponent.apply(this, arguments);
    }
    , loadSteps: function(wizard) {
        this.dta = [];

        wizard.items.each(function(it) {
            this.dta.push({ id: it.initialConfig.ref.replace('step', ''), title: it.title, cref: it.initialConfig.ref, state: 0 });
        }, this);
        this.dta[0].state = 1;
        if (this.rendered) { this.body.update(this.tpl.apply(this.dta)); }
        else { this.html = this.tpl.apply(this.dta); }
    }
    , updateState: function(stepId, state) {
        this.dta[stepId].state = state;
        if (this.rendered) { this.body.update(this.tpl.apply(this.dta)); }
        else { this.html = this.tpl.apply(this.dta); }
    }
});

Ext.reg('eBook.NewFile.State', eBook.NewFile.State);