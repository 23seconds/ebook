eBook.BizTax.ToolbarInfo = Ext.extend(Ext.Toolbar.Item, {
    constructor: function (config) {
        eBook.BizTax.ToolbarInfo.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function (ct, position) {
        this.autoEl = { cls: 'biztax-tb-info', children: [
                { cls: 'biztax-tb-status-text', html: eBook.BizTax.Status_0 }
                , { cls: 'biztax-tb-info-validation', html: '.....................' }
                 , { cls: 'biztax-tb-info-calc', html: '.....................' }
                , { cls: 'biztax-tb-info-save', html: '.....................' }
                ]
        };
        eBook.BizTax.ToolbarInfo.superclass.onRender.call(this, ct, position);
    },
    setValidationProgress: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-validation').update("validating...");
            this.el.child('.biztax-tb-info-validation').addClass('biztax-tb-info-validation-busy');
        }
    }
    ,
    setCalcProgress: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-calc').update("calculating...");
            this.el.child('.biztax-tb-info-calc').addClass('biztax-tb-info-calc-busy');
        }
    },
    setSaveProgress: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-save').update("saving...");
            this.el.child('.biztax-tb-info-save').addClass('biztax-tb-info-save-busy');
        }
    }
    , setSaveWait: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-validation').update('Waiting to save, wait 0.5s');
            this.el.child('.biztax-tb-info-validation').addClass('biztax-tb-info-validation-busy');
        }
    }
    , clearSaveWait: function () {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-validation').update('...');
            this.el.child('.biztax-tb-info-validation').removeClass('biztax-tb-info-validation-busy');
        }
    }
     , setValidation: function (d, valid) {
         var t = d == null ? '.....................' : d.format('d/m/Y H:i:s');

         if (this.rendered) {
             this.el.child('.biztax-tb-info-validation').update(t);
             this.el.child('.biztax-tb-info-validation').removeClass('biztax-tb-info-validation-busy');
             if (!valid) {
                 this.el.child('.biztax-tb-info-validation').addClass('biztax-tb-info-validation-invalid');
             } else {
                 this.el.child('.biztax-tb-info-validation').removeClass('biztax-tb-info-validation-invalid');
             }
         } else {
             this.text = t;
         }
     }
    , setCalc: function (d) {
        var t = d == null ? '---' : Ext.util.Format.number(d, '0.000,00/i') + '€';
        eBook.Interface.center.fileMenu.setBizTax(d);
        
        if (this.rendered) {
            this.el.child('.biztax-tb-info-calc').removeClass('biztax-tb-info-calc-pos');
            this.el.child('.biztax-tb-info-calc').removeClass('biztax-tb-info-calc-neg');
            this.el.child('.biztax-tb-info-calc').addClass('biztax-tb-info-calc-' + (d <= 0 ? 'pos' : 'neg'));
            this.el.child('.biztax-tb-info-calc').update(t);
            this.el.child('.biztax-tb-info-calc').removeClass('biztax-tb-info-calc-busy');
        } else {
            this.text = t;
        }
    }
    , setSave: function (d) {
        var t = d == null ? '.....................' : d.format('d/m/Y H:i:s');
        if (this.rendered) {
            this.el.child('.biztax-tb-info-save').update(t);
            this.el.child('.biztax-tb-info-save').removeClass('biztax-tb-info-save-busy');
        } else {
            this.text = t;
        }
    }
});
Ext.reg('biztax-tb-info', eBook.BizTax.ToolbarInfo);

eBook.BizTax.ToolbarStatus = Ext.extend(Ext.Toolbar.Item, {
    constructor: function (config) {
        eBook.BizTax.ToolbarStatus.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function (ct, position) {
        //this.autoEl = { cls: 'biztax-tb-status', children: [{ cls: 'biztax-tb-status-text', html: eBook.BizTax.Status_0}] };

        this.autoEl = { children: [
           
             { cls: 'biztax-tb-proxy biztax-tb-noproxy',
                children: [{ cls: 'biztax-tb-proxy-content', html: 'PROXY: !! no proxy found, upload !!'}]
            }
            ]
        };

        eBook.BizTax.ToolbarStatus.superclass.onRender.call(this, ct, position);
    },
    setStatus: function (s) {
        var t = 'Status: ' + eBook.BizTax["Status_" + s];
        var cls = 'biztax-status-' + s;
        if (s > 0) cls += ' biztax-lock-16';
        if (this.rendered) {
            this.el.child('.biztax-tb-status-text').update(t);
            this.el.dom.className = 'biztax-tb-status ' + cls;
        } else {
            this.autoEl.cls = 'biztax-tb-status ' + cls;
            this.text = t;
        }
    }
});
Ext.reg('biztax-tb-status', eBook.BizTax.ToolbarStatus);

/*
children:[
{tag:'span',cls:'biztax-tb-proxy-path',html: '1. Final Deliverables\\B. Corporate Income Tax Return\\Proxy CITR\\' }
,{tag:'span',cls:'biztax-tb-proxy-file',html: '!! no proxy found, upload !!' }
]
}
*/
eBook.BizTax.ToolbarProxy = Ext.extend(Ext.Toolbar.Item, {
    constructor: function (config) {
        eBook.BizTax.ToolbarProxy.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function (ct, position) {
        this.autoEl = { cls: 'biztax-tb-proxy biztax-tb-noproxy',
            children: [{ cls: 'biztax-tb-proxy-content', html: 'PROXY: !! no proxy found, upload !!' }
            ]
        };
        eBook.BizTax.ToolbarProxy.superclass.onRender.call(this, ct, position);
    },
    setProxy: function (s) {
        var cls = 'biztax-tb-proxy';
        var t = 'PROXY: ';
        if (!s || s == '') {
            cls += ' biztax-tb-noproxy';
            t += '!! no proxy found, upload !!';
        } else {
            t += s;
        }
        if (this.rendered) {
            this.el.dom.className = cls;
            this.el.child('.biztax-tb-proxy-content').update(t);
        } else {
            this.autoEl.cls = cls;
            this.autoEl.children[1].html = t;
        }
    }
});
Ext.reg('biztax-tb-proxy', eBook.BizTax.ToolbarProxy);



eBook.BizTax.ToolbarValidation = Ext.extend(Ext.Toolbar.Item, {
    mainTpl: new Ext.XTemplate('<div class="eBook-BizTax-errorMgs-wrapper">' +
                                    '<tpl for=".">' +
                                        '<tpl for="Messages">' +
                                            '<tpl if="Culture==eBook.Interface.Culture">' +
                                                '<div class="eBook-BizTax-errorMg" ext:qtitle="{Id}"  ext:qtip="{Message}">{parent.Id}: {[this.FirstChars(values.Message)]}</div>' +
                                              '</tpl>' +
                                        '</tpl>' +
                                     '</tpl>' +
                                '</div>'
                                , { compiled: true, FirstChars: function (msg) { if (msg.length > 100) { return msg.substr(0, 100) + "..."; } else { return msg; } } }),
    constructor: function (config) {
        eBook.BizTax.ToolbarValidation.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    updateMe: function (errorMgs) {
        /* for (var i = 0; i < this.messages.length; i++) {
        this.errorMgs.push(this.messages[i].Messages[0].Message);
        }*/
        this.el.child('.biztax-tb-validation-body').update(this.mainTpl.apply(errorMgs));
    },
    onRender: function (ct, position) {
        //this.autoEl = { cls: 'biztax-tb-status', children: [{ cls: 'biztax-tb-status-text', html: eBook.BizTax.Status_0}] };

        this.autoEl = { cls: 'biztax-tb-validation'
                , children: [
                     { cls: 'biztax-tb-validation-state' }
                     , { cls: 'biztax-tb-validation-body' }
                ]
        };

        eBook.BizTax.ToolbarValidation.superclass.onRender.call(this, ct, position);
    }
    , setBusy: function () {
        this.el.mask('Save &amp; validation in progress');
    }
});
Ext.reg('biztax-tb-validation', eBook.BizTax.ToolbarValidation);