eBook.BizTax['Status_-99'] = 'Declaration FAILED, REVIEW!!';
eBook.BizTax.Status_0 = 'In preparation';
eBook.BizTax.Status_1 = 'Locked (In validation by partner)';
eBook.BizTax.Status_10 = 'Send to be declared';
eBook.BizTax.Status_30 = 'Declaration in progress';
eBook.BizTax.Status_99 = 'Succesfully declared';


eBook.BizTax.Module = Ext.extend(Ext.ux.IconTabPanel, {
    xbrlData: {},
    biztaxRendered: false,
    initComponent: function () {
        this.validationMsgs = [];
        // this.elementStore = new Ext.data.JsonStore();
        var fileClosed = false;
        Ext.apply(this, {
            tabWidth: 200
            , enableTabTitles: true
            , itemsCls: 'eb-biztax-tabitem'
            , cls: 'eb-biztax-tab'
            //, activeTab: 0
            , ref: 'module'
            , tbar: [
                {xtype: 'tbspacer', width: 50}
                , {xtype: 'biztax-tb-info', text: '', ref: 'infoPane'}
                , {
                    ref: 'validerrors',
                    text: 'Validation errors',
                    iconCls: 'eBook-Warning-ico-24',
                    cls: 'btnValidationErrors',
                    scale: 'medium',
                    //width: 115,
                    iconAlign: 'top',
                    handler: function () {
                        var wn = new eBook.BizTax.ValidationOverview({messages: this.store.data.Data.Errors});
                        wn.show();
                        //wn.items.items[0].updateMe(this.store.data.Data.Errors);
                    },
                    scope: this
                    //,disabled: true //fileClosed
                }

                , '->'

                , {xtype: 'biztax-tb-proxy', ref: 'proxyPane'}, {
                    ref: 'addproxy',
                    text: 'Upload proxy', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onUploadProxyClick,
                    width: 70,
                    scope: this,
                    disabled: eBook.Interface.isFileClosed()
                }
                , {
                    ref: 'schema',
                    text: 'Schema',
                    iconCls: 'eBook-icon-account-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function () {
                        eBook.Interface.openWindow('eBook-File-Schema');
                    },
                    scope: this,
                    disabled: fileClosed
                }
                , {
                    ref: 'quickprint',
                    text: 'Quickprint',
                    iconCls: 'eBook-icon-quickprint-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function () {
                        eBook.Interface.openWindow('eBook-File-quickprint');
                    },
                    scope: this
                }
                , {
                    ref: 'repository',
                    text: 'Repository',
                    iconCls: 'eBook-repository-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: function () {
                        eBook.Interface.openWindow('eBook-File-repository');
                    },
                    scope: this,
                    disabled: fileClosed
                }, {
                    ref: 'taxCalc',
                    text: 'Tax calculation', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-biztax-taxcalc-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onPreviewTaxCalc,
                    width: 70,
                    scope: this
                },
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    ref: 'vitals',
                    items: [
                        {
                            ref: '../clientBundle',
                            text: 'Client Bundle',
                            iconCls: 'eBook-client-bundle-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: function () {
                                eBook.Interface.openWindow('eBook-File-ReportGeneration-biztax');
                            },
                            scope: this,
                            disabled: fileClosed
                        },
                        {
                            ref: 'ConfirmationOfApprovalCITR',
                            text: 'Confirmation of Approval CITR', // eBook.Accounts.Manage.Window_AddAccount,
                            iconCls: 'eBook-client-approval-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onConfirmationOfApprovalCITR,
                            width: 70,
                            scope: this
                        }
                        , {
                            ref: '../unlock',
                            text: 'Unlock contents',
                            iconCls: 'eBook-unlock-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: function () {
                                if (eBook.User.isActiveClientRolesFullAccess()) {
                                    this.changeStatus(0);
                                } else {
                                    Ext.Msg.show({
                                        title: 'Executive only',
                                        msg: 'Only executives are allowed to unlock the Biztax module. Contact your team manager/partner.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        scope: this
                                    });
                                }
                            },
                            scope: this,
                            hidden: true
                        }
                        , {
                            ref: '../lock',
                            text: 'Lock contents',
                            iconCls: 'eBook-lock-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: function () {
                                if (eBook.User.isActiveClientRolesFullAccess()) {
                                    this.changeStatus(1);
                                } else {
                                    Ext.Msg.show({
                                        title: 'Executive only',
                                        msg: 'Only executives are allowed to lock the Biztax module. Contact your team manager/partner.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        scope: this
                                    });
                                }
                            },
                            scope: this,
                            disabled: fileClosed
                        }
                        , {
                            ref: '../declare',
                            text: 'Declare',
                            iconCls: 'eBook-biztax-send-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: function () {
                                if (eBook.User.isActiveClientRolesFullAccess()) {
                                    var wn = new eBook.BizTax.Declaration.Window({module: this});
                                    wn.show();
                                } else {
                                    Ext.Msg.show({
                                        title: 'Executive only',
                                        msg: 'Only executives are allowed to lock declare to Biztax. Contact your team manager/partner.',
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR,
                                        scope: this
                                    });
                                }
                            },
                            scope: this,
                            disabled: fileClosed
                        }]
                }, {xtype: 'tbspacer', width: 50}
            ]
            , listeners: {
                'activate': {
                    fn: this.onActivate
                    , scope: this
                }
            }
        });
        eBook.BizTax.Module.superclass.initComponent.apply(this, arguments);

    }
    , clearTabErrors: function () {
        var els = this.tabStrip.query('.biztax-tabstrip-invalid'),
            i = 0;

        for (i = 0; i < els.length; i++) {
            Ext.get(els[i]).removeClass('biztax-tabstrip-invalid');
        }

    }
    , setTabInvalid: function (code) {
        var childIdx = this.items.indexOfKey("biztax-tab-" + code);
        var el = this.tabStrip.dom.childNodes[childIdx];
        if (el) {
            Ext.get(el).addClass('biztax-tabstrip-invalid');
        }

    }
    , preRenderTabs: function (tabids) {
        var i = 0, tabid;
        for (i = 0; i < tabids.length; i++) {
            tabid = tabids[i];
            if (tabid.indexOf("biztax-tab-") == -1) {
                tabid = "biztax-tab-" + tabid;
            }
            this.preRenderTab(tabid);
        }
    }
    , preRenderTab: function (tabid) {
        var ly = this.getLayout(),
            ct = this.container, target = this.getLayoutTarget(),
            item = this.items.get(tabid),
            idx = this.items.indexOfKey(tabid);
        if (item && !item.rendered) {
            ly.renderItem(item, idx, target);
        }
    }
    , onPreviewTaxCalc: function () {
        eBook.Splash.setText("Generating print Biztax Tax calculation ");
        eBook.Splash.show();
        Ext.Ajax.request({
            url: eBook.Service.output + 'GetBiztaxTaxDetail'
            , method: 'POST'
            , params: Ext.encode({
                cidc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.Culture
                }
            })
            , callback: this.onPreviewTaxCalcResponse
            , scope: this
        });
    }
    , onPreviewTaxCalcResponse: function (opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetBiztaxTaxDetailResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    ///Open messagebox which allows the user to choose some settings regarding the confirmation of aproval CITR
    , onConfirmationOfApprovalCITR : function() {
        var me = this;
        var msg = 'Choose the cooperative:<br /><br /><select id="coop"><option value="1">EY Tax Consultants BV CVBA</option><option value="2">EY Accountants BV CVBA</option><option value="3">EY Fiduciaire BV CVBA</option></select>';
        msg += '<br/><br/>Language:<br /><br /><select id="language" style="width:207px;"><option value="nl-BE">Nederlands</option><option value="fr-FR">Français</option><option value="en-GB">English</option></select>';
        //Ask user to choose the cvba through a messagebox containing a combobox
        Ext.MessageBox.show({
            title: 'Confirmation of Approval CITR',
            msg: msg,
            buttons: Ext.MessageBox.OKCANCEL,
            fn: function (btn) {
                if (btn == 'ok') {
                    me.GetConfirmationOfApprovalCITR(Ext.get('coop').getValue(),Ext.get('language').getValue());
                }
            }
        });
    }
    ///Get Confirmation Of Approval Corporate Income Tax Return PDF
    , GetConfirmationOfApprovalCITR: function(cooperative, language) {
        eBook.Splash.setText("Generating print Confirmation of Approval Corporate Income Tax Return");
        eBook.Splash.show();

        Ext.Ajax.request({
            url: eBook.Service.output + 'GetConfirmationOfApprovalCITR'
            , method: 'POST'
            , params: Ext.encode({
                ciaddc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: language,
                    Department: cooperative
                }
            })
            , callback: this.onGetConfirmationOfApprovalCITRResponse
            , scope: this
        });
    }
    ///Remove splash, open pdf
    , onGetConfirmationOfApprovalCITRResponse: function(opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetConfirmationOfApprovalCITRResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onUploadProxyClick: function () {
        // var wn = new eBook.Repository.LocalFileBrowser({});
        //var wn = new eBook.Window({layout:'fit',items:[new eBook.Repository.FileDetailsPanel({})]});
        var wn = new eBook.Repository.SingleUploadWindow({
            standards: {
                statusid: 'fcf7a560-3d74-4341-a32b-310c8fa336cf'
                , location: 'd33ce2b5-fbeb-4d58-9c47-2a4731804173'
            }
            , readycallback: {fn: this.store.loadData, callerCallback: this.onDataLoaded, scope: this.store, callerCallbackScope: this}
        });
        wn.show();
    }
    , onActivate: function () {
        this.biztaxType = eBook.Interface.currentClient.get('bni') ? 'nrcorp' : 'rcorp';
        this.loadBizTax('' + eBook.Interface.currentFile.get('AssessmentYear'), this.biztaxType); // (n)rcorp
    }
    , clearAll: function () {


        if (this.biztaxRendered) {
            if (this.store) {
                this.store.destroy();
                delete this.store;
            }
            this.removeAll(true);

            this.biztaxRendered = false;
        }
    }
    , removeAll: function (autoDestroy) {
        this.initItems();
        var item, rem = [], items = [];
        this.items.each(function (i) {
            rem.push(i);
        });
        for (var i = rem.length - 1; i > -1; i--) {
            item = rem[i];
            this.remove(item, autoDestroy);
            if (item.ownerCt !== this) {
                items.push(item);
            }
        }
        return items;
    }

    , changeStatus: function (newStatus) {
        var cb = null;

        this.store.data.Status = newStatus;
        this.store.saveMeta(true);
    }
    , loadBizTax: function (AY, biztaxtype) {
        //AY = '2012';

        if (parseInt(AY) >= 2014) {
            this.topToolbar.declare.disabled = true;
        }
        this.saveTriggerDte = null;
        this.biztaxtype = biztaxtype;
        if (!this.biztaxRendered) {
            this.getEl().mask("Rendering BizTax " + AY + ' ' + biztaxtype);
            var cult = eBook.Interface.Culture;
            if (cult == 'en-US') cult = 'nl-BE';
            eBook.LazyLoad.loadOnce.defer(50, this, ["BizTax/" + AY + "/" + biztaxtype + "/js/" + cult + ".js?" + eBook.SpecificVersion, this.onSpecificsLoaded, {
                AY: AY,
                biztaxtype: biztaxtype
            }, this, true]);
            // fetch biztax info (lazy load)


        } else {

            this.getEl().mask("Loading biztax data " + AY + ' ' + biztaxtype);
            if (this.store) {
                this.store.loadData(this.onDataLoaded, this);
            }
        }

    }
    , onSpecificsLoaded: function (cfg) {
        this.locator = 'AY' + cfg.AY + '_' + cfg.biztaxtype;
        if (!eBook.BizTax[this.locator]) {
            //alert("No biztax for assessmentyear " + AY);
            this.getTopToolbar().hide();
            this.add({
                xtype: 'panel',
                title: 'No BizTax module available',
                html: '<h2>There is no ' + cfg.biztaxtype + ' BizTax module for assessmentyear ' + cfg.AY + '</h2>'
            });
            this.setActiveTab(0);
            this.biztaxRendered = true;
            this.getEl().unmask();

            return;
        }
        this.getTopToolbar().show();
        this.store = new eBook.BizTax.Store({
            serviceUrl: eBook.Service.biztax
            , biztax: {
                type: cfg.biztaxtype

                , assessmentyear: cfg.AY
            }
            , moduleId: this.id
        });

        this.fields = {};
        this.contextGrids = [];
        this.fieldsIdx = [];
        this.add(Ext.clone(eBook.BizTax[this.locator].Tabs, true));
        this.biztaxRendered = true;
        this.setActiveTab(0);
        /*
         this.calcWorker = new Worker(eBook.BizTax[locator].Calculator+"?" + eBook.SpecificVersion); // forex:'js/Taxonomy/BizTax-calc-rcorp-'+AY+'.js');
         this.calcWorker.addEventListener('message', this.onCalculated, false);
         this.calcWorker.addEventListener('error', this.onCalculationError, false);

         this.validWorker = new Worker(eBook.BizTax[locator].Validator+"?" + eBook.SpecificVersion); // forex:'js/Taxonomy/BizTax-validation-rcorp-'+AY+'.js');
         this.validWorker.addEventListener('message', this.onValidated, false);
         this.validWorker.addEventListener('error', this.onValidationError, false);
         */

        this.getEl().mask("Loading biztax data " + cfg.AY + ' ' + cfg.biztaxtype);

        this.store.loadData(this.onDataLoaded, this);
    }
    , onDataLoaded: function () {


        var tb = this.getTopToolbar();
        var ip = tb.infoPane;
        if (this.store.data != null) {
            ip.setCalc(this.store.data.Data.TaxCalc);
        }
        if (this.store.lastSaved != null) {
            ip.setSave(this.store.data.Data.LastSaved);
        }
        /*
         tb.statusPane.setStatus(this.store.status);
         */
        tb.proxyPane.setProxy(this.store.data.Data.ProxyTitle);
        if (this.store.data.Data.ProxyId) {
            tb.addproxy.hide();
        } else {
            tb.addproxy.show();
        }
        tb.unlock.hide();
        tb.lock.show();
        tb.declare.disable();
        if (this.store.data.Status == 1) {
            tb.unlock.show();
            tb.lock.hide();
            if (eBook.User.isActiveClientRolesFullAccess()) {
                tb.declare.enable();
            }
            else {
            }
        } else if (this.store.data.Status > 1) {
            tb.lock.hide();
        }
        //  this.store.partner 

        // this.setFields();
        // this.setContextGrids();
        // this.triggerValidation();
        this.getEl().unmask(true);
    }


});
Ext.reg('biztax-module', eBook.BizTax.Module);

eBook.BizTax.DateField = Ext.extend(Ext.form.DateField, {});

eBook.BizTax.NumberField = Ext.extend(Ext.form.NumberField, {
    initComponent: function () {
        Ext.apply(this, {
            align: 'right', msgTarget: 'side'
        });
        eBook.BizTax.NumberField.superclass.initComponent.apply(this, arguments);
    }
    , markInvalid: function (msg) {
        var myEl = this.getEl();
        this.invalid = true;


    }
});
Ext.reg('biztax-numberfield', eBook.BizTax.NumberField);

eBook.BizTax.ValidationOverview = Ext.extend(eBook.Window, {
    initComponent: function () {
        this.errorMgs = [];
        var cultidx = eBook.Interface.Culture == 'fr-FR' ? 1 : 0;
        for (var i = 0; i < this.messages.length; i++) {
            this.errorMgs.push({id: this.messages[i].Id, msg: this.messages[i].Messages[cultidx].Message});
        }
        Ext.apply(this, {
            title: 'Validation Errors'
            , layout: 'fit'
            , width: 405
            , height: 405
            , iconCls: 'eBook-Bundle-icon-16'
            , bodyStyle: 'background-color:#FFF;'
            , items: [{xtype: 'errorPnl'}]
            , tbar: {
                xtype: 'toolbar',
                items: []
            }

        });
        eBook.BizTax.ValidationOverview.superclass.initComponent.apply(this, arguments);
    },

    show: function () {
        //alert('test show');
        eBook.Bundle.EditWindow.superclass.show.call(this);
        this.items.items[0].updateMe(this.errorMgs);
    }


});
Ext.reg('biztax-validationOverview', eBook.BizTax.ValidationOverview);


eBook.BizTax.ErrorPanel = Ext.extend(Ext.Panel, {
    errorMgs: this.messages,
    mainTpl: new Ext.XTemplate('<div class="eBook-BizTax-errorMgs-wrapper">' +
        '<tpl for=".">' +
        '<div class="eBook-BizTax-errorMgs-mgs" >{id}: {msg}</div>' +
        '</tpl>' +
        '</div>'
        , {compiled: true}),
    autoScroll: true,
    /*
     initComponent: function () {

     Ext.apply(this, {
     title:'TEST'

     });
     eBook.BizTax.ErrorPanel.superclass.initComponent.apply(this, arguments);
     },
     */
    updateMe: function (errorMgs) {
        this.body.update(this.mainTpl.apply(errorMgs));
    }

});

Ext.reg('errorPnl', eBook.BizTax.ErrorPanel);
