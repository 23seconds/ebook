
eBook.BizTax.UploadEditorWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , items: [
                { xtype: 'biztaxItemSettings', region: 'west', width: 300, ref: 'itemSettings' }
                , { xtype: 'biztaxUploadEditorPanel', region: 'center', ref: 'uploadEditorPnl', bundle: null} // DATAVIEW INSTEAD OF TREE
                , { xtype: 'bundlelibrary', region: 'east', width: 300, ref: 'library', culture: eBook.Interface.currentFile.get('Culture'), rootName: 'rootBizTax' }
            ]
            /*, tbar: [{
            ref: 'saveBundle',
            text: eBook.BizTax.Window_ReportSave,
            iconCls: 'eBook-icon-savereport-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onSaveList,
            scope: this
            }]*/
        });
        eBook.BizTax.UploadEditorWindow.superclass.initComponent.apply(this, arguments);
        if (this.data) {
            this.uploadEditorPnl.setData(this.data);
        }
    }
    , onSaveList: function() {
        var json = { "allowChildren": false, "allowDrag": true, "cls": "", "iconCls": "eBook-bundle-tree-statement", "id": "activa", "indexItem": { "__type": "StatementsDataContract:#EY.com.eBook.API.Contracts.Data", "EndsAt": 0, "FooterConfig": { "Enabled": true, "FootNote": null, "ShowFooterNote": true, "ShowPageNr": true, "Style": null }, "HeaderConfig": { "Enabled": true, "ShowTitle": true, "Style": null }, "NoDraft": false, "StartsAt": 0, "iTextTemplate": null, "iconCls": null, "id": "efa72689-3502-4bb9-b14e-da020945fd09", "indexed": true, "locked": false, "title": "Activa", "Culture": "nl-BE", "Detailed": true, "FileId": "fe14f2cd-49e8-42cc-897c-61e03afefe26", "layout": "ACTIVA", "type": "STATEMENT" }, "leaf": true, "text": "Activa" };
        var rt = new this.uploadEditorPnl.store.recordType(json);
        this.uploadEditorPnl.store.add(rt);
//        alert("save it to parent and close");
    }
    , close: function() {
        if (this.caller) {
            if(this.uploadEditorPnl) this.caller.updateValue(this.uploadEditorPnl.getData());
        }
        eBook.BizTax.UploadEditorWindow.superclass.close.call(this);
    }
    , loadNodeSettings: function(node) {
        this.itemSettings.loadNode(node, closed);
    }
});        
    