eBook.BizTax['Status_-99'] = 'Declaration FAILED, REVIEW!!';
eBook.BizTax.Status_0 = 'In preparation';
eBook.BizTax.Status_1 = 'Locked (In validation by partner)';
eBook.BizTax.Status_10 = 'Send to be declared';
eBook.BizTax.Status_30 = 'Declaration in progress';
eBook.BizTax.Status_99 = 'Succesfully declared';


eBook.BizTax.ToolbarInfo = Ext.extend(Ext.Toolbar.Item, {
    constructor: function(config) {
        eBook.BizTax.ToolbarInfo.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function(ct, position) {
        this.autoEl = { cls: 'biztax-tb-info', children: [
            { cls: 'biztax-tb-info-calc', html: '.....................' }
            , { cls: 'biztax-tb-info-validation', html: '.....................' }
            , { cls: 'biztax-tb-info-save', html: '.....................' }
            ]
        };
        eBook.BizTax.ToolbarInfo.superclass.onRender.call(this, ct, position);
    },
    setValidationProgress: function() {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-validation').update("validating...");
            this.el.child('.biztax-tb-info-validation').addClass('biztax-tb-info-validation-busy');
        }
    }
    ,
    setCalcProgress: function() {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-calc').update("calculating...");
            this.el.child('.biztax-tb-info-calc').addClass('biztax-tb-info-calc-busy');
        }
    },
    setSaveProgress: function() {
        if (this.rendered) {
            this.el.child('.biztax-tb-info-save').update("saving...");
            this.el.child('.biztax-tb-info-save').addClass('biztax-tb-info-save-busy');
        }
    }
     , setValidation: function(d, valid) {
         var t = d == null ? '.....................' : d.format('d/m/Y H:i:s');

         if (this.rendered) {
             this.el.child('.biztax-tb-info-validation').update(t);
             this.el.child('.biztax-tb-info-validation').removeClass('biztax-tb-info-validation-busy');
             if (!valid) {
                 this.el.child('.biztax-tb-info-validation').addClass('biztax-tb-info-validation-invalid');
             } else {
                 this.el.child('.biztax-tb-info-validation').removeClass('biztax-tb-info-validation-invalid');
             }
         } else {
             this.text = t;
         }
     }
    , setCalc: function(d) {
        var t = d == null ? '.....................' : d.format('d/m/Y H:i:s');
        if (this.rendered) {
            this.el.child('.biztax-tb-info-calc').update(t);
            this.el.child('.biztax-tb-info-calc').removeClass('biztax-tb-info-calc-busy');
        } else {
            this.text = t;
        }
    }
    , setSave: function(d) {
        var t = d == null ? '.....................' : d.format('d/m/Y H:i:s');
        if (this.rendered) {
            this.el.child('.biztax-tb-info-save').update(t);
            this.el.child('.biztax-tb-info-save').removeClass('biztax-tb-info-save-busy');
        } else {
            this.text = t;
        }
    }
});
Ext.reg('biztax-tb-info', eBook.BizTax.ToolbarInfo);

eBook.BizTax.ToolbarStatus = Ext.extend(Ext.Toolbar.Item, {
    constructor: function(config) {
        eBook.BizTax.ToolbarStatus.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function(ct, position) {
        this.autoEl = { cls: 'biztax-tb-status', children: [{ cls: 'biztax-tb-status-text', html: eBook.BizTax.Status_0}] };
        eBook.BizTax.ToolbarStatus.superclass.onRender.call(this, ct, position);
    },
    setStatus: function(s) {
        var t = 'Status: ' + eBook.BizTax["Status_" + s];
        var cls = 'biztax-status-' + s;
        if (s > 0) cls += ' biztax-lock-16';
        if (this.rendered) {
            this.el.child('.biztax-tb-status-text').update(t);
            this.el.dom.className = 'biztax-tb-status ' + cls;
        } else {
            this.autoEl.cls = 'biztax-tb-status ' + cls;
            this.text = t;
        }
    }
});
Ext.reg('biztax-tb-status', eBook.BizTax.ToolbarStatus);

/*
 children:[
                            {tag:'span',cls:'biztax-tb-proxy-path',html: '1. Final Deliverables\\B. Corporate Income Tax Return\\Proxy CITR\\' }
                            ,{tag:'span',cls:'biztax-tb-proxy-file',html: '!! no proxy found, upload !!' }
                        ]
                     }
*/
eBook.BizTax.ToolbarProxy = Ext.extend(Ext.Toolbar.Item, {
    constructor: function(config) {
        eBook.BizTax.ToolbarProxy.superclass.constructor.call(this, Ext.isString(config) ? { text: config} : config);
    },
    onRender: function(ct, position) {
        this.autoEl = { cls: 'biztax-tb-proxy biztax-tb-noproxy',
            children: [{ cls: 'biztax-tb-proxy-content', html: 'PROXY: !! no proxy found, upload !!' }
            ]
        };
        eBook.BizTax.ToolbarProxy.superclass.onRender.call(this, ct, position);
    },
    setProxy: function(s) {
        var cls = 'biztax-tb-proxy';
        var t = 'PROXY: ';
        if (!s || s == '') {
            cls += ' biztax-tb-noproxy';
            t += '!! no proxy found, upload !!';
        } else {
            t += s;
        }
        if (this.rendered) {
            this.el.dom.className = cls;
            this.el.child('.biztax-tb-proxy-content').update(t);
        } else {
            this.autoEl.cls = cls;
            this.autoEl.children[1].html = t;
        }
    }
});
Ext.reg('biztax-tb-proxy', eBook.BizTax.ToolbarProxy);



eBook.BizTax.Module = Ext.extend(Ext.ux.IconTabPanel, {
    xbrlData: {},
    biztaxRendered: false,
    initComponent: function() {
        this.validationMsgs = [];
        // this.elementStore = new Ext.data.JsonStore();
        var fileClosed = false;
        Ext.apply(this, {
            tabWidth: 200
            , itemsCls: 'eb-biztax-tabitem'
            , cls: 'eb-biztax-tab'
            //, activeTab: 0
            , ref: 'module'
            , tbar: [
                { xtype: 'tbspacer', width: 50 }
                , { xtype: 'biztax-tb-info', text: '', ref: 'infoPane' }
                , { xtype: 'biztax-tb-status', text: '', ref: 'statusPane' }
                , { xtype: 'biztax-tb-proxy', ref: 'proxyPane' }, {
                    ref: 'addproxy',
                    text: 'Upload proxy', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onUploadProxyClick,
                    width: 70,
                    scope: this,
                    disabled: eBook.Interface.isFileClosed()
                }
                  , '->', {
                      ref: 'validerrors',
                      text: 'Validation errors',
                      iconCls: 'eBook-Warning-ico-32 ',
                      scale: 'medium',
                      iconAlign: 'top',
                      handler: function() {
                          var wn = new eBook.BizTax.ValidationOverview({ messages: this.validationMsgs });
                          wn.show();
                      },
                      scope: this,
                      disabled: fileClosed
                  }, {
                      ref: 'repository',
                      text: 'Repository',
                      iconCls: 'eBook-repository-menu-ico',
                      scale: 'medium',
                      iconAlign: 'top',
                      handler: function() { eBook.Interface.openWindow('eBook-File-repository'); },
                      scope: this,
                      disabled: fileClosed
                  },
                  { xtype: 'buttongroup',
                      columns: 4,
                      ref: 'vitals',
                      items: [
                  {
                      ref: '../clientBundle',
                      text: 'Client Bundle',
                      iconCls: 'eBook-client-bundle-24',
                      scale: 'medium',
                      iconAlign: 'top',
                      handler: function() { eBook.Interface.openWindow('eBook-File-ReportGeneration-biztax'); },
                      scope: this,
                      disabled: fileClosed
                  }
                   , {
                       ref: '../unlock',
                       text: 'Unlock contents',
                       iconCls: 'eBook-unlock-24',
                       scale: 'medium',
                       iconAlign: 'top',
                       handler: function() {
                           if (eBook.User.isActiveClientRolesFullAccess()) {
                               this.changeStatus(0);
                           } else {
                               Ext.Msg.show({
                                   title: 'Executive only',
                                   msg: 'Only executives are allowed to unlock the Biztax module. Contact your team manager/partner.',
                                   buttons: Ext.Msg.OK,
                                   icon: Ext.MessageBox.ERROR,
                                   scope: this
                               });
                           }
                       },
                       scope: this,
                       hidden: true
                   }
                  , {
                      ref: '../lock',
                      text: 'Lock contents',
                      iconCls: 'eBook-lock-24',
                      scale: 'medium',
                      iconAlign: 'top',
                      handler: function() {
                          if (eBook.User.isActiveClientRolesFullAccess()) {
                              this.changeStatus(1);
                          } else {
                              Ext.Msg.show({
                                  title: 'Executive only',
                                  msg: 'Only executives are allowed to lock the Biztax module. Contact your team manager/partner.',
                                  buttons: Ext.Msg.OK,
                                  icon: Ext.MessageBox.ERROR,
                                  scope: this
                              });
                          }
                      },
                      scope: this,
                      disabled: fileClosed
                  }
                 , {
                     ref: '../declare',
                     text: 'Declare',
                     iconCls: 'eBook-biztax-send-24',
                     scale: 'medium',
                     iconAlign: 'top',
                     handler: function() {
                         if (eBook.User.isActiveClientRolesFullAccess()) {
                             var wn = new eBook.BizTax.Declaration.Window({ module: this });
                             wn.show();
                         } else {
                             Ext.Msg.show({
                                 title: 'Executive only',
                                 msg: 'Only executives are allowed to lock declare to Biztax. Contact your team manager/partner.',
                                 buttons: Ext.Msg.OK,
                                 icon: Ext.MessageBox.ERROR,
                                 scope: this
                             });
                         }
                     },
                     scope: this,
                     disabled: fileClosed
}]
                  }, { xtype: 'tbspacer', width: 50 }
            ]
            , listeners: {
                'activate': {
                    fn: this.onActivate
                    , scope: this
                }
            }
        });
        eBook.BizTax.Module.superclass.initComponent.apply(this, arguments);

    }
    , onUploadProxyClick: function() {
        // var wn = new eBook.Repository.LocalFileBrowser({});
        //var wn = new eBook.Window({layout:'fit',items:[new eBook.Repository.FileDetailsPanel({})]});
        var wn = new eBook.Repository.SingleUploadWindow({
            standards: {
                statusid: 'fcf7a560-3d74-4341-a32b-310c8fa336cf'
                , location: 'd33ce2b5-fbeb-4d58-9c47-2a4731804173'
            }
            , readycallback: { fn: this.loadData, scope: this }
        });
        wn.show();
    }
    , onActivate: function() {
        this.biztaxType = eBook.Interface.currentClient.get('bni') ? 'nrcorp' : 'rcorp';
        this.loadBizTax('' + eBook.Interface.currentFile.get('AssessmentYear'), this.biztaxType); // (n)rcorp
    }
    , clearAll: function() {
        if (this.biztaxRendered) {

            for (var i = 0; i < this.fieldsIdx.length; i++) {
                Ext.destroy(this.fields[this.fieldsIdx[i]]);
                delete this.fields[this.fieldsIdx[i]];
            }
            Ext.destroy(this.store);
            delete this.fields;
            delete this.fieldsIdx;
            Ext.destroy(this.contextGrids);
            if (this.calcWorker) {
                this.calcWorker.terminate();
                this.calcWorker = null;
            }
            if (this.validWorker) {
                this.validWorker.terminate();
                this.validWorker = null;
            }
            this.removeAll(true);
        }
        this.biztaxRendered = false;
    }
    , removeAll: function(autoDestroy) {
        this.initItems();
        var item, rem = [], items = [];
        this.items.each(function(i) {
            rem.push(i);
        });
        for (var i = rem.length - 1; i > -1; i--) {
            item = rem[i];
            this.remove(item, autoDestroy);
            if (item.ownerCt !== this) {
                items.push(item);
            }
        }
        return items;
    }
    , loadBizTax: function(AY, biztaxtype) {
        //AY = '2012';
        this.saveTriggerDte = null;
        this.biztaxtype = biztaxtype;
        if (!this.biztaxRendered) {
            this.getEl().mask("Rendering BizTax " + AY + ' ' + biztaxtype);
            var cult = eBook.Interface.Culture;
            if (cult == 'en-US') cult = 'nl-BE';
            eBook.LazyLoad.loadOnce.defer(50, this, ["js/Taxonomy/BizTax-" + biztaxtype + "-" + AY + "-" + cult + ".js?" + eBook.SpecificVersion, this.onSpecificsLoaded, { AY: AY, biztaxtype: biztaxtype }, this, true]);
            // fetch biztax info (lazy load)


        } else {
            var tb = this.getTopToolbar();
            tb.vitals.getEl().mask('validation in progress');
            this.loadData();
        }

    }
    , onSpecificsLoaded: function(cfg) {
        var locator = 'AY' + cfg.AY + '_' + cfg.biztaxtype;
        if (!eBook.BizTax[locator]) {
            //alert("No biztax for assessmentyear " + AY);
            this.getTopToolbar().hide();
            this.add({ xtype: 'panel', title: 'No BizTax module available', html: '<h2>There is no ' + cfg.biztaxtype + ' BizTax module for assessmentyear ' + cfg.AY + '</h2>' });
            this.setActiveTab(0);
            this.biztaxRendered = true;
            this.getEl().unmask();

            return;
        }
        this.getTopToolbar().show();
        this.store = new eBook.BizTax.Store();
        this.fields = {};
        this.contextGrids = [];
        this.fieldsIdx = [];
        this.add(Ext.clone(eBook.BizTax[locator].Items, true));
        this.biztaxRendered = true;
        this.setActiveTab(0);
        this.calcWorker = new Worker(eBook.BizTax[locator].Calculator+"?" + eBook.SpecificVersion); // forex:'js/Taxonomy/BizTax-calc-rcorp-'+AY+'.js');
        this.calcWorker.addEventListener('message', this.onCalculated, false);
        this.calcWorker.addEventListener('error', this.onCalculationError, false);

        this.validWorker = new Worker(eBook.BizTax[locator].Validator+"?" + eBook.SpecificVersion); // forex:'js/Taxonomy/BizTax-validation-rcorp-'+AY+'.js');
        this.validWorker.addEventListener('message', this.onValidated, false);
        this.validWorker.addEventListener('error', this.onValidationError, false);

        var tb = this.getTopToolbar();
        tb.vitals.getEl().mask('validation in progress');

        this.loadData();
    }
    , onCalculated: function(e) {
        var scope = Ext.getCmp(e.data.scopeId); // this
        for (var i = 0; i < e.data.result.length; i++) {
            //txt += ',' + e.data.result[i].name + '=' + e.data.result[i].value;
            scope.store.addOrUpdateElement(e.data.result[i]);
            scope.updateFieldsWithElement(e.data.result[i].id);
        }
        // set last calculated timestamp
        scope.getTopToolbar().infoPane.setCalc(new Date());
        scope.store.lastCalculated = new Date();
        scope.triggerValidation();
        scope.triggerSave();
    }
    , onCalculationError: function(e) {
        Ext.Msg.alert('CALCULATION ERROR', ['NOTIFY EBOOK ADMIN: CALCULATION WORKER ERROR: Line ', e.lineno, ' in ', e.filename, ': ', e.message].join(''));
    }
    , triggerValidation: function() {

        this.getTopToolbar().infoPane.setValidationProgress();
        this.validWorker.postMessage({ store: this.store, scopeId: this.id });
    }
    , onValidated: function(e) {
        var scope = Ext.getCmp(e.data.scopeId); // this
        scope.validationMsgs = e.data.msgs;
        scope.currentFieldMsgs = e.data.fieldMsgs;
        scope.resetFieldValidations(e.data.fieldMsgs);
        scope.processValidationResults(e.data.msgs);


        scope.store.lastValidated = new Date();
        /*scope.triggerSave();*/
    }
    , processValidationResults: function(msgs) {
        var tb = this.getTopToolbar();
        var valid = msgs == null || msgs.length == 0;
        tb.infoPane.setValidation(new Date(), valid);
        if (!valid) {
            tb.validerrors.show();
            tb.vitals.getEl().mask('Validation faults found. Review.', 'biztax-invalid-tb');
        } else {
            tb.validerrors.hide();
            tb.vitals.getEl().unmask();
        }
    }
    , clearFieldValidations: function() {

    }
    , onValidationError: function(e) {
        Ext.Msg.alert('VALIDATION ERROR', ['NOTIFY EBOOK ADMIN: VALIDATION WORKER ERROR: Line ', e.lineno, ' in ', e.filename, ': ', e.message].join(''));
    }
    , loadData: function() {
        // load data into stores
        this.getEl().mask('LOADING DATA');
        this.store.biztaxtype = this.biztaxtype;
        this.store.loadData(this.onDataLoaded, this);
    }
    , onDataLoaded: function() {

        var tb = this.getTopToolbar();
        var ip = tb.infoPane;
        if (this.store.lastCalculated != null) {
            ip.setCalc(this.store.lastCalculated);
        }
        if (this.store.lastSaved != null) {
            ip.setSave(this.store.lastSaved);
        }
        tb.statusPane.setStatus(this.store.status);
        tb.proxyPane.setProxy(this.store.proxyTitle);
        if (this.store.proxyId) {
            tb.addproxy.hide();
        } else {
            tb.addproxy.show();
        }
        tb.unlock.hide();
        tb.lock.show();
        tb.declare.disable();
        if (this.store.status == 1) {
            tb.unlock.show();
            tb.lock.hide();
            if (eBook.User.isActiveClientRolesFullAccess()) { tb.declare.enable(); }
            else { }
        } else if (this.store.status > 1) {
            tb.lock.hide();
        }
        //  this.store.partner 

        this.setFields();
        this.setContextGrids();
        this.triggerValidation();
        this.getEl().unmask(true);
    }
    // milliseconds
    , maxBufferSave: 60 * 1000 // 1 minute
    , triggerSave: function() {
        var dte = new Date();
        if (this.saveTriggerDte != null && this.saveTimer != null && (dte.getTime() - this.saveTriggerDte.getTime()) > this.maxBufferSave) {
            clearTimeout(this.saveTimer);
            this.saveTriggerDte = null;
            this.saveData();
        } else {
            var tb = this.getTopToolbar();
            tb.getEl().mask('waiting to save...');
            if (this.saveTriggerDte == null) this.saveTriggerDte = new Date();
            if (this.saveTimer) clearTimeout(this.saveTimer);
            this.saveTimer = this.saveData.defer(1000, this);
        }
    }
    , changeStatus: function(newStatus, callback, scope) {
        var cb = null;
        if (Ext.isFunction(callback)) {
            cb = { fn: callback, scope: scope };
        }
        this.store.status = newStatus;
        this.saveData(true, cb);
    }
    , executeCalculation: function() {
        this.calcTimer = null;
        this.getTopToolbar().infoPane.setCalcProgress();
        this.calcWorker.postMessage({ store: this.store, scopeId: this.id });
    }
    , saveData: function(reload, extraCallback) {
        var tb = this.getTopToolbar();
        tb.infoPane.setSaveProgress();
        tb.getEl().mask('Saving...');
        this.saveTimer = null;
        this.saveTriggerCnt = 0;
        eBook.CachedAjax.request({
            url: eBook.Service.rule(eBook.Interface.currentFile.get('AssessmentYear')) + 'SaveBizTax'
                , method: 'POST'
                , params: Ext.encode({ cxdc: {
                    FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                        , Xbrl: this.getDataContract()
                }
                })
                , callback: this.onDataSaved
                , scope: this
                , reload: reload
                , extraCallback: extraCallback
        });
    }
    , saveMetaData: function(callback) {
        this.getTopToolbar().infoPane.setSaveProgress();
        this.saveTimer = null;
        this.saveTriggerCnt = 0;
        eBook.CachedAjax.request({
        url: eBook.Service.rule(eBook.Interface.currentFile.get('AssessmentYear')) + 'UpdateBizTaxMetaData'
                , method: 'POST'
                , params: Ext.encode({ xdc: this.getMetaDataContract() })
                , callback: this.onDataSaved
                , scope: this
                , reload: true // always reload
                , extraCallback: callback
        });
    }
    , getDataContract: function() {
        var dta = this.getMetaDataContract();
        dta.Data = this.store.getDataContract();
        return dta;
    }
    , getMetaDataContract: function() {

        return {
            FileId: eBook.Interface.currentFile.get('Id')
            , Type: "BIZTAX-" + this.biztaxtype
            , Calculation: null
            , Validated: false
            , Status: this.store.status
            , Locked: false
            , department: this.store.department
            , partnerId: this.store.partnerId
            , partnerName: this.store.partnerName
            , person: eBook.User.getActivePersonDataContract()
        };
    }
    , onDataSaved: function(opts, success, resp) {
        var tb = this.getTopToolbar();
        tb.getEl().unmask();
        if (success) {
            if (opts.reload) {
                this.loadData();
            } else {
                tb.infoPane.setSave(new Date());
                this.store.lastSaved = new Date();
            }
        } else {
            eBook.Interface.showResponseError(resp, "Saving BizTax");
        }
        if (opts.extraCallback) {
            opts.extraCallback.fn.call(opts.extraCallback.scope, success);
        }
    }
    , onFieldChange: function(fld) {
        this.store.setValueByField(fld);
        this.updateFieldsWithField(fld);
        this.triggerCalculation();

    }
    , triggerCalculation: function() {
        if (this.calcTimer) clearTimeout(this.calcTimer);
        this.calcTimer = this.executeCalculation.defer(500, this);
    }
    , executeCalculation: function() {
        this.calcTimer = null;
        this.getTopToolbar().infoPane.setCalcProgress();
        this.calcWorker.postMessage({ store: this.store, scopeId: this.id });
    }
    , setFieldValueListener: function(fld) {
        var xtype = fld.getXType();
        var listType = xtype.indexOf('check') > -1 ? 'check' : xtype.indexOf('combo') > -1 ? 'select' : 'change';
        fld.addListener(listType, this.onFieldChange, this, { buffer: 300 });
    }
    , registerField: function(fld, fullname, periodcontext, path) {
        if (this.store) {
            if (this.store.status > 0) {
                fld.disable();
            } else {
                if (!fld.initialConfig.disabled) fld.enable();
            }
        }
        fld.xbrlFullname = fullname;

        this.setFieldValueListener(fld);
        if (!this.fields[fld.id]) {
            this.fields[fld.id] = fld;
            this.fieldsIdx.push(fld.id);
        }
        var val = this.store.registerField(fld.id, fullname + '_' + periodcontext, path, fld.xtype);
        if (val != null) {
            fld.suspendEvents();
            fld.setValue(val);
            fld.resumeEvents();
        }
        if (this.currentFieldMsgs && this.currentFieldMsgs[fld.xbrlFullname]) {
            if (fld.markInvalid) fld.markInvalid(this.currentFieldMsgs[fld.xbrlFullname]);
        }
    }
    , registerContextGrid: function(cmpId, targetFields) {
        var idx = this.contextGrids.length;
        this.contextGrids.push({ component: cmpId, target: targetFields[0].xbrlDef.id });
        if (this.store.status > 0) {
            Ext.getCmp(cmpId).disable();
        }
        var ret = null;
        if (this.store.elements && this.store.elements.length > 0) {
            ret = this.store.getContextGridData(targetFields[0].xbrlDef.id);
            if (ret == null && targetFields.length > 1) { // try searching on second field
                ret = this.store.getContextGridData(targetFields[1].xbrlDef.id);
                if (ret != null) {
                    this.contextGrids[idx].target = targetFields[1].xbrlDef.id;
                }
            }
        }

        return ret;
    }
    , setContextGrids: function() {
        for (var i = 0; i < this.contextGrids.length; i++) {
            var grid = Ext.getCmp(this.contextGrids[i].component);
            if (this.store.status > 0) {
                grid.disable();
            } else {
                if (!grid.initialConfig.disabled) grid.enable();
            }
            if (grid && this.store.elements) grid.loadModuleData(this.store.getContextGridData(this.contextGrids[i].target));
        }
    }
    , setFields: function() {
        for (var i = 0; i < this.fieldsIdx.length; i++) {
            var fid = this.fieldsIdx[i];
            var val = this.store.getValueForField(fid, this.fields[fid].xtype);
            this.fieldSetValue(fid, val);
            if (this.store.status > 0) {
                this.fields[fid].disable();
            } else {
                if (!this.fields[fid].initialConfig.disabled) this.fields[fid].enable();
            }

        }
    }
    , resetFieldValidations: function(fieldMsgs) {
        for (var i = 0; i < this.fieldsIdx.length; i++) {
            var fid = this.fieldsIdx[i];
            if (this.fields[fid].clearInvalid) this.fields[fid].clearInvalid();
            //  this.fields[fid].validate();
            if (fieldMsgs && fieldMsgs[this.fields[fid].xbrlFullname]) {
                if (this.fields[fid].markInvalid) this.fields[fid].markInvalid(fieldMsgs[this.fields[fid].xbrlFullname]);
            }
        }
    }
    , updateFieldsWithField: function(fld) {
        var elId = this.store.elementsByFields[fld.id];
        this.updateFieldsWithElement(elId);
    }
    , updateFieldsWithElement: function(elId) {
        var fidxs = this.store.fieldsByElements[elId];
        if (fidxs == null) return;
        for (var i = 0; i < fidxs.length; i++) {
            var xtype = this.fields[fidxs[i]].xtype;
            this.fieldSetValue(fidxs[i], this.store.getElementValueById(elId, xtype));
        }
    }
    , fieldSetValue: function(fid, val) {
        this.fields[fid].suspendEvents();
        this.fields[fid].setValue(val);
        this.fields[fid].resumeEvents();
    }
    //    , updateFieldValue: function(field, val) {

    //    }
    , removeContexts: function(refs) {
        this.store.removeContexts(refs);
    }
    , addContexts: function(contexts) {
        this.store.addContexts(Ext.clone(contexts, true));
    }
});
Ext.reg('biztax-module', eBook.BizTax.Module);



eBook.BizTax.Store = Ext.extend(Object, {
    constructor: function(config) {
        //this.listeners = config.listeners ? config.listeners : config;
        this.elementsByFields = {};
        this.fieldsByElements = {};
    }
    , biztaxGeneral: {}
    , contexts: {}
    , elements: []
    , elementsByPeriod: {}
    , elementsByNamePeriod: {}
    , elementsByContext: {}
    , elementsByNameContext: {}
    , elementsByNamePeriodContext: {} //unique
    , elementsByName: {}
    , elementsByFields: {} // single Element multiple fields -> ID
    , fieldsByElements: {} // multiple fields single Element -> ID
    , registerField: function(fieldId, elementId, path, xtype) {
        var eid = path ? xtype != 'combo' ? path + '/' + elementId : path : elementId;
        if (!this.elementsByFields[fieldId]) this.elementsByFields[fieldId] = eid;
        if (!this.fieldsByElements[elementId]) this.fieldsByElements[elementId] = [];
        this.fieldsByElements[elementId].push(fieldId);
        return this.getElementValueById(elementId, xtype);
    }
    , setValueByField: function(fld) {
        var id = this.elementsByFields[fld.id];
        if (id.indexOf('/') > -1) {
            // structured node
            var xtype = fld.getXType();
            var lst = id.split('/');
            var mi = this.elementsByNamePeriodContext[lst[0]];
            if (!mi) {
                var nmsplit = lst[0].split('_');
                this.addElement(this.createStructuredElement(nmsplit[0], nmsplit[1]));
                mi = this.elementsByNamePeriodContext[lst[0]];
            }
            var midx = mi[0];
            var el = this.elements[midx];
            for (var i = 1; i < lst.length; i++) {
                if (el.Children == null) el.Children = [];
                var nel = this.getElementChildById(el, lst[i]);
                if (nel == null) {
                    if (i < lst.length - 1) {
                        var its = lst[i].split('_');
                        nel = this.createStructuredElement(its[0], its[1]);
                        el.Children.push(nel);

                    } else {
                        if (xtype == 'combo') {
                            var its = lst[i].split('_');
                            nel = this.createStructuredElement(its[0], its[1]);
                            el.Children.push(nel);
                        } else {
                            nel = this.createElementFromField(fld);
                            el.Children.push(nel);
                        }
                    }
                }
                el = nel;
            }
            if (el == null) return null;
            this.updateElement(el, fld.getValue(), xtype, fld);
            return this.retrieveElementValue(el);
        }
        else {

            if (id == null) {
                // NEW CREATION
                this.addElementByField(fld);
                return;
            }
            var idx = this.elementsByNamePeriodContext[id];
            if (idx) {
                this.updateElement(idx[0], fld.getValue(), fld.getXType(), fld);
            } else {
                this.addElementByField(fld);
                return;
            }
        }
    }
    , createElementByCalcCfg: function(cfg) {
        var cref = cfg.period;
        if (cfg.context != null && cfg.context != '') cref += ('__' + cref.context);
        var el = {
            AutoRendered: false
                , Children: null
                , Context: cfg.context
                , ContextRef: cref
                , Decimals: "INF" // to determine?
                , Id: cfg.id
                , Name: cfg.name
                , NameSpace: ""
                , Period: cfg.period
                , Prefix: cfg.prefix
                , UnitRef: "EUR" // to determine?
                , Value: "" + (cfg.value == null ? "" : cfg.value)
        };
        return el;
    }
    , createComboValueElement: function(fld) {

        var val = fld.getValue(); // is id like pfs-vl_XCode_LegalFormCode_001
        if (Ext.isEmpty(val)) return null;
        var splitted = val.split('_');
        var prefix = splitted[0];
        var value = splitted[splitted.length - 1];
        //splitted.splice(splitted.length - 1,1);
        splitted.splice(0, 1);
        var name = splitted.join('_');
        return {
            AutoRendered: false
                , Children: null
                , BinaryValue: null
                , Context: fld.XbrlDef.Context
                , ContextRef: fld.XbrlDef.ContextRef
                , Decimals: fld.XbrlDef.Decimals
                , Id: ''
                , Name: name
                , NameSpace: ""
                , Period: fld.XbrlDef.Period
                , Prefix: prefix
                , UnitRef: fld.XbrlDef.UnitRef
                , Value: value
        };
    }
    , createElementFromField: function(fld) {
        var value = fld.getValue();
        var xtype = fld.getXType();
        var el;
        if (xtype == 'combo') {
            var child = this.createComboValueElement(fld);
            el = {
                AutoRendered: false
                , Children: child ? [child] : []
                , BinaryValue: null
                , Context: null
                , ContextRef: null
                , Decimals: null
                , Id: fld.XbrlDef.Prefix + '_' + fld.XbrlDef.Name
                , Name: fld.XbrlDef.Name
                , NameSpace: ""
                , Period: null
                , Prefix: fld.XbrlDef.Prefix
                , UnitRef: fld.XbrlDef.UnitRef
                , Value: null
            }
        } else {
            el = {
                AutoRendered: false
                , Children: null
                , BinaryValue: xtype == 'biztax-uploadfield' ? Ext.decode(Ext.encode(value)) : null
                , Context: fld.XbrlDef.Context
                , ContextRef: fld.XbrlDef.ContextRef
                , Decimals: fld.XbrlDef.Decimals
                , Id: fld.XbrlDef.Id
                , Name: fld.XbrlDef.Name
                , NameSpace: ""
                , Period: fld.XbrlDef.Period
                , Prefix: fld.XbrlDef.Prefix
                , UnitRef: fld.XbrlDef.UnitRef
                , Value: xtype != 'biztax-uploadfield' ? "" + (value == null ? "" : value) : null
            };

        }
        return el;
    }
    , addElementByField: function(fld) {

        this.addElement(this.createElementFromField(fld));
    }
    , createStructuredElement: function(prefix, name) {
        return {
            AutoRendered: false
            , Children: []
            , Context: ''
            , ContextRef: ''
            , Decimals: null
            , Id: prefix + '_' + name
            , Name: name
            , NameSpace: ""
            , Period: ''
            , Prefix: prefix
            , UnitRef: ''
            , Value: null
        };
    }
    , getValueForField: function(fieldId, xtype) {
        var id = this.elementsByFields[fieldId];
        if (id == null) return null;
        if (id.indexOf('/') > -1) {
            // structured node
            var lst = id.split('/');
            var el = this.getElementById(lst[0]);
            if (el == null) return null;
            for (var i = 1; i < lst.length; i++) {
                el = this.getElementChildById(el, lst[i]);
                if (el == null) return null;
            }
            if (el == null) return null;
            return this.retrieveElementValue(el, xtype);
        }
        else {
            return this.getElementValueById(id, xtype);
        }
    }
    , getElementChildById: function(mainEl, id) {
        if (mainEl.Children == null) return null;
        if (!Ext.isArray(mainEl.Children)) return null;
        for (var j = 0; j < mainEl.Children.length; j++) {
            if (mainEl.Children[j].Id == id) return mainEl.Children[j];
        }
        return null;
    }
    , addContexts: function(contexts) {
        for (var i = 0; i < contexts.length; i++) {
            this.contexts[contexts[i].Id] = contexts[i];
        }
    }
    , createContext: function() { }
    , removeContexts: function(refs) {
        if (!Ext.isDefined(refs)) return;
        if (!Ext.isArray(refs)) refs = [refs];
        if (refs.length == 0) return;
        var args = [this.contexts];
        args = args.concat(refs);
        Ext.destroyMembers(args);
        for (var i = 0; i < refs.length; i++) {
            refs[i] = refs[i].replace('D__', '').replace('I-Start__', '').replace('I-End__', '');
        }
        refs = Ext.unique(refs);
        var els = [];
        for (var i = 0; i < refs.length; i++) {
            var arrs = this.elementsByContext[refs[i]];
            if (arrs != null) els = els.concat(arrs);
        }
        if (els.length == 0) return;

        els = Ext.unique(els).sort(function(a, b) { return b - a; }); // reversed to not affect indexes while removing

        for (var i = 0; i < els.length; i++) {
            this.elements.splice(els[i], 1);
        }
        //alert("reindex");
        this.indexElements();
    }
    , createElement: function() { }
    , getDataContract: function() {
        var dc = {};
        Ext.apply(dc, this.biztaxGeneral);
        dc.Elements = this.elements;
        dc.Contexts = [];
        for (var att in this.contexts) {
            if (!Ext.isFunction(this.contexts[att]) && this.contexts[att].Period) dc.Contexts.push(this.contexts[att]);
        }

        this.lastSaved = new Date();
        dc.LastCalculated = this.lastCalculated;
        dc.lastSaved = this.lastSaved;
        return dc;
    }
    , loadData: function(callback, scope) {
        
        eBook.CachedAjax.request({
            url: eBook.Service.rule(eBook.Interface.currentFile.get('AssessmentYear')) + 'GetBizTax'
            , method: 'POST'
            , params: Ext.encode({ cfdc: {
                FileId: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
                    , Type:this.biztaxtype
            }
            })
            , callback: this.onDataRetreived
            , scope: this
            , callerCallback: { fn: callback, scope: scope }
        });
    }
    , onDataRetreived: function(opts, success, resp) {
        if (!success) {
            alert("failed!");
            opts.callerCallback.fn.call(opts.callerCallback.scope || this);
            return null;
        }
        var robj = Ext.decode(resp.responseText);
        var xdta = robj.GetBizTaxResult;

        var dta = xdta.Data;
        this.status = xdta.Status;
        this.department = xdta.department;
        this.partnerId = xdta.partnerId;
        this.partnerName = xdta.partnerName;

        this.proxyId = dta.ProxyId;
        this.proxyTitle = dta.ProxyTitle;

        this.lastCalculated = dta.LastCalculated != null ? Ext.data.Types.WCFDATE.convert(dta.LastCalculated) : null;
        this.lastSaved = dta.LastSaved != null ? Ext.data.Types.WCFDATE.convert(dta.LastSaved) : null;
        this.partner = dta.Partner;

        this.clearData(); // clear existing data
        // copy global info
        Ext.copyTo(this.biztaxGeneral, dta, ["AssessmentYear", "EntityIdentifier", "Units"]);

        // load in contexts by Id
        this.contexts = {};
        for (var i = 0; i < dta.Contexts.length; i++) {
            this.contexts[dta.Contexts[i].Id] = dta.Contexts[i];
        }

        // load in elements 
        this.elements = dta.Elements;

        // index elements
        this.indexElements();

        opts.callerCallback.fn.call(opts.callerCallback.scope || this);

    }
    , addIdx: function(idxName, id, idx) {
        if (!this[idxName][id]) this[idxName][id] = [];
        this[idxName][id].push(idx);
    }
    , indexElements: function() {
        this.clearElementIndexes();
        for (var i = 0; i < this.elements.length; i++) {
            var el = this.elements[i];
            this.addElementToIndex(el, i);
        }
    }
    , addElement: function(el) {
        if (!this.elementsByNamePeriodContext[el.id]) {
            this.elements.push(el);
            this.addElementToIndex(el, this.elements.length - 1);
            return this.elements.length - 1;
        } else {
            var idx = this.elementsByNamePeriodContext[el.id];
            this.elements[idx] = el;
            return idx;
        }
    }
    , addElementToIndex: function(el, i) {
        var id = el.Id,
            fullName = el.Prefix + '_' + el.Name;

        // unique idx
        // id = fullName + '_' + el.Period + '_' + el.Context;
        this.addIdx('elementsByNamePeriodContext', id, i);

        // non-unique idx's
        this.addIdx('elementsByName', fullName, i);

        id = el.Period;
        this.addIdx('elementsByPeriod', id, i);

        id = fullName + '_' + el.Period;
        this.addIdx('elementsByNamePeriod', id, i);

        id = el.Context;
        this.addIdx('elementsByContext', id, i);

        id = fullName + '_' + el.Context;
        this.addIdx('elementsByNameContext', id, i);
    }
    , clearElementIndexes: function() {
        this.elementsByPeriod = {};
        this.elementsByNamePeriod = {};
        this.elementsByContext = {};
        this.elementsByNameContext = {};
        this.elementsByNamePeriodContext = {}; //unique
        this.elementsByName = {};
    }
    , clearData: function() {
        this.biztaxGeneral = {};
        this.contexts = {};
        this.elements = [];
        this.clearElementIndexes();
        //        this.elementsByFields = {};
        //        this.fieldsByElements = {};
    }
    , getElementsByIndexes: function(idxs) {
        var res = [];
        if (!Ext.isArray(idxs)) idxs = [idxs];
        for (var i = 0; i < idxs.length; i++) {
            res.push(this.elements[idxs[i]]);
        }
        return res;
    }
    , getElementValueById: function(id, xtype) {
        var el = this.getElementById(id);
        return this.retrieveElementValue(el, xtype);
    }
    , retrieveElementValue: function(el, xtype) {
        if (!el) return null;
        if (!Ext.isEmpty(el.Decimals)) {
            return Ext.isEmpty(el.Value) ? 0 : parseFloat(el.Value);
        }
        if (xtype == "biztax-uploadfield") {
            return el.BinaryValue;
        }
        if (xtype == "combo") {
            if (el.Children != null && el.Children.length > 0) {
                return el.Children[0].Prefix + '_' + el.Children[0].Name;
            }
        }
        //  if (el.Value == "true" || el.Value == "false") return eval(el.Value);
        return el.Value;
    }
    , getNumericElementValueById: function(id) {
        var val = this.getElementValueById(id);
        if (val == null) return 0;
        return val;
    }
    , addOrUpdateElement: function(cfg) {
        if (!this.elementsByNamePeriodContext[cfg.id]) {
            this.addElement(this.createElementByCalcCfg(cfg));
        } else {
            this.updateElement(cfg.id, cfg.value, '', null, false); //true  
        }
    }
    , updateElement: function(idxorEl, value, xtype, fld, donotoverwrite) {
        if (Ext.isString(idxorEl)) {
            idxorEl = this.elementsByNamePeriodContext[idxorEl];
            if (idxorEl == null || !Ext.isDefined(idxorEl)) return;
        }
        if (Ext.isObject(idxorEl)) {
            // for child elements.
            if (idxorEl.AutoRendered && donotoverwrite) return;
            if (xtype == "biztax-uploadfield") {
                idxorEl.BinaryValue = Ext.decode(Ext.encode(value));
                idxorEl.Value = null;
            } if (xtype == "combo") {
                var child = this.createComboValueElement(fld);
                idxorEl.Children = child ? [child] : [];
            } else {
                idxorEl.Value = "" + (value == null ? "" : value);
            }
            idxorEl.AutoRendered = false;
            return idxorEl;
        } else {
            if (this.elements[idxorEl].AutoRendered && donotoverwrite) return;
            if (xtype == "biztax-uploadfield") {
                this.elements[idxorEl].BinaryValue = Ext.decode(Ext.encode(value));
                this.elements[idxorEl].Value = null;
            } if (xtype == "combo") {
                var child = this.createComboValueElement(value);
                this.elements[idxorEl].Children = child ? [child] : [];
            } else {
                this.elements[idxorEl].Value = "" + (value == null ? "" : value);
            }
            this.elements[idxorEl].AutoRendered = false;
        }
    }
    , findElementsByIndex: function(idxName, id) {
        if (!this[idxName]) return [];
        if (!this[idxName][id]) return [];
        return this.getElementsByIndexes(this[idxName][id]);
    }
    , getElementById: function(id) {
        var res = this.findElementsForId(id);
        if (res.length == 0) return null;
        return res[0];
    }
    , findElementsForId: function(id) {
        return this.findElementsByIndex('elementsByNamePeriodContext', id);
    }
    , findElementsForName: function(fullname) {
        return this.findElementsByIndex('elementsByName', fullname);
    }
    , findElementsForPeriod: function(period) {
        return this.findElementsByIndex('elementsByPeriod', period);
    }
    , findElementsForNamePeriod: function(fullname, period) {
        return this.findElementsByIndex('elementsByNamePeriod', fullname + '_' + period);
    }
    , findElementsForContext: function(context) {
        return this.findElementsByIndex('elementsByContext', context);
    }
    , findElementsForNameContext: function(fullname, context) {
        return this.findElementsByIndex('elementsByNameContext', fullname + '_' + context);
    }
    , getContextGridData: function(targetField) {
        var target = this.findElementsForName(targetField);
        if (target != null && target.length > 0) {
            var result = [];
            for (var i = 0; i < target.length; i++) {
                var ctx = target[i].Context;
                if (!Ext.isEmpty(ctx)) {
                    var context = this.contexts['D__' + ctx];
                    result.push({
                        elements: this.findElementsForContext(ctx)
                        , scenarios: context.Scenario
                        , context: ctx
                    });
                }
            }

            return result;
        }
        return null;
    }
    , destroy: function() {
        delete this.biztaxGeneral;
        delete this.contexts;
        delete this.elements;
        delete this.elementsByPeriod;
        delete this.elementsByNamePeriod;
        delete this.elementsByContext;
        delete this.elementsByNameContext;
        delete this.elementsByNamePeriodContext;
        delete this.elementsByName;
        delete this.elementsByFields;
        delete this.fieldsByElements;
    }
});


eBook.BizTax.DateField = Ext.extend(Ext.form.DateField, {
});

eBook.BizTax.NumberField = Ext.extend(Ext.form.NumberField, {
    initComponent: function() {
        Ext.apply(this, {
            align: 'right'
        });
        eBook.BizTax.NumberField.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('biztax-numberfield',eBook.BizTax.NumberField);