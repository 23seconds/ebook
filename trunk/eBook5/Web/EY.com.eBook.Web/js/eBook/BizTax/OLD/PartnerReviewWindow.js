﻿
eBook.BizTax.ProxySelection = Ext.extend(Ext.Panel, {
    initComponent: function() {
        //      this.myTpl = new Ext.XTemplate('

        //            '<tpl for="."><div class="ebook-proxyselection-body"><div class="button">repository button</div><div>name repository</div></tpl>");
        Ext.apply(this, {
            html: '<div class="ebook-proxyselection-body"><div class="eBook-proxyselection-title">Drag and drop the signed proxy from the repository.<br/><b>! required !</b></div><div class="eBook-proxyselection-content"><div class="eBook-proxyselection-selected">&nbsp;</div></div></div>'

        });
        eBook.BizTax.ProxySelection.superclass.initComponent.apply(this, arguments);
        // this.on('click', this.onBClick, this);
    }
     , afterRender: function() {
         eBook.BizTax.ProxySelection.superclass.afterRender.apply(this, arguments);
         this.selCont = this.getEl().child('.eBook-proxyselection-content');
         this.selEl = this.getEl().child('.eBook-proxyselection-selected');
         this.dvDDTarget = new Ext.dd.DropZone(this.selCont, {
             ddGroup: 'eBook.Bundle'
                   , srcPanel: this

                    , onContainerOver: function(dd, e, data) {
                        if (!data.node.attributes.IsRepository) {
                            return this.dropNotAllowed;
                        }
                        if (this.overClass) {
                            this.el.addClass(this.overClass);
                        }
                        return this.dropAllowed;
                    }
                    , onContainerDrop: function(ddSource, e, data) {

                        // If node = pdf
                        if (data.node.attributes.IsRepository) {
                            var repItem = data.node.attributes.Item;
                            attributes = {
                                __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'
                                        , type: "PDFPAGE"
                                        , ItemId: repItem.Id
                                        , id: eBook.NewGuid()
                                        , title: repItem.FileName
                                        , indexed: true
                                        , iconCls: data.node.attributes.iconCls
                            };
                            this.srcPanel.setData(attributes);
                            return true;
                        }

                        return false;
                    }
         });
     }
    , setData: function(dta) {
        if (this.dta) {
            this.selEl.removeClass(this.dta.iconCls);
            this.selEl.update("");
        }
        if (dta != null) {
            this.selEl.removeClass("eBook-proxyselection-selected-invalid");
            this.selEl.update(dta.title);
            this.selEl.addClass(dta.iconCls);
            var tb = this.refOwner.getTopToolbar();
            if (eBook.User.isActiveClientRolesFullAccess()) {
                tb.finalBundle.enable();
                tb.mgrOnly.hide();
            } else {
                tb.finalBundle.disable();
                tb.mgrOnly.show();
            }

        } else {
            this.selEl.addClass("eBook-proxyselection-selected-invalid");
            this.refOwner.getTopToolbar().finalBundle.disable();
        }
        this.dta = dta;
    }
    , dta: null
});
Ext.reg('biztax-proxyselection', eBook.BizTax.ProxySelection);



eBook.BizTax.PartnerReviewWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , items: [
                { xtype: 'biztax-proxyselection', title: 'Proxy', ref: 'proxysel', bodyStyle: 'padding:10px', region: 'north', height: 100 }
                , { xtype: 'panel', title: 'Extra information', ref: 'extrainfo', layout: 'border', region: 'center', items: [
                        { xtype: 'biztaxItemSettings', region: 'west', width: 300, ref: '../itemSettings' }
                        , { xtype: 'biztaxUploadEditorPanel', region: 'center', ref: '../uploadEditorPnl', bundle: null} // DATAVIEW INSTEAD OF TREE
                        , { xtype: 'bundlelibrary', region: 'east', width: 300, ref: '../library', culture: eBook.Interface.currentFile.get('Culture'), rootName: 'rootBizTax' }
                ]
                }
                , { xtype: 'panel', title: 'BizTax declaration', region: 'south', height: 100, bodyStyle: 'padding:20px;background-color:#EEE;', html: '<center><h2><font color="#C00"><b>The BizTax declaration is automatically added, including tax calculation!</b></font></h2></center>' }

            ]
            , tbar: [{
                ref: 'saveBundle',
                text: eBook.Bundle.Window_ReportSave,
                iconCls: 'eBook-icon-savereport-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveBundleClick,
                scope: this,
                disabled: eBook.Interface.isFileClosed()
            }, {
                ref: 'previewBundle',
                text: 'Preview', // eBook.Bundle.Window_ReportGenerate,
                iconCls: 'eBook-preview-bundle-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onPreviewClick,
                scope: this
            }
                , {
                    ref: 'finalBundle',
                    text: 'Generate final partner bundle', // eBook.Bundle.Window_ReportGenerate,
                    iconCls: 'eBook-partner-bundle-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onGenerateFinalClick,
                    scope: this,
                    disabled: true
                }
                , { xtype: 'tbtext'
                        ,hidden: true
                        , ref: 'mgrOnly', text: 'Only managers,directors & partners can generate the final review bundle.',style:'font-weight:bold;color:#C00;' }
                ]
            /*, tbar: [{
            ref: 'saveBundle',
            text: eBook.BizTax.Window_ReportSave,
            iconCls: 'eBook-icon-savereport-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onSaveList,
            scope: this
            }]*/
        });
        eBook.BizTax.PartnerReviewWindow.superclass.initComponent.apply(this, arguments);
        if (this.data) {
            this.uploadEditorPnl.setData(this.data);
        }
    }
    , setInactive: function() {
        var tb = this.getTopToolbar();
        tb.saveBundle.disable();
        tb.previewBundle.disable();
        this.proxysel.disable();
        this.extrainfo.disable();
    }
    , onGenerateFinalClick: function() {
        if (this.bundle.Draft == false) {
            Ext.Msg.show({
                title: 'BizTax review by partner',
                msg: 'Generating the final partner-review bundle will freeze the contents of the BizTax module as is. Are you sure you wish to continue?',
                buttons: Ext.Msg.YESNO,
                fn: this.generateFinal,
                icon: Ext.MessageBox.WARNING,
                scope: this
            });
        } else {
            this.generateBundle();
        }
    }
    , generateFinal: function(btnid) {
        if (btnid == 'yes') {
            this.setInactive();
            this.compileBundle();
            this.bundle.Draft = false;
            this.saveBundle('final');
        }
    }
    , onPreviewClick: function() {
        this.compileBundle();
        this.saveBundle('preview');
    }
    , generateBundle: function() {
        this.getEl().mask('Generating');

        this.bundle.RectoVerso = true;
        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'GenerateBundle'
                , method: 'POST'
                , params: Ext.encode({ bdc: this.bundle })
                , callback: this.onGenerated
                , scope: this
        });
    }
    , onGenerated: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GenerateBundleResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onSaveBundleClick: function() {
        this.compileBundle();
        this.saveBundle();
    }
    , saveBundle: function(generate) {
        this.getEl().mask('Saving');

        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'SaveBundle'
                    , method: 'POST'
                    , params: Ext.encode({ bdc: this.bundle })
                    , callback: this.onSaved
                    , scope: this
                    , generate: generate
        });
    }
    , onSaved: function(opts, success, resp) {
        this.getEl().unmask();
        if (!success) {
            eBook.Interface.showResponseError(resp, this.title);
        } else {
            this.loadBundle(Ext.decode(resp.responseText).SaveBundleResult);
            if (opts.generate) {
                switch (opts.generate) {
                    case 'preview':
                        this.generateBundle();
                        break;
                    case 'final':
                        this.module.changeStatus(1, this.continueFinal, this);
                        break;
                }
            }
        }
    }
    , continueFinal: function() {
        this.setInactive();
        this.generateBundle();
    }
    , show: function(module) {
        eBook.BizTax.PartnerReviewWindow.superclass.show.call(this);
        this.module = module;
        
        if (module.store.status > 0) this.setInactive();
        this.onLoadBundle();
    }
    , onLoadBundle: function() {
        this.getEl().mask('Loading');

        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'GetBundleByTemplate'
                        , method: 'POST'
                        , params: Ext.encode({ cfbtdc: { FileId: eBook.Interface.currentFile.get('Id'), TemplateId: '0b21e974-2dcb-4fc4-a442-74af72bc831b'} })
                        , callback: this.onLoaded
                        , scope: this
        });
    }
    , onLoaded: function(opts, success, resp) {

        if (!success) {
        } else {
            var robj = Ext.decode(resp.responseText).GetBundleByTemplateResult;
            this.loadBundle(robj);
        }
        this.getEl().unmask();

    }
    , loadBundle: function(bdl) {
        if (bdl != null) {
            this.bundle = bdl;
            var itemsIdx = 3;
            if (bdl.Contents[2].id == "10000000-0000-0000-0000-000000000000") {
                this.proxysel.setData(bdl.Contents[2]);
            } else {
                itemsIdx--;
                this.proxysel.setData(null);
            }
            this.uploadEditorPnl.setData(bdl.Contents[itemsIdx].items);
        } else {
            this.proxysel.setData(null);
        }
    }
    , compileBundle: function() {
        var proxChapter = this.proxysel.dta;
        if (proxChapter) {
            proxChapter.id = "10000000-0000-0000-0000-000000000000";
            proxChapter.title = "Signed Proxy";
        }
        var exInfoChapter = { "__type": "ChapterDataContract:#EY.com.eBook.API.Contracts.Data", "EndsAt": 0, "FooterConfig": { "Enabled": false, "FootNote": null, "ShowFooterNote": false, "ShowPageNr": false, "Style": null }, "HeaderConfig": { "Enabled": false, "ShowTitle": false, "Style": null }, "NoDraft": false, "StartsAt": 0, "iTextTemplate": null, "iconCls": null, "id": "20000000-0000-0000-0000-000000000000", "indexed": true, "locked": false, "title": "Extra Information", "items": [], "nocoverpage": false, "type": "CHAPTER" };
        exInfoChapter.items = this.uploadEditorPnl.getData();

        var exTaxCalcChapter = { "__type": "WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data", "EndsAt": 0, "FooterConfig": { "Enabled": true, "FootNote": null, "ShowFooterNote": true, "ShowPageNr": true, "Style": null }, "HeaderConfig": { "Enabled": true, "ShowTitle": true, "Style": null }, "NoDraft": false, "StartsAt": 0, "iTextTemplate": null, "iconCls": "eBook-bundle-tree-worksheet", "id": "30000000-0000-0000-0000-000000000000", "indexed": true, "locked": false, "title": "Belasting berekening", "Culture": eBook.Interface.currentFile.get('Culture'), "Data": null, "FileId": eBook.Interface.currentFile.get('Id'), "LastChanged": null, "PrintLayout": "default", "RuleApp": "BerekeningVenBApp", "WorksheetType": "803d6a06-352e-4e28-ade5-214deaac76ca", "type": "WORKSHEET" };
        var exBizTaxChapter = { "__type": "BizTaxBundleDataContract:#EY.com.eBook.API.Contracts.Data", "EndsAt": 0, "FooterConfig": { "Enabled": false, "FootNote": null, "ShowFooterNote": false, "ShowPageNr": false, "Style": null }, "HeaderConfig": { "Enabled": false, "ShowTitle": false, "Style": null }, "NoDraft": false, "StartsAt": 0, "iTextTemplate": null, "iconCls": "eBook-bundle-tree-biztax", "id": "40000000-0000-0000-0000-000000000000", "indexed": true, "locked": false, "title": "BizTax aangifte", "type": "BIZTAX" };

        if (this.bundle) {
            this.bundle.Contents = [];
        } else {
            this.bundle = { "Contents": [], "Culture": eBook.Interface.currentFile.get('Culture'), "Deleted": false, "Draft": true, "FileId": eBook.Interface.currentFile.get('Id'), "HeaderFooterType": "TAX", "Name": "BizTax Partner review", "RectoVerso": true, "TemplateId": "0b21e974-2dcb-4fc4-a442-74af72bc831b" };
        }

        /* TITLE PAGE & INDEX */
        this.bundle.Contents.push({ "__type": "CoverpageDataContract:#EY.com.eBook.API.Contracts.Data", "EndsAt": 0, "FooterConfig": { "Enabled": false, "FootNote": null, "ShowFooterNote": false, "ShowPageNr": false, "Style": null }, "HeaderConfig": { "Enabled": false, "ShowTitle": false, "Style": null }, "NoDraft": false, "StartsAt": 0, "iTextTemplate": null, "iconCls": "eBook-bundle-tree-cover", "id": "fd90d617-184a-5966-f729-d636a14b93ae", "indexed": false, "locked": false, "title": "Titelpagina", "hidden": false, "type": "COVERPAGE" });
        this.bundle.Contents.push({ "__type": "IndexPageDataContract:#EY.com.eBook.API.Contracts.Data", "EndsAt": 0, "FooterConfig": { "Enabled": false, "FootNote": null, "ShowFooterNote": false, "ShowPageNr": false, "Style": null }, "HeaderConfig": { "Enabled": false, "ShowTitle": false, "Style": null }, "NoDraft": false, "StartsAt": 0, "iTextTemplate": null, "iconCls": "eBook-bundle-tree-index", "id": "ce104b7b-fd4d-e99a-07c2-006f94254a3b", "indexed": false, "locked": false, "title": "Indexpagina", "type": "INDEXPAGE" });

        if (proxChapter) this.bundle.Contents.push(proxChapter);
        this.bundle.Contents.push(exInfoChapter);
        this.bundle.Contents.push(exTaxCalcChapter);
        this.bundle.Contents.push(exBizTaxChapter);
        return this.bundle;
    }
    , onSaveList: function() {
    var json = { "allowChildren": false, "allowDrag": true, "cls": "", "iconCls": "eBook-bundle-tree-statement", "id": "activa", "indexItem": { "__type": "StatementsDataContract:#EY.com.eBook.API.Contracts.Data", "EndsAt": 0, "FooterConfig": { "Enabled": true, "FootNote": null, "ShowFooterNote": true, "ShowPageNr": true, "Style": null }, "HeaderConfig": { "Enabled": true, "ShowTitle": true, "Style": null }, "NoDraft": false, "StartsAt": 0, "iTextTemplate": null, "iconCls": null, "id": "efa72689-3502-4bb9-b14e-da020945fd09", "indexed": true, "locked": false, "title": "Activa", "Culture": eBook.Interface.currentFile.get('Culture'), "Detailed": true, "FileId": eBook.Interface.currentFile.get('Id'), "layout": "ACTIVA", "type": "STATEMENT" }, "leaf": true, "text": "Activa" };
        var rt = new this.uploadEditorPnl.store.recordType(json);
        this.uploadEditorPnl.store.add(rt);
        //        alert("save it to parent and close");
    }
    //    , close: function() {
    //        if (this.caller) {
    //            if (this.uploadEditorPnl) this.caller.updateValue(this.uploadEditorPnl.getData());
    //        }
    //        eBook.BizTax.UploadEditorWindow.superclass.close.call(this);
    //    }
    , loadNodeSettings: function(node) {
        this.itemSettings.loadNode(node, closed);
    }
}); 