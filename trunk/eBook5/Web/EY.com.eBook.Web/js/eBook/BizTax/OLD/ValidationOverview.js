
eBook.BizTax.ValidationList = Ext.extend(Ext.DataView, {

    initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.eBook-biztax-validationlist-item'
            , style: 'padding:5px'
            , autoScroll: true
            , bodyStyle: 'background-color:#FFF;'
            , tpl: new Ext.XTemplate('<tpl for=".">'
                            , '<div class="eBook-biztax-validationlist-item">'
                            , '<div class="eBook-biztax-validationlist-field">{fieldName}</div>'
                            , '<div class="eBook-biztax-validationlist-msg">{msg}</div>'
                            , '</div>'
                            , '</tpl>', { compiled: true })
            , overClass: 'x-list-over'
            , data: this.messages
        });
        eBook.BizTax.ValidationList.superclass.initComponent.apply(this, arguments);
      //  this.on('click', this.onMsgClick, this);
    }
    , onMsgClick: function(dv, idx, nde, e) {
      /*  var it = this.messages[idx];
        this.refOwner.jumpToField(it);*/
    }
});

Ext.reg('biztax-validation-overview-list', eBook.BizTax.ValidationList);


eBook.BizTax.ValidationOverview = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            title: 'Biztax validation errors'
            , width:670
            , height:400
            ,bodyStyle:'background-color:#FFF;'
            , layout: 'fit'
            , items: [{ xtype: 'biztax-validation-overview-list', messages: this.messages, ref: 'thelist'}]
        });
        eBook.BizTax.ValidationOverview.superclass.initComponent.apply(this, arguments);
    }
});