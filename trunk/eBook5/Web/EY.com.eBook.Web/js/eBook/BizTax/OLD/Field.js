﻿
/* VERSION 0.3 */
/*eBook.BizTax.FieldLabel = Ext.extend();

eBook.BizTax.FieldItem = Ext.extend();

eBook.BizTax.CodeLabel = Ext.extend();
*/

eBook.BizTax.InstantHeading = Ext.extend(Ext.Container, {
    autoEl: {
        tag: 'div',
        cls: 'biztax-field'
    }
    , initComponent: function() {
        var style = this.totalField ? 'font-weight:bold;' : '';

        var items = [{ xtype: 'spacer', flex: 1 }
                    , { xtype: 'label', text: 'BEGIN', width: 100 }
                    , { xtype: 'label', text: 'END', width: 100}];
        Ext.apply(this, {
            layout: 'hbox'
            , layoutConfig: { defaultMargins: { top: 0, bottom: 0, left: 2, right: 2} }
            , items: items
        });
        eBook.BizTax.InstantHeading.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('biztax-instantheading', eBook.BizTax.InstantHeading);



eBook.BizTax.Field = Ext.extend(Ext.Container, {
    autoEl: {
        tag: 'div',
        cls: 'biztax-field'
    }
    , initComponent: function() {
        var style = this.totalField ? 'font-weight:bold;' : '';

        var items = [{ xtype: 'label', text: this.label, flex: 1, style: style }
                    , { xtype: 'label', text: this.oldCode, width: 50 }
                    , { xtype: 'label', text: this.code, width: 50}];

        var defaultFld = { xtype: 'numberfield' };


        if (this.isList) {
            defaultFld = {
                xtype: 'combo'
                , typeAhead: false
            , forceSelection: true
            , allowBlank: false
            , hideTrigger: false
            , mode: 'local'
            , valueField: 'id'
            , displayField: 'txt'
            , width: 100
            , queryParam: 'Query'
            , queryDelay: 0
            , minChars: 0
            , store: new Ext.data.ArrayStore({
                fields: ['id', 'txt'],
                data: this.data
            })
            };
        } else {
            switch (this.itemType) {
                case "xbrli:booleanItemType":
                    defaultFld.xtype = 'checkbox';
                    //fldType = 'checkbox';
                    break;
                case "xbrli:stringItemType":
                    //fldType = 'textfield';
                    defaultFld.xtype = 'textfield';
                    defaultFld.flex = 1;
                    break;
                default:
                    defaultFld.width = 100;
                    break;

            }
        }

        var st = {}, ed = {};

        if (!this.isAbstract || this.isList) {
            switch (this.periodType) {
                case "customdimension":
                case "duration":
                    Ext.apply(defaultFld, { ref: 'endField', contextRef: 'D' });
                    items.push(defaultFld);
                    break;
                case "instant":
                    st = Ext.apply(st, defaultFld);
                    ed = Ext.apply(ed, defaultFld);
                    st = Ext.apply(st, { ref: 'startField', contextRef: 'I-Start' });
                    ed = Ext.apply(ed, { ref: 'endField', contextRef: 'I-End' });
                    items.push(st);
                    items.push(ed);
                    break;
                case "instant-end":
                    items.push({ xtype: 'spacer', width: 100 });
                    ed = Ext.apply(ed, defaultFld);
                    ed = Ext.apply(ed, { ref: 'endField', contextRef: 'I-End' });
                    items.push(ed);
                    break;
                case "instant-start":
                    st = Ext.apply(st, defaultFld);
                    st = Ext.apply(st, { ref: 'startField', contextRef: 'I-Start' });
                    items.push(st);
                    items.push({ xtype: 'spacer', width: 100 });
                    break;
            }
        }
        else {
            items.push({ xtype: 'spacer', width: 200 });
        }

        Ext.apply(this, {
            layout: 'hbox'
            , layoutConfig: { defaultMargins: { top: 0, bottom: 0, left: 2, right: 2} }
            , items: items
        });
        eBook.BizTax.Field.superclass.initComponent.apply(this, arguments);

//        var mod = this.getModule();
//        if (this.startField) {
//            mod.fields[this.startField.xbrlId + this.startField.contextRef] = this.startField;
//        }
//        if (this.endField) {
//            mod.fields[this.endField.xbrlId + this.endField.contextRef] = this.endField;
//        }
    }
    , getModule: function() {
        return this.findParentByType('biztax-module');
    }
});
        



/* VERSION 0.2

eBook.BizTax.Field = Ext.extend(Ext.BoxComponent, {
    autoEl: {
        tag: 'div',
        cls: 'biztax-field'
        ,
        children: [
            { tag: 'div', cls: 'biztax-field-label' }
           , { tag: 'div', cls: 'biztax-field-code-1' }
           , { tag: 'div', cls: 'biztax-field-code-2' }
           , { tag: 'div', cls: 'biztax-field-container' }
        ]
    }
    , onRender: function(ct, position) {
        if (!this.el) {
            this.el = Ext.DomHelper.createDom(this.autoEl);
        }
        eBook.BizTax.Field.superclass.onRender.call(this, ct, position);
        this.labelEl = this.el.child('.biztax-field-label');
        this.code1El = this.el.child('.biztax-field-code-1');
        this.code2El = this.el.child('.biztax-field-code-2');
        this.fldContainerEl = this.el.child('.biztax-field-container');
        this.labelEl.update(this.label);
        this.code1El.update(this.oldCode);
        this.code2El.update(this.code);

        switch (this.periodType) {
            case "duration":
                this.endField = this.createField('biztax-field-duration');
                this.fldContainerEl.appendChild(this.endField);
                break;
            case "instant":
                this.startField =this.createField('biztax-field-instant-start');
                this.fldContainerEl.appendChild(this.startField);
                this.endField = this.createField('biztax-field-instant-end');
                this.fldContainerEl.appendChild(this.endField);
                break;
        }
    }
    , createField: function(cls) {
        return Ext.DomHelper.createDom({ tag: 'div', cls: cls, children: [{ tag: 'input', type: 'text'}] });
    }
});
*/
/* VERSION 0.1
eBook.BizTax.Field = Ext.extend(Ext.form.TextField, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'biztax-field'
        });
        eBook.BizTax.Field.superclass.initComponent.apply(this, arguments);
    }
});


*/

Ext.reg('biztax-field', eBook.BizTax.Field);

eBook.BizTax.ListField = Ext.extend(eBook.BizTax.Field, {
    initComponent: function() {
        this.isList = true;
        eBook.BizTax.ListField.superclass.initComponent.apply(this, arguments);
    }
});


Ext.reg('biztax-list', eBook.BizTax.ListField);