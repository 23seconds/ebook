

eBook.BizTax.Tab = Ext.extend(Ext.Panel, {
    initComponent: function() {

        Ext.apply(this, {
            autoScroll: true,
            padding: 15,
            title: '<span style="color:#900;">' + this.code + '. </span>' + this.title,
            cls:'biztax-tab-code-'+this.code
        });

        eBook.BizTax.Tab.superclass.initComponent.apply(this, arguments);
        if (this.elementsDisabledFn) this.elementsDisabled = this.elementsDisabledFn();
        this.elementsDisabled = this.elementsDisabled ? true : false;
    }
    ,getModule:function() {
        return this.ownerCt;
    }
});
Ext.reg('biztax-tab', eBook.BizTax.Tab);
