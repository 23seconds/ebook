﻿
self.addEventListener('message', function(e) {
    var data = e.data.data;
    var res = { result: data.elements.length + ' elements, and ' + data.contexts.length + ' context', scopeId: e.data.scopeId };
    self.postMessage(res);
}, false);