
Ext.empytObject = function(cfg) { };

// TODO Context definition if needed
/*
{ xtype:'biztax-pane'
      
,title:'Identificatiegegevens van de onderneming'
,code:'Identificatiegegevens van de onderneming'
,documentation:'Identificatiegegevens van de onderneming'
,multipleContexts:false
,viewTplType:'panesOnly'
    
, xbrlDef: {
id:'pfs-gcd_EntityInformation'
, name:'EntityInformation'
, itemType:''
}
,isAbstract:false
,items:[]
}
*/

/*
mainTpl:
<div class="biztax-pane biztax-pane-tpl-{viewTplType}">
</div>

*/

eBook.BizTax.Trans_DURATION = 'Belastbaar tijdperk';
eBook.BizTax.Trans_ISTART = 'Bij het begin van het belastbare tijdperk';
eBook.BizTax.Trans_IEND = 'Op het einde van het belastbare tijdperk';

eBook.BizTax.Templates = {
    panesOnly: {
        title: '<div class="biztax-pane-title biztax-pane-notcollapsable"><div class="biztax-pane-title-txt">{title}</div></div>'
        //, heading: ''
        , body: new Ext.XTemplate('<tpl for="."><div class="biztax-pane-body biztax-pane-body-onlyPanes"></div></tpl>', { compiled: true })
        , bodyLines: '<div class="biztax-pane-line" id="{id}">{item}<div class="biztax-pane-line">'
        , label: '<div class="biztax-pane-line"><div class="biztax-label" ><div>{title}</div></div></div>'
    }
    , binary: {
        title: '<div class="biztax-pane-title biztax-pane-collapsable"><div class="biztax-pane-title-txt">{title}</div><div class="biztax-pane-collapseToggle"></div></div>'
        // , heading: '<div class="biztax-pane-heading"><div class="biztax-pane-header biztax-pane-header-label">&nbsp;</div><div class="biztax-pane-header biztax-pane-header-code">OLDCODE</div><div class="biztax-pane-header biztax-pane-header-code">CODE</div><div class="biztax-pane-header biztax-pane-header-field">{[eBook.BizTax.Trans_ISTART]}</div><div class="biztax-pane-header biztax-pane-header-field">I-End</div><div class="x-clear"></div></div>'
        // , body: '<table class="biztax-pane-body biztax-pane-body-onlyPanes" border="0" width="100%" cellpadding="0" cellspacing="0" ></table>'
        , bodyLines: '<tr class="biztax-pane-line" id="{id}"><td class="biztax-line-item"><div>{title}</div><input type="text" class="biztax-item-field-{xtype}" period="D" context="" contextRef="D" xbrlId="{[ values[\'xbrlDef\'] ? values.xbrlDef.id : \'\' ]}" /></td></tr>'
        , paneTitle: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-title"><div>{title}</div></td></tr>'
        , label: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-colspan biztax-label"><div>{title}</div></td></tr>'
        , body: new Ext.XTemplate('<tpl for="."><table  border="0" width="100%" cellpadding="0" cellspacing="0" class="biztax-pivot-body"><tbody></tbody></table></tpl>', { compiled: true })
        , header: new Ext.XTemplate('<tpl for="."></tpl>', { compiled: true })
    }
    , instant: {
        title: '<div class="biztax-pane-title biztax-pane-collapsable"><div class="biztax-pane-title-txt">{title}</div><div class="biztax-pane-collapseToggle"></div></div>'
        // , heading: '<div class="biztax-pane-heading"><div class="biztax-pane-header biztax-pane-header-label">&nbsp;</div><div class="biztax-pane-header biztax-pane-header-code">OLDCODE</div><div class="biztax-pane-header biztax-pane-header-code">CODE</div><div class="biztax-pane-header biztax-pane-header-field">{[eBook.BizTax.Trans_ISTART]}</div><div class="biztax-pane-header biztax-pane-header-field">I-End</div><div class="x-clear"></div></div>'
        // , body: '<table class="biztax-pane-body biztax-pane-body-onlyPanes" border="0" width="100%" cellpadding="0" cellspacing="0" ></table>'
        , bodyLines: '<tr class="biztax-pane-line{[values[\'calculation\'] ? \' biztax-calculated-field\' : \'\' ]}" id="{id}"><td class="biztax-line-item biztax-item-label"><div>{title}</div></td><td class="biztax-line-item biztax-item-code"><tpl if="values.oldCode!=values.code">{oldCode}</tpl></td><td class="biztax-line-item biztax-item-code">{code}</td><tpl if="this.isDuration(values)"><td class="biztax-line-item biztax-item-field biztax-item-field-instant">&nbsp;</td><td class="biztax-line-item biztax-item-field biztax-item-field-instant" id="D-{id}"><input type="text" class="biztax-item-field-{xtype}" period="D" context="" contextRef="D" xbrlId="{[ values[\'xbrlDef\'] ? values.xbrlDef.id : \'\' ]}" /> </td></tpl><tpl if="!this.isDuration(values)"><tpl if="this.hasPeriod(values,\'I-Start\')"><td class="biztax-line-item biztax-item-field biztax-item-field-instant" id="I-Start-{id}"><input type="text" class="biztax-line-item biztax-item-field-{xtype}"  period="I-Start" context="" contextRef="I-Start" xbrlId="{[ values[\'xbrlDef\'] ? values.xbrlDef.id : \'\' ]}" /> </td></tpl><tpl if="!this.hasPeriod(values,\'I-Start\')"><td class="biztax-line-item biztax-item-field biztax-item-field-instant" id="I-Start-{id}"></td></tpl><tpl if="this.hasPeriod(values,\'I-End\')"><td class="biztax-line-item biztax-item-field biztax-item-field-instant" id="I-End-{id}"><input type="text" class="biztax-item-field-{xtype}" period="I-End" context="" contextRef="I-End" xbrlId="{[ values[\'xbrlDef\'] ? values.xbrlDef.id : \'\' ]}" /> </td></tpl><tpl if="!this.hasPeriod(values,\'I-End\')"><td class="biztax-line-item biztax-item-field biztax-item-field-instant" id="I-End-{id}"> </td></tpl></tpl></tr>'
        , paneTitle: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-title" colspan="5"><div>{title}</div></td></tr>'
        , label: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-colspan biztax-label" colspan="5"><div>{title}</div></td></tr>'
        , pivotBody: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-colspan" colspan="5"></td></tr>'
        , body: new Ext.XTemplate('<tpl for="."><table  border="0" width="100%" cellpadding="0" cellspacing="0" class="biztax-pivot-body"><thead><tr class="biztax-pane-heading"><th class="biztax-pane-header biztax-pane-header-label">&nbsp;</th><th class="biztax-pane-header biztax-pane-header-code" colspan="2" width="140px">Codes</th><th class="biztax-pane-header biztax-pane-header-field" width="150px">{[eBook.BizTax.Trans_ISTART]}</th><th class="biztax-pane-header biztax-pane-header-field" width="150px">{[eBook.BizTax.Trans_IEND]}</th></tr></thead><tbody></tbody></table></tpl>', { compiled: true })
        , header: new Ext.XTemplate('<tpl for="."><tr class="biztax-pane-heading"><th class="biztax-pane-header biztax-pane-header-label">&nbsp;</th><th class="biztax-pane-header biztax-pane-header-code" colspan="2" width="140px">Codes</th><th class="biztax-pane-header biztax-pane-header-field" width="150px">{[eBook.BizTax.Trans_ISTART]}</th><th class="biztax-pane-header biztax-pane-header-field" width="150px">{[eBook.BizTax.Trans_IEND]}</th></tr></tpl>', { compiled: true })
    }
    , duration: {
        title: '<div class="biztax-pane-title biztax-pane-collapsable"><div class="biztax-pane-title-txt">{title}</div><div class="biztax-pane-collapseToggle"></div></div>'
        //, heading: '<div class="biztax-pane-heading"><div class="biztax-pane-header biztax-pane-header-label">&nbsp;</div><div class="biztax-pane-header biztax-pane-header-code">OLDCODE</div><div class="biztax-pane-header biztax-pane-header-code">CODE</div><div class="biztax-pane-header biztax-pane-header-field">{[eBook.BizTax.Trans_DURATION]}</div><div class="x-clear"></div></div>'
        //, body: '<table class="biztax-pane-body biztax-pane-body-onlyPanes" border="0" width="100%" cellpadding="0" cellspacing="0"></table>'
        , bodyLines: '<tr class="biztax-pane-line{[values[\'calculation\'] ? \' biztax-calculated-field\' : \'\' ]}" id="{id}"><td class="biztax-line-item biztax-item-label"><div>{title}</div></td><td class="biztax-line-item biztax-item-code"><tpl if="values.oldCode!=values.code">{oldCode}</tpl></td><td class="biztax-line-item biztax-item-code">{code}</td><td class="biztax-line-item biztax-item-field biztax-item-field-duration" id="D-{id}"><input type="text" class="biztax-item-field-{xtype}" period="D" context="" contextRef="D" xbrlId="{[ values[\'xbrlDef\'] ? values.xbrlDef.id : \'\' ]}" /> </td></tr>'
        , paneTitle: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-title" colspan="4"><div>{title}</div></td></tr>'
        , label: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-colspan biztax-label" colspan="4"><div>{title}</div></td></tr>'
        , pivotBody: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-colspan" colspan="4"></td></tr>'
        , body: new Ext.XTemplate('<tpl for="."><table  border="0" width="100%" cellpadding="0" cellspacing="0" class="biztax-pivot-body"><thead><tr class="biztax-pane-heading"><th class="biztax-pane-header biztax-pane-header-label">&nbsp;</th><th class="biztax-pane-header biztax-pane-header-code" colspan="2" width="140px">Codes</th><th class="biztax-pane-header biztax-pane-header-field" width="200px">{[eBook.BizTax.Trans_DURATION]}</th></tr></thead><tbody></tbody></table></tpl>', { compiled: true })
        , header: new Ext.XTemplate('<tpl for="."><tr class="biztax-pane-heading"><th class="biztax-pane-header biztax-pane-header-label">&nbsp;</th><th class="biztax-pane-header biztax-pane-header-code" colspan="2" width="140px">Codes</th><th class="biztax-pane-header biztax-pane-header-field" width="200px">{[eBook.BizTax.Trans_DURATION]}</th></tr></tpl>', { compiled: true })
    }
    , pivot: {
        title: '<div class="biztax-pane-title biztax-pane-collapsable"><div class="biztax-pane-title-txt">{title}</div><div class="biztax-pane-collapseToggle"></div></div>'
        , body: new Ext.XTemplate('<tpl for="."><table  border="0" width="100%" cellpadding="0" cellspacing="0" class="biztax-pivot-body"><thead><tr class="biztax-pane-heading biztax-pane-pivot-heading"><th class="biztax-pane-header biztax-pane-header-label">&nbsp;</th><th class="biztax-pane-header biztax-pane-header-code" colspan="2" width="140px">Codes</th><tpl for="contextHeaders"><th class="biztax-pane-header biztax-pane-header-field" width="150px">{name}</th></tpl></tr></thead><tbody></tbody></table></tpl>', { compiled: true })
        , bodyLines: '<tr class="biztax-pane-line{[values[\'calculation\'] ? \' biztax-calculated-field\' : \'\' ]}" id="{id}"><td class="biztax-line-item biztax-item-label"><div>{title}</div></td><td class="biztax-line-item biztax-item-code"><tpl if="values.oldCode!=values.code">{oldCode}</tpl></td><td class="biztax-line-item biztax-item-code">{code}</td>[FIELDS]</tr>'
        , fieldBody: '<td class="biztax-line-item biztax-item-field biztax-item-field-instant" id="[CONTEXT]-{id}"><tpl if="this.hasContext(values,\'[CONTEXT]\')"><input type="text" class="biztax-item-field-{xtype}" period="D" context="[CONTEXT_NAME]" contextRef="{[this.getContextRef(values,\'[CONTEXT_NAME]\')]}" xbrlId="{[ values[\'xbrlDef\'] ? values.xbrlDef.id : \'\' ]}"/></tpl><tpl if="!this.hasContext(values,\'[CONTEXT]\')">&nbsp;</tpl></td>'
        , paneTitle: '<tr class="biztax-pane-line"><td class="biztax-line-item biztax-item-title" colspan="[COLSPAN]"><div>{title}</div></td></tr>'

    }
};






eBook.BizTax.Pane = Ext.extend(Ext.BoxComponent, {
    autoEl: {
        tag: 'div'
        , cls: 'biztax-pane'
    }
    , initComponent: function() {
        var its = this.items;
        if (this.isBinaryPane) this.viewTplType = 'binary';
        Ext.apply(this, {
            padding: 5
            //layout:'form',
            , autoEl: {
                tag: 'div'
                 , cls: 'biztax-pane biztax-pane-tpl-' + this.viewTplType
            }
            , items: its
        });
        eBook.BizTax.Pane.superclass.initComponent.apply(this, arguments);

        this.activeTplCfg = eBook.BizTax.Templates[this.viewTplType];
    }
    , onRender: function(container, position) {
        eBook.BizTax.Pane.superclass.onRender.apply(this, arguments);
        if (this.elementsDisabledFn) this.elementsDisabled = this.elementsDisabledFn();
        this.elementsDisabled = this.elementsDisabled || this.ownerCt.elementsDisabled ? true : false;
        if (this.activeTplCfg) {
            this.tplCfg = {
                templates: {
                    title: new Ext.XTemplate('<tpl for=".">' + this.activeTplCfg.title + '</tpl>')
                    , body: this.activeTplCfg.body
                    , label: new Ext.XTemplate('<tpl for=".">' + this.activeTplCfg.label + '</tpl>')
                    , paneTitle: new Ext.XTemplate('<tpl for=".">' + this.activeTplCfg.paneTitle + '</tpl>')
                    , pivotBody: new Ext.XTemplate('<tpl for=".">' + this.activeTplCfg.pivotBody + '</tpl>')
                    , header: this.activeTplCfg.header
                    , bodyLines: new Ext.XTemplate('<tpl for=".">' + this.activeTplCfg.bodyLines + '</tpl>',
                    {
                        isDuration: function(values) {
                            if (values.periods.length == 0) return true;
                            return this.hasPeriod(values, 'D');
                        }
                        , hasPeriod: function(values, per) {
                            var ps = values.periods;
                            for (var i = 0; i < ps.length; i++) {
                                if (ps[i] == per) return true;
                            }
                            return false;
                        }

                    })
                }
            };
            this.titleWrap = this.tplCfg.templates.title.append(this.el, this, true);
            //if (!this.parentTpl) this.headingWrap = this.headingTpl.append(this.el, this, true);
            this.mainWrap = this.tplCfg.templates.body.append(this.el, this, true);


            if (!this.parentTpl && this.viewTplType != "panesOnly") {
                this.parentTpl = this;
            }
            this.bodyWrap = this.mainWrap.child('tbody');
            if (this.bodyWrap == null) this.bodyWrap = this.mainWrap;
            //render items
            //render body


            if (!this.isAbstract) {
                // structured node
                if (this.ownerCt.tplCfg) {
                    if (this.ownerCt.tplCfg.path == null) {
                        this.tplCfg.path = this.xbrlDef.id;
                    } else {
                        this.tplCfg.path = this.ownerCt.tplCfg.path + '/' + this.xbrlDef.id;
                    }
                } else {
                    this.tplCfg.path = this.xbrlDef.id;
                }
            }


            if (this.items) {
                if (this.viewTplType == "panesOnly") {
                    for (var i = 0; i < this.items.length; i++) {
                        this.items[i] = this.lookupComponent(this.items[i]);
                        this.items[i].ownerCt = this;
                        this.items[i].render(this.bodyWrap, i);
                    }
                } else {
                    this.renderItems(this.bodyWrap, this.items, 0, this.tplCfg, this.elementsDisabled);
                }
            }
        }
    }
    , destroy: function() {

        Ext.destroy(this.titleWrap);
        Ext.destroy(this.mainWrap);
        Ext.destroy(this.bodyWrap);
        Ext.destroy(this.items);
        eBook.BizTax.Pane.superclass.destroy.call(this);
    }
    , renderContextGrid: function(wrapper, items, index, cfg) {
        items[index] = this.lookupComponent(items[index]);
        items[index].ownerCt = this;
        var el = cfg.templates.paneTitle.append(wrapper, { title: '' }, true);
        el = el.child('div');
        items[index].render(el);
    }
    , renderPane: function(wrapper, items, i, indentation, cfg, mod, elementsDisabled) {
        items[i] = this.lookupComponent(items[i]);
        var subCfg = Ext.clone(cfg);
        if (items[i].items) {
            // render title
            if (items[i].fieldInfo) {
                items[i].fieldInfo.calculation = function() { return 0; };
                var el = cfg.templates.bodyLines.append(wrapper, items[i].fieldInfo, true);
                el.setStyle('font-weight', 'bold');
                el.child('input').parent().setStyle('background-color', '#F0F000');
                if (indentation > 0) {
                    if (el.first() && el.first().first()) {
                        el.first().first().setStyle('padding-left', indentation + 'px');
                    }
                }
                this.renderFieldTypes(el, el.query('input'), items[i].fieldInfo, mod, cfg.path);
            } else {
                var el = cfg.templates.paneTitle.append(wrapper, items[i], true);
                if (indentation > 0) {
                    el.first().first().setStyle('padding-left', indentation + 'px');
                }
                if (!items[i].isAbstract) {
                    // structured node
                    if (subCfg.path == null) {
                        subCfg.path = items[i].xbrlDef.id;
                    } else {
                        subCfg.path += '/' + items[i].xbrlDef.id;
                    }
                }
            }
            if (!elementsDisabled && items[i].elementsDisabled) elementsDisabled = true;
            // indentation += 10;
            //this.paneTitleTpl

            this.renderItems(wrapper, items[i].items, indentation + 20, subCfg, elementsDisabled);
            //indentation -= 10;
        }
    }
    , renderListOrOtherPane: function(wrapper, items, index, indentation, cfg, elementsDisabled) {
        items[index] = this.lookupComponent(items[index]);
        items[index].ownerCt = this;
        //var el = cfg.templates.paneTitle.append(wrapper, { title: '' }, true);
        //el = el.child('div');
        items[index].renderSub(wrapper, indentation, Ext.clone(cfg), elementsDisabled);
    }
    , renderPivot: function(wrapper, items, i, indentation, cfg, mod, elementsDisabled, j) {
        items[i] = this.lookupComponent(items[i]);
        if (items[i].items) {
            // render title
            var pivotCfg = {
                colspan: 3 + items[i].contextHeaders.length
                                , isPivot: true
                                , templates: {}
                                , contextHeaders: items[i].contextHeaders
                                , defaultContext: items[i].defaultContextId
            };
            var defctx = "";
            if (!Ext.isEmpty(pivotCfg.defaultContext)) {
                defctx = pivotCfg.defaultContext
            }
            //TODO LABEL TEMPLATE
            pivotCfg.templates.paneTitle = new Ext.XTemplate('<tpl for=".">' + eBook.BizTax.Templates.pivot.paneTitle.replace(/\[COLSPAN\]/g, pivotCfg.colspan) + '</tpl>');

            var flds = '';
            for (var h = 0; h < pivotCfg.contextHeaders.length; h++) {
                flds += eBook.BizTax.Templates.pivot.fieldBody.replace(/\[CONTEXT\]/g, pivotCfg.contextHeaders[h].id).replace(/\[CONTEXT_NAME\]/g, defctx + pivotCfg.contextHeaders[h].id.split('_')[1]);
            }

            pivotCfg.templates.bodyLines = new Ext.XTemplate('<tpl for=".">' + eBook.BizTax.Templates.pivot.bodyLines.replace(/\[FIELDS\]/g, flds) + '</tpl>'
                                , {
                                    compiled: true
                                    , hasContext: function(values, ctx) {
                                        for (var i = 0; i < values.contexts.length; i++) {
                                            if (values.contexts[i] == ctx) return true;
                                        }
                                        return false;
                                    }
                                    , isDuration: function(values) {
                                        if (values.periods.length == 0) return true;
                                        return this.hasPeriod(values, 'D');
                                    }
                                    , hasPeriod: function(values, per) {
                                        var ps = values.periods;
                                        for (var i = 0; i < ps.length; i++) {
                                            if (ps[i] == per) return true;
                                        }
                                        return false;
                                    }
                                    , getContextRef: function(values, ctx) {
                                        var per = this.isDuration(values) ? 'D_' : 'UNKNOWN-I_';
                                        return per + '_' + ctx;
                                    }
                                });

            var el = cfg.templates.paneTitle.append(wrapper, items[i], true);
            var bdy = cfg.templates.pivotBody.append(wrapper, items[i], true);
            bdy = bdy.child('td');
            var tbl = eBook.BizTax.Templates.pivot.body.append(bdy, items[i], true);
            var tbody = tbl.child('tbody');
            if (!elementsDisabled && items[i].elementsDisabled) elementsDisabled = true;

            this.renderItems(tbody, items[i].items, 0, pivotCfg, elementsDisabled);
            if (j < items.length) {
                cfg.templates.header.append(wrapper, items[i], true);
            }
            //indentation -= 10;
        }
    }
    , renderField: function(wrapper, items, i, indentation, cfg, mod, elementsDisabled) {
        var it = items[i];
        var el = cfg.templates.bodyLines.append(wrapper, it, true);
        this.renderFieldTypes(el, el.query('input'), it, mod, cfg.path, elementsDisabled);
        if (indentation > 0) {
            if (el.first() && el.first().first()) {
                el.first().first().setStyle('padding-left', indentation + 'px');
            }
        }
    }
    , renderList: function(wrapper, items, i, indentation, cfg, mod, elementsDisabled) {
        var it = items[i];
        it.fieldType = { xtype: 'combo', mode: 'local', valueField: 'id', displayField: 'text', store: { xtype: 'jsonstore', autoDestroy: true, data: it.data, idProperty: 'id', fields: ['id', 'name', 'fixed', 'text']} };
        var el = cfg.templates.bodyLines.append(wrapper, it, true);
        this.renderFieldTypes(el, el.query('input'), it, mod, cfg.path, elementsDisabled);
        if (indentation > 0) {
            if (el.first() && el.first().first()) {
                el.first().first().setStyle('padding-left', indentation + 'px');
            }
        }
    }
    , renderLabel: function(wrapper, items, i, indentation, cfg, mod, elementsDisabled) {
        var it = items[i];
        var el = cfg.templates.label.append(wrapper, it, true);
        if (indentation > 0) {
            if (el.first() && el.first().first()) {
                el.first().first().setStyle('padding-left', indentation + 'px');
            }
        }
    }
    , renderItems: function(wrapper, items, indentation, cfg, elementsDisabled) {
        var j = 0;
        var mod = this.getModule();
        for (var i = 0; i < items.length; i++) {
            if (!items[i].xtype && items[i].tpe) items[i].xtype = items[i].tpe;
            items[i].id = Ext.id();
            var it = items[i];
            if (it.xtype == 'biztax-pane-pivot' && cfg.isPivot) it.xtpye = 'biztax-pane';
            switch (it.xtype) {
                case 'biztax-listorother':
                    this.renderListOrOtherPane(wrapper, items, i, indentation, cfg, elementsDisabled);
                    //(wrapper, items, i, cfg);
                    j++;
                    break;
                case 'biztax-contextgrid':
                    //                    this.items[i] = this.lookupComponent(this.items[i]);
                    //                    this.items[i].ownerCt = this;
                    //                    var el = cfg.templates.paneTitle.append(wrapper, { title: '' }, true);
                    //                    el = el.child('div');
                    //                    this.items[i].render(el);
                    this.renderContextGrid(wrapper, items, i, cfg);
                    break;
                case 'biztax-pane':
                    this.renderPane(wrapper, items, i, indentation, cfg, mod, elementsDisabled);
                    j++;
                    break;
                case 'biztax-pane-pivot':
                    this.renderPivot(wrapper, items, i, indentation, cfg, mod, elementsDisabled, j);
                    j++;
                    break;
                case 'biztax-label':
                    this.renderLabel(wrapper, items, i, indentation, cfg, mod, elementsDisabled);
                    j++;
                    break;
                case 'biztax-field':

                    this.renderField(wrapper, items, i, indentation, cfg, mod, elementsDisabled);
                    j++;
                    break;
                case 'biztax-list':
                    this.renderList(wrapper, items, i, indentation, cfg, mod, elementsDisabled);
                    j++;
                    break;
                case 'colspan': //??
                    j++;
                    break;
            }
        }
    }
    , getModule: function() {
        var moduleEl = this.getEl().findParent('.eb-biztax-tab');
        return Ext.getCmp(moduleEl.id);
    }
    , renderFieldTypes: function(el, inputEls, fieldInfo, mod, path, elementsDisabled) {
        var titleel = el.child('.biztax-item-label');
        if (titleel) {
            titleel = el.child('div');
        }
        if (titleel && fieldInfo.fieldType.maxValue == 0 && fieldInfo.fieldType.minValue < 0) {
            titleel.update(titleel.dom.innerText + ' (-)');
        } else if (titleel && fieldInfo.fieldType.minValue < 0 && fieldInfo.fieldType.maxValue > 0) {
            titleel.update(titleel.dom.innerText + ' (+)/(-)');
        }
        for (var i = 0; i < inputEls.length; i++) {
            if (fieldInfo.fieldType) {
                if (fieldInfo.fieldType.xtype == 'checkbox') {
                    inputEls[i].setAttribute('type', 'checkbox');
                }
                fieldInfo.fieldType.applyTo = inputEls[i];
                fieldInfo.fieldType.width = "90%";
                if (fieldInfo.calculation || elementsDisabled) {
                    fieldInfo.fieldType.disabled = true;
                }
                var fld = Ext.create(fieldInfo.fieldType);


                fld.XbrlDef = {
                    Context: inputEls[i].getAttribute('context')
                    , ContextRef: inputEls[i].getAttribute('contextref')
                    , Decimals: fieldInfo.decimalAttribute
                    , Id: ''
                    , Name: fieldInfo.xbrlDef.name
                    , Period: inputEls[i].getAttribute('period')
                    , Prefix: fieldInfo.xbrlDef.id.replace('_' + fieldInfo.xbrlDef.name, '')
                    , UnitRef: fieldInfo.unitAttribute
                };
                if (Ext.isEmpty(fld.XbrlDef.ContextRef)) {
                    fld.XbrlDef.Id = fieldInfo.xbrlDef.id;
                } else {
                    fld.XbrlDef.Id = fieldInfo.xbrlDef.id + '_' + fld.XbrlDef.ContextRef;
                }
                mod.registerField(fld, inputEls[i].getAttribute('xbrlid'), inputEls[i].getAttribute('contextref'), path);
            }
        }
    }
    , lookupComponent: function(comp) {
        if (Ext.isString(comp)) {
            return Ext.ComponentMgr.get(comp);
        } else if (!comp.events) {
            return this.createComponent(comp);
        }
        return comp;
    }
    , createComponent: function(config, defaultType) {
        if (config.render) {
            return config;
        }
        // add in ownerCt at creation time but then immediately
        // remove so that onBeforeAdd can handle it
        var c = Ext.create(Ext.apply({
            ownerCt: this
        }, config), defaultType || this.defaultType);
        delete c.initialConfig.ownerCt;
        delete c.ownerCt;
        return c;
    }
});

Ext.reg('biztax-pane', eBook.BizTax.Pane);

eBook.BizTax.PivotPane = Ext.extend(eBook.BizTax.Pane, {
    initComponent: function() {
        eBook.BizTax.PivotPane.superclass.initComponent.apply(this, arguments);
        this.activeTplCfg = eBook.BizTax.Templates.pivot;
    }
    , onRender: function(container, position) {
        eBook.BizTax.Pane.superclass.onRender.apply(this, arguments);
        if (this.activeTplCfg) {
            this.tplCfg = {
                colspan: 3 + this.contextHeaders.length
                            , isPivot: true
                            , templates: {
                                title: new Ext.XTemplate('<tpl for=".">' + this.activeTplCfg.title + '</tpl>')
                                , body: eBook.BizTax.Templates.pivot.body
                            }
                            , contextHeaders: this.contextHeaders
            };

            this.tplCfg.templates.paneTitle = new Ext.XTemplate('<tpl for=".">' + eBook.BizTax.Templates.pivot.paneTitle.replace(/\[COLSPAN\]/g, this.tplCfg.colspan) + '</tpl>');

            var flds = '';
            for (var h = 0; h < this.contextHeaders.length; h++) {
                flds += eBook.BizTax.Templates.pivot.fieldBody.replace(/\[CONTEXT\]/g, this.contextHeaders[h].id).replace(/\[CONTEXT_NAME\]/g, this.contextHeaders[h].id.split('_')[1]);
            }

            this.tplCfg.templates.bodyLines = new Ext.XTemplate('<tpl for=".">' + eBook.BizTax.Templates.pivot.bodyLines.replace(/\[FIELDS\]/g, flds) + '</tpl>'
                , {
                    compiled: true
                    , hasContext: function(values, ctx) {
                        for (var i = 0; i < values.contexts.length; i++) {
                            if (values.contexts[i] == ctx) return true;
                        }
                        return false;
                    }
                    , isDuration: function(values) {
                        if (values.periods.length == 0) return true;
                        return this.hasPeriod(values, 'D');
                    }
                    , hasPeriod: function(values, per) {
                        var ps = values.periods;
                        for (var i = 0; i < ps.length; i++) {
                            if (ps[i] == per) return true;
                        }
                        return false;
                    }
                    , getContextRef: function(values, ctx) {
                        var per = this.isDuration(values) ? 'D_' : 'UNKNOWN-I_';
                        return per + '_' + ctx;
                    }
                });

            this.titleWrap = this.tplCfg.templates.title.append(this.el, this, true);
            this.mainWrap = this.tplCfg.templates.body.append(this.el, this, true);

            this.bodyWrap = this.mainWrap.child('tbody');



            if (this.items) {
                this.renderItems(this.bodyWrap, this.items, 0, this.tplCfg);
            }
        }
    }
});

Ext.reg('biztax-pane-pivot', eBook.BizTax.PivotPane);



eBook.BizTax.ListOrOtherPane = Ext.extend(eBook.BizTax.Pane, {
    onRender: function(container, position) {
        this.items = [this.valueList, this.otherField];

        eBook.BizTax.ListOrOtherPane.superclass.onRender.apply(this, arguments);
    }
    , renderSub: function(wrapper, indentation, cfg, elementsDisabled) {
        this.items = [this.valueList, this.otherField];
        this.el = wrapper;
        var el = cfg.templates.paneTitle.append(wrapper, this, true);
        if (indentation > 0) {
            el.first().first().setStyle('padding-left', indentation + 'px');
        }
        if (!this.isAbstract) {
            // structured node
            if (cfg.path == null) {
                cfg.path = this.xbrlDef.id;
            } else {
                cfg.path += '/' + this.xbrlDef.id;
            }
        }

        if (!elementsDisabled && this.elementsDisabled) elementsDisabled = true;
        // indentation += 10;
        //this.paneTitleTpl
        cfg.noOther = false;
        this.renderItems(wrapper, this.items, indentation + 20, cfg, elementsDisabled);
        this.maskElements(this.others, true);
    }
    , renderPane: function(wrapper, items, i, indentation, cfg, mod, elementsDisabled) {
        if (this.others == null) this.others = [];
        items[i] = this.lookupComponent(items[i]);
        var subCfg = Ext.clone(cfg);
        if (items[i].items) {
            // render title

            var el = cfg.templates.paneTitle.append(wrapper, items[i], true);
            if (indentation > 0) {
                el.first().first().setStyle('padding-left', indentation + 'px');
            }
            if (el.first() && el.first().first()) {
                this.otherCheck = el.first().first().insertFirst({ tag: 'input', type: 'radio', name: this.id, value: 'OTHER', itype: 'paneother' });
                if (!this.elementsDisabled) this.mon(this.otherCheck, { click: this.onCheckOther, scope: this });
                if (this.elementsDisabled) this.otherCheck.dom.disabled = true;
            }
            if (!items[i].isAbstract) {
                // structured node
                if (subCfg.path == null) {
                    subCfg.path = items[i].xbrlDef.id;
                } else {
                    subCfg.path += '/' + items[i].xbrlDef.id;
                }
            }

            if (!elementsDisabled && items[i].elementsDisabled) elementsDisabled = true;
            // indentation += 10;
            //this.paneTitleTpl
            subCfg.noOther = true;
            this.renderItems(wrapper, items[i].items, indentation + 20, subCfg, elementsDisabled);
            //indentation -= 10;
        }
    }
    , renderField: function(wrapper, items, i, indentation, cfg, mod, elementsDisabled) {
        if (this.others == null) this.others = [];
        var it = items[i];
        var el = cfg.templates.bodyLines.append(wrapper, it, true);
        this.renderFieldTypes(el, el.query('input'), it, mod, cfg.path, elementsDisabled);
        if (indentation > 0) {
            if (el.first() && el.first().first()) {
                el.first().first().setStyle('padding-left', indentation + 'px');
            }
        }
        if (el.first() && el.first().first()) {
            this.others.push(el.query('input'));
            if (!cfg.noOther) {
                this.otherCheck = el.first().first().insertFirst({ tag: 'input', type: 'radio', name: this.id, value: 'OTHER', itype: 'singleother' });
                if (this.elementsDisabled) this.otherCheck.dom.disabled=true;
                if (!this.elementsDisabled) this.mon(this.otherCheck, { click: this.onCheckOther, scope: this });
            }
        }
    }
    , maskElements: function(els, mask) {
        if (!Ext.isDefined(mask)) mask = true;
        if (!Ext.isDefined(els)) return;
        if (!Ext.isArray(els)) els = [els];
        for (var i = 0; i < els.length; i++) {
            if (!Ext.isArray(els[i])) els[i] = [els[i]];
            for (var j = 0; j < els[i].length; j++) {
                //if (els[i][j].parentNode.tagName == 'div') els[i][j] = els[i][j].parentNode;
                if (mask) {
                    Ext.get(els[i][j]).parent('td').child('*').hide();
                } else {
                    Ext.get(els[i][j]).parent('td').child('*').show();
                }
            }
        }
    }
    , renderList: function(wrapper, items, i, indentation, cfg, mod, elementsDisabled) {
        if (this.lists == null) this.lists = [];
        var it = items[i];
        it.fieldType = { xtype: 'combo', triggerAction: 'all', forceSelection: true, mode: 'local', valueField: 'id', displayField: 'text', store: { xtype: 'jsonstore', autoDestroy: true, data: it.data, idProperty: 'id', fields: ['id', 'name', 'fixed', 'text']} };
        if (it.data.length > 30) {
            it.fieldType.typeAhead = true;
            it.fieldType.clearFilterOnReset = true;
        }
        var el = cfg.templates.bodyLines.append(wrapper, it, true);
        this.renderFieldTypes(el, el.query('input'), it, mod, cfg.path, elementsDisabled);
        if (indentation > 0) {
            if (el.first() && el.first().first()) {
                el.first().first().setStyle('padding-left', indentation + 'px');
            }
        }
        if (el.first() && el.first().first()) {
            this.lists.push(el.query('input'));
            this.list = el.first().first().insertFirst({ tag: 'input', type: 'radio', name: this.id, value: 'LIST', checked: true });
            if (this.elementsDisabled) this.list.dom.disabled = true;
            if (!this.elementsDisabled) this.mon(this.list, { click: this.onCheckList, scope: this });
        }
    }
    , onCheckList: function() {
        this.maskElements(this.lists, false);
        this.maskElements(this.others, true);
    }
    , onCheckOther: function() {
        this.maskElements(this.lists, true);
        this.maskElements(this.others, false);
    }
});
Ext.reg('biztax-listorother', eBook.BizTax.ListOrOtherPane);


eBook.BizTax.ContextGrid = Ext.extend(Ext.BoxComponent, {
    initComponent: function() {
        this.lineTpl = new Ext.XTemplate(this.xtplLine, { compiled: true });
        this.bodyTpl = new Ext.XTemplate(this.xtplMain);
        this.store = [];
        this.selectedLineIdx = -1;
        this.enableEvents = true;
        Ext.apply(this, {
            autoEl: {
                tag: 'div'
                , cls: 'biztax-contentpane'
                , children: [
                        { tag: 'div', cls: 'biztax-contentpane-title', html: this.title }
                        , { tag: 'div', cls: 'biztax-contentpane-body', html: this.bodyTpl.apply(this) }
                        , { tag: 'div', cls: 'biztax-contentpane-buttons',
                            children: [
                            //{ tag: 'div', cls: 'biztax-contentpane-update', html: '' }
                            //, 
                                {tag: 'div', cls: 'biztax-contentpane-button biztax-contentpane-add', html: 'Toevoegen' }
                                , { tag: 'div', cls: 'biztax-contentpane-button biztax-contentpane-remove', html: 'Verwijderen' }

                            ]
                        }
                    ]
            }
        });
        eBook.BizTax.ContextGrid.superclass.initComponent.apply(this, arguments);
    }
    , destroy: function() {
        //this.removeAll(true);
        if (this.listBody) Ext.destroy(this.listBody);
        delete this.listBody;
        Ext.destroy(this.store);
        eBook.BizTax.ContextGrid.superclass.destroy.call(this);
    }
    , enable: function() {
        this.isEnabled = !this.elementsDisabled;
        if (!this.elementsDisabled) {
            this.buttonPane.show();
            //this.isEnabled = true;
        }
    }
    , disable: function() {
        this.buttonPane.hide();
        this.isEnabled = false;

    }
    , onRender: function(container, position) {
        eBook.BizTax.ContextGrid.superclass.onRender.apply(this, arguments);
        if (this.elementsDisabledFn) this.elementsDisabled = this.elementsDisabledFn();
        this.elementsDisabled = this.elementsDisabled || this.ownerCt.elementsDisabled ? true : false;
        if (!this.initialConfig) this.initialConfig = {};
        if (!this.elementsDisabled) this.initialConfig.disabled = true;
        this.isEnabled = !this.elementsDisabled;
        this.listBody = this.el.child('tbody');
        this.buttonPane = this.el.child('.biztax-contentpane-buttons')
        if (this.elementsDisabled) this.buttonPane.hide();
        this.buttonPane.on('click', this.onButtonAction, this);
        //        var tr = document.createElement('tr');
        //        this.listBody.appendChild(tr)
        //        this.bodyTpl.overwrite(tr,this);

        this.fieldTypes = [];


        // var line = 'this.lineTemplate  = function() {   this.fields=[];this.contextRefs=[];';


        var line = { fields: [], contextRefs: [] };
        var idx = this.store.length;
        var i = 0;
        for (i = 0; i < this.contextFields.length; i++) {
            var finf = this.contextFields[i];
            finf.DimensionId = finf.Dimension.split('#')[1];
            //line += ' this["' + finf.DimensionId + '"] = { value:\'\',fieldIdx:' + i + '};';
            line[finf.DimensionId] = { value: '', fieldIdx: i };

            if (finf.fieldType) {
                finf.fieldType.allowBlank = false;
                finf.fieldType.gridFieldId = finf.DimensionId;

                finf.fieldType.width = '90%'
                line.fields.push(Ext.clone(finf.fieldType));
                // this.fieldTypes.push(finf.fieldType);
                //    this.line += ' this.fields.push(' + Ext.encode(finf.fieldType) + ');'
                //this.lineTemplate.prototype.fields.push(finf.fieldType);
                // set field events
            } else {
                //inp.value = "UNKNOWN FIELDTYPE";
            }
        }

        for (var j = 0; j < this.lineFields.length; j++) {
            var finf = this.lineFields[j];
            for (var pi = 0; pi < finf.periods.length; pi++) {
                var period = finf.periods[pi];
                var ftype = Ext.decode(Ext.encode(finf.fieldType));
                ftype.gridFieldId = finf.xbrlDef.id + period;
                //line += 'this["' + ftype.gridFieldId + '"] =  { value:\'\',fieldIdx:' + i + '};';
                line[ftype.gridFieldId] = { value: '', fieldIdx: i };

                if (ftype) {
                    ftype.xbrlperiod = period;
                    ftype.xbrlId = finf.xbrlDef.id;
                    //ftype.listeners = this.getFieldListeners(ftype.xtype);
                    ftype.width = '90%'
                    //this.fieldTypes.push(ftype);
                    line.fields.push(Ext.clone(ftype));
                    //  this.line += ' this.fields.push(' + Ext.encode(finf.fieldType) + ');'
                    // set field events
                } else {
                    inp.value = "UNKNOWN FIELDTYPE";
                }
                i++;
            }
        }

        this.lineTemplate = function() {
            Ext.apply(this, Ext.clone(this.lineCfg));
        };
        this.lineTemplate.prototype.lineCfg = line;

        var dta = this.getModule().registerContextGrid(this.id, this.lineFields); //[0].xbrlDef.id
        if (dta) this.loadModuleData(dta);

    }
    //    , reload: function() {
    //        this.removeAll();
    //        var dta = this.getModule().registerContextGrid(this.id, this.lineFields[0].xbrlDef.id);
    //        if (dta) this.loadModuleData(dta);
    //    }
    , loadModuleData: function(dta) {
        this.enableEvents = false;
        try {
            this.removeAll();

            if (dta != null) {
                //ctxs = Ext.pluck(dta, 'context');
                var its = {}
               // dta = Ext.unique(dta);
                for (var i = 0; i < dta.length; i++) {
                    var dtaBlock = dta[i];
                    var ctx = dtaBlock.ctx ? dtaBlock.ctx : dtaBlock.context;
                    if (!its[ctx]) {
                        var line = new this.lineTemplate();
                        var idx = i;


                        for (var j = 0; j < dtaBlock.scenarios.length; j++) {
                            var scen = dtaBlock.scenarios[j];

                            var propName = scen.Dimension.replace(':', '_');
                            line[propName].value = scen.Name == 'explicitMember' ? scen.Value.replace(':', '_') : scen.Value;
                        }
                        for (var j = 0; j < dtaBlock.elements.length; j++) {
                            var el = dtaBlock.elements[j];
                            var propName = el.Prefix + '_' + el.Name + el.Period;
                            line[propName].value = el.Value;
                        }
                        line.contextRefs = ['D__' + ctx, 'I-Start__' + ctx, 'I-End__' + ctx]
                        this.addLine(line, i);
                        its[ctx] = true;
                    }
                }
            }
        } catch (e) { }
        this.enableEvents = true;

    }
    , onButtonAction: function(e, el, o) {
        var btn = e.getTarget('.biztax-contentpane-button');
        if (btn) {
            btn = Ext.get(btn);
            if (btn.hasClass('biztax-contentpane-add')) {
                this.addNewLine();
            } else if (btn.hasClass('biztax-contentpane-remove')) {
                if (this.selectedLineIdx > -1) {
                    this.clearDeselector();
                    Ext.Msg.show({
                        title: 'Lijn verwijderen',
                        msg: 'Bent u zeker dat u de geselecteerde lijn wenst te verwijderen?',
                        buttons: Ext.Msg.YESNO,
                        fn: this.sureToRemove,
                        icon: Ext.MessageBox.QUESTION,
                        scope: this
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Selecteer lijn',
                        msg: 'U dient eerst een lijn te selecteren. (klik in een veld)',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING,
                        scope: this
                    });
                }
            }
        }
    }
    , sureToRemove: function(btnid) {
        if (btnid == 'yes') {
            this.removeLine();
            this.getModule().triggerCalculation();
        } else {
            this.deselector = this.deselectLine.defer(500, this, [this.selectedLineIdx]);
        }
    }
    , getContextField: function(dimension) {
        for (var i = 0; i < this.contextFields.length; i++) {
            if (this.contextFields[i].Dimension == dimension) return this.contextFields[i];
        }
        return null;
    }
    , getLineField: function(xbrlId) {
        for (var i = 0; i < this.lineFields.length; i++) {
            if (this.lineFields[i].xbrlDef.id == xbrlId) return this.lineFields[i];
        }
        return null;
    }
    , selectedLineIdx: -1
    , addNewLine: function() {
        var line = new this.lineTemplate();
        //  Ext.apply(line, this.lineTemplate);
        var idx = this.store.length;
        this.addLine(line, idx);
    }
    , addLine: function(line, idx) {
        line = this.renderLine(idx, line);
        this.store.push(line);
        if (this.enableEvents) line.fields[0].focus();
    }
    , removeAll: function(destroy) {
        for (var i = this.store.length - 1; i > -1; i--) {
            this.selectedLineIdx = i;
            this.removeLine(destroy);
        }
        this.selectedLineIdx = -1;

    }
    , renderLine: function(idx, line) {
        var lel = this.lineTpl.append(this.listBody, this, true);
        if (lel == null) lel = this.listBody.last();
        var inputs = lel.query('input');

        for (var i = 0; i < inputs.length; i++) {
            var inp = inputs[i];
            inp.setAttribute('gridFieldId', line.fields[i].gridFieldId);
            inp.setAttribute('gridIndex', idx);

            var ftype = line.fields[i];  // {};
            //Ext.apply(ftype, this.fieldTypes[i]);


            if (ftype) {
                ftype.gridIndex = idx;
                ftype.listeners = this.getFieldListeners(ftype.xtype);
                ftype.applyTo = inp;
                ftype.disabled = this.isEnabled == false;
                line.fields[i] = Ext.create(ftype);
                if (line[ftype.gridFieldId].value) {
                    line.fields[i].setValue(line[ftype.gridFieldId].value);
                }
                // set field events
            } else {
                inp.value = "UNKNOWN FIELDTYPE";
            }
        }
        return line;
    }
    , clearDeselector: function() {
        if (this.deselector) {
            clearTimeout(this.deselector);
            this.deselector = null;
        }
    }
    , removeLine: function(destroy) {

        this.clearDeselector();
        if (this.selectedLineIdx > -1) {
            this.el.mask('removing');
            var flds = this.store[this.selectedLineIdx].fields;
            var tr = this.listBody.child('tr:nth-child(' + (this.selectedLineIdx + 1) + ')');
            for (var i = 0; i < flds.length; i++) {
                flds[i].destroy();
            }
            tr.remove();
            for (var i = this.selectedLineIdx + 1; i < this.store.length; i++) {
                for (var j = 0; j < this.store[i].fields.length; j++) {
                    this.store[i].fields[j].gridIndex = this.store[i].fields[j].gridIndex - 1;
                    this.store[i].fields[j].el.dom.setAttribute('gridIndex', this.store[i].fields[j].gridIndex);
                }
            }
            var contextRefs = this.store[this.selectedLineIdx].contextRefs;

            //delete items in module data store
            if (this.enableEvents) this.getModule().removeContexts(this.store[this.selectedLineIdx].contextRefs);
            this.store.splice(this.selectedLineIdx, 1);
            this.selectedLineIdx = -1;
            this.el.unmask();
        }

        // reindex fields
    }
    , getFieldListeners: function(xtype) {
        var lst = {
            focus: { fn: this.onFieldFocus, scope: this }
            , blur: { fn: this.onFieldBlur, scope: this }
        };
        var change = 'change';
        switch (xtype) {
            case 'check':
                change = 'check';
                break;
            case 'combo':
                change = 'select';
                break;
        }
        lst[change] = { fn: this.onFieldChange, scope: this };
        return lst;
    }
    , onFieldFocus: function(fld) {
        //this.selectLine.defer(100,this, [fld.gridIndex]);
        if (this.enableEvents) this.selectLine(fld.gridIndex);
    }
    , onFieldBlur: function(fld) {
        if (this.enableEvents) this.deselector = this.deselectLine.defer(500, this, [fld.gridIndex]);

        // this.updateTask = this.updateLine(fld.gridIndex
    }
    , onFieldChange: function(fld) {
        //changed field value
    }
    , deselectLine: function(idx) {
        this.deselector = null;
        //this.buttonPane.first().update("update it - " + idx);

        if (!Ext.isDefined(idx)) idx = this.selectedLineIdx;
        if (idx > -1) {
            this.updateLine(idx);
            var tr = this.listBody.child('tr:nth-child(' + (idx + 1) + ')');
            tr.removeClass('biztax-context-selectedline');
        }
        this.selectedLineIdx = -1;
    }
    , selectLine: function(idx) {
        var tr;
        this.clearDeselector();
        if (this.selectedLineIdx == idx) return;
        if (this.selectedLineIdx > -1) this.deselectLine(this.selectedLineIdx);
        this.selectedLineIdx = idx;
        if (this.selectedLineIdx > -1) {
            tr = this.listBody.child('tr:nth-child(' + (this.selectedLineIdx + 1) + ')');
            tr.addClass('biztax-context-selectedline');
        }
    }
    , updateLine: function(idx) {
        if (!this.enableEvents) return;
        var isValid = true;
        var tr = this.listBody.child('tr:nth-child(' + (idx + 1) + ')');
        for (var i = 0; i < this.store[idx].fields.length; i++) {
            isValid = isValid && this.store[idx].fields[i].validate();
        }
        if (!isValid) {
            tr.addClass('biztax-context-invalidline');
        } else {
            tr.removeClass('biztax-context-invalidline');
            var module = this.getModule();

            module.removeContexts(this.store[idx].contextRefs);

            var result = this.createContexts(module, this.store[idx]);
            module.addContexts(result.contexts);

            this.createElements(module, result.contextRefs, this.store[idx]);
            this.store[idx].contextRefs = result.contextRefs;
            module.triggerCalculation();

        }
    }
    , cleanerRegEx: /[^a-zA-Z0-9\-]/gi
    , createContexts: function(module, line) {
        var result = { contexts: [], contextRefs: [] };
        var scenario = [];
        var mainId = '';
        var ctxLen = this.contextFields.length;
        for (var i = 0; i < ctxLen; i++) {
            var cdef = this.contextFields[i];
            var fld = line.fields[i];
            // if (fld.isValid()) { ?? -> already done by updateline
            var scen = { Dimension: cdef.Dimension.split('#')[1].replace('_', ':') };
            var idVal = '';
            if (cdef.DimensionType == "ExplicitDimension") {
                scen.__type = 'ContextScenarioExplicitDataContract:#EY.com.eBook.API.Contracts.Data';
                scen.Value = fld.getValue();
                scen.Value = scen.Value.replace('_', ':');
                scen.Name = "explicitMember";
                idVal = scen.Value.split(':')[1];
            } else if (cdef.DimensionType == "TypedDimension") {
                scen.__type = 'ContextScenarioTypeDataContract:#EY.com.eBook.API.Contracts.Data';
                scen.Value = fld.getValue();
                if (Ext.isDate(fld.getValue())) {
                    scen.Value = fld.getValue().format('Y-m-d');
                }
                scen.Name = "typedMember";
                var typePrefix = cdef.Dimension.split('#')[1].split('_')[0];
                scen.Type = typePrefix + ':' + cdef.TypeId;
                idVal = scen.Value.replace(this.cleanerRegEx, '');
            }
            mainId += '__id__' + idVal;
            scenario.push(scen);
        }
        result.contextRefs = ['D' + mainId, 'I-Start' + mainId, 'I-End' + mainId];

        var ctxD = Ext.clone(module.store.contexts.D, true);
        var ctxStart = Ext.clone(module.store.contexts['I-Start'], true);
        var ctxEnd = Ext.clone(module.store.contexts['I-End'], true);

        ctxD.Scenario = scenario;
        ctxD.Id = 'D' + mainId;

        ctxStart.Scenario = scenario;
        ctxStart.Id = 'I-Start' + mainId;

        ctxEnd.Scenario = scenario;
        ctxEnd.Id = 'I-End' + mainId;

        result.contexts = [ctxD, ctxStart, ctxEnd];
        return result;
    }
    , getModule: function() {
        var moduleEl = this.getEl().findParent('.eb-biztax-tab');
        return Ext.getCmp(moduleEl.id);
    }
    , createElements: function(module, contextRefs, line) {
        var baseRef = contextRefs[0].replace(/^(D__)/, '');
        var startI = this.contextFields.length;
        for (var i = startI; i < line.fields.length; i++) {
            var fld = line.fields[i];
            var fdef = this.getLineField(fld.xbrlId);

            fld.XbrlDef = {
                Context: baseRef
                , ContextRef: fld.xbrlperiod + '__' + baseRef
                , Decimals: fdef.decimalAttribute
                , Name: fdef.xbrlDef.name
                , NameSpace: ""
                , Period: fld.xbrlperiod
                , Prefix: fdef.xbrlDef.id.split('_')[0]
                , UnitRef: fdef.unitAttribute
            };
            fld.XbrlDef.Id = fdef.xbrlDef.id + '_' + fld.XbrlDef.ContextRef
            module.store.addElementByField(fld);
        }
    }
});
Ext.reg('biztax-contextgrid', eBook.BizTax.ContextGrid);


//Ext.reg('biztax-contextgrid', eBook.BizTax.ContextGrid);


eBook.BizTax.Field = Ext.extend(Ext.BoxComponent, {
    initComponent: function() {
        Ext.apply(this, { html: this.title });
        eBook.BizTax.Field.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('biztax-field', eBook.BizTax.Field);
Ext.reg('biztax-list', eBook.BizTax.Field);

eBook.BizTax.Label = Ext.extend(Ext.BoxComponent, {
    initComponent: function() {
        Ext.apply(this, {
            autoEl: {
                tag: 'div'
                , cls: 'biztax-pane-line'
                , children: [
                        { tag: 'div'
                        , cls: 'biztax-label'
                        , html: this.title
                        }
                    ]
            }
        });
        eBook.BizTax.Label.superclass.initComponent.apply(this, arguments);
    }

});



Ext.reg('biztax-label', eBook.BizTax.Label);
    