﻿/*
eBook.BizTax.Grid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        Ext.apply(this, {
            store: '' // to be defined or distinct in/from the definition file (fields)
            , columns: [] // to be defined or distinct in/from the definition file
        });
        eBook.BizTax.Grid.superclass.initComponent.apply(this, arguments);
    }
});
*/


eBook.BizTax.Grid = Ext.extend(Ext.Panel, {
    hasInstantPeriods: false
    , initComponent: function() {
        Ext.apply(this, {
            padding: 5,
            title:'GRID',
            //layout:'form',
            cls: 'biztax-pane'
        });
        eBook.BizTax.Grid.superclass.initComponent.apply(this, arguments);
    }

});

Ext.reg('biztax-grid', eBook.BizTax.Grid);


    


// + context of concept's dimensions