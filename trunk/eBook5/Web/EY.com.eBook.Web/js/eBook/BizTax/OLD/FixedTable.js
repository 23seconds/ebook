﻿


eBook.BizTax.FixedTable = Ext.extend(Ext.Panel, {
    hasInstantPeriods: false
    , initComponent: function() {
        Ext.apply(this, {
            padding: 5,
            title: 'FIXED TABLE',
            //layout:'form',
            cls: 'biztax-pane'
        });
        eBook.BizTax.FixedTable.superclass.initComponent.apply(this, arguments);
    }

});

Ext.reg('biztax-fixedtable', eBook.BizTax.FixedTable);