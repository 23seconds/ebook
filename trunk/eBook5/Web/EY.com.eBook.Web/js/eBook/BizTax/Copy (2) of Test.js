﻿
// FIND OUT WHICH (rcorp/nrcorp/rle)

eBook.BizTax.AY = { 
  xtype:'biztax-module'
  , targetCorp:'rcorp'
  ,items:[
    
  ] 
};

eBook.BizTax.AY.ref = 'module';

eBook.BizTax.AY_Items = [
    { xtype: 'biztax-tab'
      , title: 'Ondernemingsgegevens'
      , code: 'Id'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Identificatiegegevens van de onderneming'
    , code: ''
    , documentation: 'Identificatiegegevens van de onderneming'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_EntityInformation'
      , name: 'EntityInformation'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Ondernemingsnummer'
    , code: ''
    , documentation: 'Identificerend nummer van de onderneming'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityIdentifier'
      , name: 'EntityIdentifier'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Type nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IdentifierName'
      , name: 'IdentifierName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Opgave nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IdentifierValue'
      , name: 'IdentifierValue'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Naam'
    , code: ''
    , documentation: 'Naam'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityName'
      , name: 'EntityName'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Wettelijke benaming'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_EntityCurrentLegalName'
      , name: 'EntityCurrentLegalName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-listorother'

    , title: 'Rechtsvorm'
    , code: ''
    , documentation: 'Rechtsvorm'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityForm'
      , name: 'EntityForm'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_LegalFormCodeHead'
      , name: 'LegalFormCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_LegalFormOther'
      , name: 'LegalFormOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Adres'
    , code: ''
    , documentation: 'Adres van de onderneming'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityAddress'
      , name: 'EntityAddress'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-listorother'

    , title: 'Aard adres'
    , code: ''
    , documentation: 'Aard adres'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_AddressType'
      , name: 'AddressType'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_AddressTypeCodeHead'
      , name: 'AddressTypeCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_AddressTypeOther'
      , name: 'AddressTypeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Straat'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Street'
      , name: 'Street'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nr'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Number'
      , name: 'Number'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bus'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Box'
      , name: 'Box'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Postcode en gemeente'
    , code: ''
    , documentation: 'Postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCity'
      , name: 'PostalCodeCity'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_PostalCodeHead'
      , name: 'PostalCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere'
    , code: ''
    , documentation: 'Andere postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCityOther'
      , name: 'PostalCodeCityOther'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeOther'
      , name: 'PostalCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemeente'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_City'
      , name: 'City'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-listorother'

    , title: 'Land'
    , code: ''
    , documentation: 'Landcode'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_CountryCode'
      , name: 'CountryCode'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_CountryCodeHead'
      , name: 'CountryCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryCodeOther'
      , name: 'CountryCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Bankinformatie'
    , code: ''
    , documentation: 'Bankinformatie'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_BankInformation'
      , name: 'BankInformation'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'BIC'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_BankIdentifierCode'
      , name: 'BankIdentifierCode'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'IBAN'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_InternationalBankAccountNumber'
      , name: 'InternationalBankAccountNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Identificerend nummer van de zonder vereffening ontbonden vennootschap'
    , code: ''
    , documentation: 'Identificerend nummer van de zonder vereffening ontbonden vennootschap'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityIdentifierCompanyWoundUpWithoutLiquidationProcedure'
      , name: 'EntityIdentifierCompanyWoundUpWithoutLiquidationProcedure'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Type nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IdentifierName'
      , name: 'IdentifierName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Opgave nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IdentifierValue'
      , name: 'IdentifierValue'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Inlichtingen omtrent het betreffende boekjaar'
    , code: ''
    , documentation: 'Inlichtingen omtrent het betreffende boekjaar'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_PeriodsCovered'
      , name: 'PeriodsCovered'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Begindatum van het boekjaar'
    , code: ''
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_PeriodStartDate'
      , name: 'PeriodStartDate'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Einddatum van het boekjaar'
    , code: ''
    , oldCode: ''
    , periods: ['I-End']

    , xbrlDef: {
        id: 'tax-inc_PeriodEndDate'
      , name: 'PeriodEndDate'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanslagjaar'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AssessmentYear'
      , name: 'AssessmentYear'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aangifte'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxReturnType'
      , name: 'TaxReturnType'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Inlichtingen omtrent het document'
    , code: ''
    , documentation: 'Inlichtingen omtrent het document'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentInformation'
      , name: 'DocumentInformation'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Contactpersoon'
    , code: ''
    , documentation: 'Contactpersoon voor het document'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentContact'
      , name: 'DocumentContact'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Hoedanigheid van de contactpersoon'
    , code: ''
    , documentation: 'Hoedanigheid van de contactpersoon voor het document'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentContactType'
      , name: 'DocumentContactType'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_DocumentContactTypeCodeHead'
      , name: 'DocumentContactTypeCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Naam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactName'
      , name: 'ContactName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voornaam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactFirstName'
      , name: 'ContactFirstName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Functie'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactTitlePosition'
      , name: 'ContactTitlePosition'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Adres'
    , code: ''
    , documentation: 'Adres van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactAddress'
      , name: 'ContactAddress'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-listorother'

    , title: 'Aard adres'
    , code: ''
    , documentation: 'Aard adres'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_AddressType'
      , name: 'AddressType'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_AddressTypeCodeHead'
      , name: 'AddressTypeCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_AddressTypeOther'
      , name: 'AddressTypeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Straat'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Street'
      , name: 'Street'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nr'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Number'
      , name: 'Number'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bus'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Box'
      , name: 'Box'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Postcode en gemeente'
    , code: ''
    , documentation: 'Postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCity'
      , name: 'PostalCodeCity'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_PostalCodeHead'
      , name: 'PostalCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere'
    , code: ''
    , documentation: 'Andere postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCityOther'
      , name: 'PostalCodeCityOther'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeOther'
      , name: 'PostalCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemeente'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_City'
      , name: 'City'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-listorother'

    , title: 'Land'
    , code: ''
    , documentation: 'Landcode'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_CountryCode'
      , name: 'CountryCode'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_CountryCodeHead'
      , name: 'CountryCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryCodeOther'
      , name: 'CountryCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Telefoonnummer of faxnummer'
    , code: ''
    , documentation: 'Telefoonnummer of faxnummer van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactPhoneFaxNumber'
      , name: 'ContactPhoneFaxNumber'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanduiding of het een telefoon- dan wel een faxnummer betreft'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PhoneFaxNumber'
      , name: 'PhoneFaxNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Lokaal telefoonnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_LocalPhoneNumber'
      , name: 'LocalPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'E-mail'
    , code: ''
    , documentation: 'E-mail van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactEmail'
      , name: 'ContactEmail'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'E-mail adres'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_EmailAddress'
      , name: 'EmailAddress'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Aangifte in de vennootschapsbelasting'
      , code: '275.1.A'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Reserves'
    , code: ''
    , documentation: 'Reserves'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_ReservesSection'
      , name: 'ReservesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Belastbare gereserveerde winst'
    , code: ''
    , documentation: 'Belastbare gereserveerde winst'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_TaxableReservedProfitTitle'
      , name: 'TaxableReservedProfitTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Belastbare reserves in het kapitaal en belastbare uitgiftepremies'
    , code: '1001 PN'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_TaxableReservesCapitalSharePremiums'
      , name: 'TaxableReservesCapitalSharePremiums'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastbaar gedeelte van de herwaarderingsmeerwaarden'
    , code: '1004'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_TaxablePortionRevaluationSurpluses'
      , name: 'TaxablePortionRevaluationSurpluses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Wettelijke reserve'
    , code: '1005'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_LegalReserve'
      , name: 'LegalReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Onbeschikbare reserves'
    , code: '1006'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_UnavailableReserves'
      , name: 'UnavailableReserves'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Beschikbare reserves'
    , code: '1007'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_AvailableReserves'
      , name: 'AvailableReserves'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Overgedragen winst (verlies)'
    , code: '1008 PN'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_AccumulatedProfitsLosses'
      , name: 'AccumulatedProfitsLosses'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastbare voorzieningen'
    , code: '1009'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_TaxableProvisions'
      , name: 'TaxableProvisions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Andere in de balans vermelde reserves'
    , code: ''
    , documentation: 'Andere in de balans vermelde reserves, op het einde van het belastbare tijdperk'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_OtherReservesTitle'
      , name: 'OtherReservesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere in de balans vermelde reserves'
    , code: '1010'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_OtherReserves'
      , name: 'OtherReserves'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Andere belastbare reserves'
    , code: ''
    , documentation: 'Andere belastbare reserves, op het einde van het belastbare tijdperk'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_OtherTaxableReservesTitle'
      , name: 'OtherTaxableReservesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere belastbare reserves'
    , code: '1011 PN'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_OtherTaxableReserves'
      , name: 'OtherTaxableReserves'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Onzichtbare reserves'
    , code: ''
    , documentation: 'Onzichtbare reserves, op het einde van het belastbare tijdperk'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_UndisclosedReservesTitle'
      , name: 'UndisclosedReservesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Belastbare waardeverminderingen'
    , code: '1020'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_TaxableWriteDownsUndisclosedReserve'
      , name: 'TaxableWriteDownsUndisclosedReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Overdreven afschrijvingen'
    , code: '1021'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_ExaggeratedDepreciationsUndisclosedReserve'
      , name: 'ExaggeratedDepreciationsUndisclosedReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere onderschattingen van activa'
    , code: '1022'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'
      , name: 'OtherUnderestimationsAssetsUndisclosedReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Overschattingen van passiva'
    , code: '1023'
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'
      , name: 'OtherOverestimationsLiabilitiesUndisclosedReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastbare reserves'
    , code: '1040 PN'
    , oldCode: '004 005 012 013'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_TaxableReserves'
      , name: 'TaxableReserves'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Aanpassingen in meer van de begintoestand der reserves'
    , code: ''
    , documentation: 'Aanpassingen in meer van de begintoestand der reserves, bij het begin van het belastbare tijdperk'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_AdjustmentsReservesPlusTitle'
      , name: 'AdjustmentsReservesPlusTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Meerwaarden op aandelen'
    , code: '1051'
    , oldCode: '006 (1)'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsShares'
      , name: 'CapitalGainsShares'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Terugnemingen van vroegere in verworpen uitgaven opgenomen waardeverminderingen op aandelen'
    , code: '1052'
    , oldCode: '006 (2)'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'
      , name: 'CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Definitieve vrijstelling tax shelter erkende audiovisuele werken'
    , code: '1053'
    , oldCode: '008'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'
      , name: 'DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling gewestelijke premies en kapitaal- en interestsubsidies'
    , code: '1054'
    , oldCode: '014'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'
      , name: 'ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Definitieve vrijstelling winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'
    , code: '1055'
    , oldCode: '019'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'
      , name: 'FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: '1056'
    , oldCode: '007'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_OtherAdjustmentsReservesPlus'
      , name: 'OtherAdjustmentsReservesPlus'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanpassingen in min van de begintoestand der reserves'
    , code: '1061'
    , oldCode: '009'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_AdjustmentsReservesMinus'
      , name: 'AdjustmentsReservesMinus'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastbare reserves na aanpassing van de begintoestand der reserves'
    , code: '1070 PN'
    , oldCode: '010 011'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_TaxableReservesAfterAdjustments'
      , name: 'TaxableReservesAfterAdjustments'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastbare gereserveerde winst'
    , code: '1080 PN'
    , oldCode: '020 021'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxableReservedProfit'
      , name: 'TaxableReservedProfit'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Vrijgestelde gereserveerde winst'
    , code: ''
    , documentation: 'Vrijgestelde gereserveerde winst, op het einde van het belastbare tijdperk'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_ExemptReservedProfitTitle'
      , name: 'ExemptReservedProfitTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Waardeverminderingen op handelsvorderingen'
    , code: '1101'
    , oldCode: '301 316'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_ExemptWriteDownDebtClaim'
      , name: 'ExemptWriteDownDebtClaim'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voorzieningen voor risico’s en kosten'
    , code: '1102'
    , oldCode: '302 317'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_ExemptProvisionRisksExpenses'
      , name: 'ExemptProvisionRisksExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitgedrukte maar niet-verwezenlijkte meerwaarden'
    , code: '1103'
    , oldCode: '303 318'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'
      , name: 'UnrealisedExpressedCapitalGainsExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Verwezenlijkte meerwaarden'
    , code: ''
    , documentation: 'Verwezenlijkte meerwaarden'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGainsTitle'
      , name: 'RealisedCapitalGainsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Gespreid te belasten meerwaarden op bepaalde effecten'
    , code: '1111'
    , oldCode: '305 320'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'
      , name: 'CapitalGainsSpecificSecuritiesExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gespreid te belasten meerwaarden op materiële en immateriële vaste activa'
    , code: '1112'
    , oldCode: '305 320'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'
      , name: 'CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere verwezenlijkte meerwaarden'
    , code: '1113'
    , oldCode: '304 319'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_OtherRealisedCapitalGainsExemptReserve'
      , name: 'OtherRealisedCapitalGainsExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Meerwaarden op bedrijfsvoertuigen'
    , code: '1114'
    , oldCode: '306 321'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsCorporateVehiclesExemptReserve'
      , name: 'CapitalGainsCorporateVehiclesExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Meerwaarden op binnenschepen'
    , code: '1115'
    , oldCode: '311 327'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsRiverVesselExemptReserve'
      , name: 'CapitalGainsRiverVesselExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Meerwaarden op zeeschepen'
    , code: '1116'
    , oldCode: '307 322'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsSeaVesselExemptReserve'
      , name: 'CapitalGainsSeaVesselExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Investeringsreserve'
    , code: '1121'
    , oldCode: '308 323'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_ExemptInvestmentReserve'
      , name: 'ExemptInvestmentReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Tax shelter erkende audiovisuele werken'
    , code: '1122'
    , oldCode: '309 324'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'
      , name: 'TaxShelterAuthorisedAudiovisualWorkExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'
    , code: '1123'
    , oldCode: '312 328'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'
      , name: 'ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere vrijgestelde bestanddelen'
    , code: '1124'
    , oldCode: '310 325'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_OtherExemptReserves'
      , name: 'OtherExemptReserves'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijgestelde gereserveerde winst'
    , code: '1140'
    , oldCode: '315 326'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_ExemptReservedProfit'
      , name: 'ExemptReservedProfit'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Verworpen uitgaven'
    , code: ''
    , documentation: 'Verworpen uitgaven'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DisallowedExpensesSection'
      , name: 'DisallowedExpensesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare belastingen'
    , code: '1201'
    , oldCode: '029'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleTaxes'
      , name: 'NonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gewestelijke belastingen, heffingen en retributies'
    , code: '1202'
    , oldCode: '028'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'
      , name: 'NonDeductibleRegionalTaxesDutiesRetributions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Geldboeten, verbeurdverklaringen en straffen van alle aard'
    , code: '1203'
    , oldCode: '030'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'
      , name: 'NonDeductibleFinesConfiscationsPenaltiesAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies'
    , code: '1204'
    , oldCode: '031'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
      , name: 'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen'
    , code: '1205'
    , oldCode: '032'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleCarExpensesLossValuesCars'
      , name: 'NonDeductibleCarExpensesLossValuesCars'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'
    , code: '1206'
    , oldCode: '022 074'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'
      , name: 'NonDeductibleCarExpensesPartBenefitsAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken'
    , code: '1207'
    , oldCode: '033'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'
      , name: 'NonDeductibleReceptionBusinessGiftsExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare restaurantkosten'
    , code: '1208'
    , oldCode: '025'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleRestaurantExpenses'
      , name: 'NonDeductibleRestaurantExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kosten voor niet-specifieke beroepskledij'
    , code: '1209'
    , oldCode: '034'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'
      , name: 'NonDeductibleNonSpecificProfessionalClothsExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Overdreven interesten'
    , code: '1210'
    , oldCode: '035'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExaggeratedInterests'
      , name: 'ExaggeratedInterests'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Interesten met betrekking tot een gedeelte van bepaalde leningen'
    , code: '1211'
    , oldCode: '036'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleParticularPortionInterestsLoans'
      , name: 'NonDeductibleParticularPortionInterestsLoans'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Abnormale of goedgunstige voordelen'
    , code: '1212'
    , oldCode: '037'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AbnormalBenevolentAdvantages'
      , name: 'AbnormalBenevolentAdvantages'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Sociale voordelen'
    , code: '1214'
    , oldCode: '038'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleSocialAdvantages'
      , name: 'NonDeductibleSocialAdvantages'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques'
    , code: '1215'
    , oldCode: '023'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
      , name: 'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Liberaliteiten'
    , code: '1216'
    , oldCode: '039'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Liberalities'
      , name: 'Liberalities'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Waardeverminderingen en minderwaarden op aandelen'
    , code: '1217'
    , oldCode: '040'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WriteDownsLossValuesShares'
      , name: 'WriteDownsLossValuesShares'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Terugnemingen van vroegere vrijstellingen'
    , code: '1218'
    , oldCode: '041'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReversalPreviousExemptions'
      , name: 'ReversalPreviousExemptions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Werknemersparticipatie'
    , code: '1219'
    , oldCode: '043 072'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EmployeeParticipation'
      , name: 'EmployeeParticipation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vergoedingen ontbrekende coupon'
    , code: '1220'
    , oldCode: '026'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IndemnityMissingCoupon'
      , name: 'IndemnityMissingCoupon'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kosten tax shelter erkende audiovisuele werken'
    , code: '1221'
    , oldCode: '027'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'
      , name: 'ExpensesTaxShelterAuthorisedAudiovisualWork'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gewestelijke premies en kapitaal- en interestsubsidies'
    , code: '1222'
    , oldCode: '024'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'
      , name: 'RegionalPremiumCapitalSubsidiesInterestSubsidies'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare betalingen naar bepaalde Staten'
    , code: '1223'
    , oldCode: '054'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductiblePaymentsCertainStates'
      , name: 'NonDeductiblePaymentsCertainStates'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere verworpen uitgaven'
    , code: '1239'
    , oldCode: '042'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherDisallowedExpenses'
      , name: 'OtherDisallowedExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verworpen uitgaven'
    , code: '1240'
    , oldCode: '044'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DisallowedExpenses'
      , name: 'DisallowedExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Uitgekeerde dividenden'
    , code: ''
    , documentation: 'Uitgekeerde dividenden'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DividendsPaidSection'
      , name: 'DividendsPaidSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Gewone dividenden'
    , code: '1301'
    , oldCode: '050'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OrdinaryDividends'
      , name: 'OrdinaryDividends'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verkrijging van eigen aandelen'
    , code: '1302'
    , oldCode: '051'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionOwnShares'
      , name: 'AcquisitionOwnShares'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Overlijden, uittreding of uitsluiting van een vennoot'
    , code: '1303'
    , oldCode: '052'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeceaseDepartureExclusionPartner'
      , name: 'DeceaseDepartureExclusionPartner'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verdeling van maatschappelijk vermogen'
    , code: '1304'
    , oldCode: '053'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DistributionCompanyAssets'
      , name: 'DistributionCompanyAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitgekeerde dividenden'
    , code: '1320'
    , oldCode: '059'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxableDividendsPaid'
      , name: 'TaxableDividendsPaid'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Uiteenzetting van de winst'
    , code: ''
    , documentation: 'Uiteenzetting van de winst'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_BreakdownProfitSection'
      , name: 'BreakdownProfitSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Belastbare gereserveerde winst'
    , code: '1080 PN'
    , oldCode: '020 021'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxableReservedProfit'
      , name: 'TaxableReservedProfit'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verworpen uitgaven'
    , code: '1240'
    , oldCode: '044'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DisallowedExpenses'
      , name: 'DisallowedExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitgekeerde dividenden'
    , code: '1320'
    , oldCode: '059'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxableDividendsPaid'
      , name: 'TaxableDividendsPaid'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Resultaat van het belastbare tijdperk'
    , code: '1410 PN'
    , oldCode: '060 061'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FiscalResult'
      , name: 'FiscalResult'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Werkelijk resultaat uit de zeescheepvaart waarvoor de winst wordt vastgesteld op basis van de tonnage'
    , code: '1411 PN'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ShippingResultTonnageBased'
      , name: 'ShippingResultTonnageBased'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage'
    , code: '1412 PN'
    , oldCode: '062 063'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ShippingResultNotTonnageBased'
      , name: 'ShippingResultNotTonnageBased'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Bestanddelen van het resultaat waarop de aftrekbeperking van toepassing is'
    , code: '1420'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DeductionLimitElementsFiscalResult'
      , name: 'DeductionLimitElementsFiscalResult'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Verkregen abnormale of goedgunstige voordelen en verkregen financiële voordelen of voordelen van alle aard'
    , code: '1421'
    , oldCode: '070'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'
      , name: 'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve'
    , code: '1422'
    , oldCode: '071'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'
      , name: 'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'
    , code: '1206'
    , oldCode: '022 074'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'
      , name: 'NonDeductibleCarExpensesPartBenefitsAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Werknemersparticipatie'
    , code: '1219'
    , oldCode: '043 072'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EmployeeParticipation'
      , name: 'EmployeeParticipation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw'
    , code: '1423'
    , oldCode: '076'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'
      , name: 'CapitalSubsidiesInterestSubsidiesAgriculturalSupport'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Resterend resultaat'
    , code: '1430 PN'
    , oldCode: '077 078'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RemainingFiscalResultBeforeOriginDistribution'
      , name: 'RemainingFiscalResultBeforeOriginDistribution'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane-pivot'

    , title: 'Verdeling volgens oorsprong'
    , code: ''
    , documentation: 'Verdeling volgens oorsprong'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_OriginDistributionTitle'
      , name: 'OriginDistributionTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , contextHeaders: [{ id: 'd-origin_TaxTreatyMember'
        , name: 'Bij verdrag vrijgesteld'
        , dimension: 'be-tax-d-origin-2012-04-30.xsd#d-origin_OriginDimension'
        , domain: 'be-tax-d-origin-2012-04-30.xsd#d-origin_OriginDomain'
        , domainMember: 'be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember'
        , dimensionType: 'ExplicitDimension'
      }
    , { id: 'd-origin_NoTaxTreatyMember'
        , name: 'Niet bij verdrag vrijgesteld'
        , dimension: 'be-tax-d-origin-2012-04-30.xsd#d-origin_OriginDimension'
        , domain: 'be-tax-d-origin-2012-04-30.xsd#d-origin_OriginDomain'
        , domainMember: 'be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember'
        , dimensionType: 'ExplicitDimension'
    }
    , { id: 'd-origin_BelgiumMember'
        , name: 'Belgisch'
        , dimension: 'be-tax-d-origin-2012-04-30.xsd#d-origin_OriginDimension'
        , domain: 'be-tax-d-origin-2012-04-30.xsd#d-origin_OriginDomain'
        , domainMember: 'be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember'
        , dimensionType: 'ExplicitDimension'
    }
    ]
        // , contexts:[]
      , items: [

    { xtype: 'biztax-field'

    , title: 'Resterend resultaat volgens oorsprong'
    , code: '1431 PN'
    , oldCode: '084 085 082 083 080 081'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RemainingFiscalResult'
      , name: 'RemainingFiscalResult'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-origin_TaxTreatyMember'
        , 'd-origin_NoTaxTreatyMember'
        , 'd-origin_BelgiumMember'

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Aftrekken van de resterende winst'
    , code: ''
    , documentation: 'Aftrekken van de resterende winst'
    , multipleContexts: true
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DeductionsRemainingFiscalProfitTitle'
      , name: 'DeductionsRemainingFiscalProfitTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-belastbare bestanddelen'
    , code: '1432'
    , oldCode: '097 096'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_MiscellaneousExemptions'
      , name: 'MiscellaneousExemptions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-origin_NoTaxTreatyMember'
        , 'd-origin_BelgiumMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten'
    , code: '1433'
    , oldCode: '099 098'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PEExemptIncomeMovableAssets'
      , name: 'PEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-origin_NoTaxTreatyMember'
        , 'd-origin_BelgiumMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aftrek voor octrooi-inkomsten'
    , code: '1434'
    , oldCode: '102 101'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductionPatentsIncome'
      , name: 'DeductionPatentsIncome'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-origin_NoTaxTreatyMember'
        , 'd-origin_BelgiumMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aftrek voor risicokapitaal'
    , code: '1435'
    , oldCode: '104 103'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AllowanceCorporateEquity'
      , name: 'AllowanceCorporateEquity'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-origin_NoTaxTreatyMember'
        , 'd-origin_BelgiumMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vorige verliezen'
    , code: '1436'
    , oldCode: '106 105 236'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CompensatedTaxLosses'
      , name: 'CompensatedTaxLosses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-origin_NoTaxTreatyMember'
        , 'd-origin_BelgiumMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Investeringsaftrek'
    , code: '1437'
    , oldCode: '107'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AllowanceInvestmentDeduction'
      , name: 'AllowanceInvestmentDeduction'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-origin_BelgiumMember'

      ]
    }

    ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Resterende winst volgens oorsprong'
    , code: '1450'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RemainingFiscalProfitCommonRate'
      , name: 'RemainingFiscalProfitCommonRate'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-origin_NoTaxTreatyMember'
        , 'd-origin_BelgiumMember'

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Belastbare grondslag'
    , code: ''
    , documentation: 'Belastbare grondslag'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_BasicTaxableAmountTitle'
      , name: 'BasicTaxableAmountTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Belastbaar tegen gewoon tarief'
    , code: '1460'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_BasicTaxableAmountCommonRate'
      , name: 'BasicTaxableAmountCommonRate'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Resterende winst volgens oorsprong'
    , code: '1450'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RemainingFiscalProfitCommonRate'
      , name: 'RemainingFiscalProfitCommonRate'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Winst uit zeescheepvaart, vastgesteld op basis van de tonnage'
    , code: '1461'
    , oldCode: '108'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ShippingProfitTonnageBased'
      , name: 'ShippingProfitTonnageBased'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verkregen abnormale of goedgunstige voordelen en verkregen financiële voordelen of voordelen van alle aard'
    , code: '1421'
    , oldCode: '070'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'
      , name: 'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve'
    , code: '1422'
    , oldCode: '071'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'
      , name: 'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'
    , code: '1206'
    , oldCode: '022 074'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'
      , name: 'NonDeductibleCarExpensesPartBenefitsAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Werknemersparticipatie'
    , code: '1219'
    , oldCode: '043 072'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EmployeeParticipation'
      , name: 'EmployeeParticipation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Meerwaarden op aandelen belastbaar tegen 25%'
    , code: '1465'
    , oldCode: '118'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsSharesRate2500'
      , name: 'CapitalGainsSharesRate2500'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastbaar tegen exit tax tarief (16,5%)'
    , code: '1470'
    , oldCode: '115'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BasicTaxableAmountExitTaxRate'
      , name: 'BasicTaxableAmountExitTaxRate'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw, belastbaar tegen 5 %'
    , code: '1481'
    , oldCode: '119'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'
      , name: 'CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Afzonderlijke aanslagen'
    , code: ''
    , documentation: 'Afzonderlijke aanslagen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_SeparateAssessmentsSection'
      , name: 'SeparateAssessmentsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financiële voordelen of voordelen van alle aard'
    , code: '1501'
    , oldCode: '120'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'
      , name: 'UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen'
    , code: '1502'
    , oldCode: '123'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'
      , name: 'SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Afzonderlijke aanslag van de uitgekeerde dividenden ten name van vennootschappen die krediet voor ambachtsoutillage mogen verstrekken en vennootschappen voor huisvesting'
    , code: '1503'
    , oldCode: '124'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'
      , name: 'SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Bijzondere aanslagen met betrekking tot verrichtingen die vóór 1 januari 1990 hebben plaatsgevonden'
    , code: ''
    , documentation: 'Bijzondere aanslagen met betrekking tot verrichtingen die vóór 1 januari 1990 hebben plaatsgevonden'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_SpecialAssessmentPre1990Section'
      , name: 'SpecialAssessmentPre1990Section'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Gehele of gedeeltelijke verdeling van maatschappelijk vermogen, belastbaar tegen 33%'
    , code: '1511'
    , oldCode: '125'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'
      , name: 'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gehele of gedeeltelijke verdeling van maatschappelijk vermogen, belastbaar tegen 16,5%'
    , code: '1512'
    , oldCode: '126'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'
      , name: 'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voordelen van alle aard verleend door vennootschappen in vereffening'
    , code: '1513'
    , oldCode: '128'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'
      , name: 'SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Aanvullende heffing erkende diamanthandelaars en terugbetaling van voorheen verleend belastingkrediet voor onderzoek en ontwikkeling'
    , code: ''
    , documentation: 'Aanvullende heffing erkende diamanthandelaars en terugbetaling van voorheen verleend belastingkrediet voor onderzoek en ontwikkeling'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_AdditionalDutiesAuthorisedDiamondTradersRetributionTaxCreditResearchDevelopmentSection'
      , name: 'AdditionalDutiesAuthorisedDiamondTradersRetributionTaxCreditResearchDevelopmentSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanvullende heffing erkende diamanthandelaars'
    , code: '1531'
    , oldCode: '109'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AdditionalDutiesDiamondTraders'
      , name: 'AdditionalDutiesDiamondTraders'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling'
    , code: '1532'
    , oldCode: '223'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RetributionTaxCreditResearchDevelopment'
      , name: 'RetributionTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Niet-belastbare bestanddelen'
    , code: ''
    , documentation: 'Niet-belastbare bestanddelen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_MiscellaneousExemptionsSection'
      , name: 'MiscellaneousExemptionsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Vrijgestelde giften'
    , code: '1601'
    , oldCode: '090'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptGifts'
      , name: 'ExemptGifts'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling aanvullend personeel'
    , code: '1602'
    , oldCode: '091'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'
      , name: 'ExemptionAdditionalPersonnelMiscellaneousExemptions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling bijkomend personeel KMO'
    , code: '1603'
    , oldCode: '092'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptionAdditionalPersonnelSMEs'
      , name: 'ExemptionAdditionalPersonnelSMEs'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling stagebonus'
    , code: '1604'
    , oldCode: '094'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptionTrainingPeriodBonus'
      , name: 'ExemptionTrainingPeriodBonus'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere niet-belastbare bestanddelen'
    , code: '1605'
    , oldCode: '095'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherMiscellaneousExemptions'
      , name: 'OtherMiscellaneousExemptions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-belastbare bestanddelen'
    , code: '1610'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductibleMiscellaneousExemptions'
      , name: 'DeductibleMiscellaneousExemptions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane-pivot'

    , title: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten'
    , code: ''
    , documentation: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_PEExemptIncomeMovableAssetsSection'
      , name: 'PEExemptIncomeMovableAssetsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , contextHeaders: [{ id: 'd-br_BelgianBranchMember'
        , name: 'Belgische inrichtingen'
        , dimension: 'be-tax-d-br-2012-04-30.xsd#d-br_BranchDimension'
        , domain: 'be-tax-d-br-2012-04-30.xsd#d-br_BranchDomain'
        , domainMember: 'be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember'
        , dimensionType: 'ExplicitDimension'
      }
    , { id: 'd-br_ForeignBranchMember'
        , name: 'Buitenlandse inrichtingen'
        , dimension: 'be-tax-d-br-2012-04-30.xsd#d-br_BranchDimension'
        , domain: 'be-tax-d-br-2012-04-30.xsd#d-br_BranchDomain'
        , domainMember: 'be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember'
        , dimensionType: 'ExplicitDimension'
    }
    ]
        // , contexts:[]
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten uit aandelen'
    , code: ''
    , documentation: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten uit aandelen'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_PEExemptIncomeMovableAssetsSharesTitle'
      , name: 'PEExemptIncomeMovableAssetsSharesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane'

    , title: 'Inkomsten toegekend door een vennootschap gevestigd in een lidstaat van de EER'
    , code: ''
    , documentation: 'Inkomsten toegekend door een vennootschap gevestigd in een lidstaat van de EER'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_IncomeAttributedCompanyEEASharesPEExemptIncomeMovableAssetsTitle'
      , name: 'IncomeAttributedCompanyEEASharesPEExemptIncomeMovableAssetsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field'

    , title: 'Nettobedrag, Belgische inkomsten'
    , code: '1631'
    , oldCode: '216'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'
      , name: 'NetBelgianIncomeSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing, Belgische inkomsten'
    , code: '1632'
    , oldCode: '217'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'
      , name: 'WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nettobedrag, buitenlandse inkomsten'
    , code: '1633'
    , oldCode: '218'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'
      , name: 'NetForeignIncomeSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing, buitenlandse inkomsten'
    , code: '1634'
    , oldCode: '219'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'
      , name: 'WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }

    ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere inkomsten'
    , code: ''
    , documentation: 'Andere inkomsten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_IncomeAttributedOtherSharesPEExemptIncomeMovableAssetsTitle'
      , name: 'IncomeAttributedOtherSharesPEExemptIncomeMovableAssetsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field'

    , title: 'Nettobedrag, Belgische inkomsten'
    , code: '1635'
    , oldCode: '220'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'
      , name: 'NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing, Belgische inkomsten'
    , code: '1636'
    , oldCode: '221'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'
      , name: 'WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nettobedrag, buitenlandse inkomsten'
    , code: '1637'
    , oldCode: '225'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'
      , name: 'NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing, buitenlandse inkomsten'
    , code: '1638'
    , oldCode: '226'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'
      , name: 'WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }

    ]
    }

    ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere vrijgestelde roerende inkomsten'
    , code: '1639'
    , oldCode: '228'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherExemptIncomeMovableAssets'
      , name: 'OtherExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten'
    , code: '1640'
    , oldCode: '229'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_GrossPEExemptIncomeMovableAssets'
      , name: 'GrossPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kosten'
    , code: '1641'
    , oldCode: '230'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'
      , name: 'ExpensesSharesPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten na aftrek kosten'
    , code: '1642'
    , oldCode: '231'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NetPEExemptIncomeMovableAssets'
      , name: 'NetPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Inkomsten uit niet-vergoede inbrengen in geval van fusie, splitsing of hiermee gelijkgestelde verrichtingen omdat de overnemende of verkrijgende vennootschap in het bezit is van aandelen van de overgenomen of gesplitste vennootschap of gelijkaardige verrichtingen in een andere EU-lidstaat'
    , code: '1643'
    , oldCode: '232'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'
      , name: 'IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijgestelde roerende inkomsten uit effecten van bepaalde herfinancieringsleningen'
    , code: '1644'
    , oldCode: '233'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'
      , name: 'ExemptIncomeMovableAssetsRefinancingLoans'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten'
    , code: '1650'
    , oldCode: '234'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductiblePEExemptIncomeMovableAssets'
      , name: 'DeductiblePEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

      , contexts: [
        'd-br_BelgianBranchMember'
        , 'd-br_ForeignBranchMember'

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Overdracht aftrek definitief belaste inkomsten'
    , code: ''
    , documentation: 'Overdracht aftrek definitief belaste inkomsten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CarryOverPEExemptIncomeMovableAssetsSection'
      , name: 'CarryOverPEExemptIncomeMovableAssetsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Saldo van de overgedragen aftrek definitief belaste inkomsten'
    , code: '1701'
    , oldCode: '262'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AccumulatedPEExemptIncomeMovableAssets'
      , name: 'AccumulatedPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aftrek definitief belaste inkomsten van het belastbare tijdperk dat overdraagbaar is naar het volgende belastbare tijdperk'
    , code: '1702'
    , oldCode: '263'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'
      , name: 'CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Overgedragen aftrek definitief belaste inkomsten die werkelijk wordt afgetrokken van het belastbare tijdperk'
    , code: '1703'
    , oldCode: '264'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'
      , name: 'DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Saldo van de aftrek definitief belaste inkomsten dat overdraagbaar is naar het volgende belastbare tijdperk'
    , code: '1704'
    , oldCode: '265'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'
      , name: 'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Overdracht aftrek voor risicokapitaal'
    , code: ''
    , documentation: 'Overdracht aftrek voor risicokapitaal'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CarryOverAllowanceCorporateEquitySection'
      , name: 'CarryOverAllowanceCorporateEquitySection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Saldo van de overgedragen aftrek voor risicokapitaal'
    , code: '1711'
    , oldCode: '330'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AccumulatedAllowanceCorporateEquity'
      , name: 'AccumulatedAllowanceCorporateEquity'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar het volgende belastbare tijdperk'
    , code: '1712'
    , oldCode: '332'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'
      , name: 'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Compenseerbare verliezen'
    , code: ''
    , documentation: 'Compenseerbare verliezen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CompensableTaxLossesSection'
      , name: 'CompensableTaxLossesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Saldo van de compenseerbare vorige verliezen'
    , code: '1721 N'
    , oldCode: '235'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CompensableTaxLosses'
      , name: 'CompensableTaxLosses'
      , itemType: 'nonPositiveMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vorige verliezen'
    , code: '1436'
    , oldCode: '106 105 236'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CompensatedTaxLosses'
      , name: 'CompensatedTaxLosses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verlies van het belastbare tijdperk'
    , code: '1722 N'
    , oldCode: '237'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_LossCurrentTaxPeriod'
      , name: 'LossCurrentTaxPeriod'
      , itemType: 'nonPositiveMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verlies over te brengen naar het volgende belastbare tijdperk'
    , code: '1730 N'
    , oldCode: '238'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverTaxLosses'
      , name: 'CarryOverTaxLosses'
      , itemType: 'nonPositiveMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 0
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Tarief van de belasting'
    , code: ''
    , documentation: 'Tarief van de belasting'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_TaxRateSection'
      , name: 'TaxRateSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'De vennootschap kan bij uw weten aanspraak maken op het verminderd tarief'
    , code: '1751'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExclusionReducedRate'
      , name: 'ExclusionReducedRate'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'De vennootschap is ofwel een vennootschap die krediet voor ambachtsoutillage mag verstrekken, ofwel een vennootschap voor huisvesting, belastbaar tegen het tarief van 5%'
    , code: '1752'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CreditCorporationTradeEquipmentHousingCorporationTaxRate'
      , name: 'CreditCorporationTradeEquipmentHousingCorporationTaxRate'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Voorafbetalingen'
    , code: ''
    , documentation: 'Voorafbetalingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_PrepaymentsSection'
      , name: 'PrepaymentsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Deze aangifte heeft betrekking op één van de eerste drie boekjaren vanaf de oprichting van de vennootschap die een kleine vennootschap is in de zin van het Wetboek van vennootschappen'
    , code: '1801'
    , oldCode: '169'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FirstThreeAccountingYearsSmallCompanyCorporationCode'
      , name: 'FirstThreeAccountingYearsSmallCompanyCorporationCode'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'In aanmerking te nemen voorafbetalingen'
    , code: '1810'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_Prepayments'
      , name: 'Prepayments'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Voorafbetaling, eerste kwartaal'
    , code: '1811'
    , oldCode: '170'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentFirstQuarter'
      , name: 'PrepaymentFirstQuarter'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voorafbetaling, tweede kwartaal'
    , code: '1812'
    , oldCode: '171'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentSecondQuarter'
      , name: 'PrepaymentSecondQuarter'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voorafbetaling, derde kwartaal'
    , code: '1813'
    , oldCode: '172'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentThirdQuarter'
      , name: 'PrepaymentThirdQuarter'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voorafbetaling, vierde kwartaal'
    , code: '1814'
    , oldCode: '173'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentFourthQuarter'
      , name: 'PrepaymentFourthQuarter'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Referentienummer verschillend van het ondernemingsnummer en toegekend door de Dienst Voorafbetalingen'
    , code: ''
    , documentation: 'Referentienummer verschillend van het ondernemingsnummer en toegekend door de Dienst Voorafbetalingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierTitle'
      , name: 'PrepaymentReferenceNumberNotEntityIdentifierTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Eerste ander referentienummer'
    , code: '1821'
    , oldCode: '176'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence'
      , name: 'PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence'
      , itemType: 'integerItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Tweede ander referentienummer'
    , code: '1822'
    , oldCode: '177'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence'
      , name: 'PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence'
      , itemType: 'integerItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Derde ander referentienummer'
    , code: '1823'
    , oldCode: '178'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence'
      , name: 'PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence'
      , itemType: 'integerItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vierde ander referentienummer'
    , code: '1824'
    , oldCode: '179'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence'
      , name: 'PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence'
      , itemType: 'integerItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Verrekenbare voorheffingen'
    , code: ''
    , documentation: 'Verrekenbare voorheffingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ClearableAdvanceLeviesSection'
      , name: 'ClearableAdvanceLeviesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Niet-terugbetaalbare voorheffingen'
    , code: '1830'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonRepayableAdvanceLevies'
      , name: 'NonRepayableAdvanceLevies'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Fictieve roerende voorheffing'
    , code: '1831'
    , oldCode: '182'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonRepayableFictiousWitholdingTax'
      , name: 'NonRepayableFictiousWitholdingTax'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Forfaitair gedeelte van buitenlandse belasting'
    , code: '1832'
    , oldCode: '183'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonRepayableLumpSumForeignTaxes'
      , name: 'NonRepayableLumpSumForeignTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastingkrediet voor onderzoek en ontwikkeling'
    , code: '1833'
    , oldCode: '184'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopment'
      , name: 'TaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Terugbetaalbare voorheffingen'
    , code: '1840'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_RepayableAdvanceLevies'
      , name: 'RepayableAdvanceLevies'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'
    , code: '1841'
    , oldCode: '187'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'
      , name: 'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'
    , code: '1842'
    , oldCode: '188'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'
      , name: 'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'
    , code: '1843'
    , oldCode: '190'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableWithholdingTaxOtherPEForeign'
      , name: 'RepayableWithholdingTaxOtherPEForeign'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen'
    , code: '1844'
    , oldCode: '192'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'
      , name: 'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing op andere dividenden'
    , code: '1845'
    , oldCode: '194'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableWithholdingTaxOtherDividends'
      , name: 'RepayableWithholdingTaxOtherDividends'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere terugbetaalbare roerende voorheffing'
    , code: '1846'
    , oldCode: '195'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherRepayableWithholdingTaxes'
      , name: 'OtherRepayableWithholdingTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastingkrediet voor onderzoek en ontwikkeling dat voor het huidig belastbare tijdperk terugbetaalbaar is'
    , code: '1850'
    , oldCode: '198'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
      , name: 'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Tax shelter'
    , code: ''
    , documentation: 'Tax shelter'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_TaxShelterSection'
      , name: 'TaxShelterSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'De vennootschap is een binnenlandse vennootschap voor de productie van audiovisuele werken, die als voornaamste doel de ontwikkeling en de productie van audiovisuele werken heeft en niet zijnde een televisieomroep of een onderneming die verbonden is met Belgische of buitenlandse televisieomroepen, die een raamovereenkomst voor de productie van een erkend Belgisch audiovisueel werk heeft gesloten'
    , code: '1861'
    , oldCode: '363'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BelgianCorporationAudiovisualWorksTaxShelterAgreement'
      , name: 'BelgianCorporationAudiovisualWorksTaxShelterAgreement'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Grootte van de vennootschap in de zin van het Wetboek van Vennootschappen'
    , code: ''
    , documentation: 'Grootte van de vennootschap in de zin van het Wetboek van Vennootschappen'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_AssessmentCompanyCategorySection'
      , name: 'AssessmentCompanyCategorySection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Inlichtingen ter beoordeling kleine vennootschap'
    , code: ''
    , documentation: 'Inlichtingen ter beoordeling kleine vennootschap'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_AssessmentSmallCompanyCorporationCodeTitle'
      , name: 'AssessmentSmallCompanyCorporationCodeTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Gegevens met betrekking tot het belastbaar tijdperk'
    , code: ''
    , documentation: 'Gegevens met betrekking tot het belastbaar tijdperk'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_AssessmentSmallCompanyCorporationCodeCurrentTaxPeriodTitle'
      , name: 'AssessmentSmallCompanyCorporationCodeCurrentTaxPeriodTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'De vennootschap is verbonden met één of meerdere andere vennootschappen in de zin van het Wetboek van Vennootschappen'
    , code: '1871'
    , oldCode: '268'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AssociatedCompanyCorporationCodeCurrentTaxPeriod'
      , name: 'AssociatedCompanyCorporationCodeCurrentTaxPeriod'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }
  ,
    { xtype: 'biztax-label'

    , title: 'De gegevens op niet-geconsolideerde basis vermelden, tenzij de vennootschap verbonden is met één of meerdere andere vennootschappen in de zin van het Wetboek van Vennootschappen, dan die gegevens op geconsolideerde basis vermelden'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ConsolidatedDataIfApplicableTitle'
      , name: 'ConsolidatedDataIfApplicableTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Jaargemiddelde van het personeelsbestand'
    , code: '1872'
    , oldCode: '267'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'
      , name: 'AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Jaaromzet, exclusief btw'
    , code: '1873'
    , oldCode: '261'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'
      , name: 'AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Balanstotaal'
    , code: '1874'
    , oldCode: '251'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'
      , name: 'BalanceSheetTotalCorporationCodeCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Diverse bescheiden en opgaven'
    , code: ''
    , documentation: 'Diverse bescheiden en opgaven'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_DocumentsStatementsSection'
      , name: 'DocumentsStatementsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'De verslagen aan en besluiten van de algemene vergadering en, indien verplicht, de jaarrekening (balans, resultatenrekening en eventuele toelichting) worden bij de aangifte gevoegd.'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_StatutoryAccountsGeneralMeetingMinutesDecisionsMandatoryAnnexes'
      , name: 'StatutoryAccountsGeneralMeetingMinutesDecisionsMandatoryAnnexes'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-label'

    , title: 'De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord moeten bij de aangifte worden gevoegd.'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_DocumentsAddedExemptionProfitHomologationReorganizationPlanAmicableSettlement'
      , name: 'DocumentsAddedExemptionProfitHomologationReorganizationPlanAmicableSettlement'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-label'

    , title: 'De opgaven/staat/aangifte waarmee aanspraak kan worden gemaakt op de toepassing van de desbetreffende wettelijke bepalingen, moeten in voorkomend geval worden ingevuld.'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_DocumentsStatementsAddedTaxReturnIfMandatory'
      , name: 'DocumentsStatementsAddedTaxReturnIfMandatory'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Diverse bescheiden en opgaven'
      , code: '275.1.B'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Jaarrekening (balans, resultatenrekening en eventuele toelichting)'
    , code: ''
    , documentation: 'Jaarrekening (balans, resultatenrekening en eventuele toelichting)'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_StatutoryAccountsTitle'
      , name: 'StatutoryAccountsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'Bij te voegen, indien de belastingplichtige niet verplicht is deze neer te leggen bij de Balanscentrale van de Nationale Bank'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_AnnualAccountsMandatoryAnnexeFilingCentralBalanceSheetOfficeNotRequired'
      , name: 'AnnualAccountsMandatoryAnnexeFilingCentralBalanceSheetOfficeNotRequired'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Jaarrekening (balans, resultatenrekening en eventuele toelichting)'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_StatutoryAccounts'
      , name: 'StatutoryAccounts'
      , itemType: 'base64BinaryItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'biztax-uploadfield'
    , isRequired: false
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Verslagen aan en besluiten van de algemene vergadering'
    , code: ''
    , documentation: 'Verslagen aan en besluiten van de algemene vergadering'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_GeneralMeetingMinutesDecisionsTitle'
      , name: 'GeneralMeetingMinutesDecisionsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'Verplicht bij te voegen'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_GeneralMeetingMinutesDecisionsMandatoryAnnexe'
      , name: 'GeneralMeetingMinutesDecisionsMandatoryAnnexe'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verslagen aan en besluiten van de algemene vergadering'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_GeneralMeetingMinutesDecisions'
      , name: 'GeneralMeetingMinutesDecisions'
      , itemType: 'nonEmptyBase64BinaryItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'biztax-uploadfield'
    , isRequired: true
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'
    , code: ''
    , documentation: 'De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsTitle'
      , name: 'ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'Verplicht bij te voegen'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsMandatoryAnnexe'
      , name: 'ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsMandatoryAnnexe'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-field'

    , title: 'De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments'
      , name: 'ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments'
      , itemType: 'base64BinaryItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'biztax-uploadfield'
    , isRequired: false
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Interne jaarrekening'
    , code: ''
    , documentation: 'Interne jaarrekening'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InternalStatutoryAccountsTitle'
      , name: 'InternalStatutoryAccountsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'Optioneel bij te voegen'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_InternalStatutoryAccountsNonMandatoryAnnexe'
      , name: 'InternalStatutoryAccountsNonMandatoryAnnexe'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Interne jaarrekening'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InternalStatutoryAccounts'
      , name: 'InternalStatutoryAccounts'
      , itemType: 'base64BinaryItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'biztax-uploadfield'
    , isRequired: false
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Afschrijvingstabellen'
    , code: ''
    , documentation: 'Afschrijvingstabellen'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_DepreciationTablesTitle'
      , name: 'DepreciationTablesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'Optioneel bij te voegen'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_DepreciationTablesNonMandatoryAnnexe'
      , name: 'DepreciationTablesNonMandatoryAnnexe'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Afschrijvingstabel'
    , code: ''
    , documentation: 'Afschrijvingstabel'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DepreciationTableTitle'
      , name: 'DepreciationTableTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Lineair afschrijvingspercentage'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AnnuityLinearDepreciation'
      , name: 'AnnuityLinearDepreciation'
      , itemType: 'percentageItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , minValue: 0, maxValue: 1, maxLength: 5, decimalPrecision: 4
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Degressief afschrijvingspercentage'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AnnuityDegressiveDepreciation'
      , name: 'AnnuityDegressiveDepreciation'
      , itemType: 'percentageItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , minValue: 0, maxValue: 1, maxLength: 5, decimalPrecision: 4
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van belegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateInvestment'
      , name: 'DateInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Herwaarderingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Revaluations'
      , name: 'Revaluations'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Afschrijvingen, zonder herwaarderingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DepreciationsNoRevaluations'
      , name: 'DepreciationsNoRevaluations'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Afschrijvingen op herwaarderingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DepreciationsRevaluations'
      , name: 'DepreciationsRevaluations'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Overdreven afschrijvingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExaggeratedDepreciations'
      , name: 'ExaggeratedDepreciations'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Afschrijvingstabellen'
    , code: ''
    , documentation: 'Afschrijvingstabellen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DepreciationTableNonStructuredTitle'
      , name: 'DepreciationTableNonStructuredTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Afschrijvingstabel niet-gestandaardiseerd'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DepreciationTableNonStructured'
      , name: 'DepreciationTableNonStructured'
      , itemType: 'base64BinaryItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'biztax-uploadfield'
    , isRequired: false
    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Detail verworpen uitgaven'
    , code: ''
    , documentation: 'Detail verworpen uitgaven'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_DetailDisallowedExpensesTitle'
      , name: 'DetailDisallowedExpensesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'Optioneel bij te voegen'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_DetailDisallowedExpensesNonMandatoryAnnexe'
      , name: 'DetailDisallowedExpensesNonMandatoryAnnexe'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Niet-aftrekbare belastingen'
    , code: ''
    , documentation: 'Niet-aftrekbare belastingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleTaxesTitle'
      , name: 'NonDeductibleTaxesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Niet-terugbetaalbare voorheffingen'
    , code: ''
    , documentation: 'Niet-terugbetaalbare voorheffingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonRepayableAdvanceLeviesNonDeductibleTaxes'
      , name: 'NonRepayableAdvanceLeviesNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Fictieve roerende voorheffing'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'
      , name: 'NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Forfaitair gedeelte van buitenlandse belasting'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'
      , name: 'NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Terugbetaalbare voorheffingen'
    , code: ''
    , documentation: 'Terugbetaalbare voorheffingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_RepayableAdvanceLeviesNonDeductibleTaxes'
      , name: 'RepayableAdvanceLeviesNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'
      , name: 'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'
      , name: 'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'
      , name: 'RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'
      , name: 'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Roerende voorheffing op andere dividenden'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'
      , name: 'RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere terugbetaalbare roerende voorheffing'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'
      , name: 'OtherRepayableWithholdingTaxesNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'In aanmerking te nemen voorafbetalingen'
    , code: ''
    , documentation: 'In aanmerking te nemen voorafbetalingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_PrepaymentsNonDeductibleTaxes'
      , name: 'PrepaymentsNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Voorafbetaling, eerste kwartaal'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'
      , name: 'PrepaymentFirstQuarterNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voorafbetaling, tweede kwartaal'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'
      , name: 'PrepaymentSecondQuarterNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voorafbetaling, derde kwartaal'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'
      , name: 'PrepaymentThirdQuarterNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voorafbetaling, vierde kwartaal'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'
      , name: 'PrepaymentFourthQuarterNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vennnootschapsbelasting inclusief vermeerdering, verhoging en nalatigheidsinteresten'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'
      , name: 'CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Geraamd bedrag der belastingschulden'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EstimatedCorporateIncomeTaxDebt'
      , name: 'EstimatedCorporateIncomeTaxDebt'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Door de vennootschap gedragen roerende voorheffing op uitgekeerde dividenden'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WithholdingTaxDividendsPaid'
      , name: 'WithholdingTaxDividendsPaid'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Door de vennootschap gedragen roerende voorheffing op andere uitbetaalde inkomsten'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WithholdingTaxOtherIncomePaid'
      , name: 'WithholdingTaxOtherIncomePaid'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere niet-aftrekbare belastingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherNonDeductibleTaxes'
      , name: 'OtherNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare belastingen voor aftrek van terugbetalingen en regulariseringen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'
      , name: 'NonDeductibleTaxesNoReimbursementsRegularizations'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Terugbetalingen en regulariseringen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'
      , name: 'ReimbursementsRegularizationsNonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare belastingen'
    , code: '1201'
    , oldCode: '029'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleTaxes'
      , name: 'NonDeductibleTaxes'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Gewestelijke belastingen, heffingen en retributies'
    , code: ''
    , documentation: 'Gewestelijke belastingen, heffingen en retributies'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleRegionalTaxesDutiesRetributionsTitle'
      , name: 'NonDeductibleRegionalTaxesDutiesRetributionsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Gewestelijke belastingen, heffingen en retributies'
    , code: '1202'
    , oldCode: '028'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'
      , name: 'NonDeductibleRegionalTaxesDutiesRetributions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleRegionalTaxesDutiesRetributions'
      , name: 'DetailNonDeductibleRegionalTaxesDutiesRetributions'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Geldboeten, verbeurdverklaringen en straffen van alle aard'
    , code: ''
    , documentation: 'Geldboeten, verbeurdverklaringen en straffen van alle aard'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKindTitle'
      , name: 'NonDeductibleFinesConfiscationsPenaltiesAllKindTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Geldboeten, verbeurdverklaringen en straffen van alle aard'
    , code: '1203'
    , oldCode: '030'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'
      , name: 'NonDeductibleFinesConfiscationsPenaltiesAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleFinesConfiscationsPenaltiesAllKind'
      , name: 'DetailNonDeductibleFinesConfiscationsPenaltiesAllKind'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies'
    , code: ''
    , documentation: 'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiumsTitle'
      , name: 'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiumsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies'
    , code: '1204'
    , oldCode: '031'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
      , name: 'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
      , name: 'DetailNonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen'
    , code: ''
    , documentation: 'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleCarExpensesLossValuesCarsTitle'
      , name: 'NonDeductibleCarExpensesLossValuesCarsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Kost'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarExpense'
      , name: 'CarExpense'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Minderwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_LossValueCar'
      , name: 'LossValueCar'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbaar percentage'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'
      , name: 'NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'
      , itemType: 'percentageItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , minValue: 0, maxValue: 1, maxLength: 5, decimalPrecision: 4
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen'
    , code: '1205'
    , oldCode: '032'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleCarExpensesLossValuesCars'
      , name: 'NonDeductibleCarExpensesLossValuesCars'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vergoeding als schadeloosstelling voor herstellingskosten'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CompensationCarRepairCosts'
      , name: 'CompensationCarRepairCosts'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleCarExpensesLossValuesCars'
      , name: 'DetailNonDeductibleCarExpensesLossValuesCars'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'
    , code: ''
    , documentation: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKindTitle'
      , name: 'NonDeductibleCarExpensesPartBenefitsAllKindTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'
    , code: '1206'
    , oldCode: '022 074'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'
      , name: 'NonDeductibleCarExpensesPartBenefitsAllKind'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleCarExpensesPartBenefitsAllKind'
      , name: 'DetailNonDeductibleCarExpensesPartBenefitsAllKind'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken'
    , code: ''
    , documentation: 'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleReceptionBusinessGiftsExpensesTitle'
      , name: 'NonDeductibleReceptionBusinessGiftsExpensesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Kost'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReceptionBusinessGiftsExpense'
      , name: 'ReceptionBusinessGiftsExpense'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbaar percentage'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'
      , name: 'NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'
      , itemType: 'percentageItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , minValue: 0, maxValue: 1, maxLength: 5, decimalPrecision: 4
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken'
    , code: '1207'
    , oldCode: '033'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'
      , name: 'NonDeductibleReceptionBusinessGiftsExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleReceptionBusinessGiftsExpenses'
      , name: 'DetailNonDeductibleReceptionBusinessGiftsExpenses'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Niet-aftrekbare restaurantkosten'
    , code: ''
    , documentation: 'Niet-aftrekbare restaurantkosten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleRestaurantExpensesTitle'
      , name: 'NonDeductibleRestaurantExpensesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Kost'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RestaurantExpense'
      , name: 'RestaurantExpense'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbaar percentage'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'
      , name: 'NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'
      , itemType: 'percentageItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , minValue: 0, maxValue: 1, maxLength: 5, decimalPrecision: 4
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare restaurantkosten'
    , code: '1208'
    , oldCode: '025'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleRestaurantExpenses'
      , name: 'NonDeductibleRestaurantExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleRestaurantExpenses'
      , name: 'DetailNonDeductibleRestaurantExpenses'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Kosten voor niet-specifieke beroepskledij'
    , code: ''
    , documentation: 'Kosten voor niet-specifieke beroepskledij'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleNonSpecificProfessionalClothsExpensesTitle'
      , name: 'NonDeductibleNonSpecificProfessionalClothsExpensesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Kosten voor niet-specifieke beroepskledij'
    , code: '1209'
    , oldCode: '034'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'
      , name: 'NonDeductibleNonSpecificProfessionalClothsExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleNonSpecificProfessionalClothsExpenses'
      , name: 'DetailNonDeductibleNonSpecificProfessionalClothsExpenses'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Overdreven interesten'
    , code: ''
    , documentation: 'Overdreven interesten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ExaggeratedInterestsTitle'
      , name: 'ExaggeratedInterestsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Overdreven interesten'
    , code: '1210'
    , oldCode: '035'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExaggeratedInterests'
      , name: 'ExaggeratedInterests'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailExaggeratedInterests'
      , name: 'DetailExaggeratedInterests'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Interesten met betrekking tot een gedeelte van bepaalde leningen'
    , code: ''
    , documentation: 'Interesten met betrekking tot een gedeelte van bepaalde leningen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleParticularPortionInterestsLoansTitle'
      , name: 'NonDeductibleParticularPortionInterestsLoansTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Interesten met betrekking tot een gedeelte van bepaalde leningen'
    , code: '1211'
    , oldCode: '036'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleParticularPortionInterestsLoans'
      , name: 'NonDeductibleParticularPortionInterestsLoans'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleParticularPortionInterestsLoans'
      , name: 'DetailNonDeductibleParticularPortionInterestsLoans'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Abnormale of goedgunstige voordelen'
    , code: ''
    , documentation: 'Abnormale of goedgunstige voordelen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_AbnormalBenevolentAdvantagesTitle'
      , name: 'AbnormalBenevolentAdvantagesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Abnormale of goedgunstige voordelen'
    , code: '1212'
    , oldCode: '037'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AbnormalBenevolentAdvantages'
      , name: 'AbnormalBenevolentAdvantages'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailAbnormalBenevolentAdvantages'
      , name: 'DetailAbnormalBenevolentAdvantages'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Sociale voordelen'
    , code: ''
    , documentation: 'Sociale voordelen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleSocialAdvantagesTitle'
      , name: 'NonDeductibleSocialAdvantagesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Sociale voordelen'
    , code: '1214'
    , oldCode: '038'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleSocialAdvantages'
      , name: 'NonDeductibleSocialAdvantages'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleSocialAdvantages'
      , name: 'DetailNonDeductibleSocialAdvantages'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques'
    , code: ''
    , documentation: 'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchersTitle'
      , name: 'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchersTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques'
    , code: '1215'
    , oldCode: '023'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
      , name: 'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
      , name: 'DetailNonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Liberaliteiten'
    , code: ''
    , documentation: 'Liberaliteiten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_LiberalitiesTitle'
      , name: 'LiberalitiesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Liberaliteiten'
    , code: '1216'
    , oldCode: '039'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Liberalities'
      , name: 'Liberalities'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailLiberalities'
      , name: 'DetailLiberalities'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Waardeverminderingen en minderwaarden op aandelen'
    , code: ''
    , documentation: 'Waardeverminderingen en minderwaarden op aandelen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_WriteDownsLossValuesSharesTitle'
      , name: 'WriteDownsLossValuesSharesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Waardeverminderingen en minderwaarden op aandelen'
    , code: '1217'
    , oldCode: '040'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WriteDownsLossValuesShares'
      , name: 'WriteDownsLossValuesShares'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailWriteDownsLossValuesShares'
      , name: 'DetailWriteDownsLossValuesShares'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Terugnemingen van vroegere vrijstellingen'
    , code: ''
    , documentation: 'Terugnemingen van vroegere vrijstellingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ReversalPreviousExemptionsTitle'
      , name: 'ReversalPreviousExemptionsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Terugnemingen van vroegere vrijstellingen'
    , code: '1218'
    , oldCode: '041'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReversalPreviousExemptions'
      , name: 'ReversalPreviousExemptions'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailReversalPreviousExemptions'
      , name: 'DetailReversalPreviousExemptions'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Werknemersparticipatie'
    , code: ''
    , documentation: 'Werknemersparticipatie'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_EmployeeParticipationTitle'
      , name: 'EmployeeParticipationTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Werknemersparticipatie'
    , code: '1219'
    , oldCode: '043 072'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EmployeeParticipation'
      , name: 'EmployeeParticipation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailEmployeeParticipation'
      , name: 'DetailEmployeeParticipation'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Vergoedingen ontbrekende coupon'
    , code: ''
    , documentation: 'Vergoedingen ontbrekende coupon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_IndemnityMissingCouponTitle'
      , name: 'IndemnityMissingCouponTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Vergoedingen ontbrekende coupon'
    , code: '1220'
    , oldCode: '026'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IndemnityMissingCoupon'
      , name: 'IndemnityMissingCoupon'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailIndemnityMissingCoupon'
      , name: 'DetailIndemnityMissingCoupon'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Kosten tax shelter erkende audiovisuele werken'
    , code: ''
    , documentation: 'Kosten tax shelter erkende audiovisuele werken'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWorkTitle'
      , name: 'ExpensesTaxShelterAuthorisedAudiovisualWorkTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Kosten tax shelter erkende audiovisuele werken'
    , code: '1221'
    , oldCode: '027'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'
      , name: 'ExpensesTaxShelterAuthorisedAudiovisualWork'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailExpensesTaxShelterAuthorisedAudiovisualWork'
      , name: 'DetailExpensesTaxShelterAuthorisedAudiovisualWork'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Gewestelijke premies en kapitaal- en interestsubsidies'
    , code: ''
    , documentation: 'Gewestelijke premies en kapitaal- en interestsubsidies'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidiesTitle'
      , name: 'RegionalPremiumCapitalSubsidiesInterestSubsidiesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Gewestelijke premies en kapitaal- en interestsubsidies'
    , code: '1222'
    , oldCode: '024'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'
      , name: 'RegionalPremiumCapitalSubsidiesInterestSubsidies'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailRegionalPremiumCapitalSubsidiesInterestSubsidies'
      , name: 'DetailRegionalPremiumCapitalSubsidiesInterestSubsidies'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Niet-aftrekbare betalingen naar bepaalde Staten'
    , code: ''
    , documentation: 'Niet-aftrekbare betalingen naar bepaalde Staten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_NonDeductiblePaymentsCertainStatesTitle'
      , name: 'NonDeductiblePaymentsCertainStatesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-aftrekbare betalingen naar bepaalde Staten'
    , code: '1223'
    , oldCode: '054'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonDeductiblePaymentsCertainStates'
      , name: 'NonDeductiblePaymentsCertainStates'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailNonDeductiblePaymentsCertainStates'
      , name: 'DetailNonDeductiblePaymentsCertainStates'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere verworpen uitgaven'
    , code: ''
    , documentation: 'Andere verworpen uitgaven'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_OtherDisallowedExpensesTitle'
      , name: 'OtherDisallowedExpensesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere verworpen uitgaven'
    , code: '1239'
    , oldCode: '042'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherDisallowedExpenses'
      , name: 'OtherDisallowedExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DetailOtherDisallowedExpenses'
      , name: 'DetailOtherDisallowedExpenses'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere'
    , code: ''
    , documentation: 'Andere'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_OtherDocumentsTitle'
      , name: 'OtherDocumentsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'Optioneel bij te voegen'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_OtherDocumentsNonMandatoryAnnexe'
      , name: 'OtherDocumentsNonMandatoryAnnexe'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherDocuments'
      , name: 'OtherDocuments'
      , itemType: 'base64BinaryItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'biztax-uploadfield'
    , isRequired: false
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Waardeverminderingen voor waarschijnlijke verliezen en voorzieningen voor risico\'s en kosten'
      , code: '204.3'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Waardeverminderingen voor waarschijnlijke verliezen'
    , code: ''
    , documentation: 'Waardeverminderingen voor waarschijnlijke verliezen'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_WriteDownsDebtClaimsSection'
      , name: 'WriteDownsDebtClaimsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Naam van de schuldenaar'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IdentityTradeDebtor'
      , name: 'IdentityTradeDebtor'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Adres van de schuldenaar'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AddressTradeDebtor'
      , name: 'AddressTradeDebtor'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Schuldvordering'
    , code: ''
    , oldCode: ''
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_DebtClaim'
      , name: 'DebtClaim'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijgestelde waardevermindering'
    , code: '1101'
    , oldCode: '301 316'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_ExemptWriteDownDebtClaim'
      , name: 'ExemptWriteDownDebtClaim'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen'
    , code: ''
    , documentation: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DecreaseExemptWriteDownDebtClaimTitle'
      , name: 'DecreaseExemptWriteDownDebtClaimTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen die overeenstemt met verliezen die tijdens het boekjaar definitief geworden zijn'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'
      , name: 'DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge de gehele of gedeeltelijke inning van de vordering tijdens het boekjaar'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'
      , name: 'DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge een nieuwe schatting van het waarschijnlijke verlies tijdens het boekjaar'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'
      , name: 'DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verhoging vrijgestelde waardevermindering tijdens het boekjaar'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncreaseExemptWriteDownDebtClaim'
      , name: 'IncreaseExemptWriteDownDebtClaim'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verantwoording vrijgestelde waardevermindering'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_JustificationExemptWriteDown'
      , name: 'JustificationExemptWriteDown'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Waardeverminderingen voor waarschijnlijke verliezen en voorzieningen voor risico\'s en kosten'
      , code: '204.3'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Voorzieningen voor risico\'s en kosten'
    , code: ''
    , documentation: 'Voorzieningen voor risico\'s en kosten'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_ProvisionsRisksExpensesSection'
      , name: 'ProvisionsRisksExpensesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Waarschijnlijke kost'
    , code: ''
    , oldCode: ''
    , periods: ['I-End']

    , xbrlDef: {
        id: 'tax-inc_ProbableCost'
      , name: 'ProbableCost'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijgestelde voorziening'
    , code: '1102'
    , oldCode: '302 317'
    , periods: ['I-Start', 'I-End']

    , xbrlDef: {
        id: 'tax-inc_ExemptProvisionRisksExpenses'
      , name: 'ExemptProvisionRisksExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Vermindering van de vrijgestelde voorziening'
    , code: ''
    , documentation: 'Vermindering van de vrijgestelde voorziening'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DecreaseExemptProvisionsRisksExpensesTitle'
      , name: 'DecreaseExemptProvisionsRisksExpensesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'
      , name: 'DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'
      , name: 'DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verhoging van de vrijgestelde voorziening'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncreaseExemptProvisionRisksExpenses'
      , name: 'IncreaseExemptProvisionRisksExpenses'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verantwoording vrijgestelde voorziening'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_JustificationExemptProvision'
      , name: 'JustificationExemptProvision'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling van meerwaarden op zeeschepen'
      , code: '275B'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Meerwaarden op zeeschepen'
    , code: ''
    , documentation: 'Meerwaarden op zeeschepen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsSeaVesselSection'
      , name: 'CapitalGainsSeaVesselSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Omschrijving'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Description'
      , name: 'Description'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van belegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateInvestment'
      , name: 'DateInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van vervreemding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRealisation'
      , name: 'DateRealisation'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verkoopwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedSellingValue'
      , name: 'RealisedSellingValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscaal aangenomen afschrijvingen of waardeverminderingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxAcceptedDepreciationsWriteDownsInvestment'
      , name: 'TaxAcceptedDepreciationsWriteDownsInvestment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscale nettowaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FiscalNetWorth'
      , name: 'FiscalNetWorth'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGain'
      , name: 'RealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Gespreide belasting van meerwaarden op bepaalde effecten'
      , code: '275K'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Gespreid te belasten meerwaarden op bepaalde effecten, uitgegeven of gewaarborgd door openbare instellingen'
    , code: ''
    , documentation: 'Gespreid te belasten meerwaarden op bepaalde effecten, uitgegeven of gewaarborgd door openbare instellingen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsSpecificSecuritiesSection'
      , name: 'CapitalGainsSpecificSecuritiesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Omschrijving'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Description'
      , name: 'Description'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van belegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateInvestment'
      , name: 'DateInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van vervreemding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRealisation'
      , name: 'DateRealisation'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verkoopprijs'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_SellingPrice'
      , name: 'SellingPrice'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanschaffingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionValue'
      , name: 'AcquisitionValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscaal aangenomen waardeverminderingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxAcceptedWriteDownsInvestment'
      , name: 'TaxAcceptedWriteDownsInvestment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscale nettowaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FiscalNetWorth'
      , name: 'FiscalNetWorth'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGain'
      , name: 'RealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Monetair gedeelte verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_MonetaryFractionRealisedCapitalGain'
      , name: 'MonetaryFractionRealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
      , name: 'NonMonetaryFractionRealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Gespreid te belasten meerwaarden op materiële en immateriële vaste activa'
      , code: '276K'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Meerwaarden op materiële en immateriële vaste activa'
    , code: ''
    , documentation: 'Meerwaarden op materiële en immateriële vaste activa'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsTangibleIntangibleFixedAssetsSection'
      , name: 'CapitalGainsTangibleIntangibleFixedAssetsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Omschrijving'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Description'
      , name: 'Description'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van belegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateInvestment'
      , name: 'DateInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van vervreemding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRealisation'
      , name: 'DateRealisation'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verkoopwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedSellingValue'
      , name: 'RealisedSellingValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Ontvangen vergoeding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReceivedCompensation'
      , name: 'ReceivedCompensation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kosten van vervreemding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CostRealisation'
      , name: 'CostRealisation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscaal aangenomen afschrijvingen of waardeverminderingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxAcceptedDepreciationsWriteDownsInvestment'
      , name: 'TaxAcceptedDepreciationsWriteDownsInvestment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscale nettowaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FiscalNetWorth'
      , name: 'FiscalNetWorth'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGain'
      , name: 'RealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Monetair gedeelte verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_MonetaryFractionRealisedCapitalGain'
      , name: 'MonetaryFractionRealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
      , name: 'NonMonetaryFractionRealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijwillig verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_VoluntaryRealisedCapitalGain'
      , name: 'VoluntaryRealisedCapitalGain'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Investeringsverbintenis activa te beleggen in gebouwd onroerende goederen, vaartuigen of vliegtuigen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentCommitment'
      , name: 'InvestmentCommitment'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling van meerwaarden op bedrijfsvoertuigen'
      , code: '276N'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Meerwaarden op bedrijfsvoertuigen'
    , code: ''
    , documentation: 'Meerwaarden op bedrijfsvoertuigen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsCorporateVehiclesSection'
      , name: 'CapitalGainsCorporateVehiclesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Omschrijving'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Description'
      , name: 'Description'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van belegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateInvestment'
      , name: 'DateInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van vervreemding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRealisation'
      , name: 'DateRealisation'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van ontvangst schadevergoeding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateCompensationReceipt'
      , name: 'DateCompensationReceipt'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verkoopwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedSellingValue'
      , name: 'RealisedSellingValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Ontvangen vergoeding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReceivedCompensation'
      , name: 'ReceivedCompensation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kosten van vervreemding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CostRealisation'
      , name: 'CostRealisation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscaal aangenomen afschrijvingen of waardeverminderingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxAcceptedDepreciationsWriteDownsInvestment'
      , name: 'TaxAcceptedDepreciationsWriteDownsInvestment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscale nettowaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FiscalNetWorth'
      , name: 'FiscalNetWorth'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGain'
      , name: 'RealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijwillig verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_VoluntaryRealisedCapitalGain'
      , name: 'VoluntaryRealisedCapitalGain'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling van meerwaarden op binnenschepen voor commerciële vaart'
      , code: '276P'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Meerwaarden op binnenschepen voor de commerciële vaart'
    , code: ''
    , documentation: 'Meerwaarden op binnenschepen voor de commerciële vaart'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsRiverVesselSection'
      , name: 'CapitalGainsRiverVesselSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Omschrijving'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Description'
      , name: 'Description'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van belegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateInvestment'
      , name: 'DateInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van vervreemding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRealisation'
      , name: 'DateRealisation'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van ontvangst schadevergoeding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateCompensationReceipt'
      , name: 'DateCompensationReceipt'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verkoopwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedSellingValue'
      , name: 'RealisedSellingValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Ontvangen vergoeding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReceivedCompensation'
      , name: 'ReceivedCompensation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kosten van vervreemding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CostRealisation'
      , name: 'CostRealisation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscaal aangenomen afschrijvingen of waardeverminderingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxAcceptedDepreciationsWriteDownsInvestment'
      , name: 'TaxAcceptedDepreciationsWriteDownsInvestment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Fiscale nettowaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FiscalNetWorth'
      , name: 'FiscalNetWorth'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGain'
      , name: 'RealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijwillig verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_VoluntaryRealisedCapitalGain'
      , name: 'VoluntaryRealisedCapitalGain'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling van meerwaarden op zeeschepen'
      , code: '275B'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Herbeleggingen in zeeschepen, delen in mede-eigendom in zeeschepen, in scheepsaandelen of in aandelen van een vennootschap-scheepsexploitant met maatschappelijke zetel gevestigd in de EU'
    , code: ''
    , documentation: 'Herbeleggingen in zeeschepen, delen in mede-eigendom in zeeschepen, in scheepsaandelen of in aandelen van een vennootschap-scheepsexploitant met maatschappelijke zetel gevestigd in de EU'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentsSeaVesselSection'
      , name: 'ReinvestmentsSeaVesselSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGain'
      , name: 'RealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Omschrijving in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DescriptionReinvestment'
      , name: 'DescriptionReinvestment'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateReinvestment'
      , name: 'DateReinvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bedrag in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentValue'
      , name: 'ReinvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Gespreide belasting van meerwaarden op bepaalde effecten'
      , code: '275K'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Herbeleggingen in bepaalde effecten'
    , code: ''
    , documentation: 'Herbeleggingen in bepaalde effecten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentsSpecificSecuritiesSection'
      , name: 'ReinvestmentsSpecificSecuritiesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
      , name: 'NonMonetaryFractionRealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Omschrijving in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DescriptionReinvestment'
      , name: 'DescriptionReinvestment'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateReinvestment'
      , name: 'DateReinvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bedrag in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentValue'
      , name: 'ReinvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Gespreid te belasten meerwaarden op materiële en immateriële vaste activa'
      , code: '276K'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Herbeleggingen in afschrijfbare materiële of immateriële vaste activa'
    , code: ''
    , documentation: 'Herbeleggingen in afschrijfbare materiële of immateriële vaste activa'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentsTangibleIntangibleFixedAssetsSection'
      , name: 'ReinvestmentsTangibleIntangibleFixedAssetsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
      , name: 'NonMonetaryFractionRealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Omschrijving in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DescriptionReinvestment'
      , name: 'DescriptionReinvestment'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateReinvestment'
      , name: 'DateReinvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bedrag in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentValue'
      , name: 'ReinvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling van meerwaarden op bedrijfsvoertuigen'
      , code: '276N'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Herbeleggingen in ecologische bedrijfsvoertuigen'
    , code: ''
    , documentation: 'Herbeleggingen in ecologische bedrijfsvoertuigen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentsEcologicalCorporateVehiclesSection'
      , name: 'ReinvestmentsEcologicalCorporateVehiclesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGain'
      , name: 'RealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Omschrijving in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DescriptionReinvestment'
      , name: 'DescriptionReinvestment'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateReinvestment'
      , name: 'DateReinvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bedrag in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentValue'
      , name: 'ReinvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling van meerwaarden op binnenschepen voor commerciële vaart'
      , code: '276P'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Herbeleggingen in ecologische binnenschepen voor de commerciële vaart'
    , code: ''
    , documentation: 'Herbeleggingen in ecologische binnenschepen voor de commerciële vaart'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentsEcologicalRiverVesselCommercialTradeSection'
      , name: 'ReinvestmentsEcologicalRiverVesselCommercialTradeSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedCapitalGain'
      , name: 'RealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Omschrijving in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DescriptionReinvestment'
      , name: 'DescriptionReinvestment'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateReinvestment'
      , name: 'DateReinvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bedrag in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentValue'
      , name: 'ReinvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Gespreide belasting van meerwaarden op bepaalde effecten'
      , code: '275K'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Voor het belastbare tijdperk belastbare deel van de meerwaarde op bepaalde effecten'
    , code: ''
    , documentation: 'Voor het belastbare tijdperk belastbare deel van de meerwaarde op bepaalde effecten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_TaxablePortionCapitalGainsSpecificSecuritiesSection'
      , name: 'TaxablePortionCapitalGainsSpecificSecuritiesSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
      , name: 'NonMonetaryFractionRealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bedrag in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentValue'
      , name: 'ReinvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Te belasten deel van de meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxableCapitalGain'
      , name: 'TaxableCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Gespreid te belasten meerwaarden op materiële en immateriële vaste activa'
      , code: '276K'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Voor het belastbare tijdperk belastbare deel van de meerwaarde op materiële en immateriële vaste activa'
    , code: ''
    , documentation: 'Voor het belastbare tijdperk belastbare deel van de meerwaarde op materiële en immateriële vaste activa'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_TaxablePortionCapitalGainsTangibleIntangibleFixedAssetsSection'
      , name: 'TaxablePortionCapitalGainsTangibleIntangibleFixedAssetsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
      , name: 'NonMonetaryFractionRealisedCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verkoopwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_RealisedSellingValue'
      , name: 'RealisedSellingValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Ontvangen vergoeding'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReceivedCompensation'
      , name: 'ReceivedCompensation'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bedrag in aanmerking te nemen herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReinvestmentValue'
      , name: 'ReinvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'In aanmerking te nemen afschrijving herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DepreciationsReinvestment'
      , name: 'DepreciationsReinvestment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van vervreemding herbelegging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRealisationReinvestment'
      , name: 'DateRealisationReinvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Te belasten deel van de meerwaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxableCapitalGain'
      , name: 'TaxableCapitalGain'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Aftrek voor risicokapitaal'
      , code: '275C'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Berekening aftrek voor risicokapitaal'
    , code: ''
    , documentation: 'Berekening aftrek voor risicokapitaal'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_CalculationAllowanceCorporateEquitySection'
      , name: 'CalculationAllowanceCorporateEquitySection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Eigen vermogen'
    , code: '8001'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_Equity'
      , name: 'Equity'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Bestanddelen af te trekken van het eigen vermogen'
    , code: '8030'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_DeductionsEquityAllowanceCorporateEquity'
      , name: 'DeductionsEquityAllowanceCorporateEquity'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Eigen aandelen'
    , code: '8011'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_OwnSharesFiscalValue'
      , name: 'OwnSharesFiscalValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Financiële vaste activa die uit deelnemingen en andere aandelen bestaan'
    , code: '8012'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_FinancialFixedAssetsParticipationsOtherShares'
      , name: 'FinancialFixedAssetsParticipationsOtherShares'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aandelen van beleggingsvennootschappen'
    , code: '8013'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_SharesInvestmentCorporations'
      , name: 'SharesInvestmentCorporations'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Inrichtingen gelegen in een land met verdrag'
    , code: '8014'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_BranchesCountryTaxTreaty'
      , name: 'BranchesCountryTaxTreaty'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Onroerende goederen gelegen in een land met verdrag'
    , code: '8015'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_ImmovablePropertyCountryTaxTreaty'
      , name: 'ImmovablePropertyCountryTaxTreaty'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Materiële vaste activa in zover de erop betrekking hebbende kosten onredelijk zijn'
    , code: '8016'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'
      , name: 'TangibleFixedAssetsUnreasonableRelatedCosts'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bestanddelen die als belegging worden gehouden en geen belastbaar periodiek inkomen voortbrengen'
    , code: '8017'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_InvestmentsNoPeriodicalIncome'
      , name: 'InvestmentsNoPeriodicalIncome'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Onroerende goederen waarvan bedrijfsleiders het gebruik hebben'
    , code: '8018'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_ImmovablePropertyUseManager'
      , name: 'ImmovablePropertyUseManager'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitgedrukte maar niet-verwezenlijkte meerwaarden'
    , code: '8019'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_UnrealisedExpressedCapitalGains'
      , name: 'UnrealisedExpressedCapitalGains'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Belastingkrediet voor onderzoek en ontwikkeling'
    , code: '8020'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'
      , name: 'TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Kapitaalsubsidies'
    , code: '8021'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_InvestmentGrants'
      , name: 'InvestmentGrants'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voorraadactualisering erkende diamanthandelaars'
    , code: '8022'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_ActualisationStockRecognisedDiamondTraders'
      , name: 'ActualisationStockRecognisedDiamondTraders'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Ten name van de hoofdzetel ontleende middelen met betrekking tot dewelke de interesten ten laste van het belastbaar resultaat van de Belgische inrichting wordt gelegd'
    , code: '8023'
    , oldCode: ''
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'
      , name: 'BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden'
    , code: '8040'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'
      , name: 'MovementEquityAfterDeductionsAllowanceCorporateEquity'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Risicokapitaal van het belastbaar tijdperk'
    , code: '8050'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'
      , name: 'AllowanceCorporateEquityCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Aftrek risicokapitaal die voor het aanslagjaar in principe aftrekbaar is'
    , code: ''
    , documentation: 'Aftrek risicokapitaal die voor het aanslagjaar in principe aftrekbaar is'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYearTitle'
      , name: 'DeductibleAllowanceCorporateEquityCurrentAssessmentYearTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-label'

    , title: 'Indien het belastbare tijdperk niet gelijk is aan 12 maanden of voor het eerste belastbare tijdperk moet het tarief worden vermenigvuldigd met een breuk waarvan de teller gelijk is aan het totaal aantal dagen van het belastbare tijdperk en de noemer gelijk is aan 365.'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'tax-inc_ApplicableRateAllowanceCorporateEquity'
      , name: 'ApplicableRateAllowanceCorporateEquity'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-field'

    , title: 'In principe aftrekbaar'
    , code: '8051'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'
      , name: 'DeductibleAllowanceCorporateEquityCurrentAssessmentYear'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken'
    , code: '8052'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'
      , name: 'DeductionAllowanceCorporateEquityCurrentAssessmentYear'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Saldo van de aftrek voor risicokapitaal van het huidig aanslagjaar dat overdraagbaar is naar latere aanslagjaren'
    , code: '8053'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'
      , name: 'CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Saldo van de aftrek voor risicokapitaal dat werd gevormd tijdens vorige aanslagjaren'
    , code: '8060'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'
      , name: 'AllowanceCorporateEquityPreviousAssessmentYears'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aftrek voor risicokapitaal van vorige aanslagjaren dat werkelijk wordt afgetrokken'
    , code: '8061'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'
      , name: 'DeductionAllowanceCorporateEquityPreviousAssessmentYears'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken'
    , code: '1435'
    , oldCode: '104 103'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AllowanceCorporateEquity'
      , name: 'AllowanceCorporateEquity'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar latere aanslagjaren'
    , code: '1712'
    , oldCode: '332'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'
      , name: 'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Uitleg wijzigingen tijdens het belastbare tijdperk van het eigen vermogen'
    , code: ''
    , documentation: 'Uitleg wijzigingen tijdens het belastbare tijdperk van het eigen vermogen'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_DetailMovementEquityAfterDeductionsAllowanceCorporateEquitySection'
      , name: 'DetailMovementEquityAfterDeductionsAllowanceCorporateEquitySection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Elke beweging moet worden vermenigvuldigd met het aantal maanden die nog blijven lopen tot op het einde van het belastbare tijdperk en gedeeld door het totale aantal maanden van het belastbare tijdperk en waarbij de wijzigingen geacht worden te hebben plaatsgevonden de eerste dag van de kalendermaand volgend op die waarin ze zich hebben voorgedaan. Indien het belastbaar tijdperk geen geheel aantal maanden bevat moet de voormelde breuk (teller en noemer) drie cijfers na de komma bevatten'
    , code: ''
    , documentation: 'Elke beweging moet worden vermenigvuldigd met het aantal maanden die nog blijven lopen tot op het einde van het belastbare tijdperk en gedeeld door het totale aantal maanden van het belastbare tijdperk en waarbij de wijzigingen geacht worden te hebben plaatsgevonden de eerste dag van de kalendermaand volgend op die waarin ze zich hebben voorgedaan. Indien het belastbaar tijdperk geen geheel aantal maanden bevat moet de voormelde breuk (teller en noemer) drie cijfers na de komma bevatten'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ApplicableDurationMovementEquityAllowanceCorporateEquity'
      , name: 'ApplicableDurationMovementEquityAllowanceCorporateEquity'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Berekening als gewogen gemiddelde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CalculationWeightedAverageMovementEquityAfterDeductionsAllowanceCorporateEquity'
      , name: 'CalculationWeightedAverageMovementEquityAfterDeductionsAllowanceCorporateEquity'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden'
    , code: '8040'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'
      , name: 'MovementEquityAfterDeductionsAllowanceCorporateEquity'
      , itemType: 'monetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: -99999999999999.99, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Aftrek voor octrooi-inkomsten'
      , code: '275P'

      , items: [

    { xtype: 'biztax-field'

    , title: 'Inkomsten of het gedeelte van inkomsten uit octrooien die geheel of gedeeltelijk door de vennootschap werden ontwikkeld en waarvan zij octrooihouder is'
    , code: '8100'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped'
      , name: 'IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Gecorrigeerde aftrekbare inkomsten of het gedeelte van inkomsten uit octrooien die de vennootschap heeft verworven van derden'
    , code: '8120'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty'
      , name: 'CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Inkomsten of het gedeelte van inkomsten uit octrooien die de vennootschap heeft verworven van derden'
    , code: '8111'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncomePatentsWhollyPartiallyObtainedThirdParty'
      , name: 'IncomePatentsWhollyPartiallyObtainedThirdParty'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vergoedingen of het gedeelte van vergoedingen die aan derden zijn verschuldigd voor de octrooien die de vennootschap heeft verworven'
    , code: '8112'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CompensationOwedThirdPartiesPertainingPatents'
      , name: 'CompensationOwedThirdPartiesPertainingPatents'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Afschrijvingen of het gedeelte van de afschrijvingen die zijn toegepast op de aanschaffings- of beleggingswaarde van de door de vennootschap verworven octrooien'
    , code: '8113'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AmortisationAcquisitionInvestmentValuePatents'
      , name: 'AmortisationAcquisitionInvestmentValuePatents'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Berekeningsgrondslag van de aftrek voor octrooi-inkomsten'
    , code: '8130'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CalculationBasisDeductionPatentsIncome'
      , name: 'CalculationBasisDeductionPatentsIncome'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aftrek voor octrooi-inkomsten'
    , code: '8140'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductibleDeductionPatentsIncome'
      , name: 'DeductibleDeductionPatentsIncome'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Investeringsreserve'
      , code: '275R'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Vrijgestelde investeringsreserve'
    , code: ''
    , documentation: 'Vrijgestelde investeringsreserve'
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_ExemptInvestmentReserveSection'
      , name: 'ExemptInvestmentReserveSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve'
    , code: '8201'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncreaseTaxableReservesInvestmentReserveExcluded'
      , name: 'IncreaseTaxableReservesInvestmentReserveExcluded'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Verminderingen op deze aangroei'
    , code: '8220'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'instant'

    , xbrlDef: {
        id: 'tax-inc_ReductionsIncreaseTaxableReservesInvestmentReserveExcluded'
      , name: 'ReductionsIncreaseTaxableReservesInvestmentReserveExcluded'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Meerwaarden op aandelen'
    , code: '8211'
    , oldCode: '006 (1)'
    , periods: ['I-Start']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsSharesInvestmentReserve'
      , name: 'CapitalGainsSharesInvestmentReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gedeelte van de meerwaarden op voertuigen dat niet als winst wordt aangemerkt'
    , code: '8212'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CapitalGainsVehiclesNoProfit'
      , name: 'CapitalGainsVehiclesNoProfit'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vermindering van het gestort kapitaal'
    , code: '8213'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DecreasePaidUpCapital'
      , name: 'DecreasePaidUpCapital'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vermeerdering van bepaalde vorderingen van de vennootschap'
    , code: '8214'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncreaseParticularDebtClaims'
      , name: 'IncreaseParticularDebtClaims'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum'
    , code: '8230'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CorrectedIncreaseTaxableReservesInvestmentReserveExcluded'
      , name: 'CorrectedIncreaseTaxableReservesInvestmentReserveExcluded'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aangroei van de belaste reserves ten opzichte van de belaste reserves op het einde van het vorig belastbaar tijdperk waarvoor een investeringsreserve werd genoten'
    , code: '8231'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncreaseTaxableReservesInvestmentReserveIncluded'
      , name: 'IncreaseTaxableReservesInvestmentReserveIncluded'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Maximaal vrijgestelde reserve van het belastbare tijdperk'
    , code: '8240'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptibleInvestmentReserve'
      , name: 'ExemptibleInvestmentReserve'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gevraagde vrijgestelde investeringsreserve van het belastbare tijdperk'
    , code: '8250'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptInvestmentReserveCurrentTaxPeriod'
      , name: 'ExemptInvestmentReserveCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Investeringen in afschrijfbare materiële of immateriële vaste activa'
    , code: ''
    , documentation: 'Investeringen in afschrijfbare materiële of immateriële vaste activa'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_InvestmentsDepreciableTangibleIntangibleFixedAssetsSection'
      , name: 'InvestmentsDepreciableTangibleIntangibleFixedAssetsSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Vrijgestelde reserve van de laatste drie belastbare tijdperken, met inbegrip van het huidige belastbare tijdperk'
    , code: ''
    , documentation: 'Vrijgestelde reserve van de laatste drie belastbare tijdperken, met inbegrip van het huidige belastbare tijdperk'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_ExemptInvestmentReserveLastThreeTaxPeriodsTitle'
      , name: 'ExemptInvestmentReserveLastThreeTaxPeriodsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Vrijgestelde investeringsreserve'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptInvestmentReserveOverview'
      , name: 'ExemptInvestmentReserveOverview'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Einddatum van de investering'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FinalDateInvestment'
      , name: 'FinalDateInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Investeringen in materiële en immateriële vaste activa'
    , code: ''
    , documentation: 'Investeringen in materiële en immateriële vaste activa'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentsTangibleIntangibleFixedAssetsTitle'
      , name: 'InvestmentsTangibleIntangibleFixedAssetsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Investeringen vermeld op de vorige opgave'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentsPreviousInvestmentReserveTaxPeriod'
      , name: 'InvestmentsPreviousInvestmentReserveTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Investeringen van het huidig belastbaar tijdperk'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentCurrentTaxPeriod'
      , name: 'InvestmentCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nog te investeren saldo'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestBalance'
      , name: 'InvestBalance'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Investeringsaftrek'
      , code: '275U'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling'
    , code: ''
    , documentation: 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentSection'
      , name: 'InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Eenmalige investeringsaftrek'
    , code: ''
    , documentation: 'Eenmalige investeringsaftrek'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoTitle'
      , name: 'InvestmentDeductionOneGoTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Investeringen in nieuwe vaste activa door alle vennootschappen'
    , code: ''
    , documentation: 'Investeringen in nieuwe vaste activa door alle vennootschappen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoNewFixedAssetsAllCompaniesTitle'
      , name: 'InvestmentDeductionOneGoNewFixedAssetsAllCompaniesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalig'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveOneGo'
      , name: 'InvestmentIncentiveOneGo'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Investeringen door KMO\'s'
    , code: ''
    , documentation: 'Investeringen door KMO\'s'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoSMEsTitle'
      , name: 'InvestmentDeductionOneGoSMEsTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalig'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveOneGo'
      , name: 'InvestmentIncentiveOneGo'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Investeringen in zeeschepen door vennootschappen die uitsluitend winst uit zeescheepvaart verkrijgen'
    , code: ''
    , documentation: 'Investeringen in zeeschepen door vennootschappen die uitsluitend winst uit zeescheepvaart verkrijgen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoInvestmentsSeaVesselsShippingCompaniesTitle'
      , name: 'InvestmentDeductionOneGoInvestmentsSeaVesselsShippingCompaniesTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalig'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveOneGo'
      , name: 'InvestmentIncentiveOneGo'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Gespreide investeringsaftrek'
    , code: ''
    , documentation: 'Gespreide investeringsaftrek'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionSpreadTitle'
      , name: 'InvestmentDeductionSpreadTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde, afschrijfbaar'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValueSpread'
      , name: 'AcquisitionInvestmentValueSpread'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanneembare afschrijvingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EligibleDepreciations'
      , name: 'EligibleDepreciations'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gespreid'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveSpread'
      , name: 'InvestmentIncentiveSpread'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalige en gespreide investeringsaftrek'
    , code: '8310'
    , oldCode: '884'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoSpreadCurrentTaxPeriod'
      , name: 'InvestmentDeductionOneGoSpreadCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gespreide aftrekken voor investeringen van vorige belastbare tijdperken'
    , code: '8311'
    , oldCode: '885'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionSpreadPreviousTaxPeriods'
      , name: 'InvestmentDeductionSpreadPreviousTaxPeriods'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Gecumuleerde vroegere nog niet afgetrokken investeringsaftrekken'
    , code: '8330'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_PreviousCarryOverInvestmentDeduction'
      , name: 'PreviousCarryOverInvestmentDeduction'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen'
    , code: '8321'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PreviousCarryOverInvestmentDeductionSeaVessels'
      , name: 'PreviousCarryOverInvestmentDeductionSeaVessels'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen'
    , code: '8322'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherPreviousCarryOverInvestmentDeduction'
      , name: 'OtherPreviousCarryOverInvestmentDeduction'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'In beginsel aftrekbare investeringsaftrek'
    , code: '8340'
    , oldCode: '887'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BasicTaxableInvestmentDeduction'
      , name: 'BasicTaxableInvestmentDeduction'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk'
    , code: '1437.1'
    , oldCode: '888'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment'
      , name: 'DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Over te dragen naar volgende belastbare tijdperken'
    , code: '8370'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CarryOverInvestmentDeduction'
      , name: 'CarryOverInvestmentDeduction'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Over te dragen investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen'
    , code: '8361'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverInvestmentDeductionSeaVessels'
      , name: 'CarryOverInvestmentDeductionSeaVessels'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Over te dragen investeringsaftrekken met betrekking tot andere investeringen'
    , code: '8362'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherCarryOverInvestmentDeduction'
      , name: 'OtherCarryOverInvestmentDeduction'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling'
    , code: ''
    , documentation: 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionCompatibleTaxCreditResearchDevelopmentSection'
      , name: 'InvestmentDeductionCompatibleTaxCreditResearchDevelopmentSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Eenmalige investeringsaftrek'
    , code: ''
    , documentation: 'Eenmalige investeringsaftrek'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoCompatibleTaxCreditResearchDevelopmentTitle'
      , name: 'InvestmentDeductionOneGoCompatibleTaxCreditResearchDevelopmentTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Investeringen in nieuwe vaste activa door alle vennootschappen'
    , code: ''
    , documentation: 'Investeringen in nieuwe vaste activa door alle vennootschappen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoNewFixedAssetsAllCompaniesCompatibleTaxCreditResearchDevelopmentTitle'
      , name: 'InvestmentDeductionOneGoNewFixedAssetsAllCompaniesCompatibleTaxCreditResearchDevelopmentTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalig'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveOneGo'
      , name: 'InvestmentIncentiveOneGo'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Investeringen door KMO\'s'
    , code: ''
    , documentation: 'Investeringen door KMO\'s'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoSMEsCompatibleTaxCreditResearchDevelopmentTitle'
      , name: 'InvestmentDeductionOneGoSMEsCompatibleTaxCreditResearchDevelopmentTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalig'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveOneGo'
      , name: 'InvestmentIncentiveOneGo'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Investeringen in zeeschepen door vennootschappen die uitsluitend winst uit zeescheepvaart verkrijgen'
    , code: ''
    , documentation: 'Investeringen in zeeschepen door vennootschappen die uitsluitend winst uit zeescheepvaart verkrijgen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoInvestmentsSeaVesselsShippingCompaniesCompatibleTaxCreditResearchDevelopmentTitle'
      , name: 'InvestmentDeductionOneGoInvestmentsSeaVesselsShippingCompaniesCompatibleTaxCreditResearchDevelopmentTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalig'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveOneGo'
      , name: 'InvestmentIncentiveOneGo'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalige investeringsaftrek'
    , code: '8410'
    , oldCode: '879'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment'
      , name: 'InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gespreide investeringsaftrek van vorige belastbare tijdperken voor andere investeringen dan investeringen in onderzoek en ontwikkeling'
    , code: '8411'
    , oldCode: '895'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PreviousCarryOverSpreadInvestmentDeductionNoResearchDevelopmentCompatibleTaxCreditResearchDevelopment'
      , name: 'PreviousCarryOverSpreadInvestmentDeductionNoResearchDevelopmentCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Gecumuleerde vroegere nog niet afgetrokken investeringsaftrekken'
    , code: '8430'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , name: 'PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen'
    , code: '8421'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment'
      , name: 'PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen'
    , code: '8422'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , name: 'OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Investeringsaftrekken die niet konden worden afgetrokken voor de drie voorafgaande aanslagjaren met betrekking tot de eenmalige investeringsaftrek voor octrooien en de eenmalige en de gespreide investeringsaftrekken voor investeringen in onderzoek en ontwikkeling'
    , code: '8423'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment'
      , name: 'PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'In beginsel aftrekbare investeringsaftrek'
    , code: '8440'
    , oldCode: '897'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , name: 'BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk'
    , code: '1437.2'
    , oldCode: '898'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , name: 'DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Over te dragen naar volgende belastbare tijdperken'
    , code: '8470'
    , documentation: ''
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , name: 'CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Over te dragen investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen'
    , code: '8461'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment'
      , name: 'CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Over te dragen investeringsaftrekken met betrekking tot andere investeringen'
    , code: '8462'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment'
      , name: 'CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Belastingkrediet voor onderzoek en ontwikkeling'
      , code: '275W'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Eenmalig belastingkrediet voor onderzoek en ontwikkeling'
    , code: ''
    , documentation: 'Eenmalig belastingkrediet voor onderzoek en ontwikkeling'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopmentOneGoTitle'
      , name: 'TaxCreditResearchDevelopmentOneGoTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eenmalig'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveOneGo'
      , name: 'InvestmentIncentiveOneGo'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Gespreid belastingkrediet voor onderzoek en ontwikkeling'
    , code: ''
    , documentation: 'Gespreid belastingkrediet voor onderzoek en ontwikkeling'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopmentSpreadTitle'
      , name: 'TaxCreditResearchDevelopmentSpreadTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanneembare afschrijvingen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EligibleDepreciations'
      , name: 'EligibleDepreciations'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gespreid'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_InvestmentIncentiveSpread'
      , name: 'InvestmentIncentiveSpread'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gespreid belastingkrediet voor onderzoek en ontwikkeling voor investeringen van vorige belastbare tijdperken'
    , code: '8501'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_SpreadTaxCreditResearchDevelopmentPreviousTaxPeriods'
      , name: 'SpreadTaxCreditResearchDevelopmentPreviousTaxPeriods'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanvullend belastingkrediet voor onderzoek en ontwikkeling'
    , code: '8502'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AdditionalTaxCreditResearchDevelopment'
      , name: 'AdditionalTaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling van het belastbaar tijdperk'
    , code: '8510'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod'
      , name: 'ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Belastingkrediet voor onderzoek en ontwikkeling van vorige belastbare tijdperken'
    , code: ''
    , documentation: 'Belastingkrediet voor onderzoek en ontwikkeling van vorige belastbare tijdperken'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopmentPreviousTaxPeriodsTitle'
      , name: 'TaxCreditResearchDevelopmentPreviousTaxPeriodsTitle'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Verrekenbaar'
    , code: '8521'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods'
      , name: 'ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Terug te betalen voor het huidig aanslagjaar'
    , code: '1850'
    , oldCode: '198'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
      , name: 'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Verrekenbaar en niet-terugbetaalbaar belastingkrediet voor onderzoek en ontwikkeling van vorige belastbare tijdperken'
    , code: '8530'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods'
      , name: 'ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling'
    , code: '1833'
    , oldCode: '184'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopment'
      , name: 'TaxCreditResearchDevelopment'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Terug te betalen voor het huidig aanslagjaar'
    , code: '1850'
    , oldCode: '198'
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
      , name: 'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling bijkomend personeel'
      , code: '276T'

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Gepresteerde arbeidsdagen'
    , code: ''
    , documentation: 'Gepresteerde arbeidsdagen'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_CalculationPerformedWorkingDaysSection'
      , name: 'CalculationPerformedWorkingDaysSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Gepresteerde dagen kalenderjaar-1'
    , code: ''
    , documentation: 'Gepresteerde dagen kalenderjaar-1'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CalculationPerformedWorkingDaysCalendarYear-1Title'
      , name: 'CalculationPerformedWorkingDaysCalendarYear-1Title'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Arbeidsregeling, aantal dagen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_LabourRegulationNumberDays'
      , name: 'LabourRegulationNumberDays'
      , itemType: 'positiveIntegerMin1Max6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 1, decimalPrecision: 0, minValue: 1, maxValue: 6
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Arbeidsregeling, totaal aantal dagen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_LabourRegulationNumberDaysYear'
      , name: 'LabourRegulationNumberDaysYear'
      , itemType: 'nonNegativeInteger6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 6, decimalPrecision: 0, minValue: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Brutoloon'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_GrossIncome'
      , name: 'GrossIncome'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Arbeidsdagen, alle personeelsleden'
    , code: ''
    , documentation: 'Arbeidsdagen, alle personeelsleden'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_WorkingDaysAllPersonnel'
      , name: 'WorkingDaysAllPersonnel'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Bezoldigde dagen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WorkingDaysPaid'
      , name: 'WorkingDaysPaid'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vakantiedagen arbeiders'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_HolidaysWorkers'
      , name: 'HolidaysWorkers'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Correcties'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CorrectionsWorkingDays'
      , name: 'CorrectionsWorkingDays'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bezoldigde uren, per kwartaal'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PaidHoursQuarter'
      , name: 'PaidHoursQuarter'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemiddeld dagloon'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AverageDayPay'
      , name: 'AverageDayPay'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemiddeld uurloon'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AverageHourPay'
      , name: 'AverageHourPay'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Arbeidsdagen, personeelsleden met een laag loon'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WorkingDaysLowWages'
      , name: 'WorkingDaysLowWages'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Gepresteerde dagen kalenderjaar-2'
    , code: ''
    , documentation: 'Gepresteerde dagen kalenderjaar-2'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CalculationPerformedWorkingDaysCalendarYear-2Title'
      , name: 'CalculationPerformedWorkingDaysCalendarYear-2Title'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Arbeidsregeling, aantal dagen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_LabourRegulationNumberDays'
      , name: 'LabourRegulationNumberDays'
      , itemType: 'positiveIntegerMin1Max6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 1, decimalPrecision: 0, minValue: 1, maxValue: 6
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Arbeidsregeling, totaal aantal dagen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_LabourRegulationNumberDaysYear'
      , name: 'LabourRegulationNumberDaysYear'
      , itemType: 'nonNegativeInteger6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 6, decimalPrecision: 0, minValue: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Brutoloon'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_GrossIncome'
      , name: 'GrossIncome'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Arbeidsdagen, alle personeelsleden'
    , code: ''
    , documentation: 'Arbeidsdagen, alle personeelsleden'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_WorkingDaysAllPersonnel'
      , name: 'WorkingDaysAllPersonnel'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Bezoldigde dagen'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WorkingDaysPaid'
      , name: 'WorkingDaysPaid'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vakantiedagen arbeiders'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_HolidaysWorkers'
      , name: 'HolidaysWorkers'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Correcties'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CorrectionsWorkingDays'
      , name: 'CorrectionsWorkingDays'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bezoldigde uren, per kwartaal'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_PaidHoursQuarter'
      , name: 'PaidHoursQuarter'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemiddeld dagloon'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AverageDayPay'
      , name: 'AverageDayPay'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemiddeld uurloon'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AverageHourPay'
      , name: 'AverageHourPay'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Arbeidsdagen, personeelsleden met een laag loon'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_WorkingDaysLowWages'
      , name: 'WorkingDaysLowWages'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Vrijstelling voor bijkomend personeel'
    , code: ''
    , documentation: 'Vrijstelling voor bijkomend personeel'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CalculationExemptionAdditionalPersonnelSection'
      , name: 'CalculationExemptionAdditionalPersonnelSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Beweging alle personeelsleden, kalenderjaar-1'
    , code: '8601'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_MovementAllPersonnelCalendarYear-1'
      , name: 'MovementAllPersonnelCalendarYear-1'
      , itemType: 'integer6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 6, decimalPrecision: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aangroei van de personeelsleden met een laag loon, kalenderjaar-1'
    , code: '8602'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_IncreasePersonnelLowWageCalendarYear-1'
      , name: 'IncreasePersonnelLowWageCalendarYear-1'
      , itemType: 'nonNegativeInteger6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 6, decimalPrecision: 0, minValue: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aantal personeelsleden waarvoor de vrijstelling kan bekomen worden'
    , code: '8603'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EmployeesEligibleExemptionAdditionalPersonnel'
      , name: 'EmployeesEligibleExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeDecimal6D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 8, decimalPrecision: 2, minValue: 0, maxValue: 999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling'
    , code: '8604'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptionAdditionalPersonnel'
      , name: 'ExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Terug te nemen vrijstelling'
    , code: ''
    , documentation: 'Terug te nemen vrijstelling'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CalculationReversalPreviousExemptionAdditionalPersonnelSection'
      , name: 'CalculationReversalPreviousExemptionAdditionalPersonnelSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-field'

    , title: 'Beweging alle personeelsleden, kalenderjaar-1'
    , code: '8601'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_MovementAllPersonnelCalendarYear-1'
      , name: 'MovementAllPersonnelCalendarYear-1'
      , itemType: 'integer6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 6, decimalPrecision: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'In principe terug te nemen vrijstelling'
    , code: '8612'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BasicTaxableReversalPreviousExemptionAdditionalPersonnel'
      , name: 'BasicTaxableReversalPreviousExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling die voor het vorige aanslagjaar werd verleend'
    , code: '8613'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ExemptionAdditionalPersonnelPreviousAssessmentYear'
      , name: 'ExemptionAdditionalPersonnelPreviousAssessmentYear'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Terug te nemen vrijstelling'
    , code: '8614'
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ReversalPreviousExemptionAdditionalPersonnel'
      , name: 'ReversalPreviousExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Correcties'
    , code: ''
    , documentation: 'Correcties'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'tax-inc_CorrectionsWorkingDaysPersonnelDetailSection'
      , name: 'CorrectionsWorkingDaysPersonnelDetailSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Correcties kalenderjaar-1'
    , code: ''
    , documentation: 'Correcties kalenderjaar-1'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CorrectionsWorkingDaysPersonnelDetailCalendarYear-1Title'
      , name: 'CorrectionsWorkingDaysPersonnelDetailCalendarYear-1Title'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Equivalent van gepresteerde dagen met betrekking tot het bij deeltijdse arbeid ontstane verschil tussen de dagen die zijn vermeld in de kwartaal aangifte RSZ en de effectieve prestaties'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EquivalentWholeDayWorkingDaysPartTimePersonnelPerformedAccordingNSSOQuarterlyDeclaration'
      , name: 'EquivalentWholeDayWorkingDaysPartTimePersonnelPerformedAccordingNSSOQuarterlyDeclaration'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Wettelijke feestdagen begrepen in de bezoldigde dagen van de arbeiders'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BankHolidaysIncludedDaysPaidWorkingDaysAllPersonnel'
      , name: 'BankHolidaysIncludedDaysPaidWorkingDaysAllPersonnel'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Wettelijke feestdagen begrepen in de correcties op de bezoldigde dagen en de vakantiedagen van de arbeiders'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BankHolidaysIncludedCorrectionsWorkingDaysAllPersonnel'
      , name: 'BankHolidaysIncludedCorrectionsWorkingDaysAllPersonnel'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CorrectionWorkingDaysPersonnelDetail'
      , name: 'CorrectionWorkingDaysPersonnelDetail'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'Correcties kalenderjaar-2'
    , code: ''
    , documentation: 'Correcties kalenderjaar-2'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_CorrectionsWorkingDaysPersonnelDetailCalendarYear-2Title'
      , name: 'CorrectionsWorkingDaysPersonnelDetailCalendarYear-2Title'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Equivalent van gepresteerde dagen met betrekking tot het bij deeltijdse arbeid ontstane verschil tussen de dagen die zijn vermeld in de kwartaal aangifte RSZ en de effectieve prestaties'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_EquivalentWholeDayWorkingDaysPartTimePersonnelPerformedAccordingNSSOQuarterlyDeclaration'
      , name: 'EquivalentWholeDayWorkingDaysPartTimePersonnelPerformedAccordingNSSOQuarterlyDeclaration'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Wettelijke feestdagen begrepen in de bezoldigde dagen van de arbeiders'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BankHolidaysIncludedDaysPaidWorkingDaysAllPersonnel'
      , name: 'BankHolidaysIncludedDaysPaidWorkingDaysAllPersonnel'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Wettelijke feestdagen begrepen in de correcties op de bezoldigde dagen en de vakantiedagen van de arbeiders'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_BankHolidaysIncludedCorrectionsWorkingDaysAllPersonnel'
      , name: 'BankHolidaysIncludedCorrectionsWorkingDaysAllPersonnel'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Uitleg'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_CorrectionWorkingDaysPersonnelDetail'
      , name: 'CorrectionWorkingDaysPersonnelDetail'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling voor bijkomend personeel tewerkgesteld voor wetenschappelijk onderzoek'
      , code: '276W1'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Personeelsleden aangeworven tijdens een vorig belastbaar tijdperk en gedurende het volledige belastbare tijdperk voltijds tewerkgesteld'
    , code: ''
    , documentation: 'Personeelsleden aangeworven tijdens een vorig belastbaar tijdperk en gedurende het volledige belastbare tijdperk voltijds tewerkgesteld'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_EmployedFullTimeScientificResearchSection'
      , name: 'EmployedFullTimeScientificResearchSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Datum van aanwerving en tewerkstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
      , name: 'DateRecruitmentEmploymentAdditionalPersonnel'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FullExemptionAdditionalPersonnel'
      , name: 'FullExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Effectief vrijgesteld'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ActualExemptionAdditionalPersonnel'
      , name: 'ActualExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden'
    , code: ''
    , documentation: 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_EmployedPartTimeScientificResearchSection'
      , name: 'EmployedPartTimeScientificResearchSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Datum van aanwerving en tewerkstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
      , name: 'DateRecruitmentEmploymentAdditionalPersonnel'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FullExemptionAdditionalPersonnel'
      , name: 'FullExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Effectief vrijgesteld'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ActualExemptionAdditionalPersonnel'
      , name: 'ActualExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling voor bijkomend personeel tewerkgesteld voor de uitbouw van het technologisch potentieel van de onderneming'
      , code: '276W2'

      , items: [

    { xtype: 'biztax-contextgrid'

    , title: 'Personeelsleden aangeworven tijdens een vorig belastbaar tijdperk en gedurende het volledige belastbare tijdperk voltijds tewerkgesteld'
    , code: ''
    , documentation: 'Personeelsleden aangeworven tijdens een vorig belastbaar tijdperk en gedurende het volledige belastbare tijdperk voltijds tewerkgesteld'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_EmployedFullTimeIncreaseTechnologicalPotentialSection'
      , name: 'EmployedFullTimeIncreaseTechnologicalPotentialSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Datum van aanwerving en tewerkstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
      , name: 'DateRecruitmentEmploymentAdditionalPersonnel'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FullExemptionAdditionalPersonnel'
      , name: 'FullExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Effectief vrijgesteld'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ActualExemptionAdditionalPersonnel'
      , name: 'ActualExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-contextgrid'

    , title: 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden'
    , code: ''
    , documentation: 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'tax-inc_EmployedPartTimeIncreaseTechnologicalPotentialSection'
      , name: 'EmployedPartTimeIncreaseTechnologicalPotentialSection'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
        //create TPL
      , items: [

    { xtype: 'biztax-field'

    , title: 'Datum van aanwerving en tewerkstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
      , name: 'DateRecruitmentEmploymentAdditionalPersonnel'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FullExemptionAdditionalPersonnel'
      , name: 'FullExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Effectief vrijgesteld'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ActualExemptionAdditionalPersonnel'
      , name: 'ActualExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }

      ]
    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer'
      , code: '276W3'

      , items: [

    { xtype: 'biztax-field'

    , title: 'Naam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Identity'
      , name: 'Identity'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nationaal nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NationalNumber'
      , name: 'NationalNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van aanwerving en tewerkstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
      , name: 'DateRecruitmentEmploymentAdditionalPersonnel'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Functie'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Function'
      , name: 'Function'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FullExemptionAdditionalPersonnel'
      , name: 'FullExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Effectief vrijgesteld'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ActualExemptionAdditionalPersonnel'
      , name: 'ActualExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg'
      , code: '276W4'

      , items: [

    { xtype: 'biztax-field'

    , title: 'Naam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Identity'
      , name: 'Identity'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nationaal nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_NationalNumber'
      , name: 'NationalNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van aanwerving en tewerkstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
      , name: 'DateRecruitmentEmploymentAdditionalPersonnel'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Functie'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_Function'
      , name: 'Function'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Vrijstelling'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_FullExemptionAdditionalPersonnel'
      , name: 'FullExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Effectief vrijgesteld'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_ActualExemptionAdditionalPersonnel'
      , name: 'ActualExemptionAdditionalPersonnel'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Degressief af te schrijven vaste activa'
      , code: '328K'

      , items: [

    { xtype: 'biztax-field'

    , title: 'Vermoedelijke gebruiksduur, in jaren'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_UsefulEconomicLifeInYears'
      , name: 'UsefulEconomicLifeInYears'
      , itemType: 'nonNegativeInteger6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 6, decimalPrecision: 0, minValue: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Degressief afschrijvingspercentage'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AnnuityDegressiveDepreciation'
      , name: 'AnnuityDegressiveDepreciation'
      , itemType: 'percentageItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , minValue: 0, maxValue: 1, maxLength: 5, decimalPrecision: 4
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van verkrijging of totstandbrenging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateAcquisitionInvestment'
      , name: 'DateAcquisitionInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Vaste activa voorheen in het stelsel van degressieve afschrijvingen opgenomen'
      , code: '328L'

      , items: [

    { xtype: 'biztax-field'

    , title: 'Vermoedelijke gebruiksduur, in jaren'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_UsefulEconomicLifeInYears'
      , name: 'UsefulEconomicLifeInYears'
      , itemType: 'nonNegativeInteger6ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 6, decimalPrecision: 0, minValue: 0
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van verkrijging of totstandbrenging'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_DateAcquisitionInvestment'
      , name: 'DateAcquisitionInvestment'
      , itemType: 'dateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aanschaffings- of beleggingswaarde'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'tax-inc_AcquisitionInvestmentValue'
      , name: 'AcquisitionInvestmentValue'
      , itemType: 'nonNegativeMonetary14D2ItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'
    , maxLength: 16, decimalPrecision: 2, minValue: 0, maxValue: 99999999999999.99
    }

    }
  ]
    }
  ,
    { xtype: 'biztax-tab'
      , title: 'Identificatiegegevens en inlichtingen'
      , code: ''

      , items: [

    { xtype: 'biztax-pane'

    , title: 'Identificatiegegevens van de onderneming'
    , code: ''
    , documentation: 'Identificatiegegevens van de onderneming'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityInformation'
      , name: 'EntityInformation'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Naam'
    , code: ''
    , documentation: 'Naam'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityName'
      , name: 'EntityName'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Wettelijke benaming'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_EntityCurrentLegalName'
      , name: 'EntityCurrentLegalName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Ondernemingsnummer'
    , code: ''
    , documentation: 'Identificerend nummer van de onderneming'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityIdentifier'
      , name: 'EntityIdentifier'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Type nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IdentifierName'
      , name: 'IdentifierName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Opgave nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IdentifierValue'
      , name: 'IdentifierValue'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-listorother'

    , title: 'Rechtsvorm'
    , code: ''
    , documentation: 'Rechtsvorm'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityForm'
      , name: 'EntityForm'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_LegalFormCodeHead'
      , name: 'LegalFormCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_LegalFormOther'
      , name: 'LegalFormOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Adres'
    , code: ''
    , documentation: 'Adres van de onderneming'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityAddress'
      , name: 'EntityAddress'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-listorother'

    , title: 'Aard adres'
    , code: ''
    , documentation: 'Aard adres'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_AddressType'
      , name: 'AddressType'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_AddressTypeCodeHead'
      , name: 'AddressTypeCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_AddressTypeOther'
      , name: 'AddressTypeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Straat'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Street'
      , name: 'Street'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nr'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Number'
      , name: 'Number'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bus'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Box'
      , name: 'Box'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Postcode en gemeente'
    , code: ''
    , documentation: 'Postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCity'
      , name: 'PostalCodeCity'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_PostalCodeHead'
      , name: 'PostalCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere'
    , code: ''
    , documentation: 'Andere postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCityOther'
      , name: 'PostalCodeCityOther'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeOther'
      , name: 'PostalCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemeente'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_City'
      , name: 'City'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-listorother'

    , title: 'Land'
    , code: ''
    , documentation: 'Landcode'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_CountryCode'
      , name: 'CountryCode'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_CountryCodeHead'
      , name: 'CountryCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryCodeOther'
      , name: 'CountryCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Rechtspersonenregister (RPR) - Rechtbank van Koophandel van'
    , code: ''
    , documentation: 'Rechtbank van koophandel van het rechtsgebied waarin de maatschappelijke zetel van de onderneming gevestigd is'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_CommercialCourt'
      , name: 'CommercialCourt'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-label'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_CommercialCourtHead'
      , name: 'CommercialCourtHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CommercialCourtOther'
      , name: 'CommercialCourtOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Telefoonnummer of Faxnummer van de onderneming'
    , code: ''
    , documentation: 'Telefoonnummer of Faxnummer van de onderneming'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityPhoneFaxNumber'
      , name: 'EntityPhoneFaxNumber'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanduiding of het een telefoon- dan wel een faxnummer betreft'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PhoneFaxNumber'
      , name: 'PhoneFaxNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Land telefoonnummer of faxnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryRegion'
      , name: 'CountryRegion'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Zonenummer telefoon of fax'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CityAreaCode'
      , name: 'CityAreaCode'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Lokaal telefoonnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_LocalPhoneNumber'
      , name: 'LocalPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Toestelnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ExtensionPhoneNumber'
      , name: 'ExtensionPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'E-mail'
    , code: ''
    , documentation: 'E-mail'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityEmail'
      , name: 'EntityEmail'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'E-mail adres'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_EmailAddress'
      , name: 'EmailAddress'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Internetadres'
    , code: ''
    , documentation: 'Website'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityWebSite'
      , name: 'EntityWebSite'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'URL van de website'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_WebSiteURL'
      , name: 'WebSiteURL'
      , itemType: 'anyURIItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Specifiëring van de website'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_WebSiteDescription'
      , name: 'WebSiteDescription'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Bedrijfssector'
    , code: ''
    , documentation: 'Bedrijfssector'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_EntityIndustrySectorCodeIdentifier'
      , name: 'EntityIndustrySectorCodeIdentifier'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-label'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_EntityIndustrySectorCodeIdentifierHead'
      , name: 'EntityIndustrySectorCodeIdentifierHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van de neerlegging van de oprichtingsakte OF van het recentste stuk dat de datum van bekendmaking van de oprichtingsakte en van de akte tot statutenwijziging vermeldt.'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DepositDateLastDeed'
      , name: 'DepositDateLastDeed'
      , itemType: 'limitedDateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'
    , minValue: '1800-01-01', maxValue: '2049-12-31'
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Inlichtingen betreffende beursnoteringen'
    , code: ''
    , documentation: 'Inlichtingen betreffende beursnoteringen'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityPublicListingInformation'
      , name: 'EntityPublicListingInformation'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Beursnaam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ExchangeName'
      , name: 'ExchangeName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Beursnoteringsymbool'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_TradingSymbol'
      , name: 'TradingSymbol'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Aard van het genoteerde effect'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IssueTypeSecurityName'
      , name: 'IssueTypeSecurityName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Omschrijving van het genoteerde effect'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IssueTypeSecurityDescription'
      , name: 'IssueTypeSecurityDescription'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Contactpersonen binnen de onderneming'
    , code: ''
    , documentation: 'Contactpersonen binnen de onderneming'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityContact'
      , name: 'EntityContact'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Type contact'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactType'
      , name: 'ContactType'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Naam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactName'
      , name: 'ContactName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voornaam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactFirstName'
      , name: 'ContactFirstName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Functie'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactTitlePosition'
      , name: 'ContactTitlePosition'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Adres'
    , code: ''
    , documentation: 'Adres van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactAddress'
      , name: 'ContactAddress'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-listorother'

    , title: 'Aard adres'
    , code: ''
    , documentation: 'Aard adres'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_AddressType'
      , name: 'AddressType'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_AddressTypeCodeHead'
      , name: 'AddressTypeCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_AddressTypeOther'
      , name: 'AddressTypeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Straat'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Street'
      , name: 'Street'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nr'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Number'
      , name: 'Number'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bus'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Box'
      , name: 'Box'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Postcode en gemeente'
    , code: ''
    , documentation: 'Postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCity'
      , name: 'PostalCodeCity'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_PostalCodeHead'
      , name: 'PostalCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere'
    , code: ''
    , documentation: 'Andere postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCityOther'
      , name: 'PostalCodeCityOther'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeOther'
      , name: 'PostalCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemeente'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_City'
      , name: 'City'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-listorother'

    , title: 'Land'
    , code: ''
    , documentation: 'Landcode'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_CountryCode'
      , name: 'CountryCode'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_CountryCodeHead'
      , name: 'CountryCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryCodeOther'
      , name: 'CountryCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Telefoonnummer of faxnummer'
    , code: ''
    , documentation: 'Telefoonnummer of faxnummer van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactPhoneFaxNumber'
      , name: 'ContactPhoneFaxNumber'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanduiding of het een telefoon- dan wel een faxnummer betreft'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PhoneFaxNumber'
      , name: 'PhoneFaxNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Land telefoonnummer of faxnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryRegion'
      , name: 'CountryRegion'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Zonenummer telefoon of fax'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CityAreaCode'
      , name: 'CityAreaCode'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Lokaal telefoonnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_LocalPhoneNumber'
      , name: 'LocalPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Toestelnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ExtensionPhoneNumber'
      , name: 'ExtensionPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'E-mail'
    , code: ''
    , documentation: 'E-mail van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactEmail'
      , name: 'ContactEmail'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'E-mail adres'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_EmailAddress'
      , name: 'EmailAddress'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Bankinformatie'
    , code: ''
    , documentation: 'Bankinformatie'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_BankInformation'
      , name: 'BankInformation'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'BIC'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_BankIdentifierCode'
      , name: 'BankIdentifierCode'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'IBAN'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_InternationalBankAccountNumber'
      , name: 'InternationalBankAccountNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Identificerend nummer van de zonder vereffening ontbonden vennootschap'
    , code: ''
    , documentation: 'Identificerend nummer van de zonder vereffening ontbonden vennootschap'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_EntityIdentifierCompanyWoundUpWithoutLiquidationProcedure'
      , name: 'EntityIdentifierCompanyWoundUpWithoutLiquidationProcedure'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Type nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IdentifierName'
      , name: 'IdentifierName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Opgave nummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_IdentifierValue'
      , name: 'IdentifierValue'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Inlichtingen omtrent het document'
    , code: ''
    , documentation: 'Inlichtingen omtrent het document'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentInformation'
      , name: 'DocumentInformation'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Identificatie van het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentIdentifier'
      , name: 'DocumentIdentifier'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Identificatie van het document - Omschrijving'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentIdentifierDescription'
      , name: 'DocumentIdentifierDescription'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Benaming of titel van het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentNameTitle'
      , name: 'DocumentNameTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Omschrijving van het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentDescription'
      , name: 'DocumentDescription'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Creatiedatum van het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentCreationDate'
      , name: 'DocumentCreationDate'
      , itemType: 'limitedDateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'
    , minValue: '1800-01-01', maxValue: '2049-12-31'
    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van de laatste versie van het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentLastEditDate'
      , name: 'DocumentLastEditDate'
      , itemType: 'limitedDateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'
    , minValue: '1800-01-01', maxValue: '2049-12-31'
    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Historiek van het document'
    , code: ''
    , documentation: 'Historiek van het document'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentRevisionHistory'
      , name: 'DocumentRevisionHistory'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Versienummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_VersionNumber'
      , name: 'VersionNumber'
      , itemType: 'integerItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'numberfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Omschrijving van de bijwerking'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentRevisionDescription'
      , name: 'DocumentRevisionDescription'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Persoon door wie de bijwerking is uitgevoerd'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_RevisedName'
      , name: 'RevisedName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Functie van de persoon door wie de bijwerking is uitgevoerd'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_RevisedTitle'
      , name: 'RevisedTitle'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Datum van de bijwerking'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DateRevision'
      , name: 'DateRevision'
      , itemType: 'limitedDateItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'datefield'
    , minValue: '1800-01-01', maxValue: '2049-12-31'
    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Taal'
    , code: ''
    , documentation: 'Taal van het document'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentLanguage'
      , name: 'DocumentLanguage'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_LanguageCodeHead'
      , name: 'LanguageCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Persoon die verantwoordelijk is voor het document'
    , code: ''
    , documentation: 'Persoon die verantwoordelijk is voor het document'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentAuthor'
      , name: 'DocumentAuthor'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Naam van de persoon die verantwoordelijk is voor het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentAuthorName'
      , name: 'DocumentAuthorName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voornaam van de persoon die verantwoordelijk is voor het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentAuthorFirstName'
      , name: 'DocumentAuthorFirstName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Functie van de persoon die verantwoordelijk is voor het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_DocumentAuthorTitlePosition'
      , name: 'DocumentAuthorTitlePosition'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Adres van de persoon die verantwoordelijk is voor het document'
    , code: ''
    , documentation: 'Adres van de persoon die verantwoordelijk is voor het document'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentAuthorAddress'
      , name: 'DocumentAuthorAddress'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-listorother'

    , title: 'Aard adres'
    , code: ''
    , documentation: 'Aard adres'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_AddressType'
      , name: 'AddressType'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_AddressTypeCodeHead'
      , name: 'AddressTypeCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_AddressTypeOther'
      , name: 'AddressTypeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Straat'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Street'
      , name: 'Street'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nr'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Number'
      , name: 'Number'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bus'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Box'
      , name: 'Box'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Postcode en gemeente'
    , code: ''
    , documentation: 'Postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCity'
      , name: 'PostalCodeCity'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_PostalCodeHead'
      , name: 'PostalCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere'
    , code: ''
    , documentation: 'Andere postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCityOther'
      , name: 'PostalCodeCityOther'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeOther'
      , name: 'PostalCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemeente'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_City'
      , name: 'City'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-listorother'

    , title: 'Land'
    , code: ''
    , documentation: 'Landcode'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_CountryCode'
      , name: 'CountryCode'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_CountryCodeHead'
      , name: 'CountryCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryCodeOther'
      , name: 'CountryCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Telefoonnummer of faxnummer van de persoon die verantwoordelijk is voor het document'
    , code: ''
    , documentation: 'Telefoonnummer of faxnummer van de persoon die verantwoordelijk is voor het document'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentAuthorPhoneFaxNumber'
      , name: 'DocumentAuthorPhoneFaxNumber'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanduiding of het een telefoon- dan wel een faxnummer betreft'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PhoneFaxNumber'
      , name: 'PhoneFaxNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Land telefoonnummer of faxnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryRegion'
      , name: 'CountryRegion'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Zonenummer telefoon of fax'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CityAreaCode'
      , name: 'CityAreaCode'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Lokaal telefoonnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_LocalPhoneNumber'
      , name: 'LocalPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Toestelnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ExtensionPhoneNumber'
      , name: 'ExtensionPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'E-mail van de persoon die verantwoordelijk is voor het document'
    , code: ''
    , documentation: 'E-mail van de persoon die verantwoordelijk is voor het document'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentAuthorEmail'
      , name: 'DocumentAuthorEmail'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'E-mail adres'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_EmailAddress'
      , name: 'EmailAddress'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Eerste verantwoordelijke voor het document'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_MainAuthor'
      , name: 'MainAuthor'
      , itemType: 'booleanItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'checkbox' }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Contactpersoon'
    , code: ''
    , documentation: 'Contactpersoon voor het document'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentContact'
      , name: 'DocumentContact'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-pane'

    , title: 'Hoedanigheid van de contactpersoon'
    , code: ''
    , documentation: 'Hoedanigheid van de contactpersoon voor het document'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_DocumentContactType'
      , name: 'DocumentContactType'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_DocumentContactTypeCodeHead'
      , name: 'DocumentContactTypeCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Naam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactName'
      , name: 'ContactName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Voornaam'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactFirstName'
      , name: 'ContactFirstName'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Functie'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ContactTitlePosition'
      , name: 'ContactTitlePosition'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Adres'
    , code: ''
    , documentation: 'Adres van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactAddress'
      , name: 'ContactAddress'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-listorother'

    , title: 'Aard adres'
    , code: ''
    , documentation: 'Aard adres'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_AddressType'
      , name: 'AddressType'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_AddressTypeCodeHead'
      , name: 'AddressTypeCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_AddressTypeOther'
      , name: 'AddressTypeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Straat'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Street'
      , name: 'Street'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Nr'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Number'
      , name: 'Number'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Bus'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_Box'
      , name: 'Box'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Postcode en gemeente'
    , code: ''
    , documentation: 'Postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'panesOnly'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCity'
      , name: 'PostalCodeCity'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_PostalCodeHead'
      , name: 'PostalCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Andere'
    , code: ''
    , documentation: 'Andere postcode en gemeente'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeCityOther'
      , name: 'PostalCodeCityOther'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PostalCodeOther'
      , name: 'PostalCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Gemeente'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_City'
      , name: 'City'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-listorother'

    , title: 'Land'
    , code: ''
    , documentation: 'Landcode'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_CountryCode'
      , name: 'CountryCode'
      , itemType: ''
    }
    , isAbstract: false
      , valueList:
    { xtype: 'biztax-list'

    , title: 'Lijst'
    , code: ''
    , oldCode: ''
    , periods: []

    , xbrlDef: {
        id: 'pfs-vl_CountryCodeHead'
      , name: 'CountryCodeHead'
      , itemType: 'stringItemType'
    }
    , isAbstract: true
    , substitutionGroup: ''
    }

      , otherField:
    { xtype: 'biztax-field'

    , title: 'Andere'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryCodeOther'
      , name: 'CountryCodeOther'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'Telefoonnummer of faxnummer'
    , code: ''
    , documentation: 'Telefoonnummer of faxnummer van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactPhoneFaxNumber'
      , name: 'ContactPhoneFaxNumber'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'Aanduiding of het een telefoon- dan wel een faxnummer betreft'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_PhoneFaxNumber'
      , name: 'PhoneFaxNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Land telefoonnummer of faxnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CountryRegion'
      , name: 'CountryRegion'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Zonenummer telefoon of fax'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_CityAreaCode'
      , name: 'CityAreaCode'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Lokaal telefoonnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_LocalPhoneNumber'
      , name: 'LocalPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }
  ,
    { xtype: 'biztax-field'

    , title: 'Toestelnummer'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_ExtensionPhoneNumber'
      , name: 'ExtensionPhoneNumber'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ,
    { xtype: 'biztax-pane'

    , title: 'E-mail'
    , code: ''
    , documentation: 'E-mail van de contactpersoon'
    , multipleContexts: false
    , viewTplType: 'duration'

    , xbrlDef: {
        id: 'pfs-gcd_ContactEmail'
      , name: 'ContactEmail'
      , itemType: ''
    }
    , isAbstract: false
      , items: [

    { xtype: 'biztax-field'

    , title: 'E-mail adres'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_EmailAddress'
      , name: 'EmailAddress'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }

      ]
    }
  ,
    { xtype: 'biztax-field'

    , title: 'Softwareproducent'
    , code: ''
    , oldCode: ''
    , periods: ['D']

    , xbrlDef: {
        id: 'pfs-gcd_SoftwareVendor'
      , name: 'SoftwareVendor'
      , itemType: 'stringItemType'
    }
    , isAbstract: false, fieldType:
    { xtype: 'textfield'

    }

    }

      ]
    }
  ]
    }
  ];

eBook.BizTax.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit',
            items: [
                eBook.BizTax.AY
            ]
        });
        eBook.BizTax.Window.superclass.initComponent.apply(this, arguments);
    }
    , show: function() {
        eBook.BizTax.Window.superclass.show.call(this);
        this.getEl().mask('rendering tab');
        this.module.setActiveTab(0);
        this.getEl().unmask();
    }
});

/*
var tst = function() {
    var wn = new eBook.BizTax.Window({});
    wn.show();
};
tst.defer(3000, this);

*/