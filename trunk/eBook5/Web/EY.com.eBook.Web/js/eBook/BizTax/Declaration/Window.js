
eBook.BizTax.Declaration.Overview = Ext.extend(Ext.Panel, {
    mtpl: new Ext.XTemplate('<div class="eBook-Xbrl-Overview">'
            , '<div class="eBook-Xbrl-Overview-Block eBook-Client-ico-32">'
                , ' <b><u>{clientname}</u></b>'
                , '<br/><br/> Period: {start:date("d/m/Y")} &gt; {end:date("d/m/Y")}'
                , '<br/> Assessment: {assessmentyear}'
             , '</div>'
             , '<div class="eBook-Xbrl-Overview-Block eBook-Partner-ico-32">'
             , 'Department: <b>{department}</b>'
             , '<br/>Declaring partner: <b>{partnername}</b>'
             , '</div>'
             , '</div>', { compiled: true })
    , initComponent: function() {
        Ext.apply(this, {
            html: 'loading'
            , bbar: [{ xtype: 'button', text: eBook.Xbrl.Wizard_Previous, ref: 'bck', handler: this.onPrevious, scope: this, iconCls: 'eBook-back-24',
                scale: 'medium',
                iconAlign: 'top'
            }, '->', {
                text: eBook.Xbrl.Overview_SendToBizTax,
                iconCls: 'eBook-biztax-send-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'biztax',
                handler: this.onSendToBizTax,
                scope: this
}]
            });
            eBook.BizTax.Declaration.Overview.superclass.initComponent.apply(this, arguments);
            
            
        }
    , getBodyEl: function() {
        return this.getEl().child('.x-panel-body');
    }
    , setStep1Data: function(data) {
        var dta = {
            clientname: eBook.Interface.currentClient.get('Name')
            , start: eBook.Interface.currentFile.get('StartDate')
            , end: eBook.Interface.currentFile.get('EndDate')
            , assessmentyear: eBook.Interface.currentFile.get('AssessmentYear')
            , department: data.department
            , partnername: data.partnerName
        };
        this.getBodyEl().update(this.mtpl.apply(dta));
    }
    , onSendToBizTax: function() {
        this.refOwner.sendToBeDeclared();
    }
    , onPrevious: function() {
        this.refOwner.getLayout().setActiveItem(0);
    }
    });
Ext.reg('biztax-declaration-overview', eBook.BizTax.Declaration.Overview);

eBook.BizTax.Declaration.PartnerSelection = Ext.extend(Ext.form.FormPanel, {
    initComponent: function () {
        Ext.apply(this, {
            layoutConfig: { labelAlign: 'top' }
            , labelAlign: 'top'
            , cls: 'biztax-partner-select'
            , bodyStyle: 'padding:15px'
            , defaults: { margins: { top: 10, bottom: 10, right: 0, left: 0} }
            , items: [
                { xtype: 'checkbox', ref: 'partnerok', boxLabel: 'Did the partner formally approved this declaration?'
                    //, disabled: true
                    , listeners: { 'check': { fn: this.onPartnerCheck, scope: this} }
                }
                , { xtype: 'label', labelStyle: 'font-style:italic', text: 'If department and/or partner is not in the lists below, update the MyPMT teammembers.' }
                , { xtype: 'combo', ref: 'departments'
                     , disabled: true
                    , typeAhead: false
                    , forceSelection: true
                    , mode: 'local'
                    , triggerAction: 'all'
                    , width: 300
                    , autoSelect: true
                    , valueField: 'id'
                    , fieldLabel: 'Select the deparment which will perform the declaration'
                    , displayField: 'desc'
                    , store: new eBook.data.JsonStore({
                        selectAction: 'GetClientDepartments'
                                , serviceUrl: eBook.Service.client
                                , autoLoad: true
                                , autoDestroy: true
                                , criteriaParameter: 'cidc'
                                , baseParams: { Id: eBook.Interface.currentClient.get('Id') }
                                , fields: eBook.data.RecordTypes.SimpleList
                    })
                    , listeners: { 'select': { fn: this.onDepartmentSelect, scope: this} }
                }
                , { xtype: 'combo', ref: 'partners'
                    , disabled: true
                    , typeAhead: false
                    , forceSelection: true
                    , autoSelect: true
                    , mode: 'local'
                    , triggerAction: 'all'
                    , emptyText: 'No partners where found for this department, check/update the MyPMT teammembers.'
                    , width: 300
                    , valueField: 'id'
                    , fieldLabel: 'Select the partner in whose name the declaration will be performed'
                    , displayField: 'desc'
                    , store: new eBook.data.JsonStore({
                        selectAction: 'GetClientDepartmentPartners'
                                , serviceUrl: eBook.Service.client
                                , autoLoad: false
                                , autoDestroy: true
                                , criteriaParameter: 'ciddc'
                        // , baseParams: { Id: eBook.Interface.currentClient.get('Id'), Department: 'ACR' }
                                , fields: eBook.data.RecordTypes.SimpleList
                    })
                    , listeners: { 'select': { fn: this.onPartnerSelect, scope: this} }
                }
            ]
            , bbar: ['->', { xtype: 'button', text: eBook.Xbrl.Wizard_Next, ref: 'nxt', handler: this.onNext, scope: this, iconCls: 'eBook-next-24',
                scale: 'medium', disabled: true,
                iconAlign: 'top'
            }]
        });
        eBook.BizTax.Declaration.PartnerSelection.superclass.initComponent.apply(this, arguments);
    }
    , mdata: { department: '', partnerId: '', partnerName: '', validated: false }
    , onPartnerCheck: function (fld, chk) {
        this.getBottomToolbar().nxt.disable();
        this.mdata.validated = chk;
        this.mdata.department = null;
        this.mdata.partnerId = null;
        this.mdata.partnerName = null;
        if (chk) {
            this.departments.enable();
            if (this.departments.store.getCount() == 1) {
                this.departments.setValue(this.departments.store.getAt(0).get('id'));
                this.onDepartmentSelect(this.departments, this.departments.store.getAt(0), 0);
            }
        } else {
            this.departments.clearValue();
            this.departments.disable();
            this.partners.clearValue();
            this.partners.disable();
        }
    }
    , onDepartmentSelect: function (fld, rec, idx) {
        this.getBottomToolbar().nxt.disable();
        if (rec) {
            this.partners.clearValue();
            this.mdata.department = rec.get('id');
            this.mdata.partnerId = null;
            this.mdata.partnerName = null;
            if (rec.get('id') == 'ACR') {
                // central admin -> always Lena Plas.
                this.partners.store.removeAll();
                var rt = new this.partners.store.recordType({ id: '607d8203-f4a1-4f70-9b5d-d6078b5ea8cb', desc: 'LENA PLAS' });
                this.partners.store.add(rt);
                this.partners.setValue(rt.get('id'));
                this.onPartnerSelect(this.partners, rt, 0);
                this.partners.disable();
                if (!this.refOwner.hasproxy) {
                    this.getBottomToolbar().nxt.disable();
                    Ext.Msg.show({
                        title: 'Proxy missing',
                        msg: 'There is currently no signed proxy for this client and period. Declaration is not allowed until the signed proxy is uploaded.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        scope: this
                    });
                }
            } else {
                this.partners.store.load({ params: { Id: eBook.Interface.currentClient.get('Id'), Department: rec.get('id')} });
                this.partners.enable();
            }
        } else {
            this.mdata.department = null;
            this.mdata.partnerId = null;
            this.mdata.partnerName = null;
            this.partners.clearValue();
            this.partners.disable();
        }
    }
    , onPartnerSelect: function (fld, rec, idx) {
        if (rec) {
            this.mdata.partnerId = rec.get('id');
            this.mdata.partnerName = rec.get('desc');
            this.getBottomToolbar().nxt.enable();
        } else {
            this.mdata.partnerId = null;
            this.mdata.partnerName = null;
        }
    }
    , onNext: function () {
        this.refOwner.overview.setStep1Data(this.mdata);
        this.refOwner.getLayout().setActiveItem(1);
    }
});

/*
eBook.Service.client

CriteriaIdAndDepartmentDataContract

*/

Ext.reg('biztax-declaration-partner', eBook.BizTax.Declaration.PartnerSelection);



eBook.BizTax.Declaration.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        Ext.apply(this, {
            title: 'BizTax declaration'
            , layout: 'card'
            , activeItem: 0
            , width: 612
            , height: 375
            , modal: true
            , items: [{ xtype: 'biztax-declaration-partner', ref: 'partnerpanel' }
                      , { xtype: 'biztax-declaration-overview', ref: 'overview'}]
        });
        eBook.BizTax.Declaration.Window.superclass.initComponent.apply(this, arguments);
        this.hasproxy = !Ext.isEmpty(this.module.store.data.Data.ProxyId);
    }
    /* , show: function(module) {
    eBook.BizTax.Declaration.Window.superclass.show.call(this);
    this.module = module;
    }*/
    , sendToBeDeclared: function () {
        this.getEl().mask("Saving meta data");
        var meta = this.partnerpanel.mdata;
        // UPDATE STORE 
        this.module.store.data.department = meta.department;
        this.module.store.data.partnerId = meta.partnerId;
        this.module.store.data.partnerName = meta.partnerName;
        // SAVE META ONLY (no changes to xbrl)
        this.module.store.saveMeta(false,{ fn: this.onMetaSaved, scope: this });
    }
    , onMetaSaved: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.getEl().mask("Publishing BizTax declaration to be send");
            Ext.Ajax.request({
                url: this.module.store.myServiceUrl("Publish")
            , method: 'POST'
            , params: Ext.encode({ cbdc: {
                FileId: eBook.Interface.currentFile.get('Id'), 
                Culture: eBook.Interface.Culture,
                Type:this.module.biztaxtype
        }
             })
            , callback: this.onDeclared
            , scope: this
                // , callerCallback: { fn: module.onDataLoaded, scope: module }
            });
        } else {
            eBook.Interface.showResponseError(resp, "Failed saving meta");
        }
    }
    , onDeclared: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            this.module.store.loadData(this.module.onDataLoaded, this.module);
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, "Biztax publish failed");
        }
    }
});

Ext.reg('biztax-declaration-window', eBook.BizTax.Declaration.Window);        