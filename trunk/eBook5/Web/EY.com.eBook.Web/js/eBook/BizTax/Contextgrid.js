eBook.BizTax.Contextgrid = Ext.extend(Ext.Component, {
    lines: []
    , indexes: {
        lineIds: {}
    }
    , initComponent: function () {
        var cgDef = Ext.get(this.domConfig);

        var cgIds = cgDef.getAttribute('data-xbrl-cg-ids');

        Ext.apply(this, {
            lineTpl: cgDef.child('lineTpl').child('tr').dom.innerHTML
            , renderTo: cgDef.parent()
            , autoEl: {
                tag: 'div', cls: 'biztax-contextgrid-container'
                , children: [
                    { tag: 'div', cls: 'biztax-contextgrid-body', html: cgDef.child('contextheading').dom.innerHTML
                    }
                    , { tag: 'div', cls: 'biztax-contextgrid-footer'
                       , children: [{ tag: 'div', cls: 'biztax-contextgrid-addbutton', html: 'Toevoegen'}]
                    }
                ]
            }
            , contextId: cgDef.getAttribute('data-xbrl-cg-parent-id')
            , contextLinkedIds: cgIds ? cgIds.split(',') : [cgDef.getAttribute('data-xbrl-cg-parent-id')]
            , dimensionSets: cgDef.getAttribute('data-xbrl-dimensionset').split('/')
        });



        if (!Ext.isArray(this.dimensionSets)) this.dimensionSets = [this.dimensionSets];
        cgDef.remove();
        eBook.BizTax.Contextgrid.superclass.initComponent.apply(this, arguments);
    }
    , afterRender: function (cnt) {
        eBook.BizTax.Contextgrid.superclass.afterRender.call(this);
        this.tableEl = this.el.child('.biztax-contextgrid-body table');
        var addBtnEl = this.el.child('.biztax-contextgrid-addbutton');
        addBtnEl.on('click', this.addLine, this);
        var res = this.getModule().store.registerContextGrid(this, this.contextId, this.contextLinkedIds);
        //this.clearLines();
        this.loadExisting(res);

    }
    , beforeRender: function () {
        eBook.BizTax.Contextgrid.superclass.beforeRender.call(this);
        this.clearLines();
    }
    , loadExisting: function (res) {
        if (res && res.length > 0) {
            for (var i = 0; i < res.length; i++) {
                this.addLine(null, null, null, res[i].defid, res[i]);
            }
        }
    }
    , reload: function () {
        this.getEl().mask('reloading items');
        this.clearLines();
        this.loadExisting(this.getModule().store.getContextGridData(this.contextId));
        this.getEl().unmask();
    }
    , removeAll: function () {
        this.tableEl.child('tbody').update("");
        this.lines = [];
    }

    , clearLines: function () {
        var lid;

        for (var i = 0; i < this.lines.length; i++) {
            lid = this.contextId + this.lines[i].myId;
            Ext.get(lid).remove();
        }
        this.lines = [];
        this.indexes.lineIds = {};
    }
    , getModule: function () {
        return Ext.getCmp(this.moduleId);
    }
    , addLine: function (evt, el, n1, id, contextData) {
        var myId = id ? id : eBook.NewGuid();

        if(myId == '6197ceba-7ee0-c7bb-ed6d-c0ea7b061e65')
        {
            console.log('6197ceba-7ee0-c7bb-ed6d-c0ea7b061e65');
        }

        else if(myId == '585b3b97-ec3c-e244-4f09-6f5ca4cd903d')
        {
            console.log('585b3b97-ec3c-e244-4f09-6f5ca4cd903d');
        }


        var tr = this.tableEl.child('tbody').createChild({ tag: 'tr', html: this.lineTpl, id: this.contextId + myId, 'data-id': this.lines.length, cls: 'biztax-cg-incomplete' });
        // var ctxs = this.getModule().store.createContexts(, myId);
        var line = {
            tr: Ext.get(tr)
            , myId: myId
            , fields: []
            , contextFields: []
            , incomplete: true
            , dimensions: []
            , contextRefs: {}
        };
        for (var i = 0; i < this.dimensionSets.length; i++) {
            var dm = { dimension: this.dimensionSets[i] };
            if (contextData) {
            }
            line.dimensions.push(dm);
        }

        line = this.applyFields(line, myId, contextData);

        this.indexes.lineIds['_' + myId] = this.lines.length;
        this.lines.push(line);

        if (!contextData) {
            for (var i = 0; i < line.contextFields.length; i++) {
                this.onContextFieldChange(line.contextFields[i]);
            }
        } else {
            this.lines[this.lines.length - 1].contextRefs = contextData;
            this.resetLineDims(this.lines[this.lines.length - 1]);
        }
    }
    , verifyRemoveLine: function (el) {
        var tg = el.target;
        Ext.Msg.show({
            title: 'Lijn verwijderen',
            msg: 'Bent u zeker dat u de geselecteerde lijn wenst te verwijderen?',
            buttons: Ext.Msg.YESNO,
            fn: function (btnid) {
                this.onRemoveLine(btnid, tg);
            },
            icon: Ext.MessageBox.QUESTION,
            scope: this
        });
    }
    , onRemoveLine: function (btnid, el) {
        if (btnid == 'yes') {
            this.getEl().mask('removing item');
            el = Ext.get(el);
            var idx = el.parent('tr').getAttribute('data-id');


            // remove contexts as well
            this.getModule().store.removeContextsByDefId(this.lines[idx].myId, this.lines[idx].fields);
            this.lines[idx].tr.remove();

            this.lines.splice(idx, 1);
            // reindex
            for (i = 0; i < this.lines.length; i++) {
                this.lines[i].tr.set({ 'data-id': i });
            }
            this.getEl().unmask();
            this.getModule().store.updateLinkedContextGrids(this.contextId);
        }
    }
    , applyFields: function (line, myId, contextData) {
        var fieldTypes = eBook.BizTax[this.bizTaxLocator].FieldTypes;
        var remButton = line.tr.child('.biztax-context-removeitem');
        remButton.on('click', this.verifyRemoveLine, this);
        var locked = this.getModule().store.data.Locked;


        var flds = line.tr.select('.biztax-field-wrapper').elements;
        for (var i = 0; i < flds.length; i++) {
            var cfg = { defattribs: this.getFieldAttribs(flds[i]), renderTo: flds[i], width: '90%'
                , contextDefId: myId
            };
            if (cfg.defattribs['data-xbrl-calc'] == 'true') {
                cfg.disabled = true;
            }
            if (cfg.defattribs['data-xbrl-fieldtype']) {

                if (contextData) {
                    var period = cfg.defattribs['data-xbrl-period'];
                    var cps = contextData[period].Id.split("__");
                    cps.shift();
                    var context = cps.join('__');
                    cfg.defattribs['data-xbrl-id'] = cfg.defattribs['data-xbrl-name'] + "_" + contextData[period].Id;
                    cfg.defattribs['data-xbrl-contextRef'] = contextData[period].Id;
                    cfg.defattribs['data-xbrl-context'] = context;
                }

                if (cfg.defattribs['data-xbrl-fieldtype'] == 'valuelist') {
                    Ext.apply(cfg, Ext.clone(fieldTypes[cfg.defattribs['data-xbrl-valuelistid']]));
                    var fld = Ext.create(cfg);
                    //    fld.on('select', this.onFieldChange, this);
                    line.fields.push(fld);
                } else {
                    Ext.apply(cfg, fieldTypes[cfg.defattribs['data-xbrl-fieldtype']]);
                    var fld = Ext.create(cfg);
                    //  fld.on('change', this.onFieldChange, this);
                    // this.registerField(fld);
                    line.fields.push(fld);
                }
                if (contextData) {
                    var val = this.registerField(fld);
                    if (val) fld.setValue(val);
                    if (fld.disabled) locked = true;
                }

            } else {
                flds[i] = Ext.get(flds[i]);
                var dimid = cfg.defattribs['data-xbrl-dimension-id'].replace(':', '_')
                if (flds[i].hasClass('biztax-contextfield')) {
                    var dxt = cfg.defattribs['data-xbrl-type'];
                    if (dxt == 'select') {
                        var lst = flds[i].query('div');
                        var dta = [];
                        for (var j = 0; j < lst.length; j++) {
                            dta.push({ id: lst[j].attributes['data-value'].value.replace(':', '_'), text: lst[j].innerText });

                        }
                        cfg.dimensionType = "explicitMember";
                        flds[i].update('');
                        Ext.apply(cfg, {
                            xtype: 'combo',
                            triggerAction: 'all',
                            forceSelection: true,
                            autoSelect: true,
                            allowBlank: false,
                            mode: 'local',
                            valueField: 'id',
                            displayField: 'text',
                            store: { xtype: 'jsonstore', autoDestroy: true, data: dta, idProperty: 'id', fields: ['id', 'text'] }
                        });
                        if (dta.length == 1) {
                            cfg.value = dta[0].id;
                        }
                        if (contextData && contextData.data[dimid]) {
                            cfg.value = contextData.data[dimid].replace(':', '_');
                        }
                        var fld = Ext.create(cfg);
                        fld.on('select', this.onContextFieldChange, this);
                        line.contextFields.push(fld);
                    } else {
                        Ext.apply(cfg, fieldTypes[cfg.defattribs['data-xbrl-datatype']]);
                        cfg.allowBlank = false;
                        cfg.dimensionType = "typedMember";
                        if (contextData && contextData.data[dimid]) {
                            cfg.value = contextData.data[dimid];
                        }
                        var fld = Ext.create(cfg);
                        fld.on('change', this.onContextFieldChange, this);
                        //this.registerField(fld);
                        line.contextFields.push(fld);
                    }
                }

            }
            Ext.get(flds[i]).addClass("biztax-field-rendered");
        }
        for (var k = 0; k < line.contextFields.length; k++) {
            if (locked) {
                line.contextFields[k].disable();
            } else {
                line.contextFields[k].enable();
            }
        }
        if (locked) {
            remButton.hide();
        } else {
            remButton.show();
        }

        return line;
    }
    , resetLineDims: function (line) {
        var dims = [];
        for (var i = 0; i < line.contextFields.length; i++) {
            var fld = line.contextFields[i];
            var dim = { dimension: fld.defattribs['data-xbrl-dimension-id'], type: fld.dimensionType, value: fld.getValue() };
            if (dim.type == "explicitMember") {
                var sp = dim.value.split('_');
                dim.value = sp[0] + ":" + sp[1];
                for (var j = 2; j < sp.length; j++) {
                    dim.value = dim.value + "_" + sp[j];
                }
            }
            dim.memberType = dim.dimension.split(':')[0] + ':' + fld.defattribs['data-xbrl-datatype'];
            if (Ext.isDate(dim.value)) {
                dim.value = dim.value.format('Y-m-d');
            }
            dim.value = "" + dim.value;
            dims.push(dim);
        }
        line.dimensions = dims;
        var allDone = true;

        for (var i = 0; i < line.dimensions.length; i++) {
            if (line.dimensions[i].dimension == dim.dimension) {
                toAdd = false;
                line.dimensions[i] = dim;
            }
            if (!line.dimensions[i].value || Ext.isEmpty(line.dimensions[i].value.trim())) {
                allDone = false;
            }

        }

        if (allDone) {
            line.incomplete = false;
            line.tr.removeClass('biztax-cg-incomplete');
        }
    }
    , onContextFieldChange: function (fld) {
        var myId = fld.contextDefId;

        if(myId == '6197ceba-7ee0-c7bb-ed6d-c0ea7b061e65')
        {
            console.log('6197ceba-7ee0-c7bb-ed6d-c0ea7b061e65');
        }
        else if(myId == '585b3b97-ec3c-e244-4f09-6f5ca4cd903d')
        {
            console.log('585b3b97-ec3c-e244-4f09-6f5ca4cd903d');
        }

        var lineIdx = this.indexes.lineIds['_' + myId];
        var line = this.lines[lineIdx];
        var dim = { dimension: fld.defattribs['data-xbrl-dimension-id'], type: fld.dimensionType, value: fld.getValue() };
        if (dim.type == "explicitMember") {
            var sp = dim.value.split('_');
            dim.value = sp[0] + ":" + sp[1];
            for (var j = 2; j < sp.length; j++) {
                dim.value = dim.value + "_" + sp[j];
            }
        }
        dim.memberType = dim.dimension.split(':')[0] + ':' + fld.defattribs['data-xbrl-datatype'];
        if (!fld.isValid()) dim.value = "";
        if (Ext.isDate(dim.value)) {
            dim.value = dim.value.format('Y-m-d');
        }
        dim.value = "" + dim.value;
        var allDone = true;

        for (var i = 0; i < line.dimensions.length; i++) {
            if (line.dimensions[i].dimension == dim.dimension) {
                toAdd = false;
                line.dimensions[i] = dim;
            }
            if (!line.dimensions[i].value || Ext.isEmpty(line.dimensions[i].value.trim())) {
                allDone = false;
            }
        }

        if (allDone) { //&& line.incomplete

            var ctxResults = this.getModule().store.updateContexts(myId, line.dimensions, line.contextRefs, this.contextId, line.fields);
            if (ctxResults === false) {
                line.incomplete = true;
                line.tr.addClass('biztax-cg-incomplete');
                for (var i = 0; i < line.contextFields.length; i++) {
                    line.contextFields[i].markInvalid('Combination of contextfields can only occur once');
                }
            } else {
                line.incomplete = false;
                line.tr.removeClass('biztax-cg-incomplete');
                for (var i = 0; i < line.contextFields.length; i++) {
                    line.contextFields[i].clearInvalid();
                }

                for (var i = 0; i < line.fields.length; i++) {
                    var period = line.fields[i].defattribs['data-xbrl-period'];
                    var oldRef = line.contextRefs[period];
                    var cps = ctxResults[period].Id.split("__");
                    cps.shift();
                    var context = cps.join('__');
                    line.fields[i].defattribs['data-xbrl-id'] = line.fields[i].defattribs['data-xbrl-name'] + "_" + ctxResults[period].Id;
                    line.fields[i].defattribs['data-xbrl-contextRef'] = ctxResults[period].Id;
                    line.fields[i].defattribs['data-xbrl-context'] = context;
                    this.registerField(line.fields[i], oldRef);
                    this.getModule().store.onFieldChange(line.fields[i]);
                }
                line.contextRefs = ctxResults;
                this.getModule().store.updateLinkedContextGrids(this.contextId);

            }
        } else if (!allDone) {
            line.incomplete = true;
            line.tr.addClass('biztax-cg-incomplete');
        }


    }
    , getFieldAttribs: function (el) {
        var attribs = {};
        for (var attr, i = 0, attrs = el.attributes, l = attrs.length; i < l; i++) {
            attr = attrs.item(i);
            attribs[attr.nodeName] = attr.nodeValue;
        }
        return attribs;
    },
    registerField: function (el, oldref) {
        return this.getModule().store.registerField(el, oldref);
    }
    , onFieldChange: function (fld) {
        // this.getModule().store.onFieldChange(fld);
    }
    , destroy: function () {
        this.removeAll();
        this.clearLines();
        eBook.BizTax.Contextgrid.superclass.destroy.call(this);
    }
});