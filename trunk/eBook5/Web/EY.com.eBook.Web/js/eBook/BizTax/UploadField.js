
/* VERSION 0.3 */
/*eBook.BizTax.FieldLabel = Ext.extend();

eBook.BizTax.FieldItem = Ext.extend();

eBook.BizTax.CodeLabel = Ext.extend();
*/

// or "bundle field"

eBook.BizTax.UploadField = Ext.extend(Ext.BoxComponent, {
    autoEl: {
        tag: 'div',
        cls: 'biztax-upload-field'
    }
    , mytpl: new Ext.XTemplate('<tpl for="."><ul><tpl for="data"><li class="{iconCls}">{title}</li></tpl></ul></tpl>', { compiled: true })
    , setValue: function (val) { }
    , initComponent: function () {
        Ext.apply(this, {
            autoEl: {
                tag: 'div',
                cls: 'biztax-upload-field'//,
                // html: 'JIPPIE'
            }
        });
        this.data = [];

        eBook.BizTax.UploadField.superclass.initComponent.apply(this, arguments);
    }
    , getModule: function () {
        return this.findParentByType('biztax-module');
    }
    , applyToMarkup: function (el) {
        this.allowDomMove = false;

        this.el = Ext.get(el);
        var parent = this.el.parent();
        this.el.remove();
        this.el = parent.createChild(this.autoEl);
        //parent.update('HERE COMES THE UPLOAD');
        this.render(parent);
    }
    , onRender: function (container, position) {
        eBook.BizTax.UploadField.superclass.onRender.apply(this, arguments);
        this.ul = this.el.createChild({ tag: 'ul' });
        this.buttons = this.el.createChild({
            tag: 'div', cls: 'biztax-upload-field-buttons'
            , children: [{ tag: 'div', cls: 'biztax-contentpane-button', html: 'EDIT'}]
        });
        this.mon(this.buttons.child('.biztax-contentpane-button'), 'click', this.onEdit, this);
        this.renderContents();
        this.validate()
    }
    , onEdit: function () {
        var w = new eBook.BizTax.UploadEditorWindow({ caller: this, data: this.data });
        w.show();
    }
    , getValue: function () {
        return Ext.clone(this.data);
    }
    , setValue: function (val) {
        var upd = false;
        this.data = val;
        if (this.data) {

            for (var i = this.data.length; i > -1; i--) {
                if (this.data[i] == "") {
                    upd = true;
                    this.data.splice(i, 1);
                }
            }
        }

        this.renderContents();
        this.validate();
        if (upd) {
            this.fireEvent('change', this);
        }
    }
    , updateValue: function (val) {
        this.setValue(val);
        // write to store
        this.fireEvent('change', this);
    }
    , renderContents: function () {
        this.ul.remove();
        if (this.data == null) this.data = [];
        this.ul = this.mytpl.insertBefore(this.buttons, this, true);
    }
    , markInValid: function () {
        this.el.addClass('x-form-invalid');
    }
    , markValid: function () {
        this.el.removeClass('x-form-invalid');
    }
    , isValid: function () {
        if (this.isRequired) {
            if (this.data == null || this.data.length == 0) {
                return false;
            }
        }
        return true;
    }
    , validate: function () {
        var bl = this.isValid();
        if (!bl) {
            this.markInValid();
        } else {
            this.markValid();
        }
        return bl;
    }
});

Ext.reg('biztax-uploadfield', eBook.BizTax.UploadField);