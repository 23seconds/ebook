﻿
// FIND OUT WHICH (rcorp/nrcorp/rle)
/*
eBook.BizTax.AY = { xtype: 'biztax-module',
    items: [

    { xtype: 'biztax-tab', title: 'Id - Ondernemingsgegevens'

    , code: 'Id'
    , oldCode: ''
    , xbrlId: 'tax-inc_CompanyInformationForm'
    , xbrlName: 'CompanyInformationForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Identificatiegegevens van de onderneming'

    , code: 'Entity information'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_EntityInformation'
    , xbrlName: 'EntityInformation'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-pane', title: 'Ondernemingsnummer'

    , code: 'Entity identifier'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_EntityIdentifier'
    , xbrlName: 'EntityIdentifier'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Type nummer'

    , code: 'Name of entity identifier'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_IdentifierName'
    , xbrlName: 'IdentifierName'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Opgave nummer'

    , code: 'Value of entity identifier'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_IdentifierValue'
    , xbrlName: 'IdentifierValue'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Naam'

    , code: 'Entity name'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_EntityName'
    , xbrlName: 'EntityName'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Wettelijke benaming'

    , code: 'Entity current legal name'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_EntityCurrentLegalName'
    , xbrlName: 'EntityCurrentLegalName'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Rechtsvorm'

    , code: 'Entity legal form'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_EntityForm'
    , xbrlName: 'EntityForm'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-list', label: 'Lijst'

    , code: 'Entity legal form head'
    , oldCode: ''
    , xbrlId: 'pfs-vl_LegalFormCodeHead'
    , xbrlName: 'LegalFormCodeHead'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , substitutionGroup: 'xbrli:item'
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere'

    , code: 'Entity legal form other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_LegalFormOther'
    , xbrlName: 'LegalFormOther'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Adres'

    , code: 'Entity address'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_EntityAddress'
    , xbrlName: 'EntityAddress'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-pane', title: 'Aard adres'

    , code: 'Address type'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_AddressType'
    , xbrlName: 'AddressType'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-list', label: 'Lijst'

    , code: 'Address type head'
    , oldCode: ''
    , xbrlId: 'pfs-vl_AddressTypeCodeHead'
    , xbrlName: 'AddressTypeCodeHead'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , substitutionGroup: 'xbrli:item'
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere'

    , code: 'Address type other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_AddressTypeOther'
    , xbrlName: 'AddressTypeOther'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Straat'

    , code: 'Street'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_Street'
    , xbrlName: 'Street'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Nr'

    , code: 'Number'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_Number'
    , xbrlName: 'Number'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bus'

    , code: 'Box'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_Box'
    , xbrlName: 'Box'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Postcode en gemeente'

    , code: 'Postal code and city'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_PostalCodeCity'
    , xbrlName: 'PostalCodeCity'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-list', label: 'Lijst'

    , code: 'Postal code head'
    , oldCode: ''
    , xbrlId: 'pfs-vl_PostalCodeHead'
    , xbrlName: 'PostalCodeHead'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , substitutionGroup: 'xbrli:item'
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Andere'

    , code: 'Postal code and city other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_PostalCodeCityOther'
    , xbrlName: 'PostalCodeCityOther'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Andere'

    , code: 'Postal code other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_PostalCodeOther'
    , xbrlName: 'PostalCodeOther'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gemeente'

    , code: 'City'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_City'
    , xbrlName: 'City'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Land'

    , code: 'Country code'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_CountryCode'
    , xbrlName: 'CountryCode'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-list', label: 'Lijst'

    , code: 'Country code head'
    , oldCode: ''
    , xbrlId: 'pfs-vl_CountryCodeHead'
    , xbrlName: 'CountryCodeHead'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , substitutionGroup: 'xbrli:item'
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere'

    , code: 'Country code other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_CountryCodeOther'
    , xbrlName: 'CountryCodeOther'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Bankinformatie'

    , code: 'Bank information'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_BankInformation'
    , xbrlName: 'BankInformation'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'BIC'

    , code: 'BIC'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_BankIdentifierCode'
    , xbrlName: 'BankIdentifierCode'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'IBAN'

    , code: 'IBAN'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_InternationalBankAccountNumber'
    , xbrlName: 'InternationalBankAccountNumber'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Identificerend nummer van de zonder vereffening ontbonden vennootschap'

    , code: 'Entity identifier of the company wound up without liquidation procedure'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_EntityIdentifierCompanyWoundUpWithoutLiquidationProcedure'
    , xbrlName: 'EntityIdentifierCompanyWoundUpWithoutLiquidationProcedure'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Type nummer'

    , code: 'Name of entity identifier'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_IdentifierName'
    , xbrlName: 'IdentifierName'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Opgave nummer'

    , code: 'Value of entity identifier'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_IdentifierValue'
    , xbrlName: 'IdentifierValue'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Inlichtingen omtrent het betreffende boekjaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PeriodsCovered'
    , xbrlName: 'PeriodsCovered'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Begindatum van het boekjaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PeriodStartDate'
    , xbrlName: 'PeriodStartDate'
    , itemType: 'xbrli:dateItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Einddatum van het boekjaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PeriodEndDate'
    , xbrlName: 'PeriodEndDate'
    , itemType: 'xbrli:dateItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Aanslagjaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AssessmentYear'
    , xbrlName: 'AssessmentYear'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aangifte'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxReturnType'
    , xbrlName: 'TaxReturnType'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Inlichtingen omtrent het document'

    , code: 'Document information'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_DocumentInformation'
    , xbrlName: 'DocumentInformation'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-pane', title: 'Contactpersoon'

    , code: 'Document contact'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_DocumentContact'
    , xbrlName: 'DocumentContact'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-pane', title: 'Hoedanigheid van de contactpersoon'

    , code: 'Type of document contact'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_DocumentContactType'
    , xbrlName: 'DocumentContactType'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-list', label: 'Lijst'

    , code: 'Type of document contact code head'
    , oldCode: ''
    , xbrlId: 'pfs-vl_DocumentContactTypeCodeHead'
    , xbrlName: 'DocumentContactTypeCodeHead'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , substitutionGroup: 'xbrli:item'
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Naam'

    , code: 'Name of contact'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_ContactName'
    , xbrlName: 'ContactName'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voornaam'

    , code: 'First name of contact'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_ContactFirstName'
    , xbrlName: 'ContactFirstName'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Functie'

    , code: 'Title or Position of contact'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_ContactTitlePosition'
    , xbrlName: 'ContactTitlePosition'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Adres'

    , code: 'Address of contact'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_ContactAddress'
    , xbrlName: 'ContactAddress'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-pane', title: 'Aard adres'

    , code: 'Address type'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_AddressType'
    , xbrlName: 'AddressType'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-list', label: 'Lijst'

    , code: 'Address type head'
    , oldCode: ''
    , xbrlId: 'pfs-vl_AddressTypeCodeHead'
    , xbrlName: 'AddressTypeCodeHead'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , substitutionGroup: 'xbrli:item'
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere'

    , code: 'Address type other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_AddressTypeOther'
    , xbrlName: 'AddressTypeOther'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Straat'

    , code: 'Street'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_Street'
    , xbrlName: 'Street'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Nr'

    , code: 'Number'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_Number'
    , xbrlName: 'Number'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bus'

    , code: 'Box'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_Box'
    , xbrlName: 'Box'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Postcode en gemeente'

    , code: 'Postal code and city'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_PostalCodeCity'
    , xbrlName: 'PostalCodeCity'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-list', label: 'Lijst'

    , code: 'Postal code head'
    , oldCode: ''
    , xbrlId: 'pfs-vl_PostalCodeHead'
    , xbrlName: 'PostalCodeHead'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , substitutionGroup: 'xbrli:item'
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Andere'

    , code: 'Postal code and city other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_PostalCodeCityOther'
    , xbrlName: 'PostalCodeCityOther'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Andere'

    , code: 'Postal code other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_PostalCodeOther'
    , xbrlName: 'PostalCodeOther'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gemeente'

    , code: 'City'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_City'
    , xbrlName: 'City'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Land'

    , code: 'Country code'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_CountryCode'
    , xbrlName: 'CountryCode'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-list', label: 'Lijst'

    , code: 'Country code head'
    , oldCode: ''
    , xbrlId: 'pfs-vl_CountryCodeHead'
    , xbrlName: 'CountryCodeHead'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , substitutionGroup: 'xbrli:item'
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere'

    , code: 'Country code other'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_CountryCodeOther'
    , xbrlName: 'CountryCodeOther'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Telefoonnummer of faxnummer'

    , code: 'Phone or fax of contact'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_ContactPhoneFaxNumber'
    , xbrlName: 'ContactPhoneFaxNumber'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Aanduiding of het een telefoon- dan wel een faxnummer betreft'

    , code: 'Description of the phone or fax number'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_PhoneFaxNumber'
    , xbrlName: 'PhoneFaxNumber'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Lokaal telefoonnummer'

    , code: 'Local phone number'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_LocalPhoneNumber'
    , xbrlName: 'LocalPhoneNumber'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'E-mail'

    , code: 'E-mail of contact'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_ContactEmail'
    , xbrlName: 'ContactEmail'
    , itemType: ''
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'E-mail adres'

    , code: 'E-mail address'
    , oldCode: ''
    , xbrlId: 'pfs-gcd_EmailAddress'
    , xbrlName: 'EmailAddress'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }


    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275.1.A - Aangifte in de vennootschapsbelasting'

    , code: '275.1.A'
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxReturnResidentCorporateTaxForm'
    , xbrlName: 'TaxReturnResidentCorporateTaxForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Reserves'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReservesSection'
    , xbrlName: 'ReservesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-pane', title: 'Belastbare gereserveerde winst'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxableReservedProfitTitle'
    , xbrlName: 'TaxableReservedProfitTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Belastbare reserves in het kapitaal en belastbare uitgiftepremies'

    , code: '1001 PN'
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxableReservesCapitalSharePremiums'
    , xbrlName: 'TaxableReservesCapitalSharePremiums'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Belastbaar gedeelte van de herwaarderingsmeerwaarden'

    , code: '1004'
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxablePortionRevaluationSurpluses'
    , xbrlName: 'TaxablePortionRevaluationSurpluses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Wettelijke reserve'

    , code: '1005'
    , oldCode: ''
    , xbrlId: 'tax-inc_LegalReserve'
    , xbrlName: 'LegalReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Onbeschikbare reserves'

    , code: '1006'
    , oldCode: ''
    , xbrlId: 'tax-inc_UnavailableReserves'
    , xbrlName: 'UnavailableReserves'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Beschikbare reserves'

    , code: '1007'
    , oldCode: ''
    , xbrlId: 'tax-inc_AvailableReserves'
    , xbrlName: 'AvailableReserves'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Overgedragen winst (verlies)'

    , code: '1008 PN'
    , oldCode: ''
    , xbrlId: 'tax-inc_AccumulatedProfitsLosses'
    , xbrlName: 'AccumulatedProfitsLosses'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Belastbare voorzieningen'

    , code: '1009'
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxableProvisions'
    , xbrlName: 'TaxableProvisions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-pane', title: 'Andere in de balans vermelde reserves'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherReservesTitle'
    , xbrlName: 'OtherReservesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Andere in de balans vermelde reserves'

    , code: '1010'
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherReserves'
    , xbrlName: 'OtherReserves'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-reso_ReservesOtherPrimaryConcepts'
    , isAbstract: false
    , periodType: 'instant'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Andere belastbare reserves'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherTaxableReservesTitle'
    , xbrlName: 'OtherTaxableReservesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Andere belastbare reserves'

    , code: '1011 PN'
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherTaxableReserves'
    , xbrlName: 'OtherTaxableReserves'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: 't-reso_ReservesOtherPrimaryConcepts'
    , isAbstract: false
    , periodType: 'instant'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Onzichtbare reserves'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_UndisclosedReservesTitle'
    , xbrlName: 'UndisclosedReservesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Belastbare waardeverminderingen'

    , code: '1020'
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxableWriteDownsUndisclosedReserve'
    , xbrlName: 'TaxableWriteDownsUndisclosedReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Overdreven afschrijvingen'

    , code: '1021'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExaggeratedDepreciationsUndisclosedReserve'
    , xbrlName: 'ExaggeratedDepreciationsUndisclosedReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere onderschattingen van activa'

    , code: '1022'
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherUnderestimationsAssetsUndisclosedReserve'
    , xbrlName: 'OtherUnderestimationsAssetsUndisclosedReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Overschattingen van passiva'

    , code: '1023'
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve'
    , xbrlName: 'OtherOverestimationsLiabilitiesUndisclosedReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Belastbare reserves'

    , code: '1040 PN'
    , oldCode: '004 005 012 013'
    , xbrlId: 'tax-inc_TaxableReserves'
    , xbrlName: 'TaxableReserves'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-pane', title: 'Aanpassingen in meer van de begintoestand der reserves'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AdjustmentsReservesPlusTitle'
    , xbrlName: 'AdjustmentsReservesPlusTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Meerwaarden op aandelen'

    , code: '1051'
    , oldCode: '006 (1)'
    , xbrlId: 'tax-inc_CapitalGainsShares'
    , xbrlName: 'CapitalGainsShares'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant-start'
    }
  ,
    { xtype: 'biztax-field', label: 'Terugnemingen van vroegere in verworpen uitgaven opgenomen waardeverminderingen op aandelen'

    , code: '1052'
    , oldCode: '006 (2)'
    , xbrlId: 'tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'
    , xbrlName: 'CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant-start'
    }
  ,
    { xtype: 'biztax-field', label: 'Definitieve vrijstelling tax shelter erkende audiovisuele werken'

    , code: '1053'
    , oldCode: '008'
    , xbrlId: 'tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'
    , xbrlName: 'DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant-start'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling gewestelijke premies en kapitaal- en interestsubsidies'

    , code: '1054'
    , oldCode: '014'
    , xbrlId: 'tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'
    , xbrlName: 'ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant-start'
    }
  ,
    { xtype: 'biztax-field', label: 'Definitieve vrijstelling winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'

    , code: '1055'
    , oldCode: '019'
    , xbrlId: 'tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'
    , xbrlName: 'FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant-start'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere'

    , code: '1056'
    , oldCode: '007'
    , xbrlId: 'tax-inc_OtherAdjustmentsReservesPlus'
    , xbrlName: 'OtherAdjustmentsReservesPlus'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant-start'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Aanpassingen in min van de begintoestand der reserves'

    , code: '1061'
    , oldCode: '009'
    , xbrlId: 'tax-inc_AdjustmentsReservesMinus'
    , xbrlName: 'AdjustmentsReservesMinus'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant-start'
    }
  ,
    { xtype: 'biztax-field', label: 'Belastbare reserves na aanpassing van de begintoestand der reserves'

    , code: '1070 PN'
    , oldCode: '010 011'
    , xbrlId: 'tax-inc_TaxableReservesAfterAdjustments'
    , xbrlName: 'TaxableReservesAfterAdjustments'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Belastbare gereserveerde winst'

    , code: '1080 PN'
    , oldCode: '020 021'
    , xbrlId: 'tax-inc_TaxableReservedProfit'
    , xbrlName: 'TaxableReservedProfit'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Vrijgestelde gereserveerde winst'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptReservedProfitTitle'
    , xbrlName: 'ExemptReservedProfitTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Vrijgestelde waardevermindering'

    , code: '1101'
    , oldCode: '301 316'
    , xbrlId: 'tax-inc_ExemptWriteDownDebtClaim'
    , xbrlName: 'ExemptWriteDownDebtClaim'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijgestelde voorziening'

    , code: '1102'
    , oldCode: '302 317'
    , xbrlId: 'tax-inc_ExemptProvisionRisksExpenses'
    , xbrlName: 'ExemptProvisionRisksExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitgedrukte maar niet-verwezenlijkte meerwaarden'

    , code: '1103'
    , oldCode: '303 318'
    , xbrlId: 'tax-inc_UnrealisedExpressedCapitalGainsExemptReserve'
    , xbrlName: 'UnrealisedExpressedCapitalGainsExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-pane', title: 'Verwezenlijkte meerwaarden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGainsTitle'
    , xbrlName: 'RealisedCapitalGainsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Gespreid te belasten meerwaarden op bepaalde effecten'

    , code: '1111'
    , oldCode: '305 320'
    , xbrlId: 'tax-inc_CapitalGainsSpecificSecuritiesExemptReserve'
    , xbrlName: 'CapitalGainsSpecificSecuritiesExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Gespreid te belasten meerwaarden op materiële en immateriële vaste activa'

    , code: '1112'
    , oldCode: '305 320'
    , xbrlId: 'tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'
    , xbrlName: 'CapitalGainsTangibleIntangibleFixedAssetsExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere verwezenlijkte meerwaarden'

    , code: '1113'
    , oldCode: '304 319'
    , xbrlId: 'tax-inc_OtherRealisedCapitalGainsExemptReserve'
    , xbrlName: 'OtherRealisedCapitalGainsExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Meerwaarden op bedrijfsvoertuigen'

    , code: '1114'
    , oldCode: '306 321'
    , xbrlId: 'tax-inc_CapitalGainsCorporateVehiclesExemptReserve'
    , xbrlName: 'CapitalGainsCorporateVehiclesExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Meerwaarden op binnenschepen'

    , code: '1115'
    , oldCode: '311 327'
    , xbrlId: 'tax-inc_CapitalGainsRiverVesselExemptReserve'
    , xbrlName: 'CapitalGainsRiverVesselExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Meerwaarden op zeeschepen'

    , code: '1116'
    , oldCode: '307 322'
    , xbrlId: 'tax-inc_CapitalGainsSeaVesselExemptReserve'
    , xbrlName: 'CapitalGainsSeaVesselExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Investeringsreserve'

    , code: '1121'
    , oldCode: '308 323'
    , xbrlId: 'tax-inc_ExemptInvestmentReserve'
    , xbrlName: 'ExemptInvestmentReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Tax shelter erkende audiovisuele werken'

    , code: '1122'
    , oldCode: '309 324'
    , xbrlId: 'tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve'
    , xbrlName: 'TaxShelterAuthorisedAudiovisualWorkExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'

    , code: '1123'
    , oldCode: '312 328'
    , xbrlId: 'tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'
    , xbrlName: 'ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere vrijgestelde bestanddelen'

    , code: '1124'
    , oldCode: '310 325'
    , xbrlId: 'tax-inc_OtherExemptReserves'
    , xbrlName: 'OtherExemptReserves'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijgestelde gereserveerde winst'

    , code: '1140'
    , oldCode: '315 326'
    , xbrlId: 'tax-inc_ExemptReservedProfit'
    , xbrlName: 'ExemptReservedProfit'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'instant'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Verworpen uitgaven'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DisallowedExpensesSection'
    , xbrlName: 'DisallowedExpensesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-aftrekbare belastingen'

    , code: '1201'
    , oldCode: '029'
    , xbrlId: 'tax-inc_NonDeductibleTaxes'
    , xbrlName: 'NonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gewestelijke belastingen, heffingen en retributies'

    , code: '1202'
    , oldCode: '028'
    , xbrlId: 'tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'
    , xbrlName: 'NonDeductibleRegionalTaxesDutiesRetributions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Geldboeten, verbeurdverklaringen en straffen van alle aard'

    , code: '1203'
    , oldCode: '030'
    , xbrlId: 'tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'
    , xbrlName: 'NonDeductibleFinesConfiscationsPenaltiesAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies'

    , code: '1204'
    , oldCode: '031'
    , xbrlId: 'tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
    , xbrlName: 'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen'

    , code: '1205'
    , oldCode: '032'
    , xbrlId: 'tax-inc_NonDeductibleCarExpensesLossValuesCars'
    , xbrlName: 'NonDeductibleCarExpensesLossValuesCars'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'

    , code: '1206'
    , oldCode: '022 074'
    , xbrlId: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'
    , xbrlName: 'NonDeductibleCarExpensesPartBenefitsAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken'

    , code: '1207'
    , oldCode: '033'
    , xbrlId: 'tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'
    , xbrlName: 'NonDeductibleReceptionBusinessGiftsExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare restaurantkosten'

    , code: '1208'
    , oldCode: '025'
    , xbrlId: 'tax-inc_NonDeductibleRestaurantExpenses'
    , xbrlName: 'NonDeductibleRestaurantExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Kosten voor niet-specifieke beroepskledij'

    , code: '1209'
    , oldCode: '034'
    , xbrlId: 'tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'
    , xbrlName: 'NonDeductibleNonSpecificProfessionalClothsExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Overdreven interesten'

    , code: '1210'
    , oldCode: '035'
    , xbrlId: 'tax-inc_ExaggeratedInterests'
    , xbrlName: 'ExaggeratedInterests'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Interesten met betrekking tot een gedeelte van bepaalde leningen'

    , code: '1211'
    , oldCode: '036'
    , xbrlId: 'tax-inc_NonDeductibleParticularPortionInterestsLoans'
    , xbrlName: 'NonDeductibleParticularPortionInterestsLoans'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Abnormale of goedgunstige voordelen'

    , code: '1212'
    , oldCode: '037'
    , xbrlId: 'tax-inc_AbnormalBenevolentAdvantages'
    , xbrlName: 'AbnormalBenevolentAdvantages'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Sociale voordelen'

    , code: '1214'
    , oldCode: '038'
    , xbrlId: 'tax-inc_NonDeductibleSocialAdvantages'
    , xbrlName: 'NonDeductibleSocialAdvantages'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques'

    , code: '1215'
    , oldCode: '023'
    , xbrlId: 'tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
    , xbrlName: 'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Liberaliteiten'

    , code: '1216'
    , oldCode: '039'
    , xbrlId: 'tax-inc_Liberalities'
    , xbrlName: 'Liberalities'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Waardeverminderingen en minderwaarden op aandelen'

    , code: '1217'
    , oldCode: '040'
    , xbrlId: 'tax-inc_WriteDownsLossValuesShares'
    , xbrlName: 'WriteDownsLossValuesShares'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Terugnemingen van vroegere vrijstellingen'

    , code: '1218'
    , oldCode: '041'
    , xbrlId: 'tax-inc_ReversalPreviousExemptions'
    , xbrlName: 'ReversalPreviousExemptions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Werknemersparticipatie'

    , code: '1219'
    , oldCode: '043 072'
    , xbrlId: 'tax-inc_EmployeeParticipation'
    , xbrlName: 'EmployeeParticipation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vergoedingen ontbrekende coupon'

    , code: '1220'
    , oldCode: '026'
    , xbrlId: 'tax-inc_IndemnityMissingCoupon'
    , xbrlName: 'IndemnityMissingCoupon'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Kosten tax shelter erkende audiovisuele werken'

    , code: '1221'
    , oldCode: '027'
    , xbrlId: 'tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'
    , xbrlName: 'ExpensesTaxShelterAuthorisedAudiovisualWork'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gewestelijke premies en kapitaal- en interestsubsidies'

    , code: '1222'
    , oldCode: '024'
    , xbrlId: 'tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'
    , xbrlName: 'RegionalPremiumCapitalSubsidiesInterestSubsidies'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare betalingen naar bepaalde Staten'

    , code: '1223'
    , oldCode: '054'
    , xbrlId: 'tax-inc_NonDeductiblePaymentsCertainStates'
    , xbrlName: 'NonDeductiblePaymentsCertainStates'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere verworpen uitgaven'

    , code: '1239'
    , oldCode: '042'
    , xbrlId: 'tax-inc_OtherDisallowedExpenses'
    , xbrlName: 'OtherDisallowedExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verworpen uitgaven'

    , code: '1240'
    , oldCode: '044'
    , xbrlId: 'tax-inc_DisallowedExpenses'
    , xbrlName: 'DisallowedExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Uitgekeerde dividenden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DividendsPaidSection'
    , xbrlName: 'DividendsPaidSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Gewone dividenden'

    , code: '1301'
    , oldCode: '050'
    , xbrlId: 'tax-inc_OrdinaryDividends'
    , xbrlName: 'OrdinaryDividends'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verkrijging van eigen aandelen'

    , code: '1302'
    , oldCode: '051'
    , xbrlId: 'tax-inc_AcquisitionOwnShares'
    , xbrlName: 'AcquisitionOwnShares'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Overlijden, uittreding of uitsluiting van een vennoot'

    , code: '1303'
    , oldCode: '052'
    , xbrlId: 'tax-inc_DeceaseDepartureExclusionPartner'
    , xbrlName: 'DeceaseDepartureExclusionPartner'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verdeling van maatschappelijk vermogen'

    , code: '1304'
    , oldCode: '053'
    , xbrlId: 'tax-inc_DistributionCompanyAssets'
    , xbrlName: 'DistributionCompanyAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitgekeerde dividenden'

    , code: '1320'
    , oldCode: '059'
    , xbrlId: 'tax-inc_TaxableDividendsPaid'
    , xbrlName: 'TaxableDividendsPaid'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Uiteenzetting van de winst'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_BreakdownProfitSection'
    , xbrlName: 'BreakdownProfitSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Belastbare gereserveerde winst'

    , code: '1080 PN'
    , oldCode: '020 021'
    , xbrlId: 'tax-inc_TaxableReservedProfit'
    , xbrlName: 'TaxableReservedProfit'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verworpen uitgaven'

    , code: '1240'
    , oldCode: '044'
    , xbrlId: 'tax-inc_DisallowedExpenses'
    , xbrlName: 'DisallowedExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitgekeerde dividenden'

    , code: '1320'
    , oldCode: '059'
    , xbrlId: 'tax-inc_TaxableDividendsPaid'
    , xbrlName: 'TaxableDividendsPaid'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Resultaat van het belastbare tijdperk'

    , code: '1410 PN'
    , oldCode: '060 061'
    , xbrlId: 'tax-inc_FiscalResult'
    , xbrlName: 'FiscalResult'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Werkelijk resultaat uit de zeescheepvaart waarvoor de winst wordt vastgesteld op basis van de tonnage'

    , code: '1411 PN'
    , oldCode: ''
    , xbrlId: 'tax-inc_ShippingResultTonnageBased'
    , xbrlName: 'ShippingResultTonnageBased'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage'

    , code: '1412 PN'
    , oldCode: '062 063'
    , xbrlId: 'tax-inc_ShippingResultNotTonnageBased'
    , xbrlName: 'ShippingResultNotTonnageBased'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Bestanddelen van het resultaat waarop de aftrekbeperking van toepassing is'

    , code: '1420'
    , oldCode: '075'
    , xbrlId: 'tax-inc_DeductionLimitElementsFiscalResult'
    , xbrlName: 'DeductionLimitElementsFiscalResult'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Verkregen abnormale of goedgunstige voordelen en verkregen financiële voordelen of voordelen van alle aard'

    , code: '1421'
    , oldCode: '070'
    , xbrlId: 'tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'
    , xbrlName: 'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve'

    , code: '1422'
    , oldCode: '071'
    , xbrlId: 'tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'
    , xbrlName: 'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'

    , code: '1206'
    , oldCode: '022 074'
    , xbrlId: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'
    , xbrlName: 'NonDeductibleCarExpensesPartBenefitsAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Werknemersparticipatie'

    , code: '1219'
    , oldCode: '043 072'
    , xbrlId: 'tax-inc_EmployeeParticipation'
    , xbrlName: 'EmployeeParticipation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw'

    , code: '1423'
    , oldCode: '076'
    , xbrlId: 'tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport'
    , xbrlName: 'CapitalSubsidiesInterestSubsidiesAgriculturalSupport'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Resterend resultaat'

    , code: '1430 PN'
    , oldCode: '077 078'
    , xbrlId: 'tax-inc_RemainingFiscalResultBeforeOriginDistribution'
    , xbrlName: 'RemainingFiscalResultBeforeOriginDistribution'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Verdeling volgens oorsprong'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OriginDistributionTitle'
    , xbrlName: 'OriginDistributionTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Resterend resultaat volgens oorsprong'

    , code: '1431 PN'
    , oldCode: '084 085 082 083 080 081'
    , xbrlId: 'tax-inc_RemainingFiscalResult'
    , xbrlName: 'RemainingFiscalResult'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: 'tax-inc-rcorp_ProfitOriginPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Aftrekken van de resterende winst'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductionsRemainingFiscalProfitTitle'
    , xbrlName: 'DeductionsRemainingFiscalProfitTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-belastbare bestanddelen'

    , code: '1432'
    , oldCode: '097 096'
    , xbrlId: 'tax-inc_MiscellaneousExemptions'
    , xbrlName: 'MiscellaneousExemptions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-rcorp_ProfitOriginPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten'

    , code: '1433'
    , oldCode: '099 098'
    , xbrlId: 'tax-inc_PEExemptIncomeMovableAssets'
    , xbrlName: 'PEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-rcorp_ProfitOriginPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aftrek voor octrooi-inkomsten'

    , code: '1434'
    , oldCode: '102 101'
    , xbrlId: 'tax-inc_DeductionPatentsIncome'
    , xbrlName: 'DeductionPatentsIncome'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-rcorp_ProfitOriginPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken'

    , code: '1435'
    , oldCode: '104 103'
    , xbrlId: 'tax-inc_AllowanceCorporateEquity'
    , xbrlName: 'AllowanceCorporateEquity'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-rcorp_ProfitOriginPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gecompenseerde verliezen'

    , code: '1436'
    , oldCode: '106 105 236'
    , xbrlId: 'tax-inc_CompensatedTaxLosses'
    , xbrlName: 'CompensatedTaxLosses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Investeringsaftrek'

    , code: '1437'
    , oldCode: '107'
    , xbrlId: 'tax-inc_AllowanceInvestmentDeduction'
    , xbrlName: 'AllowanceInvestmentDeduction'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-rcorp_ProfitOriginPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Resterende winst volgens oorsprong'

    , code: '1450'
    , oldCode: ''
    , xbrlId: 'tax-inc_RemainingFiscalProfitCommonRate'
    , xbrlName: 'RemainingFiscalProfitCommonRate'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Belastbare grondslag'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_BasicTaxableAmountTitle'
    , xbrlName: 'BasicTaxableAmountTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Belastbaar tegen gewoon tarief'

    , code: '1460'
    , oldCode: '112'
    , xbrlId: 'tax-inc_BasicTaxableAmountCommonRate'
    , xbrlName: 'BasicTaxableAmountCommonRate'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Resterende winst volgens oorsprong'

    , code: '1450'
    , oldCode: ''
    , xbrlId: 'tax-inc_RemainingFiscalProfitCommonRate'
    , xbrlName: 'RemainingFiscalProfitCommonRate'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Winst uit zeescheepvaart, vastgesteld op basis van de tonnage'

    , code: '1461'
    , oldCode: '108'
    , xbrlId: 'tax-inc_ShippingProfitTonnageBased'
    , xbrlName: 'ShippingProfitTonnageBased'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verkregen abnormale of goedgunstige voordelen en verkregen financiële voordelen of voordelen van alle aard'

    , code: '1421'
    , oldCode: '070'
    , xbrlId: 'tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'
    , xbrlName: 'BenevolentAbnormalFinancialAdvantagesBenefitsAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve'

    , code: '1422'
    , oldCode: '071'
    , xbrlId: 'tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'
    , xbrlName: 'ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'

    , code: '1206'
    , oldCode: '022 074'
    , xbrlId: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'
    , xbrlName: 'NonDeductibleCarExpensesPartBenefitsAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Werknemersparticipatie'

    , code: '1219'
    , oldCode: '043 072'
    , xbrlId: 'tax-inc_EmployeeParticipation'
    , xbrlName: 'EmployeeParticipation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Meerwaarden op aandelen belastbaar tegen 25%'

    , code: '1465'
    , oldCode: '118'
    , xbrlId: 'tax-inc_CapitalGainsSharesRate2500'
    , xbrlName: 'CapitalGainsSharesRate2500'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Belastbaar tegen exit tax tarief (16,5%)'

    , code: '1470'
    , oldCode: '115'
    , xbrlId: 'tax-inc_BasicTaxableAmountExitTaxRate'
    , xbrlName: 'BasicTaxableAmountExitTaxRate'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw, belastbaar tegen 5 %'

    , code: '1481'
    , oldCode: '119'
    , xbrlId: 'tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'
    , xbrlName: 'CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Afzonderlijke aanslagen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_SeparateAssessmentsSection'
    , xbrlName: 'SeparateAssessmentsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financiële voordelen of voordelen van alle aard'

    , code: '1501'
    , oldCode: '120'
    , xbrlId: 'tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'
    , xbrlName: 'UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen'

    , code: '1502'
    , oldCode: '123'
    , xbrlId: 'tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'
    , xbrlName: 'SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Afzonderlijke aanslag van de uitgekeerde dividenden ten name van vennootschappen die krediet voor ambachtsoutillage mogen verstrekken en vennootschappen voor huisvesting'

    , code: '1503'
    , oldCode: '124'
    , xbrlId: 'tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'
    , xbrlName: 'SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Bijzondere aanslagen met betrekking tot verrichtingen die vóór 1 januari 1990 hebben plaatsgevonden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_SpecialAssessmentPre1990Section'
    , xbrlName: 'SpecialAssessmentPre1990Section'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Gehele of gedeeltelijke verdeling van maatschappelijk vermogen, belastbaar tegen 33%'

    , code: '1511'
    , oldCode: '125'
    , xbrlId: 'tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'
    , xbrlName: 'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gehele of gedeeltelijke verdeling van maatschappelijk vermogen, belastbaar tegen 16,5%'

    , code: '1512'
    , oldCode: '126'
    , xbrlId: 'tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'
    , xbrlName: 'SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voordelen van alle aard verleend door vennootschappen in vereffening'

    , code: '1513'
    , oldCode: '128'
    , xbrlId: 'tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'
    , xbrlName: 'SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Aanvullende heffing erkende diamanthandelaars en terugbetaling van voorheen verleend belastingkrediet voor onderzoek en ontwikkeling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AdditionalDutiesAuthorisedDiamondTradersRetributionTaxCreditResearchDevelopmentSection'
    , xbrlName: 'AdditionalDutiesAuthorisedDiamondTradersRetributionTaxCreditResearchDevelopmentSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanvullende heffing erkende diamanthandelaars'

    , code: '1531'
    , oldCode: '109'
    , xbrlId: 'tax-inc_AdditionalDutiesDiamondTraders'
    , xbrlName: 'AdditionalDutiesDiamondTraders'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling'

    , code: '1532'
    , oldCode: '223'
    , xbrlId: 'tax-inc_RetributionTaxCreditResearchDevelopment'
    , xbrlName: 'RetributionTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Niet-belastbare bestanddelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_MiscellaneousExemptionsSection'
    , xbrlName: 'MiscellaneousExemptionsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Vrijgestelde giften'

    , code: '1601'
    , oldCode: '090'
    , xbrlId: 'tax-inc_ExemptGifts'
    , xbrlName: 'ExemptGifts'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling aanvullend personeel'

    , code: '1602'
    , oldCode: '091'
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions'
    , xbrlName: 'ExemptionAdditionalPersonnelMiscellaneousExemptions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling bijkomend personeel KMO'

    , code: '1603'
    , oldCode: '092'
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnelSMEs'
    , xbrlName: 'ExemptionAdditionalPersonnelSMEs'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling stagebonus'

    , code: '1604'
    , oldCode: '094'
    , xbrlId: 'tax-inc_ExemptionTrainingPeriodBonus'
    , xbrlName: 'ExemptionTrainingPeriodBonus'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere niet-belastbare bestanddelen'

    , code: '1605'
    , oldCode: '095'
    , xbrlId: 'tax-inc_OtherMiscellaneousExemptions'
    , xbrlName: 'OtherMiscellaneousExemptions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-belastbare bestanddelen'

    , code: '1610'
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductibleMiscellaneousExemptions'
    , xbrlName: 'DeductibleMiscellaneousExemptions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PEExemptIncomeMovableAssetsSection'
    , xbrlName: 'PEExemptIncomeMovableAssetsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten uit aandelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PEExemptIncomeMovableAssetsSharesTitle'
    , xbrlName: 'PEExemptIncomeMovableAssetsSharesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Inkomsten toegekend door een vennootschap gevestigd in een lidstaat van de EER'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_IncomeAttributedCompanyEEASharesPEExemptIncomeMovableAssetsTitle'
    , xbrlName: 'IncomeAttributedCompanyEEASharesPEExemptIncomeMovableAssetsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Nettobedrag, Belgische inkomsten'

    , code: '1631'
    , oldCode: '216'
    , xbrlId: 'tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'NetBelgianIncomeSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing, Belgische inkomsten'

    , code: '1632'
    , oldCode: '217'
    , xbrlId: 'tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Nettobedrag, buitenlandse inkomsten'

    , code: '1633'
    , oldCode: '218'
    , xbrlId: 'tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'NetForeignIncomeSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing, buitenlandse inkomsten'

    , code: '1634'
    , oldCode: '219'
    , xbrlId: 'tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Andere inkomsten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_IncomeAttributedOtherSharesPEExemptIncomeMovableAssetsTitle'
    , xbrlName: 'IncomeAttributedOtherSharesPEExemptIncomeMovableAssetsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Nettobedrag, Belgische inkomsten'

    , code: '1635'
    , oldCode: '220'
    , xbrlId: 'tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing, Belgische inkomsten'

    , code: '1636'
    , oldCode: '221'
    , xbrlId: 'tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Nettobedrag, buitenlandse inkomsten'

    , code: '1637'
    , oldCode: '225'
    , xbrlId: 'tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing, buitenlandse inkomsten'

    , code: '1638'
    , oldCode: '226'
    , xbrlId: 'tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Andere vrijgestelde roerende inkomsten'

    , code: '1639'
    , oldCode: '228'
    , xbrlId: 'tax-inc_OtherExemptIncomeMovableAssets'
    , xbrlName: 'OtherExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten'

    , code: '1640'
    , oldCode: '229'
    , xbrlId: 'tax-inc_GrossPEExemptIncomeMovableAssets'
    , xbrlName: 'GrossPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Kosten'

    , code: '1641'
    , oldCode: '230'
    , xbrlId: 'tax-inc_ExpensesSharesPEExemptIncomeMovableAssets'
    , xbrlName: 'ExpensesSharesPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten na aftrek kosten'

    , code: '1642'
    , oldCode: '231'
    , xbrlId: 'tax-inc_NetPEExemptIncomeMovableAssets'
    , xbrlName: 'NetPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Inkomsten uit niet-vergoede inbrengen in geval van fusie, splitsing of hiermee gelijkgestelde verrichtingen omdat de overnemende of verkrijgende vennootschap in het bezit is van aandelen van de overgenomen of gesplitste vennootschap of gelijkaardige verrichtingen in een andere EU-lidstaat'

    , code: '1643'
    , oldCode: '232'
    , xbrlId: 'tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'
    , xbrlName: 'IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijgestelde roerende inkomsten uit effecten van bepaalde herfinancieringsleningen'

    , code: '1644'
    , oldCode: '233'
    , xbrlId: 'tax-inc_ExemptIncomeMovableAssetsRefinancingLoans'
    , xbrlName: 'ExemptIncomeMovableAssetsRefinancingLoans'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Definitief belaste inkomsten en vrijgestelde roerende inkomsten'

    , code: '1650'
    , oldCode: '234'
    , xbrlId: 'tax-inc_DeductiblePEExemptIncomeMovableAssets'
    , xbrlName: 'DeductiblePEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Overdracht aftrek definitief belaste inkomsten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CarryOverPEExemptIncomeMovableAssetsSection'
    , xbrlName: 'CarryOverPEExemptIncomeMovableAssetsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Saldo van de overgedragen aftrek definitief belaste inkomsten'

    , code: '1701'
    , oldCode: '262'
    , xbrlId: 'tax-inc_AccumulatedPEExemptIncomeMovableAssets'
    , xbrlName: 'AccumulatedPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aftrek definitief belaste inkomsten van het belastbare tijdperk dat overdraagbaar is naar het volgende belastbare tijdperk'

    , code: '1702'
    , oldCode: '263'
    , xbrlId: 'tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'
    , xbrlName: 'CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Overgedragen aftrek definitief belaste inkomsten die werkelijk wordt afgetrokken van het belastbare tijdperk'

    , code: '1703'
    , oldCode: '264'
    , xbrlId: 'tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'
    , xbrlName: 'DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Saldo van de aftrek definitief belaste inkomsten dat overdraagbaar is naar het volgende belastbare tijdperk'

    , code: '1704'
    , oldCode: '265'
    , xbrlId: 'tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'
    , xbrlName: 'CarryOverNextTaxPeriodPEExemptIncomeMovableAssets'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Overdracht aftrek voor risicokapitaal'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CarryOverAllowanceCorporateEquitySection'
    , xbrlName: 'CarryOverAllowanceCorporateEquitySection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Saldo van de overgedragen aftrek voor risicokapitaal'

    , code: '1711'
    , oldCode: '330'
    , xbrlId: 'tax-inc_AccumulatedAllowanceCorporateEquity'
    , xbrlName: 'AccumulatedAllowanceCorporateEquity'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar het volgende belastbare tijdperk'

    , code: '1712'
    , oldCode: '332'
    , xbrlId: 'tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'
    , xbrlName: 'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Compenseerbare verliezen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CompensableTaxLossesSection'
    , xbrlName: 'CompensableTaxLossesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Saldo van de compenseerbare vorige verliezen'

    , code: '1721 N'
    , oldCode: '235'
    , xbrlId: 'tax-inc_CompensableTaxLosses'
    , xbrlName: 'CompensableTaxLosses'
    , itemType: 'pfs-dt:nonPositiveMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gecompenseerde verliezen'

    , code: '1436'
    , oldCode: '106 105 236'
    , xbrlId: 'tax-inc_CompensatedTaxLosses'
    , xbrlName: 'CompensatedTaxLosses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verlies van het belastbare tijdperk'

    , code: '1722 N'
    , oldCode: '237'
    , xbrlId: 'tax-inc_LossCurrentTaxPeriod'
    , xbrlName: 'LossCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonPositiveMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verlies over te brengen naar het volgende belastbare tijdperk'

    , code: '1730 N'
    , oldCode: '238'
    , xbrlId: 'tax-inc_CarryOverTaxLosses'
    , xbrlName: 'CarryOverTaxLosses'
    , itemType: 'pfs-dt:nonPositiveMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Tarief van de belasting'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxRateSection'
    , xbrlName: 'TaxRateSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'De vennootschap kan bij uw weten aanspraak maken op het verminderd tarief'

    , code: '1751'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExclusionReducedRate'
    , xbrlName: 'ExclusionReducedRate'
    , itemType: 'xbrli:booleanItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'De vennootschap is ofwel een vennootschap die krediet voor ambachtsoutillage mag verstrekken, ofwel een vennootschap voor huisvesting, belastbaar tegen het tarief van 5%'

    , code: '1752'
    , oldCode: ''
    , xbrlId: 'tax-inc_CreditCorporationTradeEquipmentHousingCorporationTaxRate'
    , xbrlName: 'CreditCorporationTradeEquipmentHousingCorporationTaxRate'
    , itemType: 'xbrli:booleanItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Voorafbetalingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PrepaymentsSection'
    , xbrlName: 'PrepaymentsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Deze aangifte heeft betrekking op één van de eerste drie boekjaren vanaf de oprichting van de vennootschap die een kleine vennootschap is in de zin van het Wetboek van vennootschappen'

    , code: '1801'
    , oldCode: '169'
    , xbrlId: 'tax-inc_FirstThreeAccountingYearsSmallCompanyCorporationCode'
    , xbrlName: 'FirstThreeAccountingYearsSmallCompanyCorporationCode'
    , itemType: 'xbrli:booleanItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'In aanmerking te nemen voorafbetalingen'

    , code: '1810'
    , oldCode: '175'
    , xbrlId: 'tax-inc_Prepayments'
    , xbrlName: 'Prepayments'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Voorafbetaling, eerste kwartaal'

    , code: '1811'
    , oldCode: '170'
    , xbrlId: 'tax-inc_PrepaymentFirstQuarter'
    , xbrlName: 'PrepaymentFirstQuarter'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voorafbetaling, tweede kwartaal'

    , code: '1812'
    , oldCode: '171'
    , xbrlId: 'tax-inc_PrepaymentSecondQuarter'
    , xbrlName: 'PrepaymentSecondQuarter'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voorafbetaling, derde kwartaal'

    , code: '1813'
    , oldCode: '172'
    , xbrlId: 'tax-inc_PrepaymentThirdQuarter'
    , xbrlName: 'PrepaymentThirdQuarter'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voorafbetaling, vierde kwartaal'

    , code: '1814'
    , oldCode: '173'
    , xbrlId: 'tax-inc_PrepaymentFourthQuarter'
    , xbrlName: 'PrepaymentFourthQuarter'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Referentienummer verschillend van het ondernemingsnummer en toegekend door de Dienst Voorafbetalingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierTitle'
    , xbrlName: 'PrepaymentReferenceNumberNotEntityIdentifierTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Eerste ander referentienummer'

    , code: '1821'
    , oldCode: '176'
    , xbrlId: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence'
    , xbrlName: 'PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence'
    , itemType: 'xbrli:integerItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Tweede ander referentienummer'

    , code: '1822'
    , oldCode: '177'
    , xbrlId: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence'
    , xbrlName: 'PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence'
    , itemType: 'xbrli:integerItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Derde ander referentienummer'

    , code: '1823'
    , oldCode: '178'
    , xbrlId: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence'
    , xbrlName: 'PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence'
    , itemType: 'xbrli:integerItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vierde ander referentienummer'

    , code: '1824'
    , oldCode: '179'
    , xbrlId: 'tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence'
    , xbrlName: 'PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence'
    , itemType: 'xbrli:integerItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Verrekenbare voorheffingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ClearableAdvanceLeviesSection'
    , xbrlName: 'ClearableAdvanceLeviesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Niet-terugbetaalbare voorheffingen'

    , code: '1830'
    , oldCode: '186'
    , xbrlId: 'tax-inc_NonRepayableAdvanceLevies'
    , xbrlName: 'NonRepayableAdvanceLevies'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Fictieve roerende voorheffing'

    , code: '1831'
    , oldCode: '182'
    , xbrlId: 'tax-inc_NonRepayableFictiousWitholdingTax'
    , xbrlName: 'NonRepayableFictiousWitholdingTax'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Forfaitair gedeelte van buitenlandse belasting'

    , code: '1832'
    , oldCode: '183'
    , xbrlId: 'tax-inc_NonRepayableLumpSumForeignTaxes'
    , xbrlName: 'NonRepayableLumpSumForeignTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling'

    , code: '1833'
    , oldCode: '184'
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopment'
    , xbrlName: 'TaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Terugbetaalbare voorheffingen'

    , code: '1840'
    , oldCode: '199'
    , xbrlId: 'tax-inc_RepayableAdvanceLevies'
    , xbrlName: 'RepayableAdvanceLevies'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'

    , code: '1841'
    , oldCode: '187'
    , xbrlId: 'tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'
    , xbrlName: 'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'

    , code: '1842'
    , oldCode: '188'
    , xbrlId: 'tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'
    , xbrlName: 'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'

    , code: '1843'
    , oldCode: '190'
    , xbrlId: 'tax-inc_RepayableWithholdingTaxOtherPEForeign'
    , xbrlName: 'RepayableWithholdingTaxOtherPEForeign'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen'

    , code: '1844'
    , oldCode: '192'
    , xbrlId: 'tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'
    , xbrlName: 'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing op andere dividenden'

    , code: '1845'
    , oldCode: '194'
    , xbrlId: 'tax-inc_RepayableWithholdingTaxOtherDividends'
    , xbrlName: 'RepayableWithholdingTaxOtherDividends'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere terugbetaalbare roerende voorheffing'

    , code: '1846'
    , oldCode: '195'
    , xbrlId: 'tax-inc_OtherRepayableWithholdingTaxes'
    , xbrlName: 'OtherRepayableWithholdingTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Belastingkrediet voor onderzoek en ontwikkeling dat voor het huidig belastbare tijdperk terugbetaalbaar is'

    , code: '1850'
    , oldCode: '198'
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
    , xbrlName: 'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Tax shelter'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxShelterSection'
    , xbrlName: 'TaxShelterSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'De vennootschap is een binnenlandse vennootschap voor de productie van audiovisuele werken, die als voornaamste doel de ontwikkeling en de productie van audiovisuele werken heeft en niet zijnde een televisieomroep of een onderneming die verbonden is met Belgische of buitenlandse televisieomroepen, die een raamovereenkomst voor de productie van een erkend Belgisch audiovisueel werk heeft gesloten'

    , code: '1861'
    , oldCode: '363'
    , xbrlId: 'tax-inc_BelgianCorporationAudiovisualWorksTaxShelterAgreement'
    , xbrlName: 'BelgianCorporationAudiovisualWorksTaxShelterAgreement'
    , itemType: 'xbrli:booleanItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Grootte van de vennootschap in de zin van het Wetboek van Vennootschappen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AssessmentCompanyCategorySection'
    , xbrlName: 'AssessmentCompanyCategorySection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Inlichtingen ter beoordeling kleine vennootschap'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AssessmentSmallCompanyCorporationCodeTitle'
    , xbrlName: 'AssessmentSmallCompanyCorporationCodeTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Gegevens met betrekking tot het belastbaar tijdperk'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AssessmentSmallCompanyCorporationCodeCurrentTaxPeriodTitle'
    , xbrlName: 'AssessmentSmallCompanyCorporationCodeCurrentTaxPeriodTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'De vennootschap is verbonden met één of meerdere andere vennootschappen in de zin van het Wetboek van Vennootschappen'

    , code: '1871'
    , oldCode: '268'
    , xbrlId: 'tax-inc_AssociatedCompanyCorporationCodeCurrentTaxPeriod'
    , xbrlName: 'AssociatedCompanyCorporationCodeCurrentTaxPeriod'
    , itemType: 'xbrli:booleanItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'De gegevens op niet-geconsolideerde basis vermelden, tenzij de vennootschap verbonden is met één of meerdere andere vennootschappen in de zin van het Wetboek van Vennootschappen, dan die gegevens op geconsolideerde basis vermelden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ConsolidatedDataIfApplicableTitle'
    , xbrlName: 'ConsolidatedDataIfApplicableTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Jaargemiddelde van het personeelsbestand'

    , code: '1872'
    , oldCode: '267'
    , xbrlId: 'tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'
    , xbrlName: 'AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Jaaromzet, exclusief btw'

    , code: '1873'
    , oldCode: '261'
    , xbrlId: 'tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'
    , xbrlName: 'AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Balanstotaal'

    , code: '1874'
    , oldCode: '251'
    , xbrlId: 'tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod'
    , xbrlName: 'BalanceSheetTotalCorporationCodeCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Diverse bescheiden en opgaven'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DocumentsStatementsSection'
    , xbrlName: 'DocumentsStatementsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'De verslagen aan en besluiten van de algemene vergadering en, indien verplicht, de jaarrekening (balans, resultatenrekening en eventuele toelichting) worden bij de aangifte gevoegd.'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_StatutoryAccountsGeneralMeetingMinutesDecisionsMandatoryAnnexes'
    , xbrlName: 'StatutoryAccountsGeneralMeetingMinutesDecisionsMandatoryAnnexes'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord moeten bij de aangifte worden gevoegd.'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DocumentsAddedExemptionProfitHomologationReorganizationPlanAmicableSettlement'
    , xbrlName: 'DocumentsAddedExemptionProfitHomologationReorganizationPlanAmicableSettlement'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'De opgaven/staat/aangifte waarmee aanspraak kan worden gemaakt op de toepassing van de desbetreffende wettelijke bepalingen, moeten in voorkomend geval worden ingevuld.'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DocumentsStatementsAddedTaxReturnIfMandatory'
    , xbrlName: 'DocumentsStatementsAddedTaxReturnIfMandatory'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275.1.B - Diverse bescheiden en opgaven'

    , code: '275.1.B'
    , oldCode: ''
    , xbrlId: 'tax-inc_DocumentsStatementsForm'
    , xbrlName: 'DocumentsStatementsForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Jaarrekening (balans, resultatenrekening en eventuele toelichting)'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_StatutoryAccountsTitle'
    , xbrlName: 'StatutoryAccountsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Bij te voegen, indien de belastingplichtige niet verplicht is deze neer te leggen bij de Balanscentrale van de Nationale Bank'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AnnualAccountsMandatoryAnnexeFilingCentralBalanceSheetOfficeNotRequired'
    , xbrlName: 'AnnualAccountsMandatoryAnnexeFilingCentralBalanceSheetOfficeNotRequired'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Jaarrekening (balans, resultatenrekening en eventuele toelichting)'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_StatutoryAccounts'
    , xbrlName: 'StatutoryAccounts'
    , itemType: 'xbrli:base64BinaryItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Verslagen aan en besluiten van de algemene vergadering'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_GeneralMeetingMinutesDecisionsTitle'
    , xbrlName: 'GeneralMeetingMinutesDecisionsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Verplicht bij te voegen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_GeneralMeetingMinutesDecisionsMandatoryAnnexe'
    , xbrlName: 'GeneralMeetingMinutesDecisionsMandatoryAnnexe'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verslagen aan en besluiten van de algemene vergadering'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_GeneralMeetingMinutesDecisions'
    , xbrlName: 'GeneralMeetingMinutesDecisions'
    , itemType: 'pfs-dt:nonEmptyBase64BinaryItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsTitle'
    , xbrlName: 'ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Verplicht bij te voegen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsMandatoryAnnexe'
    , xbrlName: 'ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsMandatoryAnnexe'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments'
    , xbrlName: 'ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments'
    , itemType: 'xbrli:base64BinaryItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Interne jaarrekening'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InternalStatutoryAccountsTitle'
    , xbrlName: 'InternalStatutoryAccountsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Optioneel bij te voegen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InternalStatutoryAccountsNonMandatoryAnnexe'
    , xbrlName: 'InternalStatutoryAccountsNonMandatoryAnnexe'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Interne jaarrekening'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InternalStatutoryAccounts'
    , xbrlName: 'InternalStatutoryAccounts'
    , itemType: 'xbrli:base64BinaryItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Afschrijvingstabellen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DepreciationTablesTitle'
    , xbrlName: 'DepreciationTablesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Optioneel bij te voegen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DepreciationTablesNonMandatoryAnnexe'
    , xbrlName: 'DepreciationTablesNonMandatoryAnnexe'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Afschrijvingstabel'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DepreciationTableTitle'
    , xbrlName: 'DepreciationTableTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Lineair afschrijvingspercentage'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AnnuityLinearDepreciation'
    , xbrlName: 'AnnuityLinearDepreciation'
    , itemType: 'pfs-dt:percentageItemType'
    , concept: 't-drfa_DepreciationRevaluationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Degressief afschrijvingspercentage'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AnnuityDegressiveDepreciation'
    , xbrlName: 'AnnuityDegressiveDepreciation'
    , itemType: 'pfs-dt:percentageItemType'
    , concept: 't-dfa_DepreciationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van belegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateInvestment'
    , xbrlName: 'DateInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Herwaarderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Revaluations'
    , xbrlName: 'Revaluations'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-drfa_DepreciationRevaluationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Afschrijvingen, zonder herwaarderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DepreciationsNoRevaluations'
    , xbrlName: 'DepreciationsNoRevaluations'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-drfa_DepreciationRevaluationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Afschrijvingen op herwaarderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DepreciationsRevaluations'
    , xbrlName: 'DepreciationsRevaluations'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-drfa_DepreciationRevaluationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Overdreven afschrijvingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExaggeratedDepreciations'
    , xbrlName: 'ExaggeratedDepreciations'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-drfa_DepreciationRevaluationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Afschrijvingstabellen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DepreciationTableNonStructuredTitle'
    , xbrlName: 'DepreciationTableNonStructuredTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Afschrijvingstabel niet-gestandaardiseerd'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DepreciationTableNonStructured'
    , xbrlName: 'DepreciationTableNonStructured'
    , itemType: 'xbrli:base64BinaryItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Detail verworpen uitgaven'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailDisallowedExpensesTitle'
    , xbrlName: 'DetailDisallowedExpensesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Optioneel bij te voegen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailDisallowedExpensesNonMandatoryAnnexe'
    , xbrlName: 'DetailDisallowedExpensesNonMandatoryAnnexe'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Niet-aftrekbare belastingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleTaxesTitle'
    , xbrlName: 'NonDeductibleTaxesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Niet-terugbetaalbare voorheffingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonRepayableAdvanceLeviesNonDeductibleTaxes'
    , xbrlName: 'NonRepayableAdvanceLeviesNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Fictieve roerende voorheffing'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'
    , xbrlName: 'NonRepayableFictiousWitholdingTaxNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Forfaitair gedeelte van buitenlandse belasting'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'
    , xbrlName: 'NonRepayableLumpSumForeignTaxesNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Terugbetaalbare voorheffingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RepayableAdvanceLeviesNonDeductibleTaxes'
    , xbrlName: 'RepayableAdvanceLeviesNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'
    , xbrlName: 'RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'
    , xbrlName: 'RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'
    , xbrlName: 'RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'
    , xbrlName: 'RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Roerende voorheffing op andere dividenden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'
    , xbrlName: 'RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere terugbetaalbare roerende voorheffing'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes'
    , xbrlName: 'OtherRepayableWithholdingTaxesNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'In aanmerking te nemen voorafbetalingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PrepaymentsNonDeductibleTaxes'
    , xbrlName: 'PrepaymentsNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Voorafbetaling, eerste kwartaal'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes'
    , xbrlName: 'PrepaymentFirstQuarterNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voorafbetaling, tweede kwartaal'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes'
    , xbrlName: 'PrepaymentSecondQuarterNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voorafbetaling, derde kwartaal'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes'
    , xbrlName: 'PrepaymentThirdQuarterNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Voorafbetaling, vierde kwartaal'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes'
    , xbrlName: 'PrepaymentFourthQuarterNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Vennnootschapsbelasting inclusief vermeerdering, verhoging en nalatigheidsinteresten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'
    , xbrlName: 'CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Geraamd bedrag der belastingschulden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EstimatedCorporateIncomeTaxDebt'
    , xbrlName: 'EstimatedCorporateIncomeTaxDebt'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Door de vennootschap gedragen roerende voorheffing op uitgekeerde dividenden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WithholdingTaxDividendsPaid'
    , xbrlName: 'WithholdingTaxDividendsPaid'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Door de vennootschap gedragen roerende voorheffing op andere uitbetaalde inkomsten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WithholdingTaxOtherIncomePaid'
    , xbrlName: 'WithholdingTaxOtherIncomePaid'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere niet-aftrekbare belastingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherNonDeductibleTaxes'
    , xbrlName: 'OtherNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare belastingen voor aftrek van terugbetalingen en regulariseringen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations'
    , xbrlName: 'NonDeductibleTaxesNoReimbursementsRegularizations'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Terugbetalingen en regulariseringen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes'
    , xbrlName: 'ReimbursementsRegularizationsNonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare belastingen'

    , code: '1201'
    , oldCode: '029'
    , xbrlId: 'tax-inc_NonDeductibleTaxes'
    , xbrlName: 'NonDeductibleTaxes'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Gewestelijke belastingen, heffingen en retributies'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleRegionalTaxesDutiesRetributionsTitle'
    , xbrlName: 'NonDeductibleRegionalTaxesDutiesRetributionsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Gewestelijke belastingen, heffingen en retributies'

    , code: '1202'
    , oldCode: '028'
    , xbrlId: 'tax-inc_NonDeductibleRegionalTaxesDutiesRetributions'
    , xbrlName: 'NonDeductibleRegionalTaxesDutiesRetributions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleRegionalTaxesDutiesRetributions'
    , xbrlName: 'DetailNonDeductibleRegionalTaxesDutiesRetributions'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Geldboeten, verbeurdverklaringen en straffen van alle aard'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKindTitle'
    , xbrlName: 'NonDeductibleFinesConfiscationsPenaltiesAllKindTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Geldboeten, verbeurdverklaringen en straffen van alle aard'

    , code: '1203'
    , oldCode: '030'
    , xbrlId: 'tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind'
    , xbrlName: 'NonDeductibleFinesConfiscationsPenaltiesAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleFinesConfiscationsPenaltiesAllKind'
    , xbrlName: 'DetailNonDeductibleFinesConfiscationsPenaltiesAllKind'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiumsTitle'
    , xbrlName: 'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiumsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies'

    , code: '1204'
    , oldCode: '031'
    , xbrlId: 'tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
    , xbrlName: 'NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
    , xbrlName: 'DetailNonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleCarExpensesLossValuesCarsTitle'
    , xbrlName: 'NonDeductibleCarExpensesLossValuesCarsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Kost'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CarExpense'
    , xbrlName: 'CarExpense'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Minderwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_LossValueCar'
    , xbrlName: 'LossValueCar'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbaar percentage'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'
    , xbrlName: 'NonDeductibleAnnuityNonDeductibleCarExpensesLossValuesCars'
    , itemType: 'pfs-dt:percentageItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare autokosten en minderwaarden op autovoertuigen'

    , code: '1205'
    , oldCode: '032'
    , xbrlId: 'tax-inc_NonDeductibleCarExpensesLossValuesCars'
    , xbrlName: 'NonDeductibleCarExpensesLossValuesCars'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vergoeding als schadeloosstelling voor herstellingskosten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CompensationCarRepairCosts'
    , xbrlName: 'CompensationCarRepairCosts'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleCarExpensesLossValuesCars'
    , xbrlName: 'DetailNonDeductibleCarExpensesLossValuesCars'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKindTitle'
    , xbrlName: 'NonDeductibleCarExpensesPartBenefitsAllKindTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Autokosten ten belope van een gedeelte van het voordeel van alle aard'

    , code: '1206'
    , oldCode: '022 074'
    , xbrlId: 'tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind'
    , xbrlName: 'NonDeductibleCarExpensesPartBenefitsAllKind'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleCarExpensesPartBenefitsAllKind'
    , xbrlName: 'DetailNonDeductibleCarExpensesPartBenefitsAllKind'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleReceptionBusinessGiftsExpensesTitle'
    , xbrlName: 'NonDeductibleReceptionBusinessGiftsExpensesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Kost'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReceptionBusinessGiftsExpense'
    , xbrlName: 'ReceptionBusinessGiftsExpense'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbaar percentage'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'
    , xbrlName: 'NonDeductibleAnnuityNonDeductibleReceptionBusinessGiftsExpenses'
    , itemType: 'pfs-dt:percentageItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken'

    , code: '1207'
    , oldCode: '033'
    , xbrlId: 'tax-inc_NonDeductibleReceptionBusinessGiftsExpenses'
    , xbrlName: 'NonDeductibleReceptionBusinessGiftsExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleReceptionBusinessGiftsExpenses'
    , xbrlName: 'DetailNonDeductibleReceptionBusinessGiftsExpenses'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Niet-aftrekbare restaurantkosten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleRestaurantExpensesTitle'
    , xbrlName: 'NonDeductibleRestaurantExpensesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Kost'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RestaurantExpense'
    , xbrlName: 'RestaurantExpense'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbaar percentage'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'
    , xbrlName: 'NonDeductibleAnnuityDetailNonDeductibleRestaurantExpenses'
    , itemType: 'pfs-dt:percentageItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-aftrekbare restaurantkosten'

    , code: '1208'
    , oldCode: '025'
    , xbrlId: 'tax-inc_NonDeductibleRestaurantExpenses'
    , xbrlName: 'NonDeductibleRestaurantExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleRestaurantExpenses'
    , xbrlName: 'DetailNonDeductibleRestaurantExpenses'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Kosten voor niet-specifieke beroepskledij'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleNonSpecificProfessionalClothsExpensesTitle'
    , xbrlName: 'NonDeductibleNonSpecificProfessionalClothsExpensesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Kosten voor niet-specifieke beroepskledij'

    , code: '1209'
    , oldCode: '034'
    , xbrlId: 'tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses'
    , xbrlName: 'NonDeductibleNonSpecificProfessionalClothsExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleNonSpecificProfessionalClothsExpenses'
    , xbrlName: 'DetailNonDeductibleNonSpecificProfessionalClothsExpenses'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Overdreven interesten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExaggeratedInterestsTitle'
    , xbrlName: 'ExaggeratedInterestsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Overdreven interesten'

    , code: '1210'
    , oldCode: '035'
    , xbrlId: 'tax-inc_ExaggeratedInterests'
    , xbrlName: 'ExaggeratedInterests'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailExaggeratedInterests'
    , xbrlName: 'DetailExaggeratedInterests'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Interesten met betrekking tot een gedeelte van bepaalde leningen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleParticularPortionInterestsLoansTitle'
    , xbrlName: 'NonDeductibleParticularPortionInterestsLoansTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Interesten met betrekking tot een gedeelte van bepaalde leningen'

    , code: '1211'
    , oldCode: '036'
    , xbrlId: 'tax-inc_NonDeductibleParticularPortionInterestsLoans'
    , xbrlName: 'NonDeductibleParticularPortionInterestsLoans'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleParticularPortionInterestsLoans'
    , xbrlName: 'DetailNonDeductibleParticularPortionInterestsLoans'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Abnormale of goedgunstige voordelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AbnormalBenevolentAdvantagesTitle'
    , xbrlName: 'AbnormalBenevolentAdvantagesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Abnormale of goedgunstige voordelen'

    , code: '1212'
    , oldCode: '037'
    , xbrlId: 'tax-inc_AbnormalBenevolentAdvantages'
    , xbrlName: 'AbnormalBenevolentAdvantages'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailAbnormalBenevolentAdvantages'
    , xbrlName: 'DetailAbnormalBenevolentAdvantages'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Sociale voordelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleSocialAdvantagesTitle'
    , xbrlName: 'NonDeductibleSocialAdvantagesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Sociale voordelen'

    , code: '1214'
    , oldCode: '038'
    , xbrlId: 'tax-inc_NonDeductibleSocialAdvantages'
    , xbrlName: 'NonDeductibleSocialAdvantages'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleSocialAdvantages'
    , xbrlName: 'DetailNonDeductibleSocialAdvantages'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchersTitle'
    , xbrlName: 'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchersTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Voordelen uit maaltijd-, sport-, cultuur- of ecocheques'

    , code: '1215'
    , oldCode: '023'
    , xbrlId: 'tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
    , xbrlName: 'NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
    , xbrlName: 'DetailNonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Liberaliteiten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_LiberalitiesTitle'
    , xbrlName: 'LiberalitiesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Liberaliteiten'

    , code: '1216'
    , oldCode: '039'
    , xbrlId: 'tax-inc_Liberalities'
    , xbrlName: 'Liberalities'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailLiberalities'
    , xbrlName: 'DetailLiberalities'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Waardeverminderingen en minderwaarden op aandelen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WriteDownsLossValuesSharesTitle'
    , xbrlName: 'WriteDownsLossValuesSharesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Waardeverminderingen en minderwaarden op aandelen'

    , code: '1217'
    , oldCode: '040'
    , xbrlId: 'tax-inc_WriteDownsLossValuesShares'
    , xbrlName: 'WriteDownsLossValuesShares'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailWriteDownsLossValuesShares'
    , xbrlName: 'DetailWriteDownsLossValuesShares'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Terugnemingen van vroegere vrijstellingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReversalPreviousExemptionsTitle'
    , xbrlName: 'ReversalPreviousExemptionsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Terugnemingen van vroegere vrijstellingen'

    , code: '1218'
    , oldCode: '041'
    , xbrlId: 'tax-inc_ReversalPreviousExemptions'
    , xbrlName: 'ReversalPreviousExemptions'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailReversalPreviousExemptions'
    , xbrlName: 'DetailReversalPreviousExemptions'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Werknemersparticipatie'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EmployeeParticipationTitle'
    , xbrlName: 'EmployeeParticipationTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Werknemersparticipatie'

    , code: '1219'
    , oldCode: '043 072'
    , xbrlId: 'tax-inc_EmployeeParticipation'
    , xbrlName: 'EmployeeParticipation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailEmployeeParticipation'
    , xbrlName: 'DetailEmployeeParticipation'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Vergoedingen ontbrekende coupon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_IndemnityMissingCouponTitle'
    , xbrlName: 'IndemnityMissingCouponTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Vergoedingen ontbrekende coupon'

    , code: '1220'
    , oldCode: '026'
    , xbrlId: 'tax-inc_IndemnityMissingCoupon'
    , xbrlName: 'IndemnityMissingCoupon'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailIndemnityMissingCoupon'
    , xbrlName: 'DetailIndemnityMissingCoupon'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Kosten tax shelter erkende audiovisuele werken'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWorkTitle'
    , xbrlName: 'ExpensesTaxShelterAuthorisedAudiovisualWorkTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Kosten tax shelter erkende audiovisuele werken'

    , code: '1221'
    , oldCode: '027'
    , xbrlId: 'tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork'
    , xbrlName: 'ExpensesTaxShelterAuthorisedAudiovisualWork'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailExpensesTaxShelterAuthorisedAudiovisualWork'
    , xbrlName: 'DetailExpensesTaxShelterAuthorisedAudiovisualWork'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Gewestelijke premies en kapitaal- en interestsubsidies'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidiesTitle'
    , xbrlName: 'RegionalPremiumCapitalSubsidiesInterestSubsidiesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Gewestelijke premies en kapitaal- en interestsubsidies'

    , code: '1222'
    , oldCode: '024'
    , xbrlId: 'tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies'
    , xbrlName: 'RegionalPremiumCapitalSubsidiesInterestSubsidies'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailRegionalPremiumCapitalSubsidiesInterestSubsidies'
    , xbrlName: 'DetailRegionalPremiumCapitalSubsidiesInterestSubsidies'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Niet-aftrekbare betalingen naar bepaalde Staten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonDeductiblePaymentsCertainStatesTitle'
    , xbrlName: 'NonDeductiblePaymentsCertainStatesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-aftrekbare betalingen naar bepaalde Staten'

    , code: '1223'
    , oldCode: '054'
    , xbrlId: 'tax-inc_NonDeductiblePaymentsCertainStates'
    , xbrlName: 'NonDeductiblePaymentsCertainStates'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailNonDeductiblePaymentsCertainStates'
    , xbrlName: 'DetailNonDeductiblePaymentsCertainStates'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Andere verworpen uitgaven'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherDisallowedExpensesTitle'
    , xbrlName: 'OtherDisallowedExpensesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Andere verworpen uitgaven'

    , code: '1239'
    , oldCode: '042'
    , xbrlId: 'tax-inc_OtherDisallowedExpenses'
    , xbrlName: 'OtherDisallowedExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailOtherDisallowedExpenses'
    , xbrlName: 'DetailOtherDisallowedExpenses'
    , itemType: 'xbrli:stringItemType'
    , concept: 'tax-inc-nrcorp_DetailDisallowedExpensesPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Andere'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherDocumentsTitle'
    , xbrlName: 'OtherDocumentsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Optioneel bij te voegen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherDocumentsNonMandatoryAnnexe'
    , xbrlName: 'OtherDocumentsNonMandatoryAnnexe'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Andere'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherDocuments'
    , xbrlName: 'OtherDocuments'
    , itemType: 'xbrli:base64BinaryItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '204.3 - Waardeverminderingen voor waarschijnlijke verliezen en voorzieningen voor risico-s en kosten'

    , code: '204.3'
    , oldCode: ''
    , xbrlId: 'tax-inc_WriteDownsDebtClaimsProvisionsExpensesRisksForm'
    , xbrlName: 'WriteDownsDebtClaimsProvisionsExpensesRisksForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Waardeverminderingen voor waarschijnlijke verliezen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WriteDownsDebtClaimsSection'
    , xbrlName: 'WriteDownsDebtClaimsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Naam van de schuldenaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_IdentityTradeDebtor'
    , xbrlName: 'IdentityTradeDebtor'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-wddc_WriteDownDebtClaimsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Adres van de schuldenaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AddressTradeDebtor'
    , xbrlName: 'AddressTradeDebtor'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-wddc_WriteDownDebtClaimsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Schuldvordering'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DebtClaim'
    , xbrlName: 'DebtClaim'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-wddc_WriteDownDebtClaimsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijgestelde waardevermindering'

    , code: '1101'
    , oldCode: '301 316'
    , xbrlId: 'tax-inc_ExemptWriteDownDebtClaim'
    , xbrlName: 'ExemptWriteDownDebtClaim'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-pane', title: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DecreaseExemptWriteDownDebtClaimTitle'
    , xbrlName: 'DecreaseExemptWriteDownDebtClaimTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen die overeenstemt met verliezen die tijdens het boekjaar definitief geworden zijn'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'
    , xbrlName: 'DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-wddc_WriteDownDebtClaimsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge de gehele of gedeeltelijke inning van de vordering tijdens het boekjaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'
    , xbrlName: 'DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-wddc_WriteDownDebtClaimsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vermindering vrijgestelde waardevermindering op handelsvorderingen ingevolge een nieuwe schatting van het waarschijnlijke verlies tijdens het boekjaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'
    , xbrlName: 'DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-wddc_WriteDownDebtClaimsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Verhoging vrijgestelde waardevermindering tijdens het boekjaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_IncreaseExemptWriteDownDebtClaim'
    , xbrlName: 'IncreaseExemptWriteDownDebtClaim'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-wddc_WriteDownDebtClaimsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verantwoording vrijgestelde waardevermindering'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_JustificationExemptWriteDown'
    , xbrlName: 'JustificationExemptWriteDown'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-wddc_WriteDownDebtClaimsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '204.3 - Waardeverminderingen voor waarschijnlijke verliezen en voorzieningen voor risico-s en kosten'

    , code: '204.3'
    , oldCode: ''
    , xbrlId: 'tax-inc_WriteDownsDebtClaimsProvisionsExpensesRisksForm'
    , xbrlName: 'WriteDownsDebtClaimsProvisionsExpensesRisksForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Voorzieningen voor risico-s en kosten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ProvisionsRisksExpensesSection'
    , xbrlName: 'ProvisionsRisksExpensesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Waarschijnlijke kost'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ProbableCost'
    , xbrlName: 'ProbableCost'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-prre_ProvisionRiskExpensePrimaryConcepts'
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijgestelde voorziening'

    , code: '1102'
    , oldCode: '302 317'
    , xbrlId: 'tax-inc_ExemptProvisionRisksExpenses'
    , xbrlName: 'ExemptProvisionRisksExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-pane', title: 'Vermindering van de vrijgestelde voorziening'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DecreaseExemptProvisionsRisksExpensesTitle'
    , xbrlName: 'DecreaseExemptProvisionsRisksExpensesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'
    , xbrlName: 'DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-prre_ProvisionRiskExpensePrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'
    , xbrlName: 'DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-prre_ProvisionRiskExpensePrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Verhoging van de vrijgestelde voorziening'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_IncreaseExemptProvisionRisksExpenses'
    , xbrlName: 'IncreaseExemptProvisionRisksExpenses'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-prre_ProvisionRiskExpensePrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verantwoording vrijgestelde voorziening'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_JustificationExemptProvision'
    , xbrlName: 'JustificationExemptProvision'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-prre_ProvisionRiskExpensePrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275B - Vrijstelling van meerwaarden op zeeschepen'

    , code: '275B'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionCapitalGainsSeaVesselsForm'
    , xbrlName: 'ExemptionCapitalGainsSeaVesselsForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Meerwaarden op zeeschepen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CapitalGainsSeaVesselSection'
    , xbrlName: 'CapitalGainsSeaVesselSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Omschrijving'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Description'
    , xbrlName: 'Description'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van belegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateInvestment'
    , xbrlName: 'DateInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van vervreemding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRealisation'
    , xbrlName: 'DateRealisation'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verkoopwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedSellingValue'
    , xbrlName: 'RealisedSellingValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscaal aangenomen afschrijvingen of waardeverminderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxAcceptedDepreciationsWriteDownsInvestment'
    , xbrlName: 'TaxAcceptedDepreciationsWriteDownsInvestment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscale nettowaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FiscalNetWorth'
    , xbrlName: 'FiscalNetWorth'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGain'
    , xbrlName: 'RealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275K - Gespreide belasting van meerwaarden op bepaalde effecten'

    , code: '275K'
    , oldCode: ''
    , xbrlId: 'tax-inc_SpreadTaxableCapitalGainsSpecificSecuritiesForm'
    , xbrlName: 'SpreadTaxableCapitalGainsSpecificSecuritiesForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Gespreid te belasten meerwaarden op bepaalde effecten, uitgegeven of gewaarborgd door openbare instellingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CapitalGainsSpecificSecuritiesSection'
    , xbrlName: 'CapitalGainsSpecificSecuritiesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Omschrijving'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Description'
    , xbrlName: 'Description'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van belegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateInvestment'
    , xbrlName: 'DateInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van vervreemding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRealisation'
    , xbrlName: 'DateRealisation'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verkoopprijs'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_SellingPrice'
    , xbrlName: 'SellingPrice'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aanschaffingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionValue'
    , xbrlName: 'AcquisitionValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscaal aangenomen waardeverminderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxAcceptedWriteDownsInvestment'
    , xbrlName: 'TaxAcceptedWriteDownsInvestment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscale nettowaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FiscalNetWorth'
    , xbrlName: 'FiscalNetWorth'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGain'
    , xbrlName: 'RealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Monetair gedeelte verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_MonetaryFractionRealisedCapitalGain'
    , xbrlName: 'MonetaryFractionRealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
    , xbrlName: 'NonMonetaryFractionRealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276K - Gespreid te belasten meerwaarden op materiële en immateriële vaste activa'

    , code: '276K'
    , oldCode: ''
    , xbrlId: 'tax-inc_SpreadTaxableCapitalGainsTangibleIntangibleFixedAssetsForm'
    , xbrlName: 'SpreadTaxableCapitalGainsTangibleIntangibleFixedAssetsForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Meerwaarden op materiële en immateriële vaste activa'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CapitalGainsTangibleIntangibleFixedAssetsSection'
    , xbrlName: 'CapitalGainsTangibleIntangibleFixedAssetsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Omschrijving'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Description'
    , xbrlName: 'Description'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van belegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateInvestment'
    , xbrlName: 'DateInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van vervreemding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRealisation'
    , xbrlName: 'DateRealisation'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verkoopwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedSellingValue'
    , xbrlName: 'RealisedSellingValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Ontvangen vergoeding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReceivedCompensation'
    , xbrlName: 'ReceivedCompensation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Kosten van vervreemding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CostRealisation'
    , xbrlName: 'CostRealisation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscaal aangenomen afschrijvingen of waardeverminderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxAcceptedDepreciationsWriteDownsInvestment'
    , xbrlName: 'TaxAcceptedDepreciationsWriteDownsInvestment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscale nettowaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FiscalNetWorth'
    , xbrlName: 'FiscalNetWorth'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGain'
    , xbrlName: 'RealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Monetair gedeelte verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_MonetaryFractionRealisedCapitalGain'
    , xbrlName: 'MonetaryFractionRealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
    , xbrlName: 'NonMonetaryFractionRealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijwillig verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_VoluntaryRealisedCapitalGain'
    , xbrlName: 'VoluntaryRealisedCapitalGain'
    , itemType: 'xbrli:booleanItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Investeringsverbintenis activa te beleggen in gebouwd onroerende goederen, vaartuigen of vliegtuigen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentCommitment'
    , xbrlName: 'InvestmentCommitment'
    , itemType: 'xbrli:booleanItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276N - Vrijstelling van meerwaarden op bedrijfsvoertuigen'

    , code: '276N'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionCapitalGainsCorporateVehiclesForm'
    , xbrlName: 'ExemptionCapitalGainsCorporateVehiclesForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Meerwaarden op bedrijfsvoertuigen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CapitalGainsCorporateVehiclesSection'
    , xbrlName: 'CapitalGainsCorporateVehiclesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Omschrijving'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Description'
    , xbrlName: 'Description'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van belegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateInvestment'
    , xbrlName: 'DateInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van vervreemding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRealisation'
    , xbrlName: 'DateRealisation'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van ontvangst schadevergoeding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateCompensationReceipt'
    , xbrlName: 'DateCompensationReceipt'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verkoopwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedSellingValue'
    , xbrlName: 'RealisedSellingValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Ontvangen vergoeding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReceivedCompensation'
    , xbrlName: 'ReceivedCompensation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Kosten van vervreemding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CostRealisation'
    , xbrlName: 'CostRealisation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscaal aangenomen afschrijvingen of waardeverminderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxAcceptedDepreciationsWriteDownsInvestment'
    , xbrlName: 'TaxAcceptedDepreciationsWriteDownsInvestment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscale nettowaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FiscalNetWorth'
    , xbrlName: 'FiscalNetWorth'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGain'
    , xbrlName: 'RealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijwillig verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_VoluntaryRealisedCapitalGain'
    , xbrlName: 'VoluntaryRealisedCapitalGain'
    , itemType: 'xbrli:booleanItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276P - Vrijstelling van meerwaarden op binnenschepen voor commerciële vaart'

    , code: '276P'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionCapitalGainsRiverVesselsCommercialTradeForm'
    , xbrlName: 'ExemptionCapitalGainsRiverVesselsCommercialTradeForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Meerwaarden op binnenschepen voor de commerciële vaart'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CapitalGainsRiverVesselSection'
    , xbrlName: 'CapitalGainsRiverVesselSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Omschrijving'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Description'
    , xbrlName: 'Description'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van belegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateInvestment'
    , xbrlName: 'DateInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van vervreemding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRealisation'
    , xbrlName: 'DateRealisation'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van ontvangst schadevergoeding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateCompensationReceipt'
    , xbrlName: 'DateCompensationReceipt'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verkoopwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedSellingValue'
    , xbrlName: 'RealisedSellingValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Ontvangen vergoeding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReceivedCompensation'
    , xbrlName: 'ReceivedCompensation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Kosten van vervreemding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CostRealisation'
    , xbrlName: 'CostRealisation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscaal aangenomen afschrijvingen of waardeverminderingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxAcceptedDepreciationsWriteDownsInvestment'
    , xbrlName: 'TaxAcceptedDepreciationsWriteDownsInvestment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Fiscale nettowaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FiscalNetWorth'
    , xbrlName: 'FiscalNetWorth'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGain'
    , xbrlName: 'RealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijwillig verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_VoluntaryRealisedCapitalGain'
    , xbrlName: 'VoluntaryRealisedCapitalGain'
    , itemType: 'xbrli:booleanItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275B - Vrijstelling van meerwaarden op zeeschepen'

    , code: '275B'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionCapitalGainsSeaVesselsForm'
    , xbrlName: 'ExemptionCapitalGainsSeaVesselsForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Herbeleggingen in zeeschepen, delen in mede-eigendom in zeeschepen, in scheepsaandelen of in aandelen van een vennootschap-scheepsexploitant met maatschappelijke zetel gevestigd in de EU'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentsSeaVesselSection'
    , xbrlName: 'ReinvestmentsSeaVesselSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGain'
    , xbrlName: 'RealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Omschrijving in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DescriptionReinvestment'
    , xbrlName: 'DescriptionReinvestment'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateReinvestment'
    , xbrlName: 'DateReinvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bedrag in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentValue'
    , xbrlName: 'ReinvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275K - Gespreide belasting van meerwaarden op bepaalde effecten'

    , code: '275K'
    , oldCode: ''
    , xbrlId: 'tax-inc_SpreadTaxableCapitalGainsSpecificSecuritiesForm'
    , xbrlName: 'SpreadTaxableCapitalGainsSpecificSecuritiesForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Herbeleggingen in bepaalde effecten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentsSpecificSecuritiesSection'
    , xbrlName: 'ReinvestmentsSpecificSecuritiesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
    , xbrlName: 'NonMonetaryFractionRealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Omschrijving in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DescriptionReinvestment'
    , xbrlName: 'DescriptionReinvestment'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateReinvestment'
    , xbrlName: 'DateReinvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bedrag in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentValue'
    , xbrlName: 'ReinvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276K - Gespreid te belasten meerwaarden op materiële en immateriële vaste activa'

    , code: '276K'
    , oldCode: ''
    , xbrlId: 'tax-inc_SpreadTaxableCapitalGainsTangibleIntangibleFixedAssetsForm'
    , xbrlName: 'SpreadTaxableCapitalGainsTangibleIntangibleFixedAssetsForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Herbeleggingen in afschrijfbare materiële of immateriële vaste activa'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentsTangibleIntangibleFixedAssetsSection'
    , xbrlName: 'ReinvestmentsTangibleIntangibleFixedAssetsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
    , xbrlName: 'NonMonetaryFractionRealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Omschrijving in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DescriptionReinvestment'
    , xbrlName: 'DescriptionReinvestment'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateReinvestment'
    , xbrlName: 'DateReinvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bedrag in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentValue'
    , xbrlName: 'ReinvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276N - Vrijstelling van meerwaarden op bedrijfsvoertuigen'

    , code: '276N'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionCapitalGainsCorporateVehiclesForm'
    , xbrlName: 'ExemptionCapitalGainsCorporateVehiclesForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Herbeleggingen in ecologische bedrijfsvoertuigen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentsEcologicalCorporateVehiclesSection'
    , xbrlName: 'ReinvestmentsEcologicalCorporateVehiclesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGain'
    , xbrlName: 'RealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Omschrijving in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DescriptionReinvestment'
    , xbrlName: 'DescriptionReinvestment'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateReinvestment'
    , xbrlName: 'DateReinvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bedrag in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentValue'
    , xbrlName: 'ReinvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276P - Vrijstelling van meerwaarden op binnenschepen voor commerciële vaart'

    , code: '276P'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionCapitalGainsRiverVesselsCommercialTradeForm'
    , xbrlName: 'ExemptionCapitalGainsRiverVesselsCommercialTradeForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Herbeleggingen in ecologische binnenschepen voor de commerciële vaart'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentsEcologicalRiverVesselCommercialTradeSection'
    , xbrlName: 'ReinvestmentsEcologicalRiverVesselCommercialTradeSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedCapitalGain'
    , xbrlName: 'RealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Omschrijving in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DescriptionReinvestment'
    , xbrlName: 'DescriptionReinvestment'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateReinvestment'
    , xbrlName: 'DateReinvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bedrag in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentValue'
    , xbrlName: 'ReinvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275K - Gespreide belasting van meerwaarden op bepaalde effecten'

    , code: '275K'
    , oldCode: ''
    , xbrlId: 'tax-inc_SpreadTaxableCapitalGainsSpecificSecuritiesForm'
    , xbrlName: 'SpreadTaxableCapitalGainsSpecificSecuritiesForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Voor het belastbare tijdperk belastbare deel van de meerwaarde op bepaalde effecten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxablePortionCapitalGainsSpecificSecuritiesSection'
    , xbrlName: 'TaxablePortionCapitalGainsSpecificSecuritiesSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
    , xbrlName: 'NonMonetaryFractionRealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bedrag in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentValue'
    , xbrlName: 'ReinvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Te belasten deel van de meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxableCapitalGain'
    , xbrlName: 'TaxableCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-stcg_SpreadTaxationCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276K - Gespreid te belasten meerwaarden op materiële en immateriële vaste activa'

    , code: '276K'
    , oldCode: ''
    , xbrlId: 'tax-inc_SpreadTaxableCapitalGainsTangibleIntangibleFixedAssetsForm'
    , xbrlName: 'SpreadTaxableCapitalGainsTangibleIntangibleFixedAssetsForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Voor het belastbare tijdperk belastbare deel van de meerwaarde op materiële en immateriële vaste activa'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxablePortionCapitalGainsTangibleIntangibleFixedAssetsSection'
    , xbrlName: 'TaxablePortionCapitalGainsTangibleIntangibleFixedAssetsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Niet-monetair gedeelte verwezenlijkte meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NonMonetaryFractionRealisedCapitalGain'
    , xbrlName: 'NonMonetaryFractionRealisedCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Verkoopwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_RealisedSellingValue'
    , xbrlName: 'RealisedSellingValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Ontvangen vergoeding'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReceivedCompensation'
    , xbrlName: 'ReceivedCompensation'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Bedrag in aanmerking te nemen herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ReinvestmentValue'
    , xbrlName: 'ReinvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-ricg_ReinvestmentCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'In aanmerking te nemen afschrijving herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DepreciationsReinvestment'
    , xbrlName: 'DepreciationsReinvestment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-stcg_SpreadTaxationCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Datum van vervreemding herbelegging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRealisationReinvestment'
    , xbrlName: 'DateRealisationReinvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-stcg_SpreadTaxationCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Te belasten deel van de meerwaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxableCapitalGain'
    , xbrlName: 'TaxableCapitalGain'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-stcg_SpreadTaxationCapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275C - Aftrek voor risicokapitaal'

    , code: '275C'
    , oldCode: ''
    , xbrlId: 'tax-inc_AllowanceCorporateEquityForm'
    , xbrlName: 'AllowanceCorporateEquityForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Berekening aftrek voor risicokapitaal'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CalculationAllowanceCorporateEquitySection'
    , xbrlName: 'CalculationAllowanceCorporateEquitySection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Eigen vermogen'

    , code: '8001'
    , oldCode: ''
    , xbrlId: 'tax-inc_Equity'
    , xbrlName: 'Equity'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-pane', title: 'Bestanddelen af te trekken van het eigen vermogen'

    , code: '8030'
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductionsEquityAllowanceCorporateEquity'
    , xbrlName: 'DeductionsEquityAllowanceCorporateEquity'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, hasInstantPeriods: true
    , items: [

    { xtype: 'biztax-field', label: 'Eigen aandelen'

    , code: '8011'
    , oldCode: ''
    , xbrlId: 'tax-inc_OwnSharesFiscalValue'
    , xbrlName: 'OwnSharesFiscalValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Financiële vaste activa die uit deelnemingen en andere aandelen bestaan'

    , code: '8012'
    , oldCode: ''
    , xbrlId: 'tax-inc_FinancialFixedAssetsParticipationsOtherShares'
    , xbrlName: 'FinancialFixedAssetsParticipationsOtherShares'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Aandelen van beleggingsvennootschappen'

    , code: '8013'
    , oldCode: ''
    , xbrlId: 'tax-inc_SharesInvestmentCorporations'
    , xbrlName: 'SharesInvestmentCorporations'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Inrichtingen gelegen in een land met verdrag'

    , code: '8014'
    , oldCode: ''
    , xbrlId: 'tax-inc_BranchesCountryTaxTreaty'
    , xbrlName: 'BranchesCountryTaxTreaty'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Onroerende goederen gelegen in een land met verdrag'

    , code: '8015'
    , oldCode: ''
    , xbrlId: 'tax-inc_ImmovablePropertyCountryTaxTreaty'
    , xbrlName: 'ImmovablePropertyCountryTaxTreaty'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Materiële vaste activa in zover de erop betrekking hebbende kosten onredelijk zijn'

    , code: '8016'
    , oldCode: ''
    , xbrlId: 'tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts'
    , xbrlName: 'TangibleFixedAssetsUnreasonableRelatedCosts'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Bestanddelen die als belegging worden gehouden en geen belastbaar periodiek inkomen voortbrengen'

    , code: '8017'
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentsNoPeriodicalIncome'
    , xbrlName: 'InvestmentsNoPeriodicalIncome'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Onroerende goederen waarvan bedrijfsleiders het gebruik hebben'

    , code: '8018'
    , oldCode: ''
    , xbrlId: 'tax-inc_ImmovablePropertyUseManager'
    , xbrlName: 'ImmovablePropertyUseManager'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitgedrukte maar niet-verwezenlijkte meerwaarden'

    , code: '8019'
    , oldCode: ''
    , xbrlId: 'tax-inc_UnrealisedExpressedCapitalGains'
    , xbrlName: 'UnrealisedExpressedCapitalGains'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Belastingkrediet voor onderzoek en ontwikkeling'

    , code: '8020'
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'
    , xbrlName: 'TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Kapitaalsubsidies'

    , code: '8021'
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentGrants'
    , xbrlName: 'InvestmentGrants'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Voorraadactualisering erkende diamanthandelaars'

    , code: '8022'
    , oldCode: ''
    , xbrlId: 'tax-inc_ActualisationStockRecognisedDiamondTraders'
    , xbrlName: 'ActualisationStockRecognisedDiamondTraders'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }
  ,
    { xtype: 'biztax-field', label: 'Ten name van de hoofdzetel ontleende middelen met betrekking tot dewelke de interesten ten laste van het belastbaar resultaat van de Belgische inrichting wordt gelegd'

    , code: '8023'
    , oldCode: ''
    , xbrlId: 'tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'
    , xbrlName: 'BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden'

    , code: '8040'
    , oldCode: ''
    , xbrlId: 'tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'
    , xbrlName: 'MovementEquityAfterDeductionsAllowanceCorporateEquity'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: 't-ace_AllowanceCorporateEquityMovementDetailPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Risicokapitaal van het belastbaar tijdperk'

    , code: '8050'
    , oldCode: ''
    , xbrlId: 'tax-inc_AllowanceCorporateEquityCurrentTaxPeriod'
    , xbrlName: 'AllowanceCorporateEquityCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Aftrek risicokapitaal die voor het aanslagjaar in principe aftrekbaar is'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYearTitle'
    , xbrlName: 'DeductibleAllowanceCorporateEquityCurrentAssessmentYearTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Indien het belastbare tijdperk niet gelijk is aan 12 maanden of voor het eerste belastbare tijdperk moet het tarief worden vermenigvuldigd met een breuk waarvan de teller gelijk is aan het totaal aantal dagen van het belastbare tijdperk en de noemer gelijk is aan 365.'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ApplicableRateAllowanceCorporateEquity'
    , xbrlName: 'ApplicableRateAllowanceCorporateEquity'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'In principe aftrekbaar'

    , code: '8051'
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear'
    , xbrlName: 'DeductibleAllowanceCorporateEquityCurrentAssessmentYear'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken'

    , code: '8052'
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear'
    , xbrlName: 'DeductionAllowanceCorporateEquityCurrentAssessmentYear'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Saldo van de aftrek voor risicokapitaal van het huidig aanslagjaar dat overdraagbaar is naar latere aanslagjaren'

    , code: '8053'
    , oldCode: ''
    , xbrlId: 'tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'
    , xbrlName: 'CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Saldo van de aftrek voor risicokapitaal dat werd gevormd tijdens vorige aanslagjaren'

    , code: '8060'
    , oldCode: ''
    , xbrlId: 'tax-inc_AllowanceCorporateEquityPreviousAssessmentYears'
    , xbrlName: 'AllowanceCorporateEquityPreviousAssessmentYears'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aftrek voor risicokapitaal van vorige aanslagjaren dat werkelijk wordt afgetrokken'

    , code: '8061'
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears'
    , xbrlName: 'DeductionAllowanceCorporateEquityPreviousAssessmentYears'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken'

    , code: '1435'
    , oldCode: '104 103'
    , xbrlId: 'tax-inc_AllowanceCorporateEquity'
    , xbrlName: 'AllowanceCorporateEquity'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 'tax-inc-rcorp_ProfitOriginPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar het volgende belastbare tijdperk'

    , code: '1712'
    , oldCode: '332'
    , xbrlId: 'tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'
    , xbrlName: 'CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Uitleg wijzigingen tijdens het belastbare tijdperk van het eigen vermogen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DetailMovementEquityAfterDeductionsAllowanceCorporateEquitySection'
    , xbrlName: 'DetailMovementEquityAfterDeductionsAllowanceCorporateEquitySection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Elke beweging moet worden vermenigvuldigd met het aantal maanden die nog blijven lopen tot op het einde van het belastbare tijdperk en gedeeld door het totale aantal maanden van het belastbare tijdperk en waarbij de wijzigingen geacht worden te hebben plaatsgevonden de eerste dag van de kalendermaand volgend op die waarin ze zich hebben voorgedaan. Indien het belastbaar tijdperk geen geheel aantal maanden bevat moet de voormelde breuk (teller en noemer) drie cijfers na de komma bevatten'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ApplicableDurationMovementEquityAllowanceCorporateEquity'
    , xbrlName: 'ApplicableDurationMovementEquityAllowanceCorporateEquity'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Berekening als gewogen gemiddelde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CalculationWeightedAverageMovementEquityAfterDeductionsAllowanceCorporateEquity'
    , xbrlName: 'CalculationWeightedAverageMovementEquityAfterDeductionsAllowanceCorporateEquity'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-ace_AllowanceCorporateEquityMovementDetailPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden'

    , code: '8040'
    , oldCode: ''
    , xbrlId: 'tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity'
    , xbrlName: 'MovementEquityAfterDeductionsAllowanceCorporateEquity'
    , itemType: 'pfs-dt:monetary14D2ItemType'
    , concept: 't-ace_AllowanceCorporateEquityMovementDetailPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }


    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275F - Aangifte van gedane betalingen naar staten die niet effectief en substantieel de OESO-standaard op het gebied van uitwisseling van inlichtingen toepassen of naar staten zonder of met een lage belasting'

    , code: '275F'
    , oldCode: ''
    , xbrlId: 'tax-inc_PaymentsCertainStatesForm'
    , xbrlName: 'PaymentsCertainStatesForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Staat'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_StateName'
    , xbrlName: 'StateName'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-pcs_PaymentsCertainStatesPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Verkrijger van de betaling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_BeneficiaryPaymentCertainState'
    , xbrlName: 'BeneficiaryPaymentCertainState'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-pcs_PaymentsCertainStatesPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Betaling uitgedrukt in EUR'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PaymentCertainState'
    , xbrlName: 'PaymentCertainState'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-pcs_PaymentsCertainStatesPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275P - Aftrek voor octrooi-inkomsten'

    , code: '275P'
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductionPatentsIncomeForm'
    , xbrlName: 'DeductionPatentsIncomeForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Inkomsten of het gedeelte van inkomsten uit octrooien die geheel of gedeeltelijk door de vennootschap werden ontwikkeld en waarvan zij octrooihouder is'

    , code: '8100'
    , oldCode: ''
    , xbrlId: 'tax-inc_IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped'
    , xbrlName: 'IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Gecorrigeerde aftrekbare inkomsten of het gedeelte van inkomsten uit octrooien die de vennootschap heeft verworven van derden'

    , code: '8120'
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty'
    , xbrlName: 'CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Inkomsten of het gedeelte van inkomsten uit octrooien die de vennootschap heeft verworven van derden'

    , code: '8111'
    , oldCode: ''
    , xbrlId: 'tax-inc_IncomePatentsWhollyPartiallyObtainedThirdParty'
    , xbrlName: 'IncomePatentsWhollyPartiallyObtainedThirdParty'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vergoedingen of het gedeelte van vergoedingen die aan derden zijn verschuldigd voor de octrooien die de vennootschap heeft verworven'

    , code: '8112'
    , oldCode: ''
    , xbrlId: 'tax-inc_CompensationOwedThirdPartiesPertainingPatents'
    , xbrlName: 'CompensationOwedThirdPartiesPertainingPatents'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Afschrijvingen of het gedeelte van de afschrijvingen die zijn toegepast op de aanschaffings- of beleggingswaarde van de door de vennootschap verworven octrooien'

    , code: '8113'
    , oldCode: ''
    , xbrlId: 'tax-inc_AmortisationAcquisitionInvestmentValuePatents'
    , xbrlName: 'AmortisationAcquisitionInvestmentValuePatents'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Berekeningsgrondslag van de aftrek voor octrooi-inkomsten'

    , code: '8130'
    , oldCode: ''
    , xbrlId: 'tax-inc_CalculationBasisDeductionPatentsIncome'
    , xbrlName: 'CalculationBasisDeductionPatentsIncome'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Aftrek voor octrooi-inkomsten'

    , code: '8140'
    , oldCode: ''
    , xbrlId: 'tax-inc_DeductibleDeductionPatentsIncome'
    , xbrlName: 'DeductibleDeductionPatentsIncome'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275R - Investeringsreserve'

    , code: '275R'
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentReserveForm'
    , xbrlName: 'InvestmentReserveForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Vrijgestelde investeringsreserve'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptInvestmentReserveSection'
    , xbrlName: 'ExemptInvestmentReserveSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve'

    , code: '8201'
    , oldCode: ''
    , xbrlId: 'tax-inc_IncreaseTaxableReservesInvestmentReserveExcluded'
    , xbrlName: 'IncreaseTaxableReservesInvestmentReserveExcluded'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Verminderingen op deze aangroei'

    , code: '8220'
    , oldCode: ''
    , xbrlId: 'tax-inc_ReductionsIncreaseTaxableReservesInvestmentReserveExcluded'
    , xbrlName: 'ReductionsIncreaseTaxableReservesInvestmentReserveExcluded'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Meerwaarden op aandelen'

    , code: '8211'
    , oldCode: '006 (1)'
    , xbrlId: 'tax-inc_CapitalGainsSharesInvestmentReserve'
    , xbrlName: 'CapitalGainsSharesInvestmentReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'instant-start'
    }
  ,
    { xtype: 'biztax-field', label: 'Gedeelte van de meerwaarden op voertuigen dat niet als winst wordt aangemerkt'

    , code: '8212'
    , oldCode: ''
    , xbrlId: 'tax-inc_CapitalGainsVehiclesNoProfit'
    , xbrlName: 'CapitalGainsVehiclesNoProfit'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vermindering van het gestort kapitaal'

    , code: '8213'
    , oldCode: ''
    , xbrlId: 'tax-inc_DecreasePaidUpCapital'
    , xbrlName: 'DecreasePaidUpCapital'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vermeerdering van bepaalde vorderingen van de vennootschap'

    , code: '8214'
    , oldCode: ''
    , xbrlId: 'tax-inc_IncreaseParticularDebtClaims'
    , xbrlName: 'IncreaseParticularDebtClaims'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum'

    , code: '8230'
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectedIncreaseTaxableReservesInvestmentReserveExcluded'
    , xbrlName: 'CorrectedIncreaseTaxableReservesInvestmentReserveExcluded'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aangroei van de belaste reserves ten opzichte van de belaste reserves op het einde van het vorig belastbaar tijdperk waarvoor een investeringsreserve werd genoten'

    , code: '8231'
    , oldCode: ''
    , xbrlId: 'tax-inc_IncreaseTaxableReservesInvestmentReserveIncluded'
    , xbrlName: 'IncreaseTaxableReservesInvestmentReserveIncluded'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Maximaal vrijgestelde reserve van het belastbare tijdperk'

    , code: '8240'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptibleInvestmentReserve'
    , xbrlName: 'ExemptibleInvestmentReserve'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Investeringsreserve'

    , code: '8250'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptInvestmentReserveCurrentTaxPeriod'
    , xbrlName: 'ExemptInvestmentReserveCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Investeringen in afschrijfbare materiële of immateriële vaste activa'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentsDepreciableTangibleIntangibleFixedAssetsSection'
    , xbrlName: 'InvestmentsDepreciableTangibleIntangibleFixedAssetsSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Vrijgestelde reserve van de laatste drie belastbare tijdperken, met inbegrip van het huidige belastbare tijdperk'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptInvestmentReserveLastThreeTaxPeriodsTitle'
    , xbrlName: 'ExemptInvestmentReserveLastThreeTaxPeriodsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Vrijgestelde investeringsreserve'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptInvestmentReserveOverview'
    , xbrlName: 'ExemptInvestmentReserveOverview'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-itifa_ExemptInvestmentReserveTotalPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Einddatum van de investering'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FinalDateInvestment'
    , xbrlName: 'FinalDateInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-itifa_ExemptInvestmentReserveTotalPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Investeringen in materiële en immateriële vaste activa'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentsTangibleIntangibleFixedAssetsTitle'
    , xbrlName: 'InvestmentsTangibleIntangibleFixedAssetsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Investeringen vermeld op de vorige opgave'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentsPreviousInvestmentReserveTaxPeriod'
    , xbrlName: 'InvestmentsPreviousInvestmentReserveTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-itifa_ExemptInvestmentReserveTotalPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Investeringen van het huidig belastbaar tijdperk'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentCurrentTaxPeriod'
    , xbrlName: 'InvestmentCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-itifa_ExemptInvestmentReservePrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Nog te investeren saldo'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestBalance'
    , xbrlName: 'InvestBalance'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-itifa_ExemptInvestmentReserveTotalPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }


    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275U - Investeringsaftrek'

    , code: '275U'
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionForm'
    , xbrlName: 'InvestmentDeductionForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentSection'
    , xbrlName: 'InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Eenmalige investeringsaftrek'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoTitle'
    , xbrlName: 'InvestmentDeductionOneGoTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Investeringen in nieuwe vaste activa door alle vennootschappen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoNewFixedAssetsAllCompaniesTitle'
    , xbrlName: 'InvestmentDeductionOneGoNewFixedAssetsAllCompaniesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Eenmalig'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveOneGo'
    , xbrlName: 'InvestmentIncentiveOneGo'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Investeringen door KMO-s'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoSMEsTitle'
    , xbrlName: 'InvestmentDeductionOneGoSMEsTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Eenmalig'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveOneGo'
    , xbrlName: 'InvestmentIncentiveOneGo'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Investeringen in zeeschepen door vennootschappen die uitsluitend winst uit zeescheepvaart verkrijgen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoInvestmentsSeaVesselsShippingCompaniesTitle'
    , xbrlName: 'InvestmentDeductionOneGoInvestmentsSeaVesselsShippingCompaniesTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Eenmalig'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveOneGo'
    , xbrlName: 'InvestmentIncentiveOneGo'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Gespreide investeringsaftrek'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionSpreadTitle'
    , xbrlName: 'InvestmentDeductionSpreadTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde, afschrijfbaar'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValueSpread'
    , xbrlName: 'AcquisitionInvestmentValueSpread'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aanneembare afschrijvingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EligibleDepreciations'
    , xbrlName: 'EligibleDepreciations'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gespreid'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveSpread'
    , xbrlName: 'InvestmentIncentiveSpread'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Eenmalige en gespreide investeringsaftrek'

    , code: '8310'
    , oldCode: '884'
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoSpreadCurrentTaxPeriod'
    , xbrlName: 'InvestmentDeductionOneGoSpreadCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gespreide aftrekken voor investeringen van vorige belastbare tijdperken'

    , code: '8311'
    , oldCode: '885'
    , xbrlId: 'tax-inc_InvestmentDeductionSpreadPreviousTaxPeriods'
    , xbrlName: 'InvestmentDeductionSpreadPreviousTaxPeriods'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Gecumuleerde vroegere nog niet afgetrokken investeringsaftrekken'

    , code: '8330'
    , oldCode: '886'
    , xbrlId: 'tax-inc_PreviousCarryOverInvestmentDeduction'
    , xbrlName: 'PreviousCarryOverInvestmentDeduction'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen'

    , code: '8321'
    , oldCode: ''
    , xbrlId: 'tax-inc_PreviousCarryOverInvestmentDeductionSeaVessels'
    , xbrlName: 'PreviousCarryOverInvestmentDeductionSeaVessels'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen'

    , code: '8322'
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherPreviousCarryOverInvestmentDeduction'
    , xbrlName: 'OtherPreviousCarryOverInvestmentDeduction'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'In beginsel aftrekbare investeringsaftrek'

    , code: '8340'
    , oldCode: '887'
    , xbrlId: 'tax-inc_BasicTaxableInvestmentDeduction'
    , xbrlName: 'BasicTaxableInvestmentDeduction'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk'

    , code: '1437.1'
    , oldCode: '888'
    , xbrlId: 'tax-inc_DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Over te dragen naar volgende belastbare tijdperken'

    , code: '8370'
    , oldCode: '889'
    , xbrlId: 'tax-inc_CarryOverInvestmentDeduction'
    , xbrlName: 'CarryOverInvestmentDeduction'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Over te dragen investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen'

    , code: '8361'
    , oldCode: ''
    , xbrlId: 'tax-inc_CarryOverInvestmentDeductionSeaVessels'
    , xbrlName: 'CarryOverInvestmentDeductionSeaVessels'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Over te dragen investeringsaftrekken met betrekking tot andere investeringen'

    , code: '8362'
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherCarryOverInvestmentDeduction'
    , xbrlName: 'OtherCarryOverInvestmentDeduction'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionCompatibleTaxCreditResearchDevelopmentSection'
    , xbrlName: 'InvestmentDeductionCompatibleTaxCreditResearchDevelopmentSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Eenmalige investeringsaftrek'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoCompatibleTaxCreditResearchDevelopmentTitle'
    , xbrlName: 'InvestmentDeductionOneGoCompatibleTaxCreditResearchDevelopmentTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Investeringen in nieuwe vaste activa door alle vennootschappen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoNewFixedAssetsAllCompaniesCompatibleTaxCreditResearchDevelopmentTitle'
    , xbrlName: 'InvestmentDeductionOneGoNewFixedAssetsAllCompaniesCompatibleTaxCreditResearchDevelopmentTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Eenmalig'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveOneGo'
    , xbrlName: 'InvestmentIncentiveOneGo'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Investeringen door KMO-s'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoSMEsCompatibleTaxCreditResearchDevelopmentTitle'
    , xbrlName: 'InvestmentDeductionOneGoSMEsCompatibleTaxCreditResearchDevelopmentTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Eenmalig'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveOneGo'
    , xbrlName: 'InvestmentIncentiveOneGo'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Investeringen in zeeschepen door vennootschappen die uitsluitend winst uit zeescheepvaart verkrijgen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoInvestmentsSeaVesselsShippingCompaniesCompatibleTaxCreditResearchDevelopmentTitle'
    , xbrlName: 'InvestmentDeductionOneGoInvestmentsSeaVesselsShippingCompaniesCompatibleTaxCreditResearchDevelopmentTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Eenmalig'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveOneGo'
    , xbrlName: 'InvestmentIncentiveOneGo'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Eenmalige investeringsaftrek'

    , code: '8410'
    , oldCode: '879'
    , xbrlId: 'tax-inc_InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gespreide investeringsaftrek van vorige belastbare tijdperken voor andere investeringen dan investeringen in onderzoek en ontwikkeling'

    , code: '8411'
    , oldCode: '895'
    , xbrlId: 'tax-inc_PreviousCarryOverSpreadInvestmentDeductionNoResearchDevelopmentCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'PreviousCarryOverSpreadInvestmentDeductionNoResearchDevelopmentCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Gecumuleerde vroegere nog niet afgetrokken investeringsaftrekken'

    , code: '8430'
    , oldCode: '896'
    , xbrlId: 'tax-inc_PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen'

    , code: '8421'
    , oldCode: ''
    , xbrlId: 'tax-inc_PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen'

    , code: '8422'
    , oldCode: ''
    , xbrlId: 'tax-inc_OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Investeringsaftrekken die niet konden worden afgetrokken voor de drie voorafgaande aanslagjaren met betrekking tot de eenmalige investeringsaftrek voor octrooien en de eenmalige en de gespreide investeringsaftrekken voor investeringen in onderzoek en ontwikkeling'

    , code: '8423'
    , oldCode: ''
    , xbrlId: 'tax-inc_PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'In beginsel aftrekbare investeringsaftrek'

    , code: '8440'
    , oldCode: '897'
    , xbrlId: 'tax-inc_BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk'

    , code: '1437.2'
    , oldCode: '898'
    , xbrlId: 'tax-inc_DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Over te dragen naar volgende belastbare tijdperken'

    , code: '8470'
    , oldCode: '899'
    , xbrlId: 'tax-inc_CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Over te dragen investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen'

    , code: '8461'
    , oldCode: ''
    , xbrlId: 'tax-inc_CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Over te dragen investeringsaftrekken met betrekking tot andere investeringen'

    , code: '8462'
    , oldCode: ''
    , xbrlId: 'tax-inc_CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment'
    , xbrlName: 'CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '275W - Belastingkrediet voor onderzoek en ontwikkeling'

    , code: '275W'
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopmentForm'
    , xbrlName: 'TaxCreditResearchDevelopmentForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Eenmalig belastingkrediet voor onderzoek en ontwikkeling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopmentOneGoTitle'
    , xbrlName: 'TaxCreditResearchDevelopmentOneGoTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Eenmalig'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveOneGo'
    , xbrlName: 'InvestmentIncentiveOneGo'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Gespreid belastingkrediet voor onderzoek en ontwikkeling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopmentSpreadTitle'
    , xbrlName: 'TaxCreditResearchDevelopmentSpreadTitle'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Aanneembare afschrijvingen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EligibleDepreciations'
    , xbrlName: 'EligibleDepreciations'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gespreid'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_InvestmentIncentiveSpread'
    , xbrlName: 'InvestmentIncentiveSpread'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-idtc_InvestmentDeductionTaxCreditPrimaryConcepts'
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Gespreid belastingkrediet voor onderzoek en ontwikkeling voor investeringen van vorige belastbare tijdperken'

    , code: '8501'
    , oldCode: ''
    , xbrlId: 'tax-inc_SpreadTaxCreditResearchDevelopmentPreviousTaxPeriods'
    , xbrlName: 'SpreadTaxCreditResearchDevelopmentPreviousTaxPeriods'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Aanvullend belastingkrediet voor onderzoek en ontwikkeling'

    , code: '8502'
    , oldCode: ''
    , xbrlId: 'tax-inc_AdditionalTaxCreditResearchDevelopment'
    , xbrlName: 'AdditionalTaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling van het belastbaar tijdperk'

    , code: '8510'
    , oldCode: ''
    , xbrlId: 'tax-inc_ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod'
    , xbrlName: 'ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Belastingkrediet voor onderzoek en ontwikkeling van vorige belastbare tijdperken'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopmentPreviousTaxPeriodsTitle'
    , xbrlName: 'TaxCreditResearchDevelopmentPreviousTaxPeriodsTitle'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Verrekenbaar'

    , code: '8521'
    , oldCode: ''
    , xbrlId: 'tax-inc_ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods'
    , xbrlName: 'ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Belastingkrediet voor onderzoek en ontwikkeling dat voor het huidig belastbare tijdperk terugbetaalbaar is'

    , code: '1850'
    , oldCode: '198'
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
    , xbrlName: 'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Verrekenbaar en niet-terugbetaalbaar belastingkrediet voor onderzoek en ontwikkeling van vorige belastbare tijdperken'

    , code: '8530'
    , oldCode: ''
    , xbrlId: 'tax-inc_ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods'
    , xbrlName: 'ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling'

    , code: '1833'
    , oldCode: '184'
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopment'
    , xbrlName: 'TaxCreditResearchDevelopment'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Belastingkrediet voor onderzoek en ontwikkeling dat voor het huidig belastbare tijdperk terugbetaalbaar is'

    , code: '1850'
    , oldCode: '198'
    , xbrlId: 'tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
    , xbrlName: 'TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , items: [

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276T - Vrijstelling bijkomend personeel'

    , code: '276T'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnelForm'
    , xbrlName: 'ExemptionAdditionalPersonnelForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Gepresteerde arbeidsdagen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CalculationPerformedWorkingDaysSection'
    , xbrlName: 'CalculationPerformedWorkingDaysSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Gepresteerde dagen kalenderjaar-1'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CalculationPerformedWorkingDaysCalendarYear-1Title'
    , xbrlName: 'CalculationPerformedWorkingDaysCalendarYear-1Title'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Arbeidsregeling, aantal dagen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_LabourRegulationNumberDays'
    , xbrlName: 'LabourRegulationNumberDays'
    , itemType: 'pfs-dt:positiveIntegerMin1Max6ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Arbeidsregeling, totaal aantal dagen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_LabourRegulationNumberDaysYear'
    , xbrlName: 'LabourRegulationNumberDaysYear'
    , itemType: 'pfs-dt:nonNegativeInteger6ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Brutoloon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_GrossIncome'
    , xbrlName: 'GrossIncome'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Arbeidsdagen, alle personeelsleden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WorkingDaysAllPersonnel'
    , xbrlName: 'WorkingDaysAllPersonnel'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Bezoldigde dagen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WorkingDaysPaid'
    , xbrlName: 'WorkingDaysPaid'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vakantiedagen arbeiders'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_HolidaysWorkers'
    , xbrlName: 'HolidaysWorkers'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Correcties'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectionsWorkingDays'
    , xbrlName: 'CorrectionsWorkingDays'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Bezoldigde uren, per kwartaal'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PaidHoursQuarter'
    , xbrlName: 'PaidHoursQuarter'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gemiddeld dagloon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AverageDayPay'
    , xbrlName: 'AverageDayPay'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gemiddeld uurloon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AverageHourPay'
    , xbrlName: 'AverageHourPay'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Arbeidsdagen, personeelsleden met een laag loon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WorkingDaysLowWages'
    , xbrlName: 'WorkingDaysLowWages'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Gepresteerde dagen kalenderjaar-2'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CalculationPerformedWorkingDaysCalendarYear-2Title'
    , xbrlName: 'CalculationPerformedWorkingDaysCalendarYear-2Title'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Arbeidsregeling, aantal dagen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_LabourRegulationNumberDays'
    , xbrlName: 'LabourRegulationNumberDays'
    , itemType: 'pfs-dt:positiveIntegerMin1Max6ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Arbeidsregeling, totaal aantal dagen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_LabourRegulationNumberDaysYear'
    , xbrlName: 'LabourRegulationNumberDaysYear'
    , itemType: 'pfs-dt:nonNegativeInteger6ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Brutoloon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_GrossIncome'
    , xbrlName: 'GrossIncome'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-pane', title: 'Arbeidsdagen, alle personeelsleden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WorkingDaysAllPersonnel'
    , xbrlName: 'WorkingDaysAllPersonnel'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , items: [

    { xtype: 'biztax-field', label: 'Bezoldigde dagen'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WorkingDaysPaid'
    , xbrlName: 'WorkingDaysPaid'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vakantiedagen arbeiders'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_HolidaysWorkers'
    , xbrlName: 'HolidaysWorkers'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Correcties'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectionsWorkingDays'
    , xbrlName: 'CorrectionsWorkingDays'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-field', label: 'Bezoldigde uren, per kwartaal'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_PaidHoursQuarter'
    , xbrlName: 'PaidHoursQuarter'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gemiddeld dagloon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AverageDayPay'
    , xbrlName: 'AverageDayPay'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Gemiddeld uurloon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AverageHourPay'
    , xbrlName: 'AverageHourPay'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Arbeidsdagen, personeelsleden met een laag loon'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_WorkingDaysLowWages'
    , xbrlName: 'WorkingDaysLowWages'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Vrijstelling voor bijkomend personeel'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CalculationExemptionAdditionalPersonnelSection'
    , xbrlName: 'CalculationExemptionAdditionalPersonnelSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Beweging alle personeelsleden, kalenderjaar-1'

    , code: '8601'
    , oldCode: ''
    , xbrlId: 'tax-inc_MovementAllPersonnelCalendarYear-1'
    , xbrlName: 'MovementAllPersonnelCalendarYear-1'
    , itemType: 'pfs-dt:integer6ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aangroei van de personeelsleden met een laag loon, kalenderjaar-1'

    , code: '8602'
    , oldCode: ''
    , xbrlId: 'tax-inc_IncreasePersonnelLowWageCalendarYear-1'
    , xbrlName: 'IncreasePersonnelLowWageCalendarYear-1'
    , itemType: 'pfs-dt:nonNegativeInteger6ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Aantal personeelsleden waarvoor de vrijstelling kan bekomen worden'

    , code: '8603'
    , oldCode: ''
    , xbrlId: 'tax-inc_EmployeesEligibleExemptionAdditionalPersonnel'
    , xbrlName: 'EmployeesEligibleExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeDecimal6D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling'

    , code: '8604'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnel'
    , xbrlName: 'ExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Terug te nemen vrijstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CalculationReversalPreviousExemptionAdditionalPersonnelSection'
    , xbrlName: 'CalculationReversalPreviousExemptionAdditionalPersonnelSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Beweging alle personeelsleden, kalenderjaar-1'

    , code: '8601'
    , oldCode: ''
    , xbrlId: 'tax-inc_MovementAllPersonnelCalendarYear-1'
    , xbrlName: 'MovementAllPersonnelCalendarYear-1'
    , itemType: 'pfs-dt:integer6ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'In principe terug te nemen vrijstelling'

    , code: '8612'
    , oldCode: ''
    , xbrlId: 'tax-inc_BasicTaxableReversalPreviousExemptionAdditionalPersonnel'
    , xbrlName: 'BasicTaxableReversalPreviousExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling die voor het vorige aanslagjaar werd verleend'

    , code: '8613'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnelPreviousAssessmentYear'
    , xbrlName: 'ExemptionAdditionalPersonnelPreviousAssessmentYear'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Terug te nemen vrijstelling'

    , code: '8614'
    , oldCode: ''
    , xbrlId: 'tax-inc_ReversalPreviousExemptionAdditionalPersonnel'
    , xbrlName: 'ReversalPreviousExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: ''
    , isAbstract: false, totalField: true
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Correcties'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectionsWorkingDaysPersonnelDetailSection'
    , xbrlName: 'CorrectionsWorkingDaysPersonnelDetailSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Correcties kalenderjaar-1'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectionsWorkingDaysPersonnelDetailCalendarYear-1Title'
    , xbrlName: 'CorrectionsWorkingDaysPersonnelDetailCalendarYear-1Title'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Equivalent van gepresteerde dagen met betrekking tot het bij deeltijdse arbeid ontstane verschil tussen de dagen die zijn vermeld in de kwartaal aangifte RSZ en de effectieve prestaties'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EquivalentWholeDayWorkingDaysPartTimePersonnelPerformedAccordingNSSOQuarterlyDeclaration'
    , xbrlName: 'EquivalentWholeDayWorkingDaysPartTimePersonnelPerformedAccordingNSSOQuarterlyDeclaration'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Wettelijke feestdagen begrepen in de bezoldigde dagen van de arbeiders'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_BankHolidaysIncludedDaysPaidWorkingDaysAllPersonnel'
    , xbrlName: 'BankHolidaysIncludedDaysPaidWorkingDaysAllPersonnel'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Wettelijke feestdagen begrepen in de correcties op de bezoldigde dagen en de vakantiedagen van de arbeiders'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_BankHolidaysIncludedCorrectionsWorkingDaysAllPersonnel'
    , xbrlName: 'BankHolidaysIncludedCorrectionsWorkingDaysAllPersonnel'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectionWorkingDaysPersonnelDetail'
    , xbrlName: 'CorrectionWorkingDaysPersonnelDetail'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Correcties kalenderjaar-2'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectionsWorkingDaysPersonnelDetailCalendarYear-2Title'
    , xbrlName: 'CorrectionsWorkingDaysPersonnelDetailCalendarYear-2Title'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Equivalent van gepresteerde dagen met betrekking tot het bij deeltijdse arbeid ontstane verschil tussen de dagen die zijn vermeld in de kwartaal aangifte RSZ en de effectieve prestaties'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EquivalentWholeDayWorkingDaysPartTimePersonnelPerformedAccordingNSSOQuarterlyDeclaration'
    , xbrlName: 'EquivalentWholeDayWorkingDaysPartTimePersonnelPerformedAccordingNSSOQuarterlyDeclaration'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Wettelijke feestdagen begrepen in de bezoldigde dagen van de arbeiders'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_BankHolidaysIncludedDaysPaidWorkingDaysAllPersonnel'
    , xbrlName: 'BankHolidaysIncludedDaysPaidWorkingDaysAllPersonnel'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Wettelijke feestdagen begrepen in de correcties op de bezoldigde dagen en de vakantiedagen van de arbeiders'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_BankHolidaysIncludedCorrectionsWorkingDaysAllPersonnel'
    , xbrlName: 'BankHolidaysIncludedCorrectionsWorkingDaysAllPersonnel'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Uitleg'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_CorrectionWorkingDaysPersonnelDetail'
    , xbrlName: 'CorrectionWorkingDaysPersonnelDetail'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eap_ExemptionAdditionalPersonnelPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276W1 - Vrijstelling voor bijkomend personeel tewerkgesteld voor wetenschappelijk onderzoek'

    , code: '276W1'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnelScientificResearchForm'
    , xbrlName: 'ExemptionAdditionalPersonnelScientificResearchForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Personeelsleden aangeworven tijdens een vorig belastbaar tijdperk en gedurende het volledige belastbare tijdperk voltijds tewerkgesteld'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EmployedFullTimeScientificResearchSection'
    , xbrlName: 'EmployedFullTimeScientificResearchSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Datum van aanwerving en tewerkstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
    , xbrlName: 'DateRecruitmentEmploymentAdditionalPersonnel'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FullExemptionAdditionalPersonnel'
    , xbrlName: 'FullExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Effectief vrijgesteld'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ActualExemptionAdditionalPersonnel'
    , xbrlName: 'ActualExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EmployedPartTimeScientificResearchSection'
    , xbrlName: 'EmployedPartTimeScientificResearchSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Datum van aanwerving en tewerkstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
    , xbrlName: 'DateRecruitmentEmploymentAdditionalPersonnel'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FullExemptionAdditionalPersonnel'
    , xbrlName: 'FullExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Effectief vrijgesteld'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ActualExemptionAdditionalPersonnel'
    , xbrlName: 'ActualExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276W2 - Vrijstelling voor bijkomend personeel tewerkgesteld voor de uitbouw van het technologisch potentieel van de onderneming'

    , code: '276W2'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnelDevelopmentTechnologicalPotentialForm'
    , xbrlName: 'ExemptionAdditionalPersonnelDevelopmentTechnologicalPotentialForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Personeelsleden aangeworven tijdens een vorig belastbaar tijdperk en gedurende het volledige belastbare tijdperk voltijds tewerkgesteld'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EmployedFullTimeIncreaseTechnologicalPotentialSection'
    , xbrlName: 'EmployedFullTimeIncreaseTechnologicalPotentialSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Datum van aanwerving en tewerkstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
    , xbrlName: 'DateRecruitmentEmploymentAdditionalPersonnel'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FullExemptionAdditionalPersonnel'
    , xbrlName: 'FullExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Effectief vrijgesteld'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ActualExemptionAdditionalPersonnel'
    , xbrlName: 'ActualExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_EmployedPartTimeIncreaseTechnologicalPotentialSection'
    , xbrlName: 'EmployedPartTimeIncreaseTechnologicalPotentialSection'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-field', label: 'Datum van aanwerving en tewerkstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
    , xbrlName: 'DateRecruitmentEmploymentAdditionalPersonnel'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Vrijstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FullExemptionAdditionalPersonnel'
    , xbrlName: 'FullExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }
  ,
    { xtype: 'biztax-field', label: 'Effectief vrijgesteld'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ActualExemptionAdditionalPersonnel'
    , xbrlName: 'ActualExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , periodType: 'duration'
    }

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276W3 - Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer'

    , code: '276W3'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnelHeadExportDepartmentForm'
    , xbrlName: 'ExemptionAdditionalPersonnelHeadExportDepartmentForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Naam'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Identity'
    , xbrlName: 'Identity'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsDepartmentHeadPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Nationaal nummer'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NationalNumber'
    , xbrlName: 'NationalNumber'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsDepartmentHeadPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Datum van aanwerving en tewerkstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
    , xbrlName: 'DateRecruitmentEmploymentAdditionalPersonnel'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Functie'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Function'
    , xbrlName: 'Function'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsDepartmentHeadPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Vrijstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FullExemptionAdditionalPersonnel'
    , xbrlName: 'FullExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Effectief vrijgesteld'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ActualExemptionAdditionalPersonnel'
    , xbrlName: 'ActualExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '276W4 - Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg'

    , code: '276W4'
    , oldCode: ''
    , xbrlId: 'tax-inc_ExemptionAdditionalPersonnelHeadTQMDepartmentForm'
    , xbrlName: 'ExemptionAdditionalPersonnelHeadTQMDepartmentForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Naam'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Identity'
    , xbrlName: 'Identity'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsDepartmentHeadPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Nationaal nummer'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_NationalNumber'
    , xbrlName: 'NationalNumber'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsDepartmentHeadPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Datum van aanwerving en tewerkstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateRecruitmentEmploymentAdditionalPersonnel'
    , xbrlName: 'DateRecruitmentEmploymentAdditionalPersonnel'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Functie'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_Function'
    , xbrlName: 'Function'
    , itemType: 'xbrli:stringItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsDepartmentHeadPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Vrijstelling'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_FullExemptionAdditionalPersonnel'
    , xbrlName: 'FullExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Effectief vrijgesteld'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_ActualExemptionAdditionalPersonnel'
    , xbrlName: 'ActualExemptionAdditionalPersonnel'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-eapsf_ExemptionAdditionalPersonnelSpecificFunctionsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '328K - Degressief af te schrijven vaste activa'

    , code: '328K'
    , oldCode: ''
    , xbrlId: 'tax-inc_DegressiveDepreciationFixedAssetsForm'
    , xbrlName: 'DegressiveDepreciationFixedAssetsForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Vermoedelijke gebruiksduur, in jaren'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_UsefulEconomicLifeInYears'
    , xbrlName: 'UsefulEconomicLifeInYears'
    , itemType: 'pfs-dt:nonNegativeInteger6ItemType'
    , concept: 't-dfa_DepreciationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Degressief afschrijvingspercentage'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AnnuityDegressiveDepreciation'
    , xbrlName: 'AnnuityDegressiveDepreciation'
    , itemType: 'pfs-dt:percentageItemType'
    , concept: 't-dfa_DepreciationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Datum van verkrijging of totstandbrenging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateAcquisitionInvestment'
    , xbrlName: 'DateAcquisitionInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-dfa_DepreciationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }


    ]
    }
  ,
    { xtype: 'biztax-tab', title: '328L - Vaste activa voorheen in het stelsel van degressieve afschrijvingen opgenomen'

    , code: '328L'
    , oldCode: ''
    , xbrlId: 'tax-inc_RevocationDegressiveDepreciationFixedAssetsForm'
    , xbrlName: 'RevocationDegressiveDepreciationFixedAssetsForm'
    , itemType: 'xbrli:stringItemType'
    , concept: ''
    , isAbstract: true
    , items: [

    { xtype: 'biztax-pane', title: 'Vermoedelijke gebruiksduur, in jaren'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_UsefulEconomicLifeInYears'
    , xbrlName: 'UsefulEconomicLifeInYears'
    , itemType: 'pfs-dt:nonNegativeInteger6ItemType'
    , concept: 't-dfa_DepreciationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Datum van verkrijging of totstandbrenging'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_DateAcquisitionInvestment'
    , xbrlName: 'DateAcquisitionInvestment'
    , itemType: 'xbrli:dateItemType'
    , concept: 't-dfa_DepreciationFixedAssetsPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }

  ,
    { xtype: 'biztax-pane', title: 'Aanschaffings- of beleggingswaarde'

    , code: ''
    , oldCode: ''
    , xbrlId: 'tax-inc_AcquisitionInvestmentValue'
    , xbrlName: 'AcquisitionInvestmentValue'
    , itemType: 'pfs-dt:nonNegativeMonetary14D2ItemType'
    , concept: 't-cg_CapitalGainPrimaryConcepts'
    , isAbstract: false
    , items: [

    ]
    }


    ]
    }



   ]
};
    
  
*/

  
eBook.BizTax.AY.ref = 'module';

eBook.BizTax.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit',
            items: [
                eBook.BizTax.AY
            ]
        });
        eBook.BizTax.Window.superclass.initComponent.apply(this, arguments);
    }
    , show: function() {
        eBook.BizTax.Window.superclass.show.call(this);
        this.getEl().mask('rendering tab');
        this.module.setActiveTab(0);
        this.getEl().unmask();
    }
});

/*
var tst = function() {
    var wn = new eBook.BizTax.Window({});
    wn.show();
};
tst.defer(3000, this);

*/