eBook.BizTax.ValuelistOrOther = Ext.extend(Ext.Component, {
    xtype: 'biztax-ValuelistOrOther'
    , initComponent: function () {
        var cgDef = Ext.get(this.domConfig);
        Ext.apply(this, {
            applyTo: cgDef,
            defattribs: this.getFieldAttribs(cgDef)
        });

        eBook.BizTax.ValuelistOrOther.superclass.initComponent.apply(this, arguments);
    }
    , getFieldAttribs: function (el) {
        var attribs = {};
        if (el.dom) el = el.dom;
        for (var attr, i = 0, attrs = el.attributes, l = attrs.length; i < l; i++) {
            attr = attrs.item(i);
            attribs[attr.nodeName] = attr.nodeValue;
        }
        return attribs;
    }
    , afterRender: function (cnt) {
        eBook.BizTax.ValuelistOrOther.superclass.afterRender.call(this);
        this.tdVlEl = this.el.child('.biztax-valuelistorother-vl');
        this.defattribs["data-xbrl-prefix"] = this.tdVlEl.getAttribute('data-xbrl-prefix');
        this.tdVlEl.child('input').remove();
        var lstlbl = this.tdVlEl.dom.innerText;
        this.tdVlEl.update("");
        this.radioVl = new Ext.form.Radio({ renderTo: this.tdVlEl, name: this.id, value: 'LIST', boxLabel: lstlbl
            , tpe: 'LIST'
        });

        this.mon(this.radioVl, 'check', this.onRadioCheck, this);
        this.mon(this.radioVl, 'click', this.onRadioCheck, this);
        //this.radioVl.on('click', this.onRadioCheck, this);
        this.tdOtEl = this.el.child('.biztax-valuelistorother-other');
        this.tdOtEl.child('input').remove();
        lstlbl = this.tdOtEl.dom.innerText;
        this.tdOtEl.update("");
        this.radioOt = new Ext.form.Radio({ renderTo: this.tdOtEl, name: this.id, value: 'OTHER', boxLabel: lstlbl, tpe: 'OTHER'
        });
        this.mon(this.radioOt, 'check', this.onRadioCheck, this);

        this.applyFieldRendering();
        this.getModule().store.registerField(this);

        this.setActiveFields('valuelist');
    }
    , getValue: function () {
        var res = {
            tpe: this.activeTpe,
            fields: []
        };
        switch (this.activeTpe) {
            case 'valuelist':
                res.fields.push(this.valueField);
                break;
            default:
                for (var i = 0; i < this.otherFields.length; i++) {
                    res.fields.push(this.otherFields[i]);
                }
                break;
        }

        return res;
    }
    , onFieldChange: function (fld) {
        if (!this.suspended) this.getModule().store.onFieldChange(this);
    }
    , onRadioCheck: function (fld, chck) {
        if (chck) {
            var tpe = fld.tpe == "LIST" ? "valuelist" : "others";
            this.setActiveFields(tpe);
        }
    }
    , getModule: function () {
        return Ext.getCmp(this.moduleId);
    }
     , applyFieldRendering: function () {
         var fieldTypes = eBook.BizTax[this.getModule().locator].FieldTypes;
         this.otherFields = [];
         this.fields = [];
         var tdval = this.tdVlEl.parent('tr').child('td:nth-child(2)');
         var flds = this.el.select('.biztax-field-wrapper').elements;
         for (var i = 0; i < flds.length; i++) {
             var cfg = { defattribs: this.getFieldAttribs(flds[i]), renderTo: flds[i], width: '90%' };
             if (cfg.defattribs['data-xbrl-fieldtype']) {
                 if (cfg.defattribs['data-xbrl-fieldtype'] == 'valuelist') {
                     Ext.apply(cfg, Ext.clone(fieldTypes[cfg.defattribs['data-xbrl-valuelistid']]));
                     cfg.width = '90%'; // Ext.get(tdval.get
                     var fld = Ext.create(cfg);
                     this.valueField = fld;
                     this.valueField.setWidth(400);
                     this.mon(this.valueField, 'select', this.onFieldChange, this);
                     // this.fields.push(fld);
                 } else {
                     Ext.apply(cfg, fieldTypes[cfg.defattribs['data-xbrl-fieldtype']]);
                     var fld = Ext.create(cfg);
                     this.mon(fld, 'change', this.onFieldChange, this);
                     this.otherFields.push(fld);
                     //  this.fields.push(fld);
                 }
             } else {
             }
             Ext.get(flds[i]).addClass("biztax-field-rendered");
         }
     }
     , clearOthers: function () {
         for (var i = 0; i < this.otherFields.length; i++) {
             this.otherFields[i].suspendEvents(false);
             this.otherFields[i].setValue('');
             this.otherFields[i].resumeEvents();
         }
     }
     , hideOthers: function () {
         for (var i = 0; i < this.otherFields.length; i++) {
             this.otherFields[i].hide();
         }
     }
       , showOthers: function () {
           for (var i = 0; i < this.otherFields.length; i++) {
               this.otherFields[i].show();
           }
       }
     , setValue: function (el) {
         this.suspended = true;
         if (el.Children == null || el.Children.length == 0) {
             this.setActiveFields('valuelist');
             this.valueField.suspendEvents(false);
             this.valueField.clearValue();
             this.valueField.resumeEvents();
             this.clearOthers();

         } else {
             if (el.Children[0].FullName.indexOf('pfs-vl') > -1) {
                 this.setActiveFields('valuelist');
                 this.clearOthers();
                 this.valueField.suspendEvents(false);
                 this.valueField.setValue(el.Children[0].FullName);
                 this.valueField.resumeEvents();
             } else {
                 this.setActiveFields('others');
                 for (var i = 0; i < el.Children.length; i++) {
                     for (var j = 0; j < this.otherFields.length; j++) {

                         if (this.otherFields[j].defattribs["data-xbrl-name"] == el.Children[i].Name) {
                             this.otherFields[j].suspendEvents(false);
                             this.otherFields[j].setValue(el.Children[i].Value);
                             this.otherFields[j].resumeEvents();
                         }
                     }
                 }
                 this.valueField.clearValue();
                 //for(var i=0
             }
         }
         this.suspended = false;

     }
     , setActiveFields: function (tpe) {
         this.activeTpe = tpe;
         switch (tpe) {
             case 'valuelist':
                 this.valueField.show();
                 this.hideOthers();
                 this.radioVl.suspendEvents(false);
                 this.radioVl.setValue(true);
                 this.radioVl.resumeEvents();
                 this.radioOt.suspendEvents(false);
                 this.radioOt.setValue(false);
                 this.radioOt.resumeEvents();
                 break;
             default:
                 this.valueField.hide();
                 this.showOthers();
                 this.radioVl.suspendEvents(false);
                 this.radioVl.setValue(false);
                 this.radioVl.resumeEvents();
                 this.radioOt.suspendEvents(false);
                 this.radioOt.setValue(true);
                 this.radioOt.resumeEvents();
                 break;
         }
     }

     , destroy: function () {
         this.valueField.destroy();
         for (var j = this.otherFields.length - 1; j > -1; j--) {
             this.otherFields[j].destroy;
         }
         delete this.valueField;
         delete this.otherFields;
         this.radioVl.destroy();
         this.radiooT.destroy();
         eBook.BizTax.ValuelistOrOther.superclass.destroy.call(this);
     }


});
 Ext.reg('biztax-ValuelistOrOther', eBook.BizTax.ValuelistOrOther);