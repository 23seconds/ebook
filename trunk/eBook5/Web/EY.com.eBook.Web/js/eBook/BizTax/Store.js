eBook.BizTax.Store = Ext.extend(Object, {
    constructor: function (config) {
        //this.listeners = config.listeners ? config.listeners : config;
        this.elementsByFields = {};
        this.fieldsByElements = {};
        this.biztax = config.biztax;
        this.serviceUrl = config.serviceUrl;
        this.moduleId = config.moduleId;
    }
    , activeTabs: []
    , data: {}
    , fields: {
        byElementPath: {}
        , byElementId: {}
        , contextGrids: {}
        , linkedContextGrids: {}
    }
    , indexes: {
        elementsByContextRef: {}
        , elementsById: {}
        , elementsByPath: {}
        , contextsByDefId: {}
        , contextsByRef: {}
        , contextsByParent: {}
    }
    , getDataContract: function () {
        return this.data;
    }
    , myServiceUrl: function (method) {
        return this.serviceUrl + this.biztax.assessmentyear + "/" + this.biztax.type + ".svc/" + method;
    }
    , loadData: function (callback, scope) {
        var me = this;
        Ext.Ajax.request({
            url: me.myServiceUrl("GetBizTax")
            , method: 'POST'
            , params: Ext.encode({ cbdc: {
                FileId: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
                    , Type: me.biztax.type
            }
            })
            , callback: me.onDataRetreived
            , scope: me
            , callerCallback: { fn: callback, scope: scope }
        });
    }
    , onDataRetreived: function (opts, success, resp) {
        var robj, mod = this.getModule()
        if (!success) {
            eBook.Interface.showResponseError(resp, 'Failed retrieving Biztax data.');
            opts.callerCallback.fn.call(opts.callerCallback.scope || this);
            return null;
        }
        robj = Ext.decode(resp.responseText);
        this.data = robj.GetBizTaxResult;

        if (this.data.WorksheetsNewer) {
            this.updateFromWorksheets(opts.callerCallback);
            return;
        }

        mod.preRenderTabs(Ext.pluck(this.data.Data.Fiches, "FicheId"));


        // clear existing data
        // copy global info
        this.clearErrors();
        this.indexData();
        this.setErrors(this.data.Data.Errors);
        opts.callerCallback.fn.call(opts.callerCallback.scope || this);

    }
    , updateFromWorksheets: function () {
        this.getModule().getEl().mask("Updating worksheet info");
        eBook.CachedAjax.request({
            url: eBook.Service.rule(eBook.Interface.currentFile.get('AssessmentYear')) + 'UpdateBizTax'
                , method: 'POST'
                , params: Ext.encode({ cfdc: { FileId: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.Culture} })
                , callback: function () {
                    var mod = this.getModule();
                    mod.loadBizTax('' + eBook.Interface.currentFile.get('AssessmentYear'), mod.biztaxType)
                }
                , started: new Date().getTime()
                , scope: this
        });
    }


    , registerContextGrid: function (cg, contextid, linkedids) {
        var cgid;
        if (!Ext.isArray(linkedids)) {
            linkedids = [linkedids];
        }

        this.fields.contextGrids[contextid] = {
            cmpId: cg.id
        };

        this.fields.linkedContextGrids[contextid] = linkedids


        return this.getContextGridData(contextid);
    }
    , updateLinkedContextGrids: function (contextid) {
        var ctxids = this.fields.linkedContextGrids[contextid];
        if (ctxids.length == 0) {
            return;
        }
        for (var j = 0; j < ctxids.length; j++) {
            if (ctxids[j] != contextid && this.fields.contextGrids[ctxids[j]]) {
                Ext.getCmp(this.fields.contextGrids[ctxids[j]].cmpId).reload();
            }
        }
    }
    , getContextGridData: function (contextid) {
        var ctxids = this.fields.linkedContextGrids[contextid];
        var ids = {};
        var res = [];
        for (var k = 0; k < ctxids.length; k++) {
            cgid = ctxids[k];
            if (this.indexes.contextsByParent[cgid]) {
                for (var i = 0; i < this.indexes.contextsByParent[cgid].length; i++) {
                    var did = this.indexes.contextsByParent[cgid][i];
                    if (!ids['_' + did]) {
                        ids['_' + did] = true;
                        var rs = { defid: did, data: {} };
                        var ctx = null;
                        var ctxIdxs = this.indexes.contextsByDefId[did];
                        for (var j = 0; j < ctxIdxs.length; j++) {
                            var ct = this.data.Data.Contexts[ctxIdxs[j]];
                            var pr = ct.Id.split('_')[0];
                            if (ctx == null) {
                                ctx = ct;
                            }
                            rs[pr] = ct;
                        }
                        for (var j = 0; j < ctx.Scenario.length; j++) {
                            rs.data[ctx.Scenario[j].Dimension.replace(":", "_")] = ctx.Scenario[j].Value;
                        }

                        res.push(rs);
                    }
                }
            }
        }
        return res;
    }

    , registerField: function (fld, contextgrid) {
        var fpath = fld.defattribs["data-xbrl-path"] + "/" + fld.defattribs["data-xbrl-id"];
        var xbrlId = fld.defattribs["data-xbrl-id"];
        var xtype = fld.getXType() || fld.xtype;
        if (xtype == "combo") fpath = fld.defattribs["data-xbrl-path"] + "/" + fld.defattribs["data-xbrl-name"];

        if (fld.elementPath && this.fields.byElementPath[fld.elementPath]) {
            delete this.fields.byElementPath[fld.elementPath];
        }
        if (!this.fields.byElementPath[fpath]) this.fields.byElementPath[fpath] = [];
        fld.elementPath = fpath;

        if (this.errFields['_' + xbrlId]) {
            fld.markInvalid(this.errFields['_' + xbrlId]);
            this.getModule().setTabInvalid(fld.tabCode);
        }

        this.fields.byElementPath[fpath].push({
            cmpId: fld.id
            , elPath: fpath
            , xtype: fld.xtype
            , elId: fld.defattribs["data-xbrl-id"]
        });

        this.setFieldValueListener(fld);
        var el = null;
        var val = null;
        if (this.data.Data) {
            el = this.getElementByPath(fpath);
            val = this.getElementValue(fpath, xtype);
        }
        if (xtype == "checkbox" && (val == null || val == "")) {
            this.onFieldChange(fld);
        }

        if (!fld.isCalculated) {
            if ((el && el.Locked) || this.data.Locked) {
                fld.disable();
            } else {
                fld.enable();
            }
        }

        if (xtype == 'biztax-ValuelistOrOther') {
            return el;
        }
        return val;
    }
    , removeContextsByDefId: function (defId, flds) {
        var ctxIdxs = this.indexes.contextsByDefId[defId];
        var elidxs = [];

        if (!ctxIdxs) return;
        for (var j = 0; j < ctxIdxs.length; j++) {
            var ctx = this.data.Data.Contexts[ctxIdxs[j]];
            var idxs = this.indexes.elementsByContextRef[ctx.Id];
            if (idxs) {
                elidxs = elidxs.concat(idxs);
            }
        }

        /*
        for (var j = 0; j < elidxs.length; j++) {
        var 
        var elidx = this.indexes.elementsByPath[fld.elementPath];
        if (Ext.isDefined(elidx)) {
        if (Ext.isArray(elidx)) {
        elidxs.push(elidx[0]);
        } else {
        elidxs.push(elidx);
        }
        delete this.fields.byElementPath[fld.elementPath];
        }
        }
        */


        ctxIdxs.sort(function sortfunction(a, b) { return (a - b); });
        elidxs.sort(function sortfunction(a, b) { return (a - b); });

        for (var i = elidxs.length - 1; i > -1; i--) {
            this.data.Data.Elements.splice(elidxs[i], 1);
        }

        for (var i = ctxIdxs.length - 1; i > -1; i--) {
            this.data.Data.Contexts.splice(ctxIdxs[i], 1);
        }

        this.indexData();
        this.triggerSave();
    }

    , setFieldValueListener: function (fld) {
        var xtype = fld.getXType() || fld.xtype;

        if (xtype) {
            var listType = xtype.indexOf('check') > -1 ? 'check' : xtype.indexOf('combo') > -1 ? 'select' : 'change';
            fld.addListener(listType, this.onFieldChange, this, { buffer: 300 });
        }
    }
    , onFieldChange: function (fld) {
        if (!this.data || !this.data.Data) return;
        if (this.data.Data && this.data.Locked) return;
        var el = this.getElementByPath(fld.elementPath);
        var xtype = fld.getXType() || fld.xtype;
        var value = fld.getValue();

        if (value && value != '' && xtype == 'datefield') {
            value = value.format('Y-m-d');
        }

        if (!el) {
            el = this.createElementFromField(fld);
        }


        if (xtype == "biztax-uploadfield") {
            el.BinaryValue = Ext.clone(value);
            el.Value = null;
        } if (xtype == "biztax-ValuelistOrOther") {
            this.setValueListOrOtherValue(el, value, fld);
        } if (xtype == "combo") {
            this.setComboValueValue(el, fld);
        } else {
            el.Value = "" + (value == null ? "" : value);
        }
        this.updateFields(fld.elementPath, el)
        this.triggerSave();

    }
    , setValueListOrOtherValue: function (el, values, fld) {
        el.Children = [];
        for (var i = 0; i < values.fields.length; i++) {
            var mfld = values.fields[i];
            if (values.tpe == "valuelist") {
                var val = mfld.getValue();
                if (val != "" && val != null) {
                    val = val.split(':');
                    var valp = val[1].split('_');
                    el.Children.push({
                        AutoRendered: false
                    , Children: null
                    , BinaryValue: null
                    , Context: ""// fld.defattribs['data-xbrl-context']
                    , ContextRef: "D"
                    , Decimals: null
                    , Id: mfld.getValue().replace(':', '_') + "_" + mfld.defattribs['data-xbrl-period']
                    , Name: val[1]
                    , FullName: mfld.getValue()
                    , NameSpace: ""
                    , Period: mfld.defattribs['data-xbrl-period']
                    , Prefix: "pfs-vl"
                    , UnitRef: null
                    , Value: valp[valp.length - 1]
                    });
                }
            } else {
                el.Children.push({
                    AutoRendered: false
                    , Children: null
                    , BinaryValue: null
                    , Context: mfld.defattribs['data-xbrl-context']
                    , ContextRef: mfld.defattribs['data-xbrl-period']
                    , Decimals: mfld.defattribs['data-xbrl-decimals']
                    , Id: mfld.defattribs['data-xbrl-id']
                    , Name: mfld.defattribs['data-xbrl-name']
                     , FullName: mfld.defattribs['data-xbrl-prefix'] + ":" + mfld.defattribs['data-xbrl-name']
                    , NameSpace: ""
                    , Period: mfld.defattribs['data-xbrl-period']
                    , Prefix: mfld.defattribs['data-xbrl-prefix']
                    , UnitRef: mfld.defattribs['data-xbrl-unitref']
                    , Value: mfld.getValue()
                });
            }
        }
    }
    , setComboValueValue: function (el, fld) {
        var val = fld.getValue().split(':');
        var valp = val[1].split('_');
        el.Children = [{
            AutoRendered: false
            , Children: null
            , BinaryValue: null
            , Context: ""// fld.defattribs['data-xbrl-context']
            , ContextRef: "D"
            , Decimals: null
            , Id: fld.getValue().replace(':', '_') + "_" + fld.defattribs['data-xbrl-period']
            , Name: val[1]
            , NameSpace: ""
            , Period: fld.defattribs['data-xbrl-period']
            , Prefix: "pfs-vl"
            , UnitRef: null
            , Value: valp[valp.length - 1]
        }
        ];

    }
    , getMeta: function () {
        return {
            FileId: this.data.FileId,
            Type: this.data.Type,
            Calculation: this.data.Calculation,
            Validated: this.data.Validated,
            Status: this.data.Status,
            Locked: this.data.Status > 0,
            department: this.data.department,
            partnerId: this.data.partnerId,
            partnerName: this.data.partnerName,
            person: this.data.FileId
        };
    }
    , saveMeta: function (reloadData, cb) {
        var module = this.getModule();
        var scope = this;
        if (reloadData) {
            module.getEl().mask("Updating meta and reloading biztax data");
            cb = function () { this.loadData(this.getModule().onDataLoaded, this.getModule()); };
        } else {
            if (cb == null) {
                cb = function () {

                }; //dummy
            }
            else {
                scope = cb.scope;
                cb = cb.fn;
            }
        }


        Ext.Ajax.request({
            url: this.myServiceUrl("UpdateBizTaxMetaData")
            , method: 'POST'
            , params: Ext.encode({ fxidc: this.getMeta() })
            , callback: cb
            , scope: scope
            // , callerCallback: { fn: module.onDataLoaded, scope: module }
        });


    }
    , save: function () {
        var ip = this.getModule().getTopToolbar().infoPane;
        ip.clearSaveWait();
        ip.setSaveProgress();
        Ext.Ajax.request({
            url: this.myServiceUrl("SaveBizTax")
            , method: 'POST'
            , params: Ext.encode({ fxdc: this.data })
            , callback: this.onSaved
            , scope: this
            //, callerCallback: { fn: callback, scope: scope }
        });

    }
    , errFields: {}
    , clearErrors: function () {
        var mod = this.getModule(),
            fels = mod.getEl().query('.fld-biztax-error'),
            i = 0;
        this.errFields = {};
        for (i = 0; i < fels.length; i++) {
            Ext.getCmp(fels[i].id).clearInvalid();
        }
    }
    , setFieldError: function (mod, xbrlId, msg) {
        var dels = mod.getEl().query('div[data-xbrl-id=' + xbrlId + ']'),
            mtab = null;

        if (!this.errFields['_' + xbrlId]) {
            this.errFields['_' + xbrlId] = msg;
        }
        for (var i = 0; i < dels.length; i++) {
            dels[0] = Ext.get(dels[0]);
            var iel = dels[0].child('input');
           
            if (iel) {
                iel = Ext.getCmp(iel.id);
                iel.markInvalid(msg);
                mod.setTabInvalid(iel.tabCode);
            }
        }
    }
    , setErrors: function (errors) {
        var mod = this.getModule(),
            vitals = mod.getTopToolbar().vitals,
            i = 0, j = 0, err;

        mod.clearTabErrors();
        this.clearErrors();
        var cultidx = eBook.Interface.Culture == 'fr-FR' ? 1 : 0;

        for (i = 0; i < errors.length; i++) {
            err = errors[i];
            var msg = err.Messages[cultidx].Message;
            for (j = 0; j < err.Fields.length; j++) {
                this.setFieldError(mod, err.Fields[j], msg);
            }
        }
        //this.getModule().getTopToolbar().validationPane.updateMe(errors);
        if (errors.length > 0) {
            vitals.getEl().mask('validation errors');
            if (Ext.query('.errorCount').length == 0) {
                Ext.query('.eBook-Warning-ico-24')[0].outerHTML += "<div class='errorCount'>" + errors.length + "</div>";
            } else {
                Ext.query('.errorCount')[0].innerText = errors.length
            }
        } else {
            vitals.getEl().unmask(true);
            if (Ext.query('.errorCount').length != 0) {
                Ext.get(Ext.query('.errorCount')[0]).remove();
            }
        }
    }
    , onSaved: function (opts, success, resp) {
        var robj = Ext.decode(resp.responseText);
        var res = robj.SaveBizTaxResult;
        this.data.Data.Errors = res.Errors;
        this.data.Data.TaxCalc = res.TaxCalc;
        this.setErrors(res.Errors);

        //alert(res.Errors.length + " errors in taxonomy");
        for (var i = 0; i < res.Elements.length; i++) {
            var elPath = "/" + res.Elements[i].Name + "_" + res.Elements[i].ContextRef;
            var el = null;
            if (!this.indexes.elementsByPath[elPath]) {
                this.data.Data.Elements.push(res.Elements[i]);
                el = this.data.Data.Elements[this.data.Data.Elements.length - 1];
                this.addElementToIndex(el, this.data.Data.Elements.length - 1, "");
            } else {
                el = this.getElementByPath(elPath);
                el.Value = res.Elements[i].Value;
            }
            this.updateFields(elPath, el)
        }

        var ip = this.getModule().getTopToolbar().infoPane;
        ip.setCalc(this.data.Data.TaxCalc);
        ip.setSave(new Date());
        //alert("saved " + success);
    }
    , triggerSave: function () {
        if (!this.saveTask) {
            this.saveTask = new Ext.util.DelayedTask(function () {
                //this.getModule().saving();
                this.save();
                // this.saveTask.cancel();
            }, this);
        }
        this.saveTask.cancel();
        this.saveTask.delay(500);
        this.getModule().getTopToolbar().infoPane.setSaveWait();
    }
    , cancelSaveTrigger: function () {
        this.saveTask.cancel();
    }
    , getModule: function () {
        return Ext.getCmp(this.moduleId);
    }
    , createElementFromField: function (fld, value) {
        var cref = fld.defattribs['data-xbrl-id'].split('_');
        var xtype = fld.getXType() || fld.xtype;
        cref.shift();
        cref = cref.join('_');
        var el = {
            AutoRendered: false
            , Children: null
            , BinaryValue: null
            , Context: xtype == 'combo' ? "" : fld.defattribs['data-xbrl-context']
            , ContextRef: xtype == 'combo' ? "" : cref
            , Decimals: fld.defattribs['data-xbrl-decimals']
            , Id: xtype == 'combo' ? fld.defattribs['data-xbrl-name'] : fld.defattribs['data-xbrl-id']
            , Name: fld.defattribs['data-xbrl-name']
            , NameSpace: ""
            , Period: xtype == 'combo' ? "" : fld.defattribs['data-xbrl-period']
            , Prefix: fld.defattribs['data-xbrl-prefix']
            , UnitRef: fld.defattribs['data-xbrl-unitref']
            , Value: null
        };

        /* if (xtype != 'combo' && !this.indexes.contextsByRef[cref]) {
        // create context
        this.createContexts(defId, dimensions, oldRefs, contextId)
        }
        */

        var eps = fld.elementPath.split('/');
        eps.shift(); // clear out start
        eps.pop();
        if (eps.length > 0) {
            var arr = [];
            var path = "";
            var prevpath = "";
            var pel = null;

            for (var i = 0; i < eps.length; i++) {
                path += "/" + eps[i];
                var mel = this.getElementByPath(path);
                if (!mel) {
                    mel = {
                        AutoRendered: false
                    , Children: []
                    , BinaryValue: null
                    , Context: null
                    , ContextRef: null
                    , Decimals: null
                    , Id: eps[i]
                    , Name: eps[i]
                    , NameSpace: ""
                    , Period: null
                    , Prefix: "pfs-gcd"
                    , UnitRef: null
                    , Value: null
                    };
                    if (pel == null) {
                        arr.push(this.data.Data.Elements.length);
                        this.addElementToIndex(mel, this.data.Data.Elements.length, "");
                        this.data.Data.Elements.push(mel);
                        pel = mel;
                    } else {
                        this.addElementToIndex(mel, pel.Children.length, prevpath, arr.slice(0));
                        arr.push(pel.Children.length);
                        pel.Children.push(mel);
                        pel = mel;
                    }
                } else {
                    var parr = this.indexes.elementsByPath[path];
                    if (!Ext.isArray(parr)) parr = [parr];
                    arr = parr.slice(0);
                    pel = mel;
                }
                prevpath = path;

            }
            var idxel = pel.Children.length;

            pel.Children.push(el);
            this.addElementToIndex(el, idxel, path, arr);

        }
        else {
            this.addElementToIndex(el, this.data.Data.Elements.length, "");
            this.data.Data.Elements.push(el);
        }
        return el;
    }
    , getElementValue: function (fpath, xtype) {
        var el = this.getElementByPath(fpath);
        if (el && xtype == 'biztax-ValuelistOrOther') return el;
        if (el && xtype == 'biztax-uploadfield') return el.BinaryValue;
        if (el && xtype != 'combo') return el.Value;
        if (el && xtype == 'combo' && el.Children != null && el.Children.length > 0) {
            return el.Children[0].Prefix + ":" + el.Children[0].Name;
        }
        return null;
    }
    , getElementByPath: function (fpath) {
        return this.getElementAtIndex(this.indexes.elementsByPath[fpath]);
    }
    , getElementAtIndex: function (indxes, parent) {
        var idxes = indxes;
        var id = -1;
        if (Ext.isEmpty(idxes)) return null;
        if (Ext.isArray(idxes)) idxes = indxes.slice(0);
        if (Ext.isArray(idxes) && idxes.length == 1) {
            idxes = idxes[0]
        }
        if (Ext.isArray(idxes)) {
            id = parseInt(idxes.shift());
            if (parent) {
                return this.getElementAtIndex(idxes, parent.Children[id]);
            } else {
                return this.getElementAtIndex(idxes, this.data.Data.Elements[id]);
            }
        } else {
            id = parseInt(idxes);
            if (parent) {
                return parent.Children[id];
            } else {
                return this.data.Data.Elements[id];
            }
        }
    }
    , getElementsByIndexes: function (arrIdxs) {
        if (!arrIdxs) return [];
        var res = [];
        for (var i = 0; i < arrIdxs.length; i++) {
            res.push(this.getElementAtIndex(arrIdxs[i]));
        }
        return res;
    }
    , resetIndexes: function () {

    }
    , indexData: function () {
        //this.resetIndexes();
        this.indexContexts();
        this.indexElements();

    }
    , addIdx: function (idxName, id, idx) {
        if (!this[idxName][id]) this[idxName][id] = [];
        this[idxName][id].push(idx);
    }
    , indexContexts: function () {
        this.indexes.contextsByDefId = {};
        this.indexes.contextsByRef = {};
        this.indexes.contextsByParent = {};
        for (var i = 0; i < this.data.Data.Contexts.length; i++) {
            var ctx = this.data.Data.Contexts[i];
            if (!this.indexes.contextsByDefId[ctx.DefId]) {
                this.indexes.contextsByDefId[ctx.DefId] = [];
            }
            this.indexes.contextsByDefId[ctx.DefId].push(i);
            if (!(ctx.Id.indexOf('_') == -1 && ctx.Scenario != null && ctx.Scenario.length > 0)) {
                this.indexes.contextsByRef[ctx.Id] = i;
            }

            if (!this.indexes.contextsByParent[ctx.PresContextId]) {
                this.indexes.contextsByParent[ctx.PresContextId] = [ctx.DefId];
            } else {
                var addpres = true;
                for (var j = 0; j < this.indexes.contextsByParent[ctx.PresContextId].length; j++) {
                    if (this.indexes.contextsByParent[ctx.PresContextId][j] == ctx.DefId) {
                        addpres = false;
                        break;
                    }
                }
                if (addpres) {
                    this.indexes.contextsByParent[ctx.PresContextId].push(ctx.DefId);
                }
            }
        }
    }
    , getContextByRef: function (ref) {
        var idx = this.indexes.contextsByRef[ref];
        if (Ext.isNumber(idx)) {
            return this.data.Data.Contexts[idx];
        }
        return null;
    }
    , indexElements: function () {
        this.indexes.elementsByPath = {};
        this.indexes.elementsByContextRef = {};
        this.indexes.elementsById = {};
        for (var i = 0; i < this.data.Data.Elements.length; i++) {
            var el = this.data.Data.Elements[i];
            this.addElementToIndex(el, i, "", null, true);
        }
    }
    , addElementToIndex: function (el, i, path, arr, update) {
        var path = path + "/" + el.Name + (!Ext.isEmpty(el.ContextRef) ? "_" + el.ContextRef : "");
        if (!this.indexes.elementsByContextRef[el.ContextRef]) {
            this.indexes.elementsByContextRef[el.ContextRef] = [];
        }

        if (arr) {
            arr.push(i);
            this.indexes.elementsByPath[path] = arr;
            this.indexes.elementsByContextRef[el.ContextRef].push(arr);
            this.indexes.elementsById[el.Id] = arr;
        } else {
            arr = [i];
            this.indexes.elementsByPath[path] = i;
            this.indexes.elementsByContextRef[el.ContextRef].push(i);
            this.indexes.elementsById[el.Id] = i;
        }
        if (el.Children != null && el.Children.length > 0) {
            for (var j = 0; j < el.Children.length; j++) {
                this.addElementToIndex(el.Children[0 + j], 0 + j, "" + path, arr.slice(0), update);
            }

        }
        if (update) { this.updateFields(path, el); }

    }
    , updateFields: function (path, el) {
        var flds = this.fields.byElementPath[path];
        if (flds) {
            for (var t = 0; t < flds.length; t++) {
                var f = Ext.getCmp(flds[t].cmpId);
                if (f.xtype == 'combo') {
                    if (el.Children != null && el.Children.length > 0) {
                        f.setValue(el.Children[0].Prefix + ":" + el.Children[0].Name);
                    }
                } else if (f.xtype == 'biztax-ValuelistOrOther') {
                    f.setValue(el);
                } else if (f.xtype == 'biztax-uploadfield') {
                    f.setValue(Ext.clone(el.BinaryValue));
                } else {
                    f.setValue(el.Value);
                }
                if (!f.isCalculated) {
                    if ((el && el.Locked) || this.data.Locked) {
                        f.disable();
                    } else {
                        f.enable();
                    }
                }
            }
        }
    }
    , updateContexts: function (defId, dimensions, oldRefs, contextId, flds) {

        var ctxs = this.createContexts(defId, dimensions, oldRefs, contextId);
        var periods = ['D', 'I-Start', 'I-End'];

        for (var i = 0; i < periods.length; i++) {
            var exist = this.indexes.contextsByRef[ctxs[periods[i]].Id];
            if (Ext.isDefined(exist) && exist != null && exist > -1) {
                var ctxEx = this.data.Data.Contexts[exist];
                if (ctxEx.DefId != defId) {
                    return false;
                }
            }
        }

        this.removeContextsByDefId(defId, flds);
        for (var i = 0; i < periods.length; i++) {

            if (ctxs[periods[i]]) {
                this.data.Data.Contexts.push(ctxs[periods[i]]);
            }
        }


        this.indexContexts();
        this.triggerSave();
        return ctxs;
    }
    , removeContextIndexes: function (refid) {
        var idxs = [];
        if (this.indexes.elementsByContextRef[refid]) {
            idxs = this.indexes.elementsByContextRef[refid];
            var els = this.getElementsByIndexes(idxs);
            for (var i = 0; i < els.length; i++) {
                delete this.fields.byElementPath["/" + els[i].Name];
                delete this.indexes.elementsByPath["/" + els[i].Name];
                delete this.indexes.elementsById[els.Id];
            }
            delete this.indexes.elementsByContextRef[refid];
            idxs.sort(function sortfunction(a, b) { return (a - b); });

            for (var i = idxs.length - 1; i > -1; i--) {
                var eli = idxs[i];
                if (Ext.isArray(eli)) eli = eli[0];
                this.data.Data.Elements.splice(eli, 1);
            }
        }

        delete this.indexes.contextsByRef[refid];

    }
    , cleanerRegEx: /[^a-zA-Z0-9\-]/gi
    , createContexts: function (defId, dimensions, oldRefs, contextId) {
        if (!Ext.isArray(dimensions)) dimensions = [dimensions];

        var scenario = [];
        var mainId = '';
        var d = Ext.clone(this.getContextByRef("D"));
        var istart = Ext.clone(this.getContextByRef("I-Start"));
        var iend = Ext.clone(this.getContextByRef("I-End"));

        for (var i = 0; i < dimensions.length; i++) {
            var ds = dimensions[i];
            var scen = { Dimension: ds.dimension };
            if (!ds.value) ds.value = "";
            var idVal = '';
            switch (ds.type) {
                case "typedMember":
                    scen.__type = 'ContextScenarioTypeDataContract:#EY.com.eBook.BizTax';
                    scen.Value = ds.value;
                    scen.Name = ds.type;
                    scen.Type = ds.memberType;
                    idVal = scen.Value.replace(this.cleanerRegEx, '');
                    break;
                case "explicitMember":
                    scen.__type = 'ContextScenarioExplicitDataContract:#EY.com.eBook.BizTax';
                    scen.Value = ds.value;
                    scen.Name = ds.type;
                    idVal = scen.Value.split(':')[1];
            }
            mainId += '__id__' + idVal;
            scenario.push(scen);
        }


        d.Scenario = scenario;
        d.Id = 'D' + mainId;
        d.DefId = defId;
        d.PresContextId = contextId;

        istart.Scenario = scenario;
        istart.Id = 'I-Start' + mainId;
        istart.DefId = defId;
        istart.PresContextId = contextId;

        iend.Scenario = scenario;
        iend.Id = 'I-End' + mainId;
        iend.DefId = defId;
        iend.PresContextId = contextId;

        result = {
            'D': d
            , 'I-Start': istart
            , 'I-End': iend
        };

        return result;
    }


    // OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD
    , clearElementIndexes: function () {
        this.elementsByPeriod = {};
        this.elementsByNamePeriod = {};
        this.elementsByContext = {};
        this.elementsByNameContext = {};
        this.elementsByNamePeriodContext = {}; //unique
        this.elementsByName = {};
    }
    , clearData: function () {
        this.biztaxGeneral = {};
        this.contexts = {};
        this.elements = [];
        this.clearElementIndexes();
        //        this.elementsByFields = {};
        //        this.fieldsByElements = {};
    }

    , getElementValueById: function (id, xtype) {
        var el = this.getElementById(id);
        return this.retrieveElementValue(el, xtype);
    }
    , retrieveElementValue: function (el, xtype) {
        if (!el) return null;
        if (!Ext.isEmpty(el.Decimals)) {
            return Ext.isEmpty(el.Value) ? 0 : parseFloat(el.Value);
        }
        if (xtype == "biztax-uploadfield") {
            return Ext.clone(el.BinaryValue);
        }
        if (xtype == "combo") {
            if (el.Children != null && el.Children.length > 0) {
                return el.Children[0].Prefix + '_' + el.Children[0].Name;
            }
        }
        //  if (el.Value == "true" || el.Value == "false") return eval(el.Value);
        return el.Value;
    }
    , getNumericElementValueById: function (id) {
        var val = this.getElementValueById(id);
        if (val == null) return 0;
        return val;
    }
    , addOrUpdateElement: function (cfg) {
        if (!this.elementsByNamePeriodContext[cfg.id]) {
            this.addElement(this.createElementByCalcCfg(cfg));
        } else {
            this.updateElement(cfg.id, cfg.value, '', null, false); //true  
        }
    }
    , updateElement: function (idxorEl, value, xtype, fld, donotoverwrite) {
        if (Ext.isString(idxorEl)) {
            idxorEl = this.elementsByNamePeriodContext[idxorEl];
            if (idxorEl == null || !Ext.isDefined(idxorEl)) return;
        }
        if (Ext.isObject(idxorEl)) {
            // for child elements.
            if (idxorEl.AutoRendered && donotoverwrite) return;
            if (xtype == "biztax-uploadfield") {
                idxorEl.BinaryValue = Ext.decode(Ext.encode(value));
                idxorEl.Value = null;
            } if (xtype == "combo") {
                var child = this.createComboValueElement(fld);
                idxorEl.Children = child ? [child] : [];
            } else {
                idxorEl.Value = "" + (value == null ? "" : value);
            }
            idxorEl.AutoRendered = false;
            return idxorEl;
        } else {
            if (this.elements[idxorEl].AutoRendered && donotoverwrite) return;
            if (xtype == "biztax-uploadfield") {
                this.elements[idxorEl].BinaryValue = Ext.decode(Ext.encode(value));
                this.elements[idxorEl].Value = null;
            } if (xtype == "combo") {
                var child = this.createComboValueElement(value);
                this.elements[idxorEl].Children = child ? [child] : [];
            } else {
                this.elements[idxorEl].Value = "" + (value == null ? "" : value);
            }
            this.elements[idxorEl].AutoRendered = false;
        }
    }
    , findElementsByIndex: function (idxName, id) {
        if (!this[idxName]) return [];
        if (!this[idxName][id]) return [];
        return this.getElementsByIndexes(this[idxName][id]);
    }
    , getElementById: function (id) {
        var res = this.findElementsForId(id);
        if (res.length == 0) return null;
        return res[0];
    }
    , findElementsForId: function (id) {
        return this.findElementsByIndex('elementsByNamePeriodContext', id);
    }
    , findElementsForName: function (fullname) {
        return this.findElementsByIndex('elementsByName', fullname);
    }
    , findElementsForPeriod: function (period) {
        return this.findElementsByIndex('elementsByPeriod', period);
    }
    , findElementsForNamePeriod: function (fullname, period) {
        return this.findElementsByIndex('elementsByNamePeriod', fullname + '_' + period);
    }
    , findElementsForContext: function (context) {
        return this.findElementsByIndex('elementsByContext', context);
    }
    , findElementsForNameContext: function (fullname, context) {
        return this.findElementsByIndex('elementsByNameContext', fullname + '_' + context);
    }

    , destroy: function () {
        delete this.data;
        delete this.fields;
        delete this.indexes;
        //        eBook.BizTax.Store.superclass.destroy.call(this);
    }
});
