/* TIM G REMARKS

    CHANGES:
        Added getData and setData functionality in order to retreive or set data contents
        Changed the tpl as well to add icons
        Added container-wide events to handle mouseovers 
        Added visible selection.
        Moved listeners on dataview to initcomponent. Only render when needed and distinctive listeners if opened multiple times
        Changed the decode/encode copy which was implemented to ensure value-copy rather then reference-copy to
            newly implemented Ext.clone (code-reuse + tweaking of Ext 4)... yay
        Ensured iconCls for indexItem is filled in serverside.
        
    TODO: 
        change store approach with single array, switch from dataview to Panel extension
        
    WHY?: 
        We don't need 80% of store functionality and don't really need to create 2 components (panel+dataview)
            The only reason why a panel and a dataview are used is for top toolbar implementation.
            -> switching to customised view will improve performance and memory signature for this component (less overhead)
            
    REALLY???: 
        Yep, really. Might sound a bit stupid, though consider the magnitude of the entire application and all components.
        Keeping an extensive application such as eBook fast, means cutting back overhead all were applicable.
        Cheers :-)
*/


eBook.BizTax.UploadEditorPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            tbar: [{
                xtype: 'button'
                , text: 'Delete'
                , ref: '../btnDelete'
                , iconCls: 'eBook-icon-delete-16'
                , disabled: true
               , handler: this.onDelete
               , scope: this
            }, '-', {
                xtype: 'button'
                , text: 'Move up'
                , ref: '../btnMoveUp'
                , iconCls: 'eBook-biztax-arrowup-ico'
                , disabled: true
                , direction: 'up'
                , handler: this.onMove
                , scope: this
            }, '-', {
                xtype: 'button'
                , text: 'Move Down'
                , ref: '../btnMoveDown'
                , iconCls: 'eBook-biztax-arrowdown-ico'
                , disabled: true
                , direction: 'down'
                , handler: this.onMove
                , scope: this
}]
            , items: [{
                xtype: 'biztaxUploadEditor', ref: 'editor'
}]
            });
            eBook.BizTax.UploadEditorPanel.superclass.initComponent.apply(this, arguments);
        }
        , onDelete: function() {
            this.editor.deleteSelected();
        }
        , getData: function() {
            return this.editor.getData();
        }
        , setData: function(dta) {
            this.editor.setData(dta);
        }
        , onMove: function(btn, e) {
            this.editor.moveSelected(btn.direction);
        }
        , toggleTButtons: function(idx) {
            this.btnDelete.enable();
            (idx == 0) ? this.btnMoveUp.disable() : this.btnMoveUp.enable();
            (idx == this.editor.all.elements.length - 1) ? this.btnMoveDown.disable() : this.btnMoveDown.enable();
        }
        , disableTButtons: function() {
            this.btnDelete.disable();
            this.btnMoveUp.disable();
            this.btnMoveDown.disable();
        }
        , afterRender: function() {
            eBook.BizTax.UploadEditorPanel.superclass.afterRender.apply(this, arguments);
            // DROP
            this.dvDDTarget = new Ext.dd.DropTarget(this.getEl(), {
                ddGroup: 'eBook.Bundle'
                   , srcView: this.items.items[0]
                   , srcPanel: this
                    , notifyDrop: function(ddSource, e, data) {
                        if (this.srcView) {
                            // If node = pdf
                            if (data.node.attributes.IsRepository) {
                                var repItem = data.node.attributes.Item;
                                attributes = {
                                    __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'
                                        , type: "PDFPAGE"
                                        , ItemId: repItem.Id
                                        , id: eBook.NewGuid()
                                        , title: repItem.FileName
                                        , indexed: true
                                        , iconCls: data.node.attributes.iconCls
                                };
                                data.node.attributes.indexItem = attributes;
                            }


                            // ENCODE DECODE in order to add a unique value instead of a reference/pointer
                            // Tim G: Changed encode decode with cloning method (transferred from Ext 4) :-)
                            var rt = new this.srcView.store.recordType({ indexItem: Ext.clone(data.node.attributes.indexItem) }); //data.node.attributes
                            this.srcView.store.add(rt);
                            this.srcView.clearSelections();
                            this.srcPanel.disableTButtons();
                        }
                        return true;
                    }
            });
        }
    });
    Ext.reg('biztaxUploadEditorPanel', eBook.BizTax.UploadEditorPanel);





    // indexItem should always have a value... so why the split?
    // if indexItem doesn't exist, neither should the record ==> is invalid.
    eBook.BizTax.UploadEditor = Ext.extend(Ext.DataView, {
        tpl: new Ext.XTemplate('<tpl for=".">'
                            , '<div class="biztax-editorwindow-node">'
                                , '<tpl if="values.indexItem != undefined"><tpl for="indexItem"><div class="biztax-uploadeditor-thumb {iconCls}"></div><div class="biztax-uploadeditor-txt">{title}</div></tpl></tpl>'
                                , '<tpl if="values.indexItem == undefined"><div class="biztax-uploadeditor-thumb ebook-icon-16-unknown"></div><div class="biztax-uploadeditor-txt">{text}</div></tpl>'
                            , '</div>'
                          , '</tpl>'
                          , { compiled: true }
    )
    , initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.biztax-editorwindow-node'
            , singleSelect: true
            , selectedClass: 'biztax-editorwindow-nodeselect'
// NEVER USED...
//            , parentWindow: this.refOwner.refOwner
//            , parentPanel: this.refOwner
            , store: new Ext.data.JsonStore({
                root: 'root'
                        , autoDestroy: true
                        , autoLoad: false
                        , fields: eBook.data.RecordTypes.LibraryNode
            })
            , autoScroll: true
        });

        eBook.BizTax.UploadEditor.superclass.initComponent.apply(this, arguments);
    }
    , getData: function() {
        var recs = this.store.getRange();
        return Ext.pluck(Ext.pluck(recs, 'data'), 'indexItem');
    }
    , setData: function(vals) {
        vals = Ext.clone(vals);
        for (var i = 0; i < vals.length; i++) {
            vals[i] = { indexItem: vals[i] };
        }
        this.store.loadData({ root: vals }, false);
    }
    , moveSelected: function(direction) {
        var recs = this.getSelectedRecords();
        if (recs.length == 0) return;
        var rec = recs[0];
        var idx = this.store.indexOf(rec);
        //if ((direction == "down" && idx == this.store.getCount() - 1) || direction == "up" && idx == 0) return;
        var nidx = direction == "down" ? idx + 1 : idx - 1;
        this.store.removeAt(idx);
        this.store.insert(nidx, [rec]);

        // Select your inserted node
        this.select(this.all.elements[nidx]);
        this.refOwner.toggleTButtons(nidx);
    }
    , deleteSelected: function() {
        var recs = this.getSelectedRecords();
        if (recs.lenght == 0) return;
        var rec = recs[0];
        var idx = this.store.indexOf(rec);
        this.store.removeAt(idx); // of index

        // reset the itemsetting form
        //this.ownerCt.ownerCt.itemSettings.suspendEvents();
        this.refOwner.refOwner.itemSettings.getForm().reset();
        this.refOwner.refOwner.itemSettings.disable();
        this.refOwner.refOwner.itemSettings.resumeEvents();
        this.refOwner.disableTButtons();
    }
    , setProperties: function(props) {
        //        if (this.attributes.properties == null) this.attributes.properties = {};
        //        Ext.apply(this.attributes.properties, props);
        //        if (props.type != "ROOT") this.setText(props.title);

        if (this.currentNode.get('indexItem') == null) this.currentNode.get('indexItem') = {};
        Ext.apply(this.currentNode.get('indexItem'), props);
        this.store.commitChanges();
        this.refresh();
        //if (props.type != "ROOT") this.setText(props.title);

    }
    , listeners: {
        click: {
            fn: function(dataView, index, node, e) {

                var parentPanel = this.refOwner;
                this.currentNode = dataView.store.getAt(index);
                parentPanel.refOwner.loadNodeSettings(this.currentNode);
                //parentPanel.enableTButtons();
                parentPanel.toggleTButtons(index);
            }
        }
    }
    });
Ext.reg('biztaxUploadEditor', eBook.BizTax.UploadEditor);