eBook.BizTax.Tab = Ext.extend(Ext.Panel, {
    initComponent: function () {

        Ext.apply(this, {
            autoScroll: true,
            padding: 15,
            titleTxt: this.title,
            title: '<span style="color:#900;">' + this.code + '. </span>' + this.title,
            cls: 'biztax-tab-body biztax-tab-code-' + this.code,
            id: 'biztax-tab-' + this.code,
            autoLoad: this.url,
            tbar: ['->', {
                ref: 'printpreview',
                iconCls: 'eBook-pdf-view-24',
                scale: 'medium',
                //width: 115,
                // iconAlign: 'top',
                handler: this.onPrintPreview,
                scope: this
                //,disabled: true //fileClosed
            }]
        });

        eBook.BizTax.Tab.superclass.initComponent.apply(this, arguments);
        if (this.elementsDisabledFn) this.elementsDisabled = this.elementsDisabledFn();
        this.elementsDisabled = this.elementsDisabled ? true : false;
    }
    , onPrintPreview: function () {
        eBook.Splash.setText("Generating print preview " + this.code);
        eBook.Splash.show();
        Ext.Ajax.request({
            url: eBook.Service.output + 'GetBiztaxFichePreview'
            , method: 'POST'
            , params: Ext.encode({
                cbfpdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.currentCulture,
                    FicheId: this.code,
                    FicheTitle: this.titleTxt
                }
            })
            , callback: this.onPrintPreviewResponse
            , scope: this
        });
    }
    , onPrintPreviewResponse: function (opts, success, resp) {
        eBook.Splash.hide();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetBiztaxFichePreviewResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , getModule: function () {
        return this.ownerCt;
    }
    , afterRender: function (cnt) {
        eBook.BizTax.Tab.superclass.afterRender.call(this);
        if (!this.autoLoad) {
            this.applyBizTaxRendering();
        }
    }
    , applyBizTaxRendering: function () {
        this.applyVlOrOtherRendering();
        this.applyContextRendering();
        this.applyFieldRendering();
    }
    , applyVlOrOtherRendering: function () {
        this.valuelistOrOther = [];
        var ctxs = this.el.select('.biztax-valuelistorother').elements;
        for (var i = 0; i < ctxs.length; i++) {
            var ctx = new eBook.BizTax.ValuelistOrOther({
                domConfig: ctxs[i]
                , bizTaxLocator: this.getModule().locator, moduleId: this.getModule().id
                , tabCode: this.code
            });
            this.valuelistOrOther.push(ctx);
        }
    }
    , applyContextRendering: function () {
        this.contextgrids = [];
        var ctxs = this.el.select('contextgrid').elements;
        for (var i = 0; i < ctxs.length; i++) {
            var ctx = new eBook.BizTax.Contextgrid({
                domConfig: ctxs[i], bizTaxLocator: this.getModule().locator, moduleId: this.getModule().id
                , tabCode: this.code
            });
            this.contextgrids.push(ctx);
        }
    }
    , applyFieldRendering: function () {
        var fieldTypes = eBook.BizTax[this.getModule().locator].FieldTypes;
        this.fields = [];
        var flds = this.el.select('.biztax-field-wrapper').elements;
        for (var i = 0; i < flds.length; i++) {
            var xfld = Ext.get(flds[i]);
            if (!xfld.hasClass("biztax-field-rendered")) {
                var cfg = { defattribs: this.getFieldAttribs(flds[i]), renderTo: flds[i]
                    , width: '90%'
                    , tabCode: this.code
                };
                cfg.isCalculated = false;
                if (cfg.defattribs['data-xbrl-calc'] == 'true') {
                    cfg.isCalculated = true;
                    cfg.disabled = true;
                }

                if (!xfld.parent('table.biztax-valuelistorother')) {
                    if (cfg.defattribs['data-xbrl-fieldtype']) {

                        if (cfg.defattribs['data-xbrl-fieldtype'] == 'valuelist') {
                            cfg.defattribs = this.getFieldAttribs(flds[i])
                            Ext.apply(cfg, Ext.clone(fieldTypes[cfg.defattribs['data-xbrl-valuelistid']]));
                            var fld = Ext.create(cfg);
                            var val = this.registerField(fld);
                            if (val) fld.setValue(val);
                            if (this.code == "Id") {
                                fld.setWidth(400);
                            }
                            this.fields.push(fld);
                        } else {
                            cfg.msgTarget = 'sidebiztax';
                            Ext.apply(cfg, fieldTypes[cfg.defattribs['data-xbrl-fieldtype']]);
                            var fld = Ext.create(cfg);
                            var val = this.registerField(fld);
                            if (val) fld.setValue(val);
                            this.fields.push(fld);
                        }
                    } else {
                    }
                }
            }
        }
    },
    getFieldAttribs: function (el) {
        var attribs = {};
        for (var attr, i = 0, attrs = el.attributes, l = attrs.length; i < l; i++) {
            attr = attrs.item(i);
            attribs[attr.nodeName] = attr.nodeValue;
        }
        return attribs;
    },
    doAutoLoad: function () {
        var u = this.body.getUpdater();
        u.on("update", this.applyBizTaxRendering, this);
        if (this.renderer) {
            u.setRenderer(this.renderer);
        }
        u.update(Ext.isObject(this.autoLoad) ? this.autoLoad : { url: this.autoLoad });
    }
    , destroy: function () {
        for (var i = this.fields.length - 1; i > -1; i--) {
            this.fields[i].destroy();
            delete this.fields[i];
        }
        alert("tab destroyed, fields left: " + this.fields.length);
        eBook.BizTax.Tab.superclass.destroy.call(this);
    }
    , registerField: function (fld) {
        return this.getModule().store.registerField(fld);
    }
    , destroy: function () {

        if (this.valuelistOrOther) {
            for (var j = this.valuelistOrOther.length - 1; j > -1; j--) {
                this.valuelistOrOther[j].destroy;
            }
        }
        delete this.valuelistOrOther;

        if (this.contextgrids) {
            for (var j = this.contextgrids.length - 1; j > -1; j--) {
                this.contextgrids[j].destroy;
            }
        }
        delete this.contextgrids;

        if (this.fields) {
            for (var j = this.fields.length - 1; j > -1; j--) {
                this.fields[j].destroy;
            }
        }
        delete this.fields;


        eBook.BizTax.Tab.superclass.destroy.call(this);
    }
});
Ext.reg('biztax-tab', eBook.BizTax.Tab);