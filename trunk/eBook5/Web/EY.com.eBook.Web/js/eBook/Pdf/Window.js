

eBook.Pdf.WindowMgr = new Ext.WindowGroup();
eBook.Pdf.WindowMgr.closeAll = function() {
    eBook.Pdf.WindowMgr.each(function(lst) { lst.close(); }, eBook.Pdf.WindowMgr);
};
eBook.Pdf.WindowMgr.zseed = 10000;


eBook.Pdf.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        var isClosed = eBook.Interface.currentFile.get('Closed');
        Ext.apply(this, {
            id: 'et-pdf-window',
            title: eBook.Pdf.Window_Title,
            iconCls:'eBook-Pdf-icon',
            items: [{ xtype: 'eBook.Pdf.DataView', ref: 'dataview'}],
            manager: eBook.Pdf.WindowMgr,
            autoScroll: true,
            minimizable: true,
            tbar: [{
                text: eBook.Pdf.Window_AddPdf,
                iconCls: 'eBook-pdf-add-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'addpdf',
                handler: this.onAddPdf,
                hidden: isClosed,
                scope: this
            }, {
                text: eBook.Pdf.Window_ReplacePdf,
                iconCls: 'eBook-pdf-replace-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'replacepdf',
                handler: this.onReplacePdf,
                disabled: true,
                hidden:isClosed,
                scope: this
            }, {
                text: eBook.Pdf.Window_RemovePdf,
                iconCls: 'eBook-pdf-remove-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'removepdf',
                handler: this.onDeletePdf,
                disabled: true,
                hidden: isClosed,
                scope: this
            }, {
                text: eBook.Pdf.Window_ViewPdf,
                iconCls: 'eBook-pdf-view-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'viewpdf',
                handler: this.onViewPdf,
                disabled: true,
                scope: this
            }, {
                text: eBook.Pdf.Window_RefreshList,
                iconCls: 'eBook-pdflist-refresh-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'refreshlist',
                handler: this.onRefreshList,
                scope: this
}],
                width: 460,
                bodyStyle: 'background-color:#FFF;padding:5px;',
                height: 460
            });
            eBook.Pdf.Window.superclass.initComponent.apply(this, arguments);

        }
    , show: function(args) {
        eBook.Pdf.Window.superclass.show.call(this);
        if (args && args.snapRight) {
            this.setPosition(eBook.Interface.viewPort.getWidth() - this.width - 20, 100);
        }
    }
    , minimized: false
    , minimize: function(wn) {
        this.toggleCollapse();
        this.minimized = !this.minimized;
    }
    , maximize: function() {
        if (this.minimized) {
            this.minimize();
        } else {
            eBook.Pdf.Window.superclass.maximize.call(this);
        }
    }
    , onSelect: function() {
        var tb = this.getTopToolbar();
        tb.replacepdf.enable();
        tb.removepdf.enable();
        tb.viewpdf.enable();
    }
    , onDeselect: function() {
        var tb = this.getTopToolbar();
        tb.replacepdf.disable();
        tb.removepdf.disable();
        tb.viewpdf.disable();
    }
    , onAddPdf: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var wn = new eBook.Pdf.UploadWindow({ parentCaller: this });
        wn.show(this);
    }
    , onReplacePdf: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var pdf = this.dataview.getSelectedRecord();
        var wn = new eBook.Pdf.UploadWindow({ parentCaller: this, replacement: pdf });
        wn.show(this);
    }
    , onDeletePdf: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var pdf = this.dataview.getSelectedRecord();
        if (pdf) {

            Ext.WindowMgr.zseed = 20000;
            Ext.Msg.show({
                title: eBook.Bundle.Window_LibraryDeletePdf,
                msg: String.format(eBook.Bundle.Window_DeletePdfMsg, pdf.get('Name')),
                buttons: Ext.Msg.YESNO,
                fn: this.performDelete,
                scope: this,
                icon: Ext.MessageBox.QUESTION
            });
        }
    }
    , performDelete: function(btnid) {
        Ext.WindowMgr.zseed = 9000;
        if (btnid == "yes") {
            var pdf = this.dataview.getSelectedRecord();
            if (!pdf) return;

            var action = {
                text: String.format(eBook.Bundle.Window_DeletingPdfMsg, pdf.get('Name'))
                    , params: { cidc: {
                        Id: pdf.get('Id')
                    }
                    }
                    , action: 'DeletePdf'
            };
            this.dataview.removeSelected();
            this.onDeselect();
            this.addAction(action);
        }
    }
    , onViewPdf: function(pdf) {
        if (!pdf || !pdf.get) pdf = this.dataview.getSelectedRecord();
        window.open('ViewPdf.aspx?id=' + pdf.get('Id') + '&nm=' + pdf.get('Name'));
    }
    , refreshPdfs: function() {
        this.onRefreshList();
    }
    , onRefreshList: function() {
        this.dataview.store.load();
    }
    });

Ext.reg('eBook.Pdf.Window', eBook.Pdf.Window);


eBook.Pdf.DataView = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'et-pdf-view',
            tpl: '<tpl for=".">' +
                    '<div class="et-pdf-item" id="{Id}">' +
                        '<table><tbody>' +
                        '<tr><td style="text-align:left;" unselectable="on">' +
                            '{Name}' +
                        '</td>' +
                        '<td width="50" style="text-align:right;" unselectable="on">' +
                            '{Pages}p.' +
                        '</td></tr></table>' +
                    '</div>' +
                 '</tpl>',
            itemSelector: 'div.et-pdf-item',
            overClass: 'et-pdf-item-over',
            selectedClass: 'et-pdf-item-selected',
            singleSelect: true,
            store: new eBook.data.JsonStore({
                selectAction: 'GetFilePdfFiles'
                , autoDestroy: true
                , autoLoad: true
                , criteriaParameter: 'cidc'
                , fields: eBook.data.RecordTypes.PdfFile
                , baseParams: {
                    'Culture': eBook.Interface.Culture
                    , 'Id': eBook.Interface.currentFile.get('Id')
                }
            }),
            listeners: {
                render: this.initializeDragZone
                , selectionchange: this.onSelectionChange
                , dblclick: this.onDoubleClick
                , scope: this
            }
        });
        eBook.Pdf.DataView.superclass.initComponent.apply(this, arguments);
    }
    , onSelectionChange: function(dv, sels) {
        var selId = null; 
        if(sels && sels.length>0) selId=sels[0].id;

        if (this.selId != selId) {
            this.selId = selId;
            this.refOwner.onSelect();
        } else {
            this.selId = null;
            this.clearSelections(true);
            this.refOwner.onDeselect();
        }
    }
    , onDoubleClick: function(dv, idx, n, e) {
        this.refOwner.onViewPdf(this.store.getAt(idx));
    }
    , getSelectedRecord: function() {
        if (this.selId == null) return null;
        return this.getSelectedRecords()[0];
    }
    , removeSelected: function() {
        var sels = this.getSelectedRecords();
        this.store.remove(sels);
        this.store.commitChanges();
    }
    , initializeDragZone: function(v) {
        this.dragZone = new Ext.dd.DragZone(v.getEl(), {
            getDragData: function(e) {
                var sourceEl = e.getTarget(v.itemSelector, 10);
                if (sourceEl) {
                    d = sourceEl.cloneNode(true);
                    d.className = 'et-pdf-item-drag';
                    d.id = Ext.id();
                    return v.dragData = {
                        sourceEl: sourceEl,
                        repairXY: Ext.fly(sourceEl).getXY(),
                        ddel: d,
                        pdfData: v.getRecord(sourceEl).data
                    }
                }
            },
            getRepairXY: function() {
                return this.dragData.repairXY;
            }
        });
    }
});

Ext.reg('eBook.Pdf.DataView', eBook.Pdf.DataView);