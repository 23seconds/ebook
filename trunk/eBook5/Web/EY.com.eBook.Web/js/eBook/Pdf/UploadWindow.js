

eBook.Pdf.UploadWindow = Ext.extend(eBook.Window, {

    initComponent: function() {
        Ext.apply(this, {
            title: eBook.Pdf.UploadWindow_Title
            , layout: 'fit'
            , width: 650
            , height: 200
            , manager: eBook.Pdf.WindowMgr
            , modal: true
            //, iconCls: 'eBook-Window-account-ico'
            , items: [new Ext.FormPanel({
                        fileUpload: true
                        ,labelWidth:150
                        , items: [
                                {
                                    xtype: 'combo'
                                    , ref: 'replaceFor'
                                    , name: 'replaceFor'
                                    , fieldLabel: eBook.Pdf.UploadWindow_ReplacePdf
                                    , triggerAction: 'all'
                                    , typeAhead: false
                                    , selectOnFocus: true
                                    , autoSelect: true
                                    , allowBlank: true
                                    , forceSelection: true
                                    , valueField: 'Id'
                                    , displayField: 'Name'
                                    , editable: false
                                    , nullable: true
                                    , mode: 'local'
                                    , width: 350
                                    , store: new eBook.data.JsonStore({
                                        selectAction: 'GetFilePdfFiles'
                                        , autoDestroy: true
                                        , autoLoad: true
                                        , criteriaParameter: 'cidc'
                                        , baseParams: {
                                            'Culture': eBook.Interface.Culture
                                            , 'Id': eBook.Interface.currentFile.get('Id')
                                        }
                                        , fields: eBook.data.RecordTypes.PdfFile
                                    })
                                }
                                , new Ext.ux.form.FileUploadField({ width: 350, fieldLabel: eBook.Pdf.UploadWindow_ChoosePDF, fileTypeRegEx: /(\.pdf)$/i })]
                        , ref: 'pdfForm'

            })]
            , tbar: [{
                text: eBook.Pdf.UploadWindow_UploadPDF,
                ref: '../saveButton',
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
}]
            });

            eBook.Pdf.UploadWindow.superclass.initComponent.apply(this, arguments);
        }
    , onSaveClick: function(e) {
        var f = this.pdfForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Pdf.UploadWindow_WrongType);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Pdf.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function(fp, o) {
        //process o.result.file
        this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'ProcessNewPDF'
                , method: 'POST'
                , params: Ext.encode({ cnpdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    PdfFileName: o.result.file,
                    OriginalName: o.result.originalFile,
                    ReplacementFor: this.pdfForm.replaceFor.getValue()
                }
                })
                , callback: this.handleFileProcessing
                , scope: this
        });
    }
    , failedUpload: function(fp, o) {
        eBook.Interface.showError(o.message, this.title);
        this.getEl().unmask();
    }
    , handleFileProcessing: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            if (this.parentCaller) this.parentCaller.refreshPdfs();
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , show: function(caller) {
        this.parentCaller = caller;
        eBook.Pdf.UploadWindow.superclass.show.call(this);
        if (this.replacement) {
            this.pdfForm.replaceFor.setValue(this.replacement.get('Id'));
            this.pdfForm.replaceFor.setDisplayValue(this.replacement.get('Name')); 
        }
    }
});