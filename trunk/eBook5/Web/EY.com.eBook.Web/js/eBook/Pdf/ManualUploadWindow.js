

eBook.Pdf.ManualUploadWindow = Ext.extend(eBook.Window, {

    initComponent: function() {
        Ext.apply(this, {
            title: eBook.Pdf.UploadWindow_Title
            , layout: 'fit'
            , width: 650
            , height: 200
            , manager: eBook.Pdf.WindowMgr
            , modal: true
            //, iconCls: 'eBook-Window-account-ico'
            , items: [new Ext.FormPanel({
                fileUpload: true
                        , labelWidth: 150
                        , items: [
                                new Ext.ux.form.FileUploadField({ width: 350, fieldLabel: eBook.Pdf.UploadWindow_ChoosePDF, fileTypeRegEx: /(\.pdf)$/i })]
                        , ref: 'pdfForm'

            })]
            , tbar: [{
                text: eBook.Pdf.UploadWindow_UploadPDF,
                ref: '../saveButton',
                iconCls: 'eBook-window-save-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onSaveClick,
                scope: this
}]
            });

            eBook.Pdf.ManualUploadWindow.superclass.initComponent.apply(this, arguments);
        }
    , onSaveClick: function(e) {
        var f = this.pdfForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Pdf.UploadWindow_WrongType);
            return;
        }
        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Pdf.UploadWindow_Uploading,
            success: this.successUpload,
            failure: this.failedUpload,
            scope: this
        });
    }
    , successUpload: function(fp, o) {
        //process o.result.file
        this.getEl().mask(String.format(eBook.Pdf.UploadWindow_Processing, o.result.originalFile), 'x-mask-loading');
        Ext.Ajax.request({
                url: eBook.Service.url + 'ProcessNewManualPDF'
                , method: 'POST'
                , params: Ext.encode({ cnpdc: {
                    ManualId: this.manualId,
                    PdfFileName: o.result.file,
                    OriginalName: o.result.originalFile,
                    ReplacementFor: null
                }
                })
                , callback: this.handleFileProcessing
                , scope: this
        });
    }
    , failedUpload: function(fp, o) {
        eBook.Interface.showError(o.message, this.title)
        this.getEl().unmask();
    }
    , handleFileProcessing: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            if (this.parentCaller) this.parentCaller.refreshPdfs();
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , show: function(caller) {
        this.parentCaller = caller;
        eBook.Pdf.ManualUploadWindow.superclass.show.call(this);
        if (this.replacement) {
            this.pdfForm.replaceFor.setValue(this.replacement.get('Id'));
            this.pdfForm.replaceFor.setDisplayValue(this.replacement.get('Name'));
        }
    }
});