
eBook.Journal.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        var its = [];
        var fileClosed = eBook.Interface.currentFile.get('Closed') || (eBook.Interface.serviceYearend && eBook.Interface.serviceYearend.c);
        var tbarCfg = [{
            xtype: 'buttongroup',
            ref: 'importexport',
            id: this.id + '-toolbar-importexport',
            columns: 3,
            title: eBook.Journal.Window_ImportExport,
            items: [
                        {
                            ref: 'importProAcc',
                            text: eBook.Journal.Window_ImportProAcc,
                            iconCls: 'eBook-icon-importproacc-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onImportProAccClick,
                            width: 70,
                            scope: this,
                            disabled: fileClosed ? true : !eBook.Interface.isProAccClient()
                        }, {
                            ref: 'importProAcc',
                            text: eBook.Journal.Window_ImportExcel,
                            iconCls: 'eBook-icon-importexcel-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onImportExcelClick,
                            width: 70,
                            disabled: fileClosed,
                            scope: this
                        }, {
                            ref: 'excel',
                            text: eBook.Journal.Window_ExportExcel,
                            iconCls: 'eBook-icon-exportexcel-24',
                            scale: 'medium',
                            iconAlign: 'top',
                            handler: this.onExportExcelClick,
                            width: 70,
                            scope: this,
                            hidden: true
                        }]
        }];

        switch (this.journal) {
            case 'FINAL':
                tbarCfg = [{
                    ref: 'excel',
                    text: eBook.Journal.Window_ExportExcel,
                    iconCls: 'eBook-icon-exportexcel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onExportExcelClick,
                    width: 70,
                    scope: this
                }];
            default:
                tbarCfg = [{
                    ref: 'addbooking',
                    text: eBook.Journal.Window_AddBooking,
                    iconCls: 'eBook-icon-addbooking-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onAddBookingClick,
                    disabled: fileClosed,
                    width: 70,
                    scope: this
                }, {
                    ref: 'startexportproacc',
                    text: 'Start export ProAcc',
                    iconCls: 'eBook-icon-exportexcel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    enableToggle: true,
                    toggleHandler: this.onToggleExportProaccClick,
                    hidden: false,//!eBook.Interface.isProAccClient(),
                    width: 70,
                    scope: this
                }, {
                    ref: 'exportproacc',
                    text: eBook.Accounts.Mappings.GridPanel_ExportProAccAdjustments,
                    iconCls: 'eBook-icon-exportexcel-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onExportProaccClick,
                    width: 70,
                    hidden: true,
                    scope: this
                }
                        ];
                its.push({ xtype: 'eBook.Journal.GroupedGrid'
                            , journal: 'ALL'
                            , ref: 'grid'
                            , launchedFromSchema: this.LaunchedFromSchema
                            , bookingId: this.bookingId
                });
                break;

        }

        Ext.apply(this, {
            items: its
                , layout: 'fit'
                , tbar: new Ext.Toolbar({ items: tbarCfg })
        });
        eBook.Journal.Window.superclass.initComponent.apply(this, arguments);
    }
    , onAddBookingClick: function () {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var wn = new eBook.Journal.Booking.Window({});
        wn.show(this.grid);
    }
    , show: function () {
        eBook.Journal.Window.superclass.show.call(this);
        this.grid.store.load();
    }
    , onImportProAccClick: function () {
        if (eBook.Interface.currentFile.get('Closed')) return;
        eBook.Interface.showProAccImport(this);
    }
    , onImportExcelClick: function () {
        if (eBook.Interface.currentFile.get('Closed')) return;
        eBook.Interface.showExcelImport(this);
    }
    , onExportExcelClick: function () {
        if (this.journal == "FINAL") {
            this.getEl().mask(eBook.Journal.Window_ExcelExporting, 'x-mask-loading');
            eBook.CachedAjax.request({
                url: eBook.Service.url + 'ExportFinalTrial'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.Culture
                }
                })
                , callback: this.onExportExcelFinalComplete
                , scope: this
            });
        }
    }
    , onExportExcelFinalComplete: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open("pickup/" + robj.ExportFinalTrialResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onToggleExportProaccClick: function (btn, state) {
        this.grid.setExportModus(state);
    }
    , onExportProaccClick: function () {
        var bids = this.grid.getChecked();
        if (bids.length == 0) {
            Ext.Msg.alert('Export ProAcc', 'No bookings selected');
        } else {
            this.getEl().mask('Creating export file for ProAcc', 'x-mask-loading');
            eBook.CachedAjax.request({
                url: eBook.Service.output + 'ExportExactBookings'
                , method: 'POST'
                , params: Ext.encode({ cebdc: {
                    FileId: eBook.Interface.currentFile.get('Id')
                        , BookingIds: bids
                        , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onExportProaccCallback
                , scope: this
            });
        }
    }
    , onExportProaccCallback: function (opts, success, resp) {
        this.getEl().unmask();
        var tb = this.getTopToolbar();
        tb.startexportproacc.toggle();
        if (!success) {
            eBook.Interface.showResponseError(resp, this.title);
        } else {
            var robj = Ext.decode(resp.responseText);
            window.open("pickup/" + robj.ExportExactBookingsResult);
        }
        this.grid.store.load();
        //this.grid.setExportModus(false);
    }
    , importDone: function () {
        this.grid.store.load();
    }
    , hasChanges: false
    , onBeforeClose: function () {
        if (this.hasChanges) eBook.Interface.center.fileMenu.updateWorksheets();
    }
});
