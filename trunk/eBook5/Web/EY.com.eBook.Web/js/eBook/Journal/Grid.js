
// Normal grid, viewing ONE booking.
// Used for single booking "journals" like eBook opening & state at end last year.

eBook.Journal.FTBGrid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {

       var storeCfg = {
                    selectAction: 'GetFinalTrialBalance',
                    fields: eBook.data.RecordTypes.FinalTrialBalance,
                    idField: 'InternalAccountNr',
                    enableFileCached: true,
                    autoDestroy: true,
                    criteriaParameter: 'cicdc',
                    serviceUrl:eBook.Service.schema,
                    baseParams: {
                        Id: eBook.Interface.currentFile.get('Id')
                             , Culture: eBook.Interface.Culture
                    }
                };
        var cols = [{
                            header: eBook.Journal.Grid_Account,
                            dataIndex: 'Account',
                            align: 'left',
                            renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                                if (value != null) {
                                    return value[eBook.Interface.Culture.substr(0, 2)];
                                } else {
                                    return null;
                                }
                            },
                            summaryType: 'count',
                            summaryRenderer: function(v, params, data) {
                                return ((v === 0 || v > 1) ? '(' + v + ' Lines)' : '(1 Line)');
                            },
                            hideable: false,
                            sortable: true
                        }, {
                            header: eBook.Journal.Grid_Start,
                            width: 140,
                            dataIndex: 'StartBalance',
                            renderer: Ext.util.Format.euroMoney,
                            summaryType: 'sum',
                            align: 'right',
                            fixed: true,
                            hideable: false,
                            sortable: true
                        }, {
                            header: eBook.Journal.Grid_AdjustmentsDebet,
                            width: 140,
                            dataIndex: 'AdjustmentsDebet',
                            renderer: Ext.util.Format.euroMoneyJournal,
                            summaryType: 'sum',
                            align: 'right',
                            fixed: true,
                            hideable: false,
                            sortable: true
                        }, {
                            header: eBook.Journal.Grid_AdjustmentsCredit,
                            width: 140,
                            dataIndex: 'AdjustmentsCredit',
                            renderer: Ext.util.Format.euroMoneyJournal,
                            summaryType: 'sum',
                            align: 'right',
                            fixed: true,
                            hideable: false,
                            sortable: true
                        }, {
                            header: eBook.Journal.Grid_End,
                            width: 140,
                            dataIndex: 'EndBalance',
                            renderer: Ext.util.Format.euroMoney,
                            summaryType: 'sum',
                            align: 'right',
                            fixed: true,
                            hideable: false,
                            sortable: true
}];
                        
        Ext.apply(this, {
            store: new eBook.data.JsonStore(storeCfg)
            , plugins: [new Ext.ux.grid.GridSummary()]
            , loadMask: true
            , columnLines: true
            , columns: cols
            , viewConfig: {
                forceFit: true
            }
        });
        eBook.Journal.FTBGrid.superclass.initComponent.apply(this, arguments);
    }

});

Ext.reg('eBook.Journal.FTBGrid', eBook.Journal.FTBGrid);


eBook.Journal.FTBWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        var its = [];
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        var tbarCfg = [{
            ref: 'excel',
            text: eBook.Journal.Window_ExportExcel,
            iconCls: 'eBook-icon-exportexcel-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onExportExcelClick,
            width: 70,
            scope: this
}];
            Ext.apply(this, {
                items: [{ xtype: 'eBook.Journal.FTBGrid', ref: 'grid'}]
                , layout: 'fit'
                , tbar: new Ext.Toolbar({ items: tbarCfg })
            });
            eBook.Journal.FTBWindow.superclass.initComponent.apply(this, arguments);
        }
    , show: function() {
        eBook.Journal.FTBWindow.superclass.show.call(this);
        this.grid.store.load();
    }

    , onExportExcelClick: function() {

        this.getEl().mask(eBook.Journal.Window_ExcelExporting, 'x-mask-loading');
        eBook.CachedAjax.request({
                url: eBook.Service.output + 'ExportFinalTrial'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: eBook.Interface.Culture
                }
                })
                , callback: this.onExportExcelFinalComplete
                , scope: this
        });
    }
    , onExportExcelFinalComplete: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open("pickup/" + robj.ExportFinalTrialResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    });
