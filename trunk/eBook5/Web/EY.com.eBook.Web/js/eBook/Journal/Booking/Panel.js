

eBook.Journal.Booking.Panel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            items: [{
                        xtype:'hidden',
                        name:'bookingId',
                        ref:'bookingId'
                    },
                    { 
                        xtype:'numberfield'
                        ,hidden:true
                        ,name:'bookingNr'
                        ,ref:'bookingNr'
                    },
                    {
                        xtype:'textfield'
                        , fieldLabel: eBook.Journal.Booking.Panel_Desc
                        ,name:'bookingDescription'
                        , ref: 'bookingDescription'
                        ,width:350
                    },
                    {   
                        xtype:'checkbox'
                        ,fieldLabel:eBook.Journal.Booking.Panel_HideClients //'Hide booking for clients'
                        ,name:'bookingHidden'
                        ,ref:'bookingHidden'
                    }]
            ,bodyStyle:'padding:5px'
            , layout: 'form'
            , layoutConfig: { labelWidth: 260 }
        });
        eBook.Journal.Booking.Panel.superclass.initComponent.apply(this, arguments);
    }
    , loadBooking: function(rec) {
        this.bookingId.setValue(rec.get('BookingId'));
        this.bookingNr.setValue(rec.get('Nr'));
        this.bookingDescription.setValue(rec.get('BookingDescription'));
        this.bookingHidden.setValue(rec.get('Hidden'));
    }
    , getData: function() {
        return {
            bi: this.bookingId.getValue() == "" ? null : this.bookingId.getValue()
            , bid: this.bookingId.getValue() == "" ? null : this.bookingId.getValue()
            , bnr: this.bookingNr.getValue() == "" ? null : this.bookingNr.getValue()
            , bd: this.bookingDescription.getValue()
            , bh: this.bookingHidden.getValue()
            , bwt: null
            , bwk: null
            , bjt:this.refOwner.caller.journal == 'LASTYEAR' ? -1 : 0
        };
    }
});