

eBook.Journal.Booking.Grid = Ext.extend(Ext.grid.EditorGridPanel, {
    emptyRows: 20
    , initComponent: function () {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        this.actions = new Ext.ux.grid.RowActions({
            header: 'Actions'
                , align: 'left'
                , widthSlope: 30
                , keepSelection: true
                , actions: fileClosed ? [] : [{ iconCls: 'eBook-grid-row-delete-ico', tooltip: eBook.Journal.Booking.Grid_DeleteLineTip}]
        });

        this.actions.on({ action: this.onAction });

        Ext.apply(this, {
            store: new eBook.data.BaseJsonStore({
                root: 'data'
                , fields: eBook.data.RecordTypes.BookingLines
                , autoDestroy: true
                , autoSave: false

            })
            , plugins: [new Ext.ux.grid.GridSummary(), this.actions]
            , columnLines: true
            , columns: [{
                xtype: 'accountEditorColumn',
                header: eBook.Journal.Booking.Grid_AccountNr,
                dataIndex: 'Account',
                align: 'left',
                summaryType: 'count',
                editor: new eBook.Journal.Booking.AccountDropdown(),
                summaryRenderer: function (v, params, data) {
                    return ((v === 0 || v > 1) ? '(' + v + ' Lines)' : '(1 Line)');
                }
                , width: 300
                , renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    var acc = record.get('Account');
                    if (Ext.isObject(acc)) return acc[eBook.Interface.Culture.substr(0, 2)];
                    return acc;
                }
                , hideable: false
                , sortable: true
                , editable: !fileClosed
            }, {
                header: eBook.Journal.Booking.Grid_Debet,
                width: 140,
                dataIndex: 'Debet',
                renderer: Ext.util.Format.euroMoneyJournal,
                summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
                , editable: !fileClosed
                , editor: new Ext.form.NumberField({
                    allowBlank: true,
                    allowNegative: false,
                    allowDecimals: true,
                    decimalPrecision: 2 //,
                    //decimalSeparator: ''
                })
            }, {
                header: eBook.Journal.Booking.Grid_Credit,
                width: 140,
                dataIndex: 'Credit',
                renderer: Ext.util.Format.euroMoneyJournal,
                summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
                , editable: !fileClosed
                , editor: new Ext.form.NumberField({
                    allowBlank: true,
                    allowNegative: false,
                    allowDecimals: true,
                    decimalPrecision: 2//,
                    //decimalSeparator: ','
                })
            }, {
                header: eBook.Journal.Booking.Grid_BusinessRelations,
                width: 140,
                dataIndex: 'ClientSupplierName',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {


                    var re = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
                    
                    var match = re.exec(value);

                    
                    if (match) {
                        var data = this.editor.store.getAt(this.editor.store.find('Id', value)).data;
                        record.set('ClientSupplierId',data.Id);
                        record.set('ClientSupplierName', data.Name);
                        return data.Name;
                    } else {
                        return value;
                    }

                },
                //summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
                , editable: !fileClosed
                , editor: new Ext.form.ComboBox({
                    fieldLabel: eBook.Create.Empty.PreviousFileFieldSet_EbookFile
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , valueField: 'Id'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    //, mode: 'remote'
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetBusinessRelationList'
                            , serviceUrl: eBook.Service.businessrelation
                            , autoLoad: true
                            //, autoDestroy: true
                            , criteriaParameter: 'citdc'
                           , baseParams: {
                               Id: eBook.Interface.currentClient.id
                                , Type: null
                           }
                            , fields: eBook.data.RecordTypes.BusinessRelationList
                           
                        })
                    
                })

            }, {
                header: eBook.Journal.Booking.Grid_Comment,
                width: 300,
                dataIndex: 'Comment',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    metaData.attr = 'ext:qtip="' + value + '"';
                    return value;
                },
                //summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                //, sortable: true
                , editable: !fileClosed
                , editor: new Ext.form.TextField({
                    allowBlank: true
                    //allowNegative: false,
                    //allowDecimals: true,
                    //decimalPrecision: 2 //,
                    //decimalSeparator: ''
                })
            }, this.actions]
        });
        eBook.Journal.Booking.Grid.superclass.initComponent.apply(this, arguments);
    }
    , setEmptyRows: function () {
        var recs = [];
        for (var i = 0; i < this.emptyRows; i++) {
            recs[i] = new this.store.recordType();
        }
        this.store.add(recs);

    }
    , onAction: function (g, r, a, ri, ci) {
        g.store.removeAt(ri);
    }

});