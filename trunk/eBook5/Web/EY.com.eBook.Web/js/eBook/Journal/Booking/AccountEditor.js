if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function(str) {
        return this.indexOf(str) == 0;
    };
}

eBook.Journal.Booking.AccountDropdown = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: 'GetAccountsList'
                , baseParams: {
                    FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                }
                , serviceUrl: eBook.Service.schema
                , criteriaParameter: 'cadc'
                , autoDestroy: true
                , fields: eBook.data.RecordTypes.GlobalListItem
                 , enableFileCached: true
            })
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            ,minChars:1
            , hideTrigger: true
            , typeAhead: false
            , lazyRender: true
            , pageSize: 20
            , listWidth: 300
            ,queryParam:'Query'
            , selectOnFocus: true
        });
        eBook.Journal.Booking.AccountDropdown.superclass.initComponent.apply(this, arguments);
    }
 
});

eBook.Journal.Booking.AccountEditor = Ext.extend(Ext.grid.GridEditor, {
    startEdit: function(el, value) {
        eBook.Journal.Booking.AccountEditor.superclass.startEdit.apply(this, arguments);
        if (!this.record) return;
        this.field.setActiveRecord(value);
        //        this.field.setValue(this.record.get('Account').id);
        //        this.field.setDisplayValue(this.record.get('Account')[eBook.Interface.Culture.substr(0, 2)]);
    }
    , completeEdit: function(remainVisible) {
        // Find a way to load the account description (and full account visiblenr)
        // in the background. (preferably using window status).
        eBook.Journal.Booking.AccountEditor.superclass.completeEdit.apply(this, arguments);
        var raw = this.field.getValue();
        var rec = this.field.getRecord();

        var getRec = rec && !Ext.isEmpty(raw) && rec.data.id.startsWith(raw);

        if (getRec) {
            this.record.set('Account', rec.data);
        } else {
            if (raw == 'undefined') return;
            this.record.set('Account', raw);
            this.getAccountObject(raw, this.record);
        }

    }
    , getAccountObject: function(val, rec) {
        eBook.CachedAjax.request({
            url: eBook.Service.schema + 'GetAccountListItem'
                    , method: 'POST'
                    , params: Ext.encode({ cadc: { FileId: eBook.Interface.currentFile.get('Id'), Culture: eBook.Interface.Culture, InternalNr: val} })
                    , callback: this.onAccountObjectCallback
                    , scope: this
                    , record: rec
        });
    }
    , onAccountObjectCallback: function(opts, success, resp) {
        if (success) {
            var obj = Ext.decode(resp.responseText);
            obj = obj.GetAccountListItemResult;
            opts.record.set('Account', obj);
        }
    }
});
