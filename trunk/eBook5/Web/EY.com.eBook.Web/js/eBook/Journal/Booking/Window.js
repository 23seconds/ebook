

eBook.Journal.Booking.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        Ext.apply(this, {
            items: [new eBook.Journal.Booking.Panel({ region: 'north', height: 80, ref: 'panel' })
                , new eBook.Journal.Booking.Grid({ region: 'center', ref: 'grid' })]
            , layout: 'border'
        });

        if (!fileClosed) {
            Ext.apply(this, {
                tbar: new Ext.Toolbar({
                    items: [{
                        ref: 'save',
                        text: eBook.Journal.Booking.Window_Save,
                        iconCls: 'eBook-businessrelation-add-ico',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onSaveClick,
                        width: 70,
                        scope: this
                    }, {
                        ref: 'addlines',
                        text: eBook.Journal.Booking.Window_AddLines,
                        iconCls: 'eBook-icon-addlines-24 ',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onAddLinesClick,
                        width: 70,
                        scope: this
                    }, {
                        ref: 'importexcel',
                        text: eBook.Journal.Booking.Window_ImportExcel,
                        iconCls: 'eBook-icon-importexcel-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onImportExcelClick,
                        width: 70,
                        scope: this,
                        hidden: true
}]
                    })
                });
            }
            eBook.Journal.Booking.Window.superclass.initComponent.apply(this, arguments);

            this.on('beforeClose', this.onBeforeCloseWindow, this);
        }
    , show: function(caller, recs) {
        this.caller = caller;
        eBook.Journal.Booking.Window.superclass.show.call(this, arguments);
        if (Ext.isDefined(recs)) {
            this.grid.store.removeAll();
            this.grid.store.add(recs);
            this.panel.loadBooking(recs[0]);
        } else {
            this.grid.store.removeAll();
            this.grid.setEmptyRows();

        }
    }
    , action: 'SaveBooking'
    , onSaveClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.caller.refOwner.hasChanges = true;
        var booking = this.panel.getData();
        var bookingLines = this.grid.store.getJsonRecords(booking, function(rec) {
            return rec.get('Account') != null && rec.get('Account').id != null && !Ext.isEmpty(rec.get('Account').id);
        });
        this.getEl().mask(String.format(eBook.Journal.Booking.Window_Saving, bookingLines[0].bd), 'x-mask-loading');

        var d = Math.abs(Math.round(this.grid.store.sum('Debet') * 100) / 100);
        var c = Math.abs(Math.round(this.grid.store.sum('Credit') * 100) / 100);

        if (d != c) { //'Debet {0} is not equal to credit {1}'
            alert(String.format(eBook.Journal.Booking.Window_DebetCredit, d, c));
            this.getEl().unmask();
            return;
        }

        this.activeId = bookingLines[0].bid;

        eBook.CachedAjax.request({
            url: eBook.Service.schema + 'SaveBooking'
                        , method: 'POST'
                        , params: Ext.encode({
                            csbdc: {
                        FileId: eBook.Interface.currentFile.get('Id')
                            , Lines: bookingLines
                            , Person: eBook.User.getActivePersonDataContract()
                    }
                })
                , callback: this.onSaveBookingResponse
                , scope: this
        });

    }
    , onSaveBookingResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                var rc = robj[this.action + 'Result'];
                this.activeId = rc.id;
                this.caller.store.addListener("load", this.onReloaded, this, { single: true });
                this.caller.store.reload();

            } else {
                eBook.Interface.showResponseError(resp, this.title);
                this.getEl().unmask();
            }
        } catch (e) {
            console.log(e);
            this.getEl().unmask();
        }

    }
    , onReloaded: function() {
        var cl = this.caller.store.query("BookingId", this.activeId);
        this.grid.store.removeAll();
        this.grid.store.add(cl.items);
        this.panel.loadBooking(cl.first());
        this.getEl().unmask();
    }
    , onImportExcelClick: function() {
        // alert('import a booking from excel, using excel mapping tool = TODO');
    }
    , onAddLinesClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.grid.setEmptyRows();
    }
    , onBeforeCloseWindow: function() {
        // add check save changes?
        if (this.caller && this.caller.store) this.caller.store.reload();
        return true;
    }
    });