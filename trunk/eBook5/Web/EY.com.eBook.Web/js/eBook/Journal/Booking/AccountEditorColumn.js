
eBook.Journal.Booking.AccountEditorColumn = Ext.extend(Ext.grid.Column, {

    constructor: function(cfg) {
        eBook.Journal.Booking.AccountEditorColumn.superclass.constructor.call(this, cfg);
        //this.renderer =
    }
    ,getCellEditor: function(rowIndex){
        var ed = this.getEditor(rowIndex);
        if(ed){
            if(!ed.startEdit){
                if(!ed.gridEditor){
                    ed.gridEditor = new eBook.Journal.Booking.AccountEditor(ed);
                }
                ed = ed.gridEditor;
            }
        }
        return ed;
    }
});
Ext.grid.Column.types.accountEditorColumn = eBook.Journal.Booking.AccountEditorColumn