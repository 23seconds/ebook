
// Grid containing bookings and their booking lines, grouped by booking
// Used for manual and automatic bookings.

eBook.Journal.GroupingView = Ext.extend(Ext.grid.GroupingView, {
    initTemplates : function(){
        Ext.grid.GroupingView.superclass.initTemplates.call(this);
        this.state = {};

        var sm = this.grid.getSelectionModel();
        sm.on(sm.selectRow ? 'beforerowselect' : 'beforecellselect',
                this.onBeforeRowSelect, this);

        if(!this.startGroup){
            this.startGroup = new Ext.XTemplate(
                '<div id="{groupId}" class="x-grid-group {cls} {[this.isAuto(values)]}">',
                    '<div id="{[values.rs[0].get(\'BookingId\')]}" class="x-grid-group-hd" style="{style}"><div class="x-grid-group-title">', this.groupTextTpl, '</div></div>',
                    '<div id="{groupId}-bd" class="x-grid-group-body">'
                , {
                    isAuto: function(values) {
                        var record = values.rs[0];
                        if (!Ext.isEmpty(record.get('WorksheetTypeId'))) {
                            return 'eBook-auto-booking';
                        }
                        return '';
                    }
            });
        }
        this.startGroup.compile();

        if (!this.endGroup) {
            this.endGroup = '</div></div>';
        }
    }
});


eBook.Journal.GroupedGrid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function () {
        var gActions = [{
            iconCls: 'eBook-grid-row-edit-ico'
                , tooltip: eBook.Journal.GroupedGrid_EditBooking
        }];

        gActions.push({
            iconCls: 'eBook-grid-row-delete-ico'
                , tooltip: eBook.Journal.GroupedGrid_DeleteBooking
        });
        this.actions = new Ext.ux.grid.RowActions({
            header: 'Actions'
        , align: 'left'
        , keepSelection: true
        , actions: []
        , groupActions: (eBook.Interface.currentFile.get('Closed')) ? [] : gActions
        });

        this.actions.on({ groupaction: this.onGroupAction });

        if (this.launchedFromSchema) {
            var storeCfg = {
                selectAction: 'GetSchemaBooking',
                fields: eBook.data.RecordTypes.BookingLines,
                idField: 'Id',
                autoDestroy: true,
                criteriaParameter: 'cidc',
                serviceUrl: eBook.Service.schema,
                baseParams: {
                    Id: this.bookingId
                 , Culture: eBook.Interface.Culture
                }
                , enableFileCached: true
                , groupField: 'GroupNr'
                //, groupField:'BookingId'
            };
        } else {
            var storeCfg = {
                selectAction: 'GetAllBookings',
                fields: eBook.data.RecordTypes.BookingLines,
                idField: 'Id',
                autoDestroy: true,
                criteriaParameter: 'cfdc',
                serviceUrl: eBook.Service.schema,
                baseParams: {
                    FileId: eBook.Interface.currentFile.get('Id')
                 , Culture: eBook.Interface.Culture
                }
                , enableFileCached: true
                , groupField: 'GroupNr'
                //, groupField:'BookingId'
            };
        }
        

        

        Ext.apply(this, {
            store: new eBook.data.GroupedJsonStore(storeCfg)
            , plugins: [new Ext.ux.grid.GridSummary(), new Ext.ux.grid.GroupSummary(), this.actions]
            , loadMask: true
            , columnLines: true
            , columns: [{
                header: eBook.Journal.GroupedGrid_Booking,
                dataIndex: 'GroupNr',
                align: 'left',
                hidden: true,
                groupRenderer: function (v, unused, r, ridx, cidx, ds) {
                    var desc = r.get('BookingDescription');
                    if (!Ext.isEmpty(r.get('WorksheetTypeId'))) {
                        var wrec = eBook.Interface.center.fileMenu.getWorksheetRecord(r.get('WorksheetTypeId'));
                        if (wrec) {
                            desc = wrec.get('name') + ' ' + desc;
                        }
                    }
                    var exp = '';
                    if (r.get('ExportCount')) exp = '<span class="eb-journal-exported-msg">Already exported [' + r.get('ExportCount') + ' time(s)] to ProAcc</span>';

                    return String.format("{0} - {1} {2}", r.get('GroupNr'), desc, exp);
                }
                , hideable: false
            }, {
                header: eBook.Journal.GroupedGrid_Account,
                dataIndex: 'Account',
                align: 'left',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    if (value != null) {
                        return value[eBook.Interface.Culture.substr(0, 2)];
                    } else {
                        return null;
                    }
                },
                summaryType: 'count',
                summaryRenderer: function (v, params, data) {
                    return ((v === 0 || v > 1) ? '(' + v + ' Lines)' : '(1 Line)');
                }
                , hideable: false
                , sortable: true
                , width: 200
            }, {
                header: eBook.Journal.GroupedGrid_Debet,
                width: 140,
                dataIndex: 'Debet',
                renderer: Ext.util.Format.euroMoneyJournal,
                summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
            }, {
                header: eBook.Journal.GroupedGrid_Credit,
                width: 140,
                dataIndex: 'Credit',
                renderer: Ext.util.Format.euroMoneyJournal,
                summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
            }, {
                header: eBook.Journal.GroupedGrid_BusinessRelation,
                width: 140,
                dataIndex: 'ClientSupplierName',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    return value;
                },
                //summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
            }, {
                header: eBook.Journal.GroupedGrid_Comment,
                width: 300,
                dataIndex: 'Comment',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    metaData.attr = 'ext:qtip="' + value + '"';
                    return value;
                },
                //summaryType: 'sum',
                align: 'right',
                fixed: true
                , hideable: false
                , sortable: true
            }, this.actions
                ]
            , enableHdMenu: true
            , view: new eBook.Journal.GroupingView({
                forceFit: true
                , groupTextTpl: '<input type="checkbox" value="{[values.rs[0].get(\'BookingId\')]}" class="eb-journal-select"/> {group}'
                , showGroupName: false
                , enableGrouping: true
                , enableGroupingMenu: false
                , enableNoGroups: false
                , hideGroupedColumn: true
                // , groupMode: 'display'

            })
        });
        eBook.Journal.GroupedGrid.superclass.initComponent.apply(this, arguments);
    }
    , onGroupAction: function (grid, records, action, groupId) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        if (eBook.Interface.currentFile.get('NumbersLocked')) return;

        //console.log(groupId);
        //console.log(records);
        //console.log(grid.getStore());
        //records = grid.getStore().query('Nr', groupId.replace('Nr-',''), false).getRange();
        //records =

        if (!records) return;
        if (records.length == 0) return;
        var rec = records[0];
        if (!Ext.isEmpty(rec.get('WorksheetTypeId'))) {
            eBook.Interface.center.fileMenu.openWorksheetSh(rec.get('WorksheetTypeId'), rec.get('WorksheetCollection'), rec.get('WorksheetRowId'));
            return;
        }

        switch (action) {
            case 'eBook-grid-row-edit-ico':
                //if (this.grid.journal == 'MANUAL' || this.grid.journal == 'LASTYEAR') {
                var wn = new eBook.Journal.Booking.Window({});
                wn.show(grid, records);
                //}
                break;
            case 'eBook-grid-row-delete-ico':
                Ext.Msg.confirm(eBook.Journal.GroupedGrid_DeleteBookingTitle
                    , String.format(eBook.Journal.GroupedGrid_DeleteBookingMsg, records[0].get('BookingDescription'))
                    , function (b) { if (b == 'yes') { this.onDeleteBookingPerform(records); } }
                    , grid);

                break;
        }
    }
    , onEditBookingClick: function () {
        //alert('edit booking');
    }
    , onDeleteBookingPerform: function (records) {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var record = records[0];
        var deleteAction = {
            text: String.format(eBook.Journal.GroupedGrid_DeletingBooking, record.get('BookingDescription'))
            , params: { id: record.get('BookingId') }
            , action: 'DeleteBooking'
            , url: eBook.Service.schema
        };
        this.store.remove(records);
        this.refOwner.addAction(deleteAction);
        this.refOwner.hasChanges = true;

    }
    , onPerformDeleteBooking: function () {
        alert('performing delete');
    }
    , getChecked: function () {
        var inputs = this.getEl().query('.eb-journal-select'), i = 0, bids = [];
        for (i = 0; i < inputs.length; i++) {
            if (inputs[i].checked) bids.push(inputs[i].value);
        }
        return bids;
    }
    , setExportModus: function (onOrOff) {
        var tb = this.ownerCt.getTopToolbar();
        if (onOrOff) {
            tb.exportproacc.show();
            tb.addbooking.disable();
            this.getEl().addClass('eb-journal-export');
            var its = this.store.data.items, i = 0, col = {};
            for (i = 0; i < its.length; i++) {
                if (!col['I' + its[i].data.BookingId]) {
                    col['I' + its[i].data.BookingId] = true;
                    var group = Ext.get(its[i].data.BookingId);
                    if (its[i].data.ExportCount == 0) {
                        group.child('.eb-journal-select').dom.checked = true;
                    } else {
                        group.child('.eb-journal-select').dom.checked = false;
                    }
                }
            }
            Ext.Msg.alert('Export ProAcc', "Select all bookings you wish to export to ProAcc and perform the export.");
        } else {
            tb.exportproacc.hide();
            tb.addbooking.enable();
            this.getEl().removeClass('eb-journal-export');
        }
    }
});

Ext.reg("eBook.Journal.GroupedGrid", eBook.Journal.GroupedGrid);