
eBook.Yearend.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        Ext.apply(this, {
            width: 1000,
            layout: 'card',
            activeItem: 0,
            title: 'Yearend',
            items: [{ xtype: 'infopanel', ref: 'infoPanel', bbar: ['->', {
                ref: 'btnReject',
                text: "Close",
                iconCls: 'eBook-no-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: function () { this.close() },
                //disabled: eBook.Interface.isFileClosed(),
                scope: this
            }]
            },
                    { xtype: 'RepositoryPreview', ref: 'previewPanel', bbar: [{
                        ref: 'btnReject',
                        text: "Reject",
                        iconCls: 'eBook-no-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onRejectClick,
                        //disabled: eBook.Interface.isFileClosed(),
                        scope: this
                    }, '->', {
                        ref: 'btnAccept',
                        text: "Accept",
                        iconCls: 'eBook-yes-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onAcceptClick,
                        scope: this
                    }, {
                        ref: 'btnClose',
                        text: "Close",
                        iconCls: 'eBook-no-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        hidden: true,
                        handler: function () { this.close() },
                        //disabled: eBook.Interface.isFileClosed(),
                        scope: this
                    }]
                    }]

        });
        eBook.Yearend.Window.superclass.initComponent.apply(this, arguments);
        //eBook.Yearend.Window.superclass.constructor.call(this, config);
    }
    , show: function (serviceYearendStatus) {
        eBook.Yearend.Window.superclass.show.call(this);
        this.onStartup(serviceYearendStatus);
        //this.previewPanel.loadFile("upload/" + fobj.id + '.pdf');
    }
    , onAcceptClick: function (btn) {
        if (!this.hardclose) {
            this.layout.setActiveItem(0)
            Ext.Msg.show({
                title: 'Closing yearend service',
                msg: 'Are you sure you wish to (soft)close the numbers and workpapers?',
                buttons: Ext.Msg.YESNO,
                fn: this.softCloseService,
                icon: Ext.MessageBox.WARNING,
                scope: this
            });
        } else {
            this.layout.setActiveItem(0)
            Ext.Msg.show({
                title: 'Closing yearend service',
                msg: 'Are you sure you wish to (HARD)close the numbers and workpapers?',
                buttons: Ext.Msg.YESNO,
                fn: this.performHardcloseService,
                icon: Ext.MessageBox.WARNING,
                scope: this
            });
        }
    }
    , performHardcloseService: function (btnid, txt, opts) {
        if (btnid == 'yes') {
            eBook.Splash.setText("(HARD)Closing service...");
            eBook.Splash.show();
            Ext.Ajax.request({
                url: eBook.Service.file + 'CloseService'
                , method: 'POST'
                , jsonData: { cfspdc: { FileId: eBook.Interface.currentFile.id,
                    ServiceId: 'C3B8C8DB-3822-4263-9098-541FAE897D02',
                    Status: 1,
                    Person: eBook.User.getActivePersonDataContract()
                }
                }
                , callback: this.onCloseServiceCallback
                , scope: this
            });
        } else {
            this.layout.setActiveItem(1);
        }
    }

    , softCloseService: function (btnid, txt, opts) {
        if (btnid == 'yes') {
            eBook.Splash.setText("(soft)Closing service...");
            eBook.Splash.show();
            Ext.Ajax.request({
                url: eBook.Service.file + 'CloseService'
                , method: 'POST'
                , jsonData: { cfspdc: { FileId: eBook.Interface.currentFile.id,
                    ServiceId: 'C3B8C8DB-3822-4263-9098-541FAE897D02',
                    Status: 0,
                    Person: eBook.User.getActivePersonDataContract()
                }
                }
                , callback: this.onCloseServiceCallback
                , scope: this
            });
        } else {
            this.layout.setActiveItem(1);
        }
    }
    , onCloseServiceCallback: function (opts, success, resp) {
        if (success) {
            eBook.Splash.hide();
            eBook.CachedAjax.request({
                url: eBook.Service.bundle + 'GenerateBundleById'
                , method: 'POST'
                , params: Ext.encode({ id: this.bundleId })
                , callback: function () {
                    this.layout.setActiveItem(1)
                    this.infoPanel.getEl().unmask();
                    this.previewPdf("Repository/" + eBook.getGuidPath(eBook.Interface.currentClient.id) + this.bundleId + ".pdf", "CLOSE");
                    eBook.Interface.openFile(eBook.Interface.currentFile.get('Id'), false);
                }
                , scope: this
                , timeout: 1000 * 120
            });

        }
    },
    hardCloseService: function (btnid, txt, opts) {
        if (btnid == 'yes') {
            // HARDCLOSE
            this.hardclose = true;
            eBook.Splash.setText("Preparing to (hard)Close service...");
            eBook.Splash.show();
            // bundleservice -> CheckSignedRepos cidc { id:''}
            // result = lege array of null => GO FURTHER
            // result = array of X strings => ERROR, list = list of documentnames that are unsigned
            Ext.Ajax.request({
                url: eBook.Service.bundle + 'CheckSignedRepos'
                    , method: 'POST'
                    , jsonData: { cidc: { Id: eBook.Interface.currentFile.id} }
                    , callback: function (opts, success, resp) {
                        if (success) {
                            var obj = Ext.decode(resp.responseText).CheckSignedReposResult;
                            if (obj == null || obj.length == 0) {
                                eBook.CachedAjax.request({
                                    url: eBook.Service.bundle + 'GenerateBundleById'
                                    , method: 'POST'
                                    , params: Ext.encode({ id: this.bundleId })
                                    , callback: function () {
                                        eBook.Splash.hide();
                                        this.infoPanel.getEl().unmask();
                                        this.previewPdf("Repository/" + eBook.getGuidPath(eBook.Interface.currentClient.id) + this.bundleId + ".pdf", "ACCEPT");
                                    }
                                    , scope: this
                                    , timeout: 1000 * 120
                                });
                            } else {
                                // SHOW LIST DOCS
                                eBook.Splash.hide();
                                this.infoPanel.getEl().unmask();
                                this.infoPanel.updateMe({ Input: 'Following documents are required to have a signed status:', List: obj });
                            }
                        } else {
                            //ERROR
                        }
                    }
                    , scope: this
            });

        } else if (btnid == 'no') {
            // UNDO SOFTCLOSE
            Ext.Ajax.request({
                url: eBook.Service.file + 'ReOpenService'
                , method: 'POST'
                , jsonData: { cfspdc: { FileId: eBook.Interface.currentFile.id,
                    ServiceId: 'C3B8C8DB-3822-4263-9098-541FAE897D02',
                    Status: null,
                    Person: eBook.User.getActivePersonDataContract()
                }
                }
                , callback: this.onReOpenServiceCallback
                , scope: this
            });
        } else if (btnid == 'cancel') {
            this.close();
        }
    }
    , onReOpenServiceCallback: function (opts, success, resp) {
        if (success) {
            eBook.Splash.hide();
            this.layout.setActiveItem(0);
            this.infoPanel.updateMe({ Input: 'The (soft)close of the service has been undone.' });
            eBook.Interface.openFile(eBook.Interface.currentFile.get('Id'), false);
        }
    }
    , onRejectClick: function (btn) {
        this.close();
    }
    , previewPdf: function (url, manor) {
        this.layout.setActiveItem(1);
        var bbar = this.previewPanel.getBottomToolbar();
        bbar.btnReject.hide();
        bbar.btnAccept.hide();
        bbar.btnClose.hide();
        switch (manor) {
            case "ACCEPT":
                bbar.btnReject.show();
                bbar.btnAccept.show();
                break;
            case "CLOSE":
                bbar.btnClose.show();
                break;
        }
        this.previewPanel.loadFile(url);
    }
    , onStartup: function (serviceYearendStatus) {
        var me = this;

        me.infoPanel.getEl().mask('Loading');
        if (serviceYearendStatus == null || serviceYearendStatus == 0) {

            Ext.Ajax.request({
                url: eBook.Service.bundle + 'CheckIfBundleExists'
                    , method: 'POST'
                    , jsonData: { cbtdc: { FileId: eBook.Interface.currentFile.id, TemplateIds: ['43C27DBE-AE64-4208-BD57-060553D41EC8', 'C29A94BF-BB06-466D-9054-5B2C632E1D97']} }
                    , callback: function (opts, success, resp) {
                        if (success) {
                            var me = this,
                                obj = Ext.decode(resp.responseText).CheckIfBundleExistsResult;
                            switch (obj.Count) {
                                case 0:
                                    me.infoPanel.getEl().unmask();
                                    me.infoPanel.updateMe({ Input: 'No yearend bundle found.' });
                                    break;
                                case 1:
                                    me.bundleId = obj.BundelId;
                                    if (!obj.AccountingMemorandum && !obj.RepresentationLetter) {
                                        me.infoPanel.getEl().unmask();
                                        me.infoPanel.updateMe({ Input: 'Both accounting memorandum and representation letter are missing. Please create them in the document builder' });
                                    } else if (!obj.AccountingMemorandum && obj.RepresentationLetter) {
                                        me.infoPanel.getEl().unmask();
                                        me.infoPanel.updateMe({ Input: 'There is no accounting memorandum. Please create one in the document builder' });
                                    } else if (obj.AccountingMemorandum && !obj.RepresentationLetter) {
                                        me.infoPanel.getEl().unmask();
                                        me.infoPanel.updateMe({ Input: 'There is no representation letter. Please create one in the document builder' });
                                    } else if (obj.AccountingMemorandum && obj.RepresentationLetter) {
                                        if (serviceYearendStatus == null) {
                                            eBook.CachedAjax.request({
                                                url: eBook.Service.bundle + 'GenerateBundleById'
                                                , method: 'POST'
                                                , params: Ext.encode({ id: obj.BundelId })
                                                , callback: function () {
                                                    me.infoPanel.getEl().unmask();
                                                    me.previewPdf("Repository/" + eBook.getGuidPath(eBook.Interface.currentClient.id) + obj.BundelId + ".pdf", "ACCEPT");
                                                }
                                                , scope: me
                                                , timeout: 1000 * 120
                                            });
                                        } else if (serviceYearendStatus == 0) {
                                            me.infoPanel.getEl().unmask();
                                            Ext.Msg.show({
                                                title: 'Closing yearend service',
                                                msg: "Click 'Yes' to perform the final closure or click 'No' if you want to undo the (soft)closure.",
                                                buttons: Ext.Msg.YESNOCANCEL,
                                                fn: me.hardCloseService,
                                                icon: Ext.MessageBox.WARNING,
                                                scope: me
                                            });
                                        }
                                    }
                                    break;
                                default:
                                    me.infoPanel.getEl().unmask();
                                    me.infoPanel.updateMe({ Input: 'Multiple yearend bundles found. There can only be one yearend bundle. Please correct in order to continue' });
                                    break;
                            }

                        } else {
                            me.infoPanel.getEl().unmask();
                            me.infoPanel.updateMe({ Input: 'Something went wrong. Please contact eBook Questions' });
                        }
                    }
                    , scope: me
            });
        }
    }

});

eBook.Yearend.InfoPanel = Ext.extend(Ext.Panel, {
    mainTpl: new Ext.XTemplate('<div class="eBook-yearend-info-input">{Input}</div><div></div><div class="eBook-yearend-info-list"><ul><tpl for="List"><li>{.}</li></tpl></ul></div>', { compiled: true })
    , initComponent: function() {

       
        eBook.Yearend.InfoPanel.superclass.initComponent.apply(this, arguments);
    }
    , updateMe: function(rec) {
        if(!rec.List) rec.List = [];
        this.body.update(this.mainTpl.apply(rec));
    }
});
Ext.reg('infopanel', eBook.Yearend.InfoPanel);

