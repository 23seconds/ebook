
eBook.Recycle.IconContextMenu_Empty

eBook.Recycle.IconContextMenu = Ext.extend(Ext.menu.Menu, {
    listId: null
    , initComponent: function() {
        Ext.apply(this, {
            items: [{
                iconCls: 'eBook-recycle-menu-context-icon',
                text: eBook.Recycle.IconContextMenu_Empty,
                scale: 'medium',
                scope: this,
                handler: this.emptyBin
                }]
            });
            eBook.Recycle.IconContextMenu.superclass.initComponent.apply(this, arguments);
        }
    , emptyBin: function() {
        this.hide();
        var lst = Ext.getCmp(this.listId);
        var ids = [];
        lst.store.each(function(rec) {
            ids.push(rec.get('Id'));
        }, this);
        Ext.Ajax.request({
            url: eBook.Service.url + 'DeleteMarkedFiles'
            , method: 'POST'
            , params: Ext.encode({ cicdc: { Id: eBook.CurrentClient} })
            , success: this.onBatchFileDeleteSuccess
            , failure: this.onBatchFileDeleteFailure
            , scope: this
        });
        eBook.LoadOverlay.showFor(Ext.get('eBook-Client-files-deleted-icon'), [-30, -30]);

        //this.onBatchFileDeleteSuccess.defer(5000, this);
    }
    , onBatchFileDeleteSuccess: function(resp, opts, action) {
        var res = Ext.decode(resp.responseText).DeleteMarkedFilesResult;
        if (res) {
            Ext.getCmp(this.listId).store.reload();
        }

        //
        //alert("done");
        eBook.LoadOverlay.hide();
    }
    , onBatchFileDeleteFailure: function() {
        eBook.LoadOverlay.hide();
        //alert("batch deletion failed");
    }
});
