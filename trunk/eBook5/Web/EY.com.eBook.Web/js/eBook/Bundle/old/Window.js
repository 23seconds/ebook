

/*
Object tree nodes have a unique identifier, keeps a link between an item in the container panel 
and its offset in the bundle panel. (check existance)

Report settings
activate headers
activate footers: footnote, page

*/

eBook.Bundle.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        this.renderMenu();
        Ext.apply(this, {
            title: eBook.Bundle.Window_Title
            , layout: 'card'
            , iconCls: 'eBook-Bundle-icon-16'
            , activeItem: 0
            , items: [{
                html: ''
                        , bodyStyle: 'background-color:#ABABAB;'
            }, {
                xtype: 'eBook.Bundle.BundlePanel'
                        , ref: 'bundleContainer'
}]
            , tbar: {
                xtype: 'toolbar',
                items: [{
                    xtype: 'buttongroup',
                    ref: 'generalActions',
                    id: this.id + '-toolbar-general',
                    columns: 6,
                    title: eBook.Bundle.Window_GeneralActions,
                    items: [{
                        ref: '../newBundle',
                        text: eBook.Bundle.Window_ReportNew,
                        iconCls: 'eBook-icon-addreport-24 ',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onNewBundleClick,
                        scope: this,
                        disabled: fileClosed
                    }, {
                        ref: '../openBundle',
                        text: eBook.Bundle.Window_ReportOpen,
                        iconCls: 'eBook-icon-openreport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        menu: this.loadMenu,
                        scope: this
                    }, {
                        ref: '../saveBundle',
                        text: eBook.Bundle.Window_ReportSave,
                        iconCls: 'eBook-icon-savereport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onSaveBundleClick,
                        disabled: true,
                        scope: this
                    }, {
                        ref: '../generateBundle',
                        text: eBook.Bundle.Window_ReportGenerate,
                        iconCls: 'eBook-icon-generatereport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onGenerateBundleClick,
                        disabled: true,
                        scope: this
                    }, {
                        ref: '../closeBundle',
                        text: 'Bundel afsluiten', //eBook.Bundle.Window_ReportClose,
                        iconCls: 'eBook-icon-lockreport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onLockBundleClick,
                        disabled: true,
                        hidden: true,
                        scope: this
                    }, {
                        ref: '../deleteBundle',
                        text: eBook.Bundle.Window_ReportDelete,
                        iconCls: 'eBook-icon-deletereport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onDeleteBundleClick,
                        disabled: true,
                        scope: this
}]
                    },
                     {
                         xtype: 'buttongroup',
                         ref: 'bundleActions',
                         id: this.id + '-toolbar-bundle',
                         columns: 2,
                         hidden: true,
                         disabled: fileClosed,
                         title: eBook.Bundle.Window_ReportActions,
                         items: [{
                             ref: '../newChapter',
                             text: eBook.Bundle.Window_NewChapter,
                             iconCls: 'eBook-icon-addchapter-24',
                             scale: 'medium',
                             iconAlign: 'top',
                             handler: this.onAddChapterClick,
                             scope: this
                         }, {
                             ref: '../deleteItem',
                             text: eBook.Bundle.Window_DeleteItem,
                             iconCls: 'eBook-icon-deleteitem-24',
                             scale: 'medium',
                             iconAlign: 'top',
                             disabled: true,
                             handler: this.onDeleteItem,
                             scope: this
}]
                         }, '->', {
                             xtype: 'buttongroup',
                             ref: 'libraryActions',
                             id: this.id + '-toolbar-library',
                             columns: 3,
                             hidden: true,
                             width: 300,
                             title: eBook.Bundle.Window_LibraryActions,
                             items: [{
                                 ref: '../reloadLib',
                                 text: eBook.Bundle.Window_LibraryReload,
                                 iconCls: 'eBook-icon-refreshlib-24',
                                 scale: 'medium',
                                 iconAlign: 'top',
                                 handler: this.onRefreshLibClick,
                                 scope: this
                             }, {
                                 ref: '../uploadPdf',
                                 text: eBook.Bundle.Window_LibraryAddPdf,
                                 iconCls: 'eBook-icon-addpdf-24',
                                 scale: 'medium',
                                 iconAlign: 'top',
                                 handler: this.onUploadPdfClick,
                                 scope: this,
                                 disabled: fileClosed
                             }, {
                                 ref: '../deletePdf',
                                 text: eBook.Bundle.Window_LibraryDeletePdf,
                                 iconCls: 'eBook-icon-deletepdf-24',
                                 scale: 'medium',
                                 iconAlign: 'top',
                                 handler: this.onDeletePdfClick,
                                 disabled: true,
                                 scope: this,
                                 disabled: fileClosed
}]
}]
                }
            });
            eBook.Bundle.Window.superclass.initComponent.apply(this, arguments);
        }
    , onNewBundleClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var wn = new eBook.Bundle.NewWindow({});
        wn.show(this);
    }
    , nodeLoaded: function(isReadOnly) {
        var tb = this.getTopToolbar();
        if (isReadOnly) {
            tb.deleteItem.disable();
        } else {
            tb.deleteItem.enable();
        }
    }
    , onDeleteItem: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.bundleContainer.deleteSelected();
    }
    , onAddChapterClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.bundleContainer.addChapter();
    }
    , loadLibrary: function(culture) {
        this.lastLibCulture = culture;
        this.bundleContainer.objects.getEl().mask(eBook.Bundle.Window_LoadingLibrary, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'GetLibrary'
                , method: 'POST'
                , params: Ext.encode({ cicdc: {
                    Id: eBook.Interface.currentFile.get('Id'),
                    Culture: culture
                }
                })
                , callback: this.onLoadLibraryResponse
                , scope: this
        });
    }
    , onLoadLibraryResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.bundleContainer.objects.load(robj.GetLibraryResult);
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.bundleContainer.objects.getEl().unmask();
    }
    , onLoadBundleClickNoRec: function(id, name, culture) {
        this.loadLibrary(culture);
        this.getEl().mask(String.format(eBook.Bundle.Window_LoadingReport, name), 'x-mask-loading');
        this.loadMenu.hide(true);
        Ext.Ajax.request({
            url: eBook.Service.bundle + 'GetFileReportLayout'
                    , method: 'POST'
                    , params: Ext.encode({ cfrldc: {
                        Id: id
                    }
                    })
                    , callback: this.onLoadBundleResponse
                    , scope: this
        });
    }
    , onLoadBundleClick: function(lv, rec) {
        this.onLoadBundleClickNoRec(rec.get('Id'), rec.get('Name'), rec.get('Culture'));
    }
    , onLoadBundleResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.loadBundle(robj.GetFileReportLayoutResult);
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) {
            //console.log(e);
        }
        this.getEl().unmask();
    }
    , onSaveBundleClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.getEl().mask(String.format(eBook.Bundle.Window_SavingReport, this.activeBundle.n), 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bundle + 'SaveFileReportLayout'
                , method: 'POST'
                , params: Ext.encode({ frldc: this.getBundle() })
                , callback: this.onSaveBundleResponse
                , scope: this
        });
    }
    , onSaveBundleResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                this.loadBundle(robj.SaveFileReportLayoutResult);
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
    , onDeleteBundleClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        if (this.activeBundle) {
            this.activeBundle.id;
            Ext.Msg.show({
                title: eBook.Bundle.Window_DeleteReportTitle,
                msg: String.format(eBook.Bundle.Window_DeleteReportMsg, this.activeBundle.n),
                buttons: Ext.Msg.YESNO,
                fn: this.onPerformDeleteBundle,
                scope: this,
                icon: Ext.MessageBox.QUESTION
            });
        }
    }
    , onPerformDeleteBundle: function(btnid) {
        if (this.activeBundle && btnid == 'yes') {
            //close bundle
            this.closeAll();
            this.getEl().mask(String.format(eBook.Bundle.Window_DeletingReport, this.activeBundle.n), 'x-mask-loading');
            Ext.Ajax.request({
                url: eBook.Service.bundle + 'DeleteReport'
                    , method: 'POST'
                    , params: Ext.encode({ cidc: { Id: this.activeBundle.id} })
                    , callback: this.onDeleteBundleCallBack
                    , scope: this
            });
        }
    }
    , onDeleteBundleCallBack: function(opts, success, resp) {
        this.getEl().unmask();
        if (success == false) {
            eBook.Interface.showResponseError(resp, this.title);
        }
        // if (this.loadMenu) this.loadMenu.reload();
    }
    , closeAll: function() {
        this.getLayout().setActiveItem(0);
        var tb = this.getTopToolbar();
        tb.saveBundle.disable();
        tb.generateBundle.disable();
        tb.deleteBundle.disable();
        tb.bundleActions.hide();
        tb.libraryActions.hide();
    }
    , onGenerateBundleClick: function() {
        this.getEl().mask(String.format(eBook.Bundle.Window_GeneratingReport, this.activeBundle.n), 'x-mask-loading');
        var bundle = this.getBundle();
        if (bundle.cl) {
            this.getEl().unmask();
            window.open('ClosedPdfs/' + bundle.id + '.pdf');
            return;
        }
        Ext.Msg.show({ title: this.title, msg: eBook.Closing.Window_BundleMsg, buttons: Ext.Msg.OK });
        Ext.Ajax.request({
            url: eBook.Service.bundle + 'GenerateBundle'
                    , method: 'POST'
                    , params: Ext.encode({ frldc: bundle })
                    , callback: this.onGenerateBundleResponse
                    , scope: this
        });
    }
    , onGenerateBundleResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                window.open('pickup/' + robj.GenerateBundleResult.PdfDocumentPath);
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
    , getBundle: function() {
        var bundle = {};
        Ext.apply(bundle, this.activeBundle);
        bundle.ly = this.bundleContainer.getBundle();
        bundle.Person = eBook.User.getActivePersonDataContract();
        return bundle;
    }
    , loadBundle: function(bundle) {
        this.getEl().mask(String.format(eBook.Bundle.Window_LoadingReport, bundle.n), 'x-mask-loading');
        this.getLayout().setActiveItem(1);
        bundle.ly.title = bundle.n;
        //        console.log(Ext.encode(bundle));
        this.activeBundle = {};
        Ext.apply(this.activeBundle, bundle);
        this.bundleContainer.loadBundle(bundle.ly, bundle.cl);
        this.getEl().unmask();
        var tb = this.getTopToolbar();
        tb.saveBundle.enable();
        tb.generateBundle.enable();
        tb.deleteBundle.enable();
        tb.bundleActions.show();
        tb.closeBundle.hide();

        if (bundle.cl || eBook.Interface.currentFile.get('Closed')) {
            tb.saveBundle.disable();
            tb.deleteBundle.disable();
            tb.bundleActions.hide();
        } else {
            if (eBook.Interface.currentFile.get('NumbersLocked') && eBook.User.checkCloser(bundle.cldp, true)) {
                tb.closeBundle.show();
                tb.closeBundle.enable();
            }
        }

        tb.libraryActions.show();
    }
    , show: function(report) {
        this.bundleContainer.postShow();
        eBook.Bundle.Window.superclass.show.call(this);
        if (report) {
            this.onLoadBundleClickNoRec(report.id, report.n, report.c);
        }

    }
    , renderMenu: function() {
        this.loadMenu = new eBook.Menu.ListViewMenu({
            handler: this.onLoadBundleClick,
            reloadOnShow: true,
            hideOnClick: true,
            scope: this,
            listConfig: {
                loadingText: eBook.Bundle.Window_LoadingMenu,
                hideHeaders: true,
                store:
             new Ext.data.JsonStore({
                 autoDestroy: true,
                 root: 'GetFileReportLayoutsResult',
                 fields: eBook.data.RecordTypes.FileReportList,
                 baseParams: {
                     Id: eBook.Interface.currentFile.get('Id')
                 },
                 proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                     url: eBook.Service.bundle + 'GetFileReportLayouts'
                 , criteriaParameter: 'cicdc'
                 , method: 'POST'
                 })
             })
            , width: 200
            , multiSelect: false
            , emptyText: eBook.Bundle.Window_EmptyMenu
            , reserveScrollOffset: false
                //, autoHeight: true
            , columns: [{
                header: 'name',
                width: 1,
                dataIndex: 'Name'
}]

            }
        });
    }
    , onUploadPdfClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var wn = new eBook.Pdf.UploadWindow({ parentCaller: this });
        wn.show(this);
    }
    , onDeletePdfClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        var pdf = this.bundleContainer.objects.selectedPdf;
        if (pdf) {
            pdf = pdf.getReportObject();
            Ext.Msg.show({
                title: eBook.Bundle.Window_LibraryDeletePdf,
                msg: String.format(eBook.Bundle.Window_DeletePdfMsg, pdf.title),
                buttons: Ext.Msg.YESNO,
                fn: this.onPerformDeletePdf,
                scope: this,
                icon: Ext.MessageBox.QUESTION
            });
        }
    }
    , onPerformDeletePdf: function(btnid) {
        if (btnid == "yes") {
            var pdf = this.bundleContainer.objects.selectedPdf;
            if (!pdf) return;
            pdf = pdf.getReportObject();
            var action = {
                text: String.format(eBook.Bundle.Window_DeletingPdfMsg, pdf.title)
                , params: { cidc: {
                    Id: pdf.id
                }
                }
                , action: 'DeletePdf'
            };
            this.bundleContainer.objects.deleteSelectedPdf();
            this.addAction(action);
        }
    }
    , refreshPdfs: function() {
        this.onRefreshLibClick();
    }
    , onRefreshLibClick: function() {
        this.loadLibrary(this.lastLibCulture);
    }
    , onLockBundleClick: function() {
        this.getEl().mask(eBook.Bundle.Window_ReportClosing, 'x-mask-loading');

        Ext.Ajax.request({
            url: eBook.Service.bundle + 'CloseReportLayout'
                , method: 'POST'
                , params: Ext.encode({ ccrdc: {
                    FileId: eBook.Interface.currentFile.get('Id')
                    , ReportId: this.activeBundle.id
                    , Culture: eBook.Interface.Culture
                    , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onLockBundleResponse
                , scope: this
        });
    }
    , onLockBundleResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                window.open('ClosedPdfs/' + this.activeBundle.id + '.pdf');
                this.loadBundle(robj.CloseReportLayoutResult);
            } else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        } catch (e) { }
        this.getEl().unmask();
    }
    });


Ext.reg('eBook.Bundle.Window', eBook.Bundle.Window);
