
eBook.Bundle.ResourcesWindow = Ext.extend(eBook.Window, {
    currentData:[]
    ,initComponent: function() {
        Ext.apply(this, {
            title: 'eBook resources'
            , layout: 'border'
            , modal:true
            , items: [{ xtype: 'ResourceSelectionTabPanel', ref: 'tabs', region: 'center' }
                        , { xtype: 'ResourcesSelected', ref: 'overview', region: 'east', width: 250 }
            ]
            , bbar: ['->',{
                text: 'Cancel',
                iconCls: 'eBook-no-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'cancelbtn',
                handler: this.close,
                scope: this
            }, {
                text: 'OK (x selecties)',
                iconCls: 'eBook-yes-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'okbtn',
                handler: this.onOK,
                scope: this
            }]
        });
        eBook.Bundle.ResourcesWindow.superclass.initComponent.apply(this, arguments);
    }
    , updateSelected: function(dta) {
        this.currentData=dta;
        this.overview.loadData(dta);
        this.getBottomToolbar().okbtn.setText("OK (" + dta.length + " selecties)");
    }
    ,show:function(src) {
        this.src = src;
        eBook.Bundle.ResourcesWindow.superclass.show.call(this);
    }
    , onOK: function(e) {
        if(this.src && this.src.resourcesPicked) {
            this.src.resourcesPicked(this.currentData);
        }
        this.close();
    }
});
Ext.reg('ResourcesWindow', eBook.Bundle.ResourcesWindow);

eBook.Bundle.ResourceSelectionTabPanel = Ext.extend(Ext.TabPanel, {
    initComponent: function() {
    var cFileId = eBook.Interface.currentFile.get('Id') //'22F4A0DF-93EE-4F56-B082-AD9D0DF875BC'; //eBook.Interface.currentFile.get('Id');
        Ext.apply(this, {
            activeItem:0
            , items: [{
                        xtype: 'ResourceSelectionTab'
                        , title: 'Pdf repository'
                        ,tabIndex:0
                        ,store: new eBook.data.JsonStore({
                            selectAction: 'GetResourcesOfType'
                            , autoDestroy: true
                            , autoLoad: true
                            , criteriaParameter: 'crtdc'
                            , fields: eBook.data.RecordTypes.Resource
                            , baseParams: {
                                'Culture': eBook.Interface.Culture
                                , 'Id': cFileId
                                , 'ResourceType': 'PDF'
                            }
                        })
                    }, {
                        xtype: 'ResourceSelectionTab'
                        , title: eBook.Menu.File_Worksheets
                        , tabIndex: 0
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetResourcesOfType'
                            , autoDestroy: true
                            , autoLoad: true
                            , criteriaParameter: 'crtdc'
                            , fields: eBook.data.RecordTypes.Resource
                            , baseParams: {
                                'Culture': eBook.Interface.Culture
                                , 'Id': cFileId
                                , 'ResourceType': 'WORKSHEETS'
                            }
                        })
                    }, {
                        xtype: 'ResourceSelectionTab'
                        , title: eBook.Bundle_Statements
                        , tabIndex: 0
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetResourcesOfType'
                            , autoDestroy: true
                            , autoLoad: true
                            , criteriaParameter: 'crtdc'
                            , fields: eBook.data.RecordTypes.Resource
                            , baseParams: {
                                'Culture': eBook.Interface.Culture
                                , 'Id': cFileId
                                , 'ResourceType': 'STATEMENTS'
                            }
                        })
                    }, {
                        xtype: 'ResourceSelectionTab'
                        , title: eBook.Menu.File_Documents
                        , tabIndex: 0
                        , store: new eBook.data.JsonStore({
                            selectAction: 'GetResourcesOfType'
                            , autoDestroy: true
                            , autoLoad: true
                            , criteriaParameter: 'crtdc'
                            , fields: eBook.data.RecordTypes.Resource
                            , baseParams: {
                                'Culture': eBook.Interface.Culture
                                , 'Id': cFileId
                                , 'ResourceType': 'DOCUMENTS'
                            }
                        })
                    }]
        });
        eBook.Bundle.ResourceSelectionTabPanel.superclass.initComponent.apply(this, arguments);
    }
    , updateSelections: function() {
        var dta = [];
        Ext.each(this.items.items,function(it,idx) {
            dta = dta.concat(it.getMySelections());
        }, this);
        this.ownerCt.updateSelected(dta);
    }
});
Ext.reg('ResourceSelectionTabPanel', eBook.Bundle.ResourceSelectionTabPanel);

eBook.Bundle.ResourceSelectionTab = Ext.extend(Ext.DataView, {

    initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.eBook-resource-item'
            , style: 'padding:5px'
            , autoScroll:true
            , tpl: new Ext.XTemplate('<tpl for=".">'
                            , '<div class="eBook-resource-item {iconCls}">{Title}</div><div class="x-clear"></div>'
                            , '</tpl>', { compiled: true })
            , selectedClass: 'x-list-selected'
            , simpleSelect: true
            , multiSelect: true
            , overClass: 'x-list-over'
            // , data: [{ id: 1, title: 'test', __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'}]
            , prepareData: function(dta) {
                dta.iconCls = 'test';
                switch (dta.DC) {
                    case "PDFDataContract:#EY.com.eBook.API.Contracts.Data":
                        dta.iconCls = 'eBook-bundle-tree-pdf';
                        break;
                    case "WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data":
                        dta.iconCls = 'eBook-bundle-tree-worksheet';
                        break;
                    case "StatementsDataContract:#EY.com.eBook.API.Contracts.Data":
                        dta.iconCls = 'eBook-bundle-tree-statement';
                        break;
                    case "DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data":
                        dta.iconCls = 'eBook-bundle-tree-document';
                        break;
                }
                return dta;
            }
        });
        eBook.Bundle.ResourceSelectionTab.superclass.initComponent.apply(this, arguments);
        this.on('selectionchange', this.onChecks, this);
    }
    , onChecks: function(view, nodes) {
        // var dta = Ext.pluck(this.getSelectedRecords(), "json");
        //var dta = Ext.pluck([], "json");
        this.ownerCt.updateSelections();
    }
    , getMySelections: function() {
        return Ext.pluck(this.getSelectedRecords(), "json");
    }
});

eBook.Bundle.ResourcesSelected = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.eBook-resource-item'
                , store: new Ext.data.JsonStore({
                            root: 'root'
                            , autoDestroy: true
                            , autoLoad: false
                            , fields: eBook.data.RecordTypes.Resource
                        })
                , autoScroll: true
                , tpl: new Ext.XTemplate('<tpl for=".">'
                                , '<div class="eBook-resource-item {iconCls}">{Title}</div><div class="x-clear"></div>'
                                , '</tpl>', { compiled: true })
            // , selectedClass: 'x-list-selected'
            //, simpleSelect: true
            //, multiSelect: true
            // , overClass: 'x-list-over'
            // , data: [{ id: 1, title: 'test', __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'}]
                , prepareData: function(dta) {
                    dta.iconCls = 'test';
                    switch (dta.DC) {
                        case "PDFDataContract:#EY.com.eBook.API.Contracts.Data":
                            dta.iconCls = 'eBook-bundle-tree-pdf';
                            break;
                        case "WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data":
                            dta.iconCls = 'eBook-bundle-tree-worksheet';
                            break;
                        case "StatementsDataContract:#EY.com.eBook.API.Contracts.Data":
                            dta.iconCls = 'eBook-bundle-tree-statement';
                            break;
                        case "DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data":
                            dta.iconCls = 'eBook-bundle-tree-document';
                            break;
                    }
                    return dta;
                }
        });
        eBook.Bundle.ResourcesSelected.superclass.initComponent.apply(this, arguments);

    }
    , loadData: function(dta) {
        this.store.loadData({ root: dta });
    }
});
/*
eBook.Bundle.ResourceSelectionTab = Ext.extend(Ext.list.ListView, {
    tpl : new Ext.XTemplate('<tpl for="rows">',
          '<dl class="x-grid3-row {[xindex % 2 === 0 ? "" :  "x-grid3-row-alt"]}">',
          '<tpl for="parent.columns">', '<dt style="width:{[values.width*100]}%;text-align:{align};">',
          '<em unselectable="on"<tpl if="cls">  class="{cls}</tpl>">{[values.tpl.apply(parent)]}',
          '</em></dt></tpl><div class="x-clear"></div></dl></tpl>')
    ,initComponent:function() {
        //var pl = Ext.ComponentMgr.createPlugin({ptype : 'lvcheckboxselection'});
        Ext.apply(this, {
        //plugins:[pl],
            hideHeaders :true,
            simpleSelect:true,
           // store : store,
            multiSelect : true,
            emptyText : 'Geen onderdelen',
            reserveScrollOffset : true,
            columns: [{
                    header: '',
                    dataIndex: 'DC',
                    width: 0.02,
                    tpl: '<tpl if="{DC}=\'PDFDataContract:#EY.com.eBook.API.Contracts.Data\'>P</tpl>'
                },{
              header : '',
              dataIndex: 'Title',
              width:0.7
            }]
        });
        eBook.Bundle.ResourceSelectionTab.superclass.initComponent.apply(this,arguments);
        this.on('selectionchange', this.onChecks,this);
    }
    , onChecks: function(view, nodes) {
        console.log(this.getSelectedRecords());
    }
});
*/
Ext.reg('ResourceSelectionTab', eBook.Bundle.ResourceSelectionTab);
Ext.reg('ResourcesSelected', eBook.Bundle.ResourcesSelected);
