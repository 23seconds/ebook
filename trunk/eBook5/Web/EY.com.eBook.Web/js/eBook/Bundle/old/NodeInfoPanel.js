

eBook.Bundle.NodeInfoPanel = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        Ext.apply(this, {
            //items: [],
            title: String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, "")
            , labelAlign: 'left'
            , items: [
                {
                    xtype: 'iconlabelfield',
                    cls: 'eBook-bundle-iconlabelfield ',
                    text: '',
                    ref: 'nodeType'
                },
                 {
                     xtype: 'label'
                    , text: eBook.Bundle.NodeInfoPanel_Title
                    , style: 'margin-bottom:2px;font-size:10pt;'
                    , ref: 'titlelabel'
                 }, {
                     xtype: 'textfield',
                     hideLabel: true,
                     value: '',
                     name: 'title',
                     ref: 'titleField',
                     listeners: {
                         'change': { fn: this.onChange, scope: this }
                     }, anchor: '100%', style: 'margin-bottom:10px'
                 }, {
                     xtype: 'languagebox'
                    , ref: 'culture'
                    , fieldLabel: eBook.Bundle.NodeInfoPanel_Language
                    , allowBlank: false
                    , autoSelect: true
                    , anchor: '90%'
                    , listeners: {
                        'select': { fn: this.onChange, scope: this }
                    }
}//ShowInIndex
                , {
                    xtype: 'checkbox',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_Detailed,
                    ref: 'detailed',
                    hidden: true,
                    listeners: {
                        'check': { fn: this.onChange, scope: this }
                    }
                }, {
                    xtype: 'checkbox',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_ShowInIndex,
                    ref: 'showindex',
                    hidden: false,
                    listeners: {
                        'check': { fn: this.onChange, scope: this }
                    }
                }, {
                    xtype: 'numberfield',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_PageFrom,
                    defaultValue: 1,
                    minValue: 1,
                    maxValue: 1,
                    ref: 'pageFrom',
                    hidden: true,
                    listeners: {
                        'change': { fn: this.onChange, scope: this }
                    }
                }, {
                    xtype: 'numberfield',
                    fieldLabel: eBook.Bundle.NodeInfoPanel_PageTo,
                    ref: 'pageTo',
                    defaultValue: 1,
                    minValue: 1,
                    maxValue: 1,
                    hidden: true,
                    listeners: {
                        'change': { fn: this.onChange, scope: this }
                    }
                }
                , {
                    xtype: 'eBook.Bundle.NodeInfoFieldSet',
                    title: eBook.Bundle.NodeInfoPanel_Header,
                    checkboxToggle: true,
                    collapsed: true,
                    items: [{
                        xtype: 'checkbox',
                        fieldLabel: eBook.Bundle.NodeInfoPanel_ShowTitle,
                        ref: 'titleField',
                        listeners: {
                            'check': { fn: this.onChange, scope: this }
                        }
}],
                        ref: 'headerData'
                    }
                , {
                    xtype: 'eBook.Bundle.NodeInfoFieldSet',
                    title: eBook.Bundle.NodeInfoPanel_Footer,
                    checkboxToggle: true,
                    collapsed: false,
                    checked: true,
                    items: [
                            { xtype: 'checkbox',
                                fieldLabel: eBook.Bundle.NodeInfoPanel_ShowPageNr,
                                ref: 'pagenr',
                                listeners: {
                                    'check': { fn: this.onChange, scope: this }
                                }
                            }, {
                                xtype: 'checkbox',
                                fieldLabel: eBook.Bundle.NodeInfoPanel_ShowFootNote,
                                ref: 'footnote',
                                listeners: {
                                    'check': { fn: this.onChange, scope: this }
                                }
}],
                    ref: 'footerData'
                }
                , {
                    xtype: 'button'
                    , text: eBook.Bundle.NodeInfoPanel_ReconfigureChildren
                    , handler: this.onReconfigureChildrenClick
                    , ref: 'resetChildren'
                    , style: 'margin-top:10px;margin-bottom:10px;'
                    , scope: this
                }
                ]
        });
        eBook.Bundle.NodeInfoPanel.superclass.initComponent.apply(this, arguments);

    }
    , nodeUpdate: true
    , onChange: function() {
        if (!this.nodeUpdate) return;
        this.currentNode.setProperties(this.getProperties());
    }
    , onReconfigureChildrenClick: function() {
        Ext.Msg.show({
            title: eBook.Bundle.NodeInfoPanel_UpdateChildrenTitle,
            msg: eBook.Bundle.NodeInfoPanel_UpdateChildrenMsg,
            buttons: Ext.Msg.YESNO,
            fn: this.reconfigureChildren,
            scope: this,
            icon: Ext.MessageBox.QUESTION
        });
    }
    , reconfigureChildren: function(btnid) {

        if (btnid == 'yes') {
            var props = this.getProperties();
            for (var i = 0; i < this.currentNode.childNodes.length; i++) {
                this.currentNode.childNodes[i].applyProperties({ HeaderConfig: props.HeaderConfig, FooterConfig: props.FooterConfig }, true);
            }
        }
    }
    , getProperties: function() {
        var hd = this.headerData.checkbox.dom.checked;
        var ft = this.footerData.checkbox.dom.checked;
        var props = {
            type: this.currentNode.attributes.properties.type,
            title: this.titleField.getValue(),
            indexed: this.showindex.getValue(),
            HeaderConfig: {
                Enabled: hd
                , ShowTitle: hd ? this.headerData.titleField.getValue() : false
            },
            FooterConfig: {
                Enabled: ft
                , ShowPageNr: ft ? this.footerData.pagenr.getValue() : false
                , ShowFooterNote: ft ? this.footerData.footnote.getValue() : false
            }
        };
        if (this.culture.isVisible()) props.Culture = this.culture.getValue();
        if (props.type == "STATEMENT") props.Detailed = this.detailed.getValue();
        if (props.type == "PDFPAGE") {
            props.FromPage = this.pageFrom.getValue();
            props.ToPage = this.pageTo.getValue();
            if (props.ToPage < props.FromPage) {
                var t = props.FromPage;
                props.FromPage = props.ToPage;
                props.ToPage = t;
            }
        }
        this.setTitle(String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, props.title));
        return props;
    }
    , loadNode: function(node, closed) {
        this.nodeUpdate = false;
        this.getForm().reset();
        this.currentNode = node;
        if (node == null) { this.getForm().reset(); return }
        if (!node.attributes.properties) {
            this.getForm().reset();
            this.disable();
            return;
        }
        if(!closed) this.enable();
        this.setFields();
        if (node.attributes.readOnly || node.attributes.properties.locked) this.disable();
        this.nodeUpdate = true;
        if (closed) {
            this.currentNode.attributes.iconCls = "eBook-lock-16";
            this.disable();
        }
        // load form fields with node data.

        // show/hide conform the node type.
    }
    , setFields: function() {
        //var rsettings = this.refOwner.getReportSettings();
        var props = this.currentNode.attributes.properties;
        //this.nodeType.setValue(props.type);
        this.nodeType.update(props.title, this.currentNode.attributes.iconCls);
        if (!Ext.isDefined(props.title)) props.title = this.currentNode.attributes.text;
        this.setTitle(String.format(eBook.Bundle.NodeInfoPanel_InfoTitle, props.title));
        this.titleField.setValue(props.title);
        this.titleField.enable();
        this.headerData.show();
        this.footerData.show();
        this.showindex.show();

        if (Ext.isDefined(props.Culture)) {
            this.culture.setValue(props.Culture);
            this.culture.enable();
            this.culture.show();
        } else {
            this.culture.hide();
        }

        if (props.type == "DOCUMENT") {
            this.culture.disable(); //document language is set on creation.
        }

        if (props.type == "ROOT") {
            this.nodeType.setText(eBook.Bundle.NodeInfoPanel_ReportSettings);
            this.resetChildren.show();
            this.titleField.hide();
        } else { this.titleField.show(); this.resetChildren.hide(); }

        if (props.type == "STATEMENT") {
            this.detailed.show();
            this.detailed.setValue(props.Detailed);
        } else {
            this.detailed.hide();
        }


        if (props.type == "PDFPAGE") {
            // this.nodeType.setValue(props.type + ' (' + props.pageCount + ' pages)');
            //this.pageFrom.
            this.pageFrom.setMaxValue(props.Pages);
            this.pageFrom.setValue(Ext.isDefined(props.FromPage) ? props.FromPage : 1);
            this.pageTo.setMaxValue(props.Pages);
            this.pageTo.setValue(Ext.isDefined(props.ToPage) ? props.ToPage : props.Pages);

            this.pageFrom.show();
            this.pageTo.show();
        } else {
            this.pageFrom.hide();
            this.pageTo.hide();
        }

        this.headerData.checkbox.dom.checked = Ext.isDefined(props.HeaderConfig) ? props.HeaderConfig.Enabled : false;
        this.footerData.checkbox.dom.checked = Ext.isDefined(props.FooterConfig) ? props.FooterConfig.Enabled : false;

        this.showindex.setValue(Ext.isDefined(props.indexed) ? props.indexed : true);
        if (Ext.isDefined(props.HeaderConfig)) {
            if (props.HeaderConfig.Enabled) this.headerData.titleField.setValue(props.HeaderConfig.ShowTitle);
        }

        if (Ext.isDefined(props.FooterConfig)) {
            if (props.FooterConfig.Enabled) this.footerData.pagenr.setValue(props.FooterConfig.ShowPageNr);
            if (props.FooterConfig.Enabled) this.footerData.footnote.setValue(props.FooterConfig.ShowFooterNote);
        }
        this.headerData.onCheckClick();
        this.footerData.onCheckClick();

        if (props.type == "COVERPAGE" || props.type == "INDEXPAGE") {
            this.titleField.hide();
            this.titlelabel.hide();
            this.headerData.hide();
            this.footerData.hide();
            this.showindex.hide();
        } else {
            this.titleField.show();
            this.titlelabel.show();
            this.showindex.show();
        }

    }
});


Ext.reg('eBook.Bundle.NodeInfoPanel', eBook.Bundle.NodeInfoPanel);