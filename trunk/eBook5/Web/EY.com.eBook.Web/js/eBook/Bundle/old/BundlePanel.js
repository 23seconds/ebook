


eBook.Bundle.BundlePanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            frame: false,
            border: false,
            layout: 'border',
            split: true,
            items: [
                   {
                       xtype: 'eBook.Bundle.NodeInfoPanel'
                        , region: 'west'
                        , ref: 'info'
                        , frame: true
                        , width: 335
                   }, {
                       xtype: 'eBook.Bundle.BundleTree'
                        , region: 'center'
                        , ref: 'bundle'
                   }, {
                       xtype: 'eBook.Bundle.ObjectsTree'
                        , region: 'east'
                        , ref: 'objects'
                        , width: 335
                   }
                  ]
        });
        eBook.Bundle.BundlePanel.superclass.initComponent.apply(this, arguments);
    }
    , loadBundle: function(bundle, closed) {
        this.bundle.setRootNode(new eBook.Bundle.AsyncNode(bundle, closed));
        if (closed) {
            this.bundle.dropZone.lock();
            this.bundle.dragZone.lock();
        } else {
            this.bundle.dropZone.unlock();
            this.bundle.dragZone.unlock();
        }

    }
    , loadNode: function(node) {
        this.info.loadNode(node, this.refOwner.activeBundle != null ? this.refOwner.activeBundle.cl : false);
        this.refOwner.nodeLoaded(node.attributes.properties.locked == true || node.attributes.properties.type == 'ROOT');
    }
    , addChapter: function() {
        this.bundle.addChapter();
    }
    , deleteSelected: function() {
        this.bundle.deleteSelected();
    }
    , getBundle: function() {
        return this.bundle.getReport();
    }
    , postShow: function() {
        /*
        this.bundle.setRootNode(new eBook.Bundle.AsyncNode({
        text: 'Root test',
        expanded: true,
        type: 'ROOT',
        children: [{
        text: 'Cover',
        type: 'COVERPAGE'
        }, {
        text: 'indexpage',
        type: 'INDEXPAGE'
        }, {
        text: 'Node 1',
        type: 'CHAPTER',
        children: [{
        text: 'Node 1a',
        type: 'PDFPAGE',
        pageCount: 1
        }, {
        text: 'Node 1b',
        type: 'PDFPAGE',
        pageCount: 30
        }]
        }]
        }));
        */
        this.objects.setRootNode(new eBook.Bundle.AsyncNode({
            text: eBook.Bundle.BundlePanel_Library,
            expanded: true,
            type: 'LIBRARY',
            children: []
        }));
    }
});

Ext.reg('eBook.Bundle.BundlePanel', eBook.Bundle.BundlePanel);
