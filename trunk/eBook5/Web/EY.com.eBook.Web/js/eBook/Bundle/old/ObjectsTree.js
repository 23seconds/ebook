

eBook.Bundle.ObjectsTree = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        Ext.apply(this, {
            title: eBook.Bundle.ObjectsTree_Title,
            autoScroll: true,
            animate: true, // replace with eBook config
            enableDD: true,
            iconCls: 'eBook-icon-library-16',
            containerScroll: true,
            root: {
                nodeType: 'node',
                text: 'dummy',
                draggable: false,
                id: 'objects-root'
            },
            ddGroup: 'eBook.Bundle',
            loader: new eBook.Bundle.TreeLoader({ preloadChildren: true }),
            //dragConfig:
            //dropConfig
            //enableDrag, //drag only
            //enableDrop, //drop only
            lines: true,
            //margins:
            //requestMethod:'POST',
            rootVisible: false
        });
        this.getSelectionModel().on('selectionchange', this.onNodeSelect, this);
        eBook.Bundle.ObjectsTree.superclass.initComponent.apply(this, arguments);
    }
    , load: function(obj) {
        this.setRootNode(new eBook.Bundle.AsyncNode(obj));
    }
    , onNodeSelect: function(sm, node) {
        var win = this.refOwner.refOwner;
        win.getTopToolbar().deletePdf.disable();
        if (node != null) {
            if (node.attributes.properties.__type == 'PDFDataContract:#EY.com.eBook.API.Contracts.Data') {
                this.selectedPdf = node;
                win.getTopToolbar().deletePdf.enable();
                return;
            }
        }
        this.selectedPdf = null;

    }
    , deleteSelectedPdf: function() {
        var s = this.selectedPdf;
        if (s != null) s.remove(true);
    }
});

Ext.reg('eBook.Bundle.ObjectsTree', eBook.Bundle.ObjectsTree);
