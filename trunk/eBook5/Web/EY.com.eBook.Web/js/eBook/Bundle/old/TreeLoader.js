

eBook.Bundle.TreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    createNode: function(attr) {
        // apply baseAttrs, nice idea Corey!
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        if (Ext.isString(attr.uiProvider)) {
            attr.uiProvider = this.uiProviders[attr.uiProvider] || eval(attr.uiProvider);
        }
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                        new eBook.Bundle.Node(attr) :
                        new eBook.Bundle.AsyncNode(attr);
        }
    }
});