

eBook.Bundle.BundleTree = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        Ext.apply(this, {
            title: eBook.Bundle.BundleTree_Title,
            autoScroll: true,
            animate: true, // replace with eBook config
            enableDD: true,
            containerScroll: true,
            loader: new eBook.Bundle.TreeLoader({ preloadChildren: true }),
            ddGroup: 'eBook.Bundle',
            //dragConfig:
            //dropConfig
            //enableDrag, //drag only
            root: new eBook.Bundle.AsyncNode({
                text: 'empty',
                expanded: true,
                type: 'ROOT',
                Index: { items: [] }
            }),
            lines: true,
            plugins: [
	            { ptype: 'ux-tree-treepanel-copydropnode', nodeType: 'bundleAsyncNode', copyToDifferentTreeChangeId: false }
	        ],
            keys: [
	            {
	                key: Ext.EventObject.DELETE
	                , fn: this.onDeleteNode
	                , scope: this
	            }
	        ],
            //margins:
            //requestMethod:'POST',
            rootVisible: true
        });
        eBook.Bundle.BundleTree.superclass.initComponent.apply(this, arguments);
        this.getSelectionModel().on('selectionchange', this.onNodeSelect, this);
        this.on('nodedrop', this.onDropped, this);

    }
    , onNodeSelect: function(sm, node) {
        if (node != null) {
            this.refOwner.loadNode(node);
        } else {
            this.selectNode(this.getRootNode());
        }
    }
    , onDropped: function(dde) {
        this.getSelectionModel().select(dde.dropNode);
    }
    , setRootNode: function(node) {
        eBook.Bundle.BundleTree.superclass.setRootNode.call(this, node);
        this.selectNode.defer(500, this, [node]);
    }
    , selectNode: function(node) {
        this.getSelectionModel().select(node);
    }
    , onDeleteNode: function() {
        this.deleteSelected();
    }
    , getReport: function() {
        return this.getRootNode().getReportObject();
    }
    , deleteSelected: function() {
        var s = this.getSelectionModel().getSelectedNode();
        if (s != null) s.remove(true);
        this.selectNode(this.getRootNode());
    }
    , addChapter: function() {
        var props = {
            __type: 'ChapterDataContract:#EY.com.eBook.API.Contracts.Data'
            , type: 'CHAPTER'
            , title: eBook.Bundle.BundleTree_NewChapter
            , items: []
            , FooterConfig: { Enabled: false, ShowFooterNote: false, ShowPageNr: false }
            , HeaderConfig: { Enabled: false, ShowTitle: false }
            , id: eBook.NewGuid()
            , indexed: true
        };
        var n = new eBook.Bundle.AsyncNode(props);
        this.getRootNode().appendChild(n);
        this.selectNode(n);
    }
});

Ext.reg('eBook.Bundle.BundleTree', eBook.Bundle.BundleTree);
