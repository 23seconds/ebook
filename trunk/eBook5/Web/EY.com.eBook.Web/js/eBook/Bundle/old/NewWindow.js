
eBook.Bundle.NewWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'form'
            , labelWidth: 200
            , width: 450
            , height: 250
            , modal: true
            , maximizable: false
            , bodyStyle: 'padding:10px;'
            , resizable: false
            , items: [
                {
                    xtype: 'checkbox'
                    , ref: 'includePrevious'
                    , boxLabel: eBook.Bundle.NewWindow_IncludePrevious
                    , listeners: {
                        'check': {
                            fn: this.loadLayoutType
                            , scope: this
                        }
                    }
                    , hidden: true
                }
                , {
                    xtype: 'languagebox'
                    , ref: 'culture'
                    , fieldLabel: eBook.Bundle.NewWindow_Language
                    , allowBlank: false
                    , autoSelect: true
                    , anchor: '90%'
                    , listeners: {
                        'select': {
                            fn: this.loadLayoutType
                            , scope: this
                        }
                    }
                }, {
                    xtype: 'combo'
                    , ref: 'layouttype'
                    , fieldLabel: eBook.Bundle.NewWindow_Layout
                    , name: 'layout'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Id'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , mode: 'local'
                    , store: new eBook.data.JsonStore({
                        selectAction: 'GetReportTypes'
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: 'crtdc'
                        , listeners: {
                            'beforeload': {
                                fn: this.updateLayoutType
                                , scope: this
                            }
                        }
                        , fields: eBook.data.RecordTypes.FileReportList
                    })
                    , listeners: {
                        'select': {
                            fn: this.setName
                            , scope: this
                        }
                    }
                    , anchor: '90%'
                }, {
                    xtype: 'textfield'
                    , ref: 'reportname'
                    , name: 'Name'
                    , allowBlank: false
                    , fieldLabel: eBook.Bundle.NewWindow_Name
                    , anchor: '90%'
}]
            , tbar: [{
                ref: 'createreport',
                text: eBook.Bundle.NewWindow_Create,
                iconCls: 'eBook-bundle-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onCreateReportClick,
                scope: this
}]
            });
            eBook.Bundle.NewWindow.superclass.initComponent.apply(this, arguments);
        }
    , onCreateReportClick: function() {
        // send create to server
        if (!this.reportname.isValid()) return;
        if (!this.layouttype.isValid()) return;
        if (!this.culture.isValid()) return

        this.getEl().mask(eBook.Bundle.NewWindow_Creating, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.bundle + 'CreateFileReport'
            , method: 'POST'
            , params: Ext.encode({ cnfrldc: {
                FileId: eBook.Interface.currentFile.get('Id')
                    , TypeId: this.layouttype.getValue()
                    , Culture: this.culture.getValue()
                    , Name: this.reportname.getValue()
                    , FromPrevious: this.includePrevious.getValue() == true
                    , Person: eBook.User.getActivePersonDataContract()
            }
            })
            , callback: this.onCreatedReportCallback
            , scope: this
        });
    }
    , onCreatedReportCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            this.callingParent.loadLibrary(this.culture.getValue());
            this.callingParent.loadBundle(robj.CreateFileReportResult);
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        // load object in calling window
    }
    , show: function(parent) {
        this.callingParent = parent;
        eBook.Bundle.NewWindow.superclass.show.call(this);
        var pfid = eBook.Interface.currentFile.get('PreviousFileId');
        if (!Ext.isEmpty(pfid)) this.includePrevious.show();
    }
    , loadLayoutType: function() {
        this.layouttype.clearValue();
        this.layouttype.store.load();
    }
    , updateLayoutType: function() {
        var ipf = this.includePrevious.isVisible() ? this.includePrevious.getValue() : false;
        var cult = this.culture.getValue() != '' ? this.culture.getValue() : eBook.Interface.Culture;
        this.layouttype.store.baseParams = { FileId: eBook.Interface.currentFile.get('Id'), Culture: cult, IncludePrevious: ipf };
    }
    , setName: function(c, r, idx) {
        this.reportname.setValue(r.get('Name'));
    }
    });