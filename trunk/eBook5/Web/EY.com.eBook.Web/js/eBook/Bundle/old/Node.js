

eBook.Bundle.AsyncNode = Ext.extend(Ext.tree.AsyncTreeNode, {
    constructor: function(config,closed) {
        var cfg = {
            properties: config.properties ? config.properties : config
        };
        cfg.allowChildren = false;
        cfg.ui = eBook.Bundle.NodeUI;
        cfg.allowDrag = true;
        cfg.text = config.title ? config.title : config.text;
        cfg.leaf = true;
        config.type = config.properties ? config.properties.type : config.type;

        if (!Ext.isDefined(config.__type)) config.__type = config.type ? config.type : "ROOT";
        switch (config.__type) {
            case 'ROOT':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = true;
                cfg.allowDrop = true;
                cfg.allowDrag = false;
                cfg.iconCls = 'eBook-bundle-tree-bundle ';
                if (closed) cfg.iconCls += 'ebook-bundle-tree-closed ';
                cfg.leaf = false;
                cfg.readOnly = false;
                cfg.children = config.properties ? config.children : config.Index.items;
                cfg.expanded = true;
                cfg.properties.type = 'ROOT';
                break;
            case 'LIBRARY':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = false;
                cfg.allowDrop = false;
                cfg.allowDrag = false;
                cfg.iconCls = '';
                cfg.leaf = false;
                cfg.readOnly = true;
                cfg.children = config.folders;
                cfg.properties.type = 'LIBRARY';
                break;

            case 'FOLDER':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = false;
                cfg.allowDrop = false;
                cfg.allowDrag = false;
                cfg.icon = '';
                cfg.leaf = false;
                cfg.readOnly = true;
                cfg.children = config.items;
                cfg.properties.type = 'FOLDER';
                break;
            case 'ChapterDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'CHAPTER':
                cfg.allowChildren = true;
                cfg.isTarget = true;
                cfg.allowDrop = true;
                cfg.iconCls = 'eBook-bundle-tree-chapter';
                cfg.leaf = false;
                cfg.children = config.properties ? config.children : config.items;
                cfg.properties.type = 'CHAPTER';
                //'ChapterDataContract:#EY.com.eBook.API.Contracts.Data':
                break;
            case 'CoverpageDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'COVERPAGE':
                //'CoverpageDataContract:#EY.com.eBook.API.Contracts.Data':
                cfg.iconCls = 'eBook-bundle-tree-cover';
                cfg.properties.type = 'COVERPAGE';
                break;
            case 'IndexPageDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'INDEXPAGE':
                cfg.iconCls = 'eBook-bundle-tree-index';
                cfg.properties.type = 'INDEXPAGE';
                break;
            case 'PDFDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'PDFPAGE':
                cfg.iconCls = 'eBook-bundle-tree-pdf';
                cfg.properties.type = 'PDFPAGE';
                cfg.comment = cfg.properties.title;
                break;
            case 'DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'DOCUMENT':
                cfg.iconCls = 'eBook-bundle-tree-document';
                cfg.properties.type = 'DOCUMENT';
                break;
            case 'FicheItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'FICHE':
                cfg.iconCls = 'eBook-bundle-tree-fiche';
                cfg.properties.type = 'FICHE';
                break;
            case 'StatementsDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'STATEMENT':
                cfg.iconCls = 'eBook-bundle-tree-statement';
                cfg.properties.type = 'STATEMENT';
                break;
            case 'WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'WORKSHEET':
                cfg.iconCls = 'eBook-bundle-tree-worksheet';
                cfg.properties.type = 'WORKSHEET';
                break;
        }
        if (cfg.properties.locked == true) {
            cfg.readOnly = true;
            cfg.allowDrag = false;
        }
        cfg.properties.items = null;
        cfg.properties.Index = null;
        delete cfg.properties.items;
        delete cfg.properties.Index;
        eBook.Bundle.AsyncNode.superclass.constructor.call(this, cfg);
    }
     , setProperties: function(props) {
         if (this.attributes.properties == null) this.attributes.properties = {};
         Ext.apply(this.attributes.properties, props);
         if (props.type != "ROOT") this.setText(props.title);
     }
     , applyProperties: function(props, deep) {
         if (!(this.attributes.readOnly == true)) Ext.apply(this.attributes.properties, props);
         if (deep) {
             for (var i = 0; i < this.childNodes.length; i++) {
                 this.childNodes[i].applyProperties(props, deep);
             }
         }
     }
     , getReportObject: function() {
         var props = this.attributes.properties;
         delete props.loader;
         delete props.baseParams;
         delete props.preloadChildren;
         delete props.events;
         switch (props.type) {
             case "ROOT":
                 delete props.__type;
                 delete props.title;
                 props.Index = { items: this.getChildrenObjects() };
                 break;
             case "CHAPTER":
                 props.items = this.getChildrenObjects();
                 break;
         }
         return props;
     }
     , getChildrenObjects: function() {
         var arr = [];
         for (var i = 0; i < this.childNodes.length; i++) {
             arr.push(this.childNodes[i].getReportObject());
         }
         return arr;
     }
});

Ext.tree.TreePanel.nodeTypes.bundleAsyncNode = eBook.Bundle.AsyncNode;