
eBook.Bundle.NodeInfoFieldSet = Ext.extend(Ext.form.FieldSet, {
    onCheckClick: function() {
        eBook.Bundle.NodeInfoFieldSet.superclass.onCheckClick.call(this);
        this.refOwner.onChange();
    }
});
Ext.reg('eBook.Bundle.NodeInfoFieldSet', eBook.Bundle.NodeInfoFieldSet);