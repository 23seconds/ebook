eBook.Bundle.BundleList = Ext.extend(Ext.list.ListView, {
    initComponent: function () {
        var me = this,
            selectAction = 'GetGenericBundles',
            criteriaParameter = 'cicdc',
            baseParams = {'Id': eBook.Interface.currentFile.get('Id'), 'Culture': eBook.Interface.Culture};

        //If serviceId is defined, get service linked bundles only
        if (me.serviceId) {
            selectAction = 'GetServiceBundles';
            criteriaParameter = 'cfsdc';
            baseParams = {
                'ServiceId': this.serviceId,
                'FileId': eBook.Interface.currentFile.get('Id'),
                'Culture': eBook.Interface.Culture
            };
        }

        Ext.apply(this, {
            loadingText: eBook.Bundle.Window_LoadingMenu,
            hideHeaders: true,
            multiSelect: false,
            singleSelect: true,
            store: new eBook.data.JsonStore({
                selectAction: selectAction,
                serviceUrl: eBook.Service.bundle,
                autoDestroy: true,
                autoLoad: true,
                criteriaParameter: criteriaParameter,
                fields: eBook.data.RecordTypes.FileReportList,
                baseParams: baseParams
            }),
            emptyText: eBook.Bundle.Window_EmptyMenu,
            columns: [{
                header: 'Dossier',
                dataIndex: 'Name'
            }]
        });

        eBook.Bundle.BundleList.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('bundleList', eBook.Bundle.BundleList);

eBook.Bundle.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        var me = this;

        //var fileClosed = eBook.Interface.currentFile.get('Closed');
        var fileClosed = eBook.Interface.isFileClosed();
        Ext.apply(this, {
            title: me.serviceId ? eBook.Bundle.Window_Deliverable_Title : eBook.Bundle.Window_Bundle_Title,
            layout: 'fit',
            width: 405,
            cls: 'eBook-Bundle-Window',
            iconCls: 'eBook-Bundle-icon-16',
            bodyStyle: 'background-color:#FFF;',
            items: [{
                xtype: 'bundleList', ref: 'blst', serviceId: this.serviceId,
                listeners: {
                    'dblclick': {fn: this.onBundleItemDblClick, scope: this},
                    'selectionchange': {fn: this.onBundleSelectionChange, scope: this}
                }
            }],
            tbar: {
                xtype: 'toolbar',
                items: [{
                    xtype: 'buttongroup',
                    ref: 'generalActions',
                    id: this.id + '-toolbar-general',
                    columns: 6,
                    title: eBook.Bundle.Window_GeneralActions,
                    items: [{
                        ref: '../newBundle',
                        text: me.serviceId ? eBook.Bundle.Window_Deliverable_New : eBook.Bundle.Window_Bundle_New,
                        iconCls: 'eBook-icon-addreport-24 ',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onNewBundleClick,
                        scope: this,
                        disabled: fileClosed
                    }, {
                        ref: '../openBundle',
                        text: me.serviceId ? eBook.Bundle.Window_Deliverable_Open : eBook.Bundle.Window_Bundle_Open,
                        iconCls: 'eBook-icon-openreport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onOpenBundleClick,
                        disabled: true,
                        scope: this
                    }, {
                        ref: '../deleteBundle',
                        text: me.serviceId ? eBook.Bundle.Window_Deliverable_Delete : eBook.Bundle.Window_Bundle_Delete,
                        iconCls: 'eBook-icon-deletereport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onDeleteBundleClick,
                        disabled: true,
                        scope: this
                    }]
                }]
            },
            listeners: {
                afterrender: function () {
                    //remove mask if there is one
                    if (me.maskedCmp) {
                        me.maskedCmp.getEl().unmask();
                    }
                }
            }
        });

        eBook.Bundle.Window.superclass.initComponent.apply(this, arguments);
    },
    onBundleItemDblClick: function (dv, i, n, e) {
        var rec = dv.getStore().getAt(i);
        this.getBundle(rec.data);
        // this.onLoadBundleClick(this, rec);
    },
    onBundleSelectionChange: function (dv, nds) {
        var tb = this.getTopToolbar();
        if (nds.length === 0) {
            tb.openBundle.disable();
            tb.deleteBundle.disable();
        } else {
            var recs = dv.getRecords(nds);
            //recs[0]
            tb.openBundle.enable();
            if (eBook.Interface.currentFile && !eBook.Interface.currentFile.get('Closed')) {
                tb.deleteBundle.enable();
            }
        }
    },
    onOpenBundleClick: function () {
        var recs = this.blst.getSelectedRecords();
        this.getBundle(recs[0].data);
    },
    getBundle: function (bundle) {
        //this function is also called from interface.js, hence the if's (quick fix)
        if (this.getEl()) {
            this.getEl().mask('Loading');
        }

        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'GetBundle',
            method: 'POST',
            params: Ext.encode({id: bundle.Id}),
            callback: this.onGetBundleCallBack,
            scope: this
        });
    },
    onGetFileServiceStatus: function (bundle, serviceId) {
        var me = this;

        eBook.CachedAjax.request({
            url: eBook.Service.file + 'GetFileServiceStatus',
            method: 'POST',
            params: Ext.encode({cfsdc: {FileId: bundle.FileId, ServiceId: serviceId, ClientId: eBook.CurrentClient}}),
            callback: me.onGetFileServiceStatusCallback.createDelegate(me, [bundle, serviceId], true),
            scope: me
        });
    },
    onGetBundleCallBack: function (opts, success, resp) {
        var me = this;
        if (success && resp.responseText) {
            var robj = Ext.decode(resp.responseText);
            me.bundle = robj.GetBundleResult;

            if (me.bundle && me.serviceId) {
                //after the bundle has been retrieved, get its file service status
                me.onGetFileServiceStatus(me.bundle, me.serviceId);
            }
            else {
                me.showEditWindow({bundle: me.bundle, serviceId: me.serviceId});
            }
        }
        else {
            eBook.Interface.showResponseError(resp, me.title);
            if (me.maskedCmp) {
                me.maskedCmp.unmask();
            }
            else {
                me.getEl().unmask();
            }
        }
    },
    onGetFileServiceStatusCallback: function (opts, success, resp, bundle, serviceId) {
        var me = this;
        if (success && resp.responseText) {
            var robj = Ext.decode(resp.responseText);
            var serviceStatus = robj.GetFileServiceStatusResult;
            me.showEditWindow({bundle: bundle, serviceStatus: serviceStatus, serviceId: serviceId});
        }
        else {
            eBook.Interface.showResponseError(resp, me.title);
            if (me.maskedCmp) {
                me.maskedCmp.unmask();
            }
        }
    },
    showEditWindow: function (properties) {
        var me = this;

        if (me.maskedCmp) {
            properties.maskedCmp = me.maskedCmp;
        }

        var wn = new eBook.Bundle.EditWindow(properties);
        wn.show();
        me.close();
    },
    onNewBundleClick: function () {
        var me = this;

        if (eBook.Interface.currentFile.get('Closed')){ return;}
        var wn = new eBook.Bundle.NewWindow(
            {
                serviceId: me.serviceId, //serviceId is set in client/newHome as it is button specific
                readyCallback: me.serviceId ? {fn: me.onGetFileServiceStatus, scope: me} : null //only retrieve fileservice if service set
            });

        wn.show(this);
    },
    onDeleteItem: function () {
        if (eBook.Interface.currentFile.get('Closed')){ return;}
        this.bundleContainer.deleteSelected();
    },
    onLoadBundleClickNoRec: function (id, name, culture) {
        //to update
    },
    onLoadBundleClick: function (lv, rec) {
        this.onLoadBundleClickNoRec(rec.get('Id'), rec.get('Name'), rec.get('Culture'));
    },
    onDeleteBundleClick: function () {
        var me = this;
        if (eBook.Interface.currentFile.get('Closed')){ return;}
        var recs = this.blst.getSelectedRecords();
        if (recs.length === 0){ return;}
        var rec = recs[0];
        if (rec) {
            Ext.Msg.show({
                title: me.serviceId ? eBook.Bundle.Window_Deliverable_Delete : eBook.Bundle.Window_Bundle_Delete,
                msg: String.format(eBook.Bundle.Window_DeleteReportMsg, rec.get('Name')),
                buttons: Ext.Msg.YESNO,
                fn: me.onPerformDeleteBundle,
                scope: me,
                icon: Ext.MessageBox.QUESTION,
                rec: rec
            });
        }
    },
    onPerformDeleteBundle: function (btnid, txt, opts) {
        var me = this;

        if (opts.rec && btnid == 'yes') {
            var bundleName = opts.rec.get('Name');
            var bundleId = opts.rec.get('Id');
            //close bundle
            // this.closeAll();
            me.getEl().mask(String.format(eBook.Bundle.Window_DeletingReport, bundleName), 'x-mask-loading');
            eBook.CachedAjax.request({
                url: eBook.Service.bundle + 'DeleteBundle',
                method: 'POST',
                params: Ext.encode({id: bundleId}),
                callback: me.onDeleteBundleCallBack.createDelegate(me, [bundleId], true),
                scope: me
            });
        }
    },
    onDeleteBundleCallBack: function (opts, success, resp, bundleId) {
        var me = this;
        me.getEl().unmask();
        if (success === false) {
            eBook.Interface.showResponseError(resp, this.title);
        } else {
            me.blst.store.load();
            if (me.serviceId){ me.onResetFlow(bundleId);}
        }
        // if (this.loadMenu) this.loadMenu.reload();
    },
    onResetFlow: function (bundleId) {
        var me = this;
        me.getEl().mask("reset flow");
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'ResetServiceFlow',
            method: 'POST',
            jsonData: {
                ufsdc: {
                    FileId: eBook.Interface.currentFile.get('Id'),
                    ServiceId: me.serviceId,
                    BundleId: bundleId,
                    Person: eBook.User.getActivePersonDataContract()
                }
            },
            callback: function (options, success) {
                if (!success) {
                    Ext.Msg.alert('Could not reset service', 'Something went wrong');
                }
                me.getEl().unmask();
            },
            scope: me
        });
    }
});

Ext.reg('eBook.Bundle.Window', eBook.Bundle.Window);


eBook.Bundle.Notifier = Ext.extend(Ext.Panel, {
    initComponent: function () {
        Ext.apply(this, {
            html: 'notifier'
        });
        eBook.Bundle.Notifier.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('bundleStatusNotifier', eBook.Bundle.Notifier);

/*
 eBook.Bundle.ItemSettings = Ext.extend(Ext.Panel, {
 initComponent: function() {
 Ext.apply(this, {
 html: 'Item settings'
 });
 eBook.Bundle.ItemSettings.superclass.initComponent.apply(this, arguments);
 }
 });
 Ext.reg('bundleItemSettings', eBook.Bundle.ItemSettings);

 eBook.Bundle.Editor = Ext.extend(Ext.Panel, {
 initComponent: function() {
 Ext.apply(this, {
 html: 'editor'
 });
 eBook.Bundle.Editor.superclass.initComponent.apply(this, arguments);
 }
 });
 Ext.reg('bundleEditor', eBook.Bundle.Editor);
 */
/*
 eBook.Bundle.Library = Ext.extend(Ext.Panel, {
 initComponent: function() {
 Ext.apply(this, {
 html: 'Library'
 });
 eBook.Bundle.Library.superclass.initComponent.apply(this, arguments);
 }
 });
 Ext.reg('bundlelibrary', eBook.Bundle.Library);
 */

