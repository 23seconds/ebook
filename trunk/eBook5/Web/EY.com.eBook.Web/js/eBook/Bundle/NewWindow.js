eBook.Bundle.NewWindow = Ext.extend(eBook.Window, {
    initComponent: function () {
        var me = this,
            selectAction = 'GetGenericBundleTemplates',
            criteriaParameter = 'ccdc';

        //If serviceId is defined, get service linked bundle templates only
        if (me.serviceId) {
            selectAction = 'GetServiceBundleTemplates';
            criteriaParameter = 'cscdc';
        }

        Ext.apply(this, {
            layout: 'form'
            , labelWidth: 200
            , width: 450
            , height: 250
            , modal: true
            , maximizable: false
            , bodyStyle: 'padding:10px;'
            , resizable: false
            , items: [
                /* {
                 xtype: 'checkbox'
                 , ref: 'includePrevious'
                 , boxLabel: eBook.Bundle.NewWindow_IncludePrevious
                 , listeners: {
                 'check': {
                 fn: this.loadLayoutType
                 , scope: this
                 }
                 }
                 , hidden: true
                 }
                 , */{
                    xtype: 'languagebox'
                    , ref: 'culture'
                    , fieldLabel: eBook.Bundle.NewWindow_Language
                    , allowBlank: false
                    , autoSelect: true
                    , anchor: '90%'
                    , value: eBook.Interface.Culture
                    , listeners: {
                        'select': {
                            fn: this.loadLayoutType
                            , scope: this
                        }
                    }
                }, {
                    xtype: 'combo'
                    , ref: 'layouttype'
                    , fieldLabel: eBook.Bundle.NewWindow_Layout
                    , name: 'layout'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Id'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: new eBook.data.JsonStore({
                        selectAction: selectAction
                        , serviceUrl: eBook.Service.bundle
                        , autoLoad: true
                        , autoDestroy: true
                        , criteriaParameter: criteriaParameter
                        , listeners: {
                            'beforeload': {
                                fn: this.updateLayoutType,
                                scope: this
                            },
                            'load':{
                                fn: this.continueLoadIfPossible,
                                scope: this
                            }
                        }
                        , fields: eBook.data.RecordTypes.FileReportList
                    })
                    , listeners: {
                        'select': {
                            fn: this.setName
                            , scope: this
                        }
                    }
                    , anchor: '90%'
                }, {
                    xtype: 'textfield',
                    ref: 'reportname',
                    name: 'Name',
                    allowBlank: false,
                    value: me.serviceId == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B' ? 'Annual Accounts' : '',
                    fieldLabel: me.serviceId ? eBook.Bundle.NewWindow_Deliverable_Name : eBook.Bundle.NewWindow_Bundle_Name,
                    anchor: '90%'
                }]
            , tbar: [{
                ref: 'createreport',
                text: me.serviceId ? eBook.Bundle.NewWindow_Deliverable_Create : eBook.Bundle.NewWindow_Bundle_Create,
                iconCls: 'eBook-bundle-ico-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onCreateReportClick,
                scope: this
            }],
            listeners: {
                afterrender: function () {
                    //remove mask if there is one
                    if (me.maskedCmp) {
                        me.maskedCmp.getEl().unmask();
                    }
                    //name is set for services
                    if (me.serviceId) {
                        me.reportname.hide();
                    }
                    //language is hidden for Annual Accounts
                    if (me.serviceId == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') {
                        me.culture.hide();
                    }
                }
            }
        });
        eBook.Bundle.NewWindow.superclass.initComponent.apply(this, arguments);
    },
    ///skip user interaction when only one type of layout is available and other fields are hidden
    continueLoadIfPossible: function() {
        var me = this;

        if(me.culture.hidden && me.reportname.hidden && me.layouttype.store.getCount() == 1)
        {
            var record = me.layouttype.store.getAt(0);
            me.layouttype.setValue(record.get("Id"));
            me.createReport();
        }
    },
    onCreateReportClick: function () {
        var me = this;
        me.createReport();
    },
    createReport: function() {
        var me = this;

        //validation
        if (!me.reportname.isValid()) {return;}
        if (!me.layouttype.isValid()) {return;}
        if (!me.culture.isValid()) {return;}

        //vars
        var fileId = eBook.Interface.currentFile.get('Id'),
            templateId = me.layouttype.getValue(),
            culture = me.culture.getValue(),
            name = me.reportname.getValue(),
            service = 'CreateBundle',
            returnObject = 'CreateBundleResult',
            params = {},
            dc =  'ccbdc',
            dcParams = {
                FileId: fileId,
                TemplateId: templateId,
                Culture: culture,
                Name: name
            };

        if (me.serviceId == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') //Annual Accounts
        {
            //Service verifies and creates a custom bundle out of 5 pdf's
            service = 'CreateAnnualAccountsDeliverable';
            dc = 'ccbpdc';
            dcParams.Person = eBook.User.getActivePersonDataContract();
            returnObject = 'CreateAnnualAccountsDeliverableResult';
        }

        //Create params object
        params[dc] = dcParams;

        //mask
        me.getEl().mask(eBook.Bundle.NewWindow_Creating, 'x-mask-loading');

        //ajax call
        eBook.CachedAjax.request({
            url: eBook.Service.bundle + service
            , method: 'POST'
            , params: Ext.encode(params)
            , callback: this.onCreatedReportCallback
            , name: name
            , scope: this
            , returnObject: returnObject
        });
    }
    , onCreatedReportCallback: function (opts, success, resp) {
        var me = this,
            returnObject = null,
            robj = null;

        if (opts.returnObject) returnObject = opts.returnObject;
        if (resp && resp.responseText) {
            robj = Ext.decode(resp.responseText);
        }

        me.getEl().unmask();
        if (success) {
            //Get the bundle result object, depending on what function you called
            if (robj && robj[returnObject]) {
                var bundle = robj[returnObject];
                //Once the bundle is created, retrieve the file service status and open edit window
                if (me.readyCallback) {
                    me.readyCallback.fn.call(me.readyCallback.scope, bundle, me.serviceId);
                }
                else {
                    var wn = new eBook.Bundle.EditWindow({bundle: bundle, serviceId: me.serviceId});
                    wn.show();
                }
            }
        } else {
            if (robj) {

                Ext.Msg.show({
                    title: 'Could not create ' + opts.name ? opts.name : '' + ' bundle',
                    msg: robj.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    minWidth: 400
                });
            }
            else {
                eBook.Interface.showResponseError(resp, this.title);
            }
        }

        me.close();
    }
    , show: function (parent) {
        this.callingParent = parent;
        eBook.Bundle.NewWindow.superclass.show.call(this);
        var pfid = eBook.Interface.currentFile.get('PreviousFileId');
        //if (!Ext.isEmpty(pfid)) this.includePrevious.show();
    }
    , loadLayoutType: function () {
        this.layouttype.clearValue();
        this.layouttype.store.load();
    }
    , updateLayoutType: function () {
        var me = this,
            cult = me.culture.getValue() != '' ? me.culture.getValue() : eBook.Interface.Culture;
        baseParams = {Culture: cult};

        //var ipf = this.includePrevious.isVisible() ? this.includePrevious.getValue() : false;
        //this.layouttype.store.baseParams = { FileId: eBook.Interface.currentFile.get('Id'), Culture: cult, IncludePrevious: ipf };

        if (me.serviceId) {
            baseParams = {ServiceId: me.serviceId, Culture: cult};
        }

        me.layouttype.store.baseParams = baseParams;
    }
    , setName: function (c, r, idx) {
        if (r) {
            if (r.get("Id") == "dd3b9dc3-e3ad-4c10-800c-069b627fca52") {
                Ext.Msg.alert('Creating a year end deliverable?', 'Please use the "Year-end Deliverables" menu, when creating a year end deliverable, in order to start the process.');
            }

            this.reportname.setValue(r.get('Name'));
        }
    }
});