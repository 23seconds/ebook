//The form view resembles the review notes window
//but it adds an overview and form panel
eBook.Bundle.Form = Ext.extend(eBook.Window, {

    initComponent: function () {
        var me = this,
            clientName = eBook.Interface.currentClient ? eBook.Interface.currentClient.get("Name") : me.clientName,
            endDate = eBook.Interface.currentFile ? eBook.Interface.currentFile.get('EndDate') : me.endDate,
            store = new eBook.data.JsonStore({
                selectAction: 'GetFileServiceLog'
                , serviceUrl: eBook.Service.file
                , criteriaParameter: 'cfsdc'
                , fields: eBook.data.RecordTypes.Log
            }),
            tpl = new Ext.XTemplate(
                '<p>',
                '<h3 style="text-align: center;">Review Notes</h3>',
                '<h4 style="text-align: center;">' + clientName + '  -  ' + me.serviceName + ' ' + endDate.format("d/m/Y") + '</h4>',
                '</p>',
                '<tpl for=".">',
                '<tpl if="[this.actionPerformed(actions,\'comment\')] &gt; -1"=>',
                '<div class="eBook-reports-logEntry">',
                '<div>',
                '<div id="personName" style="float:left;" class="eBook-reports-header eBook-reports-icon eBook-icon-checklist-24">{personName}</div>',
                '<tpl if="[this.actionPerformed(actions,\'pdfAttachment\')] &gt; -1"=>',
                '<div style="float:left;  margin-left: 20px;">(</div><div id="pdfAttachment" style="float:left;" class="eBook-reports-logEntry-pdfAttachment eBook-reports-icon -eBook-bundle-tree-pdf" pdfAttachmentId="{pdfAttachmentId}">Open attachment</div>)<div style="float:left;margin: 0px 5px;"></div>',
                '</tpl>',
                '<div style="float:right;" id="timestamp">{timestamp:date("d/m/Y H:i")}</div>',
                '</div>',
                '<div id="logEntry-center" class="eBook-reports-logEntry-center" style="clear:both"><div id="comment" class="eBook-reports-logEntry-comment-only eBook-reports-logEntry-comment">{comment}</div></div>',
                '</div>',
                '</tpl>',
                '</tpl>', {
                    actionPerformed: function (actions, action) {
                        return actions.indexOf(action);
                    }
                }
            ),
            vboxItems = [
                {
                    xtype: 'dataview',
                    itemId: 'dataviewLog',
                    ref: '../dataviewlog',
                    flex: 3,
                    tpl: tpl,
                    itemSelector: '.eBook-reports-logEntry-pdfAttachment',
                    listeners: {
                        click: function (dataview, indexn, node, e) {
                            dataview.openDocument(node.getAttribute("pdfAttachmentId"), eBook.CurrentClient);
                        }
                    },
                    store: store,
                    autoScroll: true,
                    /*overClass: 'x-view-over',
                     itemSelector: 'div.thumb-wrap',*/
                    emptyText: 'No log entries to display',
                    storeLoad: function (fileId, serviceId) {
                        if (!fileId || !serviceId) {
                            Ext.Msg.alert("failure", "failed to load comments.");
                            return;
                        }

                        var me = this,
                            params = {
                                FileId: fileId,
                                ServiceId: serviceId
                            };

                        me.getEl().mask("loading data");
                        //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                        Ext.apply(me.store.baseParams, params);

                        me.store.load({
                            params: params,
                            callback: me.onLoadSuccess,
                            scope: me
                        });
                    },
                    onLoadSuccess: function (r, options, success) {
                        var me = this;
                        console.log(r);
                        if (success) {
                            me.getEl().scroll('b', Infinity);
                            me.getEl().unmask();
                        }
                        else {
                            me.getEl().mask("Loading of data failed");
                        }
                    },
                    openDocument: function (repositoryItemId, clientId) {
                        var wn = new eBook.Repository.FileDetailsWindow({
                            repositoryItemId: repositoryItemId,
                            readOnly: false,
                            readOnlyMeta: false,
                            clientId: clientId
                        });

                        wn.show();
                        wn.loadById();
                    }
                },
                new eBook.Bundle.HtmlEditor({ref: '../htmlEditor', flex: 1, title: 'Add note'})
            ],
            hboxItems = [
                {
                    xtype: 'formOverview',
                    ref: 'formOverview',
                    flex: 1,
                    serviceUrl: me.serviceUrl,
                    fileId: me.fileId
                },
                {
                    xtype: 'panel',
                    flex: 2,
                    layout: 'vbox',
                    title: 'Review notes',
                    ref: 'rightPanel',
                    layoutConfig: {
                        align: 'stretch'
                    },
                    items: vboxItems
                }
            ],
            bbar = [
                {
                    xtype: 'fieldset',
                    style: 'border: none;',
                    ref: '../../pdfAttachmentLabel',
                    html: '<div id="eBook-toolbar-pdfAttachmentLabel eBook-Pdf-final-icon" class="eBook-toolbar-pdfAttachmentLabel">' + (me.pdfAttachmentId ? 'attachment added' : '') + '</div>'
                }, {
                    xtype: 'tbfill'
                }, {
                    text: 'Attach',
                    ref: '../../pdfAttachmentButton',
                    iconCls: 'eBook-icon-uploadpdf-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.onPdfAttachClick,
                    tooltip: 'Add and attach a PDF file to the review notes (useful for adding scanned written notes).',
                    width: 100,
                    scope: this
                }, {
                    text: 'Print',
                    ref: '../../printButton',
                    iconCls: 'eBook-iconprint-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.onPrintClick,
                    width: 100,
                    scope: this
                }];


        //If form is not null, add the form and form overview panel
        if(me.form) {
            vboxItems.splice(1, 0, me.form);
            //notes panel
            vboxItems[0].flex = 1;
            //form panel
            vboxItems[1].height = 225;
            vboxItems[1].border = false;
            vboxItems[1].parent = this;
        }

        if (me.formButtons) {
            //overwrite template handler and scope of form buttons
            for (var i = 0; i < me.formButtons.length; i++) {
                me.formButtons[i].handler = me.onSaveClick;
                me.formButtons[i].scope = me;
            }

            //Add form buttons to bottom bar
            bbar.push(me.formButtons);

        } else {
            bbar.push({
                text: 'Continue',
                ref: '../../saveButton',
                iconCls: 'eBook-yes-24',
                scale: 'medium',
                iconAlign: 'top',
                buttonAlign: 'right',
                handler: me.onSaveClick,
                width: 100,
                scope: this
            });
        }

        //Overwrite handler, utiliz

        Ext.apply(this, {
            title: me.serviceName,
            ref: 'form',
            layout: 'hbox',
            layoutConfig: {
                align: 'stretch'
            },
            bodyStyle: 'background-color:#FFF;',
            //cls: 'eBook-reports-logEntries',
            width: Ext.getBody().getViewSize().width < 1300 ? Ext.getBody().getViewSize().width : 1300,
            height: Ext.getBody().getViewSize().height - 50,
            minimizable: true,
            items: hboxItems,
            bbar: bbar,
            listeners: {
                afterrender: function (p) {
                    var me = this;

                    //retrieve the fileservice store
                    if (me.fileId && me.serviceId) {
                        me.rightPanel.getComponent("dataviewLog").storeLoad(me.fileId, me.serviceId);

                        //retrieve form data
                        if(me.form)
                        {
                            //Save form data
                            Ext.Ajax.request({
                                url: me.serviceUrl + me.getAction,
                                method: 'POST',
                                jsonData: {
                                    cidc: {
                                        'Id' : me.fileId
                                    }
                                },
                                callback: me.setFormData,
                                scope: me
                            });
                        }
                    }


                    if (me.readOnly == true) {
                        me.htmlEditor.hide();
                        me.saveButton.hide();
                    }
                },
                minimize: function (p) {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');

                    if (this.minimized) {
                        p.rightPanel.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else {
                        p.restoreSize = this.getSize();
                        p.restorePos = this.getPosition(true);
                        p.rightPanel.getComponent("dataviewLog").hide();
                        p.setSize(Ext.getBody().getViewSize().width / 5, Ext.getBody().getViewSize().height / 5);
                        p.setPosition(Ext.getBody().getViewSize().width - p.getWidth() - 10, Ext.getBody().getViewSize().height - p.getHeight() - 10)
                        p.minimized = true;
                        bundleEditWindow.hide();
                    }
                },
                maximize: function (p) {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');
                    if (this.minimized) {
                        p.rightPanel.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else {
                        p.maximize();
                    }
                },
                restore: function (p) {
                    var bundleEditWindow = Ext.WindowMgr.get('bundleEditWindow');
                    if (this.minimized) {
                        p.rightPanel.getComponent("dataviewLog").show();
                        p.setWidth(Ext.getBody().getViewSize().width < 1000 ? Ext.getBody().getViewSize().width : 1000);
                        p.setHeight(Ext.getBody().getViewSize().height - 50);
                        p.expand('', false);
                        p.center();
                        p.minimized = false;
                        bundleEditWindow.show();
                        p.toFront();
                    }
                    else {
                        p.restore();
                    }
                }
            }
        });

        eBook.Bundle.Form.superclass.initComponent.apply(this, arguments);
    },
    onSaveClick: function (btn) {
        var me = this;

        if (btn.reviewNotesRequired) {
            me.required = btn.reviewNotesRequired;
        }

        if (me.form) {
            me.submitForm(btn); //if the optional form panel is present, process flow
        }
        else {
            me.save(btn);
        }
    },
    submitForm: function (btn) {
        if (!this.form) {return;}

        //Variable data retrieved from form
        var me = this,
            jsonData = {};

        //Validate user input, if it's not a rejection
        if (btn.type == "reject" || me.form.isValid()) {
            //Retrieve user form input, create json
            jsonData[me.criteriaParameter] = me.form.getData();
            //Add file id
            jsonData[me.criteriaParameter].FileId = me.fileId;

            //Save form data
            Ext.Ajax.request({
                url: me.serviceUrl + me.updateAction,
                method: 'POST',
                jsonData: jsonData,
                callback: me.formSubmitted,
                btn: btn,
                scope: me
            });
        }
    },
    formSubmitted: function (options, success, response) {
        var me = this;

        if (success) {
            me.save(options.btn); //continue main review notes flow
        }
        else {
            Ext.Msg.alert("Issue", "There was an issue with form. Please contact the administrator.");
        }
    },
    setFormData: function (options, success, response) {
        var me = this;

        if(success && response.responseText) {
            me.form.setData(Ext.decode(response.responseText)[me.getAction + "Result"]);
        }
    },
    save: function (btn) {
        var me = this,
            comment = me.htmlEditor.getValue(),
            wrapper = document.createElement('div');

        //Quick fixes in order to avoid copy pase issues as uses tend to copy paste the entire floating window
        //Proper implementation would be preferable
        wrapper.innerHTML = comment;

        if (comment.length > 0) {
            if (comment.indexOf("x-box-item") != -1) //user copied editor
            {
                alert("Please make sure you only copy the review notes and not the editor.");
                return;
            }

            var walkDom = function walk(node, func) {
                func(node);
                node = node.firstChild;
                while (node) {
                    walk(node, func);
                    node = node.nextSibling;
                }
            };

            //remove any potential hazardous attributes in order to avoid copy paste issues
            walkDom(wrapper.firstChild, function (element) {
                if (element.removeAttribute) {
                    element.removeAttribute('id');
                    element.removeAttribute('style');
                    element.removeAttribute('class');
                }
            });

            comment = wrapper.innerHTML;
        }

        //If review not required or at least 15 characters
        if (comment.length > 15 && (me.required || me.pdfAttachmentId) || (!me.required && !me.pdfAttachmentId)) { //comment also required on pdf attachment
            if (me.readyCallback) { //it requires a readyCallback function in order to continue
                if (this.readyCallback.fnName && this.readyCallback.fnName == 'closeService') //is this function closeService
                {
                    this.readyCallback.fn.call(me.readyCallback.scope, me.readyCallback.bundle, this.readyCallback.serviceId, btn.followUpStatus, comment, null, me.pdfAttachmentId);
                }
                else {
                    //If the readyCallback function triggers another function
                    if (me.readyCallback.delegateFn) { //delegate function attached to readyCallback function?
                        //Add review notes to the to be returned parameters array
                        me.readyCallback.delegateParams.push(comment);
                        me.readyCallback.delegateParams.push(me.pdfAttachmentId);

                        me.readyCallback.fn.defer(10, me.readyCallback.scope, [me.readyCallback.delegateFn, me.readyCallback.delegateParams]);
                    }
                    else { //UpdateFileService is the common standard used function
                        me.readyCallback.fn.call(me.readyCallback.scope, me.readyCallback.bundle, me.readyCallback.serviceId, btn.followUpStatus, me.readyCallback.repositoryItem, comment, null, me.pdfAttachmentId);
                    }
                }
                me.close();
            }
        }
        else {
            Ext.Msg.alert('Review notes', 'Please add review notes.');
        }
    },
    onPrintClick: function () {
        var me = this,
            divReviewNotes = me.rightPanel.getComponent("dataviewLog").getEl().dom.innerHTML,
            wn = window.open('', 'Print version', 'height=600,width=800'),
            reportImage = 'iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAEnklEQVRYha2XS2wTVxSGv3l4PE4ysYMd8sJJwCk0bYOREkpBVBWKkCqhFjVqVaWtKvWhqII9rFlXYtlNpG5qKXSRRbtAFYRNC+VVhIoqCqUEjJsmhAYcIPF47JnbRWJje2yTiTjSyHPO/ef8v8+5544tUWKtra0cOXIERVE4+HKa8NKFbY6dyy0bA9Mn/+lnadnEqwkhAJidneX48ePYtl22rpY6o6OjjI6O9kqSdFRysro1v/UtsXxfcgLdZw/seCMvSYpnAY7jAFwXQnw9NTXF1atXawtQFAUhxGFFUb5ycjnykZ2QmkIN9vbKis8TsWmaaJqGLMvYto0QYtLv909X4soECCFwHEe1LAv5xneo3ftA9aNH+jyRA0xMTBCPx4nFYoVQ1W8gF24CPjjQM4OdWSR760ekzr2Y06dRYwdxHMfzJcsy4+PjTE9PF2OF/VBVwIHXVDZk/mgxb598J/94hsy9CwS2f4Ysy66H1mJNTU0YhkEikSCZTLKwsIDP5y5CsQXNOpgP734UCG07Q3Yxpm/sRW0Iw/J9sNKeBeiyRTAYJJvNMjk5STQa7Y7H4zfv3LnDzMyMW0A2L7DV4GXr0d2Pjf53aQiFABCXjiLjffy6zSC3Gl9F0zQsy2Jubu77wcHB/ZFI5MqxY8fcAs7csPlwZNelcHTvRcdxdhfimf/+xXk6g1frBj41LiAhaHrvB+Tg5hbTND85ceLElVJcUcDcE/gp1cUhJVCYXQCyVh47m/csYMVWnitsPl3X8fv9ZYiyMXQcB9u2UZRnB45v6/tIC3+tUwBIqo7c2F5zvdo5UFaBpqFD6yavtGpj6BJg23bNmb0/P08hHAo2o+s6AKZlYduOC+9TVTRfGYUrr6sFhTZUWsY0+Wb826L/9v5hdr++E4DL1/7kwUP3qG6JdrKj/6Wa5C4Bz6tApRUwewe3PxdTy3dVIJ/Po6rlZQOQJYnWSLjYgoaGBhfm2s3bZX441ExXW2tNcpeAUmAl2OfzcXjsy7qYShJRg/S5AioTmZZFxszWTQTQ1RYp83VN89aCWsDU7Dx/J9dxGnZs5JW+3rqYNbUgFu0kFu30LKCQq56/pgoA2LbNrxcvFf2+LZvpaF854e7NzrOUcb+wNgQN2sItL0aAlcvx87nzRV/TNNrb2oprZtZy5cjl8i9mE1bzEc9iWzZ11CR4YS1QFYU39+wuxjd1dbowp879huBZrLt9I/2xHu8Cqs24LMvs2bWzbrJd8X5K+NE0tfxcqJK3ZgVK34gPHi0yv+D9Z1k4ZNAe2VAXs6YWyJKEqnr/UyJJkvc9IIRIQXkFQkYjIaPRs4DKPNUEKAAjIyOMjY0xMDCAZVkX/X7/dcMwPlgXYx0zTfOcoiinhoeHGRoaIhAIIAEkEgmADmAP0Ae09fT0fK7rehOsYFZNcmWtbaLkUwgh8qlUaiKTyfwCXANuAY8LLZCAOPAFEAOak8kkQIaVKikl5FLJVY3UKbkXgM3Kr9McsA/oB84CCeB3FSCdTotQKHQaOA8Yq1cQ8ANNq36BsAFoqSJArApeXCUVQBZYAp6WrD1ZjZnpdJr/AXMfZUp9LpliAAAAAElFTkSuQmCC';

        wn.document.write('<html><head>');
        wn.document.write('<style TYPE="text/css">.eBook-icon-checklist-24{background-image:url(data:image/gif;base64,' + reportImage + ');}.eBook-reports-column { padding:10px; } .eBook-reports-button-column { padding: 10px; } .eBook-reports-button { margin-bottom: 4px; } .eBook-reports-row { border-color: white; width: 500% !important; } .eBook-reports-poa .x-grid3-cell-inner { line-height: 15px; padding: 3px 3px 1px 5px; } .eBook-reports-poa-row-button { border: 1px solid #C1C1C1; border-radius: 2px; cursor: pointer; text-align: center; } .eBook-reports-logEntries { top: 119px !important; } .eBook-reports-logEntries-collapsed { visibility: hidden; } .eBook-reports-logEntry { position: relative; border: 1px solid gray; border-radius: 4px; background-color: #F0F0F0; text-align: left; margin: 5px; padding: 5px; } .eBook-reports-header { font-weight: bold; height: 14px; line-height: 14px; } .eBook-reports-icon { padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; padding-bottom: 7px; background-size: 12px 12px; } .eBook-reports-logEntry-details{ width: 187px; padding: 2px 5px; vertical-align: top; } .eBook-reports-logEntry-comment{ padding: 2px 2px 2px 5px; vertical-align: top; border-left: 1px solid gray; list-style-position: inside; font: normal 13px tahoma,arial,helvetica,sans-serif; } .eBook-reports-logEntry-comment-only{ border-left: 1px solid rgb(229, 172, 23); } .eBook-reports-logEntry-comment li { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-comment ul { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-comment ol { list-style: inherit; list-style-position: inside; margin: 1em 0; padding: 0 0 0 40px; } .eBook-reports-logEntry-center { margin-left: 6px; } .eBook-reports-logEntry-footer{ padding: 5px 5px 0px 5px; } .eBook-reports-logEntry-details ul{ padding:0; } .eBook-reports-logEntry-details li { padding: 0.1em 0 0.1em 1.5em; margin-bottom: 0.2em; text-indent: 0.4em; list-style: none; background-repeat: no-repeat; background-size: 12px 12px; background-position: 2px 3px; background-image: url(../images/icons/16/tick-circle.png); }</style>');
        wn.document.write('</head><body >');
        wn.document.write(divReviewNotes);
        wn.document.write('</body></html>');
        wn.document.close(); // necessary for IE >= 10
        wn.focus(); // necessary for IE >= 10
        wn.print();
        wn.close();
    },
    onPdfAttachClick: function () {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
            today = new Date(),
            dd = ("0" + today.getDate()).slice(-2),
            mm = ("0" + (today.getMonth() + 1)).slice(-2),
            yyyy = today.getFullYear();

        wn = new eBook.Repository.SingleUploadWindow({
            clientId: eBook.CurrentClient,
            standards: {
                location: 'f344c1b4-7db5-4249-9a11-8bd6fca7aafc', //F. Review Note Attachments
                fileName: me.readyCallback.bundle.Name + '-ReviewNoteAttachment-' + yyyy + mm + dd,
                statusid: null
            },
            readycallback: {
                fn: me.onUploadSuccess,
                scope: me
            }
        });

        wn.show(me);
    },
    onUploadSuccess: function (repositoryItem) {
        var me = this;
        me.pdfAttachmentId = repositoryItem;
        //update label
        me.pdfAttachmentLabel.update('<div id="eBook-toolbar-pdfAttachmentLabel eBook-Pdf-final-icon" class="eBook-toolbar-pdfAttachmentLabel">' + (me.pdfAttachmentId ? 'attachment added' : '') + '</div>');
    }
});