///The FormOverview is simple grid panel loading all form flow related data for a partular service
eBook.Bundle.FormOverview = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            title: "Overview",
            loadMask: true,
            cls: "ebook-formOverview",
            store: new eBook.data.JsonStore({
                selectAction: 'GetFormOverview',
                criteriaParameter: 'cidc',
                autoDestroy: true,
                serviceUrl: me.serviceUrl,
                fields: eBook.data.RecordTypes.Form,
                autoLoad: false
            }),
            viewConfig: {
                forceFit: true,
                autoFill: true,
                scrollOffset: 0
            },
            listeners: {
                afterrender: function (p) {
                    //if the file id is already supplied, "autoLoad" the store
                    if(p.fileId)
                    {
                        p.loadStore(p.fileId);
                    }
                }
            },
            columns: [{
                header: "Field",
                flex: 1,
                dataIndex: 'field',
                css: "font-weight: bold;background-color: #F0F0F0;"
            }, {
                header: "Value",
                flex: 1,
                dataIndex: 'value'
            }]
        });
        eBook.Bundle.FormOverview.superclass.initComponent.apply(me, arguments);
    },
    //retrieve the fileservice form overview using the file id
    loadStore: function (fileId)
    {
        var me = this;

        me.store.load({
            params: {
                Id: fileId
            }
        });
    },

    clearStore: function ()
    {
        var me = this;

        me.store.removeAll();
    }
});
Ext.reg('formOverview', eBook.Bundle.FormOverview);
