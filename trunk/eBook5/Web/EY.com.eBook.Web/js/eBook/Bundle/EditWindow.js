
eBook.Bundle.EditWindow = Ext.extend(eBook.Window, {
    initComponent: function () {
        var me = this,
            bundleWestItems =[
                {
                    xtype: 'bundleSettings',
                    ref: '../bundleSettings',
                    serviceStatus: me.serviceStatus,
                    height: 200
                },
                {
                    xtype: 'bundleItemSettings',
                    ref: '../itemSettings',
                    flex: 1,
                    disabled: this.bundle.Locked
                }
            ];

        //Dynamic panel changes in bundle edit window
        if(me.serviceId && me.serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') //if annual accounts service, hide bundleItemSettings, add form overview
        {
            //hide bundleItemSettings
            var bundleItemSettings = bundleWestItems.filter(function( item ) {
                return item.xtype == 'bundleItemSettings';
            })[0];
            if(bundleItemSettings)
            {
                bundleItemSettings.hidden = true;
            }
            //formOverview, panel shows service specific data in grid
            bundleWestItems.push({
                xtype: 'formOverview',
                ref: '../formOverview',
                flex: 1,
                serviceUrl: eBook.Service.annualAccount,
                fileId: me.bundle.FileId
            });
        }

        Ext.apply(this, {
            layout: 'border'
            , id: 'bundleEditWindow'
            , ref: 'bundleEditWindow'
            , title: me.bundle.Name
            , items: [
                {
                    xtype: 'bundleStatusNotifier',
                    ref: 'notifier',
                    region: 'north',
                    height: 0
                },
                {
                    ref: 'bundleWest',
                    region: 'west',
                    layout: 'vbox',
                    autoScroll: false,
                    width: 400,
                    items: bundleWestItems
                },
                {
                    ref: 'bundleCenter',
                    region: 'center',
                    layout: 'border',
                    layoutconfig: {columns: 2},
                    items: [
                        {
                            /*
                             ref: 'bundleView',

                             items: [
                             {
                             */
                            xtype: 'bundleEditor',
                            region: 'center',
                            ref: '../bundleEditor',
                            autoScroll: true,
                            bundle: this.bundle,
                            bodyCssClass: 'eBook-bundle-bundleEditor'
                            /*}
                             ,
                             {
                             ref: '../../bundlePDF',
                             xtype: 'RepositoryPreview',
                             height: 600
                             }
                             ]*/
                        },
                        {
                            xtype: 'bundlelibrary',
                            ref: '../library',
                            region: 'east',
                            disabled: this.bundle.Locked,
                            width: 300,
                            serviceId: this.serviceId,
                            culture: this.bundle.Culture
                        }
                    ]
                },
                {
                    ref: 'fileServicelog',
                    xtype: 'fileServicelog',
                    region: 'east',
                    width: 300,
                    serviceId: this.serviceId,
                    fileId: this.bundle.FileId,
                    collapsible: false,
                    collapsed: false,
                    hidden: false,
                    hideCollapseTool: false,
                    animCollapse: true
                }
            ]
            , tbar: [{
                xtype: 'buttongroup',
                //ref: '../bundleSettings',
                id: this.id + '-toolbar-settings',
                ref: '../bgDraft',
                columns: 3,
                title: 'Draft',
                items: [{xtype: 'tbspacer'}, {
                    ref: '../draft',
                    text: 'DRAFT',
                    iconCls: 'eBook-icon-draftreport-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    enableToggle: true,
                    pressed: this.bundle.Draft,
                    toggleHandler: this.onToggleDraft,
                    scope: this,
                    disabled: !eBook.User.checkCloser("ACR", true) || (eBook.Interface.isFileClosed()) || this.bundle.Locked
                }, {xtype: 'tbspacer'}]
            }
                , {xtype: 'tbspacer'}
                , {
                    xtype: 'buttongroup',
                    ref: '../bgGeneralActions',
                    id: this.id + '-toolbar-general',
                    columns: 6,
                    title: eBook.Bundle.Window_GeneralActions,
                    items: [{
                        ref: '../../saveBundle',
                        text: eBook.Bundle.Window_ReportSave,
                        iconCls: 'eBook-icon-savereport-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onSaveBundleClick,
                        scope: this,
                        disabled: eBook.Interface.isFileClosed() || this.bundle.Locked
                    }, {
                        ref: '../../generateBundleMail',
                        text: 'Preview for e-mail', // eBook.Bundle.Window_ReportGenerate,
                        iconCls: 'eBook-icon-rendermail-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onGenerateBundleMailClick,
                        scope: this
                    }, {
                        ref: '../../generateBundlePrint',
                        text: 'Preview for printing', //eBook.Bundle.Window_ReportGenerate,
                        iconCls: 'eBook-icon-renderprint-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onGenerateBundlePrintClick,
                        scope: this
                    }]
                }
                /*
                 , {xtype: 'tbspacer'}
                 , {
                 xtype: 'buttongroup',
                 title: eBook.Bundle.Window_Status,
                 ref:'../tbServiceStatus',
                 html: '<div id="eBook-bundle-toolbar-status" class="eBook-bundle-toolbar-status">' + (this.serviceStatus.s ? this.serviceStatus.s : '') + '</div>'
                 }
                 */
                , {xtype: 'tbspacer'}
                , {
                    xtype: 'buttongroup',
                    itemId: 'bgReviewNotes',
                    ref: '../bgReviewNotes',
                    cls: 'eBook-bundle-bgReviewnotes',

                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    //hidden: "Champion,Administrator".indexOf(eBook.User.activeClientRoles) == -1, // for testing purposes
                    columns: 2,
                    title: eBook.Bundle.Window_ReviewNotes,
                    items: [{
                        ref: 'addComment',
                        text: eBook.Bundle.Window_AddComment,
                        iconCls: 'eBook-icon-checklist-24',
                        textAlign: 'center',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onAddReviewClick,
                        scope: this,
                        width: 102
                    }]
                }
                , {xtype: 'tbspacer'}
                , {
                    xtype: 'buttongroup',
                    ref: '../bgSpecificActions',
                    //hidden: "Champion,Administrator".indexOf(eBook.User.activeClientRole) == -1, // for testing purposes
                    width: 300,
                    //id: this.id + '-toolbar-service',
                    columns: 5,
                    title: eBook.Bundle.Window_SpecificActions,
                    items: [] //updated through addServiceSpecificButtons function
                }
            ],
            listeners: {
                afterrender: function () {
                    var me = this;

                    if (me.maskedCmp) //unmask any supplied masked component
                    {
                        me.maskedCmp.getEl().unmask();
                    }

                    if (me.serviceId) {
                        me.addServiceSpecificButtons();
                        me.updateBundleRelatedComponents();
                    }

                    //if the delivarable linked to a service has specific buttons, hide certain general buttons
                    if (me.bgSpecificActions.items.getCount() > 0) {
                        //me.bgGeneralActions.hide();
                        me.bgDraft.hide();
                    }
                    else { //hide service specific buttons
                        me.bgSpecificActions.hide();
                    }
                    if (!me.serviceId) {
                        me.bgReviewNotes.hide();
                    }
                    /*
                     if(eBook.Interface.isFileClosed() || me.bundle.Locked) {
                     me.showPdfView(true);
                     }
                     else
                     {
                     me.showPdfView(false);
                     }
                     */
                },
                beforeclose: function (window) {
                    var me = this;
                    if (!window.pendingClose && !me.bundle.Locked) {
                        window.pendingClose = true;
                        Ext.Msg.confirm(eBook.Bundle.Window_Deliverable_Save + '?',
                            'Do you want to save the deliverable?',
                            function (btnid) {
                                if (btnid == 'yes') {
                                    me.onSaveBundle();
                                }
                                window.close();
                            },
                            me
                        );
                        return false;
                    }
                    delete window.pendingclose;
                }
            }
        });
        me.closed = me.bundle.Locked;
        eBook.Bundle.EditWindow.superclass.initComponent.apply(me, arguments);
    }
    ,
    closed: false
    ,
    show: function () {
        eBook.Bundle.EditWindow.superclass.show.call(this);
    }
    ,
    compileBundle: function () {
        var tb = this.getTopToolbar();

        //var bdc = this.bundle;
        if (this.bgDraft.hidden == false) {
            this.bundle.Draft = tb.draft.pressed;
        }
        this.bundle.Contents = this.bundleEditor.getContent()
    }
    ,
    loadBundle: function (id) {
        // load bundle
    }
    ,
    loadNodeSettings: function (node) {
        this.itemSettings.loadNode(node, closed);
    }
    ,
    onGenerateBundleMailClick: function () {
        this.onGenerateBundleClick(false);
    }
    ,
    onGenerateBundlePrintClick: function () {
        this.onGenerateBundleClick(true);
    }
    ,
    onGenerateBundleClick: function (rectoVerso) {
        this.getEl().mask('Generating');

        //if bundle hidden, go with locked me.bundle settings
        this.compileBundle();

        this.bundle.RectoVerso = rectoVerso;
        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'GenerateBundle'
            , method: 'POST'
            , params: Ext.encode({bdc: this.bundle})
            , callback: this.onGenerated
            , scope: this
            , timeout: 1000 * 120
        });
    }
    ,
    onGenerated: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GenerateBundleResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    ,
    onSaveBundleClick: function () {
        var me = this;
        me.onSaveBundle();
    }
    ,
    onSaveBundle: function (callback, callBackAgrs) {
        var me = this;
        //if optional args callback isn't specified, set to null
        callback = (typeof callback === "undefined") ? null : callback;
        callBackAgrs = (typeof callBackAgrs === "undefined") ? null : callBackAgrs;

        me.getEl().mask('Saving');
        me.compileBundle();
        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'SaveBundle'
            , method: 'POST'
            , params: Ext.encode({bdc: me.bundle})
            , callback: this.onSaved.createDelegate(me, [callback, callBackAgrs], true)
            , scope: me
        });
    }
    ,
    onSaved: function (opts, success, resp, callback, callbackArgs) {
        var me = this;
        me.getEl().unmask();
        if (!success) {
            eBook.Interface.showResponseError(resp, me.title);
        }
        if (callback) {
            callback.apply(this, callbackArgs);
        }
    }
    ,
    onCommitBundleClick: function (btn) {
        var me = this,
            fn = me.onCommitBundle;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    }
    ,
    onCommitBundle: function (btn) {
        var me = this;
        //if bundle valid, allow user to enter comment and continue flow
        if (!me.bundle.Locked) {
            me.onSaveBundle(me.validateBundle, [me.bundle.Id, me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]]);
        }
        else {
            me.validateBundle(me.bundle.Id, me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]);
        }
    },
    onSubmitFormClick: function (btn) {
        var me = this,
            fn = me.onSubmitForm;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    },
    onSubmitForm: function (btn) {
        var me = this,
            form = null,
            formObj = {};

        //If any, create form button property
        if (btn.formButtons) {
            formObj.formButtons = me.createServiceSpecificButtons(btn.formButtons);
        }

        //Check what the follow-up status is
        if (me.serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') {
            var formConfig = {
                fileId: me.bundle.FileId,
                serviceId: me.bundle.serviceId
            };
            //Form properties
            formObj.serviceUrl = eBook.Service.annualAccount;
            formObj.updateAction = 'UpdateAnnualAccounts';
            formObj.getAction = 'GetAnnualAccounts';
            formObj.criteriaParameter = 'cfaadc';
            formObj.serviceName = "Filing NBB";
            formObj.fileId = me.bundle.FileId;
            formObj.serviceId = me.serviceId;


            //Create instance of form
            switch (btn.followUpStatus) {
                case 2:
                    formObj.form = new eBook.Forms.AnnualAccounts.Staff(formConfig);
                    required = true;
                    break;
                case 3:
                    formObj.form = new eBook.Forms.AnnualAccounts.Manager(formConfig);
                    required = true;
                    break;
                //case 5: Partner fields moved to manager
                    //formObj.form = new eBook.Forms.AnnualAccounts.Partner(formConfig);
                    //required = true;
                    //break;
            }
        }

        //Open form, containing a subform and form overview panel, after save (not on lock) and validation
        if (!me.bundle.Locked) {
            //bundle, serviceId, repositoryItemId, readyCallback, formObj
            me.onSaveBundle(me.validateBundle, [me.bundle.Id, me.showForm, [me.bundle, me.serviceId, btn.followUpStatus, null, null, formObj]]);
        }
        else {
            me.validateBundle(me.bundle.Id, me.showForm, [me.bundle, me.serviceId, btn.followUpStatus, null, null, formObj]);
        }
    }
    ,
    onApproveBundleClick: function (btn) {
        var me = this,
            fn = me.onApproveBundle;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }

        //If OV, ask confirmation about
        /*if (Ext.Array.contains(eBook.User.activeClientRoles, "OfficeValidator") && !Ext.Array.contains(eBook.User.activeClientRoles, "OfficeValidator")) {
         Ext.Msg.show({
         title: 'Do you have a signed accounting memorandum?',
         msg: 'Do you have an accounting memorandum signed by a partner in your possession?',
         buttons: Ext.Msg.YESNO,
         fn: function (btnid) {
         if (btnid == 'yes') {
         //log
         me.logFileServiceAction(me.bundle.FileId, me.serviceId, 'ConfirmationMemorandumSigned', null);
         me.onApproveBundle(btn);
         }
         },
         scope: me,
         icon: Ext.MessageBox.QUESTION
         });
         }
         else {
         me.onApproveBundle(btn);
         }*/
    }
    ,
    onApproveBundle: function (btn) {
        var me = this;
        //if bundle valid, allow user to enter comment and continue flow
        if (!me.bundle.Locked) {
            me.onSaveBundle(me.validateBundle, [me.bundle.Id, me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]]);
        }
        else {
            me.validateBundle(me.bundle.Id, me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]);
        }
    }
    ,
    onRejectBundleClick: function (btn) {
        var me = this,
            fn = me.onRejectBundle;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    }
    ,
    onRejectBundle: function (btn) {
        var me = this;
        //if bundle valid, compel user to enter comment and continue flow
        if (!me.bundle.Locked) {
            me.onSaveBundle(me.showReviewNotes, [btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null]);
        }
        else {
            me.showReviewNotes(btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, null);
        }
    },
    onUploadBundleClick: function (btn) {
        var me = this,
            fn = me.checkFolderForExistingFile;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    },
    ///Check if the location folder already contains a file with suplied status
    checkFolderForExistingFile: function (btn) {
        var me = this,
            params = {
                cid: eBook.CurrentClient,
                sid: btn.location,
                ps: eBook.Interface.currentFile.get('StartDate'),
                pe: eBook.Interface.currentFile.get('EndDate')
            };

        //optionals
        if(btn.repositoryStatusId)
        {
            params.st = btn.repositoryStatusId.toLowerCase();
        }

        me.getEl().mask("Searching for any existing files");

        Ext.Ajax.request({
            url: eBook.Service.repository + 'QueryFiles',
            method: 'POST',
            params: Ext.encode({
                cqfdc: params
            }),
            callback: this.onFolderExistingFilesRetrieved,
            scope: this,
            btn: btn
        });
    },
    onFolderExistingFilesRetrieved: function (options, success, response) {
        var me = this,
            filename = null,
            repositoryStatusId = null,
            btn = options.btn,
            existingItemId = null;

        //optionals
        if (btn.filename)  {filename = btn.filename;}
        if (btn.repositoryStatusId) {repositoryStatusId = btn.repositoryStatusId;}

        me.getEl().unmask();
        if (success) {
            var files = Ext.decode(response.responseText).QueryFilesResult;

            if (files.length > 0) {
                existingItemId = files[0].id;
                Ext.Msg.show({
                    title: "Replace file?",
                    msg: String.format(eBook.Repository.Window_ReplaceMsg, files[0].text),
                    buttons: Ext.Msg.YESNO,
                    fn: function (btnid) {
                        if (existingItemId && btnid == 'yes') {
                            me.openUploadWindow(btn.location, btn.filename, repositoryStatusId, btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, existingItemId);
                        }
                    },
                    scope: me,
                    icon: Ext.MessageBox.QUESTION
                });
            }
            else {
                me.openUploadWindow(btn.location, btn.filename, repositoryStatusId, btn.reviewNotesRequired, me.bundle, me.bundle.FileId, me.serviceId, btn.followUpStatus, existingItemId);
            }
        } else {
            eBook.Interface.showResponseError(response, me.title);
        }
    },
    onToggleBundleClosureClick: function (btn) {
        var me = this,
            fn = me.onToggleBundleClosure;

        if (btn.confirmation) {
            me.onConfirmationRequired(btn, fn);
        }
        else {
            fn.call(me,btn);
        }
    },
    onToggleBundleClosure: function (btn) {
        var me = this;

        if (me.bundle.Locked) {
            me.reopenService(me.bundle, me.serviceId, btn.followUpStatus); //open service & bundle
        }
        else {

            var readyCallback = { //Close service & bundle
                fn: me.closeService,
                fnName: 'closeService',
                scope: me,
                bundle: me.bundle,
                serviceId: me.serviceId,
                status: btn.followUpStatus
            };

            me.onSaveBundle(me.validateBundle, [me.bundle.Id, me.showReviewNotes, [true, me.bundle, me.bundle.FileId, me.serviceId, null, null, readyCallback]]);
        }
    },
    //A button may contain a confirmation property which defines that the user has to make a confirmation depending on the evaluation
    onConfirmationRequired: function (btn, fn) {
        var me = this,
            condition = btn.confirmation.condition, //If condition
            question = btn.confirmation.question, //Question to be asked
            action = btn.confirmation.logAction; //Confirmation action to be logged

        try {
            required = eval(condition);
        }
        catch (exception) {
            Ext.Msg.alert("Eror", "Evaluation for confirmation failed");
            return;
        }

        if (required) {
            Ext.Msg.show({
                title: 'Confirmation required',
                msg: question,
                buttons: Ext.Msg.YESNO,
                fn: function (btnid) {
                    if (btnid == 'yes') {
                        me.logFileServiceAction(me.bundle.FileId, me.serviceId, action, null); //log
                        fn.call(me,btn);
                    }
                },
                scope: me,
                icon: Ext.MessageBox.QUESTION
            });
        }
        else {
            fn.call(me,btn);
        }
    },
    onResetFlow: function () {
        var me = this;
        me.getEl().mask("Resetting flow");
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'ResetServiceFlow'
            , method: 'POST'
            , jsonData: {
                ufsdc: {
                    FileId: me.bundle.FileId,
                    ServiceId: me.serviceId,
                    BundleId: me.bundle.Id,
                    Person: eBook.User.getActivePersonDataContract()
                }
            }
            , callback: function (options, success) {
                if (success) {
                    Ext.Msg.alert('flow reset executed');
                    me.close();
                }
                else {
                    Ext.Msg.alert('Could not reset service', 'Something went wrong');
                }
                me.getEl().unmask();
            }
            , scope: me
        });
    },
    closeService: function (bundle, serviceId, status, comment, pdfAttachmentId) {
        var me = this;
        me.getEl().mask("Closing service and bundle");
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'CloseService'
            , method: 'POST'
            , jsonData: {
                cfspdc: {
                    FileId: bundle.FileId,
                    ServiceId: serviceId,
                    Status: status,
                    BundleId: bundle.Id,
                    Person: eBook.User.getActivePersonDataContract()
                }
            }
            , callback: function (options, success) {
                if (success) {
                    me.updateFileService(me.bundle, me.serviceId, null, null, comment, true, pdfAttachmentId); //log locked
                }
                else {
                    Ext.Msg.alert('Could not close service', 'Something went wrong');
                }
            }
            , scope: me
        });
    },
    reopenService: function (bundle, serviceId, status) {
        var me = this;
        me.getEl().mask("Reopening service and deliverable");
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'ReOpenService'
            , method: 'POST'
            , jsonData: {
                cfspdc: {
                    FileId: bundle.FileId,
                    ServiceId: serviceId,
                    Status: status,
                    BundleId: bundle.Id,
                    Person: eBook.User.getActivePersonDataContract()
                }
            }
            , callback: function (options, success) {
                if (success) {
                    me.updateFileService(me.bundle, me.serviceId, null, null, null, false, null); //log open
                }
                else {
                    Ext.Msg.alert('Could not reopen service', 'Something went wrong');
                }
            }
            , scope: me
        });
    },
    onAddReviewClick: function (btn) {
        var me = this;

        me.showReviewNotes(false, me.bundle, me.bundle.FileId, me.serviceId, null, null);
    },
    validateBundle: function (bundleId, callback, callbackArgs) {
        var me = this,
            errorMessage = null,
            title = 'Cannot continue. Not valid.',
            templateIds = ['43C27DBE-AE64-4208-BD57-060553D41EC8', 'C29A94BF-BB06-466D-9054-5B2C632E1D97'];

        me.getEl().mask('Validating');

        Ext.Ajax.request({
            url: eBook.Service.bundle + 'ValidateBundle'
            , method: 'POST'
            , params: Ext.encode({id: bundleId})
            , callback: function (opts, success, resp) {
                if (success) {
                    var me = this,
                        obj = Ext.decode(resp.responseText).ValidateBundleResult;

                    if (!obj.Valid) {
                        errorMessage = obj.Fault;
                    }

                    //If the interface shows any upload buttons, change the icon if the validation returns that it has been added
                    var serviceButtons = this.bgSpecificActions.items.items;
                    if (serviceButtons.length > 0) {
                        //Get upload buttons
                        var uploadButtons = serviceButtons.filter(function (obj) {
                            return obj.refName == "uploadBundle";
                        });

                        for (var i = 0; i < uploadButtons.length; i++) {
                            //Check if the filename property of the button, with "Signed" added and spaces removed, is a property
                            //of the validation return and if the value is true, set the icon of the button to added.
                            if (obj[this.bgSpecificActions.items.items[i].validationId] == true) {
                                uploadButtons[i].setIconClass('eBook-repository-added-ico-24');
                            }
                        }
                    }
                } else {
                    errorMessage = 'Something went wrong. Please contact eBook Questions';
                }

                me.getEl().unmask();

                //if there is an error message, prompt the user
                if (errorMessage) {
                    Ext.Msg.alert(title, errorMessage);
                }

                if (obj.Valid && callback) {
                    callback.apply(this, callbackArgs);
                }
            }
            , scope: me
        });
    },
    showForm: function (bundle, serviceId, statusId, repositoryItemId, readyCallback, formObj) {
        var me = this,
            wn = null;

        //if optional args aren't specified, set to null
        readyCallback = (typeof readyCallback === "undefined") ? null : readyCallback;

        if (!readyCallback) {
            readyCallback = {
                fn: me.updateFileService,
                scope: me,
                bundle: bundle,
                serviceId: serviceId,
                statusId: statusId,
                repositoryItemId: repositoryItemId
            };
        }

        formObj.readyCallback = readyCallback;
        wn = new eBook.Bundle.Form(formObj);
        wn.show();
    }
    ,
    showReviewNotes: function (required, bundle, fileId, serviceId, statusId, repositoryItemId, readyCallback) {
        var me = this,
            wn = null,
            obj = {
                required: required,
                fileId: fileId,
                serviceId: serviceId
            };

        //if optional args aren't specified, set to null
        readyCallback = (typeof readyCallback === "undefined") ? null : readyCallback;

        if (!readyCallback) {
            readyCallback = {
                fn: me.updateFileService,
                scope: me,
                bundle: bundle,
                serviceId: serviceId,
                statusId: statusId,
                repositoryItemId: repositoryItemId
            };
        }

        obj.readyCallback = readyCallback;
        wn = new eBook.Bundle.ReviewNotesWindow(obj);
        wn.show();
    }
    ,
    openUploadWindow: function (location, filename, repositoryStatusId, required, bundle, fileId, serviceId, statusId, existingItemId) {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
            standards = {
                location: location
            };

        //optionals
        if (filename) standards.fileName = filename;
        if (repositoryStatusId) standards.statusid = repositoryStatusId;

        var wn = new eBook.Repository.SingleUploadWindow({
            clientId: eBook.CurrentClient,
            existingItemId: existingItemId,
            changeFileName: true,
            standards: standards,
            readycallback: {
                fn: me.onUploadSuccess,
                scope: me,
                required: required,
                bundle: bundle,
                fileId: fileId,
                serviceId: serviceId,
                statusId: statusId,
                filename: filename
            }
        });

        wn.show(me);
    }
    ,
    onUploadSuccess: function (required, bundle, fileId, serviceId, statusId, repositoryItemId, filename) {
        var me = this;

        //retrieve filename for logging purposes (should be replaced by the new filetype prop, but this requires a data fix)
        if (filename) {
            //log upload using filename
            filename = filename.replace(/ /g, '');
            filename = filename.substring(0, 1).toLowerCase() + filename.substring(1);
        }
        else {
            if (serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') //AnnualAccounts
            {
                filename = 'annualAccounts';

                //Update annual accounts record
                me.onNBBUploaded(fileId, repositoryItemId);
            }
        }

        //log action
        me.logFileServiceAction(fileId, serviceId, filename, repositoryItemId);

        var uploadBtnOnly = true;
        me.bgSpecificActions.items.each(function (btn) {
            if (btn.type != "upload") uploadBtnOnly = false;
        }, me);

        //If the step only contains upload buttons, check if bundle is valid in order to continue fileservice flow
        //Any other button has the advantage when it comes to proceeding the flow, hence no further action is taken.
        if (uploadBtnOnly) {
            me.validateBundle(bundle.Id, me.showReviewNotes, [required, bundle, fileId, serviceId, statusId, repositoryItemId]);
        }
    }
    ,
    onNBBUploaded: function (fileId, repositoryItemId) {
        var me = this;
        Ext.Ajax.request({
            url: eBook.Service.annualAccount + 'UpdateRepositoryItem',
            method: 'POST',
            jsonData: {
                cfaadc: {
                    FileId: fileId,
                    RepositoryItemId: repositoryItemId
                }
            },
            callback: function (options, success) {
                if (!success) {
                    Ext.Msg.alert("update failed", "Could not update annual accounts repository item");
                }
            },
            scope: me
        });
    }
    ,
    logFileServiceAction: function (fileId, serviceId, actions, repositoryItemId) {
        var me = this,
            ufsdc = {
                FileId: fileId,
                ServiceId: serviceId,
                Actions: actions,
                RepositoryItem: repositoryItemId,
                Person: eBook.User.getActivePersonDataContract()
            };

        //call
        Ext.Ajax.request({
            url: eBook.Service.file + 'LogFileServiceActions',
            method: 'POST',
            jsonData: {
                ufsdc: ufsdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText) {
                    me.refreshLog(me.bundle.FileId, me.serviceId)
                } else {
                    eBook.Interface.showResponseError(response, me.title);
                    me.getEl().unmask();
                }
            },
            scope: me
        });
    }
    ,
    updateFileService: function (bundle, serviceId, statusId, repositoryItemId, comment, locked, pdfAttachment) {
        //Although all three are optional, at least one must be filled in
        if (!statusId && !repositoryItemId && !comment && (locked == null)) return;

        var me = this,
        //file service update object
            ufsdc = {
                FileId: bundle.FileId,
                ServiceId: serviceId,
                BundleId: bundle.Id,
                Person: eBook.User.getActivePersonDataContract()
            };

        me.getEl().mask('Updating');

        //set optional updates
        if (statusId) ufsdc.Status = statusId;
        if (repositoryItemId) ufsdc.RepositoryItem = repositoryItemId;
        if (comment) ufsdc.Comment = comment;
        if (locked != null) ufsdc.Locked = locked;
        if (pdfAttachment) ufsdc.PdfAttachment = pdfAttachment;

        //call
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'UpdateFileService',
            method: 'POST',
            jsonData: {
                ufsdc: ufsdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText && Ext.decode(response.responseText).UpdateFileServiceResult) {
                    me.onUpdateFileServiceSuccess(Ext.decode(response.responseText).UpdateFileServiceResult);
                } else {
                    eBook.Interface.showResponseError(response, me.title);
                    me.getEl().unmask();
                }
            },
            scope: me
        });
    },
    //handle successful update of file service
    onUpdateFileServiceSuccess: function (serviceStatus) {
        var me = this;
        //Update serviceStatus
        me.serviceStatus = serviceStatus;
        //Update status
        me.updateServiceStatus();
        //Refresh log
        me.refreshLog(me.bundle.FileId, me.serviceId);
        //Update buttons
        me.addServiceSpecificButtons();
        //Optional: update formOverview
        if(me.formOverview) {me.formOverview.loadStore(me.bundle.FileId)};

        //Check if bundle is open
        me.getBundle(me.bundle.Id);

        eBook.Interface.updateFile(eBook.Interface.currentFile.get('Id'));
    },
    getBundle: function (bundleId) {
        if (this.getEl())
            this.getEl().mask('Loading');

        eBook.CachedAjax.request({
            url: eBook.Service.bundle + 'GetBundle'
            , method: 'POST'
            , params: Ext.encode({id: bundleId})
            , callback: this.onGetBundleCallBack
            , scope: this
        });
    },
    onGetBundleCallBack: function (options, success, response) {
        var me = this,
            robj = Ext.decode(response.responseText),
            bundle = robj.GetBundleResult;
        me.bundle = bundle;
        me.updateBundleRelatedComponents();
    },
    ///Refresh anything related on bundle status
    updateBundleRelatedComponents: function () {
        var me = this;

        //Handle general buttons and panels
        if (me.bundle.Locked) {
            me.saveBundle.disable();
            me.library.disable();
        }
        else {
            me.saveBundle.enable();
            me.library.enable();
        }

        //Handle specific buttons
        me.bgSpecificActions.items.each(function (btn) {
            if (me.bundle.Locked && btn.type == "lock") { //set unlock UI
                btn.setIconClass('eBook-unlock-24');
                btn.setText(eBook.Bundle.Window_Unlock);
                me.bgSpecificActions.setWidth(132);
            }
            else if (btn.type == "lock") { //set lock UI
                btn.setIconClass('eBook-lock-24');
                btn.setText(eBook.Bundle.Window_Lock);
            }
            else {
                if (!me.bundle.Locked && btn.followUpStatus > 2) { //not locked beyond status 1
                    btn.disable();
                    btn.setTooltip("Bundle must be closed.");
                }
                else {
                    if (Ext.Array.similarity(btn.arrFlowAllowedRoles, eBook.User.activeClientRoles) || btn.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1) {
                        btn.enable();
                    }

                    btn.setTooltip("The client roles " + btn.arrFlowAllowedRoles.join("/") + " can perform this action.");
                }
            }
        }, this);

        me.getEl().unmask();

        /*
         if (me.bundle.Locked) {
         me.saveBundle.disable();
         me.library.disable();
         //me.showPdfView(true);
         if (me.lockBundle) {
         me.lockBundle.setIconClass('eBook-unlock-24');
         me.lockBundle.setText(eBook.Bundle.Window_Unlock);
         me.bgSpecificActions.setWidth(132);
         }
         if (me.commitBundle)
         {
         if(Ext.Array.similarity(me.commitBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.commitBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1)
         {
         me.commitBundle.enable();
         me.commitBundle.setTooltip("The client roles " + me.commitBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         if (me.submitBundle)
         {
         if(Ext.Array.similarity(me.submitBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.submitBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1)
         {
         me.submitBundle.enable();
         me.submitBundle.setTooltip("The client roles " + me.commitBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         if (me.rejectBundle) {
         if(Ext.Array.similarity(me.rejectBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.rejectBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1) {
         me.rejectBundle.enable();
         me.rejectBundle.setTooltip("The client roles " + me.rejectBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         if (me.approveBundle) {
         if(Ext.Array.similarity(me.approveBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.approveBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1) {
         me.approveBundle.enable();
         me.approveBundle.setTooltip("The client roles " + me.approveBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         if (me.uploadBundle) {
         if(Ext.Array.similarity(me.uploadBundle.arrFlowAllowedRoles,eBook.User.activeClientRoles) || me.uploadBundle.arrFlowAllowedRoles.indexOf(eBook.User.activeRole) != -1) {
         me.uploadBundle.enable();
         me.uploadBundle.setTooltip("The client roles " + me.uploadBundle.arrFlowAllowedRoles.join("/") + " can perform this action.");
         }
         }
         }
         else {

         //me.showPdfView(false);
         if (me.lockBundle) {
         me.lockBundle.setIconClass('eBook-lock-24');
         me.lockBundle.setText(eBook.Bundle.Window_Lock);
         }
         //bundle must be locked after when status > 2
         if (me.commitBundle && me.commitBundle.followUpStatus > 2) {
         me.commitBundle.disable();
         me.commitBundle.setTooltip("Bundle must be closed.");
         }
         if (me.submitBundle) {
         me.submitBundle.disable();
         me.submitBundle.setTooltip("Bundle must be closed.");
         }
         if (me.rejectBundle) {
         me.rejectBundle.disable();
         me.rejectBundle.setTooltip("Bundle must be closed.");
         }
         if (me.approveBundle) {
         me.approveBundle.disable();
         me.approveBundle.setTooltip("Bundle must be closed.");
         }
         if (me.uploadBundle) {
         me.uploadBundle.disable();
         me.uploadBundle.setTooltip("Bundle must be closed.");
         }
         }
         //  eBook.Interface.redrawWorksheetsView();
         // me.bgSpecificActions.doLayout();
         */
        me.getEl().unmask();
    },
    //Add service specific buttons to the toolbar
    addServiceSpecificButtons: function () {
        var me = this,
            arrServiceSpecificButtons = null;

        //Get buttons
        arrServiceSpecificButtons = me.createServiceSpecificButtons(me.serviceStatus.fua);

        me.getTopToolbar().remove(me.bgSpecificActions);
        if (arrServiceSpecificButtons) {
            me.getTopToolbar().add({
                xtype: 'buttongroup',
                ref: '../bgSpecificActions',

                width: arrServiceSpecificButtons.length * 130,
                //id: this.id + '-toolbar-service',
                columns: arrServiceSpecificButtons.length,
                title: eBook.Bundle.Window_SpecificActions,
                items: arrServiceSpecificButtons //updated through addServiceSpecificButtons function
            });
        }
        me.doLayout(false, true);
        return true;
    },
    //Within the service_statuses table, each service step has its buttons and actions on those buttons defined
    //These followUpActions are merged with their button templates and returned
    createServiceSpecificButtons: function (arrFollowUpActions) {
        //vars
        var me = this,
        //button templates
            serviceSpecificButtonTemplates = {
                //Proceed to next step, backend executes necessary actions
                commit: {
                    ref: '../../commitBundle',
                    text: eBook.Bundle.Window_Submit,
                    iconCls: 'eBook-yes-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onCommitBundleClick,
                    scope: me
                },
                //Proceed to next step, after frontend interaction (e.x. form)
                submit: {
                    ref: '../../submitBundle',
                    text: eBook.Bundle.Window_Submit,
                    iconCls: 'eBook-yes-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onSubmitFormClick,
                    scope: me
                },
                //Return to certain step
                reject: {
                    ref: '../../rejectBundle',
                    text: eBook.Bundle.Window_Reject,
                    iconCls: 'eBook-no-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onRejectBundleClick,
                    disabled: !me.bundle.Locked,
                    scope: me
                },
                //Proceed to next step
                approve: {
                    ref: '../../approveBundle',
                    text: eBook.Bundle.Window_Approve,
                    iconCls: 'eBook-yes-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onApproveBundleClick,
                    disabled: !me.bundle.Locked,
                    scope: me
                },
                //Upload file and potentially proceed to next step after validateBundle returns valid
                upload: {
                    ref: '../../uploadBundle',
                    text: eBook.Bundle.Window_Upload,
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: me.onUploadBundleClick,
                    scope: me
                },
                //Lock or unlock bundle
                lock: {
                    ref: '../../lockBundle',
                    text: me.bundle.Locked ? eBook.Bundle.Window_Unlock : eBook.Bundle.Window_Lock,
                    iconCls: me.bundle.Locked ? 'eBook-unlock-24' : 'eBook-lock-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    tooltip: 'Temporary unlock of the file and bundle. Year end flow cannot be completed while unlocked.',
                    handler: me.onToggleBundleClosureClick,
                    scope: me
                }
            },
            arrServiceSpecificButtons = [],
            iServiceSpecificButtons = arrServiceSpecificButtons.length,
        //Default roles
            arrAlwaysAllowedRoles = ["Administrator", "Admin"];

        //Eval
        arrFollowUpActions = eval(arrFollowUpActions);

        //Merge service action, roles and button template
        if (arrFollowUpActions.length > 0) {
            for (i = 0; i < arrFollowUpActions.length; i++) {
                //Add template buttons based on the serviceStatus object type retrieved through the FileServiceStatus call
                arrServiceSpecificButtons.push(Object.create(serviceSpecificButtonTemplates[arrFollowUpActions[i].type]));
                //Merge all template and service specific database button properties
                for (var prop in arrFollowUpActions[i]) arrServiceSpecificButtons[i + iServiceSpecificButtons][prop] = arrFollowUpActions[i][prop];
                //Disable if active client role is not allowed
                var arrFlowAllowedRoles = arrFollowUpActions[i].activation.split(",").concat(arrAlwaysAllowedRoles);
                arrServiceSpecificButtons[i + iServiceSpecificButtons].arrFlowAllowedRoles = arrFlowAllowedRoles; //Add the always allowed roles to the flow roles
                arrServiceSpecificButtons[i + iServiceSpecificButtons].disabled = !eBook.Interface.isActiveRoleAllowed(arrFlowAllowedRoles) && !Ext.Array.similarity(arrFlowAllowedRoles, eBook.User.activeClientRoles); //disable button of both the activeRole and activeClientRole aren't within the allowed list
                arrServiceSpecificButtons[i + iServiceSpecificButtons].tooltip = "The client roles " + arrFlowAllowedRoles.join("/") + " can perform this action.";
            }
        }

        return arrServiceSpecificButtons;
    },
    //updates the status shown in the toolbar
    updateServiceStatus: function () {
        var me = this;
        //update html of status buttongroup
        me.fsServiceStatus.update('<div id="eBook-bundle-toolbar-status" class="eBook-bundle-toolbar-status">' + (this.serviceStatus.s ? this.serviceStatus.s : '') + '</div>');
    },
    //refresh fileservice log
    refreshLog: function (fileId, serviceId) {
        var me = this;

        me.fileServicelog.getComponent("dataviewLog").storeLoad(fileId, serviceId);
    },
    showPdfView: function (show) {
        var me = this;
        if (show) {
            if (me.bundle.Filepath) {
                me.bundlePDF.loadFile(me.bundle.Filepath)
                me.bundlePDF.show();
            }
        }
        else {
            //hide component
            me.bundlePDF.hide();
        }
    }
});
