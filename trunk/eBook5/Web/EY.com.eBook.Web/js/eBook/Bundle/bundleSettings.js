eBook.Bundle.bundleSettings = Ext.extend(Ext.FormPanel, {
    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            title: String.format(eBook.Bundle.NodeInfoPanel_BundleSettingsTitle, ""),
            width: 400,
            labelAlign: 'left',
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Status',
                    ref: '../../fsServiceStatus',
                    html: '<div id="eBook-bundle-toolbar-status" class="eBook-bundle-toolbar-status">' + (this.serviceStatus && this.serviceStatus.s ? this.serviceStatus.s : '') + '</div>'
                },
                {
                    xtype: 'fieldset',
                    title: 'Customer preference',
                    ref: '../../fsCustomerPreference',
                    items: [
                        {
                            xtype: 'radiogroup',
                            ref: 'rgCustomerReceivalPreference',
                            columns: 1,
                            hideLabel: true,
                            items: [
                                {
                                    boxLabel: 'E-mail',
                                    id: 'rCustomerMailPreference',
                                    ref: 'rCustomerMailPreference',
                                    name: 'rgCustomerReceivalPreference',
                                    inputValue: 'mail',
                                    checked: true
                                },
                                {
                                    boxLabel: 'Printed copy',
                                    id: 'rCustomerPrintPreference',
                                    ref: 'rCustomerPrintPreference',
                                    name: 'rgCustomerReceivalPreference',
                                    inputValue: 'print'
                                }
                            ],
                            listeners: {
                                afterrender: function (radiogroup) {
                                    Ext.Ajax.request({
                                        url: eBook.Service.client + 'GetPrintPreference',
                                        method: 'POST',
                                        jsonData: {
                                            cidc: {
                                                'Id' : eBook.CurrentClient
                                            }
                                        },
                                        callback: function (options, success, response) {
                                            if (success && response.responseText) {
                                                if(Ext.decode(response.responseText).GetPrintPreferenceResult)
                                                {
                                                    radiogroup.setValue('print');
                                                }
                                                else
                                                {
                                                    radiogroup.setValue('mail');
                                                }
                                            }
                                            else {
                                                Ext.Msg.alert('Could not retrieve client print preference.', 'Something went wrong');
                                            }
                                        },
                                        scope: me
                                    });
                                },
                                change: function (radiogroup,radio) {
                                    Ext.Ajax.request({
                                        url: eBook.Service.client + 'SetPrintPreference',
                                        method: 'POST',
                                        jsonData: {
                                            ciabdc: {
                                                'Id': eBook.CurrentClient,
                                                'Boolean': radio.inputValue == 'print' ? true : false
                                            }
                                        },
                                        callback: function (options, success, resp) {
                                            if (!success || !resp.responseText) {
                                                Ext.Msg.alert('Could not retrieve client print preference.', 'Something went wrong');
                                            }
                                        },
                                        scope: me
                                    });
                                }
                            }
                        }
                    ]
                }
            ]
        });

        eBook.Bundle.bundleSettings.superclass.initComponent.apply(me, arguments);
    }
});


Ext.reg('bundleSettings', eBook.Bundle.bundleSettings);