


eBook.Bundle.AsyncNode = Ext.extend(Ext.tree.AsyncTreeNode, {
    constructor: function (config, closed) {
        var cfg = {
            properties: config.properties ? config.properties : config
        };
        cfg.allowChildren = false;
        cfg.ui = eBook.Bundle.NodeUI;
        cfg.allowDrag = true;
        cfg.text = config.title ? config.title : config.text;
        cfg.leaf = true;
        config.type = config.properties ? config.properties.type : config.type;

        if (!Ext.isDefined(config.__type)) config.__type = config.type ? config.type : "ROOT";
        switch (config.__type) {
            case 'ROOT':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = true;
                cfg.allowDrop = true;
                cfg.allowDrag = false;
                cfg.iconCls = 'eBook-bundle-tree-bundle ';
                if (closed) cfg.iconCls += 'ebook-bundle-tree-closed ';
                cfg.leaf = false;
                cfg.readOnly = false;
                cfg.children = config.properties ? config.children : config.Index.items;
                cfg.expanded = true;
                cfg.properties.type = 'ROOT';
                break;
            case 'LIBRARY':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = false;
                cfg.allowDrop = false;
                cfg.allowDrag = false;
                cfg.iconCls = '';
                cfg.leaf = false;
                cfg.readOnly = true;
                cfg.children = config.folders;
                cfg.properties.type = 'LIBRARY';
                break;

            case 'FOLDER':
                cfg.leaf = false;
                cfg.allowChildren = true;
                cfg.isTarget = false;
                cfg.allowDrop = false;
                cfg.allowDrag = false;
                cfg.icon = '';
                cfg.leaf = false;
                cfg.readOnly = true;
                cfg.children = config.items;
                cfg.properties.type = 'FOLDER';
                break;
            case 'ChapterDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'CHAPTER':
                cfg.allowChildren = true;
                cfg.isTarget = true;
                cfg.allowDrop = true;
                cfg.iconCls = 'eBook-bundle-tree-chapter';
                cfg.leaf = false;
                cfg.children = config.properties ? config.children : config.items;
                cfg.properties.type = 'CHAPTER';
                //'ChapterDataContract:#EY.com.eBook.API.Contracts.Data':
                break;
            case 'CoverpageDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'COVERPAGE':
                //'CoverpageDataContract:#EY.com.eBook.API.Contracts.Data':
                cfg.iconCls = 'eBook-bundle-tree-cover';
                cfg.properties.type = 'COVERPAGE';
                break;
            case 'IndexPageDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'INDEXPAGE':
                cfg.iconCls = 'eBook-bundle-tree-index';
                cfg.properties.type = 'INDEXPAGE';
                break;
            case 'PDFDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'PDFPAGE':
                cfg.iconCls = 'eBook-bundle-tree-pdf';
                cfg.properties.type = 'PDFPAGE';
                cfg.comment = cfg.properties.title;
                break;
            case 'DocumentItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'DOCUMENT':
                cfg.iconCls = 'eBook-bundle-tree-document';
                cfg.properties.type = 'DOCUMENT';
                if (cfg.properties.DocumentId == eBook.EmptyGuid) {
                    cfg.cls = 'eBook-bundle-tree-missing';
                }
                break;
            case 'FicheItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'FICHE':
                cfg.iconCls = 'eBook-bundle-tree-fiche';
                cfg.properties.type = 'FICHE';
                break;
            case 'StatementsDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'STATEMENT':
                cfg.iconCls = 'eBook-bundle-tree-statement';
                cfg.properties.type = 'STATEMENT';
                break;
            case 'WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data':
            case 'WORKSHEET':
                cfg.iconCls = 'eBook-bundle-tree-worksheet';
                cfg.properties.type = 'WORKSHEET';
                break;
        }
        if (cfg.properties.locked == true) {
            cfg.readOnly = true;
            cfg.allowDrag = false;
        }
        cfg.properties.items = null;
        cfg.properties.Index = null;
        delete cfg.properties.items;
        delete cfg.properties.Index;
        eBook.Bundle.AsyncNode.superclass.constructor.call(this, cfg);
    }
     , setProperties: function (props) {
         if (this.attributes.properties == null) this.attributes.properties = {};
         Ext.apply(this.attributes.properties, props);
         if (props.type != "ROOT") this.setText(props.title);
     }
     , applyProperties: function (props, deep) {
         if (!(this.attributes.readOnly == true)) Ext.apply(this.attributes.properties, props);
         if (deep) {
             for (var i = 0; i < this.childNodes.length; i++) {
                 this.childNodes[i].applyProperties(props, deep);
             }
         }
     }
     , getReportObject: function () {
         var props = this.attributes.properties;
         delete props.loader;
         delete props.baseParams;
         delete props.preloadChildren;
         delete props.events;
         switch (props.type) {
             case "ROOT":
                 delete props.__type;
                 delete props.title;
                 props.Index = { items: this.getChildrenObjects() };
                 break;
             case "CHAPTER":
                 props.items = this.getChildrenObjects();
                 break;
         }
         return props;
     }
     , getChildrenObjects: function () {
         var arr = [];
         for (var i = 0; i < this.childNodes.length; i++) {
             arr.push(this.childNodes[i].getReportObject());
         }
         return arr;
     }
});

Ext.tree.TreePanel.nodeTypes.bundleAsyncNode = eBook.Bundle.AsyncNode;


eBook.Bundle.DropLibraryNode = function(config) {
    Ext.apply(this, config);
    eBook.Bundle.DropLibraryNode.superclass.constructor.call(this);
};
Ext.extend(eBook.Bundle.DropLibraryNode, Ext.util.Observable, {
    enabled: true,
    copyFromComponents: null,
    acceptFromComponents: null,
    acceptFromSelf: true,
    copyLeafs: true,
    copyBranches: true,
    preventCopyFromSelf: true,
    copyInSameTreeChangeId: true,
    copyToDifferentTreeChangeId: true,
    fullBranchCopy: false,
    nodeFilter: null,
    attributeFilter: null,
    attributeMapping: null,
    init: function(parent) {
        this.parent = parent;
        this.parent.copyDropNode = this;
        this.parent.on('beforenodedrop', this.onBeforeNodeDrop, this);
        this.parent.addEvents(
               'beforecopydropnode',
            'aftercopydropnode'
        );
    },
    //@private
    onBeforeNodeDrop: function(e) {
        var finishDrop = this.parent.fireEvent("beforecopydropnode", this, e);

        var enabled = this.enabled;
        var copyFromComponents = (typeof (this.copyFromComponents) == 'string') ? [this.copyFromComponents] : this.copyFromComponents;
        var acceptFromComponents = (typeof (this.acceptFromComponents) == 'string') ? [this.acceptFromComponents] : this.acceptFromComponents;

        var copyLeafs = this.copyLeafs;
        var copyBranches = this.copyBranches;
        var preventCopyFromSelf = this.preventCopyFromSelf;
        var acceptFromSelf = this.acceptFromSelf;

        var node = e.dropNode;
        var isLeaf = (typeof (node.attributes.leaf) != 'undefined' && node.attributes.leaf);
        var fromTreeId = e.source.tree.id;
        var toTreeId = e.tree.id;
        var fromSelf = (fromTreeId == toTreeId);

        var doCopy = e.doCopy;
        doCopy = (typeof (doCopy) == 'undefined') ? node.doCopy : doCopy;

        var newNode;

        finishDrop = ((Ext.isArray(acceptFromComponents) && this.inArray(acceptFromComponents, fromTreeId)) || !acceptFromComponents || (fromSelf && acceptFromSelf)) ? finishDrop : false;

        if (finishDrop) {
            if (enabled) {
                if (typeof (doCopy) == 'undefined') {
                    doCopy = ((Ext.isArray(copyFromComponents) && this.inArray(copyFromComponents, fromTreeId)) || !copyFromComponents);
                    doCopy = ((isLeaf && copyLeafs) || (!isLeaf && copyBranches)) ? doCopy : false;
                    doCopy = (!preventCopyFromSelf || !fromSelf) ? doCopy : false;
                }
                //newNode = this.copyDropNode(e, node);
                newNode = (doCopy) ? this.copyDropNode(e, node) : null;
                this.parent.fireEvent("aftercopydropnode", this, doCopy, newNode, e);
            }
            return true;
        } else {
            return false;
        }
    },
    nodeType: 'async',
    //@private
    copyDropNode: function(e, node) {
        //We make a new node based on the attributes of the node that was going to be dropped and then we swap it over the old node that was on the event in order to cause the new node to be added rather than the old node to be moved. Then the rest of the standard drag and drop functionality will proceed as normal.
        var fromSelf = (node.ownerTree.id == e.tree.id), attributes;
        if (node.attributes.IsRepository) {
            var repItem = node.attributes.Item;
            attributes = {
                __type: 'PDFDataContract:#EY.com.eBook.API.Contracts.Data'
                , Type: "PDFPAGE"
                , ItemId: repItem.Id
                , id: eBook.NewGuid()
                , title: repItem.FileName
                , indexed: true
            };
        } else {
            attributes = node.attributes.indexItem
        }
        if (fromSelf) attributes = node.attributes;

        var newNode = new Ext.tree.TreePanel.nodeTypes['bundleAsyncNode'](attributes);

        if ((fromSelf && this.copyInSameTreeChangeId) || (!fromSelf && this.copyToDifferentTreeChangeId)) {
            var dummyNode = new Ext.tree.TreePanel.nodeTypes[this.nodeType]({});
            newNode.setId(dummyNode.id);
        }
        e.dropStatus = true;
        e.dropNode = newNode;
        return newNode;
    },
    //@private
    inArray: function(array, value, caseSensitive) {
        var i;
        for (i = 0; i < array.length; i++) {
            // use === to check for Matches. ie., identical (===),
            if (caseSensitive) { //performs match even the string is case sensitive
                if (array[i].toLowerCase() == value.toLowerCase()) {
                    return true;
                }
            } else {
                if (array[i] == value) {
                    return true;
                }
            }
        }
        return false;
    }
});
Ext.preg('bundleDropLibraryNode', eBook.Bundle.DropLibraryNode);

eBook.Bundle.TreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    createNode: function(attr) {
        // apply baseAttrs, nice idea Corey!
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        if (Ext.isString(attr.uiProvider)) {
            attr.uiProvider = this.uiProviders[attr.uiProvider] || eval(attr.uiProvider);
        }
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                        new eBook.Bundle.Node(attr) :
                        new eBook.Bundle.AsyncNode(attr);
        }
    }
});

eBook.Bundle.Editor = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        var root = new eBook.Bundle.AsyncNode({
            text: 'empty',
            expanded: true,
            type: 'ROOT',
            Index: { items: [] }
        });
        if (this.bundle) {
            root = new eBook.Bundle.AsyncNode({ text: this.bundle.Name,
                expanded: true,
                type: 'ROOT',
                Index: { items: this.bundle.Contents }
            });
        }
        Ext.apply(this, {
            title: eBook.Bundle.BundleTree_Title,
            autoScroll: true,
            animate: true, // replace with eBook config
            enableDD: !eBook.Interface.isFileClosed(),
            containerScroll: true,
            loader: new eBook.Bundle.TreeLoader({ preloadChildren: true }),
            ddGroup: 'eBook.Bundle',
            //dragConfig:
            //dropConfig
            //enableDrag, //drag only
            root: root,
            lines: true,
            plugins: [
	            { ptype: 'bundleDropLibraryNode', nodeType: 'bundleAsyncNode', copyToDifferentTreeChangeId: false }
	        ],
            keys: [
	            {
	                key: Ext.EventObject.DELETE
	                , fn: this.onDeleteItem
	                , scope: this
	            }
	        ],
            tbar: [{
                ref: 'newChapter',
                text: eBook.Bundle.Window_NewChapter,
                iconCls: 'eBook-icon-addchapter-16',
                scale: 'small',
                iconAlign: 'left',
                handler: this.onAddChapterClick,
                scope: this
                , disabled: eBook.Interface.isFileClosed()
            }, {
                ref: 'deleteItem',
                text: eBook.Bundle.Window_DeleteItem,
                iconCls: 'eBook-icon-deleteitem-16',
                scale: 'small',
                iconAlign: 'left',
                disabled: true,
                handler: this.onDeleteItem,
                scope: this
                , disabled: eBook.Interface.isFileClosed()
}],
                //margins:
                //requestMethod:'POST',
                rootVisible: true
            });
            eBook.Bundle.Editor.superclass.initComponent.apply(this, arguments);
            this.getSelectionModel().on('selectionchange', this.onNodeSelect, this);
            this.on('nodedrop', this.onDropped, this);

        }
    , onNodeSelect: function(sm, node) {
        var tb = this.getTopToolbar();
        tb.deleteItem.disable();
        if (node != null) {
            if (!eBook.Interface.isFileClosed()) {
                tb.deleteItem.enable();
            }
            this.refOwner.loadNodeSettings(node);
        } else {
            this.selectNode(this.getRootNode());
        }
    }
    , onAddChapterClick: function() {
        var props = {
            __type: 'ChapterDataContract:#EY.com.eBook.API.Contracts.Data'
            , type: 'CHAPTER'
            , title: eBook.Bundle.BundleTree_NewChapter
            , items: []
            , FooterConfig: { Enabled: false, ShowFooterNote: false, ShowPageNr: false }
            , HeaderConfig: { Enabled: false, ShowTitle: false }
            , id: eBook.NewGuid()
            , indexed: true
        };
        var n = new eBook.Bundle.AsyncNode(props);
        this.getRootNode().appendChild(n);
        this.selectNode(n);
    }
    , onDropped: function(dde) {
        this.getSelectionModel().select(dde.dropNode);
    }
    , setRootNode: function(node) {
        eBook.Bundle.Editor.superclass.setRootNode.call(this, node);
        this.selectNode.defer(500, this, [node]);
    }
    , selectNode: function(node) {
        this.getSelectionModel().select(node);
    }
    , getReport: function() {
        return this.getRootNode().getReportObject();
    }
    , onDeleteItem: function() {
        var s = this.getSelectionModel().getSelectedNode();
        if (s != null) s.remove(true);
        this.selectNode(this.getRootNode());
    }
    , getContent: function() {
        var cnts = [];
        var r = this.getRootNode().getChildrenObjects();
        return r;
    }
    });

Ext.reg('bundleEditor', eBook.Bundle.Editor);
