
eBook.Bundle.LibraryTreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    filter: {
        ps: null,
        pe: null,
        stat: null
    },
    Culture: 'nl-BE',
    selectionMode: false,
    createNode: function(attr) {
        // apply baseAttrs, nice idea Corey!
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        if (Ext.isString(attr.uiProvider)) {
            attr.uiProvider = this.uiProviders[attr.uiProvider] || eval(attr.uiProvider);
        }
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                        new Ext.tree.TreeNode(attr) :
                        new Ext.tree.AsyncTreeNode(attr);
        }
    },
    requestData: function(node, callback, scope) {
        //REPOSITORY
        if (node.attributes.IsRepository || node.id == 'REPOSITORY') {
            this.transId = eBook.CachedAjax.request({
                method: 'POST',
                url: eBook.Service.repository + 'GetChildren',
                success: this.handleResponse,
                failure: this.handleFailure,
                scope: this,
                argument: { callback: callback, node: node, scope: scope },
                params: this.getRepositorySendingParam(node)
            });
        } else {
            this.transId = eBook.CachedAjax.request({
                method: 'POST',
                url: this.dataUrl || this.url,
                success: this.handleResponse,
                failure: this.handleFailure,
                scope: this,
                argument: { callback: callback, node: node, scope: scope },
                params: this.getSendingParam(node)
            });
        }

    },
    processResponse: function(response, node, callback, scope) {
        var json = response.responseText;
        try {

            var o = response.responseData || Ext.decode(json);
            var repos = false;
            if (node.attributes.IsRepository || node.id == 'REPOSITORY') {
                repos = true;
                o = o.GetChildrenResult;
            } else {
                o = o.GetItemsResult;
            }
            node.beginUpdate();
            for (var i = 0, len = o.length; i < len; i++) {
                o[i].IsRepository = repos;
                var n = this.createNode(o[i]);
                if (n) {
                    if (n.attributes.Files == false && this.selectionMode) {
                        if (!Ext.isEmpty(n.attributes.cls) && n.attributes.cls.length > 0) {
                            n.attributes.cls += ' ';
                        } else {
                            n.attributes.cls = '';
                        }
                        n.attributes.cls += "tree-node-unselectable";
                    }

                    node.appendChild(n);
                }
            }
            node.endUpdate();
            this.runCallback(callback, scope || node, [node]);
        } catch (e) {
            this.handleFailure(response);
        }
    },
    getRepositorySendingParam: function(node) {

        var o = { crcdc: {
            cid: eBook.Interface.currentClient.get('Id'),
        cult: eBook.Interface.Culture,
        strucId: node.id == 'REPOSITORY' ? null : node.id,
        key: node.attributes.Key,
        strucTp: node.attributes.StructuralType,
        showFiles: true,
        fileId: eBook.Interface.currentFile ? eBook.Interface.currentFile.get('Id') : null
        }
        };

        Ext.apply(o.crcdc, this.filter);
        var periodParent = null;
        if (node.attributes.StructuralType == "FILE") {

            periodParent = node.findParentBy(function() {
                return this.attributes.PeriodStart && !Ext.isEmpty(this.attributes.PeriodStart);
            }, null);
        }
        if (periodParent) {
            o.crcdc.ps = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodStart);
            o.crcdc.pe = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodEnd);
        }

        return Ext.encode(o);

    },
    getSendingParam: function(node) {
        var o = { cldc: {
            NodeId: node.id,
            Culture: this.Culture,
            FileId: eBook.Interface.currentFile.get('Id')
        }
        };

        /* REPOSITORY
        var o = { crcdc: {
        cid: eBook.Interface.currentClient.get('Id')
        , cult: eBook.Interface.Culture
        , strucId: node.id == 'root' ? null : node.id
        , key: node.attributes.Key
        , strucTp: node.attributes.StructuralType
        , showFiles: this.showFiles
        , fileId: eBook.Interface.currentFile ? eBook.Interface.currentFile.get('Id') : null
        }
        };

        Ext.apply(o.crcdc, this.filter);
        var periodParent = null;
        if (node.attributes.StructuralType == "FILE") {

            periodParent = node.findParentBy(function() {
        return this.attributes.PeriodStart && !Ext.isEmpty(this.attributes.PeriodStart);
        }, null);
        }
        if (periodParent) {
        o.crcdc.ps = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodStart);
        o.crcdc.pe = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodEnd);
        }
        */
        return Ext.encode(o);

    }
});


eBook.Bundle.Library = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        this.standardFilter = {
            ps: eBook.Interface.currentFile.get('StartDate'),
            pe: eBook.Interface.currentFile.get('EndDate'),
            stat: null
        };

        if (this.serviceId) {
            if (this.serviceId.toUpperCase() == "BD7C2EAE-8500-4103-8984-0131E73D07FA") {
                this.rootName = 'rootbiztaxbundle';
            } else if (this.serviceId.toUpperCase() == "60CA9DF9-C549-4A61-A2AD-3FDB1705CF55") {
                this.rootName = 'rootmanualbiztaxbundle';
            }
        }
        Ext.apply(this, {
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDD: false,
            enableDrag: true,
            containerScroll: true,
            title: eBook.Bundle.ObjectsTree_Title,
            //border: false,
            // auto create TreeLoader
            // dataUrl: 'get-nodes.php',
            toolTemplate: new Ext.XTemplate(
                '<tpl if="id=\'add\'">',
                    '<div class="eBook-repository-add-ico-24 eBook-header-tools-btn x-btn x-btn-text">Add...</div>',
                '</tpl>'),
            tools: [{
                id: 'add',
                qtip: 'Add file to repository',
                handler: function(e, toolEl, panel, tc){
                        var wn = new eBook.Repository.SingleUploadWindow({
                                clientId: eBook.CurrentClient,
                                readycallback: {fn: panel.reload, scope: panel}
                            });

                        wn.show(panel);
                }
            }],
            loader: new eBook.Bundle.LibraryTreeLoader({
                dataUrl: eBook.Service.bundle + 'GetItems',
                requestMethod: "POST", filter: this.standardFilter,
                Culture: this.culture
            }),
            ddGroup: 'eBook.Bundle',
            rootVisible: false,
            root: {
                nodeType: 'async',
                text: 'Library',
                draggable: false,
                id: this.rootName ? this.rootName : 'root',
                expanded: true
            }

        });
        eBook.Bundle.Library.superclass.initComponent.apply(this, arguments);
    }
    , reload: function() {
        this.setRootNode({
            nodeType: 'async',
            text: 'Library',
            draggable: false,
            id: 'root',
            expanded: true
        });
    }
});

Ext.reg('bundlelibrary', eBook.Bundle.Library);