
eBook.Bundle.Log = Ext.extend(Ext.Panel,{

    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetFileServiceLog'
                , serviceUrl: eBook.Service.file
                , criteriaParameter: 'cfsdc'
                , fields: eBook.data.RecordTypes.Log
            }),
            tpl = new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="eBook-reports-logEntry">',
                '<div><div id="personName" style="float:left;" class="eBook-reports-header eBook-reports-icon eBook-icon-checklist-24">{personName}</div><div style="float:right;" id="timestamp">{timestamp:date("d/m/Y H:i")}</div></div>',
                '<table style="clear:both;width:100%;">',
                '<tr>',
                '<td class="eBook-reports-logEntry-details">',
                '<ul>',
                '<tpl if="[this.actionPerformed(actions,\'changesAfterGAD\')] &gt; -1">',
                '<li class="eBook-history-16" id="changesAfterGAD">Changes made after general assembly date</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'comment\')] &gt; -1">',
                '<li class="eBook-icon-checklist-24" id="comment">Review notes added</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'pdfAttachment\')] &gt; -1">',
                '<li class="eBook-pdf-view-24" id="comment">Review notes attachment added</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'bundleLocked\')] &gt; -1">',
                '<li class="eBook-lock-16" id="bundleLocked">Deliverable locked</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'draftGenerated\')] &gt; -1">',
                '<li class="eBook-Pdf-draft-icon" id="draftGenerated">Draft PDF generated</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'finalGenerated\')] &gt; -1">',
                '<li class="eBook-Pdf-final-icon"  id="finalGenerated">PDF generated</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'bundleScanned\')] &gt; -1">',
                '<li class="eBook-Pdf-signed-icon" id="bundleScanned">Deliverable scanned</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'status\')] &gt; -1">',
                    '<tpl if="[this.isRejectionStatus(status)] &gt; -1">',
                        '<li class="eBook-icon-delete-16" id="status">{status}</li>',
                 '</tpl>',
                    '<tpl if="[this.isRejectionStatus(status)] == -1">',
                        '<li class="eBook-tickcircle-16" id="status">{status}</li>',
                    '</tpl>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'bundleOpen\')] &gt; -1">',
                '<li class="eBook-unlock-16" id="bundleOpen">Deliverable reopened</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'confirmationMemorandumSigned\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="confirmationMemorandumSigned">Signed memorandum confirmed</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'confirmationSignedDocumentsAddedToFirstFile\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="confirmationSignedDocumentsAddedToFirstFile">Signed documents added confirmed</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'representationLetterSigned\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="representationLetterSigned">Signed representation letter uploaded</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'minutesGeneralAssembleSigned\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="minutesGeneralAssembleSigned">Signed minutes general assemble uploaded</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'statutoryAccountsSigned\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="statutoryAccountsSigned">Signed statutory accounts uploaded</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'annualAccounts\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="annualAccounts">Annual Accounts uploaded</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'engagementCodeUpdate\')] &gt; -1">',
                '<li class="eBook-tickcircle-16" id="engagementCodeUpdate">Inactive engagement code replaced</li>',
                '</tpl>',
                '</ul>',
                '</td>',
                '</table>',
                '</div>',
                '</tpl>',{
                    actionPerformed: function(actions,action) {
                        return actions.indexOf(action);
                    },
                    isRejectionStatus: function(status) {
                        return status.indexOf("Rejected");
                    }
                }
            );

        Ext.apply(me, {
            title: 'Log entries',
            //cls: 'eBook-reports-logEntries',
            width: 400,
            minWidth: 400,
            //collapsedCls: 'eBook-reports-logEntries-collapsed', not working in Extjs 3 due to border layout?
            autoScroll: true,
            items: new Ext.DataView({
                itemId: 'dataviewLog',
                tpl: tpl,
                store: store,
                autoHeight:true,
                multiSelect: true,
                overClass:'x-view-over',
                itemSelector:'div.thumb-wrap',
                emptyText: 'No log entries to display',
                storeLoad: function (fileId,serviceId) {
                    if(!fileId || !serviceId)
                    { Ext.Msg.alert("failure","failed to load comments."); return; }

                    var me = this,
                        params = {
                            FileId: fileId,
                            ServiceId: serviceId,
                            Culture: null,
                            Order: 'DESC'
                        };

                    me.getEl().mask("loading data");
                    //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                    Ext.apply(me.store.baseParams, params);

                    me.store.load({
                        params: params,
                        callback: me.onLoadSuccess,
                        scope: me
                    });
                },
                onLoadSuccess: function (r, options, success) {
                    var me = this;
                    console.log(r);
                    if (success) {
                        me.getEl().unmask();
                    }
                    else {
                        me.getEl().mask("Loading of data failed");
                    }
                }
            }),
            listeners: {
                afterrender: function () {
                    var me = this;

                    //retrieve the fileservice store using the file ID and service ID
                    if(me.fileId && me.serviceId) {
                        me.getComponent("dataviewLog").storeLoad(me.fileId,me.serviceId);
                    }
                }
            }
        });

        eBook.Bundle.Log.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('fileServicelog', eBook.Bundle.Log);