eBook.Bundle.HtmlEditor = Ext.extend(Ext.ux.TinyMCE, {
    initComponent: function () {
        var me = this;
        me.pdfAttachmentId = null;

        Ext.apply(this, {
            tinymceSettings: {
                theme: "advanced",
                plugins: "pagebreak,table,iespell,insertdatetime,searchreplace,paste,directionality,noneditable,xhtmlxtras",
                theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|cut,copy,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,cleanup,code,|,insertdate,inserttime",
                theme_advanced_buttons2: "",
                theme_advanced_buttons3: "",
                //theme_advanced_buttons4: ",abbr,acronym,del,ins,|,visualchars,nonbreaking,pagebreak",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location: "bottom",
                theme_advanced_resizing: true,
                extended_valid_elements: ""
            },
            boxMinHeight: 200
        });
        eBook.Bundle.HtmlEditor.superclass.initComponent.apply(this, arguments);
    }
    , startEdit: function (el, val) {
        if (Ext.isDefined(val)) this.setValue(val);
        if (this.rendered) {
            this.el.remove();
            this.rendered = false;
        }

        this.render(el);
    }
});