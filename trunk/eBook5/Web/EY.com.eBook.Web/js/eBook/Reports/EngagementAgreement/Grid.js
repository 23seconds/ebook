eBook.Reports.EngagementAgreement.Grid = Ext.extend(eBook.grid.PagedGridPanel, {

    initComponent: function() {
        Ext.apply(this, {
            title: 'Search Result',
            storeConfig: {
                selectAction: 'GetClientEngagementLettersByPerson',
                fields: eBook.data.RecordTypes.ClientEngagementLettersByPerson,
                idField: 'clientId',
                criteriaParameter: 'cpeldc',
                serviceUrl: eBook.Service.client
            },
            pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            },
            loadMask: true,
            columns: [{
                    header: 'Clientname',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'clientName'
                    //editor: 'headerfield'
                },
                {
                    header: 'Client GFIS',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'clientGFIS'
                    //editor: 'headerfield'
                },
                {
                    header: 'Partner',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'partner'
                    //editor: 'headerfield'
                },
                {
                    header: 'Manager',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'manager'
                    //editor: 'headerfield'
                },
                {
                    header: 'Office',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'office'
                },
                {
                    header: 'Date ELEA',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'dateELEA'
                },
                {
                    header: 'Start date',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'startDate',
                    renderer: function(value, e, f, d, s, q, i) { 
                        if (value != null) {
                            return value.format("d/m/Y");
                        }else{
                            return null;  
                        } 
                    }
                },
                {
                    header: 'End date',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'endDate',
                    renderer: function(value, e, f, d, s, q, i) { 
                        if (value != null) {
                            return value.format("d/m/Y");
                        }else{
                            return null;  
                        } 
                    }
                    //editor: 'headerfield'
                },
                {
                    header: 'Template',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'template',
                    renderer: function(value) {
                        if (value != ""){
                            var obj = JSON.parse(value) ;
                            return obj.nl;
                        }else{
                            return "";
                        }
                    }
                },
                {
                    header: 'Status',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'status'
                    //editor: 'headerfield'
                },
                /*{
                    header: 'Filename',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'fileName'
                    //editor: 'headerfield'
                },*/
                {
                    header: 'Validated by GTH',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'gth'
                    //editor: 'headerfield'
                }
                
                                  
            ] ,
            //store: new eBook.data.JsonStore(storeCfg),
            listeners:{
                rowdblclick : function( t, rowIndex, e ){
                    var data = t.store.getAt(rowIndex).data;

                    if(!Ext.isEmpty(data.itemId)) {
                        var wn = new eBook.Repository.FileDetailsWindow({
                            repositoryItemId: data.itemId,
                            readOnly: false,
                            readOnlyMeta: false,
                            clientId: data.clientId
                        });
                        wn.show(this.parentCaller);
                        wn.loadById();
                    }
                },
                afterrender: function(t){

                    var searchValues = t.ownerCt.criteriaGrid.getSearchValues();
                    this.storeLoad(searchValues);

                    /*
                    Ext.Ajax.request({
                    url: '/EY.com.eBook.API.5/ClientService.svc/GetClientEngagementLettersByPerson'
                    , method: 'POST'
                    , jsonData: { cpeldc: { sdt: v.startDateTo, sdf: v.startDateFrom, edt: v.endDateTo, edf: v.endDateFrom, sid: v.status, tid: v.template, cn: v.clientName, ex: v.expiration, ro: v.renewal, gth: v.gth, pid: eBook.User.personId}}//{ cceldc: { sd: Ext.Date.format(startDate.getValue(), 'MS'), ed: Ext.Date.format(endDate.getValue(), 'MS'), stid: structureId, sid: statusCombo.getValue(), tid: templateCombo.getValue(), t: templateCombo.getRawValue()} }
                    , callback: function (options, success, response) {
                        if (success) {
                            var obj = Ext.decode(response.responseText);
                            this.store.loadData(obj.GetClientEngagementLettersByPersonResult);
                            this.getEl().unmask();
                        }
                    }
                    , scope: this
                });*/
                }
            },

            storeLoad : function(searchFilterValues) {
                var me = this,
                    sortState = me.store.getSortState(),
                    params =  {
                        Start: 0,
                        Limit: 25,
                        sdt: searchFilterValues.startDateTo,
                        sdf: searchFilterValues.startDateFrom,
                        edt: searchFilterValues.endDateTo,
                        edf: searchFilterValues.endDateFrom,
                        sid: searchFilterValues.status,
                        tid: searchFilterValues.template,
                        cn: searchFilterValues.clientName,
                        ex: searchFilterValues.expiration,
                        ro: searchFilterValues.renewal,
                        gth: searchFilterValues.gth,
                        ac: searchFilterValues.activeClient,
                        pid: eBook.User.personId
                    };

                //retrieve grid sort
                if(sortState)
                {
                    params.sf = sortState.field;
                    params.so = sortState.direction;
                }

                //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                Ext.apply(this.store.baseParams,params);

                this.store.load(
                    {
                        params: params,
                        callback: this.onLoadSuccess,
                        scope: this
                    }
                )
            },

            onLoadSuccess: function(r,options,success)  {
                this.getEl().unmask();
                if(success) {
                }
                else
                {
                    this.getEl().mask("Loading of data failed");
                }
            }
        });

        eBook.Reports.EngagementAgreement.Grid.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('reportEAgrid', eBook.Reports.EngagementAgreement.Grid);

