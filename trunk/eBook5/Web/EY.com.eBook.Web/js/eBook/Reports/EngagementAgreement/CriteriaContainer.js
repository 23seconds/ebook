eBook.Reports.EngagementAgreement.Criteria = Ext.extend(Ext.Panel, {
    initComponent: function () {

        this.isRecAdded = false;
        this.isEmptyTemplateAdded = false;
        Ext.apply(this, {
            loadMask: true,
            title: 'Criteria',
            hideBorders: true,
            layout: 'form',
            ref: 'criteriaGrid',

            items: [
                {
                    layout: 'column',
                    hideBorders: true,
                    ref: 'columnContainer',
                    items: [
                            {
                                columnWidth: .2,
                                layout: 'form',
                                ref: 'column1',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Start date (from)',
                                        format: 'd/m/Y',
                                        name: 'startDateFrom',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Start date (to)',
                                        format: 'd/m/Y',
                                        name: 'startDateTo',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Expiration within',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[30, '1 month'], [60, '2 month'], [90, '3 month'], [120, '4 month'], [150, '5 month'], [180, '6 month'], [356, '1 year'], [712, '2 year'], [1068, '3 year'], [1424, '4 year'], [1780, '5 year']]
                                        }),
                                        name: 'expiration',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: .2,
                                layout: 'form',
                                ref: 'column2',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'End date (from)',
                                        format: 'd/m/Y',
                                        name: 'endDateFrom',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'End date (to)',
                                        format: 'd/m/Y',
                                        name: 'endDateTo',
                                        //value: new Date(),
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Validated by GTH',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [['%', "All"], ['true', 'Yes'], ['false', 'No']]
                                        }),
                                        name: 'gth',
                                        value: '',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: .25,
                                layout: 'form',
                                ref: 'column3',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Client name',
                                        name: 'clientName',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        ref: '../cboStatusList',
                                        fieldLabel: 'Status',
                                        displayField: 'nl',
                                        valueField: 'id',
                                        mode: 'local',
                                        selectOnFocus: true,
                                        autoSelect: true,
                                        editable: false,
                                        forceSelection: true,
                                        triggerAction: 'all',
                                        store: new eBook.data.JsonStore({
                                            selectAction: 'GetStatusList'
                                            , criteriaParameter: 'csdc'
                                            , autoDestroy: true
                                            , serviceUrl: eBook.Service.repository
                                            , storeParams: {}
                                            , disabled: this.readOnly
                                            , fields: eBook.data.RecordTypes.GlobalListItem
                                            , autoLoad: true
                                            , baseParams: {
                                                id: "SENTSIGNEDCLIENT"
                                            }
                                            , sortInfo: {field: 'id', direction:'ASC'}
                                        }),
                                        name: 'status',
                                        listeners: {
                                            expand: function (d, e, i) {
                                                var store = d.store;
                                                //store.load();
                                                if (!this.isRecAdded) {
                                                    var defaultData = {
                                                        id: '64AFAD19-8407-4F9E-AF72-ED37468C4272',
                                                        nl: 'No engagement agreement',
                                                        fr: 'No engagement agreement',
                                                        en: 'No engagement agreement',
                                                        value: 'No engagement agreement'
                                                    };
                                                    var defaultData2 = {
                                                        id: null,//'00000000-0000-0000-0000-000000000000',
                                                        nl: "All",
                                                        fr: 'All',
                                                        en: 'All',
                                                        value: ''
                                                    };
                                                    var recId = 3; // provide unique id
                                                    var p = new store.recordType(defaultData, recId);
                                                    var p2 = new store.recordType(defaultData2, 5);
                                                    store.insert(store.getTotalCount(), p);
                                                    store.insert(0, p2);
                                                    this.isRecAdded = true;
                                                }
                                            }
                                        },
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        //group: 'EL',
                                        fieldLabel: 'Template',
                                        displayField: 'nl',
                                        valueField: 'id',
                                        autoSelect: true,
                                        editable: false,
                                        forceSelection: true,
                                        triggerAction: 'all',
                                        mode: 'local',
                                        store: new eBook.data.JsonStore({
                                            selectAction: 'GetList'
                                            , serviceUrl: eBook.Service.lists
                                            , autoLoad: true
                                            , autoDestroy: true
                                            , criteriaParameter: 'cldc'
                                           , baseParams: {
                                               c: "nl-BE",
                                               lik: "EngagementTemplate"
                                           }
                                            , fields: eBook.data.RecordTypes.GlobalListItem

                                        }),
                                        name: 'template'
                                        , listeners: {
                                            expand: function (d, e, i) {
                                                var store = d.store;
                                                //store.load();
                                                if (!this.isEmptyTemplateAdded) {
                                                    var defaultData2 = {
                                                        id: null,//'00000000-0000-0000-0000-000000000000',
                                                        nl: "All",
                                                        fr: 'All',
                                                        en: 'All',
                                                        value: ''
                                                    };
                                                    var p2 = new store.recordType(defaultData2, 5);
                                                    store.insert(0, p2);
                                                    this.isEmptyTemplateAdded = true;
                                                }
                                            }
                                        },
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: 0.15,
                                layout: 'form',
                                ref: 'column4',
                                cls: 'eBook-reports-column',
                                items: [
                                         {
                                             xtype: 'checkbox',
                                             fieldLabel: 'Show only most recent EA',
                                             checked: true,
                                             name: 'renewal'
                                         },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Client status',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['0', 'Active'], ['1', 'Inactive']]
                                        }),
                                        name: 'activeClient',
                                        value: '',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: .2,
                                layout: 'hbox',
                                layoutConfig: {
                                    align:'stretch',
                                    pack:'end',
                                    defaultMargins:{
                                        top:25,
                                        bottom:0,
                                        right:5,
                                        left:0
                                    }
                                },
                                height:95,
                                hideBorders: true,
                                cls: 'eBook-reports-button-column',
                                ref: 'columnButtonContainer',
                                items: [
                                    {
                                            xtype: 'button',
                                            text: 'Export',
                                            itemId: 'exportBtn',
                                            ref: '../../searchBtn',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-icon-excel-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            width: 100,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var searchFilterValues = btn.refOwner.getSearchValues(),
                                                        grid = btn.refOwner.refOwner.reportEAgrid,
                                                        sortState = grid.store.getSortState(),
                                                        cpeldc = {
                                                            Start: 0,
                                                            Limit: 100000,
                                                            sdt: searchFilterValues.startDateTo,
                                                            sdf: searchFilterValues.startDateFrom,
                                                            edt: searchFilterValues.endDateTo,
                                                            edf: searchFilterValues.endDateFrom,
                                                            sid: searchFilterValues.status,
                                                            tid: searchFilterValues.template,
                                                            cn: searchFilterValues.clientName,
                                                            ex: searchFilterValues.expiration,
                                                            ro: searchFilterValues.renewal,
                                                            gth: searchFilterValues.gth,
                                                            pid: eBook.User.personId,
                                                            ac: searchFilterValues.activeClient,
                                                            srt: sortState
                                                        };

                                                    if(sortState)
                                                    {
                                                        cpeldc.sf = sortState.field;
                                                        cpeldc.so = sortState.direction;
                                                    }

                                                    btn.ownerCt.getEl().mask("Generating excel");
                                                    Ext.Ajax.request({
                                                        url: '/EY.com.eBook.API.5/OutputService.svc/ExportClientEngagementLettersByPerson'
                                                        ,
                                                        method: 'POST'
                                                        ,
                                                        jsonData: {
                                                            cpeldc: cpeldc
                                                        }//{ cceldc: { sd: Ext.Date.format(startDate.getValue(), 'MS'), ed: Ext.Date.format(endDate.getValue(), 'MS'), stid: structureId, sid: statusCombo.getValue(), tid: templateCombo.getValue(), t: templateCombo.getRawValue()} }
                                                        ,
                                                        callback: function (options, success, response) {
                                                            btn.ownerCt.getEl().unmask();
                                                            if (success) {
                                                                var obj = Ext.decode(response.responseText);
                                                                var id = obj.ExportClientEngagementLettersByPersonResult.Id;
                                                                window.open("pickup/" + id + ".xlsx");
                                                                btn.ownerCt.ownerCt.ownerCt.getEl().unmask();
                                                            } else {
                                                                alert('Export failed. ');
                                                            }
                                                        }
                                                        ,
                                                        scope: this
                                                    });
                                                }
                                            },
                                            scope: this
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Search/Refresh',
                                            itemId: 'searchBtn',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-filter-ico-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            ref: '../../searchBtn',
                                            width: 100,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var me = this,
                                                        criteriaGrid = me.refOwner;

                                                    criteriaGrid.search();
                                                }
                                            },
                                            scope: this
                                        }
                                ]
                            }
                        ]
                }
            ]
            ,
            listeners: {
                afterRender: function (thisForm, options) {
                    var me = this;
                    me.keyNav = new Ext.KeyNav(me.el, {
                        //Pressing enter on any field will trigger the search
                        "enter" : function(e){

                            me.search();
                        },
                        scope : me
                    });
                }
            }
        });

        eBook.Reports.EngagementAgreement.Criteria.superclass.initComponent.apply(this, arguments);
    },
    search: function () {
        //retrieve filter settings
        var me = this,
            searchValues = me.getSearchValues();
        //utilize the storeload function in grid panel to reload data
        me.refOwner.reportEAgrid.storeLoad(searchValues);
    },
    exportToExcel: function (btn, e) {

    },
    getSearchValues: function () {
        var columnContainer = this.columnContainer,
            fields = columnContainer.findByType(Ext.form.Field),
            values = {};

        Ext.each(fields,function(fld){
            var val = fld.getValue();
            values[fld.name] = typeof(val) != 'undefined' && val != null && val != "" ? val : null;
        },this);

        return values;
    }
});
Ext.reg('reportEAcriteria', eBook.Reports.EngagementAgreement.Criteria);