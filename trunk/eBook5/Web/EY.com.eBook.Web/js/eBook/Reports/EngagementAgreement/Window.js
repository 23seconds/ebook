eBook.Reports.EngagementAgreement.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        Ext.apply(this, {
            title: 'Client - Engagement letter Report',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                { xtype: 'reportEAcriteria', height: 120, ref:'reportEAcriteria' },
                { xtype: 'reportEAgrid', flex: 1, cls: 'eBookAT-GTH-Persongrid', ref: 'reportEAgrid' }
            ]
        });
        eBook.Reports.EngagementAgreement.Window.superclass.initComponent.apply(this, arguments);
    }
});