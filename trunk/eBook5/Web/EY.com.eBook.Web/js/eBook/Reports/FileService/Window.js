eBook.Reports.FileService.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            title: 'Client - Year End Flow',
            layout: {
                type: 'border',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'reportFScriteria',
                    region: 'north',
                    height: 100,
                    ref: 'reportFScriteria',
                    serviceId: me.serviceId,
                    department: me.department
                },
                {
                    xtype: 'reportFSgrid',
                    region: 'center',
                    cls: 'eBookAT-GTH-Persongrid',
                    ref: 'reportFSgrid',
                    serviceId: me.serviceId,
                    department: me.department
                },
                {
                    xtype: 'fileServicelog',
                    region: 'east',
                    width: 300,
                    ref: 'reportFSlog',
                    serviceId: me.serviceId,
                    department: me.department,
                    fileId: null,
                    collapsible: true,
                    collapsed: true,
                    hidden: true,
                    hideCollapseTool: true,
                    animCollapse: false
                    /*                    collapsible: false,
                     collapsed: false,
                     hidden: false,
                     hideCollapseTool: false,
                     animCollapse: true*/
                }
            ]
        });

        eBook.Reports.FileService.Window.superclass.initComponent.apply(this, arguments);
    }
});