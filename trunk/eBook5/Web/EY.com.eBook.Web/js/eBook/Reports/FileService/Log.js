
eBook.Reports.FileService.Log = Ext.extend(Ext.Panel,{
    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetFileServiceLog'
                , serviceUrl: eBook.Service.client
                , criteriaParameter: 'ccpldc'
                , fields: eBook.data.RecordTypes.Log
            }),
            tpl = new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="eBook-reports-logEntry">',
                '<div id="personName" class="eBook-reports-header eBook-reports-icon eBook-icon-checklist-24">{personName}</div>',
                '<table>',
                '<tr>',
                '<td class="eBook-reports-logEntry-details">',
                '<div class="eBook-reports-header">Actions taken:</div>',
                '<ul>',
                '<tpl if="[this.actionPerformed(actions,\'status\')] &gt; -1"=>',
                '<li id="status">{status}</li>',
                '</tpl>',
                '<tpl if="[this.actionPerformed(actions,\'file\')] &gt; -1">', //check if file was uploaded
                '<li id="documentUploaded">Document uploaded</li>',
                '</tpl>',
                '</ul>',
                '</td>',
                '<tpl if="[this.actionPerformed(actions,\'comment\')] &gt; -1"=>',
                '<td class="eBook-reports-logEntry-comment">',
                '<div class="eBook-reports-header">Comment:</div>',
                '<div>{comment}</div>',
                '</td>',
                '</tpl>',
                '</tr>',
                '<tr>',
                '<td colspan="2" class="eBook-reports-logEntry-timestamp">',
                //'<div><div id="currentStatus" style="display: inline-block;" class="eBook-reports-header">Current status:&nbsp;</div>{currentStatus)</div>',
                '<div><div id="timestamp"  style="display: inline-block;" class="eBook-reports-header">Updated on:&nbsp;</div>{timestamp:date("d/m/Y H:i:s")}</div>',
                '</td>',
                '<tr>',
                '</table>',
                '</div>',
                '</tpl>',{
                    actionPerformed: function(actions,action) {
                        return actions.indexOf(action);
                    }}
            );

        Ext.apply(this, {
            title: 'Log entries',
            cls: 'eBook-reports-logEntries',
            width: 400,
            minWidth: 400,
            collapsible: true,
            collapsed: true,
            hidden: true,
            hideCollapseTool: true,
            animCollapse: false,
            //collapsedCls: 'eBook-reports-logEntries-collapsed', not working in Extjs 3 due to border layout?
            autoScroll: true,
            items: new Ext.DataView({
                itemId: 'dataviewLog',
                tpl: tpl,
                store: store,
                autoHeight:true,
                multiSelect: true,
                overClass:'x-view-over',
                itemSelector:'div.thumb-wrap',
                emptyText: 'No log entries to display',
                storeLoad: function (bankAccount) {
                    var me = this,
                        params = {
                            ba: bankAccount
                        };

                    me.getEl().mask("loading data");
                    //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                    Ext.apply(me.store.baseParams, params);

                    me.store.load({
                        params: params,
                        callback: me.onLoadSuccess,
                        scope: me
                    });
                },
                onLoadSuccess: function (r, options, success) {
                    var me = this;
                    console.log(r);
                    if (success) {
                        me.getEl().unmask();
                    }
                    else {
                        me.getEl().mask("Loading of data failed");
                    }
                }
            }),
            listeners: {
                beforeexpand: function (p) {
                    //use to selected record id in order to load the store
                    var me = this,
                        grid = p.refOwner.reportFSgrid,
                        record = grid.getActiveRecord(),
                        bankAccount = record.get("bankAccount");

                    if(bankAccount) {
                        me.getComponent("dataviewLog").storeLoad(bankAccount);
                    }
                }
            }
        });

        eBook.Reports.FileService.Log.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('reportFSlog', eBook.Reports.FileService.Log);