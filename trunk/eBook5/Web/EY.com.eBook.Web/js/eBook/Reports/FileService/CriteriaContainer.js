eBook.Reports.FileService.Criteria = Ext.extend(Ext.Panel, {
    initComponent: function () {
        var me = this;
        this.isRecAdded = false;
        this.isEmptyTemplateAdded = false;
        Ext.apply(this, {
            loadMask: true,
            title: 'Criteria',
            hideBorders: true,
            layout: 'form',
            ref: 'criteriaGrid',
            items: [
                {
                    layout: 'column',
                    hideBorders: true,
                    ref: 'columnContainer',
                    items: [
                        {
                            columnWidth: .2,
                            layout: 'form',
                            ref: 'column1',
                            cls: 'eBook-reports-column',
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Client name',
                                    name: 'clientName',
                                    anchor: '100%'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Office',
                                    name: 'office',
                                    anchor: '100%'
                                }
                            ]
                        },
                        {
                            columnWidth: .25,
                            layout: 'form',
                            ref: 'column2',
                            cls: 'eBook-reports-column',
                            items: [
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'End Date',
                                    format: 'd/m/Y',
                                    name: 'endDate',
                                    anchor: '100%'
                                },
                                {
                                    xtype: 'datefield',
                                    fieldLabel: 'Date Modified',
                                    format: 'd/m/Y',
                                    name: 'dateModified',
                                    anchor: '100%'
                                }
                            ]
                        },
                        {
                            columnWidth: .2,
                            layout: 'form',
                            ref: 'column3',
                            cls: 'eBook-reports-column',
                            items: [
                                {
                                    xtype: 'combo',
                                    fieldLabel: 'Status',
                                    displayField: 'Status',
                                    nullable: true,
                                    valueField: 'Id',
                                    mode: 'local',
                                    selectOnFocus: true,
                                    autoSelect: true,
                                    editable: false,
                                    forceSelection: true,
                                    triggerAction: 'all',
                                    store: new eBook.data.JsonStore({
                                        selectAction: 'GetServiceStatusesList'
                                        , criteriaParameter: 'cscdc'
                                        , autoDestroy: true
                                        , serviceUrl: eBook.Service.file
                                        , storeParams: {ServiceId: me.serviceId}
                                        , baseParams: {ServiceId: me.serviceId}
                                        , disabled: this.readOnly
                                        , fields: eBook.data.RecordTypes.Statuses
                                        , autoLoad: true
                                        , sortInfo: {field: 'Rank', direction:'ASC'}
                                    }),
                                    name: 'status',
                                    anchor: '100%',
                                    listWidth: 260,
                                    listeners: {
                                        expand: function (d, e, i) {
                                            var store = d.store;
                                            //store.load();
                                            if (!this.isRecAdded) {
                                                var defaultData = {Id: null, Status: "All", Rank: 0};
                                                var p = new store.recordType(defaultData, 0);
                                                store.insert(0, p);
                                                this.isRecAdded = true;
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'checkbox',
                                    fieldLabel: 'Show only most recent file',
                                    checked: true,
                                    name: 'newestFile'
                                }
                            ]
                        },
                        {
                            columnWidth: .35,
                            layout: 'hbox',
                            layoutConfig: {
                                align: 'stretch',
                                pack: 'end',
                                defaultMargins: {
                                    top: 5,
                                    bottom: 0,
                                    right: 5,
                                    left: 0
                                }
                            },
                            height: 95,
                            hideBorders: true,
                            cls: 'eBook-reports-button-column',
                            ref: 'columnButtonContainer',
                            items: [
                                {
                                    xtype: 'button',
                                    text: 'Show log',
                                    id: 'LogBtn',
                                    cls: 'eBook-reports-button',
                                    iconCls: 'eBook-icon-checklist-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    ref: '../../logBtn',
                                    width: 100,
                                    boxMaxHeight:50,
                                    disabled: true,
                                    listeners: {
                                        click: function (btn, e) {
                                            var logView = btn.refOwner.refOwner.reportFSlog,
                                                grid = btn.refOwner.refOwner.reportFSgrid;

                                            if (logView.collapsed == true) {
                                                logView.show();
                                                logView.expand(true);
                                                logView.collapsed = false;
                                                grid.refreshVisibleFileServiceLog();
                                                btn.setText("Hide log");
                                            }
                                            else {
                                                logView.hide();
                                                logView.collapse(true);
                                                btn.setText("Show log");
                                            }
                                        }
                                    },
                                    scope: this
                                },
                                {
                                    xtype: 'button',
                                    text: 'Export',
                                    itemId: 'exportBtn',
                                    ref: '../../exportBtn',
                                    cls: 'eBook-reports-button',
                                    iconCls: 'eBook-icon-excel-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    width: 100,
                                    boxMaxHeight:50,
                                    listeners: {
                                        click: function (btn, e) {
                                            var searchFilterValues = btn.refOwner.getSearchValues(),
                                                grid = btn.refOwner.refOwner.reportFSgrid,
                                                sortState = grid.store.getSortState(),
                                                ccpfsdc = {
                                                    sv: me.serviceId,
                                                    dp: me.department,
                                                    ed: searchFilterValues.endDate,
                                                    dm: searchFilterValues.dateModified,
                                                    sid: searchFilterValues.status,
                                                    cn: searchFilterValues.clientName,
                                                    of: searchFilterValues.office,
                                                    nf: searchFilterValues.newestFile,
                                                    pid: eBook.User.personId
                                                };

                                            if (sortState) {
                                                ccpfsdc.sf = sortState.field;
                                                ccpfsdc.so = sortState.direction;
                                            }

                                            btn.ownerCt.getEl().mask("Generating excel");
                                            Ext.Ajax.request({
                                                url: '/EY.com.eBook.API.5/OutputService.svc/ExportFileService'
                                                ,
                                                method: 'POST'
                                                ,
                                                jsonData: {
                                                    ccpfsdc: ccpfsdc
                                                },
                                                callback: function (options, success, response) {
                                                    btn.ownerCt.getEl().unmask();
                                                    if (success) {
                                                        var obj = Ext.decode(response.responseText);
                                                        var id = obj.ExportFileServiceResult.Id;
                                                        window.open("pickup/" + id + ".xlsx");
                                                        btn.ownerCt.ownerCt.ownerCt.getEl().unmask();
                                                    } else {
                                                        alert('Export failed. ');
                                                    }
                                                }
                                                ,
                                                scope: this
                                            });

                                        }
                                    },
                                    scope: this
                                },
                                {
                                    xtype: 'button',
                                    text: 'Search/Refresh',
                                    itemId: 'searchBtn',
                                    cls: 'eBook-reports-button',
                                    iconCls: 'eBook-filter-ico-24',
                                    scale: 'medium',
                                    iconAlign: 'top',
                                    ref: '../../searchBtn',
                                    boxMaxHeight:50,
                                    width: 100,
                                    listeners: {
                                        click: function (btn, e) {
                                            var me = this,
                                                criteriaGrid = me.refOwner;

                                            criteriaGrid.search();
                                        }
                                    },
                                    scope: this
                                }
                            ]
                        }
                    ]
                }
            ]
            ,
            listeners: {
                afterRender: function (thisForm, options) {
                    var me = this;
                    me.keyNav = new Ext.KeyNav(me.el, {
                        //Pressing enter on any field will trigger the search
                        "enter": function (e) {
                            me.search();
                        },
                        scope: me
                    });
                }
            }
        });

        eBook.Reports.FileService.Criteria.superclass.initComponent.apply(this, arguments);
    },
    search: function () {
        var me = this,
            grid = me.refOwner.reportFSgrid;

        //retrieve filter settings
        var searchValues = me.getSearchValues();
        //utilize the storeload function in grid panel to reload data
        grid.storeLoad(searchValues);
    },
    exportToExcel: function (btn, e) {

    },
    getSearchValues: function () {
        var columnContainer = this.columnContainer,
            fields = columnContainer.findByType(Ext.form.Field),
            values = {};

        Ext.each(fields, function (fld) {
            var val = fld.getValue();
            values[fld.name] = typeof(val) != 'undefined' && val != null && val != "" ? val : null;
        }, this);

        return values;
    },
    enableRowSpecificButtons: function (enable) {
        var btnLog = Ext.getCmp('LogBtn');

        if (enable) {
            btnLog.enable();
        }
        else {
            if(btnLog.getText() != "Hide log") {
                btnLog.disable();
            }
        }
    }
});

Ext.reg('reportFScriteria', eBook.Reports.FileService.Criteria);