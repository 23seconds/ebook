eBook.Reports.FileService.Grid = Ext.extend(eBook.grid.PagedGridPanel, {
    initComponent: function () {
        var me = this,
            serviceId = null;

        Ext.apply(this, {
            title: me.title ? me.title : 'Search Result',
            //id: 'FSGrid',
            //override getRowClass in order to add css to rows
            cls: 'eBook-reports',
            draggable: false,
            sortChangeEnabled: false,
            activeRecord: null,
            viewConfig: {
                getRowClass: function (record, index, rowParams, store) {
                    return 'eBook-reports-row';
                }
            },
            storeConfig: {
                selectAction: 'GetFileServiceReport',
                fields: eBook.data.RecordTypes.ClientPersonFileServiceReport,
                idField: 'itemId',
                criteriaParameter: 'ccpfsdc',
                serviceUrl: eBook.Service.file
            },
            pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            },
            loadMask: true,
            border: false,
            columns: [
                {
                    header: 'Clientname',
                    sortable: true,
                    width: 200,
                    dataIndex: 'clientName'
                },
                {
                    header: 'Client GFIS',
                    sortable: true,
                    width: 80,
                    dataIndex: 'clientGFIS'
                },
                {
                    header: 'End Date',
                    sortable: true,
                    width: 120,
                    dataIndex: 'endDate',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value !== null) {
                            return value.format("d/m/Y");
                        } else {
                            return null;
                        }
                    }
                },
                {
                    header: 'Next Annual Meeting',
                    sortable: true,
                    width: 120,
                    dataIndex: 'nextAnnualMeeting',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value !== null) {
                            return value.format("d/m/Y");
                        } else {
                            return null;
                        }
                    }
                },
                {
                    header: 'Partner',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'partner'
                    //editor: 'headerfield'
                },
                {
                    header: 'Manager',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'manager'
                    //editor: 'headerfield'
                },
                {
                    header: 'Office',
                    sortable: true,
                    width: 120,
                    dataIndex: 'office'
                },
                {
                    header: 'Status',
                    sortable: true,
                    width: 300,
                    dataIndex: 'status',
                    columnCssClass: 'eBook-reports-poa-row-button x-btn-mc',
                    renderer: function (value, e, f, d, s, q, i) {
                        return '<div class="eBook-grid-row-combo-ico" style="font-weight: bold; padding-left: 16px; background-position: 1px 1px; background-repeat: no-repeat; background-size: 14px 14px;">' + value + '</div>';
                    }
                },
                {
                    header: 'Review Notes',
                    sortable: true,
                    width: 140,
                    columnCssClass: 'eBook-reports-YE-row-button x-btn-mc',
                    dataIndex: 'fileId',
                    renderer: function (value, e, f, d, s, q, i) {
                        var text = "Show Review Notes",
                            iconCls = "eBook-icon-checklist-24";
                        return '<div style="font-weight: bold; padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; background-size: 14px 14px;" class="' + iconCls + '" >' + text + '</div>';
                    }
                },
                {
                    header: 'Person',
                    sortable: true,
                    width: 120,
                    //flex: 1,
                    dataIndex: 'person'
                    //editor: 'headerfield'
                },
                {
                    header: 'Date Modified',
                    sortable: true,
                    width: 120,
                    dataIndex: 'dateModified',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value !== null) {
                            return value.format("d/m/Y H:i:s");
                        } else {
                            return null;
                        }
                    }
                },
                {
                    header: 'Days on Status',
                    sortable: true,
                    width: 120,
                    dataIndex: 'daysOnStatus'
                }
            ],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    'rowselect': {
                        fn: function (selectionModel, rowIndex) {
                            var grid = selectionModel.grid,
                                record = grid.getStore().getAt(rowIndex);
                            //Set the active record (needed because selection of row is lost)
                            grid.setActiveRecord(record);
                            if(grid.refOwner && grid.refOwner.criteriaGrid) {
                                grid.refOwner.criteriaGrid.enableRowSpecificButtons(true);
                            }
                            grid.refreshVisibleFileServiceLog();

                        },
                        scope: this
                    },
                    'rowdeselect': {
                        fn: function (selectionModel) {
                            var grid = selectionModel.grid;

                            if(grid.refOwner && grid.refOwner.criteriaGrid) {
                                grid.refOwner.criteriaGrid.enableRowSpecificButtons(false);
                            }
                            grid.setActiveRecord(null);
                        },
                        scope: this
                    }
                }
            }),
            //store: new eBook.data.JsonStore(storeCfg),
            listeners: {
                cellclick: function (grid, rowIndex, columnIndex, e) {
                    var record = grid.getStore().getAt(rowIndex),  // Get the Record
                        fieldName = grid.getColumnModel().getDataIndex(columnIndex), // Get field name
                        fieldValue = record.get(fieldName),
                        clientName = record.get('clientName'),
                        endDate = record.get('endDate');

                    if (fieldName == 'fileId') {
                        if (fieldValue) {
                            grid.showReviewNotes(false, fieldValue, grid.serviceId, clientName, endDate);
                        }
                    }
                    else if (fieldName == 'status') {
                        //Status column clicked
                        var coords = e.getXY();

                        //Only office validators can change the status
                        if(eBook.User.activeRole == "NBB Admin" || eBook.User.activeRole == "Admin") {
                            grid.showStatusContextMenu(coords, record, grid.serviceId);
                        }
                        else
                        {
                            Ext.Msg.alert('NBB Admin','Only a NBB Admin may change the status.');
                        }
                    }
                },
                afterrender: function (t) {
                    var me = this;

                    if(t.ownerCt && t.ownerCt.criteriaGrid) { //utilize the criteriagrid values if supplied
                        me.filterValues = t.ownerCt.criteriaGrid.getSearchValues();
                    }

                    me.storeLoad(me.filterValues);
                },
                sortchange: function (t) {
                    var me = this;

                    if(me.sortChangeEnabled) {
                        if(t.ownerCt && t.ownerCt.criteriaGrid) { //utilize the criteriagrid values if supplied
                            me.filterValues = t.ownerCt.criteriaGrid.getSearchValues();
                        }

                        me.storeLoad(me.filterValues);
                    }
                }
            },

            storeLoad: function (filterValues) {
                var me = this,
                    sortState = me.store.getSortState(),
                    params = {
                        Start: 0,
                        Limit: 25,
                        sv: me.serviceId ? me.serviceId : null,
                        dp: me.department ? me.department : null,
                        pid: eBook.User.personId
                    };

                me.setActiveRecord(null);

                if(filterValues)
                {
                    params.ed = filterValues.endDate ? filterValues.endDate : null;
                    params.dm = filterValues.dateModified ? filterValues.dateModified : null;
                    params.sid = filterValues.status ? filterValues.status.toString() : null; //array
                    params.cn = filterValues.clientName ? filterValues.clientName : null;
                    params.of = filterValues.office ? filterValues.office : null;
                    params.nf = filterValues.newestFile ? filterValues.newestFile : null;
                }

                //retrieve grid sort
                if(sortState)
                {
                    params.sf = sortState.field;
                    params.so = sortState.direction;
                }

                //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                Ext.apply(me.store.baseParams, params);

                me.store.load(
                    {
                        params: params,
                        callback: me.onLoadSuccess,
                        scope: me
                    }
                );
            },

            onLoadSuccess: function (r, options, success) {
                var me = this;
                if (success) {
                    me.getEl().unmask();
                    me.sortChangeEnabled = true;
                }
                else {
                    me.getEl().mask("Loading of data failed");
                }
            },
            createGridButton: function (value, contentid, record) {
                new Ext.Button({
                    text: value, handler: function (btn, e) {
                        alert("record data=" + record.data);
                    }
                }).render(document.body, contentid);
            }
        });

        eBook.Reports.FileService.Grid.superclass.initComponent.apply(this, arguments);

        me.mon(me.store, {
            load: function () {
                //disable record specific buttons on store load
                if(me.refOwner && me.refOwner.criteriaGrid) {
                    me.refOwner.criteriaGrid.enableRowSpecificButtons(false);
                }
            }
        });
    },
    setActiveRecord: function(value)
    {
        var me = this;
        me.activeRecord = value;
    },
    getActiveRecord: function()
    {
        var me = this;
        return me.activeRecord;
    },
    getStatusContextMenu: function (serviceId, status) {
        var me = this;

        me.statusContextMenu = new eBook.Reports.FileService.StatusContextMenu({serviceId : serviceId, status: status});
        return me.statusContextMenu;
    },
    showStatusContextMenu: function (coords, record, serviceId) {
        if(serviceId.toUpperCase() == '1ABF0836-7D7B-48E7-9006-F03DB97AC28B') {
            var me = this,
                ctxMnu = me.getStatusContextMenu(serviceId,record.status);

            ctxMnu.activeRecord = record;
            ctxMnu.readyCallback = {fn: me.updateFileService, scope: me};
            ctxMnu.showAt(coords);
        }
    },
    showReviewNotes: function (required, fileId, serviceId, clientName, endDate) {
        //This will open the review notes window
        var obj = {
                required: required,
                fileId: fileId,
                serviceId: serviceId,
                clientName: clientName,
                endDate: endDate,
                readOnly: true
            },
            wn = new eBook.Bundle.ReviewNotesWindow(obj);

        wn.show();
    },
    //If the log view is visible, update the log store
    refreshVisibleFileServiceLog: function()
    {
        var me = this,
            logView = me.refOwner.reportFSlog;

        if(logView && logView.collapsed === false) {
            var record = me.getActiveRecord();

            logView.getComponent("dataviewLog").storeLoad(record.get('fileId'), me.serviceId);
            return true;
        }

        return false;
    },
    updateFileService: function (record, statusId, status) {
        var me = this,
            ufsdc = { //file service update object
                FileId: record.get('fileId'),
                ServiceId: me.serviceId ? me.serviceId : null,
                Person: eBook.User.getActivePersonDataContract()
            };

        //add optionals
        if (statusId) {ufsdc.Status = statusId;}

        //call
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'UpdateFileService',
            method: 'POST',
            jsonData: {
                ufsdc: ufsdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText && Ext.decode(response.responseText).UpdateFileServiceResult) {
                    me.onUpdateFileServiceSuccess(record, statusId, status);
                } else {
                    eBook.Interface.showResponseError(response, me.title);
                    me.getEl().unmask();
                }
            },
            scope: me
        });
    },

    onUpdateFileServiceSuccess: function (record, statusId, status) {
        var me = this;
        //update record with status change
        if (statusId) {
            record.set('statusId', statusId);
            record.set('status', status);
        }
        record.commit();
        me.refreshVisibleFileServiceLog();
        me.setActiveRecord(null);
    }
});
Ext.reg('reportFSgrid', eBook.Reports.FileService.Grid);

eBook.Reports.FileService.StatusContextMenu = Ext.extend(Ext.menu.Menu, {

    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetUserSetStatusesList',
                criteriaParameter: 'cfspdc',
                autoDestroy: true,
                serviceUrl: eBook.Service.file,
                baseParams: { ServiceId: me.serviceId, Status: me.status, Person : eBook.User.getActivePersonDataContract()},
                disabled: this.readOnly,
                fields: eBook.data.RecordTypes.Statuses,
                autoLoad: false
            });

            //NbbFilingItems = [{text: 'Invoice uploaded by GTH', statusId: 11},{text: 'Invoice paid by Finance', statusId: 12},{text: 'Refiling requested by NBB Admin', statusId: 13}];

        Ext.apply(me, {
            items: [{text: 'Loading data, please wait', statusId: null}],
            listeners: {
                itemclick: me.handleItemClick, scope: me
            }
        });

        eBook.Reports.FileService.StatusContextMenu.superclass.initComponent.apply(this, arguments);
        store.load({callback: me.loadMeItems, scope: me});
    },
    loadMeItems: function (recs, opts, success) {
        var me = this;
        me.removeAll();
        if(recs.length > 0) {
            Ext.each(recs, function (rec) {
                    me.add({text: rec.get('Status'), statusId: rec.get('Id')});
                },
                me);
        }
        else
        {
            me.destroy();
            Ext.Msg.alert("Status cannot be changed.","Role or status does not allow an update.");
        }
    },
    handleItemClick: function (baseItem) {
        var me = this,
            statusId = baseItem.statusId,
            status = baseItem.text;

        //Use parentCaller function to handle the item click
        if (!me.activeRecord|| !me.readyCallback) {return;}

        if (statusId) {
            //trigger function defined in readyCallback property
            me.readyCallback.fn.call(me.readyCallback.scope, me.activeRecord, statusId, status);
        }
    }
});