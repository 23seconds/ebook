eBook.Reports.PowerOfAttorney.Grid = Ext.extend(eBook.grid.PagedGridPanel, {
    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            title: 'Search Result',
            id: 'poaGrid',
            //override getRowClass in order to add css to rows
            cls: 'eBook-reports',
            draggable: false,
            sortChangeEnabled: false,
            activeRecord: null,
            viewConfig: {
                getRowClass: function (record, index, rowParams, store) {
                    return 'eBook-reports-row';
                }
            },
            storeConfig: {
                selectAction: 'GetCodaPoa',
                fields: eBook.data.RecordTypes.CodaPoa,
                idField: 'bankAccount',
                criteriaParameter: 'ccpdc',
                serviceUrl: eBook.Service.client
            },
            pagingConfig: {
                displayInfo: true,
                pageSize: 25,
                prependButtons: true
            },
            loadMask: true,
            border: false,
            columns: [
                {
                    header: 'Clientname',
                    sortable: true,
                    width: 191,
                    dataIndex: 'clientName'
                },
                {
                    header: 'Client GFIS',
                    sortable: true,
                    width: 68,
                    dataIndex: 'clientGfis'
                },
                {
                    header: 'Bank Account',
                    sortable: true,
                    width: 109,
                    dataIndex: 'bankAccount'
                },
                {
                    sortable: true,
                    header: 'Bic',
                    width: 71,
                    dataIndex: 'bic'
                },
                {
                    header: 'Bank',
                    sortable: true,
                    width: 102,
                    dataIndex: 'bankName'
                },
                {
                    header: 'PMT Allow Coda',
                    sortable: true,
                    width: 88,
                    dataIndex: 'allowCoda',
                    renderer: me.setCheckImage
                },
                {
                    header: 'Active Client ACR',
                    sortable: false,
                    width: 97,
                    dataIndex: 'activeClientACR',
                    renderer: me.setCheckImage
                },
                {
                    header: 'Exact EY License',
                    sortable: true,
                    width: 94,
                    dataIndex: 'EOL_EYLicense',
                    renderer: me.setYesNoValue
                },
                {
                    header: 'Exact Dig Mailbox',
                    sortable: true,
                    width: 98,
                    dataIndex: 'EOL_DigMailbox',
                    renderer: me.setCheckImage
                },
                {
                    header: 'Exact Division',
                    sortable: true,
                    width: 81,
                    dataIndex: 'EOL_Division'
                },
                {
                    header: 'Status',
                    sortable: true,
                    width: 337,
                    columnCssClass: 'eBook-reports-poa-row-button x-btn-mc',
                    dataIndex: 'status',
                    renderer: function (value, e, f, d, s, q, i) {
                        return '<div class="eBook-grid-row-combo-ico" style="font-weight: bold; padding-left: 16px; background-position: 1px 1px; background-repeat: no-repeat; background-size: 14px 14px;">' + value + '</div>';
                    }
                },
                {
                    header: 'Status Modified',
                    sortable: true,
                    width: 110,
                    dataIndex: 'statusModified',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value) {
                            return value.format("d/m/Y H:i:s");
                        } else {
                            return null;
                        }
                    }
                },
                {
                    header: 'CODA Proxies',
                    sortable: true,
                    width: 123,
                    columnCssClass: 'eBook-reports-poa-row-button x-btn-mc',
                    dataIndex: 'repositoryItemId',
                    renderer: function (value, e, f, d, s, q, i) {
                        var text = "Open Document",
                            iconCls = "eBook-repository-add-ico-24";
                        if (Ext.isEmpty(value)) {
                            text = 'Upload Document';
                            iconCls = "eBook-repository-ico-16";
                        }
                        return '<div style="font-weight: bold; padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; background-size: 14px 14px;" class="' + iconCls + '" >' + text + '</div>';
                    }
                },
                {
                    header: 'Last Modified',
                    sortable: true,
                    width: 110,
                    dataIndex: 'lastModified',
                    renderer: function (value, e, f, d, s, q, i) {
                        if (value) {
                            return value.format("d/m/Y H:i:s");
                        } else {
                            return null;
                        }
                    }
                }
            ],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    'rowselect': {
                        fn: function (grid, rowIndex) {
                            var me = this,
                                record = me.getStore().getAt(rowIndex);
                            //Set the active record (needed because selection of row is lost)
                            me.setActiveRecord(record);
                            me.refOwner.criteriaGrid.enableRowSpecificButtons(true);
                            me.refreshVisibleCodaPoaLog();
                        },
                        scope: this
                    },
                    'rowdeselect': {
                        fn: function () {
                            var critGridView = me.refOwner.criteriaGrid;

                            critGridView.enableRowSpecificButtons(false);
                        },
                        scope: this
                    }
                }
            }),
            //store: new eBook.data.JsonStore(storeCfg),
            listeners: {
                cellclick: function (grid, rowIndex, columnIndex, e) {
                    var record = grid.getStore().getAt(rowIndex),  // Get the Record
                        fieldName = grid.getColumnModel().getDataIndex(columnIndex), // Get field name
                        fieldValue = record.get(fieldName),
                        clientId = record.get('clientId');

                    if (fieldName == 'repositoryItemId') {
                        //Power of Attorney column clicked

                        if (fieldValue) {
                            grid.openDocument(fieldValue, clientId);
                        }
                        else {
                            grid.uploadDocument(record);
                        }
                    }
                    else if (fieldName == 'status') {
                        //Status column clicked
                        var coords = e.getXY();

                        //Only office validators can change the status
                        if(eBook.User.activeRole == "CODA Admin" || eBook.User.activeRole == "Admin") {
                            grid.showStatusContextMenu(coords, record);
                        }
                        else
                        {
                            Ext.Msg.alert('CODA Admin','Only a CODA Admin may change the status.');
                        }
                    }
                },

                afterrender: function (t) {
                    var me = this,
                        searchValues = t.ownerCt.criteriaGrid.getSearchValues();

                    t.header.dom.onScroll = function(){t.getView().el.dom.scrollLeft = t.header.dom.scrollLeft;};

                    me.storeLoad(searchValues);
                },

                sortchange: function (t) {
                    var me = this;

                    if(me.sortChangeEnabled) {
                        var searchValues = t.ownerCt.criteriaGrid.getSearchValues();
                        me.storeLoad(searchValues);
                    }
                }
            },

            storeLoad: function (searchFilterValues) {
                var me = this,
                    sortState = me.store.getSortState(),
                    params = {
                        Start: 0,
                        Limit: 25,
                        ba: searchFilterValues.account,
                        bic: searchFilterValues.bic,
                        lm: searchFilterValues.dateModified,
                        sid: searchFilterValues.status,
                        cn: searchFilterValues.clientName,
                        bid: searchFilterValues.bank,
                        pid: eBook.User.personId,
                        ar: eBook.User.activeRole,
                        sm: searchFilterValues.statusModified,
                        ac: searchFilterValues.allowCoda,
                        li: searchFilterValues.EOL_EYLicense,
                        dm: searchFilterValues.EOL_DigMailbox,
                        aca: searchFilterValues.activeClientACR
                    };

                //retrieve grid sort
                if(sortState)
                {
                    params.sf = sortState.field;
                    params.so = sortState.direction;
                }

                //applying parameters to the store baseparams makes sure that the filter values are also taken into account when using the pagedtoolbar options
                Ext.apply(me.store.baseParams, params);

                me.store.load(
                    {
                        params: params,
                        callback: this.onLoadSuccess,
                        scope: me
                    }
                );
            },

            onLoadSuccess: function (r, options, success) {
                var me = this;
                if (success) {
                    me.getEl().unmask();
                    me.sortChangeEnabled = true;
                }
                else {
                    me.getEl().mask("Loading of data failed");
                }
            },
            createGridButton: function (value, contentid, record) {
                new Ext.Button({
                    text: value, handler: function (btn, e) {
                        alert("record data=" + record.data);
                    }
                }).render(document.body, contentid);
            }
        });

        eBook.Reports.PowerOfAttorney.Grid.superclass.initComponent.apply(this, arguments);

        me.mon(me.store, {
            load: function () {
                //disable record specific buttons on store load
                me.refOwner.criteriaGrid.enableRowSpecificButtons(false);
            }
        });
    },
    setYesNoValue : function(value)
    {
        if(value === "true")
        {
            return "yes";
        }
        else if(value === "false")
        {
            return "no";
        }
    },
    setCheckImage: function(value)
    {
        var iconCls = "";

        if(value === "true")
        {
            iconCls = "eBook-tickcircle-16";
        }
        else if(value === "false")
        {
            iconCls = "eBook-grid-row-delete-ico";
        }

        return '<div style="background-position: 50% 50%; background-repeat: no-repeat; background-size: 12px 12px; height:20px;" class="' + iconCls + '"></div>';
    },
    setActiveRecord: function(value)
    {
        var me = this;

        me.activeRecord = value;
    },
    getActiveRecord: function()
    {
        var me = this;

        return me.activeRecord;
    },
    getStatusContextMenu: function () {
        var me = this;

        if (me.statusContextMenu)
        {
            return me.statusContextMenu;
        }
        else {
            me.statusContextMenu = new eBook.Reports.PowerOfAttorney.StatusContextMenu({});
        }

        return me.statusContextMenu;
    },
    showStatusContextMenu: function (coords, record) {
        var me = this,
            ctxMnu = me.getStatusContextMenu();

        ctxMnu.activeRecord = record;
        ctxMnu.readyCallback = {fn: me.showCommentPrompt, scope: me};
        ctxMnu.showAt(coords);
    },
    openDocument: function (repositoryItemId, clientId) {
        var wn = new eBook.Repository.FileDetailsWindow({
            repositoryItemId: repositoryItemId,
            readOnly: false,
            readOnlyMeta: false,
            clientId: clientId
        });

        wn.show();
        wn.loadById();
    },
    //open upload window
    uploadDocument: function (record) {
        //open a file upload window supplying the clientId, coda poa category, optional replacement file id and demanding a store autoload callback
        var me = this,
        wn = new eBook.Repository.SingleUploadWindow({
            clientId: record.get('clientId'),
            standards: {location: 'e611d384-4488-4371-aa2d-22224667aa8e'},
            readycallback: {fn: me.showCommentPrompt, scope: me, record: record},
            existingItemId: record.get('repositoryItemId')
        });

        wn.show(me);
    },
    //coda poa update after successful document upload
    onUploadDocumentSuccess: function (record, repositoryItemId, statusId, status) {
        //Although all three are optional, at least one must be filled in
        if (!repositoryItemId) {
            Ext.Msg.alert('Document upload', 'update of document failed');
        }

        var me = this;
        me.updateCodaPoa(record, statusId, status, repositoryItemId);
    },
    deleteDocument: function(repositoryItemId){
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'DeleteFile',
                callback: Ext.Msg.alert('file removed','File was removed from repository.'),
                scope: this,
                params: Ext.encode({ cidc: { Id: repositoryItemId} })
            });
    },
    showCommentPrompt: function (record, repositoryItemId, statusId, status) {
        var me = this,
            buttons = {yes: "Yes", no: "No", cancel: "Cancel"},
            data = {record: record, repositoryItemId: repositoryItemId, statusId: statusId, status: status};

        if(repositoryItemId)
        {
            buttons.cancel += " & delete file";
        }

        Ext.Msg.show({
            title: 'Add a comment?',
            multiline: 75,
            buttons: buttons,
            data: data,
            fn: function (buttonId, text, opt) {
                var comment = null;
                switch (buttonId) {
                    case 'cancel':
                        //user cancels update
                        if(data.repositoryItemId) {me.deleteDocument(data.repositoryItemId);}
                        break;
                    case 'yes':
                        //user adds comment
                        if (text.length === 0) {return me.showCommentPrompt(record, repositoryItemId, statusId, status);}
                        comment = text;
                    default:
                        //default action is a coda poa record update
                        me.updateCodaPoa(data.record, data.statusId, data.status, data.repositoryItemId, comment);
                }
            }
        });
    },
    //update coda poa record
    updateCodaPoa: function (record, statusId, status, repositoryItemId, comment) {
        //Although all three are optional, at least one must be filled in
        if (!statusId && !repositoryItemId && !comment) {return;}

        var me = this,
        //coda poa update object
            ucpdc = {
                Id: record.get('bankAccount'),
                Person: eBook.User.getActivePersonDataContract()
            };

        //set optional updates
        if (statusId) {ucpdc.Status = statusId;}
        if (repositoryItemId) {ucpdc.RepositoryItem = repositoryItemId;}
        if (comment) {ucpdc.Comment = comment;}

        //call
        Ext.Ajax.request({
            url: eBook.Service.client + '/UpdateCodaPoa',
            method: 'POST',
            jsonData: {
                ucpdc: ucpdc
            },
            callback: function (options, success, response) {
                if (success && response.responseText && Ext.decode(response.responseText).UpdateCodaPoaResult === true){
                    me.onUpdateCodaPoaSuccess(record, statusId, status, repositoryItemId);
                } else {
                    Ext.Msg.alert('Update failed', 'Status was not updated.');
                }
            },
            scope: me
        });
    },
    //handle successful update of coda poa
    onUpdateCodaPoaSuccess: function (record, statusId, status, repositoryItemId) {
        var me = this,
            today = new Date();
        //update record with status change
        if (statusId) {
            record.set('statusId', statusId);
            record.set('status', status);
            record.set('statusModified', today);
            record.set('lastModified', today);
        }
        if (repositoryItemId) {
            record.set('repositoryItemId', repositoryItemId);
        }
        record.commit();
        me.refreshVisibleCodaPoaLog();
        me.setActiveRecord(null);
    },
    //If the log view is visible, update the log store
    refreshVisibleCodaPoaLog: function()
    {
        var me = this,
            logView = me.refOwner.reportPOAlog;

        if(logView && logView.collapsed === false) {
            var record = me.getActiveRecord(),
                bankAccount = record.get("bankAccount");

            //load log entries when log view is visible
            logView.getComponent("dataviewLog").storeLoad(bankAccount);

            return true;
        }

        return false;
    },
    //make sure contextmenu is destroyed when panel is destroyed
    destroy: function () {
        var me = this;

        if (me.statusContextMenu) {me.statusContextMenu.destroy();}
        eBook.Reports.PowerOfAttorney.Grid.superclass.destroy.call(this);
    }
});
Ext.reg('reportPOAgrid', eBook.Reports.PowerOfAttorney.Grid);


eBook.Reports.PowerOfAttorney.StatusContextMenu = Ext.extend(Ext.menu.Menu, {

    initComponent: function () {
        var me = this,
            store = new eBook.data.JsonStore({
                selectAction: 'GetCodaPoaStatusList',
                criteriaParameter: 'cibpdc',
                autoDestroy: true,
                serviceUrl: eBook.Service.client,
                baseParams: { Boolean: true, Person : eBook.User.getActivePersonDataContract()},
                disabled: this.readOnly,
                fields: eBook.data.RecordTypes.Statuses,
                autoLoad: false
            });

        Ext.apply(me, {
            items: [{text: 'Loading data, please wait', statusId: null}],
            listeners: {
                itemclick: me.handleItemClick, scope: me
            }
        });

        eBook.Reports.PowerOfAttorney.StatusContextMenu.superclass.initComponent.apply(this, arguments);
        store.load({callback: me.loadMeItems, scope: me});


    },
    loadMeItems: function (recs, opts, success) {
        var me = this;
        me.removeAll();
        if(recs.length > 0) {
            Ext.each(recs, function (rec) {
                    me.add({text: rec.get('Status'), statusId: rec.get('Id')});
                },
                me);
        }
        else
        {
            me.destroy();
            Ext.Msg.alert("Status cannot be changed.","Role or status does not allow an update.");
        }
    },
    handleItemClick: function (baseItem) {
        var me = this,
            statusId = baseItem.statusId,
            status = baseItem.text;

        //Use parentCaller function to handle the item click
        if (!me.activeRecord || !me.readyCallback) {return;}

        if (statusId) {
            //trigger function defined in readyCallback property
            me.readyCallback.fn.call(me.readyCallback.scope, me.activeRecord, null, statusId, status);
        }
    }
});