eBook.Reports.PowerOfAttorney.Criteria = Ext.extend(Ext.Panel, {
    initComponent: function () {
        var me = this;
        this.isRecAdded = false;
        this.isEmptyTemplateAdded = false;
        Ext.apply(this, {
            loadMask: true,
            title: 'Criteria',
            hideBorders: true,
            layout: 'form',
            ref: 'criteriaGrid',

            items:
            [
                {
                    layout: 'column',
                    hideBorders: true,
                    ref: 'columnContainer',
                    items: [
                            {
                                columnWidth: 0.24,
                                layout: 'form',
                                ref: 'column1',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Client name',
                                        name: 'clientName',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Status',
                                        displayField: 'Status',
                                        nullable:true,
                                        valueField: 'Id',
                                        mode: 'local',
                                        selectOnFocus: true,
                                        autoSelect: true,
                                        editable: false,
                                        forceSelection: true,
                                        triggerAction: 'all',
                                        store: new eBook.data.JsonStore({
                                            selectAction: 'GetCodaPoaStatusList',
                                            criteriaParameter: 'cibpdc',
                                            autoDestroy: true,
                                            serviceUrl: eBook.Service.client,
                                            baseParams: { Boolean: false, Person : eBook.User.getActivePersonDataContract()},
                                            disabled: this.readOnly,
                                            fields: eBook.data.RecordTypes.Statuses,
                                            autoLoad: true,
                                            sortInfo: {field: 'Rank', direction:'ASC'}
                                        }),
                                        listeners: {
                                            expand: function (d, e, i) {
                                                var store = d.store;
                                                if (!this.isRecAdded) {
                                                    var defaultData = {Id: null,
                                                        Rank: -1,
                                                        Status: "All"};
                                                    var p = new store.recordType(defaultData, -1);
                                                    store.insert(0, p);
                                                    this.isRecAdded = true;
                                                }
                                            }
                                        },
                                        name: 'status',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Status Modified',
                                        format: 'd/m/Y',
                                        name: 'statusModified',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'Last Modified',
                                        format: 'd/m/Y',
                                        name: 'dateModified',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: 0.24,
                                layout: 'form',
                                ref: 'column2',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Bank',
                                        displayField: 'Name',
                                        nullable:true,
                                        valueField: 'Id',
                                        mode: 'local',
                                        selectOnFocus: true,
                                        autoSelect: true,
                                        typeAhead:true,
                                        forceSelection: true,
                                        triggerAction: 'all',
                                        store: new eBook.data.JsonStore({
                                            selectAction: 'GetBanks',
                                            criteriaParameter: 'cidc',
                                            autoDestroy: true,
                                            serviceUrl: eBook.Service.client,
                                            storeParams: {},
                                            disabled: this.readOnly,
                                            fields: eBook.data.RecordTypes.BankList,
                                            autoLoad: true
                                        }),
                                        listeners: {
                                            expand: function (d, e, i) {
                                                var store = d.store;
                                                if (!this.isRecAdded) {
                                                    var defaultData = {Id: null, Name: "All"};
                                                    var p = new store.recordType(defaultData, 0);
                                                    store.insert(0, p);
                                                    this.isRecAdded = true;
                                                }
                                            }
                                        },
                                        name: 'bank',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'BIC',
                                        name: 'bic',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Account',
                                        name: 'account',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: 0.24,
                                layout: 'form',
                                ref: 'column3',
                                cls: 'eBook-reports-column',
                                items: [
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'PMT Allow Coda',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['1', 'Yes'], ['0', 'No']]
                                        }),
                                        name: 'allowCoda',
                                        value: '',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Client Status ACR',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['1', 'Active'], ['0', 'Inactive']]
                                        }),
                                        name: 'activeClientACR',
                                        value: '',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Exact EY license',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['1', 'Yes'], ['0', 'No']]
                                        }),
                                        name: 'EOL_EYLicense',
                                        value: '',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Exact Dig Mailbox',
                                        valueField: 'id',
                                        displayField: 'value',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: new Ext.data.ArrayStore({
                                            id: 0,
                                            fields: [
                                                'id',
                                                'value'
                                            ],
                                            data: [[null, "All"], ['1', 'Yes'], ['0', 'No']]
                                        }),
                                        name: 'EOL_DigMailbox',
                                        value: '',
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                columnWidth: 0.28,
                                layout: 'hbox',
                                layoutConfig: {
                                    align:'stretch',
                                    pack:'end',
                                    defaultMargins:{
                                        top:50,
                                        bottom:0,
                                        right:5,
                                        left:0
                                    }
                                },
                                height:120,
                                hideBorders: true,
                                cls: 'eBook-reports-button-column',
                                ref: 'columnButtonContainer',
                                items: [
                                        {
                                            xtype: 'button',
                                            text: 'Show log',
                                            ref: '../../btnLog',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-icon-checklist-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            width: 100,
                                            disabled: true,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var logView = btn.refOwner.refOwner.reportPOAlog,
                                                        grid = btn.refOwner.refOwner.reportPOAgrid;

                                                    if(logView.collapsed === true) {
                                                        logView.show();
                                                        logView.expand(true);
                                                        btn.setText("Hide log");
                                                    }
                                                    else{
                                                        logView.hide();
                                                        logView.collapse(true);
                                                        btn.setText("Show log");
                                                    }
                                                }
                                            },
                                            scope: this
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Upload file',
                                            ref: '../../btnUpload',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-repository-add-ico-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            width: 100,
                                            disabled: true,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var grid = btn.refOwner.refOwner.reportPOAgrid,
                                                        record = grid.getActiveRecord();

                                                    //open upload window dialog
                                                    grid.uploadDocument(record);
                                                }
                                            },
                                            scope: this
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Export',
                                            itemId: 'exportBtn',
                                            ref: '../../exportBtn',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-icon-excel-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            width: 100,

                                            listeners: {
                                                click: function (btn, e) {
                                                    var searchFilterValues = btn.refOwner.getSearchValues(),
                                                        grid = btn.refOwner.refOwner.reportPOAgrid,
                                                        sortState = grid.store.getSortState(),
                                                        ccpdc = {
                                                            ba: searchFilterValues.account,
                                                            bic: searchFilterValues.bic,
                                                            lm: searchFilterValues.dateModified,
                                                            sid: searchFilterValues.status,
                                                            cn: searchFilterValues.clientName,
                                                            bid: searchFilterValues.bank,
                                                            pid: eBook.User.personId,
                                                            ar: eBook.User.activeRole,
                                                            ac: searchFilterValues.activeRole,
                                                            sm: searchFilterValues.statusModified
                                                        };

                                                    if(sortState)
                                                    {
                                                        ccpdc.sf = sortState.field;
                                                        ccpdc.so = sortState.direction;
                                                    }

                                                    btn.ownerCt.getEl().mask("Generating excel");
                                                    Ext.Ajax.request({
                                                        url: '/EY.com.eBook.API.5/OutputService.svc/ExportCodaPoa',
                                                        method: 'POST',
                                                        jsonData: {
                                                            ccpdc: ccpdc
                                                        },
                                                        callback: function (options, success, response) {
                                                            btn.ownerCt.getEl().unmask();
                                                            if (success) {
                                                                var obj = Ext.decode(response.responseText);
                                                                var id = obj.ExportCodaPoaResult.Id;
                                                                window.open("pickup/" + id + ".xlsx");
                                                                btn.ownerCt.ownerCt.ownerCt.getEl().unmask();
                                                            } else {
                                                                alert('Export failed. ');
                                                            }
                                                        },
                                                        scope: this
                                                    });

                                                }
                                            },
                                            scope: this
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Search/Refresh',
                                            itemId: 'searchBtn',
                                            cls: 'eBook-reports-button',
                                            iconCls: 'eBook-filter-ico-24',
                                            scale: 'medium',
                                            iconAlign: 'top',
                                            ref: '../../searchBtn',
                                            height: 18,
                                            width: 100,
                                            listeners: {
                                                click: function (btn, e) {
                                                    var me = this,
                                                        criteriaGrid = me.refOwner;

                                                    criteriaGrid.search();
                                                }
                                            },
                                            scope: this
                                        }
                                ]
                            }
                        ]
                }
            ],
            listeners: {
                afterRender: function (thisForm, options) {
                    var me = this;
                    me.keyNav = new Ext.KeyNav(me.el, {
                        //Pressing enter on any field will trigger the search
                        "enter" : function(e){
                            me.search();
                        },
                        scope : me
                    });
                    //Set default values
                    var columnContainer = me.columnContainer,
                        fields = columnContainer.findByType(Ext.form.Field),
                        values = {};

                    Ext.each(fields,function(fld){
                        var name = fld.getName(),
                            defaultValue = null;

                        switch(name)
                        {
                            case 'allowCoda':
                            case 'activeClientACR':
                                defaultValue = fld.getStore().getAt(1);
                                fld.setValue(defaultValue ? defaultValue.id : null);
                                break;
                        }
                    },me);
                }
            }
        });

        eBook.Reports.PowerOfAttorney.Criteria.superclass.initComponent.apply(this, arguments);
    },
    search: function () {
        var me = this,
            grid = me.refOwner.reportPOAgrid;

        //retrieve filter settings
        var searchValues = me.getSearchValues();
        //utilize the storeload function in grid panel to reload data
        grid.storeLoad(searchValues);
    },
    exportToExcel: function (btn, e) {

    },
    getSearchValues: function () {
        var columnContainer = this.columnContainer,
            fields = columnContainer.findByType(Ext.form.Field),
            values = {};

        Ext.each(fields,function(fld){
            var val = fld.getValue();
            values[fld.name] = typeof(val) != 'undefined' && val !== null && val !== "" ? val : null;
        },this);

        return values;
    },
    enableRowSpecificButtons: function(enable) {
        var me = this;

        if(enable) {
            me.btnUpload.enable();
            me.btnLog.enable();
        }
        else {
           me.btnUpload.disable();
            if(me.btnLog.getText() != "Hide log") {
                me.btnLog.disable();
            }
        }
    }
});

Ext.reg('reportPOAcriteria', eBook.Reports.PowerOfAttorney.Criteria);