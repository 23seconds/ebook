eBook.Reports.PowerOfAttorney.Window = Ext.extend(eBook.Window, {
    initComponent: function () {
        Ext.apply(this, {
            title: 'Client - CODA Proxies Report',
            layout: {
                type: 'border',
                align: 'stretch'
            },
            width: Ext.getBody().getViewSize().width - 100,
            height: Ext.getBody().getViewSize().height - 100,
            items: [
                {xtype: 'reportPOAcriteria', region: 'north', height: 145, ref:'reportPOAcriteria' },
                { xtype: 'reportPOAgrid', region:'center', cls: 'eBookAT-GTH-Persongrid', ref: 'reportPOAgrid' },
                { xtype: 'reportPOAlog', region: 'east', width: 300, ref: 'reportPOAlog' }
            ]
        });

        eBook.Reports.PowerOfAttorney.Window.superclass.initComponent.apply(this, arguments);
    }
});