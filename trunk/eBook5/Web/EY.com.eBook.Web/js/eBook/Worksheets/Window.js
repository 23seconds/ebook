

eBook.Worksheets.Window = Ext.extend(eBook.Window, {
    wsClosed: false
    , initComponent: function() {
        var tb = [{
            ref: 'recalc',
            text: eBook.Worksheet.Window_Recalc,
            iconCls: 'eBook-icon-refreshworksheet-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onReCalcClick,
            hidden: this.wsClosed,
            scope: this
        }, {
            ref: 'print',
            text: eBook.Worksheet.Window_PrintPreview,
            iconCls: 'eBook-icon-previewworksheet-24',
            scale: 'medium',
            iconAlign: 'top',
            handler: this.onPrintClick,
            scope: this
}];
            if (this.customFunctions) {
                if (this.customFunctions.ProAccImportHistory) {
                    tb.push({
                        ref: 'importhistory',
                        text: eBook.Worksheet.Window_ImportHistory,
                        iconCls: 'eBook-icon-importproacc-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onImportHistoryClick,
                        scope: this
                    });
                }
                if (this.customFunctions.ExportExcel) {
                    tb.push({
                        ref: 'exportexcel',
                        text: 'Export Excel',
                        iconCls: 'eBook-excel-ico-24',
                        scale: 'medium',
                        iconAlign: 'top',
                        handler: this.onExportExcelClick,
                        scope: this
                    });
                }
            }

            tb.push('->');
            tb.push({
                ref: 'help',
                text: eBook.Worksheet.Window_HelpInfo,
                iconCls: 'eBook-icon-helpworksheet-24',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onHelpClick,
                scope: this
            });
            Ext.apply(this, {
                tbar: tb
            , items: [{ xtype: 'worksheet-tabpanel', wsClosed: this.wsClosed, ruleEngine: this.ruleEngine, tabPanels: this.tabPanels, ref: 'pnls'}]
            , layout: 'fit'
            });
            this.tabPanels = null;
            eBook.Worksheets.Window.superclass.initComponent.apply(this, arguments);
        }
    , show: function(tab, row, field) {
        eBook.Worksheets.Window.superclass.show.call(this);
        this.pnls.load();
        if (tab) {
            this.pnls.setMsg(tab, row, field);
        }
    }

    , onImportHistoryClick: function() {
        this.getEl().mask('importing');
        eBook.CachedAjax.request({
            url: this.ruleEngine.url + this.ruleEngine.baseMethod + 'ProAccImportHistory'
            , method: 'POST'
            , params: Ext.encode({ cfdc: {
                FileId: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
            }
            })
            , success: this.onImportHistorySuccess
            , failure: this.onImportHistoryFailure
            , scope: this
        });
    }
    , onImportHistorySuccess: function(resp, opts) {
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";

        this.pnls.loadData(o[resKey]);
        this.getEl().unmask();
    }
    , onImportHistoryFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.title);
        this.getEl().unmask();
    }
    , onReCalcClick: function() {
        this.pnls.load();
    }
    , onPrintClick: function() {
        this.getEl().mask("Preparing print", 'x-mask-loading');
        var widc = {
            "__type": "WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data"
            , "EndsAt": 0
            , "FooterConfig": { "Enabled": true, "FootNote": null, "ShowFooterNote": true, "ShowPageNr": true, "Style": null }
            , "HeaderConfig": { "Enabled": true, "ShowTitle": true, "Style": null }
            , "NoDraft": false
            , "StartsAt": 0
            , "iTextTemplate": null
            , "iconCls": null
            , "id": eBook.EmptyGuid
            , "indexed": true
            , "locked": false
            , "title": this.title
            , "Culture": eBook.Interface.Culture
            , "Data": null
            , "FileId": eBook.Interface.currentFile.get('Id')
            , "PrintLayout": "default"
            , "RuleApp": this.ruleApp
            , "WorksheetType": this.typeId
            , "man": null
            , "type": null
        };
        Ext.Ajax.request({
            url: eBook.Service.output + 'GetWorksheetPrint'
            , method: 'POST'
            , params: Ext.encode({ widc: widc })
            , callback: this.onPdfGenerated
            , scope: this
        });
    }
    , onPdfGenerated: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open(robj.GetWorksheetPrintResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onExportExcelClick: function() {
        this.getEl().mask("Preparing export", 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.output + 'GetWorksheetExcelExport'
                , method: 'POST'
                , params: Ext.encode({ cewedc: { FileId: eBook.Interface.currentFile.get('Id'), TypeId: this.typeId} })
                , callback: this.onExportExcelCallback
                , scope: this
        });

    }
    , onExportExcelCallback: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open('pickup/' + robj.GetWorksheetExcelExportResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onHelpClick: function() {
        //alert("Check your local champion.");
        eBook.Help.showHelp(this.initialConfig.id);
        //this.initialConfig.id    WORKSHEETID
    }
    , onBeforeClose: function() {
        eBook.Interface.center.fileMenu.updateTaxCalculation();
    }
    });

Ext.reg('worksheet-window', eBook.Worksheets.Window);
