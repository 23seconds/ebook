eBook.Worksheets.GridRowSelectionModel = Ext.extend(Ext.grid.RowSelectionModel, {
    handleMouseDown: function(g, rowIndex, e) {
        var detailedClick = e.getTarget('.x-grid3-row-expander');
        if (detailedClick) {
            this.grid.plugins.onMouseDown(e, null);
        } else {
            eBook.Worksheets.GridRowSelectionModel.superclass.handleMouseDown.apply(this, arguments);
        }
    }
});


eBook.Worksheets.Grid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        // construct tbar
        var details;
        var expander = null;
        var cols = [];
        var plugins = null;
        if (this.detailGrid) {
            expander = new Ext.ux.grid.RowExpander({
                getBodyContent: function(record, index) {
                    if (!this.enableCaching) {
                        return this.tpl.apply(record.json);
                    }
                    var content = this.bodyContent[record.id];
                    if (!content) {
                        content = this.tpl.apply(record.json);
                        this.bodyContent[record.id] = content;
                    }
                    return content;
                }
            , enableCaching: false
            , dataSelector: this.detailGrid.dataContainer
            , tpl: new Ext.XTemplate(
                '<div class="worksheet-detail-spacing">&nbsp;</div>'
		        , '<div class="worksheet-detail">'
			        , '<div class="worksheet-detail-left-out">'
				        , '<div class="worksheet-detail-left-out-top">&nbsp;</div>'
				        , '<div class="worksheet-detail-left-out-center">&nbsp;</div>'
				        , '<div class="worksheet-detail-left-out-bottom">&nbsp;</div>'
			        , '</div>'
			        , '<div class="worksheet-detail-left-in">'
				        , '<div class="worksheet-detail-left-in-top">&nbsp;</div>'
				        , '<div class="worksheet-detail-left-in-center">&nbsp;</div>'
				        , '<div class="worksheet-detail-left-in-bottom">&nbsp;</div>'
			        , '</div>'
			        , '<div class="worksheet-detail-center">'
				        , '<div class="worksheet-detail-center-center">'
					        , '<div class="worksheet-detail-title">Calculation details:</div>'
					        , '<div class="worksheet-detail-content">'
                                , this.detailGrid.tableTpl
                            , '</div>'
		                , '</div>'
                    , '</div>'
                    , '<div class="worksheet-detail-right-in">'
                        , '<div class="worksheet-detail-right-in-top">&nbsp;</div>'
                        , '<div class="worksheet-detail-right-in-center">&nbsp;</div>'
                        , '<div class="worksheet-detail-right-in-bottom">&nbsp;</div>'
                    , '</div>'
                    , '<div class="worksheet-detail-right-out">'
	                    , '<div class="worksheet-detail-right-out-top">&nbsp;</div>'
	                    , '<div class="worksheet-detail-right-out-center">&nbsp;</div>'
	                    , '<div class="worksheet-detail-right-out-bottom">&nbsp;</div>'
                    , '</div>'
                , '</div>', { getDate: function(val, fmat) {
                    val = Ext.data.Types.WCFDATE.convert(val);
                    return val.format(fmat);
                }
                })
            });
            cols.push(expander);
            plugins = expander;
        }

        cols.push({
            header: ' ',
            width: 20,
            dataIndex: 'IsValid',
            renderer: function(v, m, r, ridx, cidx, s) {
                var css = eBook.Interface.center.fileMenu.getItemMessageType(r.get('ID'));
                if (!Ext.isEmpty(css)) {
                    m.css = 'eb-message-type-' + css;
                    r.invalid = true;
                    //this.ownerCt.setIconClass('eb-message-type-ERROR');
                } else {
                    r.invalid = false;
                }
                return "";
            },
            scope: this,
            align: 'left'
        });
        cols = cols.concat(this.columns);

        var cfg = {
            tbar: []
            , listeners: {}
            , columns: cols
            //, disableSelection: true
            , plugins: plugins
            , sm: new eBook.Worksheets.GridRowSelectionModel({
                singleSelect: true
                        , listeners: {
                            'selectionchange': {
                                fn: this.onRowSelect
                                    , scope: this
                            }
                        }
            })
        };

        var closed = eBook.Interface.currentFile.get('Closed') || this.wsClosed;
        if (!this.readOnly) this.readOnly = closed;

        if (this.allowAdd && !this.readOnly) {
            cfg.tbar.push({
                ref: 'additem',
                text: eBook.Worksheet.GridPanel_AddItem,
                iconCls: 'eBook-icon-add-16',
                scale: 'small',
                iconAlign: 'left',
                handler: this.onAddItemClick,
                scope: this
            });
        }
        if (this.allowEdit && !this.readOnly) {
            cfg.tbar.push({
                ref: 'edititem',
                text: eBook.Worksheet.GridPanel_EditItem,
                iconCls: 'eBook-icon-edit-16',
                scale: 'small',
                iconAlign: 'left',
                disabled: true,
                handler: this.onEditItemClick,
                scope: this
            });
            cfg.listeners.rowdblclick = {
                fn: this.onRowDblClick
                , scope: this
            };
        }
        if (this.allowDelete && !this.readOnly) {
            cfg.tbar.push({
                ref: 'deleteitem',
                text: eBook.Worksheet.GridPanel_DeleteItem,
                iconCls: 'eBook-icon-delete-16',
                scale: 'small',
                iconAlign: 'left',
                disabled: true,
                handler: this.onDeleteItemClick,
                scope: this
            });
        }
        if (cfg.tbar.length == 0) cfg.tbar = null;

        cfg.store = { xtype: 'eb-jsonstore', fields: this.fields, idProperty: 'ID', root: this.collectionName };
        cfg.viewConfig = {
            getRowClass: function(record, index, rowParams, store) {
                var prev = record.get('PreviousImported');
                if (prev) {
                    return 'eb-worksheet-previousimported';
                }
                var a = record.get('AutoLine');
                if (!a) a = record.get('Autoline');
                if (!a) a = record.get('Disabled');
                if (!a) a = record.get('AutoKey');
                if (a) {
                    return 'eb-worksheet-autoline';
                }
                return '';
            }
        };

        this.fields = null;
        Ext.apply(this, cfg);





        eBook.Worksheets.Grid.superclass.initComponent.apply(this, arguments);
    }
    , onAddItemClick: function() {
        this.refOwner.showEdit();
    }
    , onEditItemClick: function() {
        var rec = this.getSelectionModel().getSelected();
        var a = rec.get('AutoLine');
        if (!a) a = rec.get('Autoline');
        if (!a) a = rec.get('Disabled');
        if (!a) a = rec.get('AutoKey');
        if (a) {
            eBook.Interface.showError("This is an automatic eBook line, only manual lines can be edited.", "Automatic line");
            return;
        }
        this.refOwner.showEdit(rec);
    }
    , onDeleteItemClick: function() {

        var rec = this.getSelectionModel().getSelected();
        var idx = this.store.find('ID', rec.get('ID'));
        this.store.removeAt(idx); // TO CHANGE WITH RED MARKING, UPON COMPLETION RELOAD STORES
        var win = this.refOwner.ownerCt.ownerCt; // window
        win.getEl().mask('Removing item');
        eBook.CachedAjax.request({
            url: this.ruleEngine.url + this.ruleEngine.baseMethod + this.collectionName + 'Remove'
                    , method: 'POST'
                    , params: Ext.encode({ cfrdc: {
                        Id: eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
                            , RowId: rec.get('ID')
                    }
                    })
                    , success: this.onDeleteSuccess
                    , failure: this.onDeleteFailure
                    , scope: this
        });


    }
    , onDeleteSuccess: function(resp, opts) {
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";
        this.refOwner.ownerCt.loadData(o[resKey]);
    }
    , onDeleteFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.refOwner.ownerCt.ownerCt.title);
        this.refOwner.ownerCt.ownerCt.getEl().unmask();
    }
    , onRowDblClick: function(grd, ridx, e) {
        if (!this.allowEdit) return;
        var rec = this.store.getAt(ridx);
        if (!Ext.isEmpty(this.rowDisabledField)) {
            if (rec.get(this.rowDisabledField)) {
                return;
            }
        }
        //        var a = rec.get('AutoLine');
        //        if (!a) a = rec.get('Autoline');
        //        if (!a) a = rec.get('Disabled');
        //        if (!a) a = rec.get('AutoKey');
        //        if (a) {
        //            eBook.Interface.showError("This is an automatic eBook line, only manual lines can be edited.", "Automatic line");
        //            return;
        //        }
        this.refOwner.showEdit(rec);
    }
    , highlightRowId: function(row, field) {
        var ridx = this.store.find('ID', row);
        if (ridx > -1) {
            var sm = this.getSelectionModel();
            //sm.clearSelections();
            sm.selectRow(ridx, false);
        }
    }
    , onRowSelect: function(sm) {
        /* TOOLBAR UPDATE */

        if (this.getTopToolbar()) {

            var ed = this.getTopToolbar().edititem;
            var dl = this.getTopToolbar().deleteitem;
            if (!sm.hasSelection()) {
                if (dl) dl.disable();
                if (ed) ed.disable();
            } else {
                if (dl) dl.enable();
                if (ed) ed.enable();
                if (!Ext.isEmpty(this.rowDisabledField)) {
                    var r = sm.getSelected();
                    if (r.get(this.rowDisabledField)) {
                        if (dl) dl.disable();
                        if (ed) ed.disable();
                        sm.clearSelections();
                    }
                }

            }
        }
    }
    , setSheetStatus: function(isClosed) {
        var tb = this.getTopToolbar();
        if (tb) {
            if (this.rendered) {
                tb.show();
                if (isClosed) tb.hide();
            } else {
                tb.hidden = isClosed;
            }
        }
    }
});

Ext.reg('worksheet-grid', eBook.Worksheets.Grid);