eBook.Worksheets.TabPanel = Ext.extend(Ext.TabPanel, {
    initComponent: function() {
        this.tpeId = this.ownerCt.typeId;
        for (var i = 0; i < this.tabPanels.length; i++) {
            this.tabPanels[i].wsClosed = this.wsClosed;
            this.tabPanels[i].iconCls = eBook.Interface.center.fileMenu.checkHealthTab(this.tpeId, this.tabPanels[i].xtype.split('-')[3]); // 'eb-message-type-ERROR';
        }
        Ext.apply(this, {
            enableTabScroll: true
            , activeTab: 0
            , items: this.tabPanels
        });
        this.tabPanels = null;
        eBook.Worksheets.TabPanel.superclass.initComponent.apply(this, arguments);
        this.on('beforetabchange', this.onTabChange, this);
        this.on('tabchange', this.onAfterTabChange, this);
    }
    , setMsg: function(tab, row, field) {
        for (var i = 0; i < this.items.getCount(); i++) {
            if (this.items.get(i).xtype.indexOf(tab) > -1) {
                this.setActiveTab(i);
                if (row) {
                    this.items.get(i).setMsg(row, field);
                }
                break;
            }
        }
    }
    , onTabChange: function(tp, newtab, currenttab) {
        return true;
    }
    , onAfterTabChange: function(tp, newtab, currenttab) {
        var form;
        for (var j = 0; j < newtab.items.items.length; j++) {
            if (newtab.items.items[j].form) {
                form = newtab.items.items[j].getForm();
            }
        }
        /*
        var field = form.findField('IRRemainingResultBelgium');
        if (field) {
        field.markInvalid('You dumb shit!');
        }
        */
        var fields = eBook.Interface.center.fileMenu.getFieldsByTab(this.tpeId, form.xtype.split('-')[3]);
        if (fields) {
            for (var i = 0; i < fields.items.length; i++) {
                var field = fields.items[i].data;
                var fieldName = field.Field;
                var item = form.findField(fieldName);
                if (item) {
                    item.markInvalid(field.Text);
                }
            }
        }

        return true;
    }
    , updateTabStatus: function(sts) {
        return true;
    }
    , colStores: {}
    , colStoreIds: []
    , data: null
    , getCollectionsStore: function(collections, displayField, valueField, sid) {
        if (this.colStores[sid]) return this.colStores[sid];

        var flds = [{ name: 'id', mapping: 'ID', type: Ext.data.Types.STRING }
            , { name: 'nl', mapping: displayField, type: Ext.data.Types.STRING }
            , { name: 'fr', mapping: displayField, type: Ext.data.Types.STRING }
            , { name: 'en', mapping: displayField, type: Ext.data.Types.STRING}]
        this.colStores[sid] = new Ext.data.JsonStore({
            fields: flds
            , idProperty: 'id'
            , root: 'cols'
            , collections: collections
            , cfg: { displayField: displayField, collections: collections }
        });
        this.colStoreIds.push(sid);
        if (this.data != null) this.loadColStore[sid];
        return this.colStores[sid];
    }
    , loadColStore: function(sid) {
        var cols = [];
        var str = this.colStores[sid];
        for (var i = 0; i < str.collections.length; i++) {
            if (this.data[str.collections[i]]) {
                cols = cols.concat(this.data[str.collections[i]]);
            }
        }
        if (cols.length > 0) {
            this.colStores[sid].loadData({ cols: cols });
        } else {
            if (this.colStores[sid].getCount() > 0) this.colStores[sid].removeAll();
        }
    }
    , load: function() {
        this.ownerCt.getEl().mask('loading');

        eBook.CachedAjax.request({
            url: this.ruleEngine.url + this.ruleEngine.baseMethod + 'GetData'
                    , method: 'POST'
                    , params: Ext.encode({ cfdc: {
                        FileId: eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
                    }
                    })
                    , success: this.onLoadSuccess
                    , failure: this.onLoadFailure
                    , scope: this
        });
    }
    , onLoadSuccess: function(resp, opts) {
        if(resp.responseText) {
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";

            this.loadData(o[resKey]);
        }
        else
        {
            Ext.Msg.alert("No response from server", "Please contact IT using ebookquestions@be.ey.com.");
            this.ownerCt.getEl().unmask();
        }
    }
    , onLoadFailure: function(resp, opts) {
        eBook.Interface.showResponseError(resp, this.refOwner.title);
        this.ownerCt.getEl().unmask();
    }
    , disableRisicoKapitaalHistoriekHistoriekToolbar: function() {
        if (this.ruleEngine.baseMethod == 'RisicoKapitaalHistoriekApp') {
            this.items.each(function(it, idx, len) {
                if (it.myView.collectionName == 'Historiek') {
                    //if (this.data.HistoryTabDisabled)
                    //  it.myView.topToolbar.hide();
                    if (eBook.Interface.currentFile.data.PreviousEndDate)
                        it.myView.topToolbar.hide();
                }
            }, this);
        }
    }
    , loadData: function(dta) {
        this.data = dta;
        this.reloadStores();
        //this.disableRisicoKapitaalHistoriekHistoriekToolbar();
        this.ownerCt.getEl().unmask();
    }
    , reloadStores: function() {
        this.items.each(function(it, idx, len) {
            if (it.myView.store) {
                if (this.data[it.myView.store.root]) {
                    it.myView.store.loadData(this.data);
                } else {
                    it.myView.removeAll();
                }
            } else {
                it.myView.loadDta(this.data[it.myView.collectionName], true);
            }
        }, this);
        for (var i = 0; i < this.colStoreIds.length; i++) {
            this.loadColStore(this.colStoreIds[i]);
        }
    }
    , findCollectionItem: function(collection, field, value) {
        var col = this.data[collection];
        if (col) {
            if (!Ext.isArray(col)) return col;
            for (var i = 0; i < col.length; i++) {
                var fld = col[i][fld];
                if (Ext.isDefined(fld) && fld == value) return col[i];
            }
        }
        return {};
    }
    , tabChange: function(tabPnl, tab) {
        alert('tab clicked');
    }
});

Ext.reg('worksheet-tabpanel', eBook.Worksheets.TabPanel);