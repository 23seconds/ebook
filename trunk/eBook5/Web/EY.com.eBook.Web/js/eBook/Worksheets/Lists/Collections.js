
eBook.Worksheets.Lists.Collections = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.mode = 'local';
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes.ListAttribute1) && Ext.isDefined(cfg.attributes.ListAttribute2) && Ext.isDefined(cfg.attributes.ListAttribute3)) {
            cfg.collections = cfg.attributes.ListAttribute1.split(',');
            cfg.displayField = cfg.attributes.ListAttribute2;
            cfg.valueField = cfg.attributes.ListAttribute3;
        }

        eBook.Worksheets.Lists.Collections.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //
        id = Ext.id();
        var sid = String.format("{0}_{1}", this.entityCollection, this.name);
        Ext.apply(this, {
            store: this.ownerCt.ownerCt.ownerCt.getCollectionsStore(this.collections, this.displayField, this.valueField, id)
            , typeAhead: false
            , id:id
            , triggerAction: 'all'
            , lazyRender: true
            , width: 200
            , forceSelection: true
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            //, pageSize: 20
            //, listWidth: 100
        });
        eBook.Worksheets.Lists.Collections.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Collections', eBook.Worksheets.Lists.Collections);

