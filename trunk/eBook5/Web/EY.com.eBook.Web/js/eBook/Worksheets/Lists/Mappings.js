
eBook.Worksheets.Lists.Mappings = Ext.extend(Ext.form.TriggerField, {
    initComponent: function() {
        this.mappingKey = this.attributes['ListAttribute1'];
        Ext.apply(this, {
            triggerConfig: {
                tag: 'span', cls: 'x-form-twin-triggers', cn: [
                { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger ' },
                { tag: "img", src: Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger x-form-clear-trigger' }
                ]
            }
            , grow: true
            , growMax: 200
            , growMin: 100
            , nullable: true
        });
        this.addEvents(
            'clear',
            'change',
            'autosize'
        );
        this.on('show', function() {
            this.trigger_clear.show();
            this.fireEvent('change', this);
        });
        eBook.Worksheets.Lists.Mappings.superclass.initComponent.apply(this, arguments);
    }
    , onTriggerClick: function() {
        var mnu = eBook.Accounts.Mappings.Menus[this.mappingKey];
        mnu.showField(this.getEl());
        if (mnu.getWidth() < this.getWidth()) {
            mnu.setWidth(this.getWidth());
        }
    }
    , getValue: function() {
        return this.value;
    }
    , setValue: function(val) {
        if (val == null) {
            this.clearValue();
            return;
        }
        eBook.Worksheets.Lists.Mappings.superclass.setValue.call(this, val[eBook.Interface.Culture.substr(0, 2)]);
        this.autoSize();
        this.value = val;
    }
    , autoSize: function() {
        if (!this.grow || !this.rendered) {
            return;
        }
        if (!this.metrics) {
            this.metrics = Ext.util.TextMetrics.createInstance(this.el);
        }
        var el = this.el;
        var v = el.dom.value;
        var d = document.createElement('div');
        d.appendChild(document.createTextNode(v));
        v = d.innerHTML;
        Ext.removeNode(d);
        d = null;
        v += ' ';
        var w = Math.min(this.growMax, Math.max(this.metrics.getWidth(v) + /* add extra padding */10, this.growMin));
        this.el.setWidth(w);
        this.fireEvent('autosize', this, w);
    }
    , onRender: function(ct, position) {
        eBook.Worksheets.Lists.Mappings.superclass.onRender.call(this, ct, position);
        var self = this;
        var triggers = this.trigger.select('.x-form-trigger', true).elements;
        for (var i; i < triggers.length; i++) {
            triggers[i].addClassOnOver('x-form-trigger-over');
            triggers[i].addClassOnOver('x-form-trigger-over');
        }
        if (this.nullable) {
            this.trigger_clear = triggers[1];
            this.trigger_clear.hide = function() {
                var w = self.wrap.getWidth();
                this.dom.style.display = 'none';
                self.el.setWidth(w - self.trigger.getWidth());
            };
            this.trigger_clear.show = function() {
                var w = self.wrap.getWidth();
                this.dom.style.display = '';
                self.el.setWidth(w - self.trigger.getWidth());
            };
            this.mon(this.trigger_clear, 'click', this.clearValue, this, { stopPropagation: true });
        }

        //if (!this.getValue())
        //this.trigger_clear.hide();
    }
    , clearValue: function() {
        if (this.hiddenField) {
            this.hiddenField.value = '';
        }
        this.setRawValue('');
        this.lastSelectionText = '';
        this.applyEmptyText();
        this.value = null;
    }
    , onDestroy: function() {
        Ext.destroy(this.triggers);
        return eBook.Worksheets.Lists.Mappings.superclass.onDestroy.call(this);
    }
});

/*
eBook.Worksheets.Lists.Mappings = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        if (cfg.attributes) {
            cfg.storeMethod = 'GetMappingGroupItems';
            cfg.criteriaParameter = 'cmdc';
            cfg.mode = 'remote';
            cfg.storeParams = { Culture: eBook.Interface.Culture
                            , Key: cfg.attributes['ListAttribute1']
                            , Meta: Ext.isDefined(cfg.attributes['ListAttribute2']) ? cfg.attributes['ListAttribute2'] : null
            };
            console.log(cfg.attributes['ListAttribute1']);
        }
        eBook.Worksheets.Lists.Mappings.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: 'cmdc'
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , autoDestroy: true
            })
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.Mappings.superclass.initComponent.apply(this, arguments);
    }
});
*/
Ext.reg('ewList_Mappings', eBook.Worksheets.Lists.Mappings);
