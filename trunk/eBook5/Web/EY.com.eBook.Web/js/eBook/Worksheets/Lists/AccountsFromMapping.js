
eBook.Worksheets.Lists.AccountsFromMapping = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes['ListAttribute1'])) {
            cfg.storeMethod = 'GetAccountsListFilteredByMapping';
            cfg.criteriaParameter = 'cadc';
            cfg.mode = 'remote';
            cfg.queryParam = 'Query';
            cfg.storeParams = { FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                        , Key: cfg.attributes.ListAttribute1
                        , Meta: Ext.isDefined(cfg.attributes.ListAttribute2) ? cfg.attributes.ListAttribute2 : null
                        , MappingItemIds: Ext.isDefined(cfg.attributes.ListAttribute3) ? cfg.attributes.ListAttribute3 : null
            };
//            if (Ext.isDefined(cfg.attributes['ListAttribute3'])) {
//                if (eBook.isGuid(cfg.attributes['ListAttribute3'])) {
//                    cfg.storeParams.WorksheetTypeId = cfg.attributes.ListAttribute3;
//                }
//                else {
//                    cfg.storeParams.WorksheetTypeId = null;
//                    cfg.storeParams.WorksheetTypeRuleApp = cfg.attributes.ListAttribute3;
//                }
//            }

        }
        eBook.Worksheets.Lists.AccountsFromMapping.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.schema
            })
            , minChars: 1
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , pageSize: 20
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.AccountsFromMapping.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_AccountsFromMapping', eBook.Worksheets.Lists.AccountsFromMapping);
