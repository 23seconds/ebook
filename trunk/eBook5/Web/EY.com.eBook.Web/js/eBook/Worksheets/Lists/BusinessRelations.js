

eBook.Worksheets.Lists.Customers = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {

            if (!Ext.isDefined(cfg)) cfg = {};
            cfg.storeMethod = 'GetCustomersList';
            cfg.mode = 'remote';
            cfg.criteriaParameter = 'cbidc';
            cfg.storeParams = { Id: eBook.CurrentClient
                            ,Fields:['Name','FirstName']
            };
            cfg.queryParam = 'Query';

            eBook.Worksheets.Lists.Customers.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.businessrelation
            })
            , minChars:3
            , displayField: eBook.Interface.Culture.substr(0,2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , forceSelection:true
            , lazyRender: true
            , pageSize: 20
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.Customers.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Customers', eBook.Worksheets.Lists.Customers);

eBook.Worksheets.Lists.Suppliers = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {

        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetSuppliersList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cbidc';
        cfg.storeParams = { Id: eBook.CurrentClient
                             , Fields: ['Name', 'FirstName']
        };
        cfg.queryParam = 'Query';

        eBook.Worksheets.Lists.Suppliers.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.businessrelation
            })
            , minChars: 3
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , lazyRender: true
            , pageSize: 20
            , listWidth: 300
        });
        eBook.Worksheets.Lists.Suppliers.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Suppliers', eBook.Worksheets.Lists.Suppliers);
