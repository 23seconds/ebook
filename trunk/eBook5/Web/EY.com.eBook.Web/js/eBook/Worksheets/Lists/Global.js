

eBook.Worksheets.Lists.Global = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {

        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cldc';
        cfg.storeParams = { lik: cfg.attributes.ListAttribute1
                            ,lid:null
                            , c: eBook.Interface.Culture
                            , ay: (eBook.Interface.currentFile!=null ? eBook.Interface.currentFile.get('AssessmentYear') : null)
                            , sd: null
                            ,ed:null
        };

        eBook.Worksheets.Lists.Global.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
            })
            
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: this.width || 200
        });
        eBook.Worksheets.Lists.Global.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Global', eBook.Worksheets.Lists.Global);




eBook.Worksheets.Lists.FOD = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {

        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetFodGlobalList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cfldc';
        cfg.storeParams = { ListId: cfg.attributes.ListAttribute1
                            , Culture: eBook.Interface.Culture
        };

        eBook.Worksheets.Lists.FOD.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
            })
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheets.Lists.FOD.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_FOD', eBook.Worksheets.Lists.FOD);
