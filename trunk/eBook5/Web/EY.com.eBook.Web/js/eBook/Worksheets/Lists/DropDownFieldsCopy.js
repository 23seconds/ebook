

eBook.Worksheets.Lists.DropDownFieldsCopy = Ext.extend(eBook.Worksheets.Lists.Collections, {
    constructor: function(config) {
        if (!Ext.isDefined(config)) config = {};
        if (!Ext.isDefined(config.attributes)) config.attributes = {};
        config.attributes.ListAttribute3 = 'ID';
        eBook.Worksheets.Lists.DropDownFieldsCopy.superclass.constructor.call(this,config);
        this.on('collapse', this.onCollapse);
    }
    , onCollapse: function(combo) {
        var di = eBook.Worksheet[this.ruleApp].findDataItem(this.collections,this.valueField, this.getValue());
        var frmp = this.findParentByType(eBook.Worksheets.FormPanel);
        if (frmp && di!=null) {
            frm = frmp.getForm();
            for (var it in di) {
                if (Ext.isDefined(it) && it != 'remove' && it!='ID') {
                    var f = frm.findField(it);
                    if (Ext.isDefined(f) && f != null) {
                        f.setValue(di[it]);
                    }
                }
            }
            frmp.performEvents();
        }
    }
});
Ext.reg('ewList_DropDownFieldsCopy', eBook.Worksheets.Lists.DropDownFieldsCopy);
Ext.reg('ewDropDownFieldsCopy', eBook.Worksheets.Lists.DropDownFieldsCopy);
