
eBook.Worksheets.Lists.BookyearMonths = Ext.extend(eBook.Worksheets.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetBookyearMonths';
        cfg.mode = 'local';
        cfg.criteriaParameter = 'cmldc';
        cfg.storeParams = { From: eBook.Interface.currentFile.get('StartDate'), To: eBook.Interface.currentFile.get('EndDate') };

        eBook.Worksheets.Lists.BookyearMonths.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
                    , autoLoad:true
            })
            , displayField: 'nl'
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , forceSelection: true
            , lazyRender: true
            , width: 200
            //, pageSize: 20
            //, listWidth: 300
        });
        eBook.Worksheets.Lists.BookyearMonths.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_BookyearMonths', eBook.Worksheets.Lists.BookyearMonths);
