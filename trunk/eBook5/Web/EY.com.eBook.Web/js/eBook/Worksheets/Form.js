
eBook.Worksheets.Form = Ext.extend(Ext.form.FormPanel, {
    initComponent: function () {
        var closed = eBook.Interface.currentFile.get('Closed') || this.wsClosed;
        /*
        var test = this.findField('IRRemainingResultBelgium');
        test.markInvalid('You dumb shit!');

        
        var formItems = this.items[0].items;
        console.log(formItems[0].entityCollection);
        console.log('------');
        for (var i = 0; i < formItems.length; i++) {
        console.log(formItems[i].name);
        formItems[i].markInvalid('You dumb shit!');
        }
        console.log(' ');
        */
        Ext.apply(this, {
            bodyStyle: 'padding:10px'
            , autoScroll: true
            , cls: 'ebook-worksheet-form'
            , layoutConfig: { labelAlign: 'left', labelWidth: 300 }
            , labelWidth: 300
        });

        if (closed) {
            Ext.apply(this, {
                defaults: { disabled: true }
            });
        }

        if (!closed) {
            Ext.apply(this, {
                buttons: [{
                    //text:'Bewaar gegevens',
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onValidate,
                    scope: this
                }, {
                    //text: 'Annuleer',
                    ref: '../cancelButton',
                    iconCls: 'eBook-window-cancel-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onCancelClick,
                    scope: this
                }
                ]
            });
        }

        eBook.Worksheets.Form.superclass.initComponent.apply(this, arguments);
    }
    , onCancelClick: function (silent) {
        // detect form only
        var standalone = this.initialConfig.ref == 'myView';
        if (standalone) {
            this.getForm().reset();
        }
        else {
            this.refOwner.showView();
        }
    }
    , onValidate: function () {
        if (this.hasDirtyFields()) {
            var win = this.refOwner.ownerCt.ownerCt;
            var reqObj = {
                cedc: {
                    Id: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
                    , CollectionPath: this.collectionName
                    , Entity: this.getEntityObject()
                }
            };
            win.getEl().mask('Validating', 'x-mask-loading');



            /*
            tabs.each(function(tab) {
            tab.setIconCls = eBook.Interface.center.fileMenu.checkHealthTab(this.tabPanels[i].xtype.split('-')[3]);
            }, this);
            */
            var url = this.ruleEngine.url + this.ruleEngine.baseMethod + this.entityName + 'Validate'; ;
            eBook.CachedAjax.request({
                url: url //eBook.Service.url + action
                , method: 'POST'
                , params: Ext.encode(reqObj)
                , success: this.onValidateSuccess
                , failure: this.onValidateFailure
                , scope: this
            });

        } else {
            this.refOwner.showView();
        }
    }
    , onValidateSuccess: function (resp, opts) {
        // this.entityName
        var win = this.refOwner.ownerCt.ownerCt;
        var resultName = this.ruleEngine.baseMethod + this.entityName + 'ValidateResult'
        var o = Ext.decode(resp.responseText);
        var result = o[resultName];
        if (result.length == 0) {
            //success, can be saved
            this.saveForm();
        } else {
            win.getEl().unmask();
            //alert("ERRORS - To be implemented");
            var bfrm = this.getForm();
            for (var i = 0; i < result.length; i++) {
                var msg = result[i];
                var f = bfrm.findField(msg.wtfld);
                if (f) {
                    f.setActiveError(msg.tx, false);
                    f.markInvalid(msg.tx);
                }
            }
        }

    }
    , onValidateFailure: function (resp, opts) {
        var win = this.refOwner.ownerCt.ownerCt;
        win.unmask();
        alert("validation failed");
    }
    , saveForm: function () {

        if (this.hasDirtyFields()) {
            // find worksheet window to mask
            var win = this.refOwner.ownerCt.ownerCt;

            win.mask(eBook.Worksheet.FormPanel_UpdatingItem, 'x-mask-loading');
            var reqObj = { cedc: {
                Id: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
                    , CollectionPath: this.collectionName
                    , Entity: this.getEntityObject()
            }
            };

            var url = this.ruleEngine.url + this.ruleEngine.baseMethod + this.collectionName;
            url += (reqObj.cedc.Entity.ID == eBook.EmptyGuid) ? 'Add' : 'Update';
            //var action = url + ((id == "ADDING" || Ext.isEmpty(id)) ? "InRuleAddItem" : "InRuleUpdateItem");
            Ext.Ajax.request({
                url: url //eBook.Service.url + action
                , method: 'POST'
                , params: Ext.encode(reqObj)
                , success: this.onSaveSuccess
                , failure: this.onSaveFailure
                , scope: this
            });
        } else {
            this.refOwner.ownerCt.ownerCt.getEl().unmask();
            this.refOwner.showView();
        }
    }
    , onSaveFailure: function (resp, opts) {
        var wn = this.refOwner.ownerCt.ownerCt;
        wn.getEl().unmask();
        eBook.Interface.showResponseError(resp, this.title);
    }
    , onSaveSuccess: function (resp, opts) {
        // show/hide error icon in tab
        var win = this.refOwner.ownerCt.ownerCt;
        var tabs = win.items.items[0].items.items;
        eBook.Interface.reloadHealthUpdateTabs(tabs);


        // subset van alle velden in deze tab met error/warning
        // alle veld markeringen verwijderen, dan
        // veld ophalen voor css toe te passen
        // nml. this.getForm().findField('');

        this.refOwner.ownerCt.ownerCt.getEl().unmask();
        //update worksheet data
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";
        //  this.loadData(o[resKey].Data);
        if (o[resKey]) {
            this.refOwner.showView();
            this.refOwner.ownerCt.loadData(o[resKey]);



            eBook.Interface.reloadHealthUpdateFields(this.getForm());


            /*
            // show / hide error message on field
            var fVb = this.getForm().findField('IRRemainingResultBelgium');
            if (fVb) {
            fVb.enable();
            fVb.markInvalid('You dumb shit!');
            //var fieldId = fVb.id;
            //this.getForm().markInvalid({ id: fieldId, msg: 'You dumb shit 2' });
            }
            */
        } else {
            // eBook.Worksheet[this.ruleApp].loadMessages(o[resKey].Messages);
            //this.setFieldMessages();
            Ext.Msg.show({ icon: Ext.Msg.ERROR, modal: true, msg: eBook.Worksheet.FormPanel_ErrorsMsg, title: eBook.Worksheet.FormPanel_ErrorsTitle, buttons: Ext.Msg.OK });
        }
    }
    , getEntityObject: function () {
        var o = {},
            n,
            key,
            val;
        this.getForm().items.each(function (f) {
            n = f.getName();
            key = o[n];
            val = f.getValue();
            if (Ext.isEmpty(val) || val == "null") val = null;
            if (Ext.isDefined(key)) {
                if (Ext.isArray(key)) {
                    o[n].push(val);
                } else {
                    o[n] = [key, val];
                }
            } else {
                o[n] = val;
            }
        });
        if (o.ID == null) o.ID = eBook.EmptyGuid;
        return o;
    }
    , fieldVisibility: function () {
        var ent = this.getEntityObject();
        this.getForm().items.each(function (f) {
            if (f.functions && f.functions.visibility) {
                var vis = f.functions.visibility(ent);
                if (vis) {
                    f.show();
                } else {
                    f.hide();
                }
            }
        });
    }
    , getWorksheetObject: function () {
        return {
            data: this.refOwner.ownerCt.data
            , findCollectionItem: function (collection, field, value) {
                var col = this.data[collection];
                if (col) {
                    if (!Ext.isArray(col)) return col;
                    for (var i = 0; i < col.length; i++) {
                        var fld = col[i][field];
                        if (Ext.isDefined(fld) && fld == value) return col[i];
                    }
                }
                return {};
            }
        };
    }
    , onFieldChange: function (fld, nv, ov) {
        var funcs = false;
        if (fld.functions) {
            if (fld.functions.hasFunctions) {
                for (var fnc in fld.functions) {
                    var fnt = fld.functions[fnc];
                    if (Ext.isFunction(fnt)) {
                        var ent = fnt.call(this, fld, this, this.getEntityObject(), this.getWorksheetObject(), this);
                        funcs = true;
                        this.setValues(ent, fld.name, true);
                        this.resumeEvents();
                    }
                }
            }
        }
        if (!funcs) this.fieldVisibility();
    }
    , loadRecord: function (rec) {
        if (rec) {
            this.loadDta(rec.data, rec.invalid);
        } else {
            this.clearFields();
        }
    }
    , loadDta: function (dta, invalid, exclude, silent) {
        this.clearFields();
        if (dta) {
            this.setValues(dta, invalid, exclude, silent);

        }
    }
    , clearValues: function () {
        //        this.getForm().applyToFields({ originalValue: null });
        //        this.getForm().items.each(function(f) {
        //            f.suspendEvents();
        //            f.reset();
        //            f.resumeEvents();
        //        });
        //        this.fieldVisibility();
    }
    , clearFields: function () {
        this.getForm().applyToFields({ originalValue: null });
        this.getForm().reset();
        this.fieldVisibility();
    }
    , setValues: function (values, invalid, exclude, silent) {
        var bfrm = this.getForm();

        bfrm.loadedFromGrid = true;

        if (Ext.isArray(values)) {
            for (var i = 0, len = values.length; i < len; i++) {
                var v = values[i];
                if ((!Ext.isEmpty(exclude) && v.id != exclude) || Ext.isEmpty(exclude)) {
                    var f = bfrm.findField(v.id);
                    if (f) {
                        if (silent) f.suspendEvents();
                        if (Ext.isFunction(f.getRecord)) {
                            f.setActiveRecord(values[id]);
                        } else {
                            f.setValue(v.value);
                            f.invalid = invalid
                        }
                        if (bfrm.trackResetOnLoad) {
                            f.originalValue = f.getValue();
                        }
                        if (silent) f.resumeEvents();
                    }
                }
            }
        } else {
            var field, id;
            for (id in values) {
                if ((!Ext.isEmpty(exclude) && id != exclude) || Ext.isEmpty(exclude)) {
                    if (!Ext.isFunction(values[id]) && (field = bfrm.findField(id))) {
                        if (silent) field.suspendEvents();
                        if (Ext.isFunction(field.getRecord)) {
                            field.setActiveRecord(values[id]);
                        } else {
                            field.setValue(values[id]);
                            field.invalid = invalid;
                        }
                        if (bfrm.trackResetOnLoad) {
                            field.originalValue = field.getValue();
                        }
                        if (silent) field.resumeEvents();
                    }
                }
            }
        }
        this.fieldVisibility();
        this.markInvalid(bfrm);
        /*
        var fields = eBook.Interface.center.fileMenu.getFieldsByTab(bfrm.xtype.split('-')[3]);
        if (fields) {
        for (var i = 0; i < fields.items.length; i++) {
        var field = fields.items[i].data;
        var fieldName = field.Field;
        var item = bfrm.findField(fieldName);
        if (item) {
        item.markInvalid(field.Text);
        }
        }
        }
        */
        return this;
    }
    , hasDirtyFields: function () {
        var obj = this.getForm().getFieldValues(true);
        for (var p in obj) {
            return true;
        }
        return false;
    }
    , markInvalid: function (form) {
        var typeId = form.ownerCt.ownerCt.tpeId;
        var fields = eBook.Interface.center.fileMenu.getFieldsByTab(typeId, form.xtype.split('-')[3]);
        if (fields) {
            for (var i = 0; i < fields.items.length; i++) {
                var field = fields.items[i].data;
                var fieldName = field.Field;
                var item = form.findField(fieldName);
                if (item) {
                    if (form.loadedFromGrid) {
                        if (item.invalid) {
                            item.markInvalid(field.Text);
                        }
                    } else {
                        item.markInvalid(field.Text);
                    }
                }

            }
        }
    }
});

Ext.reg('worksheet-form', eBook.Worksheets.Form);