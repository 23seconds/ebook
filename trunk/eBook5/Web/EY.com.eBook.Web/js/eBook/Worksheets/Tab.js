eBook.Worksheets.Tab = Ext.extend(Ext.Panel, {
    initComponent: function() {
        for (var j = 0; j < this.items.length; j++) {
            this.items[j].wsClosed = this.wsClosed;
        }
        
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
        });

        eBook.Worksheets.Tab.superclass.initComponent.apply(this, arguments);
    }
    , setMsg: function(row, field) {
        if (row) {
            if (this.myView.highlightRowId) {
                this.myView.highlightRowId(row,field);
            }
        }
    }
    , showEdit: function(record) {
        if (this.myEdit) {
            this.layout.setActiveItem(this.myEdit);
            this.myEdit.loadRecord.defer(200, this.myEdit, [record]);
        }
    }
    , showView: function() {
        this.layout.setActiveItem(this.myView);
    }
    , onFieldChange: function(fld, nv, ov) {
        if (this.myEdit) {
            this.myEdit.onFieldChange.call(this.myEdit, fld, nv, ov);
        } else if (this.myView && this.myView.onFieldChange) {
            this.myView.onFieldChange.call(this.myView, fld, nv, ov);
        }
    }
});

Ext.reg('worksheet-tab', eBook.Worksheets.Tab);