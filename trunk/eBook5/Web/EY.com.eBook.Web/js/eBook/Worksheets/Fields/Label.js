

eBook.Worksheets.Fields.Label = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.isLabel = true;
    config = this.handleConstruction(config);
    config.autoWidth = true;
    if (config.labelStyle) config.style = config.labelStyle;
    config.disabledClass = 'eBook-ws-disabled';
    eBook.Worksheets.Fields.Label.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.Label, Ext.form.Label, eBook.Worksheets.Fields.Field);
Ext.reg('ewLabelField', eBook.Worksheets.Fields.Label);
