

eBook.Worksheets.Fields.PercentField = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.minValue = 0;
    config.maxValue = 100;
    config.allowNegative = false;
    config.decimalPrecision = 4;
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'changed': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config = this.handleConstruction(config);
    eBook.Worksheets.Fields.PercentField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.PercentField, Ext.form.NumberField, eBook.Worksheets.Fields.Field);
Ext.reg('ewpercentfield', eBook.Worksheets.Fields.PercentField);
Ext.reg('ewPercentageField', eBook.Worksheets.Fields.PercentField);
