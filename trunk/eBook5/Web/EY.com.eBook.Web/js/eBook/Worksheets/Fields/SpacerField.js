eBook.Worksheets.Fields.SpacerField = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.text = ' ';
    config = this.handleConstruction(config);
    config.hideLabel = true;
    eBook.Worksheets.Fields.SpacerField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.SpacerField, Ext.form.Label, eBook.Worksheets.Fields.Field);
Ext.reg('ewSpacerField', eBook.Worksheets.Fields.SpacerField);
