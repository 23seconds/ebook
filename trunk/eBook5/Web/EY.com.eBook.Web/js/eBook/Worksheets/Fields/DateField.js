
eBook.Worksheets.Fields.DateField = function(config) {
    config.width = config.width ? config.width : 100;
    config.format = 'd/m/Y';
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'changed': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config = this.handleConstruction(config);
    
    eBook.Worksheets.Fields.DateField.superclass.constructor.call(this, config);
};


Ext.extend(eBook.Worksheets.Fields.DateField, Ext.form.DateField, eBook.Worksheets.Fields.Field);

/*
{
    parseDate: function(value) {
        var v = eBook.Worksheets.Fields.DateField.superclass.parseDate.call(this, value);
        if (v == null) {
            value = Ext.data.Types.WCFDATE.convert(value);
            v = eBook.Worksheets.Fields.DateField.superclass.parseDate.call(this, value);
        }
        return v;
    }
},
*/

Ext.reg('ewdatefield', eBook.Worksheets.Fields.DateField);
