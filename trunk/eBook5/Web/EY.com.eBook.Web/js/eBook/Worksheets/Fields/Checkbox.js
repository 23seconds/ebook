
eBook.Worksheets.Fields.Checkbox = function(config) {
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'check': {
                fn: this.handleOnEventTrigger,
                scope: this
            }
        };
    }

    if (Ext.isDefined(config) && Ext.isDefined(config.attributes)) {
        if (Ext.isDefined(config.attributes.LabelSide)) {
            if (config.attributes.LabelSide == "right") {
                config.boxLabel = config.fieldLabel;
                config.hideLabel = true;
            }
        }
    }
    config = this.handleConstruction(config);

    eBook.Worksheets.Fields.Checkbox.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.Checkbox, Ext.form.Checkbox, eBook.Worksheets.Fields.Field);
Ext.reg('ewcheckbox', eBook.Worksheets.Fields.Checkbox);
