

eBook.Worksheets.Fields.TextField = function(config) {
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'changed': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config = this.handleConstruction(config);
    eBook.Worksheets.Fields.TextField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.TextField, Ext.form.TextField, eBook.Worksheets.Fields.Field);
Ext.reg('ewtextfield', eBook.Worksheets.Fields.TextField);



eBook.Worksheets.Fields.TextAreaField = function(config) {
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'changed': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };

//    }
    config = this.handleConstruction(config);
    config.grow = true;
    config.width = 400;
    config.growMin = 150;
    //config.preventScrollbars = true;
    eBook.Worksheets.Fields.TextAreaField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.TextAreaField, Ext.form.TextArea, eBook.Worksheets.Fields.Field);
Ext.reg('ewTextArea', eBook.Worksheets.Fields.TextAreaField);
