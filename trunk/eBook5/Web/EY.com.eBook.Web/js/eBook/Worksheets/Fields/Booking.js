
eBook.Worksheets.Fields.Booking = function(config) {
    this.isBookingField = true;
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'check': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config = this.handleConstruction(config);
    eBook.Worksheets.Fields.Booking.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.Booking, Ext.form.Checkbox, eBook.Worksheets.Fields.Field);
Ext.reg('ewBooking', eBook.Worksheets.Fields.Booking);