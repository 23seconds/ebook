
eBook.Worksheets.Fields.Dropdown = function(config) {
    if (Ext.isDefined(config) && Ext.isDefined(config.attributes)) {
        if (Ext.isDefined(config.attributes.ListWidth)) {
            config.listWidth = parseInt(config.attributes.ListWidth);
        }
    }
    config.resizable = Ext.isDefined(config.resizable) ? config.resizable : true;
//    if (config.hasInterfaceEvents) {
//        config.listeners = {
//            'select': {
//                fn: this.handleOnEventTrigger,
//                scope: this,
//                delay: 100
//            }
//        };
//    }
    config.nullable=true;
    config = this.handleConstruction(config);
    eBook.Worksheets.Fields.Dropdown.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheets.Fields.Dropdown, Ext.form.ComboBox, eBook.Worksheets.Fields.Field);

Ext.override(eBook.Worksheets.Fields.Dropdown, {
    show: function() {
        eBook.Worksheets.Fields.Dropdown.superclass.show.call(this);
        if(this.rendered) this.getEl().dom.style.width = "";
    }
    , getValue: function() {
        var v = eBook.Worksheets.Fields.Dropdown.superclass.getValue.call(this);
        var r = this.findRecord(this.valueField || this.displayField, v);
        if (r) return r.data;
        if (this.standardValue) return this.standardValue.data;
        return null;
    }
    , setValue: function(dta) {
        if (!dta) {
            this.standardValue = null;
            return;
        }
        if (Ext.isString(dta)) {
            eBook.Worksheets.Fields.Dropdown.superclass.setValue.call(this, dta);
        } else {
            this.standardValue = new this.store.recordType(dta);
            if (this.findIndex(this.valueField || this.displayField, dta.id) == -1) {
                this.store.add(this.standardValue);
            }
            eBook.Worksheets.Fields.Dropdown.superclass.setValue.call(this, dta.id);
        }

    }
    , findRecord: function(prop, val) {
        var idx = this.findIndex(prop, val);
        if (idx == -1) return null;
        return this.store.getAt(idx);
    }
    , findIndex: function(prop, val) {
        return this.store.find(prop, val);
    }
});
Ext.reg('ewdropdown', eBook.Worksheets.Fields.Dropdown);
