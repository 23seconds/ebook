eBook.Util = {
    ConstructObjectFromRecord: function(rec) {
        var o = {};
        rec.fields.each(function(fld) {
            o[fld.mapping] = rec.get(fld.name);
        }, this);
        return o;
    }
};