eBook.PMT.TeamWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout:'fit',
            modal: false,
            iconCls: 'eBook-Team-icon',
            title:eBook.PMT.TeamWindow_Title,
            width:550,
            items:[{
                xtype: 'eBook.PMT.TeamGrid',
                    ref:'grid'
                    }]
            /*tbar: [{
                text: 'Update from PMT',
                iconCls: 'eBook-pmt-refresh-24',
                scale: 'medium',
                iconAlign: 'top',
                ref: 'updatepmt',
                handler: this.onUpdatePMT,
                scope: this
            }]*/
        });
        eBook.PMT.TeamWindow.superclass.initComponent.apply(this, arguments);
    }
    ,onUpdatePMT:function() {
    }
});
