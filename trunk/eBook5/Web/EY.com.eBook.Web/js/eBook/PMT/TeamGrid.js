

eBook.PMT.TeamGrid = Ext.extend(Ext.grid.GridPanel, {
    includePMT: true,
    includeMail: false,
    includeEbook: true,
    initComponent: function() {
        var storeCfg;
        var cols = [];
        cols.push({
            header: '', //eBook.PMT.TeamGrid_Department,
            dataIndex: 'department',
            align: 'left',
            hidden: true,
            groupRenderer: function(v, unused, r, ridx, cidx, ds) {
                if (!v) return "GLOBAL";
                return v;
            }
                    , hideable: false
        });
        cols.push({
            header: '', //eBook.PMT.TeamGrid_Member,
            dataIndex: 'name',
            align: 'left',
            summaryType: 'count',
            summaryRenderer: function(v, params, data) {
                return ((v === 0 || v > 1) ? '(' + v + ' Members)' : '(1 Member)');
            }
                    , hideable: false
                    , sortable: true
        });
        if (this.includeMail) {
            cols.push({
                header: 'Mail', // eBook.PMT.TeamGrid_PMTRole,
                //width: 50,
                dataIndex: 'mail',
                align: 'left',
                // fixed: true
                hideable: false,
                sortable: true,
                renderer: function(value) {
                    return '<a href="mailto:' + value + '">' + value + '</a>';
                }
            });
        }
        if (this.includePMT) {
            cols.push({
                header: 'PMT', // eBook.PMT.TeamGrid_PMTRole,
                width: 50,
                dataIndex: 'role',
                align: 'left',
                // fixed: true
                hideable: false,
                sortable: true
            });
        }
        if (this.includeEbook) {
            cols.push({
                header: 'eBook', //eBook.PMT.TeamGrid_eBookRole,
                width: 50,
                dataIndex: 'level',
                align: 'left',
                renderer: function(v, m, r, ri, ci, s) {
                    if (v > 3) return eBook.PMT.TeamGrid_Editor;
                    if (v > 1) return eBook.PMT.TeamGrid_Reviewer;
                    if (v == 1) return eBook.PMT.TeamGrid_Administrator;
                },
                hideable: false,
                sortable: true
            });
        }
        if (!this.storeCfg) {
            storeCfg = {
                selectAction: 'GetClientTeam',
                fields: eBook.data.RecordTypes.TeamMember,
                serviceUrl: eBook.Service.client,
                idField: 'Id',
                autoDestroy: true,
                autoLoad: true,
                criteriaParameter: 'cicdc',
                baseParams: {
                    Id: eBook.Interface.currentClient.get('Id')
                        , Culture: eBook.Interface.Culture
                }

                , groupField: 'department'
            };
        } else {
            storeCfg = this.storeCfg
        }

        if (Ext.isDefined(storeCfg.groupField)) {
            Ext.apply(this, {
                store: new eBook.data.GroupedJsonStore(storeCfg)
                , plugins: [new Ext.ux.grid.GridSummary(), new Ext.ux.grid.GroupSummary()]
                , view: new Ext.grid.GroupingView({
                    forceFit: true
                    , groupTextTpl: '{group}'
                    , showGroupName: false
                    , enableGrouping: true
                    , enableGroupingMenu: false
                    , enableNoGroups: false
                    , hideGroupedColumn: true
                })
            });
        } else {
            Ext.apply(this, {
                store: new eBook.data.JsonStore(storeCfg)
                , view: new Ext.grid.GridView({ forceFit: true, templates: { cell: new Ext.Template(
                        '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}>',
                        '<div class="x-grid3-cell-inner x-grid3-col-{id} x-selectable" {attr}>{value}</div>',
                        '</td>'
                        )
                        , row: new Ext.Template(
                            '<div class="x-grid3-row {alt} x-selectable" style="{tstyle}"><table class="x-grid3-row-table x-selectable" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
                            '<tbody><tr>{cells}</tr>',
                            '</tbody></table></div>'
                        )
                }
                })
                , disableSelection: true
                , trackMouseOver: false
            });
        }


        Ext.apply(this, {
            // store: new eBook.data.GroupedJsonStore(storeCfg)
            // , plugins: [new Ext.ux.grid.GridSummary(), new Ext.ux.grid.GroupSummary()]
            loadMask: true
            , columnLines: true

            , columns: cols
            , enableHdMenu: true

        });
        eBook.PMT.TeamGrid.superclass.initComponent.apply(this, arguments);
    }
});

    Ext.reg("eBook.PMT.TeamGrid", eBook.PMT.TeamGrid);