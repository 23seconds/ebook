
eBook.Language.Languages = {
    nl_BE: eBook.Language_Dutch
    , fr_FR: eBook.Language_French
    , de_DE: eBook.Language_German
    , en_US: eBook.Language_English
};

eBook.grid.ColumnRenders = {
    Account: function(value, metaData, record, rowIndex, colIndex, store) {
        return record.get('nr') + ' ' + record.get('description');
    }
    , HasValueCheck: function(value, metaData, record, rowIndex, colIndex, store) {
        var wtid = this.editor.worksheetTypeId;
        var fnc = eBook.getMapping(wtid, 'LASTYEAR');
     
        if (fnc(record.json)) {
            metaData.css += 'eBook-mapping-lastyear';
            
        }
        if (store.detailed) {
            if (value != null && value != '') {
                return record.get(this.dataIndex + '_name'); // to be replaced with textual value
            }
        } else {
            if (value != null && value != '') {
                return '<img src="images/icons/16/tick.png" alt="" widht="16" height="16" style="margin:auto;"/>';
            }
        }
        return '';
    }
    , Language: function(value, metaData, record, rowIndew, colIndex, store) {
        return eBook.Language.Languages[value.replace('-', '_')];
    }
};

eBook.grid.numberColumn = Ext.extend(Ext.grid.Column, {
    emptyIfZero: false,
    format: '0,000.00',
    align:'right',
    constructor: function(cfg) {
        eBook.grid.numberColumn.superclass.constructor.call(this, cfg);
        this.renderer = Ext.util.Format.numberRenderer(this.format);
        var emptyIfZero = this.emptyIfZero;
        var format = this.format;
        this.renderer = function(v) {
            if (v == 0 && emptyIfZero) return "";
            return Ext.util.Format.number(v, format);
        };
    }

});

Ext.grid.Column.types.ebooknumbercolumn= eBook.grid.numberColumn;
Ext.reg('ebooknumbercolumn', eBook.grid.numberColumn);

eBook.grid.percentColumn = Ext.extend(Ext.grid.Column, {
    emptyIfZero: false,
    format: '0,000.00%',
    align: 'right',
    constructor: function(cfg) {
        eBook.grid.percentColumn.superclass.constructor.call(this, cfg);
        this.renderer = Ext.util.Format.numberRenderer(this.format);
        var emptyIfZero = this.emptyIfZero;
        var format = this.format;
        this.renderer = function(v) {
            if (v == 0 && emptyIfZero) return "";
            return Ext.util.Format.number(v, format);
        };
    }

});

Ext.grid.Column.types.ebookpercentcolumn = eBook.grid.percentColumn;
Ext.reg('ebookpercentcolumn', eBook.grid.percentColumn);
    