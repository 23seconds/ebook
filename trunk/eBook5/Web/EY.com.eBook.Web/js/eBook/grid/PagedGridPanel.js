eBook.grid.PagedGridPanel = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        this.store = new eBook.data.PagedJsonStore(this.storeConfig);
        Ext.apply(this.pagingConfig, {
            store: this.store
        });
        
        Ext.apply(this, {
            bbar: new Ext.PagingToolbar(this.pagingConfig)
        });
        eBook.grid.PagedGridPanel.superclass.initComponent.apply(this, arguments);
    }
});