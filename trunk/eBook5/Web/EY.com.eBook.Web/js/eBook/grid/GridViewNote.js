
eBook.grid.GridViewNote = Ext.extend(Ext.grid.GridView, {
    enableRowBody: true,
    initData: function(ds, cm) {
        
        eBook.grid.GridViewNote.superclass.initData.apply(this, arguments);
        for (var i = 0; i < this.cm.config.length; i++) {
            if (this.cm.config[i].isNote) break;
        }
        this.noteCm = this.cm.config[i];
        this.cm.config.splice(i, 1);
        this.cm = cm;
    },
    initTemplates: function() {
        
        var ts = this.templates || {};

        if (!ts.note) {
            ts.note = new Ext.Template(
            //'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="{style}" tabIndex="0" {cellAttr}>',
                    '<b>{header}:</b><i>{value}</i>'
            //'</td>'
                    );
        }

        if (!ts.row) {
            ts.row = new Ext.XTemplate(
                '<div class="x-grid3-row {alt}" style="{tstyle}"><table class="x-grid3-row-table" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
                '<tbody><tr>{cells}</tr>',
                '<tr class="x-grid3-row-body-tr eBook-gridviewnotes-body {[values.noteColumn.hidden ? "eBook-hidden" : ""]}" style="{bodyStyle}"><td colspan="{cols}" class="x-grid3-body-cell" tabIndex="0" hidefocus="on">{body}</td></tr>',
                '</tbody></table></div>'
            );
        }

        this.templates = ts;
        eBook.grid.GridViewNote.superclass.initTemplates.call(this);
    },
    doRender: function(columns, records, store, startRow, colCount, stripe) {
        
        var templates = this.templates,
            cellTemplate = templates.cell,
            noteTemplate = templates.note,
            rowTemplate = templates.row,
            last = colCount - 1;

        var tstyle = 'width:' + this.getTotalWidth() + ';';

        // buffers
        var rowBuffer = [],
            colBuffer = [],
            rowParams = { tstyle: tstyle },
            meta = {},
            column,
            noteColumn = {},
            record;

        //build up each row's HTML
        for (var j = 0, len = records.length; j < len; j++) {
            record = records[j];
            colBuffer = [];

            var rowIndex = j + startRow;

            //build up each column's HTML
            for (var i = 0; i < colCount; i++) {
                column = columns[i];

                meta.id = column.id;
                meta.css = i === 0 ? 'x-grid3-cell-first ' : (i == last ? 'x-grid3-cell-last ' : '');
                meta.attr = meta.cellAttr = '';
                meta.style = column.style;
                meta.value = column.renderer.call(column.scope, record.data[column.name], meta, record, rowIndex, i, store);

                if (Ext.isEmpty(meta.value)) {
                    meta.value = '&#160;';
                }

                if (this.markDirty && record.dirty && Ext.isDefined(record.modified[column.name])) {
                    meta.css += ' x-grid3-dirty-cell';
                }

                colBuffer[colBuffer.length] = cellTemplate.apply(meta);

            }

            if (this.noteCm) {
                noteColumn.id = this.noteCm.id;
                noteColumn.css = this.noteCm.css;
                noteColumn.attr = this.noteCm.cellAttr = '';
                noteColumn.style = this.noteCm.style;
                noteColumn.value = this.noteCm.renderer.call(this.noteCm.scope, record.data[this.noteCm.dataIndex], noteColumn, record, rowIndex, -1, store);
                noteColumn.header = this.noteCm.header;
                noteColumn.hidden = false;
                if (Ext.isEmpty(noteColumn.value) || noteColumn.value=="&#160;") {
                    noteColumn.hidden = true;
                }

                if (this.markDirty && record.dirty && Ext.isDefined(record.modified[noteColumn.dataIndex])) {
                    noteColumn.css += ' x-grid3-dirty-cell';
                }
            }
            //set up row striping and row dirtiness CSS classes
            var alt = [];

            if (stripe && ((rowIndex + 1) % 2 === 0)) {
                alt[0] = 'x-grid3-row-alt';
            }

            if (record.dirty) {
                alt[1] = ' x-grid3-dirty-row';
            }

            rowParams.cols = colCount;

            if (this.getRowClass) {
                alt[2] = this.getRowClass(record, rowIndex, rowParams, store);
            }

            rowParams.alt = alt.join(' ');
            rowParams.cells = colBuffer.join('');
            rowParams.noteColumn = noteColumn;
            rowParams.body = noteTemplate.apply(noteColumn);
            rowBuffer[rowBuffer.length] = rowTemplate.apply(rowParams);
        }

        return rowBuffer.join('');
    }
});

eBook.grid['gridviewnotes'] = eBook.grid.GridViewNote;