
eBook.Repository.SelectTree = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        Ext.apply(this, {
            root: {
                nodeType: 'async',
                text: 'Repository',
                draggable: false,
                id: 'root',
                expanded: true
            },
            cls: 'eb-repos-selection-tree',
            rootVisible: false,
            floating: true,
            autoScroll: true,
            minWidth: 200,
            minHeight: 200,
            singleExpand: true,
            enableDrag: true,
            enableDrop: false,
            ddGroup: 'repository',
            width: 250,
            height: 400,
            delay: 2,
            loader: new eBook.Repository.TreeLoader({
                dataUrl: eBook.Service.repository + 'GetChildren',
                requestMethod: "POST",
                showFiles: true,
                selectionMode: true,
                filter: { ps: eBook.Interface.currentFile.get('StartDate')
                            , pe: eBook.Interface.currentFile.get('EndDate')
                            , stat: null
                }
            })
        });
        eBook.Repository.SelectTree.superclass.initComponent.apply(this, arguments);
    }
    , initEvents: function() {
        eBook.Repository.SelectTree.superclass.initEvents.apply(this, arguments);
        /*this.mon(this.getEl(),'mouseout',this.onPanelMouseOut,this);
        this.mon(this.getEl(),'mouseover',this.onPanelMouseOver);*/
        //this.showTask = new Ext.util.DelayedTask(this.show, this);
        this.hideTask = new Ext.util.DelayedTask(function() {
            // this.showTask.cancel();
            this.hide();
        }, this);

        this.el.hover(function() {
            this.hideTask.cancel();
        }, function() {
            this.hideTask.delay(this.delay * 1000);
        }, this);
    }
    , hide: function() {
        eBook.Repository.SelectTree.superclass.hide.call(this);
        if (this.btn) this.btn.toggle(false, true);
    }
    , showMenu: function(btn) {
        this.show();
        this.getEl().alignTo(btn.getEl(), 'tl-bl?');
        this.btn = btn;
        this.startHide();
    }
    , startHide: function(delay) {
        this.hideTask.cancel();
        this.hideTask.delay((delay || this.delay) * 1000);
    }

});