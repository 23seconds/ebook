
eBook.Repository.SingleUploadWindow = Ext.extend(eBook.Window, {
    existingItemId: null,
    changeFileName: false,
    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            title: eBook.Pdf.UploadWindow_Title
            , layout: 'fit'
            , width: 650
            , height: 200
            , modal: true
            //, iconCls: 'eBook-Window-account-ico'
            , items: [new Ext.FormPanel({
                fileUpload: true
                , labelWidth: 150
                , items: [
                    //new Ext.ux.form.FileUploadField({ width: 350, fieldLabel: eBook.Pdf.UploadWindow_ChoosePDF, fileTypeRegEx: /(\.pdf)$/i })
                    {xtype: 'fileuploadfield'
                        , width: 400
                        , fieldLabel: eBook.Pdf.UploadWindow_ChoosePDF
                        , fileTypeRegEx: /(\..*)$/i
                        , ref: 'file'
                    }
                    /* , {
                     xtype: 'hidden'
                     , name: 'fileType'
                     , value: 'pdf'
                     }*/]

                , ref: 'pdfForm'

            })]
            , tbar: [{
                text: eBook.Pdf.UploadWindow_UploadPDF,
                ref: '../saveButton',
                iconCls: 'eBook-icon-24-save',
                scale: 'medium',
                iconAlign: 'top',
                handler: me.onSaveClick,
                scope: me
            }]
        });

        eBook.Repository.SingleUploadWindow.superclass.initComponent.apply(me, arguments);
    }
    , onSaveClick: function (e) {
        var me = this;
        var f = me.pdfForm.getForm();
        if (!f.isValid()) {
            alert(eBook.Pdf.UploadWindow_WrongType);
            return;
        }

        f.submit({
            url: 'Upload.aspx',
            waitMsg: eBook.Pdf.UploadWindow_Uploading,
            success: me.successUpload,
            failure: me.failedUpload,
            scope: me
        });
    }
    , successUpload: function (fp, o) {
        var me = this;
        //process o.result.file
        if (!this.existingItemId) {
            var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: o.result.id
                , standards: this.standards
                , readycallback: this.readycallback
                , clientId: me.clientId
            });
            if (!me.parentCaller) {
                wn.show()
            }
            else
            {
                wn.show(me.parentCaller);
            }
            wn.loadNewFile({ id: o.result.id, newName: o.result.file, oldName: o.result.originalFile, extension: o.result.extension, contentType: o.result.contentType });
            me.close();
        } else {
            me.getEl().mask("Replacing file", 'x-mask-loading');
            Ext.Ajax.request({
                url: eBook.Service.repository + 'ReplaceItem'
                , method: 'POST'
                , params: Ext.encode({
                    crridc: {
                        NewFileId: o.result.id,
                        Extension: o.result.extension,
                        Contenttype: o.result.contentType,
                        ItemId: me.existingItemId,
                        NewFileName: me.changeFileName ? o.result.file : null
                    }
                })
                , callback: me.onReplaceCallback
                , scope: me
            });
        }
    }
    , onReplaceCallback: function (opts, success, resp) {
        var me = this;
        me.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            if (robj.ReplaceItemResult) {
                var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: this.existingItemId, readOnly: false, readOnlyMeta: false, clientId: this.ClientId,readycallback: this.readycallback });

                //if parent defined
                if (!me.parentCaller) {
                    wn.show();
                }
                else
                {
                    wn.show(me.parentCaller);
                }
                wn.loadById();
                this.close();
            } else {
                eBook.Interface.showError("Failed to replace item", "Replace item");
            }
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , failedUpload: function (fp, o) {
        var me = this;
        eBook.Interface.showError(o.message, me.title);
        me.getEl().unmask();
    }
    , show: function (caller) {
        var me = this;
        me.parentCaller = caller;
        eBook.Repository.SingleUploadWindow.superclass.show.call(me);
    }
});