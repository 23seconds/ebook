///Gives the user the option to choose a file for replacement.
///store, repofilepath and caller are supplied by caller
eBook.Repository.ReplaceChoiceWindow = Ext.extend(eBook.Window, {
    repoFilePath: null, //supplied by caller
    store: null, //supplied by caller
    caller: null,//supplied by caller
    repoLocation: null, //supplied by caller

    initComponent: function () {
        var me = this;

        Ext.apply(this, {
            title: 'Folder overview',
            ref: 'replaceChoiceWindow',
            modal: true,
            layout: 'vbox',
            layoutConfig: {
                align: 'stretch'
            },
            bodyStyle: 'background-color:#FFF;',
            width: 800,
            height: 287,
            y: 75, //half of height difference with body viewsize when preview added (see loadPreview)
            minimizable: true,
            items: [
                {
                    xtype: 'container',
                    cls: 'x-toolbar x-box-item x-toolbar',
                    height: 30,
                    ref: 'pnlFolder',
                    html: '<img style="padding-left: 2px; margin-top: 3px; float: left;" src="images/icons/16/tree/folder-open.gif" style="padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; background-size: 14px 14px;" alt="Folder"/><div style ="margin-left: 22px; margin-top: 7px; font: 11px arial, tahoma, helvetica, sans-serif; font-weight: bold;">' + (me.repoFilePath ? me.repoFilePath : '') + '</div>'
                },
                {
                    xtype: 'fileGrid',
                    ref: 'fileGrid',
                    height: 177,
                    store: me.store ? me.store : null
                },
                {
                    xtype: 'RepositoryPreview',
                    title: 'Preview',
                    ref: 'previewPanel',
                    flex: 1,
                    hidden: true
                }
            ],
            bbar: [
                {
                    xtype: 'tbfill'
                },
                {
                    text: eBook.Repository.ReplaceFile,
                    ref: '../btnReplace',
                    iconCls: 'eBook-replace-menu-context-icon',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.replaceFile,
                    tooltip: 'Replaces the current selected file.',
                    width: 100,
                    scope: this
                },
                {
                    text: eBook.Repository.NewFile,
                    ref: '../btnNew',
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.addFile,
                    tooltip: 'Allows you to add a new file.',
                    width: 100,
                    scope: this
                },
                {
                    text: eBook.Cancel,
                    ref: '../btnCancel',
                    iconCls: 'eBook-window-cancel-ico eBook-background-image-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    buttonAlign: 'right',
                    handler: me.close,
                    width: 100,
                    scope: this
                }]
        });

        eBook.Repository.ReplaceChoiceWindow.superclass.initComponent.apply(me, arguments);
    },
    ///add a new file, repoLocation is optional
    addFile: function () {
        var me = this,
            standards = null,
            wn = null;

        if (me.repoLocation) {
            standards = {};
            standards.location = me.repoLocation;
        }

        wn = new eBook.Repository.SingleUploadWindow({
            clientId: eBook.CurrentClient,
            standards: standards,
            readycallback: {fn: me.reload, scope: me}
        });

        wn.show(me);
        me.close();
    },
    ///replace the given file
    replaceFile: function () {
        var me = this,
            wn = null,
            record = me.fileGrid ? me.fileGrid.getActiveRecord() : null,
            id = record ? record.get("Id") : null;

        if (id) {
            wn = new eBook.Repository.SingleUploadWindow({
                existingItemId: id,
                readycallback: {fn: me.reload, scope: me}
            });

            wn.show(me);
        }
        me.close();
    },
    enableRowSpecificButtons: function(enable) {
        var me = this;

        if(me.btnReplace)
        {
            if(enable) {
                me.btnReplace.enable();
            } else {
                me.btnReplace.disable();
            }
        }
    },
    loadPreview: function(clientId,physicalFileId){
        var me = this;
        if(clientId && physicalFileId) {
            me.previewPanel.loadFile("Repository/" + eBook.getGuidPath(clientId) + physicalFileId + ".pdf");
            me.previewPanel.show();
            me.doLayout();
            me.setHeight(Ext.getBody().getViewSize().height - 150);
            me.center();
        }
    },
    unloadPreview: function(){
        var me = this;
        me.previewPanel.unloadFile();
        me.previewPanel.hide();
        me.doLayout();
        me.setHeight(287);
        me.setPosition(me.getPosition()[0],75);
    }
});

Ext.reg('replaceChoiceWindow', eBook.Repository.ReplaceChoiceWindow);