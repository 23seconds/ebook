
// ONLY TO BE USED IN CUSTOM ENTERNET-BROWSER !!


eBook.Repository.DirectoryTreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    createNode: function(attr) {
        // apply baseAttrs, nice idea Corey!
        attr.leaf=false;
        attr.text = attr.name;
        attr.iconCls = 'filestruct-' + attr.type;
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                            new Ext.tree.TreeNode(attr) :
                            new Ext.tree.AsyncTreeNode(attr);
        }
    }
    ,load : function(node, callback, scope){
        if(this.clearOnLoad){
            while(node.firstChild){
                node.removeChild(node.firstChild);
            }
        }
        if(this.doPreload(node)){ // preloaded json children
            this.runCallback(callback, scope || node, [node]);
        }else {
            this.requestData(node, callback, scope || node);
        }
    }
    ,requestData : function(node, callback, scope){
        if(this.fireEvent("beforeload", this, node, callback) !== false){
            var json = EY.GetDirectoryStructure(node.attributes.path);
            try {
                var o = json;
                node.beginUpdate();
                for (var i = 0, len = o.length; i < len; i++) {
                    var n = this.createNode(o[i]);
                    if (n) {
                        node.appendChild(n);
                    }
                }
                node.endUpdate();
                this.runCallback(callback, scope || node, [node]);
                this.fireEvent("load", this, node, null);
            } catch (e) {
                this.fireEvent("loadexception", this, node, null);
            }
        }else{
            // if the load is cancelled, make sure we notify
            // the node that we are done
            this.runCallback(callback, scope || node, []);
        }
    }
});

eBook.Repository.DirectoryTree = Ext.extend(Ext.tree.TreePanel, {
    initComponent: function() {
        Ext.apply(this, {
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDD: false,
            containerScroll: true,
            border: false,
            // auto create TreeLoader
            loader: new eBook.Repository.DirectoryTreeLoader({}),
            selModel: new Ext.tree.DefaultSelectionModel({
                listeners: {
                    'selectionchange': {
                        fn: this.onSelectionChange
	                    , scope: this
                    }
                }
            }),
            rootVisible: false,
            root: new Ext.tree.AsyncTreeNode()
            
        });
        eBook.Repository.DirectoryTree.superclass.initComponent.apply(this, arguments);

    }
    , onSelectionChange: function(sel, node) {
        this.refOwner.contents.load(node.attributes.path);
    }
});
Ext.reg('directorytree', eBook.Repository.DirectoryTree);

eBook.Repository.DirectoyContents = Ext.extend(Ext.DataView, {
    initComponent: function() {
        this.listTpl = new Ext.XTemplate('<tpl for=".">',
                    '<div class="eb-dc-file-wrap" id="{path}">',
                        '<tpl if="type==\'File\'">',
		                    '<div class="eb-dc-file-ico eb-dc-file-ico-{extension}">{name}</div>',
		                    '<div class="eb-dc-file-created">{created:date("d/m/Y")}</div>',
		                    '<div class="eb-dc-file-lastupdated">{lastupdated:date("d/m/Y")}</div>',
		                '</tpl>',
		                '<tpl if="type==\'Directory\'">',
		                    '<div class="eb-dc-file-ico eb-dc-file-ico-directory">{name}</div>',
		                '</tpl>',
		            '</div>',
                    '<div class="x-clear"></div>',
                '</tpl>',
                { compiled: true }
                );


        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                url: 'get-images.php',
                root: 'contents',
                fields: ['name', 'path', 'extension', 'type', { name: 'created', type: 'date', dateFormat: 'timestamp' }, { name: 'lastupdated', type: 'date', dateFormat: 'timestamp'}]
            }),
            tpl: this.listTpl,
            bodyStyle:'background-color:#fff;',
            //autoHeight: true,
            autoSCroll:true,
            multiSelect: true,
            overClass: 'x-view-over',
            itemSelector: 'div.eb-dc-file-wrap',
            emptyText: 'No items'
        });
        eBook.Repository.DirectoyContents.superclass.initComponent.apply(this, arguments);
    }
    , load: function(path) {
        this.getEl().mask();
        var o = { contents: EY.GetDirectoryContents(path) };
        this.store.loadData(o);
        this.getEl().unmask();
    }
});
Ext.reg('directorycontents', eBook.Repository.DirectoyContents);



eBook.Repository.LocalFileBrowser = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border',
            
            items: [{ xtype: 'directorytree', ref: 'dirtree', region: 'west',split:true,width:150 },
                    { xtype: 'directorycontents', ref: 'contents', region: 'center'}]
        });
        eBook.Repository.LocalFileBrowser.superclass.initComponent.apply(this, arguments);
    }
});
