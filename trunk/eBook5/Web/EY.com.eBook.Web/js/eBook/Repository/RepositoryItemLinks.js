



eBook.Repository.ItemLinksFileNameColumn = Ext.extend(Ext.list.Column, {
    
    constructor : function(c) {
        c.tpl = new Ext.XTemplate('{[this.getFileName(values.' + c.dataIndex + ')]}'
                    , {getFileName:function(id) {
                        if (id == eBook.EmptyGuid) return eBook.Repository.ItemLinks_DefaultFile;
                        return eBook.Interface.center.clientMenu.files.getFileName(id);
                    }});      
        eBook.Repository.ItemLinksFileNameColumn.superclass.constructor.call(this, c);
    }
});
Ext.reg('lvreposfilenamecolumn', eBook.Repository.ItemLinksFileNameColumn);

eBook.Repository.ListLinkTypeColumn = Ext.extend(Ext.list.Column, {
    constructor: function(c) {
        c.tpl = c.tpl || new Ext.XTemplate('<div class="eb-repos-link-type eb-repos-link-type-{' + c.dataIndex + '}"></div>');

        eBook.Repository.ListLinkTypeColumn.superclass.constructor.call(this, c);
    }
});

Ext.reg('lvlinktype', eBook.Repository.ListLinkTypeColumn);


eBook.Repository.ItemLinksDataView = Ext.extend(Ext.list.ListView, {
    initComponent: function() {
        Ext.apply(this, {
            emptyText: 'No links to display'
            , loadingText: 'Please Wait...'
            , multiSelect: false
            , singleSelect:true
            , store: new eBook.data.JsonStore({
                selectAction: 'GetItemLinks'
                            , serviceUrl: eBook.Service.repository
                            , autoDestroy: true
                            , autoLoad: true
                            , criteriaParameter: 'cildc'
                            , fields: eBook.data.RecordTypes.RepositoryItemLinks
                            , baseParams: {
                                'ItemId': this.repositoryItemId
                                , 'FileId': this.fileId
                                , 'Culture': eBook.Interface.Culture

                            }
            })
            , columns: [{
                        xtype:'lvlinktype',
                        header: 'Type',
                        width: .05,
                        dataIndex: 'ConnectionType'
                    }, {
                        header: 'Dossier',
                        xtype: 'lvreposfilenamecolumn',
                        width: .2,
                        dataIndex: 'FileId'
                    }, {
                        header: 'Link',
                        dataIndex: 'Description'
                }]
            
            });
            eBook.Repository.ItemLinksDataView.superclass.initComponent.apply(this, arguments);
        }
    });

Ext.reg('RepositoryItemLinksDataView', eBook.Repository.ItemLinksDataView);

eBook.Repository.ItemLinksPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        this.fileId = (eBook.Interface && eBook.Interface.center && eBook.Interface.center.clientMenu && eBook.Interface.center.clientMenu.files) ? eBook.Interface.center.clientMenu.files.getLatestId() : null;
        if (this.fileId == null) {
            Ext.apply(this, {
                html: 'Client does not contain any file. <br/>At least 1 file is needed in order to be able to select an account. <br/>Default linking is disabled.'
            });

        } else {
            Ext.apply(this, {
                items: [{ xtype: 'RepositoryItemLinksDataView', fileId: this.fileId, repositoryItemId: this.repositoryItemId, ref: 'myView', listeners: { selectionchange: { fn: this.onSelect, scope: this}}}]
            , layout: 'fit'
            , tbar: [
                {
                    xtype: 'combo'
                    , store: new eBook.data.JsonStore({
                        selectAction: 'GetQueriedAccountsList'
                        , baseParams: {
                            FileId: this.fileId
                                , Culture: eBook.Interface.Culture
                        }
                        , serviceUrl: eBook.Service.schema
                        , criteriaParameter: 'cadc'
                        , autoDestroy: true
                        , fields: eBook.data.RecordTypes.GlobalListItem
                    })
                , displayField: eBook.Interface.Culture.substr(0, 2)
                , valueField: 'id'
                , minChars: 1
                , hideTrigger: true
                , typeAhead: true
                , lazyRender: true
                , forceSelect: true
                , listWidth: 300
                , queryParam: 'Query'
                , selectOnFocus: true
                , ref: 'accountSel'
                }
                , {
                    ref: 'addLnk',
                    text: 'Add account link',
                    iconCls: 'eBook-icon-add-16',
                    scale: 'small',
                    iconAlign: 'left',
                    disabled: false,
                    handler: this.onAddLinkClick,
                    scope: this
                }, {
                    ref: 'delLnk',
                    text: 'Delete link',
                    iconCls: 'eBook-icon-delete-16',
                    scale: 'small',
                    iconAlign: 'left',
                    disabled: true,
                    handler: this.onDeleteLinkClick,
                    scope: this
}]
                });
            }

            eBook.Repository.ItemLinksPanel.superclass.initComponent.apply(this, arguments);
        }
    , onSelect: function(ls, sel) {
        var tb = this.getTopToolbar();
        tb.delLnk.disable();
        this.selected = null
        if (sel && sel.length > 0) {
            tb.delLnk.enable();
            this.selected = ls.getRecord(sel[0]);
        }
    }
    , onDeleteLinkClick: function() {
        if (this.selected) {
            Ext.Msg.show({
                title: 'Delete link',
                msg: 'Are you sure you want to delete this link?',
                buttons: Ext.Msg.YESNO,
                fn: this.onAcceptDelete,
                icon: Ext.MessageBox.QUESTION,
                scope: this
            });
        }
    }
    , onAcceptDelete: function(btnid, txt, opts) {
        this.getEl().unmask();
        if (btnid == "yes") {
            this.getEl().mask("Deleting file " + this.selected.get('Description'), 'x-mask-loading');
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'DeleteLink',
                callback: this.deletePerformed,
                scope: this,
                params: Ext.encode({ rlidc: this.selected.json })
            });
            this.selected = null;
            var tb = this.getTopToolbar();
            tb.delLnk.disable();
        }
    }
    , deletePerformed: function() {
        this.myView.store.load();
        this.getEl().unmask();
        if (eBook.Interface.currentFile) {
            eBook.Interface.loadRepoLinks();
        }
    }
    , onAddLinkClick: function() {
        var tb = this.getTopToolbar();
        if (!Ext.isEmpty(tb.accountSel.getValue())) {
            Ext.Ajax.request({
                url: eBook.Service.repository + 'AddLink'
                    , method: 'POST'
                    , params: Ext.encode({ rldc: {
                        ClientId: eBook.Interface.currentClient.get('Id')
                            , FileId: eBook.EmptyGuid
                            , ItemId: this.repositoryItemId
                            , ConnectionType: 'ACCOUNT'
                            , ConnectionGuid: eBook.EmptyGuid
                            , ConnectionAccount: tb.accountSel.getValue()
                             , ConnectionDetailedPath: ''
                    }
                    })
                    , callback: this.onAddLinkResponse
                    , scope: this
            });
        }
    }
    , onAddLinkResponse: function(opts, success, resp) {
        if (success) {
            this.myView.store.load();
            var tb = this.getTopToolbar();
            tb.accountSel.reset();
        }
    }
    , allowDefaultLinking: function(allow) {
        var tb = this.getTopToolbar();
        if (tb) {
            if (allow) {
                tb.show();
            }
            else {
                tb.hide();
            }
        }
    }
    });


Ext.reg('RepositoryItemLinks', eBook.Repository.ItemLinksPanel);


eBook.Repository.ViewPdfPanel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            html: ' LOADING... '

        });
        eBook.Repository.ViewPdfPanel.superclass.initComponent.apply(this, arguments);
    }
    , afterRender: function() {
        var re = /(?:\.([^.]+))?$/;
        eBook.Repository.ViewPdfPanel.superclass.afterRender.call(this)
        if (this.fileUrl) {
            var exten = re.exec(this.fileUrl)[1];
            if (exten == "pdf") {
                this.pdfEl = new PDFObject({ url: this.fileUrl }).embed(this.body.id);
            } else {
                this.body.update("No preview, <a href='" + this.fileUrl + "'>Click here to download the file</a>");
            }
        }
        var test = 3.3;
    },
    loadFile: function(url) {
        var me = this;
        var re = /(?:\.([^.]+))?$/;
        this.fileUrl = url;
        if (me.rendered) {
            me.unloadFile();
            var exten = re.exec(this.fileUrl)[1];
            if (exten == "pdf") {
                me.pdfEl = new PDFObject({ url: me.fileUrl + "?dc=" + eBook.NewGuid() }).embed(this.body.id);
            } else {
                me.body.update("No preview, <a href='" + me.fileUrl + "'>Click here to download the file</a>");
            }
        }

    },
    unloadFile: function() {
        var me = this;
        if (me.pdfEl) Ext.get(me.pdfEl).remove();
    }
});

Ext.reg('RepositoryPreview', eBook.Repository.ViewPdfPanel);