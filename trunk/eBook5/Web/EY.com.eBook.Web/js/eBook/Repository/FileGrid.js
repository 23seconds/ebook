///The FileGrid is simple grid panel showing a flat list (name, status and path)
///Store is supplied by caller
eBook.Repository.FileGrid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            title: "Files",
            loadMask: true,
            cls: "ebook-fileGrid",
            activeRecord: null,
            autoExpandColumn:2,
            store: me.store ? me.store : null,
            viewConfig: {
                scrollOffset: 0
            },
            listeners: {
                beforerender: function (gridPanel) {
                    var store = gridPanel.getStore();
                    var result = store.queryBy(function(record){return record.get("Status") === null || record.get("Status") === "";}); //Check if status is empty. Query function couldn't differentiate empty strings therefore I used queryBy with custom function
                    if(store.getCount() == result.getCount()) //no status defined, hide column
                    {
                        gridPanel.getColumnModel().setHidden(1, true);
                    }
                }
            },
            columns: [{
                header: "FileName",
                dataIndex: 'FileName',
                renderer: function (value, metaData, record) { //add pdf icon and css
                    return '<div style="font-weight: bold; padding-left: 19px; background-position: 1px 0px; background-repeat: no-repeat; background-size: 14px 14px;" class="' + record.get("IconCls") + '" >' + value + '</div>';
                },
                width: 300
            }, {
                header: "Status",
                dataIndex: 'StatusText',
                width: 300
            },
            {
                header: "Extension",
                dataIndex: 'Extension',
                renderer: function (value, metaData, record) {
                    return value.replace(".","");
                },
                width: 50
            }],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    'rowselect': {
                        fn: function (grid, rowIndex) {
                            var me = this,
                                record = me.getStore().getAt(rowIndex);

                            me.setActiveRecord(record); //Set the active record (needed because selection of row is lost)
                            me.refOwner.enableRowSpecificButtons(true);
                            me.refOwner.loadPreview(record.get("ClientId"),record.get("PhysicalFileId"));
                        },
                        scope: me
                    },
                    'rowdeselect': {
                        fn: function () {
                            me.setActiveRecord(null);
                            me.refOwner.enableRowSpecificButtons(false);
                            me.refOwner.unloadPreview();
                        },
                        scope: this
                    }
                }
            })
        });
        eBook.Bundle.FormOverview.superclass.initComponent.apply(me, arguments);
    },
    clearStore: function ()
    {
        var me = this;

        me.store.removeAll();
    },
    setActiveRecord: function(value)
    {
        var me = this;

        me.activeRecord = value;
    },
    getActiveRecord: function()
    {
        var me = this;

        return me.activeRecord;
    }
});
Ext.reg('fileGrid', eBook.Repository.FileGrid);
