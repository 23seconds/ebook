
eBook.Repository.FileWindow = Ext.extend(eBook.Window, {
    initComponent: function() {
        var repository = new eBook.Repository.Tree();
        //repository.treeFilter = new eBook.Repository.TreeFilter(repository);
        Ext.apply(this, {
            title: 'Repository',
            items: [repository],
            layout: 'fit'
        });
        eBook.Repository.FileWindow.superclass.initComponent.apply(this, arguments);
    }
});
