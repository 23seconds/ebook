


eBook.Repository.ContextLinkListDataView = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: 'GetFileLinks'
                , serviceUrl: eBook.Service.repository
                //, autoDestroy: true
                //, autoLoad: true
                , criteriaParameter: 'cicdc'
                , fields: eBook.data.RecordTypes.RepositoryItemLinks
                , baseParams: {
                    'Id': eBook.Interface.currentFile.get('Id')
                    , 'Culture': eBook.Interface.Culture
                }
                , listeners: { load: { fn: this.onSetFileLinks, scope: this} }
            })

            , tpl: new Ext.XTemplate(
		        '<tpl for=".">',
                    '<div class="ebook-repo-file-link eb-rep-node-pdf">',
		                '<div class="ebook-repo-file-link-txt"><a href="javascript:eBook.Interface.showRepoItem(\'{ItemId}\');">{Description}</a></div>',
		            '<div class="ebook-repo-file-link-delete"></div>',
		            '</div>',
                '</tpl>',
                '<div class="x-clear"></div>'
	        )

	        , itemSelector: 'div.ebook-repo-file-link'
	        , overClass: 'ebook-repo-file-link-over'
            , cls: 'ebook-repo-file-links'
            , data: []
            , border: false
            , prepareData: function(dta) {
                return dta;
            }
            , listeners: { click: { fn: this.onMyItemClick, scope: this} }

        });
        eBook.Repository.ContextLinkListDataView.superclass.initComponent.apply(this, arguments);
    }
    , onMyItemClick: function(dv, idx, n, e) {
        if (e) {
            var tg = e.getTarget('.ebook-repo-file-link-delete');
            if (tg) {
                var rec = this.store.getAt(idx);
                Ext.Msg.show({
                    title: 'Delete link',
                    msg: 'Are you sure you want to delete this link to \'' + rec.get('Description') + '\'?',
                    buttons: Ext.Msg.YESNO,
                    rec: rec,
                    fn: this.onAcceptDelete,
                    icon: Ext.MessageBox.QUESTION,
                    scope: this
                });
            }
        }
    }
    , onAcceptDelete: function(btnid, txt, opts) {
        if (btnid == "yes") {
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'DeleteLink',
                callback: this.deletePerformed,
                scope: this,
                params: Ext.encode({ rlidc: opts.rec.json })
            });
        }
    }
    , deletePerformed: function() {
        this.store.load();
        if (this.refOwner.caller && this.refOwner.caller.doRefresh) {
            this.refOwner.caller.doRefresh();
        }
    }
    , onSetFileLinks: function() {
        var st = this.store;
        var accs = st.collect('ConnectionAccount', false);
        var guids = st.collect('ConnectionGuid', false);
        var its = guids.concat(accs);
        var lnks = {};
        for (var i = 0; i < its.length; i++) {
            lnks['L' + its[i]] = true;
        }
        eBook.Interface.fileRepository = lnks;

        eBook.Repository.ContextLinkListDataView.superclass.onDataChanged.apply(this, arguments);
    }

});

eBook.Repository.ContextLinkList = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'fit'
            , id: 'eBook-file-links'
            , maxWidth: 200
            , maxHeight: 120
            , delay: 1
            , floating: true
            , autoScroll: true
            , renderTo: Ext.getBody()
            , items: [new eBook.Repository.ContextLinkListDataView({ ref: 'dv' })]
        });
        eBook.Repository.ContextLinkList.superclass.initComponent.apply(this, arguments);
    }
    , hide: function() {
        eBook.Repository.ContextLinkList.superclass.hide.call(this);
    }
    , startHide: function(delay) {
        this.hideTask.cancel();
        this.hideTask.delay((delay || this.delay) * 1000);
    }
    , showAt: function(el, connectiontype, connectionId, caller) {
        this.caller = caller;
        this.dv.store.clearFilter();
        this.dv.store.filterBy(function(rec, id) {
            return rec.get('ConnectionType') == connectiontype
                    && (rec.get('ConnectionGuid') == connectionId
                        || rec.get('ConnectionAccount') == connectionId);
        });
        this.show();
        this.setSize(this.maxWidth, this.dv.getHeight() + 10);
        this.getEl().alignTo(Ext.get(el), 'tl-bl?');
        this.startHide();

    }
    , initEvents: function() {
        eBook.Repository.ContextLinkList.superclass.initEvents.apply(this, arguments);
        /*this.mon(this.getEl(),'mouseout',this.onPanelMouseOut,this);
        this.mon(this.getEl(),'mouseover',this.onPanelMouseOver);*/
        //this.showTask = new Ext.util.DelayedTask(this.show, this);
        this.hideTask = new Ext.util.DelayedTask(function() {
            // this.showTask.cancel();
            this.hide();
        }, this);

        this.el.hover(function() {
            this.hideTask.cancel();
        }, function() {
            this.hideTask.delay(this.delay * 1000);
        }, this);
        this.makeFloating(true);
    }
})