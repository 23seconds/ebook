
function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months;
}

eBook.Fields.ReposHide = function() {

    var pel = this.getEl().parent('.eb-repos-filemeta-field');
    pel.setVisibilityMode('display');
    pel.hide();
};

eBook.Fields.ReposShow = function() {

    var pel = this.getEl().parent('.eb-repos-filemeta-field');
    pel.setVisibilityMode('display');
    pel.show();
};

eBook.Repository.RepoCommentsView = Ext.extend(Ext.Component, {

    initComponent: function () {

        Ext.apply(this, {
           /* store: new Ext.data.JsonStore({
                fields: [{ name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
                , { name: 'PersonId', mapping: 'pid', type: Ext.data.Types.STRING }
                 , { name: 'PersonName', mapping: 'pn', type: Ext.data.Types.STRING }
                , { name: 'Comment', mapping: 'cmt', type: Ext.data.Types.STRING }
                , { name: 'PostDate', mapping: 'pd', type: Ext.data.Types.WCFDATE }
               ],
                data: [{ Id: 'test-1', PersonId: 'Pers-1', PersonName: 'Test Person', Comment: 'rgjrogjzrogjzrogjreogjg g rg gzjerg', PostDate: "/Date(1418379755867+0100)/"}]
            }),*/
            autoHeight: true,
            autoScroll: true,
            autoEl:{
                tag:'div',
                cls:'eBook-repo-comments'

            }
            , mytpl: new Ext.XTemplate(
                '<tpl for=".">',
                '<div class="eBook-repo-comment">',
                    '<div class="eBook-repo-comment-txt">{Comment}</div>',
                    '<div class="eBook-repo-comment-by"> <tpl for="status">{en}<br/></tpl>{PersonName} {PostDateString}</div>',
                '</div>',
                '</tpl>')

        });
        eBook.Repository.RepoCommentsView.superclass.initComponent.apply(this, arguments);
    }

    , setData: function (dta) {
        var me = this;


        me.getEl().update(me.mytpl.applyTemplate(dta));
    }
});

Ext.reg('repoComments-view', eBook.Repository.RepoCommentsView);

eBook.Repository.RepoCommentsAdd = Ext.extend(Ext.Container, {

    initComponent: function () {
        var me = this;
        Ext.apply(this, {
            /* store: new Ext.data.JsonStore({
            fields: [{ name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'PersonId', mapping: 'pid', type: Ext.data.Types.STRING }
            , { name: 'PersonName', mapping: 'pn', type: Ext.data.Types.STRING }
            , { name: 'Comment', mapping: 'cmt', type: Ext.data.Types.STRING }
            , { name: 'PostDate', mapping: 'pd', type: Ext.data.Types.WCFDATE }
            ],
            data: [{ Id: 'test-1', PersonId: 'Pers-1', PersonName: 'Test Person', Comment: 'rgjrogjzrogjzrogjreogjg g rg gzjerg', PostDate: "/Date(1418379755867+0100)/"}]
            }),*/
            // html: 'I can add here'
            items: [{ xtype: 'textarea', ref: 'textar', width: '100%' }, { xtype: 'button', text: 'Add', handler: me.onAddClick, scope: me}]

        });
        eBook.Repository.RepoCommentsAdd.superclass.initComponent.apply(this, arguments);
    }
    , onAddClick: function (e, evt) {
        var me = this,
            txt = me.textar.getValue(),
            owner = me.refOwner;
        if (Ext.isEmpty(txt)) return;
        me.textar.setValue('');
        owner.addComment(txt);

    }
    , setData: function (dta) {

    }
});

Ext.reg('repoComments-add', eBook.Repository.RepoCommentsAdd);


eBook.Repository.RepoComments = Ext.extend(Ext.Container, {

    initComponent: function () {

        Ext.apply(this, {
            cls: 'eBook-repo-comments-cont',
            items: [{ xtype: 'repoComments-add', height: 100, ref: 'commentAdder', width: '100%' }
                , { xtype: 'repoComments-view', ref: 'commentViewer', width: '100%' }
            ]
        });
        eBook.Repository.RepoComments.superclass.initComponent.apply(this, arguments);

    }
    , myData: []
    , setValue: function (dta) {
        var me = this;
        if (!Ext.isArray(dta)) {
            dta = [dta];
        }
        me.myData = dta;
        me.commentViewer.setData(dta);
        // me.getEl().update(me.mytpl.applyTemplate(dta));
    }
    , getValue: function () {
        return this.myData;
    }
    , addComment: function (txtComment) {
        var me = this,
             obj = { PersonId: eBook.User.personId, PersonName: eBook.User.fullName, Comment: txtComment, PostDate: new Date(), PostDateString: Ext.util.Format.date(new Date(), 'd/m/Y H:i:s') };
        obj.status = me.myOwner.refOwner.fileDetails.statuslist.getRecord().data;
        me.myData.unshift(obj);
        me.setValue(me.myData);

        //          data: [{ Id: 'test-1', PersonId: 'Pers-1', PersonName: 'Test Person', Comment: 'rgjrogjzrogjzrogjreogjg g rg gzjerg', PostDate: "/Date(1418379755867+0100)/"}]

    },
    isValid: function () {
        return true;
    }
});

Ext.reg('repoComments', eBook.Repository.RepoComments);



eBook.Fields.ReposRadioGroup = Ext.extend(Ext.form.RadioGroup, {
    initComponent: function() {
        var itsVals = [];
        var its = [];
        for (var i = 0; i < this.fldAttributes.length; i++) {
            if (this.fldAttributes[i].k == eBook.Interface.Culture) {
                var itsVals = eval(this.fldAttributes[i].v);
                break;
            }
        }
        for (var i = 0; i < itsVals.length; i++) {
            var arr = itsVals[i];
            its.push({ name: this.name + '_rd', width: 200, boxLabel: arr[0], inputValue: arr[1], checked: i == 0
                , listeners: { check: { fn: this.onRadioCheck, scope: this} }
            });
        }
        Ext.apply(this, {
            items: its
            , columns: 1
            , width: 250
        });
        eBook.Fields.ReposRadioGroup.superclass.initComponent.apply(this, arguments);
    }
    , onRadioCheck: function(fld, chk) {
        if (chk) this.fireEvent('change', this, this);
    }
    , getValue: function() {
        var rd = eBook.Fields.ReposRadioGroup.superclass.getValue.call(this);
        if (rd == null) return null;
        return rd.inputValue;
    }
});
Ext.reg('rpradiogroup', eBook.Fields.ReposRadioGroup);

eBook.Fields.LocalInputList = Ext.extend(Ext.form.ComboBox, {
    initComponent: function() {
        var lst = this.attributes[0].v;
        for (var i = 0; i < this.attributes.length; i++) {
            if (this.attributes[i].k == eBook.Interface.Culture) {
                lst = this.attributes[i].v;
            }
        }
        lst = Ext.util.JSON.decode(lst);

        Ext.apply(this, {
            store: lst
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , minChars: 1
            , autLoad: true
            , hideTrigger: true
            , typeAhead: false
            , lazyRender: false
            , forceSelect: false
            , listWidth: 300
            , triggerAction: 'all'
            , selectOnFocus: false
            , disableKeyFilter :true
            , listeners: {
                focus: { fn: function(gr) {
                    if (!gr.list) gr.initList();

                    gr.expand();
                    gr.doQuery('',true);// .clearFilter();
                }, scope: this
                }
            }
        });
        eBook.Fields.LocalInputList.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('localinputlist', eBook.Fields.LocalInputList);


eBook.Repository.FileDetailsWindow = Ext.extend(eBook.Window, {

    readOnly: false,
    readOnlyMeta: false,
    initComponent: function () {
        Ext.apply(this, { width: 607, modal: true, layout: 'fit', items: [new eBook.Repository.FileDetailsPanel({ ref: 'detailPanel', repositoryItemId: this.repositoryItemId, readOnly: this.readOnly, readOnlyMeta: this.readOnlyMeta })]
        });
        if (!this.readOnly || !this.readOnlyMeta) {
            Ext.apply(this, {
                bbar: ['->', {
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onSaveClick,
                    scope: this
                }
            ]
            , tbar: [{
                ref: '../copyItem',
                iconCls: 'eBook-window-copy-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onCopyClick,
                scope: this
            }, {
                ref: '../moveItem',
                iconCls: 'eBook-window-move-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onMoveClick,
                scope: this
            }, {
                ref: '../replaceItem',
                iconCls: 'eBook-window-move-ico',
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onReplaceClick,
                scope: this
            }]
            });
        }

        eBook.Repository.FileDetailsWindow.superclass.initComponent.apply(this, arguments);
    }
    , onCopyClick: function () {
        eBook.Interface.center.clientMenu.repository.onCopyItem();
        this.close();
    }
     , onMoveClick: function () {
         eBook.Interface.center.clientMenu.repository.onMoveItem();
         this.close();
     }
     , onReplaceClick: function () {
         var wn = new eBook.Repository.SingleUploadWindow({ existingItemId: this.repositoryItemId });
         wn.show();
         this.close();
     }
    , loadById: function () {
        this.getEl().mask("Loading", 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.repository + 'GetFileNode'
            , method: 'POST'
            , params: Ext.encode({
                cicdc: {
                    Culture: eBook.Interface.currentFile ? eBook.Interface.currentFile.get('Culture') : eBook.Interface.Culture
                     , Id: this.repositoryItemId
                }
            })
            , callback: this.onLoadedById
            , scope: this
        });
    }
    , onLoadedById: function (opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            this.loadExistingFile({ attributes: robj.GetFileNodeResult });
            this.detailPanel.tbpanel.setActiveTab(2);
        } else {
            this.getEl().unmask();
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , loadExistingFile: function (fobj) {
        if (this.tpe == 'COPY') {
            this.isNew = true;

        } else {
            this.isNew = false;
        }
        if (this.tpe == 'COPY' || this.tpe == 'MOVE') this.getTopToolbar().hide();
        this.detailPanel.loadExistingFile(fobj, this.tpe);
    }
    , loadNewFile: function (fobj) {
        this.getTopToolbar().hide();
        this.isNew = true;
        this.detailPanel.loadNewFile(fobj, this.standards);
    }
    , onSaveClick: function () {
        var sid = this.detailPanel.fileDetails.locationTree.getValue();
        if (Ext.isEmpty(sid)) {
            this.detailPanel.fileDetails.locationTree.markInvalid();
            return;
        }
        this.getEl().mask("Saving", 'x-mask-loading');
        if (this.detailPanel.metaPanel.validateData()) {
            var mtaData = this.detailPanel.metaPanel.getMetaData();
            var o = { IsNew: this.isNew
                , Item: {
                    ClientId: eBook.Interface.currentClient == null ? this.clientId : eBook.Interface.currentClient.get('Id')
                    , StructureId: this.detailPanel.fileDetails.locationTree.getValue()
                    , Id: this.detailPanel.fileDetails.fileId
                    , PhysicalFileId: this.detailPanel.fileDetails.physFileId
                    , FileName: this.detailPanel.fileDetails.fileName.getValue()
                    , Extension: this.detailPanel.fileDetails.extension
                    , ContentType: this.detailPanel.fileDetails.contentType
                    , Meta: mtaData
                    , PeriodStart: this.detailPanel.metaPanel.meta.periodStart
                    , PeriodEnd: this.detailPanel.metaPanel.meta.periodEnd
                    , TimeZoneOffset: this.detailPanel.metaPanel.meta.periodEnd.getTimezoneOffset()
                    , Prequel: this.detailPanel.metaPanel.meta.preQuel == null ? '' : this.detailPanel.metaPanel.meta.preQuel
                    , Status: this.detailPanel.fileDetails.statuslist.getValue()
                }
            };
            //if (Ext.isEmpty(o.Item.Status)) o.Item.Status = null;
            if (!this.detailPanel.fileDetails.statuslist.isVisible()) { o.Item.Status = null; }
            else if (Ext.isEmpty(o.Item.Status)) { this.detailPanel.fileDetails.statuslist.markInvalid(); this.getEl().unmask(); return; }
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'SaveItem',
                callback: this.onSaveCallback,
                scope: this,
                params: Ext.encode({ csridc: o })
            });
        } else {
            this.getEl().unmask();
        }


    }
    , onSaveCallback: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            if (!this.parentCaller) eBook.Interface.center.clientMenu.repository.reload();

            if (this.detailPanel.fileDetails.statuslist.getValue() == "b50ff824-3ccf-4ba5-b628-f8dbf6cd2b78") { //I/f status == To be send to client DS 
                if (this.detailPanel.fileDetails.extension == ".pdf") {
                    var o = { Id: eBook.Interface.currentClient.id, Department: "ACR" };
                    Ext.Ajax.request({
                        method: 'POST',
                        url: eBook.Service.client + 'GetClientPartnerByDepartment',
                        callback: this.onGetClientPartnerByDepartmentCallback,
                        scope: this,
                        params: Ext.encode({ ciddc: o })
                    });
                } else {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'You can only upload PDF files to docstore',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR,
                        scope: this
                    });
                }
            }

            /* GTH */
            var Team6Repo = Ext.getCmp('GthTeam6Repository');
            if (Team6Repo != null) {
                Team6Repo.refOwner.loadRepo();
            }

            if (this.readycallback) {
                this.readycallback.fn.call(this.readycallback.scope);
            }
            this.close();
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , onGetClientPartnerByDepartmentCallback: function (opts, success, resp) {
        //var wn = new eBook.Docstore.Window({ documentId: this.detailPanel.activeNode.attributes.Item.Id, clientId: this.detailPanel.activeNode.attributes.Item.ClientId, culture: eBook.Interface.Culture, partnerGpn: this.detailPanel.activeNode.attributes.Item.PartnerGpnAcr });
        var wn = new eBook.Docstore.Window({ documentId: opts.scope.repositoryItemId, clientId: eBook.Interface.currentClient.id, culture: eBook.Interface.Culture, partnerGpn: Ext.decode(resp.responseText).GetClientPartnerByDepartmentResult[0].gpn, uploaderGpn: eBook.User.gpn });

        wn.show();
    }
    , show: function (caller) {
        this.parentCaller = caller;
        eBook.Repository.FileDetailsWindow.superclass.show.call(this);
        if (this.tpe == "MOVE") {
            Ext.Msg.alert("Move file", "<b>Warning:</b><br/>Moving a file can result in loss of metadata (if present).<br/>To cancel, exit the window without saving.");
        }
    }
});


eBook.Repository.FileDetailsPanel = Ext.extend(Ext.Panel, {
    readOnly: false,
    readOnlyMeta: true,
    initComponent: function () {
        Ext.apply(this, {
            layout: 'border'
            , items: [
                        { xtype: 'panel', layout: 'form', region: 'north', height: 100, ref: 'fileDetails'
                            , bodyStyle: 'padding:10px;', readOnly: this.readOnly
                            , items: [
                                { xtype: 'textfield', maxLength: 250, fieldLabel: eBook.Repository.FileMetaPanel_FileName
                                    , ref: 'fileName', width: 420, disabled: this.readOnly
                                }
                                , { xtype: 'TreeSelect'
                                    , ref: 'locationTree'
                                    , width: 420
                                    , fieldLabel: eBook.Repository.FileMetaPanel_Location
						            , listWidth: 420
						            , lazyInit: false
						            , rootVisible: true
						            , loader: new eBook.Repository.TreeLoader({
						                dataUrl: eBook.Service.repository + 'GetChildren',
						                requestMethod: "POST",
						                showFiles: false,
						                selectionMode: true,
						                preloadChildren: true,
						                noLoad: true
						            })
						            , typeAhead: false
                                    , selectOnFocus: true
                                    , editable: false
                                    , forceSelection: true
                                    , disabled: this.readOnly
                                    , root: {
                                        nodeType: 'async',
                                        text: 'Repository',
                                        draggable: false,
                                        id: 'root',
                                        children: eBook.Interface.GetSelectionRepoStructure(),
                                        expanded: true
                                    }
                                    , listeners: {
                                        select: { fn: this.onSelect, scope: this }
                                    }

                                }, { xtype: 'combo'
                                , store: new eBook.data.JsonStore({
                                    selectAction: 'GetStatusList'
                                    , criteriaParameter: 'csdc'
                                    , autoDestroy: true
                                    , serviceUrl: eBook.Service.repository
                                    , storeParams: {}
                                    , disabled: this.readOnly
                                    , fields: eBook.data.RecordTypes.GlobalListItem
                                    , autoLoad: false
                                })
                                , fieldLabel: eBook.Repository.FileMetaPanel_Status
                                , allowBlank: false
                                , ref: 'statuslist'
                                , typeAhead: false
                                , mode: 'local'
                                , selectOnFocus: true
                                , autoSelect: true
                                , editable: false
                                , forceSelection: true
                                , triggerAction: 'all'
                                , nullable: false
                                , width: 200
                                , listWidth: 200
                                , displayField: eBook.Interface.Culture.substr(0, 2)
                                , valueField: 'id'
                                , hidden: true
                                , listeners: { select: { fn: this.onStatusChange, scope: this }
                                    //, disabled: this.readOnly
                                , disabled: this.readOnlyMeta == false ? this.readOnlyMeta : this.readOnly
                                , expand: { fn: this.onComboExpand, scope: this }
                                }
                                }]
                        }
                        , { xtype: 'tabpanel', ref: 'tbpanel', items: [
                                { xtype: 'reposFileMeta', readOnly: this.readOnly, readOnlyMeta: this.readOnlyMeta, autoScroll: true, title: eBook.Repository.FileDetailsPanel_MetaTitle, region: 'center', ref: '../metaPanel' }
                                , { xtype: 'RepositoryItemLinks', readOnly: this.readOnly, title: eBook.Repository.FileDetailsPanel_LinksTitle, ref: '../repolinks', repositoryItemId: this.repositoryItemId }
                                , { xtype: 'RepositoryPreview', ref: '../previewPanel', title: eBook.Repository.FileDetailsPanel_PreviewTitle}]
                            , region: 'center', activeTab: 0
                        }
                ]
        });
        eBook.Repository.FileDetailsPanel.superclass.initComponent.apply(this, arguments);
    }
    , onComboExpand: function (combo) {

        // Disable some statusses
        this.fileDetails.statuslist.store.filterBy(function (rec, id) {
            if (id != 'c0009211-8936-49f0-a2b4-e0f70a0a2a96' //Sent to DocStore
                && id != 'd54bcd3c-6b1f-46b5-b0c4-d66d307a7869' //Sent to client DS
                && id != 'eb72b2a6-8862-4544-ac7a-75c99a166ea4') { //Signed by client DS
                return rec;
            }
        }, this);
    }
    , onStatusChange: function (el) {
        var val = el.getValue();
        this.metaPanel.onStatusChange(val);
    }
    , onSelect: function (treeSel) {
        if (treeSel.selectedNode.attributes.Key == 'ENGAGEMENT') {
            this.treeSel = treeSel;
            this.userHasGTHrole = false;
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'PersonHasGTHrole',
                callback: this.onPersonHasGTHroleCallback,
                scope: this,
                params: Ext.encode({ cidc: { Id: eBook.User.personId} })
            });
        } else {
            this.onSelectProcess(treeSel);
        }

    }
    , onSelectProcess: function (treeSel) {
        if (treeSel.selectedNode) {
            this.fileDetails.statuslist.clearValue();
            if (!Ext.isEmpty(treeSel.selectedNode.attributes.StatusType)) {
                this.fileDetails.statuslist.store.baseParams.id = treeSel.selectedNode.attributes.StatusType;
                this.fileDetails.statuslist.store.load();
                this.fileDetails.statuslist.show();
            } else {
                this.fileDetails.statuslist.hide();
            }
            this.repolinks.allowDefaultLinking(treeSel.selectedNode.attributes.AllowLinks);
            var mc = { flds: [] };
            if (treeSel.selectedNode.attributes.MetaConfig != null) {
                if (treeSel.selectedNode.attributes.MetaConfig.ft == "CLIENTBANK") {
                    Ext.Ajax.request({
                        method: 'POST',
                        url: eBook.Service.client + 'GetClientExactBanks',
                        callback: this.loadExactMeta,
                        scope: this,
                        params: Ext.encode({ cidc: { Key: eBook.Interface.currentClient.get('EnterpriseNumber'), Id: eBook.Interface.currentClient.get('Id')} })
                    });
                } /*else if (treeSel.selectedNode.attributes.MetaConfig.ft == "") {
                   

                } */
                else {
                    mc = treeSel.selectedNode.attributes.MetaConfig;
                    this.metaPanel.loadConfig(mc, treeSel.selectedNode);
                }
            }

        }
    }

    , loadExactMeta: function (opts, success, resp) {
        if (success) {
            var mc = { flds: [] };


            this.metaPanel.loadConfig(Ext.decode(resp.responseText).GetClientExactBanksResult, this.fileDetails.locationTree.selectedNode);
        }
    }
    , onPersonHasGTHroleCallback: function (opts, success, resp) {
        if (success) {
            if (Ext.decode(resp.responseText).PersonHasGTHroleResult == true) {
                this.userHasGTHrole = true;
            } else {
                this.userHasGTHrole = false;
            }
        }
        this.onSelectProcess(this.treeSel)
    }
    , loadExistingFile: function (n, nw) {
        // TODO
        if (!nw) this.fileDetails.locationTree.disable();
        this.activeNode = n;
        this.ownerCt.getEl().mask("Loading", 'x-mask-loading');
        var fobj = n.attributes.Item;
        this.fileDetails.fileId = fobj.Id;
        this.fileDetails.physFileId = fobj.PhysicalFileId;
        this.fileDetails.fileName.setValue(fobj.FileName);
        this.fileDetails.extension = fobj.Extension;
        this.fileDetails.contentType = fobj.ContentType;
        this.fileDetails.statuslist.clearValue();
        this.fileDetails.locationTree.clearValue();
        this.fileDetails.locationTree.setValue(fobj.StructureId); // bank is unknown & dynamic.
        if (!this.fileDetails.locationTree.selectedNode) {
            this.fileDetails.locationTree.hide();
        }
        this.continueLoadExisting(this.fileDetails.locationTree.selectedNode ? this.fileDetails.locationTree.selectedNode : n.parentNode, true);

    }
    , continueLoadExisting: function (nde, success) {
        if (success) {
            if (!Ext.isEmpty(this.activeNode.attributes.Item.Status)) {
                this.fileDetails.statuslist.disable(this.readOnlyMeta == false ? this.readOnlyMeta : this.readOnly);
                this.onSelect({ selectedNode: nde });
                this.fileDetails.statuslist.store.on('load', function (st, rcs, opts) {
                    this.continueLoadExistingDefered.defer(500, this, [nde]);
                }, this, { single: true, scope: this });
                this.fileDetails.statuslist.store.load();

            } else {
                if (nde) this.onSelect({ selectedNode: nde });
                this.metaPanel.loadItem(this.activeNode.attributes.Item);
                this.previewPanel.loadFile("Repository/" + eBook.getGuidPath(this.activeNode.attributes.Item.ClientId) + this.activeNode.attributes.Item.PhysicalFileId + this.activeNode.attributes.Item.Extension);
                if (this.readOnly) {
                    //this.metaPanel.disable();
                    this.tbpanel.setActiveTab(2);
                }

                this.ownerCt.getEl().unmask();
            }

            //            if (this.fileDetails.statuslist.isVisible()) this.fileDetails.statuslist.store.load();
            //            this.fileDetails.statuslist.setValue(this.activeNode.attributes.Item.Status);

            //if (this.fileDetails.statuslist.isVisible()) 

        } else {
            alert("Failed loading the tree");
            this.ownerCt.getEl().unmask();
        }

    }
    , continueLoadExistingDefered: function (nde) {

        !this.readOnly ? this.fileDetails.statuslist.enable() : null;
        !this.readOnlyMeta ? this.fileDetails.statuslist.enable() : null;

        this.fileDetails.statuslist.setValue(this.activeNode.attributes.Item.Status);
        this.onStatusChange(this.fileDetails.statuslist);
        this.metaPanel.loadItem(this.activeNode.attributes.Item);
        this.previewPanel.loadFile("Repository/" + eBook.getGuidPath(this.activeNode.attributes.Item.ClientId) + this.activeNode.attributes.Item.PhysicalFileId + ".pdf");
        if (this.readOnly) {
            //this.metaPanel.disable();
            this.tbpanel.setActiveTab(2);
        }
        this.ownerCt.getEl().unmask();
    }
    , loadNewFile: function (fobj, standards) {
        this.fileDetails.locationTree.enable();
        this.fileDetails.fileId = fobj.id;
        this.fileDetails.physFileId = fobj.id;
        this.fileDetails.fileName.setValue(fobj.oldName);
        this.fileDetails.extension = fobj.extension;
        this.fileDetails.contentType = fobj.contentType;
        this.fileDetails.statuslist.clearValue();
        this.fileDetails.locationTree.clearValue();
        this.metaPanel.loadConfig({ flds: [] }, null);
        if (standards && !Ext.isEmpty(standards.location)) {
            this.fileDetails.locationTree.setValue(standards.location);
            this.fileDetails.locationTree.disable();
            this.onSelect({ selectedNode: this.fileDetails.locationTree.selectedNode });
            if (!Ext.isEmpty(standards.statusid)) {

                this.fileDetails.statuslist.store.on('load', function (st, rcs, opts) {
                    this.fileDetails.statuslist.setValue(standards.statusid);
                }, this, { single: true, scope: this });
                this.fileDetails.statuslist.store.load();

            }
        }

        if (fobj.extension == ".pdf") {
            this.previewPanel.loadFile("upload/" + fobj.id + fobj.extension);
        } else {
            // not a pdf... can't preview ?
            //this.previewPanel
        }
    }

    , getData: function () {

    }
});



eBook.Repository.FileMetaPanel = Ext.extend(Ext.Panel, {
    cfgTpl: '<tpl for="."><div><div class="eb-repos-filemeta-field" fieldId="_PERIOD"><div class="eb-repos-filemeta-field-label"></div><div class="eb-repos-filemeta-field-item"></div></div><tpl for="flds"><div class="eb-repos-filemeta-field" fieldId="{Id}"><div class="eb-repos-filemeta-field-label"></div><div class="eb-repos-filemeta-field-item"></div></div></tpl></div></tpl>'
    , initComponent: function () {
        Ext.apply(this, {
            html: '<div>Select category</div>'
            , frame: false
        });
        this.cfgTpl = new Ext.XTemplate(this.cfgTpl);
        this.cfgTpl.compile();
        eBook.Repository.FileMetaPanel.superclass.initComponent.apply(this, arguments);
    }
    , fields: []
    , periodFields: []
    , metaConfig: {}
    , periodType: 'PERIOD'
    , categoryPeriod: {
        start: null
        , end: null
    }
    , clearFields: function () {
        for (var i = 0; i < this.fields.length; i++) {
            this.fields[i].destroy();
        }
        this.fields = [];
        for (var i = 0; i < this.periodFields.length; i++) {
            this.periodFields[i].destroy();
        }
        this.periodFields = [];
    }
    , meta: {
        periodStart: null
        , periodEnd: null
        , preQuel: ''
    }
    , loadItem: function (item) {
        this.loadMeta(item.Meta);
        this.meta.preQuel = item.preQuel || '';
        this.meta.periodStart = Ext.data.Types.WCFDATE.convert(item.PeriodStart);
        this.meta.periodEnd = Ext.data.Types.WCFDATE.convert(item.PeriodEnd);
        switch (this.periodType) {
            case "YEAR":
                this.periodFields[0].setValue(this.meta.periodStart.getFullYear());
                break;
            case "QUARTERMONTH":

                if (monthDiff(this.meta.periodStart, this.meta.periodEnd) > 1) {
                    this.periodFields[0].setValue('Q');
                    var q = (this.meta.periodEnd.getMonth() + 1) / 3;

                    this.periodFields[1].setValue('Q0' + q + '/' + this.meta.periodEnd.getFullYear())
                } else {
                    this.periodFields[0].setValue('M');
                    var m = (this.meta.periodEnd.getMonth() + 1);
                    if (m < 10) m = '0' + m;
                    this.periodFields[1].setValue(m + '/' + this.meta.periodEnd.getFullYear())
                }
                break;
            default:
                if (this.periodFields && this.periodFields.length > 0) {
                    this.periodFields[0].setValue(this.meta.periodStart);
                    this.periodFields[1].setValue(this.meta.periodEnd);
                    var el = this.periodFields[1];
                    if (el.fiscalYearEl) {
                        if (this.meta.periodEnd) {
                            var fy = new Date(this.meta.periodEnd.getFullYear, this.meta.periodEnd.getMonth(), this.meta.periodEnd.getDate());
                            fy.setDate(fy.getDate() + 1);
                            fy = fy.getFullYear();
                            el.fiscalYearEl.update(fy + ' &gt; ');
                        } else {
                            el.fiscalYearEl.update('&nbsp;');
                        }
                    }
                }
                break;
        }
        if (this.readOnly) {
            if (this.periodFields[0]) this.periodFields[0].disable();
            if (this.periodFields[1]) this.periodFields[1].disable();
        }
        if (this.readOnlyMeta) {
            for (var i = 0; i < this.fields.length; i++) {
                this.fields[i].disable();
            }
        }
    }
    , loadMeta: function (meta) {
        if (meta == null) return;
        for (var i = 0; i < meta.length; i++) {
            var idx = this.fieldIdx[meta[i].Id];
            if (Ext.isDefined(idx) && idx != null) {
                // check for date.
                var fld = this.fields[idx];
                var val = meta[i].Value;
                //if (fld.xtype == "datefield")

                if (fld.xtype == "datefield" && Ext.isString(val)) {
                    var dte = Date.parseDate(val, "j/n/Y"); // fix for non-leading zeroes: j(day)/n(month)
                    fld.setValue(dte);

                } else if (fld.xtype == "ewList_Global") {
                    try {
                        val = Ext.util.JSON.decode(val);
                    } catch (e) { }
                    fld.setActiveRecord(val);
                } if (fld.xtype == "repoComments" && Ext.isString(val)) {
                    fld.setValue(Ext.util.JSON.decode(val));
                } else {
                    fld.setValue(val);

                }
            }
        }
    }
    , validateData: function () {
        var hasValues = false;
        for (var i = 0; i < this.periodFields.length; i++) {
            if (!this.periodFields[i].isValid()) return false;
        }

        if (this.fields.length == 0 && !this.feature == "CLIENTBANK") hasValues = true;

        for (var i = 0; i < this.fields.length; i++) {
            if (!this.fields[i].isValid() && this.fields[i].isVisible()) return false;
            if (!hasValues) {
                if (Ext.isBoolean(this.fields[i].getValue())) {
                    hasValues = this.fields[i].getValue();
                } else {
                    hasValues = !Ext.isEmpty(this.fields[i].getValue());
                }
            }
        }


        return hasValues;
    }
    , getMetaData: function () {
        var o = [], i;
        for (i = 0; i < this.fields.length; i++) {
            if (this.fields[i].isMetaField) {
                // check for date.
                val = this.fields[i].getValue();
                if (this.fields[i].xtype == "datefield") {
                    val = val.format('d/m/Y');
                } else if (this.fields[i].xtype == "ewList_Global") {
                    val = Ext.util.JSON.encode(val);
                } if (this.fields[i].xtype == "repoComments") {
                    val = Ext.util.JSON.encode(val);
                } else {
                    if (val != null) val = val.toString();
                }
                o.push({ Id: this.fields[i].name, Value: val });
            }
        }

        //update periods
        for (i = 0; i < this.periodFields.length; i++) {
            this.periodChange(this.periodFields[i], null, null);
        }

        return o;
    }
    , periodChange: function (el, nw, old) {
        this.meta.preQuel = '';
        switch (el.periodType) {
            case "YEAR":
                this.meta.periodStart = new Date(el.getValue(), 0, 1);
                this.meta.periodEnd = new Date(el.getValue(), 11, 31);
                break;
            case "QUARTERMONTH_TYPE":
                var val = el.getValue();
                var fldVal = this.periodFields[1];
                fldVal.store.baseParams.qm = el.getValue();
                fldVal.clearValue();
                fldVal.store.load();
                break;
            case "QUARTERMONTH_VALUE":
                var val = el.getValue();
                var endM, endY;
                //var fldVal = this.fields[el.linkedFldIdx];
                if (!Ext.isEmpty(val)) {
                    if (val.substr(0, 1) == 'Q') {
                        val = val.substr(1, 7).split('/');
                        endM = (parseInt(val[0]) * 3) - 1; // javascript months: 0-11
                        endY = parseInt(val[1]);
                        this.meta.periodStart = new Date(endY, (endM - 2), 1);
                        this.meta.periodEnd = new Date(endY, (endM + 1), 1);
                        this.meta.periodEnd.setDate(this.meta.periodEnd.getDate() - 1);
                    } else {
                        var sval = val.split('/');
                        endM = (parseInt(sval[0])) - 1; // javascript months: 0-11
                        endY = parseInt(sval[1]);
                        this.meta.periodStart = new Date(endY, (endM), 1);
                        this.meta.periodEnd = new Date(endY, (endM + 1), 1);
                        this.meta.periodEnd.setDate(this.meta.periodEnd.getDate() - 1);

                    }
                    this.meta.preQuel = val;
                }
                break;
            default:
                if (el.name == "StartDate") {
                    this.meta.periodStart = el.getValue();
                }
                if (el.name == "EndDate") {
                    this.meta.periodEnd = el.getValue();
                    if (Ext.isEmpty(this.meta.periodEnd)) {
                        this.meta.periodEnd = new Date(2100, 11, 31);
                    }
                }
                if (el.fiscalYearEl) {
                    if (this.meta.periodEnd) {
                        var fy = new Date(this.meta.periodEnd.getFullYear, this.meta.periodEnd.getMonth(), this.meta.periodEnd.getDate());
                        fy.setDate(fy.getDate() + 1);
                        fy = fy.getFullYear();
                        el.fiscalYearEl.update(fy + ' &gt; ');
                    } else {
                        el.fiscalYearEl.update('&nbsp;');
                    }
                }
                break;
        }

    }
    , renderPeriod: function (fel, nde) {
        fel = Ext.get(fel);
        var lel = fel.child('.eb-repos-filemeta-field-label');
        var ffel = fel.child('.eb-repos-filemeta-field-item');

        var ps = this.categoryPeriod ? this.categoryPeriod.start : null;
        var pe = this.categoryPeriod ? this.categoryPeriod.end : null;
        var fyEl = null;

        lel.update(eBook.Repository['FileMetaPanel_' + nde.attributes.DateType]);
        switch (nde.attributes.DateType) {
            case "CALENDERYEAR":
            case "YEAR":
                var fld = new Ext.form.ComboBox({
                    store: new eBook.data.JsonStore({
                        selectAction: 'GetYearsInt'
                        , criteriaParameter: 'cpydc'
                        , autoDestroy: true
                        , serviceUrl: eBook.Service.repository
                        , storeParams: { ps: ps, pe: pe }
                        , fields: eBook.data.RecordTypes.YearList
                        , autoLoad: true
                    })
                    , allowBlank: false
                    , renderTo: ffel
                    , typeAhead: false
                    , mode: 'local'
                    , selectOnFocus: true
                    , autoSelect: true
                    , editable: false
                    , forceSelection: true
                    , triggerAction: 'all'
                    , nullable: false
                    , width: 100
                    , listWidth: 200
                    , displayField: 'Id'
                    , valueField: 'Id'
                    , value: pe != null ? pe.getFullYear() : (new Date()).getFullYear()
                    , periodType: nde.attributes.DateType
                    , listeners: {
                        select: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                break;
            case "QUARTERMONTH":
                // ffel.createChild({ tag: 'span', html: 'Periodtype ' });
                var cbCfg = {
                    store: [['M', eBook.Repository.FileMetaPanel_MONTHLY], ['Q', eBook.Repository.FileMetaPanel_QUARTERLY]]
                    , allowBlank: false
                    , renderTo: ffel
                    , typeAhead: false
                    , mode: 'local'
                    , selectOnFocus: true
                    , autoSelect: true
                    , editable: false
                    , forceSelection: true
                    , triggerAction: 'all'
                    , nullable: false
                    , width: 100
                    , listWidth: 200
                    , periodType: "QUARTERMONTH_TYPE"
                    , linkedFldIdx: this.fields.length + 1
                    , listeners: {
                        select: { fn: this.periodChange, scope: this }
                    }
                };
                var perVal = null;
                cbCfg.value = 'M';
                if (this.meta.periodStart != null && this.meta.periodEnd != null) {

                    perVal = '' + (this.meta.periodStart.getMonth() + 1);
                    if (perVal.length == 1) perVal = '0' + perVal;

                    if (monthDiff(this.meta.periodStart, this.meta.periodEnd) > 1) {
                        cbCfg.value = 'Q';
                        sval = 'Q';
                        perVal = "Q0" + ((this.meta.periodEnd.getMonth() + 1) / 3);
                    }
                    perVal += '/' + this.meta.periodStart.getFullYear();
                }
                var fld = new Ext.form.ComboBox(cbCfg);
                this.periodFields.push(fld);
                // ffel.createChild({ tag: 'span', html: ' Period ' });
                var fcfg2 = {
                    store: new eBook.data.JsonStore({
                        selectAction: 'GetQuarterOrMonthList'
                            , criteriaParameter: 'cqmdc'
                            , autoDestroy: true
                            , serviceUrl: eBook.Service.repository
                            , baseParams: { qm: cbCfg.value, ps: ps, pe: pe }
                            , fields: eBook.data.RecordTypes.MonthList
                            , autoLoad: true
                    })
                    , periodType: "QUARTERMONTH_VALUE"
                    , allowBlank: false
                    , renderTo: ffel
                    , typeAhead: false
                    , mode: 'local'
                    , selectOnFocus: true
                    , autoSelect: true
                    , editable: false
                    , forceSelection: true
                    , triggerAction: 'all'
                    , nullable: false
                    , width: 100
                    , listWidth: 200
                    , displayField: 'Id'
                    , valueField: 'Id'
                    , linkedFldIdx: this.fields.length
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                    }
                };
                if (perVal) fcfg2.value = perVal;
                var fld2 = new Ext.form.ComboBox(fcfg2);
                this.periodFields.push(fld2);
                break;
            case "INFINITEPERIOD":
                //ffel.createChild({ tag: 'span', html: '' + eBook.Create.Empty.PreviousFileFieldSet_Startdate });
               
                var fld = new Ext.form.DateField({
                    allowBlank: false
                    , format: 'd/m/Y'
                    , name: 'StartDate'
                    , renderTo: ffel
                   // , hidden: true
                    , value: new Date()
                    , periodType: nde.attributes.DateType
                    , listeners: {
                       // change: { fn: this.periodChange, scope: this },
                       // valid: { fn: this.setDefaultEndDate, scope: this }
                    }
                });
                this.periodFields.push(fld);
               // ffel.createChild({ tag: 'span', html: ' <div class="x-clear"></div>' + eBook.Create.Empty.PreviousFileFieldSet_Enddate });

                var fld = new Ext.form.DateField({
                    allowBlank: true
                    , format: 'd/m/Y'
                    , name: 'EndDate'
                    //, hidden: true
                    , value: new Date(2099, 12, 31) // upon req. of Fred. Godinho
                    , renderTo: ffel
                    , periodType: nde.attributes.DateType
                    , listeners: {
                       // change: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                break;
            case "PERIOD":
                ffel.createChild({ tag: 'span', html: '' + eBook.Create.Empty.PreviousFileFieldSet_Startdate });

                var fld = new Ext.form.DateField({
                    allowBlank: false
                    , format: 'd/m/Y'
                    , name: 'StartDate'
                    , renderTo: ffel
                    , periodType: nde.attributes.DateType
                    , listeners: {
                        change: { fn: this.periodChange, scope: this },
                        valid: { fn: this.setDefaultEndDate, scope: this }
                    }
                });
                this.periodFields.push(fld);
                ffel.createChild({ tag: 'span', html: ' <div class="x-clear"></div>' + eBook.Create.Empty.PreviousFileFieldSet_Enddate });

                var fld = new Ext.form.DateField({
                    allowBlank: true
                    , format: 'd/m/Y'
                    , name: 'EndDate'
                    , renderTo: ffel
                    , periodType: nde.attributes.DateType
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                break;
            case "FISCALYEAR":
                var d = '';
                if (pe) {
                    d = new Date(pe.getFullYear(), pe.getMonth(), pe.getDate());
                    d.setDate(d.getDate() + 1)
                    d = d.getFullYear();
                }

                //ffel.createChild({ tag: 'span', html: d + ' &gt; ' });
                var par = ffel.parent().parent();
                par.createChild({ tag: 'div', cls: 'eb-repos-filemeta-field',
                    children: [{ tag: 'div', cls: 'eb-repos-filemeta-field-label', html: 'Aanslagjaar' }
                                             , { tag: 'div', cls: 'eb-repos-filemeta-field-item', html: d}]
                });


            case "BOOKYEAR":
                var fld = new Ext.form.DateField({
                    allowBlank: false
                    , format: 'd/m/Y'
                    , name: 'StartDate'
                    , renderTo: ffel
                    , value: ps
                    , disabled: this.categoryPeriod != null // nde.attributes.StructuralType == "FILE"
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                ffel.createChild({ tag: 'span', html: ' to ' });

                var fld = new Ext.form.DateField({
                    allowBlank: true
                    , format: 'd/m/Y'
                    , name: 'EndDate'
                    , fiscalYearEl: fyEl
                    , renderTo: ffel
                    , value: pe
                    , disabled: this.categoryPeriod != null //nde.attributes.StructuralType == "FILE"
                    , listeners: {
                        change: { fn: this.periodChange, scope: this }
                    }
                });
                this.periodFields.push(fld);
                if (nde.attributes.StructuralType == "FILE") {
                    this.meta.periodStart = ps;
                    this.meta.periodEnd = pe;
                }
                break;
        }

    }
    , fieldIdx: {}
    , statusChangeFields: []
    , renderField: function (fel, fcfg) {
        fel = Ext.get(fel);
        var lel = fel.child('.eb-repos-filemeta-field-label');
        var ffel = fel.child('.eb-repos-filemeta-field-item');
        var cfg = { xtype: '' };

        var clientSideVisibilitySource = false;
        var clientSideVisibilitySourceElements = null;
        var clientSideVisibilitySourceValues = null;
        var statusChange = [];

        for (var i = 0; i < fcfg.Attributes.length; i++) {
            switch (fcfg.Attributes[i].k) {
                case "CLIENT_VISIBILITY":
                    clientSideVisibilitySource = true;
                    break;
                case "CLIENT_VISIBILITY_TARGETS":
                    clientSideVisibilitySourceElements = Ext.util.JSON.decode(fcfg.Attributes[i].v);
                    break;
                case "CLIENT_VISIBILITY_VALUES":
                    clientSideVisibilitySourceValues = Ext.util.JSON.decode(fcfg.Attributes[i].v);
                    break;
                case "STATUS_VISIBILITY_VALUES":
                    statusChange = Ext.util.JSON.decode(fcfg.Attributes[i].v);
                    break;

            }
        }
        var listeners = {};
        lel.update(fcfg.Names[eBook.Interface.Culture.substr(0, 2)]);
        switch (fcfg.FieldType) {
            case "TextField":
                cfg = { xtype: 'textfield', name: fcfg.Id };
                break;
            case "Comments":
                cfg = { xtype: 'repoComments', name: fcfg.Id, myOwner: this };
                break;
            case "RadioGroup":
                cfg = { xtype: 'rpradiogroup', name: fcfg.Id };
                break;
            case "NumberField":
                cfg = { xtype: 'numberfield', name: fcfg.Id };
                break;
            case "EuroField":
                cfg = { xtype: 'numberfield', name: fcfg.Id };
                break;
            case "TextArea":
                cfg = { xtype: 'textarea', name: fcfg.Id };
                break;
            case "LocalInputList":
                cfg = { xtype: 'localinputlist', name: fcfg.Id };
                break;
            case "GlobalList":
                cfg = { xtype: 'ewList_Global', name: fcfg.Id, width: 300, autoLoad: true };
                cfg.attributes = [];
                for (var j = 0; j < fcfg.Attributes.length; j++) {
                    cfg.attributes[fcfg.Attributes[j].k] = fcfg.Attributes[j].v;
                }
                break;
            case "Date":
                cfg = { xtype: 'datefield', format: 'd/m/Y', name: fcfg.Id };
                break;
            case "Checkbox":
                cfg = { xtype: 'checkbox', name: fcfg.Id };
                cfg.attributes = [];
                for (var j = 0; j < fcfg.Attributes.length; j++) {
                    if (fcfg.Attributes[j].k == 'checked' && fcfg.Attributes[j].v == 'true') {
                        cfg.checked = true;
                    }
                    if (fcfg.Attributes[j].k == 'GTHonly' && fcfg.Attributes[j].v == 'true') {
                        if (!this.ownerCt.ownerCt.userHasGTHrole) {
                            cfg.disabled = true;
                        }
                    }


                }
                break;
            case "Label":
                cfg = { xtype: 'component', name: fcfg.Id, html: fcfg.Names };
                break;
        }
        cfg.fldAttributes = fcfg.Attributes;
        cfg.show = eBook.Fields.ReposShow;
        cfg.hide = eBook.Fields.ReposHide;
        if (clientSideVisibilitySource) {
            var lcfg = { fn: this.onFieldVisibilityChange, scope: this };
            listeners.change = lcfg;
            listeners.check = lcfg;
            listeners.select = lcfg;
            cfg.visibilityChange = {
                targets: clientSideVisibilitySourceElements
                , values: clientSideVisibilitySourceValues
            };
        }
        cfg.statusChange = statusChange;
        cfg.listeners = listeners;
        cfg.allowBlank = !fcfg.Required;
        cfg.isMetaField = true;
        cfg.renderTo = ffel;
        var fld = Ext.ComponentMgr.create(cfg);
        if (fcfg.FieldType != 'Label') {
            this.fields.push(fld);
            this.fieldIdx[fld.name] = this.fields.length - 1;
        }

        if (statusChange.length > 0) {
            this.statusChangeFields.push(fld.name);
        }
    }
    , setDefaultEndDate: function (field) {
        var datefield = this.periodFields[1];
        if (datefield.name == 'EndDate' && !datefield.value) {
            var res = (field.value).split("/");
            this.periodFields[1].setValue(res[0] + "/" + res[1] + "/" + (parseInt(res[2]) + 5));
        }
    }
    , onFieldVisibilityChange: function (src) {
        var val = src.getValue();
        var prefix = ''
        //if(src.xtype=="checkbox") val = src.
        if (src.xtype == "ewList_Global") {

            // prefix = 'G';
        }
        if (Ext.isObject(val)) val = val.id;
        val = prefix + val;
        for (var i = 0; i < src.visibilityChange.targets.length; i++) {
            var fn = src.visibilityChange.targets[i];
            var vals = src.visibilityChange.values[fn];
            var fld = this.fields[this.fieldIdx[fn]];
            var foundMatch = false;
            for (var j = 0; j < vals.length; j++) {
                if (val.toLowerCase() == (prefix + vals[j]).toLowerCase()) {
                    fld.allowBlank = false;
                    fld.show();
                    foundMatch = true;
                } //else {
                if (!foundMatch) {
                    fld.allowBlank = true;
                    fld.hide();
                }
            }
        }
    }
    , onStatusChange: function (val) {
        for (var i = 0; i < this.statusChangeFields.length; i++) {
            var fn = this.statusChangeFields[i];
            var fld = this.fields[this.fieldIdx[fn]];
            if (fld.statusChange) {
                fld.hide();
                for (var j = 0; j < fld.statusChange.length; j++) {
                    if (val.toLowerCase() == fld.statusChange[j].toString().toLowerCase()) {
                        fld.show();
                    }
                }
            }
        }
    }
    , loadConfig: function (cfg, selNode) {
        this.metaConfig = cfg;
        this.statusChangeFields = [];
        this.fieldIdx = {};
        this.clearFields();
        this.feature = cfg.ft;
        this.body.update(this.cfgTpl.apply(cfg));
        if (selNode == null) return;
        this.periodType = selNode.attributes.DateType
        var periodParent = null;
        if (selNode.attributes.StructuralType == "FILE") {
            periodParent = selNode.findParentBy(function () {
                return this.attributes.PeriodStart && !Ext.isEmpty(this.attributes.PeriodStart)
                       && this.attributes.PeriodEnd && !Ext.isEmpty(this.attributes.PeriodEnd);
            }, null);
        }
        if (periodParent) {
            this.categoryPeriod = {
                start: Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodStart)
                    , end: Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodEnd)
            };
        } else {
            this.categoryPeriod = null;
            if (eBook.Interface.currentFile) {
                if (!eBook.Interface.currentFile.gthTeam5FileId) {
                    this.categoryPeriod = { start: eBook.Interface.currentFile.get('StartDate')
                        , end: eBook.Interface.currentFile.get('EndDate')
                    };
                } else {
                    this.categoryPeriod = { start: eBook.Interface.currentFile.gthTeam5StartDate
                        , end: eBook.Interface.currentFile.gthTeam5EndDate
                    };
                }
            }
        }

        var flds = this.body.query('.eb-repos-filemeta-field');
        var minIdx = 0;

        for (var i = 0; i < flds.length; i++) {
            var fid = flds[i].getAttribute('fieldId');
            if (fid.indexOf('_') == 0) {
                this.renderPeriod(flds[i], selNode);
                minIdx += 1;
            } else {
                this.renderField(flds[i], cfg.flds[i - minIdx]);
            }

        }
    }
});

Ext.reg('reposFileMeta',eBook.Repository.FileMetaPanel);
