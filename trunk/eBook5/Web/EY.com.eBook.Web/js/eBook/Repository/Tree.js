

Ext.override(Ext.tree.AsyncTreeNode, {
    isExpandable: function () {
        return this.attributes.isDynamic || this.attributes.expandable || this.hasChildNodes();
    },
    hasChildNodes: function () {
        if ((!this.isLeaf() && !this.loaded) || this.isDynamic) {
            return true;
        } else {
            return Ext.tree.AsyncTreeNode.superclass.hasChildNodes.call(this);
        }
    },
    loadComplete: function (deep, anim, callback, scope) {
        this.loading = false;
        this.loaded = true;
        this.completedRun = true;
        this.ui.afterLoad(this);
        this.fireEvent("load", this);
        this.expand(deep, anim, callback, scope);
        this.completedRun = false;
    },
    expand: function (deep, anim, callback, scope) {
        if (this.loading) { // if an async load is already running, waiting til it's done
            var timer;
            var f = function () {
                if (!this.loading) { // done loading
                    clearInterval(timer);
                    this.expand(deep, anim, callback, scope);
                }
            } .createDelegate(this);
            timer = setInterval(f, 200);
            return;
        }
        if (this.attributes && this.attributes.isDynamic && !this.completedRun) {
            delete this.attributes.children;
            this.attributes.expandable = true;
            this.loaded = false;
        }
        if (!this.loaded && !this.completedRun) {
            if (this.fireEvent("beforeload", this) === false) {
                return;
            }
            this.loading = true;
            this.ui.beforeLoad(this);
            var loader = this.loader || this.attributes.loader || this.getOwnerTree().getLoader();
            if (loader) {
                loader.load(this, this.loadComplete.createDelegate(this, [deep, anim, callback, scope]), this);
                return;
            }
        }
        Ext.tree.AsyncTreeNode.superclass.expand.call(this, deep, anim, callback, scope);
    }
});  

Ext.tree.AsyncTreeNode.prototype.findParent= function(attribute, value) {
        return this.findParent(function() {
            return this.attributes[attribute] == value;
        }, null);
};
Ext.tree.AsyncTreeNode.prototype.findParentBy = function(fn, scope) {
        // include self
        if (fn.call(scope || this, this) === true) { 
            return this;
        }
        if (this.parentNode) {
            if (fn.call(scope || this.parentNode, this.parentNode) === true) {
                return this.parentNode;
            } else {
                return this.parentNode.findParentBy(fn, scope);
            }
        }
        return null;
};


eBook.Repository.TreeLoader = Ext.extend(Ext.tree.TreeLoader, {
    filter: {
        ps: null
        , pe: null
        , stat: null
    }
    , load: function (node, callback, scope) {
        if (this.clearOnLoad) {
            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
        }
        if (this.doPreload(node)) { // preloaded json children
            this.runCallback(callback, scope || node, [node]);
        } else if (this.directFn || this.dataUrl || this.url) {
            this.requestData(node, callback, scope || node);
        }
    }
    , doPreload: function (node) {
        if (node.attributes.children) {
            if (node.childNodes.length < 1) { // preloaded?
                var cs = node.attributes.children;
                node.beginUpdate();
                for (var i = 0, len = cs.length; i < len; i++) {
                    var cn = node.appendChild(this.createNode(cs[i]));
                    if (this.preloadChildren) {
                        this.doPreload(cn);
                    }
                }
                node.endUpdate();
            }
            return true;
        }
        // alert("false dopreload");
        return false;
    }
    , selectionMode: false
    , createNode: function (attr, parent) {
        // apply baseAttrs, nice idea Corey!
        if (!Ext.isDefined(attr.originalConfig)) {
            attr.originalConfig = Ext.decode(Ext.encode(attr));
        }
        if (this.baseAttrs) {
            Ext.applyIf(attr, this.baseAttrs);
        }
        if (!attr.leaf) {
            if (!Ext.isDefined(attr.StructureId)) {
                Ext.apply(attr, { StructureId: attr.id });
            }
            attr.id = Ext.id();
        }

        if (this.applyLoader !== false && !attr.loader) {
            attr.loader = this;
        }
        if (Ext.isString(attr.uiProvider)) {
            attr.uiProvider = this.uiProviders[attr.uiProvider] || eval(attr.uiProvider);
        }
        if (attr.nodeType) {
            return new Ext.tree.TreePanel.nodeTypes[attr.nodeType](attr);
        } else {
            return attr.leaf ?
                        new Ext.tree.TreeNode(attr) :
                        new Ext.tree.AsyncTreeNode(attr);
        }
    }
    , requestData: function (node, callback, scope) {
        if (node.attributes.StructureId == 'root') {
            this.processNodes(node, eBook.Interface.GetRepoStructure('perm'), callback, scope);
        } else {
            if (node.attributes.Key == 'PERIOD_ITEM') {
                this.processNodes(node, eBook.Interface.GetRepoStructure('period'), callback, scope);
            } else {
                // if (!this.selectionMode) {
                this.transId = Ext.Ajax.request({
                    method: 'POST',
                    url: this.dataUrl || this.url,
                    success: this.handleResponse,
                    failure: this.handleFailure,
                    scope: this,
                    argument: { callback: callback, node: node, scope: scope },
                    params: this.getSendingParam(node)
                });
                //} else {
                //    this.runCallback(callback, scope || node, [node]);
                //}
            }
        }

    }
    , processNodes: function (node, o, callback, scope) {
        node.beginUpdate();
        for (var i = 0, len = o.length; i < len; i++) {
            var n = this.createNode(o[i], node);
            if (n) {
                if (n.attributes.Files == false && this.selectionMode) {
                    if (!Ext.isEmpty(n.attributes.cls) && n.attributes.cls.length > 0) {
                        n.attributes.cls += ' ';
                    } else {
                        n.attributes.cls = '';
                    }
                    n.attributes.cls += "tree-node-unselectable";
                }

                node.appendChild(n);

            }
        }
        node.endUpdate();
        this.runCallback(callback, scope || node, [node]);
    }
    , processResponse: function (response, node, callback, scope) {
        var json = response.responseText;
        try {
            var o = response.responseData || Ext.decode(json);
            o = o.GetChildrenResult;
            this.processNodes(node, o, callback, scope);
        } catch (e) {
            this.handleFailure(response);
        }
    }
    , getSendingParam: function (node) {
        var ot = node.getOwnerTree();
        // file
        var fileId = null;        
        if (eBook.Interface.currentFile) {
            if (eBook.Interface.currentFile.gthTeam5FileId) {
                fileId = eBook.Interface.currentFile.gthTeam5FileId;
            } else {
                fileId = eBook.Interface.currentFile.get('Id');
            }
        }

        // client
        var clientId = null;
        if (eBook.Interface.currentClient) {
            if (eBook.Interface.currentClient.gthTeam5ClientId) {
                clientId = eBook.Interface.currentClient.gthTeam5ClientId;
            } else {
                clientId = eBook.Interface.currentClient.get('Id');
            }
        }

        var o = { crcdc: {
                cid: clientId
                //cid: eBook.Interface.currentClient ? eBook.Interface.currentClient.get('Id') : ot.ClientId
                , cult: eBook.Interface.Culture
                , strucId: node.attributes.StructureId == 'root' ? null : node.attributes.StructureId
                , key: node.attributes.Key
                , strucTp: node.attributes.StructuralType
                , showFiles: this.showFiles
                , fileId: fileId
            //, fileId: eBook.Interface.currentFile ? eBook.Interface.currentFile.get('Id') : null
        }
        };

        Ext.apply(o.crcdc, this.filter);
        if (ot.filter) {
            Ext.apply(o.crcdc, ot.filter);
        }
        var periodParent = null;
        if (node.attributes.StructuralType == "FILE") {

            periodParent = node.findParentBy(function () {
                return this.attributes.PeriodStart && !Ext.isEmpty(this.attributes.PeriodStart);
            }, null);
        }
        if (periodParent) {
            o.crcdc.ps = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodStart);
            o.crcdc.pe = Ext.data.Types.WCFDATE.convert(periodParent.attributes.PeriodEnd);
        }
        return Ext.encode(o);
    }
});


eBook.Repository.Tree = Ext.extend(Ext.tree.TreePanel, {
    readOnly: false,
    readOnlyMeta: false,
    standardFilter: {
        ps: null
            , pe: null
            , stat: null
    },
    initComponent: function () {
        if (eBook.Interface.currentFile) {
            this.standardFilter = {
                ps: eBook.Interface.currentFile.get('StartDate')
                , pe: eBook.Interface.currentFile.get('EndDate')
                , stat: null
            };
        }

        var loader = new eBook.Repository.TreeLoader({
            dataUrl: eBook.Service.repository + 'GetChildren',
            requestMethod: "POST", showFiles: true, filter: this.standardFilter
        });

        var root = {
            nodeType: 'async',
            text: 'Repository',
            draggable: false,
            id: 'root'//,
            // expanded: true
        };
        if (this.startFrom) {
            //'period'
            root = loader.createNode(eBook.Interface.GetRepoStructureById(this.startFrom.type, this.startFrom.id));
        }

        if (!this.readOnly) {
            Ext.apply(this, {
                tbar: [{
                    ref: 'addfile',
                    text: 'Upload file', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-repository-add-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onAddFileClick,
                    width: 70,
                    scope: this,
                    disabled: eBook.Interface.isFileClosed()
                }, {
                    ref: 'actFilter',
                    text: 'Filter', // eBook.Accounts.Manage.Window_AddAccount,
                    iconCls: 'eBook-filter-ico-24',
                    scale: 'medium',
                    enableToggle: true,
                    pressed: false,
                    iconAlign: 'top',
                    toggleHandler: this.onFilterClick,
                    width: 70,
                    hidden: eBook.Interface.currentFile != null,
                    scope: this
                },
                    {
                        xtype: 'buttongroup',
                        ref: 'dateFilters',
                        disabled: true,
                        columns: 2,
                        hidden: eBook.Interface.currentFile != null,
                        title: 'Date/period filter',
                        items: [
                        { xtype: 'datefield', format: 'd/m/Y', name: 'StartDate', ref: 'startDate', tooltip: 'Start date', listeners: { select: { fn: this.onDateFilterChange, scope: this }, change: { fn: this.onDateFilterChange, scope: this}} }
                        , { xtype: 'datefield', format: 'd/m/Y', name: 'EndDate', ref: 'endDate', tooltip: 'End date', listeners: { select: { fn: this.onDateFilterChange, scope: this }, change: { fn: this.onDateFilterChange, scope: this}} }
                        ]
                    }
               , {
                   ref: 'replacefile',
                   text: 'Replace file', // eBook.Accounts.Manage.Window_AddAccount,
                   iconCls: 'eBook-replace-menu-context-icon',
                   scale: 'medium',
                   iconAlign: 'top',
                   handler: this.onReplaceItemClick,
                   width: 70,
                   scope: this,
                   disabled: true
               }
               , {
                   ref: 'deletefile',
                   text: 'Delete file', // eBook.Accounts.Manage.Window_AddAccount,
                   iconCls: 'eBook-recycle-menu-context-icon ',
                   scale: 'medium',
                   iconAlign: 'top',
                   handler: this.onDeleteItemClick,
                   width: 70,
                   scope: this,
                   disabled: true
               }
               , {
                   ref: 'sentFileToDocstore',
                   text: 'Send file to DocStore', // eBook.Accounts.Manage.Window_AddAccount,
                   iconCls: 'eBook-Docstore-24-ico',
                   scale: 'medium',
                   iconAlign: 'top',
                   handler: this.sentToDocstore,
                   width: 70,
                   scope: this
               }

                    ]
            });
        }
        Ext.apply(this, {
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDD: false,
            containerScroll: true,
            //border: false,
            // auto create TreeLoader
            // dataUrl: 'get-nodes.php',
            loader: loader,
            rootVisible: false,
            root: root
            , selModel: new Ext.tree.DefaultSelectionModel({ listeners: { selectionchange: { fn: this.onNodeSelect, scope: this}} })
            , listeners: {
                dblclick: { fn: this.onNodeDblClick, scope: this }
                , contextmenu: { fn: this.onNodeContextClick, scope: this }
                , expandnode: { fn: this.expandNode, scope: this }
                , collapsenode: { fn: this.expandNode, scope: this }
            }
            , keys: [
	            {
	                key: Ext.EventObject.DELETE
	                , fn: this.onDeleteItemClick
	                , scope: this
	            }
	        ]
        });
        eBook.Repository.Tree.superclass.initComponent.apply(this, arguments);
    }
    , expandNode: function (node) {
        if (this.topToolbar) {
            if (node.attributes.DocstoreAccess) {
                this.topToolbar.sentFileToDocstore.show();
                this.topToolbar.sentFileToDocstore.disable();
            } else {
                this.topToolbar.sentFileToDocstore.hide();
            }
        }
    }
    , sentToDocstore: function (t, e) {
        var node = t.scope.getSelectionModel().selNode;
        if (node.attributes.Item.Extension == ".pdf") {
            var wn = new eBook.Docstore.Window({ documentId: node.attributes.Item.Id, clientId: node.attributes.Item.ClientId, culture: eBook.Interface.Culture, partnerGpn: node.attributes.Item.PartnerGpnAcr, uploaderGpn: eBook.User.gpn });
            wn.show();
        } else {
            Ext.Msg.show({
                title: 'Error',
                msg: 'You can only upload PDF files to docstore',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                scope: this
            });
        }


        //        var par = {
        //            ciddc: {
        //                'Id': node.attributes.Item.ClientId
        //                , 'Department': 'ACR'//eBook.Interface.center.clientMenu.files.getLatestId()
        //            }
        //        };
        //        Ext.Ajax.request({
        //            method: 'POST',
        //            url: eBook.Service.client + 'GetClientPartnerByDepartment',
        //            callback: this.sentToDocstoreCallback,
        //            scope: this,
        //            node: node,
        //            params: Ext.encode(par)
        //        });


        /*       
        Ext.Ajax.request({
        method: 'POST',
        url: eBook.Service.docstore + 'GetDocumentInfo',
        callback: this.sentToDocstoreCallback,
        scope: this,
        params: Ext.encode(par)
        });
        */
    }
    //    , sentToDocstoreCallback: function (opts, success, resp) {
    //        var node = opts.node;
    //        var partnerGpn = Ext.decode(resp.responseText).GetClientPartnerByDepartmentResult[0].gpn
    //        var wn = new eBook.Docstore.Window({ documentId: node.attributes.Item.Id, clientId: node.attributes.Item.ClientId, culture: eBook.Interface.Culture, partnerGpn: partnerGpn });
    //        wn.show();
    //    }
    , onNodeContextClick: function (nde, e) {
        if (this.readOnly) return;
        if (!Ext.isDefined(nde.attributes.Item)) return;
        if (!nde.isLeaf()) return;
        nde.select();
        if (!this.contextMenu) {
            this.contextMenu = new eBook.Repository.ContextMenu({ scope: this });
        }
        if (!nde.attributes.Item.DocstoreAccess) {
            this.contextMenu.docstoreItem.hide();
        } else {
            this.contextMenu.docstoreItem.show();
        }
        this.contextMenu.showAt(e.getXY());

    }
    , onNodeSelect: function (sm, nd) {
        if (this.readOnly) return;
        var tb = this.getTopToolbar();
        tb.deletefile.disable();
        tb.replacefile.disable();
        if (nd && nd.leaf) {
            tb.deletefile.enable();
            tb.replacefile.enable();
        }
        if (!this.contextMenu) {
            this.contextMenu = new eBook.Repository.ContextMenu({ scope: this });
        }

        if (nd.attributes.Item) {
            if (nd.attributes.Item.DocstoreAccess) {
                tb.sentFileToDocstore.show();
                tb.sentFileToDocstore.enable();
            } else {
                tb.sentFileToDocstore.hide();
            }
            if (nd.attributes.Item.DocstoreId) {
                tb.deletefile.disable();
                tb.replacefile.disable();
                tb.sentFileToDocstore.disable();
                this.contextMenu.docstoreItem.disable();
                this.contextMenu.copyItem.disable();
                this.contextMenu.moveItem.disable();
                this.contextMenu.deleteItem.disable();
            }
        } else {
            if (nd.attributes.DocstoreAccess) {
                tb.sentFileToDocstore.show();
                tb.sentFileToDocstore.disable();
            } else {
                tb.sentFileToDocstore.hide();

            }
        }

    }
    , getNewId: function (callback, scope, extraparams) {
        var cscope = scope || this;
        this.getEl().mask("Generating new id.", 'x-mask-loading');
        Ext.Ajax.request({
            method: 'POST',
            url: eBook.Service.url + 'GetNewId',
            callback: callback,
            scope: cscope,
            extras: extraparams
        });
    }
    , onMoveItem: function () {
        var sm = this.getSelectionModel();
        var n = sm.getSelectedNode();
        if (!n) return;
        var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: n.attributes.Item.Id, tpe: 'MOVE' });
        wn.show(this.parentCaller);
        wn.loadExistingFile(n);
    }
    , onCopyItem: function () {
        this.getNewId(this.onLoadCopied, this);
    }
    , onLoadCopied: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            var id = robj.GetNewIdResult;
            var sm = this.getSelectionModel();
            var n = sm.getSelectedNode();
            if (!n) return;

            var attr = Ext.decode(Ext.encode(n.attributes.originalConfig));
            attr.id = id;
            attr.text = 'Copy of ' + attr.text;
            attr.Item.FileName = 'Copy of ' + attr.Item.FileName;
            attr.Item.Id = id;
            n = new Ext.tree.AsyncTreeNode(attr);
            var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: n.attributes.Item.Id, tpe: 'COPY' });
            wn.show(this.parentCaller);
            wn.loadExistingFile(n);
        } else {
            eBook.Interface.showResponseError(resp, "New Id");
        }
    }
    , onViewItem: function () {
        var sm = this.getSelectionModel();
        var n = sm.getSelectedNode();
        if (!n) return;
        var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: n.attributes.Item.Id, tpe: 'VIEWFILE' });
        wn.show(this.parentCaller);
        wn.loadExistingFile(n);
    }
    , onReplaceItemClick: function () {
        var sm = this.getSelectionModel(),
            n = sm.getSelectedNode();

        if (!n || !n.leaf) {
            Ext.Msg.show({
                title: 'Not a file',
                msg: 'No file was selected. Please select a file first',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING,
                scope: this
            });
            return;
        } else {
            //replace window
            var wn = new eBook.Repository.SingleUploadWindow({ existingItemId: n.attributes.Item.Id });
            wn.show(this);
        }
    }
    , onDeleteItem: function () { this.onDeleteItemClick(); }
    , onDeleteItemClick: function () {
        var sm = this.getSelectionModel();
        var n = sm.getSelectedNode()
        if (!n || !n.leaf) {
            Ext.Msg.show({
                title: 'Not a file',
                msg: 'No file was selected. Please select a file first',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING,
                scope: this
            });
        } else {
            this.checkLinksBeforeDeleting(n);
        }
    }
    , checkLinksBeforeDeleting: function (node) {
        this.getEl().mask("Checking for active links on " + node.text, 'x-mask-loading');
        var par = {
            cildc: {
                'ItemId': node.attributes.Item.Id
                , 'FileId': eBook.Interface.center.clientMenu.files.getLatestId()
                , 'Culture': eBook.Interface.Culture
            }
        };
        Ext.Ajax.request({
            method: 'POST',
            url: eBook.Service.repository + 'GetItemLinks',
            callback: this.checkLinksBeforeDeletingCallback,
            scope: this,
            node: node,
            params: Ext.encode(par)
        });


    }
    , checkLinksBeforeDeletingCallback: function (opts, success, resp) {

        if (success) {
            var robj = Ext.decode(resp.responseText);
            robj = robj.GetItemLinksResult;
            if (robj.length > 0) {
                Ext.Msg.show({
                    title: 'Links detected',
                    msg: 'The file \'' + opts.node.text + '\' wich you are trying to delete has ' + robj.length + ' active links.<br/>Are you sure you want to delete this file and <u>all</u> links?',
                    buttons: Ext.Msg.YESNO,
                    fn: this.onAcceptDelete,
                    icon: Ext.MessageBox.WARNING,
                    node: opts.node,
                    scope: this
                });
            } else {
                Ext.Msg.show({
                    title: 'No Links detected',
                    msg: 'The file \'' + opts.node.text + '\' doesn\'t have any active link.<br/>Do you wish to delete this file?',
                    buttons: Ext.Msg.YESNO,
                    fn: this.onAcceptDelete,
                    icon: Ext.MessageBox.QUESTION,
                    node: opts.node,
                    scope: this
                });
            }
        } else {
            //alert('Could not retrieve file links. If persists, contact champion.');
            // means no file links
            Ext.getBody().unmask();
        }
    }
    , onAcceptDelete: function (btnid, txt, opts) {
        this.getEl().unmask();
        if (btnid == "yes") {
            this.getEl().mask("Deleting file " + opts.node.text, 'x-mask-loading');
            Ext.Ajax.request({
                method: 'POST',
                url: eBook.Service.repository + 'DeleteFile',
                callback: this.deletePerformed,
                scope: this,
                node: opts.node,
                params: Ext.encode({ cidc: { Id: opts.node.attributes.Item.Id} })
            });
        } else {
            Ext.Msg.show({
                title: 'File deletion CANCELLED',
                msg: 'You have cancelled the deletion of the file <u>\'' + opts.node.text + '\'</u>.<br/> <b>The file was not deleted</b>',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO,
                scope: this
            });
        }
    }
    , deletePerformed: function (opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            opts.node.remove(true);
        }
    }
    , onFilterClick: function (btn, state) {
        var tb = this.getTopToolbar();
        if (state) {
            tb.dateFilters.enable();
            this.onDateFilterChange();
        } else {
            tb.dateFilters.disable();
            this.getLoader().filter.ps = null;
            this.getLoader().filter.pe = null;
            this.reload();
            //tb.dateFilters.startDate.setValue();
            //tb.dateFilters.endDate.setValue();
        }

    }
    , onDateFilterChange: function () {
        var tb = this.getTopToolbar();
        var st = tb.dateFilters.startDate.getValue();
        var ed = tb.dateFilters.endDate.getValue();
        if (this.standardFilter.ps && st < this.standardFilter.ps) {
            st = this.standardFilter.ps;
            tb.dateFilters.startDate.setValue(st);
        }
        if (this.standardFilter.pe && ed > this.standardFilter.pe) {
            ed = this.standardFilter.pe;
            tb.dateFilters.endDate.setValue(ed);
        }

        this.getLoader().filter.ps = st == '' ? null : st;
        this.getLoader().filter.pe = ed == '' ? null : ed;
        this.reload();
    }
    , reload: function () {
        this.setRootNode({
            nodeType: 'async',
            text: 'Repository',
            draggable: false,
            id: 'root',
            expanded: true
        });
    }
    , reloadRepo: function () {
        var loader = new eBook.Repository.TreeLoader({
            dataUrl: eBook.Service.repository + 'GetChildren',
            requestMethod: "POST", showFiles: true, filter: this.standardFilter
        });
        loader.requestData();
        var root = {
            nodeType: 'async',
            text: 'Repository',
            draggable: false,
            id: 'root'//,
            // expanded: true
        };
        if (this.startFrom) {
            //'period'
            root = loader.createNode(eBook.Interface.GetRepoStructureById(this.startFrom.type, this.startFrom.id));
        }
    }
    , onAddFileClick: function () {
        // var wn = new eBook.Repository.LocalFileBrowser({});
        //var wn = new eBook.Window({layout:'fit',items:[new eBook.Repository.FileDetailsPanel({})]});
        var wn = new eBook.Repository.SingleUploadWindow({});
        wn.show(this);
    }
    , onNodeDblClick: function (n, e) {
        if (n.leaf) {
            var wn = new eBook.Repository.FileDetailsWindow({ repositoryItemId: n.attributes.Item.Id, readOnly: this.readOnly, readOnlyMeta: this.readOnlyMeta, clientId: this.ClientId });
            wn.show(this.parentCaller);
            wn.loadExistingFile(n);

            wn.on('beforeclose', function () {
                if (this.ownerCt.ownerCt.ownerCt.ownerCt.repos != null) {
                    this.ownerCt.ownerCt.ownerCt.ownerCt.loadRepo();
                }
            }, this);
        }
    }
});

Ext.reg('repository', eBook.Repository.Tree);



eBook.Repository.ContextMenu = Ext.extend(Ext.menu.Menu, {
    initComponent: function() {
        Ext.apply(this, {
            cls: 'eBook-repository-context-menu'
            , shadow: false
            , items: [{
                text: 'Sent to DocStore',
                iconCls: 'eBook-Docstore-16-ico',
                handler: this.scope.sentToDocstore,
                ref: 'docstoreItem',
                scope: this.scope
                //,hidden:true
            },'-', {
                text: 'Copy...',
                iconCls: 'eBook-icon-copy-16',
                handler: this.scope.onCopyItem,
                ref: 'copyItem',
                scope: this.scope
            }, {
                text: 'Move...',
                iconCls: 'eBook-icon-move-16',
                handler: this.scope.onMoveItem,
                ref: 'moveItem',
                scope: this.scope
            },'-', {
                text: 'Delete...',
                iconCls: 'eBook-icon-delete-16',
                handler: this.scope.onDeleteItem,
                ref:'deleteItem',
                scope: this.scope
            }]

            });
            eBook.Repository.ContextMenu.superclass.initComponent.apply(this, arguments);
        }
//    , showMe: function(el, accountNr, hta) {
//        this.activeEl = el;
//        this.accountNr = accountNr;
//        eBook.Accounts.ContextMenu.superclass.show.apply(this, [el, 'c']);
//        if (!hta) {
//            this.editAdjustments.disable();
//        } else {
//            this.editAdjustments.enable();
//        }
//    }
//    , showMeAt: function(el, accountNr, hta, xy) {
//        this.activeEl = el;
//        this.accountNr = accountNr;
//        eBook.Accounts.ContextMenu.superclass.showAt.apply(this, [xy]);
//        if (!hta) {
//            this.editAdjustments.disable();
//        } else {
//            this.editAdjustments.enable();
//        }
//    }
});

Ext.reg('repositorycontextmenu', eBook.Repository.ContextMenu);
