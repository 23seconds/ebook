

eBook.Statistics.DashBoard = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'anchor',
            defaults: {
                anchor: '50% 50%'
                            , style: 'float:left;margin:5px;'
            },
            frame: false,
            border: false,
            items: [{ title: 'Dossiers', xtype: 'eBook.Statistics.Files', ref: 'stat1' }
                    , { title: 'Dossiers per einde boekjaar [ALLE]', xtype: 'eBook.Statistics.EndFile', ref: 'stat2' }
                    , { title: 'Werkpapieren', xtype: 'eBook.Statistics.Worksheets', ref: 'stat3' }
                    , { title: 'Dossiers per einde boekjaar [AFGESLOTEN]', xtype: 'eBook.Statistics.EndFileClosed', ref: 'stat4'}]
        });
        eBook.Statistics.DashBoard.superclass.initComponent.apply(this, arguments);
        this.on('show', this.onStartLoad, this);
    }
    , onStartLoad: function() {
        this.getEl().mask('loading', 'x-mask-loading');
        this.loadCharts.defer(200, this);
    }
    , loadCharts: function() {
        this.stat1.load();
        this.stat2.load.defer(300,this.stat2);
        this.stat3.load.defer(500, this.stat3);
        this.stat4.load.defer(700, this.stat4);
        this.getEl().unmask();
    }
    , focusOn: function(refName) {
        var bx = this.getEl().getBox(true);
        this.stat1.hide();
        this.stat2.hide();
        this.stat3.hide();
        this.stat4.hide();
        this[refName].show();
        this[refName].setSize(bx.width, bx.height);
        this.currentFocus = refName;
    }
    , unfocus: function() {
        //this[this.currentFocus].syncSize();
        this.stat1.show();
        this.stat2.show();
        this.stat3.show();
        this.stat4.show();
        this.doLayout(false, true);
    }
});

Ext.reg('eBook.Statistics.DashBoard', eBook.Statistics.DashBoard);