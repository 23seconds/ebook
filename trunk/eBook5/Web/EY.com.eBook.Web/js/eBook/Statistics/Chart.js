
eBook.Statistics.FilesChart = Ext.extend(Ext.chart.ColumnChart, {
    initComponent: function() {
        Ext.apply(this, {
            xField: 'LabelY',
            yAxis: new Ext.chart.NumericAxis({
                displayName: 'Total1',
                labelRenderer: Ext.util.Format.numberRenderer('0,0')
            }),
            tipRenderer: function(chart, record, index, series) {
                return Ext.util.Format.number(record.data.Total1, '0,0') + ' dossiers ' + record.data.LabelY;
            },
            chartStyle: {
                padding: 10,
                animationEnabled: true,
                font: {
                    name: 'Tahoma',
                    color: 0x444444,
                    size: 11
                },
                dataTip: {
                    padding: 5,
                    border: {
                        color: 0x99bbe8,
                        size: 1
                    },
                    background: {
                        color: 0xDAE7F6,
                        alpha: .9
                    },
                    font: {
                        name: 'Tahoma',
                        color: 0x15428B,
                        size: 10,
                        bold: true
                    }
                },
                xAxis: {
                    color: 0x69aBc8,
                    majorTicks: { color: 0x69aBc8, length: 4 },
                    minorTicks: { color: 0x69aBc8, length: 2 },
                    majorGridLines: { size: 1, color: 0xeeeeee }
                },
                yAxis: {
                    color: 0x69aBc8,
                    majorTicks: { color: 0x69aBc8, length: 4 },
                    minorTicks: { color: 0x69aBc8, length: 2 },
                    majorGridLines: { size: 1, color: 0xdfe8f6 }
                }
            },
            series: [{
                type: 'column',
                displayName: 'Dossiers',
                yField: 'Total1',
                style: {
                    image: 'bar.gif',
                    mode: 'stretch',
                    color: 0x99BBE8
                }
                }]
            });
            eBook.Statistics.FilesChart.superclass.initComponent.apply(this, arguments);
        }
    });

    Ext.reg('eBook.Statistics.FilesChart', eBook.Statistics.FilesChart);


    eBook.Statistics.SheetsChart = Ext.extend(Ext.chart.ColumnChart, {
        initComponent: function() {
            Ext.apply(this, {
                xField: 'LabelY',
                yAxis: new Ext.chart.NumericAxis({
                    displayName: 'Total1',
                    labelRenderer: Ext.util.Format.numberRenderer('0,0')
                }),
                tipRenderer: function(chart, record, index, series) {
                    return Ext.util.Format.number(record.data.Total1, '0') + ' dossiers met ' + record.data.LabelY + ' valabele werkpapieren' ;
                },
                chartStyle: {
                    padding: 10,
                    animationEnabled: true,
                    font: {
                        name: 'Tahoma',
                        color: 0x444444,
                        size: 11
                    },
                    dataTip: {
                        padding: 5,
                        border: {
                            color: 0x99bbe8,
                            size: 1
                        },
                        background: {
                            color: 0xDAE7F6,
                            alpha: .9
                        },
                        font: {
                            name: 'Tahoma',
                            color: 0x15428B,
                            size: 10,
                            bold: true
                        }
                    },
                    xAxis: {
                        color: 0x69aBc8,
                        majorTicks: { color: 0x69aBc8, length: 4 },
                        minorTicks: { color: 0x69aBc8, length: 2 },
                        majorGridLines: { size: 1, color: 0xeeeeee }
                    },
                    yAxis: {
                        color: 0x69aBc8,
                        majorTicks: { color: 0x69aBc8, length: 4 },
                        minorTicks: { color: 0x69aBc8, length: 2 },
                        majorGridLines: { size: 1, color: 0xdfe8f6 }
                    }
                },
                series: [{
                    type: 'column',
                    displayName: 'Werkpapieren',
                    yField: 'Total1',
                    style: {
                        image: 'bar.gif',
                        mode: 'stretch',
                        color: 0x99BBE8
                    }
}]
                });
                eBook.Statistics.SheetsChart.superclass.initComponent.apply(this, arguments);
            }
        });

        Ext.reg('eBook.Statistics.SheetsChart', eBook.Statistics.SheetsChart);

        eBook.Statistics.EndChart = Ext.extend(Ext.chart.ColumnChart, {
            initComponent: function() {
                Ext.apply(this, {
                    xField: 'LabelY',
                    yAxis: new Ext.chart.NumericAxis({
                        displayName: 'Total1',
                        labelRenderer: Ext.util.Format.numberRenderer('0,0')
                    }),
                    tipRenderer: function(chart, record, index, series) {
                        if (series.yField == 'Total1') {
                            return Ext.util.Format.number(record.data.Total1, '0,0') + ' dossiers ' + record.data.LabelY;
                        } else {
                            return Ext.util.Format.number(record.data.Total2, '0,0') + ' dossiers '  + record.data.MonthName + '/' + (record.data.Year-1);
                        }
                    },
                    chartStyle: {
                        padding: 10,
                        animationEnabled: true,
                        font: {
                            name: 'Tahoma',
                            color: 0x444444,
                            size: 11
                        },
                        dataTip: {
                            padding: 5,
                            border: {
                                color: 0x99bbe8,
                                size: 1
                            },
                            background: {
                                color: 0xDAE7F6,
                                alpha: .9
                            },
                            font: {
                                name: 'Tahoma',
                                color: 0x15428B,
                                size: 10,
                                bold: true
                            }
                        },
                        xAxis: {
                            color: 0x69aBc8,
                            majorTicks: { color: 0x69aBc8, length: 4 },
                            minorTicks: { color: 0x69aBc8, length: 2 },
                            majorGridLines: { size: 1, color: 0xeeeeee }
                        },
                        yAxis: {
                            color: 0x69aBc8,
                            majorTicks: { color: 0x69aBc8, length: 4 },
                            minorTicks: { color: 0x69aBc8, length: 2 },
                            majorGridLines: { size: 1, color: 0xdfe8f6 }
                        }
                    },
                    series: [{
                        type: 'column',
                        displayName: ' ',
                        yField: 'Total2',
                        style: {
                            color: 0x15428B
                        }
                    }, {
                    type: 'column',
                        displayName: 'LabelY',
                        yField: 'Total1',
                        style: {
                            color: 0x99BBE8
                        }
                    }]
                    });
                    eBook.Statistics.EndChart.superclass.initComponent.apply(this, arguments);
                }
            });

            Ext.reg('eBook.Statistics.EndChart', eBook.Statistics.EndChart);