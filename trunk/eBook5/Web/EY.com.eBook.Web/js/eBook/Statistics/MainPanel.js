
eBook.Statistics.generateData = function() {
    var data = [];
    for (var i = 0; i < 12; ++i) {
        data.push({ name: Date.monthNames[i], visits: (Math.floor(Math.random() * 11) + 1) * 50, views: (Math.floor(Math.random() * 11) + 1) * 100 });
    }
    return data;
};

eBook.Statistics.MainPanel = Ext.extend(Ext.Panel, {
    focused: false
    , hideExport:false
    , initComponent: function() {
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , tbar: [
                    '->', { tooltip: 'load', iconCls: 'eBook-icon-arrowrefresh-16', handler: this.load, scope: this }
                    , { tooltip: 'Excel export', iconCls: 'eBook-icon-exportexcel-16', handler: this.exportExcel, hidden: this.hideExport, scope: this }
                    , { tooltip: 'focus', iconCls: 'eBook-icon-maximize-16', enableToggle: true, toggleHandler: this.focusMe, scope: this}]
        });

        eBook.Statistics.MainPanel.superclass.initComponent.apply(this, arguments);
        this.on('afterlayout', this.fixToolbar, this);
    }
    , fixToolbar: function() {

        var tb = this.getTopToolbar();
        if (!tb) return;
        if (this.refOwner.isVisible()) {
            this.configToolbar();
        }
    }
    , configToolbar: function() { }
    , load: function() { }
    , exportExcel: function(btn, ste) {
        this.getEl().mask('loading', 'x-mask-loading');
        
        Ext.Ajax.request({
            url: eBook.Service.url + 'ExportStatistic'
                , method: 'POST'
                , params: Ext.encode({ csdc: this.getStatParams() })
                , callback: this.onExportExcelComplete
                , scope: this
        });
    }
    , onExportExcelComplete: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open("renderedPdfs/" + robj.ExportStatisticResult);
        } else {
            eBook.Interface.showResponseError(resp);
        }
    }
    , focusMe: function(btn, ste) {
        if (!ste) {
            this.setSize(this.prevSize.width, this.prevSize.height);
            this.refOwner.unfocus();
            this.focused = false;
        } else {
            this.prevSize = this.getBox();
            this.focused = true;
            this.refOwner.focusOn(this.initialConfig.ref);
        }
    }
    , syncSize: function() {
    }
    , startLoad: function() {
        this.getEl().mask('loading', 'x-mask-loading');
    }
    , endLoad: function() {
        this.getEl().unmask();
    }
});


eBook.Statistics.Files = Ext.extend(eBook.Statistics.MainPanel, {
    focused: false
    , initComponent: function() {
        this.store = new eBook.data.JsonStore({
            selectAction: 'GetStatistics'
                , id: this.id + '-store'
                , autoDestroy: true
                , criteriaParameter: 'csdc'
                , fields: eBook.data.RecordTypes.Statistics
        });
        this.store.on('beforeload', this.startLoad, this);
        this.store.on('load', this.endLoad, this);

        Ext.apply(this, {
            items: [{ xtype: 'eBook.Statistics.FilesChart', store: this.store, title: 'Chart', ref: 'chart' }
                   , { xtype: 'eBook.Statistics.Grid', store: this.store, ref: 'grid'}]
        });

        eBook.Statistics.Files.superclass.initComponent.apply(this, arguments);
    }
    , configToolbar: function() {

        var tb = this.getTopToolbar();
        if (!tb.types) {
            this.typeSelect = new Ext.form.ComboBox({
                store: [['filescreated', 'Datum creatie'], ['filesupdated', 'Datum laatst aangepast'], ['filesclosed', 'Datum afgesloten']]
                        , typeAhead: false
                        , mode: 'local'
                        , selectOnFocus: true
                        , autoSelect: true
                        , editable: false
                        , forceSelection: true
                        , triggerAction: 'all'
                        , nullable: false
                        , width: 100
                        , listWidth: 200
                        , value: 'filescreated' //eBook.User.defaultOfficeObj ? eBook.User.defaultOfficeObj.id : null
                        , iconCls: 'no-icon'
                        , getListParent: function() {
                            return this.el.up('.x-menu');
                        }
                        , ref: 'types'
            });
            tb.insert(0, this.typeSelect);
            this.officeSelect = new Ext.form.ComboBox({
                store: new eBook.data.JsonStore({
                    selectAction: 'GetOffices'
                    , autoDestroy: true
                    , autoLoad: true
                    , criteriaParameter: ''
                    , fields: eBook.data.RecordTypes.Offices
                    , params: {}
                })

                        , typeAhead: false
                        , mode: 'local'
                        , selectOnFocus: true
                        , autoSelect: true
                        , editable: false
                        , forceSelection: true
                        , triggerAction: 'all'
                        , nullable: false
                        , width: 100
                        , listWidth: 200
                        , displayField: 'Name'
                        , valueField: 'Id'
                        , value: ""
                        , iconCls: 'no-icon'
                        , getListParent: function() {
                            return this.el.up('.x-menu');
                        }
                        , ref: 'offices'
            });
            tb.insert(1, this.officeSelect);

            this.startDate = new Ext.form.DateField({ allowBlank: false
                    , format: 'd/m/Y', value: new Date().add(Date.MONTH, -13), ref: 'start'
            });
            tb.insert(2, this.startDate);
            this.endDate = new Ext.form.DateField({ allowBlank: false
                    , format: 'd/m/Y', value: new Date(), ref: 'end'
            });
            tb.insert(3, this.endDate);
            tb.doLayout();
        }
    }
    , load: function() {
        var tb = this.getTopToolbar();
        this.store.load({
            params: {
                StatisticType: tb.types.getValue(),
                Culture: eBook.Interface.Culture,
                Start: this.startDate.getValue(),
                End: this.endDate.getValue(),
                Office: tb.offices.getValue()
            }
        });
    }
    , getStatParams: function() {
        var tb = this.getTopToolbar();
        return {
            StatisticType: tb.types.getValue(),
            Culture: eBook.Interface.Culture,
            Start: this.startDate.getValue(),
            End: this.endDate.getValue(),
            Office: tb.offices.getValue()
        };
    }
});

eBook.Statistics.Worksheets = Ext.extend(eBook.Statistics.MainPanel, {
    focused: false
    ,hideExport:true
    , initComponent: function() {
        this.store = new eBook.data.JsonStore({
            selectAction: 'GetStatisticsSimple'
                , id: this.id + '-store'
                , autoDestroy: true
                , criteriaParameter: 'csdc'
                , fields: eBook.data.RecordTypes.StatisticsSimple
        });
        this.store.on('beforeload', this.startLoad, this);
        this.store.on('load', this.endLoad, this);

        Ext.apply(this, {
        items: [{ xtype: 'eBook.Statistics.SheetsChart', store: this.store, ref: 'chart' }
                   , { xtype: 'eBook.Statistics.Grid', store: this.store, ref: 'grid'}]
        });

        eBook.Statistics.Worksheets.superclass.initComponent.apply(this, arguments);
    }
    , configToolbar: function() {

        var tb = this.getTopToolbar();
        if (!tb.offices) {
            
            this.officeSelect = new Ext.form.ComboBox({
                        store: new eBook.data.JsonStore({
                            selectAction: 'GetOffices'
                                , autoDestroy: true
                                , autoLoad: true
                                , criteriaParameter: ''
                                , fields: eBook.data.RecordTypes.Offices
                                , params: {}
                        })
                        , typeAhead: false
                        , mode: 'local'
                        , selectOnFocus: true
                        , autoSelect: true
                        , editable: false
                        , forceSelection: true
                        , triggerAction: 'all'
                        , nullable: false
                        , width: 100
                        , listWidth: 200
                        , displayField: 'Name'
                        , valueField: 'Id'
                        , value: ""//eBook.User.defaultOfficeObj ? eBook.User.defaultOfficeObj.id : ""
                        , iconCls: 'no-icon'
                        , getListParent: function() {
                            return this.el.up('.x-menu');
                        }
                        , ref: 'offices'
            });
            tb.insert(0, this.officeSelect);

            this.startDate = new Ext.form.DateField({ allowBlank: false
                    , format: 'd/m/Y', value: new Date().add(Date.MONTH, -13), ref: 'start'
            });
            tb.insert(1, this.startDate);
            this.endDate = new Ext.form.DateField({ allowBlank: false
                    , format: 'd/m/Y', value: new Date(), ref: 'end'
            });
            tb.insert(2, this.endDate);
            tb.doLayout();
        }
    }
    , load: function() {
        var tb = this.getTopToolbar();
        this.store.load({
            params: {
                StatisticType: 'worksheets',
                Culture: eBook.Interface.Culture,
                Start: this.startDate.getValue(),
                End: this.endDate.getValue(),
                Office: tb.offices.getValue()
            }
        });
    }
});

eBook.Statistics.EndFile = Ext.extend(eBook.Statistics.MainPanel, {
    focused: false
    , initComponent: function() {
        this.store = new eBook.data.JsonStore({
            selectAction: 'GetStatistics'
                , id: this.id + '-store'
                , autoDestroy: true
                , criteriaParameter: 'csdc'
                , fields: eBook.data.RecordTypes.Statistics
        });
        this.store.on('beforeload', this.startLoad, this);
        this.store.on('load', this.endLoad, this);

        Ext.apply(this, {
        items: [{ xtype: 'eBook.Statistics.EndChart', store: this.store, ref: 'chart' }
                   , { xtype: 'eBook.Statistics.Grid', store: this.store, ref: 'grid'}]
        });

        eBook.Statistics.Worksheets.superclass.initComponent.apply(this, arguments);
    }
    , configToolbar: function() {

        var tb = this.getTopToolbar();
        if (!tb.offices) {

            this.officeSelect = new Ext.form.ComboBox({
                        store: new eBook.data.JsonStore({
                            selectAction: 'GetOffices'
                                , autoDestroy: true
                                , autoLoad: true
                                , criteriaParameter: ''
                                , fields: eBook.data.RecordTypes.Offices
                                , params: {}
                        })
                        , typeAhead: false
                        , mode: 'local'
                        , selectOnFocus: true
                        , autoSelect: true
                        , editable: false
                        , forceSelection: true
                        , triggerAction: 'all'
                        , nullable: false
                        , width: 100
                        , listWidth: 200
                        , displayField: 'Name'
                        , valueField: 'Id'
                        , value: "" //eBook.User.defaultOfficeObj ? eBook.User.defaultOfficeObj.id : ""
                        , iconCls: 'no-icon'
                        , getListParent: function() {
                            return this.el.up('.x-menu');
                        }
                        , ref: 'offices'
            });
            tb.insert(0, this.officeSelect);

            this.startDate = new Ext.form.DateField({ allowBlank: false
                    , format: 'd/m/Y', value: new Date().add(Date.MONTH, -6), ref: 'start'
            });
            tb.insert(1, this.startDate);
            this.endDate = new Ext.form.DateField({ allowBlank: false
                    , format: 'd/m/Y', value: new Date().add(Date.MONTH, +6), ref: 'end'
            });
            tb.insert(2, this.endDate);
            tb.doLayout();
        }
    }
    , load: function() {
        var tb = this.getTopToolbar();
        this.store.load({
            params: {
                StatisticType: 'filesend',
                Culture: eBook.Interface.Culture,
                Start: this.startDate.getValue(),
                End: this.endDate.getValue(),
                Office: tb.offices.getValue()
            }
        });
    }
    , getStatParams: function() {
        var tb = this.getTopToolbar();
        return {
            StatisticType: 'filesend',
            Culture: eBook.Interface.Culture,
            Start: this.startDate.getValue(),
            End: this.endDate.getValue(),
            Office: tb.offices.getValue()
        };
    }
});


eBook.Statistics.EndFileClosed = Ext.extend(eBook.Statistics.MainPanel, {
    focused: false
    , initComponent: function() {
        this.store = new eBook.data.JsonStore({
            selectAction: 'GetStatistics'
                , id: this.id + '-store'
                , autoDestroy: true
                , criteriaParameter: 'csdc'
                , fields: eBook.data.RecordTypes.Statistics
        });
        this.store.on('beforeload', this.startLoad, this);
        this.store.on('load', this.endLoad, this);

        Ext.apply(this, {
            items: [{ xtype: 'eBook.Statistics.EndChart', store: this.store, ref: 'chart' }
                   , { xtype: 'eBook.Statistics.Grid', store: this.store, ref: 'grid'}]
        });

        eBook.Statistics.Worksheets.superclass.initComponent.apply(this, arguments);
    }
    , configToolbar: function() {

        var tb = this.getTopToolbar();
        if (!tb.offices) {

            this.officeSelect = new Ext.form.ComboBox({
                        store: new eBook.data.JsonStore({
                            selectAction: 'GetOffices'
                                , autoDestroy: true
                                , autoLoad: true
                                , criteriaParameter: ''
                                , fields: eBook.data.RecordTypes.Offices
                                , params: {}
                        })
                        , typeAhead: false
                        , mode: 'local'
                        , selectOnFocus: true
                        , autoSelect: true
                        , editable: false
                        , forceSelection: true
                        , triggerAction: 'all'
                        , nullable: false
                        , width: 100
                        , listWidth: 200
                        , displayField: 'Name'
                        , valueField: 'Id'
                        , value: "" //eBook.User.defaultOfficeObj ? eBook.User.defaultOfficeObj.id : ""
                        , iconCls: 'no-icon'
                        , getListParent: function() {
                            return this.el.up('.x-menu');
                        }
                        , ref: 'offices'
            });
            tb.insert(0, this.officeSelect);

            this.startDate = new Ext.form.DateField({ allowBlank: false
                    , format: 'd/m/Y', value: new Date().add(Date.MONTH, -6), ref: 'start'
            });
            tb.insert(1, this.startDate);
            this.endDate = new Ext.form.DateField({ allowBlank: false
                    , format: 'd/m/Y', value: new Date().add(Date.MONTH, +6), ref: 'end'
            });
            tb.insert(2, this.endDate);
            tb.doLayout();
        }
    }
    , load: function() {
        var tb = this.getTopToolbar();
        this.store.load({
            params: {
                StatisticType: 'filesendclosed',
                Culture: eBook.Interface.Culture,
                Start: this.startDate.getValue(),
                End: this.endDate.getValue(),
                Office: tb.offices.getValue()
            }
        });
    }
    , getStatParams: function() {
        var tb = this.getTopToolbar();
        return {
            StatisticType: 'filesendclosed',
            Culture: eBook.Interface.Culture,
            Start: this.startDate.getValue(),
            End: this.endDate.getValue(),
            Office: tb.offices.getValue()
        };
    }
});


Ext.reg('eBook.Statistics.MainPanel', eBook.Statistics.MainPanel);
Ext.reg('eBook.Statistics.Files', eBook.Statistics.Files);
Ext.reg('eBook.Statistics.Worksheets', eBook.Statistics.Worksheets);
Ext.reg('eBook.Statistics.EndFile', eBook.Statistics.EndFile);
Ext.reg('eBook.Statistics.EndFileClosed', eBook.Statistics.EndFileClosed);




                   