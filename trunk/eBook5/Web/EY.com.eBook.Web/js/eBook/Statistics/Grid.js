

eBook.Statistics.Grid = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        Ext.apply(this, {
            columns: [{ header: ' ', dataIndex: 'LabelY' }
                    , { header: 'Data 1', dataIndex: 'Total1' }
                    , { header: 'Data 2', dataIndex: 'Total2'}]
        });
        eBook.Statistics.Grid.superclass.initComponent.apply(this, arguments);
    }
});        
        
Ext.reg('eBook.Statistics.Grid',eBook.Statistics.Grid);