
eBook.Language.LanguagesList = [
    ['nl-BE', eBook.Language_Dutch]
    , ['fr-FR', eBook.Language_French]
    , ['de-DE', eBook.Language_German]
    , ['en-US', eBook.Language_English]
];

eBook.Repository.CategoriesList = [
    ['perm', eBook.RepositoryCategory_Permanent]
    , ['period', eBook.RepositoryCategory_Period]
];


eBook.data.RecordTypes = {
    SimpleList: [{ name: 'id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'desc', mapping: 'desc', type: Ext.data.Types.STRING}]
    , GlobalListItem: [{ name: 'id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'nl', mapping: 'nl', type: Ext.data.Types.STRING }
            , { name: 'fr', mapping: 'fr', type: Ext.data.Types.STRING }
            , { name: 'en', mapping: 'en', type: Ext.data.Types.STRING }
            , { name: 'value', mapping: 'value', type: Ext.data.Types.NUMBER}]
    , FodList: [
            { name: 'id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'name', mapping: 'd', type: Ext.data.Types.STRING }
            , { name: 'listid', mapping: 'li', type: Ext.data.Types.STRING }
            , { name: 'meta', mapping: 'm', type: Ext.data.Types.STRING}]
    , RecentFiles: [
        { name: 'file', mapping: 'f', type: Ext.data.Types.AUTO }
        , { name: 'client', mapping: 'c', type: Ext.data.Types.AUTO }
        , { name: 'clientName', mapping: 'cn', type: Ext.data.Types.STRING }
    //, { name: 'assessment', mapping: 'fas', type: Ext.data.Types.INT }
    //, { name: 'start', mapping: 'fsd', type: Ext.data.Types.WCFDATE }
    //, { name: 'end', mapping: 'fed', type: Ext.data.Types.WCFDATE }
        , { name: 'lastaccessed', mapping: 'la', type: Ext.data.Types.WCFDATE }
    ]
    , Form: [
        { name: 'field', mapping: 'f', type: Ext.data.Types.STRING  }
        , { name: 'value', mapping: 'v', type: Ext.data.Types.STRING  }
    ],
    QueryFiles: [
        { name: 'field', mapping: 'f', type: Ext.data.Types.STRING  }
        , { name: 'value', mapping: 'v', type: Ext.data.Types.STRING  }
    ]
    , CreateFileActions: [
                        { name: 'id', mapping: 'id', type: Ext.data.Types.STRING }
                        , { name: 'action', mapping: 'action', type: Ext.data.Types.STRING }
                        , { name: 'status', mapping: 'status', type: Ext.data.Types.STRING }
                        , { name: 'container', mapping: 'container', type: Ext.data.Types.STRING }
                        , { name: 'errorMsg', mapping: 'status', type: Ext.data.Types.STRING }
                        , { name: 'text', mapping: 'text', type: Ext.data.Types.STRING }
                    ]
    , FileType: [
                { name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'Order', mapping: 'o', type: Ext.data.Types.INT }
                ]

    , FileHistory: [
                { name: 'clientId', type: Ext.data.Types.STRING }
                , { name: 'clientGfis', type: Ext.data.Types.STRING }
                , { name: 'clientName', type: Ext.data.Types.STRING }
                , { name: 'fileId', type: Ext.data.Types.STRING }
                , { name: 'fileName', type: Ext.data.Types.STRING }
                , { name: 'startDate', type: Ext.data.Types.DATE }
                , { name: 'endDate', type: Ext.data.Types.DATE }
                , { name: 'lastaccess', type: Ext.data.Types.DATE}]
    , ClientBase: [
                { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'GFISCode', mapping: 'gc', type: Ext.data.Types.STRING }
                , { name: 'Address', mapping: 'a', type: Ext.data.Types.STRING }
                , { name: 'ZipCode', mapping: 'zip', type: Ext.data.Types.STRING }
                , { name: 'City', mapping: 'ci', type: Ext.data.Types.STRING }
                , { name: 'Country', mapping: 'co', type: Ext.data.Types.STRING }
                , { name: 'VatNumber', mapping: 'vnr', type: Ext.data.Types.STRING }
                , { name: 'EnterpriseNumber', mapping: 'enr', type: Ext.data.Types.STRING }
                , { name: 'ProAccServer', mapping: 'pas', type: Ext.data.Types.STRING }
                , { name: 'ProAccDatabase', mapping: 'pad', type: Ext.data.Types.STRING }
                , { name: 'ProAccLinkUpdated', mapping: 'palu', type: Ext.data.Types.WCFDATE }
                , { name: 'Shared', mapping: 'sh', type: Ext.data.Types.BOOLEAN }
                , { name: 'bni', mapping: 'bni', type: Ext.data.Types.BOOLEAN }
                ]
    , FileInfo: [
                { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
                , { name: 'ClientId', mapping: 'cid', type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                , { name: 'StartDate', mapping: 'sd', type: Ext.data.Types.WCFDATE }
                , { name: 'EndDate', mapping: 'ed', type: Ext.data.Types.WCFDATE }
                , { name: 'PreviousStartDate', mapping: 'psd', type: Ext.data.Types.WCFDATE }
                , { name: 'PreviousEndDate', mapping: 'ped', type: Ext.data.Types.WCFDATE }
                , { name: 'AssessmentYear', mapping: 'ay', type: Ext.data.Types.INT }
                , { name: 'Display', mapping: 'dsp', type: Ext.data.Types.STRING }
                , { name: 'Closed', mapping: 'cl', type: Ext.data.Types.BOOLEAN }
                , { name: 'Errors', mapping: 'ers', type: Ext.data.Types.INT }
                , { name: 'Warnings', mapping: 'wrs', type: Ext.data.Types.INT }
                , { name: 'ImportDate', mapping: 'imd', type: Ext.data.Types.WCFDATE }
                , { name: 'ImportType', mapping: 'imt', type: Ext.data.Types.STRING }
                , { name: 'NumbersLocked', mapping: 'nl', type: Ext.data.Types.BOOL }
                , { name: 'Services', mapping: 'svc', type: Ext.data.Types.AUTO }
                ]
    , DistinctCounterFiles: [
                { name: 'StartDate', mapping: 'sd', type: Ext.data.Types.WCFDATE }
                , { name: 'EndDate', mapping: 'ed', type: Ext.data.Types.WCFDATE }
                , { name: 'Counter', mapping: 'cnt', type: Ext.data.Types.INT }
                , { name: 'ClientName', mapping: 'cn', type: Ext.data.Types.STRING }
                , { name: 'ClientId', mapping: 'cid', type: Ext.data.Types.STRING }
                , { name: 'FileId', mapping: 'fid', type: Ext.data.Types.STRING }
    ]
    , FileBase: [
                { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
                , { name: 'ClientId', mapping: 'cid', type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                , { name: 'StartDate', mapping: 'sd', type: Ext.data.Types.WCFDATE }
                , { name: 'EndDate', mapping: 'ed', type: Ext.data.Types.WCFDATE }
                , { name: 'AssessmentYear', mapping: 'ay', type: Ext.data.Types.INT }
    //  , { name: 'LastUpdated', mapping: 'lud', type: Ext.data.Types.WCFDATE }
    // , { name: 'LastUpdatedBy', mapping: 'lub', type: Ext.data.Types.STRING }
                , { name: 'Closed', mapping: 'cl', type: Ext.data.Types.BOOLEAN }
                , { name: 'NumbersLocked', mapping: 'nl', type: Ext.data.Types.BOOLEAN }
    // , { name: 'AccountLength', mapping: 'al', type: Ext.data.Types.INT }
                , { name: 'ImportDate', mapping: 'imd', type: Ext.data.Types.WCFDATE }
                , { name: 'ImportType', mapping: 'imt', type: Ext.data.Types.STRING }
                , { name: 'MarkDelete', mapping: 'md', type: Ext.data.Types.BOOLEAN }
                , { name: 'Deleted', mapping: 'd', type: Ext.data.Types.BOOLEAN }
                , { name: 'Display', mapping: 'dsp', type: Ext.data.Types.STRING }
                , { name: 'PreviousFileId', mapping: 'pfid', type: Ext.data.Types.STRING }
                , { name: 'PreviousStartDate', mapping: 'psd', type: Ext.data.Types.WCFDATE }
                , { name: 'PreviousEndDate', mapping: 'ped', type: Ext.data.Types.WCFDATE }
                , { name: 'Errors', mapping: 'ers', type: Ext.data.Types.INT }
                , { name: 'Warnings', mapping: 'wrs', type: Ext.data.Types.INT }
                ]
    , BusinessRelation: [
                { name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
                , { name: 'ImportedId', mapping: 'iid', type: Ext.data.Types.STRING }
                , { name: 'ClientId', mapping: 'cid', type: Ext.data.Types.STRING }
                , { name: 'LastName', mapping: 'ln', type: Ext.data.Types.STRING }
                , { name: 'FirstName', mapping: 'fn', type: Ext.data.Types.STRING }
                , { name: 'Address', mapping: 'ad', type: Ext.data.Types.STRING }
                , { name: 'Zip', mapping: 'zc', type: Ext.data.Types.STRING }
                , { name: 'City', mapping: 'ci', type: Ext.data.Types.STRING }
                , { name: 'Country', mapping: 'co', type: Ext.data.Types.STRING }
                , { name: 'VatNumber', mapping: 'vt', type: Ext.data.Types.STRING }
                , { name: 'EnterpriseNumber', mapping: 'cn', type: Ext.data.Types.STRING }
                , { name: 'RelationType', mapping: 'bt', type: Ext.data.Types.STRING }
                ]
    , BusinessRelationList: [
                { name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                ]
    , YearList: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.INTEGER}]
    , MonthList: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING}]
    , MetaLookup: [
                 { name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
                  , { name: 'Meta', mapping: 'm', type: Ext.data.Types.STRING }
                  , { name: 'Value', mapping: 'v', type: Ext.data.Types.STRING }
                  , { name: 'Seq', mapping: 's', type: Ext.data.Types.INTEGER }
                  , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                  , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                ]
    , RepositoryItem: [
        { name: 'Id', mapping: 'Id', type: Ext.data.Types.STRING }
        , { name: 'ClientId', mapping: 'ClientId', type: Ext.data.Types.STRING }
        , { name: 'FileName', mapping: 'FileName', type: Ext.data.Types.STRING }
        , { name: 'ContentType', mapping: 'ContentType', type: Ext.data.Types.STRING }
        , { name: 'DocstoreId', mapping: 'DocstoreId', type: Ext.data.Types.STRING }
        , { name: 'PhysicalFileId', mapping: 'PhysicalFileId', type: Ext.data.Types.STRING }
        , { name: 'PeriodStart', mapping: 'PeriodStart', type: Ext.data.Types.WCFDATE }
        , { name: 'PeriodEnd', mapping: 'PeriodEnd', type: Ext.data.Types.WCFDATE  }
        , { name: 'Status', mapping: 'Status', type: Ext.data.Types.STRING }
        , { name: 'StatusText', mapping: 'StatusText', type: Ext.data.Types.STRING }
        , { name: 'Extension', mapping: 'Extension', type: Ext.data.Types.STRING }
        , { name: 'IconCls', mapping: 'IconCls', type: Ext.data.Types.STRING }
    ]
    , RepositoryItemLinks: [
                 { name: 'ClientId', mapping: 'ClientId', type: Ext.data.Types.STRING }
                  , { name: 'PhysicalFileId', mapping: 'PhysicalFileId', type: Ext.data.Types.STRING }
                  , { name: 'ItemId', mapping: 'ItemId', type: Ext.data.Types.STRING }
                  , { name: 'ConnectionType', mapping: 'ConnectionType', type: Ext.data.Types.STRING }
                  , { name: 'ConnectionGuid', mapping: 'ConnectionGuid', type: Ext.data.Types.STRING }
                  , { name: 'ConnectionAccount', mapping: 'ConnectionAccount', type: Ext.data.Types.STRING }
                  , { name: 'ConnectionDetailedPath', mapping: 'ConnectionDetailedPath', type: Ext.data.Types.STRING }
                  , { name: 'Description', mapping: 'Description', type: Ext.data.Types.STRING }

                ]
    , Account: [
         { name: 'Id', mapping: 'inr', index: true, type: Ext.data.Types.STRING }
        , { name: 'nr', mapping: 'vnr', type: Ext.data.Types.STRING }
        , { name: 'description', mapping: 'd', type: Ext.data.Types.STRING }
        , { name: 'taxAdjustment', mapping: 'txd', type: Ext.data.Types.STRING }
        , { name: 'taxParent', mapping: 'txp', type: Ext.data.Types.STRING }
        , { name: 'existedLastYear', mapping: 'ely', type: Ext.data.Types.BOOLEAN }
        , { name: 'hasTaxAdjustments', mapping: 'hta', type: Ext.data.Types.BOOLEAN}]
    , AccountsMappingGrid: [
                { name: 'Id', mapping: 'inr', index: true, type: Ext.data.Types.STRING }
                , { name: 'nr', mapping: 'vnr', type: Ext.data.Types.STRING }
                , { name: 'description', mapping: 'd', type: Ext.data.Types.STRING }
                , { name: 'saldo', mapping: 'ta', type: Ext.data.Types.NUMBER }
                , { name: 'saldoPrevious', mapping: 'tap', type: Ext.data.Types.NUMBER }
                , { name: 'taxAdjustment', mapping: 'txd', type: Ext.data.Types.STRING }
                , { name: 'taxParent', mapping: 'txp', type: Ext.data.Types.STRING }
                , { name: 'existedLastYear', mapping: 'ely', type: Ext.data.Types.BOOLEAN }
                , { name: 'hasTaxAdjustments', mapping: 'hta', type: Ext.data.Types.BOOLEAN }
                , { name: 'BelastingenApp', meta: '10eea163-5e43-4501-9f1c-35ae838be615', mapping: eBook.getMapping('10eea163-5e43-4501-9f1c-35ae838be615', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'BelastingenApp_name', meta: '10eea163-5e43-4501-9f1c-35ae838be615', mapping: eBook.getMapping('10eea163-5e43-4501-9f1c-35ae838be615', 'NAME'), type: Ext.data.Types.STRING }
                , { name: 'PersoneelApp', meta: 'bd2374b4-42c3-4c09-96b2-d8debe3fa1b1', mapping: eBook.getMapping('bd2374b4-42c3-4c09-96b2-d8debe3fa1b1', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'PersoneelApp_name', meta: 'bd2374b4-42c3-4c09-96b2-d8debe3fa1b1', mapping: eBook.getMapping('bd2374b4-42c3-4c09-96b2-d8debe3fa1b1', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'BTWApp', meta: '2ca965ed-eacb-43ff-9a91-cfe7ef09eef9', mapping: eBook.getMapping('2ca965ed-eacb-43ff-9a91-cfe7ef09eef9', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'BTWApp_name', meta: '2ca965ed-eacb-43ff-9a91-cfe7ef09eef9', mapping: eBook.getMapping('2ca965ed-eacb-43ff-9a91-cfe7ef09eef9', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'VoordelenApp', meta: '67855d3d-4e11-4c7b-96a3-e6602ddaaa6f', mapping: eBook.getMapping('67855d3d-4e11-4c7b-96a3-e6602ddaaa6f', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'VoordelenApp_name', meta: '67855d3d-4e11-4c7b-96a3-e6602ddaaa6f', mapping: eBook.getMapping('67855d3d-4e11-4c7b-96a3-e6602ddaaa6f', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'ErelonenHuurApp', meta: '3e9bb601-ea23-4072-bd49-e5dc61a6b5a8', mapping: eBook.getMapping('3e9bb601-ea23-4072-bd49-e5dc61a6b5a8', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'ErelonenHuurApp_name', meta: '3e9bb601-ea23-4072-bd49-e5dc61a6b5a8', mapping: eBook.getMapping('3e9bb601-ea23-4072-bd49-e5dc61a6b5a8', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'VoorschottenApp', meta: '997a80e4-ea39-42c7-95d5-cdf8322c4c11', mapping: eBook.getMapping('997a80e4-ea39-42c7-95d5-cdf8322c4c11', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'VoorschottenApp_name', meta: '997a80e4-ea39-42c7-95d5-cdf8322c4c11', mapping: eBook.getMapping('997a80e4-ea39-42c7-95d5-cdf8322c4c11', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'RVIntrestenApp', meta: '3b260f00-e6e8-4c6b-83ea-69457799a068', mapping: eBook.getMapping('3b260f00-e6e8-4c6b-83ea-69457799a068', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'RVIntrestenApp_name', meta: '3b260f00-e6e8-4c6b-83ea-69457799a068', mapping: eBook.getMapping('3b260f00-e6e8-4c6b-83ea-69457799a068', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'BelasteReservesApp', meta: '7df44f01-2f13-4cd5-ada1-7ad8003a85c1', mapping: eBook.getMapping('7df44f01-2f13-4cd5-ada1-7ad8003a85c1', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'BelasteReservesApp_name', meta: '7df44f01-2f13-4cd5-ada1-7ad8003a85c1', mapping: eBook.getMapping('7df44f01-2f13-4cd5-ada1-7ad8003a85c1', 'NAME'), type: Ext.data.Types.STRING }

                , { name: 'DBIApp', meta: '77d8dbbc-feec-478e-9e11-83838e346caf', mapping: eBook.getMapping('77d8dbbc-feec-478e-9e11-83838e346caf', 'VALUE'), type: Ext.data.Types.STRING }
                , { name: 'DBIApp_name', meta: '77d8dbbc-feec-478e-9e11-83838e346caf', mapping: eBook.getMapping('77d8dbbc-feec-478e-9e11-83838e346caf', 'NAME'), type: Ext.data.Types.STRING }
            ]
   , MappingItem: [
                { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
                , { name: 'meta', mapping: 'm', type: Ext.data.Types.STRING }
                , { name: 'value', mapping: 'v', type: Ext.data.Types.STRING }
                , { name: 'assessmentYear', mapping: 'ay', type: Ext.data.Types.STRING }
                , { name: 'name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'groupname', mapping: 'gn', type: Ext.data.Types.STRING }
            ]
    , AccountDescriptions: [
                 { name: 'AccountId', mapping: 'inr', index: true, type: Ext.data.Types.STRING }
                 , { name: 'culture', mapping: 'c', index: true, type: Ext.data.Types.STRING }
                 , { name: 'description', mapping: 'd', type: Ext.data.Types.STRING }
            ]
    , AccountMapping: [
                { name: 'FileId', mapping: 'ft', type: Ext.data.Types.STRING }
                , { name: 'InternalNr', mapping: 'inr', type: Ext.data.Types.STRING }
                , { name: 'worksheetTypeId', mapping: 'wt.id', index: true, type: Ext.data.Types.STRING }
                , { name: 'worksheetTypeName', mapping: 'wt.n', type: Ext.data.Types.STRING }
                , { name: 'mappingId', mapping: 'mi.id', type: Ext.data.Types.STRING }
                , { name: 'mappingName', mapping: 'mi.n', type: Ext.data.Types.STRING }
            ]
    , Messages: [
                 { name: 'FileId', mapping: 'fid', type: Ext.data.Types.STRING }
                , { name: 'MessageId', mapping: 'mid', index: true, type: Ext.data.Types.STRING }
                , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                , { name: 'Type', mapping: 'tp', type: Ext.data.Types.STRING }
                , { name: 'Text', mapping: 'tx', type: Ext.data.Types.STRING }
                , { name: 'ConnectionType', mapping: 'ct', type: Ext.data.Types.STRING }
                , { name: 'Account', mapping: 'ca', type: Ext.data.Types.STRING }
                , { name: 'Guid', mapping: 'cg', type: Ext.data.Types.STRING }
                , { name: 'Collection', mapping: 'wtc', type: Ext.data.Types.STRING }
                , { name: 'CollectionText', mapping: 'wtct', type: Ext.data.Types.STRING }
                , { name: 'RowIndex', mapping: 'wtrid', type: Ext.data.Types.STRING }
                , { name: 'Field', mapping: 'wtfld', type: Ext.data.Types.STRING }
                , { name: 'FieldText', mapping: 'wtfldt', type: Ext.data.Types.STRING }
                , { name: 'Worksheet', type: Ext.data.Types.STRING }
            ]
   , WorksheetTypes: [
                 { name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
                , { name: 'RuleApp', mapping: 'ra', type: Ext.data.Types.STRING }
                , { name: 'Order', mapping: 'o', index: true, type: Ext.data.Types.INTEGER }
                , { name: 'Type', mapping: 't', type: Ext.data.Types.STRING }
                , { name: 'IsActive', mapping: 'ia', type: Ext.data.Types.BOOL }
                , { name: 'IsFiche', mapping: 'if', type: Ext.data.Types.BOOL }
                , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
                , { name: 'Errors', mapping: 'ers', type: Ext.data.Types.INT }
                , { name: 'Closed', mapping: 'cl', type: Ext.data.Types.BOOL }
                , { name: 'Warnings', mapping: 'wrs', type: Ext.data.Types.INT }
            ]
    , BookingLines: [
            { name: 'BookingId', mapping: 'bid', type: Ext.data.Types.STRING }
            , { name: 'CreationDate', mapping: 'bcd', type: Ext.data.Types.WCFDATE }
            , { name: 'GroupNr', mapping: 'gnr', type: Ext.data.Types.INT }
            , { name: 'BookingDescription', mapping: 'bd', type: Ext.data.Types.STRING }
            , { name: 'JournalType', mapping: 'bjt', type: Ext.data.Types.INT }
            , { name: 'WorksheetTypeId', mapping: 'wt', type: Ext.data.Types.STRING }
            , { name: 'WorksheetCollection', mapping: 'wtc', type: Ext.data.Types.STRING }
            , { name: 'WorksheetRowId', mapping: 'wtr', type: Ext.data.Types.STRING }
            , { name: 'WorksheetKey', mapping: 'wbk', type: Ext.data.Types.STRING }
            , { name: 'Hidden', mapping: 'bh', type: Ext.data.Types.BOOL }
            , { name: 'Id', mapping: 'id', type: Ext.data.Types.STRING }
            , { name: 'Debet', mapping: 'd', type: Ext.data.Types.NUMBER }
            , { name: 'Credit', mapping: 'c', type: Ext.data.Types.NUMBER }
            , { name: 'Account', mapping: 'acc', type: Ext.data.Types.AUTO }
            , { name: 'InternalAccountNr', mapping: 'ianr', type: Ext.data.Types.STRING }
            , { name: 'ExportCount', mapping: 'ec', type: Ext.data.Types.INT }
            , { name: 'ClientSupplierName', mapping: 'csn', type: Ext.data.Types.STRING }
            , { name: 'ClientSupplierId', mapping: 'csi', type: Ext.data.Types.STRING }
            , { name: 'Comment', mapping: 'com', type: Ext.data.Types.STRING }
            , { name: 'Bold', type: Ext.data.Types.STRING }
    ]
    , FinalTrialBalance: [
            { name: 'InternalAccountNr', mapping: 'ian', type: Ext.data.Types.STRING }
            , { name: 'Account', mapping: 'acc', type: Ext.data.Types.AUTO }
            , { name: 'StartBalance', mapping: 'sb', type: Ext.data.Types.NUMBER }
            , { name: 'AdjustmentsDebet', mapping: 'ad', type: Ext.data.Types.NUMBER }
            , { name: 'AdjustmentsCredit', mapping: 'ac', type: Ext.data.Types.NUMBER }
            , { name: 'EndBalance', mapping: 'eb', type: Ext.data.Types.NUMBER }
    ]
    , DocumentBlock: [
        { name: 'Order', mapping: 'o', index: true, type: Ext.data.Types.INTEGER }
        , { name: 'Title', mapping: 'ti', type: Ext.data.Types.STRING }
        , { name: 'Text', mapping: 'text', type: Ext.data.Types.STRING }
        , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
        , { name: 'EditOnStart', mapping: 'eos', type: Ext.data.Types.BOOL}]
    , DocumentType: [
          { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
        , { name: 'Key', mapping: 'k', type: Ext.data.Types.STRING }
        , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
        , { name: 'IsFullyEditable', mapping: 'ife', type: Ext.data.Types.BOOL }
        , { name: 'IsActive', mapping: 'ia', type: Ext.data.Types.BOOL}]
    , DocumentList: [
          { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
        , { name: 'Culture', mapping: 'c', type: Ext.data.Types.STRING }
        , { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
        , { name: 'DocumentTypeId', mapping: 'dtid', type: Ext.data.Types.STRING}]
    , Bookyear: [{ name: 'Bookyear', mapping: 'Bookyear', index: true, type: Ext.data.Types.INT}]
    , Periods: [{ name: 'Period', mapping: 'Period', index: true, type: Ext.data.Types.STRING }
                , { name: 'Name', mapping: 'Name', type: Ext.data.Types.STRING}]
    , ExtraFiles: [{ name: 'Name', mapping: 'Name', index: true, type: Ext.data.Types.STRING}]
    , InRuleList: [{ name: 'Display', mapping: 'Display', type: Ext.data.Types.STRING },
                    { name: 'Value', mapping: 'Value', index: true, type: Ext.data.Types.STRING}]
    , FileReportList: [{ name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
                        , { name: 'Name', mapping: 'Name', type: Ext.data.Types.STRING }
                        , { name: 'Culture', mapping: 'Culture', type: Ext.data.Types.STRING }
                        ]
    , ExcelPreview: [{ name: 'accountnr', mapping: 'AccountNr', index: true, type: Ext.data.Types.STRING }
                        , { name: 'accountdescription', mapping: 'AccountDescription', type: Ext.data.Types.STRING }
                        , { name: 'saldo', mapping: 'Saldo', type: Ext.data.Types.NUMBER }
                        , { name: 'debet', mapping: 'Debet', type: Ext.data.Types.NUMBER }
                        , { name: 'credit', mapping: 'Credit', type: Ext.data.Types.NUMBER }
                        ]
    , Statistics: [{ name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING },
                    { name: 'LabelY', mapping: 'LabelY', type: Ext.data.Types.STRING },
                    { name: 'Year', mapping: 'Year', type: Ext.data.Types.INTEGER },
                    { name: 'Month', mapping: 'Month', type: Ext.data.Types.INTEGER },
                    { name: 'MonthName', mapping: 'MonthName', type: Ext.data.Types.STRING },
                    { name: 'Total1', mapping: 'Total1', type: Ext.data.Types.NUMBER },
                    { name: 'Total2', mapping: 'Total2', type: Ext.data.Types.NUMBER}]

    , StatisticsSimple: [{ name: 'LabelY', mapping: 'LabelY', index: true, type: Ext.data.Types.STRING },
                    { name: 'Total1', mapping: 'Total1', type: Ext.data.Types.NUMBER },
                    { name: 'Total2', mapping: 'Total2', type: Ext.data.Types.NUMBER },
                    { name: 'Total3', mapping: 'Total3', type: Ext.data.Types.NUMBER },
                    { name: 'Total4', mapping: 'Total4', type: Ext.data.Types.NUMBER },
                    { name: 'Total5', mapping: 'Total5', type: Ext.data.Types.NUMBER },
                    { name: 'Total6', mapping: 'Total6', type: Ext.data.Types.NUMBER },
                    { name: 'Total7', mapping: 'Total7', type: Ext.data.Types.NUMBER },
                    { name: 'Total8', mapping: 'Total8', type: Ext.data.Types.NUMBER },
                    { name: 'Total9', mapping: 'Total9', type: Ext.data.Types.NUMBER },
                    { name: 'Total10', mapping: 'Total10', type: Ext.data.Types.NUMBER}]

    , Offices: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                    { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING}]
    , PdfFile: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                { name: 'FileId', mapping: 'fid', type: Ext.data.Types.STRING },
                { name: 'Name', mapping: 'd', type: Ext.data.Types.STRING },
                { name: 'Pages', mapping: 'p', type: Ext.data.Types.INT}]
    , Champion: [{ name: 'personId', mapping: 'pid', index: true, type: Ext.data.Types.STRING },
                { name: 'officeId', mapping: 'oid', type: Ext.data.Types.STRING },
                { name: 'firstName', mapping: 'fn', type: Ext.data.Types.STRING },
                { name: 'lastName', mapping: 'ln', type: Ext.data.Types.STRING },
                { name: 'email', mapping: 'em', type: Ext.data.Types.STRING },
                { name: 'officeName', mapping: 'o', type: Ext.data.Types.STRING}]
    , TeamMember: [{ name: 'personId', mapping: 'pid', index: true, type: Ext.data.Types.STRING },
                { name: 'clientId', mapping: 'cid', type: Ext.data.Types.STRING },
                { name: 'department', mapping: 'd', type: Ext.data.Types.STRING },
                { name: 'role', mapping: 'r', type: Ext.data.Types.STRING },
                { name: 'name', mapping: 'n', type: Ext.data.Types.STRING },
                { name: 'mail', mapping: 'm', type: Ext.data.Types.STRING },
                { name: 'level', mapping: 'l', type: Ext.data.Types.INT}]
    , ProAccJournals: [{ name: 'id', mapping: 'id', index: true, type: Ext.data.Types.INT },
                { name: 'name', mapping: 'n', type: Ext.data.Types.STRING}]
    , ManualWorksheets: [{ name: 'id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                { name: 'name', mapping: 'n', type: Ext.data.Types.STRING },
                { name: 'lastUpdated', mapping: 'lu', type: Ext.data.Types.WCFDATE}]
    , LibraryNode: [{ name: 'indexItem', mapping: 'indexItem', type: Ext.data.Types.AUTO}]

    , Resource: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                { name: 'Title', mapping: 'title', type: Ext.data.Types.STRING },
                { name: 'ResourceType', mapping: 'type', type: Ext.data.Types.STRING },
                { name: 'DC', mapping: '__type', type: Ext.data.Types.STRING}]
    , XbrlFiches: [
        { name: 'id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
        { name: 'automated', mapping: 'automated', type: Ext.data.Types.BOOL },
        { name: 'checked', mapping: 'checked', type: Ext.data.Types.BOOL },
        { name: 'pdfid', mapping: 'pdfid', type: Ext.data.Types.STRING },
        { name: 'pdfname', mapping: 'pdfname', type: Ext.data.Types.STRING }
        ]
     , LegalTypes: [
        { name: 'id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
        { name: 'listdesc', mapping: 'ld', type: Ext.data.Types.STRING },
        { name: 'code', mapping: 'c', type: Ext.data.Types.STRING },
        { name: 'name', mapping: 'n', type: Ext.data.Types.STRING }
        ]
    , Partners: [
        { name: 'id', mapping: 'id', index: true, type: Ext.data.Types.STRING }
        , { name: 'name', mapping: 'n', type: Ext.data.Types.STRING }
        , { name: 'department', mapping: 'd', type: Ext.data.Types.STRING }
        ]
    , GTHClientsList: [
        { name: 'Id', mapping: 'Id', index: true, type: Ext.data.Types.STRING }
        , { name: 'ClientName', mapping: 'ClientName', type: Ext.data.Types.STRING }
        , { name: 'ClientGfis', mapping: 'ClientGfis', type: Ext.data.Types.STRING }
        , { name: 'ClientEnterprise', mapping: 'ClientEnterprise', type: Ext.data.Types.STRING }
        , { name: 'StartDate', mapping: 'StartDate', type: Ext.data.Types.WCFDATE }
        , { name: 'EndDate', mapping: 'EndDate', type: Ext.data.Types.WCFDATE }
    ]
     , WorksheetHelp: [{ name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
                { name: 'worksheetTypeID', mapping: 'wid', index: true, type: Ext.data.Types.STRING },
                { name: 'culture', mapping: 'c', type: Ext.data.Types.STRING },
                { name: 'subject', mapping: 's', type: Ext.data.Types.STRING },
                { name: 'body', mapping: 'b', type: Ext.data.Types.STRING },
                { name: 'order', mapping: 'o', type: Ext.data.Types.INT}]

    , ExactAdmin: [{ name: 'ExactAdminCode', mapping: 'eacode', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminNr', mapping: 'eanr', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminName', mapping: 'eaname', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminAddresLine1', mapping: 'eaal1', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminAddresLine2', mapping: 'eaal2', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminAddresLine3', mapping: 'eaal3', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminZipCode', mapping: 'eazc', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminCity', mapping: 'eacity', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminState', mapping: 'eas', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminCountry', mapping: 'eacountry', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminCurrency', mapping: 'eacurrency', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminVat', mapping: 'eavat', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminClusterTermId', mapping: 'eactid', index: true, type: Ext.data.Types.INT },
                    { name: 'ExactAdminClusterCode', mapping: 'eacc', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminMainPostbox', mapping: 'eamp', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminDisplay', mapping: 'ead', index: true, type: Ext.data.Types.STRING },
                    { name: 'ExactAdminHasFinancialPeriods', mapping: 'eahfp', index: true, type: Ext.data.Types.BOOL}]

    , ExactFinYears: [{ name: 'ExactFinYearId', mapping: 'efyid', index: true, type: Ext.data.Types.INT },
                        { name: 'ExactFinYearAdminCode', mapping: 'efyac', index: true, type: Ext.data.Types.STRING },
                        { name: 'ExactFinYearNumber', mapping: 'efyn', index: true, type: Ext.data.Types.INT },
                        { name: 'ExactFinYearStartDate', mapping: 'efysd', index: true, type: Ext.data.Types.WCFDATE },
                        { name: 'ExactFinYearEndDate', mapping: 'efyed', index: true, type: Ext.data.Types.WCFDATE },
                        { name: 'ExactFinYearDisplay', mapping: 'efyd', index: true, type: Ext.data.Types.STRING}]

    , ClientEngagementLettersByPerson: [{ name: 'clientName', mapping: 'c', index: true, type: Ext.data.Types.STRING },
                        { name: 'clientGFIS', mapping: 'cg', index: true, type: Ext.data.Types.STRING },
                        { name: 'manager', mapping: 'm', index: true, type: Ext.data.Types.STRING },
                        { name: 'office', mapping: 'o', index: true, type: Ext.data.Types.STRING },
                        { name: 'partner', mapping: 'p', index: true, type: Ext.data.Types.STRING },
                        { name: 'status', mapping: 's', index: true, type: Ext.data.Types.STRING },
                        { name: 'template', mapping: 't', index: true, type: Ext.data.Types.STRING },
                        { name: 'startDate', mapping: 'sd', index: true, type: Ext.data.Types.WCFDATE },
                        { name: 'endDate', mapping: 'ed', index: true, type: Ext.data.Types.WCFDATE },
                        { name: 'dateELEA', mapping: 'de', index: true, type: Ext.data.Types.STRING },
                        { name: 'fileName', mapping: 'fn', index: true, type: Ext.data.Types.STRING },
                        { name: 'gth', mapping: 'gth', index: true, type: Ext.data.Types.STRING },
                        { name: 'itemId', mapping: 'iid', index: true, type: Ext.data.Types.STRING },
                        { name: 'clientId', mapping: 'cid', index: true, type: Ext.data.Types.STRING }
                        ]

    ,Statuses:[
        { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.INT },
        { name: 'Status', mapping: 'sn', type: Ext.data.Types.STRING },
        { name: 'Rank', mapping: 'rnk', type: Ext.data.Types.INT }
        ]

    ,Engagement: [
        { name: 'Code', mapping: 'id', index: true, type: Ext.data.Types.STRING },
        { name: 'CustomerId', mapping: 'cid', index: true, type: Ext.data.Types.STRING },
        { name: 'Description', mapping: 'd', type: Ext.data.Types.STRING },
        { name: 'Status', mapping: 's', type: Ext.data.Types.STRING },
        { name: 'ServiceLine', mapping: 'sl', type: Ext.data.Types.STRING },
        { name: 'ServiceLineDescription', mapping: 'sld', type: Ext.data.Types.STRING }
    ]

    ,CodaPoa:[
        { name: 'clientName', mapping: 'cn',  type: Ext.data.Types.STRING },
        { name: 'clientGfis', mapping: 'cg', type: Ext.data.Types.STRING },
        { name: 'bic', mapping: 'bic', type: Ext.data.Types.STRING },
        { name: 'lastModified', mapping: 'lm', type: Ext.data.Types.WCFDATE },
        { name: 'bankAccount', mapping: 'ba',index: true, type: Ext.data.Types.STRING },
        { name: 'bankName', mapping: 'bn', type: Ext.data.Types.STRING },
        { name: 'clientId', mapping: 'cid', type: Ext.data.Types.STRING },
        { name: 'status', mapping: 'sn',  type: Ext.data.Types.STRING },
        { name: 'statusId', mapping: 'sid',  type: Ext.data.Types.INT },
        { name: 'docstoreId', mapping: 'dsid', type: Ext.data.Types.STRING },
        { name: 'repositoryItemId', mapping: 'riid', type: Ext.data.Types.STRING },
        { name: 'EOL_EYLicense', mapping: 'eoll',  type: Ext.data.Types.STRING },
        { name: 'EOL_DigMailbox', mapping: 'eolm',  type: Ext.data.Types.STRING },
        { name: 'EOL_Division', mapping: 'eold',  type: Ext.data.Types.STRING },
        { name: 'statusModified', mapping: 'sm',  type: Ext.data.Types.WCFDATE },
        { name: 'allowCoda', mapping: 'ac',  type: Ext.data.Types.STRING },
        { name: 'activeClientACR', mapping: 'aca', type: Ext.data.Types.STRING }
        ]

    ,Log:[
        { name: 'actions', mapping: 'act', type: Ext.data.Types.STRING },
        { name: 'comment', mapping: 'cm', type: Ext.data.Types.STRING },
        { name: 'status', mapping: 'st',  type: Ext.data.Types.STRING },
        { name: 'timestamp', mapping: 'ts', type: Ext.data.Types.WCFDATE },
        { name: 'documentUploaded', mapping: 'du', type: Ext.data.Types.BOOL },
        { name: 'personName', mapping: 'pn', type: Ext.data.Types.STRING },
        { name: 'pdfAttachmentId', mapping: 'pi', type: Ext.data.Types.STRING }
    ]

    ,BankList:[  { name: 'Id', mapping: 'id', index: true, type: Ext.data.Types.STRING },
        { name: 'Name', mapping: 'n', type: Ext.data.Types.STRING }
        ]

    , ClientPersonFileServiceReport: [{ name: 'clientId', mapping: 'cid', index: true, type: Ext.data.Types.STRING },
        { name: 'clientName', mapping: 'c', index: true, type: Ext.data.Types.STRING },
        { name: 'clientGFIS', mapping: 'cg', index: true, type: Ext.data.Types.STRING },
        { name: 'statusId', mapping: 'sid', index: true, type: Ext.data.Types.INT },
        { name: 'status', mapping: 's', index: true, type: Ext.data.Types.STRING },
        { name: 'startDate', mapping: 'sd', index: true, type: Ext.data.Types.WCFDATE },
        { name: 'endDate', mapping: 'ed', index: true, type: Ext.data.Types.WCFDATE },
        { name: 'nextAnnualMeeting', mapping: 'nam', index: true, type: Ext.data.Types.WCFDATE },
        { name: 'manager', mapping: 'm', index: true, type: Ext.data.Types.STRING },
        { name: 'office', mapping: 'o', index: true, type: Ext.data.Types.STRING },
        { name: 'partner', mapping: 'p', index: true, type: Ext.data.Types.STRING },
        { name: 'person', mapping: 'pe', index: true, type: Ext.data.Types.STRING },
        { name: 'dateModified', mapping: 'dm', index: true, type: Ext.data.Types.WCFDATE },
        { name: 'daysOnStatus', mapping: 'dos', index: true, type: Ext.data.Types.STRING },
        { name: 'fileId', mapping: 'fid', index: true, type: Ext.data.Types.STRING },
        { name: 'fileName', mapping: 'f', index: true, type: Ext.data.Types.STRING }
    ]
};
//

