

eBook.data.BaseJsonStore = Ext.extend(Ext.data.JsonStore, {
    getRecordTypeDataObject: function(rec, baseObj) {
        var flds = this.reader.meta.fields;
        var obj = {};
        for (var i = 0; i < flds.length; i++) {
            obj[flds[i].mapping] = rec.get(flds[i].name);
            if (Ext.isString(obj[flds[i].mapping]) && Ext.isEmpty(obj[flds[i].mapping])) {
                obj[flds[i].mapping] = null;
            }
        }
        if (Ext.isDefined(baseObj)) Ext.apply(obj, baseObj);
        return obj;
    }
    , getJsonRecords: function(baseObj, validationFn) {
        var recs = [];

        this.each(function(rec) {
            if (!this.isRecordEmpty(rec)) {
                if (Ext.isFunction(validationFn)) {
                    if (validationFn(rec)) {
                        recs.push(this.getRecordTypeDataObject(rec, baseObj));
                    }
                } else {
                    recs.push(this.getRecordTypeDataObject(rec, baseObj));
                }
            }
        }, this);
        return recs;
    }
    , isRecordEmpty: function(rec) {
        var empty = true;
        for (var prop in rec.data) {
            if (rec.data.hasOwnProperty(prop)) {
                if (!Ext.isEmpty(rec.data[prop])) return false;
            }
        }
        return empty;
    }
});

eBook.data.JsonStore = Ext.extend(eBook.data.BaseJsonStore, {
    serviceUrl: eBook.Service.url
    , constructor: function(config) {
        if (config.serviceUrl) this.serviceUrl = config.serviceUrl;
        if (config.selectAction) {
            config.root = config.selectAction + 'Result';
            if (config.rootProp) config.root = config.root + '.' + config.rootProp;
            config.proxy = new eBook.data.WcfProxy({
                url: this.serviceUrl + config.selectAction
                , criteriaParameter: config.criteriaParameter
                , method: 'POST'
            });
            config.proxy.enableFileCached = config.enableFileCached;
        }
        eBook.data.JsonStore.superclass.constructor.call(this, config);
    }
    , loadRecords: function(o, options, success) {
        eBook.data.JsonStore.superclass.loadRecords.apply(this, arguments);
    }
});
Ext.reg('eb-jsonstore', eBook.data.JsonStore);

eBook.data.GroupedJsonStore = Ext.extend(Ext.data.GroupingStore, {
    serviceUrl: eBook.Service.url
    , constructor: function(config) {
        if (config.serviceUrl) this.serviceUrl = config.serviceUrl;
        if (config.selectAction) {
            config.root = config.selectAction + 'Result';

            config.proxy = new eBook.data.WcfProxy({
                url: this.serviceUrl + config.selectAction
                , criteriaParameter: config.criteriaParameter
                , method: 'POST'
                , enableFileCached: config.enableFileCached
            });
            config.proxy.enableFileCached = config.enableFileCached;
        }
        eBook.data.GroupedJsonStore.superclass.constructor.call(this, Ext.apply(config, {
            reader: new Ext.data.JsonReader(config)
        }));
    }
    , loadRecords: function(o, options, success) {
        eBook.data.GroupedJsonStore.superclass.loadRecords.apply(this, arguments);
    }
});


eBook.data.PagedJsonStore = Ext.extend(Ext.data.JsonStore, {
    serviceUrl: eBook.Service.url
    , constructor: function(config) {
        if (config.serviceUrl) this.serviceUrl = config.serviceUrl;
        if (config.selectAction) {
            config.root = config.selectAction + 'Result.Data';
            config.totalProperty = config.selectAction + 'Result.Total',
            config.paramNames = {
                start: 'Start'
                 , limit: 'Limit'
            };
            config.proxy = new eBook.data.WcfProxy({
                url: this.serviceUrl + config.selectAction
                , criteriaParameter: config.criteriaParameter
                , method: 'POST'
            });
            config.proxy.enableFileCached = config.enableFileCached;
        }
        eBook.data.PagedJsonStore.superclass.constructor.call(this, config);
    }
    , load: function(options) {
        eBook.data.PagedJsonStore.superclass.load.call(this, options);
    }
});