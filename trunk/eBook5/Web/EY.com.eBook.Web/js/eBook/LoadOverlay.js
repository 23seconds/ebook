
eBook.LoadOverlay = {
    el: null
    , setEl: function(el) {
        this.el = el;
        this.el.setVisibilityMode(Ext.Element.DISPLAY);
        this.el.hide();
    }
    , showFor: function(target, offsets) {
        this.el.show();
        this.el.anchorTo(target, 'br',offsets);
    }
    , hide: function() {
        this.el.hide();
    }
};