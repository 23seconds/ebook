
eBook.Activity.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        Ext.apply(this, {
            title: '',
            items: []
        });
        eBook.Activity.Window.superclass.initComponent.apply(this, arguments);
    }
});