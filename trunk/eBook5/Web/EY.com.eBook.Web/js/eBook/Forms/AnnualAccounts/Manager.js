//This form is, as a panel, not stand-alone.
//It does not have the server-contact capability. It does however contain all the functions and info for the parent to utilize this form.
//Its functions allow form interaction, data validation and retrieval.
eBook.Forms.AnnualAccounts.Manager = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        //Vars
        var me = this,
            invoiceRechargeFilingExpenses = ["Filing costs + supplementary cost for late filing","Only filing costs","Only supplementary costs"],
            invoiceLanguage = ["Dutch","French","English"];
            /*invoiceLanguage = Ext.create('Ext.data.Store', {
            fields: ['value', 'display'],
            data : [
                {"value":"nl-BE", "display":"Dutch"},
                {"value":"fr-FR", "display":"French"},
                {"value":"en-GB", "display":"English"}
                //...
            ]
        })*/
        //Set properties that will be used by parent

        //Form
        Ext.apply(this, {
            layout: 'form',
            labelWidth: 200,
            title: 'Annual Accounts information form',
            bodyStyle: 'padding:10px;',
            //, resizable: true,
            padding:10,
            ref: 'formPanel',
            items: [
                {
                    xtype: 'combo'
                    , ref: 'engagementCode'
                    , fieldLabel: 'Engagement code'
                    , name: 'engagementCode'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , forceSelection: true
                    , valueField: 'Code'
                    , displayField: 'Code'
                    ,tpl: new Ext.XTemplate(
                    '<tpl for="."><div class="x-combo-list-item">{Code} - {Description}</div></tpl>')
                    , editable: false
                    , mode: 'local'
                    , anchor: '95%'
                    , store: new eBook.data.JsonStore({
                    selectAction: 'GetEngagementCodes'
                    , criteriaParameter: 'cidc'
                    , autoDestroy: true
                    , serviceUrl: eBook.Service.annualAccount
                    , storeParams: {Id: eBook.CurrentClient}
                    , baseParams: {Id: eBook.CurrentClient}
                    , disabled: this.readOnly
                    , fields: eBook.data.RecordTypes.Engagement
                    , autoLoad: true
                    })
                },
                {
                    xtype: 'combo'
                    , ref: 'invoiceRechargeFilingExpense'
                    , fieldLabel: 'Invoice recharge filing expense'
                    , name: 'invoiceRechargeFilingExpense'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Name'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: invoiceRechargeFilingExpenses
                    , anchor: '95%'
                }  ,
                {
                    xtype: 'combo'
                    , ref: 'invoiceLanguage'
                    , fieldLabel: 'Invoice language'
                    , name: 'invoiceLanguage'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Name'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: invoiceLanguage
                    , anchor: '95%'
                }
            ],
            listeners: {
                afterrender: function () {
                    //remove mask if there is one
                    if(me.maskedCmp) {
                        me.maskedCmp.getEl().unmask();
                    }
                }
            }
        });
        eBook.Forms.AnnualAccounts.Manager.superclass.initComponent.apply(this, arguments);
    },
    //Check if form is valid
    isValid: function()
    {
        //vars
        var me = this,
            invalid = null;

        //validation
        if(!me.engagementCode.isValid() || !me.engagementCode.getValue()) invalid = " engagement code";
        if(!me.invoiceRechargeFilingExpense.isValid() || !me.invoiceRechargeFilingExpense.getValue()) {invalid = " invoice recharge filing expense";}
        if(!me.invoiceLanguage.isValid() || !me.invoiceLanguage.getValue()) {invalid = "invoice language";}

        //return en alert
        if(invalid) {
            Ext.Msg.alert("Not filled in", "Please fill in the " + invalid + ".");
            return false;
        }

        return true;
    },
    //Get json data of form
    getData: function() {
        //vars
        var me = this,
            cfaadc = {},
            combo = me.engagementCode,
            store = combo ? combo.getStore() : null,
            record = store ? store.getById(combo.getValue()): null; //get record

        //create contract
        cfaadc.InvoiceRechargeFilingExpense = me.invoiceRechargeFilingExpense.getValue();
        cfaadc.InvoiceLanguage = me.invoiceLanguage.getValue();
        cfaadc.EngagementCode = record ? record.get("Code") : null;
        cfaadc.ServiceLine = record ? record.get("ServiceLine"): null;

        return cfaadc;
    },
    setData: function(values) {
        var me = this,
            combo = me.engagementCode,
            store = combo ? combo.getStore() : null,
            record = store ? store.getAt(0) : null; //get first record

        if(values) {
            me.getForm().setValues(values);
        }

        if(!values.engagementCode) //pre-set the first engagement code if it wasn't already set
        {
            combo.setValue(record ? record.id : null);
        }
    }
    , show: function(parent) {
        var me = this;
        me.callingParent = parent;
        eBook.Forms.AnnualAccounts.Manager.superclass.show.call(me);
    }
});