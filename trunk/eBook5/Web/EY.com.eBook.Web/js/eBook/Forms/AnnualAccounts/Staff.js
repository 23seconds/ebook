//This form is, as a panel, not stand-alone.
//It does not have the server-contact capability. It does however contain all the functions and info for the parent to utilize this form.
//Its functions allow form interaction, data validation and retrieval.
eBook.Forms.AnnualAccounts.Staff = Ext.extend(Ext.FormPanel, {
    initComponent: function() {
        //Vars
        var me = this,
            companyTypes = ["Enterprise","Non-profit institution","Foreign enterprise","Foreign non-profit institution"],
            models = ["Full presentation","Abbreviated presentation","Consolidated accounts","correction","Other"];

        //Set properties that will be used by parent
        me.changesAfterGeneralAssemblyDescriptionValue = null;
        me.serviceUrl = eBook.Service.annualAccount;
        me.action = 'UpdateAnnualAccounts';
        me.criteriaParameter = 'cfaadc';

        //Form
        Ext.apply(this, {
            layout: 'form',
            labelWidth: 200,
            title: 'Annual Accounts information form',
            bodyStyle: 'padding:10px;',
            padding:10,
            ref: 'formPanel',
            items: [
                /*{ Currently not auto-filled in
                    xtype:'label',
                    fieldLabel: 'Is the estimated general assembly date accurate? If not add the definitive date',
                    labelStyle: 'width: 800px;'
                },*/
                {
                    xtype: 'datefield',
                    ref: 'definitiveGeneralAssembly',
                    anchor: '95%',
                    name: 'definitiveGeneralAssembly',
                    fieldLabel: 'Definitive general assembly date',
                    maxValue: new Date(),
                    format: 'd/m/Y'
                },
                {
                    xtype: 'combo'
                    , ref: 'companyType'
                    , fieldLabel: 'Type of company'
                    , name: 'companyType'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Name'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: companyTypes
                    , anchor: '95%'
                }  ,
                {
                    xtype: 'combo'
                    , ref: 'model'
                    , fieldLabel: 'Model'
                    , name: 'model'
                    , triggerAction: 'all'
                    , typeAhead: false
                    , selectOnFocus: true
                    , autoSelect: true
                    , allowBlank: true
                    , forceSelection: true
                    , valueField: 'Name'
                    , displayField: 'Name'
                    , editable: false
                    , nullable: true
                    , listWidth: 300
                    , mode: 'local'
                    , store: models
                    , anchor: '95%'
                },
                {
                    xtype:'label',
                    fieldLabel: 'Can you confirm that no adjustments, by EY, have been made to the annual accounts after the date of the AGM',
                    labelStyle: 'width: 800px;'
                },
                {
                    xtype: 'radiogroup',
                    ref: 'changesAfterGAD',
                    name: 'changesAfterGeneralAssemblyGroup',
                    columns: 1,
                    hideLabel: true,
                    items: [
                        {
                            boxLabel: 'I confirm',
                            ref: 'changesAfterGADNo',
                            name: 'changesAfterGeneralAssembly',
                            inputValue: false
                        },
                        {
                            boxLabel: 'I do not confirm',
                            ref: 'changesAfterGADYes',
                            name: 'changesAfterGeneralAssembly',
                            inputValue: true
                        }
                    ],
                    listeners: {
                    'change': {
                        fn: me.toggleChangesAfterGADDescription,
                        scope: this
                        }
                    }
                },
                {
                    xtype:'label',
                    ref: 'changesAfterGADlabel',
                    fieldLabel: 'Please add a note as to why adjustments were made',
                    labelStyle: 'width: 800px;'
                }
            ],
            listeners: {
                afterrender: function () {
                    //disable editor of parent as its availability depends on a form component
                    me.parent.htmlEditor.disable();
                    me.changesAfterGADlabel.hide();
                }
            }
        });
        eBook.Forms.AnnualAccounts.Staff.superclass.initComponent.apply(this, arguments);
    }
    //Check if form is valid
    , isValid: function()
    {
        //vars
        var me = this,
            invalid = null;

        //validation
        if(!me.model.isValid() || !me.model.getValue()) {invalid = "model";}
        if(!me.companyType.isValid() || !me.companyType.getValue()) {invalid = "company type";}
        if(!me.definitiveGeneralAssembly.isValid() || !me.companyType.getValue()) {invalid = "general assembly date";}
        if(me.parent) { //review notes window
            if (me.changesAfterGAD.getValue().inputValue && me.parent && me.parent.htmlEditor.getValue() == "") {invalid = "text area";}
        }

        //return en alert
        if(invalid) {
            Ext.Msg.alert("Not filled in", "Please fill in the " + invalid + ".");
            return false;
        }

        return true;
    }
    //Get json data of form
    , getData: function() {
        //vars
        var me = this,
            cfaadc = {};

        //create contract
        cfaadc.DefinitiveGeneralAssembly = me.definitiveGeneralAssembly.getValue();
        cfaadc.CompanyType = me.companyType.getValue();
        cfaadc.Model = me.model.getValue();
        cfaadc.ChangesAfterGeneralAssembly = me.changesAfterGAD.getValue().inputValue;
        if(cfaadc.ChangesAfterGeneralAssembly === true && me.parent && me.parent.htmlEditor)
        {
            cfaadc.ChangesAfterGeneralAssemblyDescription = me.parent.htmlEditor.getValue();
        }

        return cfaadc;
    }
    , setData: function(values) {
        var me = this;

        if(values) {
            me.getForm().setValues(values);
        }
        if(values.definitiveGeneralAssembly) //Date not being properly filled in through setValues as it is supplied in millisenconds since 1970, format u
        {
            me.definitiveGeneralAssembly.setValue(Ext.data.Types.WCFDATE.convert(values.definitiveGeneralAssembly));
        }
        if(values.changesAfterGeneralAssembly)
        {
            //Could not get the setValue on the group to work so I went with the filter function on the items as a dynamic solution
            me.changesAfterGAD.items.items.filter(function(obj){return obj.inputValue == values.changesAfterGeneralAssembly})[0].setValue(true);
        }
        if(values.changesAfterGeneralAssemblyDescription)
        {
            me.changesAfterGeneralAssemblyDescriptionValue = values.changesAfterGeneralAssemblyDescription;
            me.parent.htmlEditor.setValue(me.changesAfterGeneralAssemblyDescriptionValue);
        }

    }
    , show: function(parent) {
        var me = this;
        me.callingParent = parent;
        eBook.Forms.AnnualAccounts.Staff.superclass.show.call(me);
    }
    , toggleChangesAfterGADDescription: function(buttongroup, newValue)
    {
        //Toggle review notes editor on and off
        var me = this;

        if(me.parent) {
            if (newValue.inputValue) {
                me.parent.htmlEditor.enable();
                me.parent.htmlEditor.setValue(me.changesAfterGeneralAssemblyDescriptionValue);
                me.changesAfterGADlabel.show();
            }
            else {
                me.parent.htmlEditor.disable();
                me.parent.htmlEditor.setValue(null);
                me.changesAfterGADlabel.hide();
            }
        }
    }
});