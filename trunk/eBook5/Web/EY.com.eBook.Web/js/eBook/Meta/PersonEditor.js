
eBook.Meta.PersonEditor = Ext.extend(Ext.Panel, {
    classType: 'MetaPersonDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName: 'Person'
    , gridEditor: false
    , initComponent: function() {
        Ext.apply(this, {
            border: false,
            layout: 'form',
            labelWidth: 200,
            items: [
                {
                    xtype: 'label',
                    text: this.fieldText
                    , disabled: eBook.Interface.isFileClosed()
                },
                { //eBook.Meta.PersonEditor_Male eBook.Meta.PersonEditor_Female
                    xtype: 'combo'
                    , ref: 'gr'
                    , name: 'gr'
                    , value: 'M'
                    , lazyRender: true
                    , mode: 'local'
                    , disabled: eBook.Interface.isFileClosed()
                    , store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: [
                            'id',
                            'display'
                        ],
                        data: [['M', eBook.Meta.PersonEditor_Male ], ['F', eBook.Meta.PersonEditor_Female]]
                    })
                    , valueField: 'id'
                    , displayField: 'display'
                    , typeAhead: false
                    , triggerAction: 'all'
                    , fieldLabel:eBook.Meta.PersonEditor_Gender // eBook.Meta.PersonEditor_Gender
                }, {

                xtype: 'textfield'
                    , disabled: eBook.Interface.isFileClosed()
                    , width: 250
                    , ref: 'nme'
                    , name: 'nme'
                    , value: ''
                    , fieldLabel:eBook.Meta.PersonEditor_Name // eBook.Meta.PersonEditor_Name
}]
        });

        if (this.gridEditor) {
            Ext.apply(this, {
                buttons: [{
                    //text:'Bewaar gegevens',
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onSaveClick,
                    scope: this
                }, {
                    //text: 'Annuleer',
                    ref: '../cancelButton',
                    iconCls: 'eBook-window-cancel-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onCancelClick,
                    scope: this
                }
                ]
            });
        }
        eBook.Meta.PersonEditor.superclass.initComponent.apply(this, arguments);
    }
    , onSaveClick: function() {
        this.saveLine();
        return;
    }
    , onCancelClick: function() {
        if (!this.refOwner) return;
        if (this.refOwner.onSaveItem) {
            this.refOwner.onSaveItem();
        }
        return;
    }
    , loadLine: function(rec) {
        this.activeRecord = rec;
        this.loadObject(rec.data);
    }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        this.loadObject(mobj);
    }
    , loadObject: function(obj) {
        if (obj) {
            this.gr.setValue(obj.gr);
            this.nme.setValue(obj.nme);
        }
    }
    , getData: function() {
        return {
            gr: this.gr.getValue()
            , nme: this.nme.getValue()
        };
    }
    , saveLine: function() {
        if (!this.refOwner) return;
        
        if (this.activeRecord) {
            this.activeRecord.data = this.getData();
            this.activeRecord.markDirty();
            this.activeRecord.commit();
            this.refOwner.onSaveItem();
        } else {
            if (!this.refOwner) return;
            if (this.refOwner.gridpanel) {
                var rect = this.refOwner.gridpanel.store.recordType;
                var rec = new rect(this.getData());
                this.refOwner.gridpanel.store.add(rec);
                this.refOwner.onSaveItem();
            }
        }
    }
    , reset: function() {
        this.clearAll();
    }
    , clearAll: function() {
        this.gr.reset();
        this.nme.reset();
        this.activeRecord = null;
    }
});

Ext.reg('metapersoneditor', eBook.Meta.PersonEditor);