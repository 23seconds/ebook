
eBook.Meta.DataTypes = {
    SHORTTEXT:'shorttext'
    ,NUMBER:'number'
    ,BOOLEAN:'boolean'
    ,DATE:'date'
    ,TIME:'time'
    ,COLLECTION:'collection'
    ,ENTITY:'entity'
    ,HTMLEDIT:'htmledit'
};


eBook.Meta.Node = Ext.extend(Ext.Component, {
    applyTo: ''
    , editorcfg: {
        shadow: false,
        completeOnEnter: true,
        cancelOnEsc: true,
        updateEl: true,
        ignoreNoChange: true
    }
    , constructor: function(config) {
        var cs = [];
        if (config.data) {
            if (config.data.children) {
                cs = config.data.children;
                config.data.children = null;
            }
            var dt = '' + config.data.dataType;

            // editor field

            if (config.data.isList) {
                config.editorField = {
                    xtype: 'combo',
                    allowBlank: false,
                    xtype: 'combo',
                    store: new eBook.data.JsonStore({
                        selectAction: 'GetMetaDataLookupList'
                        , baseParams: { 'metaDataLookupListId': config.data.listId, 'culture': eBook.Interface.Culture }
                        , fields: eBook.data.RecordTypes.MetaLookup
                        , idField: 'Id'
                    }),
                    valueField: 'Value',
                    displayField: eBook.Meta.PersonEditor_Name,
                    forceSelection: true,
                    typeAhead: false,
                    triggerAction: 'all',
                    width: 100
                };
                this.editorcfg.updateEl = false;
            } else {
                switch (dt) {
                    case eBook.Meta.DataTypes.BOOLEAN:
                        config.editorField = {
                            xtype: 'combo',
                            allowBlank: false,
                            xtype: 'combo',
                            store: [[true, 'Jakes'], [false, 'Neekes']],
                            forceSelection: true,
                            typeAhead: false,
                            triggerAction: 'all',
                            width: 100
                            //selectOnFocus:true
                        };
                        break;
                    case eBook.Meta.DataTypes.NUMBER:
                        config.editorField = {
                            allowBlank: true,
                            xtype: 'numberfield',
                            width: 90,
                            selectOnFocus: true
                        };
                        break;
                    case eBook.Meta.DataTypes.DATE:
                        config.editorField = {
                            allowBlank: true,
                            xtype: 'datefield',
                            width: 90,
                            selectOnFocus: true,
                            format: 'd/m/Y'
                        };
                        break;
                    case eBook.Meta.DataTypes.TIME:
                        config.editorField = {
                            allowBlank: true,
                            xtype: 'timefield',
                            width: 90,
                            selectOnFocus: true,
                            format: 'H:i'
                        };
                        break;
                    // case eBook.Meta.DataTypes.HTMLEDIT:              
                    //     break;              
                    default:
                        config.editorField = {
                            allowBlank: true,
                            xtype: 'textfield',
                            width: 90,
                            selectOnFocus: true
                        };
                        break;
                }
            }

            this.data = {
                id: config.data.id
                , text: config.data.text
                , value: config.data.value ? config.data.value : ''
                , dataType: config.data.dataType ? config.data.dataType : 'STRING'
                , isList: config.data.isList
                , listId: config.data.listId
            };
        } else {
            this.data = {};
        }
        delete config['data'];

        config.children = [];
        for (var i = 0; i < cs.length; i++) {
            config.children.push(new eBook.Meta.Node({ data: cs[i], showExpanded: this.showExpanded }));
        }
        eBook.Meta.Node.superclass.constructor.call(this, config);
    }
    , onEditorComplete: function(ed, v, sv) {
        this.setValue(v);
    }
    , titleWidth: 120
    , isRoot: false
    , showExpanded: false
    , hasChildren: function() { return this.children.length > 0; }
    , children: []
    , data: {}
    , onRender: function(ct, position) {
        if (!this.rendered) {
            eBook.Meta.Node.superclass.onRender.call(this, ct, position);
            this.el.dom.setAttributeNS("eBook", "keyid", this.data.keyId);
            this.el.dom.setAttributeNS("eBook", "id", this.data.id);
            this.el.addClass("eBook-Meta-Tree-Node");
            if (this.hasChildren()) this.el.addClass("eBook-Meta-Tree-Node-folder");
            if (this.hasChildren() && this.showExpanded) this.el.addClass("eBook-Meta-Tree-Node-folder-open");
            var d = {
                hasChildren: this.hasChildren()
                , showExpanded: this.showExpanded
                , isRoot: this.isRoot
                , id: this.id
                , dataId: this.data.id
                , text: this.data.text
                , value: this.data.value
                , dataType: this.data.dataType
            };
            this.nodeTpl.overwrite(this.el, d);
            if (this.titleWidth) this.getTitleEl().setWidth(this.titleWidth);
            if (this.hasChildren) this.renderChildren(ct, d);
            this.setEvents();
        }
    }
    , getValueEl: function() {
        return this.el.child(".eBook-Meta-Tree-Node-Value");
    }
    , getTitleEl: function() {
        return this.el.child(".eBook-Meta-Tree-Node-Title");
    }
    , setValue: function(value) {
        if (this.hasChildren()) return;
        if (this.data.isList) {
            var idx = this.editor.field.store.find('Value', value);
            this.data.valueText = this.editor.field.store.getAt(idx).get(eBook.Meta.PersonEditor_Name);
        } else {
            this.data.valueText = value;
        }
        this.data.value = value;
        var vEl = this.getValueEl();
        if (vEl) Ext.get(vEl).update(this.data.valueText);
    }
    , setText: function(text) {
        this.getTitleEl().update(text);
    }
    , renderChildren: function(ct, d) {
        this.childContainerEl = this.childTpl.append(ct, d, true);
        this.childContainerEl = Ext.get(this.childContainerEl);
        for (var i = 0; i < this.children.length; i++) {
            this.children[i].onRender(this.childContainerEl);
        }

    }
    , nodeTpl: new Ext.XTemplate('<tpl for="."><div id="{id}-title" class="eBook-Meta-Tree-Node-Title" style="float:left;">{text}</div><span id="{id}-value" class="eBook-Meta-Tree-Node-Value">{value} </span></tpl>', { compiled: true })
    , childTpl: new Ext.XTemplate('<tpl for="."><div id="{id}-childcontainer" class=\"eBook-Meta-Tree-Node-children'
                , '<tpl if="!showExpanded"> eBook-hidden</tpl>'
                , '\"></div></tpl>', { compiled: true })
    , toggle: function() {
        if (this.hasChildren()) {
            this.el.toggleClass("eBook-Meta-Tree-Node-folder-open");
            this.childContainerEl.toggleClass("eBook-hidden");
        } else {
            this.editNode();
        }
    }
    , editNode: function() {
        this.editor.startEdit(this.getValueEl(), this.data.value);
    }
    , setEvents: function() {
        this.mon(this.el, {
            "click": this.onClick,
            // "dblclick": this.onDblClick,
            "contextmenu": this.onContextMenu,
            scope: this
        });
        if (!this.hasChildren()) {
            this.editor = new eBook.Editor(Ext.apply(this.editorcfg, {
                alignment: 'bl',
                field: this.editorField,
                listeners: { 'complete': { fn: this.onEditorComplete, scope: this} }
            }));
        }
    }
    , onClick: function(e) {
        this.toggle();
    }
});