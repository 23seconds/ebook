

eBook.Meta.ShareholdersMeetingEditor = Ext.extend(Ext.form.FormPanel, {
    classType: 'ShareHoldersMeetingDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName:'Meeting'
    , initComponent: function() {
        Ext.apply(this, {
            labelWidth: 200
            , labelPad: 10
            , border: 0
            , bodyStyle: 'padding:10px;'
            ,items:[{
                    xtype:'datefield'
                    , ref:'dte'
                    , name:'dte'
                    , value:''
                    , format:'d/m/Y'
                    , fieldLabel: eBook.Meta.MeetingEditor_Date
                    , disabled: eBook.Interface.isFileClosed()
                   },{
                    xtype:'timefield'
                    , ref: 'time'
                    , name:'time'
                    , value:''
                    , format:'H:i'
                    , fieldLabel: eBook.Meta.MeetingEditor_Time
                    , disabled: eBook.Interface.isFileClosed()
                   }, {
                    xtype:'metapersoneditor'
                    , ref: 'chm'
                    , fieldLabel: eBook.Meta.MeetingEditor_Chairman // eBook.Meta.MeetingEditor_Chairman
                    , propertyName:'chm'
                   }, {
                    xtype: 'metapersoneditor'
                    , ref: 'secr'
                    , fieldLabel: eBook.Meta.MeetingEditor_Secretary
                    , propertyName: 'secr'
                   }, {
                    xtype: 'metapersoneditor'
                    , ref: 'tell'
                    , fieldLabel: eBook.Meta.MeetingEditor_Teller
                    , propertyName: 'tell'
                   },{
                        xtype: 'numberfield'
                        , width: 250
                        , ref: 'shrs'
                        , name: 'shrs'
                        , allowDecimals: false
                        , value: 0
                        , fieldLabel: eBook.Meta.ShareHolderEditor_Shares
                        , disabled: eBook.Interface.isFileClosed()
                    }, { 
                    xtype: 'fieldset'
                     , ref: 'del'
                     , checkboxToggle:true
                     , checkboxName: 'del'
                     , title: eBook.Meta.MeetingEditor_Delayed
                     , autoHeight:true
                     , items : [{
                                    xtype:'datefield'
                                    , name: 'deldte'
                                    , ref: '../deldte'
                                    , value:''
                                    , format:'d/m/Y'
                                    , fieldLabel: eBook.Meta.MeetingEditor_Date// eBook.Meta.MeetingEditor_Date
                                    , disabled: eBook.Interface.isFileClosed()
                                   },{
                                    xtype:'timefield'
                                    , ref: '../deltime'
                                    , name: 'deltime'
                                    , value:''
                                    , format:'H:i'
                                    , fieldLabel: eBook.Meta.MeetingEditor_Time // eBook.Meta.MeetingEditor_Time
                                    , disabled: eBook.Interface.isFileClosed()
                                   }]
                    }]
        });

        eBook.Meta.ShareholdersMeetingEditor.superclass.initComponent.apply(this, arguments);
    }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        if (mobj) {
            if (mobj.dte) this.dte.setValue(Ext.data.Types.WCFDATE.convert(mobj.dte));
            this.time.setValue(mobj.time);
            this.chm.loadData(mobj);
            this.secr.loadData(mobj);
            this.tell.loadData(mobj);
            if (this.del.checkbox) {
                this.del.checkbox.dom.checked = mobj.del;
            } else {
                this.del.collapsed = !mobj.del;
            }
            if (mobj.deldte) this.deldte.setValue(Ext.data.Types.WCFDATE(mobj.deldte));
            this.deltime.setValue(mobj.deltime);
        }
    }
    , getData: function() {
        return {
            dte: !Ext.isEmpty(this.dte.getValue()) ? this.dte.getValue() : null
            ,time:this.time.getValue()
            , chm: this.chm.getData()
            , secr: this.secr.getData()
            , tell: this.tell.getData()
            , del: this.del.checkbox ? this.del.checkbox.dom.checked : false
            , deldte: !Ext.isEmpty(this.deldte.getValue()) ? this.deldte.getValue() : null
            ,deltime:this.deltime.getValue()
        };
    }
});

Ext.reg('metashareholdersmeetingeditor', eBook.Meta.ShareholdersMeetingEditor);