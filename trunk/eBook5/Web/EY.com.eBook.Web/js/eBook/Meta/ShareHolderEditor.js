

eBook.Meta.ShareHolderEditor = Ext.extend(Ext.form.FormPanel, {
    classType: 'ShareHolderDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName: ''
    , initComponent: function() {
        Ext.apply(this, {
            labelWidth: 200
            , labelPad: 10
            , border: 0
            , bodyStyle: 'padding:10px;'
            , items: [{  
                xtype: 'combo'
                    , ref: 'gr'
                    , name: 'gr'
                    , value: 'M'
                    , lazyRender: true
                    , mode: 'local'
                    , store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: [
                            'id',
                            'display'
                        ],
                        data: [['M', eBook.Meta.PersonEditor_Male], ['F', eBook.Meta.PersonEditor_Female]]
                    })
                    , valueField: 'id'
                    , displayField: 'display'
                    , typeAhead: false
                    , triggerAction: 'all'
                    , fieldLabel: eBook.Meta.PersonEditor_Gender // eBook.Meta.PersonEditor_Gender
            }, {
                xtype: 'textfield'
                    , width: 250
                    , ref: 'nme'
                    , name: 'nme'
                    , value: ''
                    , fieldLabel: eBook.Meta.PersonEditor_Name// eBook.Meta.PersonEditor_Name
            }, {
                xtype: 'numberfield'
                    , width: 250
                    , ref: 'shrs'
                    , name: 'shrs'
                    , allowDecimals: false
                    , value: 0
                    , fieldLabel: eBook.Meta.ShareHolderEditor_Shares
            }, {
                xtype: 'metapersoneditor'
                    , ref: 'repr'
                    , fieldLabel: eBook.Meta.ShareHolderEditor_RepresentedBy
                    , propertyName: 'repr'
}]
            });

            if (this.gridEditor) {
                Ext.apply(this, {
                    buttons: [{
                        //text:'Bewaar gegevens',
                        ref: '../saveButton',
                        iconCls: 'eBook-window-save-ico',
                        scale: 'large',
                        iconAlign: 'top',
                        handler: this.onSaveClick,
                        scope: this
                    }, {
                        //text: 'Annuleer',
                        ref: '../cancelButton',
                        iconCls: 'eBook-window-cancel-ico',
                        scale: 'large',
                        iconAlign: 'top',
                        handler: this.onCancelClick,
                        scope: this
                    }
                ]
                });
            }
            eBook.Meta.ShareHolderEditor.superclass.initComponent.apply(this, arguments);
        }
   , onSaveClick: function() {
       this.saveLine();
       return;
   }
    , onCancelClick: function() {
        if (!this.refOwner) return;
        if (this.refOwner.onSaveItem) {
            this.refOwner.onSaveItem();
        }
        return;
    }
    , loadLine: function(rec) {
        this.activeRecord = rec;
        this.loadObject(rec.data);
    }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        if (mobj) {
            this.loadObject(mobj);
        }

    }
    , loadObject: function(obj) {
        if (obj) {
            //his.itemid.setValue(obj.id);
            this.gr.setValue(obj.gr);
            this.nme.setValue(obj.nme);
            this.shrs.setValue(obj.shrs);
            this.repr.loadData(obj);
        }
    }
    , getData: function() {
        return {
            gr: this.gr.getValue()
            , nme: this.nme.getValue()
            , shrs: this.shrs.getValue()
            , repr: this.repr.getData()
        };
    }
    , getDataUn: function() {
        var repr = this.repr.getData();
        return {
            gr: this.gr.getValue()
            , nme: this.nme.getValue()
            , shrs: this.shrs.getValue()
            , repr_nme: repr.nme
            , repr_gr: repr.gr
            , repr: repr
        };
    }
    , saveLine: function() {
        if (!this.refOwner) return;

        if (this.activeRecord) {
            this.activeRecord.data = this.getData();
            this.activeRecord.markDirty();
            this.activeRecord.commit();
            this.refOwner.onSaveItem();
        } else {
            if (!this.refOwner) return;
            if (this.refOwner.gridpanel) {
                var rect = this.refOwner.gridpanel.store.recordType;
                var rec = new rect(this.getDataUn());
                rec.json = this.getData();
                this.refOwner.gridpanel.store.add(rec);
                this.refOwner.gridpanel.store.commitChanges();
                this.refOwner.onSaveItem();
            }
        }
    }
    , reset: function() {
        this.clearAll();
    }
    , clearAll: function() {
        // this.itemid.reset();
        this.gr.reset();
        this.nme.reset();
        this.shrs.reset();
        this.repr.reset();
        this.activeRecord = null;
    }
    });

Ext.reg('shareholdereditor', eBook.Meta.ShareHolderEditor);