
eBook.Meta.Window = Ext.extend(eBook.Window, {
    initComponent: function() {
        var fileClosed = eBook.Interface.currentFile.get('Closed');
        Ext.apply(this, {
             title: eBook.Meta.Window_Title
            , layout: 'fit'
            , width: 866
            , height: 450
            
            , tbar: [{
                //text:'Bewaar gegevens',
                ref: '../saveButton',
                iconCls: 'eBook-window-save-ico',
                scale: 'large',
                iconAlign: 'top',
                handler: this.onSaveClick,
                disabled: eBook.Interface.isFileClosed(),
                scope: this
}]
            , items: [{ xtype: 'tabpanel'
                     , activeTab: 0
                     , ref: 'tabpnl'
                     ,deferredRender :true
                     , items: [ {
                        
                         xtype: 'metagrid'
                                    , title: 'Peer group'
                                    , propertyName: 'pgroup'
                                    , columnsCfg: [{ mapping: 'ond', header: eBook.BusinessRelations.FormPanel_EnterpriseNumber }, { mapping: 'nme', header: 'Company name'}]
                                    , fieldsCfg: [{ name: 'ond' }, { name: 'nme'}]
                                    , editorPanel: {
                                            xtype: 'peergroupeditor'
                                            , title: 'Peer group - company'
                                            , propertyName: ''
                                            , disabled: eBook.Interface.isFileClosed()
                                     }
                     },{
                         xtype: 'metagrid'
                                    , title: 'Managers'
                                    , propertyName: 'mgrs'
                                    , columnsCfg: [{ mapping: 'gr', header: eBook.Meta.PersonEditor_Gender }, { mapping: 'nme', header: 'Manager'}]
                                    , fieldsCfg: [{ name: 'gr' }, { name: 'nme'}]
                                    , editorPanel: {
                                        xtype: 'metapersoneditor'
                                        , classType: 'MetaPersonDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
                                        , propertyName: ''
                                    }
                     }, {
                         xtype: 'metagrid'
                                    , title: 'Shareholders'
                                    , propertyName: 'shrhls'
                                    , columnsCfg: [{ mapping: 'gr', header: eBook.Meta.PersonEditor_Gender }, { mapping: 'nme', header: 'Shareholder' }
                                                   , { mapping: 'shrs', header: 'Shares'}
                                                   , { mapping: 'repr_nme', header: 'Represented by' }, { mapping: 'repr_gr', header: 'Representor Gender'}]
                                    , fieldsCfg: [{ name: 'gr' }, { name: 'nme' }, { name: 'shrs' }, { name: 'repr_nme', mapping: 'repr.nme' }, { name: 'repr_gr', mapping: 'repr.gr'}]
                                    , editorPanel: {
                                        xtype: 'shareholdereditor'
                                        , propertyName: ''
                                    }
                     }, {
                         xtype: 'metameetingeditor'
                                    , title: 'Board Meeting'
                                    , propertyName: 'bmeet'
                     }, {
                         xtype: 'metashareholdersmeetingeditor'
                                    , title: 'ShareHolders Meeting'
                                    , propertyName: 'smeet'
                     }, {
                         xtype: 'metameetingeditor'
                                    , title: 'Managers Meeting'
                                    , propertyName: 'mmeet'
                     }, {
                         xtype: 'metameetingeditor'
                                    , title: 'Partners Meeting'
                                    , propertyName: 'pmeet'
                     }
                     ]
}]
            });
            eBook.Meta.Window.superclass.initComponent.apply(this, arguments);
        }
    , show: function() {
        eBook.Meta.Window.superclass.show.call(this);
        this.load();
    }
    , load: function() {
        eBook.CachedAjax.request({
            url: eBook.Service.file + 'GetFileMeta'
                    , method: 'POST'
                    , params: Ext.encode({
                        cipdc: {
                            Id: eBook.Interface.currentFile.get('Id')
                            , Person: eBook.User.getActivePersonDataContract()
                        }
                    })
                    , callback: this.onLoadResponse
                    , scope: this
        });
    }
    , onLoadResponse: function(opts, success, resp) {
       // try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                var rc = robj.GetFileMetaResult;
                this.loadObject(rc);
                this.getEl().unmask();
            } else {
                eBook.Interface.showResponseError(resp, this.title);
                this.getEl().unmask();
            }
       /* } catch (e) {
            //console.log(e);
            this.getEl().unmask();
            eBook.Interface.showError(e.message, this.title);
        }*/

    }
    , loadObject: function(robj) {
        robj = robj.dta;
        this.tabpnl.items.each(function(it) {
            it.loadData(robj);
        }, this);
    }
    , onSaveClick: function() {
        this.getEl().mask(eBook.Meta.Window._Saving, 'x-mask-loading');
        var o = {};
        this.tabpnl.items.each(function(it) {
            o[it.propertyName] = it.getData();
        }, this);

        var f = {
            fid: eBook.Interface.currentFile.get('Id')
            , dta: o
        };
        eBook.CachedAjax.request({
                url: eBook.Service.file + 'SaveFileMeta'
                , method: 'POST'
                , params: Ext.encode({
                    cfmdc: {
                        fmdc: f
                        , Person: eBook.User.getActivePersonDataContract()
                    }
                })
                , callback: this.onSaveResponse
                , scope: this
        });

    }
    , onSaveResponse: function(opts, success, resp) {
        try {
            if (success) {
                var robj = Ext.decode(resp.responseText);
                var rc = robj.SaveFileMetaResult;
                this.getEl().unmask();
            } else {
                eBook.Interface.showResponseError(resp, this.title);
                this.getEl().unmask();
            }
        } catch (e) {
            //console.log(e);
            this.getEl().unmask();
            eBook.Interface.showError(e.message, this.title);
        }

    }

    });    
