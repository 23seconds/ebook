
eBook.Meta.ExampleData = {
        id:1
        ,keyId:'k0'
        ,text:'root'
        ,value:''
        ,children: [
            { id: 2
            , keyId: 'k1'
            ,text:'Property of meta'
            , leaf: true
            , dataType: 'shorttext'
            , isList: true
            ,listId: '9c422191-23ac-46fd-b672-1f056e3b43b8'//'9b9547a3-b37b-4094-b1b6-755d74546a60'
            ,value:null
            }, {
                id:3
                , keyId: 'k2'
                ,text:'container'
                ,value:''
                , children:[ 
                        {id:4
                        , keyId: 'k3'
                        , text: 'child'
                        , dataType: 'boolean'
                        ,leaf:true
                        ,value:false
                        }, { id: 5
                        , keyId: 'k4'
                        , text: 'child2'
                        , dataType: 'number'
                        , leaf: true
                        , value: 123.2
}]
            }
        ]
};





eBook.Meta.TreeEditor = Ext.extend(Ext.Component, {
    folderClass: 'eBook-Meta-Tree-Node-folder'
    , nodeSelector: '.eBook-Meta-Tree-Node'
    , showExpanded: false
    // , root: new eBook.Meta.Node({})
    , rootVisible: false
    , indent: 10
    , onRender: function(ct, position) {
        eBook.Meta.TreeEditor.superclass.onRender.call(this, ct, position);
        this.el.addClass("eBook-Meta-Tree");
        this.renderRoot(this.el);
       // this.setEvents();
    }
    , renderRoot: function(parentElement) {

        this.root.onRender(parentElement);
    }
    
    
});