


eBook.Meta.PeerGroupEditor = Ext.extend(Ext.form.FormPanel, {
classType: 'PeerGroupItemDataContract:#EY.com.eBook.API.Contracts.Data.Meta'
    , propertyName: ''
    , initComponent: function() {
        Ext.apply(this, {
            labelWidth: 200
            , labelPad: 10
            , border: 0
            , bodyStyle: 'padding:10px;'
            , items: [ {
                xtype: 'textfield'
                    , width: 250
                    , ref: 'ond'
                    , name: 'ond'
                    , value: ''
                    ,allowBlank:false
                    , fieldLabel: eBook.BusinessRelations.FormPanel_EnterpriseNumber// eBook.Meta.PersonEditor_Name
            }, {
                xtype: 'textfield'
                    , width: 250
                    , ref: 'nme'
                    , name: 'nme'
                    , value: ''
                    , allowBlank: false
                    , fieldLabel: eBook.Meta.PersonEditor_Name// eBook.Meta.PersonEditor_Name
                }]
            });

            if (this.gridEditor) {
                Ext.apply(this, {
                    buttons: [{
                        //text:'Bewaar gegevens',
                        ref: '../saveButton',
                        iconCls: 'eBook-window-save-ico',
                        scale: 'large',
                        iconAlign: 'top',
                        handler: this.onSaveClick,
                        scope: this
                    }, {
                        //text: 'Annuleer',
                        ref: '../cancelButton',
                        iconCls: 'eBook-window-cancel-ico',
                        scale: 'large',
                        iconAlign: 'top',
                        handler: this.onCancelClick,
                        scope: this
                    }
                ]
                });
            }
            eBook.Meta.PeerGroupEditor.superclass.initComponent.apply(this, arguments);
        }
   , onSaveClick: function() {
       this.saveLine();
       return;
   }
    , onCancelClick: function() {
        if (!this.refOwner) return;
        if (this.refOwner.onSaveItem) {
            this.refOwner.onSaveItem();
        }
        return;
    }
    , loadLine: function(rec) {
        this.activeRecord = rec;
        this.loadObject(rec.data);
    }
    , loadData: function(parentObj) {
        var mobj = parentObj[this.propertyName];
        if (mobj) {
            this.loadObject(mobj);
        }

    }
    , loadObject: function(obj) {
        if (obj) {
            //his.itemid.setValue(obj.id);
            this.ond.setValue(obj.ond);
            this.nme.setValue(obj.nme);
        }
    }
    , getData: function() {
        return {
            ond: this.ond.getValue()
            , nme: this.nme.getValue()
        };
    }
    , getDataUn: function() {
        
        return {
            ond: this.ond.getValue()
            , nme: this.nme.getValue()
        };
    }
    , saveLine: function() {
        if (!this.refOwner) return;

        if (this.activeRecord) {
            this.activeRecord.data = this.getData();
            this.activeRecord.markDirty();
            this.activeRecord.commit();
            this.refOwner.onSaveItem();
        } else {
            if (!this.refOwner) return;
            if (this.refOwner.gridpanel) {
                var rect = this.refOwner.gridpanel.store.recordType;
                var rec = new rect(this.getDataUn());
                rec.json = this.getData();
                this.refOwner.gridpanel.store.add(rec);
                this.refOwner.gridpanel.store.commitChanges();
                this.refOwner.onSaveItem();
            }
        }
    }
    , reset: function() {
        this.clearAll();
    }
    , clearAll: function() {
        // this.itemid.reset();
        this.ond.reset();
        this.nme.reset();
        this.activeRecord = null;
    }
    });

    Ext.reg('peergroupeditor', eBook.Meta.PeerGroupEditor);