
eBook.Meta.Grid = Ext.extend(Ext.Panel, {
    propertyName: ''
    , initComponent: function() {
        this.editorPanel = Ext.apply(this.editorPanel, { ref: 'editor', gridEditor: true });
        Ext.apply(this, {
            layout: 'card'
            , activeItem: 0
            , items: [{
                xtype: 'metagridpanel'
                       , ref: 'gridpanel'
                       , columns: this.columnsCfg
                       , fieldsCfg: this.fieldsCfg
            }, this.editorPanel]
        });
        eBook.Meta.Grid.superclass.initComponent.apply(this, arguments);
    }
    , onEditItem: function(idx) {
        var rec = this.gridpanel.store.getAt(idx);
        this.editor.loadLine(rec);
        this.getLayout().setActiveItem(1);
    }
    , onAddRow: function() {
        this.editor.clearAll();
        this.getLayout().setActiveItem(1);
    }
    , onSaveItem: function() {
        this.getLayout().setActiveItem(0);
        this.editor.clearAll();
    }
    , loadData: function(parentObj) {
        this.gridpanel.store.loadData(parentObj[this.propertyName]);
    }
    , getData: function(parentObj) {
        var arr = [];
        var g = this;
        this.gridpanel.store.each(function(rec) {
            var o = rec.data;
            arr.push(o);
        });
        return arr;
    }
});
Ext.reg('metagrid', eBook.Meta.Grid);

eBook.Meta.GridPanel = Ext.extend(Ext.grid.GridPanel, {
    initComponent: function() {
        Ext.apply(this, {
            tbar: new Ext.Toolbar({
                items: [
                                {
                                    ref: 'additem',
                                    text: eBook.Meta.GridPanel_AddItem,
                                    iconCls: 'eBook-icon-add-16',
                                    scale: 'small',
                                    iconAlign: 'left',
                                    handler: this.onAddRow,
                                    disabled: eBook.Interface.isFileClosed(),
                                    scope: this
                                }, {
                                    ref: 'edititem',
                                    text: eBook.Meta.GridPanel_EditItem,
                                    iconCls: 'eBook-icon-edit-16',
                                    scale: 'small',
                                    iconAlign: 'left',
                                    disabled: true,
                                    handler: this.onEditRow,
                                    
                                    scope: this
                                }, {
                                    ref: 'deleteitem',
                                    text:  eBook.Meta.GridPanel_DeleteItem,
                                    iconCls: 'eBook-icon-delete-16',
                                    scale: 'small',
                                    iconAlign: 'left',
                                    disabled: true,
                                    handler: this.onDeleteRow,
                                    scope: this
                                }
                            ]
            })
            , store: { xtype:'jsonstore',fields: this.fieldsCfg }
            , sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
                        , listeners: {
                            'selectionchange': {
                                fn: this.onRowSelect
                                    , scope: this
                            }
                        }
            })
        });

        eBook.Meta.GridPanel.superclass.initComponent.apply(this, arguments);
        this.on('rowdblclick', this.onEditRow);
    }
    , onRowSelect: function(sm) {
        /* TOOLBAR UPDATE */
        if (this.getTopToolbar()) {

            var ed = this.getTopToolbar().edititem;
            var dl = this.getTopToolbar().deleteitem;
            if (!sm.hasSelection()) {
                dl.disable();
                ed.disable();
            } else {
                dl.enable();
                ed.enable();
                if (!Ext.isEmpty(this.rowDisabledField)) {
                    var r = sm.getSelected();
                    if (r.get(this.rowDisabledField)) {
                        dl.disable();
                        ed.disable();
                        sm.clearSelections();
                    }
                }

            }
        }
    }
    , onEditRow: function(grid, ridx, e) {
        this.refOwner.onEditItem(ridx);
    }
    , onAddRow: function() {
        this.refOwner.onAddRow(); //bubble
    }
    , onDeleteRow: function() {
        var r = this.getSelectionModel().getSelected();
        this.store.remove(r);
        this.getSelectionModel().clearSelections(true);
    }
});

Ext.reg('metagridpanel', eBook.Meta.GridPanel);