

eBook.Health.Messages = Ext.extend(Ext.DataView, {
    initComponent: function() {
        //var worksheetName = eBook.Interface.center.fileMenu.getWorksheetRecord
        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                autoDestroy: true,
                root: 'GetMessagesWithoutNotesResult',
                fields: eBook.data.RecordTypes.Messages,
                proxy: new eBook.data.WcfProxy({   // proxy mogelijks algemeen maken? aka eBook.Response
                    url: eBook.Service.file + 'GetMessagesWithoutNotes'
                     , criteriaParameter: 'cfdc'
                     , method: 'POST'
                    //                     , baseParams: { FileId: eBook.currentFile.get('Id')
                    //                                    , Culture: eBook.Interface.Culture
                    //                     }
                })
                , listeners: { datachanged: { fn: this.onDataChanged, scope: this }
                            , afterrender: { fn: this.updateHealthClass, scope: this }
                            , click: { fn: this.onItemClick, scope: this }
                }
            })
            , id: 'eBook-file-healthmonitor'
            , tpl: new Ext.XTemplate(
		        '<tpl for=".">',
		            '<div class="ebook-message">',
		                '<tpl if="this.test(values)"><div><b>Worksheet:</b> {Worksheet}</div></tpl>', //
		                '<tpl if="CollectionText"><div><b>Tab:</b> {CollectionText}</div></tpl>', //
		                '<tpl if="FieldText"><div><b>Field:</b> {FieldText}</div></tpl>', //
                        '<div class="ebook-message-source-align ebook-message-source-{ConnectionType}-type-{Type}" id="{MessageId}">',
		                    '{Text}',
		                    
		                '</div>',
		            '</div>',
                '</tpl>',
                '<div id="eBook-health-joint" class="x-tab-joint" style="height: 100px; left: -2px; top: 11px;"></div>',
                '<div class="x-clear"></div>',
                {
                    test: function(values) {
                        var record = eBook.Interface.center.fileMenu.getWorksheetRecord(values.Guid);
                        values.Worksheet = record.data.name;
                        return values;
                    }
                }
	        )
	        , itemSelector: 'div.ebook-message'
	        , overClass: 'ebook-message-over'
            , cls: 'health-view'
            , data: []
            , border: false
            , autoScroll: true
        });
        eBook.Health.Messages.superclass.initComponent.apply(this, arguments);
        Ext.ComponentMgr.register(this);
    }
    , healthClass: ''
    , onItemClick: function(dv, idx, nde, e) {
        var rec = this.store.getAt(idx);
        if (rec.get('ConnectionType') == '0') {

            eBook.Interface.center.fileMenu.openWorksheetSh(rec.get('Guid'), rec.get('Collection'), rec.get('RowIndex'), rec.get('Field'));
        }
    }
    , onDataChanged: function() {

        if (this.store.query('Type', '0').length > 0) {
            this.healthClass = 'ebook-health-icon-ERROR';
        } else if (this.store.query('Type', '1').length > 0) {
            this.healthClass = 'ebook-health-icon-WARNING';
        } else if (this.store.query('Type', '2').length > 0) {
            this.healthClass = 'ebook-health-icon-INFO';
        }
        this.updateHealthClass();
        eBook.Interface.redrawWorksheetsView();
        this.refresh();
    }
    , updateHealthClass: function() {
        if (this.rendered) {
            var oel = this.ownerCt.getEl().parent();
            var icon = oel.child('.ebook-health-icon');
            icon.removeClass('ebook-health-icon-ERROR');
            icon.removeClass('ebook-health-icon-WARNING');
            icon.removeClass('ebook-health-icon-INFO');
            icon.addClass(this.healthClass);
        }
    }
    , getItemMessageType: function(id) {
        var its = this.store.query('Guid', id);
        if (its.getCount() == 0) {
            its = this.store.query('RowIndex', id);
        }
        var err = false, warn = false, info = false;
        its.each(function(it) {
            if (it.get('Type') == '0') err = true;
            if (it.get('Type') == '1') warn = true;
            if (it.get('Type') == '2') info = true;
        }, this);
        if (err) return 'ERROR';
        if (warn) return 'WARNING';
        if (info) return 'INFO';
        return '';
    }
    , checkHealthTab: function(tpeId, name) {
        var tabs = this.store.queryBy(function(rec) {
            return rec.get('Collection') == name && rec.get('Guid').toLowerCase() == tpeId.toLowerCase();
        }, this);


        if (tabs.getCount() > 0) {
            var err = false, warn = false, info = false;
            tabs.each(function(tab) {
                if (tab.get('Type') == '0') err = true;
                if (tab.get('Type') == '1') warn = true;
                if (tab.get('Type') == '2') info = true;
            }, this);
            if (err) { return 'eb-message-type-ERROR'; }
            else if (warn) { return 'eb-message-type-WARNING'; }
            else if (info) { return 'eb-message-type-INFO'; }
        }
        return '';
    }
    , getFieldsByTab: function(tpeId, tab) {
        var fields = new Array();
        fields = this.store.queryBy(function(rec) {
            return rec.get('Collection') == tab && rec.get('Guid').toLowerCase() == tpeId.toLowerCase();
        });
        if (fields.getCount() > 0) {
            return fields;
        }
        return '';
    }
});
Ext.reg('health-messages', eBook.Health.Messages);

eBook.Health.Panel = Ext.extend(Ext.Panel, {
    initComponent: function() {
        Ext.apply(this, {
            layout: 'border'
            , cls: 'eBook-health-region-border'
            , items: [{
                xtype: 'panel'
                         , cls: 'eBook-health-center'
                        , border: false
                        , layout: 'fit', region: 'center'
                        , items: [{ xtype: 'health-messages',
                            style: 'border:1px; solid #999;',
                            ref: '../messages',
                            border: false}]
            }
                    , { xtype: 'panel'
                        , cls: 'eBook-health-selector'
                        , bodyStyle: 'background-color:transparent;'
                         , border: false,
                        html: '<div class="ebook-health-icon ebook-health-selected"></div>'
                         , region: 'east', width: 110
                    }
                    ]
        });
        eBook.Health.Panel.superclass.initComponent.apply(this, arguments);
    }
    , getItemMessageType: function(id) {
        return this.messages.getItemMessageType(id);
    }
    , checkHealthTab: function(tpeId,name) {
        return this.messages.checkHealthTab(tpeId, name);
    }
    , getFieldsByTab: function(tpeId, tab) {
        return this.messages.getFieldsByTab(tpeId, tab);
    }
});

Ext.reg('health', eBook.Health.Panel);



