
/**
* eBook.lazyLoad
*
* Based upon Ext.ux.LazyLoad by Jerry Sosa
*
*/

eBook.LazyLoad = function() {

    // -- Group: Private Variables -----------------------------------------------

    /*
    Object: d
    Shorthand reference to the browser's *document* object.
    */
    var d = document,

    /*
    Object: pending
    Pending request object, or null if no request is in progress.
    */
  pending = null,

    /*
    Array: queue
    Array of queued load requests.
    */
  queue = [];



    return {
        // -- Group: Public Methods ------------------------------------------------

        /*
        Method: load
        Loads the specified script(s) and runs the specified callback function
        when all scripts have been completely loaded.

    Parameters:
        urls     - URL or array of URLs of scripts to load
        callback - function to call when loading is complete
        obj      - (optional) object to pass to the callback function
        scope    - (optional) if true, *callback* will be executed in the scope
        of *obj* instead of receiving *obj* as an argument.
        */
        load: function(urls, callback, obj, scope) {
            var head = d.getElementsByTagName('body')[0],
          i, script;

            if (urls) {
                // Cast urls to an Array.
                urls = Ext.isArray(urls) ? urls : [urls];

                // Create a request object for each URL. If multiple URLs are specified,
                // the callback will only be executed after the last URL is loaded.
                for (i = 0; i < urls.length; ++i) {
                    queue.push({
                        'url': urls[i],
                        'callback': i === urls.length - 1 ? callback : null,
                        'obj': obj,
                        'scope': scope
                    });
                }
            }

            // If a previous load request is currently in progress, we'll wait our
            // turn. Otherwise, grab the first request object off the top of the
            // queue.
            if (pending) {
                return;
            } else {
                pending = queue.shift();
                if (!pending) return;
            }

            // Determine browser type and version for later use.


            // Load the script.
            script = d.createElement('script');
            script.src = pending.url;

            if (Ext.isIE) {
                // If this is IE, watch the last script's ready state.
                script.onreadystatechange = function() {
                    if (this.readyState === 'loaded' ||
              this.readyState === 'complete') {
                        eBook.LazyLoad.requestComplete();
                    }
                };
            } else { //if (Ext.isGecko || (Ext.isSafari && !Ext.isSafari2)) {
                // Firefox and Safari 3.0+ support the load/error events on script
                // nodes.
                script.onload = eBook.LazyLoad.requestComplete;
                script.onerror = eBook.LazyLoad.requestComplete;
            }

            head.appendChild(script);
            /*
            if (!Ext.isIE && !Ext.isGecko && !(Ext.isSafari && !Ext.isSafari2)) {
                // Try to use script node blocking to figure out when things have
                // loaded. This works well in Opera, but may or may not be reliable in
                // other browsers. It definitely doesn't work in Safari 2.x.
                script = d.createElement('script');
                script.appendChild(d.createTextNode('eBook.LazyLoad.requestComplete.defer(500,eBook.LazyLoad);'));
                head.appendChild(script);
            }
            */
        },

        /*
        Method: loadOnce
        Loads the specified script(s) only if they haven't already been loaded
        and runs the specified callback function when loading is complete. If all
        of the specified scripts have already been loaded, the callback function
        will not be executed unless the *force* parameter is set to true.

    Parameters:
        urls     - URL or array of URLs of scripts to load
        callback - function to call when loading is complete
        obj      - (optional) object to pass to the callback function
        scope    - (optional) if true, *callback* will be executed in the scope
        of *obj* instead of receiving *obj* as an argument
        force    - (optional) if true, *callback* will always be executed, even if
        all specified scripts have already been loaded
        */
        loadOnce: function(urls, callback, obj, scope, force) {
            var newUrls = [],
          scripts = d.getElementsByTagName('script'),
          i, j, loaded, url;

            urls = Ext.isArray(urls) ? urls : [urls];

            for (i = 0; i < urls.length; ++i) {
                loaded = false;
                url = urls[i];

                for (j = 0; j < scripts.length; ++j) {
                    if (url === scripts[j].src) {
                        loaded = true;
                        break;
                    }
                }

                if (!loaded) {
                    newUrls.push(url);
                }
            }

            if (newUrls.length > 0) {
                eBook.LazyLoad.load(newUrls, callback, obj, scope);
            } else if (force) {
                if (obj) {
                    if (scope) {
                        callback.call(scope,obj);
                    } else {
                        callback.call(window, obj);
                    }
                } else {
                    callback.call();
                }
            }
        },

        /*
        Method: requestComplete
        Handles callback execution and cleanup after a request is completed. This
        method should not be called manually.
        */
        requestComplete: function() {
            // Execute the callback.
            
            var pd = pending;
            pending = null;

            // Execute the next load request on the queue (if any).
            if (queue.length) {
                eBook.LazyLoad.load.defer(10, this);
            }
            if (pd.callback) {
                if (Ext.isDefined(pd.obj)) {
                    if (pd.scope) {
                        pd.callback.call(pd.scope, pd.obj);
                    } else {
                        pd.callback.call(window, pd.obj);
                    }
                } else {
                    if (pd.scope) {
                        pd.callback.call(pd.scope);
                    } else {
                        pd.callback.call(window);
                    }
                }
            }


        }
    };
} ();