/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>

eBook.Worksheet.Fields.NumberField = function(config) {
    if (config.attributes) {
        if (config.attributes["minimumValue"]) config.minValue = parseFloat(config.attributes["minimumValue"]);
        if (config.attributes["maximumValue"]) config.maxValue = parseFloat(config.attributes["maximumValue"]);
        if (config.attributes["negative"]) config.allowNegative = config.attributes["negative"].toLower() == 'true';
        if (config.attributes["decimalPrecision"]) config.decimalPrecision = parseInt(config.attributes["decimalPrecision"]);
        //autoStripChars
        
    }
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'changed': {
                fn: this.handleOnEventTrigger,
                scope: this,
                delay: 100
            }
        };
    }
    config = this.handleConstruction(config);
    eBook.Worksheet.Fields.NumberField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.NumberField, Ext.form.NumberField, eBook.Worksheet.Fields.Field);
Ext.reg('ewnumberfield', eBook.Worksheet.Fields.NumberField);