/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>

eBook.Worksheet.Fields.PercentField = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.minValue = 0;
    config.maxValue = 100;
    config.allowNegative = false;
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'changed': {
                fn: this.handleOnEventTrigger,
                scope: this,
                delay: 100
            }
        };
    }
    config = this.handleConstruction(config);
    eBook.Worksheet.Fields.PercentField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.PercentField, Ext.form.NumberField, eBook.Worksheet.Fields.Field);
Ext.reg('ewpercentfield', eBook.Worksheet.Fields.PercentField);