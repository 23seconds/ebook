/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>

eBook.Worksheet.Fields.SpacerField = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.text = ' ';
    config = this.handleConstruction(config);
    config.hideLabel = true;
    eBook.Worksheet.Fields.SpacerField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.SpacerField, Ext.form.Label, eBook.Worksheet.Fields.Field);
Ext.reg('ewSpacerField', eBook.Worksheet.Fields.SpacerField);