/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>

eBook.Worksheet.Fields.Label = function(config) {
    if (!Ext.isDefined(config)) config = {};
    config.isLabel = true;
    config = this.handleConstruction(config);
    config.autoWidth = true;
    if (config.labelStyle) config.style = config.labelStyle;
    config.disabledClass = 'eBook-ws-disabled';
    eBook.Worksheet.Fields.Label.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.Label, Ext.form.Label, eBook.Worksheet.Fields.Field);
Ext.reg('ewLabelField', eBook.Worksheet.Fields.Label);