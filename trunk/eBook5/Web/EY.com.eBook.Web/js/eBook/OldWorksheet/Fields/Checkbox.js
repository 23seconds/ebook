/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>

/*
eBook.Worksheet.Fields.Checkbox = function(config) {
config.decimalPrecision = 2;
if (config.attributes) {
if (config.attributes["minimumValue"]) config.minValue = parseFloat(config.attributes["minimumValue"]);
if (config.attributes["maximumValue"]) config.maxValue = parseFloat(config.attributes["maximumValue"]);
if (config.attributes["negative"]) config.allowNegative = config.attributes["negative"].toLower() == 'true';
}
}
*/

eBook.Worksheet.Fields.Checkbox = function(config) {
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'check': {
                fn: this.handleOnEventTrigger,
                scope: this
            }
        };
    }

    if (Ext.isDefined(config) && Ext.isDefined(config.attributes)) {
        if (Ext.isDefined(config.attributes.LabelSide)) {
            if (config.attributes.LabelSide == "right") {
                config.boxLabel = config.fieldLabel;
                config.hideLabel = true;
            }
        }
    }
    config = this.handleConstruction(config);

    eBook.Worksheet.Fields.Checkbox.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.Checkbox, Ext.form.Checkbox, eBook.Worksheet.Fields.Field);
Ext.reg('ewcheckbox', eBook.Worksheet.Fields.Checkbox);