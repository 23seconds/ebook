/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>
/// <reference path="NumberField.js"/>

eBook.Worksheet.Fields.CurrencyField = function(config) {
    config.decimalPrecision = 2;
    if (config.attributes) {
        if (config.attributes["minimumValue"]) config.minValue = parseFloat(config.attributes["minimumValue"]);
        if (config.attributes["maximumValue"]) config.maxValue = parseFloat(config.attributes["maximumValue"]);
        if (config.attributes["negative"]) config.allowNegative = config.attributes["negative"].toLower() == 'true';
    }
    eBook.Worksheet.Fields.CurrencyField.superclass.constructor.call(this, config);
};

Ext.extend(eBook.Worksheet.Fields.CurrencyField, eBook.Worksheet.Fields.NumberField);
Ext.reg('ewCurrencyField', eBook.Worksheet.Fields.CurrencyField);