/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>


eBook.Worksheet.Fields.Booking = function(config) {
    this.isBookingField = true;
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'check': {
                fn: this.handleOnEventTrigger,
                scope: this,
                delay: 100
            }
        };
    }
    config = this.handleConstruction(config);
    eBook.Worksheet.Fields.Booking.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.Booking, Ext.form.Checkbox, eBook.Worksheet.Fields.Field);
Ext.reg('ewBooking', eBook.Worksheet.Fields.Booking);