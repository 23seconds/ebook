/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>

eBook.Worksheet.Fields.Field = {
    attributes: []

    , handleConstruction: function(config) {
        // read attributes
        if (Ext.isDefined(config) && Ext.isDefined(config.attributes)) {
            if (Ext.isDefined(config.attributes.LabelSeperate) && config.isLabel && Ext.isDefined(config.attributes.LabelClass)) {
                config.cls = config.attributes.LabelClass;
            }
            if (Ext.isDefined(config.attributes.LabelSeperate) && !config.isLabel) {
                config.hideLabel = config.attributes.LabelSeperate == 'true';
            }
            if (Ext.isDefined(config.attributes.ItemWidth)) {
                config.width = parseInt(config.attributes.ItemWidth);
            }
            if (Ext.isDefined(config.attributes.ItemColspan)) {
                config.colspan = parseInt(config.attributes.ItemColspan);
            }
            if (Ext.isDefined(config.attributes.LabelSeperate) && config.isLabel && Ext.isDefined(config.attributes.LabelColspan)) {
                config.colspan = parseInt(config.attributes.LabelColspan);
            }
            if (Ext.isDefined(config.attributes.ItemRowspan)) {
                config.rowspan = parseInt(config.attributes.ItemRowspan);
            }
            if (Ext.isDefined(config.attributes.LabelSeperate) && config.isLabel && Ext.isDefined(config.attributes.LabelRowSpan)) {
                config.rowspan = parseInt(config.attributes.LabelRowSpan);
            }
            if (Ext.isDefined(config.attributes.column)) {
                config.column = parseInt(config.attributes.column);
            }
            if (Ext.isDefined(config.attributes.ItemClass) && !config.isLabel) {
                config.cls = config.attributes.ItemClass;
            }
            if (Ext.isDefined(config.attributes.LabelHidden && !config.isLabel)) {
                config.hideLabel = config.attributes.LabelHidden == 'true';
            }
            if (Ext.isDefined(config.attributes.Disabled)) {
                if (config.attributes.Disabled == 'true' || config.attributes.Disabled == 'false') {
                    config.disabled = config.attributes.Disabled == 'true';
                }
                else {
                    config.disabledField = config.attributes.Disabled;
                }
            }

            // construct style
            var style = this.getStyleFromAttributes(config.attributes, 'Item');
            var labelStyle = this.getStyleFromAttributes(config.attributes, 'Label');
            if (config.isLabel) {
                if (labelStyle.length > 0) {
                    config.labelStyle = labelStyle;
                } else {
                    if (style.length > 0) { config.labelStyle = style; }
                }
            } else {
                if (!Ext.isDefined(config.hideLabel) || config.hideLabel != false) {
                    if (labelStyle.length > 0) {
                        config.labelStyle = labelStyle;
                    } else {
                        if (style.length > 0) { config.labelStyle = style; }
                    }
                }

            }
            if (Ext.isDefined(config.attributes.InterfaceEvents)) {
                var events = config.attributes.InterfaceEvents.split(',');
                if (!Ext.isArray(events)) {
                    events = [config.attributes.InterfaceEvents];
                }
                if (events.length > 0) {
                    config.interfaceEvents = {};
                    for (var i = 0; i < events.length; i++) {
                        if (Ext.isDefined(config.attributes['InterfaceEvent' + events[i] + '_Field'])
                            && Ext.isDefined(config.attributes['InterfaceEvent' + events[i] + '_Values'])) {
                            config.interfaceEvents[events[i]] = {
                                field: config.attributes['InterfaceEvent' + events[i] + '_Field']
                                    , values: config.attributes['InterfaceEvent' + events[i] + '_Values']
                            };
                        }
                    }
                }
            }
        }
        return config;
    }
    , handleOnEventTrigger: function() {
        if (this.ownerCt) {
            this.ownerCt.onEventTrigger(this.name, this.getValue());
        }
    }
    , onShow: function() {
        this.getVisibilityEl().removeClass('x-hide-' + this.hideMode);
        if (this.disabledField) {
            var fpanel = this.findParentByType(eBook.Worksheet.FormPanel);
            if (!fpanel) return;
            var fld = fpanel.getForm().findField(this.disabledField);
            if (!fld) return;
            var val = fld.getValue();
            if (!Ext.isBoolean(val)) {
                val = val == "true" || val == 1;
            }
            if (val) {
                this.disable();
            } else {
                this.enable();
            }
        }
    }
    , getStyleFromAttributes: function(attribs, tpe) { //tpe = 'Item' / 'Label'
        var style = '';
        if (Ext.isDefined(attribs[tpe + 'Style'])) {
            if (attribs[tpe + 'Style'].length > 0) {
                style = attribs[tpe + 'Style'];
                style = style.lastIndexOf(';') < style.length - 1 ? style + ';' : style;
            }
        }
        if (Ext.isDefined(attribs[tpe + 'Align'])) {
            style += 'textalign:' + attribs[tpe + 'Align'] + ';';
        }
        if (Ext.isDefined(attribs[tpe + 'TextDecoration'])) {
            style += 'textdecoration:' + attribs[tpe + 'TextDecoration'] + ';';
        }
        if (Ext.isDefined(attribs[tpe + 'PaddingLeft'])) {
            style += 'padding-left:' + parseInt(attribs[tpe + 'PaddingLeft']) + 'px;';
        }
        if (Ext.isDefined(attribs[tpe + 'PaddingRight'])) {
            style += 'padding-right:' + parseInt(attribs[tpe + 'PaddingRight']) + 'px;';
        }
        if (Ext.isDefined(attribs[tpe + 'PaddingTop'])) {
            style += 'padding-top:' + parseInt(attribs[tpe + 'PaddingTop']) + 'px;';
        }
        if (Ext.isDefined(attribs[tpe + 'PaddingBottom'])) {
            style += 'padding-bottom:' + parseInt(attribs[tpe + 'PaddingBottom']) + 'px;';
        }
        if (Ext.isDefined(attribs[tpe + 'Style'])) {
            style += attribs[tpe + 'Style'];
            if (style.charAt[style.length - 1] != ';') style += ';';
        }

        return style;
    }
};

