/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>

/*
eBook.Worksheet.Fields.DateField = function(config) {
    config.decimalPrecision = 2;
    if (config.attributes) {
        if (config.attributes["minimumValue"]) config.minValue = parseFloat(config.attributes["minimumValue"]);
        if (config.attributes["maximumValue"]) config.maxValue = parseFloat(config.attributes["maximumValue"]);
        if (config.attributes["negative"]) config.allowNegative = config.attributes["negative"].toLower() == 'true';
    }
}
*/
eBook.Worksheet.Fields.DateField = function(config) {
    config.width = config.width ? config.width : 100;
    config.format = 'd/m/Y';
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'changed': {
                fn: this.handleOnEventTrigger,
                scope: this,
                delay: 100
            }
        };
    }
    config = this.handleConstruction(config);
    eBook.Worksheet.Fields.DateField.superclass.constructor.call(this, config);
};

Ext.extend(eBook.Worksheet.Fields.DateField, Ext.form.DateField, eBook.Worksheet.Fields.Field);
//Ext.extend(eBook.Worksheet.Fields.DateField, eBook.Worksheet.Fields.Field);
Ext.reg('ewdatefield', eBook.Worksheet.Fields.DateField);