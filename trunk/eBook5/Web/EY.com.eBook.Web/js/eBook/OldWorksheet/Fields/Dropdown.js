
eBook.Worksheet.Fields.Dropdown = function(config) {
    if (Ext.isDefined(config) && Ext.isDefined(config.attributes)) {
        if (Ext.isDefined(config.attributes.ListWidth)) {
            config.listWidth = parseInt(config.attributes.ListWidth);
        }
    }
    config.resizable = Ext.isDefined(config.resizable) ? config.resizable : true;
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'select': {
                fn: this.handleOnEventTrigger,
                scope: this,
                delay: 100
            }
        };
    }
    config.nullable=true;
    config = this.handleConstruction(config);
    eBook.Worksheet.Fields.Dropdown.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.Dropdown, Ext.form.ComboBox, eBook.Worksheet.Fields.Field);

Ext.override(eBook.Worksheet.Fields.Dropdown, {
    show: function() {
        eBook.Worksheet.Fields.Dropdown.superclass.show.call(this);
        this.getEl().dom.style.width = "";
    }
});
Ext.reg('ewdropdown', eBook.Worksheet.Fields.Dropdown);