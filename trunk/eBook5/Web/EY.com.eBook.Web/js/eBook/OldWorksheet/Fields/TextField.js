/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="Field.js"/>

/*
eBook.Worksheet.Fields.Checkbox = function(config) {
config.decimalPrecision = 2;
if (config.attributes) {
if (config.attributes["minimumValue"]) config.minValue = parseFloat(config.attributes["minimumValue"]);
if (config.attributes["maximumValue"]) config.maxValue = parseFloat(config.attributes["maximumValue"]);
if (config.attributes["negative"]) config.allowNegative = config.attributes["negative"].toLower() == 'true';
}
}
*/


eBook.Worksheet.Fields.TextField = function(config) {
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'changed': {
                fn: this.handleOnEventTrigger,
                scope: this,
                delay: 100
            }
        };
    }
    config = this.handleConstruction(config);
    eBook.Worksheet.Fields.TextField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.TextField, Ext.form.TextField, eBook.Worksheet.Fields.Field);
Ext.reg('ewtextfield', eBook.Worksheet.Fields.TextField);



eBook.Worksheet.Fields.TextAreaField = function(config) {
    if (config.hasInterfaceEvents) {
        config.listeners = {
            'changed': {
                fn: this.handleOnEventTrigger,
                scope: this,
                delay: 100
            }
        };

    }
    config = this.handleConstruction(config);
    config.grow = true;
    config.width = 400;
    config.growMin = 150;
    //config.preventScrollbars = true;
    eBook.Worksheet.Fields.TextAreaField.superclass.constructor.call(this, config);
};
Ext.extend(eBook.Worksheet.Fields.TextAreaField, Ext.form.TextArea, eBook.Worksheet.Fields.Field);
Ext.reg('ewTextArea', eBook.Worksheet.Fields.TextAreaField);