eBook.Worksheet.Rules.DEFAULT_RULES = 'DEFAULT';
eBook.Worksheet.Rules.IMPORTPREVIOUS_RULES = 'IMPORTPREVIOUS';

eBook.Worksheet.Rules.Window = Ext.extend(eBook.Window, {
    mode: eBook.Worksheet.Rules.DEFAULT_RULES
    , initComponent: function() {
        Ext.apply(this, {
            modal: true,
            title: eBook.Worksheet.Rules.Window_Title,
            autoScroll: true,
            layout:'fit',
            items: [{
                xtype: 'eBook-createFile-actions'
                , ref: 'create'
                , autoScroll: true
                , title: eBook.Worksheet.Rules.Window_Title}]
            });
            eBook.Worksheet.Rules.Window.superclass.initComponent.apply(this, arguments);
            this.create.on('actionsDone', this.postCreate, this);
        }
    , show: function(mode, data) {
        this.mode = mode;
        eBook.Worksheet.Rules.Window.superclass.show.call(this);
        if (Ext.isDefined(data) && Ext.isArray(data)) {
            this.loadData(data);
            //this.performMass(data)
        } else {
            this.performAll();
        }
    }
    , performMass: function(data) {
        this.startedAt = new Date();
        this.getEl().mask('Loading', 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'InRuleMassExecuteDefaultRules'
                , method: 'POST'
                , params: Ext.encode({
                    cirmrdc: {
                        FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.currentFile.get('Culture')
                        , Worksheets:data
                    }
                })
                , callback: this.onPerformAllCallback
                , scope: this
        });
        
    }
    , onPerformMassCallback:function() {
        var tme = (this.staredAt.getTime() - (new Date()).getTime() )/1000;
        alert(tme);
        this.getEl().unmask(true);
        this.close();
    }
    , performAll: function(mode) {
        if (mode) this.mode = mode;
        // Get Run order worksheets.
        this.getEl().mask('Loading', 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'InRuleGetOrderedRunList'
                , method: 'POST'
                , params: Ext.encode({ cacdc: {
                    c: eBook.Interface.Culture
                    ,a:eBook.Interface.currentFile.get('AssessmentYear')
                }
                })
                , callback: this.onPerformAllCallback
                , scope: this
        });
    }
    , onPerformAllCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            this.loadData(robj.InRuleGetOrderedRunListResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
    }
    , loadData: function(data) {
        var acts = {
            ref: ''// this.ref
                , actions: []
                , revertActions: []
                , postActions: []
        };
        for (var i = 0; i < data.length; i++) {
            var act = {
                response: null
                , ignoreFailure:true
                , text: data[i].WorksheetTypeName
                , status: 'ready'
                , revertByFailure: false
                , params: {
                    cirbdc: {
                        FileId: eBook.Interface.currentFile.get('Id')
                                    , Culture: eBook.Interface.currentFile.get('Culture')
                                    , RuleApp: data[i].RuleApp
                    }
                }
            };

            switch (this.mode) {
                case eBook.Worksheet.Rules.DEFAULT_RULES:
                    act.action = 'InRuleExecuteDefaultRules';
                    break;
                case eBook.Worksheet.Rules.IMPORTPREVIOUS_RULES:
                    act.action = 'InRuleImportPreviousFile';
                    break;
            }

            if (act.action) acts.actions.push(act);
        }

        this.create.setActions(acts);
        this.create.start.defer(500, this.create);
    }
    , postCreate: function(success, resultActions, data) {
        eBook.Interface.clientMenu.reloadFilesAndOpen(eBook.Interface.currentFile.get('Id'),false);
        this.close.defer(500,this);
    }

});

Ext.reg('eBook.Worksheet.Rules.Window', eBook.Worksheet.Rules.Window);