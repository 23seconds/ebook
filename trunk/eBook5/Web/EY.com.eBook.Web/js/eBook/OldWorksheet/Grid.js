
eBook.Worksheet.GridPanel = Ext.extend(Ext.grid.GridPanel, {
    ruleApp: ''
    , collectionName: ''
    , initComponent: function() {
        var par = this.findParentByType(eBook.Worksheet.Window);
        this.manual = par.manual;
        var wcfg = eBook.Worksheet[this.ruleApp].meta[this.collectionName];
        this.ruleEngine = eBook.Worksheet[this.ruleApp].ruleEngine;
        this.entityName = wcfg.entity;
        this.notEditable = wcfg.attributes.ActionEdit == 'false';
        var cols = [{
            header: ' ',
            width: 20,
            dataIndex: 'status',
            renderer: function(v, m, r, ridx, cidx, s) {
                switch (v) {
                    case 1:
                        m.css = "eBook-icon-16-warning";
                        break;
                    case 2:
                        m.css = "eBook-icon-16-error";
                        break;
                }
                return "";
            },
            align: 'left'
}];
            cols = cols.concat(wcfg.columns);

            Ext.apply(this, {
                store: eBook.Worksheet[this.ruleApp].getStore(this.collectionName, this.id)
                , loadMask: true
                , columns: cols
                , listeners: {
                    'rowdblclick': {
                        fn: this.onRowDblClick
                        , scope: this
                    }
                }
                , sm: new Ext.grid.RowSelectionModel({
                    singleSelect: true
                        , listeners: {
                            'selectionchange': {
                                fn: this.onRowSelect
                                    , scope: this
                            }
                        }
                })
            });

            if (this.collectionAttributes.RowDisabledField) {
                this.rowDisabledField = this.collectionAttributes.RowDisabledField;
                Ext.apply(this, {
                    viewConfig: {
                        getRowClass: function(record, rowIndex, rp, ds) { // rp = rowParams
                            if (record.get(this.grid.rowDisabledField) == true) return "eBook-worksheet-grid-row-lock";
                            //if (record.get('status') == 1) return "eBook-worksheet-grid-row-warning";
                            return "";
                        }
                    }
                });
            }
            if (this.viewType) {
                Ext.apply(this, { view: new eBook.grid[this.viewType]({}) });
            }
            var closed = false;
            if (!this.manual) closed = eBook.Interface.currentFile.get('Closed');

            if (!this.isReadOnly && !closed) {
                Ext.apply(this, {
                    tbar: new Ext.Toolbar({

                        items: [
                        {
                            ref: 'additem',
                            text: eBook.Worksheet.GridPanel_AddItem,
                            iconCls: 'eBook-icon-add-16',
                            scale: 'small',
                            iconAlign: 'left',
                            handler: this.onAddItemClick,
                            scope: this,
                            hidden: wcfg.attributes.ActionAdd == 'false'
                        }, {
                            ref: 'edititem',
                            text: eBook.Worksheet.GridPanel_EditItem,
                            iconCls: 'eBook-icon-edit-16',
                            scale: 'small',
                            iconAlign: 'left',
                            disabled: true,
                            handler: this.onEditItemClick,
                            scope: this,
                            hidden: wcfg.attributes.ActionEdit == 'false'
                        }, {
                            ref: 'deleteitem',
                            text: eBook.Worksheet.GridPanel_DeleteItem,
                            iconCls: 'eBook-icon-delete-16',
                            scale: 'small',
                            iconAlign: 'left',
                            disabled: true,
                            handler: this.onDeleteItemClick,
                            scope: this,
                            hidden: wcfg.attributes.ActionDelete == 'false'
                        }
                    ]
                    })
                });
            }

            eBook.Worksheet.GridPanel.superclass.initComponent.apply(this, arguments);
        }
    , onAddItemClick: function() {
        if (this.findParentByType(eBook.Worksheet.Window).sheetClosed) return;
        this.findParentByType(eBook.Worksheet.EntityPanel).showForm();
    }
    , onEditItemClick: function() {
        if (this.findParentByType(eBook.Worksheet.Window).sheetClosed) return;
        var rec = this.getSelectionModel().getSelected();
        this.findParentByType(eBook.Worksheet.EntityPanel).showForm(rec);
    }
    , onDeleteItemClick: function() {
        if (this.findParentByType(eBook.Worksheet.Window).sheetClosed) return;
        var rec = this.getSelectionModel().getSelected();
        eBook.Worksheet[this.ruleApp].removeItem(this.collectionName, rec.get('ID'));
        var win = this.findParentByType(eBook.Worksheet.Window);
        this.manual = win.manual;
        
        var action = {
            text: String.format(eBook.Worksheet.GridPanel_DeletingItem, this.collectionName)
            , params: {
                cfrdc: {
                        Id: this.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                        , RowId: rec.get('ID')
                }
            }
            , action:  this.ruleEngine.baseMethod + this.collectionName + 'Remove'
            ,url:this.ruleEngine.url 
            // , record: e.record
        };
        win.addAction(action);
    }

    , onRowDblClick: function(grd, ridx, e) {
        if (this.findParentByType(eBook.Worksheet.Window).sheetClosed) return;
        if (this.notEditable) return;
        var rec = this.store.getAt(ridx);
        if (!Ext.isEmpty(this.rowDisabledField)) {
            if (rec.get(this.rowDisabledField)) {
                return;
            }
        }
        this.findParentByType(eBook.Worksheet.EntityPanel).showForm(rec);
    }
    , onRowSelect: function(sm) {
        /* TOOLBAR UPDATE */
        if (this.getTopToolbar()) {

            var ed = this.getTopToolbar().edititem;
            var dl = this.getTopToolbar().deleteitem;
            if (!sm.hasSelection()) {
                dl.disable();
                ed.disable();
            } else {
                dl.enable();
                ed.enable();
                if (!Ext.isEmpty(this.rowDisabledField)) {
                    var r = sm.getSelected();
                    if (r.get(this.rowDisabledField)) {
                        dl.disable();
                        ed.disable();
                        sm.clearSelections();
                    }
                }

            }
        }


    }
    , setSheetStatus: function(isClosed) {
        var tb = this.getTopToolbar();
        if (tb) {
            if (this.rendered) {
                tb.show();
                if (isClosed) tb.hide();
            } else {
                tb.hidden = isClosed;
            }
        }
    }
    });

    Ext.reg('eBook.Worksheet.GridPanel', eBook.Worksheet.GridPanel);