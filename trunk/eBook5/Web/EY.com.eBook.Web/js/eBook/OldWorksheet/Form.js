

eBook.Worksheet.FormPanel = Ext.extend(Ext.form.FormPanel, {
    constructor: function(config) {
        if (config) {

            if (config.items) {
                var blnSearchIDField = false;
                for (var i = 0; i < config.items.length; i++) {
                    if (config.items[i].name == 'ID') {
                        blnSearchIDField = true;
                        break;
                    }
                }
                if (!blnSearchIDField) {
                    config.items.unshift(new Ext.form.Hidden({ name: 'ID' }));
                }
            }
        }
        eBook.Worksheet.FormPanel.superclass.constructor.call(this, config);
    }
    , onEventTrigger: function(callerName, callerValue) {
        var cVal = '' + callerValue.toString();
        var cfg = eBook.Worksheet[this.ruleApp].meta[this.collectionName].formConfig.events;
        if (cfg != null) {

            var triggers = cfg.Visibility[callerName];
            if (Ext.isArray(triggers)) {
                for (var i = 0; i < triggers.length; i++) {
                    var rs = this.evaluateValues(cVal, triggers[i].values);
                    this.eventVisibility(triggers[i].fields, rs);
                }
            }

        }
        this.doLayout();
        return true;
    }
    , performEvents: function() {
        var cfg = eBook.Worksheet[this.ruleApp].meta[this.collectionName].formConfig.events;
        if (!cfg) return;
        var bf = this.getForm();
        var me = this;
        Ext.iterate(cfg.Visibility, function(k, v, o) {
            var fld = bf.findField(k);
            if (fld) {
                var vl = '' + fld.getValue();
                me.onEventTrigger(k, vl);
            }
        }, this);

    }
    , eventVisibility: function(fields, visible) {
        var baseForm = this.getForm();
        for (var k = 0; k < fields.length; k++) {
            var f = baseForm.findField(fields[k]);
            if (f != null) {
                if (visible) {
                    f.show();
                } else {
                    f.hide();
                }
            }
        }
    }
    , evaluateValues: function(valueToSearchFor, valuesList) {
        var vals = valuesList.split(',');
        if (Ext.isArray(vals)) {
            for (var j = 0; j < vals.length; j++) {
                if (vals[j].toLowerCase() == "{empty}" && Ext.isEmpty(valueToSearchFor)) return true;
                if (vals[j].toLowerCase() == valueToSearchFor.toLowerCase()) return true;
            }
        } else {
            if (vals == "{empty}" && Ext.isEmpty(valueToSearchFor)) return true;
            return vals.toLowerCase() == valueToSearchFor.toLowerCase();
        }
        return false;
    }
    , defaultOnAdd: false // get default data from server on adding
    , isStandalone: false // is a standalone (no grid) form
    , recordLoaded: false // has a loaded record
    , ruleApp: '' // ruleApplication name (shorthand for worksheettype
    , collectionName: '' // name of the root field
    , initComponent: function() {
        var wcfg = eBook.Worksheet[this.ruleApp].meta[this.collectionName];
        this.ruleEngine = eBook.Worksheet[this.ruleApp].ruleEngine;
        this.entityName = wcfg.entity;
        var par = this.findParentByType(eBook.Worksheet.Window);
        this.manual = par.manual;
        Ext.apply(this, {
            bodyStyle: 'padding:10px'
            , autoScroll: true
            , layoutConfig: { labelAlign: 'left', labelWidth: 200 }
            , labelWidth: 200
        });
        var closed = false;
        if (!this.manual) closed = eBook.Interface.currentFile.get('Closed');

        if (!closed) {
            Ext.apply(this, {
                buttons: [{
                    //text:'Bewaar gegevens',
                    ref: '../saveButton',
                    iconCls: 'eBook-window-save-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onValidate,
                    scope: this
                }, {
                    //text: 'Annuleer',
                    ref: '../cancelButton',
                    iconCls: 'eBook-window-cancel-ico',
                    scale: 'large',
                    iconAlign: 'top',
                    handler: this.onCancelClick,
                    scope: this
                }
                ]
            });
        }

        if (this.formLayout.toLowerCase() == 'formtable') {
            Ext.apply(this, {
                layout: 'tableform'
            });
            if (Ext.isDefined(wcfg.attributes.Columns)) this.layoutConfig.columns = parseInt(wcfg.attributes.Columns);
            if (Ext.isDefined(wcfg.attributes.ColumnWidths)) this.layoutConfig.columnWidths = Ext.decode('[' + wcfg.attributes.ColumnWidths + ']');
        }

        if (this.isStandalone) {


        }

        eBook.Worksheet.FormPanel.superclass.initComponent.apply(this, arguments);
    }
    , onValidate: function() {
        if (this.findParentByType(eBook.Worksheet.Window).sheetClosed) return;

        if (this.hasDirtyFields()) {
            var wn = this.findParentByType(eBook.Worksheet.Window);
            wn.mask('validating', 'x-mask-loading'); // eBook.Worksheet.FormPanel_UpdatingItem);
            var reqObj = { cedc: this.createObject() };
            //reqObj.ciredc.Person = eBook.User.getActivePersonDataContract();
            var url = this.ruleEngine.url + this.ruleEngine.baseMethod + this.entityName + 'Validate';

            Ext.Ajax.request({
                url: url //eBook.Service.url + action
                , method: 'POST'
                , params: Ext.encode(reqObj)
                , success: this.onValidateSuccess
                , failure: this.onValidateFailure
                , scope: this
            });
        }
    }
    , onSaveClick: function() {
        if (this.findParentByType(eBook.Worksheet.Window).sheetClosed) return;

        if (this.hasDirtyFields()) {
            // find worksheet window to mask
            var id = this.getIdField().getValue();
            var wn = this.findParentByType(eBook.Worksheet.Window);
            wn.mask(eBook.Worksheet.FormPanel_UpdatingItem, 'x-mask-loading');
            var reqObj = { cedc: this.createObject() };
            //reqObj.ciredc.Person = eBook.User.getActivePersonDataContract();

            var url = this.ruleEngine.url + this.ruleEngine.baseMethod + this.collectionName;
            url += (id == "ADDING" || Ext.isEmpty(id)) ? 'Add' : 'Update';
            //var action = url + ((id == "ADDING" || Ext.isEmpty(id)) ? "InRuleAddItem" : "InRuleUpdateItem");
            Ext.Ajax.request({
                url: url //eBook.Service.url + action
                , method: 'POST'
                , params: Ext.encode(reqObj)
                , success: this.onSaveSuccess
                , failure: this.onSaveFailure
                , scope: this
            });
        } else {
            //alert("no dirty fields");
        }
    }
    , onValidateSuccess: function(resp, opts) {
        // this.entityName
        var wn = this.findParentByType(eBook.Worksheet.Window);
        wn.unmask();
        var resultName = this.ruleEngine.baseMethod + this.entityName + 'ValidateResult'
        var o = Ext.decode(resp.responseText);
        var result = o[resultName];
        if (result.length == 0) {
            //success, can be saved
            this.onSaveClick();
        } else {
            alert("ERRORS - To be implemented");
        }

    }
    , onValidateFailure: function(resp, opts) {
        var wn = this.findParentByType(eBook.Worksheet.Window);
        wn.unmask();
        alert("validation failed");
    }
    , onSaveFailure: function(resp, opts) {
        // unmask worksheet window
        var wn = this.findParentByType(eBook.Worksheet.Window);
        wn.unmask();
        eBook.Interface.showResponseError(resp, this.title);
        // if is on add, get field messages.
        // else show error window

    }
    , onSaveSuccess: function(resp, opts) {

        //update worksheet data
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";
        //  this.loadData(o[resKey].Data);
        if (o[resKey]) {
            eBook.Worksheet[this.ruleApp].loadData(o[resKey]);
            //eBook.Worksheet[this.ruleApp].loadMessages(o[resKey].Messages);
            // Load messages apart.
            // clear form
            var ent = this.findParentByType(eBook.Worksheet.EntityPanel);
            if (!this.isStandalone) {
                this.clearFields();
                ent.showGrid();
            } else {
                this.loadStandalone();
            }
        } else {
            // eBook.Worksheet[this.ruleApp].loadMessages(o[resKey].Messages);
            //this.setFieldMessages();
            Ext.Msg.show({ icon: Ext.Msg.ERROR, modal: true, msg: eBook.Worksheet.FormPanel_ErrorsMsg, title: eBook.Worksheet.FormPanel_ErrorsTitle, buttons: Ext.Msg.OK });
        }

        // unmask worksheet window
        var wn = this.findParentByType(eBook.Worksheet.Window);
        wn.unmask();
    }
    , getFieldValues: function() {
        var o = {},
            n,
            key,
            val;
        this.getForm().items.each(function(f) {

            n = f.getName();
            key = o[n];
            val = f.getValue();
            if (f.getRecord) {
                if (!Ext.isEmpty(val) && val != "null") {
                    val = f.getRecord().data;
                } else {
                    val = null;
                }
            }
            if (Ext.isDefined(key)) {
                if (Ext.isArray(key)) {
                    o[n].push(val);
                } else {
                    o[n] = [key, val];
                }
            } else {
                o[n] = val;
            }
        });
        return o;
    }
    , createObject: function() {
        var obj = this.getFieldValues(); //getForm().getFieldValues(false);
        var wn = this.findParentByType(eBook.Worksheet.Window);
        this.manual = wn.manual;
        var nobj = {
            Id: this.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
            , Culture: eBook.Interface.Culture
            , CollectionPath: this.collectionName
            , Entity: {}
            //            , Id: this.getIdField().getValue()
        };


        for (var i in obj) {
            if (i != 'ID') {
                nobj.Entity[i] = obj[i];
            }
            /*
            if (Ext.isDate(obj[i])) {

                nobj.Entity.Fields.push({ Name: i, DateValue: obj[i] });
            } else {
            nobj.Entity.Fields.push({ Name: i, Value: obj[i] });
            }
            */
        }
        var id = this.getIdField().getValue();
        if (!Ext.isEmpty(id)) nobj.Entity.ID = id;
        return nobj;
    }
    , onCancelClick: function(silent) {
        var ent = this.findParentByType(eBook.Worksheet.EntityPanel);
        var rId = this.getIdField().getValue();
        var str = eBook.Worksheet[this.ruleApp].getStore(this.collectionName, this.id);
        if (this.isStandalone) {
            var rec = str.getAt(0);
            rec.reject();
            str.rejectChanges();
            this.loadRecord(rec);
        } else {
            if (rId != '') {
                var rec = str.getById(rId);
                if (rId == 'ADDING' && rec) {
                    // send remove request to server (window action)
                    str.remove(rec);
                    str.commitChanges();
                } else {
                    if (rec) rec.reject();
                    str.rejectChanges();
                }
            }
            this.getForm().reset();
        }

        ent.showGrid();
        eBook.Worksheet[this.ruleApp].load();
    }
    , createNew: function() {
        this.clearFields();
        if (this.defaultOnAdd) {
            // send request to server, update collection data, loadrecord
            this.getDefaultData();
        }
    }
    , getDefaultData: function() {
        /*
        var wn = this.findParentByType(eBook.Worksheet.Window);
        wn.mask("Adding default");
        Ext.Ajax.request({
        url: eBook.Service.url + "InRuleAddItemWithDefault"
        , method: 'POST'
        , params: Ext.encode({ cirdc: {
        FileId: this.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
        , Culture: eBook.Interface.Culture
        , RuleApp: this.ruleApp
        , BaseFieldName: this.collectionName
        , Manual: this.manual
        }
        })
        , callback: this.onDefaultDataResponse
        , scope: this
        });
        */
    }
    , onDefaultDataResponse: function(opts, success, resp) {
        var wn = this.findParentByType(eBook.Worksheet.Window);
        wn.unmask();
        if (success) {
            var robj = Ext.decode(resp.responseText);
            var dObj = {};
            var flds = robj.InRuleAddItemWithDefaultResult.Fields;
            for (var i = 0; i < flds.length; i++) {
                dObj[flds[i].Name] = flds[i].Value;
            }
            this.setValues(dObj);
        }
    }
    , clearFields: function() {
        this.getForm().applyToFields({ originalValue: null });
        this.getForm().reset();
    }
    , loadRecord: function(rec) {
        this.setValues(rec.data);
    }
    , setValues: function(values) {
        var bfrm = this.getForm();
        if (Ext.isArray(values)) {
            for (var i = 0, len = values.length; i < len; i++) {
                var v = values[i];
                var f = bfrm.findField(v.id);
                if (f) {

                    if (Ext.isFunction(f.getRecord)) {
                        f.setActiveRecord(values[id]);
                    } else {
                        f.setValue(v.value);
                    }
                    if (bfrm.trackResetOnLoad) {
                        f.originalValue = f.getValue();
                    }
                }
            }
        } else {
            var field, id;
            for (id in values) {
                if (!Ext.isFunction(values[id]) && (field = bfrm.findField(id))) {

                    if (Ext.isFunction(field.getRecord)) {
                        field.setActiveRecord(values[id]);
                    } else {
                        field.setValue(values[id]);
                    }
                    if (bfrm.trackResetOnLoad) {
                        field.originalValue = field.getValue();
                    }
                }
            }
        }
        return this;
    }
    , loadStandalone: function() {
        this.recordLoaded = false;
        var str = eBook.Worksheet[this.ruleApp].getStore(this.collectionName, this.id);
        if (str.getCount() == 1) {
            var rec = str.getAt(0);
            this.loadRecord(rec);
            this.getForm().items.each(function(f) {
                f.originalValue = f.getValue();
            });
            this.recordLoaded = true;
            this.setFieldMessages();
            return this;
        }
    }
    , saveStandalone: function() {
    }
    , getIdField: function() {
        return this.find('name', 'ID')[0];
    }
    , hasDirtyFields: function() {
        var obj = this.getForm().getFieldValues(true);
        for (var p in obj) {
            return true;
        }
        return false;
    }
    , isDataChanged: function() {

        var rId = this.getIdField().getValue();
        if (rId == 'ADDING') {
            return true;
        } else {
            return this.hasDirtyFields();
        }
    }
    , onChangeMessageResponse: function(btn, hasGrid) {
        if (btn == "yes") {
            this.onSaveClick();
        } else {
            this.onCancelClick(true);
        }
    }
    , setFieldMessages: function() {
        var win = this.findParentByType(eBook.Worksheet.Window);
        var f = this;
        //console.log('set messages');
        win.messages.store.each(function(rec) {
            if (rec.get('Field') && rec.get('Type') == 99) {
                var m = this.find('name', rec.get('Field'));
                if (Ext.isArray(m) && m.length == 1) {
                    var fld = m[0];
                    fld.markInvalid(rec.get('Text'));
                }
            }
        }, this);
    }
    , setSheetStatus: function(isClosed) {
        this.enable();
        if (isClosed) this.disable();
    }
    , focusField: function(fldName) {
        var fld = this.find('name', fldName);
        if (fld.length == 0) return;
        fld = fld[0];
        fld.focus(true);
    }
});

Ext.reg('eBook.Worksheet.FormPanel', eBook.Worksheet.FormPanel);