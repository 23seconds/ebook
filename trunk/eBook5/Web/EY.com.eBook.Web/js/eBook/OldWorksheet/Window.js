

eBook.Worksheet.Window = Ext.extend(eBook.Window, {
    ruleApp: ''
    , worksheetId: ''
    , manual: null
    , constructor: function(config) {
        this.ruleApp = config.ruleApp;
        this.panels = config.panels;
        delete config.ruleApp;
        delete config.panels;
        this.updatePanels();
        eBook.Worksheet.Window.superclass.constructor.call(this, config);
    }
    , loadJavascript: function() {
        //load javascript for worksheet (lazy load) (move to interface). Trigger & load before creation window
    }
    , updatePanels: function() {
        for (var i = 0; i < this.panels.length; i++) {
            Ext.apply(this.panels[i], { ruleApp: this.ruleApp });
        }
    }
    , initComponent: function() {
        var closeAccess = eBook.User.checkCloser(this.closingDepartment, eBook.Interface.currentClient.get('Shared'));
        if (!this.manual) closeAccess = closeAccess && eBook.Interface.currentFile.get('NumbersLocked');
        var def = eBook.Worksheet[this.ruleApp];
        closeAccess = closeAccess && (def.readBy == null || def.readBy.length == 0)

        var closed = false;
        if (!this.manual) closed = eBook.Interface.currentFile.get('Closed');
        Ext.apply(this, {
            layout: 'border'
            , sheetClosed: closed
            , items: [
                { xtype: 'eBook.Worksheet.TabPanel'
                    , activeTab: 0
                    , defaultType: 'worksheetEntityPanel'
                    , ref: 'tabPanel'
                    , items: [this.panels]
                    , region: 'center'
                }, { xtype: 'panel'
                    , split: true
                    , minWidth: 200
                    , region: 'east'
                    , autoScroll: true
                    , width: 200
                    , bodyStyle: 'overflow:auto;'
                    , title: eBook.Worksheet.Window_QualityControl
                    , items: [{ xtype: 'eBook.Worksheet.Messages'
                                , ref: '../messages'
                                , store: eBook.Worksheet[this.ruleApp].getMessagesStore()

}]
}]
            , tbar: new Ext.Toolbar({
                items: [{
                    ref: 'recalc',
                    text: eBook.Worksheet.Window_Recalc,
                    iconCls: 'eBook-icon-refreshworksheet-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onReCalcClick,
                    hidden: closed,
                    scope: this
                }, {
                    ref: 'importhistory',
                    text: eBook.Worksheet.Window_ImportHistory,
                    iconCls: 'eBook-icon-importproacc-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    hidden: (eBook.Worksheet[this.ruleApp].historyImport == 0 || closed),
                    handler: this.onImportHistoryClick,
                    scope: this
                }, {
                    ref: 'importbtw',
                    text: eBook.Worksheet.Window_ImportBTW,
                    iconCls: 'eBook-icon-importxml-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    hidden: (eBook.Worksheet[this.ruleApp].btwImport == 0 || closed),
                    handler: this.onImportBTWClick,
                    scope: this
                }, {
                    ref: 'expotrexcel',
                    text: eBook.Worksheet.Window_ExportExcel,
                    iconCls: 'eBook-excel-ico-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    hidden: eBook.Worksheet[this.ruleApp].excelExport != 1,
                    handler: this.onExportExcelClick,
                    scope: this
                }, {
                    ref: 'print',
                    text: eBook.Worksheet.Window_PrintPreview,
                    iconCls: 'eBook-icon-previewworksheet-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onPrintClick,
                    scope: this
                }, {
                    ref: 'closeWS',
                    text: eBook.Worksheet.Window_Close,
                    iconCls: 'eBook-icon-closeworksheet-24', //TO ADD
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onFinalCloseClick,
                    hidden: (closed || !closeAccess || this.manual != null),
                    scope: this
                }
                /*, {
                ref: 'uncloseWS',
                text: eBook.Worksheet.Window_UnClose,
                iconCls: 'eBook-icon-uncloseworksheet-24', //TO ADD
                scale: 'medium',
                iconAlign: 'top',
                handler: this.onUnCloseClick,
                hidden: !(closed && closeAccess && this.manual == null),
                scope: this
                }*/
                , {
                    ref: 'help',
                    text: eBook.Worksheet.Window_HelpInfo,
                    iconCls: 'eBook-icon-helpworksheet-24',
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onHelpClick,
                    scope: this
                }, {
                    ref: 'biztax',
                    text: 'BizTax', // eBook.Worksheet.Window_BizTax,
                    iconCls: 'eBook-icon-biztax-24', //TO ADD
                    scale: 'medium',
                    iconAlign: 'top',
                    handler: this.onPerformBizTax,
                    hidden: this.manual == null,
                    scope: this
}]
                })
            });
            eBook.Worksheet.Window.superclass.initComponent.apply(this, arguments);
            this.on('beforedestroy', this.onBeforeDestroy, this);
        }
     , onHelpClick: function() {
         Ext.Msg.alert(eBook.Worksheet.Window_HelpTitle, eBook.Worksheet.Window_HelpMsg);
     }
    , getStoreConfig: function(collection) {
        for (var i = 0; i < this.stores.length; i++) {
            if (this.stores[i].collectionName == collection) {
                return this.stores[i];
            }
        }
    }
    , onBeforeDestroy: function(me) {
        eBook.Worksheet[this.ruleApp].clear.defer(100, eBook.Worksheet[this.ruleApp]);
        delete this.ruleApp;
        delete this.panels;
        return true;
    }
    , updateTabStatus: function(sts) {
        this.tabPanel.updateTabStatus(sts);
    }
    , show: function() {
        eBook.Worksheet.Window.superclass.show.call(this, arguments);
        eBook.Splash.hide();
        if (eBook.Worksheet[this.ruleApp].isLoading) this.getEl().mask(eBook.Worksheet.Window_LoadingRules, 'x-mask-loading');
        this.tabPanel.getActiveTab().onActivated();
    }
    , close: function() {
        eBook.Worksheet[this.ruleApp].refreshReads.defer(200, eBook.Worksheet[this.ruleApp]);
        eBook.Worksheet.Window.superclass.close.call(this, arguments);
    }
    , selectMsgRecord: function(rec) {
        this.tabPanel.getActiveTab().selectMsgRecord(rec);
    }
    , filterMessages: function(collection, rowId) {
        this.messages.store.clearFilter();
        if (Ext.isDefined(rowId) && Ext.isDefined(collection)) {
            this.messages.store.filter({
                fn: function(record) {
                    return record.get('Collection') == collection
                                && record.get('RowIndex') == rowId;
                },
                scope: this
            });
        } else if (Ext.isDefined(collection)) {
            this.messages.store.filter({
                fn: function(record) {
                    return record.get('Collection') == collection
                            || record.get('Collection') == null
                            || !(record.get('Collection'));
                },
                scope: this
            });
        }
    }
    , onExportExcelClick: function() {
        this.getEl().mask('Exporting to excel', 'x-mask-loading');
        var pars = { cirbdc: {
            FileId: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
                    , RuleApp: this.ruleApp
                    , Person: eBook.User.getActivePersonDataContract()
        }
        };
        Ext.Ajax.request({
            url: eBook.Service.url + 'ExportExcelWorksheet'
                , method: 'POST'
                , params: Ext.encode(pars)
                , callback: this.onExportExcelDone
                , scope: this
        });
    }
     , onExportExcelDone: function(opts, success, resp) {

         this.getEl().unmask();
         if (success) {
             var robj = Ext.decode(resp.responseText);
             window.open("renderedPdfs/" + robj.ExportExcelWorksheetResult);
         } else {
             eBook.Interface.showResponseError(resp, this.title);
         }

     }
    , onImportBTWClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.getEl().mask('Importing BTW', 'x-mask-loading');
        var pars = { cirbdc: {
            FileId: eBook.Interface.currentFile.get('Id')
                , Culture: eBook.Interface.Culture
                , RuleApp: this.ruleApp
                , Person: eBook.User.getActivePersonDataContract()
        }
        };
        Ext.Ajax.request({
            url: eBook.Service.url + 'InRulePerformImportBTW'
            , method: 'POST'
            , params: Ext.encode(pars)
            , callback: this.onImportBTWDone
            , scope: this
        });
    }
    , onImportBTWDone: function(opts, success, resp) {
        var o = Ext.decode(resp.responseText);
        var resKey = "InRulePerformImportBTWResult";
        //  this.loadData(o[resKey].Data);
        if (o[resKey].Success) {
            eBook.Worksheet[this.ruleApp].loadData(o[resKey].Data);
            eBook.Worksheet[this.ruleApp].loadMessages(o[resKey].Messages);
        } else {
            eBook.Worksheet[this.ruleApp].loadMessages(o[resKey].Messages);
            eBook.Interface.showResponseError(resp, this.title);
            //Ext.Msg.show({ icon: Ext.Msg.ERROR, modal: true, msg: eBook.Worksheet.FormPanel_ErrorsMsg, title: eBook.Worksheet.FormPanel_ErrorsTitle, buttons: Ext.Msg.OK });
        }

        // unmask worksheet window
        this.getEl().unmask();
    }
    , onImportHistoryClick: function() {
        if (eBook.Interface.currentFile.get('Closed')) return;
        this.getEl().mask(eBook.Worksheet.Window_ImportingHistory, 'x-mask-loading');
        var pars = { cirbdc: {
            FileId: eBook.Interface.currentFile.get('Id')
                , Culture: eBook.Interface.Culture
                , RuleApp: this.ruleApp
                , Person: eBook.User.getActivePersonDataContract()
        }
        };
        Ext.Ajax.request({
            url: eBook.Service.url + 'ImportHistoryInInRule'
                    , method: 'POST'
                    , params: Ext.encode(pars)
                    , callback: this.onImportHistoryDone
                    , scope: this
        });
    }
    , onImportHistoryDone: function(opts, success, resp) {
        if (success) {
            eBook.Worksheet[this.ruleApp].onLoadSuccess(resp, opts);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
            this.getEl().unmask();
        }
    }
    , onPrintClick: function() {
        this.getEl().mask(eBook.Worksheet.Window_GeneratingPreview, 'x-mask-loading');

        var pars = { cpgdc: {
            fileId: this.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
                , culture: this.manual ? this.manual.File.c : eBook.Interface.currentFile.get('Culture')
                , item: {
                    __type: this.isFiche ? 'FicheItemDataContract:#EY.com.eBook.API.Contracts.Data' : 'WorksheetItemDataContract:#EY.com.eBook.API.Contracts.Data'
                    , id: eBook.NewGuid()
                    , man: this.manual ? this.manual : null
                    , title: this.title
                    , WorksheetType: this.typeId
                    , RuleApp: this.ruleApp
                    , FileId: this.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
                    , Culture: this.manual ? this.manual.File.c : eBook.Interface.currentFile.get('Culture')
                    , PrintLayout: 'DEFAULT'
                    , type: this.isFiche ? 'FICHE' : 'WORKSHEET'
                }
        }
        };
        if (this.manual) {
            pars.cpgdc.header = {
                Enabled: false
                , ShowTitle: false
            };
            pars.cpgdc.footer = {
                Enabled: false
                , ShowFooterNote: false
                , ShowPageNr: false
            };
        }

        Ext.Ajax.request({
            url: eBook.Service.bundle + 'GeneratePDF'
            , method: 'POST'
            , params: Ext.encode(pars)
            , callback: this.onPdfGenerated
            , scope: this
        });

    }
    , onPdfGenerated: function(opts, success, resp) {
        if (success) {
            var robj = Ext.decode(resp.responseText);
            window.open('renderedPdfs/' + robj.GeneratePDFResult);
        } else {
            eBook.Interface.showResponseError(resp, this.title);
        }
        this.getEl().unmask();
    }
    , onReCalcClick: function() {
        //if (eBook.Interface.currentFile.get('Closed')) return;
        eBook.Worksheet[this.ruleApp].load(this.manual);
    }
    , onUnCloseClick: function() {
        this.getEl().mask(eBook.Worksheet.Window_UnClosing, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'UnCloseWorksheet'
                , method: 'POST'
                , params: Ext.encode({ ccwdc: { FileId: this.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
                                                , WorksheetTypeId: this.typeId
                                                , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onUnCloseClickCallback
                , scope: this
        });
    }
    , onUnCloseClickCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            eBook.Worksheet.show.defer(300, eBook.Worksheet, [this.ruleApp, this.title]);
            eBook.Worksheet.Window.superclass.close.call(this, arguments);
        }
        else {
            eBook.Interface.showResponseError(resp, this.title);
        }

    }
    , onFinalCloseClick: function() {
        this.getEl().mask(eBook.Worksheet.Window_Closing, 'x-mask-loading');
        Ext.Ajax.request({
            url: eBook.Service.url + 'CloseWorksheet'
                , method: 'POST'
                , params: Ext.encode({ ccwdc: { FileId: this.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
                                                , WorksheetTypeId: this.typeId
                                                , Person: eBook.User.getActivePersonDataContract()
                }
                })
                , callback: this.onFinalCloseCallback
                , scope: this
        });
        // }
    }
    , onFinalCloseCallback: function(opts, success, resp) {
        this.getEl().unmask();
        if (success) {
            eBook.Worksheet[this.ruleApp].load();
        }
        else {
            eBook.Interface.showResponseError(resp, this.title);
        }

    }
    , sheetClosed: false
    , setSheetStatus: function(isClosed) {
        if (isClosed != this.sheetClosed) {
            this.sheetClosed = isClosed;
            var tb = this.getTopToolbar();
            var closeAccess = eBook.User.checkCloser(this.closingDepartment, eBook.Interface.currentClient.get('Shared'));
            if (!this.manual) closeAccess = closeAccess && eBook.Interface.currentFile.get('NumbersLocked');
            tb.recalc.enable();
            tb.importhistory.enable();
            tb.closeWS.hide();
            if (isClosed) {
                tb.recalc.disable();
                tb.importhistory.disable();
            } else {
                if (closeAccess) tb.closeWS.show();
            }
            this.tabPanel.items.each(function(it) {
                it.setSheetStatus(isClosed);
            }, this);
        }
    }
    , onPerformBizTax: function() {
        var t = new eBook.Xbrl.BizTaxManualWindow({});
        t.show(this.manual);
    }
    });


Ext.reg('eBook.Worksheet.Window', eBook.Worksheet.Window);
