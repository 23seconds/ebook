
eBook.Worksheet.TabPanel = Ext.extend(Ext.TabPanel, {
    initComponent: function() {
        Ext.apply(this, {
            enableTabScroll: true
        });
        eBook.Worksheet.TabPanel.superclass.initComponent.apply(this, arguments);
        this.on('beforetabchange', this.onTabChange, this);
    }
    , onTabChange: function(tp, newtab, currenttab) {
        if (currenttab) {
            if (currenttab.hasForm && currenttab.form.isVisible()) {
                var val = currenttab.form.isDataChanged();
                if (val) {
                    var msg = Ext.Msg.show.defer(10, Ext.Msg, [{
                        title: eBook.Worksheet.TabPanel_TabCloseTitle,
                        msg: eBook.Worksheet.TabPanel_TabCloseMsg,
                        buttons: Ext.Msg.YESNO,
                        fn: function(btn) { currenttab.form.onChangeMessageResponse(btn, currenttab.hasGrid); },
                        //animEl: this.buttons[1].getEl(),
                        scope: this,
                        icon: Ext.MessageBox.QUESTION//,
                        // modal:true
}]);
                    }
                    return !val;
                }
            }
            if (newtab.rendered) {
                if (newtab.hasForm && !newtab.hasGrid) {
                    newtab.form.loadStandalone();
                } else if (newtab.hasGrid) {
                    newtab.layout.setActiveItem(0);
                }
            }
            return true;
        }
    , updateTabStatus: function(sts) {
        //alert("updating");
        for (var i = 0; i < this.items.length; i++) {
            var it = this.get(i);
            if (Ext.isDefined(sts[it.collectionName])) {
                var t = Ext.get(this.getTabEl(i)).child(".x-tab-strip-text");
                t.removeClass("eBook-tab-error");
                t.removeClass("eBook-tab-warning");
                switch (sts[it.collectionName]) {
                    case 2:
                        t.addClass('eBook-tab-error');
                        break;
                    case 1:
                        t.addClass('eBook-tab-warning');
                        break;
                    

                }

            } else {

            }

        }
    }
    });
//Ext.extend(Ext.TabPanel,
    Ext.reg('eBook.Worksheet.TabPanel', eBook.Worksheet.TabPanel);