/*
/// * reference path="../../Ext/ext-base-debug.js"/>
/// * reference path="../../Ext/ext-all-debug.js"/>
/// * reference path="../eBook.js"/>
/// * reference path="../LazyLoad.js"/>
/// * reference path="../Service.js"/>
/// * reference path="../data/WcfProxy.js"/>
/// * reference path="../data/RecordTypes.js"/>
/// * reference path="../Language/ext-lang-nl-BE.js"/>
/// * reference path="Window.js"/>
/// * reference path="TabPanel.js"/>
/// * reference path="EntityPanel.js"/>
/// * reference path="Form.js"/>
*/

eBook.Worksheet.WorksheetItem = Ext.extend(Object, {
    ruleApp: ''
    , readBy: []
    , historyImport: 0
    , constructor: function(config) {
        Ext.apply(this, config);
        eBook.Worksheet.WorksheetItem.superclass.constructor.apply(this, arguments);

    }
    , isLoading: false
    , refreshReads: function() {
        if (this.readBy == null || this.readBy.length == 0) return;
        Ext.Msg.show({
            title: this.title,
            msg: eBook.Worksheet_UpdateLinkedSheets,
            buttons: Ext.Msg.YESNO,
            fn: this.executeRefreshReads,
            scope: this,
            icon: Ext.MessageBox.QUESTION
        });
    }
    , executeRefreshReads: function(btnid) {
        //console.log('Readby');
        if (btnid == 'yes') {
            var wn = new eBook.Worksheet.Rules.Window({ width: 400, height: 400 });
            wn.show(eBook.Worksheet.Rules.DEFAULT_RULES, this.readBy);
        }
    }
    , loadManual: function(manual) {
        this.isLoading = true;
        if (this.win) this.win.getEl().mask("LOADING", 'x-mask-loading');

        var pars = { cirbdc: {
            FileId: eBook.EmptyGuid
                , Culture: eBook.Interface.Culture
                , RuleApp: this.ruleApp
                , Manual: manual
        }
        };
        Ext.Ajax.request({
            url: eBook.Service.url + 'InRuleCreateOrUpdateManual'
                    , method: 'POST'
                    , params: Ext.encode(pars)
                    , success: this.onCreateOrUpdateManualSuccess
                    , failure: this.onCreateOrUpdateManualFailure
                    , scope: this
        });
    }
    , onCreateOrUpdateManualSuccess: function(resp, opts) {
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace("/ey.com.ebook.api.5/EbookService.svc/", "") + "Result";
        manual = o[resKey];
        if (this.win) this.win.manual = manual;
        this.load(manual);
        eBook.Interface.clientMenu.updateList('eBook-Client-manualworksheets-list');

    }
    , onCreateOrUpdateManualFailure: function(resp, opts) {
        alert("failed");
    }
    , firstLoad: function(manual) {
        if (manual) {
            this.loadManual(manual);
        } else {
            this.load();
        }
    }
    , load: function(manual) {
        //alert("test");
        if (!manual && this.myType == "MAN") manual = true;
        this.isLoading = true;
        if (this.win) this.win.getEl().mask("LOADING", 'x-mask-loading');
        var pars;
        if (manual) {
            pars = { cirbdc: {
                FileId: eBook.EmptyGuid
                    , Culture: eBook.Interface.Culture
                    , RuleApp: this.ruleApp
                    , Manual: manual
            }
            };
        } else {
            pars = { cfdc: {
                FileId: eBook.Interface.currentFile.get('Id')
                    , Culture: eBook.Interface.Culture
            }
            };
        }



        eBook.CachedAjax.request({
            url: this.ruleEngine.url + this.ruleEngine.baseMethod + 'GetData'
                    , method: 'POST'
                    , params: Ext.encode(pars)
                    , success: this.onLoadSuccess
                    , failure: this.onLoadFailure
                    , scope: this
        });
    }
    , transformData: function(src) {
        if (src == null) return null;
        var dt = { status: {} };
        for (var i = 0; i < src.Collections.length; i++) {
            var coll = src.Collections[i]
            if (!dt[coll.Name]) dt[coll.Name] = [];
            dt.status[coll.Name] = coll.Status;
            for (var j = 0; j < coll.Entities.length; j++) {
                var entObj = coll.Entities[j];
                var ent = { status: entObj.Status };
                for (var k = 0; k < entObj.Fields.length; k++) {
                    ent[entObj.Fields[k].Name] = entObj.Fields[k].Value;
                }
                dt[coll.Name].push(ent);
            }
        }
        return dt;
    }
    , onLoadSuccess: function(resp, opts) {
        var o = Ext.decode(resp.responseText);
        var resKey = opts.url.replace(this.ruleEngine.url, "") + "Result";
        //this.loadMessages(o[resKey].Messages);
        this.loadData(o[resKey]);
        this.isLoading = false;
        if (this.win) {
            var cl = false;
            if (eBook.Interface.currentFile) cl = eBook.Interface.currentFile.get('Closed')
            this.win.setSheetStatus(o[resKey].Closed || cl);
            this.win.getEl().unmask();
        }
        // alert("success");
    }
    , onLoadFailure: function(resp) {
        this.isLoading = false;
        if (this.win) this.win.getEl().unmask();
        eBook.Interface.showResponseError(resp);
    }
    , loadData: function(dta) {
        this.data = dta;
        this.reloadStores();
        if (this.win != null) this.win.tabPanel.getActiveTab().onActivated();
    }
    , loadMessages: function(dta) {
        if (this.messagesStore) {
            this.messagesStore.removeAll();
            if (dta) {
                this.messagesStore.loadData(dta);
            }
        }
    }
    , messagesStore: null
    , getMessagesStore: function() {
        if (this.messagesStore == null) {
            this.messagesStore = new Ext.data.JsonStore({
                fields: eBook.data.RecordTypes.Messages
                , idProperty: 'Id'
                , sortInfo: { field: 'Type', direction: 'DESC' }
            });
        }
        return this.messagesStore;
    }
    , reloadStores: function() {
        if (this.data) {
            for (var sid in this.stores) {
                if (Ext.isDefined(sid) && !(this.stores[sid] instanceof Function)) {
                    if (Ext.isDefined(this.stores[sid].collections)) {
                        this.stores[sid].loadData(this.getCollectionsData(this.stores[sid].collections));
                    } else {
                        if (this.data[this.stores[sid].root]) {
                            this.stores[sid].loadData(this.data);
                        } else {
                            this.stores[sid].removeAll();
                        }
                    }
                }
            }
            if (this.win != null) {
                //this.win.updateTabStatus(this.data.status);
            }
        }

    }
    // replace with server request in grid & loadData?
    , removeItem: function(collection, id) {
        if (this.data) {
            for (var i = this.data[collection].length - 1; i >= 0; i--) {
                if (this.data[collection][i].ID == id) {
                    this.data[collection].remove(this.data[collection][i]);
                    break;
                }
            }
            this.reloadStores();
        }
    }
    , stores: []
    , clear: function() {
        for (var i in this.stores) {
            if (!(this.stores[i] instanceof Function)) {
                this.stores[i].destroy();
                this.stores[i] = null;
                delete this.stores[i];
            }
        }
        this.messagesStore.destroy();
        this.messagesStore = null;
        this.data = null;
        this.win = null;
    }
    , meta: {}
    , findDataItem: function(collections, fieldName, fieldValue) {
        if (!Ext.isArray(collections)) {
            if (collections.indexOf(',') > -1) {
                collections = collections.split(',');
            } else {
                collections = [collections];
            }
        }
        for (var i = 0; i < collections.length; i++) {
            var coll = collections[i];
            if (Ext.isDefined(this.data[coll])) {
                for (var j = 0; j < this.data[coll].length; j++) {
                    var row = this.data[coll][j];
                    if (Ext.isDefined(row[fieldName])) {
                        if (row[fieldName] == fieldValue) return row;
                    }
                }
            }
        }
        return null;
    }

    , getCollectionsData: function(collections) {
        if (Ext.isArray(collections)) {
            collections = '|' + collections.join('|') + '|';
        }
        var arr = [];
        for (var t in this.data) {
            if (Ext.isDefined(t) && !(this.data[t] instanceof Function) && collections.indexOf('|' + t + '|') > -1) {
                arr = arr.concat(this.data[t]);
            }
        }
        return { cols: arr };
    }
    , getCollectionsStore: function(collections, displayField, valueField, id) {
        if (id && this.stores[id]) {
            return this.stores[id];
        }
        var flds = [{ name: 'ID', type: Ext.data.Types.STRING }
                      , { name: displayField, type: Ext.data.Types.STRING}];
        if (valueField != "ID") {
            flds.push({ name: valueField, type: Ext.data.Types.STRING });
        }
        this.stores[id] = new Ext.data.JsonStore({
            fields: flds
            , idProperty: 'ID'
            , root: 'cols'
            , collections: collections
        });
        if (this.data != null) {
            this.stores[id].loadData(this.getCollectionsData(collections));
        }
        return this.stores[id];
    }
    , getStore: function(collection, id) {
        if (id && this.stores[id]) {
            return this.stores[id];
        }
        this.stores[id] = new Ext.data.JsonStore({
            fields: this.meta[collection].recordType
                    , idProperty: 'ID'
                    , root: collection
        });
        if (this.data != null && this.data[collection]) {
            this.stores[id].loadData(this.data);
        }
        return this.stores[id];
    }
    , showWindow: function(manual) {

        this.showWindowDefered.defer(50, this, [manual]);
    }
    , showWindowDefered: function(manual) {
        this.firstLoad(manual);
        var panels = [];
        for (var i = 0; i < this.meta.Collections.length; i++) {
            if (!this.meta[this.meta.Collections[i]].hidden) {
                panels.push({
                    collectionName: this.meta.Collections[i]
                                , title: eBook.Language.Worksheets[this.ruleApp][this.meta.Collections[i]].title
                });
            }
        }

        this.win = new eBook.Worksheet.Window({
            ruleApp: this.ruleApp
                , panels: panels
                , title: this.title
                , myType: this.myType
                , isFiche: this.isFiche
                , isActive: this.isActive
                , typeId: this.typeId
                , fileId: manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
                , manual: manual
        });
        this.win.show();
    }
    , alert1: function() { alert("1"); }
});

// Lazy load of worksheet config
Ext.apply(eBook.Worksheet, {
    show: function(ruleApp, title, ass) {
        eBook.Splash.setText(title + " wordt opgemaakt.");
        eBook.Splash.show();
        var cb = {
            ruleApp: ruleApp
            , manual: null
        };
        if (!ass && eBook.Interface.currentFile) { ass = eBook.Interface.currentFile.get('AssessmentYear'); }
        eBook.LazyLoad.loadOnce.defer(50, this, ["js/RuleApps/" + ass + "/" + ruleApp + "." + eBook.Interface.Culture + ".js?" + eBook.SpecificVersion, this.performShow, cb, this, true]);
    }
    , showManual: function(ruleApp, title, ass, manual) {
        eBook.Splash.setText(title + " wordt opgemaakt.");
        eBook.Splash.show();
        if (!ass && eBook.Interface.currentFile) { ass = eBook.Interface.currentFile.get('AssessmentYear'); }
        var cb = {
            ruleApp: ruleApp
            , manual: manual
        };
        eBook.LazyLoad.loadOnce.defer(50, this, ["js/RuleApps/" + ass + "/" + ruleApp + "." + eBook.Interface.Culture + ".js?" + eBook.SpecificVersion, this.performShow,cb, this, true]);
    }
    , performShow: function(cb) {
        eBook.Worksheet[cb.ruleApp].showWindow(cb.manual);
    }
});