
eBook.Worksheet.MessagesTemplate = new Ext.XTemplate(
			'<tpl for=".">',
				'<div class="eBook-worksheet-messages-wrap" id="{Id}">',
				'<div class="{itemcls}">{Text}</div>',
				'</div>',
			'</tpl>'
		);

eBook.Worksheet.MessagesTemplate.compile();


eBook.Worksheet.Messages = Ext.extend(Ext.DataView, {
    initComponent: function() {
        Ext.apply(this, {
            title: 'Messages'
            , singleSelect: true
            , itemSelector: '.eBook-worksheet-messages-wrap'
            , emptyText: eBook.Worksheet.Messages_NoMessages
            , tpl: eBook.Worksheet.MessagesTemplate
            , overClass: 'eBook-worksheet-messages-wrap-over'
        });
        eBook.Worksheet.Messages.superclass.initComponent.apply(this, arguments);
        this.on('click', this.onItemClick, this);
    }
    , prepareData: function(data) {
        data.itemcls = 'eBook-icon-16-info';
        switch (data.Type) {
            case 50:
                data.itemcls = 'eBook-icon-16-warning';
                break;
            case 99:
                data.itemcls = 'eBook-icon-16-error';
                break;
        }
        return data;
    }
    , onItemClick: function(dv, idx, n, e) {
        var rec = this.store.getAt(idx);
        this.refOwner.selectMsgRecord(rec);
    }
});

Ext.reg('eBook.Worksheet.Messages', eBook.Worksheet.Messages);