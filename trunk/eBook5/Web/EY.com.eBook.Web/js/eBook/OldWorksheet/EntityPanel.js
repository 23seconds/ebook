/// <reference path="../../Ext/ext-base-debug.js"/>
/// <reference path="../../Ext/ext-all-debug.js"/>
/// <reference path="../eBook.js"/>
/// <reference path="../Service.js"/>
/// <reference path="../data/WcfProxy.js"/>
/// <reference path="../data/RecordTypes.js"/>
/// <reference path="../Language/ext-lang-nl-BE.js"/>
/// <reference path="Window.js"/>
/// <reference path="TabPanel.js"/>
/// <reference path="Form.js"/>
/// <reference path="Grid.js"/>


eBook.Worksheet.EntityPanel = Ext.extend(Ext.Panel, {
    isCollection: false
    , hasForm: false
    , hasGrid: false
    , ruleApp: ''
    , collectionName: 'defaultCollection'
    , isReadOnly: false
    , formLayout: 'default'
    , viewType: null
    , viewAttr: null
    , processAttributes: function(atts) {
        if (Ext.isDefined(atts)) {
            if (Ext.isDefined(atts.ReadOnly)) {
                this.isReadOnly = atts.ReadOnly.toLowerCase() == 'true';
            }
            if (Ext.isDefined(atts.Layout)) {
                this.formLayout = atts.Layout.toLowerCase();
            }
            if (Ext.isDefined(atts.ViewType)) {
                this.viewType = atts.ViewType.toLowerCase();
            }
        }
    }
    , selectMsgRecord: function(rec) {
        var itemId = rec.get('RowIndex');
        var fld = rec.get('Field');
        if (this.grid && this.grid.id == this.layout.activeItem.id && itemId) {
            var idx = this.grid.store.find("ID", itemId);
            if (idx > -1) {
                var sm = this.grid.getSelectionModel();
                sm.selectRow(idx, false);
                this.grid.getView().focusRow(idx);
            }
        } else if (this.form && this.form.id == this.layout.activeItem.id && fld) {
            this.form.focusField(fld);
        }
    }
    , initComponent: function() {
        var cfg = eBook.Worksheet[this.ruleApp].meta[this.collectionName];
        this.processAttributes(cfg.attributes);
        var par = this.findParentByType(eBook.Worksheet.Window);
        this.manual = par.manual;
        
        var previousFile = null;
        if(!this.manual) previousFile = eBook.Interface.currentFile.get('PreviousFileId');


        if (cfg.editable == 0 || (cfg.editable == -1 && !Ext.isEmpty(previousFile))) {
            this.isReadOnly = true;
        }
        var its = [];
        if (cfg.columns) {
            its.push({
                    xtype:'eBook.Worksheet.GridPanel'
                    ,ref: 'grid'
                    , collectionName: this.collectionName
                    , ruleApp: this.ruleApp
                    , isReadOnly: cfg.formConfig == null || this.isReadOnly
                    , viewType: this.viewType
                    , viewAttr: this.viewAttr
                    , collectionAttributes: cfg.attributes
                //, actionButtons: cfg.actionButtin
            });
            this.hasGrid = true;

        }

        if (cfg.formConfig && !this.isReadOnly) {
            this.hasForm = true;
            var flds = [];
            flds = flds.concat(cfg.formConfig.items);

            its.push({
                xtype:'eBook.Worksheet.FormPanel',
                ref: 'form',
                formLayout: this.formLayout,
                collectionName: this.collectionName,
                ruleApp: this.ruleApp,
                items: flds,
                isStandalone: cfg.columns == null,
                defaultOnAdd: Ext.isDefined(cfg.defaultOnAdd) ? cfg.defaultOnAdd : false
            });
        }
        Ext.apply(this, {
            //html: cfg.test
            layout: 'card'
            , activeItem: 0
            , items: its
        });
        eBook.Worksheet.EntityPanel.superclass.initComponent.apply(this, arguments);
        this.on('activate', this.onActivated, this);
        // this.on('deactivate', this.onDeactivated, this);
        /* if (this.items.getCount() > 1) {
        this.isCollection = true;
        } else {
        this.isCollection = false;
        }*/
    }
    , showForm: function(rec) {
        if (this.form) {
            this.layout.setActiveItem(this.form);
            if (rec) {
                this.form.loadRecord(rec);
            } else {
                this.form.createNew();
            }

            var win = this.findParentByType(eBook.Worksheet.Window);
            var id = this.form.getIdField().getValue();
            if (id == '') id = null;
            win.filterMessages(this.collectionName, id);

            this.form.setFieldMessages();
            this.doLayout();
            this.form.performEvents();
        }
    }
    , showGrid: function() {
        if (this.grid) {
            this.layout.setActiveItem(this.grid);
            var win = this.findParentByType(eBook.Worksheet.Window);
            win.filterMessages(this.collectionName);
        }
    }
    , onActivated: function() {
        // filter messages
        var win = this.findParentByType(eBook.Worksheet.Window);
        win.filterMessages(this.collectionName);

        if (this.hasForm && !this.hasGrid) {
            this.form.loadStandalone();
        }
    }
    , onDeactivated: function() {
        if (this.hasForm && !this.hasGrid) {
            //val = this.form.onDeactivate();
            if (!val) {

            }
        }
    }
    , setSheetStatus: function(isClosed) {
        this.items.each(function(its) {
            its.setSheetStatus(isClosed);
        }, this);
    }
});

Ext.reg('worksheetEntityPanel', eBook.Worksheet.EntityPanel);