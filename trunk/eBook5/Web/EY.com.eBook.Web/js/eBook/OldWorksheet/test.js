

Ext.ns('eBook.Language.Worksheets.eBookApp');

Ext.ns('eBook.Language.Worksheets.eBookApp.NormalCollection');
eBook.Language.Worksheets.eBookApp.NormalCollection.title = 'NormalCollection';

eBook.Language.Worksheets.eBookApp.NormalCollection.ID = 'ID';

eBook.Language.Worksheets.eBookApp.NormalCollection.Name = 'Name';

eBook.Language.Worksheets.eBookApp.NormalCollection.ListAccounts = 'ListAccounts';

eBook.Language.Worksheets.eBookApp.NormalCollection.Currency = 'Currency';

eBook.Language.Worksheets.eBookApp.NormalCollection.Boolean = 'Boolean';

eBook.Language.Worksheets.eBookApp.NormalCollection.Integer = 'Integer';

eBook.Language.Worksheets.eBookApp.NormalCollection.Date = 'Date';

eBook.Language.Worksheets.eBookApp.NormalCollection.DateTime = 'DateTime';

Ext.ns('eBook.Language.Worksheets.eBookApp.CollectionLinkedAction');
eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.title = 'CollectionLinkedAction';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.ID = 'ID';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Name = 'Name';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.ListCollections = 'ListCollections';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.ListAccounts = 'ListAccounts';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Currency = 'Currency';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Boolean = 'Boolean';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Integer = 'Integer';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Date = 'Date';

eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.DateTime = 'DateTime';

Ext.ns('eBook.Language.Worksheets.eBookApp.ReadOnlyCollection');
eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.title = 'ReadOnlyCollection';

eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.ID = 'ID';

eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Name = 'Name';

eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.ListAccounts = 'ListAccounts';

eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Currency = 'Currency';

eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Boolean = 'Boolean';

eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Integer = 'Integer';

eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Date = 'Date';

eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.DateTime = 'DateTime';

Ext.ns('eBook.Language.Worksheets.eBookApp.FormOnly');
eBook.Language.Worksheets.eBookApp.FormOnly.title = 'FormOnly';

eBook.Language.Worksheets.eBookApp.FormOnly.ID = 'ID';

eBook.Language.Worksheets.eBookApp.FormOnly.Name = 'Name';

eBook.Language.Worksheets.eBookApp.FormOnly.ListAccounts = 'ListAccounts';

eBook.Language.Worksheets.eBookApp.FormOnly.Currency = 'Currency';

eBook.Language.Worksheets.eBookApp.FormOnly.Boolean = 'Boolean';

eBook.Language.Worksheets.eBookApp.FormOnly.Integer = 'Integer';

eBook.Language.Worksheets.eBookApp.FormOnly.Date = 'Date';

eBook.Language.Worksheets.eBookApp.FormOnly.DateTime = 'DateTime';

eBook.Language.Worksheets.eBookApp.FormOnly.ListCollections = 'ListCollections';

Ext.ns('eBook.Language.Worksheets.eBookApp.FormOnlyColumn');
eBook.Language.Worksheets.eBookApp.FormOnlyColumn.title = 'FormOnlyColumn';

eBook.Language.Worksheets.eBookApp.FormOnlyColumn.ID = 'ID';

eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Name = 'Name';

eBook.Language.Worksheets.eBookApp.FormOnlyColumn.ListAccounts = 'ListAccounts';

eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Currency = 'Currency';

eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Boolean = 'Boolean';

eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Integer = 'Integer';

eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Date = 'Date';

eBook.Language.Worksheets.eBookApp.FormOnlyColumn.DateTime = 'DateTime';

Ext.ns('eBook.Language.Worksheets.eBookApp.FormOnlyTable');
eBook.Language.Worksheets.eBookApp.FormOnlyTable.title = 'FormOnlyTable';

eBook.Language.Worksheets.eBookApp.FormOnlyTable.ID = 'ID';

eBook.Language.Worksheets.eBookApp.FormOnlyTable.Name = 'Name';

eBook.Language.Worksheets.eBookApp.FormOnlyTable.ListAccounts = 'ListAccounts';

eBook.Language.Worksheets.eBookApp.FormOnlyTable.Currency = 'Currency';

eBook.Language.Worksheets.eBookApp.FormOnlyTable.Boolean = 'Boolean';

eBook.Language.Worksheets.eBookApp.FormOnlyTable.Integer = 'Integer';

eBook.Language.Worksheets.eBookApp.FormOnlyTable.Date = 'Date';

eBook.Language.Worksheets.eBookApp.FormOnlyTable.DateTime = 'DateTime';


eBook.Worksheet.eBookApp = new eBook.Worksheet.WorksheetItem({
    ruleApp: 'eBookApp'
        , meta: {
            Collections: ['NormalCollection', 'CollectionLinkedAction', 'ReadOnlyCollection', 'FormOnly', 'FormOnlyColumn', 'FormOnlyTable']

            , 'NormalCollection': {
                columns: [{
                    header: eBook.Language.Worksheets.eBookApp.NormalCollection.Name,
                    dataIndex: 'Name',
                    align: 'left'

                }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.NormalCollection.ListAccounts,
                            dataIndex: 'ListAccounts',
                            align: 'left'

                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.NormalCollection.Currency,
                            dataIndex: 'Currency',
                            align: 'left'
                            , xtype: 'numbercolumn', format: '0.00'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.NormalCollection.Boolean,
                            dataIndex: 'Boolean',
                            align: 'left'
                            , xtype: 'booleancolumn', falseText: '-', trueText: 'v'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.NormalCollection.Integer,
                            dataIndex: 'Integer',
                            align: 'left'
                            , xtype: 'numbercolumn', format: '0.00'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.NormalCollection.Date,
                            dataIndex: 'Date',
                            align: 'left'
                            , xtype: 'datecolumn', format: 'd/m/Y'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.NormalCollection.DateTime,
                            dataIndex: 'DateTime',
                            align: 'left'
                            , xtype: 'datecolumn', format: 'd/m/Y'
                        }
                        ]

                    , recordType:
                        [{
                            name: 'ID'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Name'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'ListAccounts'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Currency'
                              , type: Ext.data.Types.NUMBER
                        }
                        , {
                            name: 'Boolean'
                              , type: Ext.data.Types.BOOL
                        }
                        , {
                            name: 'Integer'
                              , type: Ext.data.Types.INT
                        }
                        , {
                            name: 'Date'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , {
                            name: 'DateTime'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , { name: 'status', type: Ext.data.Types.INT}]
                    , formConfig:
                        {
                            items: [
                                          {
                                              xtype: 'ewtextfield'
                                            , name: 'Name'
                                            , entityCollection: 'NormalCollection'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.NormalCollection.Name
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewList_Accounts'
                                            , name: 'ListAccounts'
                                            , entityCollection: 'NormalCollection'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.NormalCollection.ListAccounts
                                            , attributes: { 'List': 'Accounts', 'UIControlInfo': 'DropDownEdit' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewCurrency'
                                            , name: 'Currency'
                                            , entityCollection: 'NormalCollection'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.NormalCollection.Currency
                                            , attributes: { 'Interface': 'Currency' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewcheckbox'
                                            , name: 'Boolean'
                                            , entityCollection: 'NormalCollection'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.NormalCollection.Boolean
                                            , attributes: { 'Interfaces': 'Checkbox', 'UIControlInfo': 'CheckBox' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewnumberfield'
                                            , name: 'Integer'
                                            , entityCollection: 'NormalCollection'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.NormalCollection.Integer
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'Date'
                                            , entityCollection: 'NormalCollection'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.NormalCollection.Date
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'DateTime'
                                            , entityCollection: 'NormalCollection'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.NormalCollection.DateTime
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    , { xtype: 'hidden' // switch to'hidden'
                                      , name: 'ID'
                                      , value: 'hidden'
                                    }
                                ]
                          , layout: null
                          , events: null
                        }

                    , attributes: {}
                    , defaultOnAdd: false
            }
            , 'CollectionLinkedAction': {
                columns: [{
                    header: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Name,
                    dataIndex: 'Name',
                    align: 'left'

                }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.ListCollections,
                            dataIndex: 'ListCollections',
                            align: 'left'

                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.ListAccounts,
                            dataIndex: 'ListAccounts',
                            align: 'left'

                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Currency,
                            dataIndex: 'Currency',
                            align: 'left'
                            , xtype: 'numbercolumn', format: '0.00'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Boolean,
                            dataIndex: 'Boolean',
                            align: 'left'
                            , xtype: 'booleancolumn', falseText: '-', trueText: 'v'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Integer,
                            dataIndex: 'Integer',
                            align: 'left'
                            , xtype: 'numbercolumn', format: '0.00'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Date,
                            dataIndex: 'Date',
                            align: 'left'
                            , xtype: 'datecolumn', format: 'd/m/Y'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.DateTime,
                            dataIndex: 'DateTime',
                            align: 'left'
                            , xtype: 'datecolumn', format: 'd/m/Y'
                        }
                        ]

                    , recordType:
                        [{
                            name: 'ID'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Name'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'ListCollections'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'ListAccounts'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Currency'
                              , type: Ext.data.Types.NUMBER
                        }
                        , {
                            name: 'Boolean'
                              , type: Ext.data.Types.BOOL
                        }
                        , {
                            name: 'Integer'
                              , type: Ext.data.Types.INT
                        }
                        , {
                            name: 'Date'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , {
                            name: 'DateTime'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , { name: 'status', type: Ext.data.Types.INT}]
                    , formConfig:
                        {
                            items: [
                                          {
                                              xtype: 'ewtextfield'
                                            , name: 'Name'
                                            , entityCollection: 'CollectionLinkedAction'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Name
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewList_DropDownFieldsCopy'
                                            , name: 'ListCollections'
                                            , entityCollection: 'CollectionLinkedAction'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.ListCollections
                                            , attributes: { 'List': 'DropDownFieldsCopy', 'ListAttribute1': 'NormalCollection', 'ListAttribute2': 'Name', 'UIControlInfo': 'DropDownEdit' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewList_Accounts'
                                            , name: 'ListAccounts'
                                            , entityCollection: 'CollectionLinkedAction'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.ListAccounts
                                            , attributes: { 'List': 'Accounts', 'UIControlInfo': 'DropDownEdit' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewCurrency'
                                            , name: 'Currency'
                                            , entityCollection: 'CollectionLinkedAction'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Currency
                                            , attributes: { 'Interface': 'Currency' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewcheckbox'
                                            , name: 'Boolean'
                                            , entityCollection: 'CollectionLinkedAction'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Boolean
                                            , attributes: { 'Interfaces': 'Checkbox', 'UIControlInfo': 'CheckBox' }
                                            , ruleApp: 'eBookApp'
                                            , hasInterfaceEvents: true
                                          }


                                    ,
                                          {
                                              xtype: 'ewnumberfield'
                                            , name: 'Integer'
                                            , entityCollection: 'CollectionLinkedAction'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Integer
                                            , attributes: { 'InterfaceEvents': 'Visibility', 'InterfaceEventVisibility_Field': 'Boolean', 'InterfaceEventVisibility_Values': 'true' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'Date'
                                            , entityCollection: 'CollectionLinkedAction'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.Date
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'DateTime'
                                            , entityCollection: 'CollectionLinkedAction'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.CollectionLinkedAction.DateTime
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    , { xtype: 'hidden' // switch to'hidden'
                                      , name: 'ID'
                                      , value: 'hidden'
                                    }
                                ]
                          , layout: null
                          , events:
                                  {
                                      'Visibility': {
                                          'Boolean': [
                                           { values: 'true', fields: ['Integer'] }

                                          ]

                                      }

                                  }

                        }

                    , attributes: {}
                    , defaultOnAdd: false
            }
            , 'ReadOnlyCollection': {
                columns: [{
                    header: eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Name,
                    dataIndex: 'Name',
                    align: 'left'

                }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.ListAccounts,
                            dataIndex: 'ListAccounts',
                            align: 'left'

                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Currency,
                            dataIndex: 'Currency',
                            align: 'left'
                            , xtype: 'numbercolumn', format: '0.00'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Boolean,
                            dataIndex: 'Boolean',
                            align: 'left'
                            , xtype: 'booleancolumn', falseText: '-', trueText: 'v'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Integer,
                            dataIndex: 'Integer',
                            align: 'left'
                            , xtype: 'numbercolumn', format: '0.00'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.Date,
                            dataIndex: 'Date',
                            align: 'left'
                            , xtype: 'datecolumn', format: 'd/m/Y'
                        }
                        , {
                            header: eBook.Language.Worksheets.eBookApp.ReadOnlyCollection.DateTime,
                            dataIndex: 'DateTime',
                            align: 'left'
                            , xtype: 'datecolumn', format: 'd/m/Y'
                        }
                        ]

                    , recordType:
                        [{
                            name: 'ID'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Name'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'ListAccounts'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Currency'
                              , type: Ext.data.Types.NUMBER
                        }
                        , {
                            name: 'Boolean'
                              , type: Ext.data.Types.BOOL
                        }
                        , {
                            name: 'Integer'
                              , type: Ext.data.Types.INT
                        }
                        , {
                            name: 'Date'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , {
                            name: 'DateTime'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , { name: 'status', type: Ext.data.Types.INT}]
                    , formConfig: null
                    , attributes: { 'ReadOnly': 'true' }
                    , defaultOnAdd: false
            }
            , 'FormOnly': {
                columns: null
                    , recordType:
                        [{
                            name: 'ID'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Name'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'ListAccounts'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Currency'
                              , type: Ext.data.Types.NUMBER
                        }
                        , {
                            name: 'Boolean'
                              , type: Ext.data.Types.BOOL
                        }
                        , {
                            name: 'Integer'
                              , type: Ext.data.Types.INT
                        }
                        , {
                            name: 'Date'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , {
                            name: 'DateTime'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , {
                            name: 'ListCollections'
                              , type: Ext.data.Types.STRING
                        }
                        , { name: 'status', type: Ext.data.Types.INT}]
                    , formConfig:
                        {
                            items: [
                                          {
                                              xtype: 'ewtextfield'
                                            , name: 'Name'
                                            , entityCollection: 'FormOnly'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnly.Name
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewList_Accounts'
                                            , name: 'ListAccounts'
                                            , entityCollection: 'FormOnly'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnly.ListAccounts
                                            , attributes: { 'List': 'Accounts', 'UIControlInfo': 'DropDownEdit' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewCurrency'
                                            , name: 'Currency'
                                            , entityCollection: 'FormOnly'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnly.Currency
                                            , attributes: { 'Interface': 'Currency' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewcheckbox'
                                            , name: 'Boolean'
                                            , entityCollection: 'FormOnly'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnly.Boolean
                                            , attributes: { 'Interfaces': 'Checkbox', 'UIControlInfo': 'CheckBox' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewnumberfield'
                                            , name: 'Integer'
                                            , entityCollection: 'FormOnly'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnly.Integer
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'Date'
                                            , entityCollection: 'FormOnly'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnly.Date
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'DateTime'
                                            , entityCollection: 'FormOnly'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnly.DateTime
                                            , attributes: {}
                                            , ruleApp: 'eBookApp'

                                          }


                                    ,
                                          {
                                              xtype: 'ewList_Collections'
                                            , name: 'ListCollections'
                                            , entityCollection: 'FormOnly'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnly.ListCollections
                                            , attributes: { 'List': 'Collections', 'ListAttribute1': 'NormalCollection,CollectionLinkedAction,ReadOnlyCollection', 'ListAttribute2': 'Name', 'ListAttribute3': 'ID', 'UIControlInfo': 'DropDownEdit' }
                                            , ruleApp: 'eBookApp'

                                          }


                                    , { xtype: 'hidden' // switch to'hidden'
                                      , name: 'ID'
                                      , value: 'hidden'
                                    }
                                ]
                          , layout: null
                          , events: null
                        }

                    , attributes: {}
                    , defaultOnAdd: false
            }
            , 'FormOnlyColumn': {
                columns: null
                    , recordType:
                        [{
                            name: 'ID'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Name'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'ListAccounts'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Currency'
                              , type: Ext.data.Types.NUMBER
                        }
                        , {
                            name: 'Boolean'
                              , type: Ext.data.Types.BOOL
                        }
                        , {
                            name: 'Integer'
                              , type: Ext.data.Types.INT
                        }
                        , {
                            name: 'Date'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , {
                            name: 'DateTime'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , { name: 'status', type: Ext.data.Types.INT}]
                    , formConfig:
                        {
                            items: [
                                  {
                                      layout: 'column'
                                    , defaults: {
                                        columnWidth: 0.3333333333333333
                                      , layout: 'form'
                                      , border: false
                                    }
                                    , items: [
                                        { // column 1
                                            items: [

                                          {
                                              xtype: 'ewtextfield'
                                            , name: 'Name'
                                            , entityCollection: 'FormOnlyColumn'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Name
                                            , attributes: { 'column': '1' }
                                            , ruleApp: 'eBookApp'

                                          }

  ,
                                          {
                                              xtype: 'ewList_Accounts'
                                            , name: 'ListAccounts'
                                            , entityCollection: 'FormOnlyColumn'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyColumn.ListAccounts
                                            , attributes: { 'List': 'Accounts', 'column': '1', 'UIControlInfo': 'DropDownEdit' }
                                            , ruleApp: 'eBookApp'

                                          }

  ,
                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'DateTime'
                                            , entityCollection: 'FormOnlyColumn'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyColumn.DateTime
                                            , attributes: { 'column': '1' }
                                            , ruleApp: 'eBookApp'

                                          }


                                          ]
                                        }
                                      , { // column 2
                                          items: [

                                          {
                                              xtype: 'ewCurrency'
                                            , name: 'Currency'
                                            , entityCollection: 'FormOnlyColumn'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Currency
                                            , attributes: { 'Interface': 'Currency', 'column': '2' }
                                            , ruleApp: 'eBookApp'

                                          }

  ,
                                          {
                                              xtype: 'ewnumberfield'
                                            , name: 'Integer'
                                            , entityCollection: 'FormOnlyColumn'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Integer
                                            , attributes: { 'InterfaceEvents': 'Visibility', 'InterfaceEventVisibility_Field': 'Boolean', 'InterfaceEventVisibility_Values': 'true', 'column': '2' }
                                            , ruleApp: 'eBookApp'

                                          }


                                          ]
                                      }
                                      , { // column 3
                                          items: [

                                          {
                                              xtype: 'ewcheckbox'
                                            , name: 'Boolean'
                                            , entityCollection: 'FormOnlyColumn'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Boolean
                                            , attributes: { 'Interfaces': 'Checkbox', 'column': '3', 'UIControlInfo': 'CheckBox' }
                                            , ruleApp: 'eBookApp'
                                            , hasInterfaceEvents: true
                                          }

  ,
                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'Date'
                                            , entityCollection: 'FormOnlyColumn'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyColumn.Date
                                            , attributes: { 'column': '3' }
                                            , ruleApp: 'eBookApp'

                                          }


                                          ]
                                      }

                                        , { xtype: 'hidden' // switch to'hidden'
                                          , name: 'ID'
                                          , value: 'hidden'
                                        }

                                      ]
                                  }
      ]
                          , layout: 'columnform'
                          , events:
                                  {
                                      'Visibility': {
                                          'Boolean': [
                                           { values: 'true', fields: ['Integer'] }

                                          ]

                                      }

                                  }

                        }

                    , attributes: { 'Layout': 'columnform', 'Columns': '3' }
                    , defaultOnAdd: false
            }
            , 'FormOnlyTable': {
                columns: null
                    , recordType:
                        [{
                            name: 'ID'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Name'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'ListAccounts'
                              , type: Ext.data.Types.STRING
                        }
                        , {
                            name: 'Currency'
                              , type: Ext.data.Types.NUMBER
                        }
                        , {
                            name: 'Boolean'
                              , type: Ext.data.Types.BOOL
                        }
                        , {
                            name: 'Integer'
                              , type: Ext.data.Types.INT
                        }
                        , {
                            name: 'Date'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , {
                            name: 'DateTime'
                              , type: Ext.data.Types.WCFDATE
                        }
                        , { name: 'status', type: Ext.data.Types.INT}]
                    , formConfig:
                        {
                            items: [
                                  { xtype: 'panel'
                                  , layout: 'tableform'
                                  , layoutConfig: {
                                      columns: 3
                                  }
                                  , defaults: {
                                      borders: false
                                  }
                                  , items: [

                                          {
                                              xtype: 'ewLabelField'
                                            , name: 'Name_label'
                                            , entityCollection: 'FormOnlyTable'
                                            , text: eBook.Language.Worksheets.eBookApp.FormOnlyTable.Name
                                            , attributes: { 'LabelColspan': '1', 'LabelSeperate': 'true', 'ItemColspan': '2' }
                                            , ruleApp: 'eBookApp'

                                          },

                                          {
                                              xtype: 'ewtextfield'
                                            , name: 'Name'
                                            , entityCollection: 'FormOnlyTable'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyTable.Name
                                            , attributes: { 'LabelColspan': '1', 'LabelSeperate': 'true', 'ItemColspan': '2' }
                                            , ruleApp: 'eBookApp'
                                            , hideLabel: true
                                          }

  ,
                                          {
                                              xtype: 'ewLabelField'
                                            , name: 'ListAccounts_label'
                                            , entityCollection: 'FormOnlyTable'
                                            , text: eBook.Language.Worksheets.eBookApp.FormOnlyTable.ListAccounts
                                            , attributes: { 'List': 'Accounts', 'LabelColspan': '1', 'LabelSeperate': 'true', 'ItemColspan': '2', 'UIControlInfo': 'DropDownEdit' }
                                            , ruleApp: 'eBookApp'

                                          },

                                          {
                                              xtype: 'ewList_Accounts'
                                            , name: 'ListAccounts'
                                            , entityCollection: 'FormOnlyTable'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyTable.ListAccounts
                                            , attributes: { 'List': 'Accounts', 'LabelColspan': '1', 'LabelSeperate': 'true', 'ItemColspan': '2', 'UIControlInfo': 'DropDownEdit' }
                                            , ruleApp: 'eBookApp'
                                            , hideLabel: true
                                          }

  ,
                                          {
                                              xtype: 'ewCurrency'
                                            , name: 'Currency'
                                            , entityCollection: 'FormOnlyTable'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyTable.Currency
                                            , attributes: { 'Interface': 'Currency', 'ItemColspan': '1' }
                                            , ruleApp: 'eBookApp'

                                          }

  ,
                                          {
                                              xtype: 'ewcheckbox'
                                            , name: 'Boolean'
                                            , entityCollection: 'FormOnlyTable'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyTable.Boolean
                                            , attributes: { 'Interfaces': 'Checkbox', 'ItemColspan': '1', 'UIControlInfo': 'CheckBox' }
                                            , ruleApp: 'eBookApp'
                                            , hasInterfaceEvents: true
                                          }

  ,
                                          {
                                              xtype: 'ewnumberfield'
                                            , name: 'Integer'
                                            , entityCollection: 'FormOnlyTable'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyTable.Integer
                                            , attributes: { 'InterfaceEvents': 'Visibility', 'InterfaceEventVisibility_Field': 'Boolean', 'InterfaceEventVisibility_Values': 'true', 'ItemColspan': '1' }
                                            , ruleApp: 'eBookApp'

                                          }

  ,
                                          {
                                              xtype: 'ewLabelField'
                                            , name: 'Date_label'
                                            , entityCollection: 'FormOnlyTable'
                                            , text: eBook.Language.Worksheets.eBookApp.FormOnlyTable.Date
                                            , attributes: { 'LabelColspan': '2', 'LabelSeperate': 'true', 'ItemColspan': '1' }
                                            , ruleApp: 'eBookApp'

                                          },

                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'Date'
                                            , entityCollection: 'FormOnlyTable'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyTable.Date
                                            , attributes: { 'LabelColspan': '2', 'LabelSeperate': 'true', 'ItemColspan': '1' }
                                            , ruleApp: 'eBookApp'
                                            , hideLabel: true
                                          }

  ,
                                          {
                                              xtype: 'ewLabelField'
                                            , name: 'DateTime_label'
                                            , entityCollection: 'FormOnlyTable'
                                            , text: eBook.Language.Worksheets.eBookApp.FormOnlyTable.DateTime
                                            , attributes: { 'LabelSeperate': 'true', 'LabelColspan': '2', 'ItemColspan': '1' }
                                            , ruleApp: 'eBookApp'

                                          },

                                          {
                                              xtype: 'ewdatefield'
                                            , name: 'DateTime'
                                            , entityCollection: 'FormOnlyTable'
                                            , fieldLabel: eBook.Language.Worksheets.eBookApp.FormOnlyTable.DateTime
                                            , attributes: { 'LabelSeperate': 'true', 'LabelColspan': '2', 'ItemColspan': '1' }
                                            , ruleApp: 'eBookApp'
                                            , hideLabel: true
                                          }


                                  ]
                                  }

                                      , { xtype: 'hidden' // switch to'hidden'
                                        , name: 'ID'
                                        , value: 'hidden'
                                      }
                                    ]
                          , layout: 'tableform'
                          , events:
                                  {
                                      'Visibility': {
                                          'Boolean': [
                                           { values: 'true', fields: ['Integer'] }

                                          ]

                                      }

                                  }

                        }

                    , attributes: { 'Layout': 'tableform', 'Columns': '3' }
                    , defaultOnAdd: false
            }
        }
});
  