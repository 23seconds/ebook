/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="./Fields/Field.js"/>



eBook.Worksheet.Lists.DropDownFieldsCopy = Ext.extend(eBook.Worksheet.Lists.Collections, {
    constructor: function(config) {
        if (!Ext.isDefined(config)) config = {};
        if (!Ext.isDefined(config.attributes)) config.attributes = {};
        config.attributes.ListAttribute3 = 'ID';
        eBook.Worksheet.Lists.DropDownFieldsCopy.superclass.constructor.call(this,config);
        this.on('collapse', this.onCollapse);
    }
    , onCollapse: function(combo) {
        var di = eBook.Worksheet[this.ruleApp].findDataItem(this.collections,this.valueField, this.getValue());
        var frmp = this.findParentByType(eBook.Worksheet.FormPanel);
        if (frmp && di!=null) {
            frm = frmp.getForm();
            for (var it in di) {
                if (Ext.isDefined(it) && it != 'remove' && it!='ID') {
                    var f = frm.findField(it);
                    if (Ext.isDefined(f) && f != null) {
                        f.setValue(di[it]);
                    }
                }
            }
            frmp.performEvents();
        }
    }
});
Ext.reg('ewList_DropDownFieldsCopy', eBook.Worksheet.Lists.DropDownFieldsCopy);
Ext.reg('ewDropDownFieldsCopy', eBook.Worksheet.Lists.DropDownFieldsCopy);
