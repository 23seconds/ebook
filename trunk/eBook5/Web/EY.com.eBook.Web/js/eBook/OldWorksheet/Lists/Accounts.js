
eBook.Worksheet.Lists.Accounts = Ext.extend(eBook.Worksheet.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        if (cfg.attributes && cfg.attributes['ListAttribute1']) {
            cfg.storeMethod = 'GetAccountsListByRanges';
            cfg.criteriaParameter = 'cardc';
            cfg.mode = 'remote';
            cfg.queryParam = 'Query';
            cfg.storeParams = { FileId: eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
                            , Ranges: cfg.attributes.ListAttribute1.split(',')
            };
        } else {
            cfg.storeMethod = 'GetAccountsList';
            cfg.mode = 'remote';
            cfg.criteriaParameter = 'cadc';
            cfg.storeParams = { FileId: eBook.Interface.currentFile.get('Id')
                            , Culture: eBook.Interface.Culture
            };
            cfg.queryParam = 'Query';
        }
        // exclude zero saldi?
        if (cfg.attributes && cfg.attributes['ListAttribute2']) {
            if (cfg.attributes.ListAttribute2 == "true") {
                cfg.storeParams.IncludeZeroes = false;
            }
        }
        eBook.Worksheet.Lists.Accounts.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.schema
            })
            , minChars: 1
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , pageSize: 20
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheet.Lists.Accounts.superclass.initComponent.apply(this, arguments);
    }
    , getValue: function() {
        var val = "" + eBook.Worksheet.Lists.Accounts.superclass.getValue.call(this);
        var idx = val.indexOf(" ");
        if (idx > 0) {
            val = val.substr(0, idx);
        }
        return val;
    }
});
Ext.reg('ewList_Accounts', eBook.Worksheet.Lists.Accounts);