

eBook.Worksheet.Lists.Global = Ext.extend(eBook.Worksheet.Fields.Dropdown, {
    constructor: function(cfg) {

        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cldc';
        cfg.storeParams = { lik: cfg.attributes.ListAttribute1
                            ,lid:null
                            , c: eBook.Interface.Culture
                            , ay: eBook.Interface.currentFile.get('AssessmentYear')
                            , sd: null
                            ,ed:null
        };

        eBook.Worksheet.Lists.Global.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
            })
            , minChars: 3
            , displayField: 'name'
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheet.Lists.Global.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Global', eBook.Worksheet.Lists.Global);

