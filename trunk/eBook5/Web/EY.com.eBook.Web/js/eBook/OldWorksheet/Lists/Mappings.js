/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="./Fields/Field.js"/>



eBook.Worksheet.Lists.Mappings = Ext.extend(eBook.Worksheet.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        if (cfg.attributes) {
            cfg.storeMethod = 'GetMappingGroupItems';
            cfg.criteriaParameter = 'cmdc';
            cfg.mode = 'remote';
            cfg.storeParams = { Culture: eBook.Interface.Culture
                            , Key: cfg.attributes['ListAttribute1']
                            , Meta: Ext.isDefined(cfg.attributes['ListAttribute2']) ? cfg.attributes['ListAttribute2'] : null
            };
        } 
        eBook.Worksheet.Lists.Mappings.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: 'cmdc'
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , autoDestroy: true
            })
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheet.Lists.Mappings.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Mappings', eBook.Worksheet.Lists.Mappings);