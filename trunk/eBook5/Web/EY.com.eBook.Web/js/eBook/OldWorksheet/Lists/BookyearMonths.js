
eBook.Worksheet.Lists.BookyearMonths = Ext.extend(eBook.Worksheet.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetBookyearMonths';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cfdc';
        cfg.storeParams = { FileId: eBook.Interface.currentFile.get('Id') };

        eBook.Worksheet.Lists.BookyearMonths.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
            })
            , displayField: 'nl'
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , forceSelection: true
            , lazyRender: true
            , width: 200
            //, pageSize: 20
            //, listWidth: 300
        });
        eBook.Worksheet.Lists.BookyearMonths.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_BookyearMonths', eBook.Worksheet.Lists.BookyearMonths);
