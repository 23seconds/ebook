
eBook.Worksheet.Lists.AccountsFromMapping = Ext.extend(eBook.Worksheet.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes['ListAttribute1']) && Ext.isDefined(cfg.attributes['ListAttribute2'])) {
            cfg.storeMethod = 'GetAccountsListFilteredByMapping';
            cfg.criteriaParameter = 'cadc';
            cfg.mode = 'remote';
            cfg.queryParam = 'Query';
            cfg.storeParams = { FileId: eBook.Interface.currentFile.get('Id')
                        , Culture: eBook.Interface.Culture
                        , Meta: cfg.attributes.ListAttribute1
                        , Value: cfg.attributes.ListAttribute2
                        , WorksheetTypeId: cfg.worksheetTypeId
                        , WorksheetTypeRuleApp:cfg.ruleApp
            };
            if (Ext.isDefined(cfg.attributes['ListAttribute3'])) {
                if (eBook.isGuid(cfg.attributes['ListAttribute3'])) {
                    cfg.storeParams.WorksheetTypeId = cfg.attributes.ListAttribute3;
                }
                else {
                    cfg.storeParams.WorksheetTypeId = null;
                    cfg.storeParams.WorksheetTypeRuleApp = cfg.attributes.ListAttribute3;
                }
            }

        }
        eBook.Worksheet.Lists.AccountsFromMapping.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.PagedJsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.schema
            })
            , minChars: 1
            , displayField: eBook.Interface.Culture.substr(0, 2)
            , valueField: 'id'
            , typeAhead: true
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , pageSize: 20
            , listWidth: 300
            , width: 200
        });
        eBook.Worksheet.Lists.AccountsFromMapping.superclass.initComponent.apply(this, arguments);
    }
    , getValue: function() {
        var val = "" + eBook.Worksheet.Lists.Accounts.superclass.getValue.call(this);
        var idx = val.indexOf(" ");
        if (idx > 0) {
            val = val.substr(0, idx);
        }
        return val;
    }
});
Ext.reg('ewList_AccountsFromMapping', eBook.Worksheet.Lists.AccountsFromMapping);