/// <reference path="../../../Ext/ext-base-debug.js"/>
/// <reference path="../../../Ext/ext-all-debug.js"/>
/// <reference path="../../eBook.js"/>
/// <reference path="./Fields/Field.js"/>



eBook.Worksheet.Lists.Years = Ext.extend(eBook.Worksheet.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'GetYears';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cydc';
        cfg.storeParams = { FileId: cfg.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
                        , From: -1
                        , To: 1
                        , Base: 'Assessment'
        };
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes.ListAttribute1) && Ext.isDefined(cfg.attributes.ListAttribute2)) {
            cfg.storeParams.From = parseInt(cfg.attributes.ListAttribute1);
            cfg.storeParams.To = parseInt(cfg.attributes.ListAttribute2);
            if (Ext.isDefined(cfg.attributes.ListAttribute3)) {
                cfg.storeParams.Base = cfg.attributes.ListAttribute3;
            }
            if (Ext.isDefined(cfg.attributes.ListAttribute4)) {
                cfg.storeParams.Min = parseInt(cfg.attributes.ListAttribute4);
            }
            if (Ext.isDefined(cfg.attributes.ListAttribute5)) {
                cfg.storeParams.Max = parseInt(cfg.attributes.ListAttribute5);
            }
        }

        eBook.Worksheet.Lists.Years.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.GlobalListItem
                    , serviceUrl: eBook.Service.lists
            })
            , displayField: 'nl'
            , valueField: 'id'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , width: 200
            //, pageSize: 20
            //, listWidth: 100
        });
        eBook.Worksheet.Lists.Years.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Years', eBook.Worksheet.Lists.Years);

