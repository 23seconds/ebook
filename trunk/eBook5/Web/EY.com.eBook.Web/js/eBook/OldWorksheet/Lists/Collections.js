
eBook.Worksheet.Lists.Collections = Ext.extend(eBook.Worksheet.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.mode = 'local';
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes.ListAttribute1) && Ext.isDefined(cfg.attributes.ListAttribute2) && Ext.isDefined(cfg.attributes.ListAttribute3)) {
            cfg.collections = cfg.attributes.ListAttribute1.split(',');
            cfg.displayField = cfg.attributes.ListAttribute2;
            cfg.valueField = cfg.attributes.ListAttribute3;
        }

        eBook.Worksheet.Lists.Collections.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //
        var sid = String.format("{0}_{1}_{2}", this.ruleApp, this.entityCollection, this.name);
        Ext.apply(this, {
        store: eBook.Worksheet[this.ruleApp].getCollectionsStore(this.collections, this.displayField, this.valueField, sid)
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , width: 200
            , forceSelection: true
            //, pageSize: 20
            //, listWidth: 100
        });
        eBook.Worksheet.Lists.Collections.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_Collections', eBook.Worksheet.Lists.Collections);

