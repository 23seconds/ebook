

eBook.Worksheet.Lists.InnerRuleAppList = Ext.extend(eBook.Worksheet.Fields.Dropdown, {
    constructor: function(cfg) {
        if (!Ext.isDefined(cfg)) cfg = {};
        cfg.storeMethod = 'InRuleGetList';
        cfg.mode = 'remote';
        cfg.criteriaParameter = 'cirldc';
        cfg.storeParams = {
            FileId: cfg.manual ? eBook.EmptyGuid : eBook.Interface.currentFile.get('Id')
            , Culture: eBook.Interface.Culture
            , RuleApp: cfg.ruleApp
            , BaseFieldName: cfg.entityCollection
            , ListFieldName: cfg.name
        };
        if (Ext.isDefined(cfg.attributes) && Ext.isDefined(cfg.attributes.ListAttribute1) && Ext.isDefined(cfg.attributes.ListAttribute2)) {

        }

        eBook.Worksheet.Lists.InnerRuleAppList.superclass.constructor.call(this, cfg);
    },
    initComponent: function() {
        //

        var parfrm = this.findParentByType(eBook.Worksheet.FormPanel);
        if (parfrm && this.manual) {
            this.storeParams.Manual = parfrm.manual;
        }
        Ext.apply(this, {
            store: new eBook.data.JsonStore({
                selectAction: this.storeMethod
                , rootProp: 'Data'
                    , baseParams: this.storeParams
                    , criteriaParameter: this.criteriaParameter
                    , autoDestroy: true
                    , fields: eBook.data.RecordTypes.InRuleList
            })
            , displayField: 'Display'
            , valueField: 'Value'
            , typeAhead: false
            , triggerAction: 'all'
            , lazyRender: true
            , forceSelection: true
            , width: 200
            //, pageSize: 20
            //, listWidth: 100
        });
        eBook.Worksheet.Lists.InnerRuleAppList.superclass.initComponent.apply(this, arguments);
    }
});
Ext.reg('ewList_InRule', eBook.Worksheet.Lists.InnerRuleAppList);