﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Collections.Generic;
using EY.com.eBook.API.Contracts.Data;

[ServiceContract(Namespace = "")]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class eBookTest
{
	// Add [WebGet] attribute to use HTTP GET
	[OperationContract]
    [WebInvoke(BodyStyle=WebMessageBodyStyle.Wrapped,
        RequestFormat=WebMessageFormat.Json,
        ResponseFormat=WebMessageFormat.Json,
        UriTemplate="/GetPerson")]
	public Person GetPerson(string first, string last, int age)
	{
		// Add your operation implementation here
        return new Person(first, last, age);
	}

    [OperationContract]
    [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetClients")]
    public IList<ClientBaseDataContract> GetClients(CriteriaClientDataContract ccdc)
    {
        List<ClientBaseDataContract> list = new List<ClientBaseDataContract>();
        ClientBaseDataContract cbdc = new ClientBaseDataContract();
        //cbdc.Id = 49440208; replace with guid
        cbdc.Name = "Cala Capital";
        cbdc.GfisCode = "60709273";
        cbdc.Address = "Rue Edith Cavell 128";
        cbdc.ZipCode = "1180";
        cbdc.City = "BRUXELLES";

        list.Add(cbdc);
        return  list;
    }

    [OperationContract]
    [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetRelatives")]
    public IList<Person> GetRelatives(Person person)
    {
        System.Collections.Generic.List<Person> people = new System.Collections.Generic.List<Person>();

        people.Add(new Person("Joe", person.LastName, 41));
        people.Add(new Person("John", person.LastName, 33));
        people.Add(new Person("Jane", person.LastName, 30));

        return people;
    }

    [OperationContract]
    [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/SaveRelatives")]
    public Boolean SaveRelatives(Person[] people)
    {
        return true;
    }

	// Add more operations here and mark them with [OperationContract]
}
