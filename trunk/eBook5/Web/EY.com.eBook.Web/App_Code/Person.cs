﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for Person
/// </summary>
/// 
[DataContract]
public class Person
{
	public Person(string first, string last, int age) {


        FirstName = first;
        LastName = last;
        Age = age;
		
	}
    [DataMember]
    public string FirstName { get; set; }
    [DataMember]
    public string LastName { get; set; }
    [DataMember]
    public int Age { get; set; }
}
