﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Configuration;

public partial class Upload : System.Web.UI.Page
{
    public class MessageOb
    {

        public bool success { get; set; }

        public string message { get; set; }

        public string file { get; set; }

        public string extension { get; set; }

        public string contentType { get; set; }

        public string originalFile { get; set; }

        public Guid id { get; set; }

    }



    public class JSONHelper
    {

        public static string Serialize<T>(T obj)
        {

            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());

            MemoryStream ms = new MemoryStream();

            serializer.WriteObject(ms, obj);

            string retVal = Encoding.UTF8.GetString(ms.ToArray());

            return retVal;

        }



        public static T Deserialize<T>(string json)
        {

            T obj = Activator.CreateInstance<T>();

            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));

            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());

            obj = (T)serializer.ReadObject(ms);

            ms.Close();

            return obj;

        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {

        MessageOb result = new MessageOb();

        HttpContext context = HttpContext.Current;

        string fileType = context.Request.Params["fileType"];
        string keepOriginalFileName = context.Request.Params["keepOriginalFileName"];

        if (context.Request.Files.Count > 0)
        {
            bool cont = true;

            HttpPostedFile file = context.Request.Files[0];
            if (file.ContentLength > 0 ) //&& file.ContentType == "application/pdf")
            {
                if (!string.IsNullOrEmpty(fileType))
                {
                    switch (fileType.ToLower())
                    {
                        case "excel":
                            if (!file.ContentType.ToLower().Contains("excel") && !file.ContentType.ToLower().Contains("spreadsheet"))
                            {
                                result.success = false;
                                result.message = "invalid contenttype";
                                cont = false;
                            }
                            break;
                        case "pdf":
                            if (file.ContentType.ToLower() != "application/pdf")
                            {
                                result.success = false;
                                result.message = "invalid contenttype";
                                cont = false;
                            }
                            break;
                        case "txt":

                            break;
                        default:
                            result.message = "unknown filetype";
                            cont = false;
                            break;

                    }
                    
                }
                if (cont)
                {
                    result.originalFile = Path.GetFileName(file.FileName);
                    result.id = Guid.NewGuid();
                    result.extension = Path.GetExtension(file.FileName);
                    result.contentType = file.ContentType;
                    result.file = string.Format("{0}{1}", result.id.ToString(), Path.GetExtension(file.FileName));
                    
                    if (!string.IsNullOrEmpty(keepOriginalFileName)) result.file = result.originalFile;
                    //result.file = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(file.FileName));
                    byte[] binaryWriteArray = new byte[file.InputStream.Length];
                    file.InputStream.Read(binaryWriteArray, 0, (int)file.InputStream.Length);
                    string path = Path.Combine(EY.com.eBook.Core.Config.UploadFolder, result.file);
                    FileStream objfilestream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);
                    //Write the file and close it
                    objfilestream.Write(binaryWriteArray, 0, binaryWriteArray.Length);
                    objfilestream.Close();
                    result.message = "";
                    result.success = true;
                }
            }



        }



        // prepare response
        
        
        Literal1.Text = JSONHelper.Serialize<MessageOb>(result);



    }

}





